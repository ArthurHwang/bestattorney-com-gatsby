describe("bestatto-gatsby.netlify.app", () => {
  it("should visit about us from nav", () => {
    cy.visit("/")
      // .wait(10000)
      .get("nav")
      // .wait(10000)
      .contains(/about us/i)
      .click()
      .findByText(/our mission statement:/i)
  })
  it("should visit reviews and ratings page", () => {
    cy.visit("/")
      .findByText(/see all awards/i)
      .click()
      .findByText(/speak to one of our award-winning attorneys/i)
  })
  it("should visit the blog", () => {
    cy.visit("/")
      .get("nav ul li:nth-child(8)")
      .contains(/blog/i)
      .click()
      .findByText(/bisnar chase - california personal injury blog/i)
  })
})
