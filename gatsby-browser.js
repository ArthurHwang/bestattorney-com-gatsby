// exports.disableCorePrefetching = () => true

// NavContext is injected here to stop navbar hover bug
import React from "react"
import { NavProvider } from "./src/context/NavContext"
export const wrapRootElement = ({ element }) => (
  <NavProvider>{element}</NavProvider>
)
