const babelOptions = {
  //   presets: ["babel-preset-gatsby", "@babel/preset-typescript"]
  presets: ["babel-preset-gatsby"]
}
module.exports = require("babel-jest").createTransformer(babelOptions)
