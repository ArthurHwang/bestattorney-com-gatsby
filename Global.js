import { createGlobalStyle } from "styled-components"
import { normalize } from "polished"

const GlobalStyle = createGlobalStyle`
${normalize()}
  *,
  *::before,
  *::after {
    box-sizing: inherit;
    margin: 0;
    padding: 0;
  }

  html {
    box-sizing: border-box;
  }

  body {
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    line-height: 1.5;
  }

  h1 {
    /* For some reason typography.js is not applying H1 styles, adding here instead */
    font-style: italic;
    margin-bottom: 0;
    font-size: 2.7rem;
    text-transform: uppercase;
    letter-spacing: -0.5px;
    border-bottom: 2px solid #EC7D29;
    padding-bottom: 0.1rem;

    @media (max-width: 768px) {
      font-size: 2.5rem !important;
      text-align: center !important;
    }

    @media (max-width: 500px) {
      font-size: 2.2rem !important;
      text-align: center !important;
    }
  }

  h2 {
    text-transform: uppercase;
    border-bottom: medium double #8E8E8E;
    padding-bottom: 0.5rem;
    clear: both;
    line-height: 1;

    @media (max-width: 768px) {
      text-align: center !important;
      font-size: 2.2rem !important;
    }
    @media (max-width: 500px) {
      font-size: 1.8rem !important;
      text-align: center !important;
    }
  }


  h3 {
    margin-bottom: 0.5rem;
    line-height: 1.4;
  }

  h4 {
    line-height: 1;
  }

  p, li {
    text-align: justify;

    @media(max-width: 700px) {
      font-size: 1.2rem;
    }
  }

  ul.bullet-inside {
    list-style-position:inside; 
    margin-left: 2px; 
    display: table;

    li {
      margin-left: 26px; text-indent: -23px;
      text-align: left;
    }
  }

  ul, ol {
    @media(max-width: 768px) {
      margin-left: 0 !important;
      list-style-position: inside;
      
    }
  }

  a {
    color: ${({ theme }) => theme.links.normal};
    text-decoration: none;
  }

  a:visited {
    color: ${({ theme }) => theme.links.normal};
  }

  a:hover,
  a:active {
    color: ${({ theme }) => theme.links.hoverOrange};
    cursor: pointer;
    transition: color 0.2s linear;
    text-decoration: none;
  }

  img {
    @media (max-width: 700px) {
      float: none !important;
      display: block;
      margin: 0 auto 2rem;
    }
  }

  .breadcrumb-wrapper {
    margin-bottom: 2rem;

    @media(max-width: 700px) {
    text-align: center;
    }
  }

  .clearfix {
    clear: both;
  }

  .imgleft-fixed {
    float: left;
    margin: 0.5rem 2rem 0 0;

    @media (max-width: 768px) {
      float: none;
      display: block;
      margin: 0 auto 1.6rem !important;
    }
  }

  .imgright-fixed {
    float: right;
    margin: 0.5rem 0 0 2rem;

    @media (max-width: 768px) {
      float: none;
      display: block;
      margin: 0 auto 1.6rem !important;
    }
  }


  .imgcenter-fixed {
    display: block;
    text-align: center;
    margin: 0 auto 2rem;

    @media (max-width: 768px) {
      margin: 0 auto 1.6rem !important;
    }
  }

  .imgright-fluid {
    float: right;
    margin: 0.5rem 0 0 2rem;

      @media (max-width: 768px) {
      float: none;
      display: block;
      margin: 0 auto 2rem !important;
      width: 100%;
    }
  }

  .imgleft-fluid {
    float: left;
    margin: 0.5rem 2rem 0 0;

    @media (max-width: 768px) {
      float: none;
      display: block;
      margin: 0 auto 2rem !important;
      width: 100%;
    }
  }

  .imgcenter-fluid {
    display: block;
    text-align: center;
    margin: 0 auto 2rem;

    @media (max-width: 768px) {
      width: 100% ;
      margin: 0 auto 2rem;
    }
  }

  .mb {
    margin-bottom: 2rem;
  }

  .ytube {
    width: 100%;
    height: 400px;

    @media (max-width: 700px) {
      height: 200px;
    }
  }

  .panel-heading {
    font-size: 1.6rem;
    @media (max-width: 700px) {
      text-align: center;
    }
  }

  .mini-header-image {
    overflow: hidden;
    margin-bottom: 1rem;

    img {
      width: 100%;
      margin-bottom: 0;
    }

    .banner-image {
      min-height: 200px;
    }
  }

  thead th {
    font-size: 1.5rem;
    @media (max-width: 700px) {
      text-align: center;
    }
  }

  tbody td {
    font-size: 1.2rem;
    @media (max-width: 700px) {
      text-align: center;
    }
  }

  .content-well {
    padding: 15px 20px ;
    box-shadow: 3px 3px 8px -1px #999, 0px 0px 10px 0px #e8ddbc inset;
    border: 1px solid #d4cebe;
    border-radius: 6px;
    background-color: #fffbf1;
    margin-bottom: 10px;
    background-position: center 0%;
  }

   .text-header {
    padding: 150px 20px 20px ;
    border: none;
    background-size: cover ;

    @media(max-width: 768px) {
      padding: 95px 20px 20px ;
    }
  }

  .text-header h2 {
    color: #c1b19a ;
    font-size: 28px ;
    text-shadow: 1px 1px 0px #423104, 2px 2px 0px #352615;
  }

  .content-well-margin-bottom {
    margin-bottom: 50px ;
  }


  blockquote {
    margin-bottom: 2rem;
  }

  iframe {
    background-color: #eee;

    @media(max-width: 700px) {
      height: 250px;
    }
  }

  td,
  th {
    border: 1px solid #050505;
    text-align: center;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #dddddd;
  }

  /* FOR GOOGLE MAPS ADDRESS (ITS UGLY) */
  .gmap-addr {
    display: none;
  }

/* FOR PAGES WITH JUMP BUTTONS */
  .nav-button {
    font-size: 1.4rem !important;
    width: 100%;
    margin-bottom: 1rem;
    margin: 1rem auto;
    display: flex;
    justify-content: space-between;

    .arrow-right {
      position: relative;
      left: 4px;
      top: 2px;

      @media (max-width: 1340px) {
        display: block;
        margin: 0 auto;
      }
    }

    @media (max-width: 1340px) {
      height: auto;
      display: block;
      padding: 0.5rem !important;
    }

    @media (max-width: 700px) {
      font-size: 1.2rem !important;
    }
  }
/* THIS HAS TO EXIST TO GET LAZY LOAD BLUR TO WORK */
  .lazyload-img-wrapper {
    display: block !important;
    width: 100% !important;
  }
`

export default GlobalStyle
