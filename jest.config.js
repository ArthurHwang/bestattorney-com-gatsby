// module.exports = {
//   transform: {
//     "^.+\\.jsx?$": `<rootDir>/tests/jest-preprocess.js`
//   },
//   moduleNameMapper: {
//     ".+\\.(css|styl|less|sass|scss)$": `identity-obj-proxy`,
//     ".+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": `<rootDir>/tests/file-mock.js`
//   },
//   testPathIgnorePatterns: [`node_modules`, `.cache`, `public`],
//   transformIgnorePatterns: [`node_modules/(?!(gatsby)/)`],
//   globals: {
//     __PATH_PREFIX__: ``
//   },
//   testURL: `http://localhost`
//   //   setupFiles: [`<rootDir>/loadershim.js`]
// }

module.exports = {
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.jsx?$": "<rootDir>/tests/jest-preprocess.js"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.([tj]sx?)$",
  moduleNameMapper: {
    ".+\\.(css|styl|less|sass|scss)$": "identity-obj-proxy",
    ".+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/tests/file-mock.js"
  },
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  testPathIgnorePatterns: ["node_modules", ".cache", "public"],
  coveragePathIgnorePatterns: ["utilities"],
  transformIgnorePatterns: ["node_modules/(?!(gatsby)/)"],
  globals: {
    __PATH_PREFIX__: ""
  },
  modulePaths: ["<rootDir>"],
  testURL: "http://localhost",
  setupFiles: ["<rootDir>/tests/loadershim.js"]
}
