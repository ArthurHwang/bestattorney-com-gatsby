require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
})

const path = require("path")
const createPaginatedPages = require("gatsby-paginate")

// Implement the Gatsby API “onCreatePage”. This is
// called after every page is created.
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/app/)) {
    page.matchPath = "/admin/*"

    // Update the page.
    createPage(page)
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  actions.createTypes(`
    type SitePage implements Node @dontInfer {
      path: String!
    }
  `)
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const { data, errors } = await graphql(
    `
        {
          allWpPost(limit: ${process.env.BLOG_LIMIT}) {
            edges {
              node {
                id
                slug
                title
                content
                excerpt
                date(formatString: "MMMM Do, YYYY")
                modified(formatString: "MMMM Do, YYYY")
                categories {
                  nodes {
                    name
                  }               
                }
                author {
                  node {
                    name
                  }               
                }
               featuredImage {
                 node {
                   localFile {
                     childImageSharp {
                       fluid(srcSetBreakpoints: [900]) {
                         src
                         srcSet
                         sizes
                         aspectRatio
                         tracedSVG
                       }
                     }
                   }
                 }
               }
              }
            }
          }
        }
      `
  )

  if (errors) {
    return Promise.reject(errors)
  }

  createPaginatedPages({
    edges: data.allWpPost.edges,
    createPage: createPage,
    pageTemplate: "./src/components/elements/blog/Blog_PostsTemplate.tsx",
    pageLength: 10,
    pathPrefix: "/blog"
  })

  data.allWpPost.edges.forEach(({ node }) => {
    createPage({
      path: "blog/" + node.slug,
      component: path.resolve(
        "./src/components/elements/blog/Blog_PostTemplate.tsx"
      ),
      context: {
        slug: node.slug,
        imageSharp: node.featuredImage,
        title: node.title,
        author: node.author.name,
        categories: node.categories,
        modified: node.modified,
        content: node.content,
        excerpt: node.excerpt,
        id: node.id
      }
    })

    // createPage({
    //   path: `blog/${node.slug}/amp`,
    //   component: path.resolve(
    //     "./src/components/elements/blog/Blog_PostTemplate-amp.tsx"
    //   ),
    //   context: {
    //     slug: node.slug,
    //     imageSharp: node.featured_media,
    //     title: node.title,
    //     author: node.author.name,
    //     categories: node.categories,
    //     modified: node.modified,
    //     content: node.content,
    //     excerpt: node.excerpt
    //   }
    // })
  })
}
