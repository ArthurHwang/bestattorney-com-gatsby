import React, { useReducer } from "react"

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case "CLICK":
      return { ...state, clicked: true }
    case "MOVE":
      return { ...state, clicked: false }
    default:
      return
  }
}
const initialState = { clicked: false }

const NavContext: any = React.createContext(initialState)

function NavProvider(props: any) {
  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <NavContext.Provider value={{ state, dispatch }}>
      {props.children}
    </NavContext.Provider>
  )
}
export { NavContext, NavProvider }
