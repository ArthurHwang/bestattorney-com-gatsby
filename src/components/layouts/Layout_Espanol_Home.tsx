import React, { ReactNode, ReactElement } from "react"
import Media from "react-media"
import styled, { ThemeProvider } from "styled-components"
import GlobalStyle from "../../../Global"
import MasterTheme from "../utilities/masterTheme"
import { Footer } from "../elements/footer/Footer"
import { EspanolHeaderHome } from "../elements/espanol/header/Espanol_Header_Home"
import { EspanolNav } from "../elements/espanol/nav/Espanol_Nav"
import { EspanolNavMobile } from "../elements/espanol/nav/Espanol_Nav_Mobile"

interface Props {
  children: ReactNode
}

export const LayoutEspanolHome: React.FC<Props> = ({
  children
}): ReactElement => {
  return (
    <ThemeProvider theme={MasterTheme}>
      <>
        <GlobalStyle />
        <nav>
          <Media query="(max-width: 1024px)">
            {matches =>
              matches ? (
                <>
                  <MobileContentPush />
                  <EspanolNavMobile />
                </>
              ) : (
                <EspanolNav />
              )
            }
          </Media>
        </nav>
        <EspanolHeaderHome />
        <StyledMainNoSidebar>{children}</StyledMainNoSidebar>
        <Footer />
      </>
    </ThemeProvider>
  )
}

const MobileContentPush = styled("div")`
  width: 100%;
  height: 70px;
`

const StyledMainNoSidebar = styled("main")`
  max-width: 100%;
  margin: 0 auto;

  @media (max-width: 1024px) {
    max-width: 100%;
  }
`
