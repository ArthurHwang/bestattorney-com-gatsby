import React, { ReactNode, ReactElement } from "react"
import styled, { ThemeProvider } from "styled-components"
import GlobalStyle from "../../../Global"
import MasterTheme from "../utilities/masterTheme"
import { ContactUsFooter } from "../elements/ContactUsFooter"
import { ContactFormSidebar } from "../elements/forms/Form_Sidebar"
import { CallBanner } from "../elements/CallBanner"
import { FooterAMP } from "../elements/amp/Footer-AMP"
import { BraggingRightsAMP } from "../elements/amp/BraggingRights-AMP"
import { HeaderAMP } from "../elements/amp/Header-AMP"
import { NavAMP } from "../elements/amp/Nav-AMP"
import {
  SidebarAttorneys,
  SidebarExtra,
  SidebarHead,
  SidebarLinks,
  SidebarBlog
} from "../elements/sidebar"

interface Props {
  children: ReactNode
  sidebar?: boolean
}

export const LayoutBlogAMP: React.FC<Props> = ({
  children,
  sidebar = false,
  ...rest
}): ReactElement => {
  const folderOptions: any = rest

  const contentSwitch = (): ReactElement => {
    if (sidebar === true) {
      return (
        <GridWrapper>
          <StyledMainSidebar>{children}</StyledMainSidebar>
          <StyledAside>
            {folderOptions.showContactSidebar && <ContactFormSidebar />}
            <SidebarHead content={folderOptions.sidebarHead} />
            <SidebarLinks content={folderOptions.sidebarLinks} />
            <SidebarExtra content={folderOptions.extraSidebar} />
            {folderOptions.showAttorneySidebar && <SidebarAttorneys />}
            {folderOptions.showBlogSidebar && <SidebarBlog />}
          </StyledAside>
        </GridWrapper>
      )
    } else {
      return <StyledMainNoSidebar>{children}</StyledMainNoSidebar>
    }
  }

  return (
    <ThemeProvider theme={MasterTheme}>
      <>
        <GlobalStyle />
        <nav>
          <MobileContentPush />
          <NavAMP />
        </nav>
        <HeaderAMP />
        <CallBanner />
        {contentSwitch()}
        <BraggingRightsAMP borderTop={true} />
        <ContactUsFooter />
        <FooterAMP />
      </>
    </ThemeProvider>
  )
}

const MobileContentPush = styled("div")`
  width: 100%;
  height: 70px;
`

const StyledMainSidebar = styled("main")`
  max-width: 95%;

  li {
    margin-bottom: 0.7rem;
  }

  @media (max-width: 1024px) {
    max-width: 100%;
  }
`

const StyledMainNoSidebar = styled("main")`
  margin: 0 auto;
  max-width: 1522.39px;
  width: 80%;

  li {
    margin-bottom: 0.7rem;
  }

  @media (max-width: 1024px) {
    width: initial;
    max-width: 90%;
  }
`

const StyledAside = styled("aside")`
  background-color: #eee;
  max-height: 100%;
  padding: 0 2rem 2rem;

  & > * {
    margin-top: 2rem;

    @media(max-width: 550px) {
      margin-top: 1rem;
    }
  }

  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    background-color: ${({ theme }) => theme.colors.secondary};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.accent};
  }

  &::-webkit-scrollbar-button {
    /* background-color: ${({ theme }) => theme.colors.secondary}; */
  }

  &::-webkit-scrollbar-corner {
    background-color: ${({ theme }) => theme.colors.secondary};
  }

  @media(max-width: 550px) {
    padding: 0 0 1rem ;
  }
`

const GridWrapper = styled("div")`
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 450px;
  max-width: 1522.39px;
  width: 80%;

  @media (max-width: 1024px) {
    grid-template-columns: 100%;
    width: initial;
    max-width: 90%;
  }
`
