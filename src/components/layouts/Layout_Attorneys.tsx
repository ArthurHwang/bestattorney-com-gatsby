import { Location } from "@reach/router"
import styled, { ThemeProvider } from "styled-components"
import debounce from "lodash/debounce"
import React, {
  ReactNode,
  useEffect,
  useRef,
  useState,
  ReactElement
} from "react"
import Media from "react-media"
import GlobalStyle from "../../../Global"
import MasterTheme from "../utilities/masterTheme"
import { ContactUsFooter } from "../elements/ContactUsFooter"
import { Footer } from "../elements/footer/Footer"
import { ContactFormSidebar } from "../elements/forms/Form_Sidebar"
import { HeaderDefault } from "../elements/header"
import { BraggingRights } from "../elements/home"
import { Nav, NavMobile } from "../elements/nav"
import { CallBanner } from "../elements/CallBanner"
import {
  SidebarAchievements,
  SidebarAttorneys,
  SidebarBlog,
  SidebarCaseResults,
  SidebarReviews,
  SidebarVideos
} from "../elements/sidebar"

interface Props {
  children: ReactNode
  folderOptions: any
}

export const LayoutAttorneys: React.FC<Props> = ({
  children,
  ...rest
}): ReactElement => {
  const [height, setHeight] = useState(0)
  const ref: any = useRef(null)
  const folderOptions: any = rest

  // ? Its ugly but useEffect needs to be written this way due to 16 years of wordpress data
  // ? Some objects created in the first few years in the data don't include some fields and cause errors
  // ? useEffect listeners are check

  useEffect(() => {
    if (
      !ref ||
      !ref.current ||
      !ref.current.clientHeight ||
      ref.current.clientHeight === null
    ) {
      return
    } else {
      // Add resize event listener for to calculate new height of sidebar
      setHeight(ref.current.clientHeight)
      window.addEventListener("resize", () => {
        if (
          !ref ||
          !ref.current ||
          !ref.current.clientHeight ||
          ref.current.clientHeight === null
        ) {
          return
        } else {
          setHeight(ref.current.clientHeight)
        }
      })

      // Add scroll event listener and debounce to calculate new height of sidebar
      window.addEventListener(
        "scroll",
        debounce(() => {
          if (
            !ref ||
            !ref.current ||
            !ref.current.clientHeight ||
            ref.current.clientHeight === null
          ) {
            return
          } else {
            setHeight(ref.current.clientHeight)
          }
        }, 100)
      )

      // Cleanup event listeners on component unmount
      return () => {
        window.removeEventListener("resize", () => {})
        window.removeEventListener("scroll", () => {})
      }
    }
  })

  const sidebarSwitch = (pathname: string): ReactElement => {
    if (pathname.includes("brian-chase")) {
      return (
        <StyledAside maxHeight={height}>
          <SidebarAchievements />
          {folderOptions.showContactSidebar && <ContactFormSidebar />}
          {folderOptions.showAttorneySidebar && <SidebarAttorneys />}
          {folderOptions.showCaseResultsSidebar && <SidebarCaseResults />}
          {folderOptions.showBlogSidebar && <SidebarBlog />}
          <SidebarVideos content={folderOptions.videosSidebar} />
          {folderOptions.showReviewsSidebar && <SidebarReviews />}
        </StyledAside>
      )
    } else {
      return (
        <StyledAside maxHeight={height}>
          {folderOptions.showContactSidebar && <ContactFormSidebar />}
          {folderOptions.showAttorneySidebar && <SidebarAttorneys />}
          {folderOptions.showCaseResultsSidebar && <SidebarCaseResults />}
          {folderOptions.showBlogSidebar && <SidebarBlog />}
          <SidebarVideos content={folderOptions.videosSidebar} />
          {folderOptions.showReviewsSidebar && <SidebarReviews />}
        </StyledAside>
      )
    }
  }

  return (
    <ThemeProvider theme={MasterTheme}>
      <>
        <GlobalStyle />
        <nav>
          <Media query="(max-width: 1024px)">
            {matches =>
              matches ? (
                <>
                  <MobileContentPush />
                  <NavMobile />
                </>
              ) : (
                <Nav spanishLink={folderOptions.spanishPageEquivalent} />
              )
            }
          </Media>
        </nav>
        <HeaderDefault />
        <CallBanner />
        <GridWrapper>
          <StyledMainSidebar ref={ref}>{children}</StyledMainSidebar>
          <Location>
            {({ location }) => <>{sidebarSwitch(location.pathname)}</>}
          </Location>
        </GridWrapper>
        <BraggingRights borderTop={true} />
        <ContactUsFooter />
        <Footer />
      </>
    </ThemeProvider>
  )
}

const MobileContentPush = styled("div")`
  width: 100%;
  height: 70px;
`

const StyledMainSidebar = styled("main")`
  max-width: 95%;

  li {
    margin-bottom: 0.7rem;
  }

  @media (max-width: 1024px) {
    max-width: 100%;
  }
`

const StyledAside = styled("aside")<{ maxHeight: number }>`
  background-color: #eee;
  max-height: ${props => props.maxHeight + "px"};
  overflow-y: auto;
  padding: 0 2rem 2rem;

  @media (max-width: 550px) {
    padding: 0 0 1rem;
  }

  & > * {
    margin-top: 2rem;

    @media (max-width: 550px) {
      margin-top: 1rem;
    }
  }

  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    background-color: ${({ theme }) => theme.colors.secondary};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.accent};
  }

  &::-webkit-scrollbar-corner {
    background-color: ${({ theme }) => theme.colors.secondary};
  }
`

const GridWrapper = styled("div")`
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 450px;
  max-width: 1522.39px;
  width: 80%;
  min-height: 800px;

  @media (max-width: 1024px) {
    grid-template-columns: 100%;
    max-width: 90%;
    width: initial;
  }
`
