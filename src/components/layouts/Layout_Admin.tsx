import { Link } from "src/components/elements/Link"
import React, { ReactNode, ReactElement } from "react"
import Helmet from "react-helmet"
import Media from "react-media"
import styled, { ThemeProvider } from "styled-components"
import GlobalStyle from "../../../Global"
import MasterTheme from "../utilities/masterTheme"
import { Nav, NavMobile } from "../elements/nav"

interface Props {
  children: ReactNode
}

export const LayoutAdmin: React.FC<Props> = ({ children }): ReactElement => {
  return (
    <>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <ThemeProvider theme={MasterTheme}>
        <>
          <GlobalStyle />
          <nav>
            <Media query="(max-width: 1024px)">
              {matches =>
                matches ? (
                  <>
                    <MobileContentPush />
                    <NavMobile />
                  </>
                ) : (
                  <Nav />
                )
              }
            </Media>
          </nav>
          <GridWrapper>
            <StyledAside>
              <p>Menu</p>
              <ul>
                <li>
                  <Link to="/admin">Home</Link>
                </li>
                <li>
                  <Link to="/admin/technical-operations">
                    Technical Operations
                  </Link>
                </li>
                <li>
                  <Link to="/admin/technical-operations">
                    Edit Website Content
                  </Link>
                </li>
                <li>
                  <Link to="/admin/technical-operations">Build Pipeline</Link>
                </li>
                <li>
                  <Link to="/admin/technical-operations">
                    The Website Structure
                  </Link>
                </li>
              </ul>
            </StyledAside>
            <StyledMain>{children}</StyledMain>
          </GridWrapper>
        </>
      </ThemeProvider>
    </>
  )
}

const MobileContentPush = styled("div")`
  width: 100%;
  height: 70px;
`

const GridWrapper = styled("div")`
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 3fr;
  max-width: 100%;
  max-height: calc(100vh - 70px);
`

const StyledMain = styled("main")`
  height: calc(100vh - 70px);
  overflow-y: auto;
  padding: 2rem;
  background-color: #eee;

  li {
    margin-bottom: 0.7rem;
  }

  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    background-color: ${({ theme }) => theme.colors.secondary};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.accent};
  }

  &::-webkit-scrollbar-corner {
    background-color: ${({ theme }) => theme.colors.secondary};
  }
`

const StyledAside = styled("aside")`
  max-height: calc(100vh - 70px);
  overflow-y: auto;
  padding: 2rem;

  ul li {
    margin: 1rem 0;
  }

  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    background-color: ${({ theme }) => theme.colors.secondary};
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.accent};
  }

  &::-webkit-scrollbar-corner {
    background-color: ${({ theme }) => theme.colors.secondary};
  }
`
