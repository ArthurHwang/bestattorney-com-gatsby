import React, { ReactNode, ReactElement } from "react"
import styled, { ThemeProvider } from "styled-components"
import Media from "react-media"
import GlobalStyle from "../../../Global"
import MasterTheme from "../utilities/masterTheme"
import { Footer } from "../elements/footer/Footer"
import { HeaderHome } from "../elements/header"
import { Nav, NavMobile } from "../elements/nav"

interface Props {
  children: ReactNode
}

export const LayoutHome: React.FC<Props> = ({ children }): ReactElement => {
  return (
    <ThemeProvider theme={MasterTheme}>
      <>
        <GlobalStyle />
        <nav>
          <Media query="(max-width: 1024px)">
            {matches =>
              matches ? (
                <>
                  <MobileContentPush />
                  <NavMobile />
                </>
              ) : (
                <Nav />
              )
            }
          </Media>
        </nav>
        <HeaderHome />
        <StyledMainNoSidebar>{children}</StyledMainNoSidebar>
        <Footer />
      </>
    </ThemeProvider>
  )
}

const MobileContentPush = styled("div")`
  width: 100%;
  height: 70px;
`

const StyledMainNoSidebar = styled("main")`
  max-width: 100%;
  margin: 0 auto;

  @media (max-width: 1024px) {
    max-width: 100%;
  }
`
