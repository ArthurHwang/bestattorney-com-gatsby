import Typography from "typography"
import wordpress2011Theme from "typography-theme-wordpress-2011"

wordpress2011Theme.includeNormalize = false
wordpress2011Theme.baseFontSize = "12px"
wordpress2011Theme.headerGray = 0
wordpress2011Theme.bodyGray = 0

// ! CHANGE ENTIRE WEBSITE FONT BELOW
wordpress2011Theme.headerFontFamily = [
  "-apple-system",
  "BlinkMacSystemFont",
  "Segoe UI",
  "Roboto",
  "Oxegyn-Sans",
  "Helvetica Neue",
  "sans-serif"
]
wordpress2011Theme.bodyFontFamily = [
  "-apple-system",
  "BlinkMacSystemFont",
  "Segoe UI",
  "Roboto",
  "Oxegyn-Sans",
  "Helvetica Neue",
  "sans-serif"
]

// ! CHANGE ENTIRE WEBSITE FONT SCALING BELOW
wordpress2011Theme.overrideThemeStyles = () => ({
  // img: {
  //   marginBottom: "0"
  // },
  "h3,h4,h5,h6,p,li": {
    fontSize: "1.4rem"
  },
  "span,li,a,p,div": {
    fontWeight: "400"
  },
  h1: {
    fontSize: "3rem",
    marginBottom: "2rem",
    color: "#00303F"
  },
  h2: {
    color: "#00303F",
    fontSize: "2rem",
    marginBottom: "1.2rem"
  },
  h3: {
    color: "#00303F"
  },
  p: {
    color: "#222533"
  },
  div: {
    color: "#222533"
  },
  a: {
    color: "#222533"
  }
})

const typography = new Typography(wordpress2011Theme)

export default typography
