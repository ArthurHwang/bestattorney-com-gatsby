// ! Format wordpress date
export function formatWPdate(wordpressDate) {
  const date = wordpressDate.substring(0, 10)
  const month = date.substring(5, 7)
  const day = date.substring(8, 10)
  const year = date.substring(0, 4)
  const time = wordpressDate.substring(11, 19)
  let hour = parseInt(time.substring(0, 2))
  const minute = parseInt(time.substring(3, 5))
  const amPm = hour >= 12 ? "pm" : "am"
  hour = hour - 12
  return `${month}-${day}-${year}, ${hour}:${minute}${amPm}`
}

export function formatWPmeta(meta) {
  return meta
    .replace(/&#8217;/, "'")
    .replace(/&#8221;/, '"')
    .replace(/&#8230;/, "...")
    .replace(/&#038;/, "&")
    .replace(/&#8220;/, '"')
    .replace(/<p>/, "")
    .replace(/&#8230;<\/p>/, "")
    .replace(/<\/p>/, "")
    .replace(/&#8216;/, "'")
}

export function formatWPtitle(title) {
  return title
    .replace(/&#8217;/, "'")
    .replace(/&#8221;/, '"')
    .replace(/&#8230;/, "...")
    .replace(/&#038;/, "&")
    .replace(/&#8220;/, '"')
    .replace(/&#8216;/, "'")
}
