import { ReviewsData } from "../../data/reviews"

function getRandomItems(arr, items) {
  const ret = []
  const indexes = []
  const arrLength = arr.length

  // If we don't have enough items to return - return the original array
  if (arrLength < items) {
    return arr
  }

  while (ret.length < items) {
    let i = Math.floor(Math.random() * arrLength)
    if (indexes.indexOf(i) === -1) {
      indexes[indexes.length] = i
      ret[ret.length] = arr[i]
    }
  }
  return ret
}

// change number to return different amount of review objects
export const getReviews = getRandomItems(ReviewsData, 5)
