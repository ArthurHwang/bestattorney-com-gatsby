import React from "react"
import { ThemeProvider } from "styled-components"

const themeMock = {
  colors: {
    accent: "#EC7D29",
    grey: "#8E8E8E",
    primary: "#FFFFFF",
    secondary: "#222533"
  },
  headers: {
    primary: "#333A56"
  },
  links: {
    hoverBlue: "#91BED4",
    hoverOrange: "#EC7D29",
    normal: "#005B99"
  }
}

export const ThemeProviderWrapper = ({ children }) => (
  <ThemeProvider theme={themeMock}>{children}</ThemeProvider>
)
