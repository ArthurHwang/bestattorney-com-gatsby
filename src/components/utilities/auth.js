export const isBrowser = () => typeof window !== "undefined"

export const getUser = () =>
  isBrowser() && window.localStorage.getItem("bestattorneyAdmin")
    ? JSON.parse(window.localStorage.getItem("bestattorneyAdmin"))
    : {}

const setUser = user =>
  window.localStorage.setItem("bestattorneyAdmin", JSON.stringify(user))

export const handleLogin = ({ username, password }) => {
  if (
    username === `${process.env.GATSBY_ADMIN_USERNAME}` &&
    password === `${process.env.GATSBY_ADMIN_PASSWORD}`
  ) {
    return setUser({
      username: `bestattorney`,
      email: `marketing@bestatto-gatsby.netlify.app`
    })
  }
  window.alert("WRONG TRY AGAIN BUDDY")
  return false
}

export const isLoggedIn = () => {
  const user = getUser()

  return !!user.username
}

export const logout = callback => {
  setUser({})
  callback()
}
