export default [
  "box-shadow: inset 0 7px 9px -7px rgba(0,0,0, 0.7)",
  "box-shadow: 0 1px 3px rgba(0,0,0, 0.12), 0 1px 2px rgba(0,0,0, 0.24)",
  "box-shadow: 0 1px 3px rgba(0,0,0, 0.24), 0 3px 6px rgba(0,0,0, 0.43)",
  "box-shadow: 0 2px 5px rgba(0,0,0, 0.34), 0 4px 10px rgba(0,0,0, 0.64)",
  "box-shadow: 0 2px 5px rgba(255,255,255, 0.34), 0 4px 10px rgba(255,255,255, 0.64)"
]
