import styled from "styled-components"
import { applyStyleModifiers } from "styled-components-modifiers"
import { elevation } from "."

const BUTTON_MODIFIERS = {
  small: () => `
    font-size: 1rem;
    padding: 3px 10px;
  `,
  cancel: ({ theme }) => `
    background: ${theme.colors.secondary}
  `
}

const Button = styled.button`
  padding: 5px 20px;
  border-radius: 0.2rem;
  border: none;
  color: white;
  cursor: pointer;
  font-size: 1.7rem;
  transition: transform 0.3s ease-out, box-shadow 0.2s linear, color 0.3s;

  &:hover {
    transform: scale(1.03);
  }
`

export const DefaultOrangeButton = styled(Button)`
  background: ${({ theme }) => theme.colors.accent};
  color: ${({ theme }) => theme.colors.primary};
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const DefaultGreyButton = styled(Button)`
  background: #dfdfdf;
  color: ${({ theme }) => theme.colors.secondary};
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const DefaultWhiteButton = styled(Button)`
  background: ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.secondary};
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const DefaultBlueButton = styled(Button)`
  background: ${({ theme }) => theme.colors.secondary};
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const DefaultGreenButton = styled(Button)`
  background: #0eeb57;
  color: black;
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const CallButton = styled(Button)`
  background: ${({ theme }) => theme.colors.secondary};
  text-transform: uppercase;
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`

export const AwardsButton = styled(Button)`
  background: ${({ theme }) => theme.colors.secondary};
  text-transform: uppercase;
  ${elevation[1]};
  &:hover {
    color: ${({ theme }) => theme.colors.accent};
    ${elevation[2]};
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)};
`
