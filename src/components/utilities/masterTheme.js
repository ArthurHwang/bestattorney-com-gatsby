// ! Change sitewide colors here!
// Values below control all colors sitewide.  Use caution!

const MasterTheme = {
  colors: {
    accent: "#EC7D29",
    grey: "#8E8E8E",
    primary: "#FFFFFF",
    secondary: "#222533"
  },
  headers: {
    primary: "#333A56"
  },
  links: {
    hoverBlue: "#91BED4",
    hoverOrange: "#EC7D29",
    normal: "#005B99"
  }
}

export default MasterTheme
