const string = `		  "Elegir un Abogado" => "/abogados/recursos/elegir-un-abogado.html",
		"Resultos Destacados" => "/abogados/resultos-destacados.html",
		"Contactenos" => "/abogados/contactenos.html",`

// This function will take a PHP associate array and return an array of objects containing linkName and linkURL

function turnPHPintoJS(string) {
  const payload = []
  // console.log(Object.entries(string))

  // let removeWhitespace = string.replace(/ /g, "")
  let splitItems = string
    .replace(/[\n\r]+/g, "")

    .split(",")
  // remove last item from array
  splitItems.pop()
  // trim start and end of each array item, also remove double quotes
  const sanitizedItems = splitItems.map(item =>
    item
      .replace(/"/g, "")
      .replace(".html", "")
      .trim()
  )
  // create object to push
  sanitizedItems.forEach(item => {
    const splitItem = item.split(" => ")

    const constructObject = {
      linkName: splitItem[0],
      linkURL: splitItem[1]
    }

    payload.push(constructObject)
  })

  return payload
}

console.log(turnPHPintoJS(string))
