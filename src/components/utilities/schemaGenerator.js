// This does not support nested pages (level 2 and up)
// If you're working with deeply nested pages, remove this or rework it.

export default ({
  location,
  canonical,
  siteUrl,
  pageTitle,
  siteTitle,
  pageTitleFull,
  imageUrl,
  pageSubject,
  isBlog,
  isAmp
}) => {
  const isSubPage =
    (pageTitle && location.pathname !== "/") ||
    location.pathname !== "/abogados"

  let schema = [
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      url: canonical,
      name:
        isBlog || isAmp
          ? "California Personal Injury Blog - Bisnar Chase"
          : pageTitle || siteTitle,
      alternateName:
        isBlog || isAmp
          ? "California Personal Injury Blog - Bisnar Chase"
          : pageTitleFull
    },
    {
      "@context": "https://schema.org/",
      "@type": "WebPage",
      name:
        isBlog || isAmp
          ? "California Personal Injury Blog - Bisnar Chase"
          : "Bisnar Chase Personal Injury Attorneys",
      speakable: {
        "@type": "SpeakableSpecification",
        xpath: [
          "/html/head/title",
          "/html/head/meta[@name='description']/@content"
        ]
      },
      url:
        isBlog || isAmp
          ? "https://www.bestatto-gatsby.netlify.app/blog"
          : "https://www.bestatto-gatsby.netlify.app"
    }
  ]

  if (location.pathname === "/" || location.pathname === "/abogados") {
    schema.push(
      {
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Personal Injury Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          priceRange: "$",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          `${imageUrl}` ||
          `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`,
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Personal Injury Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Settlements"
              }
            }
          ]
        }
      },
      {
        "@context": "http://schema.org",
        "@type": "Attorney",
        name: "Bisnar Chase Personal Injury Attorneys",
        image:
          "https://www.bestatto-gatsby.netlify.app/images/text-header-images/Bisnar-Chase-Staff-Photo-2017.jpg",
        address: "1301 Dove St, #120 Newport Beach, CA 92660",
        telephone: "(949) 203-3814",
        priceRange: "$",
        aggregateRating: {
          "@type": "AggregateRating",
          ratingValue: "5",
          reviewCount: "3"
        },
        review: [
          {
            "@type": "Review",
            author: "Vanessa Sampson",
            description:
              "After a bad accident, we were lost on how to handle the insurance agents, the paperwork involved and the claims representatives.  We contacted Bisnar Chase and they took care of everything.  It gave us the peace of mind we needed to heal.  I am very impressed with their level of professionalism and their compassion.  We definitely recommend Bisnar Chase.",
            reviewRating: {
              "@type": "Rating",
              bestRating: "5",
              ratingValue: "5",
              worstRating: "1"
            }
          },
          {
            "@type": "Review",
            author: "Eva Shaffer",
            description:
              "I have used Bisnar and Chase twice now, both for car accidents. Each time I received a fair settlement and honestly received more than I expected. The entire team of Bisnar and Chase were professional, courteous and friendly. The paralegals there are top notch! I cannot express enough how much I appreciated the constant communication from them and how they explained things I didn't understand in such a way that I could. <br> Will I ever use them again? I only hope not to get into another car accident ever so that I don't have to, but I will definitely use them if I do (knock on wood I don't). I highly recommend Bisnar and Chase if you are considering hiring a lawyer for an automobile accident.",
            reviewRating: {
              "@type": "Rating",
              bestRating: "5",
              ratingValue: "5",
              worstRating: "1"
            }
          },
          {
            "@type": "Review",
            author: "Brady M.",
            description:
              "John Bisnar was so professional and caring. When I found myself in this situation and never having a car accident before I didn't know what steps to take. Mr. Bisnar explained every single detail to me and what to expect. His staff was amazing, friendly and seemed to really care about me as a person. I lost all doubt I may have had about dealing with personal injury lawyers and couldn't be happier with the level of service I received.  I was constantly updated on my case status and received far more from my settlement than I expected.",
            reviewRating: {
              "@type": "Rating",
              bestRating: "5",
              ratingValue: "5",
              worstRating: "1"
            }
          }
        ]
      }
    )
  }
  // Push Location JSON
  if (isSubPage) {
    if (location.pathname.includes("orange-county")) {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Attorney",
        address: {
          "@type": "PostalAddress",
          addressLocality: "Newport Beach",
          addressRegion: "CA",
          postalCode: "92660",
          streetAddress: "1301 Dove St, #120"
        },
        description:
          "Bisnar Chase Personal Injury Attorneys are award winning trial lawyers representing injured victims in personal injury cases in Orange County and throughout California. We fight for fair compensation from insurance companies and guilty parties in cases such as car accidents, dog bites, defective products, and other serious injuries. We have won over 500 Million Dollars for our clients.",
        name: "Bisnar Chase Personal Injury Attorneys",
        telephone: "(949) 203-3814",
        openingHours: "Mo,Tu,We,Th,Fr 08:30-17:00",
        geo: {
          "@type": "GeoCoordinates",
          latitude: "33.662136",
          longitude: "-117.866176"
        },
        areaServed: {
          "@type": "GeoShape",
          circle: "33.662136, -117.866176, 30000"
        },
        priceRange: "$",
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        sameAs: [
          "https://www.facebook.com/california.attorney",
          "https://twitter.com/bisnarchase",
          "https://plus.google.com/+Bestattorney"
        ],
        additionalType:
          "http://www.productontology.org/id/Personal_injury_lawyer"
      })
    }

    if (location.pathname.includes("los-angeles")) {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Attorney",
        address: {
          "@type": "PostalAddress",
          addressLocality: "Los Angeles",
          addressRegion: "CA",
          postalCode: "90045",
          streetAddress: "6701 Center Dr W, 14th. Fl"
        },
        description:
          "Bisnar Chase Personal Injury Attorneys are award winning trial lawyers representing injured victims in personal injury cases in Los Angeles and throughout California. We fight for fair compensation from insurance companies and guilty parties in cases such as car accidents, dog bites, defective products, and other serious injuries. We have won over 500 Million Dollars for our clients.",
        name: "Bisnar Chase Personal Injury Attorneys",
        telephone: "(323) 238-4683",
        openingHours: "Mo,Tu,We,Th,Fr 08:30-17:00",
        geo: {
          "@type": "GeoCoordinates",
          latitude: "33.978699",
          longitude: "-118.393204"
        },
        areaServed: {
          "@type": "GeoShape",
          circle: "34.053700, -118.246177, 30000"
        },
        priceRange: "$",
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        sameAs: [
          "https://plus.google.com/+BisnarChasePersonalInjuryAttorneysLosAngeles/",
          "https://www.facebook.com/bisnarchaselosangeles/"
        ],
        additionalType:
          "http://www.productontology.org/id/Personal_injury_lawyer"
      })
    }

    if (location.pathname.includes("riverside")) {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Attorney",
        address: {
          "@type": "PostalAddress",
          addressLocality: "Riverside",
          addressRegion: "CA",
          postalCode: "92506",
          streetAddress: "6809 Indiana Avenue #148"
        },
        description:
          "Bisnar Chase Personal Injury Attorneys are award winning trial lawyers representing injured victims in personal injury cases in Riverside and throughout California. We fight for fair compensation from insurance companies and guilty parties in cases such as car accidents, dog bites, defective products, and other serious injuries. We have won over 500 Million Dollars for our clients.",
        name: "Bisnar Chase Personal Injury Attorneys",
        telephone: "(951) 530-3711",
        openingHours: "Mo,Tu,We,Th,Fr 08:30-17:00",
        geo: {
          "@type": "GeoCoordinates",
          latitude: "33.943320",
          longitude: "-117.392013"
        },
        areaServed: {
          "@type": "GeoShape",
          circle: "33.943469, -117.392016, 20000"
        },
        priceRange: "$",
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        sameAs: [
          "https://plus.google.com/111886011829023985223",
          "https://www.facebook.com/bisnarchaseriverside/"
        ],
        additionalType:
          "http://www.productontology.org/id/Personal_injury_lawyer"
      })
    }

    if (location.pathname.includes("san-bernardino")) {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Attorney",
        address: {
          "@type": "PostalAddress",
          addressLocality: "San Bernardino",
          addressRegion: "CA",
          postalCode: "92408",
          streetAddress: "473 E Carnegie Dr #221"
        },
        description:
          "Bisnar Chase Personal Injury Attorneys are award winning trial lawyers representing injured victims in personal injury cases in San Bernardino and throughout California. We fight for fair compensation from insurance companies and guilty parties in cases such as car accidents, dog bites, defective products, and other serious injuries. We have won over 500 Million Dollars for our clients.",
        name: "Bisnar Chase Personal Injury Attorneys",
        telephone: "(909) 253-0750",
        openingHours: "Mo,Tu,We,Th,Fr 08:30-17:00",
        geo: {
          "@type": "GeoCoordinates",
          latitude: "34.067786",
          longitude: "-117.274716"
        },
        areaServed: {
          "@type": "GeoShape",
          circle: "34.069474, -117.274076, 20000"
        },
        priceRange: "$",
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        sameAs: [
          "https://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-san-bernardino",
          "https://www.facebook.com/bisnarchasesanbernardino/"
        ],
        additionalType:
          "http://www.productontology.org/id/Personal_injury_lawyer"
      })
    }
    // Push Page Type JSON
    if (pageSubject === "car-accident") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Car Accident Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Car Accident Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Car Accident Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Car Accident Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Car Accident Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Car Accident Settlements"
              }
            }
          ]
        }
      })
    }

    if (pageSubject === "auto-defect") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Auto Defect Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Auto Defect Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Lawsuits Against Auto Manufacturers"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Auto Defect Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Auto Defect Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Automaker Defect Settlements"
              }
            }
          ]
        }
      })
    }

    if (pageSubject === "dog-bite") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Dog Bite Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Dog Bite Injury Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Dog Bite Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Dog Bite Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Dog Bite Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Dog Bite Settlements"
              }
            }
          ]
        }
      })
    }

    if (pageSubject === "product-defect") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Product Defect Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Product Defect Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Product Defect Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Product Defect Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Product Defect Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Product Defect Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "medical-defect") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Medical Defect Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Medical Defect Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Medical Device Defect Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Medical Device Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Medical Defect Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Defective Medical Device Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "drug-defect") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Defective Drug Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Defective Drug Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Lawsuits against Pharmaceutical Companies"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Defective Drug Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Big Pharma Defect Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Dangerous Drug Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "premises-liability") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Premises Liability Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Premises Liability Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Premises Liability Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Premises Liability Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Premises Liability Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Premises Liability Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "truck-accident") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Truck Accident Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Truck Accident Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Truck Accident Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Truck Accident Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Truck Accident Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Truck Accident Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "pedestrian-accident") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Pedestrian Accident Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Pedestrian Accident Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Pedestrian Accident Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Pedestrian Accident Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Pedestrian Accident Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Pedestrian Accident Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "motorcycle-accident") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Motorcycle Accident Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Motorcycle Accident Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Motorcycle Accident Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Motorcycle Accident Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Motorcycle Accident Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Motorcycle Accident Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "bicycle-accident") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Bicycle Accident Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Bicycle Accident Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Bicycle Accident Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Bicycle Accident Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Bicycle Accident Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Bicycle Accident Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "employment-law" || pageSubject === "class-action") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Employment Law Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Employment Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Lawsuits Against Employers"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Employer Illegal Practices Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Employment Law Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Employment Law Settlements"
              }
            }
          ]
        }
      })
    }
    if (pageSubject === "wrongful-death") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Wrongful Death Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",
          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Wrongful Death Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Wrongful Death Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Wrongful Death Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Wrongful Death Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Wrongful Death Settlements"
              }
            }
          ]
        }
      })
    }

    if (pageSubject === "") {
      schema.push({
        "@context": "http://schema.org",
        "@type": "Service",
        serviceType: "Personal Injury Legal Claims",
        provider: {
          "@type": "Attorney",
          name: "Bisnar Chase Personal Injury Attorneys",

          image:
            "https://bestatto-gatsby.netlify.app/images/carousel/bc-staff-2019.jpg",
          priceRange: "$",
          address: "1301 Dove St, #120 Newport Beach, CA 92660",
          telephone: "(949) 203-3814"
        },
        image:
          `${imageUrl}` ||
          `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`,
        hasOfferCatalog: {
          "@type": "OfferCatalog",
          name: "Personal Injury Law",
          itemListElement: [
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Lawsuits"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Insurance Claims"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Legal Advice"
              }
            },
            {
              "@type": "Offer",
              itemOffered: {
                "@type": "Service",
                name: "Personal Injury Settlements"
              }
            }
          ]
        }
      })
    }
    // schema.push({
    //   "@context": "http://schema.org",
    //   "@type": "BreadcrumbList",
    //   itemListElement: [
    //     {
    //       "@type": "ListItem",
    //       position: 1,
    //       item: {
    //         "@id": siteUrl,
    //         name: siteTitle
    //       }
    //     },
    //     {
    //       "@type": "ListItem",
    //       position: 2,
    //       item: {
    //         "@id": canonical,
    //         name: pageTitle
    //       }
    //     }
    //   ]
    // })

    // schema.push()
  }

  return schema
}
