export default [
  {
    featureType: "administrative",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#444444"
      }
    ]
  },
  {
    featureType: "landscape",
    elementType: "all",
    stylers: [
      {
        color: "#f2f2f2"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "labels",
    stylers: [
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "labels.icon",
    stylers: [
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "all",
    stylers: [
      {
        saturation: -100
      },
      {
        lightness: 45
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "all",
    stylers: [
      {
        visibility: "simplified"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "geometry.fill",
    stylers: [
      {
        visibility: "on"
      },
      {
        color: "#ff6600"
      }
    ]
  },
  {
    featureType: "road.arterial",
    elementType: "labels.icon",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "transit",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "all",
    stylers: [
      {
        color: "#46bcec"
      },
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "geometry.fill",
    stylers: [
      {
        visibility: "on"
      },
      {
        color: "#003884"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels.text.fill",
    stylers: [
      {
        visibility: "on"
      },
      {
        color: "#ff6600"
      },
      {
        weight: "0.01"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels.text.stroke",
    stylers: [
      {
        hue: "#ff6600"
      },
      {
        visibility: "off"
      }
    ]
  }
]

// export default [
//   {
//     featureType: "water",
//     stylers: [
//       {
//         color: "#19a0d8"
//       }
//     ]
//   },
//   {
//     featureType: "administrative",
//     elementType: "labels.text.stroke",
//     stylers: [
//       {
//         color: "#ffffff"
//       },
//       {
//         weight: 6
//       }
//     ]
//   },
//   {
//     featureType: "administrative",
//     elementType: "labels.text.fill",
//     stylers: [
//       {
//         color: "#e85113"
//       }
//     ]
//   },
//   {
//     featureType: "road.highway",
//     elementType: "geometry.stroke",
//     stylers: [
//       {
//         color: "#efe9e4"
//       },
//       {
//         lightness: -40
//       }
//     ]
//   },
//   {
//     featureType: "road.arterial",
//     elementType: "geometry.stroke",
//     stylers: [
//       {
//         color: "#efe9e4"
//       },
//       {
//         lightness: -20
//       }
//     ]
//   },
//   {
//     featureType: "road",
//     elementType: "labels.text.stroke",
//     stylers: [
//       {
//         lightness: 100
//       }
//     ]
//   },
//   {
//     featureType: "road",
//     elementType: "labels.text.fill",
//     stylers: [
//       {
//         lightness: -100
//       }
//     ]
//   },
//   {
//     featureType: "road.highway",
//     elementType: "labels.icon"
//   },
//   {
//     featureType: "landscape",
//     elementType: "labels",
//     stylers: [
//       {
//         visibility: "off"
//       }
//     ]
//   },
//   {
//     featureType: "landscape",
//     stylers: [
//       {
//         lightness: 20
//       },
//       {
//         color: "#efe9e4"
//       }
//     ]
//   },
//   {
//     featureType: "landscape.man_made",
//     stylers: [
//       {
//         visibility: "off"
//       }
//     ]
//   },
//   {
//     featureType: "water",
//     elementType: "labels.text.stroke",
//     stylers: [
//       {
//         lightness: 100
//       }
//     ]
//   },
//   {
//     featureType: "water",
//     elementType: "labels.text.fill",
//     stylers: [
//       {
//         lightness: -100
//       }
//     ]
//   },
//   {
//     featureType: "poi",
//     elementType: "labels.text.fill",
//     stylers: [
//       {
//         hue: "#11ff00"
//       }
//     ]
//   },
//   {
//     featureType: "poi",
//     elementType: "labels.text.stroke",
//     stylers: [
//       {
//         lightness: 100
//       }
//     ]
//   },
//   {
//     featureType: "poi",
//     elementType: "labels.icon",
//     stylers: [
//       {
//         hue: "#4cff00"
//       },
//       {
//         saturation: 58
//       }
//     ]
//   },
//   {
//     featureType: "poi",
//     elementType: "geometry",
//     stylers: [
//       {
//         visibility: "on"
//       },
//       {
//         color: "#f0e4d3"
//       }
//     ]
//   },
//   {
//     featureType: "road.highway",
//     elementType: "geometry.fill",
//     stylers: [
//       {
//         color: "#efe9e4"
//       },
//       {
//         lightness: -25
//       }
//     ]
//   },
//   {
//     featureType: "road.arterial",
//     elementType: "geometry.fill",
//     stylers: [
//       {
//         color: "#efe9e4"
//       },
//       {
//         lightness: -10
//       }
//     ]
//   },
//   {
//     featureType: "poi",
//     elementType: "labels",
//     stylers: [
//       {
//         visibility: "simplified"
//       }
//     ]
//   }
// ]
