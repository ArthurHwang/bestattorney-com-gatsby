//  Iframe

/*

<center>
  <blockquote>
    How can I win against the insurance companies? Attorney Brian Chase
    explains how in this video.
  </blockquote>
</center>

FOR IFRAME:
<LazyLoad>
  <iframe
    width="100%"
    height="500"
    src="https://www.youtube.com/embed/lVUUCtM3Ebg"
    frameBorder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen={true}
  />
</LazyLoad>

FOR IMAGES:
<LazyLoad >
  <img
    src="/images/text-header-images/bullying-in-the-workplace.jpg"
    width="75%"
    alt="bullying-in-the-workplace.jpg"
  />
</LazyLoad>


FOR BACKGROUND IMAGE HEADER:
<LazyLoad >
<div
  className="text-header content-well"
  title="Value of my personal injury claim"
  style={{
    backgroundImage: "url('/images/text-header-images/gavel.jpg')"
  }}
>
  <h2>The Value of my Claim: Contents</h2>
</div>
</LazyLoad>



// mini header image

<div className="mini-header-image">
  <img
    src="/images/bike-accidents/anaheim-bicycle-accident-lawyer.jpg"
    alt="Anaheim Bicycle Accident Lawyer"
  />
</div>

import { LazyLoad } from "src/components/elements/LazyLoad"



import { graphql } from "gatsby"
import Img from "gatsby-image"

<div className="mini-header-image">
  <Img
    className="banner-image"
    alt="Anaheim Bicycle Accident Lawyer"
    title="Anaheim Bicycle Accident Lawyer"
    fluid={data.headerImage.childImageSharp.fluid}
  />
</div>

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "brain-injury/anaheim-brain-injury-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`


*/
