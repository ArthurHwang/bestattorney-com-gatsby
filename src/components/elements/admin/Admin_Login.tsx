import { navigate } from "gatsby"
import React, { SyntheticEvent, FormEvent } from "react"
import styled from "styled-components"
import { handleLogin, isLoggedIn } from "../../utilities/auth"

export class AdminLogin extends React.Component {
  public state = {
    username: ``,
    password: ``
  }

  public handleUpdate = (event: FormEvent) => {
    this.setState({
      [(event.target as HTMLInputElement)
        .name]: (event.target as HTMLInputElement).value
    })
  }

  public handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault()
    handleLogin(this.state)
  }

  public render() {
    if (isLoggedIn()) {
      navigate(`/admin`)
    }

    return (
      <StyledLogin>
        <StyledForm>
          <h2>BESTATTORNEY ADMIN</h2>
          <form
            method="post"
            onSubmit={event => {
              this.handleSubmit(event)
              navigate(`/admin`)
            }}
          >
            <label>
              <input
                placeholder="Username"
                type="text"
                name="username"
                onChange={this.handleUpdate}
              />
            </label>
            <label>
              <input
                placeholder="Password"
                type="password"
                name="password"
                onChange={this.handleUpdate}
              />
            </label>
            <input className="btn-submit" type="submit" value="Log In" />
          </form>
        </StyledForm>
      </StyledLogin>
    )
  }
}

const StyledLogin = styled("div")`
  min-height: calc(100vh - 70px);
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #eee;

  h2 {
    color: #222533;
    text-align: center;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-bottom: 0;
    width: 100%;

    label {
      text-align: center;
    }

    input {
      width: 100%;
      height: 4rem;
      width: 75%;
      margin: 1rem 0;
    }

    .btn-submit {
      margin: 1rem auto 0;
      border: none;
      width: 75%;
      background-color: ${({ theme }) => theme.colors.secondary};
      color: ${({ theme }) => theme.colors.accent};
      font-size: 1.6rem;
      font-variant: all-small-caps;
      font-weight: 600;
    }
  }
`

const StyledForm = styled("div")`
  width: 40rem;
  border: 1px double #222533;
  background-color: #fff;
  height: 30rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
