import { navigate } from "gatsby"
import React, { ReactElement } from "react"
import { isLoggedIn } from "../../utilities/auth"

export const PrivateRoute: React.FC<any> = ({
  component: Component,
  location,
  ...rest
}): ReactElement | null => {
  if (
    !isLoggedIn() &&
    location.pathname !== `/admin/login` &&
    typeof window !== "undefined" &&
    window
  ) {
    // If the user is not logged in, redirect to the login page.
    navigate(`/admin/login`)
    return null
  }
  return <Component {...rest} />
}
