import React, { ReactElement } from "react"
import styled from "styled-components"

export const TechnicalOperations: React.FC = (): ReactElement => {
  return (
    <StyledDocumentation>
      <p>Technical operations</p>
    </StyledDocumentation>
  )
}

const StyledDocumentation = styled("div")``
