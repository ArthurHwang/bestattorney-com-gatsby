import React, { ReactElement } from "react"
import styled from "styled-components"

export const AdminHome: React.FC = (): ReactElement => {
  return (
    <StyledDocumentation>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perferendis
        eum nam tempora consectetur non eligendi vitae sed laboriosam laudantium
        a, quis earum ducimus sit numquam quia labore ullam accusamus voluptatem
        inventore officiis. Itaque nemo culpa libero deserunt alias blanditiis.
        Mollitia omnis autem voluptas et eveniet delectus praesentium molestias
        ullam architecto expedita obcaecati officiis maxime corporis molestiae,
        asperiores, dolor officia magni nisi. Officia dolore culpa similique,
        fugiat quo earum doloribus consequatur et deleniti iure vero vitae. Sunt
        consequuntur obcaecati molestiae, voluptatibus at beatae fugiat, quia
        architecto suscipit ea cupiditate maxime expedita ab nemo quibusdam,
        illo nihil nam! Dolorem cumque sapiente laudantium.
      </p>
    </StyledDocumentation>
  )
}

const StyledDocumentation = styled("div")``
