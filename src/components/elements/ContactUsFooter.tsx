import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import { IoMdCall, IoMdDocument } from "react-icons/io"
import styled from "styled-components"
import { DefaultOrangeButton, DefaultWhiteButton } from "../utilities"

interface Props {
  geoLocation?: string | undefined
}

export const ContactUsFooter: React.FC<Props> = ({
  geoLocation
}): ReactElement => {
  function switchCallNumber(location: string | undefined): string | undefined {
    switch (location) {
      case "orange-county":
        return "9492033814"
      case "los-angeles":
        return "3232384683"
      case "riverside":
        return "9515303711"
      case "san-bernardino":
        return "9092530750"
      default:
        return "8005614887"
    }
  }

  function switchLocationNumber(
    location: string | undefined
  ): string | undefined {
    switch (location) {
      case "orange-county":
        return "(949) 203-3814"
      case "los-angeles":
        return "(323) 238-4683"
      case "riverside":
        return "(951) 530-3711"
      case "san-bernardino":
        return "(909) 253-0750"
      default:
        return "(800) 561-4887"
    }
  }
  return (
    <StyledContactUs>
      <ContentWrap>
        <p>Have a question that wasn't answered here?</p>
        <ButtonWrapper>
          <a href={`tel:${switchCallNumber(geoLocation)}`}>
            <DefaultWhiteButton id="btn-call" className="btn">
              <IoMdCall className="icon icon-call" />
              <ButtonTextWrapper>
                <span>Call Us!</span>
                <br />
                <span>{switchLocationNumber(geoLocation)}</span>
              </ButtonTextWrapper>
            </DefaultWhiteButton>
          </a>
          <Link to="/contact">
            <DefaultOrangeButton id="btn-contact" className="btn">
              <ButtonTextWrapper>
                <span>Fill Out Our</span>
                <br />
                <span>Contact Form</span>
              </ButtonTextWrapper>
              <IoMdDocument className="icon icon-contact" />
            </DefaultOrangeButton>
          </Link>
        </ButtonWrapper>
      </ContentWrap>
    </StyledContactUs>
  )
}

const StyledContactUs = styled("section")`
  width: 100%;
  height: 200px;
  background-color: ${({ theme }) => theme.colors.secondary};

  @media (max-width: 850px) {
    padding: 2rem 1rem 2.5rem;
    height: auto;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2rem;
    text-transform: uppercase;
    margin-bottom: 1rem;

    @media (max-width: 850px) {
      text-align: center;
      font-size: 2rem;
    }
  }
`

const ButtonWrapper = styled("div")`
  display: flex;

  @media (max-width: 850px) {
    flex-direction: column;
  }

  #btn-contact {
    @media (max-width: 850px) {
      margin-top: 2rem;
    }
  }

  .btn {
    margin: 0 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 270px;
    height: 65px;

    @media (max-width: 850px) {
    }
  }

  .icon {
    font-size: 4rem;
  }

  .icon-call {
    margin-right: 1rem;
    color: ${({ theme }) => theme.colors.accent};
  }

  .icon-contact {
    margin-left: 1rem;
    color: ${({ theme }) => theme.colors.primary};
  }

  span {
    font-size: 1.3rem;
    text-transform: uppercase;
    font-weight: 600;
  }
`

const ButtonTextWrapper = styled("div")``

const ContentWrap = styled("div")`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
