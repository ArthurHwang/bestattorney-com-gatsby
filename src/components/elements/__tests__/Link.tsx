// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "src/components/utilities/themeMock.js"
import { Link } from "../Link"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <Link to="/case-results/rickey-prock">
        Motorcyclist hit while on the job
      </Link>
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Link", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should render", () => {
    const component = instance.find(el => el.type === "a")
    expect(component).toBeTruthy()
  })

  it("should render its children", () => {
    const component = instance.find(el => el.type === "a")
    expect(component.children).toBeTruthy()
  })

  it("should render correct href", () => {
    const component = instance.find(el => el.type === "a")
    expect(component.props.href).toBe("/case-results/rickey-prock")
  })
})
