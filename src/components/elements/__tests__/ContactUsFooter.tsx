// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "src/components/utilities/themeMock.js"
import { ContactUsFooter } from "../ContactUsFooter"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <ContactUsFooter />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Contact Us Footer", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
    console.log(instance)
  })

  //   it("should render", () => {
  //     const component = instance.find(
  //       el =>
  //         el.type === "span" &&
  //         el.props.className.includes("lazyload-img-wrapper")
  //     )
  //     expect(component).toBeTruthy()
  //   })

  //   it("should render its children", () => {
  //     const component = instance.find(
  //       el =>
  //         el.type === "span" &&
  //         el.props.className.includes("lazyload-img-wrapper")
  //     )
  //     expect(component.children).toBeTruthy()
  //   })
})
