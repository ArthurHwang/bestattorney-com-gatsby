// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "src/components/utilities/themeMock.js"
import { LazyLoad } from "../LazyLoad"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <LazyLoad>
        <img
          src="/images/bus-accidents/carrier-bus-crash.jpg"
          alt="California Bus Collision Lawyers"
          width="100%"
          title="Rights in a California Bus Crash"
        />
      </LazyLoad>
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("LazyLoad", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should render", () => {
    const component = instance.find(
      el =>
        el.type === "span" &&
        el.props.className.includes("lazyload-img-wrapper")
    )
    expect(component).toBeTruthy()
  })

  it("should render its children", () => {
    const component = instance.find(
      el =>
        el.type === "span" &&
        el.props.className.includes("lazyload-img-wrapper")
    )
    expect(component.children).toBeTruthy()
  })
})
