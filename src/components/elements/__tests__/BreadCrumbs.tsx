// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "src/components/utilities/themeMock.js"
import { BreadCrumbs } from "../BreadCrumbs"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  const mockLocation = {
    pathname: "/about-us"
  }
  container = renderer.create(
    <ThemeProviderWrapper>
      <BreadCrumbs location={mockLocation} />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Breadcrumbs", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should render", () => {
    const breadCrumb = instance.find(
      el => el.type === "a" && el.props.className === "active"
    )
    expect(breadCrumb).toBeTruthy()
  })

  it("should have correct ending breadcrumb", () => {
    const breadCrumb = instance.find(
      el => el.type === "a" && el.props.className === "active"
    )
    expect(breadCrumb.props.href).toBe("/about-us")
  })

  it("should always have home path as first breadcrumb", () => {
    const breadCrumbs = instance.findAllByType("a")
    expect(breadCrumbs[0].props.href).toBe("/")
  })
})
