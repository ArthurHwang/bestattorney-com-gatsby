import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"

interface Props {
  className: string
}

export const FooterDisclaimerAMP: React.FC<Props> = ({
  className
}): ReactElement => {
  return (
    <StyledDisclaimer className={className}>
      <p className="disclaimer-title"> Copyright & Disclaimer</p>
      <div className="text-container">
        <p className="disclaimer-body">
          <strong>Disclaimer:</strong> The legal information presented at this
          site should not be construed to be formal legal advice, nor the
          formation of an attorney-client relationship. Any results set forth
          here were dependent on the facts of that case and the results will
          differ from case to case. <strong>Bisnar Chase </strong> serves all of
          California. In addition, we represent clients in various other states
          through our affiliations with local law firms. Through the local firm
          we will be admitted to practice law in their state, pro hac vice.{" "}
          <Link to="/privacy-policy">Cookies &amp; Privacy Policy</Link> - All
          rights reserved. In-house internet marketing by Bisnar Chase.
          Location: 1301 Dove St, #120, Newport Beach, CA 92660
        </p>
      </div>
    </StyledDisclaimer>
  )
}

const StyledDisclaimer = styled("div")`
  .text-container {
    margin: 0 auto;
    max-width: 70%;
    padding-bottom: 1rem;

    @media (max-width: 480px) {
      max-width: 337px;
    }
  }

  .disclaimer-title {
    font-size: 2rem;
    height: 52px;
    text-align: center;
    border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};
    padding: 1rem 0;
    margin: 0;
  }

  .disclaimer-body {
    margin: 1rem auto 0;
    font-size: 1rem;
  }

  .disclaimer-copyright {
    margin: 0 auto 1rem;
    font-size: 1rem;
  }
`
