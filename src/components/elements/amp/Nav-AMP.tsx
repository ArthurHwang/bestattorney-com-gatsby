import React, { ReactElement } from "react"
import styled from "styled-components"
import Logo from "src/images/logo/bisnar-chase-logo.svg"
import { Link } from "src/components/elements/Link"
import { FaAngleRight } from "react-icons/fa"
import { IoIosMenu } from "react-icons/io"

export const NavAMP: React.FC = (): ReactElement => {
  return (
    <StyledAmpNav>
      <NavWrapperMobile>
        <Link to="/">
          <amp-img
            className="nav-logo"
            height="75"
            width="252.75"
            layout="fixed"
            alt="Bisnar Chase Personal Injury Attorneys"
            src={Logo}
          />
        </Link>
        {/* 
        //@ts-ignore */}
        <div
          role="button"
          // @ts-ignore
          on="tap:sidebar1.toggle"
          tabindex="0"
          class="hamburger"
        >
          <IoIosMenu className="menu-icon" />
        </div>
      </NavWrapperMobile>

      <amp-sidebar id="sidebar1" layout="nodisplay" side="left">
        {/* 
        //@ts-ignore */}
        <div
          role="button"
          aria-label="close sidebar"
          // @ts-ignore
          on="tap:sidebar1.toggle"
          tabindex="0"
          class="close-sidebar"
        >
          ✕
        </div>
        <MobileDropDownMenu className="sidebar">
          <ul>
            <li>
              <Link to="/">HOME</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/about-us">ABOUT US</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/practice-areas">PRACTICE AREAS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/case-results">RESULTS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/resources">RESOURCES</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/about-us/testimonials">CLIENT REVIEWS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/referrals">ATTORNEY REFFERALS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>

            <li>
              <Link to="/locations">LOCATIONS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/blog">BLOG</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/contact">CONTACT</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
          </ul>

          <ContactTextWrapper>
            <p style={{ borderTop: "medium double #ec7d29" }} />
            <p>inquiry@bestatto-gatsby.netlify.app</p>
            <p>(949) 203-3814</p>
          </ContactTextWrapper>
        </MobileDropDownMenu>
      </amp-sidebar>
    </StyledAmpNav>
  )
}

const ContactTextWrapper = styled("div")`
  margin-left: 2.7rem;
  animation: moveInLeft 1.3s;
  padding-right: 2.5em;
  bottom: 0;

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 1rem;
  }

  a {
    color: ${({ theme }) => theme.colors.accent};
  }
`

const MobileDropDownMenu = styled("div")`
  width: 100%;
  position: absolute;
  z-index: 1000;
  top: 0;
  left: 0;
  height: 100%;
  background-color: ${({ theme }) => theme.colors.secondary};
  color: ${({ theme }) => theme.colors.primary};
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  padding-top: 1rem;
  padding-bottom: 1rem;

  ul {
    color: ${({ theme }) => theme.colors.primary};
    list-style: none;
    list-style-position: inside;
    margin-left: 2.5em !important;

    li {
      margin: 1.2rem 0;
      animation: moveInLeft 1.3s;
      span {
        margin-left: 0.5rem;
        color: ${({ theme }) => theme.colors.accent};
      }
    }

    li a {
      color: ${({ theme }) => theme.colors.primary};
      font-size: 1.8rem;
    }
  }
`

const StyledAmpNav = styled("div")`
  .hamburger {
    padding: 1rem;
    padding-top: 1.7rem;
    font-size: 3.5rem;
  }
  .sidebar {
    padding: 10px;
    margin: 0;
  }
  .sidebar > li {
    list-style: none;
    margin-bottom: 10px;
  }
  .sidebar a {
    text-decoration: none;
  }
  .close-sidebar {
    font-size: 1.5em;
    padding-left: 5px;
  }

  #sidebar1 {
    width: 90vw;
    position: relative;
  }
`

const NavWrapperMobile = styled("div")`
  display: flex;
  position: fixed;
  justify-content: space-between;
  height: 70px;
  background-color: ${({ theme }) => theme.colors.primary};
  width: 100%;
  z-index: 15;
  top: 0;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};

  .menu-icon {
    font-size: 3.5rem;
  }
`
