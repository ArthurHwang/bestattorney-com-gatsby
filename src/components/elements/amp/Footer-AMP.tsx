import React, { ReactElement } from "react"
import styled from "styled-components"
import { FooterAddressAMP } from "./Footer_Address-AMP"
import { FooterConnectAMP } from "./Footer_Connect-AMP"
import { FooterDisclaimerAMP } from "./Footer_Disclaimer-AMP"
import { FooterMetroAMP } from "./Footer_Metro-AMP"
import { FooterSitemapAMP } from "./Footer_Sitemap-AMP"

export const FooterAMP: React.FC = (): ReactElement => {
  return (
    <StyledFooter>
      <FooterAddressAMP className="footer-address" />
      <FooterMetroAMP className="footer-metros" />
      <FooterSitemapAMP className="footer-sitemap" />
      <FooterConnectAMP className="footer-connect" />
      <FooterDisclaimerAMP className="footer-disclaimer" />
    </StyledFooter>
  )
}

const StyledFooter = styled("footer")`
  display: grid;
  grid-template-areas:
    "address address address"
    "sitemap connect metros"
    "disclaimer disclaimer disclaimer";
  border-top: 1px solid #222533;
  grid-template-rows: repeat(3, auto);
  grid-template-columns: 1fr 240px 1fr;
  height: auto;

  .footer-address {
    grid-area: address;
  }

  .footer-metros {
    grid-area: metros;
  }

  .footer-sitemap {
    grid-area: sitemap;
  }

  .footer-connect {
    grid-area: connect;
  }

  .footer-disclaimer {
    grid-area: disclaimer;
  }

  @media (max-width: 1411px) {
  }

  @media (max-width: 1160px) {
    grid-template-rows: repeat(4, auto);
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      "address address"
      "connect connect"
      "sitemap metros"
      "disclaimer disclaimer";
  }

  @media (max-width: 850px) {
    grid-template-rows: repeat(5, auto);
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      "address address"
      "connect connect"
      "metros metros"
      "sitemap sitemap"
      "disclaimer disclaimer";
  }
`
