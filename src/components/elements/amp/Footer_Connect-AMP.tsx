import React, { ReactElement } from "react"
import {
  FaFacebookF,
  FaInstagram,
  FaLinkedinIn,
  FaPinterest,
  FaTwitter,
  FaYoutube
} from "react-icons/fa"
import styled from "styled-components"
import DunBradstreet from "src/images/logo/dun-bradstreet-logo.png"
import Google from "src/images/logo/google-logo.png"
import Justia from "src/images/logo/justia-logo.png"

interface Props {
  className: string
}

export const FooterConnectAMP: React.FC<Props> = ({
  className
}): ReactElement => {
  return (
    <StyledConnect className={className}>
      <div className="connect">Connect</div>
      <a
        href="https://www.facebook.com/california.attorney"
        className="grid-item facebook"
      >
        <FaFacebookF />
      </a>
      <a href="https://twitter.com/bisnarchase" className="grid-item twitter">
        <FaTwitter />
      </a>
      <a
        href="https://www.instagram.com/bisnarchase"
        className="grid-item instagram"
      >
        <FaInstagram />
      </a>
      <a
        href="https://www.youtube.com/user/BisnarChaseAttorneys"
        className="grid-item youtube"
      >
        <FaYoutube />
      </a>
      <a
        href="https://www.pinterest.com/bisnarchase/"
        className="grid-item pinterest"
      >
        <FaPinterest />
      </a>
      <a
        href="https://www.linkedin.com/company/bisnar-chase"
        className="grid-item linkedin"
      >
        <FaLinkedinIn />
      </a>
      <a
        href="https://www.google.com/maps/place/Bisnar+Chase+Personal+Injury+Attorneys/@33.6620595,-117.8664064,15z/data=!4m5!3m4!1s0x0:0xae2eee17ae4e213e!8m2!3d33.6620595!4d-117.8664064"
        className="grid-item google"
      >
        <amp-img
          alt=""
          layout="fixed"
          width="43"
          height="43"
          className="google-icon"
          src={Google}
        />
      </a>
      <a
        href="https://www.dandb.com/verified/business/574414421/"
        className="grid-item dunbradstreet"
      >
        <amp-img
          alt=""
          layout="fixed"
          width="30"
          height="30"
          className="bradstreet-icon"
          src={DunBradstreet}
        />
      </a>
      <a
        href="https://lawyers.justia.com/lawyer/brian-doster-chase-145784"
        className="grid-item justia"
      >
        <amp-img
          alt=""
          layout="fixed"
          width="28"
          height="28"
          className="justia-icon"
          src={Justia}
        />
      </a>
    </StyledConnect>
  )
}

const StyledConnect = styled("div")`
  display: grid;
  grid-template-columns: 80px 80px 80px;
  grid-template-rows: 80px 50px 50px 50px;

  margin: 0 auto;

  img {
    margin: 0;
  }

  .grid-item {
    align-self: start;
    justify-self: center;
    font-size: 2.5rem;

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  .connect {
    border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};
    grid-column: 1 / -1;
    padding: 1rem;
    text-align: center;
    font-size: 2rem;
    align-self: center;
  }

  .facebook {
    color: #3c5a99;
  }

  .twitter {
    color: #26a7de;
  }

  .instagram {
    color: #3f729b;
  }

  .youtube {
    color: #c4302b;
  }

  .pinterest {
    color: #c8232c;
  }

  .linkedin {
    color: #0077b5;
    position: relative;
    top: -0.2rem;
  }

  .google {
    width: 35px;
  }

  .google-icon {
    position: relative;
    bottom: 3px;
    right: 3px;
  }

  .dunbradstreet {
    width: 30px;
  }

  .bradstreet-icon {
    position: relative;
    left: 2px;
  }

  .justia {
    width: 30px;
  }

  .justia-icon {
    position: relative;
    left: 2px;
  }

  @media (max-width: 1160px) {
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    width: 824px;
  }

  @media (max-width: 850px) {
    max-width: 400px;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: 80px 50px 50px 50px;
  }

  @media (max-width: 425px) {
    max-width: 100%;
  }
`
