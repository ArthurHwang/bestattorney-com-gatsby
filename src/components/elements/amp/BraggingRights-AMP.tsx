import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import AsSeenOn from "src/images/logo/as-seen-on.jpg"
import CAOC from "src/images/logo/consumer-attorneys-of-california.png"
import Newsweek from "src/images/logo/newsweeklogo.png"
import Time from "src/images/logo/1280px-Time_Magazine_logo.svg.png"
import TrialAdvocates from "src/images/logo/american-trial-lawyers.png"
import FiveMillion from "src/images/2019-assets/500-million.png"
import Distinguished from "src/images/logo/distinguished-justice-advocates.png"
import AmericasTop from "src/images/logo/americas-top-100.png"
import { FaAngleRight } from "react-icons/fa"

interface Props {
  borderTop?: boolean
}

export const BraggingRightsAMP: React.FC<Props> = ({
  borderTop = false
}): ReactElement => {
  return (
    <StyledBraggingRights bordertop={borderTop}>
      <ContentWrapper>
        <BadgeWrapper>
          <Link
            className="badge"
            to="https://abc7.com/former-oc-officer-files-lawsuit-against-ford-over-co-leak-in-patrol-cars/2274376/"
          >
            <amp-img
              alt="As Seen On NBC FOX CBS ABC"
              layout="fixed"
              src={AsSeenOn}
              height="67"
              width="100"
            />
          </Link>
          <Link className="badge" to="/blog/chase-ritsema-attorney-award">
            <amp-img
              alt="Consumer Attorneys of California"
              layout="fixed"
              src={CAOC}
              width="150"
              height="53"
            />
          </Link>

          <Link className="badge" to="https://www.abota.org/index.cfm">
            <amp-img
              alt="American Board of Trial Advocates"
              layout="fixed"
              src={TrialAdvocates}
              width="135"
              height="86"
            />
          </Link>
          <Link className="badge" to="https://aaoaus.com/million-dollar-club/">
            <amp-img
              alt="Five Millon Dollar Club"
              layout="fixed"
              src={FiveMillion}
              width="150"
              height="110"
            />
          </Link>
          <Link
            className="badge"
            to="https://distinguishedjusticeadvocates.com/"
          >
            <amp-img
              alt="Distinguished Justice Advocates"
              layout="fixed"
              src={Distinguished}
              width="105"
              height="105"
            />
          </Link>
          <Link
            className="badge"
            to="https://www.top100highstakeslitigators.com/listing/brian-d-chase/"
          >
            <amp-img
              alt="Americas Top 100 Personal Injury Attorneys"
              layout="fixed"
              src={AmericasTop}
              width="100"
              height="100"
            />
          </Link>

          <Link className="badge" to="/about-us/lawyer-reviews-ratings">
            <amp-img
              alt="Newsweek Magazine"
              layout="fixed"
              src={Newsweek}
              width="175"
              height="27"
            />
          </Link>
          <Link className="badge" to="/about-us/lawyer-reviews-ratings">
            <amp-img
              alt="Time Magazine"
              layout="fixed"
              src={Time}
              width="115"
              height="35"
            />
          </Link>
        </BadgeWrapper>

        <TextWrapper>
          <Link className="badge" to="/about-us/lawyer-reviews-ratings">
            See our reviews and rewards{" "}
            <span className="right-icon">
              <FaAngleRight />
            </span>
          </Link>
        </TextWrapper>
      </ContentWrapper>
    </StyledBraggingRights>
  )
}

const StyledBraggingRights = styled("section")<{ bordertop: boolean }>`
  height: 200px;
  border-top: ${props =>
    props.bordertop ? `1px solid ${props.theme.colors.secondary}` : "null"};

  @media (max-width: 1160px) {
    height: 300px;
  }

  @media (max-width: 715px) {
    height: 500px;
  }
`

const ContentWrapper = styled("div")`
  height: 100%;
`

const TextWrapper = styled("div")`
  text-align: center;
  position: relative;
  bottom: 2.5rem;
  height: 10%;

  .right-icon {
    position: relative;
    top: 0.1rem;
    font-size: 1rem;
    color: ${({ theme }) => theme.colors.accent};
  }
`

const BadgeWrapper = styled("div")`
  padding: 2rem 2rem 2rem;
  max-width: 80%;
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  justify-content: space-between;
  align-content: space-around;
  align-items: center;
  margin: 0 auto;

  .badge {
    transition: 0.3s ease all;

    &:hover {
      transform: scale(1.05);
    }
  }

  @media (max-width: 1550px) {
    max-width: 95%;
  }

  @media (max-width: 1160px) {
    max-width: 675px;
  }

  @media (max-width: 715px) {
    max-width: 427px;
    justify-content: space-evenly;
    align-items: center;
    padding-bottom: 3rem;
  }
`
