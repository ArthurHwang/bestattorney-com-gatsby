import React, { ReactElement } from "react"
import MobileHeaderWEBP from "src/images/bisnar-chase-mobile-headerv2.webp"
import MobileHeaderJPG from "src/images/bisnar-chase-mobile-bannerV2.jpg"
import styled from "styled-components"

export const HeaderAMP: React.FC = (): ReactElement => {
  return (
    <StyledDefaultHeader>
      <div className="fixed-height-container">
        <amp-img
          className="cover"
          layout="fill"
          src={MobileHeaderWEBP}
          alt="Bisnar Chase Personal Injury Attorneys"
        >
          <amp-img
            className="cover"
            fallback={true}
            layout="fill"
            src={MobileHeaderJPG}
            alt="Bisnar Chase Personal Injury Attorneys"
          />
        </amp-img>
      </div>
    </StyledDefaultHeader>
  )
}

const StyledDefaultHeader = styled("header")`
  .fixed-height-container {
    height: 250px;
    position: relative;
    width: 100%;

    img {
      object-fit: cover;
    }
  }
`
