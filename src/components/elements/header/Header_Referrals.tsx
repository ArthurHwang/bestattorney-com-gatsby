import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"

const REFERRALS_HEADER_QUERY = graphql`
  query {
    referralsDesktop: file(
      relativePath: { eq: "headers/referrals-content-bg-crop.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 1400, srcSetBreakpoints: [1400]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }

    referralsMobile: file(
      relativePath: { eq: "headers/referrals-content-bg-tablet.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 700, srcSetBreakpoints: [700]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const HeaderReferrals: React.FC = (): ReactElement => {
  return (
    <StaticQuery
      query={REFERRALS_HEADER_QUERY}
      render={data => {
        const sources = [
          data.referralsMobile.childImageSharp.fluid,
          {
            ...data.referralsDesktop.childImageSharp.fluid,
            media: `(min-width: 1025px)`
          }
        ]

        return (
          <StyledReferralsHeader>
            <Img
              alt="Bisnar Chase Referrals"
              className="referrals-header"
              loading="eager"
              fluid={sources}
            />
          </StyledReferralsHeader>
        )
      }}
    />
  )
}

const StyledReferralsHeader = styled("header")`
  .referrals-header {
    min-height: 400px;

    @media (max-width: 1024px) {
      min-height: 160px;
    }
  }
`
