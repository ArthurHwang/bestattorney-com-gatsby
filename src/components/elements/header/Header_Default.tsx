import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"

const DEFAULT_HEADER_QUERY = graphql`
  query {
    defaultDesktop: file(relativePath: { eq: "bisnar-chase-2019-banner.png" }) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    defaultMobile: file(
      relativePath: { eq: "bisnar-chase-mobile-bannerV2.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const HeaderDefault: React.FC = (): ReactElement => {
  return (
    <StaticQuery
      query={DEFAULT_HEADER_QUERY}
      render={data => {
        const sources = [
          data.defaultMobile.childImageSharp.fluid,
          {
            ...data.defaultDesktop.childImageSharp.fluid,
            media: `(min-width: 1025px)`
          }
        ]
        return (
          <StyledDefaultHeader>
            <Img
              alt="Bisnar Chase 2019 attorneys"
              className="default-header"
              loading="eager"
              fluid={sources}
            />
          </StyledDefaultHeader>
        )
      }}
    />
  )
}

const StyledDefaultHeader = styled("header")`
  .default-header {
    max-height: 320px;
    min-height: 250px;

    @media (max-width: 1024px) {
      height: 200px;
    }

    @media (min-width: 2130px) {
      min-height: 400px;
      max-height: 600px;
    }

    img {
      @media (min-width: 1025px) {
        object-position: 50% 100% !important;
      }

      @media (max-width: 1024px) {
        object-position: 50.6% center !important;
      }
    }
  }
`
