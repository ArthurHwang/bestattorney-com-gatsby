import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { ContactPAGEO } from "../forms/Form_PAGEO"

const PAGEO_HEADER_QUERY = graphql`
  query {
    pageoDesktop: file(relativePath: { eq: "content-pa-bg.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 900, srcSetBreakpoints: [900]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    pageoMobile: file(relativePath: { regex: "/content-pa-bg-mobile.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 400, srcSetBreakpoints: [400]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const HeaderPAGEO: React.FC<any> = ({ pageSubject }): ReactElement => {
  let formTitle: string
  let contactText: string

  switch (pageSubject) {
    case "dog-bite":
      formTitle = "Do I have a case for my dog bite injury?"
      contactText = "Brief description of dog bite incident"
      break
    case "truck-accident":
      formTitle = "Do I have a case for my truck accident injury?"
      contactText = "Brief description of your truck accident"
      break
    case "bicycle-accident":
      formTitle = "Do I have a case for my bicycle accident injury?"
      contactText = "Brief description of your bicycle accident"
      break
    case "pedestrian-accident":
      formTitle = "Do I have a case for my pedestrian accident injury?"
      contactText = "Brief description of your pedestrian accident"
      break
    case "motorcycle-accident":
      formTitle = "Do I have a case for my motorcycle accident injury?"
      contactText = "Brief description of your motorcycle accident"
      break
    case "car-accident":
      formTitle = "Do I have a case for my car accident injury?"
      contactText = "Brief description of your car accident"
      break
    case "auto-defect":
      formTitle = "Do I have a case for my auto defect?"
      contactText = "Brief description of your auto defect"
      break
    case "product-defect":
      formTitle = "Do I have a case for my product defect injury?"
      contactText = "Brief description of your product defect"
      break
    case "drug-defect":
      formTitle = "Do I have a case for my defective drug injury?"
      contactText = "Brief description of your defective drug injury"
      break
    case "medical-defect":
      formTitle = "Do I have a case for my medical defect injury?"
      contactText = "Brief description of your medical defect injury"
      break
    case "premises-liability":
      formTitle = "Do I have a premises liability case?"
      contactText = "Brief description of your premises liability incident"
      break
    case "employment-law":
      formTitle = "Do I have an employment law violation case?"
      contactText = "Brief description of your employment situation"
      break
    case "wrongful-death":
      formTitle = "Do I have a wrongful death case?"
      contactText = "Brief description of the wrongful death circumstances"
      break
    case "class-action":
      formTitle = "Do I have a class action case?"
      contactText = "Brief description of your class action case"
      break
    default:
      formTitle = "Do I have a case for my personal injury?"
      contactText = "Brief description of your personal injury"
      break
  }

  return (
    <StaticQuery
      query={PAGEO_HEADER_QUERY}
      render={data => {
        const sources = [
          data.pageoMobile.childImageSharp.fluid,
          {
            ...data.pageoDesktop.childImageSharp.fluid,
            media: `(min-width: 769px)`
          }
        ]
        return (
          <StyledPAGEOHeader>
            <Img
              alt="Bisnar Chase Practice Areas and Locations"
              className="pageo-header"
              loading="eager"
              fluid={sources}
            />

            <ContactPAGEO
              className="PAGEO-contact"
              title={formTitle}
              text={contactText}
            />
            <div className="mobile-text-block">
              <div className="mobile-content-wrap">
                <p className="mobile-title">{formTitle}</p>
                <p className="mobile-subtext">
                  <span>Free Case Evaluation</span> - Our full time staff is
                  ready to evaluate your case submission and will respond in a
                  timely manner.
                </p>
              </div>
            </div>
            <div className="opacity-container" />
          </StyledPAGEOHeader>
        )
      }}
    />
  )
}

const StyledPAGEOHeader = styled("header")`
  position: relative;
  background-color: #150b02;

  .PAGEO-contact {
    @media (max-width: 768px) {
      display: none;
    }
  }

  .pageo-header {
    height: 400px;
    max-height: 400px;
    margin: 0 auto;
    max-width: 1300px;
    position: relative;
    right: 6rem;

    @media (max-width: 768px) {
      height: 200px;
      max-height: 200px;
      right: initial;

      img {
        filter: blur(2.5px);
        object-position: 36% center !important;
      }
    }
  }

  .opacity-container {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    margin: auto;
    height: 100%;
    width: 325px;
    background: #000;
    opacity: 0.5;

    @media (min-width: 769px) {
      display: none;
    }
  }

  .mobile-text-block {
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 1;

    @media (min-width: 769px) {
      display: none;
    }

    & .mobile-content-wrap {
      width: 300px;
      margin: 0 auto;
      display: flex;
      flex-direction: column;
      height: 100%;
      justify-content: center;

      p {
        text-align: center;
        color: ${({ theme }) => theme.colors.primary};
        text-transform: uppercase;
        font-weight: 500;
      }

      p.mobile-title {
        border-bottom: 1px solid ${({ theme }) => theme.colors.accent};
        margin-bottom: 0.5rem;
        font-size: 1.6rem;
      }
      p.mobile-subtext {
        font-style: italic;
        margin-bottom: 0;
        font-size: 1rem;
      }

      span {
        text-decoration: underline;
        color: ${({ theme }) => theme.colors.accent};
        font-weight: 600;
      }
    }
  }
`
