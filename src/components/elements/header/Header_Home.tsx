import { graphql, StaticQuery } from "gatsby"
import React, { ReactElement } from "react"
import Media from "react-media"
import Slider from "react-slick"
import styled from "styled-components"
import Video from "src/images/video/bc-brand-video.mp4"
import VideoEncoded from "src/images/carousel//bc-brand-video-encoded.mp4"
import VideoWebm from "src/images/carousel/bc-brand-video.webm"
import "./slick.css"
import { Slide1 } from "../carousel/Carousel_Slide1"
import { Slide2 } from "../carousel/Carousel_Slide2"
import { Slide3 } from "../carousel/Carousel_Slide3"
import { Slide4 } from "../carousel/Carousel_Slide4"
import { Slide5 } from "../carousel/Carousel_Slide5"
import { HeaderHomeMobile } from "./Header_Home-Mobile"

const CAROUSEL_IMAGE_QUERY = graphql`
  query {
    heroImage: file(
      relativePath: { eq: "carousel/unoptimized/brian-chase-john-bisnar.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 1600, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    staffImage: file(relativePath: { eq: "carousel/BC-2019-staff.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    officeImage: file(relativePath: { eq: "carousel/BC-office.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
    awardsImage: file(relativePath: { eq: "carousel/BC-awards.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
  }
`

export const HeaderHome: React.FC = (): ReactElement => {
  const settings = {
    autoplay: true,
    autoplaySpeed: 9000,
    dots: true,
    fade: true,
    infinite: true,
    lazyLoad: "ondemand",
    pauseOnFocus: false,
    pauseOnHover: false,
    slidesToScroll: 1,
    slidesToShow: 1,
    speed: 2500
  }
  return (
    <StaticQuery
      query={CAROUSEL_IMAGE_QUERY}
      render={data => (
        // ! WORKING
        <header>
          <Media query="(max-width: 1024px)">
            {matches =>
              matches ? (
                <MobileHeader className="mobile-home-header">
                  <HeaderHomeMobile
                    video={Video}
                    videoEncoded={VideoEncoded}
                    videoWebm={VideoWebm}
                  />
                </MobileHeader>
              ) : (
                <DesktopHeader className="desktop-home-header">
                  {/* 
                  //@ts-ignore */}
                  <Slider {...settings}>
                    <Slide1 img={data.heroImage.childImageSharp.fluid} />
                    <Slide2 img={data.staffImage.childImageSharp.fluid} />
                    <Slide3
                      video={Video}
                      videoEncoded={VideoEncoded}
                      videoWebm={VideoWebm}
                    />
                    <Slide4 img={data.officeImage.childImageSharp.fluid} />
                    <Slide5 img={data.awardsImage.childImageSharp.fluid} />
                  </Slider>
                </DesktopHeader>
              )
            }
          </Media>
        </header>
      )}
    />
  )
}

const MobileHeader = styled("div")`
  height: 100%;

  .gatsby-image-wrapper {
    height: 100%;
  }

  @media (min-width: 1025px) {
    display: none;
  }
`

const DesktopHeader = styled("section")`
  height: calc(100vh - 115px);

  @media (max-width: 1024px) {
    display: none;
  }

  .gatsby-image-wrapper {
    height: 100%;
  }

  .slick-track {
    height: calc(100vh - 115px);
  }

  .slick-prev {
    left: 3%;
    z-index: 10;
    bottom: 2rem;
    opacity: 0.6;

    &::before {
      color: ${({ theme }) => theme.colors.accent};
    }

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
      opacity: 1;
    }
  }

  .slick-next {
    right: 3%;
    z-index: 10;
    bottom: 2rem;
    opacity: 0.6;

    &::before {
      color: ${({ theme }) => theme.colors.accent};
    }

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
      opacity: 1;
    }
  }

  .slick-dots {
    bottom: 0.3rem;
    z-index: 100;

    li {
      position: relative;
      left: 1px;
    }

    & li button:before {
      color: white;
    }

    & li.slick-active button::before {
      color: ${({ theme }) => theme.colors.accent};
      opacity: 1;
    }
  }
`
