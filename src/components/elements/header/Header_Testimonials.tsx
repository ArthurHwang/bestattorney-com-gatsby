import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"

const TESTIMONIALS_HEADER_QUERY = graphql`
  query {
    testimonialsDesktop: file(
      relativePath: { regex: "/testimonials-content-bg.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    testimonialsMobile: file(
      relativePath: { regex: "/testimonials-content-bg-tablet.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 700, srcSetBreakpoints: [700]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const HeaderTestimonials: React.FC = (): ReactElement => {
  return (
    <StaticQuery
      query={TESTIMONIALS_HEADER_QUERY}
      render={data => {
        const sources = [
          data.testimonialsMobile.childImageSharp.fluid,
          {
            ...data.testimonialsDesktop.childImageSharp.fluid,
            media: `(min-width: 1025px)`
          }
        ]
        return (
          <StyledTestimonialsHeader>
            <Img
              alt="Bisnar Chase Testimonials"
              className="testimonials-header"
              loading="eager"
              fluid={sources}
              style={{ minHeight: "600px" }}
            />
          </StyledTestimonialsHeader>
        )
      }}
    />
  )
}

const StyledTestimonialsHeader = styled("header")`
  background-color: #eee;
  .testimonials-header {
    max-width: 1903px;
    margin: 0 auto;
    min-height: 600px;

    @media (max-width: 1024px) {
      height: auto;
      min-height: 200px !important;
    }

    @media (max-width: 500px) {
      img {
        transform: scale(1.2);
      }
    }
  }
`
