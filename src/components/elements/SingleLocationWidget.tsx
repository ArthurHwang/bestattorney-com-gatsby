import React, { ReactElement } from "react"
import styled from "styled-components"
import { FaMapMarkerAlt } from "react-icons/fa"
import { LazyLoad } from "../elements/LazyLoad"
import { Link } from "../elements/Link"

export const SingleLocationWidget: React.FC<any> = (props): ReactElement => {
  const avgInjuries = Math.round(
    (props.locationBox1.totalAccidents * 200) / props.locationBox1.population
  )

  function cityStringParse(city: string): string {
    if (city.includes(" ")) {
      return city.split(" ").join("-")
    }
    return city
  }

  return (
    <StyledSingleLocationWidget>
      <StyledTopContent
        dangerouslySetInnerHTML={{ __html: props.topContent }}
      />
      <div className="hr" />
      <StyledBox1>
        <div className="col-1">
          <h3>
            <FaMapMarkerAlt /> City of {props.locationBox1.city}
          </h3>
          <p>Population: {props.locationBox1.population}</p>
        </div>

        <div className="col-2">
          <p>Most Dangerous Intersection in {props.locationBox1.city}:</p>
          <p className="col-2-city">{props.locationBox1.intersection1}</p>
          <p>{props.locationBox2.population}</p>
          <p>
            Last 5 Years: {props.locationBox1.intersection1Accidents} Accidents
            | {props.locationBox1.intersection1Injuries} Injuries |{" "}
            {props.locationBox1.intersection1Deaths} Deaths
          </p>
        </div>
      </StyledBox1>
      <div className="hr" />
      <StyledBox2 dataCheck={props.locationBox2}>
        <div className="col-1">
          <div className="top-content">
            <p className="content">Avg Annual Auto Injuries per 1,000:</p>
            <p className="average">{avgInjuries}</p>
          </div>
          <div className="bottom-content">
            <div className="bottom-left-content">
              <p>{props.locationBox1.city} Crime Index*</p>
              <p id="bottom-avg" className="average">
                {props.locationBox2.mainCrimeIndex}
              </p>
            </div>

            {props.locationBox2.city1Name && (
              <div className="bottom-right-content">
                <p>Nearby Cities:</p>
                <div className="related-city">
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city1Name}:{" "}
                  </span>
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city1Index}
                  </span>
                </div>
                <div className="related-city">
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city2Name}:{" "}
                  </span>
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city2Index}
                  </span>
                </div>
                <div className="related-city">
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city3Name}:{" "}
                  </span>
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city3Index}
                  </span>
                </div>

                <div className="related-city">
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city4Name}:{" "}
                  </span>
                  <span style={{ fontSize: "0.9rem" }}>
                    {props.locationBox2.city4Index}
                  </span>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="col-2">
          <table>
            <thead>
              <tr>
                <th>More Unsafe Intersections**</th>
                <th>Accidents</th>
                <th>Injuries</th>
                <th style={{ paddingRight: "8px" }}>Deaths</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <a
                    target="_blank"
                    href={`https://www.google.com/maps/place/${props.locationBox3.intersection2.replace(
                      / /gi,
                      "+"
                    )}+${props.locationBox1.city}`}
                  >
                    {props.locationBox3.intersection2}
                  </a>
                </td>

                <td>{props.locationBox3.intersection2Accidents}</td>
                <td>{props.locationBox3.intersection2Injuries}</td>
                <td>{props.locationBox3.intersection2Deaths}</td>
              </tr>

              <tr>
                <td>
                  <a
                    target="_blank"
                    href={`https://www.google.com/maps/place/${props.locationBox3.intersection3.replace(
                      / /gi,
                      "+"
                    )}+${props.locationBox1.city}`}
                  >
                    {props.locationBox3.intersection3}
                  </a>
                </td>

                <td>{props.locationBox3.intersection3Accidents}</td>
                <td>{props.locationBox3.intersection3Injuries}</td>
                <td>{props.locationBox3.intersection3Deaths}</td>
              </tr>

              <tr>
                <td>
                  <a
                    target="_blank"
                    href={`https://www.google.com/maps/place/${props.locationBox3.intersection4.replace(
                      / /gi,
                      "+"
                    )}+${props.locationBox1.city}`}
                  >
                    {props.locationBox3.intersection4}
                  </a>
                </td>

                <td>{props.locationBox3.intersection4Accidents}</td>
                <td>{props.locationBox3.intersection4Injuries}</td>
                <td>{props.locationBox3.intersection4Deaths}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </StyledBox2>

      <div className="hr" />

      <StyledBox3>
        <div className="col-3">
          <LazyLoad>
            <img
              style={{ marginBottom: "0", width: "100%", height: "100%" }}
              src={`/images/location-images/city-images/${props.locationBox1.city
                .toLowerCase()
                .replace(/ /gi, "-")}-street-view.jpg`}
              alt=""
            />
          </LazyLoad>
        </div>
        <div className="col-2">
          <LazyLoad>
            <iframe
              width="100%"
              height="98.4%"
              frameBorder="0"
              style={{ border: "0" }}
              src={`https://www.google.com/maps/embed/v1/place?zoom=14&key=${
                process.env.GATSBY_GOOGLE_MAPS_APIKEY
              }&q=${props.locationBox1.intersection1
                .replace("&", "and")
                .replace(/ /gi, "+")}+${props.locationBox1.city}`}
            />
          </LazyLoad>
        </div>
      </StyledBox3>

      <p style={{ fontSize: "0.9rem", margin: "0", marginTop: "0.5rem" }}>
        * Crime index data taken from{" "}
        <Link
          to={`https://www.city-data.com/city/${cityStringParse(
            props.locationBox1.city
          )}-California.html`}
        >
          city-data.com.
        </Link>{" "}
        The national average is 315.5. The crime index is calculated using the
        population and severity of crimes vs the frequency of crimes committed.
      </p>
      <p style={{ fontSize: "0.9rem", margin: "0" }}>
        **All intersection data is taken from the{" "}
        <Link to="http://iswitrs.chp.ca.gov/">
          California Statewide Integrated Traffic Records System
        </Link>{" "}
        and is measured over the last 5 years of local and arterial street car
        accidents (non-highway). The data is not updated in real time.{" "}
      </p>
    </StyledSingleLocationWidget>
  )
}

const StyledSingleLocationWidget = styled("div")`
  border: 2px solid black;
  padding: 1rem;
  box-shadow: 0 0 200px -20px #e0f1ff inset;
  margin-bottom: 2rem;

  .hr {
    border: medium double grey;
    margin-bottom: 1rem;
    margin-top: 1rem;
  }
`

const StyledTopContent = styled("div")``

const StyledBox1 = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: center;
  margin-bottom: 1rem;

  @media (max-width: 870px) {
    grid-template-columns: 1fr;
  }

  h3 {
    font-size: 2.8rem;
    text-align: center;

    @media (max-width: 500px) {
      font-size: 2rem;
    }
  }

  p {
    text-align: center;
    margin: 0;
  }

  .col-1 {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .col-2-city {
    font-size: 2.6rem;
    color: ${({ theme }) => theme.links.normal};
    @media (max-width: 500px) {
      font-size: 1.8rem;
    }
  }
`
const StyledBox2 = styled("div")<{ dataCheck: { city1Name: string } }>`
  display: grid;
  grid-template-columns: 1fr 1fr;

  @media (max-width: 1572px) {
    grid-template-columns: 1fr;
    grid-template-rows: auto auto;
  }

  @media (max-width: 870px) {
    grid-template-columns: 1fr;
  }

  .col-1 {
    border-right: 2px solid black;

    @media (max-width: 1572px) {
      border: none;
    }

    p {
      margin: 0;
    }

    .bottom-content {
      display: grid;
      grid-template-columns: ${props =>
        props.dataCheck.city1Name ? "1.5fr 1fr" : "1fr"};
      align-items: center;
      justify-items: center;
      padding-top: 1rem;

      .bottom-left-content {
        display: flex;
        flex-direction: column;
        justify-content: center;
      }

      #bottom-avg {
        font-size: 3rem;
        text-align: center;
      }
    }

    .top-content {
      display: flex;
      justify-content: space-between;
      border-bottom: 2px solid black;

      .content {
        font-size: 1.2rem;
        width: 100%;
        align-self: center;
        text-align: center;
      }

      .average {
        background-color: yellow;
        color: ${({ theme }) => theme.colors.accent};
        font-size: 3rem;
        width: 5rem;
        text-align: center;
        font-weight: 600;
        border-left: 1px solid black;
      }
    }
  }

  .col-2 {
    padding-left: 1rem;
    padding-top: 1rem;

    @media (max-width: 1572px) {
      padding-left: 0;
    }

    @media (max-width: 1200px) {
      margin-top: 1rem;
      margin-bottom: -1rem;
    }

    table {
      width: 100%;
      border-collapse: collapse;

      th {
        @media (max-width: 1200px) {
          padding: 0 0.5rem;
          font-size: 1rem;
        }
      }

      td {
        @media (max-width: 1200px) {
          padding: 0 0.5rem;
          font-size: 0.8rem;
        }
      }
    }
  }
`
const StyledBox3 = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto;

  img {
    width: 100%;
    height: 100%;
    margin-bottom: 0;
  }

  iframe {
    @media (max-width: 700px) {
      height: 100% !important;
    }
  }

  @media (max-width: 870px) {
    grid-template-columns: 1fr;
    grid-template-rows: auto 350px;
  }

  @media (max-width: 450px) {
    grid-template-columns: 1fr;
    grid-template-rows: auto 250px;
  }
`
