// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "../../../../components/utilities/themeMock.js"
import { Contact } from "../Form_Contact"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <Contact />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Contact Page Form", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should have a LETS TALK button", () => {
    const button = instance.findByType("button")
    expect(button.props.type).toBe("submit")
    expect(button.props.children).toContain("LETS TALK")
  })

  it("should include text Pongase en contacto con nosotros", () => {
    const titleElement = instance.find(
      el => el.type === "p" && el.props.className === "title"
    )
    expect(titleElement.props.children[0].props.children).toBe("FREE")
    expect(titleElement.props.children[1]).toBe(" Case Evaluation")
  })

  it("should have an name input", () => {
    const nameInput = instance.find(
      el => el.type === "input" && el.props.name === "name"
    )
    expect(nameInput).toBeTruthy()
  })

  it("should have an email input", () => {
    const emailInput = instance.find(
      el => el.type === "input" && el.props.name === "email"
    )
    expect(emailInput).toBeTruthy()
  })

  it("should have an phone number input", () => {
    const phoneInput = instance.find(
      el => el.type === "input" && el.props.name === "number"
    )
    expect(phoneInput).toBeTruthy()
  })

  it("should have an textarea", () => {
    const nameInput = instance.find(
      el => el.type === "textarea" && el.props.name === "message"
    )
    expect(nameInput).toBeTruthy()
  })
})
