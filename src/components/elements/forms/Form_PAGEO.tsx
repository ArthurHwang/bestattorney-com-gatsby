import React, { ReactElement } from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { DefaultOrangeButton } from "../../utilities"

interface Props {
  title: string
  text: string
  className: string
}

export const ContactPAGEO: React.FC<Props> = ({
  title,
  text,
  className
}): ReactElement => {
  return (
    <FormWrapper className={className}>
      <ContentWrapper>
        <TextWrapper>
          <p className="title">{title}</p>
          <p className="subtext">
            <span>Free Case Evaluation</span> - Our full time staff is ready to
            evaluate your case submission and will respond in a timely manner.
          </p>
        </TextWrapper>
        <StyledContact
          method="post"
          action="/thank-you"
          name="PAGEO-contact"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
        >
          <input type="hidden" name="bot-field" />
          <input type="hidden" name="form-name" value="PAGEO-contact" />
          <div className="name credentials-left">
            <input
              autoComplete="off"
              type="text"
              placeholder="NAME:"
              name="name"
              id="name"
              required={true}
            />
          </div>
          <div className="email credentials-left">
            <input
              autoComplete="off"
              type="text"
              placeholder="EMAIL ADDRESS:"
              name="email"
              id="email"
              required={true}
            />
          </div>
          <div className="number credentials-left">
            <input
              autoComplete="off"
              type="text"
              placeholder="PHONE NUMBER:"
              name="number"
              id="number"
              pattern="[0-9]{10}"
              required={true}
            />
          </div>
          <div className="message credentials-right">
            <textarea
              autoComplete="off"
              name="message"
              placeholder={`${text.toUpperCase()} :`}
              id="message"
              rows={6}
              required={true}
            />
          </div>
          <div className="submit">
            <p>
              *Submitting this form does not create an attorney-client
              relationship*
            </p>
            <DefaultOrangeButton className="submit-btn" type="submit">
              LETS TALK
              <span>
                <FaArrowAltCircleRight />
              </span>
            </DefaultOrangeButton>
          </div>
        </StyledContact>
      </ContentWrapper>
    </FormWrapper>
  )
}

const ContentWrapper = styled("div")`
  margin: 0 auto;
  max-width: 722px;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
  left: 22rem;

  form {
    margin-bottom: 0;
  }

  @media (max-width: 1250px) {
    left: 0;
  }
`

const FormWrapper = styled("section")`
  position: absolute;
  width: 100%;
  top: 0;
  height: 100%;
`

const TextWrapper = styled("div")`
  p {
    color: ${({ theme }) => theme.colors.primary};
    text-transform: uppercase;
    font-style: italic;
    text-align: left;
  }

  span {
    text-decoration: underline;
    color: ${({ theme }) => theme.colors.accent};
    font-weight: 600;
  }

  .title {
    font-size: 2.2rem;
    border-bottom: 1px solid ${({ theme }) => theme.colors.accent};
    margin-bottom: 0.5rem;
    font-weight: 500;

    @media (max-width: 1024px) {
      font-size: 1.8rem;
    }
  }

  .subtext {
    font-size: 1.2rem;
    font-weight: 500;
    margin-bottom: 0.7rem;

    @media (max-width: 1024px) {
      font-size: 1rem;
    }
  }
`

const StyledContact = styled.form`
  display: grid;
  grid-gap: 1.8rem;
  grid-template-rows: auto auto auto 1fr;
  grid-template-columns: repeat(4, 1fr);
  grid-template-areas:
    "name name message message"
    "email email message message"
    "number number message message"
    "submit submit submit submit";

  @media (max-width: 1024px) {
    grid-gap: 1rem;
  }

  .credentials-left {
    width: 100%;

    input {
      width: 100%;
      height: 40px;
      padding-left: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;
    }
  }

  .credentials-right {
    width: 100%;

    textarea {
      resize: none;
      width: 100%;
      height: 100%;
      padding: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;

      &::placeholder {
        font-size: 1.2rem;
      }
    }
  }

  .submit {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-area: submit;

    p {
      color: #8e8f88;
      font-size: 0.9rem;
      text-transform: uppercase;
      margin-bottom: 0;
      align-self: center;

      @media (max-width: 1024px) {
        font-size: 1rem;
      }

      @media (max-width: 650px) {
        font-size: 0.8rem;
      }
    }

    span {
      position: relative;
      left: 0.6rem;
      font-size: 1.3rem;
    }

    .submit-btn {
      color: ${({ theme }) => theme.colors.secondary};
      width: 15rem;
      height: 3.5rem;
      font-weight: 600;
      justify-self: flex-end;
    }
  }

  .name {
    grid-area: name;
  }

  .email {
    grid-area: email;
  }

  .number {
    grid-area: number;
  }

  .message {
    grid-area: message;
  }
`
