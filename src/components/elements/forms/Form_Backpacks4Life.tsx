import React, { ReactElement } from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { DefaultOrangeButton } from "../../utilities"

export const BackpacksContact: React.FC = (): ReactElement => {
  return (
    <FormWrapper>
      <TextWrapper>
        <p className="title">
          Contact Us to Request Backpacks or Donate Supplies!
        </p>
        <p className="subtext">
          There is <span>no</span> cost <span>or</span> obligation
        </p>
      </TextWrapper>
      <StyledContact
        method="post"
        action="/thank-you"
        name="backpacks-contact"
        data-netlify="true"
        data-netlify-honeypot="bot-field"
      >
        <input type="hidden" name="bot-field" />
        <input type="hidden" name="form-name" value="backpacks-contact" />
        <div className="name credentials-left">
          <input
            autoComplete="off"
            type="text"
            placeholder="NAME:"
            name="name"
            id="name"
            required={true}
          />
        </div>
        <div className="email credentials-left">
          <input
            autoComplete="off"
            type="text"
            placeholder="EMAIL ADDRESS:"
            name="email"
            id="email"
            required={true}
          />
        </div>
        <div className="number credentials-left">
          <input
            autoComplete="off"
            type="text"
            placeholder="PHONE NUMBER:"
            name="number"
            id="number"
            pattern="[0-9]{10}"
            required={true}
          />
        </div>
        <div className="message credentials-right">
          <textarea
            autoComplete="off"
            name="message"
            placeholder="DO YOU NEED BACKPACKS FOR YOUR COMMUNITY OR WOULD YOU LIKE TO DONATE SUPPLIES?"
            id="message"
            rows={6}
            required={true}
          />
        </div>
        <div className="submit">
          <DefaultOrangeButton className="submit-btn" type="submit">
            SUBMIT
            <span>
              <FaArrowAltCircleRight />
            </span>
          </DefaultOrangeButton>
        </div>
      </StyledContact>
    </FormWrapper>
  )
}

const FormWrapper = styled("section")`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding: 2rem 0 1rem;
`

const TextWrapper = styled("div")`
  p {
    text-align: center;
    margin: 0 auto;
    color: ${({ theme }) => theme.colors.primary};
    text-transform: uppercase;
  }

  .title {
    font-size: 2.5rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }

    @media (max-width: 1024px) {
      max-width: 664px;
    }

    @media (max-width: 650px) {
      font-size: 2rem;
      max-width: 521px;
    }

    @media (max-width: 487px) {
      max-width: 375px;
    }

    @media (max-width: 362px) {
      font-size: 1.9rem;
    }
  }

  .subtext {
    border-top: medium double ${({ theme }) => theme.colors.grey};
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    font-size: 2rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }

    @media (max-width: 1024px) {
      font-size: 2rem;
    }

    @media (max-width: 650px) {
      font-size: 1.5rem;
    }
  }
`

const StyledContact = styled.form`
  padding: 2rem;
  margin: 0 auto;
  max-width: 1220px;
  display: grid;
  grid-gap: 2rem;
  grid-template-rows: auto auto auto 1fr;
  grid-template-columns: repeat(4, 1fr);
  grid-template-areas:
    "name name message message"
    "email email message message"
    "number number message message"
    "submit submit submit submit";

  @media (max-width: 1024px) {
    max-width: 70%;
    grid-template-rows: auto auto auto 200px 1fr;
    grid-template-columns: repeat(2, 1fr);
    grid-template-areas:
      "name name "
      "email email "
      "number number"
      "message message"
      "submit submit";
  }

  @media (max-width: 650px) {
    max-width: 98%;
  }

  .credentials-left {
    width: 100%;

    input {
      width: 100%;
      height: 50px;
      padding-left: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;
    }
  }

  .credentials-right {
    width: 100%;

    textarea {
      resize: none;
      width: 100%;
      height: 100%;
      padding: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;

      &::placeholder {
        font-size: 1.2rem;
      }
    }
  }

  .name {
    grid-area: name;
  }

  .email {
    grid-area: email;
  }

  .number {
    grid-area: number;
  }

  .message {
    grid-area: message;
  }

  .submit {
    display: grid;
    grid-area: submit;
    justify-self: center;
    align-self: center;

    p {
      color: ${({ theme }) => theme.colors.grey};
      font-size: 1.4rem;
      border-bottom: medium double ${({ theme }) => theme.colors.grey};

      @media (max-width: 1024px) {
        font-size: 1rem;
      }

      @media (max-width: 650px) {
        font-size: 0.8rem;
      }
    }

    span {
      position: relative;
      left: 0.6rem;
      font-size: 1.3rem;
    }

    .submit-btn {
      color: ${({ theme }) => theme.colors.secondary};
      width: 15rem;
      height: 4rem;
      font-weight: 600;
      justify-self: center;
    }
  }
`
