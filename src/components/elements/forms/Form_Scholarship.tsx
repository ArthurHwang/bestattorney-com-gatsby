import React, { ReactElement } from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { DefaultOrangeButton } from "../../utilities"

export const ScholarshipContact: React.FC = (): ReactElement => {
  return (
    <FormWrapper>
      <TextWrapper>
        <p className="title">SUBMIT YOUR SCHOLARSHIP APPLICATION</p>
        <p style={{ color: "#EC7D29" }} className="subtext">
          GOOD LUCK!
        </p>
      </TextWrapper>
      <StyledContact
        method="post"
        action="/thank-you"
        name="branchout-contact"
        data-netlify="true"
        data-netlify-honeypot="bot-field"
      >
        <input type="hidden" name="bot-field" />
        <input type="hidden" name="form-name" value="branchout-contact" />
        <div className="name credentials-left">
          <input
            type="text"
            placeholder="NAME:"
            name="name"
            id="name"
            required={true}
          />
        </div>
        <div className="email credentials-left">
          <input
            type="text"
            placeholder="EMAIL ADDRESS:"
            name="email"
            id="email"
            required={true}
          />
        </div>
        <div className="number credentials-left">
          <input
            type="text"
            placeholder="PHONE NUMBER:"
            name="number"
            id="number"
            pattern="[0-9]{10}"
            required={true}
          />
        </div>
        <div className="number credentials-left">
          <input
            type="text"
            placeholder="SCHOOL ATTENDING:"
            name="school"
            id="school"
            required={true}
          />
        </div>

        <div className="email credentials-left">
          <input
            type="text"
            placeholder="CITY / STATE YOU RESIDE:"
            name="city-state"
            id="city-state"
            required={true}
          />
        </div>

        <div className="form-group">
          <label htmlFor="grade">Education:</label>
          <div className="form-check">
            <label className="form-check-label">
              {" "}
              <input
                type="radio"
                className="form-check-input"
                name="grade"
                id="grade1"
                value="Senior"
              />{" "}
              High School Senior{" "}
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              {" "}
              <input
                type="radio"
                className="form-check-input"
                name="grade"
                id="grade2"
                value="Undergrad"
              />{" "}
              Undergrad{" "}
            </label>
          </div>
          <div className="form-check">
            <label className="form-check-label">
              {" "}
              <input
                type="radio"
                className="form-check-input"
                name="grade"
                id="grade3"
                value="Grad School"
              />{" "}
              Grad School{" "}
            </label>
          </div>
        </div>

        <div className="message credentials-right">
          <label className="label-underline" htmlFor="q1">
            Introduce Yourself:
            <i
              className="scholarship-question fa fa-question-circle tippy"
              title="<strong>Tips:</strong> When introducing yourself, keep it short and relevant. Tell us about your experiences and qualifications that relate to how you are going to improve your community. <br>Recommended length: 50-100 words."
              aria-hidden="true"
            />
          </label>
          <textarea
            id="scholarship-q1"
            name="q1"
            placeholder="TELL US WHO YOU ARE AND WHY YOU'RE A GOOD CANDIDATE FOR THIS SCHOLARSHIP AWARD:"
            className="form-control"
            rows={3}
            required={true}
          />
        </div>
        <div className="message credentials-right">
          <label className="label-underline" htmlFor="q2">
            Why are you committed to education?
            <i
              className="scholarship-question fa fa-question-circle tippy"
              title="<strong>Tips:</strong> Tell us not only why education is important to you, but what it can do for others around you too. <br>Recommended length: 50-100 words."
              aria-hidden="true"
            />
          </label>
          <textarea
            id="scholarship-q2"
            name="q2"
            placeholder="GIVE US A PERSONAL REASON AS TO WHY EDUCATION IS IMPORTANT TO YOU:"
            className="form-control"
            rows={3}
            required={true}
          />
        </div>
        <div className="message credentials-right">
          <label className="label-underline" htmlFor="q3">
            What are the struggles your community goes through?
            <i
              className="scholarship-question fa fa-question-circle tippy"
              title="<strong>Tips:</strong> Be specific and focus on one problem. What are the underlying problems? What do you think is the cause? What is the cause of that cause? <br> Help us understand the problem and how it affects the community. <br>Recommended length: 100-200 words."
              aria-hidden="true"
            />
          </label>
          <textarea
            id="scholarship-q3"
            name="q3"
            placeholder="TELL US ABOUT THE STRUGGLES THAT YOU SEE IN YOUR COMMUNITY:"
            className="form-control"
            rows={5}
            required={true}
          />
        </div>
        <div className="message credentials-right">
          <label className="label-underline" htmlFor="q4">
            What is your plan to solve or deal with these issues in your
            community?
            <i
              className="scholarship-question fa fa-question-circle tippy"
              title="<strong>Tips:</strong> This is the most important answer of your submission. Think big picture! What would you do to eradicate this problem within 20 years if you had the resourses? How will you get support? What outside resources can you make use of to make this happen? <br>Recommended Length: 200-400 words."
              aria-hidden="true"
            />
          </label>
          <textarea
            id="scholarship-q4"
            name="q4"
            placeholder="GIVE US A SPECIFIC, DETAILED ANSWER ABOUT HOW YOU CAN DEAL WITH THE PROBLEM AS A WHOLE:"
            className="form-control"
            rows={8}
            required={true}
          />
        </div>
        <div className="message credentials-right">
          <label className="label-underline" htmlFor="q5">
            How will your continued education help you to make your plan a
            reality?
            <i
              className="scholarship-question fa fa-question-circle tippy"
              title="<strong>Tips:</strong> Your education encompasses classes, Degrees/Certifications, Personal Experiences, Work Experiences, Relationships, and more. Figure out what aspect of your education you think will be most important to your future success. <br>Knowing this, how will you continue pursuing that part of your education to give you a better chance to improve your community?  Recommended length: 50-150 words."
              aria-hidden="true"
            />
          </label>
          <textarea
            id="scholarship-q5"
            name="q5"
            placeholder="WHAT PART OR EXPERIENCE OF YOUR EDUCATION WILL HELP YOU PUT THIS PLAN IN ACTION?"
            className="form-control"
            rows={3}
            required={true}
          />
        </div>

        <div className="form-group">
          <div className="form-check">
            <label />{" "}
            <input
              type="checkbox"
              className="form-check-input"
              name="terms"
              id="terms"
              value="true"
              required={true}
            />
            <small> I have read the terms and conditions below. </small>
          </div>
          <p className="small">
            Submitting your application in essay form? Email
            marketing@bestatto-gatsby.netlify.app with the subject line:
            "Scholarship Submission from: [Your Name] - Due 12/15/18"
          </p>
          <p id="scholarship-form-messages" />
        </div>
        <div className="submit">
          <DefaultOrangeButton className="submit-btn" type="submit">
            SUBMIT
            <span>
              <FaArrowAltCircleRight />
            </span>
          </DefaultOrangeButton>
        </div>
        <div id="scholarship-fine-print">
          <h3>Terms and Conditions:</h3>
          <p>
            For questions contact: K. Feathers,
            marketing@bestatto-gatsby.netlify.app or call 800-561-4887.
          </p>
          <p>
            Bisnar Chase represents California plaintiffs injured in car
            accidents and other serious injuries. For over 35 years we have
            helped people in Orange County, Los Angeles and Riverside. If you
            need a{" "}
            <a href="https://www.bestatto-gatsby.netlify.app/">
              California personal injury attorney
            </a>
            please call for a free consultation.
          </p>
          <p className="small">
            Bisnar Chase Personal Injury Attorneys ultimately retains the right
            to determine what organization they will donate to but will honor
            the scholarship winner's request unless the character of the
            organization can be called into question or the donation will
            adversely affect the firm's public image.
          </p>
          <p className="small">
            Bisnar Chase reserves the right to determine the winner of the
            scholarship based on the information provided to them. No employees
            or immediate family members of Bisnar Chase employees may win this
            scholarship award, and previous scholarship winners are not eligible
            to win. Applicants who have entered their personal statement and who
            are not chosen to win are eligible to submit another application
            before the next scholarship deadline, but must apply with a new
            submission.
          </p>
          <p className="small">
            By submitting an application for this scholarship award, the
            applicant agrees to allow Bisnar Chase to contact them regarding
            this scholarship program and to use their application answers in
            Bisnar Chase marketing publications and content.
          </p>
        </div>
      </StyledContact>
    </FormWrapper>
  )
}

const FormWrapper = styled("section")`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding: 2rem 0 1rem;
  margin-bottom: 2rem;
`

const TextWrapper = styled("div")`
  p {
    text-align: center;
    margin: 0 auto;
    color: ${({ theme }) => theme.colors.primary};
    text-transform: uppercase;
  }

  .title {
    font-size: 2.5rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }

    @media (max-width: 1024px) {
      max-width: 664px;
    }

    @media (max-width: 650px) {
      font-size: 2rem;
      max-width: 521px;
    }

    @media (max-width: 487px) {
      max-width: 375px;
    }

    @media (max-width: 362px) {
      font-size: 1.9rem;
    }
  }

  .subtext {
    border-top: medium double ${({ theme }) => theme.colors.grey};
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    font-size: 2rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }

    @media (max-width: 1024px) {
      font-size: 2rem;
    }

    @media (max-width: 650px) {
      font-size: 1.5rem;
    }
  }
`

const StyledContact = styled.form`
  padding: 2rem;
  margin: 0 auto;
  max-width: 1220px;
  width: 100%;

  p {
    margin: 0 auto;
    color: ${({ theme }) => theme.colors.primary};
    text-transform: uppercase;
    font-size: 1.2rem;
  }

  h3 {
    margin-top: 2rem;
    color: ${({ theme }) => theme.colors.accent};
    text-align: center;
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};
  }

  small {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 1rem;
  }

  label {
    font-size: 2rem;
    color: ${({ theme }) => theme.colors.primary};
  }

  #scholarship-fine-print {
    p {
      font-size: 1rem;
      margin: 0.5rem 0;
      text-align: justify;
    }
  }

  .label-underline {
    border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
  }

  .form-check-label {
    font-size: 1.8rem;
    color: ${({ theme }) => theme.colors.primary};
  }

  @media (max-width: 1024px) {
    max-width: 70%;
    grid-template-rows: auto auto auto 200px 1fr;
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 650px) {
    max-width: 98%;
  }

  .credentials-left {
    width: 100%;

    input {
      width: 100%;
      height: 50px;
      padding-left: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;
      margin: 1rem 0;
    }
  }

  .credentials-right {
    width: 100%;

    textarea {
      resize: none;
      width: 100%;
      height: 100%;
      padding: 1rem;
      font-size: 1.2rem;
      border-radius: 0.2rem;
      margin: 1rem 0;

      &::placeholder {
        font-size: 1.2rem;
      }
    }
  }

  .name {
    grid-area: name;
  }

  .email {
    grid-area: email;
  }

  .number {
    grid-area: number;
  }

  .message {
    grid-area: message;
  }

  .submit {
    display: grid;
    grid-area: submit;
    justify-self: center;
    align-self: center;
    border-top: medium double ${({ theme }) => theme.colors.grey};
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    margin-top: 2rem;
    padding: 1rem 0;

    p {
      color: ${({ theme }) => theme.colors.grey};
      font-size: 1.4rem;
      border-bottom: medium double ${({ theme }) => theme.colors.grey};

      @media (max-width: 1024px) {
        font-size: 1rem;
      }

      @media (max-width: 650px) {
        font-size: 0.8rem;
      }
    }

    span {
      position: relative;
      left: 0.6rem;
      font-size: 1.3rem;
    }

    .submit-btn {
      color: ${({ theme }) => theme.colors.secondary};
      width: 15rem;
      height: 4rem;
      font-weight: 600;
      justify-self: center;
    }
  }
`
