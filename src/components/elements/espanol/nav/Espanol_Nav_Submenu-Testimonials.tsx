import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  className: string
  activeClassName: string
}

export const EspanolNavTestimonials: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  return (
    <StyledReferrals className={className}>
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/abogados/testimonios"
      >
        Testimonios
      </Link>
    </StyledReferrals>
  )
}

const StyledReferrals = styled.li`
  z-index: 1;
`
