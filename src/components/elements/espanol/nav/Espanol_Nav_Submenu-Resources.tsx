import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { elevation } from "../../../utilities"

interface Props {
  className: string
  activeClassName: string
}

export const EspanolNavResources: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  return (
    <StyledResources className={className}>
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/abogados/recursos"
      >
        Recursos
      </Link>
    </StyledResources>
  )
}

const StyledResources = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 280px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.8rem 1rem;
      text-decoration: none;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .resources-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .resources-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`
