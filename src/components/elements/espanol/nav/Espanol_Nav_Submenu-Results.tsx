import { Link } from "src/components/elements/Link"
import styled from "styled-components"
import React, { ReactElement } from "react"

interface Props {
  className: string
  activeClassName: string
}

export const EspanolNavResults: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  return (
    <StyledResults className={className}>
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/abogados/resultos-destacados"
      >
        Resultados
      </Link>
    </StyledResults>
  )
}

const StyledResults = styled.li``
