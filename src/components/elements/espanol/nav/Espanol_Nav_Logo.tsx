import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"
import Logo from "src/images/logo/bisnar-chase-logo.svg"

interface Props {
  className: string
}

export const EspanolNavLogo: React.FC<Props> = ({
  className
}): ReactElement => {
  return (
    <Link to="/abogados" className={className}>
      <StyledImg
        style={{ height: "75px", marginLeft: "0", display: "initial" }}
        alt="Bisnar Chase Personal Injury Attorneys"
        src={Logo}
      />
    </Link>
  )
}

const StyledImg = styled.img`
  height: 75px;

  @media (max-width: 1024px) {
    width: 75%;
  }
`
