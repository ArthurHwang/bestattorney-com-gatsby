import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import { IoIosCall } from "react-icons/io"
import styled from "styled-components"
import BBBLogo from "src/images/logo/bbb-logo.png"

interface Props {
  className?: string
  geoLocation?: string
}

export const EspanolNavCall: React.FC<Props> = ({
  className,
  geoLocation
}): ReactElement => {
  function switchLocationNumber(location: string | undefined): string {
    switch (location) {
      case "orange-county":
        return "(949) 203-3814"
      case "los-angeles":
        return "(323) 238-4683"
      case "riverside":
        return "(951) 530-3711"
      case "san-bernardino":
        return "(909) 253-0750"
      default:
        return "(800) 561-4887"
    }
  }

  function switchCallNumber(location: string | undefined): string {
    switch (location) {
      case "orange-county":
        return "9492033814"
      case "los-angeles":
        return "3232384683"
      case "riverside":
        return "9515303711"
      case "san-bernardino":
        return "9092530750"
      default:
        return "8005614887"
    }
  }

  return (
    <StyledNavCall className={className}>
      <div className="call">
        <a href={`tel:${switchCallNumber(geoLocation)}`} className="number">
          <IoIosCall className="call-icon" />
          <span className="call-text">{switchLocationNumber(geoLocation)}</span>
        </a>
        <div className="espanol">
          <Link to="/">En Inglés</Link>
        </div>
      </div>
      <div className="logo-container">
        <a href="https://www.bbb.org/us/ca/newport-beach/profile/lawyers/bisnar-chase-personal-injury-attorneys-1126-100046710">
          <img
            alt="Better Business Bureau A+ Rated"
            className="bbb-logo"
            src={BBBLogo}
          />
        </a>
      </div>
    </StyledNavCall>
  )
}

const StyledNavCall = styled("div")`
  position: relative;
  top: 7.5px;
  width: 357px;
  height: 100%;
  margin-left: auto;
  display: grid;
  grid-template-areas:
    "call bbb"
    "call bbb";
  grid-template-columns: 8fr 3fr;

  a {
    text-decoration: none;
  }

  .number {
    grid-area: number;
    justify-self: flex-end;
    margin-right: 1rem;
    margin-top: 0.85rem;
  }

  .logo-container {
    height: 70px;
    margin-right: 1rem;

    .bbb-logo {
      object-fit: contain;
      grid-area: bbb;
    }
  }

  .espanol {
    position: relative;
    justify-self: flex-end;
    grid-area: espanol;
    right: 17px;
    top: -14px;

    a {
      color: ${({ theme }) => theme.colors.accent};
      font-size: 1rem;
      font-weight: 600;

      &:hover {
        color: ${({ theme }) => theme.links.hoverBlue};
      }
    }
  }

  .call {
    height: 70px;
    display: grid;
    grid-template-rows: 4fr 1.7fr;
    grid-template-columns: 4fr 1fr;
    grid-area: call;
    grid-template-areas:
      "number number"
      "espanol espanol";

    .call-icon {
      position: relative;
      font-size: 2.8rem;
      color: ${({ theme }) => theme.colors.accent};
    }

    .call-text {
      position: relative;
      bottom: 7.5px;
      right: 0;
      font-size: 2.5rem;
      color: #000;
      text-decoration: none;
    }
  }

  .search {
    grid-area: search;
    font-size: 1.7rem;
    position: relative;
    bottom: 7px;
    right: 20px;
    justify-self: center;
    align-self: middle;
    height: 100%;
  }
`
