import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { EspanolNavAboutUs } from "./Espanol_Nav_Submenu-AboutUs"
import { EspanolNavContact } from "./Espanol_Nav_Submenu-Contact"
import { EspanolNavPracticeAreas } from "./Espanol_Nav_Submenu-PracticeAreas"
import { EspanolNavResources } from "./Espanol_Nav_Submenu-Resources"
import { EspanolNavResults } from "./Espanol_Nav_Submenu-Results"
import { EspanolNavTestimonials } from "./Espanol_Nav_Submenu-Testimonials"

interface Props {
  className: string
}

export const EspanolNavLinks: React.FC<Props> = ({
  className
}): ReactElement => {
  return (
    <StyledNavLinks className={className}>
      <li className="nav-menu-item ">
        <Link activeClassName="active" className="nav-link" to="/">
          Incio
        </Link>
      </li>
      <EspanolNavAboutUs activeClassName="active" className="nav-menu-item" />
      <EspanolNavPracticeAreas
        activeClassName="active"
        className="nav-menu-item"
      />
      <EspanolNavResults activeClassName="active" className="nav-menu-item" />
      <EspanolNavTestimonials
        activeClassName="active"
        className="nav-menu-item"
      />
      <EspanolNavResources activeClassName="active" className="nav-menu-item" />
      <li className="nav-menu-item ">
        <Link
          activeClassName="active"
          partiallyActive={true}
          className="nav-link"
          to="/blog"
        >
          Blog
        </Link>
      </li>
      <EspanolNavContact activeClassName="active" className="nav-menu-item" />
    </StyledNavLinks>
  )
}

const StyledNavLinks = styled.ul`
  margin: 0;
  display: flex;
  position: relative;
  list-style-type: none;
  height: 35px;

  .active {
    &::after {
      height: 1px;
      background: ${({ theme }) => theme.colors.accent};
      content: "";
      width: 65%;
      position: absolute;
      transform: translateX(-50%);
      left: 49%;
      bottom: 7px;
      animation: appear ease-in-out 0.3s;
    }

    @keyframes appear {
      0% {
        width: 0;
      }

      80% {
        width: 85%;
      }

      90% {
        width: 80%;
      }

      100% {
        width: 75%;
      }
    }
  }

  .nav-menu-item {
    display: flex;
    position: relative;

    &:first-child::before {
      display: none;
    }
  }

  a.nav-link {
    white-space: nowrap;
    display: flex;
    text-transform: uppercase;
    padding: 1rem 0.8rem;
    align-items: center;
    text-decoration: none;
    font-weight: 600;
    font-size: 0.95rem;
    margin: 0 1rem;
    border: 0;
    cursor: pointer;
    color: ${({ theme }) => theme.colors.primary};

    &:hover,
    &:focus {
      transition: transform 0.2s ease-in-out;
      transform: scale(1.07);
    }

    @media (max-width: 1120px) {
      font-size: 0.83rem;
    }
  }
`
