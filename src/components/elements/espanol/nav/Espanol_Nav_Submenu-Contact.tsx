import { Link } from "src/components/elements/Link"
import styled from "styled-components"
import { elevation } from "../../../utilities"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../../context/NavContext"

interface Props {
  className: string
  activeClassName: string
}

export const EspanolNavContact: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { dispatch } = useContext(NavContext)
  return (
    <StyledContact className={className}>
      <Link
        onClick={() => dispatch({ type: "CLICK" })}
        onMouseEnter={() => dispatch({ type: "MOVE" })}
        activeClassName={activeClassName}
        className="nav-link"
        to="/abogados/contactenos"
      >
        Contactenos
      </Link>
    </StyledContact>
  )
}

const StyledContact = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 280px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.8rem 1rem;
      text-decoration: none;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .contact-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .contact-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`
