import { graphql, Link, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { useContext, ReactElement } from "react"
import styled from "styled-components"
import { NavContext } from "../../../../context/NavContext"
import { elevation, DefaultOrangeButton } from "../../../utilities"

interface Props {
  className: string
  activeClassName: string
}

const PRACTICE_AREA_QUERY = graphql`
  query {
    carAccidents: file(
      relativePath: { regex: "/thumb-pa-car-accidents.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    truckAccidents: file(
      relativePath: { regex: "/thumb-pa-truck-accident.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    bicycleAccidents: file(
      relativePath: { regex: "/thumb-pa-bicycle-accident.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    autoDefects: file(relativePath: { regex: "/thumb-pa-auto-defects.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    dogBites: file(relativePath: { regex: "/thumb-pa-dog-bite.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    premisesLiability: file(
      relativePath: { regex: "/thumb-pa-premises-liability.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    wrongfulDeath: file(
      relativePath: { regex: "/thumb-pa-wrongful-death.png/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    productLiability: file(
      relativePath: { regex: "/thumb-pa-product-liability.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    medicalDevices: file(
      relativePath: { regex: "/thumb-pa-defective-medical-product.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    defectiveProducts: file(
      relativePath: { regex: "/thumb-pa-defective-product.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    brainInjury: file(relativePath: { regex: "/thumb-pa-brain-injury.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const EspanolNavPracticeAreas: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)
  return (
    <StaticQuery
      query={PRACTICE_AREA_QUERY}
      render={data => (
        <StyledPracticeAreas
          onClick={() => dispatch({ type: "CLICK" })}
          onMouseEnter={() => dispatch({ type: "MOVE" })}
          className={className}
          state={state}
        >
          <Link
            activeClassName={activeClassName}
            partiallyActive={true}
            className="nav-link"
            to="/abogados/lesiones-personales"
          >
            Areas de Práctica
          </Link>
          {!state.clicked && (
            <PracticeAreasSubmenu className="practice-areas-submenu">
              <PATitle>
                <Link to="/abogados/lesiones-personales">
                  Areas de Práctica
                </Link>
              </PATitle>
              <PAColumn className="col-1">
                <Link to="/abogados/accidentes-de-auto">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.carAccidents.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">
                        Accidentes Automovilísticos
                      </span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/accidentes-de-camiones">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.truckAccidents.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Accidentes de Camiones</span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/accidentes-de-bicicleta">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.bicycleAccidents.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Accidentes de Bicicleta</span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/defectos-automovilisticos">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.autoDefects.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">
                        Defectos Automovilísticos
                      </span>
                    </div>
                  </PACard>
                </Link>
              </PAColumn>
              <PAColumn className="col-2">
                <Link to="/abogados/mordeduras-de-perro">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.dogBites.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">
                        Lesiones por Mordedura de Perro
                      </span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/responsibilidad-civil">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.premisesLiability.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">
                        {" "}
                        Responsabilidad del Local
                      </span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.wrongfulDeath.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">
                        Homicidio por negligencia
                      </span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.productLiability.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Acción de clase</span>
                    </div>
                  </PACard>
                </Link>
              </PAColumn>
              <PAColumn className="col-3">
                <Link to="/abogados/derecho-laboral">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.medicalDevices.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Derecho Laboral</span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/productos-defectuosos">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.defectiveProducts.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Productos defectuosos</span>
                    </div>
                  </PACard>
                </Link>
                <Link to="/abogados/lesiones-personales">
                  <PACard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="PA-image"
                      alt=""
                      fluid={data.brainInjury.childImageSharp.fluid}
                    />
                    <div className="PA-text-container">
                      <span className="PA-title">Daño cerebral</span>
                    </div>
                  </PACard>
                </Link>
              </PAColumn>
              <PAColumn className="col-4">
                <div className="link-container">
                  <Link
                    className="about-link"
                    to="/abogados/recursos/que-hacer-despues-un-accidente"
                  >
                    Qué hacer despues de un accidente
                  </Link>
                  <Link
                    to="/abogados/resultos-destacados"
                    className="about-link"
                  >
                    Resultados
                  </Link>

                  <Link className="about-link" to="/abogados/testimonios">
                    Testimonios
                  </Link>
                  <Link
                    to="/abogados/recursos/elegir-un-abogado"
                    className="about-link"
                  >
                    Cuota De Base Eventual
                  </Link>
                  <Link to="/abogados/lesiones-personales">
                    {/* 
                    //@ts-ignore */}
                    <DefaultOrangeButton
                      className="PA-button"
                      modifiers="small"
                    >
                      Todos Areas de Práctica
                    </DefaultOrangeButton>
                  </Link>
                </div>
              </PAColumn>
            </PracticeAreasSubmenu>
          )}
        </StyledPracticeAreas>
      )}
    />
  )
}

const PACard = styled("div")`
  border-radius: 0.2rem;
  border: medium double ${({ theme }) => theme.colors.secondary};
  display: grid;
  grid-template-columns: 65px 3fr;
  margin-top: 0.8rem;
  width: 100%;
  transition: transform 0.2s ease-out, box-shadow 0.2s linear;

  span {
    display: block;
  }

  .PA-text-container {
    width: 100%;
    padding: 0.6rem 1rem 0;

    span {
      text-align: left;
    }
  }

  .PA-image {
    object-fit: cover;
    width: 100%;
    height: 70px;
  }

  .PA-title {
    font-size: 1.5rem;
  }

  &:hover {
    transform: scale(1.03);
    ${elevation[2]};
  }
`

const StyledPracticeAreas: any = styled.li`
  .practice-areas-submenu {
    opacity: 0;
    transform: translateY(-8000px);
  }

  &:hover {
    .practice-areas-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const PATitle = styled.span`
  font-size: 2rem;
  grid-area: title;
  align-self: center;
  position: relative;
  top: 1rem;
  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
  }
`

const PAColumn = styled("div")`
  display: flex;
  flex-direction: column;

  .link-container {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    height: 97%;
    border-left: 4px solid ${({ theme }) => theme.colors.grey};
    position: relative;
    top: 52%;
    transform: translateY(-50%);
    margin-left: 0.5rem;
    padding-left: 1rem;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding-left: 0.3rem;
      margin-top: 0.8rem;
    }
  }

  .PA-button {
    height: 40px;
    margin-top: 2rem;
  }
`

const PracticeAreasSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 1100px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: -105px;
  top: 35px;
  height: auto;
  display: grid;
  grid-gap: 1rem;
  padding: 0 2.5rem 2.5rem;
  grid-template-rows: auto 1fr;
  grid-template-columns: 1fr 1fr 1fr 0.8fr;
  grid-template-areas:
    "title title title title"
    "col1 col2 col3 col4";

  a {
    text-decoration: none;
  }

  .col-1 {
    grid-area: col1;
  }

  .col-2 {
    grid-area: col2;
  }

  .col-3 {
    grid-area: col3;
  }

  .col-4 {
    grid-area: col4;
    .about-link {
      text-transform: uppercase;
    }

    button {
      text-transform: uppercase;
      width: 100%;
    }
  }
`
