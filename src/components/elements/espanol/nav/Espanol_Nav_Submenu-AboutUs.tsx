import { graphql, Link, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../../context/NavContext"
import styled from "styled-components"
import { elevation } from "../../../utilities"

interface Props {
  className: string
  activeClassName: string
}

const ABOUT_US_QUERY = graphql`
  query {
    johnBisnar: file(relativePath: { regex: "/john-bisnar-headshot.png/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    brianChase: file(relativePath: { regex: "/brian-chase-headshot.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    scottRitsema: file(
      relativePath: { regex: "/scott-ritsema-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    shannonBarker: file(
      relativePath: { regex: "/shannon-barker-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    jerusalemBeligan: file(
      relativePath: { regex: "/jerusalem-beligan-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    gavinLong: file(relativePath: { regex: "/gavin-long-headshot.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    stevenHilst: file(relativePath: { regex: "/steven-hilst-headshot.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    tomAntunovich: file(
      relativePath: { regex: "/tom-antunovich-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    ianSilvers: file(relativePath: { regex: "/ian-silvers-headshot.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    kristBiakanja: file(
      relativePath: { regex: "/krist-biakanja-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    jordanSouthwick: file(
      relativePath: { regex: "/jordan-southwick-headshot.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 200, srcSetBreakpoints: [200]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const EspanolNavAboutUs: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)

  return (
    <StaticQuery
      query={ABOUT_US_QUERY}
      render={data => (
        <StyledAboutUs
          onClick={() => dispatch({ type: "CLICK" })}
          onMouseEnter={() => dispatch({ type: "MOVE" })}
          className={className}
        >
          <Link
            activeClassName={activeClassName}
            partiallyActive={true}
            className="nav-link"
            to="/abogados/sobre-nosotros"
          >
            Abogados
          </Link>
          {!state.clicked && (
            <AboutUsSubmenu className="about-us-submenu">
              <AttorneysTitle>
                <Link to="/abogados/sobre-nosotros">ABOGADOS</Link>
              </AttorneysTitle>
              <AttoColumn className="col-1">
                <Link to="/abogados/sobre-nosotros/john-bisnar">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      className="atto-image"
                      // loading="lazy"
                      alt=""
                      fluid={data.johnBisnar.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">John Bisnar</span>
                      <span className="atto-position">Socio Fundador</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/brian-chase">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.brianChase.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Brian Chase</span>
                      <span className="atto-position">Socio Mayoritario</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/scott-ritsema">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.scottRitsema.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Scott Ritsema</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/legal-administrator-shannon-barker">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.shannonBarker.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Shannon Barker</span>
                      <span className="atto-position">Administradora</span>
                    </div>
                  </AttoCard>
                </Link>
              </AttoColumn>
              <AttoColumn className="col-2">
                <Link to="/abogados/sobre-nosotros/jerusalem-beligan">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.jerusalemBeligan.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Jerusalem Beligan</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/gavin-long">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.gavinLong.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">H. Gavin Long</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/steven-hilst">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.stevenHilst.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Steven Hilst</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/tom-antunovich">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.tomAntunovich.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Tom Antunovich</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
              </AttoColumn>
              <AttoColumn className="col-3">
                <Link to="/abogados/sobre-nosotros/ian-silvers">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.ianSilvers.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Ian Silvers</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/krist-biakanja">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.kristBiakanja.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Krist Biakanja</span>
                      <span className="atto-position">Abogado</span>
                    </div>
                  </AttoCard>
                </Link>
                <Link to="/abogados/sobre-nosotros/jordan-southwick">
                  <AttoCard>
                    <Img
                      durationFadeIn={1000}
                      loading="lazy"
                      alt=""
                      className="atto-image"
                      fluid={data.jordanSouthwick.childImageSharp.fluid}
                    />
                    <div className="atto-text-container">
                      <span className="atto-title">Jordan Southwick</span>
                      <span className="atto-position">Laywer</span>
                    </div>
                  </AttoCard>
                </Link>
              </AttoColumn>
              <AttoColumn className="col-4">
                <div className="link-container">
                  <Link to="/abogados/sobre-nosotros" className="about-link">
                    SOBRE EL BUFETE DE BISNAR CHASE
                  </Link>
                  <Link
                    to="/about-us/lawyer-reviews-ratings"
                    className="about-link"
                  >
                    POR QUE´ SOMOS LOS MEJORES?
                  </Link>
                  <Link to="/press-releases" className="about-link">
                    COMUNICADOS DE PRENSA
                  </Link>
                  <Link to="/giving-back" className="about-link">
                    SERVICIO COMUNITARIO
                  </Link>

                  <Link to="/blog" className="about-link">
                    LEE NUESTROS ARTICULOS
                  </Link>
                </div>
              </AttoColumn>
            </AboutUsSubmenu>
          )}
        </StyledAboutUs>
      )}
    />
  )
}

const AttoCard = styled("div")`
  border-radius: 0.2rem;
  border: medium double ${({ theme }) => theme.colors.secondary};
  display: grid;
  grid-template-columns: 65px 3fr;
  margin-top: 0.8rem;
  width: 100%;
  transition: transform 0.2s ease-out, box-shadow 0.2s linear;

  span {
    display: block;
  }

  .atto-text-container {
    width: 100%;
    padding: 0.6rem 1rem 0;
  }

  .atto-image {
    object-fit: cover;
    width: 100%;
    height: 70px;
  }

  .atto-title {
    font-size: 1.5rem;
  }

  .atto-position {
    font-size: 1.1rem;
    color: ${({ theme }) => theme.colors.accent};
  }

  &:hover {
    transform: scale(1.03);
    ${elevation[2]};
  }
`

const StyledAboutUs = styled.li`
  .about-us-submenu {
    opacity: 0;
    transform: translateY(-8000px);
  }

  &:hover {
    .about-us-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const AttorneysTitle = styled.span`
  font-size: 2rem;
  grid-area: title;
  align-self: center;
  position: relative;
  top: 1rem;
  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
  }
`

const AttoColumn = styled("div")`
  display: flex;
  flex-direction: column;

  .link-container {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    align-items: flex-start;
    height: 97%;
    border-left: 4px solid ${({ theme }) => theme.colors.grey};
    position: relative;
    top: 52%;
    transform: translateY(-50%);
    margin-left: 0.5rem;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding-left: 1rem;
      margin-top: 0.8rem;
    }
  }
`

const AboutUsSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 1100px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: 0px;
  top: 35px;
  height: auto;
  display: grid;
  grid-gap: 1rem;
  padding: 0 2.5rem 2.5rem;
  grid-template-rows: auto 1fr;
  grid-template-columns: 1fr 1fr 1fr 0.8fr;
  grid-template-areas:
    "title title title title"
    "col1 col2 col3 col4";

  a {
    text-decoration: none;
  }

  .about-link {
    text-align: left;
  }

  .col-1 {
    grid-area: col1;
  }

  .col-2 {
    grid-area: col2;
  }

  .col-3 {
    grid-area: col3;
  }

  .col-4 {
    grid-area: col4;
  }
`
