import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

export const EspanolResultsContent: React.FC = (): ReactElement => {
  return (
    <StyledResultsContent>
      <p>
        <strong>
          Póngase en contacto con nuestros experimentados y dedicados Abogados
          de Lesiones Personales de California para obtener ayuda inmediata.
          Llame al 949-537-2651. Fuera de California, llame gratis al
          1-877-705-6556.
        </strong>
      </p>
      <p>
        Si se ha lesionado en un accidente causado por la negligencia de otra
        persona u organización, puede enfrentar desafíos insuperables.
        <strong>Tiene derecho a buscar compensación</strong> por lesiones por
        las graves pérdidas que ha experimentado a través de un caso de{" "}
        <Link to="/abogados/lesiones-personales" target="_blank">
          lesiones personales
        </Link>{" "}
        que hace que las partes culpables sean responsables de sus acciones. No
        hable con las compañías de seguros antes de consultar con un abogado de
        lesiones personales calificado y con experiencia que protegerá su
        interés, no el de las compañías de seguros.
      </p>
      <h2>Representación Legal De Primera Clase En California</h2>
      <p>
        El éxito en los casos de lesiones en California solo puede garantizarse
        a través de la representación legal especializada de algunos de los
        mejores abogados. Bisnar Chase representa exclusivamente a los
        demandantes por lesiones que son el resultado de accidentes
        automovilísticos, mordeduras severas de perros, accidentes de
        motocicletas, defectos de productos, lesiones por defectos de
        automóviles y otras lesiones graves o catastróficas. Tenemos los
        recursos, la experiencia y el talento para enfrentar algunos de los
        casos de lesiones personales más complejos con resultados
        sobresalientes. Cubrimos todo California y ofrecemos una garantía
        <strong>
          para protegerlo de la carga financiera hasta que gane su caso.
        </strong>
      </p>
      <h2>Abogados Con Experiencia En Juicios Que Ganan</h2>
      <p>
        Por todo el estado de California los bufetes de defensa y los tribunales
        locales saben quiénes somos. Hemos pasado décadas dentro y fuera de la
        sala de justicia luchando por justicia para las víctimas heridas.
        Representar a los californianos es algo en lo que nos sentimos
        orgullosos y trabajamos todos los días. Algunos de los mejores abogados
        remiten sus casos más difíciles a Bisnar Chase. Nuestra reputación de
        ganar casos desafiantes se sostiene por sí misma.
      </p>
      <h2>Galardonados Abogados De Lesiones Personales En California</h2>
      <p>
        Fundado por John Bisnar en 1978, Bisnar Chase es una firma de abogados
        de lesiones en California dedicada a una experiencia de cliente
        superior. Brian Chase, abogado de litigio y socio, es un respetado
        abogado de accidentes y lesiones en todo el país que ha asumido algunos
        de los casos más grandes de defectos de automóviles y fabricantes de
        productos defectuosos. En Bisnar Chase, nuestros galardonados abogados
        han encontrado un éxito continuo en el tribunal desde 1978. Con más de{" "}
        <strong>300 millones de dólares ganados </strong>incluyendo daños
        punitivos y miles de personas representadas, nuestra firma se ha ganado
        la reputación de ser una de las firmas de abogados de lesiones más duras
        de California. Llame al 877-705-6556 para una consulta gratuita.
      </p>
      <p>
        <strong>
          ¡Abogados de lesiones personales de California! Llame al 877-705-6556
        </strong>
      </p>
    </StyledResultsContent>
  )
}

const StyledResultsContent = styled("div")`
  h2 {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 2.4rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 4rem;
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 3rem;
    letter-spacing: 0.8px;
    line-height: 1.8;
    font-size: 1.3rem;

    @media (max-width: 700px) {
      font-size: 1.2rem;
    }

    &:last-child {
      margin-bottom: 2rem;
    }
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.links.hoverOrange};
    }
  }
`
