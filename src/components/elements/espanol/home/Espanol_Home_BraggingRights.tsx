import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { URLS } from "../../../../data/home/braggingRights"
import { FaAngleRight } from "react-icons/fa"

// ! Per image size is easliy changed by adjusting width parameter below
// ! _withWebpp for some reason gives pixelated images, turned off for now
const BRAGGING_RIGHTS_QUERY = graphql`
  query {
    asSeenOnImage: file(relativePath: { regex: "/as-seen-on.jpg/" }) {
      childImageSharp {
        fixed(width: 100) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }
    consumerAttorneysImage: file(
      relativePath: { regex: "/consumer-attorneys-of-california.png/" }
    ) {
      childImageSharp {
        fixed(width: 150) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }

    newsWeekImage: file(relativePath: { regex: "/newsweeklogo.png/" }) {
      childImageSharp {
        fixed(width: 175) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    timeImage: file(
      relativePath: { regex: "/1280px-Time_Magazine_logo.svg.png/" }
    ) {
      childImageSharp {
        fixed(width: 115) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    trialAdvocatesImage: file(
      relativePath: { regex: "/american-trial-lawyers.png/" }
    ) {
      childImageSharp {
        fixed(width: 135) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    fiveMillionImage: file(
      relativePath: { eq: "2019-assets/500-million.png" }
    ) {
      childImageSharp {
        fixed(width: 150) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    distinguishedImage: file(
      relativePath: { regex: "/distinguished-justice-advocates.png/" }
    ) {
      childImageSharp {
        fixed(width: 105) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    americasTopImage: file(relativePath: { regex: "/americas-top-100.png/" }) {
      childImageSharp {
        fixed(width: 100) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
  }
`

interface Props {
  borderTop?: boolean
}

export const EspanolBraggingRights: React.FC<Props> = ({
  borderTop = false
}): ReactElement => {
  return (
    <StaticQuery
      query={BRAGGING_RIGHTS_QUERY}
      render={data => {
        const images = Object.entries(data)

        // Use Object.entries() returned array data to loop and update href dynamically by using dictionary lookup

        return (
          <StyledBraggingRights bordertop={borderTop}>
            <ContentWrapper>
              <LazyLoad>
                <BadgeWrapper>
                  {images.map((image: [string, any], index) => (
                    <Link
                      className="badge"
                      key={index}
                      to={URLS[image[0]].href}
                    >
                      <Img
                        alt={URLS[image[0]].alt}
                        loading="lazy"
                        fixed={image[1].childImageSharp.fixed}
                      />
                    </Link>
                  ))}
                </BadgeWrapper>
              </LazyLoad>

              <TextWrapper>
                <Link to="/about-us/lawyer-reviews-ratings">
                  Vea nuestras reseñas y recompensas
                  <span className="right-icon">
                    <FaAngleRight />
                  </span>
                </Link>
              </TextWrapper>
            </ContentWrapper>
          </StyledBraggingRights>
        )
      }}
    />
  )
}

const StyledBraggingRights = styled("section")<{ bordertop: boolean }>`
  height: 200px;
  border-top: ${props =>
    props.bordertop ? `1px solid ${props.theme.colors.secondary}` : "null"};

  @media (max-width: 1160px) {
    height: 300px;
  }

  @media (max-width: 715px) {
    height: 500px;
  }
`

const ContentWrapper = styled("div")`
  height: 100%;
`

const TextWrapper = styled("div")`
  text-align: center;
  position: relative;
  bottom: 2.5rem;
  height: 10%;

  .right-icon {
    position: relative;
    top: 0.1rem;
    font-size: 1rem;
    color: ${({ theme }) => theme.colors.accent};
  }
`

const BadgeWrapper = styled("div")`
  padding: 2rem 2rem 2rem;
  max-width: 80%;
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  justify-content: space-between;
  align-content: space-around;
  align-items: center;
  margin: 0 auto;

  .badge {
    transition: 0.3s ease all;

    &:hover {
      transform: scale(1.05);
    }
  }

  @media (max-width: 1550px) {
    max-width: 95%;
  }

  @media (max-width: 1160px) {
    max-width: 675px;
  }

  @media (max-width: 715px) {
    max-width: 427px;
    justify-content: space-evenly;
    align-items: center;
    padding-bottom: 3rem;
  }
`
