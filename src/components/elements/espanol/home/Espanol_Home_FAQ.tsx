import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"

export const EspanolFAQ: React.FC = (): ReactElement => {
  return (
    <StyledFAQ>
      <ContentWrapper>
        <h2>¿Por qué Elegir a Bisnar Chase?</h2>
        <p>
          California no tiene escasez de abogados que practiquen la ley de
          lesiones personales, pero en Bisnar Chase nos hemos ganado una
          reputación como uno de los mejores bufetes de abogados en California.
          Nuestro equipo de abogados litigantes tiene la pasión, el impulso y el
          deseo de ayudar a otros y ayudar a que nuestro mundo sea un lugar más
          seguro. Debido a los casos en los que hemos peleado y ganado, hemos
          penalizado a algunas de las corporaciones más grandes de Estados
          Unidos. Y hemos defendido al consumidor, que de otro modo no hubiera
          tenido voz.
        </p>
        <h2>¿Cómo Sé Que Puedo Ser Representado?</h2>
        <p>
          Si está herido y no es culpable, tiene derecho a pedir una
          indemnización. Ya sea un{" "}
          <Link
            to="/abogados/recursos/que-hacer-despues-un-accidente"
            target="_blank"
          >
            accidente automovilístico
          </Link>
          , un ataque de un perro o un resbalón y caída, los demandantes
          lesionados tienen el derecho absoluto de buscar la cantidad más justa
          de compensación por todo lo que han sufrido. Eso puede incluir tiempo
          libre en el trabajo, problemas de salud relacionados con la lesión,
          pérdida de su vehículo y otros costos que surgen por la lesión.
          Representarse a sí mismo podría costarle.
        </p>
        <h2>¿Cuánto Vale Mi Caso?</h2>
        <p>
          Eso dependerá de la gravedad de la lesión y del tipo de representación
          que tenga. Un buen abogado de lesiones personales con décadas en el
          campo de la ley de accidentes puede generar acuerdos mucho más altos
          que alguien que no esté familiarizado con la ley de lesiones, los
          tribunales locales y los equipos de defensa. Bisnar Chase es
          regularmente buscado cuando los abogados luchan por obtener un acuerdo
          justo para sus clientes.
        </p>
        <h2>Cómo Presentar Su Reclamo Por Lesiones Personales En El Tiempo</h2>
        <p>
          Debido a que hay{" "}
          <strong>
            un estatuto de limitación estricto de dos años en California
          </strong>
          , es importante que actúe rápidamente y consiga un abogado seguro. Su
          abogado tendrá mucho trabajo por delante preparando su caso y
          resolviendo los problemas que puedan surgir, incluso si el caso se
          lleva a juicio. Se recomienda que tenga todo en orden y a la mano para
          entregarlo a su equipo legal de manera oportuna.
        </p>
        <h2>¿Cómo Me Protegen Financieramente?</h2>
        <p>
          Nuestra garantía de no si no ganamos no cobramos garantiza que no
          pagará a su abogado hasta que se gane su caso. Incluso después de que
          nuestros abogados hayan realizado un esfuerzo considerable en su caso
          y acumulado costos, si el caso se pierde,{" "}
          <strong>usted no pagará absolutamente nada</strong>. Además, lo
          protegemos de responsabilidad hasta que termine el caso para que no
          esté preocupado por los costos asociados con su caso. Solicite ahora
          su <strong>consulta GRATUITA</strong> con uno de nuestros
          experimentados y mejor calificados.
        </p>
        <p>
          <strong>
            ¡Abogados de lesiones personales de California! Llame al
            877-705-6556
          </strong>
        </p>
      </ContentWrapper>
    </StyledFAQ>
  )
}

const StyledFAQ = styled("section")`
  background-color: ${({ theme }) => theme.colors.secondary};

  .no-fee-link {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.links.hoverOrange};
    }
  }

  h2 {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 2.4rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 3rem;
    letter-spacing: 0.8px;
    line-height: 1.8;
    font-size: 1.3rem;

    @media (max-width: 700px) {
      font-size: 1.2rem;
    }

    &:last-child {
      margin-bottom: 2rem;
    }
  }
`

const ContentWrapper = styled("div")`
  max-width: 1100px;
  margin: 0 auto;
  padding: 3rem 0;
  a {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.links.hoverOrange};
    }
  }

  @media (max-width: 1024px) {
    max-width: 90%;
  }
`
