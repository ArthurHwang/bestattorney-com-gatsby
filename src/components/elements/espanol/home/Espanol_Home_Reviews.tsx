import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { Link } from "src/components/elements/Link"
import { IoMdStar } from "react-icons/io"

const REVIEWS_IMAGE_QUERY = graphql`
  query {
    vanessa: file(relativePath: { regex: "/thumb-vanessa-sampson.png/" }) {
      childImageSharp {
        fixed(width: 60) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    eva: file(relativePath: { regex: "/thumb-eva-shaffer-min.png/" }) {
      childImageSharp {
        fixed(width: 60) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
    brady: file(relativePath: { regex: "/thumb-brady-m-min.png/" }) {
      childImageSharp {
        fixed(width: 60) {
          ...GatsbyImageSharpFixed_noBase64
        }
      }
    }
  }
`

interface Props {
  invert?: boolean
}

export const EspanolReviews: React.FC<Props> = ({
  invert = false
}): ReactElement => {
  return (
    <StaticQuery
      query={REVIEWS_IMAGE_QUERY}
      render={data => {
        return (
          <ContentWrapper invert={invert}>
            <StyledReviews>
              <StyledReview
                invert={invert}
                itemType="http://schema.org/AggregateRating"
              >
                <meta
                  itemProp="itemReviewed"
                  content="https://www.bestatto-gatsby.netlify.app"
                />
                <p className="text-wrapper" itemProp="reviewBody">
                  After a bad accident... we contacted Bisnar Chase and they
                  took care of everything. It gave us the peace of mind we
                  needed to heal. I am very impressed with their level of
                  professionalism and their compassion. We definitely recommend
                  Bisnar Chase.
                </p>
                <div className="review-wrapper">
                  <div>
                    <LazyLoad>
                      <Img
                        fixed={data.vanessa.childImageSharp.fixed}
                        alt="Client Ratings & Reviews"
                        loading="lazy"
                      />
                    </LazyLoad>
                  </div>
                  <div className="review-body">
                    <p itemProp="author" itemScope={true}>
                      by Vanessa Sampson
                    </p>
                    <p>
                      reviewed at{" "}
                      <Link
                        className="review-link"
                        rel="noopener"
                        to="https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
                        target="_blank"
                        itemProp="url"
                      >
                        Google
                      </Link>
                    </p>
                    <p itemProp="reviewRating">
                      <span className="review-stars">
                        {" "}
                        {[1, 2, 3, 4, 5].map(star => (
                          <IoMdStar key={star} className="review-star" />
                        ))}
                      </span>{" "}
                      <meta itemProp="worstRating" content="1" />
                      <span itemProp="ratingValue">5</span> /{" "}
                      <span itemProp="bestRating">5</span>
                    </p>
                  </div>
                </div>
              </StyledReview>
              <StyledReview
                invert={invert}
                itemType="http://schema.org/AggregateRating"
              >
                <meta
                  itemProp="itemReviewed"
                  content="https://www.bestatto-gatsby.netlify.app"
                />
                <p className="text-wrapper" itemProp="reviewBody">
                  I have used Bisnar and Chase twice now, both for car
                  accidents. Each time I received a fair settlement and honestly
                  received more than I expected. The entire team of Bisnar and
                  Chase were professional, courteous and friendly. The
                  paralegals there are top notch!
                </p>
                <div className="review-wrapper">
                  <div>
                    <LazyLoad>
                      <Img
                        fixed={data.eva.childImageSharp.fixed}
                        alt="Client Ratings & Reviews"
                        loading="lazy"
                      />
                    </LazyLoad>
                  </div>
                  <div className="review-body">
                    <p itemProp="author" itemScope={true}>
                      by Eva Shaffer
                    </p>
                    <p>
                      reviewed at{" "}
                      <Link
                        className="review-link"
                        rel="noopener"
                        to="https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
                        target="_blank"
                        itemProp="url"
                      >
                        Google
                      </Link>
                    </p>
                    <p itemProp="reviewRating">
                      <span className="review-stars">
                        {" "}
                        {[1, 2, 3, 4, 5].map(star => (
                          <IoMdStar key={star} className="review-star" />
                        ))}
                      </span>{" "}
                      <meta itemProp="worstRating" content="1" />
                      <span itemProp="ratingValue">5</span> /{" "}
                      <span itemProp="bestRating">5</span>
                    </p>
                  </div>
                </div>
              </StyledReview>

              <StyledReview
                invert={invert}
                itemType="http://schema.org/AggregateRating"
              >
                <meta
                  itemProp="itemReviewed"
                  content="https://www.bestatto-gatsby.netlify.app"
                />
                <p className="text-wrapper" itemProp="reviewBody">
                  John Bisnar was so professional. His staff was amazing and
                  seemed to really care about me as a person. I couldn't be
                  happier with the level of service I received. I was constantly
                  updated on my case status and received far more from my
                  settlement than I expected.
                </p>
                <div className="review-wrapper">
                  <div>
                    <LazyLoad>
                      <Img
                        fixed={data.brady.childImageSharp.fixed}
                        alt="Client Ratings & Reviews"
                        loading="lazy"
                      />
                    </LazyLoad>
                  </div>
                  <div className="review-body">
                    <p itemProp="author" itemScope={true}>
                      by Brady M.
                    </p>
                    <p>
                      reviewed at{" "}
                      <Link
                        className="review-link"
                        rel="noopener"
                        to="https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
                        target="_blank"
                        itemProp="url"
                      >
                        Yelp
                      </Link>
                    </p>
                    <p itemProp="reviewRating">
                      <span className="review-stars">
                        {" "}
                        {[1, 2, 3, 4, 5].map(star => (
                          <IoMdStar key={star} className="review-star" />
                        ))}
                      </span>{" "}
                      <meta itemProp="worstRating" content="1" />
                      <span itemProp="ratingValue">5</span> /{" "}
                      <span itemProp="bestRating">5</span>
                    </p>
                  </div>
                </div>
              </StyledReview>
            </StyledReviews>
          </ContentWrapper>
        )
      }}
    />
  )
}

const ContentWrapper = styled("section")<{ invert: boolean }>`
  background: ${props =>
    props.invert
      ? `${props.theme.colors.primary}`
      : `${props.theme.colors.secondary}`};
`

const StyledReviews: any = styled("div")<{ invert: boolean }>`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  padding: 4rem 2rem 4rem;
  max-width: 1500px;
  margin: 0 auto;
  border-top: 2px solid #394c5c;
  border-top: ${props => (props.invert ? `none` : `2px solid #394C5C`)};

  @media (max-width: 840px) {
    flex-direction: column;
    align-items: center;
    padding: 1rem 2rem 1rem;
  }
`

const StyledReview = styled("div")<{ invert: boolean }>`
  display: flex;
  flex-direction: column;
  width: 250px;

  @media (max-width: 840px) {
    width: 100%;
    margin: 2rem 0;
  }

  .review-link {
    color: ${({ theme }) => theme.links.hoverBlue};
  }

  .text-wrapper {
    font-size: 1rem;
    font-style: italic;
    color: ${props =>
      props.invert
        ? `${props.theme.colors.secondary}`
        : `${props.theme.colors.primary}`};
  }

  .review-wrapper {
    display: grid;
    grid-template-columns: 80px 1fr;

    .review-stars {
      position: relative;
      top: 3px;
    }

    .review-star {
      color: ${({ theme }) => theme.colors.accent};
      font-size: 1.4rem;
    }

    p {
      color: ${props =>
        props.invert
          ? `${props.theme.colors.secondary}`
          : `${props.theme.colors.primary}`};
      margin: 0;
      font-size: 1.1rem;
    }
  }
`
