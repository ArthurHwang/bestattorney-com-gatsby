import React, { ReactElement } from "react"
import Media from "react-media"
import styled from "styled-components"
import { EspanolResultsContent } from "./Espanol_Home_Results-Content"
import { EspanolResultsSidebar } from "./Espanol_Home_Results-Sidebar"

export const EspanolResults: React.FC = (): ReactElement => {
  return (
    <StyledResults>
      <TitleWrapper>
        <h1>Abogados de Lesiones Personales de California</h1>
      </TitleWrapper>
      <ContentWrapper>
        <EspanolResultsContent />
        <Media query="(max-width: 1024px)">
          {matches =>
            matches ? null : (
              <>
                <EspanolResultsSidebar />
              </>
            )
          }
        </Media>
      </ContentWrapper>
    </StyledResults>
  )
}

const StyledResults = styled("section")`
  padding: 4rem 2rem 0;
  margin: 0;
  color: ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.colors.secondary};

  h1 {
    text-shadow: none;
    color: ${({ theme }) => theme.colors.accent};
    font-size: 3.8rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 0 0rem 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
    font-weight: 600;
    margin-top: 0;
    margin-bottom: 2rem;
  }
`

const ContentWrapper = styled("div")`
  margin: 0 auto;
  max-width: 1500px;
  grid-gap: 4rem;
  display: grid;
  grid-template-columns: 1fr 450px;

  @media (max-width: 1024px) {
    grid-template-columns: initial;
  }
`

const TitleWrapper = styled("div")`
  max-width: 1500px;
  margin: 0 auto;
`
