import { graphql, StaticQuery } from "gatsby"
import React, { ReactElement } from "react"
import Media from "react-media"
import Slider from "react-slick"
import styled from "styled-components"
import Video from "src/images/video/bc-brand-video.mp4"
import VideoEncoded from "src/images/carousel//bc-brand-video-encoded.mp4"
import VideoWebm from "src/images/carousel/bc-brand-video.webm"
import "../../header/slick.css"
import { EspanolSlide1 } from "../../carousel/Espanol_Carousel_Slide1"
import { EspanolHomeMobile } from "./Espanol_Header_Home-Mobile"

const CAROUSEL_IMAGE_QUERY = graphql`
  query {
    heroImage: file(
      relativePath: { eq: "carousel/unoptimized/brian-chase-john-bisnar.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 1800, srcSetBreakpoints: [1800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export const EspanolHeaderHome: React.FC = (): ReactElement => {
  const settings = {
    autoplay: false,
    autoplaySpeed: 12000,
    dots: true,
    fade: true,
    infinite: true,
    lazyLoad: "ondemand",
    pauseOnFocus: false,
    pauseOnHover: false,
    slidesToScroll: 1,
    slidesToShow: 1,
    speed: 2500
  }
  return (
    <StaticQuery
      query={CAROUSEL_IMAGE_QUERY}
      render={data => (
        <header>
          <Media query="(max-width: 1024px)">
            {matches =>
              matches ? (
                <MobileHeader className="mobile-home-header">
                  <EspanolHomeMobile
                    video={Video}
                    videoEncoded={VideoEncoded}
                    videoWebm={VideoWebm}
                  />
                </MobileHeader>
              ) : (
                <DesktopHeader className="desktop-home-header">
                  {/* 
                  //@ts-ignore */}
                  <Slider {...settings}>
                    <EspanolSlide1 img={data.heroImage.childImageSharp.fluid} />
                  </Slider>
                </DesktopHeader>
              )
            }
          </Media>
        </header>
      )}
    />
  )
}

const MobileHeader = styled("div")`
  height: 100%;

  .gatsby-image-wrapper {
    height: 100%;
  }

  @media (min-width: 1025px) {
    display: none;
  }
`

const DesktopHeader = styled("section")`
  height: calc(100vh - 115px);

  @media (max-width: 1024px) {
    display: none;
  }

  .gatsby-image-wrapper {
    height: 100%;
  }
`
