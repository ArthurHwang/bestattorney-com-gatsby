import React, { useEffect, useState, ReactElement } from "react"
import styled from "styled-components"
import { IoMdCall, IoMdText } from "react-icons/io"
import { CallButton } from "../../../utilities"

interface Props {
  video: string
  videoEncoded: string
  videoWebm: string
}

export const EspanolHomeMobile: React.FC<Props> = ({
  video,
  videoEncoded,
  videoWebm
}): ReactElement => {
  const windowGlobal: any = typeof window !== "undefined" && window
  const [height, setHeight] = useState(0)

  useEffect(() => {
    setHeight(windowGlobal.innerHeight - 70)

    window.addEventListener("resize", () => {
      setHeight(windowGlobal.innerHeight - 70)
    })

    return () => {
      window.removeEventListener("resize", () => {})
    }
  }, [])

  return (
    <Wrapper viewportHeight={height}>
      <StyledSlideMobile viewportHeight={height}>
        <ContentContainer viewportHeight={height}>
          <TopTextContainer>
            <p className="mobile-title">
              Representación
              <br /> superior<span>.</span>
            </p>
            <p className="mobile-subtitle">
              El 1% de los mejores abogados de la nación
            </p>

            <p className="mobile-title">
              siempre hacer lo correcto
              <br /> por los clientes<span>.</span>
            </p>
            <p className="mobile-subtitle">Abogado del año 2019</p>

            <p className="mobile-title">
              sin ganar sin tarifa <br />
              garantizar<span>.</span>
            </p>
            <p className="mobile-subtitle">adelantamos todos los costos</p>

            <p className="mobile-title">
              resultados
              <br /> impulsados<span>.</span>
            </p>
            <p className="mobile-subtitle">96% de tasa de éxito</p>
          </TopTextContainer>

          <FlexWrapper>
            <BottomTextContainer>
              <p className="mobile-title">
                IF WE DON'T WIN
                <br />
                YOU DON'T PAY<span>.</span>
              </p>
              <span className="mobile-subtitle">
                más de $ 500 millones de won
              </span>
            </BottomTextContainer>
            <ButtonsContainer>
              <a href="tel:8777056556">
                <MobileCallButton>
                  <IoMdCall />
                  CALL US
                </MobileCallButton>
              </a>
              <a href="sms:9492643108">
                <MobileTextButton>
                  <IoMdText />
                  TEXT US
                </MobileTextButton>
              </a>
            </ButtonsContainer>
          </FlexWrapper>
        </ContentContainer>

        <video
          title="Brian Chase, Trial Lawyer in Pursuit of Justice"
          muted={true}
          autoPlay={true}
          loop={true}
          preload="auto"
        >
          <source src={videoEncoded} type="video/mp4" />
          <source src={video} type="video/mp4" />
          <source src={videoWebm} type="video/webm" />
        </video>
        <OpacityContainer viewportHeight={height} />
      </StyledSlideMobile>
    </Wrapper>
  )
}

const Wrapper = styled("div")<{ viewportHeight: number }>`
  min-height: ${props =>
    props.viewportHeight > 0
      ? props.viewportHeight + "px"
      : "calc(100vh - 70px)"};
  background-color: rgba(0, 0, 0, 0.8);
`

const FlexWrapper = styled("div")`
  z-index: 1;
`

const ContentContainer = styled("div")<{ viewportHeight: number }>`
  flex-direction: column;
  position: absolute;
  justify-content: space-between;
  height: ${props => props.viewportHeight + "px"};
  width: 100%;
  padding: 2.5rem 1rem 1rem;
  display: flex;
  display: ${props => (props.viewportHeight > 0 ? "flex" : "none")};
`

const BottomTextContainer = styled("div")`
  z-index: 1;
  width: 100%;
  text-align: center;

  .mobile-title span {
    font-size: 1.6rem;
  }

  .mobile-title {
    text-align: center;
  }

  span {
    color: ${({ theme }) => theme.colors.accent};
    font-weight: 600;
    font-variant: small-caps;
    font-size: 1.2rem;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2.8rem;
    line-height: 1;
    margin-bottom: 0;
    font-variant: small-caps;
    letter-spacing: 4px;
    text-shadow: black 0px 0px 2px;
    font-weight: 600;
  }
`

const MobileCallButton = styled(CallButton)`
  width: 170px;
  height: 45px;

  svg {
    position: relative;
    top: 2.5px;
    right: 10px;
    color: ${({ theme }) => theme.colors.accent};
  }
`

const MobileTextButton = styled(CallButton)`
  width: 170px;
  height: 45px;

  svg {
    position: relative;
    top: 4px;
    right: 8px;
    color: ${({ theme }) => theme.colors.accent};
  }
`

const ButtonsContainer = styled("div")`
  z-index: 1;
  display: flex;
  justify-content: space-between;

  button {
    display: block;
    margin: 2rem 0;
  }
`

const TopTextContainer = styled("div")`
  z-index: 1;
  width: 100%;

  @media (min-width: 700px) {
    padding-left: 1rem;
  }

  span {
    color: ${({ theme }) => theme.colors.accent};
  }

  p {
    display: block;
    text-shadow: black 0px 0px 2px;
  }

  .mobile-title {
    font-size: 1.8rem;
    color: ${({ theme }) => theme.colors.primary};
    font-variant: small-caps;
    line-height: 1;
    margin-bottom: 0.1rem;
    text-shadow: black 0px 0px 2px;

    &:nth-of-type(2) {
      margin-top: 1rem;
    }
  }

  .mobile-subtitle {
    font-size: 1.2rem;
    color: ${({ theme }) => theme.colors.accent};
    font-variant: small-caps;
    font-weight: 600;
    text-shadow: black 0px 0px 2px;
  }
`

const OpacityContainer = styled("div")<{ viewportHeight: number }>`
  height: ${props => props.viewportHeight + "px"};
  display: ${props => (props.viewportHeight > 0 ? "block" : "none")};
  background: rgba(0, 0, 0, 0.25);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
`

const StyledSlideMobile = styled("div")<{ viewportHeight: number }>`
  position: relative;
  height: ${props => props.viewportHeight + "px"};
  display: ${props => (props.viewportHeight > 0 ? "block" : "none")};

  video {
    position: relative;
    min-height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    object-position: 68% 50%;
    object-fit: cover;
  }
`
