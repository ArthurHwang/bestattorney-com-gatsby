import React, { ReactElement } from "react"
import styled from "styled-components"
import { LazyLoad } from "../elements/LazyLoad"
import { Link } from "../elements/Link"

interface Props {
  locationBox1: any
  locationBox2: any
  locationBox3: any
}

export const MiniLocationWidget: React.FC<Props> = ({
  locationBox1,
  locationBox2,
  locationBox3
}): ReactElement => {
  const avgInjuries = Math.round(
    (locationBox1.totalAccidents * 200) / locationBox1.population
  )

  function cityStringParse(city: string): string {
    if (city.includes(" ")) {
      return city.split(" ").join("-")
    }
    return city
  }

  return (
    <ContentWrap>
      <StyledTop>
        <p className="top-title">{`Most Dangerous Intersection in ${locationBox1.city}**`}</p>
        <p className="top-intersection">{locationBox1.intersection1}</p>
        <div className="grid-cities">
          <div className="col col-1">
            <p className="title">Last 5 Years:</p>
            <p>{`${locationBox1.intersection1Accidents} Accidents`}</p>
            <p>{`${locationBox1.intersection1Injuries} Injuries`}</p>
            <p>{`${locationBox1.intersection1Deaths} Deaths`}</p>
          </div>

          <div className="col col-2">
            <p className="title">{`City of ${locationBox1.city}`}</p>
            <p>{`Population: ${locationBox1.population}`}</p>
            <p>{`Avg Auto Injuries per 1,000: ${avgInjuries}`}</p>
            <p>{`${locationBox1.city} Crime Index*: ${locationBox2.mainCrimeIndex}`}</p>
          </div>

          <div className="col col-3">
            <p className="title">Nearby Cities Crime Indicies*</p>
            <p>{`${locationBox2.city1Name}: ${locationBox2.city1Index}`}</p>
            <p>{`${locationBox2.city2Name}: ${locationBox2.city2Index}`}</p>
            <p>{`${locationBox2.city3Name}: ${locationBox2.city3Index}`}</p>
          </div>
        </div>
        <div className="grid-media">
          <div className="col-3">
            <LazyLoad>
              <img
                style={{ marginBottom: "0", width: "100%", height: "100%" }}
                src={`/images/location-images/city-images/${locationBox1.city
                  .toLowerCase()
                  .replace(/ /gi, "-")}-street-view.jpg`}
                alt=""
              />
            </LazyLoad>
          </div>
          <div className="col-2">
            <LazyLoad>
              <iframe
                width="100%"
                height="98.4%"
                frameBorder="0"
                style={{ border: "0" }}
                src={`https://www.google.com/maps/embed/v1/place?zoom=14&key=${
                  process.env.GATSBY_GOOGLE_MAPS_APIKEY
                }&q=${locationBox1.intersection1
                  .replace("&", "and")
                  .replace(/ /gi, "+")}+${locationBox1.city}`}
              />
            </LazyLoad>
          </div>
        </div>
        <TextWrap>
          <p style={{ fontSize: "0.9rem", margin: "0", marginTop: "0.5rem" }}>
            * Crime index data taken from{" "}
            <Link
              target="_blank"
              to={`https://www.city-data.com/city/${cityStringParse(
                locationBox1.city
              )}-California.html`}
            >
              city-data.com.
            </Link>{" "}
            The national average is 315.5. The crime index is calculated using
            the population and severity of crimes vs the frequency of crimes
            committed.
          </p>
          <p style={{ fontSize: "0.9rem", margin: "0" }}>
            **All intersection data is taken from the{" "}
            <Link target="_blank" to="http://iswitrs.chp.ca.gov/">
              California Statewide Integrated Traffic Records System
            </Link>{" "}
            and is measured over the last 5 years of local and arterial street
            car accidents (non-highway). The data is not updated in real time.{" "}
          </p>
        </TextWrap>
      </StyledTop>

      <hr
        style={{
          color: "#dad1b5",
          backgroundColor: "#dad1b5",
          height: 2,
          margin: "1rem 0"
        }}
      />
      <StyledBottom>
        <p className="top-title">{`More Unsafe Intersections in ${locationBox1.city}**`}</p>
        <div className="grid-cities">
          <div className="col col-1">
            <p className="title">{locationBox3.intersection2}</p>
            <p>{`Accidents: ${locationBox3.intersection2Accidents}`}</p>
            <p>{`Injuries: ${locationBox3.intersection2Injuries}`}</p>
            <p>{`Deaths: ${locationBox3.intersection2Deaths}`}</p>
          </div>

          <div className="col col-2">
            <p className="title">{locationBox3.intersection3}</p>
            <p>{`Accidents: ${locationBox3.intersection3Accidents}`}</p>
            <p>{`Injuries: ${locationBox3.intersection3Injuries}`}</p>
            <p>{`Deaths: ${locationBox3.intersection3Deaths}`}</p>
          </div>

          <div className="col col-3">
            <p className="title">{locationBox3.intersection4}</p>
            <p>{`Accidents: ${locationBox3.intersection4Accidents}`}</p>
            <p>{`Injuries: ${locationBox3.intersection4Injuries}`}</p>
            <p>{`Deaths: ${locationBox3.intersection4Deaths}`}</p>
          </div>
        </div>

        <p
          style={{
            fontSize: "0.9rem",
            margin: "0",
            marginTop: "0.5rem",
            padding: "0 2rem 1rem"
          }}
        >
          ** All intersection data is taken from the California Statewide
          Integrated Traffic Records System and is measured over the last 5
          years of local and arterial street car accidents (non-highway). The
          data is not updated in real time.
        </p>
      </StyledBottom>
    </ContentWrap>
  )
}

const ContentWrap = styled("div")`
  margin-bottom: 2rem;
  .top-title {
    background: ${({ theme }) => theme.colors.secondary};
    color: ${({ theme }) => theme.colors.accent};
    text-align: center;
    font-size: 2rem;
    padding: 0.6rem;
    text-transform: uppercase;
    font-weight: 600;
    font-style: italic;
  }

  .top-intersection {
    text-align: center;
    font-size: 1.8rem;
    color: ${({ theme }) => theme.links.normal};
    font-weight: 600;
  }

  .grid-cities {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    margin-bottom: 2rem;

    @media (max-width: 767px) {
      grid-gap: 1rem;
    }

    @media (max-width: 520px) {
      grid-template-columns: initial;
      grid-template-rows: 1fr 1fr 1fr;
    }

    .col {
      padding: 0 1rem;

      @media (max-width: 767px) {
        padding: 0 1rem;
      }

      @media (max-width: 520px) {
        padding: 0 4rem;
      }

      p.title {
        font-size: 1.4rem;
        color: ${({ theme }) => theme.links.normal};
        border-bottom: 1px solid black;
        font-weight: 600;
      }
      p {
        text-align: center;
        font-size: 1.2rem;
        margin-bottom: 0.3rem;
      }
    }
  }
`

const TextWrap = styled("div")`
  padding: 0 2rem 1rem;
`

const StyledTop = styled("div")`
  background: #eee;

  .hr {
    border-bottom: 1px solid black;
    padding: 0;
    margin: 0;
  }

  .grid-media {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: auto;

    img {
      width: 100%;
      height: 100%;
      margin-bottom: 0;
    }

    iframe {
      @media (max-width: 700px) {
        height: 100% !important;
      }
    }

    @media (max-width: 870px) {
      grid-template-columns: 1fr;
      grid-template-rows: auto 350px;
    }

    @media (max-width: 450px) {
      grid-template-columns: 1fr;
      grid-template-rows: auto 250px;
    }
  }
`

const StyledBottom = styled("div")`
  background: #eee;
`
