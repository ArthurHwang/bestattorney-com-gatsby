import { Link as GatsbyLink } from "gatsby"
import React, { ReactElement } from "react"

interface Props {
  children?: any
  to: any
  activeClassName?: string
  className?: string
  partiallyActive?: boolean
  target?: string
  itemProp?: string
}

export const Link: React.FC<Props> = ({
  children,
  to,
  activeClassName,
  partiallyActive,
  ...other
}: Props): ReactElement => {
  const internal = /^\/(?!\/)/.test(to)

  // Use Gatsby Link for internal links, and <a> for others
  // We are also checking if the link is a PDF, if PDF it should return normal anchor
  // It is written this way due to static folder path makes it seem like a gatsby link

  if (internal && to.includes("pdf")) {
    return (
      <a href={to} {...other}>
        {children}
      </a>
    )
  }

  if (internal && to.includes("jpg")) {
    return (
      <a href={to} {...other}>
        {children}
      </a>
    )
  }

  if (internal && to.includes("png")) {
    return (
      <a href={to} {...other}>
        {children}
      </a>
    )
  }

  if (internal) {
    return (
      <GatsbyLink
        to={to}
        activeClassName={activeClassName}
        partiallyActive={partiallyActive}
        {...other}
      >
        {children}
      </GatsbyLink>
    )
  }

  return (
    <a href={to} {...other}>
      {children}
    </a>
  )
}

// export default Link
