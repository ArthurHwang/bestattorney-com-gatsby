import React, { ReactElement } from "react"
import {
  LazyLoadImage,
  LazyLoadComponent
} from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"

interface Props {
  children: ReactElement
}

export const LazyLoad: React.FC<Props> = ({ children }): ReactElement => {
  // ! If type === img, add no script tag underneath for SSR SEO / Google to be able to be aware of images for SEO purposes

  // ! Adding height to LazyLoad prop will result in weird total page heights (use caution)
  if (children.type === "img") {
    return (
      <>
        <LazyLoadImage
          threshold={0}
          effect="blur"
          alt={children.props.alt}
          src={children.props.src}
          placeholderSrc={children.props.src}
          width={children.props.width}
          className={children.props.className}
          wrapperClassName="lazyload-img-wrapper"
          {...children.props}
        />
        <noscript>
          <style>{`.lazy-load-image-background { display: none; }`}</style>
          {children}
        </noscript>
      </>
    )
  }

  if (children.type === "iframe") {
    return (
      <LazyLoadComponent {...children.props} threshold={700}>
        {children}
      </LazyLoadComponent>
    )
  }

  // ! Default return is specified towards h2 header with background image
  return (
    <LazyLoadComponent {...children.props} threshold={0}>
      {children}
    </LazyLoadComponent>
  )
}

// export default LazyLoad
