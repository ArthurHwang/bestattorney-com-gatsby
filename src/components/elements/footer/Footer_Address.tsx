import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  className: string
}

export const FooterAddress: React.FC<Props> = ({ className }): ReactElement => {
  return (
    <StyledAddress className={className}>
      <p>
        BISNAR CHASE PERSONAL INJURY ATTORNEYS • 1301 DOVE ST, #120 NEWPORT
        BEACH, CA 92660 <span>FREE</span> INITIAL CONSULTATION • LOCAL: (877)
        958-8092
      </p>
    </StyledAddress>
  )
}

const StyledAddress = styled("div")`
  padding-top: 0.8rem;

  p {
    font-size: 1.2rem;
    font-weight: 600;
    max-width: 70%;
    text-align: center;
    margin: 0 auto;

    @media (max-width: 480px) {
      width: 297px;
      text-align: justify;
      max-width: 100%;
    }
  }

  span {
    font-weight: 600;
    color: ${({ theme }) => theme.colors.accent};
  }
`
