import React, { ReactElement } from "react"
import styled from "styled-components"
import { FooterAddress } from "./Footer_Address"
import { FooterConnect } from "./Footer_Connect"
import { FooterDisclaimer } from "./Footer_Disclaimer"
import { FooterMetro } from "./Footer_Metro"
import { FooterSitemap } from "./Footer_Sitemap"

export const Footer: React.FC = (): ReactElement => {
  return (
    <StyledFooter>
      <FooterAddress className="footer-address" />
      <FooterMetro className="footer-metros" />
      <FooterSitemap className="footer-sitemap" />
      <FooterConnect className="footer-connect" />
      <FooterDisclaimer className="footer-disclaimer" />
    </StyledFooter>
  )
}

const StyledFooter = styled("footer")`
  display: grid;
  grid-template-areas:
    "address address address"
    "sitemap connect metros"
    "disclaimer disclaimer disclaimer";
  border-top: 1px solid #222533;
  grid-template-rows: repeat(3, auto);
  grid-template-columns: 1fr 240px 1fr;
  height: auto;

  .footer-address {
    grid-area: address;
  }

  .footer-metros {
    grid-area: metros;
  }

  .footer-sitemap {
    grid-area: sitemap;
  }

  .footer-connect {
    grid-area: connect;
  }

  .footer-disclaimer {
    grid-area: disclaimer;
  }

  @media (max-width: 1411px) {
  }

  @media (max-width: 1160px) {
    grid-template-rows: repeat(4, auto);
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      "address address"
      "connect connect"
      "sitemap metros"
      "disclaimer disclaimer";
  }

  @media (max-width: 850px) {
    grid-template-rows: repeat(5, auto);
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      "address address"
      "connect connect"
      "metros metros"
      "sitemap sitemap"
      "disclaimer disclaimer";
  }
`
