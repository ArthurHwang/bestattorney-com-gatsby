// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "../../../../components/utilities/themeMock.js"
import { FooterMetro } from "../Footer_Metro"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <FooterMetro className="footer-metro" />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Footer Metro", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should include orange county address", () => {
    const list: any = instance.find(
      el => el.type === "ul" && el.props.className === "address"
    )
    const listItem = list.children[0]
    expect(listItem.children[0]).toBe(
      "1301 Dove St, #120 Newport Beach, CA 92660"
    )
  })
  it("should include Los Angeles address", () => {
    const list: any = instance.find(
      el => el.type === "ul" && el.props.className === "address"
    )
    const listItem = list.children[1]
    expect(listItem.children[0]).toBe(
      "6701 Center Drive West, 14th Fl. LA, CA 90045"
    )
  })
  it("should include Riverside address", () => {
    const list: any = instance.find(
      el => el.type === "ul" && el.props.className === "address"
    )
    const listItem = list.children[2]
    expect(listItem.children[0]).toBe(
      "6809 Indiana Avenue #148, Riverside, CA 92506"
    )
  })
  it("should include San Bernardino address", () => {
    const list: any = instance.find(
      el => el.type === "ul" && el.props.className === "address"
    )
    const listItem = list.children[3]
    expect(listItem.children[0]).toBe("473 S Carnegie Dr 2nd Floor, Room #221")
  })
})
