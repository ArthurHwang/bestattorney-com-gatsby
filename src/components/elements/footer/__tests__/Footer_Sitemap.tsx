// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "../../../../components/utilities/themeMock.js"
import { FooterSitemap } from "../Footer_Sitemap"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <FooterSitemap className="footer-metro" />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Footer Sitemap", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should include giving back link", () => {
    const listItem = instance.find(
      el => el.type === "a" && el.props.href === "/giving-back"
    )
    expect(listItem.children[0]).toBe("Giving Back")
  })
  it("should include home link", () => {
    const listItem = instance.find(
      el => el.type === "a" && el.props.href === "/"
    )
    expect(listItem.children[0]).toBe("Home")
  })
  it("should include reviews link", () => {
    const listItem = instance.find(
      el => el.type === "a" && el.props.href === "/resources"
    )
    expect(listItem.children[0]).toBe("Resources")
  })
  it("should include brian chase link", () => {
    const listItem = instance.find(
      el => el.type === "a" && el.props.href === "/attorneys/brian-chase"
    )
    expect(listItem.children[0]).toBe("Brian Chase")
  })
})
