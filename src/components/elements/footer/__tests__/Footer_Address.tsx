// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "../../../../components/utilities/themeMock.js"
import { FooterAddress } from "../Footer_Address"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <FooterAddress className="footer-address" />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Footer Address", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should include address and phone number", () => {
    const address = instance.findByType("p")
    expect(address.props.children).toContain(
      "BISNAR CHASE PERSONAL INJURY ATTORNEYS • 1301 DOVE ST, #120 NEWPORT BEACH, CA 92660 "
    )
  })
})
