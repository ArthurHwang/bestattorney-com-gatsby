// import { render } from "react-dom"

import React from "react"
import renderer from "react-test-renderer"
import { ThemeProviderWrapper } from "../../../../components/utilities/themeMock.js"
import { FooterDisclaimer } from "../Footer_Disclaimer"

let container: renderer.ReactTestRenderer
let instance: renderer.ReactTestInstance

beforeAll(() => {
  container = renderer.create(
    <ThemeProviderWrapper>
      <FooterDisclaimer className="footer-disclaimer" />
    </ThemeProviderWrapper>
  )
  instance = container.root
})

describe("Footer Disclaimer", () => {
  it("should match snapshot", () => {
    const tree = container.toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("should include disclaimer", () => {
    const disclaimer = instance.find(
      el => el.type === "p" && el.props.className === "disclaimer-body"
    )
    expect(disclaimer.children[1]).toContain(
      "The legal information presented at this site should not be construed to be formal legal advice, nor the formation of an attorney-client relationship. Any results set forth here were dependent on the facts of that case and the results will differ from case to case."
    )
    expect(disclaimer.children[3]).toContain(
      " serves all of California. In addition, we represent clients in various other states through our affiliations with local law firms. Through the local firm we will be admitted to practice law in their state, pro hac vice"
    )
  })
})
