import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  className: string
}

export const FooterSitemap: React.FC<Props> = ({ className }): ReactElement => {
  return (
    <StyledSitemap className={className}>
      <div className="sitemap">Sitemap</div>
      <div className="col col-1">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about-us">About Us</Link>
          </li>
          <li>
            <Link to="/attorneys/john-bisnar">John Bisnar</Link>
          </li>
          <li>
            <Link to="/attorneys/brian-chase">Brian Chase</Link>
          </li>
        </ul>
      </div>
      <div className="col col-2">
        <ul>
          <li>
            <Link to="/giving-back">Giving Back</Link>
          </li>
          <li>
            <Link to="/about-us/lawyer-reviews-ratings">Why Hire Us?</Link>
          </li>
          <li>
            <Link to="/practice-areas">Practice Areas</Link>
          </li>
          <li>
            <Link to="/about-us/testimonials">Reviews</Link>
          </li>
        </ul>
      </div>
      <div className="col col-3">
        <ul>
          <li>
            <Link to="/press-releases">Press Releases</Link>
          </li>
          <li>
            <Link to="/about-us/testimonials">Testimonials</Link>
          </li>
          <li>
            <Link to="/case-results">Case Results</Link>
          </li>
          <li>
            <Link to="/contact">Contact Us</Link>
          </li>
        </ul>
      </div>
      <div className="col col-4">
        <ul>
          <li>
            <Link to="/blog">Read Our Blog</Link>
          </li>
          <li>
            <Link to="/resources">Resources</Link>
          </li>
          <li>
            <Link to="/abogados">En Español</Link>
          </li>
          <li>
            <a href="https://bestatto-gatsby.netlify.app/sitemap.xml">
              Sitemap
            </a>
          </li>
        </ul>
      </div>
    </StyledSitemap>
  )
}

const StyledSitemap = styled("div")`
  display: grid;
  grid-template-rows: 80px 1fr;
  grid-template-columns: repeat(4, 100px);
  margin: 0 4rem 0 auto;

  ul {
    list-style-type: none;
    margin: 0;
    display: flex;
    flex-direction: column;
    height: 80%;
    margin-top: 1.7rem;

    @media (max-width: 1160px) {
      margin: 0;
    }

    li {
      flex: 1;
      align-self: center;
      margin: 0;
      font-size: 1rem;

      a {
        flex: 1;
        display: flex;
      }
    }
  }

  .col {
    justify-self: center;
  }

  .sitemap {
    border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};
    grid-column: 1 / -1;
    padding: 1rem;
    text-align: center;
    font-size: 2rem;
    align-self: center;
  }

  @media (max-width: 1160px) {
    margin: 0 1rem 0 auto;
    height: 200px;
    ul {
      justify-content: space-between;
      height: 100%;
    }
  }

  @media (max-width: 850px) {
    margin: 0 auto;
    height: 182px;
    grid-template-rows: 80px 1fr;
  }

  @media (max-width: 425px) {
    width: 100%;
    height: 250px;
    margin-bottom: 1rem;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 52px 1fr 1fr;
    grid-template-areas:
      "sitemap sitemap"
      "col1 col3"
      "col2 col4";

    ul {
      margin-top: 1rem;
    }

    .col-1 {
      grid-area: col1;
    }

    .col-2 {
      grid-area: col2;
    }

    .col-3 {
      grid-area: col3;
    }

    .col-4 {
      grid-area: col4;
    }
  }
`
