import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  className: string
}

export const FooterMetro: React.FC<Props> = ({ className }): ReactElement => {
  return (
    <StyledSitemap className={className}>
      <div className="metros">Metros Served</div>
      <ul className="address">
        <li>1301 Dove St, #120 Newport Beach, CA 92660</li>
        <li>6701 Center Drive West, 14th Fl. LA, CA 90045</li>
        <li>6809 Indiana Avenue #148, Riverside, CA 92506</li>
        <li>
          473 S Carnegie Dr 2nd Floor, Room #221
          <br /> San Bernardino, CA 92408
        </li>
      </ul>
      <ul className="numbers">
        <li id="phone-1">(949) 203-3814</li>
        <li id="phone-2">(323) 238-4683</li>
        <li id="phone-3">(951) 530-3711</li>
        <li id="phone-4">(909) 253-0750</li>
      </ul>
    </StyledSitemap>
  )
}

const StyledSitemap = styled("div")`
  display: grid;
  grid-template-rows: 80px 1fr;
  grid-template-columns: 300px 100px;
  margin: 0 auto 0 4rem;

  .metros {
    border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};
    grid-column: 1 / -1;
    padding: 1rem;
    text-align: center;
    font-size: 2rem;
    align-self: center;
  }

  .address {
    margin: 0 auto;
    margin-top: 1.5rem;

    @media (max-width: 1160px) {
      margin: 0;
    }
  }

  .numbers {
    margin-top: 1.3rem;
    text-align: center;
    color: ${({ theme }) => theme.colors.accent};

    li {
      font-weight: 600;
    }

    @media (max-width: 1160px) {
      margin: 0;

      li {
        position: relative;
        bottom: 1px;
      }

      & li + li {
        position: relative;
        bottom: 2px;
      }
    }

    @media (max-width: 425px) {
      align-items: flex-end;
      padding-right: 1.2rem;

      #phone-1 {
        bottom: 6px;
      }
      #phone-2 {
        bottom: 6px;
      }
      #phone-3 {
        bottom: 4px;
      }
      #phone-4 {
        bottom: 3px;
      }
    }
  }

  ul {
    list-style-type: none;
    margin: 0;
    display: flex;
    flex-direction: column;

    li {
      flex: 1;
      font-size: 1rem;
    }
  }

  @media (max-width: 1160px) {
    margin: 0 auto 0 1rem;
    height: 200px;

    .address {
      li {
        padding-left: 1rem;
      }
    }
  }

  @media (max-width: 850px) {
    margin: 0 auto;
  }

  @media (max-width: 425px) {
    grid-template-columns: 2fr 1fr;
    height: 300px;

    .numbers {
      padding-top: 0.5rem;
    }
  }
`
