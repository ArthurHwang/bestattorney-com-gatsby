import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { FaPhone, FaArrowAltCircleRight } from "react-icons/fa"
import { DefaultBlueButton, DefaultOrangeButton } from "../utilities"

// This component only shows up on Tablet / Mobile
// See Layout Components

export const CallBanner: React.FC = (): ReactElement => {
  return (
    <StyledCallBanner>
      <Link to="tel:8005614887">
        <DefaultBlueButton>
          <FaPhone className="fa-phone" /> Call Us Today
        </DefaultBlueButton>
      </Link>
      <Link to="/contact">
        <DefaultOrangeButton>
          Free Case Review <FaArrowAltCircleRight className="fa-arrow" />
        </DefaultOrangeButton>
      </Link>
    </StyledCallBanner>
  )
}

const StyledCallBanner = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 30px;

  @media (min-width: 1025px) {
    display: none;
  }

  .fa-phone {
    transform: rotate(90deg);
    margin-right: 5px;
  }

  .fa-arrow {
    margin-left: 5px;
  }

  .fa-phone,
  .fa-arrow {
    position: relative;
    top: 1px;
  }

  button {
    border-radius: 0;
    font-size: 1rem;
    text-transform: uppercase;
    width: 100%;
    box-shadow: none;
    font-weight: 500;
    height: 100%;
    padding: 0;
  }
`
