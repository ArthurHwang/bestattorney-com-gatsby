import { graphql, StaticQuery } from "gatsby"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { formatWPtitle } from "../../utilities"
import { useLocation } from "@reach/router"

const RECENT_BLOG_QUERY = graphql`
  query {
    allWpPost(limit: 5) {
      edges {
        node {
          title
          slug
          date
        }
      }
    }
  }
`
function monthSwap(numericMonth: string): string | null {
  switch (numericMonth) {
    case "01":
      return "JAN"
    case "02":
      return "FEB"
    case "03":
      return "MAR"
    case "04":
      return "APR"
    case "05":
      return "MAY"
    case "06":
      return "JUN"
    case "07":
      return "JUL"
    case "08":
      return "AUG"
    case "09":
      return "SEP"
    case "10":
      return "OCT"
    case "11":
      return "NOV"
    case "12":
      return "DEC"
    default:
      return null
  }
}

export const SidebarBlog: React.FC = (): ReactElement => {
  const location = useLocation()

  console.log(location)
  return (
    <StaticQuery
      query={RECENT_BLOG_QUERY}
      render={data => {
        const posts = data.allWpPost.edges

        return (
          <StyledBlogNews>
            <p className="title">
              <span>News</span> From Our Blog
            </p>
            {posts.map(({ node }: any, index: any) => {
              const date = node.date.split("T")[0].split("-")
              return (
                <Link
                  className="link"
                  key={index}
                  to={
                    location.pathname.includes("blog")
                      ? `${node.slug}`
                      : `blog/${node.slug}`
                  }
                >
                  <StyledBlogCard>
                    <StyledBlogDate>
                      <p className="month">{monthSwap(date[1])}</p>
                      <p className="day">{date[2]}</p>
                    </StyledBlogDate>
                    <StyledBlogContent>
                      {formatWPtitle(node.title)}
                    </StyledBlogContent>
                  </StyledBlogCard>
                </Link>
              )
            })}
          </StyledBlogNews>
        )
      }}
    />
  )
}

const StyledBlogNews = styled("div")`
  background-color: ${({ theme }) => theme.colors.secondary};

  span {
    color: ${({ theme }) => theme.colors.accent};
    font-style: italic;
    font-weight: 600;
  }

  .title {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2rem;
    text-transform: uppercase;
    text-align: center;
    padding: 2rem;
    padding-bottom: 1.5rem;
    margin-bottom: 0;
    border-bottom: medium double grey;
  }
`

const StyledBlogDate = styled("div")`
  color: ${({ theme }) => theme.colors.primary};
  background-color: ${({ theme }) => theme.colors.accent};
  display: flex;
  flex-direction: column;
  justify-content: center;

  p {
    margin-bottom: 0;
    text-align: center;
    font-weight: 600;
    font-size: 1.6rem;

    &.month {
      border-bottom: 2px solid ${({ theme }) => theme.colors.secondary};
    }

    &.day {
      color: ${({ theme }) => theme.links.normal};
    }
  }
`

const StyledBlogContent = styled("div")`
  color: ${({ theme }) => theme.colors.primary};
  padding-left: 1rem;
  font-size: 1.2rem;
  display: flex;
  align-items: center;
`

const StyledBlogCard = styled("div")`
  display: grid;
  grid-template-columns: 5rem 3fr;
  height: 80px;
  border-bottom: 3px solid grey;
  transition: transform 0.3s ease-out;
  background-color: ${({ theme }) => theme.colors.secondary};

  &:hover {
    transform: scale(1.03);
  }
`
