import React, { ReactElement } from "react"
import { FaRegArrowAltCircleRight } from "react-icons/fa"
import Slider from "react-slick"
import styled from "styled-components"
import { CaseResults } from "../../../data/home/caseResults"
import { DefaultOrangeButton, elevation } from "../../utilities"
import { Link } from "src/components/elements/Link"

function formatNumber(num: number) {
  return new Intl.NumberFormat().format(num)
}

export const SidebarCaseResults: React.FC = (): ReactElement => {
  const settings = {
    dots: false,
    infinite: true,
    lazyLoad: "onedemand",
    slidesToScroll: 1,
    slidesToShow: 5,
    vertical: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 9000,
    draggable: false,
    swipe: false,
    touchMove: false
  }

  return (
    <StyledCaseResultsSidebar>
      <TitleWrapper>
        <p className="title-text">
          <span>OUR</span> CASE RESULTS
        </p>
      </TitleWrapper>

      <ResultsCarousel>
        {/* 
        //@ts-ignore */}
        <Slider className="carousel" {...settings}>
          {CaseResults.map(result => (
            <Link key={result.id} to={result.href}>
              <ResultCard>
                <div className="result-id">{result.id}</div>
                <div>
                  <p className="result-amount">
                    {`$${formatNumber(result.amountWon)}`}
                  </p>
                  <p className="result-type">{result.type}</p>
                </div>
                <FaRegArrowAltCircleRight className="result-arrow" />
              </ResultCard>
            </Link>
          ))}
        </Slider>
      </ResultsCarousel>
      <div className="btn-wrapper">
        <Link className="btn-link" to="/case-results">
          {/* 
          //@ts-ignore */}
          <DefaultOrangeButton className="btn-case-results" modifiers="small">
            VIEW OUR CASE RESULTS
          </DefaultOrangeButton>
        </Link>
      </div>
    </StyledCaseResultsSidebar>
  )
}

const TitleWrapper = styled("div")`
  height: auto;
  display: flex;
  align-items: center;
  justify-content: center;

  .title-text {
    font-size: 2rem;
    border-bottom: medium double grey;
    width: 100%;
    padding-top: 2rem;
    padding-bottom: 1.5rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-weight: 600;
      font-style: italic;
    }
  }
`

const ResultCard = styled("div")`
  display: grid !important;
  grid-template-columns: 100px 1fr 100px;
  color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  border-bottom: 2px solid #394c5c;
  transition: transform 0.2s ease-out, box-shadow 0.2s linear;

  .dollar-sign {
    position: relative;
    top: 5px;
    color: #85bb65;
    left: 2px;
  }

  .result-id {
    display: flex;
    justify-content: center;
    align-items: center;
    background: linear-gradient(to right, #ffd89b, #19547b) center center / 80px
      80px no-repeat;
    height: 85px;
    width: 85px;
    border: 15px solid;
    border-radius: 50%;
    font-size: 2rem;
    font-weight: 600;
    margin: 0 auto;
  }

  .result-amount {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2.5rem;
    font-style: italic;
    font-variant: proportional-nums;
    justify-self: center;

    @media (max-width: 445px) {
      font-size: 1.8rem;
    }
  }

  .result-type {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 1.4rem;

    @media (max-width: 445px) {
      font-size: 1.1rem;
    }
  }

  .result-arrow {
    justify-self: center;
    font-size: 2.5rem;
    color: ${({ theme }) => theme.colors.accent};
  }

  &:hover {
    transform: scale(1.07);
    ${elevation[3]};
  }
`

const ResultsCarousel = styled("div")`
  .carousel {
    .slick-list {
    }
    .slick-track {
      border: none;
      .slick-slide {
        height: 90px;
      }
    }

    .slick-list {
      border: none;
    }
  }
`

const StyledCaseResultsSidebar = styled("div")`
  position: relative;
  background-color: ${({ theme }) => theme.colors.secondary};
  overflow: hidden;

  p {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 1.6rem;
    text-align: center;
    margin: 0;
  }

  .btn-wrapper {
    background-color: ${({ theme }) => theme.colors.secondary};
    height: 64px;
  }

  .btn-link {
    position: absolute;
    left: 50%;

    margin-left: -88px;
    margin-top: 1rem;

    .btn-case-results {
      height: 3rem;
      width: 176px;
    }
  }
`
