import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

export const SidebarAttorneys: React.FC = (): ReactElement => {
  return (
    <StyledAttorneys>
      <p className="title">
        <span>OUR</span> ATTORNEYS
      </p>
      <p className="subtext">FIND OUT MORE ABOUT OUR STAFF</p>
      <ul>
        <li>
          <strong>
            <Link to="/attorneys/john-bisnar">John Bisnar</Link>
          </strong>
          <br /> Founding Partner
        </li>
        <li>
          <strong>
            <Link to="/attorneys/brian-chase">Brian Chase</Link>
          </strong>
          <br /> Senior Partner
        </li>
        <li>
          <strong>
            <Link to="/attorneys/scott-ritsema">Scott Ritsema</Link>
          </strong>
          <br /> Partner
        </li>
        <li>
          <strong>
            <Link to="/attorneys/jerusalem-beligan">Jerusalem Beligan</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/gavin-long">H. Gavin Long</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/steven-hilst">Steven Hilst</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/tom-antunovich">Tom Antunovich</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/ian-silvers">Ian Silvers</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/krist-biakanja">Krist Biakanja</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/jordan-southwick">Jordan Southwick</Link>
          </strong>
          <br /> Lawyer
        </li>
        <li>
          <strong>
            <Link to="/attorneys/legal-administrator-shannon-barker">
              Shannon Barker
            </Link>
          </strong>
          <br />
          Administrator
        </li>
      </ul>
    </StyledAttorneys>
  )
}

const StyledAttorneys = styled("div")`
  width: 100%;
  padding: 2rem;
  height: auto;
  background-color: ${({ theme }) => theme.colors.secondary};

  ul {
    list-style: none;
    margin-bottom: 1rem;
    margin-left: 0;
  }

  .title {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2rem;
    text-align: center;
    margin-bottom: 1rem;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }
  }

  .subtext {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 1rem;
    text-align: center;
    width: calc(100% + 4rem);
    padding: 1rem 0;
    margin-left: -2rem;
    border-top: medium double ${({ theme }) => theme.colors.grey};
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};
    font-size: 1.5rem;

    a:visited {
      color: ${({ theme }) => theme.links.hoverBlue};
    }

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  li {
    font-size: 1.2rem;
    color: ${({ theme }) => theme.colors.grey};
    margin-bottom: 1rem;
    text-align: center;

    &:last-child {
      margin-bottom: 0;
    }
  }
`
