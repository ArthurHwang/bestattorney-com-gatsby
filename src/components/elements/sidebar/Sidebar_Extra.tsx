import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  content: any
}

export const SidebarExtra: React.FC<Props> = ({ content }): ReactElement => {
  return (
    content.title && (
      <StyledExtraSidebar>
        <p>{content.title}</p>
        <div
          id="inject-content"
          dangerouslySetInnerHTML={{ __html: content.HTML }}
        />
      </StyledExtraSidebar>
    )
  )
}

const StyledExtraSidebar = styled("div")`
  #inject-content {
    padding: 0 2rem 2rem;
  }
  p {
    padding: 2rem;
    padding-bottom: 1.5rem;
    margin-bottom: 1rem;
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.accent};
    text-transform: uppercase;
    font-style: italic;
  }

  div {
    padding: 0 2rem 0.1rem 1rem;
    color: ${({ theme }) => theme.colors.primary};
  }

  ul {
    list-style-type: none;
    margin: 0;
    list-style-position: inside;
  }

  li {
    font-size: 1.2rem;
    margin-bottom: 0.3rem;
    margin-left: 45px;
    text-indent: -20px;
    text-align: left;
    padding-right: 2rem;
    background: ${({ theme }) => theme.colors.secondary};

    &:before {
      content: "•";
      font-size: 1.6rem;
      color: ${({ theme }) => theme.colors.accent};
      display: inline-block;
      width: 1em;
    }
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  strong {
    color: ${({ theme }) => theme.colors.accent};
    font-weight: 600;
    transition: all 0.3s;

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  width: 100%;
  background-color: ${({ theme }) => theme.colors.secondary};
`
