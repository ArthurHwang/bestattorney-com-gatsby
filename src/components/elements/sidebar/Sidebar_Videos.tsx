import React, { ReactElement } from "react"
import LazyLoad from "react-lazyload"
import styled from "styled-components"

interface Props {
  content: Array<{ videoName: string; videoUrl: string }>
}

export const SidebarVideos: React.FC<Props> = ({ content }): ReactElement => {
  return content[0].videoName ? (
    <LazyLoad offset={2500} height={579}>
      <StyledSidebarVideos>
        <p>
          <span>Bisnar Chase</span> Videos
        </p>
        {content.map((video, index: number) => (
          <div key={index} className="video-container">
            <iframe
              width="100%"
              height="220"
              src={`https://www.youtube.com/embed/${video.videoUrl}`}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            <StyledVideoText>{video.videoName}</StyledVideoText>
          </div>
        ))}
      </StyledSidebarVideos>
    </LazyLoad>
  ) : (
    <LazyLoad height={579} offset={1000}>
      <StyledSidebarVideos>
        <p>
          <span>Bisnar Chase</span> Videos
        </p>
        <div className="video-container">
          <iframe
            width="100%"
            height="220"
            src={`https://www.youtube.com/embed/wmv1HKnhW7U`}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
          <StyledVideoText>Bisnar Chase Brand Video</StyledVideoText>
        </div>
        <div className="video-container">
          <iframe
            width="100%"
            height="220"
            src={`https://www.youtube.com/embed/jKy3wa5M2yM`}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
          <StyledVideoText>Brian Chase on American Law TV</StyledVideoText>
        </div>
      </StyledSidebarVideos>
    </LazyLoad>
  )
}

const StyledVideoText = styled("div")`
  color: ${({ theme }) => theme.colors.primary};
  background: grey;
  width: 100%;
  font-size: 1.2rem;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  font-weight: 600;
  bottom: -30px;
`

const StyledSidebarVideos = styled("div")`
  width: 100%;
  background-color: ${({ theme }) => theme.colors.secondary};

  .video-container {
    position: relative;
    margin-bottom: 4rem;
  }

  iframe {
    margin-bottom: 0;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    text-align: center;
    text-transform: uppercase;
    padding: 2rem;
    padding-bottom: 1.5rem;
    margin-bottom: 0;
    font-size: 2rem;
    border-bottom: medium double grey;
  }

  span {
    font-weight: 600;
    font-style: italic;
    color: ${({ theme }) => theme.colors.accent};
  }
`
