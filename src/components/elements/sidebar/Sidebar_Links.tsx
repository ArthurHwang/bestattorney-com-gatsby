import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import { FaRegArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { DefaultOrangeButton, DefaultGreyButton } from "../../utilities"

interface Props {
  content: any
}

export const SidebarLinks: React.FC<Props> = ({ content }): ReactElement => {
  return (
    content.title && (
      <StyledSidebarLinks>
        <p className="title-text">{content.title}</p>
        <ul className="title-list">
          {content.links.map((listItem: any, index: any) => {
            return (
              <li key={index}>
                <Link to={listItem.linkURL}>
                  <DefaultGreyButton>
                    {listItem.linkName}{" "}
                    <FaRegArrowAltCircleRight className="arrow-right" />
                  </DefaultGreyButton>
                </Link>
              </li>
            )
          })}
        </ul>

        <div className="buttons">
          <p>
            <Link to="/about-us/testimonials">
              <DefaultOrangeButton>
                Testimonials & Reviews
                <FaRegArrowAltCircleRight className="arrow-right" />
              </DefaultOrangeButton>
            </Link>
          </p>
          <p>
            <Link to="/case-results">
              <DefaultOrangeButton>
                Case Results & Success Stories
                <FaRegArrowAltCircleRight className="arrow-right" />
              </DefaultOrangeButton>
            </Link>
          </p>
          <p>
            <Link to="/about-us">
              <DefaultOrangeButton>
                The Bisnar Chase Difference
                <FaRegArrowAltCircleRight className="arrow-right" />
              </DefaultOrangeButton>
            </Link>
          </p>
        </div>
      </StyledSidebarLinks>
    )
  )
}

const StyledSidebarLinks = styled("div")`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding-bottom: 2rem;
  padding-top: 2rem;
  width: 100%;

  ul {
    margin: 0;
    padding: 0 2rem;
    list-style-type: none;
  }

  li {
    margin-bottom: 1rem;

    &:last-child {
      margin-bottom: 2rem;
    }
  }

  .title-text {
    color: ${({ theme }) => theme.colors.primary};
    text-align: center;
    text-transform: uppercase;
    font-size: 2rem;
    border-bottom: medium double grey;
    padding-bottom: 1.5rem;
    padding-left: 2rem;
    padding-right: 2rem;
  }

  button {
    width: 100%;
    text-align: left;
    font-size: 1.2rem;
    height: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-transform: uppercase;
    font-weight: 600;
    padding: 0.7rem;
    background-color: #bdbdbd;
  }

  .buttons {
    border-top: medium double grey;
    padding: 2rem 2rem 0 2rem;
    button {
      background: ${({ theme }) => theme.colors.accent};
    }

    p:last-child {
      margin-bottom: 0;
    }
  }

  .arrow-right {
    font-size: 1.4rem;
  }
`
