import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  content: any
}

export const SidebarHead: React.FC<Props> = ({ content }): ReactElement => {
  return (
    content.textHeading && (
      <StyledSidebarHead>
        <p className="title-text">{content.textHeading}</p>
        <ul className="title-list">
          {content.textBody.map((listItem: any, index: any) => {
            if (listItem.includes("</a>")) {
              return (
                <li
                  key={index}
                  dangerouslySetInnerHTML={{
                    __html: listItem
                  }}
                />
              )
            } else {
              return <li key={index}>{listItem}</li>
            }
          })}
        </ul>
      </StyledSidebarHead>
    )
  )
}

const StyledSidebarHead = styled("div")`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding-bottom: 2rem;
  padding-top: 2rem;
  width: 100%;

  a {
    color: ${({ theme }) => theme.links.hoverBlue};
    font-size: 1.2rem;

    a:visited {
      color: ${({ theme }) => theme.links.hoverBlue};
    }

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  p,
  li {
    color: ${({ theme }) => theme.colors.primary};
    text-align: left;
  }

  ul {
    margin: 0;
    padding: 0 3rem;
    list-style-type: none;
  }

  li {
    margin-bottom: 1rem;
    font-size: 1.2rem;

    &:before {
      content: "•";
      font-size: 1.6rem;
      color: ${({ theme }) => theme.colors.accent};
      display: inline-block;
      width: 1em;
      margin-left: -1em;
    }
  }

  p {
    padding: 0 2rem;
  }

  .title-text {
    text-align: center;
    text-transform: uppercase;
    font-size: 2rem;
    border-bottom: medium double grey;
    padding-bottom: 1.5rem;
  }

  button {
    width: 100%;
    text-align: left;
    font-size: 1.2rem;
    height: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-transform: uppercase;
    font-weight: 600;
    padding: 1rem;
  }

  .arrow-right {
    font-size: 1.4rem;
  }
`
