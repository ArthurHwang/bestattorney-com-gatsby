import React, { ReactElement } from "react"
import { IoMdStar } from "react-icons/io"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { getReviews } from "../../utilities/getReviews"

function swapReviewLink(reviewLocation: string): string | null {
  switch (reviewLocation) {
    case "Google":
      return "https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
    case "Yelp":
      return "https://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-newport-beach-4"
    case "Facebook":
      return "https://www.facebook.com/california.attorney/"
    case "Yahoo!":
      return "https://local.yahoo.com/info-45552418-bisnar-chase-personal-injury-attorneys-newport-beach?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuYmVzdGF0dG9ybmV5LmNvbS9hYm91dC11cy8&guce_referrer_sig=AQAAABa5z_CT7RczzSaegeV9hGXNTzqfP5lMEAniKibi1Esl1aNDRWUrNQjR19w1UjvbYG0r-K8xe_5Jnl5eZOYWOGXYaLi_1_AUZGpQxz39PEYBYdPKEl01CbMpPwZLJphtwTWiSEb1Hh9OMp0C5jKNiR8hUqa52LlUCQTTxMODp3e7"
    case "City Search":
      return "http://www.citysearch.com/profile/607714/newport_beach_ca/bisnar_chase_personal_injury.html"
    case "Super Pages":
      return "https://www.superpages.com/bp/newport-beach-ca/bisnar-chase-personal-injury-attorneys-L2050662459.htm"
    default:
      return null
  }
}

interface Props {
  invert?: boolean
}

// getReviews() returns 3 random reviews from case database.  See File in utilities folder

export const SidebarReviews: React.FC<Props> = ({
  invert = false
}): ReactElement => {
  return (
    <ContentWrapper invert={invert}>
      <StyledReviews>
        <p className="title-text">
          <span>Client </span>Reviews
        </p>
        {getReviews.map((review: any, index: any) => (
          <StyledReview key={index} invert={invert}>
            <p className="text-wrapper">{review.reviewExcerpts}</p>
            <div className="review-wrapper">
              <div>
                <img
                  src={review.thumbnailPath}
                  alt="Client Ratings & Reviews"
                />
              </div>
              <div className="review-body">
                <p>by {review.reviewer}</p>
                <p>
                  reviewed at{" "}
                  <Link
                    className="review-link"
                    to={swapReviewLink(review.reviewedAt)}
                    target="_blank"
                    itemProp="url"
                  >
                    {review.reviewedAt}
                  </Link>
                </p>
                <p>
                  {" "}
                  <span className="review-stars">
                    {" "}
                    {[1, 2, 3, 4, 5].map(star => (
                      <IoMdStar key={star} className="review-star" />
                    ))}{" "}
                    <span className="numeric-rating"> (5/5)</span>
                  </span>
                </p>
              </div>
            </div>
          </StyledReview>
        ))}
      </StyledReviews>
    </ContentWrapper>
  )
}

const ContentWrapper = styled("section")<{ invert: boolean }>`
  background: ${props =>
    props.invert
      ? `${props.theme.colors.primary}`
      : `${props.theme.colors.secondary}`};
`

const StyledReviews = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  padding: 2rem 0 0;
  margin: 0 auto;

  .title-text {
    text-align: center;
    text-transform: uppercase;
    font-size: 2rem;
    border-bottom: medium double grey;
    padding-bottom: 1.5rem;
    color: ${({ theme }) => theme.colors.primary};

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-style: italic;
      font-weight: 600;
    }
  }
`

const StyledReview = styled("div")<{ invert: boolean }>`
  display: flex;
  flex-direction: column;
  width: 100%;
  border-bottom: 1px solid grey;
  margin-bottom: 2rem;
  padding: 0 2rem;

  &:last-child {
    border-bottom: none;
    margin-bottom: 0;
  }

  .review-link {
    color: ${({ theme }) => theme.links.hoverBlue};
  }

  .text-wrapper {
    font-size: 1rem;
    font-style: italic;
    color: ${props =>
      props.invert
        ? `${props.theme.colors.secondary}`
        : `${props.theme.colors.grey}`};
  }

  .review-wrapper {
    display: grid;
    grid-template-columns: 80px 1fr;

    .review-stars {
      position: relative;
      top: 3px;

      .numeric-rating {
        position: relative;
        bottom: 3px;
      }
    }

    .review-star {
      color: ${({ theme }) => theme.colors.accent};
      font-size: 1.4rem;
    }

    p {
      color: ${props =>
        props.invert
          ? `${props.theme.colors.secondary}`
          : `${props.theme.colors.primary}`};
      margin: 0;
      font-size: 1.1rem;
    }
  }
`
