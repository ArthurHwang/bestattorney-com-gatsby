import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"

export const SidebarAchievements: React.FC = (): ReactElement => {
  return (
    <StyledAchievementsSidebar>
      <p>
        AWARDS <span>&</span> HONORS
      </p>

      <p className="subtext">BRIAN CHASE'S ACCOMPLISHMENTS</p>
      <ul className="xspace">
        <li>2019 Top 1% by the Natl. Assoc. of Distinguished Counsel</li>
        <li>2019 Million Dollar Advocate</li>
        <li>2019 Lawyer of the Year</li>
        <li>2019 America’s Top 100 Attorneys</li>
        <li>2017 Top 1% by the Natl. Assoc. of Distinguished Counsel</li>
        <li> 2017 Top 100 High Stakes Litigator</li>
        <li>2016 Top 10% Lawyers of Distinction</li>
        <li>2016 SuperLawyer</li>
        <li>2016 Best Lawyers</li>
        <li>
          2015{" "}
          <Link
            to="https://www.caoc.org/index.cfm?pg=CAOC-Board"
            target="_blank"
          >
            CAOC
          </Link>{" "}
          President
        </li>
        <li>2015 NADC Top One Percent Lawyers</li>
        <li>
          2014, 2004 OCTLA Trial Lawyer of the Year for Products Liability{" "}
          <Link to="/press-releases/brian-chase-award" target="new">
            OCTLA
          </Link>
        </li>
        <li>2013 Top Attorney in O.C. Award</li>
        <li>
          2012 Trial Attorney of the Year,{" "}
          <Link to="/blog/chase-ritsema-attorney-award" target="new">
            CAOC
          </Link>
        </li>
        <li>
          2012 Trial Lawyer of the Year Nominee,{" "}
          <Link to="/blog/brian-chase-lawyer-of-the-year" target="new">
            CAALA
          </Link>
        </li>
        <li>2007 President of OCTLA</li>
        <li>California Supreme Court and Appellate Court case winner</li>
        <li>Top 100 Trial Lawyers in America, ATLA - Since 2007</li>
        <li>Past President of Orange County Trial Lawyers Assoc.</li>
        <li>
          Top 50 Orange County Lawyers,{" "}
          <Link
            to="http://www.superlawyers.com/california-southern/lawyer/Brian-D-Chase/eeb57498-819c-48b6-816d-b41187d50a28.html"
            target="new"
          >
            SuperLawyers
          </Link>
        </li>
        <li>
          Author,{" "}
          <Link to="/resources/book-order-form">
            'Still Unsafe At Any Speed'
          </Link>
        </li>
        <li>
          Author,{" "}
          <Link to="/blog/brian-chase-new-book">'The Next Big Thing'</Link>
        </li>
        <li>Author of numerous legal publication articles</li>
        <li>National Speaker on Personal Injury and Product Defect Law</li>
        <li>
          Member{" "}
          <Link to="https://www.abota.org/index.cfm?pg=Mission" target="new">
            American Board of Trial Advocates
          </Link>
        </li>
        <li>Numerous television and radio news program interviews</li>
      </ul>
    </StyledAchievementsSidebar>
  )
}

const StyledAchievementsSidebar = styled("div")`
  padding: 2rem 3rem;
  background-color: ${({ theme }) => theme.colors.secondary};
  color: ${({ theme }) => theme.colors.grey};

  p {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 2rem;
    text-align: center;
    margin-bottom: 1rem;
  }

  ul {
    margin: 0;
  }

  .subtext {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 1rem;
    text-align: center;
    width: calc(100% + 6rem);
    padding: 1rem 0;
    margin-left: -3rem;
    border-top: medium double ${({ theme }) => theme.colors.grey};
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
  }

  span {
    color: ${({ theme }) => theme.colors.accent};
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  li {
    font-size: 1.2rem;
    margin-bottom: 0.3rem;
  }
`
