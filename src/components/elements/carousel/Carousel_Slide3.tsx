import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import LazyLoad from "react-lazyload"
import styled from "styled-components"
import NAOPIABadge from "src/images/logo/NAOPIA-top-ten.png"
import { IoIosApps, IoIosArrowDroprightCircle, IoMdCall } from "react-icons/io"
import { AwardsButton, CallButton } from "../../utilities"

interface Props {
  video: string
  videoEncoded: string
  videoWebm: string
}

export const Slide3: React.FC<Props> = ({
  video,
  videoEncoded,
  videoWebm
}): ReactElement => {
  return (
    <StyledSlide3>
      <ContentWrapper>
        <OpacityWrapper />
        <TextWrapper>
          <VerticalCenterWrapper>
            <span className="slide-title">
              The Bisnar
              <br />
              Chase Difference
            </span>
            <span className="slide-subtitle">
              Brian Chase Wakes Up Every Day with One Purpose
            </span>
            <div className="badges">
              <a href="http://www.naopia.com/attorneys/chase-brian-d">
                <img alt="NAOPIA Top Ten" src={NAOPIABadge} />
              </a>
            </div>
            <p className="slide-text">
              <span>
                Brian Chase has spent decades defending and fighting for injured
                plaintiffs that are entitled to maximum compensation. Our
                mission is to provide superior client representation in a
                compassionate and professional manner.
              </span>
              <Link className="slide-link" to="/attorneys/brian-chase">
                MEET ATTORNEY BRIAN CHASE
                <IoIosArrowDroprightCircle className="arrow" />
              </Link>
            </p>
            <ButtonWrapper>
              <Link to="/contact">
                <CallButton className="call-button btn">
                  Call Us Today
                  <IoMdCall className="btn-icon call" />
                </CallButton>
              </Link>
              <Link to="/about-us/lawyer-reviews-ratings">
                <AwardsButton className="awards-button btn">
                  See All Awards
                  <IoIosApps className="btn-icon awards" />
                </AwardsButton>
              </Link>
            </ButtonWrapper>
          </VerticalCenterWrapper>
        </TextWrapper>
      </ContentWrapper>
      <LazyLoad>
        <video
          title="Brian Chase, Trial Lawyer in Pursuit of Justice"
          muted={true}
          autoPlay={true}
          loop={true}
          preload="auto"
        >
          <source src={videoEncoded} type="video/mp4" />
          <source src={video} type="video/mp4" />
          <source src={videoWebm} type="video/webm" />
        </video>
      </LazyLoad>
    </StyledSlide3>
  )
}

const VerticalCenterWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: calc(100vh - 115px);
`

/****************************/
/************Slide***********/
/****************************/

const StyledSlide3 = styled("div")`
  height: calc(100vh - 115px);
  width: 100vw;
  overflow: hidden;

  video {
    position: relative;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    object-position: 58% 50%;
    object-fit: cover;
  }
`

/****************************/
/*******Content Wrapper******/
/****************************/

const ContentWrapper = styled("div")`
  position: relative;
  width: calc(100vw / 2);
`

/****************************/
/*******Opacity Wrapper******/
/****************************/

const OpacityWrapper = styled("div")`
  position: absolute;
  width: 52rem;
  height: calc(100vh - 115px);
  z-index: 1;
  background: #000;
  opacity: 0.7;
  left: 0;
  right: 0;
  margin: auto;
`

/****************************/
/*******Button Wrapper*******/
/****************************/

const ButtonWrapper = styled("div")`
  margin-top: 1rem;

  @media (max-height: 900px) {
    margin-top: 0;
  }
`

/**************************/
/*******Text Wrapper*******/
/**************************/

const TextWrapper = styled("div")`
  position: absolute;
  height: calc(100vh - 115px);
  width: 52rem;
  z-index: 1;
  animation: moveInLeft 3s;
  left: 0;
  right: 0;
  margin: auto;

  /**************************/
  /*******Badge Section******/
  /**************************/

  .badges {
    text-align: center;
    margin-bottom: 1rem;
    animation-name: moveInRight;
    animation-duration: 3s;

    @media (max-height: 900px) {
      margin-bottom: 0;
    }

    img {
      width: 185px;
      display: inline-block;
      margin: 0;
    }
  }

  /****************************/
  /*******Buttons Section******/
  /****************************/

  .call-button {
    padding-left: 2.5rem;
  }

  .btn {
    display: block;
    margin: 2.5rem auto;
    width: 23rem;
    height: 4rem;

    .btn-icon {
      vertical-align: -0.25rem;
      margin-left: 1.3rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  /**************************/
  /*******Text Areas*********/
  /**************************/

  .slide-title {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    max-width: 575px;
    margin: 0 auto 1rem;
    line-height: 1;
    text-align: center;
    text-shadow: 1.3px 1.3px 0 #000;

    font-size: 5.3rem;
    font-variant: all-small-caps;
  }

  .slide-subtitle {
    color: ${({ theme }) => theme.colors.accent};
    display: block;
    font-size: 1.4rem;
    font-weight: 700;
    text-align: center;
    text-shadow: 1px 1px 0 #000;
    text-transform: uppercase;
    margin-top: -0.5rem;
    margin-bottom: 3.5rem;

    @media (max-height: 900px) {
      margin-bottom: 1rem;
    }
  }

  .slide-text {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 1.5rem;
    line-height: 1.7;
    margin: 1rem auto 0;
    padding: 0 5.5rem;
    text-shadow: 1.5px 1.5px 0 #000;

    span {
      word-spacing: 2px;
    }

    @media (max-height: 900px) {
      margin-top: 2rem;
    }

    .arrow {
      vertical-align: -0.15rem;
      margin-left: 0.5rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  .slide-link {
    display: block;
    font-size: 1.2rem;
    margin-top: 1rem;
    font-weight: 600;
    text-align: center;
    color: ${({ theme }) => theme.colors.accent};

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  /***************************/
  /****Animation Keyframes****/
  /***************************/

  @keyframes moveInLeft {
    0% {
      opacity: 0;
      transform: translateX(-100px);
    }

    80% {
      transform: translateX(20px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }

  @keyframes moveInRight {
    0% {
      opacity: 0;
      transform: translateX(200px);
    }

    80% {
      transform: translateX(-50px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }
`
