import { Link } from "src/components/elements/Link"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import AmericasTopBadge from "src/images/logo/americas-top-100-min.png"
import NationsTopBadge from "src/images/logo/badge-2018-min.png"
import LawyerOfTheYearBadge from "src/images/logo/brian-chase-lawyer-of-the-year-2019-min.png"
import { IoIosApps, IoIosArrowDroprightCircle, IoMdCall } from "react-icons/io"
import { AwardsButton, CallButton } from "../../utilities"

interface Props {
  img: {
    base64: string
    aspectRatio: number
    src: string
    srcSet: string
    sizes: string
  }
}

export const Slide1: React.FC<Props> = ({ img }): ReactElement => {
  return (
    <StyledSlide1>
      <ContentWrapper>
        <OpacityWrapper />
        <TextWrapper>
          <VerticalCenterWrapper>
            <span className="slide-title">
              Nations' Top <span>1%</span>
            </span>
            <span className="slide-subtitle">
              of attorneys as selected by the NADC for 2018
            </span>
            <div className="badges">
              <a href="https://www.distinguishedcounsel.org/members/brian-chase/5241/">
                <img alt="Nations Top One Percent" src={NationsTopBadge} />
              </a>
              <a href="https://www.bestlawyers.com/lawyers/brian-doster-chase/156673">
                <img
                  alt="Lawyer of The Year"
                  className="best-lawyers"
                  src={LawyerOfTheYearBadge}
                />
              </a>
              <a href="https://www.bestlawyers.com/lawyers/brian-doster-chase/156673">
                <img alt="Americas Top 100" src={AmericasTopBadge} />
              </a>
            </div>
            <p className="slide-text">
              <span>
                Brian Chase has dedicated his career to helping injured people
                obtain justice. As former CAOC President and Trial Lawyer of the
                Year, Brian works tirelessly to help create awareness about
                product safety. It is his honor to be named the Nation’s Top 1%
                of attorneys by the National Association of Distinguished
                Counsel for 2019.
              </span>
              <Link className="slide-link" to="/attorneys/brian-chase">
                MEET ATTORNEY BRIAN CHASE
                <IoIosArrowDroprightCircle className="arrow" />
              </Link>
            </p>
            <ButtonWrapper>
              <Link to="/contact">
                <CallButton className="call-button btn">
                  Call Us Today
                  <IoMdCall className="btn-icon call" />
                </CallButton>
              </Link>
              <Link to="/about-us/lawyer-reviews-ratings">
                <AwardsButton className="awards-button btn">
                  See All Awards
                  <IoIosApps className="btn-icon awards" />
                </AwardsButton>
              </Link>
            </ButtonWrapper>
          </VerticalCenterWrapper>
        </TextWrapper>
      </ContentWrapper>
      <Img alt="Injury Attorneys in California" fluid={img} loading="eager" />
    </StyledSlide1>
  )
}

const VerticalCenterWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: calc(100vh - 115px);
`

/****************************/
/************Slide***********/
/****************************/

const StyledSlide1 = styled("div")`
  height: calc(100vh - 115px);

  .gatsby-image-wrapper {
    img {
      object-position: center 42% !important;

      @media (max-width: 1025px) {
        object-position: center center !important;
      }

      @media (min-width: 1910px) {
        object-position: center 25% !important;
      }
    }
  }
`

/****************************/
/*******Content Wrapper******/
/****************************/

const ContentWrapper = styled("div")`
  position: relative;
  width: calc(100vw / 2);
`

/****************************/
/*******Opacity Wrapper******/
/****************************/

const OpacityWrapper = styled("div")`
  position: absolute;
  width: 52rem;
  height: calc(100vh - 115px);
  z-index: 1;
  background: #000;
  opacity: 0.7;
  left: 0;
  right: 0;
  margin: auto;
`

/****************************/
/*******Button Wrapper*******/
/****************************/

const ButtonWrapper = styled("div")`
  margin-top: 1rem;

  @media (max-height: 900px) {
    margin-top: 0;
  }
`

/**************************/
/*******Text Wrapper*******/
/**************************/

const TextWrapper = styled("div")`
  position: absolute;
  height: calc(100vh - 115px);
  width: 52rem;
  z-index: 1;
  animation: moveInLeft 3s;
  left: 0;
  right: 0;
  margin: auto;

  /**************************/
  /*******Badge Section******/
  /**************************/

  .badges {
    max-width: 575px;
    text-align: center;
    margin: 0 auto 0;
    display: flex;
    align-content: space-evenly;
    animation: moveInRight 3s;

    img {
      width: 135px;
      display: inline-block;
      margin: 0 2rem;
    }

    .best-lawyers {
      width: 125px;
    }
  }

  /****************************/
  /*******Buttons Section******/
  /****************************/

  .call-button {
    padding-left: 2.5rem;
  }

  .btn {
    display: block;
    margin: 2.5rem auto;
    width: 23rem;
    height: 4rem;

    .btn-icon {
      vertical-align: -0.25rem;
      margin-left: 1.3rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  /**************************/
  /*******Text Areas*********/
  /**************************/

  .slide-title {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 6rem;
    text-align: center;
    font-variant: all-small-caps;
    text-shadow: 1.5px 1.5px 0 #000;

    span {
      font-size: 4.4rem;
    }
  }

  .slide-subtitle {
    color: ${({ theme }) => theme.colors.accent};
    display: block;
    font-size: 1.4rem;
    font-weight: 700;
    margin-bottom: 3.5rem;
    margin-top: -1rem;
    padding: 0 2rem;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1.5px 1.5px 0 #000;

    @media (max-height: 900px) {
      margin-bottom: 2rem;
    }
  }

  .slide-text {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 1.5rem;
    line-height: 1.7;
    margin: 2.5rem auto 0;
    padding: 0 5.5rem;
    text-shadow: 1.5px 1.5px 0 #000;

    span {
      word-spacing: 7.06px;
    }

    .arrow {
      vertical-align: -0.15rem;
      margin-left: 0.5rem;
      color: ${({ theme }) => theme.colors.accent};
    }

    @media (max-height: 900px) {
      margin-top: 2rem;
    }
  }

  .slide-link {
    text-align: center;
    display: block;
    font-size: 1.2rem;
    font-weight: 600;
    margin-top: 1rem;
    color: ${({ theme }) => theme.colors.accent};

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  /***************************/
  /****Animation Keyframes****/
  /***************************/

  @keyframes moveInLeft {
    0% {
      opacity: 0;
      transform: translateX(-100px);
    }

    80% {
      transform: translateX(20px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }

  @keyframes moveInRight {
    0% {
      opacity: 0;
      transform: translateX(200px);
    }

    80% {
      transform: translateX(-50px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }
`
