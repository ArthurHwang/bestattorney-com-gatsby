import { Link } from "src/components/elements/Link"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import DistinguishedBadge from "src/images/logo/distinguished-justice-advocates.png"
import { IoIosApps, IoIosArrowDroprightCircle, IoMdCall } from "react-icons/io"
import { AwardsButton, CallButton } from "../../utilities"

interface Props {
  img: {
    base64: string
    aspectRatio: number
    src: string
    srcSet: string
    sizes: string
  }
}

export const Slide4: React.FC<Props> = ({ img }): ReactElement => {
  return (
    <StyledSlide4>
      <OpacityWrapper />
      <TextWrapper>
        <VerticalCenterWrapper>
          <span className="slide-title">No Win, No Fee Guarantee</span>
          <span className="slide-subtitle">
            WE ADVANCE ALL COSTS, IF WE DON'T WIN, YOU DON'T PAY
          </span>
          <div className="badges">
            <a href="https://distinguishedjusticeadvocates.com/listing/attorney-brian-chase/">
              <img alt="Top 1% Attorneys in America" src={DistinguishedBadge} />
            </a>
          </div>
          <div className="slide-text">
            <ul className="awards-list">
              <li>
                2019 <span className="dash">-</span> Lawyer of the Year
              </li>
              <li>
                2019 <span className="dash">-</span> Million Dollar Advocate
              </li>
              <li>
                2019 <span className="dash">-</span> America's Top 100 Attorneys
              </li>
              <li>
                2018 <span className="dash">-</span> Top 1% by the Natl. Assoc.
                of Distinguished Counsel
              </li>
              <li>
                2017 <span className="dash">-</span> Top 100 High Stakes
                Litigator
              </li>
              <li>
                2016 <span className="dash">-</span> Top 10% Lawyers of
                Distinction
              </li>
              <li>
                2015 <span className="dash">-</span> CAOC President
              </li>
            </ul>
            <Link
              className="slide-link"
              to={"/about-us/no-fee-guarantee-lawyer"}
            >
              LEARN MORE ABOUT OUR NO FEE GUARANTEE
              <IoIosArrowDroprightCircle className="arrow" />
            </Link>
          </div>
          <ButtonWrapper>
            <Link tp="/contact">
              <CallButton className="call-button btn">
                Call Us Today
                <IoMdCall className="btn-icon call" />
              </CallButton>
            </Link>
            <Link to="/about-us/lawyer-reviews-ratings">
              <AwardsButton className="awards-button btn">
                Why Hire Us
                <IoIosApps className="btn-icon awards" />
              </AwardsButton>
            </Link>
          </ButtonWrapper>
        </VerticalCenterWrapper>
      </TextWrapper>
      <Img
        alt="California Personal Injury Law Firm of Bisnar Chase"
        fluid={img}
      />
    </StyledSlide4>
  )
}

const VerticalCenterWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: calc(100vh - 115px);
`

/****************************/
/************Slide***********/
/****************************/

const StyledSlide4 = styled("div")`
  height: calc(100vh - 115px);
  position: relative;
`

/****************************/
/*******Opacity Wrapper******/
/****************************/

const OpacityWrapper = styled("div")`
  background-color: #000;
  height: calc(100vh - 115px);
  left: 0;
  margin: auto;
  opacity: 0.7;
  position: absolute;
  right: 0;
  text-align: center;
  width: 52rem;
  z-index: 1;
`

/****************************/
/*******Button Wrapper*******/
/****************************/

const ButtonWrapper = styled("div")`
  @media (max-height: 900px) {
    margin-top: 0;
  }
`

/**************************/
/*******Text Wrapper*******/
/**************************/

const TextWrapper = styled("div")`
  animation: moveInLeft 3s;
  height: calc(100vh - 115px);
  position: absolute;
  width: 100vw;
  z-index: 1;

  /**************************/
  /*******Badge Section******/
  /**************************/

  .badges {
    text-align: center;
    margin-bottom: 1.5rem;
    animation: moveInRight 3s;

    @media (max-height: 900px) {
      margin-bottom: 0;
    }

    img {
      width: 140px;
      display: inline-block;
      margin: 0;
    }
  }

  /****************************/
  /*******Buttons Section******/
  /****************************/

  .call-button {
    padding-left: 2.5rem;
  }

  .btn {
    display: block;
    height: 4rem;
    margin: 2.5rem auto;
    width: 23rem;

    @media (max-height: 900px) {
      margin: 1.5rem auto;
    }

    .btn-icon {
      vertical-align: -0.25rem;
      color: ${({ theme }) => theme.colors.accent};
    }

    .awards {
      position: relative;
      left: 25px;
    }

    .call {
      position: relative;
      left: 15px;
    }
  }

  /**************************/
  /*******Text Areas*********/
  /**************************/

  .slide-title {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 5.3rem;
    font-variant: all-small-caps;
    line-height: 1;
    margin: 0 auto 1rem;
    max-width: 575px;
    text-align: center;
    text-shadow: 1.3px 1.3px 0 #000;
  }

  .slide-subtitle {
    color: ${({ theme }) => theme.colors.accent};
    display: block;
    font-size: 1.4rem;
    font-weight: 700;
    text-align: center;
    text-shadow: 1px 1px 0 #000;
    text-transform: uppercase;
    margin-top: -0.5rem;
    margin-bottom: 2.5rem;

    @media (max-height: 900px) {
      margin-bottom: 1rem;
    }
  }

  .slide-text {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 1.5rem;
    line-height: 2;
    margin: 0 auto 0;
    max-width: 625px;
    text-shadow: 1.5px 1.5px 0 #000;

    .awards-list {
      list-style-type: none;
      margin: 0;
      line-height: 1.5;

      li {
        font-size: 1.5rem;
        text-align: center;
      }
      .dash {
        color: ${({ theme }) => theme.colors.accent};
        font-weight: 600;
        font-size: 1.8rem;
      }
    }

    .arrow {
      vertical-align: -0.15rem;
      margin-left: 0.5rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  .slide-link {
    display: block;
    text-align: center;
    font-size: 1.2rem;
    margin-top: 1rem;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.accent};

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  /***************************/
  /****Animation Keyframes****/
  /***************************/

  @keyframes moveInLeft {
    0% {
      opacity: 0;
      transform: translateX(-100px);
    }

    80% {
      transform: translateX(20px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }

  @keyframes moveInRight {
    0% {
      opacity: 0;
      transform: translateX(200px);
    }

    80% {
      transform: translateX(-50px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }
`
