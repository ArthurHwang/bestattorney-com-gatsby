import { Link } from "src/components/elements/Link"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { IoIosApps, IoIosArrowDroprightCircle, IoMdCall } from "react-icons/io"
import { AwardsButton, CallButton } from "../../utilities"

interface Props {
  img: {
    base64: string
    aspectRatio: number
    src: string
    srcSet: string
    sizes: string
  }
}

export const Slide2: React.FC<Props> = ({ img }): ReactElement => {
  return (
    <StyledSlide2>
      <OpacityWrapper />
      <TextWrapper>
        <span className="slide-title">No Win, No Fee Guarantee</span>
        <span className="slide-subtitle">
          OVER $500M WON, 96% SUCCESS RATE AND SUPERIOR CLIENT REPRESENTATION
          FOR OVER 40 YEARS
        </span>
        <p className="slide-text">
          <span>
            You can obtain our legal services without any money up front or out
            of your pocket. We guarantee you that no matter the outcome of your
            injury case, you will not pay any attorney's fees or costs if your
            case is not successful
          </span>
          <Link className="slide-link" to="/about-us/no-fee-guarantee-lawyer">
            LEARN MORE ABOUT OUR NO FEE GUARANTEE
            <IoIosArrowDroprightCircle className="arrow" />
          </Link>
        </p>
        <ButtonWrapper>
          <Link to="/contact">
            <CallButton className="call-button btn">
              Call Us Today
              <IoMdCall className="btn-icon call" />
            </CallButton>
          </Link>
          <Link to="/about-us/lawyer-reviews-ratings">
            <AwardsButton className="awards-button btn">
              See All Awards
              <IoIosApps className="btn-icon awards" />
            </AwardsButton>
          </Link>
        </ButtonWrapper>
      </TextWrapper>
      <Img
        alt="California Personal Injury Law Firm"
        imgStyle={{ objectPosition: "50% 38%" }}
        fluid={img}
      />
    </StyledSlide2>
  )
}

/****************************/
/************Slide***********/
/****************************/

const StyledSlide2 = styled("div")`
  height: calc(100vh - 115px);

  .gatsby-image-wrapper {
    img {
      @media (min-width: 1910px) {
      }
    }
  }
`

/****************************/
/*******Opacity Wrapper******/
/****************************/

const OpacityWrapper = styled("div")`
  position: absolute;
  width: 100vw;
  height: calc(100vh / 2.5);
  z-index: 1;
  background-image: linear-gradient(to top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));
`

/****************************/
/*******Button Wrapper*******/
/****************************/

const ButtonWrapper = styled("div")`
  position: absolute;
  bottom: 3rem;
  width: 100vw;
  text-align: center;
`
/**************************/
/*******Text Wrapper*******/
/**************************/

const TextWrapper = styled("div")`
  position: absolute;
  height: calc(100vh - 115px);
  width: 100vw;
  z-index: 1;
  animation: moveInTop 3s;

  /**************************/
  /*******Badge Section******/
  /**************************/

  .nations-top-badge {
    text-align: center;

    img {
      width: 150px;
      display: inline-block;
      margin: 0;
    }
  }

  /****************************/
  /*******Buttons Section******/
  /****************************/

  .call-button {
    padding-left: 2.5rem;
  }

  .btn {
    display: inline-block;
    margin: 0 12rem;
    width: 21rem;
    height: 4rem;

    .btn-icon {
      vertical-align: -0.25rem;
      margin-left: 1.3rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  /**************************/
  /*******Text Areas*********/
  /**************************/

  .slide-title {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 5.3rem;
    font-variant: all-small-caps;
    text-align: center;
    text-shadow: 1.3px 1.3px 0 #000;
  }

  .slide-subtitle {
    color: ${({ theme }) => theme.colors.accent};
    display: block;
    font-size: 1.4rem;
    font-weight: 700;
    text-align: center;
    text-shadow: 1px 1px 0 #000;
    text-transform: uppercase;
    margin-top: -1rem;
    margin-bottom: 2.5rem;
  }

  .slide-text {
    color: ${({ theme }) => theme.colors.primary};
    display: block;
    font-size: 1.5rem;
    line-height: 1.7;
    margin: -1.5rem auto 0;
    max-width: 696px;
    text-align: center;
    text-shadow: 1.5px 1.5px 0 #000;

    .arrow {
      vertical-align: -0.15rem;
      margin-left: 0.5rem;
      color: ${({ theme }) => theme.colors.accent};
    }
  }

  .slide-link {
    display: block;
    font-size: 1.2rem;
    margin-top: 0.5rem;
    text-align: center;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.accent};

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  /***************************/
  /****Animation Keyframes****/
  /***************************/

  @keyframes moveInTop {
    0% {
      opacity: 0;
      transform: translateY(-100px);
    }

    80% {
      transform: translateY(10px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }
`
