import { Location } from "@reach/router"
import { graphql, StaticQuery } from "gatsby"
import React, { ReactElement } from "react"
import Helmet from "react-helmet"
import favicon from "src/images/favicon.ico"
import schemaGenerator from "../utilities/schemaGenerator"

interface Props {
  siteTitle?: string
  siteDescription?: string
  siteUrl?: string
  pageTitle: string
  pageDescription: string
  pageTitleFull?: string
  social?: any
  imageUrl?: string | null
  location?: any
  canonical?: string
  isBlog?: boolean
  isAmp?: boolean
  pageSubject?: string
}

const Head: React.FC<Props> = ({
  siteTitle,
  siteDescription,
  siteUrl,
  pageTitle,
  pageDescription,
  pageTitleFull = pageTitle ? `${siteTitle}: ${pageTitle}` : siteTitle,
  social,
  imageUrl,
  location,
  pageSubject = "",
  canonical = siteUrl + (location.pathname || ""),
  isBlog = false,
  isAmp = false
}): ReactElement => {
  if (isBlog) {
    return (
      <Helmet>
        <html lang="en" />
        <link rel="icon" href={favicon} type="image/png" />
        <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
        <meta
          name="google-site-verification"
          content="2pIouHj8Sx_VsHVHyYLT_ybrm-MhfpkedLpFbNXI5Fw"
        />
        <meta name="robots" content="noindex, nofollow" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1.0,user-scalable=yes"
        />
        <title>
          {!pageTitle
            ? "California Personal Injury Blog - Bisnar Chase"
            : pageTitle + " | California Personal Injury Blog"}
        </title>
        <meta name="description" content={pageDescription || siteDescription} />
        <meta
          name="application-name"
          content={"California Personal Injury Blog"}
        />
        <link rel="canonical" href={canonical} />

        {/* FACEBOOK META */}
        <meta
          property="og:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />
        <meta
          property="og:description"
          content={pageDescription || siteDescription}
        />
        <meta property="fb:app_id" content={social.fbAppId} />
        <meta
          property="og:site_name"
          content={"California Personal Injury Blog"}
        />
        <meta property="og:url" content={canonical} />
        <meta
          property="og:image"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="og:image:secure_url"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="og:image:alt"
          content="injury attorneys in california"
        />
        <meta property="og:image:type" content="image/jpg" />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />
        <meta property="og:type" content="website" />
        {/* FACEBOOK META */}

        {/* TWITTER META */}
        <meta
          name="twitter:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />
        <meta
          name="twitter:description"
          content={pageDescription || siteDescription}
        />
        <meta name="twitter:url" content={canonical} />
        <meta
          name="twitter:image"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="twitter:image:alt"
          content="injury attorneys in california"
        />
        <meta name="twitter:image:width" content="400" />
        <meta name="twitter:image:height" content="300" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={`@bisnarchase`} />
        <meta name="twitter:creator" content={`@briandchase`} />
        <meta
          name="twitter:text:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />

        {/* TWITTER META */}

        {/* JSONLD Generator */}

        <script type="application/ld+json">
          {JSON.stringify(
            // @ts-ignore
            schemaGenerator({
              location,
              canonical,
              siteUrl,
              pageSubject,
              pageTitle,
              siteTitle,
              pageTitleFull,
              imageUrl,
              isBlog
            })
          )}
        </script>

        {/* JSONLD Generator */}
      </Helmet>
    )
  }

  if (isAmp) {
    return (
      <Helmet>
        <html lang="en" />
        <link rel="icon" href={favicon} type="image/png" />
        <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
        <meta
          name="google-site-verification"
          content="2pIouHj8Sx_VsHVHyYLT_ybrm-MhfpkedLpFbNXI5Fw"
        />
        <meta name="robots" content="noindex, nofollow" />

        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, shrink-to-fit=no"
        />

        <title>
          {!pageTitle
            ? "California Personal Injury Blog - Bisnar Chase"
            : pageTitle + " | California Personal Injury Blog"}
        </title>
        <meta name="description" content={pageDescription || siteDescription} />
        <meta
          name="application-name"
          content={"California Personal Injury Blog"}
        />
        {/* <link rel="canonical" href={canonical.replace(/\/amp/g, "")} /> */}

        {/* FACEBOOK META */}
        <meta
          property="og:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />
        <meta
          property="og:description"
          content={pageDescription || siteDescription}
        />
        <meta property="og:type" content="article" />

        <meta property="og:locale" content="en_US" />

        <meta
          property="article:publisher"
          content="https://www.facebook.com/california.attorney"
        />
        <meta
          property="article:author"
          content="https://www.facebook.com/california.attorney"
        />

        <meta property="fb:app_id" content={social.fbAppId} />
        <meta
          property="og:site_name"
          content={"California Personal Injury Blog"}
        />
        <meta property="og:url" content={canonical.replace(/\/amp/g, "")} />
        <meta
          property="og:image"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="og:image:secure_url"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="og:image:alt"
          content="injury attorneys in california"
        />
        <meta property="og:image:type" content="image/jpg" />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />
        <meta property="og:type" content="website" />
        {/* FACEBOOK META */}

        {/* TWITTER META */}
        <meta
          name="twitter:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />
        <meta
          name="twitter:description"
          content={pageDescription || siteDescription}
        />
        <meta name="twitter:url" content={canonical.replace(/\/amp/g, "")} />
        <meta
          name="twitter:image"
          content={
            imageUrl ||
            `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
          }
        />
        <meta
          property="twitter:image:alt"
          content="injury attorneys in california"
        />
        <meta name="twitter:image:width" content="400" />
        <meta name="twitter:image:height" content="300" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content={`@bisnarchase`} />
        <meta name="twitter:creator" content={`@briandchase`} />
        <meta
          name="twitter:text:title"
          content={
            !pageTitle
              ? "California Personal Injury Blog"
              : pageTitle + " | California Personal Injury Blog"
          }
        />

        {/* TWITTER META */}

        {/* JSONLD Generator */}

        <script type="application/ld+json">
          {JSON.stringify(
            // @ts-ignore
            schemaGenerator({
              location,
              canonical,
              siteUrl,
              pageSubject,
              pageTitle,
              siteTitle,
              pageTitleFull,
              imageUrl,
              isBlog,
              isAmp
            })
          )}
        </script>

        {/* JSONLD Generator */}
      </Helmet>
    )
  }

  return (
    <Helmet>
      <html lang="en" />
      <link rel="icon" href={favicon} type="image/png" />
      <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
      {/* TAKE OUT WHEN READY FOR LIVE */}
      <meta
        name="google-site-verification"
        content="2pIouHj8Sx_VsHVHyYLT_ybrm-MhfpkedLpFbNXI5Fw"
      />
      <meta name="robots" content="noindex, nofollow" />
      {/* TAKE OUT WHEN READY FOR LIVE */}
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1.0,user-scalable=yes"
      />
      <title>{pageTitle || siteTitle}</title>
      <meta name="description" content={pageDescription || siteDescription} />
      <meta name="application-name" content={siteTitle} />
      <link rel="canonical" href={canonical} />

      {/* FACEBOOK META */}
      <meta property="og:title" content={pageTitle || siteTitle} />
      <meta
        property="og:description"
        content={pageDescription || siteDescription}
      />
      <meta property="fb:app_id" content={social.fbAppId} />
      <meta property="og:site_name" content={siteTitle} />
      <meta property="og:url" content={canonical} />
      <meta
        property="og:image"
        content={
          imageUrl ||
          `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
        }
      />
      <meta
        property="og:image:secure_url"
        content={`https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`}
      />
      <meta property="og:image:alt" content="injury attorneys in california" />
      <meta property="og:image:type" content="image/jpg" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="300" />
      <meta property="og:type" content="website" />
      {/* FACEBOOK META */}

      {/* TWITTER META */}
      <meta name="twitter:title" content={pageTitle || siteTitle} />
      <meta
        name="twitter:description"
        content={pageDescription || siteDescription}
      />
      <meta name="twitter:url" content={canonical} />
      <meta
        name="twitter:image"
        content={
          imageUrl ||
          `https://bestatto-gatsby.netlify.app/images/carousel/mobile/brian-chase-john-bisnar-mobile-small.jpg`
        }
      />
      <meta
        property="twitter:image:alt"
        content="injury attorneys in california"
      />
      <meta name="twitter:image:width" content="400" />
      <meta name="twitter:image:height" content="300" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content={`@bisnarchase`} />
      <meta name="twitter:creator" content={`@briandchase`} />
      <meta name="twitter:text:title" content={pageTitle || siteTitle} />

      {/* TWITTER META */}

      {/* JSONLD Generator */}

      <script type="application/ld+json">
        {JSON.stringify(
          // @ts-ignore
          schemaGenerator({
            location,
            canonical,
            pageSubject,
            siteUrl,
            pageTitle,
            siteTitle,
            pageTitleFull,
            imageUrl
          })
        )}
      </script>

      {/* JSONLD Generator */}
    </Helmet>
  )
}

export const SEO: React.FC<Props> = props => {
  return (
    <StaticQuery
      query={graphql`
        query {
          site {
            siteMetadata {
              siteTitle
              siteDescription
              siteUrl
              social {
                twitter
                fbAppId
              }
            }
          }
        }
      `}
      render={data => (
        <Location>
          {({ location }) => (
            <Head {...data.site.siteMetadata} {...props} location={location} />
          )}
        </Location>
      )}
    />
  )
}
