import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"

export const HomeFAQ: React.FC = (): ReactElement => {
  return (
    <StyledFAQ>
      <ContentWrapper>
        <h2>Why Choose Bisnar Chase?</h2>
        <p>
          California has no shortage of attorneys practicing personal injury law
          but at Bisnar Chase we have earned a reputation as one of the premiere
          law firms in California. Our team of trial attorneys have the passion,
          drive & desire to help others and to help make our world a safer
          place. Because of cases we have fought & won, we have penalized some
          of the biggest corporations in the U.S. and stood up for the consumer
          - who otherwise may not have had a voice.
        </p>

        <h2>How Do I know I Can Be Represented?</h2>
        <p>
          If you are injured and not at-fault, you have the right to seek
          compensation. Whether it's a car accident, dog attack or slip and
          fall, injured plaintiffs have an absolute right to seek the fairest
          amount of compensation for everything they've gone through. That can
          include time off work, health issues related to the injury, loss of
          your vehicle and other costs that arise from being injured.
          Representing yourself could cost you.
        </p>

        <h2>How Much is My Injury Case Worth?</h2>
        <p>
          That will all depend based on how injured you are and the type of
          representation you have. A good personal injury lawyer with decades in
          the field of accident law can yield much higher settlements than
          someone unfamiliar with injury law, local courts, and defense teams.
          Bisnar Chase is regularly sought after when attorneys are struggling
          to get a fair settlement for their clients.
        </p>
        <h2>Filing Your Personal Injury Claim in Time</h2>
        <p>
          Because there is a strict
          <strong> 2 year statute of limitation in California</strong>, it is
          important that you move quickly to secure counsel. Your attorney will
          have a lot of work ahead of him preparing your case and dealing with
          issues that may come up, including if the case is taken to trial.
          Having everything in order &amp; turned over to your legal team in a
          timely manner is well advised.
        </p>
        <h2>How Do You Protect Me Financially?</h2>
        <p>
          Our
          <Link className="no-fee-link" to="/about-us/no-fee-guarantee-lawyer">
            {" "}
            no win, no fee guarantee{" "}
          </Link>
          ensures that you will not pay your lawyer until your case is won. Even
          after our attorneys have put considerable effort into your case and
          accrued costs, if the case is lost, <strong>you will not pay</strong>.
          In addition we shield you from liability until the case is over so
          that you are not worried sick about costs associated with your case.
          Ask for your <strong>FREE consultation</strong> now with one of our
          experienced and top rated
          <strong> California personal injury attorneys</strong>!{" "}
          <strong>Call 800-561-4887</strong>
        </p>
      </ContentWrapper>
    </StyledFAQ>
  )
}

const StyledFAQ = styled("section")`
  background-color: ${({ theme }) => theme.colors.secondary};

  .no-fee-link {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.links.hoverOrange};
    }
  }

  h2 {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 2.4rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 3rem;
    letter-spacing: 0.8px;
    line-height: 1.8;
    font-size: 1.3rem;

    @media (max-width: 700px) {
      font-size: 1.2rem;
    }

    &:last-child {
      margin-bottom: 2rem;
    }
  }
`

const ContentWrapper = styled("div")`
  max-width: 1100px;
  margin: 0 auto;
  padding: 3rem 0;

  @media (max-width: 1024px) {
    max-width: 90%;
  }
`
