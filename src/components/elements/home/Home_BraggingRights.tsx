import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import { FaAngleRight } from "react-icons/fa"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { URLS } from "../../../data/home/braggingRights"

// ! Per image size is easliy changed by adjusting width parameter below
// ! _withWebpp for some reason gives pixelated images, turned off for now
const BRAGGING_RIGHTS_QUERY = graphql`
  query {
    asSeenOnImage: file(relativePath: { regex: "/as-seen-on.jpg/" }) {
      childImageSharp {
        fixed(width: 100) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    consumerAttorneysImage: file(
      relativePath: { regex: "/consumer-attorneys-of-california.png/" }
    ) {
      childImageSharp {
        fixed(width: 150) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }

    newsWeekImage: file(relativePath: { regex: "/newsweeklogo.png/" }) {
      childImageSharp {
        fixed(width: 175) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    timeImage: file(
      relativePath: { regex: "/1280px-Time_Magazine_logo.svg.png/" }
    ) {
      childImageSharp {
        fixed(width: 115) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    trialAdvocatesImage: file(
      relativePath: { regex: "/american-trial-lawyers.png/" }
    ) {
      childImageSharp {
        fixed(width: 135) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    fiveMillionImage: file(
      relativePath: { eq: "2019-assets/500-million.png" }
    ) {
      childImageSharp {
        fixed(width: 150) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    distinguishedImage: file(
      relativePath: { regex: "/distinguished-justice-advocates.png/" }
    ) {
      childImageSharp {
        fixed(width: 105) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
    americasTopImage: file(relativePath: { regex: "/americas-top-100.png/" }) {
      childImageSharp {
        fixed(width: 100) {
          ...GatsbyImageSharpFixed_withWebp
        }
      }
    }
  }
`

interface Props {
  borderTop?: boolean
}

export const BraggingRights: React.FC<Props> = ({
  borderTop = false
}): ReactElement => {
  return (
    <StaticQuery
      query={BRAGGING_RIGHTS_QUERY}
      render={data => {
        const images = Object.entries(data)

        // Use Object.entries() returned array data to loop and update href dynamically by using dictionary lookup

        return (
          <StyledBraggingRights bordertop={borderTop}>
            <ContentWrapper>
              <BadgeWrapper>
                {images.map((image: [string, any], index) => (
                  <Link className="badge" key={index} to={URLS[image[0]].href}>
                    <Img
                      alt={URLS[image[0]].alt}
                      loading="lazy"
                      fixed={image[1].childImageSharp.fixed}
                    />
                  </Link>
                ))}
              </BadgeWrapper>
              <TextWrapper>
                <Link to="/about-us/lawyer-reviews-ratings">
                  See our reviews and rewards{" "}
                  <span className="right-icon">
                    <FaAngleRight />
                  </span>
                </Link>
              </TextWrapper>
            </ContentWrapper>
          </StyledBraggingRights>
        )
      }}
    />
  )
}

const StyledBraggingRights = styled("section")<{ bordertop: boolean }>`
  height: 200px;
  border-top: ${props =>
    props.bordertop ? `1px solid ${props.theme.colors.secondary}` : "null"};

  @media (max-width: 1160px) {
    height: 300px;
  }

  @media (max-width: 715px) {
    height: 500px;
  }
`

const ContentWrapper = styled("div")`
  height: 100%;
`

const TextWrapper = styled("div")`
  text-align: center;
  position: relative;
  bottom: 2.5rem;
  height: 10%;

  .right-icon {
    position: relative;
    top: 0.1rem;
    font-size: 1rem;
    color: ${({ theme }) => theme.colors.accent};
  }
`

const BadgeWrapper = styled("div")`
  padding: 2rem 2rem 2rem;
  max-width: 80%;
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  justify-content: space-between;
  align-content: space-around;
  align-items: center;
  margin: 0 auto;

  .badge {
    transition: 0.3s ease all;

    &:hover {
      transform: scale(1.05);
    }
  }

  @media (max-width: 1550px) {
    max-width: 95%;
  }

  @media (max-width: 1160px) {
    max-width: 675px;
  }

  @media (max-width: 715px) {
    max-width: 427px;
    justify-content: space-evenly;
    align-items: center;
    padding-bottom: 3rem;
  }
`
