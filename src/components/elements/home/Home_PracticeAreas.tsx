import { graphql, Link, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { PAData } from "../../../data/home/practiceAreas"

const PRACTICE_AREA_QUERY = graphql`
  query {
    allFile(
      sort: { fields: [name], order: ASC }
      filter: { relativeDirectory: { eq: "homepage" } }
    ) {
      edges {
        node {
          childImageSharp {
            fluid(maxWidth: 800, maxHeight: 800, srcSetBreakpoints: [800]) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  }
`

interface GqlNode {
  node: {
    childImageSharp: {
      fluid: {
        base64: string
        sizes: string
        src: string
        srcSet: string
      }
    }
  }
}

// * TODO:  ADD ALT TAGS TO IMAGES

// !  The data is sorted first in the graphql query, then matched to PAData object import

export const PracticeAreas: React.FC = (): ReactElement => {
  return (
    <StaticQuery
      query={PRACTICE_AREA_QUERY}
      render={data => {
        const images = data.allFile.edges
        return (
          <StyledPracticeArea>
            <TextWrapper>
              <p className="PA-title">How we can help you</p>
              <p className="PA-subtext">
                We have the resources, experience, and talent to take on some of
                the most complex accident & injury cases in California. Browse
                through our practice areas below for more information:
              </p>
            </TextWrapper>
            <PAGrid>
              {images.map(
                ({ node: { childImageSharp } }: GqlNode, index: number) => (
                  <Link
                    className="card-wrapper"
                    to={PAData[index].href}
                    key={index}
                  >
                    <PACard>
                      <Img
                        loading="lazy"
                        alt={PAData[index].alt}
                        className="card-image"
                        // @ts-ignore
                        fluid={childImageSharp.fluid}
                      />
                      <p className="card-text">
                        <span className="card-title">
                          {PAData[index].name}{" "}
                          <span className="arrow-right">
                            <FaArrowAltCircleRight />
                          </span>
                        </span>
                        <span className="card-desc">{PAData[index].desc}</span>
                      </p>
                    </PACard>
                  </Link>
                )
              )}
            </PAGrid>
          </StyledPracticeArea>
        )
      }}
    />
  )
}

const TextWrapper = styled("div")`
  max-width: 1100px;
  margin: 0 auto;

  @media (max-width: 1024px) {
    max-width: 90%;
  }

  @media (max-width: 700px) {
    max-width: 90%;
  }

  .PA-title,
  .PA-subtext {
    text-align: center;
  }

  .PA-title {
    color: ${({ theme }) => theme.colors.secondary};
    font-size: 3.4rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 0 0rem 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
    font-weight: 600;
    width: 100%;

    @media (max-width: 700px) {
      font-size: 2.2rem;
    }
  }

  .PA-subtext {
    text-align: center;
    letter-spacing: 1px;
    line-height: 1.8;
    font-size: 1.4rem;
    margin: 3rem 0;

    @media (max-width: 700px) {
      text-align: initial;
      max-width: 100%;
      font-size: 1.2rem;
    }
  }
`

const StyledPracticeArea = styled("section")`
  padding: 4rem 2rem 5rem 2rem;

  .card-wrapper {
    overflow: hidden;
    border-radius: 0.5rem;
  }

  @media (max-width: 1024px) {
    padding: 4rem 0 4rem;
  }

  @media (max-width: 700px) {
    padding: 3rem 0 3rem;
  }
`

const PACard = styled("div")`
  transition: all 0.3s;
  position: relative;
  overflow: hidden;

  @media (max-width: 700px) {
    height: 200px;
  }

  .card-image {
    transition: transform 0.5s;

    img {
      @media (max-width: 700px) {
        object-position: center -102px !important;
      }
    }
  }

  .card-text {
    position: absolute;
    background: RGBA(0, 0, 0, 0.8);
    height: 100%;
    width: 100%;
    bottom: 0;
    margin: 0;
    text-align: center;
    font-size: 2rem;
    font-variant: common-ligatures small-caps;
    color: ${({ theme }) => theme.colors.primary};

    transform: translateY(calc(100% / 1.18));
    transition: transform 0.5s;

    @media (max-width: 700px) {
      transform: translateY(calc(100% / 1.41));
    }

    @media (max-width: 465px) {
      transform: translateY(calc(100% / 1.325));
    }

    .card-title {
      display: block;
      width: 100%;
      height: 70px;
      padding-top: 2.5%;

      @media (max-width: 1220px) {
        font-size: 1.6rem;
      }

      @media (max-width: 877px) {
        font-size: 1.2rem;
      }

      @media (max-width: 700px) {
        font-size: 1.6rem;
      }
    }

    .arrow-right {
      font-size: 1.6rem;
      position: relative;
      top: 3px;
      left: 5px;
      color: ${({ theme }) => theme.colors.accent};
    }

    .card-desc {
      display: block;
      font-size: 55%;
      font-variant: normal;
      padding: 0 4rem;
      line-height: 2;
      position: relative;
      bottom: 15px;

      @media (max-width: 1660px) {
        font-size: 50%;
      }

      @media (max-width: 1050px) {
        padding: 1rem;
      }

      @media (max-width: 700px) {
        bottom: 30px;
        padding: 1rem;
      }

      @media (max-width: 465px) {
        font-size: 0.92rem;
        bottom: 38px;
      }
    }
  }

  &:hover {
    .card-image {
      transform: scale(1.2);
    }

    .card-text {
      transform: translateY(calc(100% / 4));
    }
  }
`

const PAGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin: 0 auto;
  max-width: 1100px;
  grid-gap: 2rem;

  @media (max-width: 1220px) {
    max-width: 90%;
  }

  @media (max-width: 1024px) {
    max-width: 100%;
    grid-gap: 1rem;
    padding: 1rem;
  }

  @media (max-width: 700px) {
    max-width: 100%;
    padding: 1rem;
    grid-template-columns: initial;
    grid-template-rows: repeat(9, 1fr);
  }
`
