import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React, { ReactElement } from "react"
import { FaRegArrowAltCircleRight, FaYoutube } from "react-icons/fa"
import Slider from "react-slick"
import styled from "styled-components"
import { CaseResults } from "../../../data/home/caseResults"
import { DefaultOrangeButton, elevation } from "../../utilities"
import { Link } from "src/components/elements/Link"

const BACKGROUND_IMAGE_QUERY = graphql`
  query {
    brianChaseImage: file(
      relativePath: { regex: "/brian-chase-results.png/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 500, srcSetBreakpoints: [500]) {
          ...GatsbyImageSharpFluid_withWebp_tracedSVG
        }
      }
    }
  }
`

function formatNumber(num: number): string {
  return new Intl.NumberFormat().format(num)
}

export const ResultsSidebar: React.FC = (): ReactElement => {
  const settings = {
    dots: false,
    infinite: true,
    lazyLoad: "ondemand",
    slidesToScroll: 1,
    slidesToShow: 8,
    vertical: true
  }

  return (
    <StaticQuery
      query={BACKGROUND_IMAGE_QUERY}
      render={data => {
        const image = data.brianChaseImage.childImageSharp.fluid
        return (
          <StyledResultsSidebar>
            <TitleWrapper>
              <p className="title-text">Introduction to Brian Chase</p>
            </TitleWrapper>
            <VideoWrapper>
              <a
                target="_blank"
                href="https://www.youtube.com/embed/wmv1HKnhW7U?autoplay=1&wmode=transparent&enablejsapi=1&rel=0"
              >
                <Img
                  title="Brian Chase Highlight Video"
                  alt="Brian Chase Video Highlight Thumbnail"
                  fluid={image}
                />
                <FaYoutube className="youtube-icon" />
              </a>
            </VideoWrapper>
            <ResultsCarousel>
              {/* 
              //@ts-ignore */}
              <Slider className="carousel" {...settings}>
                {CaseResults.map(result => {
                  return (
                    <Link key={result.id} to={result.href}>
                      <ResultCard>
                        <div className="result-id">
                          <span>{result.id}</span>
                        </div>
                        <div>
                          <p className="result-amount">
                            {`$${formatNumber(result.amountWon)}`}
                          </p>
                          <p className="result-type">{result.type}</p>
                        </div>
                        <FaRegArrowAltCircleRight className="result-arrow" />
                      </ResultCard>
                    </Link>
                  )
                })}
              </Slider>
            </ResultsCarousel>
            <Link className="btn-link" to="/case-results">
              {/* 
              //@ts-ignore */}
              <DefaultOrangeButton
                className="btn-case-results"
                // @ts-ignore
                modifiers="small"
              >
                VIEW OUR CASE RESULTS
              </DefaultOrangeButton>
            </Link>
          </StyledResultsSidebar>
        )
      }}
    />
  )
}

const TitleWrapper = styled("div")`
  height: 50px;
  margin-bottom: 1.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  ${elevation[3]};

  .title-text {
    font-size: 2.2rem;
    font-variant: all-petite-caps;
  }
`

const ResultCard = styled("div")`
  display: grid !important;
  grid-template-columns: 100px 1fr 100px;
  color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  border-bottom: 2px solid #394c5c;
  transition: transform 0.2s ease-out, box-shadow 0.2s linear;

  .dollar-sign {
    position: relative;
    top: 5px;
    color: #85bb65;
    left: 2px;
  }

  .result-id {
    display: flex;
    justify-content: center;
    align-items: center;
    background: linear-gradient(to right, #ffd89b, #19547b) center center / 80px
      80px no-repeat;
    height: 85px;
    width: 85px;
    border: 15px solid;
    border-radius: 50%;
    span {
      position: relative;
      font-size: 2rem;
      font-weight: 600;
      bottom: 2px;
    }
  }

  .result-amount {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 3rem;
    font-style: italic;
    font-variant: proportional-nums;
    justify-self: center;
  }

  .result-type {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 1.5rem;
  }

  .result-arrow {
    justify-self: center;
    font-size: 2.5rem;
    color: ${({ theme }) => theme.colors.accent};
  }

  &:hover {
    transform: scale(1.07);
    ${elevation[3]};
  }
`

const ResultsCarousel = styled("div")`
  padding-top: 1rem;
  .carousel {
    .slick-list {
    }
    .slick-track {
      border: none;
      .slick-slide {
        height: 90px;
      }
    }

    .slick-list {
      border: none;
    }
  }

  .slick-next {
    transform: rotate(90deg);
    right: 75px;
    top: 740px;
    z-index: 30;

    &:before {
      color: ${({ theme }) => theme.colors.accent};
    }
  }
  .slick-prev {
    transform: rotate(90deg);
    left: 75px;
    top: 740px;
    z-index: 30;

    &:before {
      color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const StyledResultsSidebar = styled("div")`
  position: relative;

  p {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 1.6rem;
    text-align: center;
    margin: 0;
  }

  .btn-link {
    position: absolute;
    left: 50%;

    margin-left: -88px;
    margin-top: 1rem;

    .btn-case-results {
      height: 3rem;
      width: 176px;
    }
  }
`

const VideoWrapper = styled("div")`
  position: relative;

  .youtube-icon {
    font-size: 6rem;
    position: absolute;
    margin: auto;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }

  a:hover {
    color: #c4302b;
  }
`
