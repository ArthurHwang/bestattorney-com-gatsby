import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

export const ResultsContent: React.FC = (): ReactElement => {
  return (
    <StyledResultsContent>
      <p>
        <strong>
          Contact our experienced and dedicated Personal Injury Lawyers for
          immediate help. Call 949-446-1783. Outside of California call toll
          free 1-877-974-6875.
        </strong>
      </p>
      <p>
        If you have been injured in an accident caused by another person or
        organization's negligence, you may be facing insurmountable challenges.{" "}
        <strong>You have the right to seek injury compensation</strong> for the
        serious losses you have experienced through a personal injury case that
        holds at-fault parties responsible for their actions. Do not speak with
        insurance companies before you consult with a qualified and experienced
        personal injury lawyer in California who will protect your interest, not
        the insurance companies. Important issues will need to be discussed
        about your case like comparative negligence, strict liability, pain and
        suffering and economic damages. Your attorney can protect you regarding
        these elements of the case.
      </p>
      <p>
        We offer 5 locations throughout California to make it very convenient
        for our law firm to be near you. You may schedule a free consultation
        with our <Link to="/orange-county">Orange County office</Link>,{" "}
        <Link to="/los-angeles">Los Angeles office</Link>,{" "}
        <Link to="/san-bernardino">San Bernardino office</Link> and our{" "}
        <Link to="/riverside">Riverside office</Link>. For specialized cases we
        also have a{" "}
        <Link to="/san-francisco">San Francisco meeting location</Link>.
      </p>
      <h2>Top Notch Legal Representation in California</h2>
      <p>
        Success in California injury cases can only be assured through skilled
        legal representation by some of the best lawyers. Bisnar Chase
        exclusively represents plaintiffs for injuries resulting from car
        accidents, severe dog bites, motorcycle accidents, product defects, auto
        defect injuries, and other serious or catastrophic injuries. We have the
        resources, experience, and talent to take on some of the most complex
        personal injury cases with outstanding results. We cover all of
        California and offer a guarantee to
        <strong>
          {" "}
          shield you from financial burden until your case is won
        </strong>
        .
      </p>
      <h2>Experienced Trial Lawyers Who Win</h2>
      <p>
        Throughout California, defense teams and local courts know who we are.
        We have spent decades in and out of the courtroom fighting for justice
        for injured victims. Representing Californians is something we put a lot
        of pride and work into every day. Some of the best lawyers refer their
        toughest cases to Bisnar Chase. Our reputation of winning challenging
        injury cases stands on its own. We have a 96% success rate and have
        collected almost a half a billion for our clients.
      </p>
      <h2>Award Winning Personal Injury Attorneys</h2>
      <p>
        Founded by <Link to="/attorneys/john-bisnar">John Bisnar</Link> in 1978,
        Bisnar Chase is a California injury law firm dedicated to a superior
        client experience. Trial Lawyer &amp; Partner,{" "}
        <Link to="/attorneys/brian-chase">Brian Chase</Link> is a respected
        accident &amp; injury attorney throughout the nation having taken on
        some of the largest auto defect cases and defective product
        manufacturers. At Bisnar Chase, our award winning attorneys have found
        continued courtroom success since 1978. With over{" "}
        <strong>$500M won</strong> including punitive damages and thousands
        represented, our firm has earned a reputation as one of
        <strong> California's toughest injury law firms</strong>. Call
        877-974-6875 for a free consultation.
      </p>
    </StyledResultsContent>
  )
}

const StyledResultsContent = styled("div")`
  h2 {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 2.4rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
  }

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 4rem;
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 3rem;
    letter-spacing: 0.8px;
    line-height: 1.8;
    font-size: 1.3rem;

    @media (max-width: 700px) {
      font-size: 1.2rem;
    }

    &:last-child {
      margin-bottom: 2rem;
    }
  }

  a {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.links.hoverOrange};
    }
  }
`
