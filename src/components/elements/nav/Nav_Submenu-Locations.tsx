import { Link } from "src/components/elements/Link"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../context/NavContext"
import styled from "styled-components"
import { elevation } from "../../utilities"

interface Props {
  className: string
  activeClassName: string
}

export const SubmenuLocations: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)
  return (
    <StyledLocations
      onClick={() => dispatch({ type: "CLICK" })}
      onMouseEnter={() => dispatch({ type: "MOVE" })}
      className={className}
    >
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/locations"
      >
        Locations
      </Link>
      {!state.clicked && (
        <LocationsSubmenu className="locations-submenu">
          <LocationsTitle>
            <Link to="/locations">Locations</Link>
          </LocationsTitle>
          <div className="atto-text-container">
            <Link to="/orange-county">ORANGE COUNTY</Link>
            <Link to="/los-angeles">LOS ANGELES</Link>
            <Link to="/riverside">RIVERSIDE</Link>
            <Link to="/san-bernardino">SAN BERNARDINO</Link>
            <Link to="/locations">OTHER SERVICE AREAS</Link>
          </div>
        </LocationsSubmenu>
      )}
    </StyledLocations>
  )
}

const StyledLocations = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 320px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.8rem 1rem;
      text-decoration: none;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .locations-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .locations-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const LocationsTitle = styled.span`
  font-size: 2rem;
  position: relative;
  display: block;
  border-bottom: 2px solid ${({ theme }) => theme.colors.grey};
  top: 1rem;

  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
  }
`

const LocationsSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 300px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: -13px;
  top: 35px;
  height: 370px;
  padding: 0 2.5rem 0 2.5rem;
`
