import { Link } from "src/components/elements/Link"
import React, { useEffect, useState, ReactElement } from "react"
import { FaAngleRight } from "react-icons/fa"
import { IoIosMenu } from "react-icons/io"
import styled from "styled-components"

interface Props {
  className: string
}

export const NavMobileMenu: React.FC<Props> = ({ className }): ReactElement => {
  const windowGlobal: any = typeof window !== "undefined" && window
  const [menuOpen, setMenuOpen] = useState(false)
  const [height, setHeight] = useState(0)

  useEffect(() => {
    setHeight(windowGlobal.innerHeight - 70)

    window.addEventListener("resize", () => {
      setHeight(windowGlobal.innerHeight - 70)
    })

    return () => {
      window.removeEventListener("resize", () => {})
    }
  }, [])

  function handleClick() {
    return setMenuOpen(!menuOpen)
  }

  return (
    <NavMobileWrapper className={className}>
      <StyledNavMobileMenu
        rotate={menuOpen ? "true" : "false"}
        onClick={handleClick}
      >
        <IoIosMenu className="menu-icon" />
      </StyledNavMobileMenu>
      {menuOpen && (
        <MobileDropDownMenu
          viewportHeight={height}
          className={menuOpen ? "fade" : "fadeOut"}
        >
          <ul>
            <li>
              <Link to="/">HOME</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/about-us">ABOUT US</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/practice-areas">PRACTICE AREAS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/case-results">RESULTS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/resources">RESOURCES</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/about-us/testimonials">CLIENT REVIEWS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/referrals">ATTORNEY REFFERALS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>

            <li>
              <Link to="/locations">LOCATIONS</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/blog">BLOG</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
            <li>
              <Link to="/contact">CONTACT</Link>
              <span>
                <FaAngleRight />
              </span>
            </li>
          </ul>

          <ContactTextWrapper>
            <p style={{ borderTop: "medium double #ec7d29" }} />
            <p>inquiry@bestatto-gatsby.netlify.app</p>
            <p>(949) 203-3814</p>
          </ContactTextWrapper>
        </MobileDropDownMenu>
      )}
    </NavMobileWrapper>
  )
}

const ContactTextWrapper = styled("div")`
  margin-left: 2.7rem;
  animation: moveInLeft 1.3s;
  padding-right: 2.5em;
  bottom: 0;

  p {
    color: ${({ theme }) => theme.colors.primary};
    margin-bottom: 1rem;
  }

  a {
    color: ${({ theme }) => theme.colors.accent};
  }
`

const StyledNavMobileMenu = styled("div")<{ rotate: string }>`
  position: relative;
  right: 15px;
  top: 1px;
  transform: ${props => (props.rotate === "true" ? `rotate(90deg)` : "")};
  transition: transform 0.3s;

  .menu-icon {
    font-size: 3.5rem;
    color: ${props =>
      props.rotate === "true"
        ? props.theme.colors.accent
        : props.theme.colors.secondary};
  }
`

const MobileDropDownMenu = styled("div")<{ viewportHeight: number }>`
  width: 100vw;
  position: fixed;
  z-index: 1000;
  top: 70px;
  left: 0;
  height: ${props => props.viewportHeight + "px"};
  background-color: ${({ theme }) => theme.colors.secondary};
  color: ${({ theme }) => theme.colors.primary};
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  padding-top: 1rem;
  padding-bottom: 1rem;

  ul {
    color: ${({ theme }) => theme.colors.primary};
    list-style: none;
    list-style-position: inside;
    margin-left: 2.5em !important;

    li {
      margin: 1.2rem 0;
      animation: moveInLeft 1.3s;
      span {
        margin-left: 0.5rem;
        color: ${({ theme }) => theme.colors.accent};
      }
    }

    li a {
      color: ${({ theme }) => theme.colors.primary};
      font-size: 1.8rem;
    }
  }
`

const NavMobileWrapper = styled("div")`
  .fade {
    animation: fadein 0.8s;
    -moz-animation: fadein 0.8s; /* Firefox */
    -webkit-animation: fadein 0.8s; /* Safari and Chrome */
    -o-animation: fadein 0.8s; /* Opera */
  }

  .fadeOut {
    animation: fadeout 2s;
    -moz-animation: fadeout 2s; /* Firefox */
    -webkit-animation: fadeout 2s; /* Safari and Chrome */
    -o-animation: fadeout 2s; /* Opera */
  }

  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  @-moz-keyframes fadein {
    /* Firefox */
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  @-webkit-keyframes fadein {
    /* Safari and Chrome */
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  @-o-keyframes fadein {
    /* Opera */
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  @keyframes fadeout {
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }
  @-moz-keyframes fadeout {
    /* Firefox */
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }
  @-webkit-keyframes fadeout {
    /* Safari and Chrome */
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }
  @-o-keyframes fadeout {
    /* Opera */
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }

  @keyframes moveInLeft {
    0% {
      opacity: 0;
      transform: translateX(-100px);
    }

    80% {
      transform: translateX(20px);
    }

    100% {
      opacity: 1;
      transform: translate(0);
    }
  }
`
