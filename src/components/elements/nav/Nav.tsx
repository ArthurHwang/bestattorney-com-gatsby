// import React, { useReducer, useContext } from "react"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { NavCall } from "./Nav_Call"
import { NavLinks } from "./Nav_Links"
import { NavLogo } from "./Nav_Logo"

interface Props {
  spanishLink?: string
  geoLocation?: string
}

export const Nav: React.FC<Props> = ({
  spanishLink,
  geoLocation
}): ReactElement => {
  const spanishPageEquivalent = spanishLink ? spanishLink : "/abogados"

  return (
    <NavWrapper>
      <StyledNavTop>
        <NavLogo className="nav-logo" />
        <NavCall
          spanishPageEquivalent={spanishPageEquivalent}
          className="nav-call"
          geoLocation={geoLocation}
        />
      </StyledNavTop>
      <StyledNavBottom>
        <NavLinks className="nav-links" />
      </StyledNavBottom>
    </NavWrapper>
  )
}

const StyledNavTop = styled("div")`
  display: flex;
  justify-content: space-between;
  height: 80px;
  align-items: center;
  padding: 0 2rem;
`
const StyledNavBottom = styled("div")`
  width: 100%;
  background-color: ${({ theme }) => theme.colors.secondary};
  height: 35px;
  display: flex;
  align-items: center;
  padding: 0 3rem 0 2rem;
  display: flex;
  justify-content: space-between;
`

const NavWrapper = styled("section")`
  background-color: ${({ theme }) => theme.colors.primary};
  width: 100%;
  z-index: 15;
  top: 0;
  justify-content: space-between;
  font-size: 1rem;
  border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};

  @media (max-width: 1024px) {
    display: none;
  }

  .nav-logo {
    width: 357px;
    margin-right: auto;
  }

  img {
    height: 67px;
    margin-bottom: 0;
  }
`
