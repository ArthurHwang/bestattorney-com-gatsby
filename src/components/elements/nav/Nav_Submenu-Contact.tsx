import { Link } from "src/components/elements/Link"
import styled from "styled-components"
import { elevation } from "../../utilities"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../context/NavContext"

interface Props {
  className: string
  activeClassName: string
}

export const SubmenuContact: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)
  return (
    <StyledContact className={className}>
      <Link
        onClick={() => dispatch({ type: "CLICK" })}
        onMouseEnter={() => dispatch({ type: "MOVE" })}
        activeClassName={activeClassName}
        className="nav-link"
        to="/contact"
      >
        Contact
      </Link>
      {!state.clicked && (
        <ContactSubmenu className="contact-submenu">
          <ContactTitle>
            <Link to="/contact">Contact</Link>
          </ContactTitle>
          <div className="atto-text-container">
            <Link to="/orange-county/contact">ORANGE COUNTY OFFICE</Link>
            <Link to="/los-angeles/contact">LOS ANGELES OFFICE</Link>
            <Link to="/riverside/contact">RIVERSIDE OFFICE</Link>
            <Link to="/san-bernardino/contact">SAN BERNARDINO OFFICE</Link>
          </div>
        </ContactSubmenu>
      )}
    </StyledContact>
  )
}

const StyledContact = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 280px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.8rem 1rem;
      text-decoration: none;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .contact-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .contact-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const ContactTitle = styled.span`
  font-size: 2rem;
  position: relative;
  display: block;
  border-bottom: 2px solid ${({ theme }) => theme.colors.grey};
  top: 1rem;

  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
  }
`

const ContactSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 300px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: -13px;
  top: 35px;
  height: 330px;
  padding: 0 2.5rem 0 2.5rem;
`
