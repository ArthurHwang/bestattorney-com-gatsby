import React, { ReactElement } from "react"
import styled from "styled-components"
import { NavLogo } from "./Nav_Logo"
import { NavMobileMenu } from "./Nav_Mobile-Menu"

export const NavMobile: React.FC<any> = (): ReactElement => {
  return (
    <NavWrapperMobile>
      <NavLogo className="nav-logo" />
      <NavMobileMenu className="nav-mobile-menu" />
    </NavWrapperMobile>
  )
}

const NavWrapperMobile = styled("div")`
  display: flex;
  position: fixed;
  justify-content: space-between;
  height: 70px;
  background-color: ${({ theme }) => theme.colors.primary};
  width: 100%;
  z-index: 15;
  top: 0;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.secondary};

  @media (min-width: 1025px) {
    display: none;
  }

  .nav-mobile-menu {
    @media (min-width: 1025px) {
      display: none;
    }
  }

  .nav-logo {
    @media (max-width: 1024px) {
      width: 337px;
    }
    @media (min-width: 1025px) {
      display: none;
    }
  }

  img {
    height: 67px;
    margin-bottom: 0;
  }
`
