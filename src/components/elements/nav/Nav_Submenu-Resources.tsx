import { Link } from "src/components/elements/Link"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../context/NavContext"
import styled from "styled-components"
import { elevation } from "../../utilities"

interface Props {
  className: string
  activeClassName: string
}

export const SubmenuResources: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)
  return (
    <StyledResources
      onClick={() => dispatch({ type: "CLICK" })}
      onMouseEnter={() => dispatch({ type: "MOVE" })}
      className={className}
    >
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/resources"
      >
        Resources
      </Link>
      {!state.clicked && (
        <ResourcesSubmenu className="resources-submenu">
          <ResourcesTitle>
            <Link to="/resources">Resources</Link>
          </ResourcesTitle>
          <div className="atto-text-container">
            <Link to="/resources">HELPFUL RESOURCES</Link>
            <Link to="/faqs">FREQUENTLY ASKED QUESTIONS</Link>
            <Link to="/resources/book-order-form">BOOKS BY BISNAR CHASE</Link>
          </div>
        </ResourcesSubmenu>
      )}
    </StyledResources>
  )
}

const StyledResources = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 280px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.8rem 1rem;
      text-decoration: none;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .resources-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .resources-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const ResourcesTitle = styled.span`
  font-size: 2rem;
  position: relative;
  display: block;
  border-bottom: 2px solid ${({ theme }) => theme.colors.grey};
  top: 1rem;

  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
  }
`

const ResourcesSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 300px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: -13px;
  top: 35px;
  height: 330px;
  padding: 0 2.5rem 0 2.5rem;
`
