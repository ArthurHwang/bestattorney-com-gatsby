import { Link } from "src/components/elements/Link"
import styled from "styled-components"
import { elevation } from "../../utilities"
import React, { useContext, ReactElement } from "react"
import { NavContext } from "../../../context/NavContext"

interface Props {
  className: string
  activeClassName: string
}

export const SubmenuResults: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  const { state, dispatch } = useContext(NavContext)
  return (
    <StyledResults
      onClick={() => dispatch({ type: "CLICK" })}
      onMouseEnter={() => dispatch({ type: "MOVE" })}
      className={className}
    >
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/case-results"
      >
        Results
      </Link>
      {!state.clicked && (
        <ResultsSubmenu className="results-submenu">
          <ResultsTitle>
            <Link to="/case-results">Results</Link>
          </ResultsTitle>
          <div className="atto-text-container">
            <Link to="/case-results">CASE RESULTS</Link>
            <Link to="/about-us/testimonials">CLIENT REVIEWS</Link>
          </div>
        </ResultsSubmenu>
      )}
    </StyledResults>
  )
}

const StyledResults = styled.li`
  .atto-text-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    height: 160px;

    a {
      font-weight: 600;
      font-size: 1rem;
      padding: 0.9rem 0 0.3rem 1rem;
      text-decoration: none;
      height: 40px;
      border-bottom: 1px solid ${({ theme }) => theme.colors.grey};

      &:hover {
        transition: 0.3s ease box-shadow;
        ${elevation[1]};
      }
    }
  }

  .results-submenu {
    opacity: 0;
    transform: translateY(-800px);
  }

  &:hover {
    .results-submenu {
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const ResultsTitle = styled.span`
  font-size: 2rem;
  position: relative;
  display: block;
  border-bottom: 2px solid ${({ theme }) => theme.colors.grey};
  top: 1rem;
  text-transform: uppercase;
  font-style: italic;

  a {
    font-weight: 800;
  }
`

const ResultsSubmenu = styled("div")`
  z-index: 1;
  background: ${({ theme }) => theme.colors.primary};
  width: 300px;
  position: absolute;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  left: -13px;
  top: 35px;
  height: 200px;
  padding: 0 2.5rem 0 2.5rem;
`
