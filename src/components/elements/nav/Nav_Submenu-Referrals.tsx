import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"

interface Props {
  className: string
  activeClassName: string
}

export const SubmenuReferrals: React.FC<Props> = ({
  className,
  activeClassName
}): ReactElement => {
  return (
    <StyledReferrals className={className}>
      <Link
        activeClassName={activeClassName}
        className="nav-link"
        to="/referrals"
      >
        Attorney Referrals
      </Link>
    </StyledReferrals>
  )
}

const StyledReferrals = styled.li`
  z-index: 1;
`
