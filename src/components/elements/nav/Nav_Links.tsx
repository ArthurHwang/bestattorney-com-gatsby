import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { SubmenuAboutUs } from "./Nav_Submenu-AboutUs"
import { SubmenuContact } from "./Nav_Submenu-Contact"
import { SubmenuLocations } from "./Nav_Submenu-Locations"
import { SubmenuPracticeAreas } from "./Nav_Submenu-PracticeAreas"
import { SubmenuReferrals } from "./Nav_Submenu-Referrals"
import { SubmenuResources } from "./Nav_Submenu-Resources"
import { SubmenuResults } from "./Nav_Submenu-Results"

interface Props {
  className: string
}

export const NavLinks: React.FC<Props> = ({ className }): ReactElement => {
  return (
    <StyledNavLinks className={className}>
      <li className="nav-menu-item ">
        <Link activeClassName="active" className="nav-link" to="/">
          Home
        </Link>
      </li>
      <SubmenuAboutUs activeClassName="active" className="nav-menu-item" />
      <SubmenuPracticeAreas
        activeClassName="active"
        className="nav-menu-item"
      />
      <SubmenuResults activeClassName="active" className="nav-menu-item" />
      <SubmenuResources activeClassName="active" className="nav-menu-item" />
      <SubmenuReferrals activeClassName="active" className="nav-menu-item" />
      <SubmenuLocations activeClassName="active" className="nav-menu-item" />
      <li className="nav-menu-item ">
        <Link
          activeClassName="active"
          partiallyActive={true}
          className="nav-link"
          to="/blog"
        >
          Blog
        </Link>
      </li>
      <SubmenuContact activeClassName="active" className="nav-menu-item" />
    </StyledNavLinks>
  )
}

const StyledNavLinks = styled.ul`
  margin: 0;
  display: flex;
  position: relative;
  list-style-type: none;
  height: 35px;

  .active {
    &::after {
      height: 1px;
      background: ${({ theme }) => theme.colors.accent};
      content: "";
      width: 65%;
      position: absolute;
      transform: translateX(-50%);
      left: 49%;
      bottom: 7px;
      animation: appear ease-in-out 0.3s;
    }

    @keyframes appear {
      0% {
        width: 0;
      }

      80% {
        width: 85%;
      }

      90% {
        width: 80%;
      }

      100% {
        width: 75%;
      }
    }
  }

  .nav-menu-item {
    display: flex;
    position: relative;

    &:first-child::before {
      display: none;
    }
  }

  a.nav-link {
    white-space: nowrap;
    display: flex;
    text-transform: uppercase;
    padding: 1rem 0.8rem;
    align-items: center;
    text-decoration: none;
    font-weight: 600;
    font-size: 0.95rem;
    margin: 0 1rem;
    border: 0;
    cursor: pointer;
    color: ${({ theme }) => theme.colors.primary};

    &:hover,
    &:focus {
      transition: transform 0.2s ease-in-out;
      transform: scale(1.07);
    }

    @media (max-width: 1120px) {
      font-size: 0.83rem;
    }
  }
`
