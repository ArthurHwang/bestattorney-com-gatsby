import Img from "gatsby-image"
import React, { ReactElement, useEffect } from "react"
import styled from "styled-components"
import { LayoutDefault } from "../../layouts/Layout_Default"
import { BreadCrumbs } from "../BreadCrumbs"
import { SEO } from "../SEO"
import { formatWPmeta, formatWPtitle } from "../../utilities"
import { Disqus } from "gatsby-plugin-disqus"
import folderOptions from "./folder-options"

interface Props {
  location: { pathname: any }
  pageContext: {
    title: string
    id: string
    slug: string
    content: string
    modified: string
    socialLink: string
    excerpt: string
    author: string
    categories: any
    imageSharp: {
      node: {
        localFile: {
          childImageSharp: {
            fluid: {
              aspectRatio: number
              base64: string
              src: string
              srcSet: string
              sizes: string
            }
          }
        }
      }
    }
  }
}

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

function removeNBSP(innerHTML: string): string {
  return innerHTML
    .replace(/<p>&nbsp;<\/p>/g, "")
    .replace(/https:\/\/www\.bestattorney\.com(.*?)\.html/g, "$1")
}

function removeBestattorney(innerHTML: string): string {
  return innerHTML.replace(
    /www.bestattorney.com/g,
    "www.bestatto-gatsby-netlify.app"
  )
}

const PostTemplate: React.FC<Props> = ({
  pageContext,
  location
}): ReactElement => {
  useEffect(() => {
    const shareButtons = document.getElementsByClassName("mashsb-container")[0]
    if (shareButtons) {
      shareButtons.remove()
    }
  }, [])

  let featuredImage = null
  let socialImageURL = null

  if (
    !pageContext.imageSharp ||
    !pageContext.imageSharp.node ||
    !pageContext.imageSharp.node.localFile ||
    !pageContext.imageSharp.node.localFile.childImageSharp
  ) {
    featuredImage = null
    socialImageURL = null
  } else {
    socialImageURL = `${pageContext.imageSharp.node.localFile.childImageSharp.fluid.src}`

    featuredImage = (
      <Img
        className="post-image"
        alt={pageContext.title}
        fluid={pageContext.imageSharp.node.localFile.childImageSharp.fluid}
        loading="lazy"
        fadeIn={true}
      />
    )
  }

  const parsedInnerHTML = removeBestattorney(removeNBSP(pageContext.content))

  const disqusConfig = {
    url: `https://bestatto-gatsby.netlify.app/blog/${pageContext.slug}`,
    identifier: pageContext.id,
    title: pageContext.title
  }

  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageTitle={formatWPtitle(pageContext.title)}
        pageDescription={formatWPmeta(pageContext.excerpt)}
        // socialImage will default to main homepage image if not supplied
        imageUrl={socialImageURL ? socialImageURL : null}
        isBlog={true}
      />
      <BlogPageWrapper>
        <h1>{formatWPtitle(pageContext.title)}</h1>

        <BreadCrumbs location={location} />

        <div className="post-metadata">
          <p className="post-author">
            <span>Posted By </span>
            <span className="post-author-name">{pageContext.author} </span>{" "}
            <span> on {pageContext.modified}</span>
          </p>
          <p className="post-category">
            Posted in: <span>{pageContext.categories.nodes[0].name}</span>
          </p>
        </div>
        <div className="post-content">
          {featuredImage}
          <p className="image-caption">{formatWPtitle(pageContext.title)}</p>
        </div>
        <br />
        <div dangerouslySetInnerHTML={{ __html: parsedInnerHTML }} />
        <div className="disqus-wrapper">
          <Disqus style={{ marginTop: "4rem" }} config={disqusConfig} />
        </div>
      </BlogPageWrapper>
    </LayoutDefault>
  )
}

export default PostTemplate

const BlogPageWrapper = styled("section")`
  width: 100%;

  .disqus-wrapper {
    padding: 0 10px;
    background: #eee;
    margin-bottom: 2rem;
  }

  .post-author {
    font-weight: 600;
    font-size: 1.2rem;
    margin-bottom: 0;

    @media (max-width: 600px) {
      display: none;
    }

    .post-author-name {
      font-weight: 600;
    }
  }

  .post-metadata {
    display: flex;
    justify-content: space-between;
    padding-top: 0.5rem;
    align-items: flex-end;
  }

  .post-category {
    font-size: 1.2rem;
    margin-bottom: 0;

    span {
      color: ${({ theme }) => theme.colors.secondary};
      font-weight: 600;
    }
  }

  .post-image {
    max-height: 600px;
  }

  .image-caption {
    font-size: 0.8rem;
    text-align: right;
    margin: 0;

    @media (max-width: 500px) {
      display: none;
    }
  }

  h1 {
    font-size: 2.5rem;
    margin-bottom: 0;
  }
`
