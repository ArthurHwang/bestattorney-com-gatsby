import { Link } from "src/components/elements/Link"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { LayoutDefault } from "../../layouts/Layout_Default"
import { BreadCrumbs } from "../BreadCrumbs"
import folderOptions from "./folder-options"
import { SEO } from "../SEO"
import { BlogCard } from "./Blog_Card"
import { FaArrowAltCircleLeft, FaArrowAltCircleRight } from "react-icons/fa"

interface Props {
  pathContext: any
  location: { pathname: any }
  data: {
    allWordpressPost: {
      edges: {
        node: Node
      }
    }
  }
}

interface NavLinkProps {
  test: string
  url: string
  text: string
}

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

const NavLink = (props: NavLinkProps): ReactElement => {
  if (!props.test) {
    return <Link to={props.url}>{props.text}</Link>
  } else {
    return <span style={{ textDecoration: "line-through" }}>{props.text}</span>
  }
}

const BlogIndex: React.FC<Props> = ({
  location,
  pathContext
}): ReactElement => {
  const { group, index, first, last } = pathContext
  const previousUrl = index - 1 === 1 ? "" : (index - 1).toString()
  const nextUrl = (index + 1).toString()
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageTitle=""
        pageDescription="The California Personal Injury Attorneys of Bisnar Chase represent injured plaintiffs. This legal blog is for local news and events relating to the personal injury field."
        isBlog={true}
      />
      <h1>Bisnar Chase - California Personal Injury Blog</h1>
      <BreadCrumbs location={location} />
      <BlogPostsWrapper>
        <ul>
          {group.map(({ node }: any) => (
            <li key={node.id}>
              <Link
                className="card-wrapper"
                to={
                  location.pathname === "/blog"
                    ? `blog/${node.slug}`
                    : node.slug
                }
              >
                <BlogCard data={node} />
              </Link>
            </li>
          ))}
        </ul>
      </BlogPostsWrapper>
      <StyledPagination>
        <div className="previous-link">
          <FaArrowAltCircleLeft className="arrow-left" />
          <NavLink
            test={first}
            url={location.pathname === "/blog/2" ? "/blog" : previousUrl}
            text="Previous"
          />
        </div>
        <div className="next-link">
          <NavLink
            test={last}
            url={location.pathname === "/blog" ? `blog/${nextUrl}` : nextUrl}
            text="Next"
          />

          <FaArrowAltCircleRight className="arrow-right" />
        </div>
      </StyledPagination>
    </LayoutDefault>
  )
}

export default BlogIndex

const BlogPostsWrapper = styled("section")`
  width: 100%;
  margin-bottom: 2rem;

  ul {
    margin: 0;
    list-style-type: none;
  }
`
const StyledPagination = styled("div")`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2rem;
  align-items: center;

  .previous-link,
  .next-link {
    display: flex;
    align-items: center;
  }
  .arrow-left,
  .arrow-right {
    font-size: 2rem;
    margin: 0.5rem;
  }
`
