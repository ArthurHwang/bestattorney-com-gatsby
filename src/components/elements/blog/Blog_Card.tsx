import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { elevation, formatWPtitle } from "../../utilities"

interface Props {
  data: {
    author: {
      name: string
    }
    content: string
    date: string
    excerpt: string
    categories: any
    featuredImage: any
    id: string
    modified: string
    slug: string
    title: string
  }
}

export const BlogCard: React.FC<Props> = ({ data }): ReactElement => {
  let featuredImage = null

  if (
    !data.featuredImage ||
    !data.featuredImage.node ||
    !data.featuredImage.node.localFile ||
    !data.featuredImage.node.localFile.childImageSharp
  ) {
    featuredImage = null
  } else {
    featuredImage = (
      <Img
        className="card-image"
        alt={data.title}
        fluid={data.featuredImage.node.localFile.childImageSharp.fluid}
        loading="lazy"
        fadeIn={true}
      />
    )
  }

  return (
    <StyledBlogCard>
      <h2 className="post-title">{formatWPtitle(data.title)}</h2>
      <div className="content-wrapper">
        {featuredImage}
        <div
          className="post-text"
          dangerouslySetInnerHTML={{ __html: data.excerpt }}
        />
      </div>
      <div className="post-metadata">
        <p className="post-author">
          <span>Posted By </span>
          <span className="post-author-name">{data.author.name} </span>{" "}
          <span> on {data.modified}</span>
        </p>
        <p className="post-category">
          Posted in: <span>{data.categories.nodes[0].name}</span>
        </p>
      </div>
    </StyledBlogCard>
  )
}

const StyledBlogCard = styled("div")`
  width: 100%;
  height: auto;
  border: medium double ${({ theme }) => theme.colors.grey};
  margin: 2rem 0;
  padding: 1.2rem 2rem 1.2rem;
  transition: 0.3s ease all;
  overflow: hidden;

  .post-metadata {
    display: flex;
    justify-content: space-between;
    margin-top: 1.5rem;
    padding-top: 0.5rem;
    align-items: flex-end;
    border-top: 1px solid ${({ theme }) => theme.colors.grey};
  }

  .post-category {
    font-size: 1.2rem;

    span {
      color: ${({ theme }) => theme.colors.secondary};
      font-weight: 600;
    }
  }

  .content-wrapper {
    display: grid;
    grid-gap: 2rem;
    grid-template-columns: 0.8fr 1.6fr;
    grid-template-rows: 200px;

    .post-text {
      line-height: 2;
      text-align: justify;
      overflow: hidden;

      @media (max-width: 1230px) {
        line-height: 1.5;
      }
      @media (max-width: 1024px) {
        line-height: 2;
      }

      @media (max-width: 777px) {
        line-height: 1.4;
      }
      @media (max-width: 720px) {
        line-height: 1.6;
      }
    }

    @media (max-width: 777px) {
      grid-template-columns: 1.5fr 2fr;
      grid-template-rows: 200px;
    }

    @media (max-width: 720px) {
      grid-template-columns: 1fr;
      grid-template-rows: 1fr;
    }
  }

  .post-author {
    font-weight: 600;
    font-size: 1.2rem;
    margin: 0;

    @media (max-width: 600px) {
      display: none;
    }

    .post-author-name {
      font-weight: 600;
    }
  }

  .post-title {
    border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
    font-size: 1.8rem;
    line-height: 1.4;
    padding-bottom: 0.5rem;
    margin-bottom: 1.6rem;
  }

  &:hover {
    transform: scale(1.004);
    ${elevation[3]};
  }
`
