// import Img from "gatsby-image"
import React, { ReactElement } from "react"
import styled from "styled-components"
import { LayoutBlogAMP } from "../../layouts/Layout_Blog-AMP"
import { formatWPmeta, formatWPtitle } from "../../utilities"
import { BreadCrumbs } from "../BreadCrumbs"
import { SEO } from "../SEO"
import folderOptions from "./folder-options"

interface Props {
  location: any
  pageContext: {
    title: string
    slug: string
    id: string
    content: string
    modified: string
    socialLink: string
    excerpt: string
    author: string
    categories: any
    imageSharp: {
      localFile: {
        childImageSharp: {
          fluid: {
            aspectRatio: number
            base64: string
            src: string
            srcSet: string
            sizes: string
          }
        }
      }
    }
  }
}

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

function removeNBSP(innerHTML: string): string {
  return innerHTML.replace(/<p>&nbsp;<\/p>/g, "")
}

const PostTemplate: React.FC<Props> = ({
  pageContext,
  location
}): ReactElement => {
  let featuredImage = null
  let socialImageURL = null
  const parsedInnerHTML = removeNBSP(pageContext.content)

  if (
    !pageContext.imageSharp ||
    !pageContext.imageSharp.localFile ||
    !pageContext.imageSharp.localFile.childImageSharp
  ) {
    featuredImage = null
    socialImageURL = null
  } else {
    socialImageURL = `https://bestatto-gatsby.netlify.app${pageContext.imageSharp.localFile.childImageSharp.fluid.src}`

    featuredImage = (
      <img
        src-set={pageContext.imageSharp.localFile.childImageSharp.fluid.srcSet}
        src={pageContext.imageSharp.localFile.childImageSharp.fluid.src}
        className="post-image"
        alt={pageContext.title}
      />
    )
  }

  return (
    <LayoutBlogAMP sidebar={true} {...FolderOptions}>
      <SEO
        pageTitle={formatWPtitle(pageContext.title)}
        pageDescription={formatWPmeta(pageContext.excerpt)}
        // socialImage will default to main homepage image if not supplied
        imageUrl={socialImageURL ? socialImageURL : null}
        isAmp={true}
      />
      <BlogPageWrapper>
        <h1>{formatWPtitle(pageContext.title)}</h1>

        <BreadCrumbs location={location} />

        <div className="post-metadata">
          <p className="post-author">
            <span>Posted By </span>
            <span className="post-author-name">{pageContext.author} </span>{" "}
            <span> on {pageContext.modified}</span>
          </p>
          <p className="post-category">
            Posted in: <span>{pageContext.categories[0].name}</span>
          </p>
        </div>
        <div className="post-content">
          {featuredImage}
          <p className="image-caption">{formatWPtitle(pageContext.title)}</p>
        </div>
        <br />
        <div dangerouslySetInnerHTML={{ __html: parsedInnerHTML }} />
      </BlogPageWrapper>
    </LayoutBlogAMP>
  )
}

export default PostTemplate

const BlogPageWrapper = styled("section")`
  width: 100%;

  .post-author {
    font-weight: 600;
    font-size: 1.2rem;
    margin-bottom: 0;

    .post-author-name {
      font-weight: 600;
    }

    @media (max-width: 600px) {
      display: none;
    }
  }

  .post-metadata {
    display: flex;
    justify-content: space-between;
    padding-top: 0.5rem;
    align-items: flex-end;
  }

  .post-category {
    font-size: 1.2rem;
    margin-bottom: 0;

    span {
      color: ${({ theme }) => theme.colors.secondary};
      font-weight: 600;
    }
  }

  .post-image {
    max-height: 600px;
  }

  .image-caption {
    font-size: 0.8rem;
    text-align: right;
    margin: 0;

    @media (max-width: 500px) {
      display: none;
    }
  }

  h1 {
    font-size: 2.5rem;
    margin-bottom: 0;
  }
`
