import React, { ReactElement } from "react"
import styled from "styled-components"
import { Link } from "src/components/elements/Link"
import { FaHome } from "react-icons/fa"

// helper function to insert slashes in-between rendered breadcrumbs
const insertBetween = (insertion: string | undefined, array: any[]): string[] =>
  array.reduce(
    (newArray, member, i, arr) =>
      i < arr.length - 1
        ? newArray.concat(member, insertion)
        : newArray.concat(member),
    []
  )

interface Props {
  location: {
    pathname: any
  }
}

export const BreadCrumbs: React.FC<Props> = ({ location }): ReactElement => {
  // split initial pathname into array of crumbs
  const pathCrumbs = location.pathname.split("/")

  // check if last item in array or includes amp, remove if true
  if (
    pathCrumbs[pathCrumbs.length - 1] === "" ||
    pathCrumbs[pathCrumbs.length - 1] === "amp"
  ) {
    pathCrumbs.pop()
  }

  // loop over pathCrumbs array and return Gatsby Link components
  const breadCrumbs: ReactElement[] = pathCrumbs.map(
    (crumb: string): JSX.Element => {
      // this function takes the incoming crumb and find where in pathCrumbs array it exists
      // it will loop and push into a new array up until it finds itself to construct its pathname
      function generatePath(pathCrumb: string): string {
        const pathsArray = []
        // get index of crumb
        const crumbIndex = pathCrumbs.indexOf(pathCrumb)

        for (let i = 0; i <= crumbIndex; i++) {
          pathsArray.push(pathCrumbs[i])
        }
        // adds slash between all items.
        // The first item in the array is an empty string however still adds the slash to use for gatsby internal link
        return pathsArray.join("/")
      }

      if (crumb === "") {
        return (
          <Link key="/" to="/">
            <FaHome
              style={{ position: "relative", top: "1.5px", marginRight: "7px" }}
            />
            Home
          </Link>
        )
      }

      // the last item of the array is always the active item, add active className here
      if (crumb === pathCrumbs[pathCrumbs.length - 1]) {
        return (
          <Link key={crumb} className="active" to={location.pathname}>
            {crumb}
          </Link>
        )
      }

      return (
        <Link key={crumb} to={generatePath(crumb)}>
          {crumb}
        </Link>
      )
    }
  )

  const transformedCrumbs: string[] = insertBetween(" / ", breadCrumbs) // insert slashes between array items

  return <StyledBreadCrumbs>{transformedCrumbs}</StyledBreadCrumbs>
}

const StyledBreadCrumbs = styled("div")`
  font-size: 1.2rem;
  margin-bottom: 2rem;

  .active {
    color: ${({ theme }) => theme.colors.accent};
    font-weight: 600;
  }

  a {
    color: #8e8e8e;
    font-size: 1.2rem;
    text-transform: uppercase;
    font-weight: 600;

    &:hover {
      color: ${({ theme }) => theme.links.hoverBlue};
    }
  }

  @media (max-width: 768px) {
    text-align: center;
  }
`
