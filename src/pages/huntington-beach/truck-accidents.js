// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/huntington-beach-truck-accident-lawyers-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Huntington Beach Truck Accident Lawyers - Semi-Truck Collision Attorneys"
        pageDescription="Injured in a semi-truck crash? Please call 949-203-3814. Our Huntington Beach truck accident attorneys are here to help injured plaintiffs receive fair compensation. Free consultations since and a 96% success rate. We can help you understand your legal rights."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Huntington Beach Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Huntington beach truck accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <strong> Huntington Beach Truck Accident Lawyers</strong> at Bisnar
          Chase can help gather the evidence that is required to hold negligent
          parties liable for your vehicle wreck.{" "}
        </p>
        <p>
          Injured victims can seek compensation for damages including medical
          expenses, lost wages, cost of hospitalization, rehabilitation and
          other related damages.{" "}
        </p>
        <p>
          The drunk or impaired truck driver and the company that negligently
          hired him or her should be held accountable for the losses you and
          your family have faced.{" "}
        </p>
        <p>
          Do you find yourself asking "who is the best attorney near me that I
          can turn to?"
        </p>
        <p>
          If you or a loved one has been injured by a negligent truck driver,
          the{" "}
          <Link to="/huntington-beach" target="_blank">
            {" "}
            personal injury
          </Link>{" "}
          law firm of Bisnar Chase can help you better understand your legal
          rights and options.{" "}
        </p>
        <p>
          Please call us at{" "}
          <strong> 949-203-3814 for a free consultation</strong> and
          comprehensive case assessment.{" "}
        </p>
        <h2>
          Huntington Beach Semi-Truck Collisions that Take Place Frequently
        </h2>
        <p>
          Big-rig accidents can be deadly. Semi-trucks weigh up to three
          thousand pounds and because of their weight, it is difficult for
          trucks to come to an immediate stop in case of an emergency. If truck
          companies, drivers and manufacturers become careless this can cause
          serious property damage and can even cost lives as well.{" "}
        </p>
        <p>
          <strong> Common causes of truck accidents include: </strong>
        </p>
        <ul>
          <li>
            <strong> Jackknife accidents: </strong>When an 18-wheel-truck
            suddenly swings it's trailer at a 90-degree angle this is known as
            "jackknifing". Jackknifing can take place if the weather is making
            driving conditions hazardous. If the wheels of a truck are locked
            and the highways are slippery due to snow or rain the trailer of the
            truck can swing uncontrollably. When a trucker is driving over a
            safe speed limit and suddenly brakes immediately this can also cause
            the cargo to swing at an angle.
          </li>
          <li>
            <strong> Rollovers:</strong> Truckers who speed at a curve have the
            potential to cause a rollover accident. In a study conducted by the{" "}
            <Link
              to="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3256782/"
              target="_blank"
            >
              {" "}
              US National Library of Medicine National Institutes of Health
            </Link>
            , 45% of rollover collisions took place due to a trucker speeding.
            One of the biggest factors of why rollovers take place is due to
            misjudgment. If a truck driver does not slow down when approaching a
            turn this can lead to a semi to flipping over.
          </li>
          <LazyLoad>
            <img
              src="../images/truck-accidents/huntington-beach-semi-truck-accident-lawyers.jpg"
              width="100%"
              alt="Trucker backing up"
            />
          </LazyLoad>
          <li>
            <strong> Backing collisions:</strong> Truck drivers who are careless
            when reversing can be labeled as negligent in a court of law. When
            backing up a trucker should do so slowly and become pay special
            attention to their surroundings. Even if a trucker takes precautions
            though they can still accidentally injure someone due to a person or
            car being in their blind spot. The blind spot of a 18 wheeler can be
            as large as 70 feet. Semi-truck drivers can avoid colliding into
            someone or damaging property by simply adjusting their mirrors.{" "}
          </li>
          <li>
            <strong> Dangerous cargo crashes:</strong> Cargo crashing into a
            vehicle can be fatal especially if the cargo that was being carried
            was hazardous. Some examples of dangerous cargo can include
            radioactive substances, poisonous materials, flammable objects or
            explosives. It is very common for a trucker to lose their life if
            they collide or rollover with flammable cargo.{" "}
          </li>
          <li>
            <strong> T-bone accidents: </strong>T-bone collisions commonly take
            place at intersections when a semi-truck is making a left-hand turn.
            Factors that contribute to a T-bone accident can include a driver
            texting and driving, running a red light or switching lanes
            abruptly. T-bone car accidents can be deadly especially if the
            impact occurs on the driver side of a four-door vehicle.{" "}
          </li>
        </ul>
        <h2>Who is Liable for your Injuries?</h2>
        <p>
          Semi-truck accident victims automatically assume that the person who
          would be accountable for their injuries and losses would be the truck
          driver. While that can be the case at times, other entities can also
          be liable for the damages a truck accident victim has faced. Entities
          that can also be held responsible for injuries and losses can be the
          trucking company, cargo company or the manufacturer of the big-rig.
        </p>
        <p>
          At times many trucking companies feel a need to cut corners and save
          money on upkeep. This can potentially lead to a semi truck collision.
          Parts of a truck that are important to check regularly are coolant
          leaks, oil fluid level for diesel exhaust and air filters.{" "}
        </p>
        <h2>Common Huntington Beach Big-Rig Accident Injuries</h2>
        <p>
          According to the National Highway Traffic Safety Administration, over
          23% of truck crashes leave victims with severe injuries. The{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts"
            target="_blank"
          >
            {" "}
            Federal Motor Carrier Safety Administration
          </Link>{" "}
          stated that over the years truck accidents had increased by 9%. With
          truck collisions on the rise, many have been disabled or the
          collisions were fatal.{" "}
        </p>
        <p>
          <strong>
            {" "}
            For those who survive common injuries that may be experienced are:
          </strong>
        </p>
        <ul>
          <li>
            <strong> Broken bones:</strong> Four-door motor vehicle drivers run
            the risk of suffering from multiple bone fractures due to the weight
            of a semi. Drivers also face multiple injuries due to the airbag
            deploying after an accident. Airbag accidents can leave a person
            with fractures and lacerations to the face. Many will need facial
            reconstruction surgery in order to fix a broken jaw or deep cut on
            the face.
          </li>
          <li>
            <strong> Traumatic brain injuries: </strong>Head injuries are one of
            the most severe injuries a{" "}
            <Link to="/huntington-beach/car-accidents">auto accident </Link>{" "}
            victim can face. Traumatic brain injuries are classified as any blow
            to the head that disturbs the brain from functioning regularly.
            Unlike most injuries, head injuries are dangerous because a strong
            blow to the head and the effects that follow can be long-lasting. If
            a person experiences trauma to the head it may be difficult for a
            person to retain information, verbalize feelings or it can cause a
            person to be suicidal.{" "}
          </li>
          <li>
            <strong> Spinal cord injuries</strong>: The spinal cord is one of
            the most sensitive parts of the body and if just one piece is
            disrupted it can be paralyzing. Spinal cord injuries are separated
            into four categories which include cervical spinal cord injuries,
            lumbar spinal cord injuries, thoracic spinal cord injuries and
            sacral spinal cord injuries. Each differs in which part of the body
            will be affected. For example, those who have suffered from a
            "sacral spinal cord injury" will be left with injuries that will
            affect the back of the thighs, buttocks hips and organs in the
            pelvic region.{" "}
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Different colored coins stacked on top of eachother"
            style={{
              backgroundImage:
                "url('../images/truck-accidents/huntington-beach-truck-collision-lawyers-text-header.jpg')"
            }}
          >
            <h2>Various Types of Compensation You Can Win </h2>
          </div>
        </LazyLoad>

        <p>
          After a truck accident, many injury victims are left with hefty
          medical expenses and have lost out on wages due to being out of work
          for weeks or even months at a time. At times insurance companies may
          not cover all the costs for the damages. If you were involved in a
          semi-truck collision, contact one of our best Huntington Beach truck
          accident attorneys to learn more about the compensation you can
          receive from your crash.{" "}
        </p>
        <p>
          <strong> You can be compensated for expenses such as: </strong>
        </p>
        <ul>
          <li>Medical bills</li>
          <li>Property damages</li>
          <li>Pain and suffering </li>
        </ul>
        <h2>Contact a Huntington Beach, Ca Truck Accident Lawyer</h2>
        <p>
          If a drunk or impaired truck driver caused your truck accident, the
          driver and his or her employer can be held criminally and civilly
          liable for the injuries and damages caused. Injured victims can seek
          compensation for damages including medical expenses, loss of wages,
          cost of hospitalization, rehabilitation and other related damages.
          Families of deceased victims can file a wrongful death accident claim
          seeking compensation for damages such as lost future income, funeral
          expenses and medical costs.{" "}
        </p>
        <p>
          The experienced Huntington Beach truck accident attorneys at Bisnar
          Chase can help gather the evidence that is required to hold the
          negligent parties liable. The drunk or impaired truck driver and the
          company that negligently hired him/her should be held accountable.{" "}
        </p>
        <p>
          Immediately call an experienced and reputable Orange County Personal
          Injury Law Office of Bisnar Chase for a{" "}
          <strong> free consultation at 949-203-3814.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
