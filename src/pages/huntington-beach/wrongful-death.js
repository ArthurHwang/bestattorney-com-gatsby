// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "wrongful-death/huntington-beach-wrongful-death-lawyers-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Huntington Beach Wrongful Death Lawyers - Orange County, CA"
        pageDescription="Call 949-203-3814 for high-rated wrongful death attorneys in Huntington Beach, California. We specialize in catastrophic injuries and hold wrongdoers accountable for the injuries they cause. Free consultation, no win, no fee, guaranteed."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Huntington Beach Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Huntington Beach Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Coping with the death of a family member is one of life's most
          difficult challenges. At Bisnar Chase, our{" "}
          <strong> Huntington Beach Wrongful Death Lawyers</strong> are
          passionate about holding wrongdoers accountable for their negligence.
          If you think your loved one's death may be a wrongful death lawsuit,
          our attorneys will unearth the facts and find out what really happened
          immediately.{" "}
        </p>
        <p>
          For aggressive litigation, straight talk, and honest answers,{" "}
          <strong> Call 949-203-3814</strong> now to take advantage of our{" "}
          <strong> free consultation</strong> and find out what your options
          are.{" "}
        </p>
        <p>
          Contact the{" "}
          <Link to="/huntington-beach" target="_blank">
            {" "}
            Personal Injury
          </Link>{" "}
          Law Firm of Bisnar Chase to gain free legal advice.{" "}
        </p>
        <h2>5 Different Types of Orange County Wrongful Death Claims</h2>
        <p>
          The legal term known as "wrongful death" refers to a person losing
          their life due to someone else's carelessness. A wrongful death can
          effect a family tremendously emotionally and financially. Costs such
          as funeral expenses and medical bills that were accumulated before a
          loved one's death can be expensive. If the deceased was the primary
          income producer of the family, those family members can struggle
          financially to get by.
        </p>
        <p>
          <strong>
            {" "}
            Family members who suffer the loss of a loved one usually file
            wrongful death claims such as:{" "}
          </strong>
        </p>
        <ol>
          <li>
            <strong> Car accidents:</strong> Many traffic collision can be
            avoided if drivers behind the wheel adhered to the rules of the
            road. One common behavior that leads to car accidents is distracted
            driving. An estimated amount of over 390,000 injuries take place
            annually due to drivers texting and driving. In one year, exactly
            263 adolescents lost their lives at the hands of a distracted
            driver. Answering a text is not worth seriously injuring someone or
            losing a life. The majority of wrongful deaths that occur in
            Huntington Beach are a result of auto collisions. According to a
            recent{" "}
            <Link to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system">
              California Statewide Integrated Traffic Records System{" "}
            </Link>{" "}
            (SWITRS), thirteen people lost their lives in Huntington Beach car
            accidents. Of those who suffered fatal injuries, 3 were pedestrians,
            1 was on a motorcycle, and 4 were alcohol-related.{" "}
          </li>
          <li>
            <strong> Work injuries:</strong> Employees that are in the
            construction industry can lose their lives by being struck by an
            object, electrocutions or slip and falls. Many workers are also
            killed at the hands of faulty equipment. Employers have the duty to
            make sure that employees are safe at all times on the job. Employers
            who fail to do so can be held accountable for damages.{" "}
          </li>
          <li>
            <strong> Motorcycle collisions: </strong>Bike accidents are one of
            the most dangerous that a person can be involved in. Motorcycles do
            not shield people from objects in the road or four-door vehicles.
            Many motorcycle injury victims can end up with a traumatic brain
            injury that will leave families with the decision whether or not
            their loved one should stay on life support. Family members who make
            the difficult choice to take their loved ones off of life support at
            times are left emotionally damaged. Speak to a wrongful death lawyer
            in Huntington Beach to learn more on the compensation you can
            receive from losing a family member.{" "}
          </li>
          <li>
            <strong> Pedestrian accidents: </strong>The{" "}
            <Link
              to="https://www.cdc.gov/motorvehiclesafety/pedestrian_safety/index.html"
              target="_blank"
            >
              {" "}
              Centers for Disease Control and Prevention{" "}
            </Link>{" "}
            reported that in one year, 5,987 pedestrians lost their lives in
            traffic collisions. The primary victims of pedestrian accidents were
            children and senior citizens. The CDC reported that over 48% of
            pedestrian deaths occurred due to impaired driving. Motorists who
            have an alcoholic level of .08 or more are putting crosswalks and
            fellow motorists at risk.{" "}
          </li>
          <li>
            <strong> Medical malpractice:</strong> Any patient that is under the
            care of a medical professional is entitled to the best treatment
            possible. Doctors who are negligent during surgery or misdiagnose a
            patient can be held accountable for wrongful death. Nurses can also
            be responsible for a patients death. For example, if a nurse gives
            an elderly resident the wrong medication in a senior living home
            then this can be seen as carelessness on behalf of the nurse of the
            senior living facility.{" "}
          </li>
        </ol>

        <LazyLoad>
          <img
            src="../images/wrongful-death/huntington-beach-wrongful-death-attorneys.jpg"
            width="100%"
            alt="A woman with a rose at burial site"
          />
        </LazyLoad>
        <h2>Who Can File a Wrongful Death Claim? </h2>
        <p>
          There are two factors that should be present in order to have a strong
          foundation for a wrongful death claim. First that the victim died due
          to the negligence of another and that the loss of that individual
          caused the surviving family members great pain and suffering.{" "}
        </p>
        <p>
          People that can file a wrongful death claim include surviving
          children, spouses and the parents of the deceased.{" "}
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CCP&sectionNum=377.60."
              target="_blank"
            >
              {" "}
              Code of Civil Procedure 377.60{" "}
            </Link>
            states
          </strong>{" "}
          "
          <em>
            The decedent’s surviving spouse, domestic partner, children, and
            issue of deceased children, or, if there is no surviving issue of
            the decedent, the persons, including the surviving spouse or
            domestic partner, who would be entitled to the property of the
            decedent by intestate succession.
          </em>
          "
        </p>
        <p>
          The process of the collection of assets can change if the deceased
          left a will. An executor will then be appointed to announce what
          assets will go to which person.{" "}
        </p>
        <h2>Damages You Can Receive in a Wrongful Death Lawsuit</h2>
        <p>
          When you file a wrongful death claim you have the potential to win
          compensation for economic and noneconomic damages.
        </p>
        <p>
          Economic damages involve losses that have a monetary value attached to
          them. Some examples of economic damages can be the loss of future
          earnings the deceased would have received, damages to property,
          medical bills and burial or funeral expenses.
        </p>
        <p>
          A surviving family member or spouse can also claim noneconomic damages
          such as pain and suffering. Pain and suffering can be difficult to
          determine in terms of compensation though. Pain and suffering is
          usually measured on how the spouse or loved one has been affected
          emotionally from the loss of their loved one.{" "}
        </p>
        <p>
          Serving as a Huntington Beach wrongful death attorney is a difficult
          task; the heartache and rage that our clients experience as they
          struggle to recover from their loved one's death is contagious and we
          cannot help but develop a connection with each one of our clients.
          When you sign with Bisnar Chase Personal Injury Attorneys, you become
          a client for life and you will receive a very personal experience from
          very passionate attorneys.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="A rose in front of a casket"
            style={{
              backgroundImage:
                "url('../images/wrongful-death/huntington-beach-wrongful-death-attorneys-text-header.jpg')"
            }}
          >
            <h2>Wrongful Death Attorneys in Huntington Beach</h2>
          </div>
        </LazyLoad>
        <p>
          If you have lost a loved one as a result of negligence, contact one of
          our wrongful death lawyers in Huntington Beach to take advantage of
          our free professional evaluation. Our success rate and history of high
          client satisfaction will give you the peace of mind to concentrate on
          your treatment, rather than your monetary recovery.
          <strong> Call for a free consultation at 949-203-3814. </strong>{" "}
        </p>
        <p>
          Our attorneys will not complete your wrongful death lawsuit until
          every potential avenue of compensation has been investigated.{" "}
        </p>
        <p>
          Contact the Personal Injury Law Firm of Bisnar Chase for a free case
          analysis.{" "}
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
