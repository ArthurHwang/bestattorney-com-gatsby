// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/huntington-beach-personal-injury-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Huntington Beach",
      population: 197575,
      totalAccidents: 8490,
      intersection1: "Magnolia & Atlanta",
      intersection1Accidents: 81,
      intersection1Injuries: 75,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 184.5,
      city1Name: "Fountain Valley",
      city1Index: 153.0,
      city2Name: "Westminster",
      city2Index: 229.8,
      city3Name: "Costa Mesa",
      city3Index: 214.3,
      city4Name: "Garden Grove",
      city4Index: 181.0
    },
    locationBox3: {
      intersection2: "Goldenwest & Edinger",
      intersection2Accidents: 115,
      intersection2Injuries: 65,
      intersection2Deaths: 0,
      intersection3: "Magnolia & Adams",
      intersection3Accidents: 100,
      intersection3Injuries: 63,
      intersection3Deaths: 0,
      intersection4: "Edinger & Springdale",
      intersection4Accidents: 93,
      intersection4Injuries: 50,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Huntington Beach Personal Injury Lawyers - O.C. Accident Attorneys"
        pageDescription="Contact the top rated personal injury attorneys in Huntington Beach if you've been injured in a car accident or other serious injuries. Free consultation, 96% success rate. Serving Orange County since 1978"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Huntington Beach Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Personal injury attorneys in Huntington Beach"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have sustained an injury and need to know the facts of your
          potential claim, call the Huntington Beach Personal Injury Lawyers of
          Bisnar Chase at
          <strong> 949-344-2968 for a free consultation</strong>.
        </p>
        <p>
          We've been helping Huntington Beach and Orange County clients receive
          the compensation they need to recover from their serious accident for
          over 40 years. At Bisnar Chase, we provide every client with an
          experience designed plan to help them recover financially, physically,
          and emotionally. We have a large staff of highly trained legal
          professionals who have experience handling cases in a multitude of
          practice areas.
        </p>
        <p>
          Looking for a personal injury lawyer
          <strong> nearby 92605 area?</strong>
        </p>
        <p>
          We offer a free no obligation case evaluation to every accident
          victim. So don't wait until it is too late to receive legal advice for
          your claim. <Link to="/orange-county/contact">Contact us</Link> today
          at our Newport Beach offices near Huntington Beach.
        </p>
        <h2>7 Common Huntington Beach Accident Claims</h2>
        <p>
          Personal injury claims can range from being minor to fatal. Many
          personal injury victims who have experienced serious accidents do not
          know if they even have a strong enough case that will earn them the
          compensation they need to recover.{" "}
        </p>
        <p>
          Huntington Beach personal injury claims fall into an array of
          categories. There are specific accident claims that happen quite often
          in this specific region though.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Frequent incidents that are usually claimed in Huntington Beach
            include:{" "}
          </strong>
        </p>
        <ol>
          <li>
            <strong> Vehicle Collisions:</strong> According to{" "}
            <Link
              to="http://www.city-data.com/accidents/acc-Huntington-Beach-California.html"
              target="_blank"
            >
              {" "}
              City-Data.com{" "}
            </Link>{" "}
            there have been 14 fatalities reported due to Huntington Beach{" "}
            <Link to="/huntington-beach/car-accidents.html">
              car accidents.{" "}
            </Link>{" "}
            The cause of these wrongful death incidents? People who have been
            held accountable for these fatalities were drunk drivers. Coastline
            Behavioral Center reported that{" "}
            <Link
              to="https://coastlinerehabcenters.com/huntington-beach-dui-capital-california/"
              target="_blank"
            >
              {" "}
              Huntington Beach is the DUI capital of California
            </Link>
            . Although authorities have taken measures to decrease the number of
            DUI-related car accidents in Huntington Beach today, there are still
            drivers who prove to be reckless.{" "}
            <Link
              to="https://cdan.nhtsa.gov/SASStoredProcess/guest"
              target="_blank"
            >
              {" "}
              Traffic Safety Performance{" "}
            </Link>{" "}
            has indicated that California has experienced 3,000 car accidents
            just in one year. Experts say that the best option to take after a
            night out of drinking is to stay at a trusted friend's home for the
            night or contact a ride-sharing service.{" "}
          </li>
          <li>
            <strong> Pedestrian Accidents:</strong> Huntington Beach, California
            is no stranger to crosswalk accidents. ABC7 news reported that "{" "}
            <Link
              to="https://abc7.com/pedestrian-killed-in-huntington-beach-crash/5182198/"
              target="_blank"
            >
              A pedestrian was killed in a Huntington Beach crash
            </Link>
            ." The victim was identified as a woman in her 50's. One of the main
            causes of a pedestrian being struck by a vehicle is due to
            distracted driving. Motorists who are partaking in activities as
            small as reaching to change the radio or eating can cause a serious
            collision. There are instances when a walker can be at fault though.
            Just like drivers pedestrians do have a responsibility to keep
            themselves safe. Pedestrians who ignore crosswalk signals have an
            increased risk of being injured or losing their life. Some{" "}
            <Link
              to="https://www.nhtsa.gov/road-safety/pedestrian-safety"
              target="_blank"
            >
              {" "}
              pedestrian safety tips{" "}
            </Link>{" "}
            that will help prevent an accident include making eye contact with a
            driver, only crossing on marked areas and wearing bright colors so
            they may be noticeable while walking at night.{" "}
          </li>
          <li>
            <strong> Truck crashes:</strong> Truck accidents can be devastating
            and because of their size can cause a huge amount of damage to
            property, fellow drivers and even the trucker themselves.
            Twenty-six-year-old Zachary John Gillespie crashed his 2006 Nissan
            Titan pickup into a power pole and wall. He was not wearing a
            seatbelt.{" "}
            <Link
              to="https://patch.com/california/losalamitos/man-dies-days-after-crash-huntington-beach"
              target="_blank"
            >
              {" "}
              He died days later after the crash
            </Link>
            . Many truckers underestimate the power and force of a semi or large
            vehicle. The average weight of a pick-up truck can be up to 3,000
            pounds which is one thousand more pounds than a regular four-door
            vehicle. When operating a large vehicle never be too close to cars
            and always decrease your speed when turning.{" "}
          </li>
          <li>
            <strong> Employment-related injuries: </strong>Work injuries cannot
            only compromise an accident victim's health but also their ability
            to provide financially for themselves in the future. Many families
            also suffer from losing their loved one. OSHA (
            <Link
              to="https://www.osha.gov/oshstats/commonstats.html"
              target="_blank"
            >
              {" "}
              occupational safety and health administration{" "}
            </Link>
            ) reported that in one year, 5,147 employees died due to job-related
            injuries. The industries that contained the most injuries and deaths
            were construction, transportation and warehouse-related occupations.
            If your employer does not have workers compensation coverage than
            you may have the potential to file a claim for a Huntington Beach
            personal injury.{" "}
          </li>
          <li>
            <strong> Nursing home abuse:</strong> There are over 50
            assisted-living facilities in Huntington Beach, CA alone. Elderly
            citizens and their loved ones trust and rely on a nursing home to
            properly care for their family members. When they fail to do so, the
            negligent staff members or abusers need to be brought to justice. If
            you suspect that your loved one is being harmed in a nursing home
            you can file a Huntington nursing home abuse claim on their behalf.
            You can file a claim for your family member if you believe that they
            are being abused emotionally, physically or financially (extortion).
          </li>
          <LazyLoad>
            <img
              alt="A dog biting someone's jacket"
              width="100%"
              title="personal injury lawyers in Huntington Beach, CA"
              src="../images/personal-injury/huntington-beach-personal-injury-lawyer-dog-bites.jpg"
            />
          </LazyLoad>
          <li>
            <strong> Dog bites: </strong>According to the Orange County
            Register, dog bites are on the rise. In one year, reports indicated
            that 2,384 bites occurred. Dog bites can be dangerous because
            animals have the ability to rupture the skin and if the dog is not
            vaccinated, spread diseases. Dogs can also spread parasites to
            humans as well. This can cause a person to experience symptoms such
            as fever, abdominal pain or diarrhea. If your child has been bitten
            by a dog immediately after the incident has occurred seek medical
            attention. Some preventative measures you can take to protect your
            child from being attacked by an animal can include constant
            supervision and prohibiting your child from approaching a dog they
            do not know.
          </li>
          <li>
            <strong> Wrongful deaths: </strong>A{" "}
            <Link to="/huntington-beach/wrongful-death">wrongful death </Link>{" "}
            can take place for a multitude of reasons. Common wrongful death
            claims can involve a child drowning in a pool, a person dying due to
            a negligent medical professional or a defective product. Whatever
            the case may be a surviving family member has the right to file a
            lawsuit on their loved one's behalf. Potential claimants can be a
            spouse, child or parent of the deceased. A family member who has
            passed due to someone else's carelessness can gain compensation for
            funeral expenses, earnings the deceased would have made if they were
            still alive and medical bills that accumulated before a loved one's
            death.{" "}
          </li>
        </ol>
        <h2>Southern California Statutes of Limitations </h2>
        <p>
          Accident victims who have been catastrophically injured due to someone
          else's negligence have two years to file a personal injury claim. The
          California Statute of Limitations states that negligent parties that
          were at fault in an accident are responsible for the losses the injury
          victims have faced.{" "}
        </p>
        <p>
          <strong>
            {" "}
            To receive full compensation for a personal injury there are three
            vital factors that need to be proven in the court of law
          </strong>
          .{" "}
        </p>
        <p>
          <strong> Duty of care:</strong> To prove negligence in a personal
          injury case one of the primary elements that needs to be present is
          that the careless party had an obligation to the injury victim. An
          example of a party that had a responsibility to the victim could be a
          doctor, an employer or a commercial truck driver.{" "}
        </p>
        <p>
          <strong> Breach of duty:</strong> If the party that was responsible
          for keeping the claimant safe and failed to do so, this can be used as
          a strong basis for a personal injury lawsuit. Breach of duty refers to
          the defendant putting others at risk due to negligence or
          recklessness. When a driver decides to operate a motor vehicle under
          the influence of a substance they are creating a dangerous situation
          for fellow motorists. When a grocery store manager fails to put a
          "caution" sign on a wet floor after being informed this can lead a
          customer to slip and fall.{" "}
        </p>
        <p>
          <strong> Causation:</strong> Lastly, what would have to be absolutely
          proven is that the at-fault party's actions led to your injuries. If a
          person is proved to be accountable legally for the damages you have
          faced, then they will have no choice but to compensate you for your
          losses. When you speak to a personal injury attorney in the{" "}
          <strong> 92605</strong> Huntington Beach area you will be able to gain
          insight on how much you can potentially win for your accident claim.{" "}
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="X-rays of of a personal head injury"
            style={{
              backgroundImage:
                "url('../images/personal-injury/huntington-beach-accident-attorneys-brain-text-header.jpg')"
            }}
          >
            <h2>Typical Accident Injuries in Huntington Beach</h2>
          </div>
        </LazyLoad>

        <p>
          Unfortunately, hundreds of people suffer personal injuries within
          Huntington Beach on a yearly basis, many of whom do not receive the
          proper information to make an educated decision on how to move
          forward. Too often, our Huntington Beach personal injury lawyers speak
          with victims who have made detrimental mistakes while trying to
          properly handle their case and the results can be as devastating as
          the injuries.
        </p>
        <p>
          Bisnar Chase's personal injury attorneys in Huntington Beach have over
          40 years of experience earning millions of dollars for the physical
          and emotional repercussions accident victims have faced.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Some of the different types of injuries that we have gained recovery
            for include injuries that involve:{" "}
          </strong>
        </p>
        <ul>
          <li>
            <strong> Internal bleeding:</strong> Internal bleeding is one of the
            most dangerous injuries one can experience in an accident. This type
            of injury is not initially recognizable and can worsen within hours.
            Common causes of internal bleeding can involve a person being
            penetrated suddenly by an object leading to the crushing or tearing
            of the blood vessels. If blood vessels in a vital part of the body,
            such as an organ are ruptured a person can bleed to death.{" "}
          </li>
          <li>
            <strong> Fractured bones: </strong>In accidents if a injury victim
            suddenly experiences a slip and fall or is hit by a heavy object
            they can potentially obtain broken bones. In nursing homes, elderly
            residents must be watched carefully due to their fragile physical
            condition. Slipping and falling for a senior does not just result in
            a bruise. A senior citizen experiencing a slip and fall can lead to
            internal bleeding and many can even servilely fracture their pelvic
            area.{" "}
          </li>
          <li>
            <strong> PTSD:</strong> Nightmares, sudden emotional outbursts or
            severe anxiety are one of the many symptoms that people can possibly
            squire after an accident. Professional help is key to recovery but
            many do not have the medical insurance or income to seek this type
            of treatment. If you or someone you know is exhibiting these types
            of behaviors after an accident they may be able to receive the
            medical attention they need by filing a personal injury claim.{" "}
          </li>
          <li>
            <strong> Spinal cord injuries:</strong> Every year over 11,000
            spinal cord injuries take place in the United States. One of the
            most common areas in the spine that is damaged due to blunt trauma
            is the vertebral column. The vertebral column is the part of the
            spine that transmits messages from the nervous system to the brain.
            People who have suffered damage to the vertebral colon can become
            semi or entirely paralyzed. Victims who had not been partially or
            completely paralyzed in an accident can experience extreme pain and
            discomfort if they have been diagnosed with a herniated disc. A
            herniated disc is when the rubbery cushion that is between the bones
            in the spine is compromised causing the victim to be in excruciating
            pain.{" "}
          </li>
          <li>
            <strong> Head trauma: </strong>The American Association of
            Neurological Surgeons has reported that 500,000 deaths take place
            each year due to traumatic brain injuries. Research has also
            concluded that an estimated 50%-70% of head injuries were caused by
            motor vehicle accidents. If you or a loved one has been involved in
            a major wreck there are predominate symptoms that you need to look
            out for. Symptoms of a traumatic brain injury can include dilated
            pupils, dizziness, confusion, vomiting or ringing in the ears.{" "}
          </li>
          <li>
            <strong> Cuts:</strong> People who were involved in a road collision
            can be left with serious cuts and scrapes due to the debris from the
            vehicle or being dragged across the highway. In severe cases, skin
            can be torn from the body. Surgery for skin damaged on the face or
            body can be costly and can lead a person being out of work for
            months so they may recover. Do not try to treat a cut or scrape
            yourself. Medical professionals have the ability and tools to take
            debris such as gravel and glass out of the wound so the wound can
            properly heal.{" "}
          </li>
          <li>
            <strong> Lost or chipped teeth: </strong>Injury victims do not have
            to suffer from the embarrassment that comes with missing or chipped
            teeth after an accident. After an incident many people think that
            teeth are solely cosmetic and may not know that they can receive
            compensation for dental reconstruction. Missing teeth can leave
            people to not chew properly and may cause extensive damage to the
            jaw for those who grind their teeth at night. Not only that but
            missing teeth can also lead a person to be depressed. When you hire
            a personal injury lawyer in Huntington Beach, CA you will be able to
            potentially be able to gain the recovery you would need for dental
            reconstruction.{" "}
          </li>
        </ul>
        <h2>What Does the Term" No Win, No Fee" Mean?</h2>
        <p>
          When a law firm states that "if there is no win, there is no fee" this
          is stating that if the attorneys on your case do not win you any
          compensation you do not have to pay them for their services. This type
          of offer that is available by a personal injury law firm is known as a
          contingency fee.
        </p>
        <p>
          Throughout your case in order to win you as much compensation as
          possible your Huntington Beach personal injury lawyer will need to
          compile a team of paralegals and investigators for your injury case.
          These expenses are known as litigation costs. Your recovery will not
          only pay for your losses but it will also compensate for the costs
          that accumulated for the law firm as well.{" "}
        </p>
        <LazyLoad>
          <img
            alt="A driver taking a photo of the damage done to his car after an auto collision"
            width="100%"
            title="Huntington Beach accident lawyers"
            src="../images/personal-injury/personal-injury-lawyers-huntington-beach-fault.jpg"
          />
        </LazyLoad>
        <h2>Orange County Shared Fault Laws</h2>
        <p>
          One of the most important questions in a personal injury case is "who
          was responsible for the accident?" Once this question is answered and
          proven the claims process will be easier to maneuver through.{" "}
        </p>
        <p>
          What if both parties were at fault for an accident or injury though?
          Can both parties file for compensation for their injuries as well?
        </p>
        <p>
          The state of California enforces a comparative law for all parties and
          all possible circumstances that can take place in an accident.
          Comparative fault states that both people who were involved in the
          incident have the ability to file for compensation. Different states
          vary with comparative negligence in terms how much each party will
          receive for their injuries.{" "}
        </p>
        <h2>Damages You Can Recover for Your Accident Claim</h2>
        <p>
          The compensation you receive for the damages you have after an
          accident are paid by the negligent party (defendant) insurance
          company. The amount that you will receive for your personal injury
          case will depend on the number of injuries that you have experienced
          after the incident.{" "}
        </p>
        <p>
          There are two types of damages you can receive after a personal injury
          which are compensatory and punitive damages.
        </p>
        <p>
          Compensatory damages are the physical losses that a person has
          accumulated after a wreck which has an exact dollar amount attached.
          This can include medical bills, lost wages or property damage.
          Punitive damages take longer and are more complex to determine.
          Punitive damages that are often awarded by insurance companies can
          encompass emotional pain and suffering a person has experienced since
          the collision.{" "}
        </p>
        <h2>Our Huntington Beach Personal Injury Lawyers Can Help </h2>
        <p>
          If you have suffered personal injuries in Huntington Beach due to
          beach accidents, bicycle accidents, auto collisions, premises
          liability or a{" "}
          <Link to="/huntington-beach/motorcycle-accidents.html">
            motorcycle accident
          </Link>
          , it is to your benefit to consult a Huntington Beach personal injury
          law firm.{" "}
        </p>
        <LazyLoad>
          <img
            src="../images/personal-injury/huntington-beach-lawyers.jpg"
            width="100%"
            alt="A lawyer speaking to a client in their law office "
          />
        </LazyLoad>
        <p>
          Your accident attorney will explain the legal issues of your personal
          injury case and fight for damages owed to you. While there are many
          personal injury and accident lawyers claiming to have your best
          interests at heart, it is helpful to keep in mind that the best law
          firms will always offer a free consultation on your case.
        </p>
        <p>
          Law firms with strong records of successfully handling personal injury
          lawsuits meantime may also offer a no-fee guarantee meaning that you
          don't have to pay them unless they win your case.
        </p>
        <p>
          If you need help immediately, call one of our expert personal injury
          lawyers now at 949-344-2968 and we will schedule a consultation the
          same day or by the next business day at the latest.
        </p>
        <p>
          <strong> Visit our at our O.C. law firm near 92605.</strong>
        </p>
        <p>
          <strong> Call 949-344-2968 </strong>
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
