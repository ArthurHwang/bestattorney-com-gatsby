// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/huntington-beach-motorcycle-accident-lawyers-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Huntington Beach Motorcycle Accident Lawyers -Bike Crash Attorneys"
        pageDescription="Call 949-203-3814 for the highest-rated Huntington Beach motorcycle accident attorneys. If you have experienced injuries from a bike accident contact the law firm of Bisnar Chase for a no-obligation legal consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Huntington Beach Motorcycle Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Huntington beach motorcycle accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Finding the best{" "}
          <strong> Huntington Beach Motorcycle Accident Lawyer</strong> can be
          stressful and can seem like a daunting process. Who do you turn to
          after you have experienced serious injuries or have lost a loved one
          in a bike collision? 
        </p>
        <p>
          The <Link to="/huntington-beach">personal injury </Link> attorneys at
          Bisnar Chase are here for you. Our legal representation has won
          clients over <strong> $500 million in compensation</strong>. Our law
          firm has held a{" "}
          <strong>
            {" "}
            96% success rate at winning accident cases for over 40 years
          </strong>
          . 
        </p>
        <p>
          When you hire us we will represent you under a contingency fee basis,
          meaning if we don't win your case you don't have to pay. 
        </p>

        <p>
          Call us at <strong> 949-203-3814 for a free consultation</strong> for
          your Huntington Beach accident or wrongful death claim.
        </p>
        <h2>Huntington Beach Car and Motorcycle Accident Statistics</h2>
        <p>
          Many intersections are prone to serious-injury accidents and have been
          found to create blind spots putting motorcycle riders in serious
          danger. Motorcycle and bicycle accidents are not uncommon in
          Huntington Beach, for this, locals are extra cautious when they cross
          streets with a history of collisions. 
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.chp.ca.gov/traffic" target="_blank">
            {" "}
            California Statewide Integrated Traffic Records System
          </Link>{" "}
          (SWITRS), there were 13 people killed and 993 injured in Huntington
          Beach car accidents, which puts Huntington as one of the top 3 cities
          for fatal car accidents in Orange County. Of those who suffered fatal
          injuries, 4 were alcohol-related, 3 were motorcyclists and 1 was a
          bicyclist. 
        </p>
        <p>
          Tourists can cause a lot of collisions in Huntington Beach, but drunk
          driving car accidents are by far the biggest threat. 
        </p>
        <p>
          The{" "}
          <Link
            to="https://www.ocregister.com/2011/08/13/police-motorcyclist-injured-after-being-struck-by-dui-suspect/"
            target="_blank"
          >
            {" "}
            Orange County Register
          </Link>{" "}
          reported that a driver by the name of Jesse Rodriguez was driving
          drunk in a Chevy Silverado when he struck a man riding a
          Harley-Davidson on Pacific Coast Highway near Goldenwest Street. The
          motorcyclist sustained serious injuries and was rushed to a local
          hospital while the drunk driver fled the scene and was later arrested.
          Instances such as these are not uncommon in this area; Main Street is
          in close proximity to this location and is notorious for being the
          starting line for drunk drivers. {" "}
        </p>
        <LazyLoad>
          <img
            src="../images/motorcycle-accidents/huntington-beach-motorcycle-accident-lawyers-bike-on-road.jpg"
            width="100%"
            alt="motorcycle laying in the middle of the street"
          />
        </LazyLoad>
        <h2>Common Causes of Bike Collisions</h2>
        <p>
          Motorcycle accidents are far more dangerous than car collisions
          because unlike a four-door vehicle, do not offer much protection for
          the rider. If a motorcyclist takes measures to ride safely though they
          can prevent themselves from being in an accident. 
        </p>
        <p>
          In many instances, there is nothing you can do to prevent an accident
          or motorcycle injuries, not even if you are the police.
        </p>
        <p>
          Reports stated that in one year, a police officer and a driver were
          injured in a Huntington Beach motorcycle accident when they collided
          at the intersection of Beach Boulevard and Adams Avenue. Both parties
          were transported to a local hospital. 
        </p>
        <p>
          Intersection accidents are not the only common cause of bike crashes
          though.
        </p>
        <p>
          <strong> Other common causes include</strong>: 
        </p>
        <ul>
          <li>
            <strong> Alcohol usage</strong>: At times it is not a drunk driver
            that causes a motorcycle or{" "}
            <Link to="/huntington-beach/car-accidents">car accident</Link>.
            Motorcyclists can also be the ones responsible for a collision. The{" "}
            <Link
              to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812148"
              target="_blank"
            >
              {" "}
              National Highway Traffic Safety Administration{" "}
            </Link>{" "}
            reported that annually, 40% of motorcycle riders die due to being
            under the influence while operating a motorcycle. Studies have
            indicated that just .02 oz of alcohol can impair a cyclist while
            riding. Do not drink and ride. 
          </li>
          <li>
            <strong> Speeding</strong>: Reckless driving can lead to deadly
            accidents. Excessive speeding can not only cause major vehicle
            damages but it can also cause a cyclist to be dragged through the
            road. Amatuer motorcyclists often do not know how to control a
            motorbike or underestimate a turn when riding. It is important that
            a rider adheres to the speed limits of a highway, not to prevent
            themselves from receiving citations but to also keep themselves
            safe. 
          </li>
          <li>
            <strong> Unsafe road conditions</strong>: Riders need to be cautious
            when there are dangerous weather conditions and also when there is
            hazardous debris on the road. Motorcyclists frequently are involved
            in accidents due to poor construction of the road as well. Hazards
            for bike riders may include wet surfaces, animals crossing, railroad
            crossing or uneven gravel. Experts strongly suggest wearing
            protective gear on every ride you take.
          </li>
          <li>
            <strong> Head-on collisions</strong>: Motorcycle accidents that
            involve a car hitting the front of a bike often lead to deadly
            results. Head on collisions happen for two reasons: distracted
            driving and impaired driving. If you have been seriously injured due
            to a negligent driver call the Huntington Beach Motorcycle Accident
            Lawyers of Bisnar Chase at 949-203-3814 for a free case analysis. 
          </li>
          <li>
            <strong> Left-hand turns</strong>: Over 36% of motorcycle crashes
            occur due to left-hand turns. Left-hand crashes can be avoided if a
            driver is aware of his/her surroundings. A motor vehicle driver can
            also prevent a collision if they perform an action as small and
            simple as turning on his/her blinker on. If you are a cyclist,
            trying to beat a car at a turn will always put your life at risk. {" "}
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="a stack of gold coins"
            style={{
              backgroundImage:
                "url('../images/motorcycle-accidents/huntington-beach-motorcycle-accident-lawyer-text-header-image.jpg')"
            }}
          >
            <h2>The Value of Your Huntington Beach Motorcycle Crash Case </h2>
          </div>
        </LazyLoad>

        <p>
          Not every accident case is similar. Many factors are taken into
          consideration when asking for a certain amount of compensation. 
        </p>
        <p>
          For example, a motorcyclist that was bumped by a car and did not
          experience any severe injuries might receive a different settlement
          amount from someone who became paralyzed after a bike crash. There is
          not a standard amount of compensation that every injury victim will
          receive. Speak to a legal expert in Huntington Beach to inquire about
          the amount you can potentially win after your motorcycle crash. 
        </p>
        <h2>Who is Liable for Your Medical Expenses and Lost Wages?</h2>
        <p>
          Liability for your injuries and the damages that have been accumulated
          from in an accident is dependent on several factors. One of these
          vital factors is negligence. Proving negligence for your personal
          injury claim will not only hold wrongdoers accountable but it will
          give you the ability to earn you the maximum financial compensation
          for your injury case. 
        </p>
        <p>
          Negligence is split into two categories comparative and contributory
          negligence. The state of California operates under comparative
          negligence which means that both plaintiff and defendants can file for
          compensation for damages. This means that the settlement amount can
          potentially be divided in two ways.  
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Our Orange County Accident Lawyers Are Here to Help </h2>
        <p>
          Finding the right motorcycle accident attorney in Huntington Beach can
          be difficult, but it doesn't have to be. Bisnar Chase's Personal
          Injury Lawyers have over<strong> 40 years of experience</strong>{" "}
          taking on tough insurance companies. We prioritize having an excellent
          attorney-client relationship and aim to provide you with the best
          formal legal advice. 
        </p>
        <p>
          If you are looking for a lawyer you can trust to take your case, or
          you just want free information on how to go about your case, call one
          of our Huntington Beach motorcycle accident attorneys today at{" "}
          <strong> 949-203-3814 for a free professional evaluation</strong>. 
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
