// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Interstate Route 91 Freeway Accident Attorneys"
        pageDescription="Were you severely injured in an interstate route 91 freeway accident? Our attorneys can help. Call 949-203-3814 for a free consultation. No win, no fee, no risk. Contact us now."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Interstate Route 91 Freeway Accidents</h1>
        <BreadCrumbs location={location} />
        <p>
          State Route 91 is a major freeway in southern California that travels
          east and west. This busy freeway runs from Gardena through several
          regions of Los Angeles to Riverside. SR-91 is one of the busiest and
          most dangerous freeways because it is the only highway that links Los
          Angeles, Orange and Riverside Counties. Parts of this freeway are
          known as the Gardena Freeway, the Long Beach Freeway, the Artesia
          Freeway and the Riverside Freeway.
        </p>
        <ul>
          <li>
            The Gardena Freeway is a short portion of Interstate Route 91 in
            southern Los Angeles County. It travels from the eastern edge of
            Gardena to the Long Beach Freeway.
          </li>
          <li>
            The Artesia Freeway travels through southeastern Los Angeles County
            and northwestern Orange County. This portion of the freeway goes
            from the Long Beach Freeway in Long Beach to the Fullerton and
            Anaheim border in Orange County.
          </li>
          <li>
            The Riverside Freeway goes from the Santa Ana Freeway in Buena Park
            to State Route 60 in Riverside. After the Riverside Freeway passes
            the I-215/CA-60/CA-91 junction, it continues on as the I-215.
          </li>
        </ul>
        <h2>A Busy Freeway</h2>
        <p>
          The design of the 91 Freeway is unique in its use of express lanes.
          There is a 10-mile, high-occupancy toll road that runs from the Costa
          Mesa Freeway in Anaheim to the Riverside County line. The express
          lanes have two primary lanes in each direction that are separated from
          the regular main lanes of traffic.
        </p>
        <p>
          One of the reasons why Interstate Route 91 is so busy and dangerous is
          because it is one of only a few routes that run through the Santa Ana
          Mountains that connect Orange and Riverside counties. Much of this
          highway gets so congested during rush hour that it is commonly
          referred to as "the Corona Crawl" by local traffic reporters.
          Motorists often, commuters have to struggle with slowed and even
          stopped traffic along the freeway.
        </p>
        <h2>Accident Causes</h2>
        <p>
          There are many potential causes of freeway accidents. Common causes of
          injury accidents along SR-91 include:
        </p>
        <ul>
          <li>
            <strong> Roadway congestion:</strong> This is an extremely busy
            freeway. It does not have to be rush hour for traffic jams to result
            in stopped traffic. As the only freeway that connects Orange, Los
            Angeles and Riverside counties, there are hundreds of thousands of
            drivers who need to take the 91 Freeway every workday. Drivers who
            are impatient, anxious or fatigued can cause devastating accidents
            that bring traffic to a standstill. Slowed traffic often leads to
            even greater frustration and more opportunities for road rage and
            rear-end collisions.
          </li>
          <li>
            <strong> High volume of commercial trucking:</strong> Many truckers
            and trucking companies rely on the Interstate 91 to connect
            businesses in Orange, Riverside and Los Angeles counties. Wherever
            there are large trucks, there is the potential for devastating truck
            accidents. All drivers should avoid staying within the many blind
            spots surrounding tractor-trailers.
          </li>
          <li>
            <strong> Mountainous terrain:</strong> This freeway does travel
            through hilly and mountainous areas such as the Santa Ana Mountains.
            These areas typically result in traffic congestion and uneven
            traffic flow.
          </li>
          <li>
            <strong> Negligent driving:</strong> This freeway is so busy that
            one driver can create hazardous conditions for thousands of
            motorists. Drivers who are distracted, speeding, drunk or fatigued
            can cause devastating rear-end collisions and chain reaction
            crashes. It is common for unsafe lane changes and tailgating
            collisions to result in additional accidents and a multitude of
            injuries.
          </li>
          <li>
            <strong> Inattentive driving:</strong> If you have to travel long
            distances on Interstate 91, you will likely get stuck in traffic for
            long hours at a time. Motorists can be seen talking on the cell
            phone, eating or drinking while driving. These are dangerous habits
            that can lead to devastating car accidents.
          </li>
        </ul>
        <h2>Contact an Accident Attorney</h2>
        <p>
          Victims of southern California freeway accidents have it in their best
          interest to discuss their legal rights and options with an experienced
          personal injury lawyer. A skilled{" "}
          <Link to="/orange-county/car-accidents">
            {" "}
            Orange County car accident lawyer
          </Link>{" "}
          at our law firm can help prove liability for the crash and help
          injured victims obtain fair and full compensation for their losses.
          Please contact us for a free consultation and comprehensive case
          evaluation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
