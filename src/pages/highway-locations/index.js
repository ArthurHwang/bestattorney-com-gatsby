// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Highway Locations"
        pageDescription="Injured in an interstate or highway accident? Our attorneys can help. Call 949-203-3814 for qualified auto accident attorneys. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Highway Locations</h1>
        <BreadCrumbs location={location} />

        <p>
          Highway, interstate and freeway accidents are commonplace amongst
          California's travel roadways. Congestion and impatience plays a big
          role in the number of car accidents we see on a daily basis in
          California.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/highway-locations/freeway-101-accidents">
              101 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/freeway-70-accidents">
              70 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/freeway-710-accidents">
              710 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-5-freeway-accidents">
              Interstate Route 5 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-8-freeway-accidents">
              Interstate 8 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-10-freeway-accidents">
              Interstate Route 10 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-40-freeway-accidents">
              Interstate 40 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-55-freeway-accidents">
              Interstate Route 55 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-57-freeway-accidents">
              Interstate Route 57 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-73-freeway-accidents">
              Interstate Route 73 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-91-freeway-accidents">
              Interstate Route 91 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-101-freeway-accidents">
              Interstate Route 101 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-210-freeway-accidents">
              Interstate Route 210 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-405-freeway-accidents">
              Interstate Route 405 Freeway Accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/highway-locations/interstate-route-605-freeway-accidents">
              Interstate Route 605 Freeway Accidents{" "}
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
