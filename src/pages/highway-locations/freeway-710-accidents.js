// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="710 Freeway Accidents - Los Angeles Freeway Accident Lawyers"
        pageDescription="Were you seriously injured in a 710 freeway accident? Call 323-238-4683 for a free consultation. Passionate representation, superior results. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>710 Freeway Accidents</h1>
        <BreadCrumbs location={location} />

        <p>
          Interstate 710 is called &ldquo;the seven-ten&rdquo; by area residents
          in southern California, and travels north and south for 23 miles in
          Los Angeles County.  This important highway connects Long Beach to
          Alhambra and is also called the &ldquo;Long Beach Freeway.&rdquo;  The
          710 begins at the Cerritos Channel and travels over the Gerald Desmond
          Bridge, then ends at Pasadena with a connection to I-210.  This
          important arterial highway sees a very high volume of traffic on a
          daily basis, and accidents are unfortunately rather common.  Our{" "}
          <Link to="/los-angeles/car-accidents">
            {" "}
            Los Angeles car accident lawyers
          </Link>{" "}
          are ready to represent the victims of these accidents and help them
          recover damages for injuries they have sustained.
        </p>
        <p>
          Car accidents on California freeways tend to fall into one of two
          types.  Either they happen at low speeds and occur because of traffic
          jams, or they happen at high speeds and involve reckless drivers. 
          There are very few accidents that fall into the middle ground between
          these two extremes, although of course they can happen.  Slow-speed
          accidents are usually rear-end collisions that result in whiplash
          injuries.  As drivers travel the freeway, they may be forced to slow
          or even stop for congestion.  If drivers are not paying attention,
          they may rear-end the vehicle in front and cause a collision.  If
          drivers are traveling too fast and weaving, they may also cause
          serious accidents that result in rollovers or other potentially fatal
          crashes.
        </p>
        <p>
          While everyone understands that traffic is crowded, some people are
          simply more prepared to deal with congestion than others.  Drivers who
          practice defensive driving and other safety precautions are less like
          to cause accidents than those who are reckless or distracted.  The
          recent advent of cell phones means that there is a high probability
          that many drivers at any given moment are talking or texting and not
          paying attention to their surroundings.  This can result in very
          serious injuries or even death.
        </p>
        <p>
          Even if you are a very careful driver, there is a possibility that you
          will be the victim of a freeway collision.  No matter how much you try
          to avoid one, you cannot control other drivers.  If you have been the
          victim and you are suffering from injuries someone else has caused,
          you should talk to expert 710 Freeway accident attorneys as soon as
          possible.
        </p>
        <p>
          Not only is it essential that you get sound legal advice to collect
          damages for your case, it is also very important that you file your
          case in a timely manner.  California's statute of limitations for
          personal injury cases varies according to the age of the victim and
          the circumstances of the accident, but it is important that you do not
          miss the filing deadline, no matter what it is.  If you miss the
          deadline imposed by the law to file your claim, you may lose your
          right to collect damages.
        </p>
        <p>
          Talk to a California freeway accident attorney today and find out the
          facts about your case, how long you have to file, and how much you can
          expect to collect in damages for your injuries.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
