// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Interstate 40 Freeway Accidents - San Bernardino"
        pageDescription="Were you injured in an Interstate 40 freeway accident? Call 949-203-3814 for attorneys who can help. Free consultations. Passionate representation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Interstate 40 Freeway Accidents</h1>
        <BreadCrumbs location={location} />

        <p>
          Interstate 40 is a major east-west artery that starts its California
          run from the Mojave Desert.  Often called the &ldquo;Needles
          Freeway,&rdquo; the entire length of I-40 in California lies in San
          Bernardino County.  I-40 intersects with I-15 near Barstow and the
          Marine base located there.  Most of the length of I-40 is desert and
          sparsely inhabited. Nevertheless, there are still accidents that claim
          victims of both fatalities and injuries on I-40.  When these accidents
          occur, Interstate 40 Freeway accident lawyers are ready to help these
          victims recover damages.
        </p>
        <p>
          No one really wants to pursue a personal injury case, given the fact
          that these lawsuits can be exhausting and drawn-out; however, pursuing
          damages on your own will be much harder without the help of a
          professional{" "}
          <Link to="/san-bernardino/car-accidents.html">
            San Bernardino car accident attorney
          </Link>
          .  An injury attorney has the experience and knowledge to help you get
          the maximum amount possible from your case, and an attorney who
          specializes in car collisions has very specialized knowledge that
          allows you to get a much better settlement or judgment than you would
          alone.
        </p>
        <p>
          Attorneys all attend law school and all have the same basic legal
          knowledge.  They are required to pass classes in contract law, real
          estate law, personal injury law, and criminal law.  However, not every
          attorney makes the decision to pursue all these areas of the law in
          practice.  Attorneys, like doctors, are drawn to areas of the law that
          interest them, and some attorneys are interested in helping victims
          collect damages from people who have hurt them.
        </p>
        <p>
          This is one reason why it is wise to talk to a lawyer who specializes
          in personal injury when you are involved in a car collision.  It is
          certainly better to use a general practice lawyer than to handle a
          lawsuit yourself, but if you really want the best settlement, you
          should rely on attorneys who handle these types of cases for a living
          and have expert knowledge not only about the law but also about the
          negotiation process that is so often crucial to getting a good
          settlement for your injuries and damages.
        </p>
        <p>
          Interstate 40 Freeway accident attorneys have this specialized
          knowledge and understand not only the process of pursuing a personal
          injury case but also the particular type of cases often seen on
          California highways.  By immersing themselves in the culture and
          people of this area, they develop a sensitivity to the issues that may
          surround any personal injury case and the obstacles that may arise.
        </p>
        <p>
          Talk to a professional car collision attorney today about your case. 
          You are entitled by law to compensation for your injuries, including
          medical payments, pain and suffering, and other related costs. 
          However, the chances of you collecting the full amount of your claim
          are low unless you avail yourself of the help offered by expert car
          crash lawyers and allow them to guide you in the process of recovering
          your money.  No matter how big or small your case, it is likely that
          you will receive more with the help of a professional attorney than
          without.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
