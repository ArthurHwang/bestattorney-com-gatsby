// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="101 Freeway Accidents - Bisnar Chase"
        pageDescription="Seriously injured in a 101 freeway accident? Call 323-238-4683 for Los Angeles attorneys who can help. Free consultations. Passionate representation. Award-winning attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>101 Freeway Accidents</h1>
        <BreadCrumbs location={location} />

        <p>
          Highway 101 is one of the longest U. S. Highways still in operation in
          entire United States, and the longest in California.  It hugs the
          coast all the way from Los Angeles, where it intersects with
          Interstate 5, to the Oregon State line.  With a road this long and
          traveling through so many different types of terrain, it is probably
          inevitable that there will be accidents that involve injuries and
          deaths along this highway.  If you have been the victim of a car
          collision and have suffered injuries, 101 Freeway accident lawyers can
          help.
        </p>
        <p>
          Car accidents are a commonplace type of incident in California given
          the extreme congestion of our highways and roads.  Under California
          law, every accident has a cause and someone is charged with the fault
          in the collision.  This happens under California's tort liability
          statutes, which are different than some states &ldquo;no-fault&rdquo;
          laws.  In California, you are not allowed to simply let your own
          insurance take care of the damage for your vehicle or pay your medical
          bills; if another person is involved and caused the accident and
          injuries, that person must pay.  Although California drivers often
          have uninsured motorist coverage to pay for accidents in which the
          at-fault driver does not have coverage, it is still the driver's
          responsibility to pay for the damage and the insurance company can
          pursue payment from the at-fault party if they choose.
        </p>
        <p>
          What this means is that every accident has a cause and an at-fault
          party.  While it may sometimes be difficult to determine the true
          cause of an accident, especially in cases where the collision was
          particularly serious and damaging to the vehicles involved, it is the
          responsibility of the authorities to make this determination. 
          Sometimes, when a lawyer who specializes in car collisions is
          unsatisfied with the police findings, he or she may hire an accident
          reconstruction expert to examine the collision evidence and make an
          independent determination.  It has sometimes happened that these
          experts' findings have led the authorities to revise their initial
          opinion of an accident and make an official determination that another
          party was actually at fault.
        </p>
        <p>
          If you have been involved in any type of accident, it is very
          important that you get sound legal advice before you attempt to
          negotiate with any group or individual.  You may be told the accident
          is your fault, but that may not be true.  You have nothing to lose by
          talking to our{" "}
          <Link to="/los-angeles/car-accidents">
            {" "}
            Los Angeles car accident attorneys
          </Link>{" "}
          about your case; you are offered a free initial consultation
          appointment to get the truth about your case, so you spend no money to
          talk to these lawyers.  If the lawyer agrees you have a claim and you
          want to retain him or her, you will be asked to sign a fee agreement
          that allows the attorney to collect a percentage of the final
          settlement in place of monthly billed fees.  These two practices mean
          that you risk nothing by talking to or retaining an attorney to
          represent you in your car collision case.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
