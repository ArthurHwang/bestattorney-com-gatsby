// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "wrongful-death/fullerton-wrongful-death-lawyer.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Wrongful Death Lawyers - Orange County"
        pageDescription="Looking for a wrongful death attorney in Fullerton? Call 949-203-3814. Top client care and legal service since 1978. Call today to take advantage of our free consultation and our 96% success rate winning difficult wrongful death cases in California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fullerton Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have lost a loved one due to someone else's negligence,
          speaking with one of our{" "}
          <strong> Fullerton Wrongful Death Lawyers</strong> is in your best
          interest. Our attorneys have represented over 12,000 injury victims
          while sustaining a <strong> 96% success rate</strong>. Our team of
          aggressive litigators have taken Fortune 500 companies to court and
          achieved record-breaking verdicts for those we represent.
        </p>
        <p>
          At Bisnar Chase, you can trust that our team will{" "}
          <strong> get you the best possible outcome for your case.</strong>
        </p>
        <p>
          Bisnar Chase <Link to="/wrongful-death">wrongful death </Link> clients
          receive a personal, first-class treatment from a staff who is
          dedicated to obtaining justice. Our team of nationally recognized
          attorneys have assisted victims with cases similar to yours, and we
          will use what we have learned to give you the best possible
          experience.
        </p>
        <p>
          Bisnar Chase partners, John Bisnar and Brian Chase, have instilled a
          passion for helping injury victims in every staff member, which has
          helped us become one of the best wrongful death law firms in the
          nation.
        </p>
        <p>
          <strong> Call 949-203-3814 and receive a free consultation</strong>.
        </p>
        <h2>Filing a California Wrongful Death Claim</h2>
        <p>
          Surviving family members such as spouses, children, siblings or
          parents of the deceased may file a{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Wrongful_death_claim"
            target="_blank"
          >
            {" "}
            wrongful death claim
          </Link>
          . What needs to be proven if you are filing a wrongful death claim
          though, is that the deceased had lost their life due to the negligent
          party and that the negligent party had a responsibility to the
          deceased as well.
        </p>
        <p>
          Our attorneys will make sure that your family member's death is fully
          investigated and that no clue or fact will be left undiscovered. We
          will make sure that any entity who may be held responsible for the
          accident is, and that every avenue of compensation will be fully
          explored. If your family member died because of a hazardous street or
          defective product, we will do our best to hold those who are
          responsible accountable, and deter them from making future defective
          devices.
        </p>
        <h2>Plaintiff Compensation for Wrongful Death Cases</h2>
        <p>
          The compensation that is awarded to the surviving members of the
          deceased is based on what financial losses had come after the death of
          a family member.
        </p>
        <p>
          <strong>
            {" "}
            The damages are usually an estimated amount of the financial losses
            and are based on a number of factors including
          </strong>
          :
        </p>
        <ul>
          <li>Age of the departed</li>
          <li>State of health of the deceased</li>
          <li>Funeral costs</li>
          <li>Medical expenses</li>
          <li>
            Future earnings that the deceased would accumulated while being
            employed
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/wrongful-death/funeral-crying-image.jpg"
            width="100%"
            alt="Fullerton wrongful death attorneys"
          />
        </LazyLoad>
        <h2>5 Different Types of Wrongful Death Cases</h2>
        <p>
          For a family to experience a wrongful death is devastating. When a
          person dies at the hands of someone else's negligence this can be
          grounds for a wrongful death. There are various reasons of why people
          lose their lives due to someone else's carelessness. Below are the
          most common kinds of wrongful death cases.
        </p>
        <p>
          <strong> 5 different types of wrongful death cases</strong>:
        </p>
        <ol>
          <li>
            <strong> Car crash</strong>: It has been reported, that in one year
            over 3,126 people had lost their lives in a car accident. Many of
            those crashes occur due to a driver being careless. The leading
            cause of accidents is distracted driving such as eating food or
            texting. .
          </li>
          <li>
            <strong> Work accidents</strong>: Many wrongful deaths take place at
            work because of tripping over equipment or not the proper handling
            of equipment. These incidents mostly occur at factories or
            construction sites. The surviving members of the employee can file
            either a workers compensation claim or can also file a suit against
            the negligent party.
          </li>
          <li>
            <strong> Medical malpractice</strong>: Medical malpractice is
            defined as when a patient is harmed or killed due to a medical
            professional such as a doctor not performing to the best of her/his
            abilities. A doctor/client relationship needs to be established and
            proved in order to pursue a medical malpractice suit. It needs to
            proven that the doctor had been treating the patient for a period of
            time.
          </li>
          <li>
            <strong> Defective products</strong>: Liability falls on the
            manufacturer if a person is deceased because of a defective product.
            Some examples include food poisoning, products with the intent to
            protect children such as car seats or defective car parts.
          </li>
          <li>
            <strong> Semi-truck crashes</strong>: Big-rig crashes may seem like
            they should be placed in the same category as auto accidents but due
            to their size they can cause a different, substantial amount of
            damage. There are also causes that are unique to semi-truck
            accidents such as over-sized loads or mechanical problems.
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Fullerton wrongful death lawyers in Fullerton"
            style={{
              backgroundImage:
                "url('/images/images/fullerton-wrongful-death-text-header.jpg')"
            }}
          >
            <h2>Wrongful Death Attorneys with National Experience</h2>
          </div>
        </LazyLoad>
        <p>
          John Bisnar, managing partner of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>
          , created Bisnar and Associates in 1978 because he had a horrible
          experience with his attorney; he had suffered serious injuries in an
          auto accident and his lawyers made several mistakes throughout his
          case. Mr. Bisnar felt that victims of wrongful deaths and personal
          injuries deserved better representation and devoted his life to making
          it happen. He now speaks nationally at seminars to help teach other
          law firms how to best manage their employees and client experience.
        </p>
        <p>
          Brian Chase, senior partner, has experienced an unrivaled career
          representing victims of serious injury and wrongful death. Brian has
          received countless awards for his courtroom contributions, including
          Trial Lawyer of the Year. In the last year alone, Brian has obtained a
          record breaking 24.7 million dollar verdict for a victim of
          catastrophic injuries.
        </p>
        <h2>Wrongful Death Lawyers for all Personal Injuries</h2>
        <p>
          The death of a loved one can arise from any type of personal injury
          accident, but not all wrongful death attorneys in Orange County have
          the experience to handle certain complex cases.
        </p>
        <p>
          For instance, wrongful deaths that arise as a result of a defective
          auto part could cost hundreds of thousands of dollars in experts and
          analysis, costs that would later be deducted from your settlement or
          verdict. Fortunately, our law firm has tried several auto defect cases
          and has already spent the time and money to investigate many of the
          vehicles that are deemed defective.
        </p>
        <p>
          <strong>
            {" "}
            The following are categories of wrongful death in which we have
            experienced great success
          </strong>
          :
        </p>
        <ul>
          <li>Defective Products</li>
          <li>Truck Accidents</li>
          <li>Brain Injury</li>
          <li>Nursing Home Abuse</li>
          <li>Car Accidents</li>
          <li>Construction Accidents</li>
        </ul>
        <h2>Wrongful Death Lawyers in Fullerton</h2>
        <LazyLoad>
          <img
            src="/images/images/fullerton-wrongful-death-lawyers.jpg"
            width="100%"
            alt="Wrongful death attorneys in Fullerton"
          />
        </LazyLoad>
        <p>
          If you are looking for the best legal representation in Orange County,
          call one of our wrongful death lawyers in Fullerton to take advantage
          of our free consultation. We provide a{" "}
          <strong>
            {" "}
            <Link to="/about-us/no-fee-guarantee-lawyer" target="_blank">
              {" "}
              No Win No Fee guarantee{" "}
            </Link>
          </strong>{" "}
          which means that if your case is not won, you will not have to pay us
          for our services. Call today to get the information you need to start
          your case off right.
        </p>
        <p>
          <strong> Call 949-203-3814 NOW to get FREE LEGAL ADVICE</strong>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
