// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/fullerton-employment-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Employment Lawyers - Orange County CA"
        pageDescription="Have you experienced job discrimination, sexual harassment, physical abuse or employee rights violations? The Fullerton Employment Lawyers of Bisnar Chase have been representing & winning cases for over 40 years and will get you Maximum Compensation. Call for your Free consultation at 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Employment Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="fullerton employment lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Fullerton{" "}
            <Link to="/employment-law" target="new">
              Employment Lawyers{" "}
            </Link>
          </strong>{" "}
          at <strong> Bisnar Chase</strong> are dedicated to ensuring your
          workplace health and safety. Millions of workers experience harassment
          at some point, and many times let it go. This is unfortunate and
          should be reported.
        </p>
        <p>
          With <strong> over 40 years of experience</strong>, out attorneys have
          the know-how, skills and resources to take on and win the most complex
          cases other well-known law firms have denied.
        </p>
        <p>
          Our law firm has a <strong> 96% success rate</strong> and has won over{" "}
          <strong> $500 Million</strong> for our clients. The paralegals,
          mediators, negotiators, lawyers and other legal staff members of
          Bisnar Chase are 5-Star rated and known for their professionalism,
          positive energy and great environment. The Newport Beach office is
          open Monday through Friday, 8:00am-5:00pm, and you are encouraged to
          come meet and speak with us about your case.
        </p>
        <p>
          If you have experienced workplace harassment, job and or worker
          violations at your work, call us for your{" "}
          <strong> Free consultation</strong> at <strong> 949-203-3814</strong>.
        </p>

        <h2>The Advantages of Filing a Class Action lawsuit</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/class-action-rally-sexual-harassment-fullerton.jpg"
            width="100%"
            alt="fullerton employment law lawyer"
          />
        </LazyLoad>

        <p>
          When a number of employees have suffered the same type of mistreatment
          in the hands of an employer, they can band together and file what is
          known as a <strong> class action lawsuit</strong>.
        </p>
        <p>
          If, for example, a major corporation has failed to pay its employees
          overtime, the many workers affected by the corporation's wrongdoing
          can file a single lawsuit together.
        </p>
        <p>
          There are positives and negatives to any{" "}
          <strong> Orange County</strong> class action lawsuit and anyone
          considering becoming a part of a class action should discuss all of
          their options with a Fullerton employment lawyer.
        </p>
        <p>
          There are many potential advantages to filing a class action lawsuit.
        </p>
        <ul>
          <li>
            Aggregation of a number of lawsuits into one lawsuit increases the
            efficiency of the legal process
          </li>
          <li>
            Having many potential claimants for one legal team is considerably
            cheaper than having many claims with multiple attorneys involved.
          </li>
          <li>
            There will be no need for the same witnesses to testify in each
            case.
          </li>
          <li>
            Some of the best employment lawyers may not be interested in
            handling a small claim case for an individual. If, however, many
            cases are combined into a larger class action suit, then, the
            claimants will have an easier time securing a skilled and
            resourceful attorney who has experience fighting large corporations.
          </li>
          <li>
            Compensation that is recovered will be evenly distributed among the
            members of the class.
          </li>
          <li>
            A large class action suit against a corporation is likely to bring
            about a change in the company's policies. While a small, individual
            lawsuit may not affect how a company does business, a large class
            action lawsuit may actually inspire change for the betterment of the
            workforce.
          </li>
          <li>
            class action lawsuits have the power to give a voice to the
            powerless. Someone who cannot afford an employment lawyer and who
            cannot stand up to a negligent corporation may benefit by joining a
            class action lawsuit.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/fullerton-class-action-lawyers.jpg"
            width="100%"
            alt="fullerton employment attorneys"
          />
        </LazyLoad>

        <h2>Disadvantages in Filing a class Action Lawsuit</h2>
        <p>
          A class action is not the best legal option for all potential
          claimants. If the claimants win their case, there will be a settlement
          that is divided equally between them. This means that someone who has
          suffered losses that are greater than the other claimants may want to
          pull out of the class action lawsuit and file a separate claim.
        </p>
        <p>
          There is also the potential for a class action lawsuit decision to
          bind all of the plaintiffs to a small benefit.
        </p>

        <h2>
          Top 5 Most Common Types of Workplace Harassment &amp; Violations
        </h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/top-5-most-common-types-of-workplace-harassment-fullerton.jpg"
            width="100%"
            alt="fullerton employment law attorneys"
          />
        </LazyLoad>

        <p>
          Without getting into specifics and one by one cases, there are 5 main
          types of workplace harassment and inappropriate behavior.
        </p>
        <p>
          Here is a list of the{" "}
          <strong>
            {" "}
            Top 5 most common types of workplace harassment and violations:
          </strong>
        </p>
        <p>
          <strong> 1. Verbal Harassment</strong>: This can consist of many
          things, such as:
        </p>

        <ul>
          <ul>
            <li>imitating or mimicking an employee's foreign accent</li>
            <li>
              constantly pestering other co-workers to do things like go on
              breaks, lunch every day, hang out after work, on holidays and
              weekends, especially when they express not wanting to
            </li>
            <li>
              teasing or asking inappropriate questions about genetic disorders,
              sexual preference or other personal information
            </li>
          </ul>
        </ul>
        <p>
          <strong> 2. Physical Harassment</strong>:
        </p>
        <ul>
          <ul>
            <li>
              pushing, shoving, kicking or any unwanted physical acts of
              aggression
            </li>
            <li>
              hand, face or body gestures representing derogatory demeanor
            </li>
            <li>
              physical confrontations, whether serious or joking can result in
              serious, catastrophic and permanently disabling injuries, and in
              unfortunate cases, death. To learn more on improper workplace
              roughhousing, aggression and work accidents, visit{" "}
              <Link to="https://www.osha.gov/" target="new">
                Osha.com{" "}
              </Link>
              . Job accidents are something to take very serious in every work
              environment.
            </li>
          </ul>
        </ul>
        <p>
          <strong> 3. Visual Harassment</strong>:
        </p>
        <ul>
          <ul>
            <li>
              showing a co-worker inappropriate personal pictures containing
              violence, nudity or pornography
            </li>
            <li>gestures of an inappropriate and derogatory nature</li>
          </ul>
        </ul>
        <p>
          <strong> 4. Emotional Harassment</strong>:
        </p>
        <ul>
          <ul>
            <li>making fun of or publicly embarrassing others</li>
            <li>
              just because you find a situation or something you say harmless,
              doesn't necessarily mean that it is not hurtful to someone else
            </li>
          </ul>
        </ul>
        <p>
          <strong> 5. Mental Harassment</strong>:
        </p>
        <ul>
          <ul>
            <li>
              similar to emotional harassment, mental harassment can go on for a
              long time before it is ever presented as an issue
            </li>
            <li>
              guilting, belittling, snide comments, being micro-managed or
              mentally strained, while those doing the harassing are protected
              by authority, leverage or systematic priority
            </li>
          </ul>
        </ul>
        <p>
          The 5 examples of harassment in the workplace can all vary drastically
          depending on the individuals situation and circumstances. Sometimes
          these types of harassment can go on unseen for days, weeks, months and
          even years.{" "}
          <Link to="/employment-law/sexual-harassment" target="new">
            Sexual Harassment
          </Link>{" "}
          can fit into each of these types of harassment, depending on the way
          the sexual harassment is performed.
        </p>
        <p>
          When there are people who are very direct and outspoken who may have
          no problem at all speaking up for themselves, more reserved and
          introverted individuals may have a more difficult time doing so.
        </p>
        <p>
          It may not be the case of a person being too afraid to speak up for
          themselves, but quite often, it is due to{" "}
          <strong> fear of losing their job</strong>, losing a possible
          promotion or not getting a raise or bonus.
        </p>
        <p>
          Regardless of financial stability, dependence and keeping your job, if
          harassment of any kind is being experienced or has been experienced,
          don't waste any more of your time, take action, and make things right
          for you and potentially other victims to this harassment.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="fullerton employment law lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/workplace-class-action-examples-fullerton.jpg')"
            }}
          >
            <h2>Examples of Workplace Class Action Lawsuits</h2>
          </div>
        </LazyLoad>

        <p>
          Class action claims are common in employment law because a company
          that treats one employee poorly is likely mistreating many employees.
          Common issues such as overtime pay, improper deductions and employee
          misclassification disputes are common at large corporations.
        </p>
        <p>
          Overtime pay disputes may involve a company regularly refusing to pay
          employees overtime for working over eight hours a day or 40 hours a
          week. They may also involve a failure to provide adequate meal and
          rest breaks to their employees. Companies involved in an improper
          deduction claim may be responsible for wrongfully splitting
          commissions.
        </p>
        <p>
          Corporations in trouble for misclassification disputes may wrongfully
          claim an employee is exempt from overtime because of their position or
          they may wrongfully consider an employee an independent contractor
          instead of a full time employee worthy of proper pay and benefits.
        </p>
        <h2>Contact a Fullerton Employment Lawyer</h2>
        <p>
          If you worry that you and your coworkers are being cheated or
          mistreated by your employer, please do not hesitate to speak with an
          experienced <strong> Fullerton employment lawyer</strong>. The reputed{" "}
          <strong> Fullerton employment attorneys</strong> at Bisnar Chase have
          a successful track record of fighting for the rights of workers.
        </p>
        <p>
          Whether your employer fails to provide benefits, refuses to pay
          overtime, fosters a workplace environment that allows sexual
          harassment or racial discrimination or simply mistreats employees,
          please call us at <strong> 949-203-3814</strong> or contact us to
          better understand your legal rights and options.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
