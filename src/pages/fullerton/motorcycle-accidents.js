// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/motorcycle-crash-fullerton-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Motorcycle Accident Lawyers - Orange County, CA"
        pageDescription="Contact top rated Fullerton motorcycle accident lawyers for a free consultation. If you've suffered injuries from a motorcycle crash we can help you get compensation. 96% success rate, serving Orange County since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Motorcycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fullerton motorcycle accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong> Fullerton Motorcycle Accident Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          know that it can be hard to get your life back on track after a bike
          crash. Along with serious injuries, victims can accumulate large
          medical bills and lose valuable time and pay from work.
        </p>
        <p>
          <strong>
            {" "}
            The law firm of Bisnar Chase has been helping clients gain millions
            in compensation
          </strong>{" "}
          for recovering individuals that have experienced lost wages and
          counseling for the emotional, mental aftermath.{" "}
          <strong> For 40 years</strong> our Fullerton motorcycle accident
          attorneys have held a <strong> 96% success rate</strong> for cases
          involving bike incidents.
        </p>
        <p>
          If you or someone you know is struggling with the damages from a
          motorcycle crash the injury lawyers of Bisnar Chase are here for you.
          Upon your call you will receive a free consultation with a top-notch
          legal team member.
        </p>
        <p>
          <strong> Call 949-203-3814 now</strong>.
        </p>
        <h2>Are You Qualified to Receive a Settlement in Fullerton?</h2>
        <p>
          How do you know if you meet the requirements for a motorcycle
          accident? Major factors for determining whether or not you receive
          compensation is based on liability and the damages you have
          accumulated after the accident. If you can prove that there was
          carelessness on the behalf of the other party and you have experienced
          devastating losses, you can obtain a substantial amount of recoveries.
        </p>
        <p>
          <strong>
            {" "}
            In personal injury cases damages are divided up into two categories
          </strong>
          :
        </p>
        <ul>
          <li>Special damages</li>
          <li>Damages that are not easily calculated</li>
        </ul>
        <p>
          Special damages have an exact calculation of the past, present and
          future losses of the plaintiff's wages or medical bills. Damages that
          are not easily calculated can be, for example, emotional pain and
          suffering.
        </p>
        <h2>The Do’s and Dont’s of Handling Your Motorcycle Accident Case</h2>
        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/fullerton-motorcycle-lawyer-image.jpg"
            width="100%"
            alt="Fullerton motorcycle accident attorneys"
          />
        </LazyLoad>
        <p>
          According to California Highway Patrol's 2009{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/userLogin.jsp"
            target="_blank"
          >
            {" "}
            Statewide Integrated Traffic Records System
          </Link>{" "}
          (SWITRS), there were no fatalities, but 34 injuries reported as a
          result of motorcycle accidents in the city of Fullerton. During that
          same year, there were 17 fatalities and 735 injuries reported due to{" "}
          <Link to="/orange-county/motorcycle-accidents" target="_blank">
            {" "}
            Orange County motorcycle accidents
          </Link>
          .
        </p>
        <p>
          If you or a loved one has been injured in a Fullerton motorcycle
          accident, it is important to contact a Fullerton Motorcycle Accident
          Lawyer to discuss your options. Also, it is a good idea that you
          understand the Do's and Don'ts after a motorcycle accident to make
          sure that your rights are protected.
        </p>
        <p>
          <strong> Do's</strong>
        </p>
        <p>
          What you do immediately following an accident can affect your ability
          to pursue compensation for your injuries.  If you or anyone else has
          been injured, please call the police right away and remain at the
          crash site until the authorities arrive. You will want your account of
          the incident in the police report and it is against the law to leave
          the scene of an injury accident.
        </p>
        <p>
          <strong> Do:</strong>
        </p>
        <ul>
          <li>
            Document the scene of the accident. If you do not have a camera, or
            a camera phone, call a friend to arrive at the crash site to take
            photos. You will want pictures of yourself, your motorcycle and the
            crash site. Having photos of tire marks, damaged vehicles and
            injuries can help bolster you case.
          </li>
          <li>
            Exchange information. Get the contact information for anyone who may
            have witnessed the crash. You should also collect insurance details
            and contact information from any other motorist involved in the
            crash.
          </li>
          <li>
            Seek medical attention. There are a number of reasons why it is in
            your best interest to see a doctor right away. First and foremost,
            you chances of having a full recovery are greatly increased when you
            receive proper diagnosis and treatment immediately following the
            crash. Medical records can also be used during the claims process as
            proof of the severity of the injury suffered.
          </li>
          <li>
            Contact a motorcycle accident lawyer. Having an experienced
            Fullerton motorcycle accident lawyer can help you obtain the
            compensation you need and to hold the negligent parties accountable.
          </li>
        </ul>
        <p>
          <strong> Don'ts</strong>
        </p>
        <p>
          There are a few things that you may think are in your best interest
          that can actually hurt you during the claim process.
        </p>
        <ul>
          <li>
            Don't rush to call the other driver's insurance company. Insurance
            companies often look for reasons to deny claims and rushing to call
            an insurance company before you are prepared to handle the phone
            call can be a big mistake. If you are on pain medication following
            the accident, you shouldn't call the insurance company. If you have
            suffered a serious injury, it would be in your best interest to have
            an attorney call the insurance company for you or at least prepare
            you for the phone call.
          </li>
          <li>
            Don't fix your motorcycle. You may be anxious to get back to riding
            or at least to repair your damaged vehicle, but you may be hurting
            your case. You may be able to claim compensation for property damage
            in addition to your injuries.
          </li>
          <li>
            Don't rush into a settlement with an insurance company or any other
            party. Once you sign an agreement, your case is settled or closed.
            It would be in your best interest to carefully and thoroughly
            analyze all your current and future losses and then make a claim.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/motorcycle-crash-accident.jpg"
            width="100%"
            alt="Bike crash attorneys in Fullerton"
          />
        </LazyLoad>
        <h2>Common Types of Motorcycle Accidents</h2>
        <p>
          Motorbikes, if not handled with care can be dangerous and even deadly.
          The way in which you obtain your injuries can dictate liability and
          also the amount of compensation you will receive. The Fullerton bike
          accident attorneys of Bisnar Chase have over 40 years of experience
          representing clients who have been in various kinds of{" "}
          <Link
            to="https://one.nhtsa.gov/people/injury/pedbimot/motorcycle/safebike/anatomy.html"
            target="_blank"
          >
            {" "}
            motorcycle crashes
          </Link>
          .
        </p>
        <p>
          <strong>
            {" "}
            Below are the different types of motorcycle collisions that can
            occur
          </strong>
          :
        </p>
        <p>
          <strong> Head-on crashes</strong>: One of the drawbacks to riding a
          motorcycle is the lack of protection that the cyclist has. Wearing a
          helmet can only safeguard a rider so much. Even if a rider tries to
          quickly move out of the direction of the vehicle there is still a
          strong possibility that the rider will suffer severe injuries.
        </p>
        <p>
          <strong> Un-safe lane changes</strong>: When a driver does not signal
          that they are about to make a lane change or they are texting and
          driving while making a lane change, they have the potential to hurt or
          even kill somebody on a motorcycle. Drivers hold the responsibility to
          check their blind spots before turning as well. Drivers and cyclist
          both can help prevent crashes by being aware of their surroundings.
        </p>
        <p>
          <strong> Rear-end accidents</strong>: Cyclist that are hit from behind
          are more susceptible to broken bones, extreme harm to the brain and{" "}
          <Link to="/head-injury/spinal-cord-injuries" target="_blank">
            {" "}
            spinal cord injuries
          </Link>
          . If a motorcyclist is hit from the back it can propel the rider
          forward onto the road. The cyclist chances of not being to stop or
          speed up increases when they are rear-ended.
        </p>
        <h2>5 Tips for Riding a Motorbike in Fullerton</h2>
        <p>
          Motorcycle riders can decrease their chances of being in an accident
          if they follow certain safety precautions. Proper equipment and riding
          techniques potentially can save a cyclist life.
        </p>
        <p>
          <strong>
            {" "}
            The following are five measures that riders can take to prevent a
            crash and severe injuries
          </strong>
          :
        </p>
        <ol>
          <li>Wear the correct equipment such as a protective jacket</li>
          <li>Do not ride between cars when traffic slows down</li>
          <li>
            Make sure your bike is properly running, for instance, making sure
            your brakes are functioning accordingly
          </li>
          <li>Always make sure that motor vehicle drivers can see you</li>
          <li>Drive at the posted speed limits</li>
        </ol>
        <LazyLoad>
          <div
            className="text-header content-well"
            title=" Motorcycle injury lawyers in Fullerton"
            style={{
              backgroundImage:
                "url('/images/motorcycle-accidents/fullerton-motor-bike-text-header.jpg')"
            }}
          >
            <h2>Why Should You Hire a Fullerton Bike Crash Lawyer?</h2>
          </div>
        </LazyLoad>
        <p>
          A knowledgeable Fullerton motorcycle accident lawyer will stay abreast
          of the official investigation and help determine liability for the
          collision. In cases involving negligence or wrongdoing, financial
          compensation may be available for the victim's medical bills, lost
          wages, cost of rehabilitation services and other related damages.
        </p>
        <p>
          The experienced{" "}
          <strong> Fullerton motorcycle accident lawyers</strong> at Bisnar
          Chase Personal Injury Attorneys have a long and successful track
          record of winning cases involving motorcycle accidents. We will fight
          for your rights and ensure that you are fairly compensation for all
          the damages you have sustained as a result of someone else's
          negligence. Please contact us if you or someone you know have been
          injured in a bike accident to receive a free case evaluation and more
          information about pursuing your legal rights.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
