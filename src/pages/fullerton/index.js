// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/fullerton-broken-bone-resized-two.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Personal Injury Lawyers - Bisnar Chase"
        pageDescription="The Fullerton personal injury lawyers at Bisnar Chase have been serving clients whose cases involve injuries from instances such as a dog bite, slip and falls or car accidents for 40 years. Our lawyers have won millions in compensation for people who have experienced injuries due to someone's negligence. Call (949) 203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fullerton personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Contact the <strong>Fullerton Personal Injury Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          for a free consultation if you've been injured. Many people do not
          know who to turn to when they have experienced a catastrophic injury.
        </p>
        <p>
          <strong>
            Since 1978 Bisnar Chase has been the voice for the severely injured.
          </strong>
        </p>
        <p>
          We represent plaintiffs and have collected over{" "}
          <strong>$500 Million in winnings</strong> for over{" "}
          <strong>12,000 clients</strong>. Bisnar Chase has earned a reputation
          for being top-notch lawyers who obtain high recoveries for their
          clients.
        </p>
        <p>
          <strong>Call 949-203-3814</strong> for a{" "}
          <strong>free consultation</strong>.
        </p>
        <h2>3 Elements That Classify a Personal Injury Case</h2>
        <p>
          Every suit is different depending on the type of injury that has been
          experienced. There are three main factors for solidifying that in
          fact, you do have a personal injury case. The following are three
          consistent determinate for a personal injury claim.
        </p>
        <ol>
          <li>
            <strong>Negligence:</strong> For example, if you have suffered from
            a dog bite due to the owner not supervising their pet this can be
            grounds for negligence. Negligent parties that are responsible for
            severe injuries can include property owners, employers, reckless
            drivers or pet owners.
          </li>
          <li>
            <strong>
              {" "}
              <Link
                to="https://en.wikipedia.org/wiki/Pain_and_suffering"
                target="_blank"
              >
                {" "}
                Pain and suffering
              </Link>
            </strong>
            : There are several ways that you can prove pain and suffering. Pain
            and suffering includes emotional, psychological and physical
            damages. Evidence of these types of losses can involve an
            accumulation of medical bills, lost wages and photos that were taken
            at the scene of the accident.
          </li>
          <li>
            <strong>Redeemable losses</strong>: The number of earnings that you
            will win will depend on the severity of your injury. Much of
            personal injury suits have what you would call “compensatory
            damages.” Compensatory damages are the losses a victim has faced
            after their accident. It isn’t until the person involved in the
            accident is compensated that they are made “whole” again. Types of
            losses that could be recovered in your personal injury settlement
            are medical expenses, physical therapy and loss of wages.
          </li>
        </ol>
        <LazyLoad>
          <img
            src="/images/personal-injury/fullerton-dog-bite-resized.jpg"
            width="100%"
            alt="Fullerton personal injury attorneys"
          />
        </LazyLoad>
        <h2>Personal Injury Claims for Animal Bites</h2>
        <p>
          The{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=3342.&lawCode=CIV"
            target="_blank"
          >
            {" "}
            California dog bite law
          </Link>{" "}
          states that if a person falls victim to a dog bite the owner becomes
          strictly responsible for the damages the victim has faced. If your dog
          has been involved in an attack you must report the incident right
          away.  In some cases, if the dog attack was severe the dog is to be
          put in an enclosed area where it cannot reach other people. Whether
          your dog faces euthanization after an attack depends on the laws of
          the state.
        </p>
        <p>
          <strong>What to do after a Fullerton animal attack</strong>
          <strong>:</strong>
        </p>
        <p>
          <strong>Receive medical attention immediately</strong>: You must seek
          help from a medical professional right away. The quicker you get to a
          hospital the quicker you will receive care and the necessary
          medication for your injuries. It is also strongly suggested that you
          seek medical attention immediately just in case the dog is not
          vaccinated.
        </p>
        <p>
          <strong>Compile as much evidence as possible</strong>: If there were
          any people that witnessed the attack, talk to them and ask them if
          they would contribute to your case. Also, any photographs of your
          wounds following the incident will aid you in receiving compensation
          for your medical bills.
        </p>
        <p>
          <strong>Speak to a Fullerton personal injury lawyer</strong>: First
          and foremost, hold off on speaking to the dog owner’s insurance
          company. Contact an attorney first who specializes in these cases, so
          they may earn you the settlement you are entitled to.
        </p>
        <h2>Who Is Held Accountable?</h2>
        <p>
          Each year, millions of people experience serious injuries or even lose
          their lives to tragic accidents. Victims of the incidents and family
          members seek justice and compensation to move forward in the healing
          process.
        </p>
        <p>
          There are a few components that hold an individual responsible for
          personal injuries. For example, if you have experienced a slip and
          fall due to a floorboard being wobbly at a store, the property owner
          would be held as the responsible party.
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/fullterton-party-accountable-resized.jpg"
            width="100%"
            className="lawyer-image-space"
            alt="Personal injury lawyers in Fullerton"
          />
        </LazyLoad>
        <p>
          One element of holding the negligent party responsible is the legal
          term of “the burden of proof” and the “preponderance of the evidence.”
          In a civil suit, both are needed to prove carelessness.
        </p>
        <p>
          The burden of proof involves the plaintiff(victim) proving that the
          negligent party was the cause of their injuries. The preponderance of
          evidence displays to a jury evidence proves “more likely than not” the
          fact that the <strong>defendant indeed is accountable</strong>.
        </p>
        <h2>Injured at a Fullerton Construction Site?</h2>
        <p>
          In one year alone, over 5,190 construction workers were killed on the
          job.{" "}
        </p>
        <p>
          <strong>Most of the causes of workplace injuries or deaths</strong>:
        </p>
        <ul>
          <li>Electrocutions</li>
          <li>Falls</li>
          <li>Caught in machinery</li>
          <li>Struck by equipment</li>
        </ul>
        <p>
          There are many misconceptions of who is liable for an injured worker.
          Employers are not the only one who can be held accountable for the
          damages an employee faces. Other responsible parties can be
          manufactures of equipment used on the job site, contractors or
          engineers.
        </p>
        <h2>What to Look for in a Fullerton Personal Injury Lawyer</h2>
        <p>
          Many accident victims do not know how to claim a personal injury let
          alone find a suitable lawyer for their case. It is important though
          that you seek an experienced personal injury lawyer that specializes
          in the type of case that you have. The Fullerton personal injury
          attorneys at Bisnar Chase have been representing clients that have
          cases involving motorcycle accidents,{" "}
          <Link to="/car-accidents" target="_blank">
            {" "}
            car accidents
          </Link>
          , dog bites and slip and falls.
        </p>
        <p>
          <strong>
            There are certain qualities that your personal injury lawyer should
            obtain such as
          </strong>
          :
        </p>
        <p>
          <strong>Experience</strong>
        </p>
        <p>
          Years of experience adds to the credibility of an attorney's legal
          representation skills. Through out the years the personal injury
          lawyer may have acquired good relationships with claims adjusters or
          even insurance companies. Hers/his reputation may be widely known as
          either good or bad. Search for current reviews Online to find out the
          types of experiences past clients have had with before proceeding with
          your injury claim.
        </p>
        <p>
          <strong>Personality</strong>
        </p>
        <p>
          Do you initially feel uncomfortable or intimidated the first time you
          meet with your attorney? If so it may serve in your best interest to
          seek out legal representation elsewhere. Suffering from an accident
          can be devastating and not building a good relationship with your
          lawyer does not only add stress to you but it can also weaken your
          case.
        </p>
        <p>
          <strong>Offering the “No Win, No Fee” Option</strong>
        </p>
        <p>
          If your personal injury lawyer does not win your case, then you are
          omitted from paying any fees. At Bisnar Chase we do not only make this
          our promise, but we also offer a free consultation with an experienced
          trial lawyer upon your call. Bisnar Chase mission statement is: "
          <em>
            To provide superior client representation in a compassionate and
            professional manner while making our world a safer place
          </em>
          ."
        </p>
        <h2>Bisnar Chase Success Stories</h2>
        <p>
          From horrific car accidents to paralyzing on the job-incidents, since
          1978 our personal injury attorneys have strived to win our clients
          what they deserve. John Bisnar and Brian Chase have built their
          success on excellent customer satisfaction.
        </p>
        <p>
          Below are just a few of many{" "}
          <Link to="/about-us/testimonials" target="_blank">
            {" "}
            client reviews
          </Link>{" "}
          of Bisnar Chase attorneys:
        </p>

        <div>
          <div>
            <h5>
              John Bisnar is like family, he really took care of us. I'm so
              happy that we found Bisnar Chase. They've been outstanding. They
              kept us informed with what's going on with the case.
            </h5>
            <p>
              - <strong>Maria Chan, former client</strong>
            </p>
          </div>
        </div>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <div>
          <div>
            <h5>
              They were always diligent in following up with me and keeping me
              abreast of where they were in the process. They did everything,
              everything. They actually doubled the original offer from the
              insurance company.
            </h5>
            <p>
              - <strong>Ed Solomon, former client</strong>
            </p>
          </div>
        </div>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9-UJdHzYPw4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Personal injury attorneys in Fullerton"
            style={{
              backgroundImage:
                "url('/images/personal-injury/fullerton-text-header-image.jpg')"
            }}
          >
            <h2>Top-Notch Legal Representation</h2>
          </div>
        </LazyLoad>
        <p>
          Many attorneys simply settle cases -- much to the chagrin of their
          clients, but our law firm has trial lawyers representing our clients
          and are willing to go to court for you. A jury trial can be a powerful
          tool in justice and if we feel the other party is not offering fair
          compensation, we are prepared to take your case to a jury.
        </p>
        <p>
          <strong>
            Our mission statement has always been to provide superior client
            representation
          </strong>
          . We provide you with the highest quality of representation in the
          legal field. We will fight for you and with you until the job is done.
          We also strive to be extremely compassionate as we understand an
          injury carries a lot of physical and emotional stress for many.
        </p>
        <p>
          Contact us today to see if you have a case. Whether its a car
          accident, dog bite or auto defect, our trained personal injury
          attorneys may be able to help you. Call
          <strong> 949-203-3814 for a free consultation</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
