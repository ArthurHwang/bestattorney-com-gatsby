// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/fullerton-dog-bite-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Dog Bite Lawyers - Dog Bite Attorneys in Fullerton"
        pageDescription="Call 949-203-3814 for our Fullerton Dog Bite Lawyers if you've been the victim of a dog bite and need compensation. Trusted personal injury attorneys serving Orange County since 1978. The law firm of Bisnar Chase has obtained a 96% success rate and hundreds of millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Dog Bite Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fullerton dog bite lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Bisnar Chase's{" "}
          <strong>
            {" "}
            Fullerton{" "}
            <Link to="/dog-bites" target="_blank">
              {" "}
              Dog Bite Lawyers{" "}
            </Link>
          </strong>{" "}
          are ready to assist you with any questions you may have about being
          bitten by a dog owned by another. Dog bites happen often in Fullerton.
          Getting bitten by a dog is the fifth most frequent cause of visits to
          emergency rooms caused by activities common among children.
        </p>
        <p>
          Our injury attorneys have over <strong> 40 years</strong> representing
          Orange County residents who need legal representation after suffering
          a serious dog bite injury.
        </p>
        <p>We may be able to get you fair gains for your injuries.</p>
        <p>
          <strong> For a free consultation or you may call 949-203-3814</strong>
          .
        </p>
        <h2>Fullerton Dog Bite Statistics</h2>
        <p>
          <strong> Dog bite losses exceed $1 billion per year</strong>.
        </p>
        <p>
          More than 350,000 dog bite victims are seen in emergency rooms every
          year. Approximately 800,000 victims receive some form of medical
          attention annually. Nevertheless, only 16,000 victims per year receive
          money from homeowners insurance companies and renters insurance
          companies. This equals 0.004% of the victims - just 4 out of every
          1,000. Although these insurers pay over $500 million to all victims,
          the average insurance payment for a dog bite case is only $18,750.
        </p>
        <p>
          An American has a one in 50 chance of being bitten by a dog each year.
          (
          <Link to="https://www.cdc.gov/" target="_blank">
            {" "}
            CDC
          </Link>{" "}
          )
        </p>
        <h2>3 Reasons Why a Dog May Bite</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/sad-dog-fullerton-lawyers-image.jpg"
            width="100%"
            alt="Dog bite lawyers in Fullerton"
          />
        </LazyLoad>
        <p>
          There are a great many issues to consider with a dog bite. Despite
          Fullerton being a safe, neighborhood-oriented community it does have
          its share of dog bite accidents. Even if the dog is not intentionally
          provoked by a child there are numerous reasons why a dog might attack.
          The following reasons explain why a dog may bite.
        </p>
        <p>
          <strong>
            {" "}
            Reasons{" "}
            <Link
              to="https://www.avma.org/public/Pages/Why-do-dogs-bite.aspx"
              target="_blank"
            >
              {" "}
              why a dog would bite{" "}
            </Link>
          </strong>
          :
        </p>

        <ol>
          <li>
            <strong> The dog may be suffering from an unknown injury</strong>:
            If your dog is suddenly being aggressive, there may be a physical
            problem at hand. Take your dog to the local veterinarian office to
            see if there is a particularly sensitive spot that is triggering the
            dog's hostile behavior.
          </li>
          <li>
            <strong> It is protecting an object</strong>: When a dog has just
            given birth, experts say that the dog becomes very protective of her
            pups and extremely suggest putting the mother in a secluded area
            where she and her pups will not be disturbed. Other objects that a
            dog may be protecting their food.
          </li>
          <li>
            <strong> Feeling threatened may trigger aggressive behavior</strong>
            : In a stressful situation, such as the dog being surrounded by
            strangers, may provoke the dog to attack. Animal professionals
            recommend that the dog be socialized at an early age to prevent it
            to feel uncomfortable with unfamiliar people.
          </li>
        </ol>
        <h2>What Should I Do If I Was Bitten by a Dog in Fullerton?</h2>
        <p>
          Being attacked by a dog is not only physically life-threatening but
          emotionally debilitating. The first thing that a person should do
          after a dog bite is to seek medical attention immediately. Dog bites
          can range from a minor tear in the skin to extreme nerve damage. It is
          important that you obtain treatment from a medical professional right
          away.
        </p>
        <p>
          After receiving medical care collecting evidence for your dog bite
          case is crucial. Gathering proof such as eye-witness reports, photos
          and medical bills can greatly help your case.
        </p>
        <h2>Avoiding a Fullerton Dog Bite</h2>
        <p>
          The leading causes of emergency room visits overall are falls, being
          struck by or against an object, natural or environmental causes,
          poisoning, being cut or pierced, and motor vehicle accident. Dog bites
          are one of the most prominent causes emergency room visits for
          children. There are ways in which you can avoid a vicious dog attack
          though to protect not only your child but you as well. Below are the
          do's and don'ts of what to do to prevent dog bites.
        </p>
        <p>
          <strong> Do and Don't to avoid dog bites</strong>:
        </p>
        <p>
          <strong> Do</strong>:
        </p>
        <ul>
          <li>Confirm with the owner that it is okay the pet the dog</li>
          <li>Remain still if a dog approaches you</li>
          <li>
            Notify any surrounding adults that there is a stray dog on the loose
          </li>
          <li>Curl up in a ball if you are pushed over by a dog</li>
        </ul>
        <p>
          <strong> Don'ts</strong>
        </p>
        <ul>
          <li>Never run away from a dog</li>
          <li>Do not make any loud noises</li>
          <li>Refrain from looking directly into a dog's eyes</li>
          <li>
            At no time should you speak to the dog with commands such as "stop"
            or "no"
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog bite attorneys in Fullerton"
            style={{
              backgroundImage:
                "url('/images/dog-bites/dog-bite-fullerton-lawyers-text-header.jpg')"
            }}
          >
            <h2>Choosing Highly-Rated Legal Representation</h2>
          </div>
        </LazyLoad>

        <p>
          {" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          <strong> Fullerton Dog Bite Lawyers</strong> have dedicated their
          practice to victims of serious injuries due to negligence. Our
          highly-rated legal team is consistent and honest about your case from
          start to finish.
        </p>
        <p>
          If you or a loved one ever incurs a dog bite related incident, call us
          right away.{" "}
        </p>
        <p>
          <strong> Call 949-203-3814 </strong>for a{" "}
          <strong> free case evaluation. </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
