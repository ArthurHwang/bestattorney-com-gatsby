// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/truck-accident-fullerton-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fullerton Truck Accident Lawyers, Orange County CA"
        pageDescription="Contact the truck accident lawyers in Fullerton California if you are injured and entitled to compensation. The injury and accident attorneys of Bisnar Chase have a 96% success rate and have recovered hundreds of millions for their clients. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fullerton Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Truck Accident Lawyers In Fullerton"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Since 1978, the <strong> Fullerton Truck Accident Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          have been successfully settling claims against negligent truck
          drivers. Semi-truck and big rig drivers and the companies they work
          for have a responsibility to operate their vehicles in a safe and
          careful manner.
        </p>
        <p>
          Our Fullerton truck accident attorneys have the unique experience to
          know what to look for when analyzing and researching trucking
          accidents. We have an expert team of investigators, specialists in
          finding the details that have made the difference between a $15,000
          case into a $3,000,000 case simply by deep research and experience.
        </p>
        <p>
          If you or someone close has been injured in an accident involving a
          semi-truck or tractor trailer, contact the law offices of Bisnar Chase
          today.
        </p>
        <p>
          <strong> Call us now for a free consultation at 949-203-3814</strong>.
        </p>
        <h2>Truck Accidents in Fullerton</h2>
        <p>
          Fullerton California boasts a population of 132,000 and is located in
          North Orange County just South of Anaheim. With a fast-growing
          population, the city is a center of education with one of the most
          prestigious high schools in the nation, Troy High, and two
          universities: Cal State Fullerton and Fullerton Community College.
        </p>
        <p>
          Due to the high level of traffic, regular truck accidents occur in
          Fullerton which in some cases could have been prevented. If you were
          involved in a truck accident in or around Fullerton, California, you
          can contact the <strong> Fullerton Truck Accident Lawyers</strong> for
          a free consultation to see what financial compensation and help you
          may be eligible to receive.
        </p>
        <h2>Semi-Vehicle Crash Statistics</h2>
        <p>
          More than 140,000 injuries occur due to a truck accident and over one
          third suffer catastrophic damages. Large trucks represent just 3% of
          all registered vehicles, but they are involved in 12% of all traffic
          fatalities.{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2014"
            target="_blank"
          >
            {" "}
            FMSCA
          </Link>{" "}
          reports that California averages 400 fatal and 10,000 non-fatal truck
          accidents a year.
        </p>
        <p>
          Truck accidents often result in severe or even fatal injuries because
          cars are not made to withstand the impact of a vehicle like a truck.
        </p>
        <LazyLoad>
          <img
            src="/images/truck-accidents/man-falling-asleep-fullerton-lawyers.jpg"
            width="100%"
            alt="Fullerton truck accident attorneys"
          />
        </LazyLoad>
        <h2>Causes of Truck Incidents in Fullerton</h2>
        <p>
          Since a truck is so much larger than a car, injuries from truck
          accidents are usually much more serious and catastrophic than
          collisions involving two automobiles. Because truck drivers are
          operating a vehicle that is maintained by their employer, and possibly
          loaded by a third party shipping company, there may be more than one
          party responsible for a Fullerton truck accident.
        </p>
        <p>
          <strong>
            {" "}
            There are various type of trucking accidents, including truck
            accidents caused by
          </strong>
          :
        </p>
        <ul>
          <li>Driving under influence of alcohol or drugs</li>
          <li>Failure to yield to other party's right of way</li>
          <li>Falling Debris</li>
          <li>
            {" "}
            <Link
              to="https://keeptruckin.com/blog/driver-fatigue-impact/"
              target="_blank"
            >
              {" "}
              Fatigued or overworked drivers{" "}
            </Link>
          </li>
          <li>Illegal lane change</li>
          <li>A blown out tire</li>
          <li>Aggressive driving</li>
          <li>An overloaded or oversized truck</li>
          <li>Improper vehicle maintenance</li>
          <li>Inexperienced Driver</li>
          <li>Poor highway road maintenance</li>
          <li>Poor visibility</li>
          <li>Speeding or reckless driving</li>
          <li>Tailgating</li>
          <li>Unsafe driving in poor weather conditions</li>
        </ul>
        <h2>4 Steps to Take After Being Involved in a Big-Rig Accident</h2>
        <p>
          Being involved in a semi-truck accident can be deadly and if survived
          debilitating. Injuries that can occur with truck accidents may include
          spinal cord damage, brain damage, migraines for life, concussions,
          multiple bone fractures, severed limb and death.
        </p>
        <p>
          Before you seek compensation for your severe injuries that have been
          sustained in a truck accident in Fullerton, there are a few steps you
          should follow.
        </p>
        <ol>
          <li>
            Call 911 and give as many details as to your location as you can
          </li>
          <li>
            Obtain as much information as possible, including name and contact
            information of the truck driver, license plate number, and contact
            information of passengers and eyewitnesses to the accident
          </li>
          <li>Be sure to get insurance information</li>
          <li>Hire an expert in the field of commercial truck accidents</li>
        </ol>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bus crash attorneys in Fullerton"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/liability-truck-crash-lawyers-text-banner.jpg')"
            }}
          >
            <h2>Who Is Usually Responsible in a Fullerton Truck Accident?</h2>
          </div>
        </LazyLoad>
        <p>
          Truck drivers are expected to meet higher standards than the average
          driver. If you are injured in a truck accident and violation of the
          Federal Motor Carrier Safety Regulation Act can be proven, you may
          have a case against not only the driver, but the carrier as well. Of
          noteworthy importance is the fact that truck drivers are only allowed
          to drive a certain number of hours per day before they must sleep.
        </p>
        <p>
          Establishing the fact that the log was properly kept is essential to
          evaluating if violation of the law contributed to the accident.
        </p>
        <h2>We Will Win You the Compensation You Deserve</h2>
        <p>
          The Bisnar Chase{" "}
          <strong>
            {" "}
            Fullerton{" "}
            <Link to="/truck-accidents" target="_blank">
              {" "}
              Truck Accident Lawyers{" "}
            </Link>
          </strong>{" "}
          have represented truck and other auto accident victims for over{" "}
          <strong> forty years</strong>. We understand that accidents can leave
          victims paralyzed and even worse, are the victims of a wrongful death.
          Families also are left to care for their loved ones due to their
          severe injuries.
        </p>
        <p>
          If you or a loved one has experienced pain and suffering from a truck
          crash contact the law firm of Bisnar Chase. When you contact us today
          you will receive a free case analysis.
        </p>
        <p>
          Call <strong> 949-203-3814</strong> to learn more about your legal
          options.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
