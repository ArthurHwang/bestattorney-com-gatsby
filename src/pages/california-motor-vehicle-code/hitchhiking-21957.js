// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21957 - Hitchhiking"
        pageDescription="Hitchhiking is illegal by California Motor Vehicle Code 21957. Get the facts here."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21957</h1>
        <BreadCrumbs location={location} />
        <h2>Hitchhiking</h2>
        <p>
          California Vehicle Code 21957 states: No person shall stand in a
          roadway for the purpose of soliciting a ride from the driver of any
          vehicle.
        </p>
        <p>
          This rule is designed to keep pedestrians and motorists safe. It is OK
          to hitchhike through California, but you must obtain rides from places
          other than roadways. You could hitchhike at truck stops, or rest
          areas. You can also solicit a ride from the sidewalk -- so long as the
          driver of the car does not stop in the moving lanes of the street to
          pick you up.
        </p>
        <p>
          The California legislature has enacted pedestrian rules in an effort
          to provide all residents of the state safe and convenient travel
          whether on foot, by wheelchair, or by car. Even though this rule is
          enacted under the California Vehicle Code, it still applies to people
          on foot.
        </p>
        <p>
          The State of California wants to promote pedestrian travel, and cut
          down on the use of cars. Therefore, hitchhiking should be considered a
          perfectly acceptable means of travel within the state. However,
          pedestrians must still travel in a manner that provides safe passage
          for all residents using the roadways -- including vehicle drivers. The
          Department of Transportation and the California Highway Patrol are
          charged with finding ways to reduce pedestrian fatalities and injuries
          each year. If a CHP officer sees you standing in the roadway to obtain
          a ride, you will get a ticket.
        </p>
        <p>
          Because of pedestrian safety efforts, anyone who violates this section
          of the California Vehicle Code will receive a ticket that could cost
          as much as $200 after court costs. Why risk it? Only hitchhike in
          places where there is no chance you could be in a roadway or someplace
          where it is safe for you and motorists.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
