// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21950 - Right of Way at Crosswalks"
        pageDescription="Pedestrians have the right of way in crosswalks. California Motor Vehicle Code 21950 outlines the details."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21950</h1>
        <BreadCrumbs location={location} />
        <h2>Right of Way at Crosswalks</h2>
        <p>
          California Vehicle Code 21950 gives pedestrians the right of way when
          they are in the crosswalk. Pedestrians who cross outside the crosswalk
          must yield to cars and trucks.
          <b>
            If a pedestrian is crossing at an intersection, they have the right
            of way
          </b>
          . Not all intersections have marked crosswalks. If there is no marked
          crosswalk at an intersection, and you see a pedestrian crossing the
          street at the corner, you must still yield the right of way to the
          pedestrian.
        </p>
        <p>
          Pedestrians still have to use caution when crossing. A pedestrian may
          not run in front of an approaching vehicle and put lives in jeopardy
          simply because they are standing in a crosswalk. The law specifically
          states, "no pedestrian may suddenly leave a curb or other place of
          safety and walk or run into the path of a vehicle that is so close as
          to constitute an immediate hazard." If the approaching car is too
          close to safely stop, you should not walk in front of them.
        </p>
        <p>
          Pedestrians must continue walking when inside the crosswalk. It is
          illegal for a pedestrian to unnecessarily stop or delay traffic while
          they are in a marked or unmarked crosswalk.
        </p>
        <p>
          Drives approaching pedestrians in crosswalks must slow down or stop to
          allow the pedestrian to cross safely. Speeding up to beat them to the
          crosswalk is illegal because the driver is not exercising due care.
          Pedestrian safety is more important than shaving a few seconds off
          your drive time.
        </p>
        <p>
          If you see a pedestrian cross outside the crosswalk, you do not have
          to give them the right-of-way. Pedestrians who cross outside the
          crosswalk are crossing illegally and must yield the right-of-way to
          passing vehicles. However, you cannot speed up of try to play chicken
          with a pedestrian just because they are not inside a crosswalk.
        </p>
        <p>
          If you are stopped behind a car that is waiting for a pedestrian to
          exit the crosswalk, you are not allowed to pass the car. It is
          incredibly unsafe to pass a stopped car at a crosswalk. You may not be
          able to see the pedestrian, and the pedestrian will certainly not be
          expecting you to come around the cars in front.
        </p>
        <p>
          If you've been hit by a car in a pedestrian crosswalk or while obeying
          all laws, contact the{" "}
          <Link to="/pedestrian-accidents">
            California pedestrian attorneys
          </Link>{" "}
          of Bisnar Chase. We've been representing hit and run plaintiffs,
          pedestrian plaintiff's and those who've suffered a serious injury for
          over three decades. Call 949-203-3814.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
