// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23109 - Speed Contests"
        pageDescription="California Motor Vehicle Code 23109 speed racing contests are illegal in the state of California. Coviction is jail time and up to $1000 fine."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23109</h1>
        <BreadCrumbs location={location} />
        <h2>Speed Contests</h2>
        <p>
          Speed racing contests are illegal for drivers to participate in,
          unless done so in an approved setting. California Vehicle Code 23109
          discusses the specifics of speed contests and their corresponding
          repercussions.
        </p>
        <p>
          First of all, no person shall engage, aid, or abet in any motor
          vehicle speed contest on a highway. This includes assisting a race in
          any way by timing or constructing a barricade which is considered an
          obstruction of the highway.
        </p>
        <p>
          Any person who is convicted of a participating in a speed racing
          contest may be punished by imprisonment in county jail between 24
          hours and 90 days, a fine between $355 and $1,000, or a combination of
          a fine and imprisonment. With the punishment, the convicted must
          complete 40 hours of community service, and may lose the privilege of
          operating a motor vehicle for anywhere from 90 days to six months. An
          exception may be granted for people who use a vehicle as part of their
          employment.
        </p>
        <p>
          If a person is convicted of speed racing and doing so caused bodily
          injury to another person, the driver is subject to further fines and
          possible imprisonment. If said driver has also committed the offense
          of racing within the last 5 years and is caught again, he will be
          punished by imprisonment and a fine between $500 and $1,000.
        </p>
        <p>
          All in all, racing on the road is illegal and dangerous, and may
          result in hefty fines, imprisonment, and loss of vehicle usage
          privileges. The punishment increases if it is the driver's second or
          third offense, or if anyone is injured during the speed race.
        </p>
        <p>
          Any "contest" where a driver does not exceed the speed limit and
          covers a prescribed route of more than 20 miles, is not considered a
          race according to this section.
        </p>
        <p>
          It is the driver's responsibility to abide by maximum speed limit laws
          and to not race in speed contests.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
