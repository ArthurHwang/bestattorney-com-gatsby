// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21260 - Prohibitions for Low-Speed Vehicles"
        pageDescription="There are certain prohibitions for low speed vehicle as California motor vehicle code 21260 explains."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Prohibitions for Low-Speed Vehicles</h1>
        <BreadCrumbs location={location} />
        <p>
          Low-speed vehicles, with their reduced motor vehicle safety standards,
          are essentially less safe than standard vehicles, especially if
          operated in an illegal or unsafe manner. Drivers of low-speed vehicles
          must be fully aware of the different laws for low-speed vehicles and
          the restrictions.
        </p>
        <p>
          Low-speed vehicles must only be able to travel at 25 miles per hour
          and are not to travel on any roadway with a speed limit exceeding 35
          miles per hour. The only exception to this law is if the vehicle is
          driven in an area where a neighborhood electric vehicle transportation
          plan has been implemented, and the rules surrounding that plan advise
          otherwise.
        </p>
        <p>
          Drivers of low-speed vehicles are only allowed to cross a roadway with
          a speed limit over 35 miles per hour if the roadway the driver is
          traveling on has a speed limit of 35 miles per hour or less before and
          after the high-speed road intersection. The intersection described
          above must also occur at approximately 90 degrees, as diagonal
          intersections often require drivers to be in the middle of the
          intersection for larger stretches of road. This would require the
          low-speed vehicle driver to travel on a road with much faster traffic
          for longer.
        </p>
        <p>
          Further, low-speed vehicles are not to cross any uncontrolled
          intersections with a state highway unless the intersection has been
          reviewed and approved by the agency that enforces traffic
          responsibilities for low-speed vehicles at that crossing.
        </p>
        <p>
          The point that is arises with the prohibitions surrounding low-speed
          vehicles is that, a driver who chooses to travel by low-speed vehicle
          must not only be very aware of the laws which apply to his or her
          vehicle, but also the roads where he or she will travel. Having a good
          understanding of the roads and speed limits where the low-speed
          vehicle driver would like to drive is imperative to being comfortable
          and safe while driving.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
