// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Guidelines And Regulations For Operating A Motorized Scooter"
        pageDescription="According to California motor vehicle code 21235, traffic laws are different for the operation of motorized scooters."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Prohibitions for the Operation of Motorized Scooters</h1>
        <BreadCrumbs location={location} />
        <p>
          California vehicle code 21235 details exactly what is prohibited while
          operating a motorized scooter. This is important because, though
          motorized scooters have engines like cars, they are often subject to
          different traffic laws.
        </p>
        <p>
          Where motorized scooters can operate is one of the points that
          deviates from laws governing cars. Scooters are not to be driven on a
          highway with a speed limit over 25 miles per hour unless the motorized
          scooter is driven in a className II bicycle lane.
        </p>
        <p>
          Motorized scooter operators are never allowed to carry a passenger on
          the motorized bike, or any object that would prevent the driver from
          keeping at least one hand on the handlebars. It is further prohibited
          for a motorized scooter to be driven on a highway with the handlebars
          raised such that the driver must place his hands above shoulder-level
          in order to hold the steering area. A comfortable steering position
          must always be assumed in order for safe operation of a motorized
          scooter.
        </p>
        <p>
          Motorized scooters are not to be operated if the driver is not wearing
          proper safety equipment. A secure bicycle helmet, that meets the
          standards described in Section 21212, must be worn by the driver while
          operating the scooter.
        </p>
        <p>
          Drivers of motorized scooters are not allowed to operate scooters that
          have insufficient brakes. Motorized scooters must be equipped with a
          brake that allows the operator to make the wheel skid on dry pavement
          when the brake is applied.
        </p>
        <p>
          Motorized scooters must operate on their own. The driver is prohibited
          from attaching himself or herself to another vehicle, or attaching any
          part of the scooter to another vehicle while in operation.
        </p>
        <p>
          Motorized scooters are prohibited from operating on sidewalks, as
          doing so would put pedestrians in danger. Scooter are allowed to cross
          sidewalks only to leave a property and join traffic on the road. It
          follows that drivers of motorized scooters are not allowed to park
          their scooters on a sidewalk, in any position. Obstructing the
          sidewalk for pedestrian traffic prohibited.
        </p>
        <p>
          Similar to cars and any other motor vehicle, an operator is unlawful
          to drive a motorized scooter with out a valid driver's license or
          instructional permit.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
