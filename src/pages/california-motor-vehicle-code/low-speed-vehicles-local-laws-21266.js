// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21266 - Low-Speed Vehicles and Local Laws"
        pageDescription="California Motor Vehicle Code 21266. Local laws for low-speed vehicles."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21266</h1>
        <BreadCrumbs location={location} />
        <h2>Low Speed Vehicles and Local Laws</h2>
        <p>
          According to California Vehicle Code 21266, low speed vehicles have
          the same rights on the roads as all other vehicles. Low speed vehicles
          are electric vehicles that resemble glorified golf-carts. Low speed
          vehicles are usually designed to go speeds up to 25 mph, where golf
          carts are designed to travel at only 15 mph.
        </p>
        <p>
          Electric cars are legally permitted to operate on public roadways in
          most states. In California, local authorities have been given the
          right to restrict or prohibit the use of low-speed vehicles in certain
          circumstances. Local law enforcement or the California Highway Patrol
          may prohibit the operation of any low-speed vehicles within their
          jurisdiction if the prohibitions are determined to be in the best
          interest of public safety.
        </p>
        <p>
          If local authorities determine the use of low speed vehicles on their
          roads is unsafe, they must post signs letting drivers know they have
          been prohibited. Prohibitions and restrictions will not be effective
          until the appropriate signs have been posted.
        </p>
        <p>
          Keep in mind that low speed vehicles do not come with the same safety
          equipment of higher speed motor vehicles. They require no air bags, no
          roll bars, and offer very little protection if you are involved in a
          crash with a larger car or truck. While operating on a heavily
          traveled roadway may be legal in most towns, it may not always be
          safe. It is for this reason that local officials have the authority to
          restrict or prohibit their use on some roadways.
        </p>
        <p>
          Remember to use caution and common sense when operating on a busy
          street. Other drivers may not be vigilant in their quest for safety.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
