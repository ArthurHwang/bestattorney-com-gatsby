// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23123 - Driving & Cell Phones"
        pageDescription="California Motor Vehicle Code 23123 is the law that states that talking on a cell phone while driving is illegal."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23123</h1>
        <BreadCrumbs location={location} />
        <h2>Driving and Cell Phones</h2>
        <p>
          New vehicle codes have been added in recent years to address the rise
          in cell phone usage. Many people in California have cell phones and
          may be tempted to talk on the phone while driving. The need to have
          one hand free from the wheel to hold the device, coupled with
          attention the driver directs at the phone conversation instead of the
          road, has caused many people to believe that cell phone use while
          driving is a distraction and a hazard.
        </p>
        <p>
          Vehicle Code 23123, which has been revised as of January 2011, states
          that no driver in a motor vehicle is permitted to use a handheld
          wireless telephone while driving. Exceptions to this law include the
          use of a hands-free device which allows the driver to speak and listen
          to a phone call without removing his or her hands from the wheel. This
          law was passed with the belief that having both hands available to
          manipulate the steering wheel is essential to safe driving and
          avoiding accidents. Those who violate this code are subject to a $20
          fine for the first offense, and a $50 fine for each subsequent
          offense.
        </p>
        <p>
          There are a few other instances exempt from this vehicle code. The
          first is during an emergency situation. If the wireless telephone is
          being used for emergency purposes such as calling a law enforcement
          agency, health care provider, fire department or other emergency
          services agency. Further, the law does not apply to emergency services
          professionals who use a wireless telephone while operating an
          authorized emergency vehicle.
        </p>
        <p>
          School bus and transit vehicle drivers are also permitted to use a
          wireless telephone while driving their respective vehicles.
        </p>
        <p>
          Lastly, anyone driving a motor vehicle on private property is exempt
          from this code, though all drivers should exercise safe,
          distraction-less driving.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
