// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Vehicle Code 385.5 -- Protocol For Driving A Low Speed Vehicle"
        pageDescription="Correctly operating a modified or altered low speed vehicle on public roads is a law listed under California Motor Vehicle Code 21254."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Guidelines for Driving a Modified or Altered Low-Speed Vehicle</h1>
        <BreadCrumbs location={location} />
        <h2>Low Speed Vehicles vs. Standard Vehicles</h2>
        <p>
          A low-speed vehicle is described in California vehicle code 385.5 as a
          motor vehicle that meets specific requirements. It must have four
          wheels, be able to reach a speed of more than 20 miles per hour within
          a one mile stretch, and must not exceed 25 miles per hour. Further,
          the total gross weight rating of the vehicle can not be less than
          3,000 pounds. Low-speed vehicles are not golf carts, as golf carts are
          designed to travel at even lower speeds, and are instead referred to
          as "neighborhood electric vehicles."
        </p>
        <p>
          The benefits of driving low-speed vehicles are most obvious in a local
          setting. They produce less pollution than larger, faster vehicles, and
          can help people transport themselves at a slower rate, while using
          less gas.
        </p>
        <p>
          Because these vehicles are not considered regular cars, they are
          subject to relaxed federal Motor Vehicle Safety Standards. Low-speed
          vehicles are often lacking in safety features like air-bags that
          regular automobiles have. They are also considerably smaller and will
          suffer greater damage in a collision with a standard vehicle than the
          standard vehicle. The reason low-speed vehicles are allowed relaxed
          safety standards is because they are not designed to, and can not
          legally be driven over 25 miles per hour - ensuring that, despite the
          lack of safety features, the driver will be much safer traveling at a
          low speed.
        </p>
        <h2>
          California Vehicle Code 21254 Addresses Altered Low Speed Vehicles
        </h2>
        <p>
          Some people modify or alter their low-speed vehicle. If this is done,
          and the vehicle can exceed 25 miles per hour, the driver must note
          that the modified vehicle will not qualify for the relaxed federal
          Motor Vehicle Safety Standards. Instead, the driver is responsible for
          operating the modified low-speed vehicle only if it means all of the
          federal Motor Vehicle Safety Standards for regular passenger vehicles.
        </p>
        <p>
          As a vehicle is able to travel at higher speeds, the standards for
          safety also increase. Impacts, collisions, and accidents at higher
          speeds often involve more risk of personal injury, thus making safety
          provisions extremely necessary.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
