// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21702 - Limitation on Driving Hours"
        pageDescription="Truckers and bus drivers are required to take a break after a certain amount of hours on the road- California Motor Vehicle Code 21702."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21702</h1>
        <BreadCrumbs location={location} />
        <h2>Limitation on Driving Hours</h2>
        <p>
          California vehicle code 21702 limits the number of hours that truckers
          and bus drivers can be on the roads without taking a break. Sleepy
          truckers cause hundreds of accidents each year -- many of them fatal.
          A regular size car is no match for 80,000 pounds of big rig. Ask any
          honest trucker you know and they will tell you they've probably driven
          tired at least once in the past month.
        </p>
        <p>
          Vehicle code 21702 requires truckers and commercial bus drivers to get
          at least eight consecutive hours of sleep every day. The law states
          that truckers may not travel more than 12 hours at a time, and must
          rest at least 3 hours in a 15-hour stretch. Getting the proper amount
          of sleep is important in maintaining safe roads for everyone. No one
          wants to see 12 tons of steel coming down the road with a sleepy
          driver behind the wheel.
        </p>
        <p>
          Commercial bus drivers must not work more than 10 consecutive hours in
          a day. They also cannot work more than ten hours in a 15-hour period.
          This means they can't work 6 hours, take an hour break, and then work
          another 6 hours. Keeping the roads safe for everyone is more important
          than making a tight schedule.
        </p>
        <p>
          This law is in place to keep the roads safe. Commercial bus drivers
          have stricter regulations than truckers do because they haul people
          and not cargo. They also have more distractions inside their vehicle
          than truckers do so it is more imperative that they remain alert.
        </p>
        <p>
          Anyone caught in violation of this law could receive fines up to $1000
          for each offense. It is a myth to think the trucking companies will
          pay your tickets. The driver is the one responsible for the ticket and
          for ensuring safety on the roads. Your employer is not the one
          responsible for making sure you are following the rules of the road,
          no matter how demanding he or she may be.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
