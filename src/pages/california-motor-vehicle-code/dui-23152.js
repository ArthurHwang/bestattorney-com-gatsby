// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23152 - Driving Under the Influence"
        pageDescription="It is illegal to drive under the influence of alcohol regardless of age. See California Motor Vehicle Code 23152."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23152</h1>
        <BreadCrumbs location={location} />
        <h2>Driving Under the Influence</h2>
        <p>
          Vehicle Code 23152 warns that no person under the influence of any
          alcoholic beverage or drug, or combination of substances, is permitted
          to operate a vehicle.
        </p>
        <p>
          The blood alcohol content limit is set at .08 percent, and any driver
          at or over this limit will be considered intoxicated. The .08 percent
          limit is based upon grams of alcohol per 100 milliliters of blood or
          grams of alcohol per 210 liters of breath. If a driver's BAC is tested
          in due time after the event but not on the scene, and the BAC is found
          to be .08 or higher, it is presumed that the driver's BAC was at or
          above .08 at the time of driving.
        </p>
        <p>
          For commercial motor vehicles, the BAC limit is set at a more
          stringent .04 percent. This is because a commercial vehicle, as
          defined in Section 15210, is often much heavier, is used to transport
          a large number of passengers, is used to transport hazardous
          materials, and is often associated with a person's job.Alcohol should
          not be consumed on the job, and especially not not under any of the
          circumstances previously stated.
        </p>
        <p>
          This law extends the prohibition of driving to any one who is under
          the influence of or addicted to any drug. Though drug levels cannot be
          calculated the way BAC levels are with alcohol, the use of all illegal
          drugs is punishable, and the use of drugs while driving is dangerous.
        </p>
        <p>
          Drivers under the influence of prescription medication must be careful
          to not drive if the effect of the medication impairs the driver's
          ability to operate a vehicle. Police officers may observe a driver
          under the influence of a legal, prescription medication to be a
          hazard.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
