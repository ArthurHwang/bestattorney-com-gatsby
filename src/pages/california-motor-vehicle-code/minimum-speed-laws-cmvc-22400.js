// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22400 - Minimum Speed Laws"
        pageDescription="California Motor Vehicle Code 22400 is a minimum speed law. Read to find out more about the minimum speed limit."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22400</h1>
        <BreadCrumbs location={location} />
        <h2>Minimum Speed Laws</h2>
        <p>
          Most drivers are used to adjusting their speeds to go slower and not
          exceed maximum speed limits to avoid fines. Yet, drivers must also be
          aware of minimum speed limits and where it is not appropriate to drive
          excessively slow unless doing so can not be avoided.
        </p>
        <p>
          Vehicle code 22400 explains that no driver is permitted to drive on a
          highway at such a slow speed as to become a block or impediment to the
          normal and reasonable flow of traffic, unless the speed limit has been
          reduced by signs posted in compliance with the law. It is important
          for drivers to not block quick-moving traffic, as this is a hazard
          which can cause collisions.
        </p>
        <p>
          Further, no vehicle is allowed to be completely stopped on a highway,
          as this is also a serious danger to quick-moving traffic. The only
          time drivers are permitted to stop is if it is in compliance with the
          law, perhaps shown by signs posted or traffic guards. Occasionally a
          vehicle may breakdown on the highway, and the driver must carefully
          figure out that fastest way to remove the vehicle from blocking the
          movement of traffic.
        </p>
        <p>
          The Department of Transportation may declare a minimum speed limit on
          certain highways, which no driver is permitted to drive below. This is
          done on the basis of engineering and traffic surveys which explain
          areas on highways that become hazardous when drivers impede the normal
          traffic flow by moving too slowly.
        </p>
        <p>
          The best way to drive safely on any highway is to stay as close to the
          posted speed limit as is safely possible. Taking notice to not exceed
          or excessively underachieve the speed limit allows for a comfortable
          and reasonably paced traffic flow.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
