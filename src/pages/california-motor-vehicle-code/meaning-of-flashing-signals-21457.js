// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21457 - The Meaning of Flashing Signals"
        pageDescription="The meaning of flashing traffic signals and the laws accompanying them are defined in California Motor Vehicle Code 21457"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21457</h1>
        <BreadCrumbs location={location} />
        <h2>The Meaning of Flashing Signals</h2>
        <p>
          California Vehicle Code 21457 explains the indication of flashing red
          or yellow lights in traffic signals. A flashing red stop signal that
          flashes intermittently means that the driver facing the signal must
          approach the intersection, stop completely before entering the
          crosswalk or the intersection, and then proceed through the
          intersection under the same rules as stopping at a stop sign.
        </p>
        <p>
          A traffic signal that usually guides vehicles with green, red, and
          yellow lights, may occasionally show a flashing red signal. This
          usually means that something has gone wrong with the light system, and
          that all vehicles entering the intersection must do so as if it was a
          four-way stop. Proceeding with caution is especially important when an
          intersection has signals which are out of the ordinary, as approaching
          drivers will expect the usual traffic lights.
        </p>
        <p>
          A flashing yellow light is shown, it means the driver is allowed to
          pass through the intersection or turn with caution. Like a regular
          yellow signal, "caution" is key. Flashing yellow left-turn signals
          indicate that vehicles wishing to turn can enter the intersection in
          the left-turn space, but must wait for through-traffic to pass through
          the intersection.
        </p>
        <p>
          A good way to approach flashing lights is to do so with extra
          attentions. Remembering the rules of stop signs at flashing red lights
          and watch out for road hazards or special regulations in effect when
          encountering flashing yellow lights.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
