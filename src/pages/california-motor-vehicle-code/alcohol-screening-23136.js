// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23136 - Alcohol Screenings"
        pageDescription="California Motor Vehicle Code 23136 addresses underage drinking and driving while intoxicated."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23136</h1>
        <BreadCrumbs location={location} />
        <h2>Preliminary Alcohol Screening Tests for People Under 21</h2>
        <p>
          Drunk driving is a serious offense, and a consistent problem in
          California. According to Century Council data, there were 950
          alcohol-impaired driving fatalities in 2009, 156 of which involved
          persons under the age of 21.
        </p>
        <p>
          Vehicle Code 23136 addresses this issue. The law states that any
          person who drives a motor vehicle may be subject to a preliminary
          alcohol screening (PAS) test, including drivers under the age of 21.
          The purpose of the test is to find the driver's blood alcohol content
          (BAC) and, for drivers over the age of 21, to determine if the driver
          is over the .08 percent BAC limit. Drivers under the age of 21, for
          whom drinking is illegal, found to have a BAC of .01 percent will be
          prosecuted under this law for underage drinking and driving under the
          influence.
        </p>
        <p>
          Over or under 21 years of age, drivers who are found to be over the
          limit will be prosecuted, with punishment increasing in severity for
          higher blood alcohol contents. License suspension is immediate for
          drivers found to be driving under the influence, and may be restricted
          for a period of one to three years.
        </p>
        <p>
          Underage drinking is a problem in itself, but driving under the
          influence is especially dangerous as it puts the lives of other people
          at risk. All drivers, regardless of age, must be responsible or they
          will lose the privilege.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
