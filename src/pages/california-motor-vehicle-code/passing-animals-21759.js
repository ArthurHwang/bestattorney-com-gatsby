// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21759 - Passing Animals"
        pageDescription="According to California Motor Vehicle Code 21759 animals must be approached with care. See details here."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21759</h1>
        <BreadCrumbs location={location} />
        <h2>Passing Animals</h2>
        <p>
          California Vehicle Code 21759 states that a driver of a car or truck
          must approach animals with care. If you come upon a horse-drawn
          vehicle, someone on horseback or livestock crossing the road, you must
          maintain control of your vehicle and slow down. If necessary, you may
          have to stop your car and wait for the animals to pass you. You must
          also follow all signals indicated by the handler of the animal, and
          stop or slow down as instructed.
        </p>
        <p>
          The purpose of this law is to avoid frightening the animal and insure
          the safety of the riders, animals, and handlers. Usually, the handler
          or rider will allow you to pass at a reasonable pace. If they don't
          allow you to pass, there may be a reason for it -- like a narrow
          roadway or a skittish animal. If the handler signals for you to stop,
          it is for your own safety as well as the handler's and animal's
          safety.
        </p>
        <p>
          If you whiz by the animals honking your horn and cursing, you are
          likely to cause the rider to have an accident. Moreover, you'll be a
          jerk too. A spooked horse is a dangerous horse, and many people have
          been killed or seriously injured when falling off a runaway horse.
          Horses are prey animals, and once they start becoming intimidated, it
          is sometimes difficult to get them back under control. Be courteous
          and yield the right of way to any animals you see on the roads.
        </p>
        <p>
          If you're not cautious, and don't comply with the law you could be in
          trouble. If the rider or handler gets your license plate number you
          can expect to see a ticket and subpoena in the mail because a
          negligence and intentional infliction of emotional distress and
          violation of CVC 21759 charge may have just been filed against you.
        </p>
        <p>
          Regardless of whether you are in a vehicle or riding a horse, it is
          important to work together to stay safe when out on the roadways.
          There are many shared responsibilities between the horseback rider and
          the driver of a car.
        </p>
        <p>
          At BISNAR CHASE we love animals, and we love our cars. We want to make
          sure the roads stay safe for all people using the roads.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
