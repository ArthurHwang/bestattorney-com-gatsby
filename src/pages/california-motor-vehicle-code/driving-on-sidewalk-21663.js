// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21663 - Driving on Sidewalk"
        pageDescription="Driving on the sidewalk is not legal or safe. See California Motor Vehicle Code 21663 for details."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21663</h1>
        <BreadCrumbs location={location} />
        <h2>Driving on Sidewalk</h2>
        <p>
          Driving on the sidewalks is both illegal and dangerous. Unless you are
          crossing a sidewalk when entering a parking lot, or leaving your
          driveway, you should never drive your car on the sidewalk. Sidewalks
          are for pedestrian use only.
        </p>
        <p>
          There are a limited number of exceptions that allow big trucks on the
          sidewalks. Sometimes, local authorities give express permission to
          certain companies allowing their vehicles to drive and park on the
          sidewalks, such as the media, emergency vehicles, and utility
          work-trucks. This is allowed for convenience and worker safety.
        </p>
        <p>
          Express permission is not the same as implied or assumed. If you have
          express permission to be on a sidewalk, some local authority has
          spoken directly to you and given you permission. If you are unsure if
          you have express permission, then you probably don't have it.
        </p>
        <p>
          Implied permission is when you see others using the sidewalks and
          therefore assume that everyone is allowed to drive on the sidewalk.
          This is not a good assumption and could get you into trouble. If you
          have not personally been granted permission to drive on the sidewalks,
          then stay off them.
        </p>
        <p>
          If you are caught driving on the sidewalks without permission you will
          be ticketed and face large fines. Stay safe while driving your car or
          truck, and stay off the sidewalks.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
