// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23128 - Operating Snowmobiles"
        pageDescription="Looking for information about snowmobile laws? Read California Motor Vehicle Code 23128."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23128</h1>
        <BreadCrumbs location={location} />
        <h2>Operating Snowmobiles</h2>
        <p>
          There are some vehicles that must be operated under very specific
          conditions for which the vehicles were created. One example of such a
          vehicle is the snowmobile. A snowmobile, also sometimes called a
          snowmachine or a sled, is a land vehicle for winter travel on snow.
          Snowmobiles are able to operate on snow and ice and can travel through
          open terrain, deep forests, and frozen lakes with no trails or paths.
          Designed for winter-use, snowmobiles are like all-terrain vehicles
          (ATVs) that can be used where other vehicles cannot go.
        </p>
        <p>
          While snowmobiles are a special kind of vehicle with special uses,
          Vehicle Code 23128 draws attention to what snowmobiles are not
          permitted to do:
        </p>
        <p>
          Snowmobiles are not to be used on highways unless the road is not
          maintained by snow removal equipment and has been shut down to regular
          motor vehicle use, as explained in Vehicle Code 38025.
        </p>
        <p>
          Snowmobiles, like any vehicle, are not permitted to be driven in a
          careless or negligent manner that may danger people or property.
        </p>
        <p>
          Though snowmobiles are used in deep forests, they are not permitted to
          be used as vehicles in pursuit of deer or game, or to harass animals
          in their natural habitat in any kind of way. In other words, hunting
          from a snowmobile is prohibited.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
