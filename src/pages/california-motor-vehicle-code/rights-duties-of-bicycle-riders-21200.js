// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21200 - Rights & Duties of Bicycle Riders"
        pageDescription="21200. Call 949-203-3814 for immediate legal help. Leading California personal injury attorneys. Free consultations since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21200</h1>
        <BreadCrumbs location={location} />
        <h2>Rights & Duties of Bicycle Riders</h2>
        <img
          src="/images/bike-accidents/bike-traffic-signal.jpg"
          className="imgright-fixed"
          alt="traffic signal"
        />
        <p>
          The rights and duties of bicycle riders are clearly stated under
          California law and were written with the intention of{" "}
          <Link to="/bicycle-accidents">
            keeping bicycle riders safe while sharing the roads with cars
          </Link>{" "}
          and other motorized vehicles. Section 21200 of the California Vehicle
          Code specifically states that bike riders have the same rights and
          responsibilities as car drivers, with certain exceptions.{" "}
        </p>
        <p>
          Some of the responsibilities that are shared between bike riders and
          car owners are the duty to operate the bicycle in a safe manner.
          Riders should wear helmets and obey all traffic signs and signals.
          Also, all bike riders must operate their bicycle with due regard for
          the safety of all persons using the highway. They must also have
          functioning white headlights and red tail lights or blinkers on the
          bicycle when riding after dark. The handlebars must be in a
          comfortable position that does not require the rider to hold his or
          her hands above their heads, and at least one brake must work and be
          easy for the rider to use.{" "}
        </p>
        <p>
          Similar to car drivers, bicycle riders can be ticketed for driving
          under the influence of drugs and alcohol. Anyone caught biking under
          the influence will be charged with a DUI and receive a fine of up to
          $250 for each offense. Sections 21202 and 21208 list more duties and
          obligations that all bicycle riders must follow when riding on public
          streets.{" "}
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
