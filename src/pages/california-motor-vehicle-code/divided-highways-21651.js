// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21651 - Driving on Divided Highways"
        pageDescription="California Motor Vehicle Code 21651 defines lane divisions and how traffic should flow. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21651</h1>
        <BreadCrumbs location={location} />
        <h2>Driving on Divided Highways</h2>
        <p>
          California vehicle code 21651 explains the division of roads into
          lanes and opposing flows of traffic.
        </p>
        <p>
          When roads have multiple lanes and two-way traffic, drivers are
          required to be on the right side of the road. Dividers may split the
          road into two or more sections by barrier markings, curbs,
          double-parallel lines, or other markings. When barriers are present,
          drivers are not allowed to cross the divider.
        </p>
        <p>
          Not crossing the divider includes to make a left or U-turn on the
          divided highway unless the barrier has an opening which is designated
          for making turns. If making U-turn the driver must continue with the
          flow of traffic on what is now his right side of the road.
        </p>
        <p>
          Any driver who crosses a barrier divider to drive on the opposite,
          wrong, left side of the road may be punished by imprisonment if it
          injures or causes the death of any person.
        </p>
        <p>
          Drivers must know to drive with the flow of traffic and not against,
          and to stay to the right. Using common sense and staying in an
          appropriate state to drive by not being under the influence of drugs
          or alcohol, can help drivers make safe decisions and stay on the right
          side of the road.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
