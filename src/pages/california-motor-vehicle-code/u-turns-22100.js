// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22100 - U-Turns"
        pageDescription="Laws regarding left turns and u-turns is covered in California Motor Vehicle Code 22100."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22100</h1>
        <BreadCrumbs location={location} />
        <h2>U-Turns</h2>
        <p>
          California Vehicle Code 22100 covers all the legal requirements of
          turning while driving on the roads.
        </p>
        <p>
          Drivers who intend to turn onto highways (or any roadway) must do as
          follows:
        </p>
        <p>
          Right Turns: Both the approach for a right-hand turn and a right-hand
          turn itself shall be made as close as practicable to the right-hand
          curb or edge of the roadway except:
        </p>
        <ul>
          <li>
            Upon a highway having three marked lanes for traffic moving in one
            direction which terminates at an intersecting highway accommodating
            traffic in both directions. In this instance, a driver of a vehicle
            located in the middle lane may turn right from either the middle
            lane or the far right lane.
          </li>
          <li>
            When a right-hand turn is made from a one-way highway at an
            intersection,(as stated above) a driver shall approach the turn as
            provided in subdivision (a) and shall complete the turn in any lane
            lawfully available to traffic moving in that direction upon the
            roadway being entered.
          </li>
          <li>
            Upon a highway having signs or marking allowing drivers to turn from
            more than the far right-hand lane.
          </li>
        </ul>
        <p>
          Left Turns: The approach for a left turn shall be made as far as
          practicable to the left-hand edge of the extreme left-hand lane or
          portion of the roadway lawfully available to traffic moving in the
          direction of travel as the turning vehicle. When turning at an
          intersection, the left turn shall not be made before entering the
          intersection (i.e., don't cut the corners short). When making a left
          turn onto an intersection, turn into lanes made for travel in your
          direction and not into the lane for on-coming vehicles expect:
        </p>
        <ul>
          <li>
            Upon a highway having three marked lanes for traffic moving in one
            direction which terminates at an intersecting highway that
            accommodates traffic in both directions. In this instance, the
            driver in a middle lane may turn left from either the middle lane or
            the far left-hand lane and into any lane lawfully available upon the
            roadway being entered.
          </li>
        </ul>
        <p>
          U-turns: Only make u-turns at intersections where u-turns are allowed.
          You must always make a u-turn from the far left-hand lane.
        </p>
        <ul>
          <li>
            U-turns are not allowed in business districts unless an opening has
            been left in the lane divider to allow u-turns. Some intersections
            also allow u-turns if it is safe to do so.
          </li>
          <li>
            U-turns shall not be made in a residential district if an oncoming
            car is less than 200 feet away, except at an intersection where the
            oncoming car is stopped at a traffic light or stop sign.
          </li>
          <li>
            No u-turns shall be made in front of fire station driveways. Using
            fire station driveways as turnarounds is also prohibited.
          </li>
          <li>
            Do not make u-turns when you cannot see at least 200 feet in all
            directions.
          </li>
        </ul>
        <p>
          Signals: any signal of intention to turn must be given continuously
          during the last 100 feet traveled before making the turn.
        </p>
        <p>At BISNAR CHASE we want everyone to be safe using the roads. </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
