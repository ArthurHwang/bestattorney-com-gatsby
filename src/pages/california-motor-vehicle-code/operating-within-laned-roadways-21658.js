// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21658 - Operating Within Laned Roadways"
        pageDescription="It is the law that you must drive your vehicle reasonably inside of a lane according to California Vehicle Code 21658."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Operating Within Laned Roadways</h1>
        <BreadCrumbs location={location} />
        <p>
          Vehicle Code 21658 brings up the way traffic must operate within
          lanes. Lanes are the painted columns that designate the areas of
          opposing traffic as well as, within a flow of traffic, where vehicles
          may travel next to one another.
        </p>
        <p>
          If a roadway has been divided into two or more clearly marked lanes
          within traffic moving in one direction, vehicles must be driven
          reasonably within a single lane. This means that drivers must be aware
          of their vehicle and be careful to not glide over into other lanes
          without lawfully signaling. Switching lanes is allowed only if the
          movement can be made with reasonable safety and the use of a signal.
        </p>
        <p>
          On certain highways, signs will designate whether slow-moving traffic
          should use a certain lane. Often, the signs will require slow-moving
          traffic to use the lanes toward the right side of the road where
          vehicles enter and exit the roadway. When entering and exiting a
          highway, vehicles must adjust their speed and often slow down to exit
          or slowly speed up to join the flow of traffic upon entering. Having
          the right lane designated for these changes, as well as those
          traveling relatively slower than the rest of traffic, allows other
          drivers to maintain higher speeds in the left lanes.
        </p>
        <p>
          Drivers are responsible for knowing where they are on the road and
          driving respectfully within their lanes. Doing so helps drivers avoid
          collision with other drivers in nearby lanes as well as those in
          opposing traffic.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
