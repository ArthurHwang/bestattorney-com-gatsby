// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21756 - Passing Stopped Bus"
        pageDescription="California Motor Vehicle Code 21756 is a law that pertains to stopping and doing so at a safe distance behind a stopped trolley with passengers boardig and exiting."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21756</h1>
        <BreadCrumbs location={location} />
        <h2>Passing Stopped Bus</h2>
        <p>
          California Vehicle Code 21756 states that the driver of a car that is
          overtaking a stopped streetcar or trolley must stop their car at a
          safe distance behind the trolley and wait for all passengers to board
          and exit the trolley or bus before driving again. You cannot pass a
          trolley, streetcar or bus unless there is a designated safety zone for
          you to use. A safety zone is marked by raised buttons or markers on
          the road and is meant for pedestrians to use. You will most often see
          safety zones in areas where streetcars, trolleys or buses and vehicles
          share the roadway.
        </p>
        <p>
          If you are in an area where there is a safety zone or an officer
          directing traffic, you may pass a streetcar or trolley. First, you
          must make sure it is safe to pass. If it is safe to pass then you are
          allowed to pass so long as you don't exceed 10 mph. You also must use
          due caution and yield to pedestrians.
        </p>
        <p>
          You may only pass a trolley in a safety zone when the trolley has
          stopped to accept or discharge passengers. Never pass a moving trolley
          outside the safety zone. Be extra observant when you are trying to
          pass near an intersection. Cross-street traffic may not be able to see
          you.
        </p>
        <p>
          If you pass a trolley or streetcar too fast, or when it is unsafe you
          will be ticketed and fined. You could also be charged with negligence
          or reckless endangerment. Trolleys attract pedestrians, and many times
          the pedestrians will not watch for cars around the trolley. It is up
          to drivers to use caution and avoid accidents.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
