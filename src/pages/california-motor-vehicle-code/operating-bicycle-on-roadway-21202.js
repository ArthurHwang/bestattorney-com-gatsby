// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21202 - Operating a Bicycle on a Roadway"
        pageDescription="California Motor Vehicle Code 21202: Bike riders must stay as close to the right side of the roadway as possible while riding in the same lane as cars."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21202</h1>
        <BreadCrumbs location={location} />
        <h2>Operating a Bicycle on a Roadway</h2>
        <img
          src="/images/bike-accidents/bicyclist-on-road.jpg"
          alt="bicyclist on roadway"
          className="imgright-fixed"
        />
        <p>
          Vehicle Code 21202 is applicable to slow moving bicyclists and is
          similar in meaning to section 21654, which applies to slow moving cars
          and trucks. California Vehicle Code section 21202 states that bike
          riders must stay as close to the right side of the roadway as possible
          while riding in the same lane as cars. This law is intended to provide
          a safe and pleasant traveling experience for all persons using
          California roadways. This rule does have many safety exceptions,
          however.
        </p>

        <p>
          First, if you can bike fast enough to keep up with the other vehicles,
          then you don't have to stay close to the right side of the road. For
          those bikers who cannot keep up with the flow of traffic, you must
          stick close to the side of the road unless there is an unsafe
          circumstance that prevents riding too close to the side.
        </p>

        <p>
          Second, if the lane you are riding in is too narrow for a bicycle and
          a car to travel next to each other safely, then you should not ride
          too close to the side. Traveling too close to the side of the road
          will give the driver of the car the false sense that it is safe to
          pass you when it may not be. This could create a car accident that
          would cause{" "}
          <Link to="/bicycle-accidents">serious injuries to the bicyclist</Link>
          .
        </p>
        <p>
          Finally, you don't need to stay next to the curb if (1) you are
          preparing for a left hand turn, (2) you are passing other bikes,
          animals, or vehicles moving slower than you, (3) you are approaching a
          right hand turn, or (4) you are riding on a one-way street. If you are
          riding on a one-way road and there is more than one lane traveling in
          the same direction, you may also ride closer to the left hand curb if
          you so desire.
        </p>
        <p>
          By following this traffic law, and considering safety to both yourself
          and others, the streets can be an enjoyable place for everyone using
          them.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
