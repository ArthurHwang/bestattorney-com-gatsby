// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21456 - Pedestrian Traffic Signals"
        pageDescription="There are laws that pertain to pedestrians just as there are laws that pertain to motorists. See California Motor Vehicle Code 21456."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Pedestrian Traffic Signals</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/pedestrian-accidents/pedestrian-signal.jpg"
          className="imgleft-fixed"
          width="300"
          alt="pedestrian traffic signal"
        />
        <p>
          Pedestrians must heed the indications made by regular traffic signals
          and signs unless there is a pedestrian control signal present. These
          controls explicitly tell pedestrians when it is legal to cross through
          an intersection using a cross walk, and when it is not.
        </p>
        <p>
          "WALK" or a signal showing a "walking person" is the indication that
          it is OK for a pedestrian facing the signal to cross the road.
          Pedestrians must, however, yield to vehicles that are already in the
          intersection when the pedestrian control signal goes on. This traffic
          is considered to have the right-of-way, and a pedestrian may continue
          across the street when the vehicles have passed and it is safe.
        </p>
        <p>
          Flashing pedestrian signals showing "DON'T WALK", "WAIT", or the
          "upraised hand" mean that pedestrians facing this signal are not
          allowed to begin crossing the roadway. Any pedestrian who has already
          begun traveling across the street when one of these symbols flashes is
          allowed to complete crossing.
        </p>
        <p>
          Pedestrians who cross the street even when the pedestrian traffic
          signal or regular traffic signals show it is not time to cross may be
          subject to jaywalking fines. Jaywalking is considered illegal or
          reckless pedestrian crossing of a roadway that ignores traffic signals
          or happens outside a crosswalk.
        </p>
        <p>
          Paying attention to pedestrian and regular traffic signals helps make
          traveling by foot safe even when done so in the presence of vehicles.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
