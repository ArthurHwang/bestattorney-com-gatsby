// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23111 - Discharging Items Onto Roads"
        pageDescription="California Motor Vehicle Code 23111 is the law that refers to littering."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23111</h1>
        <BreadCrumbs location={location} />
        <h2>Discharging Items Onto Roads</h2>
        <p>
          Most people understand that littering is bad for the environment, but
          it is also illegal and can be punished with a fine. What Code 23111
          refers to as the "Paul Buzzo Act," explains that no person in a
          vehicle or pedestrian is permitted to throw litter or any object on a
          highway, private, or public adjoining space.
        </p>
        <p>
          This code applies to all objects or substances including lit or unlit
          cigarettes and cigars, matches, or any other glowing substances.
          Expelling items from moving vehicles, or onto roads where vehicles
          operate, is not only unsightly but dangerous. Objects on fire are
          hazardous and can ignite exposed gasoline while other debris may be
          sharp and cause flat tires in vehicles. Larger objects may obstruct
          the flow of traffic or cause vehicles to suddenly swerve and lose
          control.
        </p>
        <p>
          It is the responsibility of drivers, pedestrians, and anyone who uses
          the roads and highways to do their personal best to keep them clean
          and safe.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
