// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21716 - Golf Carts"
        pageDescription="Golf carts are restricted from driving on fast-moving roadways. Find out more about this law here- California Motor Vehicle Code 21716. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21716</h1>
        <BreadCrumbs location={location} />
        <h2>Golf Carts</h2>
        <p>
          According to California Vehicle Code 21716, golf carts have the same
          rights on the roads as all other vehicles, except they are restricted
          from driving on fast-moving roadways. They also come with the same
          regulations as other vehicles, meaning that most of the time the local
          authorities will required having a drivers license to drive on public
          roadways. Wearing a seatbelt and having safety features like mirrors,
          a windshield, and brake lights are also required. The exception to
          this rule may be on private property such as golf courses our private
          roads, where local rules specifically allow golf carts. Most golf
          carts are designed to travel at 15 mph or less. This is why they are
          only allowed on roads where the speed limit is at or below 25 mph.
        </p>
        <p>
          California legislators are concerned with the high levels of air
          pollution in the major cities, and the use of more environmentally
          friendly transportation is encouraged. For this reason, golf carts are
          permitted on many city streets. They are a great means of
          transportation for a quick trip to the grocery store, or an afternoon
          at the park.
        </p>
        <p>
          There are a few exceptions to the rules of the road that apply to golf
          carts. These vehicles are not allowed to operate on any street where
          the speed limit exceeds 25 miles per hour. Crossing roadways where the
          speed is greater than 25 mph is permitted only if the street you are
          leaving and the street you are going to are both 25 mph roads, and you
          can cross the street safely. Golf carts can also be prohibited from
          operating on any public street if local authorities in your city deem
          them unsafe. Signs must be posted to let you know they are prohibited.
        </p>
        <p>
          Keep in mind that golf carts do not come with the same safety
          equipment as cars and trucks. There are no air bags, no roll bars, and
          very little protection for passengers if you are involved in a{" "}
          <Link to="/car-accidents">crash with another vehicle</Link>. While
          operating on a heavily traveled roadway may be legal in your town, it
          may not be safe. Use good judgment when traveling in a golf cart.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
