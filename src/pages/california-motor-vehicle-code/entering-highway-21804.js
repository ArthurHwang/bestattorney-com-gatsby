// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21804 - Entering Onto a Highway"
        pageDescription="California Motor Vehicle Code 21804 discusses entering onto a highway and the laws associated."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21804</h1>
        <BreadCrumbs location={location} />
        <h2>Entering Onto a Highway</h2>
        <p>
          Vehicle code 21804 discusses the way drivers must enter onto or cross
          a highway. If the driver intends to enter onto a highway from a space
          of public or private property or an alley, the driver must yield the
          right-of-way to all closely approaching traffic already present on the
          highway.
        </p>
        <p>
          After slowing and yielding, the driver may continue onto the highway
          if no oncoming vehicles are too close such that entering the road
          would be hazardous. As the driver enters the highway, however, all the
          oncoming vehicles must yield so the driver can merge with the flow of
          traffic and reach an appropriate speed.
        </p>
        <p>
          Using common sense and being aware of the surroundings is essentially
          when operating a vehicle and crossing other vehicles' paths. Pulling
          out into a road too closely to oncoming traffic could cause an
          accident. Drivers on the road who continue to speed up behind drivers
          who are merging also cause a hazard.
        </p>
        <p>
          Making space for other drivers, maintaining a reasonable speed, and
          keeping a reasonable distance from the vehicle in front are important
          factors for the success of merging entry roads with highways.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
