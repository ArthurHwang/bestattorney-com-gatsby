// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23110 - Throwing Objects at Vehicles"
        pageDescription="California Motor Vehicle Code 23110- It is dangerous and illegal to throw objects at moving vehicles. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23110</h1>
        <BreadCrumbs location={location} />
        <h2>Throwing Objects at Vehicles</h2>
        <p>
          California Vehicle Code 23110 addresses the illegality of throwing any
          object or substance at a vehicle in operation. According to the code,
          any person who throws anything at a vehicle or occupant of a vehicle
          while the vehicle is being operated on a highway is guilty of a
          misdemeanor.
        </p>
        <p>
          Though it may seem very strict, the code helps to prevent people from
          doing anything that might obstruct a driver's view of the road and
          cause a collision. Objects and substances include litter and debris
          that drivers may illegally throw from their vehicles. This trash could
          land on another driver's windshield or be tossed upon or under another
          vehicle, causing the driver to lose control.
        </p>
        <p>
          Code 23110 also notes that if a person intentionally attempts to cause
          bodily injury to a driver or occupant of a vehicle by throwing a rock,
          brick, bottle, or any other object at a moving car, it is considered a
          felony. Any intention to cause bodily harm to a driver or other
          vehicle may be punished by imprisonment.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
