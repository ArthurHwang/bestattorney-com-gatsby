// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21453 - The Red Light Indicator"
        pageDescription="Laws pertaining to a red traffic signal is explained in California Motor Vehicle Code 21453."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The Red Light Indicator</h1>
        <BreadCrumbs location={location} />
        <p>
          The red circular signal always indicates that the driver facing the
          red light must stop before the crosswalk and intersection. It is
          illegal to enter the intersection until after the red light changes
          and a green light is indicated. In warning that a red light will soon
          appear, a yellow one is shown.
        </p>
        <p>
          <img
            src="/images/pedestrian-accidents/no-turn-on-red.jpg"
            className="imgleft-fixed"
            alt="red traffic light"
          />
          A driver is allowed to continue through an intersection legally if the
          red light has turned after the driver already entered the
          intersection. Reading the yellow light carefully and slowing down will
          prevent drivers from "running a red light" illegally.
        </p>
        <p>
          There are couple instances when it is legal to enter the intersection
          while the red light is indicated. The first instance is the more
          common: a driver may stop at the red light and then proceed through a
          portion of the intersection to make a right hand turn, as long as the
          driver makes a complete stop and yields to any pedestrians or vehicles
          that have the right-of-way. It is also legal to enter the intersection
          during a red light if the driver is turning from a one-way street onto
          another one-way street. These actions may be taken unless there is a
          sign prohibiting turns during red lights.
        </p>
        <p>
          A red arrow signal indicates the same thing: vehicles are prohibited
          to enter the intersection, unless making a safe, legal turn.
        </p>
        <p>
          Red lights mean the same to pedestrians crossing an intersection in
          the cross walk. If the red light is shown, the pedestrian must wait
          for a sign that allows movement, unless a pedestrian control signal
          indicates otherwise.
        </p>
        <p>
          Drivers should avoid entering intersections during red lights by
          slowing down during yellow lights. "Running a red light" is dangerous
          and often results in accidents involving cross-traffic.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
