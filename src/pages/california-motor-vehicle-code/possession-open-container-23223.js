// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23223 - Possession of Open Container"
        pageDescription="California Motor Vehicle Code 23223 states that possession of an open container of alcohol while driving is prohibited."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23223</h1>
        <BreadCrumbs location={location} />
        <h2>The Possession of Alcohol While Driving a Motor Vehicle</h2>
        <p>
          According to Vehicle Code 23223, no one is permitted to drive a
          vehicle with an open container of alcohol whether it has been opened,
          seal broken, or partially empty. This includes bottles, cans, or other
          receptacles.
        </p>
        <p>This same law applies to passengers in a moter vehicle.</p>
        <p>
          Please go to{" "}
          <Link
            to="https://dmv.ca.gov/pubs/vctop/d11/vc23223.htm"
            target="_blank"
          >
            https://dmv.ca.gov
          </Link>{" "}
          for more information.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
