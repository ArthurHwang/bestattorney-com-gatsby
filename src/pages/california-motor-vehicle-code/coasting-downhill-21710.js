// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21710 - Coasting Downhill Prohibited"
        pageDescription="California Motor Vehicle Code 21710 makes it illegal to coast your car when travelling downhill "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21710</h1>
        <BreadCrumbs location={location} />
        <h2>Coasting Downhill Prohibited</h2>
        <p>
          California vehicle code 21710 states that it is illegal to coast your
          car when traveling downhill.
        </p>
        <p>
          This law may seem dumb at first, but there are good reasons for
          keeping your car in gear while going down a hill:
        </p>
        <ol>
          <li>
            Better control of the vehicle. If you are coasting and something
            runs out in front of you, you have less control over your vehicle
            when it is not in gear. Gravity may not be your friend if you
            accidentally overcorrect to avoid a deer or a pothole.
          </li>
          <li>
            Engine could die in neutral. No matter how great you may think your
            car is, it can stop running unexpectedly. Driving at high altitudes
            is hard on the motor, and a car in neutral will sometimes die on you
            while you're coasting down hills, especially if it's a long hill.
          </li>
          <li>
            Brakes burn up faster on downgrades. If you're car is in neutral, it
            will naturally speed up going downhill. This requires you to apply
            more pressure on the brakes to keep it at a reasonable speed. The
            extra pressure on the brake pads will cause them to heat up and burn
            away faster. It is possible to lose your braking abilities if it is
            an exceptionally long hill.
          </li>
          <li>
            Coasting will not save gas. Once you are at the bottom of the hill,
            you will have to put the car back into a driving gear. The extra gas
            used to do this, makes up for the little bit you may have just saved
            while in neutral. It evens out in the end, and does not save you any
            gas mileage.
          </li>
          <li>
            Removal of feet from pedals. Many people take their feet away from
            the pedals while coasting. They take this time to relax and take a
            break from the driving strain on their ankles. If your feet are not
            on the pedals, you have no way to avoid sudden and unexpected road
            hazards.
          </li>
        </ol>
        <p>
          If those reasons are not enough to convince you to keep your car in
          gear at all times, there are always fines and penalties that can
          convince you. You could pay fines and court costs up to $200 for your
          first offense. If you are a repeat offender, you could pay around
          $1000 after all penalties and fees have been assessed.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
