// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Driving Safely Through Intersections -- Green Lights"
        pageDescription="California Motor Vehicle Code 21451 is about green lights and traveling safely through intersections. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Traveling Through Intersections Safely: Green Lights</h1>
        <BreadCrumbs location={location} />
        <p>
          Green is the favorite light. It allows drivers to continue on their
          respective routes. Still, it is important to understand the signals
          that different kinds of green lights indicate for drivers, and to
          further consider the surrounding vehicles and pedestrians while
          passing through intersections.
        </p>
        <p>
          <img
            src="/images/pedestrian-accidents/green-traffic-signal.jpg"
            className="imgleft-fixed"
            alt="green traffic light"
          />
          The green light typically means "go." A driver facing the circular
          green signal is allowed to proceed straight through the intersection,
          make a right-hand turn, make a left-hand turn, or make a U-turn. All
          of these actions are appropriate unless there is a sign posted
          prohibiting a U-turn or pedestrians obstructing the passageway while
          using the crosswalk. In the latter situation, the vehicle with a green
          light must yield to the pedestrians.
        </p>
        <p>
          If a driver is facing a left green arrow signal, he or she may enter
          the intersection only to complete the movement indicated by the left
          green arrow light. Sometimes, green arrow lights are accompanied by
          other lights - such as circular green lights - in which case, the
          driver facing the pair of light can complete either movement. Drivers
          facing the left green arrow light, may make a U-turn unless a sign
          shows that it is prohibited.
        </p>
        <p>
          Pedestrians must also pay attention to green lights. If a pedestrian
          is facing the circular green signal, or a left arrow green signal, he
          or she may cross cross the roadway within a marked or unmarked
          crosswalk. If there are other pedestrian control signals (as described
          in Section 21456) present, the pedestrian must also pay attention to
          these and noting any restrictions the signals might show. For
          instance, a pedestrian may face a green signal and a do-not-cross
          pedestrian control signal at the same time. In this case, the
          pedestrian must wait for the pedestrian control signal to show that it
          is OK to cross.
        </p>
        <p>
          Paying attention to traffic signals is vital to the success of
          traveling by motor vehicle. Those on foot or in a vehicle who break
          laws governing traffic signals may receive fines or cause harmful
          accidents.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
