// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23222 - Open Containers"
        pageDescription="California Motor Vehicle Code 23222 goes hand in hand with vehicle code 23220 regarding open containers of alcohol in a vehicle."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23222</h1>
        <BreadCrumbs location={location} />
        <h2>Open Containers While Driving</h2>
        <p>
          Working alongside code 23220, vehicle code 23222 prohibits any driver
          from operating a vehicle with an open container of alcohol. An open
          container is considered any bottle, can, or receptacle containing an
          alcoholic beverage that has a broken seal or the contents of which
          have been even partially removed.
        </p>
        <p>
          Drivers must take specific care when transporting open containers, or
          alcoholic containers where the seal has been broken, even if the
          driver is not drinking from the container while driving. Keeping
          containers of this variety out of arm's reach and in the trunk is the
          best option, though a police officer may be suspicious of any open
          container in the the vehicle.
        </p>
        <p>
          This law is extended to marijuana. Any driver found to be operating a
          vehicle with less than one avoirdupois ounce of marijuana in his
          possession, will be punished with an infraction and fine. Larger
          amounts of marijuana, or larger concentrations of cannabis, will be
          punished with more serious consequences.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
