// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22112 - Stopping Behind a School Bus - DMV"
        pageDescription="California Motor Vehicle Code 22112. Call 949-203-3814 for top-rated personal injury lawyers. No win, no fee attorneys. Free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22112</h1>
        <BreadCrumbs location={location} />
        <h2>Stopping Behind a School Bus</h2>
        <p>Drivers must know what to expect when driving near a schoolbus.</p>
        <p>
          Schoolbuses carry students of all ages to and from school and are
          subject to make frequent stops. When a schoolbus is going to make a
          stop, surrounding vehicles can expect to see the bus show an amber
          warning light at least 200 feet before the schoolbus stop. The amber
          light is turned off after the bus reaches the stop, and a red flashing
          light signal and stop signal arm will be turned on the whole duration
          the bus is stopped. These red lights indicate that pupils are loading
          and unloading, and will be outside the schoolbus and possibly in or
          near the road.
        </p>
        <p>
          Schoolbuses will only show these amber and red lights when stopping,
          and will only stop at a designated schoolbus stop that has been
          authorized by the superintendent. The lights will always be shown
          before the door to the schoolbus is opened.
        </p>
        <p>
          Occasionally schoolbus drivers must stop on a highway or private road
          where traffic is not controlled by a traffic officer. Under these
          circumstances, the schoolbus driver will escort all children up to the
          8th grade if they need to cross the highway or private road. The
          driver will hold a hand-held "STOP" sign while the children cross, and
          make sure that everyone crossing does so in front of the bus. When all
          students have loaded and those who have unloaded are a safe distance
          from the bus, the schoolbus can safely continue its route.
        </p>
        <p>
          Schoolbus drivers are not permitted to signal the amber warning light
          system, flashing red light signal system, or the stop signal arm under
          these circumstances:
        </p>
        <ul>
          <li>When the schoolbus is lawfully parked.</li>
          <li>If the schoolbus is disabled due to mechanical problems.</li>
          <li>
            When the driver is helping a pupil in need of physical assistance
            and the assistance extends the length of time of a regular schoolbus
            stop.
          </li>
          <li>
            Where the road surface is covered in show or ice and requiring
            traffic to stop for the bus would be hazardous.
          </li>
          <li>
            When on a highway where the speed limit is 55 miles per hour or
            higher.
          </li>
          <li>
            In a location that has been deemed hazardous by the school district
            and approved by the Department of California Highway Patrol.
          </li>
        </ul>
        <p>
          Please see complete listing of{" "}
          <Link to="/california-motor-vehicle-code" target="_blank">
            California Motor Vehicle Codes
          </Link>
          .
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
