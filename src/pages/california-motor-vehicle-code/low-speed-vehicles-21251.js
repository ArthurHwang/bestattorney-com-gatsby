// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21251 - Low-Speed Vehicles"
        pageDescription="California Motor Vehicle Code 21251 states that low speed vehicles have the same rights as all other vehicles on the roadway."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21251</h1>
        <BreadCrumbs location={location} />
        <h2>Low-Speed Vehicles</h2>
        <p>
          According to California Vehicle Code 21251, low speed vehicles have
          the same rights on the roads as all other vehicles. Low speed vehicles
          are electric vehicles that resemble glorified golf-carts. Low speed
          vehicles are usually designed to go speeds up to 25 mph, where golf
          carts are designed to travel at only 15 mph.
        </p>
        <p>
          These electric cars are fuel efficient and better for the environment.
          California legislators are concerned with the high levels of air
          pollution in the major cities, and the use of more environmentally
          friendly transportation is encouraged. For this reason, electric cars
          are permitted on many city streets.
        </p>
        <p>
          There are a few exceptions to the rules of the road that apply to low
          speed vehicles. These vehicles are not allowed to operate on any
          street where the speed limit exceeds 35 miles per hour. Crossing
          roadways where the speed is greater than 35 mph is permitted only if
          the street you are leaving and the street you are going to are both 35
          mph roads. These cars can also be prohibited from operating on any
          public street if local authorities in your city deem them unsafe.
        </p>
        <p>
          If local authorities determine the use of low speed vehicles on their
          roads is unsafe, they must post signs letting drivers know the use of
          low speed vehicles has been prohibited.
        </p>
        <p>
          Keep in mind that low speed vehicles do not come with the same safety
          equipment of higher speed motor vehicles. There are no air bags, no
          roll bars, and very little protection for passengers if you are
          involved in a crash with another car or truck. While operating on a
          heavily traveled roadway may be legal in your town, it may not be
          safe. Use good judgment when traveling in a low speed vehicle.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
