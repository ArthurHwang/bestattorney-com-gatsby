// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21661 - Narrow Roadways"
        pageDescription="California Motor Vehicle Code 21661 states that you must yield to other drivers for safety on a narrow road. Drivers going uphill have the right of way."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21661</h1>
        <BreadCrumbs location={location} />
        <h2>Narrow Roadways</h2>
        <p>
          If you are driving on a narrow roadway, you must be aware of other
          drivers, and yield to them for safety. California Vehicle Code 21661
          states that the driver traveling downhill must pull over and allow the
          driver driving up hill to pass. If there is no room for you to pull
          over, you must backup enough so the driver facing uphill has room to
          pass.
        </p>
        <p>
          A narrow roadway is one that is not designed to allow two cars to pass
          each other safely. These roads are usually private drives, or rural
          roads where there is not enough room to widen the road to allow for
          two cars.
        </p>
        <p>
          If you are going downhill, you must pull off the road as soon as it is
          safe to do so, to allow the approaching car to pass safely. The car
          traveling downhill is in a better position to pull over than the one
          traveling uphill. The driver going downhill can see farther, has a
          better vantage point to see where there is a safe spot to pull over,
          and will be able to pull back safely onto the roadway when safe. The
          car facing downhill also has more control of their vehicle when
          backing up to allow for safe passage. The car going uphill does not
          have these advantages.
        </p>
        <p>
          If there is enough room for the approaching car to pass safely, you do
          not need to pull completely off the road. Just pull over and give the
          ascending vehicle enough safe distance to pass. The ascending vehicle
          should have at three-fourths of the roadway available to them.
        </p>
        <p>
          Recreational vehicles and larger trucks are usually not allowed on
          narrow roads. They cannot safely backup to allow other cars to pass.
          Be sure to check with your local authorities if you are unsure whether
          your RV is allowed to drive on rural roads.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
