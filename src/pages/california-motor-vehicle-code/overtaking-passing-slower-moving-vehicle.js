// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21750 - Overtaking"
        pageDescription="Overtaking is defined in the California Motor Vehicle Code 21750 as passing another vehicle in an unsafe manner such as passing on a 2 lane road."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21750</h1>
        <BreadCrumbs location={location} />
        <h2>Overtaking</h2>
        <p>
          If a driver is behind a bicycle or a slower-moving vehicle, he can
          pass the person in front of him by overtaking. This is must be done
          carefully and without interfering with the safe operation of the
          overtaken vehicle or bicycle.
        </p>
        <p>
          Passing of another vehicle is often done on two-lane roads, when the
          center dividing line is broken. A driver in this situation must be
          sure that no oncoming traffic is close before crossing over into the
          left lane and moving ahead of the vehicle or cyclist in the right
          lane.
        </p>
        <p>
          Passing must always be done to the left of the vehicle being passed,
          and can be done on multi-lane roads as well. It is important to leave
          the passed vehicle a safe cushion of space and to not pass excessively
          fast or aggressively. If passing is done on two-lane road, where the
          driver must pass through the opposing traffic's lane, then it must be
          done quickly but never in excess of the speed limit.
        </p>
        <p>
          There are times when the center dividing line is comprised of double
          yellow lines with or without road bumps. These are lines that are not
          to be crossed, and no passing or overtaken should be done where a
          vehicle passes over the solid double yellow lines.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
