// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23224 - Possession of Alcohol"
        pageDescription="If a person is under age 21 it is illegal to drive a vehicle that is transporting an alcoholic beverage as stated in vehicle code 23224."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23224</h1>
        <BreadCrumbs location={location} />

        <h2>
          Persons Under the Age of 21 and the Possession of Alcohol in a Vehicle
        </h2>
        <p>
          According to Vehicle Code 23224, no one under the age of 21 is
          permitted to drive a vehicle that is also transporting an alcoholic
          beverage. The reason for this is that it is illegal for anyone under
          the age of 21 to consume alcohol, and further illegal for anyone under
          the age of 21 to have alcohol in his or her possession.
        </p>
        <p>
          Any alcohol, or other substances, in the vehicle with the underage
          driver will be considered within the driver's possession. An exemption
          to this rule is if the underage driver is accompanied by an adult,
          adult relative, or other adult designated by a legal guardian who
          assumes possession of the alcohol.
        </p>
        <p>
          If the driver is employed by a licensee under the Alcoholic Beverage
          Control Act and is driving with alcohol in the vehicle during work
          hours and in the course of his or her employment, it is also OK to
          transport alcohol.
        </p>
        <p>
          If an underage driver is found to have alcohol in the vehicle while
          driving, but a defense is supported by the parents that the alcohol
          played a role in reasonable instructions given by a parent or legal
          guardian and the alcohol does not belong to the driver, then
          punishment for the underage driver may be avoided.
        </p>
        <p>
          For the sake of young drivers, avoiding contact with alcohol in any
          form or container while driving may be the easiest way to steer clear
          from confusion or penalty.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
