// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Adhering to a person Controlling Traffic --California Code 21100.3"
        pageDescription="21100.3. If you were injured because another person violated vehicle code 21100, you might have a personal injury case. Call 949-203-3814 for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21100.3</h1>
        <BreadCrumbs location={location} />
        <h2>Obedience to Person Controlling Traffic</h2>
        <p>
          California Vehicle Code 21100.3 states that every driver must obey an
          authorized traffic officer. However, there is no definition given as
          to what constitutes an authorized traffic control officer. For the
          layperson, a traffic control officer could mean anyone from a police
          officer to a crossing guard to a firefighter working at an accident
          scene.
        </p>
        <p>
          The State of California defines an authorized traffic control officer
          as someone who is "authorized or appointed by a local government
          authority." In addition to being appointed by local government
          officials, this person must also be in uniform and/or wearing an
          official insignia that has been given to them at their time of
          appointment. Failure to follow the directions of an authorized traffic
          control officer could result in your receiving a traffic ticket and
          having one point added to your license.
        </p>
        <p>
          School crossing guards, firefighters, construction workers, and
          private citizens directing traffic are not authorized persons for the
          purpose of CVC 21100.3. Even though the aforementioned persons are not
          authorized, it is still a good idea to follow the directions of anyone
          directing traffic. There could be dangerous consequences involved if
          their directions are ignored. If you decide to ignore anyone directing
          traffic, you could be charged with reckless driving; especially if
          there is an injury or property damage.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
