// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21714 - Prohibitd Areas for Vehicles"
        pageDescription="California Motor Vehicle Code 21714 defines the law pertaining to motorbikes and bicycles riding in between lanes or cars."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21714</h1>
        <BreadCrumbs location={location} />
        <h2>
          Motorcyclists or Motorized Bicyclists; Operation of Vehicle;
          Prohibited Areas
        </h2>
        <p>
          California Vehicle Code 21714 makes it illegal to ride on, or
          immediately adjacent to, markers designating lanes or between two or
          more vehicles traveling in adjacent lanes. This is often referred to
          as lane splitting.
        </p>
        <p>
          Vehicle Code 21714 only applies to fully enclosed three-wheeled
          motorcycles and cars. If you're familiar with the Sparrows that were
          built in California from 2000-2002, then you've seen what the
          legislators had in mind when they enacted this rule.
        </p>
        <p>
          Regular two-wheeled motorcycles are allowed to split lanes. California
          is one of the only states that allows for lane splitting on
          motorcycles. If you split lanes, remember that it is still incredibly
          unsafe, and most car and truck drivers don't know its legal and may
          try to cut you off.
        </p>
        <p>
          Vehicle Code 21714 makes it illegal to split lanes in a three-wheeled
          motorcycles or cars because they are wider than two wheeled
          motorcycles. Because of their extra width, there is often not enough
          room between cars for a three-wheeled cycle to fit safely.
        </p>
        <p>
          It is also illegal for these three-wheeled vehicles to operate in the
          HOV lanes, unless there are signs posted which specifically allow it.
          With the new technology being introduced with these vehicles, the law
          may change in the future. For now, however, stay out of the car pool
          lanes.
        </p>
        <p>
          If you violate vehicle code section 21714, you will have points added
          to your license and receive fines. For your first offense, you could
          receive as much as $300 in penalties and court costs.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
