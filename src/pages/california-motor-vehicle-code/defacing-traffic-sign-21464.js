// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21464 - Defacing Traffic Signs"
        pageDescription="Defacing traffic signs is against the law. Read California Motor Vehicle Code 21464 to find out more. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21464</h1>
        <BreadCrumbs location={location} />
        <h2>Defacing Traffic Signs</h2>
        <p>
          If you are caught defacing a traffic device or traffic sign, you will
          be ticketed and could receive as much as five-thousand-dollars in
          fines. If your defacing of the signs or traffic devices causes
          injuries or death, you may also receive serious jail time.
        </p>
        <p>
          There are several means of defacing devices such as: placing bumper
          stickers on signs, spray-painting over them, placing graffiti on
          traffic devices or road signs, or any other means of damaging or
          changing the appearance of the device. Any of these measures could
          cause a driver to become distracted and have an accident.
        </p>
        <p>
          A mobile infrared transmitter is a device that is capable of changing
          traffic signals from red to green on demand. These devices are illegal
          to own, sell, or use. These MITs are easy to find online, but you will
          face severe penalties if you are caught with one. They are dangerous
          to have in your possession because the temptation to use the device is
          too great. This is why it is illegal to own one in California.
        </p>
        <p>
          The original purpose of the mobile infrared traffic device (MIRT) was
          to allow emergency vehicles and funeral escorts to change traffic
          lights ahead of them. Emergency responders cannot always depend on the
          public to pull to the right and allow them to pass. The use of these
          mobility devices allows emergency vehicles to change lights ahead of
          them from red to green so they can arrive at emergencies sooner.
        </p>
        <p>
          Traffic signals, road signs, and other temporary traffic devices are
          placed on the streets for the safety of the public. Any changes to
          these devices could result in serious injuries or death. If a car is
          involved in an accident as a result of a non-functioning traffic
          device, the person who altered the device could face serious
          consequences. For this reason, defacing these devices is a serious
          offense and will be punished accordingly.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
