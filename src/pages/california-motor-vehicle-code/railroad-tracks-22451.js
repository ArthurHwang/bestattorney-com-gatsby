// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22451 - Railroad Tracks"
        pageDescription="All vehicles and pedestrians have to stop at railroad tracks and stay 15 feet away when a train is approaching. California Motor Vehicle Code 22451"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22451</h1>
        <BreadCrumbs location={location} />
        <h2>Crossing Railroad Tracks</h2>
        <p>
          California Vehicle Code 22451 discusses the necessary space vehicles
          and pedestrians must keep from railroad tracks when stopping.
        </p>
        <p>
          When a visible electric or mechanical signal device, or a flagman, is
          present at the railroad crossing and it signals that a train is
          approaching, all vehicles and pedestrians must stop at least 15 feet
          away from the nearest rail.
        </p>
        <p>
          Vehicles and pedestrians must also stay 15 feet from the rail anytime
          a train is approaching even if the crossing is not guided by a signal
          or flagman. In this situation,the driver or pedestrian must exercise
          utmost safety by looking for an approaching train or car, and by
          listening for an audible signal. Because trains travel at extremely
          high speeds, keeping the 15 feet distance from the rail until the
          train passes is the best way to avoid an immediate hazard.
        </p>
        <p>
          If a railroad crossing is equipped with a signal device of any kind,
          all pedestrians and vehicles that wish to cross are subject to
          stopping the required distance from the rail until the train passes
          and the signal ends. It is illegal and dangerous to attempt to proceed
          through or under any railroad or rail transit crossing gate when the
          gate is closed and warning of an approaching train.
        </p>
        <p>Respect the train and the space required for safe passing.</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
