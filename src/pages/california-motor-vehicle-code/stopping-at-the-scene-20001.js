// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 20001 - Stopping at the Scene of an Accident"
        pageDescription="California Motor Vehicle Code 20001 states that it is unlawful to drive away from the scene of an accident if you caused the accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 20001</h1>
        <BreadCrumbs location={location} />
        <h2>Stopping at the Scene of an Accident</h2>
        <p>
          If a driver causes or is involved in a car accident, he must
          immediately stop at the scene. Driving away from an accident,
          especially one where occupants of other vehicles may be injured, is
          not only irresponsible but against laws discussed in vehicle codes
          20001, 20003, and 20004.
        </p>
        <img
          src="/images/car-accidents/clay-cars.jpg"
          className="imgright-fixed"
          alt="car accident scene"
        />
        <p>
          Suppose an accident occurs where one driver collides with another. The
          colliding driver must stop, report the accident, call for emergency
          aid, check on the occupants of the other vehicle, and give his or her
          name, driver's license, and address information to authorities. The
          information of all involved people and vehicles must be reported.
        </p>
        <p>
          {" "}
          <Link to="/pedestrian-accidents/hit-and-run">
            Fleeing a scene of a car accident
          </Link>{" "}
          or failing to provide the required information after an accident is
          punishable by imprisonment or fines no less than $1,000. If an
          occupant of the collided vehicle is injured or killed, the penalty for
          a driver who "hits and runs" may increase.
        </p>
        <p>
          The reason for such severe punishments as described in this section is
          that a driver who causes an auto accident must be held accountable for
          causing damage to another person. Not notifying authorities and
          emergency services of an accident could result in the death of an
          injured passenger, who could have been saved had help come sooner.
          Further, a driver who causes an accident may be held responsible for
          breaking other pertinent laws, such as driving under the influence of
          drugs or alcohol, which played a role in the collision.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
