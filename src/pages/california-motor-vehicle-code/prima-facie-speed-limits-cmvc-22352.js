// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22352 - Prima Facie Speed Limits"
        pageDescription="California Motor Vehicle Code 22352 is all about Prima Facie Speed Limits and what drivers need to know."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22352</h1>
        <BreadCrumbs location={location} />
        <h2>Prima Facie Speed Limits CMVC 22352</h2>
        <p>
          California vehicle code 22352 informs drivers of what they need to
          know about prima facie speed limits. These limits are instated unless
          otherwise posted.
        </p>
        <h2>Fifteen miles per hour:</h2>
        <ul>
          <li>
            Drivers must travel at fifteen miles an hour when traversing the
            last 100 feet toward a railway grade crossing where the view is not
            clear for a distance of at least 400 feet in the distance. This
            speed limit does not apply when a flagman is on duty or a clearly
            visible electrical or mechanical crossing signal is installed.
          </li>
          <li>
            Drivers must also operate at fifteen miles an hour when approaching
            an intersection where, during the last 100 feet, the driver does not
            have a clear view of the intersection and all of the highways
            crossing. This rule does not apply when the intersection is equipped
            with a stop sign, yield right-of-way signs, or an official traffic
            control signal.
          </li>
          <li>
            Drivers must drive through alleyways at no faster than fifteen miles
            per hour. This allows for the driver to stop and yield to any
            people, animals, or objects that may cross the path of the alley.
          </li>
        </ul>
        <h2>Twenty-five miles per hour:</h2>
        <ul>
          <li>
            Any highway other than a state highway, business, or residence
            district requires a speed limit of twenty-five miles per hour. This
            is statewide unless otherwise determined by a local authority and
            clearly posted.
          </li>
          <li>
            Drivers must travel at twenty-five miles per hour when approaching
            or passing a school building or grounds. This speed limit is
            required during all times when students may be outside of the school
            including the morning when school begins, noon recess, and after
            school when students leave. Most schools are equipped with standard
            "SCHOOL" warning signs which are located 500 feet from school
            grounds to remind drivers to slow down in the presence of students.
          </li>
          <li>
            Twenty-five miles an hour is the speed limit when passing a senior
            center or facility for senior citizens that is located on the street
            other than a state highway. These facilities are also often posted
            with "SENIOR" warning signs.
          </li>
        </ul>
        <p>
          Adhering to these speed limits is important for the safety of
          surrounding pedestrians, residents, students, and seniors, as well as
          the safety drivers.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
