// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21221 - Motorized Scooters"
        pageDescription="California Motor Vehicle Code 21221 refers to motorized scooter laws in the state of Caklifornia. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21221</h1>
        <BreadCrumbs location={location} />
        <p>
          For the purpose of this code, the state of California defines
          motorized scooters as any self propelled two wheeled scooter designed
          to travel under 20 mph. The DMV definition is closer to what you would
          typically associate with a child's toy. If it has a place to stand,
          with or without a seat, and can also be powered by human propulsion,
          then it is probably a scooter. Mopeds also fall under motorized
          scooters for this code section.
        </p>
        <img
          src="/images/bike-accidents/scooter.jpg"
          className="imgright-fixed"
          alt="motorized scooter"
        />{" "}
        <p>
          Motorized scooters have the same rights and responsibilities as cars
          on California roads. If you are riding a scooter on the streets, think
          of it as if it were a motorized bicycle.{" "}
          <Link to="/california-motor-vehicle-code/motorized-scooters-bike-lanes-21228">
            Bicycle laws are very similar to the laws for scooters
          </Link>
          . Riders must have proper safety equipment and a fully functional
          vehicle to operate on public roads. Also, riders who are convicted of
          a DUI while riding on their scooters will receive the same $250 fine
          as car drivers and bicyclists. There are noise laws that apply to
          scooters and mopeds. If your scooter came with a muffler, it must work
          if you want to ride it in the city.
        </p>
        <p>
          Scooter owners must have working brakes, lights and reflectors and
          other required safety equipment. Scooter owners are also required to
          wear helmets while riding on the streets. Anyone operating a motorized
          scooter or moped must be 16 years old before riding on a public street
          or in a park.
        </p>
        <p>
          Scooter owners may not ride on the sidewalks or other areas where cars
          and bikes would not be allowed to operate. This is intended to keep
          both the scooter enthusiast and those around them safe. If a bike lane
          has been designated in your city, you should always ride your
          motorized scooter inside the designated lane.
        </p>
        <p>
          The State of California encourages scooter and moped ownership. For
          this reason, most scooters do not have to be registered with the DMV.{" "}
          <Link to="/california-motor-vehicle-code/motorized-scooter-exemptions-21224">
            Scooter owners are also exempt
          </Link>{" "}
          from many of the financial responsibility laws that apply to car and
          truck owners.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
