// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21058 - Physician Responding to an Emergency"
        pageDescription="California Motor Vehicle Code 21058 pertains to the speed limit law for doctors responding to emergencies."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21058</h1>
        <BreadCrumbs location={location} />
        <h2>Physician Responding to an Emergency</h2>
        <p>
          California Vehicle Code 21058 is intended to protect patients who live
          in rural areas that may go to hospitals that do not have doctors on
          staff at all hours of the day and night. There are numerous situations
          when a doctor may be needed at legitimate emergencies, and getting
          there fast is essential. If there's a shooting victim, a car accident
          patient or a woman with labor complications, at a rural hospital it's
          important to get a doctor to the operating room fast. In rural areas
          the on-call surgeon may live 15 or 20 miles away from the hospital and
          it's important for that surgeon to be able to get there in a hurry
          without worrying about legal repercussions. There are still many rural
          hospitals in California that still rely on the on-call physician for
          emergencies.
        </p>
        <p>
          This law was not designed to give doctors a "get out of jail free"
          excuse. There are still stipulations as to when the law applies.
          First, the doctor must be going to a legitimate emergency. Having
          dinner reservations, or being late to the airport will not constitute
          an emergency. Second, the doctor must maintain a reasonably safe
          speed. There is no excuse for driving recklessly, and doctors who
          cause harm to another person while speeding to an emergency will still
          be held accountable for their actions. Lastly, the doctors cannot
          allow someone else to borrow their car for the purposes of using the
          CMA sticker. This law only applies to a vehicle that is registered to
          a licensed doctor; if that vehicle is being used by the doctor; while
          the doctor is on his or her way to a real emergency.
        </p>
        <p>
          This law does not prevent an officer from pulling over a doctor. The
          doctor must show that he or she is on their way to an emergency. While
          most officers will probably not question a licensed physician, an
          officer that questions the authenticity of the emergency can still
          issue a citation. The physician would not be held accountable, and
          would not have to pay the ticket upon showing the court proof of the
          emergency. However, the doctor will have lost valuable time getting to
          the emergency while the officer is writing the ticket.
        </p>
        <p>
          There has been some debate between the California Medical Association
          (CMA) and the California Highway Patrol (CHP) over the necessity of
          this law. The CHP argues that the law, which was passed in 1954, is
          outdated and no longer necessary. The CMA argues that in rural areas
          the law is still needed and could save lives. For now, the law is
          still on the books and the CMA regularly gives stickers to licensed
          physicians to use in case of emergencies.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
