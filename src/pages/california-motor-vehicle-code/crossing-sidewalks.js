// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21952 - Crossing Sidewalks"
        pageDescription="Pedestrians have the right of way when cars are pulling out of driveways. California Motor Vehicle Code 21952 explains."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21952</h1>
        <BreadCrumbs location={location} />
        <h2>Crossing Sidewalks</h2>
        <p>
          California vehicle code 21952 explains that any driver of a motor
          vehicle who intends to drive over a sidewalk must yield to the
          right-of-way of any nearby pedestrian.
        </p>
        <p>
          For example, if a driver wants to exit his driveway, he must wait for
          the sidewalk to be clear of any pedestrians who may be crossing behind
          him.
        </p>
        <p>
          This also applies to vehicles exiting parking lots or any other
          instance when a driver must cross a sidewalk path.
        </p>
        <p>
          The best way to be prepared for approaching pedestrians is to make a
          complete stop before crossing or backing up over a side walk. Use
          rear-view and side mirrors to check spaces around the vehicle that are
          not easy to see. Check to the left, then right, then left again to be
          sure that both the sidewalk and the closest lane are clear of
          pedestrians, cars, or bicycles. Remember that pedestrians on sidewalks
          do not only approach from the left side as cars do. If parked cars or
          other objects obstruct a driver's vision of the sidewalk or road,
          approach very slowly so a quick stop can be made if necessary.
        </p>
        <p>
          It is important for drivers to pay attentions to the actions of
          pedestrians, even if pedestrians are walking out of turn or in the
          street. The reason for this is that pedestrians are highway users the
          most at risk in traffic. Drivers in vehicles are protected by the
          vehicle itself, but pedestrians are vulnerable and with out any
          protection. Pedestrians are even vulnerable to the momentum of a
          bicycle collision, and can be badly injured. For these reasons, the
          law requires drivers and cyclists alike to exercise great care to
          avoid striking pedestrians.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
