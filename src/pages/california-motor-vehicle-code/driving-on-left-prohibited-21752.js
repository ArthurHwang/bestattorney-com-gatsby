// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21752 - Driving on Left Prohibited"
        pageDescription="In the state of California driving on the left side of the road is illegal. See Motor Vehicle Code 21752."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21752</h1>
        <BreadCrumbs location={location} />
        <h2>Driving on Left Prohibited</h2>
        <p>
          California Vehicle Code 21752 makes it illegal to drive on the left
          side of the road under certain circumstances. Don't assume this means
          you can drive on the left side of the road at other times. Driving on
          the left side of the road is only legal is you are passing other cars,
          or if you are avoiding road hazards.
        </p>
        <p>
          Other vehicle codes address driving on the left to pass cars when
          driving in the mountains. This law is for rural roads that are
          infrequently traveled. You can never drive on the left side of the
          road if you can't see at least 100 feet in front of you. This includes
          going around curves or over the crest of a hill. If you can't see, you
          have no idea if there is anything blocking the left side of the road
          ahead.
        </p>
        <p>
          According to the U.S. Federal Highway Administration, the majority of
          roadway fatalities occur on rural roads. Rural roads account for only
          around 40 percent of vehicle travel but lodge nearly 57 percent of all
          fatalities. Approximately 15,000 people die each year on rural roads.
          Play it safe and stay to the right when you can't see what's coming.
        </p>
        <p>
          You never want to cross over the middle of the road, even a small
          distance, if you can't see what's ahead of you. This is especially
          true near bridges and railroad crossings, where getting into an
          accident could be deadly. If you drive left of center you can be
          ticketed. The fines will cost between $150 and $300 for your first
          offense. Fines vary according to the dangerousness of the situation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
