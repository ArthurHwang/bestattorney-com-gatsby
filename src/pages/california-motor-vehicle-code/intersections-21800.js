// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21800 - Intersections - DMV - BISNAR CHASE Law Firm"
        pageDescription="Do you know the proper way to yield when approaching an intersection? Learn here. California Motor Vehicle Code 21800."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21800</h1>
        <BreadCrumbs location={location} />
        <h2>Intersections</h2>
        <p>
          California Vehicle Code 21800 lets drivers know the proper way to
          yield when approaching various intersections. Many drivers are
          confused as to who has the right-of-way at intersections. This law is
          designed to eliminate that confusion.
        </p>
        <p>
          First, if you are on a highway and approaching an on-ramp, you should
          yield the right-of-way to cars entering from a different highway. This
          does not mean to stop or pull over. Just do your best to speed up or
          slow down to allow enough room for the approaching car to enter the
          highway safely.
        </p>
        <p>
          When approaching a four way stop where there are two roads
          intersecting, the person on the right shall have the right of way.
          Only yield to the person on the right if you have both approached the
          stop at the same time. If you get to the intersection at different
          times, the car arriving first gets to go first, regardless of their
          location.
        </p>
        <p>
          If you come to a T-intersection, the person on the straight thru
          street goes first. The car that is on the ending street must yield to
          drivers on the continuous street. This applies to three-way stops as
          well.
        </p>
        <p>
          If you approach in intersection with a non-working traffic light, you
          must treat the intersection as though it were a four-way stop.
          Everyone approaching an intersection where the traffic light is not
          working, must stop and yield the right-of-way to the car on your
          immediate right. Many drivers ignore this rule, and it ends in an
          accident. Make sure to stop any time you see a light that has stopped
          working.
        </p>
        <p>This law does not apply to any of the following situations:</p>
        <ol>
          <li>An intersection with a working traffic signal</li>
          <li>An intersection with a Yield Right-of-Way sign</li>
          <li>
            When vehicles approach an intersection from opposite directions and
            one driver is intending to turn left.
          </li>
        </ol>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
