// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21460 - Laws Regarding Double Lines"
        pageDescription="Learn California laws regarding double lines listed in Motor Vehicle Code 21460."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laws Regarding Double Lines</h1>
        <BreadCrumbs location={location} />
        <p>
          Painted lines on the roadway indicate where it is appropriate for
          vehicles to be driven. There are many lines, varying in color,
          thickness, and number, all which divide the road into legal and
          illegal spaces. Knowing what each line means is necessary for driving
          lawfully and safely.
        </p>
        <p>
          Double lines are two parallel lines that will appear on the left of a
          driver and between
          <img
            src="/images/car-accidents/double-yellow-lines.jpg"
            className="imgright-fluid"
            alt="double yellow lines"
          />
          the opposing lanes of traffic. If the lines are solid, no person
          driving shall cross over or drive to the left of the lines.
          Occasionally, these double parallel lines are indicated by raised
          pavement markers simulating painted lines. These raised double lines
          indicate the same as regular double lines, but are more obvious to a
          car that passes over them due to the change in texture on the road.
        </p>
        <p>
          If a driver sees that the double lines have one solid line and one
          broken line and the broken line is closest to this driver's left, it
          means that the driver is allowed to cross over the double lines
          briefly to overtake and pass a vehicle in front of him.
        </p>
        <p>
          There are a few instances where drivers are allowed to cross over
          solid double lines, even in the driver does not have a broken line on
          his or her side. When making a legal left turn at an intersection,
          turning into a driveway or private road, or making a legal U-turn, a
          driver may cross over the double lines.
        </p>
        <p>
          If at any time other authorized signs are posted designating the flow
          of traffic differently than otherwise shown by the double lines, the
          signs designate the current laws. Paying attention to painted lines
          and any signs indicating changes to the roadway is integral to keeping
          drivers on the same page while driving.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
