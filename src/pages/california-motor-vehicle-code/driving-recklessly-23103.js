// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23103 - Driving Recklessly"
        pageDescription="Vehicle code 23103 - this is the reckless driving code and covers a variety of actions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23103</h1>
        <BreadCrumbs location={location} />
        <h2>Driving Recklessly</h2>
        <p>
          California Vehicle Code 23103 describes reckless driving as "wanton
          disregard for the safety of persons or property" while driving a
          vehicle upon a public road, highway, or offstreet parking facility.
        </p>
        <p>
          Reckless driving may be observed as a variety of different actions
          taken by a driver and a combined lack of care of the driver which
          inhibits his ability to practice safe driving in accordance with the
          law. For instance, if a driver decides to go through a drive-thru and
          eat a hamburger while driving, doing so may require the driver to
          remove one hand from the wheel to hold the burger. Focusing on eating
          the burger, he may have less attention on the road and could begin to
          swerve. He then may be unable to stabilize the vehicle because he
          cannot grasp the wheel with both hands. If a police officer were
          present to observe the apparent lack of control of this vehicle, he
          may issue an offense.
        </p>
        <p>
          The reckless driving offense is punished by imprisonment in a county
          jail for any amount of time between five and 90 days, or a fine
          between $145 and $1,000 depending on the specific situation. The
          punishment could also combine both imprisonment and a fine.
        </p>
        <p>
          Like the hungry driver mentioned above, many drivers like to
          multi-task while operating a vehicle. Nowadays, people notoriously
          search for and change CDs, talk on cell phones, text with cell phones,
          and do other irresponsible actions while driving. Not all of these
          actions will be punished as reckless driving, but could cause a
          situation where the driver is seen as showing disregard for others on
          the road and creating a hazard. If these actions are observed by an
          officer of the law, a reckless driving,or other offense may be issued.
        </p>
        <p>
          Other reckless driving offenses may be more deliberate on the part of
          the driver. Any combination of exceeding the speed limit, quickly
          changing lanes, tailgating other drivers, driving off the road or in
          opposing traffic, racing and playing dangerous games while driving,
          etc., may cause the driver to incur a reckless driving offense, as
          well as other sorts of offenses.
        </p>
        <p>
          Drivers should remember to consider whether, at all times while
          driving, they feel as though they have complete control of the
          vehicle. If at any moment the answer might be "no", it is the drivers
          responsibility to correct his or her actions or stop driving.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
