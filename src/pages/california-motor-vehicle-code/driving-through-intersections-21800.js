// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21800 - How to Drive Through Intersections"
        pageDescription="Safely navigate through intersections by understanding california motor vehicle code 21800"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21800</h1>
        <BreadCrumbs location={location} />
        <h2>How to Drive Through Intersections</h2>
        <p>
          An intersection can be a dangerous place. Different streams of
          transportation meet and must somehow cross paths with out collision.
          Luckily, vehicle code 21800 details the specific requirements for
          keeping intersections in safe, working order.
        </p>
        <p>
          Basic idea in any intersections is to yield to the right-of-way. This
          is common sense to some extent, that some drivers must wait while
          others proceed. Drivers who have the "right-of-way" are those who are
          already passing through the intersection.
        </p>
        <p>
          If two vehicles approach an intersection at the same time from
          different highways, the driver of the vehicle on the left must yield
          to the vehicle on his immediate right. Further, if a driver is on a
          terminating highway and has to merge, he must yield to any vehicle on
          the continuing highway.
        </p>
        <p>
          Many intersections are made simple with the posting of stop signs or
          traffic control signals. If stop signs are present, all drivers must
          make a complete stop before continuing through the intersection and
          are required to yield to the driver on their immediate right. Traffic
          signals require drivers to follow the indicating lights and only
          proceed with caution when shown a green "GO" signal.
        </p>
        <p>
          If an intersection only has stop signs posted for two parallel
          directional roads, drivers traveling here must stop until the cross
          highway is clear before proceeding through the intersection.
        </p>
        <p>
          Drivers should always check for vehicles coming from crossing highways
          when passing through intersections, even when traffic control signals
          and stop signs are present. Doing so will keep drivers alert to any
          other vehicles who may be unaware of their need to yield, and can help
          prevent collisions.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
