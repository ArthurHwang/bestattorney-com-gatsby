// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21662 - Mountain Driving"
        pageDescription="You must sound your horn when driving around mountains, within 200 feet of underpass or curve. This is California Motor Vehicle Code 21662 "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21662</h1>
        <BreadCrumbs location={location} />
        <h2>Mountain Driving</h2>
        <p>
          California vehicle code 21662 requires you to stay in control of your
          vehicle at all times. You must drive as far to the right side of the
          road as is safe to do so, and you must use your horn to signal your
          presence if you are going around a curb or other obstruction where
          oncoming vehicles may not be able to see you.
        </p>
        <p>
          If you are driving in the mountains and your view is obstructed by
          trees, rocks or
          <img
            src="/images/car-accidents/mountain-road.jpg"
            className="imgright-fixed"
            alt="mountain road"
          />{" "}
          curves you must sound your horn within 200 feet of a curve or
          underpass to alert other drivers to your presence. Oncoming cars
          should be able to hear you even if they can't see you. Sounding your
          horn gives other drivers a chance to move to the far right of the road
          and avoid a possible collision.
        </p>
        <p>
          You should also drive slow enough to stay on your own side of the road
          and maintain proper control of your vehicle. Mountain driving can be
          fun and exhilarating, but it must also be safe for all drivers. If you
          can't see around the curve ahead of you, drivers coming in the
          opposite direction cannot see either. If you are driving fast, neither
          driver may have sufficient time to get out of the other's way.
        </p>
        <p>
          Always sound your horn at 200 feet before a blind spot. If you are
          unsure what distance is 200 feet, you can think of it as approximately
          the distance of 10-12 cars placed end to end. You can also judge the
          distance as how far you can travel in 4 seconds if you are traveling
          35 mph. This is the distance needed to ensure you are close enough to
          the curve to be heard, but far enough away to allow time for the
          oncoming cars to move over.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
