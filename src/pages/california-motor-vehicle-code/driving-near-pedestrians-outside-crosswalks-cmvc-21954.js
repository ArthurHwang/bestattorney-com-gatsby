// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Vehicle Code 21954--Driving Alongside Pedestrians"
        pageDescription="California Motor Vehicle Code 21954 is about driving near pedestrians outside crosswalks."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21954</h1>
        <BreadCrumbs location={location} />
        <h2>Driving Near Pedestrians Outside Crosswalks</h2>
        <p>
          California vehicle code 21954 explains the the way pedestrians and
          vehicles must coexist on the road. Pedestrians have the right of way
          only when traveling within a marked crosswalk or within an unmarked
          crosswalk at an intersection. All other instances require pedestrians
          to yield the right-of-way to vehicles on the roadway. Doing otherwise
          could create an immediate hazard that might result in serious personal
          injury.
        </p>
        <p>
          California, however, instills in its laws the necessary due care
          toward pedestrians. This means that even though pedestrians may not
          always have the right of way, vehicles must use utmost caution around
          pedestrians, and avoid hazards even if they arise because of
          pedestrians traveling unlawfully.
        </p>
        <p>
          Pedestrians may jay walk and cross a road outside of a legally drawn
          crosswalk. Jaywalking is penalized in certain areas and can result in
          a fine for the pedestrian doing so, but this may not be the biggest
          problem jaywalking involves. If a driver were to refuse to yield to a
          jaywalking pedestrian in order to uphold the driver's right-of-way,
          collision with the pedestrian could end in fatality. California law
          does not relieve the driver of a vehicle from the duty to exercise due
          care for the safety of pedestrians.
        </p>
        <p>
          It is in driver's best interest to always be alert as possible on the
          road and to pay special attention to those who are traveling outside
          of a vehicle. Pedestrians, bicyclists, motorcyclists, and other low
          speed vehicles are vulnerable to the force and weight of other faster
          vehicles on the road, and often will bare the serious injury in cases
          of collision.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
