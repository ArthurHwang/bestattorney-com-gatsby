// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21656 - Slow Moving Vehicle Turnouts"
        pageDescription="Slow moving vehicles are required to use designated turnouts to move out of the way of faster moving vehicles as stated in California Motor Vehicle Code 21656 "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21656</h1>
        <BreadCrumbs location={location} />
        <h2>Slow Moving Vehicle Turnouts</h2>
        <p>
          From time to time, we all encounter slow moving vehicles on rural
          roads. These vehicles are allowed on the roads, but according to
          California Vehicle Code 21656, the drivers must maintain some common
          courtesy and allow faster moving cars and trucks to pass.
        </p>
        <p>
          If you are driving a farm tractor, buggy, bike or other slower moving
          vehicle on a small two-lane road and notice a long line of cars
          forming behind you, you are required to pull off the road and allow
          cars to pass you. This is especially true if you are in an area where
          it is unsafe for cars to pass you on the left side of the road.
        </p>
        <p>
          Only pull off the road in designated turnouts or where it is otherwise
          safe to do so. Designated turnouts are special sections of road
          designed to give slow moving cars a place to get off the roadway and
          allow others to pass. Usually these turnouts are indicated by special
          signs that read "Slower Traffic Use Turnouts."
        </p>
        <p>
          If there are five or more cars behind you, then you must pull off the
          road and allow them to pass. This makes driving safe for the drivers
          behind you who are anxious to pass and makes you a more polite and
          courteous driver.
        </p>
        <p>
          This law is designed to ease traffic congestion in rural areas and
          keep the roads safe for everyone using them. It also helps to
          eliminate road rage by drivers who may be stuck on long windy roads
          behind drivers who may travel slower than the normal flow of traffic.
        </p>
        <p>
          A slow moving vehicle is any car, bus, farm equipment, bicycle or
          other vehicle that is operating slower than the normal flow of
          traffic. There is no set speed to determine if you are a slow moving
          vehicle. If you are traveling at a speed slower than the rest of
          traffic on the roadway, and if you notice a line of cars behind you
          who look anxious to pass, then you are a slow moving vehicle and you
          must get out of the way as soon as it is safe to do so.
        </p>
        <p>
          If you are riding behind a slow moving vehicle, be sure to take extra
          safety precautions when passing. Only pass one vehicle at a time, give
          a small toot of your horn to announce your presence, and get back into
          the driving lane as soon as it is safe to do so.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
