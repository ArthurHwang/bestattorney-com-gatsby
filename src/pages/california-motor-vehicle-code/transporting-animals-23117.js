// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23117 - Transporting Animals"
        pageDescription="Read California Motor Vehicle Code 23117 to get information on law regarding transporting animals."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23117</h1>
        <BreadCrumbs location={location} />
        <h2>Transporting Animals in a Motor Truck</h2>
        <p>
          When it comes to transporting animals, the laws in place serve to keep
          them just as safe as humans. Vehicle code 23117 addresses carrying
          animals in the back of a motor truck.
        </p>
        <p>
          On a highway, it is unlawful to transport any animal in the back of a
          truck intended for loading objects if the animal is left unrestrained
          in the space. If a driver wishes to transport his dog, for example,
          the height of the side and tail racks on the truck must be at least 46
          inches and the dog must be cross tethered to the vehicle. It is a good
          idea to put the dog or other animal in a carrier cage and secure the
          cage to the back of the bed. Doing this will help keep your animal
          safely in the truck and prevent its being ejected in the case of a
          quick stop or collision.
        </p>
        <p>
          Many people previously allowed their dogs to roam freely in the back
          of the open truck bed, but concerns about animal cruelty helped enact
          this vehicle code. Drivers who break this law may incur fines anywhere
          from $50 to $200. As of 2009, California is one of 8 states with
          strict laws regarding the transportation of animals.
        </p>
        <p>
          Other legislation is trying to be passed which might require drivers
          to restrain their animals while inside the vehicle as well as in the
          truck bed. Those in favor of this addition to the law believe that
          loose dogs in vehicles, including lap-dogs on driver's laps, can
          distract drivers and cause accidents. While it is not passed yet,
          drivers should consider whether having a loose animal in the car
          affects their driving negatively.
        </p>
        <p>
          Vehicle Code 23117 does not apply to the transportation of livestock
          or apply to animals transported in trucks on rural, ranching roads or
          farms, though it is always a good idea to transport animals as safely
          as possible.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
