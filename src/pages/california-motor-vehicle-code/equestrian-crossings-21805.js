// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21805 - Equestrian Crossings"
        pageDescription="California Motor Vehicle Code 21805 is a law pertaining to equestrian crossings. Cars must yield to horses and horseback riders."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21805</h1>
        <BreadCrumbs location={location} />
        <h2>Equestrian Crossings</h2>
        <p>
          California Vehicle Code 21805 makes it mandatory for cars to yield to
          horses and riders crossing in designated horse crossing zones. There
          will be signs posted at designated crossings letting cars know to
          watch for horses.
        </p>
        <p>
          Any intersection may be used as an authorized bridle crossing. It is
          up to local authorities to determine official horse crossing areas. If
          there are additional steps drivers must take to ensure safe crossing,
          the signs should indicate such.
        </p>
        <p>
          If there is a sign, and you see a horseback rider approaching the
          intersection, you must yield the right-of-way to the rider. If there
          is no sign, or the sign is missing, you must still yield to the rider
          under California law. Horses can spook easily, and it's up to drivers
          to make sure the horses are not afraid when you pass them. If the
          rider indicates to you that the horse is having a problem, slow down
          or stop until the horse has safely passed you.
        </p>
        <p>
          This law does not relieve horseback riders from liability. If you are
          a rider, you must still use due care to ensure your own safety and the
          safety of your horse. Do not dart out in front of approaching traffic.
          Make sure the driver sees you and is aware of your crossing before
          safely proceeding into the intersection.
        </p>
        <p>
          There are basic safety precautions horseback riders can take when
          sharing the roads with cars.
        </p>
        <ul>
          <li>
            When approaching a bike or car, slow down. If you have to, dismount
            and secure the horse quietly with your riding aids. If the horse is
            frightened, circle back and calm the animal if possible.
          </li>
          <li>
            Wear a helmet and carry a cell phone. The best way to keep yourself
            and your animal safe is to avoid situations that might scare the
            horse. In case of injury, keep your phone nearby to call for help.
          </li>
          <li>
            Know local schedules. If you know of a bike event or race being held
            in your area, it's best to stay home and not risk injury to yourself
            or your horse.
          </li>
          <li>
            Know your animal. If your horse spooks easily, try to ride him or
            her on trails that will not cross busy intersections.
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
