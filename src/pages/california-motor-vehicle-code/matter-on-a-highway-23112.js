// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23112 - Matter on a Highway"
        pageDescription="Dumping matter on a highway is illegal by California Motor Vehicle Code 23112."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23112</h1>
        <BreadCrumbs location={location} />
        <h2>No Throwing, Depositing, or Dumping Matter on a Highway</h2>
        <p>
          There are a variety of reasons why it is illegal to toss things on the
          highway. For one, it is considered littering and contributes to
          pollution. Secondly, if a driver throws something on the road, who
          does he or she expect will pick it up? Thirdly, it is a hazard and
          could cause a collision.
        </p>
        <p>
          Vehicle code 23112 explains that no one - driver, passenger or
          pedestrian - shall throw or deposit anything on the highway. This
          includes but is not limited to: bottles, cans, garbage, nails, offal,
          paper, wire, or any offensive material of any kind. Even dirt and
          rocks can be a hazard if placed on the road without consent of the
          state or local agency.
        </p>
        <p>
          If a driver drinks a cup of soda and tosses it out the window, it
          could hit another driver's windshield and make it very hard for him to
          see and continue driving safely. If someone deposits nails or sharp
          objects on the road, another driver could experience a blow out and
          lose control of his vehicle. If someone loses or places a piece of
          furniture in the road, it could cause a traffic jam as other vehicles
          have to slowly switch lanes to avoid the obstacle.
        </p>
        <p>
          Drivers should secure items being transported so they are not lost
          during transit and become road debris.
        </p>
        <p>
          It is the responsibility of all who use the road to be considerate and
          keep it clean.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
