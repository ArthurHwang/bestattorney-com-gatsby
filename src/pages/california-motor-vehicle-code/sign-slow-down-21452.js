// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21452 - The Sign to Slow Down"
        pageDescription="California Motor Vehicle Code 21452 explains the purpose of a yellow traffic signal and law surrounding it."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The Sign to Slow Down</h1>
        <BreadCrumbs location={location} />
        <p>
          According to California Vehicle Code 21452, yellow lights warn that
          the vehicles facing the signal will soon be shown a red indicator.
          Whether a circular yellow light, or a left arrow yellow light,
          vehicles must heed the warning of the yellow light as a transition
          from a green light to a red one.
        </p>
        <p>
          Some drivers interpret yellow lights as a chance to speed up and
          "make" the light
          <img
            src="/images/car-accidents/yellow-traffic-signal.jpg"
            className="imgright-fixed"
            alt="yellow traffic light"
          />
          before it turns red and passing through the intersection is illegal.
          Though many do this, it is not always the safest choice. A vehicle who
          enters the intersection during a yellow light is allowed to pass
          through with out penalty, but attempting to catch the yellow light and
          passing into the intersection when the light has just turned red, is
          not allowed. This is why slowing down for a yellow light, and waiting
          for the red, is often the safest approach to reading a yellow
          indicator.
        </p>
        <p>
          There are times, however, when continuing through a yellow light is
          preferred. If a car is traveling with a momentum which makes slowing
          and breaking for the yellow light difficult, or if the vehicles is in
          the intersection when the yellow light is indicated, then stopping for
          the yellow light would be a bad choice. Maintaining the appropriate
          speed limit is always important when entering an intersection, whether
          the light is green or yellow.
        </p>
        <p>
          Pedestrians also must heed the yellow light indicator. If a person
          traveling on foot is a facing the circular or left arrow yellow light,
          the light is showing that there is not enough time for the pedestrian
          to cross the road. Attempting to begin crossing during the yellow
          light is illegal, unless a pedestrian control signal shows otherwise.
        </p>
        <p>
          Drivers must use their best judgment about whether or not to enter an
          intersection during a yellow light, as doing so is important in
          preventing accidents and injuries.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
