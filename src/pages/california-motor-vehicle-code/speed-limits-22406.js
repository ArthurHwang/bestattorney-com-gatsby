// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22406 - Speed Limits for Specific Vehicles"
        pageDescription="California Motor Vehicle Code 22406 pertains to speed limits for specific vehicles."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22406</h1>
        <BreadCrumbs location={location} />
        <h2>Speed Limits for Specific Vehicles</h2>
        <p>
          While most vehicles must abide by posted speed limits and speed limits
          designated to certain zones, other vehicles are also subject to other
          maximum speed limit laws.
        </p>
        <p>
          California Vehicle Code 22406 states the max speed limit as 55 miles
          per hour for the following designated vehicles:
        </p>
        <ul>
          <li>
            Motor trucks or tractors which are composed of three or more axles.
            This includes any motor truck or truck tractor that is drawing
            another vehicle.
          </li>
          <li>Passenger vehicles or buses drawing other vehicles.</li>
          <li>Schoolbuses carrying students.</li>
          <li>Farm labor vehicles transporting passengers.</li>
          <li>Vehicles legally transporting explosives.</li>
          <li>Any trailer bus.</li>
        </ul>
        <p>
          The 55 miles per hour max speed limit is applied to these large, heavy
          vehicles for safety reasons. The law also considers the roads and
          terrain, such as farmland, that many of these vehicles travel on.
          Abiding by max speed limit laws is essential to keep drivers in
          various vehicles safe on the road.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
