// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21050 - Riding an Animal on a Public Highway"
        pageDescription="California Motor Vehicle Code 21050 applies to any driver or rider of animals on public roadways."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21050</h1>
        <BreadCrumbs location={location} />
        <h2>Riding an Animal on a Public Highway</h2>
        <p>
          This statute most often applies to equestrian riders and drivers of
          horse-drawn vehicles, although it may also pertain to llamas, goats,
          cattle or any other animal that can be ridden on the streets. This
          code is written for "highway" riding, but the law applies to any and
          all public roadways.
        </p>
        <p>
          The California Vehicle Code 360 defines a "highway" as any roadway
          that is "publicly maintained and open to the public for the purposes
          of vehicular travel." Many people think of highways as interstates and
          major roadways, but for the purposes of this code section a highway is
          any paved or non-paved public right-of-way. The law pertains to all of
          the public right of way from fence to fence, including ditches and
          grassy area, not just the paved portions on which motorized vehicles
          travel.
        </p>
        <h2>Following the Rules of the Road</h2>
        <p>
          While riding your animal on the highway, you are subject to the same
          rules as drivers in automobiles. You must behave in a way that is safe
          for you, your animal and those sharing the roadway with you. This
          means you must signal all turns and stops, ride with the flow of
          traffic (in the same direction) and obey stop signs and traffic
          signals. Failure to do so could result in a traffic ticket for you.
        </p>
        <p>
          You are also required to ride your animal at a reasonably safe speed
          and not ride in a reckless manner. For example, if the speed limit on
          the roadway is 55 miles per hour, you cannot ride your horse in a
          travel lane at a pace of only 25 miles per hour. The same applies in
          reverse, whereas you cannot gallop your horse at speeds of 35 miles
          per hour on a roadway where the speed limit is only 20 miles per hour.
          Remember the old motto of "safety first" when riding on the roadway
          and you should have no worries.
        </p>
        <p>
          An easily spooked animal could be considered reckless if ridden on a
          public street. You must maintain control of your animal at all times.
          If something spooks your horse, and it darts into traffic, you will
          most likely be held responsible for any damage caused by the animal.
          Anyone riding an animal on the highway can be charged with reckless
          operations just as they would if they were operating an automobile.
        </p>
        <h2>Safety and Common Sense are Requirements</h2>
        <p>
          Keep in mind that your animal is treated the same as a vehicle under
          California traffic laws. In addition to signaling and operating in a
          reasonable manner, you will also have to illuminate the animal after
          dark. This is to keep everyone involved safe during hours when
          visibility is poor. Just as it would be illegal to drive after dark
          without lights, it is also illegal to ride an animal on the streets
          after dark without some form of lighting. This is for the safety of
          the rider, the animal, and drivers of other vehicles sharing the road
          with the riders. If your horse cannot be seen, then you are just
          inviting a possible tragedy.
        </p>
        <p>
          Drinking while riding your horse can also get you into trouble. If you
          ride your horse in an impaired state, it poses risks to everyone
          around you. It is possible to be charged with a DUI while riding your
          horse on a public street. The laws are meant to keep people safe, and
          if you are riding your horse in an intoxicated state, then you are not
          being safe and will receive some form of punishment. At the very
          least, you can be charged with public intoxication.
        </p>
        <p>
          Riding your animal on the highway can be done safely and legally. All
          you need to do is remember that your horse is treated the same as a
          car when it is on a public street. If you obey all the traffic laws
          just as you would in a motorized vehicle, then you should have no
          worries when riding with your best four-legged friend.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
