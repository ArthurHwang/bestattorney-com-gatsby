// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21802 - Laws Of Stop Signs"
        pageDescription="DMV Code 21802 covers stop sign laws including California roll."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21802</h1>
        <BreadCrumbs location={location} />
        <h2>The Laws of Stop Signs</h2>
        <p>
          Stop signs are common to encounter on the road, particularly at
          intersections or where opposing streams of traffic cross.
        </p>
        <p>
          If approaching a stop sign, the driver should begin to slow his
          vehicle and continue to make a complete stop at the intersection. The
          driver then must yield to any vehicles which have the right-of-way.
          These may be vehicles that have already entered the intersection or
          vehicles that are not required to stop. After all vehicles with the
          right-of-way are a safe distance ahead, and no more vehicles are
          approaching, the driver who has stopped may carefully enter and
          proceed through the intersection.
        </p>
        <p>
          The basic guidelines for all stop signs are straight-forward: make a
          complete stop until it is safe to continue. A "complete stop" means
          the tires on the car are no longer rotating, and is sometimes confused
          with the "California roll," which is when a driver slows down at the
          stop sign and then continues without stopping. This is illegal and
          punishable by fine. Further, stopping at the stop sign means taking
          care to stop the vehicle at or before the stop sign and behind any
          marked lines or crosswalks.
        </p>
        <p>
          There are some instances when stop signs and right-of-way situations
          are a bit tricky. An example of such is the 4-way stop, where an
          intersection has stop signs posted for all streams of traffic. In this
          situation the driver must slow his vehicle, stop, and scan all
          directions for cars at other stop signs. If there are other vehicles,
          the rule is that they shall proceed through the intersection in the
          order in which they arrived. If car A stops at the intersection, then
          car B, then car C, car A will pass through first, then B, then C.
        </p>
        <p>
          When there is more than one vehicle arriving at the 4-way stop
          intersection at the same time, the vehicle furthest to the right is
          allowed to proceed first.
        </p>
        <p>
          Drivers must be aware and conscientious while making complete stops at
          stop signs. Following the law can help prevent accidents and injuries.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
