// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23127 - Trails and Paths"
        pageDescription="Certain restrictions apply to trails and paths as California Motor Vehicle Code 23127 states."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23127</h1>
        <BreadCrumbs location={location} />
        <h2>Driving on Trails and Paths</h2>
        <p>
          Certain areas, like hiking or horseback riding trails, are created for
          specific uses. They are subject to strict rules regarding who and what
          are allowed to travel on them. Because the purpose of the trail may be
          for pedestrians to hike, or horseback riders to ride, unauthorized
          motor vehicles are prohibited in these areas.
        </p>
        <p>
          Vehicle Code 23127 addresses these restrictions. The code states that
          no one may drive an unauthorized motor vehicle on state, county, city,
          or private trails, including hiking, horseback riding, or bicycle
          paths, that are marked with signs at entrances and exits that show the
          area to be illegal for motor vehicle operation.
        </p>
        <p>
          The importance of vehicle restrictions in these areas may be to
          preserve nature and scenic routes. Motor vehicles of any kind would
          disrupt the flora and fauna in these areas, and could injure any
          pedestrians that are hiking. Motor vehicles could also spook horses
          that horseback riders are traveling with, or hit bicyclists that do
          not expect to encounter high-speed vehicles on the path. Furthermore,
          some of these routes are unfinished, rocky, and steep, making them
          unsafe for regular motor vehicles to traverse.
        </p>
        <p>
          The only exception to this rule is in the case of an emergency. If an
          accident has occurred, authorized emergency maintenance vehicles are
          permitted to enter these trails to prevent injury or death. Any other
          unauthorized vehicles that operate on trails or paths illegally may be
          found guilty of a misdemeanor.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
