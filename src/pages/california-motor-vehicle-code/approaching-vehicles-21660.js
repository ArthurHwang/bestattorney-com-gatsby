// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21660 - Approaching Vehicles"
        pageDescription="According to DMV Motor Vehicle Code 21660 it is unlawful to pass oncoming vehicles on the left side of the roadway. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21660</h1>
        <BreadCrumbs location={location} />
        <h2>Approaching Vehicles</h2>
        <p>
          California Vehicle Code 21660 states that cars must pass oncoming
          vehicles on the right side of the roadway and allow the other car at
          least half of the road for safe passage. According to the Guiness Book
          of Records Book of Answers, 58 countries drive on the left side of the
          roads, and 163 drive on the right. Tradition seems to play a key
          factor in which side of the roads countries choose for travel.
        </p>
        <p>
          Do not confuse this rule with passing cars that are traveling in the
          same direction as you. When you pass cars traveling in the same
          direction, you should pass them on the left. Meaning, their car will
          be on your passenger side. Passing cars on the right that are
          traveling in the same direction is illegal in almost every state.
        </p>
        <p>
          When you are passing approaching vehicles, you pass to the right of
          the road. This means that the oncoming cars will pass the driver's
          side of your car or truck. This allows you to see them better, since
          drivers in the United States are situated on the left side of their
          cars.
        </p>
        <p>
          You should remember to follow this rule when driving lanes are not
          clearly marked on the roads. You will most likely use this rule when
          traveling on rural roads and traveling in the mountains.
        </p>
        <p>
          If you fail to pass to the right or yield half the roadway to oncoming
          cars, you could receive a traffic ticket and face fines up to $250.
          You could also be liable for civil damages if you cause an accident.
        </p>
        <p>
          Although this law may seem self-explanatory, there are many accidents
          each year because one driver failed to yield enough traveling space to
          the other driver. You must give at least one-half the roadway to the
          other car so they may pass safely. If you fail to do this, and the
          other driver has an accident, you could be at fault for not yielding
          the right-of-way.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
