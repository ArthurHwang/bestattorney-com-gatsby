// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23123.5 - Prohibited Wireless Device Use"
        pageDescription="Texting and driving is now illegal. See California Motor Vehicle Code 23123.5 for details. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23123.5</h1>
        <BreadCrumbs location={location} />
        <h2>Prohibited Use of Electronic Wireless Communications Device</h2>
        <p>
          Vehicle Code 23123.5 works alongside 23123 to further extend the
          prohibitions of cell phone and wireless device usage while driving.
          Code 23123.5 explains that no driver of a motor vehicle is permitted
          to use an electronic wireless communications device to write, send, or
          receive text-based communication. More simply, do not "text" while
          driving.
        </p>
        <p>
          This means that not only are drivers prohibited from talking on
          hand-held cell phones, but they are also not allowed to communicate to
          anyone via text-based conversation either on the cellphone, or through
          a lap top or other device.
        </p>
        <p>
          To clarify this section, typing in a telephone number or pushing a
          button to receive a call is not considered sending a text-based
          message. Typing in a telephone number may be done if using a
          hands-free wireless device.
        </p>
        <p>
          Like its other counterparts, this code has the same fines of $20 for
          the first offense and $50 for each continuing offense, and does not
          apply to emergency service professionals in the event of an emergency.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
