// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Vehicle Code 21751-- Adequate Amount of Space To Pass Cars"
        pageDescription="California Motor Vehicle Code 21751 pertains to the safe distance required when passing another vehicle on a two-lane highway."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21751</h1>
        <BreadCrumbs location={location} />
        <h2>Sufficient Clearance Necessary for Safe Overtaking</h2>
        <p>
          California vehicle code 21751 draws attention to the space that is
          required to safely pass another vehicle on a two-lane highway. If a
          driver plans to overtake the vehicle in front of him by passing into
          the left lane, there must be a sufficient distance ahead where no
          oncoming vehicle is approaching. Trying to pass a vehicle when an
          oncoming vehicle is too close is very dangerous and can cause a
          head-on collision.
        </p>
        <p>
          Other things to consider when passing vehicles is how long it will
          take to clear the vehicle in front of you. More space will be required
          to pass buses or trucks, as they are longer and will require the
          passing car to be in the left lane for longer. For this same reason,
          drivers should avoid passing multiple vehicles at one time. After
          overtaking the single vehicle in front of a driver, he should make his
          way back into the safety of the right lane until it is safe to pass
          the next vehicle.
        </p>
        <p>
          Drivers must also remember that passing is illegal when double yellow
          lines divide the road. These lines are not to be crossed unless to
          pull into a driveway on the left hand side or unless traffic signs are
          posted saying that it is legal.
        </p>
        <p>
          Attempting to use the right hand shoulder of a vehicle to pass is also
          illegal and a dangerous move. The right margin is typically full of
          debris and is normally only used to slow and stop under emergency
          circumstances. Passing requires a certain amount of acceleration, and
          speeding through a debris-filled margin to pass a car could result in
          a flat tire or a collision, especially since the driver in front would
          not expect a pass from that side.
        </p>
        <p>
          Drivers should always pass to the left of a vehicle, whether on a
          two-lane road or multi-lane road.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
