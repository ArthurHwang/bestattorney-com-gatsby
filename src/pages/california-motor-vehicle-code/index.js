// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code Explained - California Car Accident Laws"
        pageDescription="Easy-to-Understand Rules of the Road.  All California Motor Vehicle Codes."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California "Rules of the Road"</h1>
        <BreadCrumbs location={location} />

        <h2>California Motor Vehicle Code - Car Accident Laws</h2>
        <div className="fltrt-wide"></div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>20001 - </strong>
            Stopping at the Scene of an Accident
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/stopping-at-the-scene-20001">
              Motor Vehicle Code 20001
            </Link>
            , if a driver causes or is involved in an accident, he must
            immediately stop at the scene. Driving away from an accident,
            especially one where occupants of other vehicles may be injured, is
            not only irresponsible but against laws discussed in vehicle codes
            20001, 20003, and 20004.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21050 - </strong>
            Riding an Animal
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/riding-animal-on-public-highway-21050">
              Motor Vehicle Code 21050
            </Link>
            , if you ride an animal on a highway, you have all the rights and
            are subject to all of the duties as may apply to the driver of a
            vehicle&hellip;except those provisions which by their very nature
            can have no application.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21058 - </strong>A physician responding to an emergency
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/physician-responding-to-emergency-21058">
              Motor Vehicle Code 21058
            </Link>
            , if a physician's car displays an insignia indicating that the
            vehicle is owned by a licensed physician and is driving to an
            emergency call, the driver is exempt from basic speed law.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21100.3 - </strong>
            Obedience to person controlling traffic
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/obedience-to-person-controlling-traffic-21100">
              Motor Vehicle Code 21100.3
            </Link>
            , a driver must obey the directions of an authorized traffic control
            officer.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21200 - </strong>
            Rights and duties of bicycle riders
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/rights-duties-of-bicycle-riders-21200">
              Motor Vehicle Code 21200
            </Link>
            , if you ride a bicycle on a highway you have all the rights and are
            subject to all of the duties as may apply to the driver of a
            vehicle, including riding under the influence&hellip;except those
            provisions which by their very nature can have no application.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21202 - </strong>
            Operating bicycle on roadway
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/operating-bicycle-on-roadway-21202">
              Motor Vehicle Code 21202
            </Link>
            , when moving at a speed less than normal speed of traffic moving in
            the same direction, bicycle rider must ride as close as possible to
            the right-hand curb or edge of the roadway, except to pass or
            preparing to turn left or to avoid unsafe conditions.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21208 - </strong>
            Bicycle lanes
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/bicycle-lanes-21208">
              Motor Vehicle Code 21208
            </Link>
            , when established by a local authority, bicycles must use the
            bicycle lane and cannot leave that lane unless it is safe to do so.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21221 - </strong>
            Motorized scooters
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/motorized-scooters-21221">
              Motor Vehicle Code 21221
            </Link>
            , driver of motorize scooters have the same rights and are subject
            to the same duties as that of a driver of a vehicle.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21224 - </strong>
            Motorized scooters exempt from some vehicle provisions
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/motorized-scooter-exemptions-21224">
              Motor Vehicle Code 21224
            </Link>
            , the rider of a motorized scooter is exempt from financial
            responsibility laws, registration, and vehicle licensing
            requirements -- not a motor vehicle for that purpose.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21228 - </strong>
            Motorized scooters rules of the road
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/motorized-scooters-bike-lanes-21228">
              Motor Vehicle Code 21228
            </Link>
            , like a bicycle -- must use bicycle lane if one exists, otherwise
            must stay to right-hand curb or edge of the road except where
            unsafe.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21235 - </strong>
            Motorized scooters operation restrictions
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            When operating a{" "}
            <Link to="/california-motor-vehicle-code/vehicle-code-prohibitions-for-operation-motorized-scooters-21235">
              motorized scooter
            </Link>{" "}
            you cannot use on a street with speed limit in excess of 25 mph
            unless there is a bike lane; cannot ride on sidewalk; cannot carry
            passengers.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21251 - </strong>
            Application of provisions applicable to motor vehicles; exemptions
            for low-speed vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/low-speed-vehicles-21251">
              Motor Vehicle Code 21251
            </Link>
            , if you operate a low-speed vehicle you have all the rights and are
            subject to all of the duties as may apply to the driver of a
            vehicle, including driving under the influence&hellip;except those
            provisions which by their very nature can have no application.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21254 - </strong>
            Low-speed vehicles modified to exceed 25 mph
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            A{" "}
            <Link to="/california-motor-vehicle-code/guidelines-driving-modified-altered-low-speed-vehicle-21254">
              Modified or altered low-speed vehicle
            </Link>{" "}
            must meet passenger vehicle federal Motor Vehicle Safety Standards
            instead of relaxed standards.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21260 - </strong>
            Maximum Speed; crossing of intersections for low-speed vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            {" "}
            <Link
              to="/california-motor-vehicle-code/prohibitions-low-speed-vehicles-21260"
              target="_blank"
            >
              Low-speed vehicles
            </Link>{" "}
            cannot be operated on any roadway with a speed limit over 35 mph,
            unless crossing to and from a highway with a speed limit below 35
            mph.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21266 - </strong>
            Local laws about low-speed vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/low-speed-vehicles-local-laws-21266">
              Motor Vehicle Code 21266
            </Link>
            , be aware of and obey all local laws and restrictions for low-speed
            vehicles.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21281.5 - </strong>
            Speeding; safe operation of electric personal assistive mobility
            devices
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/personal-mobility-devices-21281">
              Motor Vehicle Code 21281
            </Link>
            , you must give right of way to all pedestrians when riding on any
            sidewalk, road or path, and operate at a safe speed for the
            conditions.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21451 - </strong>
            Green lights
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Drivers facing a{" "}
            <Link to="/california-motor-vehicle-code/traveling-through-intersections-safely-green-lights-21451">
              green light
            </Link>{" "}
            may enter an intersection to perform legally permitted turns or
            proceed straight through. Green arrows allow protected turns,
            including U turns when not specifically prohibited. Pedestrians
            facing a green light may begin to cross an intersection unless a
            pedestrian control signal shows otherwise.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21452 - </strong>
            Yellow lights
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/sign-slow-down-21452">
              Motor Vehicle Code 21452
            </Link>{" "}
            a yellow light warns drivers that the red light will appear soon,
            prohibiting drivers from entering an intersection. Pedestrians do
            not have sufficient time to cross an intersection if they start
            crossing on a yellow light.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21453 - </strong>
            Steady red lights and arrows
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Drivers facing a solid{" "}
            <Link to="/california-motor-vehicle-code/red-light-indicator-21453">
              red light signal or arrow
            </Link>{" "}
            must stop before entering crosswalks or intersections. Right turns
            are permitted on solid red signals (NOT arrows) after stopping, if
            no sign prohibits it, and if no hazards are present.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21456 - </strong>
            Walk, wait, or don't walk
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/pedestrian-traffic-signal-21456">
              Motor Vehicle Code 21456
            </Link>{" "}
            No pedestrian shall start to cross a road way when facing the Don't
            Walk, Wait, or upraised hand sign.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21457 - </strong>
            Illuminated flashing red or yellow lights used in traffic signal or
            with traffic sign
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Flashing red light is to be treated as a stop sign. Drivers facing a{" "}
            <Link to="/california-motor-vehicle-code/meaning-of-flashing-signals-21457">
              flashing yellow light
            </Link>{" "}
            may proceed through an intersection exercising greater caution.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21460 - </strong>
            Double Lines
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Cross{" "}
            <Link to="/california-motor-vehicle-code/double-lines-21460">
              double parallel lines
            </Link>{" "}
            only if turning left into a private road or driveway, making a legal
            U-turn, or crossing a broken line to overtake another car.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21464 - </strong>
            Interference with traffic devices
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/defacing-traffic-sign-21464">
              Motor Vehicle Code 21464
            </Link>
            , no person shall deface or alter an official traffic sign or
            device. Mobile infrared transmitters and other devices that could
            alter traffic signals are illegal to own, install, or sell.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21650.1 - </strong>
            Bicycles; direction on roadways
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/how-bicycles-should-ride-21650.1">
              California Motor Vehicle Code 21650.1
            </Link>
            , bicycles should always ride in the same direction as traffic.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21651 - </strong>
            Driving on Divided Highways
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/divided-highways-21651">
              California Motor Vehicle Code 21651
            </Link>
            , when roads have multiple lanes and two-way traffic, drivers are
            required to be on the right side of the road.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21654 - </strong>
            Slow moving vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/slow-moving-vehicles-21654">
              Motor Vehicle Code 21654
            </Link>
            , you should stay as close as possible to the right side of the road
            when traveling in the same direction as traffic but at slower
            speeds.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21655 - </strong>
            Lanes for high occupancy vehicles (car pool lanes)
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/high-occupancy-vehicle-lanes-21655">
              Motor Vehicle Code 21655
            </Link>
            , do not enter the car pool lane, not even to pass a vehicle, if
            your car is not transporting the minimum number of passengers
            required by law. Do not cross double yellow lines to enter the car
            pool lane.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21656 - </strong>
            Turning out of slow moving vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/slow-moving-vehicle-turnouts-21656">
              Motor Vehicle Code 21656
            </Link>
            , on a two lane road, where passing on the left is unsafe, a vehicle
            with five other vehicles behind it in a line must turn out at a
            designated turnout and let them all pass.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21658 - </strong>
            Laned roadways
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            When driving within{" "}
            <Link to="/california-motor-vehicle-code/operating-within-laned-roadways-21658">
              laned roadways
            </Link>{" "}
            drive completely in one lane when there are multiple lanes in the
            same direction, switching lanes only when it is safe to do so. Obey
            signs that reserve lanes for specific purposes.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21660 - </strong>
            Approaching vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/approaching-vehicles-21660">
              Motor Vehicle Code 21660
            </Link>
            , pass each other on the right, and give each other at least half of
            the road whenever possible.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21661 - </strong>
            Narrow roadways
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/narrow-roadways-21661">
              Motor Vehicle Code 21661
            </Link>
            , the person driving downhill gives the right of way to the
            ascending car, and will turn off the road to make room for safe
            passing if necessary.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21662 - </strong>
            Mountain driving
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/mountain-driving-21662">
              Motor Vehicle Code 21662
            </Link>
            , stay in control of your vehicle at all times. Drive as far to the
            right on the road as possible. Use your horn to warn other drivers
            you are approaching within 200 feet of a curve on a narrow road with
            an obstructed view.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21663 - </strong>
            Driving on sidewalk
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/driving-on-sidewalk-21663">
              Motor Vehicle Code 21663
            </Link>
            , do not drive on sidewalks unless unless it is expressly allowed or
            when entering or exiting adjacent property.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21700 - </strong>
            Obstruction to driving
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/obstruction-to-driving-21700">
              Motor Vehicle Code 21700
            </Link>
            , do not drive if your vehicle is so loaded that it interferes with
            your vision and control of the vehicle.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21701 - </strong>
            Interference with driver or mechanism
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/distracted-driving-21701">
              Motor Vehicle Code 21701
            </Link>
            , do not interfere with the driver of a vehicle or the mechanism
            thereof in a way that affects the driver's control.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21702 - </strong>
            Limitation on driving hours
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/limitation-on-driving-hours-21702">
              Motor Vehicle Code 21702
            </Link>
            , no person may drive a bus or other vehicle for transporting people
            for compensation more than 10 hours in a 24-hour period unless they
            have had 8 consecutive hours of rest. No commercial driver may drive
            12 consecutive hours or 12 hours spread over 15 hours.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21703 - </strong>
            Following too closely
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/following-too-closely-21703">
              Motor Vehicle Code 21703
            </Link>
            , leave a safe distance between your vehicle and the one in front of
            you considering the speeds, road conditions, traffic, and general
            regard for safety.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21706 - </strong>
            Following emergency vehicle
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/how-drive-behind-emergency-vehicles-21706">
              Motor Vehicle Code 21706
            </Link>
            , vehicles should not follow within 300 feet of an emergency
            vehicle.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21710 - </strong>
            Coasting prohibited
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/coasting-downhill-21710">
              Motor Vehicle Code 21710
            </Link>
            , traveling downhill in neutral is illegal.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21712 - </strong>
            Unlawful riding and towing
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/unlawful-riding-towing-21712">
              Motor Vehicle Code 21712
            </Link>
            , no passenger may sit in any part of a vehicle that is not intended
            for passengers, nor shall anybody tow a motorcycle, trailer or coach
            containing a passenger.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21714 - </strong>
            Motorcyclists or motorized bicyclists; operation of vehicle;
            prohibited area
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/prohibited-areas-for-vehicles-21714">
              Motor Vehicle Code 21714
            </Link>
            , it is illegal to ride on, or immediately adjacent to, markers
            designating lanes or between two or more vehicles traveling in
            adjacent lanes.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21716 - </strong>
            Golf carts, operation
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/golf-carts-21716">
              Motor Vehicle Code 21716
            </Link>
            , golf carts are only to be operated where the speed limit is 25 mph
            or less.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21718 - </strong>
            Parking or leaving a vehicle on a freeway
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            {" "}
            <Link to="/california-motor-vehicle-code/when-drivers-can-stop-freeway">
              Drivers may stop on the freeway
            </Link>{" "}
            to avoid injury, told to do so by an officer, when engaging in
            maintenance, or where specifically permitted.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21750 - </strong>
            Overtake and pass to left
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            If it is safe to{" "}
            <Link to="/california-motor-vehicle-code/overtaking-passing-slower-moving-vehicle">
              overtake
            </Link>{" "}
            a vehicle or bicycle in front of you traveling in the same
            direction, do so on the left and allow them enough space to operate
            normally.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21751 - </strong>
            Passing without sufficient clearance
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Pass only when you can clearly see it is safe to do so with{" "}
            <Link to="/california-motor-vehicle-code/sufficient-clearance-necessary-for-safe-overtaking">
              sufficient clearance necessary for save overtaking
            </Link>
            . This includes instances on 2 lane roads.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21752 - </strong>
            When driving on left prohibited
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/driving-on-left-prohibited-21752">
              Motor Vehicle Code 21752
            </Link>
            , no driving is allowed on the left side of the roadway when
            approaching a curve or crest where the other driver's view is
            obstructed, or within 100 feet of a bridge tunnel, railroad grade,
            or intersection.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21753 - </strong>
            Yielding when passing
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/yield-to-passing-cars-21753">
              Motor Vehicle Code 21753
            </Link>
            , do not speed up when being passed -- slow and allow safe passing.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21756 - </strong>
            Passing standing streetcar, trolley coach, or bus
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/passing-stopped-bus-21756">
              Motor Vehicle Code 21756
            </Link>
            , you must stop behind a vehicle discharging passengers, unless
            there is a specially designed safety zone.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21759 - </strong>
            Caution in passing animals
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/passing-animals-21759">
              Motor Vehicle Code 21759
            </Link>
            , you should slow down and obey any signals by any person in charge
            of the animals to ensure safe passing.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21800 - </strong>
            Intersections without functioning traffic control signs, or with
            all-directional stop signs
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/driving-through-intersections-21800">
              Motor Vehicle Code 21800
            </Link>
            , when entering an intersection at the same time, yield right of way
            to the driver on your right. In case of a broken traffic control
            device, stop at an intersection and proceed when safe to do so.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21801 - </strong>
            Left or U turn right of way
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/u-turns-22100">
              Motor Vehicle Code 22100
            </Link>
            , Unless you have a protective green arrow, always yield right of
            way to oncoming traffic.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21802 - </strong>
            The Laws of Stop Signs
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/stop-signs-21802">
              Motor Vehicle Code 21802
            </Link>
            , if approaching a stop sign, the driver should begin to slow his
            vehicle and continue to make a complete stop at the intersection.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21803 - </strong>
            Yield right-of-way
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Yield when there is an{" "}
            <Link
              to="/california-motor-vehicle-code/indications-yield-signs"
              target="_blank"
            >
              indication of a yield sign
            </Link>{" "}
            that says to do so, when merging onto a road, or when a vehicle is
            crossing ahead of you.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21804 - </strong>
            Entering Onto a Highway
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            {" "}
            <Link to="/california-motor-vehicle-code/entering-highway-21804">
              Motor Vehicle Code 21804
            </Link>{" "}
            states that if the driver intends to enter onto a highway from a
            space of public or private property or an alley, the driver must
            yield the right-of-way to all closely approaching traffic already
            present on the highway.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21805 - </strong>
            Equestrian crossings
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/equestrian-crossings-21805">
              Motor Vehicle Code 21805
            </Link>
            , horseback riders may cross roads at officially designated crossing
            points, but should always use caution. Vehicles must yield to horses
            crossing.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21950 - </strong>
            Right of way at crosswalks
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/crosswalks-21950">
              Motor Vehicle Code 21950
            </Link>
            , pedestrians always have the right of way at crosswalks. Drivers
            should exercise the right of care when pedestrians are crossing
            roads illegally.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21952 - </strong>
            Right-of-way on sidewalk
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Always give the right-of-way to pedestrians if you are driving over
            or upon a{" "}
            <Link to="/california-motor-vehicle-code/crossing-sidewalks">
              sidewalk
            </Link>{" "}
            for any reason.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21954 - </strong>
            Pedestrians outside crosswalks
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Yield right-of-way to all vehicles. However, vehicles must exercise
            due care for the safety of{" "}
            <Link
              to="/california-motor-vehicle-code/driving-near-pedestrians-outside-crosswalks-cmvc-21954"
              target="_blank"
            >
              pedestrians outside of crosswalks
            </Link>
            .
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>21957 - </strong>
            Hitchhiking
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/hitchhiking-21957">
              Motor Vehicle Code 21957
            </Link>
            , standing in a road to solicit a ride is illegal.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22100 - </strong>
            Turning
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/u-turns-22100">
              Motor Vehicle Code 22100
            </Link>
            , only make U turns at intersections that allow it. No U turns near
            fire station driveways, on streets with obstructed views, in
            residence districts when another vehicle is approaching.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22112 - </strong>
            Stopping behind school bus
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/school-bus-signals-and-stops-cmvc-22112">
              Motor Vehicle Code 22112
            </Link>
            , stop behind and do not pass a school bus that is loading/unloading
            children and has flashing red lights and a stop sign.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22350 - </strong>
            Basic Speed Law
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/basic-speed-law-22350">
              Motor Vehicle Code 22350
            </Link>
            , always drive at a speed that is appropriate for the given road and
            weather conditions. Remember it is illegal to exceed the stated
            speed limit.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22352 - </strong>
            Speed Limits
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            Motor Vehicle Code 22352 requires the following{" "}
            <Link to="/california-motor-vehicle-code/prima-facie-speed-limits-cmvc-22352">
              prima facie speed limits
            </Link>{" "}
            unless otherwise posted. 15 mph: obstructed view railway crossing
            and intersections, in alleys; 25 mph: residential zones, school
            zones, near senior facilities, near park playgrounds.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22400 - </strong>
            Minimum speed law
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            With regards to the{" "}
            <Link to="/california-motor-vehicle-code/minimum-speed-laws-cmvc-22400">
              minimum speed law
            </Link>
            , you should not drive so slowly that you interfere or impede normal
            traffic, thus becoming a safety hazard.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22406 - </strong>
            Maximum speed limit for certain vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/speed-limits-22406">
              Motor Vehicle Code 22406
            </Link>
            , The following may not be driven at speeds over 55 mph: vehicle
            with 3 or more axles, vehicle towing another vehicle, school bus
            transporting any pupil, farm labor vehicle, vehicle transporting
            explosives, trailer bus.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>22451 - </strong>
            Train signals
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/railroad-tracks-22451">
              Motor Vehicle Code 22451
            </Link>
            , stop no less than 15 from rails and proceed when safe. Never pass
            through, around, or under the protective gates.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23103 - </strong>
            Reckless Driving
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/driving-recklessly-23103">
              Motor Vehicle Code 23103
            </Link>
            , driving on a highway or in a parking lot showing wanton disregard
            for the safety of persons or property.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23109 - </strong>
            Speed contests and exhibitions of speed
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/speed-contests-23109">
              Motor Vehicle Code 23109
            </Link>
            , racing, assisting in a race, or clocking high speeds is punishable
            with jail time and fines.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23110 - </strong>
            Throwing substance at vehicles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/throwing-objects-23110">
              Motor Vehicle Code 23110
            </Link>
            , do not throw anything at cars. Doing so will be treated as a
            misdemeanor or felony, depending on the damage.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23111 - </strong>
            Throwing substances on highways or adjoining areas
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/discharging-items-23111">
              Motor Vehicle Code 23111
            </Link>
            , throwing anything, including cigarettes lit or unlit, garbage,
            etc. is illegal.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23112 - </strong>
            No throwing, depositing, or dumping matter on a highway
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/matter-on-a-highway-23112">
              Motor Vehicle Code 23112
            </Link>
            , no one - driver, passenger or pedestrian- shall throw or deposit
            anything on the highway.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23116 - </strong>
            Pickup of flatbed trucks; transportation in back; section
            application
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/back-of-truck-23116">
              Motor Vehicle Code 23116
            </Link>
            , do not transport unrestrained passengers in the back of a truck
            unless you are on a farm, in an emergency, or in a law enforcement
            supervised parade.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23117 - </strong>
            Transportation of animals; enclosure or restraint requirements
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/transporting-animals-23117">
              Motor Vehicle Code 23117
            </Link>
            , do not transport animals in the back of a vehicle unless the space
            for it is enclosed, has side and tail racks 46 inches high, has
            means to prevent the animal from escaping, being thrown, or falling
            out of the vehicle. This does not apply to transporting cattle or
            traveling with dogs for farming reasons.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23123 - </strong>
            Driving a motor vehicle while using a wireless telephone
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/driving-cell-phones-23123">
              Motor Vehicle Code 23123
            </Link>
            , it is not allowed unless the phone is specifically designed and
            configured to be used hands-free. Making emergency phone calls to
            police or medical personnel is allowed.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23124 - </strong>
            Minor using wireless telephone while driving
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/minors-wireless-phones-23124">
              Motor Vehicle Code 23124
            </Link>
            , drivers under 18 cannot drive while using a wireless phone -- not
            even hands free.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23123.5 - </strong>
            Driving a motor vehicle while writing, sending or reading text-based
            communication
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to Motor Vehicle Code{" "}
            <Link to="/california-motor-vehicle-code/ewc-device-23123-5">
              23123.5
            </Link>
            , do not read, write, or send text based communication while
            driving.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23127 - </strong>
            Trails and paths
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/trails-and-paths-23127">
              Motor Vehicle Code 23127
            </Link>
            , do not operate a motor vehicle on a bike path, hiking trail, or
            horse trail where signs prohibit it, unless you have written
            permission from the owner of the trail. Bike paths that are adjacent
            and contiguous to a roadway are an exception.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23128 - </strong>
            Snowmobiles
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/operating-snowmobiles-23128">
              Motor Vehicle Code 23128
            </Link>
            , do not hunt or harass animals while on a snowmobile. Do not
            operate a snowmobile on a highway or in a way that is dangerous to
            somebody or their property.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23136 - </strong>
            Blood alcohol concentration of .01 or greater for someone under 21
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/alcohol-screening-23136">
              Motor Vehicle Code 23136
            </Link>
            , a person under 21 cannot drive with a BAC of .01 or greater.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23152 - </strong>
            Driving under influence; blood alcohol percentage, presumptions
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/dui-23152">
              Motor Vehicle Code 23152
            </Link>
            , do not drive under the influence of foreign substances. No person
            is allowed to drive if they are addicted to a drug, unless they are
            in rehab. Operating a vehicle with BAC of .08 or greater results in
            a DUI. Commercial vehicles cannot be operated with a BAC of .04 or
            greater.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23220 - </strong>
            Drinking while driving of any vehicle
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/drinking-while-driving-is-prohibited-23220">
              Motor Vehicle Code 23220
            </Link>
            , no drinking alcoholic beverages while driving or as a passenger in
            a vehicle on a highway.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23222 - </strong>
            Possession of open container containing alcoholic beverage or
            marijuana while driving
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/open-containers-23222">
              Motor Vehicle Code 23222
            </Link>
            , open containers in the car are illegal, as is a container with
            marijuana of more than one avoirdupois ounce.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23223 - </strong>
            Possession of opened container in a motor vehicle
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/possession-open-container-23223">
              Motor Vehicle Code 23223
            </Link>
            , drivers or passengers may not have open containers in a motor
            vehicle on a highway.
          </p>
        </div>
        <div className="clear"></div>
        <div className="fltlt-thin">
          <p>
            <strong>23224 - </strong>
            Possession of alcoholic beverage in vehicle; persons under 21
          </p>
        </div>
        <div className="fltrt-wide">
          <p>
            According to{" "}
            <Link to="/california-motor-vehicle-code/21-possession-alcohol-23224">
              Motor Vehicle Code 23224
            </Link>
            , drivers or passengers under 21 years old cannot possess alcoholic
            beverages in their car, unless accompanied by a parent or
            responsible adult relative designated by the parents.
          </p>
        </div>
        <div className="clear"></div>
        <div className="line-break"></div>
        <h2>Frequently Asked Questions Regarding Motor Vehicle Codes</h2>
        <strong>
          Is there a Motor Vehicle Code against "Failing to Avoid an Accident?"
          -{" "}
        </strong>
        <p>
          There isn't anything in the VC concerning "failing to avoid an
          accident". However, any time you change lanes, turn, or enter a
          roadway, you need to make sure that the maneuver can be performed
          without endangering another driver or pedestrian unless you have the
          right of way which has been yielded to you. The entry of the roadway
          section is 21804, which says "The driver of a vehicle about to enter
          or cross a highway... shall yield the right of way to all traffic...
          until he or she can proceed with reasonable safety." The "unsafe speed
          for the conditions" (22350) and "reckless driving" (23103) behaviors
          are the arguments against the speeding driver who did nothing
          reasonable to avoid a collision that was obviously waiting to happen.
        </p>
        <div className="line-break"></div>

        <h2>
          Call 949-203-3814 for award-winning Orange County Personal Injury
          Attorneys in your area!
        </h2>
        <p>
          NOTICE: This is not legal advice. It is created for your reference and
          interest, only. It is not an exhaustive statement of the content of
          the "Rules of the Road" from the California Vehicle Code or any
          section of the code. It is offered to you to help guide you to
          statutory provisions that you might want to read more completely. It
          does not give you an analysis of the application of a particular code
          section to the facts of any specific incident, ticket, or accident. To
          receive legal advice you need to discuss the particular facts of
          interest to you with a licensed California attorney.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
