// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23124 - Minors & Wireless Phones"
        pageDescription="California Motor Vehicle Code 23124 is about distracted driving caused by minors using cell phones while driving."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23124</h1>
        <BreadCrumbs location={location} />
        <h2>Driving and Cell Phones</h2>
        <p>
          Statistics from Carnegie Mellon University show that distraction while
          driving (DWD) is responsible for 25 percent of all car accidents, as
          the amount of brain activity devoted to driving can decrease by as
          much as 37 percent when using a wireless telephone.
        </p>
        <p>
          Young people are considered new drivers and it is important that all
          of their attention is focused on the road while operating a vehicle.
          The combination of a new driver and DWD could cause a very serious
          accident and result in injury or death.
        </p>
        <p>
          Vehicle Code 23124 was implemented in conjunction with Code 23123, and
          aims to prevent accidents that are caused by distracted driving due to
          cell phone use. Code 23124 prohibits people under 18 years old from
          driving a motor vehicle while using a wireless device of any kind.
          This includes hands-free or mobile service devices such as a laptop,
          pager, mobile radio, or other handheld devices.
        </p>
        <p>
          Young drivers who commit the offense of using a cell phone while
          driving will incur a base fine of $20 the first time. The fine will be
          raised to $50 for each subsequent offense.
        </p>
        <p>
          Law officers are allowed to stop drivers if they are using their
          cellphone illegally while driving, as in Code 23123, but are not
          allowed to stop drivers for the sole purpose of determining whether
          the driver is violating the law. The officer can only stop a driver if
          he or she has identified that a wireless device is being used while
          driving.
        </p>
        <p>
          This code does not apply in instances of emergency. If a call must be
          made for emergency purposes to a law enforcement agency, health care
          provider, fire department, or other emergency services agency, the
          driver may use a cell phone while driving.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
