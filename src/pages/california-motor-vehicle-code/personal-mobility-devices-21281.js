// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21281.5 - Personal Mobility Devices"
        pageDescription="California Motor Vehicle Code 21281.5 relates to laws regarding personal mobility devices like electric wheelchairs and Rascal scooters."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21281.5</h1>
        <BreadCrumbs location={location} />
        <h2>Electric Personal Mobility Devices</h2>
        <img
          src="/images/personal-injury/electric-wheelchair.jpg"
          className="imgright-fixed"
          alt="electric wheelchair"
        />
        <p>
          California Motor Vehicle Code 21281.5 allows personal mobility devices
          such as Rascal scooters and electric wheelchairs to operate on
          sidewalks and bike paths so long as pedestrians are given the
          right-of-way and they are operated in a safe manner.
        </p>
        <p>
          There is no set speed limit for the operation of personal mobility
          devices. The driver must use common sense and good judgment to remain
          safe and avoid a ticket. What may be a safe speed on a slow the
          afternoon under dry conditions may not be a safe speed the very next
          day during a busy lunch rush while it's raining.
        </p>
        <p>
          Local authorities will not tolerate wheelchair operators who show a
          general disregard for public safety. Anyone operating a mobility
          device in a manner that is unsafe to persons, animals, or property
          will be ticketed and fined. Horseplay, driving too fast for the
          conditions, and not yielding to pedestrians and service animals could
          all be viewed as a general disregard for public safety. You may also
          be ticketed for driving your mobility device under the influence of
          drugs or alcohol.
        </p>
        <p>
          When riding on a mobility device, you must yield to all pedestrians on
          foot, other disability assistance devices and service animals that may
          be in your path. Common courtesy is expected when you are riding on
          public sidewalks and pathways.
        </p>
        <p>
          A good rule-of-thumb is to yield the right-of-way to anyone who is
          traveling at speeds slower than your mobility unit is moving. Being a
          courteous and considerate operator on public sidewalks, bike paths,
          and trails will help keep everyone safe.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
