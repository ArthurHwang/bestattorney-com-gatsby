// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 22350 - Basic Speed Law"
        pageDescription="California Motor Vehicle Code 22350 defines the basic speed law. Get the details here."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 22350</h1>
        <BreadCrumbs location={location} />
        <h2>Basic Speed Law</h2>
        <p>
          The California Vehicle Code section 22350 states that no person shall
          drive a vehicle upon a highway at a speed greater than is reasonable
          or prudent having due regard for weather, visibility, the traffic on,
          and the surface and width of, the highway, and in no event at a speed
          which endangers the safety of persons or property.
        </p>
        <p>
          This applies to all roadways, even those with posted speed limits. In
          other words, the maximum speed may be considerably lower than the
          posted speed limit, depending on condition of the roadway.
        </p>
        <p>
          The fine for violating CVC 22350 is dependent on county and the number
          of MPH you were traveling over the posted speed limit, or over safe
          speeds. Fines will usually range between $150 - $400.
        </p>
        <p>
          Several other vehicle codes dealing with speed limits refer to the
          basic speed law. For example, if you exceed the{" "}
          <Link to="/california-motor-vehicle-code/prima-facie-speed-limits-cmvc-22352">
            prima facie speeds
          </Link>{" "}
          set for school zones, railroad crossings, and alleys you will get a
          ticket, unless you can show that your speed did not violate the basic
          speed law. (For more on this law, see CVC 22352, under Rules of the
          Road)
        </p>
        <p>
          When a local police or CHP officer, or a Sheriff's Deputy, states in
          his or her report that you were violating the basic speed law, the
          officer will be allowed to testify against you if you should go to
          trial. Since the officer has specialized training to know how to drive
          in all road conditions -- good and bad -- his or her testimony will
          probably be all the judge needs to find you guilty.
        </p>
        <p>At BISNAR CHASE we want everyone to be safe using the roads. </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
