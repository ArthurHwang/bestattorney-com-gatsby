// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21718 - When Drivers Can Stop on the Freeway"
        pageDescription="California Motor Vehicle Code 21718 defines stopping on the freeway as illegal and dangerous. Get more information here."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21718</h1>
        <BreadCrumbs location={location} />
        <h2>When Drivers Can Stop on the Freeway</h2>
        <p>
          It is illegal and dangerous to stop or park a vehicle on the freeway,
          but occasionally it has to be done. There are a few instances where
          stopping, parking, or leaving a vehicle on a fully accessible freeway
          is a reasonable decision to make.
        </p>
        <p>
          Parking a vehicle on the freeway to avoid injury or damage to persons
          or property is the first example. As a driver, acting to prevent
          dangerous situations that will harm other people or peoples' property
          is hugely important. Everyone must strive for the safest possible
          driving situation.
        </p>
        <p>
          If a driver is required by law, he must pull over. This can occur in
          obedience with a peace officer or official traffic control device.
        </p>
        <p>
          Those who are employed by public agencies to maintain the freeway or
          work in construction of the freeway are allowed to park on freeway
          space while engaged in the performance of these official duties.
        </p>
        <p>
          If a driver's vehicle becomes severely disabled on the road, he may
          need to stop. This is acceptable so long as another vehicle has been
          summoned to assist the disabled vehicle. Leaving a disabled vehicle on
          the freeway for a long period of time is illegal, and towing of said
          vehicle must be prompt to avoid the risk of collision created by
          leaving a stationary object in a high speed roadway.
        </p>
        <p>
          On the freeway, if any space is provided for stopping, standing, or
          parking, then it is permissible. This is applicable in the case of
          buses which are allowed to stop when sidewalks and shoulders with
          sufficient width are located on the side of the roadway. However,
          there has to be enough space for the bus to pullover without
          interfering with the normal movement of traffic.
        </p>
        <p>
          If an emergency has occurred on the freeway, a driver may pullover and
          stop to make the necessary report to a peace officer by emergency
          telephone or other device. This will help notify officers to aid the
          scene if those involved in the accident are unable to call for help
          themselves.
        </p>
        <p>
          Occasionally other drivers will have to stop on the freeway as an
          impeding object or vehicle is towed. The tow truck must operate under
          an agreement with the Department of the California Highway Patrol, and
          all other vehicle will wait until the road is clear and safe to
          proceed. Failing to stop at times like this is considered a violation
          of safe operation of a motor vehicle upon the highway.
        </p>
        <p>
          Common sense is the best thing to employ when unforeseen circumstances
          occur while driving on the freeway. Drivers should consider the safest
          way to deal with situations that make require stopping on a freeway.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
