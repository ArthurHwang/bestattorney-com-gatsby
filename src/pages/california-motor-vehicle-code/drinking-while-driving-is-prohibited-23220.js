// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23220 - Drinking While Driving is Prohibited"
        pageDescription="Drinking while driving is not legal and open containers of alcohol must be a far distance from the driver. Read California Motor Vehicle Code 23220."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 23220</h1>
        <BreadCrumbs location={location} />
        <h2>Drinking While Driving is Prohibited</h2>
        <p>
          If driving under the influence of alcohol is illegal, there is no
          situation during which drinking while driving is permissible. Vehicle
          code 23220 points out that no person shall drink any alcoholic
          beverage while operating a motor vehicle on a highway.
        </p>
        <p>
          Drinking an alcoholic beverage while driving will be defined as having
          an open container in possession or close proximity to the driver while
          the vehicle is being operated. Drivers must be careful to have any
          open or opened container of alcohol that is not being consumed but
          merely transported, far away from the driver and in the trunk of the
          vehicle if possible. If a driver in this situation were to be pulled
          over for any reason, a police officer may be suspicious of any open
          container.
        </p>
        <p>
          Drivers must use common sense and be safe while driving. Drinking
          while driving is a dangerous distraction and is not worth the
          punishment.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
