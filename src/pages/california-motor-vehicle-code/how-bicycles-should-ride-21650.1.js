// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21650.1 - How Bicycles Should Ride"
        pageDescription="The direction that bicyclists should ride on roadways and the laws regarding it can be found in California Motor Vehicle Code 21752."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>How Bicycles Should Ride</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/bike-accidents/wrong-way-bicycle.jpg"
          className="imgleft-fixed"
          alt="wrong way bicycle sign"
        />
        <p>
          California Vehicle Code 21202 discusses many of the important laws
          guiding bicycle riders on the roadway, but code 21650.1 draws specific
          attention to the direction bicycles should ride. Because bicycles are
          subject to many of the same laws as motor vehicles, bicycles must be
          operated in the same direction as vehicles are required to be driven.
        </p>
        <p>
          This law is key, as bicycles on the roadway are required to use the
          right side of the road, either in the shoulder or in a bike lane, if
          car traffic is fast. In order for the bicycle to travel on the right
          side of the road, he must be traveling within the right flow of
          traffic, otherwise he would be in the center of opposing traffic. It
          is not appropriate for bicycle riders to travel on the far opposite
          side of the road, which may be the left hand side, against the flow of
          traffic.
        </p>
        <h3>Riding On The Sidewalk</h3>
        <p>
          In some cities it is lawful for bicycles to be used on the sidewalk.
          This law is often determined locally, and bicyclists must learn what
          is legal for where they ride. If it is legal for bicycles to travel on
          the sidewalk, it may not be required for the bicycle to be on the
          right side of the road, traveling with the flow of traffic.
        </p>
        <p>
          However, most laws allowing bicycles to be used on sidewalks still
          require that bicycles give the right of way to pedestrians and that
          bicycles be operated at a slow, safe speed. Traveling against
          pedestrian traffic on the sidewalk may be dangerous or illegal
          depending on local laws.
        </p>
        <h3>Riding Side by Side</h3>
        <p>
          The California Vehicle Code doesn't state anything about riding
          side-by-side in the bike lane. However,{" "}
          <Link to="https://en.wikipedia.org/wiki/Bicycle_law_in_California">
            this article on wikipedia
          </Link>{" "}
          shows that there may be some local municipality rules governing bike
          riding side-by-side, so we recommend that you contact your city to
          make sure that it is legal.
        </p>
        <p>
          In other states like colorado, it is legal to ride two abreast unless
          doing so impedes traffic, or is otherwise dangerous (On a single lane
          highway or a windy canyon road). It would be wise to take similar
          safety precautions in California.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
