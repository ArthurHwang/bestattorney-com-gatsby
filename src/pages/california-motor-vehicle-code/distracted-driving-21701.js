// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21701 - Distracted Driving"
        pageDescription="It is illegal to interfere with the driving mechanism of a vehicle or the driver as stated in California Motor Vehicle Code 21701."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21701</h1>
        <BreadCrumbs location={location} />
        <h2>Distracted Driving</h2>
        <p>
          California vehicle code 21701 makes it illegal to interfere with the
          driver of a vehicle. It is also illegal to interfere with the driving
          mechanism in a way that may affect the driver's ability to control the
          car. In addition to being illegal, distracting the driver is also
          unsafe and a very bad idea.
        </p>
        <p>
          There are many ways that this law can be broken either intentionally
          or unintentionally. Teen drivers and passengers often like to
          horseplay or turn up the music when they are alone in the car without
          adult supervision. This is why many states, including California, have
          enacted legislation limiting the number of teen drivers allowed in a
          car at one time.
        </p>
        <p>
          Even if you are not intentionally bothering the driver, just messing
          around and keeping them from focusing on the road will get you into
          serious trouble if they have an accident. If you are moving around a
          lot, or dancing, or touching the steering wheel while your friend is
          trying to drive, you could be putting many lives in danger. When
          riding in the car, sit still and don't bother the driver. It could
          save lives.
        </p>
        <p>
          If you receive a ticket for a violation of CVC 21701, you will receive
          hefty fines and have points added to your license. You could even
          receive jail time if it{" "}
          <Link to="/wrongful-death">
            causes an accident where someone is killed or injured
          </Link>
          .
        </p>
        <p>
          Remember that driving is a privilege and not a right. If you are
          caught driving in a way that is dangerous, or if you are the passenger
          who is distracting the driver, you could be ticketed by the local
          authorities or the California Highway Patrol.
        </p>
        <h2>Rights for Victims of Distracted Drivers</h2>
        <p>
          If you've been injured because of a distracted driver you may be
          entitled to compensation. Plaintiff's of a distracted driving case may
          be eligible to collect compensation including time off work and
          medical bills. Contact the car accident attorneys of Bisnar Chase for
          a free consultation and to learn of your rights. Call 949-203-3814.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
