// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21700 - Obstruction to Driving"
        pageDescription="It is illegal and dangerous to drive your vehicle with an obstructed view. California Motor Vehicle Code 21700."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21700</h1>
        <BreadCrumbs location={location} />
        <h2>Obstruction to Driving</h2>
        <p>
          Driving your vehicle with an obstructed view is both illegal and
          dangerous. If you have your car so loaded that you can't see out the
          rear and side windows, you could be placing yourself and others in
          harms way.
        </p>
        <p>
          Some cars and trucks are so loaded with kids, moving boxes, and junk
          that the driver cannot see properly. If the driver gets into an
          accident, he or she could be charged with criminal penalties and face
          civil penalties as well. It's never a good idea to drive when you
          can't see out the car windows.
        </p>
        <p>
          If you are moving, or some other activity that requires carrying many
          belongings in your car, make sure you can see properly. You should at
          least be able to see all of your mirrors, see out the rear window, and
          see out the passenger side front and rear windows. When stacking boxes
          in the car, remember to leave room for visibility once the doors are
          closed.
        </p>
        <p>
          Sometimes drivers will overload their cars with passengers instead of
          boxes. It is also illegal to have so many people in the car that it
          obstructs your view and your ability to drive. If you are going to an
          event that involves a large number of people, you should take two cars
          or rent a van large enough to transport everyone safely. You cannot
          depend on your passengers to move their heads in time for you to see
          oncoming cars.
        </p>
        <p>
          If you are pulled over for driving a car that is overloaded, you will
          receive a ticket and have points added to your license. You could
          receive fines up to $1000 for each offense. It would be much cheaper
          to rent a moving van than it would be to pay the fines for overloading
          your car. Weigh your options before packing your car full of boxes.
        </p>
        <p>
          When in doubt, it is always best to err on the side of caution. If you
          can't see out every window, and see all mirrors, then you are probably
          overloaded.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
