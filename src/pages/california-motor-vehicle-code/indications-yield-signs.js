// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21803 - Indications of Yield Signs"
        pageDescription="Slowing to oncoming traffic such as entering a freeway is a requirement according to California Motor Vehicle Code 21803."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21803</h1>
        <BreadCrumbs location={location} />
        <h2>Indications of Yield Signs</h2>
        <p>
          Yield signs are typically triangular in shape, pointing downward, and
          either red or yellow. The signs read "YIELD" and mean "give way" to
          cross traffic.
        </p>
        <p>
          When a driver approaches an intersection that has a yield sign posted
          and facing the driver, he must slow to the oncoming traffic which has
          the right-of-way. The driver at the yield sign may continue into the
          intersection when the road is clear and no other vehicles are close
          enough to pose an immediate hazard to the driver as he progresses
          through the intersection.
        </p>
        <p>
          Yield signs are not stop signs, but should be treated similarly. The
          yield sign indicates that a driver be cautious of on-coming traffic,
          and that the driver is responsible for waiting until the intersection
          is clear. While yield signs do not require drivers to make a complete
          stop, drivers should be prepared to stop, but can continue if no
          oncoming traffic is blocking the road.
        </p>
        <p>
          Drivers can expect yield signs to be posted on through streets that
          meet highways, at crossroads of divided highways, on separated turn
          lanes, at intersections where special problems are occurring, or
          facing roadways that merge but require extra control due to inadequate
          acceleration geometries or sight distances.
        </p>
        <p>
          If drivers fail to yield to oncoming traffic when a yield sign is
          posted collisions may occur where vehicles must merge.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
