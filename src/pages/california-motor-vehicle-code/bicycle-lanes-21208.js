// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21208 - Bicycle Lanes"
        pageDescription="California Motor Vehicle Code 21208 states that bicyclists must ride in bike lane if one is provided.There are exceptions to this. Read more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21208</h1>
        <BreadCrumbs location={location} />
        <h2>Bicycle Lanes</h2>
        <p>
          According to California Vehicle Code 21208, if your city has a lane
          dedicated specifically for bicycles, then all bike riders must use the
          bike lane. This law is meant to give riders a safe place to ride on
          busy city streets. Thousands of{" "}
          <Link to="/bicycle-accidents">bicycle accidents</Link> occur each year
          on busy southern California streets, and many cities have dedicated
          bike lanes for those who wish to travel in a more environmentally
          friendly manner. There are exceptions to this, though, and it is
          important to know what those may be. If it is unsafe to ride inside
          the bike lane, for whatever reason, then riders should use common
          sense and ride where it will be the safest for them.
        </p>
        <p>
          <img
            src="/images/bike-accidents/bike-lane.jpg"
            className="imgright-fixed"
            alt="California Motor Vehicle Code 21208"
          />
          Drivers are not always courteous to bicyclists, and they sometimes
          ignore dedicated lanes. If there are cars illegally parked in the bike
          lane, then you should not ride in the lane until it is safe to do so.
          A little vigilance goes a long way in protecting yourself and others.
        </p>
        <p>
          If you are an avid cyclist who can travel at high rates of speed, you
          may not want to risk other rider's safety by riding in the bicycle
          lane. One of the common exceptions to this law is for those who can
          safely ride at the same speed as cars and trucks. In that scenario,
          you don't have to ride in the dedicated bike lane. It would be
          incredibly unsafe to go whizzing past slower riders inside a tiny
          bicycle lane.{" "}
        </p>
        <p>
          If you are leaving the bicycle lane for any reason, such as: passing
          other bikers, turning onto another street, or avoiding road hazards;
          you must signal your intentions to all moving vehicles before leaving
          the bicycle lane. Just because there is a dedicated bike lane, this
          does not give you an excuse to dart in front of passing motorists or
          other bikes.{" "}
        </p>
        <p>
          Safety will always be the number one factor considered in any
          situation. If you ride outside the dedicated bike lane in an unsafe
          manner, you will most likely get a ticket for your actions. When in
          doubt, it's best to stay inside the dedicated bike lane.{" "}
        </p>
        <p>
          By following this traffic law, and considering the safety of both
          yourself and others, the streets can be an enjoyable place for
          everyone using them.{" "}
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
