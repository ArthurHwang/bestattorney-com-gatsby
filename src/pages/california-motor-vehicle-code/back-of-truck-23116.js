// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 23116 - Riding in the Back of a Motor Truck"
        pageDescription="California vehicle code 23116 is about riding in the back of a truck. Get more information here."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Motor Vehicle Code 23116 - Riding in the Back of a Truck
        </h1>
        <BreadCrumbs location={location} />
        <h2>When Is It Okay to be Riding in the Back of a Truck?</h2>
        <p>
          According to California Vehicle Code 23116, no one is permitted to
          transport other people in the bed or moving motor trucks. This
          includes transporting occupants on a highway, unless the bed of the
          truck is equipped with a restraining system which meets or exceeds the
          federal motor vehicle safety standards for seat belts. In the case
          that an occupant is riding in the back of a truck unrestrained, both
          the driver and the occupant may be subject to punishment for the
          offense.
        </p>
        <p>
          There are exceptions to this code. For instance, if a driver operates
          the truck with an unrestrained occupant in the back of the truck on a
          farm or ranch, though it may be unsafe, it is not illegal. A driver is
          permitted to transport people in a truck bed without proper restraints
          if the vehicle is used exclusively within the boundaries of land owned
          by the farmer or rancher and not on a highway.
        </p>
        <p>
          It is also acceptable to transport people in the back of a truck if
          the truck or flatbed is being used in an emergency response situation
          by a public agency. "Emergency response situations" include times when
          necessary measures are taken in order to prevent injury, death, or the
          destruction of property.
        </p>
        <p>
          The last instance when it is lawful to carry passenger in a motor
          truck's flatbed is if the truck is being transported slowly in a
          parade supervised by a law enforcement agency. Trucks in this
          situation are not permitted to travel over 8 miles per hour.
        </p>
        <p>
          Drivers and passengers should always wear safety restraints to prevent
          serious personal injury in the case of an accident or collision.
        </p>
        <p>
          Generally speaking, with a few minor exceptions, according to the
          California vehicle code, it is never okay to be riding in the back of
          the truck.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
