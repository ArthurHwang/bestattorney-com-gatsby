// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Vehicle Code 21706 -- Driving Behind Emergency Vehicles"
        pageDescription="Do you know what to do when there is an emergency vehicle on the road? Read Motor Vehicle Code 21752 for information."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>How to Drive Behind Emergency Vehicles</h1>
        <BreadCrumbs location={location} />
        <p>
          California Vehicle Code 21706 explains what to do when there is an
          emergency vehicle on the road. Emergency vehicles are typically
          equipped with flashing lights and sirens to make themselves obvious,
          and may be a police car, ambulance, or a fire engine. These vehicles
          always receive the right of way, as they are traveling as quickly as
          possible to address an emergency situation.
        </p>
        <p>
          It is important for all other drivers to get out of the way of
          emergency vehicles. All drivers, upon hearing emergency sirens, must
          make their way as closely as possible to the right shoulder of the
          road and wait for the emergency vehicle to pass. If a driver is unable
          to move from their position in the road, staying stationary and
          allowing other vehicles to move rightward is acceptable.
        </p>
        <p>
          After the emergency vehicle has made their way through the roadway and
          has a clear trajectory, other vehicles may begin driving. However, all
          drivers must be at least 300 feet behind or away from the authorized
          emergency vehicle. Waiting for the emergency vehicle to gain a
          sufficient gap before rejoining the roadway is important.
        </p>
        <p>
          Any driver who travels within 300 feet of an emergency vehicle is
          traveling illegally and in an unsafe manner.
        </p>
        <p>
          There is one instance where this section does not apply. As noted in
          Section 21057, police or traffic officers serving as escorts on the
          roadway do not require the same 300 feet gap behind them.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
