// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21224 - Motorized Scooter Exemptions"
        pageDescription="California Motor Vehicle Code 21224 states that some scooters are exempt from this vehicle code such as scooters that provide a platform to stand on."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21224</h1>
        <BreadCrumbs location={location} />
        <h2>Motorized Scooter Exemptions</h2>
        <img
          src="/images/bike-accidents/motorized-scooter.jpg"
          className="imgleft-fixed"
          alt="segway scooter"
        />
        <p>
          According to California law, motorized scooters do not have to be
          registered by the state. This rule applies to scooters designed to
          allow the rider to stand on a platform, motorized bicycles, and mopeds
          designed to travel less than 30 mph. For any of these modes of travel,
          there is no requirement for licensing, registration, or insurance.
        </p>

        <p>
          Local authorities are free to place parking restrictions and bicycle
          lane restrictions on scooters if they so choose. These restrictions
          may not interfere with the state laws on motorized scooter operation.
          If the speed limit on the roadway is greater than 25 mph, then you
          must ride your scooter inside the bike lane.
        </p>
        <p>
          Scooters are not required to have horns, mirrors, or other similar
          features that would be required on motorcycles and cars. If the
          scooter was designed to have a muffler (many mopeds have this feature)
          then the muffler must be functional.
        </p>
        <p>
          Scooters must be lighted after dark. The light is only required on the
          front, so the scooter operator can see what any hazards in the road. A
          rear light is not required, but there must be some sort of reflector
          on the rear and sides of the scooter that is visible to oncoming cars.
          Even though not required, it is a good idea to have some form of rear
          flashing or stationary light. If a rear light is used, it must be red.
        </p>
        <p>
          {" "}
          <Link to="/california-motor-vehicle-code/vehicle-code-prohibitions-for-operation-motorized-scooters-21235">
            This vehicle code does not apply to Vespas, Hondas and other faster
            and more powerful scooters
          </Link>
          . If your scooter is designed to travel at speeds faster than 30 mph,
          then you are considered a motorcycle under California laws. These
          scooters must be registered with the state DMV, and you must have an
          M1 or M2 motorcycle license to operate them.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
