// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21654 - Slow Moving Vehicles"
        pageDescription="Slow moving vehicles are required to stay in the lane farthest to the right as stated in California Motor Vehicle Code 21654."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21654</h1>
        <BreadCrumbs location={location} />
        <h2>Slow Moving Vehicles</h2>
        <p>
          California Vehicle Code 21654 is applicable to slow moving vehicles
          other than bicycles, such as cars and trucks. California Vehicle Code
          section 21654 states that all slow moving vehicles must stay as close
          to the right side of the roadway as possible while driving slower than
          other cars and trucks on the road. This law is intended to provide a
          safe and pleasant traveling experience for all persons using
          California roadways.
        </p>
        <p>
          There are exceptions to this rule. If you are passing vehicles moving
          slower than you,{" "}
          <img
            src="/images/truck-accidents/truck-lane.jpg"
            className="imgright-fixed"
            alt="slow moving truck"
          />
          you can pass them in the left lane, and then return to the right lane
          as soon as it is safe to do so. Also, if you are preparing for a left
          hand turn you should not remain in the far right lane.
        </p>
        <p>
          If you are passing a slower moving vehicle, you should return to the
          far right lane as soon as it is safe to do so, if you are still
          traveling slower than other traffic on the road. Often drivers will
          get into the left lane to pass and then forget to return to the right
          lane. The exception for passing slower vehicles only applies while you
          are immediately passing them. You cannot get pulled over several miles
          down the road and use the excuse that you pulled into the left lane to
          pass someone.
        </p>
        <p>
          If you are preparing for a left hand turn, you must signal your
          intentions to other drivers. Use your turn signals and follow all
          safety precautions. Once the turn has been completed, you must return
          to the far right hand lane again if you are driving slower than
          traffic moving in your new direction. Traveling in the far right lane
          applies to slow moving traffic on all roads where there is more than
          one lane moving in the same direction.
        </p>
        <p>
          Many jurisdictions will place signs on roadways to signal slower
          drivers to move to the right lanes of the road. The law still applies,
          however, even if there are no signs on the road. Drivers are
          responsible to know the traffic laws, and lack of reminder signs will
          not be an acceptable excuse to avoid a traffic ticket. It will also
          not relieve you of liability if you were to cause an accident by
          driving slower than everyone else on the roads.
        </p>
        <p>
          By following this traffic law, and considering the safety of both
          yourself and others, the streets can be an enjoyable place for
          everyone using them.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
