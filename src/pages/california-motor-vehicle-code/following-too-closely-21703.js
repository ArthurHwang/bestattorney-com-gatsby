// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motor Vehicle Code 21703 - Following Too Closely"
        pageDescription="California Motor Vehicle Code 21703 makes it illegal to drive to closely to the vehicle in front of you. A safe distance is required."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motor Vehicle Code 21703</h1>
        <BreadCrumbs location={location} />
        <h2>Following Too Closely</h2>
        <p>
          California vehicle code 21703 requires you to leave a safe distance
          between you and the car in front of you. This distance will vary
          according to speeds, road conditions, traffic congestion and weather.
          The standard that the courts look at is whether your distance is
          "reasonable and prudent" for the circumstances. If you are cited, and
          go to court to fight the ticket, the judge will determine if the
          distance between your car and the car in front of you were far enough
          to be considered reasonable and prudent.
        </p>
        <p>
          Drivers caught tailgating will receive a ticket, have points added to
          their license, and have to pay fines. After fines, various fees and
          court costs, you could easily spend $400 or more fighting the ticket.
          The added points on your license could also make your insurance rates
          go up.
        </p>
        <p>
          Tailgating tickets are handed out every day on busy California
          streets. Many drivers don't realize the dangerousness of driving too
          close and get irritated when they are pulled over for it. Drivers who
          tailgate are typically inattentive and in a hurry. They don't look for
          road hazards and other potential obstacles ahead. If the driver in
          front of the tailgater were to brake suddenly, the tailgater would not
          have time to stop. This could be a deadly mistake, and courts take CVC
          21703 citations seriously.
        </p>
        <p>
          The standard here is subjective. There's no set distance to explain
          reasonable and prudent, it all depends on road conditions and officer
          observation. If you are given a ticket for this offense, it will be
          the officer's opinion versus your own opinion. Typically, the judge
          will side with the officer. Cops are extremely busy, and very few
          judges will believe that your officer pulled you over and gave you a
          ticket without a good reason.
        </p>
        <p>
          Following the two-second rule is a good judge of your distance. If
          there is a two to three second gap between your car and the one in
          front of you, then you should be far enough away so long as the roads
          are dry and visible.
        </p>
        <h2>Were You Rear-ended in a No Fault Car Accident?</h2>
        <p>
          The California motor vehicle accident attorneys of Bisnar Chase can
          evaluate your case with a no obligation free consultation. We've been
          helping car accident plaintiffs since 1978 and advance all costs until
          your case is won. Call 949-203-3814 today.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
