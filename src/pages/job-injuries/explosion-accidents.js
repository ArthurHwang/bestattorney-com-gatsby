// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Explosion Attorney - Fire and Explosion Lawyers"
        pageDescription="Explosions or fires triggered by explosions often result in loss of life, catastrophic injuries and property damage. If you have been a victim of an explosionv in California, call 949-203-3814 immediately for a free case evaluation and to get the compensation you deserve."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Explosion Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            {" "}
            If you have been a victim of an explosion, call 949-203-3814
            immediately for a free case evaluation and to get the compensation
            you deserve.
          </strong>
        </p>
        <p>
          Explosions or fires triggered by explosions often result in{" "}
          <Link to="/wrongful-death">wrongful death </Link>, catastrophic
          injuries and property damage. Explosions may occur in the workplace,
          in your vehicle, on a public street or even in the safety of your own
          home. While medical insurance, homeowners insurance or workers'
          compensation insurance may cover part or some of your significant
          losses sustained in an explosion, you may not be fully compensated.
        </p>
        <p>
          In addition to causing significant pain and suffering, burn injuries
          can also be extremely costly to treat. Those who have suffered severe
          burns often need extended hospital stays, multiple surgeries and even
          skin graft procedures. Even after all these treatments, a burn injury
          victim may not recover completely. He or she may become disfigured and
          emotionally traumatized for life.
        </p>
        <h2>Steps to Take after an Explosion</h2>
        <p>
          If you or a family member has been injured in an explosion, you are
          probably completely overwhelmed at this time physically, emotionally
          and financially. There are probably a number of questions going
          through your mind. How could this have happened to you? What caused
          the explosion? Who was responsible? How long will it take for you to
          recover? Will you ever be able to return to work and earn your
          livelihood? While these are all valid and important questions, there
          are several steps victims or their families would be well advised to
          take in the aftermath of such a catastrophic incident.
        </p>
        <p>
          <strong> Get prompt medical attention:</strong> If you have suffered
          severe burn injuries, then you are probably being rushed to an
          emergency room for immediate treatment. However, if your injuries are
          not apparent or if you "feel fine" after an explosion, it is still
          important that you go to the hospital and get thoroughly examined by a
          doctor. Victims sometimes do not feel their injuries because they are
          still in shock. Some explosion victims may suffer internal injuries.
          Others may find that their vision or hearing is affected. You do not
          want to take chance when it comes to your health and long-term
          well-being. When you get prompt medical attention, you not only
          maximize your chances of a full recovery, but you also create a paper
          trail in your case and a means to document your injuries.
        </p>
        <p>
          <strong> Contact the authorities:</strong> In the event of an
          explosion and fire, you will already have firefighters and paramedics
          at the scene. Often, arson investigators are also called to the scene
          to conduct an investigation into what caused the fire or explosion or
          whether it was intentionally set. It is crucial that your account of
          the incident gets into the report. Make sure that you talk to
          investigators and officials at the scene if possible.
        </p>
        <p>
          <strong> Take basic safety steps:</strong> For example, do not go into
          the burned building even if it may be to retrieve an important item.
          Often, the structural integrity of buildings is compromised after an
          explosion or fire. It is also important to contact the utility
          companies so gas and electricity is turned off. This will help prevent
          further incidents or injuries.
        </p>
        <p>
          <strong> Get in touch with the insurance company:</strong> If the
          explosion involved your home or car, please contact the relevant
          insurance companies. Often, insurance companies require that reports
          of any accidents be promptly made. It is also important that you
          understand your insurance policy so you know what is covered and what
          is not. If you have lost your home and possessions, you can seek
          assistance from the American Red Cross (1-800-RED-CROSS or
          www.redcross.org) or your local Salvation Army or Goodwill Stores. Red
          Cross often helps victims of fires and explosions find free or
          low-cost temporary accommodation. These organizations will also be
          able to connect victims with much-needed resources.
        </p>
        <p>
          <strong> Save all your receipts:</strong> It is crucial that you keep
          track of all the expenses you incurred immediately after such an
          incident. This could include hotel stay, loss of income, emergency
          room costs, cost of ambulance transportation, all medical and
          hospitalization costs, rehabilitation and continuing treatment and
          care, property damage and cost of rebuilding.
        </p>
        <h2>Get the Help You Need</h2>
        <p>
          It is also important that you protect your legal rights by contacting
          an experienced California explosion attorney who will fight for your
          rights and protect your best interests. It is crucial that you choose
          the right lawyer who has experience successfully handling cases
          similar to yours. In these types of cases, there may be several
          at-fault parties with different levels of involvement and liability.
          In addition, there may also be other injured parties claiming
          compensation. A renowned California personal injury law firm will have
          the knowledge, skill and resources that are required to handle these
          types of complex cases in order to conduct a thorough investigation
          and{" "}
          <Link to="/job-injuries/explosion-injuries">
            {" "}
            secure maximum compensation
          </Link>{" "}
          for severely injured victims.
        </p>
        <p>
          <strong>
            {" "}
            If you have been a victim of an explosion, you should call
            949-203-3814 immediately for a free case evaluation and to get the
            compensation you deserve.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
