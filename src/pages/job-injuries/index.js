// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Job Injury Attorneys & Workers Compensation Lawyers"
        pageDescription="Injured on the job? Contact our top-rated California Job Injury Attorneys & Workers Comp Lawyers for a free consultation. Millions in recovery for our injured clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Job Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <img
            src="/images/employment/back-injury.jpg"
            alt="man injures back lifting heavy box."
            className="imgright-fixed"
          />
          <p>
            <strong>
              {" "}
              For Immediate Help with a Job Injury Case Call 949-203-3814
            </strong>
          </p>
          <p>
            Every workplace injury should be carefully and thoroughly evaluated
            for a potential third party claim. The reputed California job injury
            attorneys at Bisnar Chase have an outstanding track record for
            finding and pursuing third party liability claims.
          </p>
          <p>
            We understand the serious repercussions of job-related injuries -
            that they can impose severe emotional and financial burdens on
            workers and their families. We work diligently to help you obtain
            maximum compensation for your significant losses.
          </p>
          <h2>Job Injury</h2>
          <p>
            Accidents and injuries are common in any workplace. They are more
            widely prevalent in some industries such as construction and
            manufacturing where workers tend to operate heavy machinery and
            perform their jobs in a highly volatile environment. However, it is
            the employer's responsibility to ensure that workers have a safe
            environment to do their jobs. The U.S. Occupational Safety and
            Health Administration (OSHA) lists safety measures for each industry
            to help prevent tragic or{" "}
            <Link to="/catastrophic-injury">catastrophic accidents </Link> in
            the workplace. A number of job-related accidents occur as a result
            of safety violations in the workplace. When workers are injured on
            the job, there are ways and means in which they can seek
            compensation for their significant losses.
          </p>
        </div>
        <h2>Common Job Site Fatalities</h2>
        <p>
          <strong>
            According to OSHA the{" "}
            <Link
              to="https://www.osha.gov/oshstats/commonstats.html"
              target="_blank"
            >
              {" "}
              most common job fatalities reported{" "}
            </Link>{" "}
            are related to construction and OSHA has named them the "Fatal
            Four". We have listed them from highest to lowest percent:
          </strong>
        </p>
        <ul>
          <li>35%- Falls is the leading type of construction fatality.</li>
          <li>10% of fatalities are from an object falling on a worker.</li>
          <li>Electrocutions make up 9% of fatalities.</li>
          <li>
            2% of workers on construction sites die or are seriously injured by
            being caught in between objects.
          </li>
        </ul>
        <p>
          Even though only 9% of job site fatalities are from electrocution, one
          person per day is electrocuted on the job leading to serious injury.
          OSHA has provided a "
          <Link
            to="https://www.osha.gov/dte/grant_materials/fy07/sh-16586-07/4_electrical_safety_participant_guide.pdf"
            target="_blank"
          >
            Electrical Safety Participant Guide
          </Link>
          " for construction safety and health.
        </p>
        <p>
          OSHA says that 419 workers lives would be saved each year in U.S. with
          stricter safety measures implemented.
        </p>
        <p>
          Since 1992 job site fatalities have declined. Unfortunately even one
          death or permanent injury on a job site is devastating. It has a
          direct impact both emotionally and financially on the worker's entire
          family.
        </p>
        <h2>Steps to Take Following an On-the-Job Injury</h2>
        <p>
          If you have suffered an injury on the job, you may be completely
          overwhelmed. However, it is important that you or a family member
          complete several procedural requirements in order to ensure that your
          rights are protected and that you receive the compensation to which
          you are entitled.
        </p>
        <ul>
          <li>
            Report the accident right away. Regardless of the nature of the
            injury or illness, your supervisor should be notified immediately.
          </li>
          <li>
            Fill out an Employee's Claim for Workers' Compensation Benefits and
            request your supervisor fill out the "employer" section.
          </li>
          <li>
            Discuss with your supervisor if you are able to see your own doctor
            or if there is a recommended hospital you must visit. Depending on
            the insurance plan your employer has, you may have to receive a
            second opinion from one of their approved doctors if you see your
            own doctor initially.
          </li>
          <li>
            Accurately describe to the medical professional exactly how the
            accident occurred and which parts of your body have been affected or
            hurt. Tell them what you can or cannot do as the result of the
            injuries.
          </li>
          <li>
            Attend all required medical appointments, keep copies of your
            medical slips, notes and bills and obtain copies of the notes your
            doctor writes excusing you from work.
          </li>
          <li>
            Keep copies of all documents from your employer and the insurance
            carrier regarding your injury.
          </li>
          <li>
            Keep up-to-date records regarding the number of workdays you have
            missed, the dates of your medical appointments, the mileage you have
            traveled for treatment and all of the medical costs you have
            incurred.
          </li>
          <li>
            Keep a journal of the challenges you are experiencing on a regular
            basis since the accident.
          </li>
          <li>
            Discuss your legal options with an experienced California workers'
            compensation attorney before accepting a settlement from an
            insurance provider.
          </li>
        </ul>
        <h2>Workers' Compensation Disputes</h2>
        <p>
          Unfortunately, many workers have their claims denied and are forced to
          return to work before they have fully recovered from their injuries.
          There are a number of reasons why a claim could be denied. Insurance
          providers will often look for any excuse to save money. Even
          individuals who clearly suffered an injury while conducting business
          can have their claim denied if they make mistakes while filling out
          the forms.
        </p>
        <p>
          Many victims of California job injuries are not aware of their legal
          rights and options and fail to get the support they need. You are
          allowed to refuse an inadequate settlement and you can appeal the
          decision of an insurance provider, if it is unsatisfactory to you.
        </p>
        <h2>Third Party Claim</h2>
        <p>
          A workers' compensation claim is not the only route by which injured
          workers can seek damages. Very often, be it a construction accident or
          an industrial mishap, we see that there are many other parties
          involved. For example, the injured worker may be a contractor or a
          sub-contractor who may have been injured as the result of a dangerous
          condition on the property. In such cases, the property owner can be
          held accountable as well. In some cases, a worker may get seriously
          injured or killed as the result of a defective product such as a
          malfunctioning piece of machinery or a faulty part. In such a
          situation, the manufacturer of that piece of machinery or defective
          part can also be held liable.
        </p>
        <p>
          In such cases, the worker or his family will need to file what is
          known as a "third-party claim," which is a personal injury or wrongful
          death claim filed against a party other than an employer. This claim
          can be filed in addition to the workers' compensation claim.
          Third-party claims are often worth many times more than the standard
          workers' compensation claim, which is why they can prove invaluable to
          injured workers and their families.
        </p>
        <p>
          <strong>
            {" "}
            For Immediate Help with a Job Injury Case Call 949-203-3814
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
