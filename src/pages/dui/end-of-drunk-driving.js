// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="End of Drunk Driving"
        pageDescription="In association with MADD (Mothers Against Drunk Driving),we can eradicate drunk driving once and for all. Here's how to help."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The End of Drunk Driving</h1>
        <BreadCrumbs location={location} />
        <p>Can we end drunk driving? We're halfway there.</p>
        <p>
          Since the inception of MADD (Mothers Against Drunk Driving), we have
          seen drunk driving fatalities dwindle, and now we are left to wonder
          if we can eradicate it once and for all. 30 years ago, the mere
          concept of an end to drunk driving would have warranted nervous
          laughter; it was such a frightening epidemic, it didn't seem possible
          to conquer. These days, we have come to a more enlightened state; we,
          as individuals, have the power to save lives from drunk drivers.
        </p>
        <p>
          You can join MADD in their fight against drunk driving with a
          donnation of any amount. Visit their website to Donate to{" "}
          <Link to="https://www.madd.org/" target="_blank">
            MADD
          </Link>{" "}
          and become part of the solution to drunk driving.
        </p>
        <h2>Great News</h2>
        <p>
          New data from the National Highway Traffic Safety Administration
          indicates that drunk driving fatalities dropped to 10,839 deaths in
          2009, a decrease of 7 percent. This decline means that deaths have
          been cut by about half from when MADD was founded 30 years ago and by
          almost 20% from when the Campaign to Eliminate Drunk Driving started
          just four years ago. Those efforts have helped save almost 300,000
          lives.
        </p>
        <p>
          Although drastic steps have been taken to combat drunk driving, we are
          still left to reflect on the 10,839 deaths from 2009. The death toll
          alone is tragic, but the effect left on parents, children and friends
          of the drunk driving victims are equally horrific. Loved ones are left
          to wonder why they have to mourn their mother, brother, coworker or
          friend. The callous disregard of one intoxicated person flips worlds
          upside down and changes people, with some never recovering.
        </p>
        <p>
          Now, more than ever, the public is stricken with the knowledge that
          they can save lives. Students go to parties with the sole purpose of
          giving drunkards a ride home. Friends refuse to let others drive if
          they know they've had too much. We owe this to MADD.
        </p>
        <h2>Statement from MADD National President Laura Dean-Mooney</h2>
        <p>
          "While encouraged by these recent numbers, MADD knows there is still a
          lot of work to be done on Capitol Hill and in our communities because
          drunk driving fatalities make up a larger portion of traffic
          fatalities than in 2008. With drunk driving now 32 percent of all
          highway fatalities, MADD) remains as committed as ever to eliminating
          drunk driving through support for the heroes on our highways, laws
          requiring convicted{" "}
          <Link to="/dui/victims-rights">drunk drivers</Link> to blow before
          they go with ignition interlocks and efforts to turn cars into the
          cure through smart car technology. Additional plans will be shared
          during the celebration of MADD's 30th anniversary, September 23, 2010
          on Capitol Hill."
        </p>
        <h2>Taking Action</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys has taken action to fight drunk
          driving. They have helped raise several thousand dollars through the
          Walk Like MADD campaign and created a hit-and-run reward program. If
          you have been injured in an accident involving a drunk driver, you
          need an experienced DUI Accident Victim Lawyer. Our attorneys at
          Bisnar Chase Personal Injury Attorneys are here to answer any
          questions you may have.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/dui/drunk-driving-murder">
              The DUI Crash: Is it Murder on the Highway?
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dui/drunk-driving-handbook">
              Personal Rights Handbook for the Victims of Drunk Driving
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dui">DUI Accident Victim Lawyer</Link>
          </li>
          <li>
            {" "}
            <Link to="/dui/victims-rights">DUI Victims' Rights</Link>
          </li>
          <li>
            {" "}
            <Link to="/dui/recovering-personal-damages">
              Recovering Personal Damages From a DUI Accident
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
