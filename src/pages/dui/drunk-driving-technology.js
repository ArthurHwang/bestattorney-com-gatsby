// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Technology Keeps Drunk Drivers Off of Streets"
        pageDescription="A Massachusetts company is developing a new technology that will help prevent drunk drivers from driving on the roads."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>New Technology to Keep Drunk Drivers Off the Road</h1>
        <BreadCrumbs location={location} />
        <h2>
          A Massachusetts company is developing a new technology that will help
          prevent drunk drivers from driving on the roads.
        </h2>

        <img
          src="/images/hit-and-run-accident.jpg"
          alt="New Technology to Keep Drunk Drivers Off the Road"
          className="imgright-fluid"
        />
        <p>
          A company near Boston is developing new technology, which many hope,
          could help stop drunk driving altogether and save thousands of lives
          every year. According to a Jan. 2{" "}
          <Link to="http://www.cbsnews.com/8301-505263_162-57561558/new-technology-aims-to-stall-drunk-drivers/">
            {" "}
            CBS news report
          </Link>
          , the $10-million project is supported both by the National Highway
          Traffic Safety Administration (NHTSA) and 16 major automakers who are
          splitting the funding. The company has narrowed the project to two
          technologies, one that is breath-based and another that is
          touch-based, but the goal is to prevent the car from moving if the
          person behind the wheel is impaired, the article states.
        </p>
        <h2>Drunk Driving Video</h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/c1ttUZVNSCQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          In the touch-based approach, a sensor that is embedded in the car's
          start/stop button, sends an infrared light to the fingertip thereby
          measuring the tissue's alcohol content. In the breath-based approach,
          a sensor that is mounted near the steering wheel could test the
          driver's breath, the article explains. In as much as half a second,
          the sensor determined whether the driver's blood alcohol level is
          above 0.08 percent, which is the national legal limit, the report
          states. The technology, which is being opposed by the American
          Beverage Institute, a trade group representing 8,000 U.S. chain
          restaurants, could be ready by the end of this decade, the article
          reports.
        </p>
        <p>
          'Any technology that is effective in preventing more DUI deaths should
          be given serious consideration', said John Bisnar, founder of the
          Bisnar Chase personal injury law firm. "This is a great opportunity
          for us to prevent thousands of people from dying each year. If this
          technology can do what seatbelts did, that would be truly remarkable."
        </p>
        <p>
          While some are concerned that targeting all Americans with
          alcohol-sensing technology may prevent people from even having a glass
          of wine at dinner or a beer at a ballgame, it is important to look at
          the bigger picture here, Bisnar says. "In many tragic cases where we
          see people{" "}
          <Link to="/dui/drunk-driving-murder">
            {" "}
            catastrophically injured or killed from a DUI accident
          </Link>
          , we often see that the drivers are drunk way over the legal limit.
          These new technologies will come too late for families that have lost
          loved ones or for those who are caring for seriously injured or
          disabled family members. But, hopefully, the new technology will help
          prevent heartbreaking tragedies in the future."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The <Link to="/dui">California DUI victim attorneys</Link> of
          represent victims of auto accidents, defective products, dangerous
          roadways, and many other serious personal injuries. The firm has been
          featured on a number of popular media outlets including Newsweek, Fox,
          NBC, and ABC and is known for its passionate pursuit of results for
          their clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>
        <p>
          Sources:
          http://www.cbsnews.com/8301-505263_162-57561558/new-technology-aims-to-stall-drunk-drivers/
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
