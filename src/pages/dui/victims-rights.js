// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/victims-rights-california-dui-accidents-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California DUI Victims Rights - Rights to Compensation"
        pageDescription="DUI victims have the right to be compensated for their injuries and loss. As California DUI accident victim attorneys, we protect your rights and help you get compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California DUI Victims' Rights</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california DUI Victims' Rights"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Drunk driving crashes are often classified as "car accidents." But
          it's important to remember that there is nothing "accidental" about
          why they happened. These are collisions that would have been prevented
          had the driver involved better used his or her judgment and not driven
          while impaired. According to the National Highway Traffic Safety
          Administration (NHTSA), there are about 10,000 people who die every
          year in the United States as a result of{" "}
          <Link
            to="https://www.nhtsa.gov/search?keywords=Drunk%20Driving"
            target="_blank"
          >
            {" "}
            drunk and drugged driving
          </Link>
          .  Even when these crashes don't result in fatalities, they tend to
          cause catastrophic injuries that may leave individuals disabled for
          the rest of their lives.
        </p>
        <p>
          Those who have been injured may have trouble performing daily tasks or
          may need round-the-clock nursing and medical care for the remainder of
          their lives. In addition to suffering physical and emotional trauma,
          these DUI victims may be faced with mounting medical and therapy costs
          as well.
        </p>
        <p>
          If you have been injured in a DUI crash, you have the right to seek
          and obtain compensation for your injuries and losses.
          <strong>
            {" "}
            Immediately contact an experienced{" "}
            <Link to="/dui">DUI victim lawyer</Link> who will fight to protect
            your rights every step of the way.
          </strong>
        </p>
        <h2>DUI Victims' Bill of Rights</h2>
        <p>
          In California,{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Marsy%27s_Law"
            target="_blank"
          >
            Marsy's Law
          </Link>{" "}
          significantly expands the rights of victims including those who have
          been injured in DUI collisions. Under Marsy's Law, the California
          Constitution article I, § 28, section (b), victims have the following
          rights:
        </p>
        <ul>
          <li>
            To be treated fairly and with respect of their privacy and dignity
            and to be free from intimidation, harassment and abuse through the
            criminal or juvenile justice process.
          </li>
          <li>
            To be reasonably protected from the defendant and those acting on
            behalf of the defendant.
          </li>
          <li>
            To have the safety of the victim and victim's family considered when
            it comes to fixing the amount of bail and release conditions of the
            defendant.
          </li>
          <li>
            To ensure that confidential information or records are not disclosed
            to the defendant, his or her attorney or anyone who is acting on
            behalf of the defendant.
          </li>
          <li>
            To refuse an interview, deposition, or discovery request by the
            defendant, the defendant's attorney, or any other person acting on
            behalf of the defendant, and to set reasonable conditions on the
            conduct of any such interview to which the victim consents.
          </li>
          <li>
            To be given reasonable notice of all public proceedings, including
            delinquency proceedings, upon request, at which the defendant and
            the prosecutor are entitled to be present and of all parole or other
            post-conviction release proceedings, and to be present at all such
            proceedings.
          </li>
          <li>
            To be heard, upon request, at any proceeding, including any
            delinquency proceeding, involving a post-arrest release decision,
            plea, sentencing, post-conviction release decision, or any
            proceeding in which a right of the victim is at issue.
          </li>
          <li>
            To a speedy trial and a prompt and final conclusion of the case and
            any related post-judgment proceedings.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="California DUI Victim compensation"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dui-victims-rights-compensation-restitution.jpg')"
            }}
          >
            <h2>The Right to Receive Restitution</h2>
          </div>
        </LazyLoad>

        <p>
          All persons who suffer losses as a result of criminal actions have the
          <strong>
            {" "}
            right under California law to seek and obtain restitution
          </strong>{" "}
          from those convicted of the crimes causing their losses. The court
          orders the convicted wrongdoer to pay restitution regardless of the
          sentence or disposition imposed in which a crime victim suffers a
          loss. The law also requires that all monetary payments and/or property
          collected from any person who has been ordered to make restitution is
          first applied to pay the amounts ordered as restitution to the victim.
        </p>
        <h2>DUI Victims' Right to Compensation</h2>
        <p>
          In DUI cases, victims often assume their only recourse is to file a
          claim against the drunk driver's insurance company to obtain
          compensation for their damages and losses. However, in many cases,
          this may not be possible if the person who caused the crash while
          driving under the influence does not have insurance. Sometimes, these
          individuals don't even have a valid license. And often, this is
          because the driver's license was suspended or revoked because of a
          prior drunk driving crash or conviction.
        </p>
        <p>
          If you have uninsured motorist coverage,{" "}
          <strong>
            you may be able to seek compensation from your own auto insurance
            company
          </strong>
          . In addition, depending on the circumstances of the incident, you may
          be able to seek compensation from the{" "}
          <Link to="/los-angeles/overserving-alcohol-injuries" target="_blank">
            {" "}
            establishment that sold alcohol to the driver
          </Link>
          . However, those types of options are limited in California since the
          state has a number of restrictions on dram shop laws.
        </p>
        <p>
          <strong>As a DUI collision victim you have other options:</strong> You
          may be able to file a lawsuit against the driver's employer if he or
          she was on the job at the time of the crash or if they were driving
          from an employer-sponsored event such as a holiday party or some other
          event. Since the laws in this area are complex and challenging to
          comprehend, it is crucial that you contact an experienced California
          DUI victim lawyer who can help determine fault and liability in these
          cases, and secure maximum compensation for all your losses.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/ZXHmbw3ongk"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Staying on Top of the DUI Investigation</h2>
        <p>
          You may have a strong DUI personal injury or wrongful death claim if
          the at-fault driver received a citation or was arrested on suspicion
          of driving under the influence after a police investigation.{" "}
          <strong>
            This puts you in the best place to negotiate the claim
          </strong>
          . There are also several other steps you would be well advised to take
          in order to protect your legal rights.
        </p>
        <p>
          First and foremost,{" "}
          <strong>get yourself to a hospital or emergency room</strong> so you
          can receive prompt treatment and care. If possible, try to remain at
          the scene of the crash and gather evidence that will help your claim.
          If you smell alcohol or marijuana on the driver's breath, be sure to
          tell the investigating officer.
        </p>
        <p>
          <strong>Make sure you file a police report</strong> and get a signed
          copy of that report for your own records. Also, ask the officer for
          his or her name and badge number. Try to get as much evidence as
          possible from the crash site including photographs and contact
          information for eyewitnesses. If you are injured and need to get to
          the hospital, ask a family member of friend to collect these crucial
          pieces of evidence from the accident scene.
        </p>
        <p>
          It is also <strong>critical that you follow up on the case</strong> to
          see if the driver was charged and if the case went to court. If so,
          find out if the driver pleaded guilty or was convicted and sentenced.
          Your personal injury lawyer can help you gather all the court records
          showing the disposition of the case. This can help bolster your civil
          personal injury or wrongful death claim, particularly if the driver
          was charged and/or convicted of the crime and sentenced.
        </p>
        <h2>Blood Alcohol Content and DUI Claims</h2>
        <p>
          One of the main pieces of evidence that is needed to charge a driver
          with driving under the influence in California is his or her blood
          alcohol concentration or BAC. This is a number that shows how much
          alcohol is present in the driver's bloodstream. The{" "}
          <Link
            to="http://mcwell.nd.edu/your-well-being/physical-well-being/alcohol/blood-alcohol-concentration/"
            target="_blank"
          >
            BAC
          </Link>{" "}
          is typically determined through chemical tests of blood, breath or
          urine. A breath test is usually conducted roadside with a portable
          instrument called the Breathalyzer. A blood or urine test is done at a
          police station or hospital.
        </p>
        <p>
          If you or a loved one has been injured in a DUI crash, it is important
          to be aware of how the driver's BAC might affect your personal injury
          or wrongful death claim. Such vital evidence can be used by your{" "}
          <Link to="/">California personal injury lawyer</Link> to consolidate
          your claim and prove that the other driver was negligent and reckless
          by choosing to drive while under the influence.
        </p>
        <p>
          It is even better for your case when you have an actual BAC level that
          shows how drunk the driver was. For example, if the driver had a BAC
          of 0.17, you could essentially show that the driver's blood alcohol
          level at the time of the crash was more than twice the legal limit of
          0.08.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/blood-alcohol-level-bac-california-dui.jpg"
            width="100%"
            alt="california dui victims' rights blood alcohol content"
          />
        </LazyLoad>

        <h2>The Right to Personal Damages</h2>
        <p>
          If you have been injured in a DUI crash, there are a number of damages
          and losses you may have suffered. The driver will most likely face
          criminal charges. Driving under the influence of alcohol and/or drugs
          is illegal under California Vehicle Code Section 23152 (a).
        </p>
        <p>
          According to California Vehicle Code Section 23136: "It is unlawful
          for a person under the age of 21 years who has a blood-alcohol
          concentration of 0.01 percent or greater, as measured by a preliminary
          alcohol screening test or other chemical test, to drive a vehicle." A
          driver whose act of driving under the influence results in the death
          of another will likely face vehicular manslaughter charges under
          California Penal Code section 191.5 (a).
        </p>
        <p>
          In addition to facing criminal charges and penalties such as jail
          time, probation and hefty fines, DUI drivers can be held financially
          responsible for the injuries, damages and losses they cause. While
          criminal prosecution might punish the drunk driver, it does not
          provide plaintiffs much in terms of financial compensation. DUI
          victims and their families typically suffer from significant financial
          losses because they are unable to return to their jobs while facing
          mounting medical expenses and therapy costs.
        </p>
        <p>
          <strong>
            Here are some of the damages DUI victims have the right to claim:
          </strong>
        </p>
        <p>
          <strong>Medical expenses:</strong> This tends to be a significant
          portion of the damages sustained by DUI victims and could include
          everything from emergency transportation costs, hospitalization fees
          and cost of surgery. It also includes other recurring expenses such as
          doctor visits, diagnostic tests such as X-Rays and MRIs, cost of
          medications and medical equipment.
        </p>
        <p>
          <strong>Rehabilitation costs:</strong> Apart from hospital stays and
          other medical expenses, expenditures relating to rehabilitation can
          add up very quickly. Rehabilitation may include everything from
          physical therapy and chiropractic care to occupational therapy and
          psychological counseling. These are services patients may need to
          regain some of the physical strength and flexibility they had prior to
          the accident and also to regain emotional stability.
        </p>
        <p>
          <strong>Loss of income:</strong> When you take time off work to
          recover from your injuries, you may end up losing a significant amount
          of pay. While you may be able to get a portion of your income in
          disability benefits, you may still lose a substantial amount of money,
          particularly if you are self-employed or do temporary or
          contract-based projects. In addition to the income you lose while
          recovering from your injuries, you may also suffer future losses. For
          example, some people may lose their jobs because of extended time away
          from work. Others may not be able to perform the jobs they used to
          prior to the accident. For example, if you have suffered an
          amputation, you may never be able to return to the construction job
          you had before the accident. So, anyone who faces diminished income,
          loss of career or livelihood may be entitled to additional damages.
        </p>
        <p>
          <strong>Permanent injuries and disabilities:</strong> Car accidents
          have the potential to cause permanent injuries, scarring,
          disfigurement and lifelong disabilities. In addition to having a
          physical impact on DUI victims, such permanent injuries and
          disabilities may have severe emotional and financial impacts.
          Individuals are likely to become depressed as a result of not being
          able to return to work. Their family members might suffer financially
          if the injured victim was the sole breadwinner or wage earner.
        </p>
        <p>
          <strong>Pain and suffering:</strong> DUI victims can also collect
          damages for the physical pain and mental anguish they endure in the
          aftermath of a DUI crash. Our California personal injury lawyers
          advise victims, regardless of the circumstances of their injuries, to
          maintain a journal after the accident describing in detail their
          physical pain, emotions and activities they can or cannot do. This
          might include playing with kids, cleaning the house, doing gardening
          work or playing sports, etc. Such a journal helps give a detailed
          account of how the injuries have affected your everyday activities and
          quality of life.
        </p>
        <p>
          <strong>Wrongful death:</strong> Those who have lost loved ones as the
          result of a DUI crash can file a wrongful death claim against the
          at-fault parties seeking compensation for damages including medical
          expenses, funeral costs, lost future income, pain and suffering and
          loss of love and companionship.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california victims rights lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/california-dui-victims-rights-lawyers.jpg')"
            }}
          >
            <h2>Contacting an Experienced Lawyer</h2>
          </div>
        </LazyLoad>

        <p>
          The experienced California personal injury lawyers at Bisnar Chase
          have a long and successful track record of protecting the rights of
          victims of impaired or negligent drivers. We don't collect any fees
          from clients until we have secured compensation for them. If you have
          been injured in a DUI crash or if you have lost a loved one in a drunk
          driving car accident, please
          <strong> call us at (800) 561-4887</strong> to find out how we can
          help you.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
