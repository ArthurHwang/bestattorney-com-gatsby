// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-motorcycle-accident-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Motorcycle Accident Lawyers - Experienced Attorneys 949-203-3814"
        pageDescription="Irvine Motorcycle Accident Lawyers at Bisnar Chase has over 40 years of experience, has won over $500 Million and have established a 96% success rate. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine motorcycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> Irvine Motorcycle Accident Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> have been representing and winning
          motorcycle accident cases for <strong> over 40 years.</strong> With
          over <strong> $500 million won</strong>, our impressive{" "}
          <strong> 96% success rate</strong> stands strong as a solid credential
          for our ability to win you maximum compensation.
        </p>
        <p>
          It is common for motorcyclists to suffer serious and even
          life-changing injuries in a collision with another vehicle. In such
          cases, the victims may struggle to pay their mounting medical bills
          while being unable to earn wages because of their new physical
          limitations.
        </p>
        <p>
          Anyone who has suffered an injury that is classified as catastrophic
          would be well advised to contact an experienced{" "}
          <Link to="/irvine" target="new">
            {" "}
            Irvine Personal Injury Lawyer
          </Link>{" "}
          who can help him or her pursue the financial compensation they need
          and rightfully deserve.
        </p>
        <p>
          If you or a loved one has been injured in a motorcycle accident, the
          experienced Irvine Motorcycle Accident Lawyers at Bisnar Chase can
          help you better understand your legal rights and options.
        </p>
        <p>
          We always offer free consultations to injured victims and their
          families and work on a contingency basis. This means that we do not
          get paid until you recover compensation for your losses.
        </p>
        <p>
          If you would like to get an honest and comprehensive evaluation of
          your case, please <strong> call 949-203-3814</strong> to schedule your
          free consultation and case evaluation.
        </p>
        <h2>Minor Motorcycle Injuries in Orange County</h2>
        <p>
          In some accidents, an injured motorcyclist may get off with only minor
          bumps and bruises. Other minor injuries may include scraps and cuts
          resulting from the victim sliding on the roadway.
        </p>
        <p>
          Victims of these types of injuries are in fact lucky, possibly because
          the accident was not violent or intense or because they were well
          protected with a helmet and other riding gear.
        </p>
        <p>
          Unfortunately, it is common for motorcyclists to suffer much more
          serious injuries.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/rRijyD6aP-E"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Orange County Motorcycle Accident Statistics</h2>

        <p>
          Ownin a riding a motorcycle can definitely help cut down your commute
          time, especially if there is a considerable amount of traffic on your
          way to work or place of business. Even school, recreational activities
          and other trips you make on public or private roadways can pose a
          massive risk, compared to driving a car.
        </p>
        <p>
          Why is driving a motorcycle more dangerous than driving a car? Well,
          for starters, when driving a car, you are (by law) wearing a seatbelt,
          which will keep you from flying into or through your windshield or
          other open window in the event of a car accident.
        </p>
        <p>
          You also have the security of side, above and below airbags, the
          strength of the car doors and surrounding parts of the interior.
        </p>
        <p>
          On a motorcycle, it's just you and the road, and other extremely heavy
          and dangerous vehicles sharing the road with you. While riding a
          motorcycle, you don't have the securities or protection of a car,
          which is why motorcycle accident statistics are very high, especially
          in place like Irvine, CA, in Orange County.
        </p>
        <p>In a recent years study:</p>
        <ul>
          <li>Almost 500 people were killed in motorcycle accidents</li>
          <li>
            24% of these motorcycle accident victims had a Blood Alcohol
            Concentration of 0.08% or higher
          </li>
          <li>
            40% of these fatalities happened on bikes with an engine size of
            501-1,000cc,
          </li>
          <li>
            90% of these fatalities were in California wearing a helmet,
            compared to 57% nationally
          </li>
        </ul>
        <p>
          According to{" "}
          <Link to="https://www.dmv.ca.gov/portal/dmv" target="new">
            DMV.ca.gov
          </Link>
          :
        </p>
        <ul>
          <li>
            As of January 1st, 2018, there were motorcycles registered in the
            state of California
          </li>
          <li>
            9.3% increase in motorcyle collisions between recent year's studies
          </li>
        </ul>
        <h2>Moderate and Serious Motorcycle Injuries in Irvine, California</h2>
        <p>
          Victims of Irvine motorcycles accidents commonly suffer moderate or
          serious injuries including broken bones and deep lacerations. Others
          sustain internal injuries that require immediate medical attention.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.cdc.gov/" target="new">
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>{" "}
          :
        </p>
        <ul>
          <li>
            Approximately 30 percent of all injury accidents involving
            motorcycles result in trauma to the rider's legs or feet.
          </li>
          <li>
            These types of injuries can take a long time to heal and, in many
            cases, the victim may never fully regain their strength or mobility.
          </li>
          <li>
            Sadly, the CDC also reports that about 22 percent of motorcycle
            injuries involve head or neck trauma, which is often even more
            serious than broken bone injuries.
          </li>
        </ul>
        <h2>When Motorcycle Injuries are Catastrophic</h2>
        <p>
          A catastrophic injury results in a decrease in the victim's quality of
          life. They may have lost certain physical abilities or suffered a
          disability from which they may never fully recover.
        </p>
        <p>
          Many victims of injuries that are considered catastrophic may lose a
          substantial portion of their earning ability or they may even lose
          their ability to work completely.
        </p>
        <p>
          Injuries classified as catastrophic often involve a permanent
          disability such as an amputation, a devastating back injury that
          includes damage to the spinal cord or a head injury.
        </p>
        <p>
          While victims of spinal cord injuries may lose physical ability or
          feeling below the site of the trauma, head injury victims may also
          experience a loss in physical and mental abilities.
        </p>
        <p>
          In such cases, it is important that the victim, or the victim's
          family, contact an experienced Irvine motorcycle accident lawyer who
          will help protect their legal rights and best interests.
        </p>
        <h2>Seeking Compensation for Your Irvine Motorcycle Accident</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/brian-chase-motorcycle-attorney.jpg"
            width="38%"
            className="imgleft-fluid"
            alt="irvine motorcycle accident lawyer"
          />
        </LazyLoad>

        <p>
          It is crucial that anyone suffering from a serious injury or
          disability contact a skilled Irvine motorcycle accident lawyer who can
          help determine liability and hold the at-fault party accountable for
          the crash.
        </p>
        <p>
          Victims of catastrophic injuries are often offered inadequate
          settlements that may cover a portion of their losses, but fail to
          cover future losses.
        </p>
        <p>
          A knowledgeable personal injury lawyer will consider all past and
          future medical bills, all lost wages and loss of earning capacity, the
          costs associated with rehabilitation services, surgical procedures and
          non-economic losses such as physical pain and mental anguish.
        </p>
        <p>
          It is important to work with an attorney because acceptance of an
          inadequate settlement could result in the victim being unable to
          pursue future compensation for the accident.
        </p>
        <p>
          If you or a loved one has been involved in a motorcycle accident, call
          the <strong> Irvine Mototrcycle Accident Lawyers</strong> of{" "}
          <strong> Bisnar Chase.</strong>
        </p>
        <p>
          With over <strong> 40 years of experience</strong>, over{" "}
          <strong> $500 Million won</strong> and an impressive{" "}
          <strong> 96% success rate</strong> established, we will win you
          <strong> maximum compensation</strong> or{" "}
          <strong> you do not pay</strong>.
        </p>
        <p>
          Call 949-203-3814 for your Free consultation and case evaluation with
          a skilled member or our legal team.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
