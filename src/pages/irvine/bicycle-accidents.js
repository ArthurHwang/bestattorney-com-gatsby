// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-bicycle-accident-law-firm-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Bicycle Accident Attorneys - Bike Crash Lawyers"
        pageDescription="Experiencing a bike crash with a vehicle or pedestrian can be traumatizing and exhausting. The Irvine Bicycle Accident Lawyers of Bisnar Chase understand how to fight and win these cases, or you do not pay anything. Call 949-203-3814 for your Free consultation and case evaluation with our skilled team of attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine bicycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong> Irvine Bicycle Accident Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> understand how devastating bicycle
          accident, pedestrian accidents and car accidents can be, that is why
          our legal team has dedicated so much of our lives to helping victims
          of these unfortunate and truly horrible situations.
        </p>
        <p>
          The city of Irvine provides a network of bike paths and bike trails
          specifically to encourage the use of bicycles as a safe and convenient
          means of transportation, both for commuting and recreational purposes.
          Irvine has about 300 miles of bike lanes and nearly 62 miles of
          off-road bike paths, making it probably one of the most well connected
          bicycling cities in Orange County and perhaps, in Southern California.
          In fact, the city was awarded a{" "}
          <Link to="https://www.bikeleague.org/community" target="new">
            Silver Level Bicycle Friendly Community
          </Link>{" "}
          by the{" "}
          <Link to="https://www.bikeleague.org/" target="new">
            {" "}
            League of American Bicyclists
          </Link>
          .
        </p>
        <p>
          While this is all great news for bicyclists who live and work in
          Irvine or visit the area for recreational bicycling, it is important
          to remember that bicycle safety is still a major issue here. Each year
          people are killed and injured in Irvine and in surrounding communities
          as a result of bicycle accidents. If you or a loved one has been
          injured in a bicycle accident or if you have lost a loved one as the
          result of a tragic crash, the experienced Irvine bicycle accident
          attorneys at{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          can help you better understand your legal rights and options.
        </p>
        <p>
          If you have been involved in a <strong> bicycle accident</strong> and
          need to speak with an{" "}
          <Link to="/irvine" target="new">
            Irvine Personal Injury Lawyer
          </Link>{" "}
          about your options and legal advice, call us to receive your{" "}
          <strong> Free consultation</strong> and{" "}
          <strong> case evaluation</strong> at <strong> 949-203-3814</strong>.
        </p>
        This page will cover the following:
        <div className="mb">
          {" "}
          <Link to="/irvine/bicycle-accidents#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Do Bicycle Accidents Occur? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Injured in a California Bicycle Accident?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Bicycle Accident Victim B|C Client Testimony Video{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What You Should Not Do During Your Bicycle Accident Case{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Who Can Be Held Liable in a Bike Accident?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Damages and Compensation in an Irvine Bike Accident{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/irvine/bicycle-accidents#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Contacting an Experienced Irvine Bike Accident Lawyer{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>Why Do Bicycle Accidents Occur?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/irvine-bicycle-and-car-collision.jpg"
            id="biker-down"
            width="100%"
            alt="irvine bicycle accident attorneys"
          />
        </LazyLoad>
        <p>
          There are a number of reasons why bicycle accidents occur in Irvine.
          Here are some of the most common causes:
        </p>
        <p>
          <strong> Impaired driving:</strong> Driving under the influence of
          alcohol and/or drugs puts everyone on the roadway in grave danger.
          Bicyclists are no exception. When a driver is under the influence,
          there is an increased likelihood that he or she might swerve or crash
          into a bicyclist. This could prove catastrophic or fatal for the
          bicyclist.
        </p>
        <p>
          <strong> Failure to yield:</strong> Under California law, bicyclists
          have the same rights and responsibilities as drivers of other
          vehicles. Therefore, the rules of the road, with regard to yielding at
          street intersections, also apply to bicyclists. One of the common
          types of bicycle accidents we see at Irvine intersections is when a
          motorist fails to yield to an oncoming bicyclist. The most common
          excuse is that the driver "did not see the bicyclist coming." This
          tragic scenario often arises from motorists' inattention or their
          failure to consciously look out for smaller vehicles such as bicycles
          at street intersections.
        </p>
        <p>
          <strong> Distracted driving:</strong> Just like drunk or impaired
          driving, distracted drivers can be lethal for bicyclists. Looking at a
          cell phone or electronic device, talking to passengers, eating or
          drinking while driving – are all examples of being distracted while
          driving. Distracted drivers also often rear-end bicyclists because
          they are not paying attention to the roadway.
        </p>
        <p>
          <strong> Driving too close:</strong> Under California law, motorists
          are required to maintain a 3-foot buffer while passing bicyclists.
          Drivers often break this law, and this causes bicyclists to be
          sideswiped or even struck. These types of accidents could result in
          serious injuries for bicyclists.
        </p>
        <p>
          <strong> Bike lane intrusion:</strong> Motorists are also prohibited
          from driving in or intruding into a bike lane. But, often, we see
          distracted, inattentive or impaired drivers swerve into bike lanes,
          causing a horrific crash.
        </p>
        <p>
          <strong> Dooring:</strong> This refers to when a person inside a car
          opens the door of the vehicle striking a passing bicyclist. Dooring
          leads to major injuries or death. It is not unusual for a bicyclist
          who is struck by a vehicle door to get ejected from the bicycle and
          being thrown into the path of another oncoming vehicle. California law
          requires vehicle occupants to use caution before opening the doors and
          exiting.
        </p>
        <p>
          <strong> Bicycle defects:</strong> In some cases, bicycle defects or
          faulty parts could also lead to major injury bike accidents. In such
          cases the product manufacturers or the maker of the defective product
          can be held liable for the injuries and damages caused.
        </p>
        <p>
          <strong> Defective roadways:</strong> There are a number of potential
          roadway defects that could endanger bicyclists. A crack in the
          pavement, uneven pavement or rough surfaces could all make bicyclists
          extremely vulnerable to losing their balance and getting injured.
          Defectively designed roadways where visibility is poor or where is no
          adequate lighting after dark could also put bicyclists in an extremely
          dangerous situation.
        </p>
        <div id="header2"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine bicycle accident lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/injured-in-california-bicycle-accident.jpg')"
            }}
          >
            <h2>Injured in a California Bicycle Accident?</h2>
          </div>
        </LazyLoad>
        <p>
          If you have been injured in a bicycle accident in Irvine, there are a
          number of steps you can take in order to protect and preserve your
          legal rights:
        </p>
        <ul>
          <li>
            First and foremost, make sure you get the medical attention you
            need. Bicyclists are often the more seriously affected in a
            collision because they have very little to protect themselves.
            Accept the medical care you get at the scene from emergency
            personnel. If you are injured, make sure you get to the emergency
            room right away. Even if you "feel fine" after the collision, be
            sure to see the doctor or go to a nearby hospital to make sure you
            haven't suffered any internal injuries. Many crash-related injuries
            don't manifest until a few days after the crash.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.cityofirvine.org/irvine-police-department/i-report-online-reporting"
              target="new"
            >
              File a police report
            </Link>
            . If you have been involved in a collision, police should arrive at
            the scene shortly to investigate. The report will typically contain
            details such as the date, time, location and a brief narrative of
            what occurred. A police report could be a valuable tool, sometimes,
            in terms of determining fault and liability, especially if it
            categorically states who was at fault for the crash. Get a copy of
            this report for your own records.
          </li>
          <li>
            Collect as much evidence from the scene as possible. Since crash
            sites are cleared fairly quickly, you may lose a lot of this if you
            don't get it right away. This means taking pictures on your cell
            phone of details from the crash scene such as your damaged bicycle,
            your injuries, position of the other vehicles, etc. If possible,
            shoot video. Talk to anyone who saw the incident and get their
            contact information and name. Their testimony could be very valuable
            in your claims process. If you can't gather this information due to
            your injuries, ask a loved one or a friend if they could do it for
            you. An Irvine bicycle accident lawyer will also be able to help you
            with this work.
          </li>
          <li>
            Save all paperwork stemming from the incident and your injuries.
            This means every receipt and invoice, records of doctor visits and
            surgery and rehab bills. It is also important that you keep track of
            the number of days you stayed away from work.
          </li>
          <li>
            Contact an experienced Irvine bicycle accident lawyer who will help
            you avoid making mistakes during the claim process that could
            jeopardize your case.
          </li>
        </ul>
        <div id="header3"></div>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <div id="header4"></div>
        <h2>What You Should Not Do During Your Bicycle Accident Case</h2>
        <p>
          What you do in the aftermath of a bicycle accident can also ruin your
          case. This is why it is extremely important that you don't do certain
          things. For example, never admit fault for the accident. You never
          know who was at fault until a thorough investigation has been
          completed. It is better to be patient and get all the facts.
        </p>
        <p>
          Don't sign any paperwork you do not understand. Some accident victims
          end up signing papers and giving access to personal information such
          as financials and medical records. This is a grave mistake because
          insurance companies and defendants' lawyers will use this information
          against you in an attempt to minimize the payout or settlement.
        </p>
        <p>
          Do not rush into a settlement with other parties until you have talked
          to your injury lawyer. Insurance companies don't care about what you
          get. They only care about their own bottom line and protecting their
          interests. You injury lawyer is the only one who will be advocating
          and fighting for your rights and best interests. Also, if you
          prematurely sign a settlement agreement, you will not be able to claim
          compensation for other accident-related expenses that crop up later
          such as medical expenses or rehabilitation costs.
        </p>
        <p>
          Stay away from social media and the Internet until after your bicycle
          accident lawsuit is resolved. Shut down all your accounts so no one,
          including you, has any access. There is no such thing as privacy or
          security on the Internet. If someone is determined enough to get to
          your information, they will. Insurance companies and defense attorneys
          have been known to manipulate information such as comments, photos and
          videos found on social media sites or personal blogs to minimize a
          payout. So, it would be in your best interest not to engage until
          after you have entered into a settlement and received payment.
        </p>
        <div id="header5"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine bicycle accident attorney"
            style={{
              backgroundImage:
                "url('/images/text-header-images/who-can-be-held-liable-bicycle-accident-irvine.jpg')"
            }}
          >
            <h2>Who Can Be Held Liable in a Bike Accident?</h2>
          </div>
        </LazyLoad>
        <p>
          There are several parties that could be held liable in bicycle
          accident cases. Fault and liability depends on the nature and
          circumstances of the incident. Every bicycle accident case is unique.
          Here are some of the parties that could be held liable in a bicycle
          accident case:
        </p>
        <p>
          <strong> Drivers:</strong> Some of the most devastating bicycle
          accidents are those that involve collisions with other vehicles. If
          the driver is determined to be fully or partially responsible for the
          accident, he or she can be held liable. Usually, you would go through
          the driver's insurance company to seek compensation for your losses.
          If the driver is uninsured, you may be able to seek compensation
          through the uninsured motorist clause of your own auto insurance
          policy. This also applies when the driver who hit you does not have
          adequate coverage to compensate you for your losses.
        </p>
        <p>
          <strong> Employers:</strong> In cases where the at-fault driver was on
          the job, his or her employer may also be held liable. In some cases
          the driver operating his or her personal vehicle might be involved. In
          other cases, the person might be driving a company vehicle such as a
          delivery vehicle. As long as the driver was on th e job, the employer
          can also be held liable for your injuries, damages and losses.
        </p>
        <p>
          <strong> Governmental entities:</strong> This could happen in cases
          where bicyclists are injured by dangerous or defective roadways. It
          could also happen when the bicyclist is struck by a government
          employee such as a peace officer or city worker who is on the job. In
          such cases involving governmental entities, personal injury claims
          must be filed within 180 days of the accident or injury.
        </p>
        <p>
          <strong> Product manufacturers:</strong> If your bicycle accident was
          caused by a defective bicycle or bike part, you may be able to file a
          product liability lawsuit against the manufacturer seeking
          compensation for your losses. This is one of the most important
          reasons why you should preserve your bicycle after the accident. This
          way, an expert can thoroughly examine it for defects, malfunctions,
          etc.
        </p>
        <div id="header6"></div>
        <h2>Damages and Compensation in an Irvine Bike Accident</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/bicycle-accident-compensation.jpg"
            id="compensation"
            width="100%"
            alt="irvine bicycle accident law firm"
          />
        </LazyLoad>
        <p>
          Injured bicycle accident victims may be able to seek compensation for
          a number of losses including, but not limited to:
        </p>
        <ul>
          <li>
            Medical expenses: This includes everything from ambulance costs,
            doctor office visits, diagnostic tests and surgeries to medication
            and medical equipment.
          </li>
          <li>
            Lost income: When you are recovering from an injury, you may not be
            able to work for several days or even weeks or months. As part of
            your bicycle accident claim, you may be able to seek compensation
            for income or wages lost. If you have been catastrophically injured
            and cannot return to work, you may also seek compensation for lost
            future income and loss of earning capacity.
          </li>
          <li>
            Rehabilitation costs: Bicyclists often suffer traumatic brain
            injuries, spinal cord trauma, broken bones and other damages that
            require lengthy and extensive rehabilitation therapies. This could
            prove costly because many health insurance policies don't completely
            cover them.
          </li>
          <li>
            Pain and suffering: The physical pain and emotional suffering
            bicycle accident victims endure cannot be minimized. These can be
            claimed as noneconomic damages in a bicycle accident lawsuit.
          </li>
          <li>
            Wrongful death: Bicycle accidents are often fatal. In such cases,
            family members of deceased victims can file a wrongful death lawsuit
            seeking compensation for damages such as medical expenses, funeral
            costs, lost future income, pain and suffering and loss of love and
            companionship.
          </li>
        </ul>
        <div id="header7"></div>
        <h2>Contacting an Experienced Irvine Bike Accident Lawyer</h2>
        <p>
          The <strong> Irvine bicycle accident lawyers</strong> at{" "}
          <strong> Bisnar Chase</strong> have had an excellent track record of
          fighting for the rights of victims and families. We have obtained
          verdicts and settlements for catastrophically injured bicyclists.
          Whether it's a governmental agency or a large corporation, we have
          what it takes to stand up and seek justice for our injured clients.
          Please call us at 949-203-3814 for a free consultation and
          comprehensive case evaluation. &nbsp;&nbsp;
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
