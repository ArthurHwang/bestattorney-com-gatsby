// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/abuse-of-neglect-elderly-what-to-do.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Nursing Home Abuse Lawyers"
        pageDescription="After trying to find the perfect fit for your loved one, the idea of them suffering from nursing home abuse or neglect is hard to believe. The Irvine Nursing Home Abuse and Neglect Lawyers have been offering legal services for cases involving elder abuse for 40 years. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Nursing Home Abuse and Neglect Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine nursing home abuse lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Choosing a nursing home in which to place a loved one can be a
          stressful and challenging process. After struggling to find the right
          place for your loved one, the idea of them suffering from{" "}
          <Link to="/nursing-home-abuse" target="new">
            {" "}
            Irvine nursing home abuse
          </Link>{" "}
          or neglect must seem unfathomable.
        </p>
        <p>
          Unfortunately, these types of incidents do occur and many instances of
          nursing home abuse go unreported for fear of embarrassment or
          retaliation. It is important to understand the critical issues
          surrounding nursing home abuse and neglect so patients and their
          families better understand their legal rights.
        </p>
        <p>
          There are many forms of abuse that may occur at an Irvine nursing
          home. In addition to physical abuse, there is emotional abuse,
          financial fraud, sexual abuse and neglect. All of these terrible forms
          of abuse and neglect can have a devastating affect on victims and
          their families.
        </p>
        <h2>The Signs and Symptoms of Abuse</h2>
        <p>
          Many elderly victims are physically weak and suffering injuries in
          falling accidents is rather common for seniors. It is important to
          understand that it is not common for someone to regularly suffer
          bruises and broken bones while in the care of a specialized nursing
          facility.
        </p>
        <p>
          Signs of physical abuse may include unnatural bruising, bleeding,
          broken bones and changes in personality.
        </p>
        <p>
          Unlike physical abuse, there are very few visual symptoms to look for
          in cases involving emotional abuse. Victims may feel embarrassed by
          the way they are treated and refuse to see visiting family members. In
          some cases, victims of emotional abuse may become easily agitated,
          quiet, withdrawn, depressed or angry.
        </p>

        <h2>Nursing Home Neglect</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/elder-fraud-sign-lawyers.jpg"
            width="40%"
            className="imgleft-fluid"
            alt="elderly financial abuse attorneys"
          />
        </LazyLoad>

        <p>
          Nursing homes are legally required to provide adequate care to their
          residents. This includes paying attention to them and making sure they
          get the nutrition and the medical care they need.
        </p>
        <p>
          They must make sure the right medications are given and that they are
          provided the level of care recommended by a medical professional.
        </p>
        <p>Signs of Irvine nursing home neglect include:</p>
        <ul>
          <li>Malnutrition</li>
          <li>Dehydration</li>
          <li>Bedsores</li>
          <li>Bruises from restraints</li>
          <li>Overmedication or chemical restraint</li>
          <li>General lack of hygiene</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine personal injury lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/Irvine-Nursing-Home-Abuse-and-Neglect-Lawyers.jpg')"
            }}
          >
            <h2>What to do in the Event of Abuse of Neglect</h2>
          </div>
        </LazyLoad>

        <p>
          If your loved one told you that he or she was abused or neglected, the
          first step to take is to determine what occurred and why. You may want
          to request his or her medical records for signs of recent injuries.
        </p>
        <p>
          You may want to speak with nearby residents and see if they have
          concerns as well. If the issue is with a specific staff member or the
          quality of services provided, talk to the administrator first.
        </p>
        <p>
          If the matter is still not rectified, you may want to contact an
          ombudsman or an attorney to take appropriate action against the
          negligent nursing home.
        </p>
        <p>
          In California, you are legally required to report cases of elder abuse
          to the authorities. There are a number of organizations and services
          that should be notified.
        </p>
        <p>
          You should reach out to your county{" "}
          <Link to="http://www.napsa-now.org/" target="new">
            Adult Protective Services (APS) agency
          </Link>
          . You may also want to contact the Licensing and Certification Program
          of the{" "}
          <Link to="http://www.dhcs.ca.gov/Pages/default.aspx" target="new">
            California Department of Health Services (DHS)
          </Link>{" "}
          and the{" "}
          <Link to="https://www.aging.ca.gov/programs/ltcop/" target="new">
            {" "}
            Long-Term Care Ombudsman's Office of the California Department of
            Aging
          </Link>
          . You may even notify the{" "}
          <Link to="https://oag.ca.gov/bmfea" target="new">
            {" "}
            California Attorney General Bureau of Medi-Cal Fraud and Elder Abuse
          </Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/fw_KqWUQAQ4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>How an Attorney Can Help</h2>
        <p>
          Reporting Irvine nursing home abuse is a good way to ensure that a
          care facility is held criminally liable for their actions. It will
          not, however, result in financial compensation for the losses the
          victim or the victim’s family has suffered.
        </p>
        <p>
          The skilled Irvine nursing home abuse and neglect lawyers at Bisnar
          Chase Personal Injury Attorneys can help victims and their families
          determine if they have a case.
        </p>
        <p>
          In cases where neglect or abuse has taken place, punitive damages may
          also be awarded in addition to compensation for medical bills, pain
          and suffering, emotional distress and other related damages.
        </p>
        <p>
          We have more than three decades of experience fighting for the rights
          of injured victims and holding wrongdoers accountable. Please{" "}
          <Link to="/contact"> contact us </Link> to find out how we can help
          you and your family.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
