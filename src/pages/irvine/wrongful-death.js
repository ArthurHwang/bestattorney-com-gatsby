// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "wrongful-death/irvine-wrongful-death.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Wrongful Death Attorneys - Filing a Wrongful Death Claim"
        pageDescription="After the death of a loved one, seeking an experienced Irvine Wrongful Death Lawyer can be daunting. Call 949-203-3814 to speak to one of our top-rated catastrophic injury attorneys who has been serving victims of a wrongful death for 40 years. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Wrongful Death Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Irvine Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <strong>
            {" "}
            Irvine{" "}
            <Link to="/wrongful-death" target="_blank">
              {" "}
              Wrongful Death Lawyers{" "}
            </Link>
          </strong>{" "}
          of Bisnar Chase realize that finding legal representation after the
          death of a loved one can be daunting. Losing a family member due to
          someone's negligence is unjust and devastating.
        </p>
        <p>
          Not only does the family of the deceased inherit grief and despair,
          but they also face the financial burdens that have been left behind.
          The law firm of Bisnar Chase believes that you are entitled to
          compensation for expenses such as medical bills, funeral arrangements
          and future earnings.
        </p>
        <p>
          <strong> For over 40 years, </strong>our Irvine wrongful death
          attorneys
          <strong> have attained hundreds of millions in earnings </strong>for
          the families who have experienced the loss of a loved one due to
          someone's carelessness. Contact the law office of Bisnar Chase today
          to learn more about your legal options.
        </p>
        <p>
          <strong> Call 949-203-3814 for a free consultation</strong>.
        </p>
        <h2>What Does an Irvine Wrongful Death Lawyer Do?</h2>
        <p>
          It is unfortunate, but at times, negligent parties are not held
          responsible for a wrongful death. A wrongful death is characterized as
          a person dying due to an outside party's carelessness. In cases when
          the negligent are brought to the courts, the families are not
          compensated to the fullest at times as well.
        </p>

        <p>
          A wrongful death lawyer will not only bring those negligent parties to
          justice but they will also win you the financial losses that you have
          faced when your family member has passed.{" "}
        </p>
        <LazyLoad>
          <img
            src="/images/wrongful-death/wrongdul-death-sad-woman.jpg"
            width="100%"
            alt="Irvine wrongful death attorneys"
          />
        </LazyLoad>
        <p>
          The California wrongful death statute states that families or heirs to
          the deceased estate can recover damages such as:
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Loss_of_consortium"
              target="_blank"
            >
              {" "}
              Loss of consortium{" "}
            </Link>
          </li>
          <li>Burial costs</li>
          <li>Loss of income </li>
        </ul>
        <p>
          The statue also requires that the wrongful death suit be filed 2 years
          from the death.
        </p>
        <h2>Common Causes of a Wrongful Death in Irvine</h2>
        <p>
          Accidents are the fourth cause of deaths in the United States. Most
          recently, a 14-year-old girl named Ashton Sweet was killed by a drunk
          driver when her vehicle was struck in Irvine. There are many factors
          that can play into a wrongful death as well. Below are the common
          causes of a wrongful death.
        </p>
        <p>
          <strong> Common causes of a wrongful death</strong>:
        </p>
        <ul>
          <li>
            <strong> Medical malpractice</strong>: Contrary to popular belief
            medical malpractice does not solely involve a surgery going awry. It
            can involve a medical professional prescribing the wrong medication
            or even a wrong diagnosis. Each year there are over 15,000 medical
            malpractice suits.
          </li>
          <li>
            <strong> Defective products</strong>: Products that have caused
            irreparable damage to a consumer either physically or emotionally
            can be a basis for a product liability lawsuit. Some defective
            product suits in the past have included defective airbags,{" "}
            <Link
              to="/blog/urgent-warning-on-hoverboards-after-fatal-fire"
              target="_blank"
            >
              {" "}
              hoverboard explosions{" "}
            </Link>{" "}
            and trans-vaginal mesh.
          </li>
          <li>
            <strong> Employment-related accidents</strong>: Over 57% of
            work-related deaths occur in the construction industry. Some of the
            causes can be electrocutions, caught in a dangerous machine, falls
            or hit by an object. If your loved one has passed away on-the-job
            you have the right to file a survivor's claim.
          </li>
          <li>
            <strong> Pedestrian accidents</strong>: Texting and driving is one
            of the recent root causes of car accidents.{" "}
            <Link
              to="http://www.medicaldaily.com/distracted-drivers-cause-pedestrian-deaths-rise-50-texting-talking-phone-or-eating-wheel-264077"
              target="_blank"
            >
              Distracted driving makes up over 50% of pedestrian deaths
            </Link>
            . Victims of distracted driving usually are hit at marked
            crosswalks.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Wrongful death attorneys in Irvine"
            style={{
              backgroundImage:
                "url('/images/wrongful-death/irvine-wrongful-death-text-header-image.jpg')"
            }}
          >
            <h2>3 Elements of a Wrongful Death Suit</h2>
          </div>
        </LazyLoad>
        <p>
          There are certain factors that need to be proven in a wrongful death
          case in order to receive the maximum compensation. Carelessness must
          be demonstrated by either a drunk driver, an employer or medical
          professional. Below are the three main components of a wrongful death
          suit.
        </p>
        <p>
          <strong> 3 elements of a wrongful death suit</strong>:
        </p>
        <ol>
          <li>
            <strong> Causation</strong>: If a person has passed due to the
            action of another then this is grounds for causation. One of the
            easiest ways you can determine causation is using the "but for"
            assessment. "But for the doings of the negligent party, the victim
            would still be alive." For example, if the drunk driver had not gone
            behind the wheel my son would still be alive."
          </li>
          <li>
            <strong> Duty of Care</strong>: The family member of the deceased
            must prove that the negligent party had an obligation to the
            plaintiff. For instance, a car manufacturer had the duty to make
            sure the airbags in a vehicle are functioning well so they may
            protect the driver in an accident.
          </li>
          <li>
            <strong> Breach of duty of care</strong>: Breach of duty falls after
            the duty of care in which the individual who had the obligation,
            failed to perform the duty that they owed to the plaintiff. An
            example of this can be if a doctor does not perform to the best of
            their ability such as giving a patient the proper medication or did
            not follow the appropriate procedure if the client was having a
            heart attack.
          </li>
        </ol>
        <h2>Can Only Family File a Wrongful Death Claim?</h2>
        <p>
          Family members such as spouses or surviving children can file a
          wrongful death claim. People who were close to the deceased can also
          inherit their estate. If the deceased left a will stating that an
          outside party was to be given a piece of their estate or their estate
          as a whole, then the non-family member can collect the earnings.
        </p>
        <h2>Who Can You Turn To?</h2>
        <LazyLoad>
          <img
            src="/images/charity/brian-john-image-wrongful-death.jpg"
            width="37%"
            className="imgleft-fluid"
            alt="Wrongful death lawyers in Irvine"
          />
        </LazyLoad>
        <p>
          Irvine is considered to be one of the safest places in the world, but
          when you take into account the dangers posed by reckless drivers, that
          claim may be inaccurate. Many wrongful death claims that arise from
          Irvine residents are due to catastrophic auto accidents that result in
          fatal injuries. Many people come to our Irvine injury attorneys for
          help in obtaining justice against those who are responsible for the
          death of their loved ones, and we take great pride in obtaining just
          compensation.
        </p>
        <p>
          No matter how your loved one suffered fatal injuries, we can assist
          you. We have experienced great success in all types of wrongful death
          lawsuits, especially in the following personal injury lawsuits.
        </p>
        <ul>
          <li>Brain Injuries</li>
          <li>Spinal Cord Injuries</li>
          <li>Auto Accident Lawsuits</li>
          <li>Product Defect Cases</li>
          <li>Auto Defect Cases</li>
        </ul>

        <h2>Our Irvine Wrongful Death Lawyers are Here for You</h2>
        <p>
          Our law firm has assisted more than <strong> 12,000 clients</strong>{" "}
          and have sustained a <strong> 97% success rate</strong> for{" "}
          <strong> more than 40 years</strong>. We have a long history of highly
          satisfied clients and will give you a personal experience that has
          been replicated but never duplicated.
        </p>
        <p>
          When you hire a{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase Personal Injury Attorneys
          </Link>
          , we begin working on your case immediately. We will send
          investigators to the scene and contact the responsible entity
          immediately.
        </p>
        <p>
          We will give you the peace of mind associated with attorneys who make
          your loss a priority, immediately. Our attorneys care about more than
          a monetary reward, and you will experience their passion for assisting
          wrongful death victims the moment you call us.
        </p>
        <p>
          <strong> Call 949-203-3814</strong> to take advantage of your
          consultation and receive <strong> FREE LEGAL ADVICE</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
