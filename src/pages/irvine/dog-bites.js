// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-dog-bite-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Dog Bite Lawyers - Animal Attack Attorneys Orange County"
        pageDescription="If you are seeking compensation due to a vicious dog attack the Irvine Dog Bite Lawyers of Bisnar Chase are here to fight for the lost wages or medical expenses you have suffered from your dog bite injuries. For over 40 years our attorneys have held negligent dog owners strictly liable for injuries. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine dog bite lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          A{" "}
          <Link to="/dog-bites" target="new">
            dog attack
          </Link>{" "}
          can be an extremely traumatic experience for a victim as well as his
          or her family regardless of where and how it occurred. Not only does a
          dog attack have the potential to inflict severe physical damage in the
          form of injuries and scars, but it also causes significant{" "}
          <Link
            to="https://www.wikihow.com/Deal-with-Emotional-Trauma-After-a-Dog-Bites-You"
            target="new"
          >
            {" "}
            psychological damage
          </Link>
          , irrespective of the victim's age. In addition, dog bite injuries
          could be costly to treat because most insurance providers do not cover
          cosmetic surgical procedures that are needed to mend dog bite scars.
        </p>
        <p>
          If you have been attacked by a dog in Irvine or if you have suffered a
          severe dog bite injury, the experienced Irvine dog bite lawyers at
          Bisnar Chase can assist you with getting the compensation you need and
          rightfully deserve. We know that these devastating incidents can leave
          you with deep physical and psychological injuries. We will fight hard
          and advocate for your rights every step of the way.
        </p>
        <h2>How Prevalent Are Dog Bites?</h2>
        <p>
          There are about 70 million dogs in the United States. The U.S. Centers
          for Disease Control and Prevention (
          <Link to="https://www.cdc.gov/" target="new">
            CDC
          </Link>
          ) estimates that 4.5 million people are bitten by dogs each year in
          the country. About 90,000 dog bite victims seek emergency medical care
          at U.S. hospitals each year and approximately 25,000 victims of dog
          bites undergo reconstructive surgery each year.
        </p>
        <p>
          According to the website Dogsbite.org, in 2017, 39 people in the
          United States died from dog attacks. Pit bulls contributed to 74
          percent of these deaths despite making up only 6.5 percent of the
          total U.S. dog population. During the 13-year period between 2005 and
          2017, dogs killed 433 people nationwide. Two dog breeds – pit bulls
          and rottweilers, contributed to 76 percent of these deaths and 35
          different dog breeds contributed to the remaining fatal dog attacks.
          German shepherds were responsible for the second highest death rate in
          2017, inflicting four deaths.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine dog bite attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/physical-emotional-injuries-dog-bite-irvine.jpg')"
            }}
          >
            <h2>Physical and Emotional Injuries in Dog Attacks</h2>
          </div>
        </LazyLoad>

        <p>
          Since dogs have rounded teeth, the pressure exerted by their jaws can
          cause significant damage to the tissues and organs under the skin
          including the bones, muscles, tendons, blood vessels, and nerves. At
          the same time, dog bites may also result in serious emotional and
          psychological damage. Here are some of the common physical and
          emotional{" "}
          <Link to="/dog-bites/injury-infections-dog-bites" target="new">
            {" "}
            injuries victims suffer in dog attacks
          </Link>
          .
        </p>
        <h3>Bodily injury</h3>
        <LazyLoad>
          <img
            src="/images/text-header-images/dog-bite-bodily-injury-irvine.jpg"
            width="50%"
            className="imgleft-fluid"
            alt="irvine dog bite lawyer"
          />
        </LazyLoad>
        <p>
          Dog bites often result in severe physical injuries including
          abrasions, lacerations, puncture wounds, tissue loss, crush injuries,
          fractured bones, sprains and scars. Victims of dog bites, particularly
          children, should be closely examined for facial injuries such as
          fractures and nerve damage. Death could also occur as a result of
          these bodily injuries. When a dog attack occurs, some of the physical
          injuries happen when the dog bites the victim, while others may result
          as the victim is pushed to the ground by the dog. So, it is important
          to remember that in addition to bite wounds and lacerations, victims
          may also suffer broken bones, skin abrasions, head trauma and back
          injuries as a result of making contact with the ground.
        </p>
        <p>
          While injuries from dog bites can be treated, dog bite wounds that
          bleed often result in scars. It is tough to tell right away if the
          injuries will be disfiguring, disabling or painful. Some types of skin
          produce what are known as "keloid scars." This involves scar tissue
          that grows out of control producing raised, disfiguring scars. Small
          scars may fade away with time. Others may be fixed by procedures such
          as surgical excision, dermabrasion and pressure scar modification.
        </p>
        <p>
          Surgeries may also be required after severe dog attacks. When portions
          of skin or flesh is missing because of the bite, surgeons may perform
          skin graft surgery by taking skin from another part of the body to
          cover up the exposed part. In some cases, surgeons may have to
          re-attach fingers or toes after a dog attack. There are also instances
          where doctors may have to amputate limbs due to the severe nature of
          the attack.
        </p>
        <h3>Psychological injuries</h3>
        <p>
          After a traumatic event such as a dog attack, a victim may not feel or
          behave normally for a significant amount of time. He or she may
          experience depression, nervousness or fear. The individual may also
          display changes in behavior such as taking risks and not getting along
          with friends and family. Dog bite victims may suffer from what is
          known as post-traumatic stress disorder (PTSD) for months after the
          attack. It is important that victims get the treatment and counseling
          from a qualified mental health practitioner to help cope with this
          condition.
        </p>
        <p>
          One of the most common psychological effects of a dog attack is the
          fear of dogs. Sometimes, victims – even those who had previously liked
          dogs or had termed themselves as dog lovers may develop a fear of dogs
          after being attacked. Such fears may interfere with relationships,
          friendships and quality of life. For example, some people are afraid
          to take walks or participate in other social activities after they are
          attacked, which could significantly affect their lifestyle.
        </p>
        <p>
          Dog bite victims who have suffered a disfiguring injury may feel
          humiliated to go out in public. They might be extremely conscious
          about their scars, which could prevent them from having a social life.
          Disfigured victims may also have to endure stares, painful questions
          and face discrimination in the workplace.
        </p>
        <p>
          Children, in particular, may be affected emotionally after a dog
          attack. They might show symptoms such as withdrawal, anger, fear and
          depression. For very young children typical reactions may include a
          fear of being separated from a parent, crying, clinging or trembling.
          Older children may show some withdrawal, disruptive behavior or lack
          of attention. Children may also have trouble sleeping or find it hard
          to stay focused in school. Teens may exhibit responses similar to
          adults. They may have flashbacks of the attack or nightmare and may
          even resort to substance abuse. There are treatments available for
          children who have suffered such emotional trauma. Art therapy can help
          young children. For older children, support from family and friends
          can be an important part of the recovery process. It's also important
          to remember that how parents respond to a traumatic event can strongly
          influence their children's ability to recover. For example, if a
          mother appears troubled or anxious, she may need to get counseling
          first in order to be able to help her child.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/BEHDQeIRTgs"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What to Do after an Irvine Dog Attack</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/aggressive-dog-warning-stance-dog-bite-irvine.jpg"
            width="30%"
            className="imgright-fluid"
            alt="irvine-dog-bite-attorney"
          />
        </LazyLoad>
        <p>
          There are a number of steps you would be well advised to take in the
          aftermath of a dog attack. Here are some of those measures:
        </p>
        <ul>
          <li>
            <strong> Identify the dog and its owner.</strong> If it's a stray
            and you are unable to identify it, you may need to get rabies shots,
            which could be painful. Also, when you know who the dog owner is, it
            is easier for you to file a claim against him or her seeking
            compensation for your injuries and damages. It is also good to know
            if the dog has been licensed or if it has displayed violent behavior
            in the past toward other dogs or toward humans.
          </li>
          <li>
            <strong>
              {" "}
              Take photographs of the injuries before you treat them
            </strong>
            . Preserve any type of physical evidence you have from the scene
            including the dog's collar, tags or torn or bloody clothing. You may
            also want to get contact information for anyone who may have
            witnessed the dog attack or may have even helped you or rescued you
            from the dog.
          </li>
          <li>
            <strong>
              {" "}
              Get prompt medical attention, treatment and care for your injuries
            </strong>
            . Dog bites may result in infections, which can even prove fatal.
            Nearly 1,000 people are treated in U.S. emergency rooms each day as
            a result of dog bites. If you have suffered facial injuries, be sure
            to request a plastic surgeon so you get the appropriate treatment
            and care. Make sure you are following the doctor's instructions.
            Take all prescribed medications as directed.
          </li>
          <li>
            <strong>
              {" "}
              File a report with your local Orange County animal control agency
            </strong>
            . Don't depend on the hospital or some other agency to report the
            incident for you. In Irvine, the Animal Services department is a
            part of the Irvine Police Department.
          </li>
          <li>
            <strong> File a police report with the Irvine Police Dept</strong>.
          </li>
          <li>
            <strong> If the dog owner is insured</strong>, you may get a call
            from his or her homeowner's insurance company, which typically
            covers dog attacks. If you get this call, it is important you get
            information including their name, office address, telephone number,
            email address, the claim number for your case, the name of the
            person who is insured and the amount of money that is available to
            pay for your medical expenses.
          </li>
        </ul>

        <p>
          There are also certain mistakes that victims tend to commit in the
          aftermath of a dog bite incident. Here is how these may be avoided ­–
          Do not discuss money, settlement or the value of your case with anyone
          other than your Irvine personal injury lawyer. Do not write a letter,
          memo or provide any written statements about your case.
        </p>
        <p>
          Don't allow the insurance company to record what you say. Any
          statements you make can and probably will be used against. Don't make
          admissions such as you may have been trespassing or that you may have
          unknowingly provoked the dog. Don't make any guesses or verbalize
          assumptions about the case. Don't discuss who is responsible and
          finally, do not accept any money or sign off on paperwork without
          consulting your injury lawyer.
        </p>
        <h2>California's Dog Bite Law</h2>
        <p>
          California has what is known as a "strict liability statute" when it
          comes to dog attacks. California's Civil Code Section 3342 states:
          "The owner of any dog is liable for the damages suffered by any person
          who is bitten by the dog while in a public place or lawfully in a
          private place, including the property of the owner of the dog,
          regardless of the former viciousness of the dog or the owner's
          knowledge of such viciousness."
        </p>
        <p>
          This{" "}
          <Link to="/dog-bites/california-dog-bite-laws" target="new">
            dog bite law
          </Link>{" "}
          essentially means that dog owners can be held financially responsible
          for the actions of their pets. The only exceptions may be when a
          person was bitten by a dog while trying to commit a crime or while
          trespassing or if the injured person abused or deliberately provoked
          the dog. The law also categorically states that the dog owner is
          liable regardless of whether the attack occurred on public or private
          property or whether the dog had a prior history of attacking others.
        </p>
        <h2>What Damages Can Victims Seek After a Dog Attack?</h2>
        <p>
          Dog bite injuries can have a devastating impact on the victim while
          taking an emotional and financial toll on the victim's family as well.
          The value or worth of your dog bite claim will likely depend on the
          nature of the attack, the severity of your injuries and the extent of
          liability of the dog owner. Victims of dog bites can seek compensation
          for economic losses such as medical expenses, lost wages, cost of
          hospitalization, surgery (including reconstructive procedures),
          rehabilitation services and psychological therapy. They may also seek
          compensation for non-economic losses such as physical pain and
          emotional suffering caused by the dog attack and injuries. If a victim
          of the attack dies from the injuries, family members may seek
          additional damages by filing a wrongful death lawsuit.
        </p>
        <p>
          There may be several sources from which you may be able to receive
          compensation. Homeowner's insurance, and sometimes, even renter's
          insurance, may cover dog bite injuries. Please remember that the
          homeowner's and renter's insurance may also cover the dog bite
          injuries even if they occur outside the dog owner's property. If the
          dog owner has a personal umbrella liability policy, this could provide
          coverage over and above what the homeowner's policy provides.
        </p>
        <p>
          An auto liability policy's coverage may apply if the dog bite occurred
          in the car. In some cases, dog owners may also have a separate policy
          that specifically provides coverage for dog attacks. In addition, if a
          person other than the dog's owner is liable for your dog bite
          injuries, their insurance could cover your injuries as well.
          Sometimes, a property owner who allowed a dangerous dog to remain on
          his or her premises could also face a premises liability lawsuit in
          connection with a dog bite incident.
        </p>
        <h2>Contacting an Experienced Irvine Dog Bite Lawyer</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/vicious-chihuahua-dog-bite-irvine.jpg"
            width="100%"
            alt="irvine dog bite law firm"
          />
        </LazyLoad>
        <p>
          The Irvine dog bite lawyers at Bisnar Chase know and understand the
          physical, emotional and financial strain dog bite victims and their
          families are under. We will fight for your rights and help ensure that
          you are fairly and fully compensated for your losses. Please call us
          at 949-203-3814 for a free consultation and comprehensive case
          evaluation.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.128531086887!2d-117.8691849076402!3d33.66213583862208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1453155619865"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
