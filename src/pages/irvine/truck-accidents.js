// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-trucking-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Truck Accident Lawyers - Bisnar Chase"
        pageDescription="The Irvine Truck Accident Attorneys of Bisnar Chase has been helping accident victims win millions of dollars in compensation. If you have been injured in a truck incident contact the law firm of Bisnar Chase. Call 949-203-3814 to speak with top rated lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine truck accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Distracted driving is a serious problem on our roadways. Talking on a
          cell phone or texting are the most common distracted driving issues we
          deal with today.
        </p>
        <p>
          But distracted driving can refer to any action on the part of the
          driver that takes his or her attention off the road. For example,
          eating, drinking, grooming, fiddling with radio controls, looking at
          the GPS – are all examples of distractions that can result in serious
          injury Irvine truck accidents.
        </p>
        <p>
          Large commercial trucks can be difficult to maneuver even when a
          driver has both hands free and is focused on driving. When a truck
          driver is operating a large vehicle while distracted, the consequences
          can be devastating.
        </p>
        <h2>Dangers of Distracted Driving</h2>
        <p>
          A distraction can be defined as any time a driver diverts his or her
          attention from the task of driving. This may include external
          distractions such as looking out the window or internal distractions
          such as talking on the cell phone or texting.
        </p>
        <p>
          According to the{" "}
          <Link to="https://ai.fmcsa.dot.gov/LTCCS/" target="new">
            Federal Motor Carrier Safety Administration's Large Truck Crash
            Causation Study (LTCCS)
          </Link>{" "}
          :
        </p>
        <ul>
          <li>
            8 percent of large truck crashes occurred when the drivers were
            externally distracted
          </li>
          <li>
            2 percent of large truck accident occurred when the driver was
            internally distracted.
          </li>
          <li>
            In 2009, nearly 5,500 people died and half a million were injured in
            crashes involving a distracted driver.
          </li>
        </ul>
        <p>
          According to{" "}
          <Link to="https://www.nhtsa.gov/" target="new">
            National Highway Traffic Safety Administration's (NHTSA)
          </Link>{" "}
          research, distraction-related fatalities represented 16 percent of
          overall traffic fatalities in 2009.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine distracted driving law for truck drivers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/irvine-distracted-truck-driver-accidents.jpg')"
            }}
          >
            <h2>Distracted Driving Law for Truckers</h2>
          </div>
        </LazyLoad>

        <p>
          The{" "}
          <Link to="https://www.fmcsa.dot.gov/" target="new">
            Federal Motor Carrier Safety Administration (FMCSA)
          </Link>{" "}
          issued a new rule last year banning truck drivers from texting, using
          cell phones and other communication devices while driving. This policy
          has serious implications for interstate truck drivers who carry cargo.
        </p>
        <p>
          The FMCSA in issuing the rule cited recent research by{" "}
          <Link to="https://vt.edu/" target="new">
            Virginia Tech
          </Link>
          , which found that truck drivers who send text messages on a cell
          phone are about 23 times more likely to get into an accident than
          drivers who keep their eye on the road.
        </p>
        <p>
          On an average texting drivers take their eyes off the road for about
          five seconds per message.
        </p>
        <p>
          During that time, researchers say, a truck can travel the length of a
          football field. Imagine the damage an unattended, 80,000-pound
          tractor-trailer can cause. Any truck driver who violates the federal
          texting ban is subject to a penalty of up to $2,750.
        </p>

        <h2>Truck Accidents Caused by Distracted Driving</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/irvine-truck-accident-injuries.jpg"
            width="100%"
            alt="irvine truck accident injury attorneys"
          />
        </LazyLoad>

        <p>
          A truck accident caused by a distracted driver can result in major
          injuries especially for those in smaller passenger vehicles.
          Distracted driving truck accidents usually involve head-on collisions
          or rear-end crashes that involve several vehicles.
        </p>
        <p>
          Injuries sustained in these types of crashes include traumatic brain
          injuries, spinal cord damage, broken bones and internal injuries. In
          cases where a distracted truck driver causes an accident, injured
          victims can seek compensation for damages including medical expenses,
          lost wages, cost of hospitalization, rehabilitation, pain and
          suffering and emotional distress.
        </p>
        <p>
          Families who have lost loved ones in distracted driving truck
          accidents can file a wrongful death claim seeking damages including
          medical costs, funeral expenses, lost future income and loss of love
          and companionship.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/7AR06hXs3E0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Contacting an Irvine Truck Accident Lawyer</h2>
        <p>
          An experienced Irvine{" "}
          <Link to="/orange-county/truck-accidents" target="new">
            truck accident lawyer
          </Link>{" "}
          can help victims and their families take the steps that are necessary
          to bolster their claim.
        </p>
        <p>
          In a distracted driving truck accident, cell phone records are
          critical in order to determine whether the truck driver was on the
          phone or texting at the time of the collision. Such crucial evidence
          can get misplaced, lost or destroyed if too much time passes.
        </p>
        <p>
          The knowledgeable Irvine truck accident lawyers at Bisnar Chase have a
          long and successful track record of helping injured victims and their
          families. We will remain on your side, fight for your rights and help
          secure just compensation for your losses.
        </p>
        <p>
          Trucking companies are often represented by large insurance companies
          and powerful defense lawyers who strive to protect their best
          interests.
        </p>
        <p align="center">
          Immediately call an experienced and reputable{" "}
          <strong> Orange County Personal Injury Lawyer</strong> for a free
          consultation at <strong> 949-203-3814</strong> or visit the Bisnar
          Chase Personal Injury Attorneys Newport Beach personal injury
          attorneys in our Newport Beach, CA office.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
