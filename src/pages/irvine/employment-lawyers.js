// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/irvine-employment-lawyers.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Employment Lawyer - Bisnar Chase"
        pageDescription="Have you been wrongfully terminated? The Irvine Employment Lawyers of Bisnar Chase are here to assist you with obtaining justice. Our wrongful termination attorneys have been specializing in employment law for the past 40 years. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Employment Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine employment attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Employees have a number of rights that must be protected. When
          companies cut corners by violating the rights of their employees, the
          wronged workers may file a class action lawsuit to pursue financial
          compensation for their losses.
        </p>
        <p>
          Employers have it in their best interest to avoid these types of
          bitter situations because they often prove costly and are bad
          publicity. Unfortunately, some companies continue to blatantly break
          employment laws forcing employees to make some difficult decisions
          regarding the actions they must take.
        </p>
        <p>
          In such cases, wronged employees would be well advised to discuss
          their legal rights and options with an experienced Irvine employment
          lawyer.
        </p>

        <h2>Examples of Labor Law Violations</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/examples-of-labor-law-violations-office-meeting-lawyers.jpg"
            width="100%"
            id="labor-laws"
            alt="labor law violation lawyers"
          />
        </LazyLoad>

        <p>
          There are a number of issues that can lead to a wage claim or class
          action lawsuit against an employer. Some examples of wage claim issues
          include:
        </p>
        <ul>
          <li>
            Failure to provide correct wages or compensation for overtime. It is
            common for employers to ask employees to work more than eight hours
            without additional pay. Under California law, even salaried
            employees must be provided overtime pay whenever they work more than
            eight hours in one day.
          </li>
          <li>
            Failure to give meal or rest breaks. Some employees feel like they
            must work through meal periods or that they will be fired if they
            ask for their legally mandated breaks. Companies that foster this
            type of behavior or that don't allow breaks can be held liable.
          </li>
          <li>
            Working off the clock. Employers are not allowed to ask employees to
            "punch-out" before continuing to work. When an employee is working,
            he or she should be paid for the hours worked.
          </li>
          <li>
            Failure to pay all wages due. Employers may owe workers for wages
            that they have neglected to pay. Companies that routinely pay the
            wrong amount or employers who owe a number of their workers back pay
            may be fostering a workplace that violates state laws on a regular
            basis.
          </li>
          <li>
            Failure to pay at time of termination. Just because an employee has
            been fired, it does not mean that he or she does not have rights.
            Employers must pay their terminated employees for the hours they
            worked up until the time of the termination.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="class action lawsuit lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/class-action-lawsuit-lawyers.jpg')"
            }}
          >
            <h2>Class Action Employment Lawsuits</h2>
          </div>
        </LazyLoad>
        <p>
          Class action lawsuits involve the consolidation of many similar claims
          against a company. They allow the claimants to remain anonymous and
          for one law firm to handle the case.
        </p>
        <p>
          This way the court will not have to hear several claims that are
          similar in nature and victims of wrongdoing can pursue compensation as
          a group.
        </p>
        <p>
          It is common for wage claims to result in class action cases because a
          company that violates the rights of one employee is likely violating
          the rights of multiple employees.
        </p>
        <p>
          Corporations and companies that fail to pay their employees properly,
          who do not provide sick leave, who do not allow meal breaks or who
          fail to pay overtime should be held accountable for their actions.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/egUg1PbJFSU"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Protecting Employees' Rights</h2>

        <p>
          If you have been working for a prolonged period of time without proper
          pay or breaks, you may worry that speaking up with cost you your job.
        </p>
        <p>
          The experienced <Link to="/contact">Irvine employment lawyers </Link>{" "}
          at{" "}
          <Link to="/" target="new">
            {" "}
            Bisnar Chase Personal Injury Attorneys
          </Link>{" "}
          can help workers who are in such tough situations better understand
          their legal rights and options. It is possible those other employees
          have similar complaints or that there has already been a claim filed
          against your employer.
        </p>
        <p>
          It would be in your best interest to discuss your situation with a
          class action and wage claim attorney who will remain on your side and
          fight for your rights. Please contact us to find out how we can help
          you.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.128531086887!2d-117.8691849076402!3d33.66213583862208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1453155619865"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
