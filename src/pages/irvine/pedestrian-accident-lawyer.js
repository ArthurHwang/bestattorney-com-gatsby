// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/irvine-pedestrian-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Pedestrian Accident Lawyer - O.C Pedestrian Accident Attorneys"
        pageDescription="Contact the Irvine Pedestrian Accident Lawyers of Bisnar Chase if you have been injured in Orange County at no fault of your own. Over $500M recovered and an impressive 96% success rate with our cases. Free consultation and no win, no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Pedestrian Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Irvine Pedestrian Accident Attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          As a pedestrian, having the right away does not always mean you are
          safe to cross. The experienced
          <strong> Irvine Pedestrian Accident Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          know that drivers can be reckless and unaware of their surroundings.
          Victims can find themselves in the emergency room or even worse dead.
        </p>
        <p>
          The clients of Bisnar Chase have been awarded millions in compensation
          and our injury attorneys have retained a{" "}
          <strong> 96% success rate for 40 years</strong>. We also guarantee
          that if there is
          <strong> no win, there is no fee</strong>.
        </p>
        <p>
          If you or someone you know has been a victim of a pedestrian accident
          contact the law offices of Bisnar Chase. Upon your call, you will
          receive a <strong> free consultation </strong>with one of our
          highly-qualified legal team members.
        </p>
        <p>
          <strong> Call 949-203-3814</strong>.
        </p>
        <h2>Causes of Pedestrian Accidents in Irvine</h2>
        <p>
          Every day hundreds of people lose their lives by simply crossing the
          street. Unfortunately, children account for many of those losses as
          well. Usually, carelessness and negligence on the behalf of the driver
          is to blame. There are numerous reasons why pedestrians fall victim to
          physical damages such as{" "}
          <Link to="/head-injury/spinal-cord-injuries" target="_blank">
            {" "}
            spinal cord injuries
          </Link>
          , head injuries and broken bones.
        </p>
        <p>
          <strong>
            {" "}
            The following are the most common causes of pedestrian accidents
          </strong>
          :
        </p>
        <p>
          <strong> Bike riders</strong>: Although drivers do cause pedestrian
          incidents most of the time, cyclists are also responsible for some
          pedestrian/traffic accidents as well. For example, when a rider is in
          the bike lane, and the rider is sharing the lane with a runner, the
          cyclist can possibly force the runner onto on-going traffic. Data has
          indicated that in one year, over 7,904 pedestrians experienced severe
          injuries due to a person riding a bike.
        </p>
        <p>
          <strong> Faded crosswalks</strong>: Unmarked or not visibly obvious
          crosswalks increases the chances of a pedestrian being involved in an
          accident. It is crucial that as you cross the street there is a clear
          path which indicates people can walk. Signaled crosswalks are the
          safest paths for people to take while crossing a street.
        </p>{" "}
        <LazyLoad>
          <img
            src="/images/pedestrian-accidents/left-hand-turn-resized.jpg"
            width="100%"
            className="imgright-fluid"
            alt="Pedestrian accident lawyer in Irvine"
          />
        </LazyLoad>
        <p>
          <strong> Cars turning left</strong>: Pedestrians are three times more
          likely to get injured on a left-hand turn. Drivers are usually too
          distracted from looking out for on-coming vehicles that they forget to
          also look out for people crossing. As a pedestrian, you should always
          make sure that before a driver makes a left turn, acknowledges you
          with eye-contact.
        </p>
        <p>
          <strong> Black clothing</strong>: Attire may not seem like an
          important factor as a pedestrian, but surprisingly, what you wear can
          possibly prevent you from being a crosswalk victim. When it is dark
          and if streets are not well lit, it can be easy not to see a
          pedestrian who is dressed in black. Make sure to wear bright colors or
          reflective gear if you are walking at night.
        </p>
        <p>
          <strong> Driving under the influence</strong>: In one year, over 1.4
          million drivers are under the influence. Victims can collect
          compensation for damages such as medical bills for their pedestrian
          accident case. When you hire a trustworthy Irvine pedestrian accident
          attorney to fight for your rights, you'll have a much better chance of
          being fully and justly compensated for your pain, suffering and
          financial expenses.
        </p>
        <h2>Types of Charges That Can Be Filed for a Pedestrian Accident</h2>
        <p>
          Pedestrians can experience extreme pain and suffer from an accident
          and drivers can consequently face criminal charges. The various kinds
          of allegations depend on the severity of the crash and how many
          damages were accumulated as well. Below are the most common charges
          that reckless drivers face in car-pedestrian related incidents.
        </p>
        <p>
          <strong> DUI</strong>: Receiving a citation for driving under the
          influence and harming a pedestrian can lead to severe criminal
          penalties. If a person is harmed while crossing the street by a driver
          under the influence, then this can act as an “{" "}
          <Link
            to="https://www.scribd.com/doc/85043809/Aggravating-Circumstance"
            target="_blank"
          >
            aggravated circumstance
          </Link>
          ”. Aggravated circumstances are factors that can make the incident
          worse and can aid the plaintiff’s party as proof of negligence.
        </p>
        <p>
          <strong> Manslaughter</strong>: When a person dies due to someone’s
          reckless driving this constitutes as vehicular manslaughter. About 11
          percent of traffic deaths involve pedestrians being struck by motor
          vehicles. Almost all states have penalties for vehicular homicide as
          well. If the incident was truly proven to be an accident, meaning
          there was not any involvement of carelessness while driving or alcohol
          consumption, then there is a chance that criminal charges may be
          eliminated.
        </p>
        <p>
          <strong> Hit and Run</strong>: Leaving the scene of an accident that
          you were involved in is illegal. Hit and runs can count as a
          misdemeanor charges can range though if the incident was more severe.
          Hit and run offenders need to be accountable so that the accident
          victim can receive the compensation they deserve.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pedestrian lawyer in Irvine"
            style={{
              backgroundImage:
                "url('/images/pedestrian-accidents/irvine-cross-walk-signal.jpg')"
            }}
          >
            <h2>Tips for Avoiding a Pedestrian Accident in Irvine</h2>
          </div>
        </LazyLoad>
        <p>
          There are preventive measures pedestrians can take to decrease their
          chances of being involved in an accident. It is also important that
          small children understand the severity of walking across the street.
          Elderly people also have a high risk of being hit by a driver.
        </p>
        <p>
          <strong>
            {" "}
            The way in which a walker can avoid a pedestrian incident is by
          </strong>
          :
        </p>
        <p>
          <strong> Do not be on your phone while crossing the street</strong>:
          Walking and texting on your phone can make you less aware of your
          surroundings and could lead to dangerous consequences. If someone
          sends you a text or calls you before you cross the street, take the
          call or send the text after crossing.
        </p>
        <p>
          <strong> Make eye-contact with drivers before walking</strong>: Make
          sure to establish eye contact with a driver to ensure that he/she is
          aware of you.
        </p>
        <p>
          <strong>
            {" "}
            If you are intoxicated, make sure you are not walking by yourself
          </strong>
          : Alcohol consumption can impair your judgment and the outcome can be
          fatal. Either call an Uber or leave with a close friend or family
          member to ensure you arrive home safely.
        </p>
        <h2>Compensation for an Irvine Pedestrian Injury</h2>
        <p>
          Recovering from a pedestrian accident can be a slow and painful
          process. The Orange County pedestrian accident lawyers of Bisnar Chase
          believe that victims of cross-walk accidents are entitled to a
          settlement. The total number of damages that can be recovered depends
          on the severity of your injuries. Compensation can cover costs such as
          medical treatment and loss of wages.
        </p>
        <p>
          The kind of compensation and the amount of compensation also depends
          on the assets of the defendant and the insurance companies of both the
          pedestrian victim and the perpetrator.
        </p>
        <h2>Can a Pedestrian Be Responsible for an Accident?</h2>
        <p>
          Pedestrians can be partially or completely at fault for a pedestrian
          or car accident. Although witness reports can help prove
          accountability, the question of “who is at fault?” will ultimately be
          answered by a group of jurors.
        </p>
        <p>
          Some examples where pedestrians can be at fault when they are
          crossing:
        </p>
        <ul>
          <li>when intoxicated</li>
          <li>against the do not walk sign</li>
          <li>
            bridges and other areas where pedestrians are not allowed to cross
          </li>
        </ul>
        <p>
          In California, pedestrians who were at fault can be proven to have{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Comparative_negligence"
            target="_blank"
          >
            {" "}
            comparative negligence.
          </Link>{" "}
          Which means that if a plaintiff was proven to be “at fault” for an
          accident, can have the amount that she/him is awarded reduced.
        </p>
        <LazyLoad>
          <img
            src="/images/pedestrian-accidents/compensation-lawyer-image-.jpg"
            width="100%"
            alt="Crosswalk victim attorney in Irvine"
          />
        </LazyLoad>
        <h2>We Will Win You the Compensation You Deserve</h2>
        <p>
          Since 1978, the Irvine Pedestrian Accident Lawyers of Bisnar Chase
          have fought passionately for the rights of accident victims and have
          held negligent parties accountable for their wrongdoings. Individuals
          harmed in pedestrian accidents accrue numerous costs after an accident
          such as medical bills.
        </p>
        <p>
          The law offices of Bisnar Chase’s mission statement is: "To provide
          superior client representation in a compassionate and professional
          manner while making our world a safer place.”
        </p>
        <p>
          Our legal team members are on standby waiting for your{" "}
          <strong> call from Monday-Friday 9AM - 5PM</strong>.
        </p>
        <p>
          <strong> Call 949-203-3814 to schedule a free consultation.</strong>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
