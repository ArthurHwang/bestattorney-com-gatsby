// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dangerous Intersections in Irvine, La Habra and Cypress"
        pageDescription="Avoiding dangerous Irvine, La Habra and Cypress intersections are harder than you think, especially if they're one of the worse intersections in California. Each of these Orange County cities is known to have a high traffic flow and accident rate. If you have experienced an accident at an intersection call 949-203-381."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Most Dangerous Intersections in Irvine, La Habra and Cypress</h1>
        <BreadCrumbs location={location} />
        <p>
          What do Irvine, La Habra, and Cypress have in common? They are all on
          the Orange County Transportation Authority's list for busiest
          intersections. Each of these cities has a high traffic flow, high
          population and high car accident rate. When driving through these
          cities, pay special attention as you approach the following
          intersections.
        </p>
        <p>
          Because of Irvine's good schools, jobs and housing, the city was
          chosen in 2008 by CNNMoney.com as the fourth best place to live in the
          United States. In June 2009, the Federal Bureau of Investigation
          reported that Irvine had the lowest violent crime rate among cities in
          the United States. Families flock to Irvine to take advantage of the
          community's progress in safety, but the streets of Irvine are no
          safe-haven for motorists.
        </p>
        <p>
          According to the Statewide Integrated Traffic Records System (SWITRS),
          4 people were killed and 827 people were injured as a result of Irvine
          car accidents. In August of this year a woman walking across Barranca
          Parkway received catastrophic injuries after being hit by a suspected
          Irvine DUI collision.
        </p>
        <h2>Jamboree Road and Barranca Parkway</h2>
        <LazyLoad>
          <img
            width="50%"
            src="/images/katella-valley-view.jpg"
            alt="Most Dangerous Intersections in Irvine, La Habra and Cypress"
            className="imgcenter-fluid"
          />
        </LazyLoad>

        <p>88,800 vehicles pass through per day.</p>
        <p>
          If the driver is found guilty of operating his car under the
          influence, then, he could be held criminally and civilly liable in
          this case. Victims in such cases would be well-advised to seek the
          counsel of an experienced Irvine personal injury lawyer.
        </p>
        <h2>La Habra</h2>
        <p>
          La Habra, located in the northwestern corner of Orange County,
          California is home to 62,635 people. A quiet residential community, it
          is conveniently located within an hour's drive of many beach, mountain
          and desert recreation areas.
        </p>
        <p>
          According to the Statewide Integrated Traffic Records System (SWITRS),
          there were no fatalities as a result of La Habra car accidents but 223
          people were injured in 2008. In April of 2009 a woman was killed and a
          man was critically injured when the vehicle in which they were riding
          collided with a La Habra Police patrol car on the way to assist other
          officers. It wasn't long before the family filed a{" "}
          <Link to="/wrongful-death" target="_blank">
            {" "}
            wrongful death
          </Link>{" "}
          suit against the city.
        </p>
        <p>
          One year later, the family has recently turned down a settlement and a
          $100,000 offer of financial support for the family's youngest child,
          who is a minor, and so the civil trial will go forward.
        </p>
        <h2>Beach Boulevard and Imperial Highway</h2>
        <LazyLoad>
          <img
            src="/images/jamboree-barranca.jpg"
            alt="Beach Boulevard and Imperial Highway"
            className="imgcenter-fluid"
          />
        </LazyLoad>

        <p>86,500 cruise through this area on a daily basis.</p>
        <h2>Cypress</h2>
        <p>
          Cypress is a small suburban city located in the northern region of
          Orange County, as of January 1st 2010 its population was about 49,981.
          According to the Statewide Integrated Traffic Records System (SWITRS),
          there was 1 fatality as a result of Cypress car accident and 103
          people were injured.
        </p>
        <p>
          <strong> Katella Avenue and Valley View</strong>
        </p>
        <LazyLoad>
          <img
            src="/images/beach-imperial.jpg"
            alt="Katella Avenue and Valley View"
            className="imgcenter-fluid"
          />
        </LazyLoad>

        <p>85,500 vehicles pass through this intersection on a daily basis.</p>
        <h2>Irvine, La Habra, and Cypress Personal Injury Attorneys</h2>
        <p>
          Are you in need of an{" "}
          <Link to="/" target="_blank">
            {" "}
            Irvine personal injury attorney
          </Link>
          , La Habra personal injury attorney, or Cypress personal injury
          attorney? Want to know if you have a case? Want to know what your case
          is worth? Want compensation for your injuries? Want justice? Want to
          make sure the same thing doesn't happen to someone else?
        </p>
        <p>Call your friends in the legal industry.</p>
        <p>949-203-3814</p>
        <p>The call is free. The advice may be priceless.</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
