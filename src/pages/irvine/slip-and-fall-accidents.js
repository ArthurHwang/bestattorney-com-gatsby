// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-slip-and-fall-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Slip & Fall Attorneys"
        pageDescription="Slip and fall accidents can happen at any place at any moment due to dangerous conditions. The Irvine Slip & Fall Attorneys have been providing legal representation to victims of slip and falls for 40 years. Call 949-203-3814 the premise liability lawyers of Bisnar Chase and receive a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Slip & Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine slip and fall attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Slip and fall accidents can happen anywhere, at any time.  Although we
          tend to associate these types of accidents with slipping on a wet
          floor, there are many other ways people are injured every year by
          slips and falls.  For example, the Centers for Disease
        </p>
        <p>
          Control estimate that 1,800 elderly residents of nursing homes die
          from falls every year, and at least 15,000 people over 65 die each
          year as a direct result of injuries sustained during a fall.
        </p>
        <p>
          In fact, the CDC reports that 2 million people over 65 are treated in
          an emergency room each year as the result of fall-related accidents.
        </p>

        <h2>The Prevalence of Slip & Fall Accidents</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/irvine-slip-and-fall-back-injury.jpg"
            width="52%"
            className="imgleft-fluid"
            alt="irvine-slip-and-fall-back-injury-attorneys"
          />
        </LazyLoad>

        <p>
          Why are slip and fall accidents so prevalent? There can be several
          reasons for this phenomenon.  First, people, especially the elderly,
          are more mobile than in previous generations.
        </p>
        <p>
          With older people out golfing, fishing, walking, and biking, there are
          going to be more injuries per capita than in earlier years when older
          people often stayed at home and &ldquo;rocked on the porch.&rdquo; 
          Further, stores and other venues are far more crowded these days than
          in the past.  It is very easy for someone to knock into or otherwise
          accidentally push a senior citizen and cause an accident.
        </p>
        <p>
          Finally, employees and employers both seem to suffer from a certain
          degree of apathy regarding keeping store or job site conditions
          safe—at least until someone falls and is injured.  At that point, the
          defending party may decide to fight the payment of just damages by
          citing all sorts of reasons that the accident is not its
          responsibility.
        </p>
        <p>
          This is one reason why you need the services of professionals such as{" "}
          <Link to="/premises-liability/slip-and-fall-accidents" target="new">
            {" "}
            Irvine slip and fall lawyers
          </Link>{" "}
          if you have been involved in one of these accidents.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/-c_zppq6YfQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          It is not likely that you will experience a sudden surge of generosity
          on the part of the defendants, so you will probably have to pursue a
          claim in order to get them to pay you fairly for your injuries.
        </p>
        <p>
          This is difficult to do alone, although some people try.  However, by
          hiring expert slip and fall attorneys in Irvine, you are far more
          likely to win your case and collect fair damage compensation than if
          you attempt to handle your case alone.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="irvine slip and fall accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/irvine-slip-and-fall-attorneys.jpg')"
            }}
          >
            <h2>Contacting An Expert Slip and Fall Attorney</h2>
          </div>
        </LazyLoad>

        <p>
          Remember, expert slip and fall lawyers have handled many cases just
          like yours. They have the experience and knowledge to ensure that you
          are taken seriously by the defense and that your injuries and the
          suffering you have endured are compensated adequately.
        </p>
        <p>
          If you attempt to negotiate with the defense or its agents yourself,
          it is likely you will be offered a small sum that in no way represents
          the totality of your needs or what you deserve to be paid for your
          damages.
        </p>
        <p>
          Consultation with an expert personal injury attorney should always be
          free, so you have nothing but an hour of time to lose by making an
          appointment and talking with a personal injury lawyer about your slip
          and fall case.
        </p>
        <p>
          You may be pleasantly surprised at how much your case is actually
          worth and how easily you could collect this amount. Of course, not
          every case turns into a significant monetary recovery, but you have
          nothing to lose by finding out if you have one. Please call us at
          949-203-3814 for a free, confidential case evaluation.
        </p>
        <p align="center">
          Immediately call an experienced and reputable Orange County Personal
          Injury Lawyer for a free consultation at 949-203-3814 or visit the
          Bisnar Chase Personal Injury Attorneys{" "}
          <Link to="/newport-beach" target="new">
            {" "}
            Newport Beach personal injury attorneys
          </Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
