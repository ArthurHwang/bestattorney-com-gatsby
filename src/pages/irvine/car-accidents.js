// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "car-accidents/irvine-car-accident-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Car Accident Lawyer | The Law Firm of Bisnar Chase"
        pageDescription="The amount of compensation that accident victims can accumulate can be great. Call 949-203-3814 to receive a free consultation from an experienced Irvine Car Accident Lawyer. The Orange County car accident attorneys of Bisnar Chase have helped injury victims gain over $500 Million dollars in wins. Contact us now."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Irvine Car Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          What would you do if you got into a car accident? If you were severely
          injured with mounting medical bills that just kept piling up? If
          doctors told you couldn't go back to work for a year? What do you do?
        </p>
        <p>
          You contact the skilled <strong> Irvine Car Accident Lawyers</strong>{" "}
          of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>
          .
        </p>
        <p>
          For <strong> 40 years</strong> our attorneys have legally represented
          car crash victims and earned them millions in compensation. The law
          firm of Bisnar Chase has held a<strong> 96% success rate</strong> and
          if we do not win your case, you do not have to pay. If you or someone
          you know has suffered from extreme injuries following a motor vehicle
          accident contact Bisnar Chase's Irvine car accident attorneys.
        </p>
        <p>
          Enduring a tragic incident is painful, reaching out for help shouldn't
          be.
        </p>
        <p>
          <strong>
            {" "}
            Call 949-203-3814 for a free case evaluation, Monday-Friday from 8
            a.m.-5 p.m
          </strong>
          .
        </p>
        <p>
          Our top-notch legal team is here to take your call. Contact us today
          to explore your legal options and rights as an accident victim.
        </p>
        <h2>When Should I Reach Out to an Irvine Car Crash Lawyer?</h2>
        <p>
          Most people think that if they only experienced minor injuries that
          they do not need to hire a car accident attorney. This type of
          mentality can be costly on behalf of the victim. Experts say that if
          you do not feel pain at the time, days or weeks later the situation
          may change. Making a claim is not enough to cover the damages from the
          incident.
        </p>
        <p>
          The intent of insurance companies is to get an accident victim to
          settle for the lowest amount possible. If you have long-lasting side
          effects from injuries such as a traumatic head wound, a small
          settlement will not be enough to pay for your rehabilitation expenses.
        </p>
        <p>
          The Irvine car accident attorney's of Bisnar Chase{" "}
          <strong>
            {" "}
            will provide you high-quality legal representation to win you the
            maximum compensation you will need after a collision
          </strong>
          . You'll have a skilled negotiator in the courtroom and at the
          settlement table.
        </p>
        <p>
          Someone who will be fully aware of all state and federal insurance and
          liability laws. An expert who can take care of all the paperwork,
          court motions and other legal procedures.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Car collision injury attorneys in Irvine"
            style={{
              backgroundImage:
                "url('/images/car-accidents/car-colission-scene-text-banner.jpg')"
            }}
          >
            <h2>Irvine Vehicle Collision Statistics</h2>
          </div>
        </LazyLoad>
        <p>
          Granted, Irvine is somewhat safe, but car accidents can and do happen
          in this city of 266,122 people.
        </p>
        <p>
          Recently, the{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/userLogin.jsp"
            target="_blank"
          >
            {" "}
            California Highway Patrol's Statewide Integrated Traffic Records
            System
          </Link>{" "}
          (SWITRS) revealed that eight people were killed and 842 were injured
          in Irvine car collisions.
        </p>
        <p>
          <strong> More data has also indicated that:</strong>
        </p>
        <ul>
          <li>
            Nineteen{" "}
            <Link to="/irvine/pedestrian-accident-lawyer" target="_blank">
              {" "}
              pedestrians were injured in car crashes{" "}
            </Link>
          </li>
          <li>Motorcycle accidents injured 39</li>
          <li>Car accidents killed two and injured 43 bicyclists.</li>
          <li>Drunk drivers caused two fatalities and 65 injuries.</li>
          <li>
            In one year, seven car collisions resulted in seven fatalities.
          </li>
        </ul>
        <h2>Reaching a Settlement in Irvine</h2>
        <p>
          Handling a car accident claim can be difficult and at times insurance
          adjusters may trick you into thinking you are settling for a good
          deal. An Irvine Car Accident Lawyer will evaluate your case and send a
          demand letter to the negligent party's insurance company. The demand
          letter will include what compensation that is necessary to recover
          from property, physical and emotional damage.
        </p>
        <p>
          If the insurance company of the liable party refuses to pay the
          requested amount then the case would go to trial. Another important
          element for increasing your chances of earning the compensation you
          need is hiring a trial lawyer.
        </p>
        <p>
          <strong>
            {" "}
            Brian Chase shares why hiring a trial lawyer should be your main
            priority
          </strong>
          .
        </p>
        <p>
          "...you want to hire a trial lawyer not a settlement lawyer. So many
          lawyers are settling cases and they're getting fifty cents on the
          dollar because they're not trial attorneys. You want to find a law
          firm where the lawyers are trying cases, the judges know who they are,
          the insurance companies and then if you do settle that case you're
          going to get a hundred cents on the dollar not 50 cents on the
          dollar." Chase tells American Law Television host.
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="https://www.youtube.com/watch?v=jKy3wa5M2yM"
              target="_blank"
            >
              {" "}
              Watch the full interview here:{" "}
            </Link>
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/jKy3wa5M2yM?t=25"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Reviews on the law office of Bisnar Chase</h2>
        <p>
          The injury attorneys at Bisnar Chase are dedicated to not only winning
          your case but also catering to your needs. Our clients are our
          priority and making sure that they are aware of what is happening at
          every step of their case is essential. Below are a few reviews of the
          excellent legal representation that Bisnar Chase has to offer you.
        </p>

        <div>
          <div>
            <center>
              <blockquote>
                <h5>
                  I was very happy that everyone I worked with from coming in
                  and being greeted by the receptionist to my own actual
                  personal lawyers. They were so amazing that I couldn't have
                  wished or wanted for a better experience here at Bisnar Chase.
                </h5>
                <p style={{ textAlign: "center" }}>
                  - <strong> Alexis Bennet, former client</strong>
                </p>
              </blockquote>
            </center>
          </div>
          <div></div>

          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/oCeN5TviMcY"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
        </div>

        <p></p>
        <div>
          <div>
            <center>
              <blockquote>
                <h5>
                  My first impression of Bisnar Chase is I really felt that they
                  cared about me. They made me feel special. I was confident in
                  them. I knew that they would help me.
                </h5>
                <p style={{ textAlign: "center" }}>
                  - <strong> Gloria Levesque, former client</strong>
                </p>
              </blockquote>
            </center>
          </div>
          <div></div>

          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/K1JggkiZOdg"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
        </div>

        <h2>
          Bisnar Chase Offers Free Consultations and a Preview of Your Options
        </h2>
        <p>
          When you hire a{" "}
          <strong>
            {" "}
            Bisnar Chase Irvine{" "}
            <Link to="/car-accidents" target="_blank">
              {" "}
              Car Accident Lawyer{" "}
            </Link>
          </strong>{" "}
          <Link to="/car-accidents" target="_blank"></Link> to fight for your
          rights, you'll have a much better chance of being fully and justly
          compensated for your pain, suffering and financial expenses.
        </p>
        <p>
          You'll want someone who has the knowledge, financial and legal
          resources to go up against the experienced insurance company lawyers
          and their army of experts. This is why the car accident lawyer you
          retain should have a proven track record of wins in court or at the
          settlement table. He or she should have gained the respect of the
          legal community and be responsive to your needs.
        </p>
        <p>
          The law firm of Bisnar Chase obtains all of these elements along with
          providing an <strong> excellent client/attorney relationship</strong>.
        </p>
        <p>
          Bisnar Chase has won over{" "}
          <strong> $500 Million dollars in compensation for clients</strong> who
          have experienced catastrophic injuries due to a car crash and we don't
          show any signs of stopping.
        </p>
        <p>
          <strong> Call 949-203-3814 for a free case analysis</strong> with one
          of our legal team members.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
