// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/irvine-hoverboard-injuries-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Irvine Hoverboard Injury Lawyers - Bisnar Chase"
        pageDescription="Injured from a Hoverboard accident? Let Bisnar Chase personal injury lawyers win maximum compensation for you or you don't pay. Free consultation 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Irvine Hoverboard Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="irvine hoverboard injuries and accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          In Irvine, hoverboards have become a common sight. Recently, they have
          been the most exci ting addition to holiday or birthday wish lists.
          But, as suspected, these devices have also been extremely dangerous.
          Over the last few months, we've been getting reports of these{" "}
          <Link to="/blog/exploding-hoverboards" target="_blank">
            {" "}
            hoverboards exploding
          </Link>
          , catching fire, causing burn injuries, property damage and
          contributing to serious fall-related injuries.
        </p>
        <p>
          It is also becoming increasingly clear that a number of hoverboard
          injuries are being caused by devices that are improperly made or
          manufactured using substandard parts and accessories. If you or a
          loved one has been injured by a defective or malfunctioning
          hoverboard, it is important that you contact a knowledgeable Irvine
          hoverboard injury lawyer to evaluate your options. Call (949) 203-3814
          for a free consultation.
        </p>
        <h2>If You Have Been Injured</h2>
        <p>
          If you or a loved one has been injured in a hoverboard-related
          accident, explosion or fire, it is critical that you preserve the
          hoverboard or its remains so it can be thoroughly examined by an
          expert for evidence of defects and design flaws. Be sure to preserve
          the charger and battery since they could prove to be valuable pieces
          of evidence as well. Get prompt medical attention, treatment and care
          for your injuries. Save all receipts for expenses relating to your
          hoverboard injury. Contact an experienced Irvine{" "}
          <Link to="/defective-products">product defect attorney </Link> who
          will ensure that your rights are protected and that the manufacturer
          is held accountable.
        </p>
        <p>
          The experienced hoverboard injury lawyers at Bisnar Chase can help you
          seek and obtain compensation for damages including medical expenses,
          lost wages, hospitalization, rehabilitation, pain and suffering and
          emotional distress.{" "}
        </p>
        <h2>How a Hoverboard Works</h2>
        <p>
          The word "hoverboard" might conjure images of a rider levitating or
          hovering above ground as Michael J. Fox's character Marty McFly did in
          the sci-fi flick "Back to the Future II." However, in reality, what we
          call hoverboards today are segway-like devices that roll on two
          wheels. They are basically self-balancing scooters. Inside these
          scooters are a gyroscope, microprocessors and two or more independent
          motors that help balance the board. Hoverboards are powered by
          rechargeable lithium-ion batteries.
        </p>
        <h2>Hoverboard Injury Risks</h2>
        <p>
          There have been several reports nationwide of these hoverboards
          exploding or catching fire. Both injuries and property damage have
          been reported as a result of these explosions and fires involving
          hoverboards. A number of these fires occurred when the hoverboard was
          charging or when it was overcharged.{" "}
        </p>
        <p>
          However, there were also cases where the boards exploded even when
          they were not overcharged. The U.S. Consumer Product Safety Commission
          (CPSC) has{" "}
          <Link
            to="http://www.cpsc.gov/en/About-CPSC/Chairman/Kaye-Biography/Chairman-Kayes-Statements/Statements/Statement-from-the-US-CPSC-Chairman-Elliot-F-Kaye-on-the-safety-of-hoverboards/"
            target="_blank"
          >
            {" "}
            launched a probe into what is causing these explosions and fires
          </Link>
          . Such incidents could result in serious burn injuries, broken bones,
          disfigurement and internal organ damage.
        </p>
        <p>
          These hoverboard explosions and fires have been attributed to cheap,
          poorly manufactured lithium-ion batteries most of which are exported
          from China. A hoverboard or battery that is made with substandard
          parts or wiring also poses a heightened risk of a fire or an
          explosion.
        </p>
        <p>
          Hoverboards also pose the risk of other serious physical injuries.
          CPSC officials have announced receiving numerous reports of
          fall-related injuries from emergency rooms nationwide. Common
          fall-related injuries caused by hoverboards include concussions, bone
          fractures, contusions, abrasions and internal organ injuries.
        </p>
        <h2>New California Laws</h2>
        <p>
          A new law, which went into effect January 1, 2016, in California,
          requires that hoverboard riders must be 16 years old to operate the
          devices in public. They are also required to wear a helmet and ride
          only on streets where the speed limit is under 35 mph. Anyone who
          violates those restrictions or rides under the influence or alcohol
          and/or drugs could face a $250 fine. In addition to this law,
          Metrolink riders cannot carry hoverboards on trains. The California
          Highway Patrol will also enforce the new law.{" "}
        </p>
        <h2>Who Can Be Held Liable</h2>
        <p>
          A number of parties can be held liable for injuries and damages caused
          by defective or malfunctioning hoverboards including manufacturers,
          wholesalers, distributors and retailers. A product may be deemed
          defective if it has a manufacturing defect, design defect or a lack of
          adequate warning of known safety hazards.{" "}
        </p>
        <p>
          A plaintiff in a product liability lawsuit must prove that the product
          was defective, that it caused his or her injuries and that he or she
          suffered losses as a result of the injury. Hoverboards are currently
          unsafe because they are manufactured without any proper regulation or
          oversight. In spite of knowing this fact, manufacturers market and
          sell these products as safe.
        </p>
        <p>
          If you have received injuries from an exploding hoverboard please
          contact our law firm for a free case evaluation. Call (949) 203-3814.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.128531086887!2d-117.8691849076402!3d33.66213583862208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1453155619865"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
