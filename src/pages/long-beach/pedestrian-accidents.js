// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Pedestrian Accident Attorneys - Long Beach Hit & Run Lawyers"
        pageDescription="Were you injured in a Long Beach pedestrian accident? Please call 949-203-3814 for a free attorney consultation. Justice for the injured since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Pedestrian Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Recent government reports have shown that the conditions in many of
          California's nursing homes leave much to be desired. Nursing home
          residents suffer from severe neglect not to mention lack of attention.
          Task forces that conducted random inspections of Los Angeles area
          nursing homes also found falsification of patient records and
          medication mix-ups. When you visit your loved one at a Long Beach
          nursing home, there are warning signs of abuse and neglect for which
          family members should look out. If you find that your loved one has
          been suffering signs of nursing home abuse, consulting a{" "}
          <Link to="/nursing-home-abuse">Long Beach nursing home attorney</Link>{" "}
          will be a necessary step towards obtaining justice. The following
          information includes how to spot signs of neglect.
        </p>
        <h2>Signs of Neglect</h2>
        <p>
          All Long Beach nursing homes have a legal obligation to provide a
          level of care and attention that will keep their residents healthy and
          comfortable. When a facility does not have a large enough staff or
          adequate supervision, there is the potential for neglect to occur.
          When visiting your loved one, ask yourself these questions:
        </p>
        <ul>
          <li>
            Is the facility clean? Infections can spread through a facility that
            lacks cleanliness. Soiled materials should be cleaned immediately.
            Towels and clothes should not be placed on the floor.
          </li>
          <li>
            Is the room tidy? When rooms are dirty, it may be a sign that the
            residents are not receiving enough individual attention.
          </li>
          <li>
            Is the staff paying attention to your loved one's personal hygiene?
            Are proper baths being given? Are his or her clothes properly
            laundered? Being dirty may not necessarily result in an injury,
            illness or infection, but it is symptomatic of neglect that is
            potentially dangerous.
          </li>
          <li>
            Are the medications being administered properly? Unusual doses of
            medications may be a sign that your loved one is not given the
            medical care he or she needs. Also, when your loved one is being
            given anti-psychotic medications unnecessarily, it could be a sign
            of chemical restraint.
          </li>
          <li>
            Has your loved one lost a significant amount of weight? Dehydration
            and malnutrition are serious issues that can lead to illness or
            death. Care facilities must not only provide adequate nutrition, but
            also ensure that the meals and drinks are being provided in a timely
            manner.
          </li>
        </ul>
        <h2>Signs of Abuse</h2>
        <p>
          There are many forms of abuse that must be reported to the authorities
          right away. If you notice any of the following signs of abuse, do not
          hesitate to contact the authorities:
        </p>
        <ul>
          <li>
            Unexplained injuries. Whenever a resident falls down or suffers an
            injury, it should be on their medical records and you should be
            contacted. When a resident has unexplained bruises or broken bones,
            it may be a sign of abuse and an attempt to cover it up.
          </li>
          <li>
            Restraints and overmedication. Some facilities may wrongfully
            restrain their residents through physical restraints or through over
            medication. Drug-induced sedation and physical confinement are
            serious forms of abuse that can have fatal consequences.
          </li>
          <li>
            Lost belongings. It is common for residents to have items and
            belongings stolen from their room. In the most severe cases of
            financial abuse, an elderly person may have their checks stolen or
            their wills changed.
          </li>
          <li>
            Severe changes in personality. Emotional abuse is more difficult to
            detect than physical abuse. The scars of belittling, threatening,
            insulting and berating are internal. Victims may become isolated,
            quiet or quick to aggression. These sudden changes in personality
            should be taken seriously. Something tragic may have occurred or
            your loved one may be suffering from regular mistreatment.
          </li>
        </ul>
        <h2>Know Your Rights</h2>
        <p>
          Under California law, all concerns over physical abuse must be
          reported to law enforcement. Concerned loved ones can also contact:
        </p>
        <ul>
          <li>
            The Los Angeles County Adult Protective Services agency. They have a
            link on their official site to report abuse.
          </li>
          <li>
            The Licensing and Certification Program of the California Department
            of Health Services.
          </li>
          <li>
            Long-Term Care Ombudsman's Office. Officials with the local
            Ombudsman Programs can offer assistance on the legal options
            available.
          </li>
          <li>
            California Attorney General Bureau of Medi-Cal Fraud and Elder Abuse
          </li>
        </ul>
        <p>
          The experienced{" "}
          <Link to="/nursing-home-abuse/elder-neglect">
            Long Beach nursing home abuse lawyers
          </Link>{" "}
          at our law firm are passionate about representing the rights of
          nursing home residents, victims and their families. We believe that
          nursing homes have a legal and moral obligation to provide quality
          care to their residents. If you or a loved one has been the victim of
          nursing home abuse or neglect in Long Beach, please contact us for a
          free and comprehensive consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
