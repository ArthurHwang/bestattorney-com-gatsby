// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/long-beach-personal-injury-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Long Beach",
      population: 469428,
      totalAccidents: 14172,
      intersection1: "Wardlow Rd & Long Beach Blvd",
      intersection1Accidents: 116,
      intersection1Injuries: 60,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 318.9,
      city1Name: "Signal Hill",
      city1Index: 312.2,
      city2Name: "Lakewood",
      city2Index: 228.6,
      city3Name: "Hawaiian Gardens",
      city3Index: 198.4,
      city4Name: "Seal Beach",
      city4Index: 113.0
    },
    locationBox3: {
      intersection2: "Cherry Ave & 7th St ",
      intersection2Accidents: 115,
      intersection2Injuries: 40,
      intersection2Deaths: 0,
      intersection3: "Redondo Ave & 7th St",
      intersection3Accidents: 115,
      intersection3Injuries: 43,
      intersection3Deaths: 1,
      intersection4: "Pacific Coast Hwy & 2nd St",
      intersection4Accidents: 121,
      intersection4Injuries: 41,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Call Bisnar Chase for your Long Beach injuries. We handle car accidents, dog bites and product liability cases and offer free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="long beach personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          For over thirty five years Bisnar Chase has been representing Long
          Beach residents in a variety of serious injury cases including dog
          bites, <Link to="/long-beach/car-accidents">car accidents </Link> and
          product liability. Our firm has a 96% success rate and doesn't charge
          you a fee unless we win your case. If in the end we have not won for
          you, there is no fee. Our trial lawyers bring decades of experience
          and our client's satisfaction is always our top priority. We have
          built a reputation on results.
        </p>
        <p>
          If you've been injured in an accident at no fault of your own, you owe
          it to yourself to speak with an experienced Long Beach personal in
          jury lawyer. One that understands injury law in and out and the courts
          in Los Angeles.
        </p>

        <h2>Recent Victories</h2>
        <ul>
          <li>$9,800,000.00 - Motor vehicle accident</li>
          <li>$8,500,000.00 - Motor vehicle accident - wrongful death</li>
          <li>$7,998,073.00 - Product liability - motor vehicle accident</li>
          <li>$5,000,000.00 - Auto Defect</li>
          <li>$4,250,000.00 - Product Liability</li>
        </ul>

        <h2>Do I Qualify for a Personal Injury Case?</h2>
        <p>
          We are plaintiff attorneys. If you've been injured or had an accident
          in Long Beach at the fault of another then we can represent you. We do
          not represent insurance companies or the at-fault party. We will
          evaluate your case and tell you a honest legal opinion. We have the
          reputation, resources and experience to represent even the most costly
          personal injury cases such as: wrongful death, auto defects, product
          liabilities, auto accidents, hit and run and premises liability issues
          like slip and falls.
        </p>
        <h2>First Class Representation</h2>
        <p>
          One of the patterns we see repeated in{" "}
          <Link to="/about-us/testimonials" name="client testimonials">
            client interviews
          </Link>{" "}
          is the satisfaction of how they were treated. Each client at Bisnar
          Chase is treated like family from beginning to end. Once you are a
          client of ours we will be there for you whenever you need us; even
          after your case is closed.
        </p>
        <h2>Long Beach Practice Areas</h2>
        <p>
          We handle a wide variety of injuries and accidents but we excel in
          serious and catastrophic cases. While we take a standard car accident
          or dog bite case, we are known for going after the wrongdoer's who've
          seriously injured someone or caused a wrongful death. We know LA
          County including Long Beach courts. This is an important relationship
          for your attorney to have and it can be the difference between winning
          and losing.
        </p>
        <h2>Meet Your Personal Injury Attorney</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/john-bisnar-long-beach.jpg"
              alt="John Bisnar"
              className="imgleft-fixed"
            />
          </LazyLoad>{" "}
          <Link to="/attorneys/john-bisnar">John Bisnar </Link> is a nationally
          known personal injury attorney practicing in California. John's
          expertise can be seen across the country in the form of publications,
          speaking engagements and training other attorneys on how to run a
          successful practice.
        </p>
        <p>
          John takes every case very personal and wants the best compensation
          possible for his clients. In John's words the client's are the
          backbone of our entire law practice. When you meet with John Bisnar
          you'll see right away that he is an extremely caring, compassionate
          and results oriented attorney.
        </p>
        <p>
          Schedule a free consultation. You have nothing to lose and everything
          to gain by speaking with personal injury experts who are extremely
          knowledgeable and familiar with Long Beach courts, defense attorneys
          and insurance companies.
        </p>
        <h2>Expect Top Notch Legal Care</h2>
        <ul>
          <li>
            Over 500 Million dollars recovered in settlements and verdicts.
          </li>
          <li>Successfully representing Californians since 1978.</li>
          <li>Serving Long Beach, and LA County.</li>
          <li>
            Over 12,000 clients including catastrophic and serious injuries
          </li>
          <li>We are trial lawyers. Not all personal injury attorneys are.</li>
          <li>
            We will come to you if you are unable to come in our LA office
          </li>
          <li>FREE consultation and no pressure interview.</li>
          <li>
            Always <Link to="/giving-back">giving back </Link> to our
            communities.
          </li>
        </ul>
        <p>
          {" "}
          <Link to="/contact">
            Contact our Long Beach Personal Injury Attorneys
          </Link>{" "}
          6701 Center Drive West, 14th Fl. Los Angeles, CA 90045 (323) 238-4683
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
