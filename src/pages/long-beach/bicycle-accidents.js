// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Bicycle Accident Lawyer. Free Consultation. 323-238-4683"
        pageDescription="Call 323-238-4683 for top rated bicycle accident lawyers in long beach."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Bicycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Many cities have implemented wider bike lanes marked in green for more
          visibility. With so many bicycle and pedestrian accidents in Los
          Angeles, the county knows it must do something to protect the safety
          of its residents.
        </p>
        <p>
          Bicycle accidents are common in the beach cities. With so many
          tourists and pedestrians, the road is crowded and driver's aggressive.
          A bicycle accident can be a difficult injury claim. Typically because
          the driver blames the bike rider for disobeying laws or being
          reckless. We've taken on some very tough bicycle accident injury
          claims and have had great results.
        </p>
        <ul>
          <li>35 years representing Los Angeles</li>
          <li>Highest rated trial lawyers</li>
          <li>Over $500 Million in settlements and verdicts</li>
          <li>'Best Place to Work' 4 years in a row</li>
          <li>Passionate and professional staff</li>
          <li>96% success rate</li>
        </ul>
        <p>
          The <Link to="/bicycle-accidents">bicycle accident lawyers </Link> of
          Bisnar Chase have decades of experience settling and litigating
          bicycle accidents. if' you've been injured in a bike accident in long
          Beach, contact our legal team for a free consultation. You may be
          entitled to compensation.{" "}
          <strong> Call 323-238-4683 for a free consultation today.</strong>
        </p>
        <h2>Bicycle Accident Injuries</h2>
        <p>
          Broken bones and concussions are common injuries from a bicycle
          accident. Many times, even while wearing a helmet the victim suffers
          serious injuries often times requiring months of medical treatment.
        </p>
        <p>
          A body is no match for a 2000 pound vehicle and your injuries will be
          a big part of your personal injury claim. Depending on the severity,
          you may need months of physical therapy and treatment. Our team works
          with medical providers to shield you from cost until your case is won.
        </p>
        <h2>Common Bicycle Accidents</h2>
        <p>
          We all know that we should obey the rules of the road but many people
          take bike safety for granted. Many of the cases we have represented
          have been cyclists riding the wrong way, riding on sidewalks and using
          an entire lane. These are deadly mistakes and often have catastrophic
          results. Typically you should never ride against the flow of traffic
          and be aware of the door zone of parked cars.
        </p>
        <h2>Passionate Legal Team</h2>
        <p>
          If you or anyone you know has been a victim of a bicycle accident you
          should contact an experienced Long Beach personal injury attorney who
          can help you with your case. Bisnar Chase has always put our clients
          first and we work tirelessly to win your case. Our team of 7 trial
          lawyers work as a cohesive group who all have the same goal in mind.
          To win your case. Our prelitigation and litigation team are one of the
          best with decades of success. Contact us today and see for yourself
          why past clients continue to refer loved ones to us.
        </p>
        <p>
          <strong> Los Angeles Office</strong>
        </p>
        <div className="mb">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th Fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">90045</span>
            <div>
              <span className="tel">(323) 238-4683</span>
            </div>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
