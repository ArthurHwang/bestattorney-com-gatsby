// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Car Accident Lawyer - Los Angeles Auto Accident Attorney"
        pageDescription="Call 949-203-3814 for highest-rated car accident attorneys serving Long Beach California. Specialize in serious injury and wrongful death related to auto accidents. Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <h2>Passion, Trust, Results Since 1978</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys have been helping accident
          victims for over 35 years and when it comes to local courts and rules,
          our roots in Los Angeles and Orange County put us squarely in your
          favor.{" "}
          <strong>
            {" "}
            Call 323-238-4683 for immediate assistance or to answer any
            questions you may have about our firm.
          </strong>
        </p>
        <p>
          We have two offices, both just miles from either end of Long Beach.
          Most information can even be obtained over the phone. If you're
          looking for an experienced car accident attorney in Long Beach, call
          us today for a free case review.
        </p>
        <p>
          Should you ever fall victim to a car accident in Long Beach,
          California, experienced{" "}
          <Link to="/long-beach">Long Beach personal injury lawyers </Link> will
          advise you seek the advice of a knowledgeable lawyer -- before you
          sign papers and settle with insurance company adjusters.
        </p>
        <h2>Understand Car Accident Lawyer Fees</h2>
        <p>
          Long Beach car accident lawyers will tell you, should you ever fall
          victim to a car accident, is to consult a knowledgeable car crash
          lawyer. What you will discover is that they are eager to discuss their
          fees, and whether or not they can be of help in your particular case.
          Remember, fees are usually paid on a contingency basis, which means
          <strong> you pay only if the attorney wins the case</strong>.
        </p>
        <h2>Insurance Companies Have Their Agenda</h2>
        <p>
          One thing accident lawyers tell most potential clients is that
          insurance companies have their own agenda, which is to constantly seek
          ways to reduce your compensation. That's why when you deal with
          adjusters, investigators, and insurance company lawyers assigned to
          your{" "}
          <Link to="/resources/negotiating-your-personal-injury-claim">
            car accident claim
          </Link>
          , you'll get the feeling they're not looking out for you.
        </p>
        <h2>An Auto Accident Lawyer Can Help with Endless Details</h2>
        <p>
          Car accident claims are beset by seemingly endless rules, regulations
          and complex procedures that are intimidating to accident victims. This
          is why it's so important to seek the advice of an experienced car
          accident lawyer. Only they can help you wade through the stacks of
          paperwork, court motions and other legal maneuvers necessary to
          compensate you for your pain and expenses.
        </p>
        <p>
          So how does one choose a competent car crash lawyer? Keep in mind that
          this individual will be a staunch advocate for your rights and that
          you will need to depend on him or her as a skilled negotiator. This is
          why it's so important to learn how they work, their track record, and
          how they are regarded by their peers in the practice of car accident
          law. You should also inquire about all your options. And never feel
          pressured into doing anything. Once you retain them, they should
          always treat you with respect and courtesy, promptly returning your
          phone calls.
        </p>
        <p>
          <strong> LA County Office</strong>
        </p>
        <div className="vcard mb">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th Fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">90045</span>
            <br></br>
            <span className="tel">323-238-4683</span>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
