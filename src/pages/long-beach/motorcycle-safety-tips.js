// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle='Long Beach Police Launch "Motorcycle Safety Enforcement Operation"'
        pageDescription="New program launched by Long Beach police may help prevent motorcycle accidents. Injured in a Long Beach motorcycle collision? Call 949-203-3814 to speak with an experienced attorney for free with no obligation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Long Beach Police Launch "Motorcycle Safety Enforcement Operation" -
          Bisnar Chase Provides Motorcycle Safety Tips
        </h1>
        <BreadCrumbs location={location} />
        <p>
          According to everythinglongbeach.com, the Long Beach police department
          launched their "Motorcycle Safety Enforcement Operation" on Saturday,
          June 30 to help reduce the number of motorcycle accident related
          injuries and deaths.
        </p>
        <p>
          The program is reported to target areas that are prone to serious
          injury motorcycle collisions. Increased law enforcement will be
          present where Long Beach motorcyclists tend to gather.
        </p>
        <p>
          Officers are expected to be especially vigilant towards motorists who
          commit traffic violations.
        </p>
        <p>
          Brian Chase, Bisnar Chase Long Beach motorcycle attorney, has been
          assisting injury victims in Long Beach for decades. "Any special
          attention that results in increased awareness for motorcyclists in
          Long Beach is a step in the right direction. In the case of the Long
          Beach police department's newest motorcycle safety program, my staff
          and I are optimistic as efforts have been made to focus on limiting
          minor violations in areas prone to collisions.
        </p>
        <p>
          "Too many motorcyclists are killed and injured, as a result of other
          drivers committing minor traffic violations. It is not uncommon for
          these types of violations to result in minor collisions, and riders
          have very little protection," says Brian Chase, a lifelong motorcycle
          enthusiast.
        </p>
        <p>
          According to the 2008 and 2009 California Statewide Integrated Traffic
          Records System (SWITRS), motorcycle accident injuries and deaths are
          on the rise. In 2008, 2 people were killed and 100 were injured in
          Long Beach motorcycle crashes. In 2009, 5 people were killed and 86
          suffered injuries.
        </p>
        <p>
          The U.S. Department of Transportation/National Highway Traffic Safety
          Administration lists head injuries as the number one cause of death in
          motorcycle crashes. Wearing a helmet, as well as other protective
          gear, is part of the motorcycle safety tips distributed to Bisnar
          Chase contacts.
        </p>
        <h2>Motorcycle Safety Tips Provided By Bisnar Chase Law Firm</h2>
        <ul>
          <li>
            Wear a Helmet
            <ul>
              <li>
                The National Highway and Traffic Safety Administration has found
                motorcycle helmets to be close to 37 percent effective in
                preventing fatal injuries amongst motorcyclists. Wearing a
                helmet is required by California Vehicle Code Section 27803.
              </li>
            </ul>
          </li>
          <li>
            Ride at Your Own Pace
            <ul>
              <li>
                Beginners should be extra cautious. Knowing your personal limits
                can be the difference between life and death for motorcyclists.
              </li>
            </ul>
          </li>
          <li>
            Stay Educated
            <ul>
              <li>
                Because of the increased danger that motorcyclists are exposed
                to, it becomes important to consider safety classes, especially
                if you have not used your bike in a while.
              </li>
            </ul>
          </li>
          <li>
            Eliminate Distractions
            <ul>
              <li>
                Riders should be prepared to take evasive action at all times.
                Consider storing mobile phones and saving playlists.
              </li>
            </ul>
          </li>
          <li>
            Keep your Cargo Contained
            <ul>
              <li>
                If you are carrying extra cargo, make sure it is properly
                stored. If you begin to lose items, you may make a fatal
                mistake.
              </li>
            </ul>
          </li>
        </ul>
        <h2>
          About: Bisnar Chase Motorcycle Accident Attorneys, Southern California
        </h2>
        <p>
          Bisnar Chase is a California based motorcycle accident law firm that
          has developed a reputation as experienced legal professionals who
          produce consistent proven results. The Firm has been featured on CBC,
          Fox 11, and NBC. Their{" "}
          <Link to="/motorcycle-accidents">
            California motorcycle accident attorneys
          </Link>{" "}
          have assisted over 12,000 clients and have recovered hundreds of
          millions of dollars in compensation.
        </p>
        <p>
          Join Brian Chase, Jay Leno and Peter Fonda at the Love Ride 29 to
          Benefit the USO on October 21, 2012. Go to LoveRide.org to sign up.
        </p>
        <p>
          For a free legal motorcycle injury consultation call 949-203-3814 or
          visit their website at <Link to="/">/</Link>.
        </p>

        <p>
          {" "}
          <Link to="/motorcycle-accidents">
            Click Here For More Motorcycle Accident Press Releases
          </Link>{" "}
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
