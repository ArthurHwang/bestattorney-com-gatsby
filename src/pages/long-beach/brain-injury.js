// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Brain Injury Lawyer - 323-238-4683"
        pageDescription="contact the top rated brain injury lawyers in Long Beach for a free consultation. 96% success rate.contact the top rated brain injury lawyers in Long Beach for a free consultation. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Brain Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <h2>Protecting Victims' Rights</h2>
        <p>
          The experienced Long Beach{" "}
          <Link to="/head-injury">brain injury lawyers </Link> at our firm
          understand the tremendous challenges injured victims and their
          families face. We have seen first-hand the emotional roller-coaster
          ride these families go through and the financial challenges they face.
        </p>
        <p>
          If you or a loved one has suffered a brain injury as the result of
          someone else's negligence or wrongdoing, you may be able receive
          compensation for your losses. Please contact us for a free,
          comprehensive and confidential consultation.
          <strong> Call 323-238-4683.</strong>
        </p>
        <ul>
          <li>
            <strong> Free case evaluation</strong>
          </li>
          <li>
            <strong> We fight for maximum compensation</strong>
          </li>
          <li>
            <strong> Serving Los Angeles since 1978</strong>
          </li>
          <li>
            <strong> Over $500 Million in verdicts and settlements</strong>
          </li>
          <li>
            <strong> Served over 12,000 clients</strong>
          </li>
        </ul>
        <h2>Trial Lawyers Experienced in Brain Injury Cases</h2>
        <p>
          Anyone who has suffered a brain injury as the result of someone else's
          negligence or wrongdoing would be well advised to contact a Long Beach
          brain injury lawyer to obtain more information about protecting your
          legal rights.{" "}
          <strong>
            {" "}
            Our attorneys are experienced in dealing with traumatic and
            catastrophic cases
          </strong>{" "}
          and we are familiar with Los Angeles courts and defense lawyers. We've
          spent decades fighting the insurance industry to obtain fair
          compensation for our clients.
        </p>
        <h2>Location of the Injury</h2>
        <p>
          Depending on how and where the brain injury occurred, the victim may
          experience a range of debilitating symptoms. Here is a breakdown of
          the sections of the brain and the symptoms that may result from trauma
          to that specific region of the brain:
        </p>
        <ul>
          <li>
            Cerebellum: Did the victim fall backwards and strike his or her
            lower head on the pavement or on a vehicle? If the base of the skull
            suffered trauma, the cerebellum may have been damaged. In such
            cases, the victim may suffer from dizziness, tremors or even the
            loss of the ability to walk. Victims of trauma to their cerebellum
            may also experience vertigo or slurred speech.
          </li>
          <li>
            Brain stem: Was the impact so severe that trauma was experienced
            deep within the brain? In such cases, the victim may have
            difficulties breathing, swallowing, balancing or sleeping.
          </li>
          <li>
            Temporal lobes: Was the injury the result of a broadside or
            side-impact collision? If the sides of the head are impacted in the
            collision, then, the victim may suffer from memory issues. Some
            victims of temporal lobe trauma have to deal with changes in
            personality and aggressive behavior.
          </li>
          <li>
            Occipital lobes: Did an object strike the back of the victim's head?
            When this occurs, there is the potential for the victim to suffer
            vision problems. In some cases, victims of occipital lobe trauma may
            also struggle with recognizing words or drawn objects.
          </li>
          <li>
            Parietal lobe: Did a heavy object fall onto the top or back of the
            victim's head? When the parietal lobe is damaged, the victim may
            suffer from issues with hand-eye coordination. In some cases,
            parietal lobe damage can result in issues with reading, writing and
            drawing.
          </li>
          <li>
            Frontal lobe: Did the victim slam his head against the windshield or
            steering wheel in a Long Beach auto accident? In such cases, the
            resulting symptoms can include paralysis or significant changes in
            social behavior. Other symptoms include an inability to focus on a
            task, loss of flexibility in thinking and mood changes.
          </li>
        </ul>
        <h2>Treatment for Brain Trauma</h2>
        <p>
          Immediately following a{" "}
          <Link to="/head-injury/traumatic-brain-injury">
            <strong> traumatic brain injury</strong>
          </Link>
          , medical professionals will have to determine the severity of the
          trauma. It is likely that they will have to take steps to reduce any
          swelling that has occurred and surgery may be necessary. After the
          initial treatment, the victim will likely have to stay in the hospital
          for a considerable amount of time. After that period, brain injury
          victims typically get sent to a rehabilitation center to begin
          learning how to deal with their disabilities. With intensive therapy,
          it may be possible to recover some physical and mental abilities. Some
          patients may need long-term rehabilitation.
        </p>
        <p>
          Victims of physical disabilities may need extensive physiotherapy.
          Victims may also need occupational therapy to relearn how to perform
          basic tasks. Other brain injury victims require speech therapy to
          relearn how to communicate. There are hospital-based medical
          rehabilitation programs and outpatient rehabilitation services as
          well. There are also transitional and supported living programs for
          traumatic brain injury victims. Unfortunately, many of these vital
          forms of treatment are not covered by insurance and can be extremely
          costly.
        </p>
        <h2>Compassionate Attorneys</h2>
        <p>
          The brain injury lawyers of Bisnar Chase have been representing
          victims of traumatic brain injuries for over three decades. We've
          taken on some of the most complex catastrophic cases in Los Angeles
          with great results for our clients. We may be able to help you too.
          Call our legal team today for a free consultation with compassionate
          trial lawyers. Call 323-238-4683.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
