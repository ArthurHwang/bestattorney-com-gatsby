// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Slip and Fall Lawyers - Slip and Fall Attorneys Long Beach"
        pageDescription="Long Beach slip and fall lawyers. Call 949-203-3814 for top slip and fall attorneys. Free consultations. High quality client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Slip and Fall Lawyers </h1>
        <BreadCrumbs location={location} />
        <p>
          Long Beach residents suffer their share of accidents. What are known
          as{" "}
          <Link to="/premises-liability/slip-and-fall-accidents">
            slip and fall accidents
          </Link>{" "}
          are consistently the leading cause of Long Beach area injury-producing
          incidents. They account for more than 1 million injuries each year in
          the United States. If you have been involved in a slip and fall
          accident, and have been severely injured, it might be time to consider
          hiring a <Link to="/about-us">slip and fall injury attorney</Link>.
        </p>
        <p>
          Ask yourself, why are there so many falls? Actually, this is not
          simple at all. There is quite a complexity in same-surface slip and
          fall accidents. The assumption usually tends to be that people are not
          paying attention.  They fall because the floor is slick, or because
          they are clumsy. They fall due to being careless, or because they step
          on some foreign object. These accidents lead to investigations that
          are one-dimensional and to repeated incidents at the same location.
        </p>
        <p>
          To determine responsibility for an accident, there are many possible
          causes. People fall for numerous reasons. Reasons include: interaction
          of the walking surface with shoes; the physical environment, area
          distractions; and the physical and mental limitations of the victim.
          Falls are consistently the leading cause of injury-producing
          accidents.
        </p>
        <p>
          Long Beach slip and fall cases fall under Long Beach premises
          liability law. There are generally four different types of personal
          injury slip and fall accidents:
        </p>
        <ol>
          <li>
            The person may lose footing, slip on the walking surface and fall, a
            characteristic &ldquo;Slip and Fall&rdquo;
          </li>
          <li>
            The person may stagger over an obstacle on the walkway or surface
            and fall, referred to as a &ldquo;Stump and Fall&rdquo;
          </li>
          <li>
            The individual may lose his/her sense of balance and fall due to a
            hole in the walkway or surface, or some type of unexpected defect,
            called a &ldquo;Step and Fall&rdquo;
          </li>
          <li>
            An individual may trip over a foreign object and fall, known as a
            &ldquo;Trip and Fall&rdquo;.
          </li>
        </ol>
        <p>
          Regardless of the location in Long Beach or any other city, the goal
          of the injured party and his/her attorney, is to demonstrate that the
          owner of the property is liable for the accident. To establish
          liability, the injured party must demonstrate that the property owner
          did not exercise reasonable care, acted negligently or had knowledge
          of the unsafe condition that caused the accident.
        </p>
        <p>
          In court, when an attorney is defending a slip and fall claim, a
          lawyer may deny that there was any negligence or claim that the
          injured party is actually the one at fault because if he/she had shown
          reasonable care for his/her safety, the hazard would have been seen
          and avoided.
        </p>
        <p>
          If a victim has been previously injured, is disabled in some other
          way, or is elderly, there is a legal issue that might well assume a
          physical infirmity had caused the accident. But assumptions can
          prevent seeing the actual cause. There is actually no "correct" way to
          stand or walk. What we view as correct posture and movement is often a
          matter of opinion or pleasing esthetics. Handicapped people sometimes
          need to use a high level of concentration in the walking process.
        </p>
        <p>
          Investigators of falls in Long Beach and other cities should
          understand the principles of human movement and have a working
          knowledge of floor materials, cleaning methods, and lighting.
        </p>
        <p>
          When reconstructing a slip-and-fall accident, avoid coming to any
          conclusion too quickly. The victim should be extensively interviewed,
          and the verbal report used to focus the investigation. The
          slip-resistance of the floor should be measured, and accurate
          measurements of the light intensity and contrast should be made.
        </p>
        <p>
          Long Beach building codes and various industry standards should be
          studied. Although slip and fall situations are seemingly the simplest
          of all accidents, they are not.
        </p>
        <p>
          If you have been injured in a slip and fall accident, it's important
          to enlist the services of an accomplished, expert slip and fall injury
          attorney at Bisnar Chase Personal Injury Attorneys. Call us at
          949-203-3814 or <Link to="/contact">contact us </Link> for a free
          consultation.
        </p>
        <h3 align="center">
          Free Long Beach office, hospital or home consultations by appointment
          only.
        </h3>
        <p style={{ textAlign: "center" }}>Call 949-203-3814</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
