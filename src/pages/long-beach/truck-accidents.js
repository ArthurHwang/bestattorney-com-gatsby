// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Truck Accident Attorneys - Long Beach Truck Collision Lawyers"
        pageDescription="Were you injured in a Long Beach truck accident? Call 949-203-3814 for attorneys who can help you achieve justice. Since 1978. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Thousands suffer serious injuries each year in crashes involving large
          trucks. Whenever a tractor-trailer strikes a vehicle, pedestrian,
          bicyclist or motorcyclist, there is the potential for life-changing
          injuries to occur. Very often, the occupants of the passenger vehicle
          involved in a truck accident are the ones who are seriously injured or
          killed. If you or a loved one has been injured in a big-rig crash, an
          experienced Long Beach truck accident lawyer can help you better
          understand your legal rights and options.
        </p>
        <h2>Truck Accident Statistics</h2>
        <p>
          According to the National Highway Traffic Safety Administration
          (NHTSA), 3,675 people were killed in large truck crashes in the United
          States in the year 2010. That reflects an 8.7 percent increase from
          the previous year. In the year 2010, 529 truck occupants, 2,790
          occupants of other vehicles and 356 others were killed.
        </p>
        <h2>The Legal Obligations of Truck Companies</h2>
        <p>
          There are many ways in which a truck company can prevent truck
          accidents from occurring. Whenever a Long Beach truck accident occurs,
          there are a number of significant questions that must be asked:
        </p>
        <ul>
          <li>
            Was the trucking company negligent in hiring the truck driver? It is
            the responsibility of the truck company to carefully examine the
            individual's driving history and background before hiring.
            Commercial drivers must be properly licensed and trained. Companies
            that hire drivers with a history of accidents or negligent driving
            can be held accountable by injured victims. Trucking companies are
            also required to randomly test their drivers for alcohol and drug
            use. When companies are lax when it comes to testing and
            enforcement, drivers are more likely to take advantage of the
            situation.
          </li>
          <li>
            Did the trucking company properly maintain the vehicle? Some truck
            accidents occur as the result of worn or damaged brakes, tires and
            other truck parts. It is the responsibility of every truck driver to
            perform an inspection of the vehicle before a long trip. At the same
            time, it is also the duty of truck owners or trucking companies to
            make sure that vehicle repairs and maintenance work are done in a
            timely manner. Truck companies that cut corners by refusing to
            maintain or repair their vehicles can be held accountable for their
            negligence.
          </li>
          <li>
            Was the trailer properly loaded? Improperly loading a trailer can
            cause a truck to veer out of control. If all the weight is to one
            side or at the front of the trailer, the truck may become difficult
            to handle. If the trailer is overloaded, it may become difficult to
            slow down. Some improperly loaded trailer incidents result in the
            trailer jackknifing or disconnecting from the truck resulting in
            devastating runaway trailer accidents.
          </li>
          <li>
            Are the truck drivers getting adequate rest between shifts? Truck
            drivers must keep a log of the hours they work. There are also
            federal laws that prevent them from working long hours and rules
            that require them to take adequate rest between shifts. Some
            companies do not actively monitor their drivers while others have no
            idea that their employees are putting people at risk by driving
            while fatigued. There are even companies that encourage drivers to
            work beyond the legally allowed hours of operation to make a
            deadline.
          </li>
        </ul>
        <h2>Holding Truck Companies Liable</h2>
        <p>
          Victims of Long Beach truck accidents can hold the at-fault party
          accountable for their losses. In some cases, the at-fault party may be
          a negligent truck driver. In other cases, there may be a number of
          other potentially liable parties including the truck driver's
          employer, trucking firm or the distribution company.  Injured victims
          of truck accidents can seek compensation from the at-fault parties for
          damages including but not limited to medical expenses, loss of wages,
          cost of hospitalization, rehabilitation and pain and suffering.
        </p>
        <p>
          The experienced{" "}
          <Link to="/contact">Long Beach truck accident lawyers </Link> at our
          law firm have a long history of helping injured victims and their
          families protect their rights, obtain just compensation and hold the
          wrongdoers accountable. If you or a loved one has been injured in a
          Long Beach truck accident, please contact us for a free consultation
          and a comprehensive case evaluation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
