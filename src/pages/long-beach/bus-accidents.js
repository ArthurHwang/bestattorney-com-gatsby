// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Bus Accident Lawyer - Long Beach Bus Crash Attorney"
        pageDescription="Call the experienced Long Beach bus accident lawyers of Bisnar Chase. 96% success rate, hundreds of millions won. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Bus Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          Do you regularly ride on the Long Beach Transit bus to travel down
          Cherry, Orange or Atlantic Avenue? Does your child ride a school bus
          every weekday? Do you ever use the Long Beach Passport bus to get to
          the Long Beach Museum of Art, 4th Street's Retro Row or the Aquarium
          of the Pacific?
        </p>
        <p>
          If riding the bus is an integral part of your life in the city of Long
          Beach, it would be in your best interest to know your rights and
          understand the steps to take if you are ever involved in a bus crash.
          One of the first steps to take is to contact a knowledgeable Long
          Beach <Link to="/bus-accidents">Bus Accident Attorney. </Link>
          <strong> Call 323-238-4683 for a free consultation</strong>. If we
          don't win your case, you don't pay!
        </p>
        <h2>Is Bus Travel Safe?</h2>
        <p>
          First, it is important to know that riding a bus is generally safe.
          Many bus collisions that may result in major injuries to occupants of
          other vehicles, may only result in minor injuries to bus occupants
          because the size and weight of the bus. It is important to remember,
          however, that many buses do not have seatbelts or other safety devices
          such as airbags that passenger vehicles normally do. Therefore, when a
          high-speed bus accident occurs, there is the potential for serious or
          even fatal injuries to occur. In the event of a powerful impact or
          rollover, bus passengers may suffer broken bone injuries. There is
          also the potential for other serious injuries such as brain trauma,
          spinal cord injuries, whiplash, neck and back injuries, lacerations
          and internal injuries.
        </p>
        <h2>What to do After a Long Beach Bus Crash</h2>
        <p>
          If you are an injured bus passenger or the occupant of another vehicle
          who has been injured in a bus collision, it is important that you take
          several important steps in order to protect your rights. First, the
          authorities must be notified right away. If the bus driver is unable
          to call the police, make sure that someone notifies the authorities
          immediately. If your neck, head or back hurts, it may be in your best
          interest to stay put until emergency crews arrive. If, however, you
          are able to move around, there are a few steps you can take to help
          protect your rights.
        </p>
        <ul>
          <li>
            <strong> Get contact information</strong>. Write down the bus line,
            bus company and the contact information for the bus company. Write
            down the bus driver's name as well and his or her commercial
            driver's license number. You may also want get the contact
            information of anyone who witnessed the accident whose testimony may
            prove invaluable in the future.
          </li>
          <li>
            <strong> Get on the police report</strong>. If many people were
            injured, you could get lost in the shuffle. Having your name on the
            police report as someone who was injured in the crash will help
            bolster your case.
          </li>
          <li>
            <strong> Write down the details</strong>. You never know what piece
            of information will prove useful. Write down where and when the
            accident occurred. Where were you seated? Where were you going? What
            happened when the collision occurred?
          </li>
          <li>
            <strong> Take photos.</strong> If you have a phone that can take
            photos, use it. Photograph the bus, other damaged vehicles, the
            crash site and your injuries.
          </li>
          <li>
            Seek medical attention. Seeing a doctor right away will increase
            your chances of a full recovery. Your medical records will also
            serve as legal documentation of your injuries.
          </li>
          <li>
            <strong> Keep a journal</strong>. Write down your day-to-day
            struggles since the accident. In what ways are you suffering? How
            has the accident negatively affected your life? Are you in
            significant pain? Have you missed out on financial or career
            opportunities because of your injuries?
          </li>
          <li>
            <strong> Speak with an attorney.</strong> Bus companies have a legal
            obligation to provide the utmost duty of care to their passengers.
            Unfortunately, it is common for bus companies to shirk this duty by
            offering a quick and cheap settlement that does not cover the cost
            of the medical bills and lost wages.
          </li>
        </ul>
        <h2>Compensation for Bus Crash Victims</h2>
        <p>
          The Long Beach bus accident lawyers at our personal injury law firm
          fight hard to ensure that injured victims and their families are
          fairly compensated for their injuries and losses. Bus accident victims
          can seek compensation to cover damages including but not limited to
          medical expenses, lost wages, pain and suffering, loss of earning
          potential and emotional distress. Please contact us for a free
          consultation and comprehensive case evaluation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
