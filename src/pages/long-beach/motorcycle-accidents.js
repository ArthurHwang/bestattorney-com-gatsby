// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Motorcycle Accident Lawyer"
        pageDescription="Call 949-203-3814 for Long Beach motorcycle accident attorneys who can help. Free consultations. Great client care."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Have you been involved in a motorcycle accident? If you have, chances
          are you have been injured as a result. Injuries due to motorcycle
          accidents can sometimes be life threatening, and other times injuries
          can be so severe that they become lifetime changes that you have to
          adapt to. Unfortunately, someone else's negligence can contribute to
          some of the most disastrous times of our lives. In these situations,
          costs can become astronomical and out of the realm of feasibility for
          most people to overcome. Fortunately, there are solutions and ways
          around this. The best thing you can do is contact a Long Beach{" "}
          <Link to="/motorcycle-accidents">motorcycle accident lawyer</Link>.
          They will be able to help you with more than just telling you you have
          a case. They will be your advocate and a strong member of your team as
          you tavel down the often harsh, difficult road of recovery. Let's take
          a look at some of the latest information about motorcycle accidents.
        </p>
        <p>
          Teams of researchers from universities and the National Highway
          Traffic Safety Administration are going out to the scenes of
          motorcycle crashes nationwide that various police agencies are
          actively investigating. Their goal is to gather the latest data. Each
          analysis plans to comprise a detailed examination of the motorbike and
          all its components. They plan to interview the rider if he survived
          the crash, and check out the various rider and passenger impact
          points. Interviews will be with riders who crashed as well as bikers
          who run the same route who avoided collisions. Assessment of health
          accounts are included and a lot more. Each accident study will require
          study of 300 incidents and is anticipated to take until the year 2013
          to complete.
        </p>
        <p>
          Other experiments underway include studying whether safety training
          actually saves lives, or whether just gaining skill through practice
          plays a more significant role in avoiding a incidents. Researchers are
          also examining directions bikers look as they ride, and if visual
          scanning abilities can be honed.
        </p>
        <p>
          An investigative assembly is checking at dissimilar groups of
          enthusiasts such as cruisers, sport bikers, touring drivers and others
          to decide if any particular group have habits universal to the initial
          group putting them at additional danger of incidents than the others.
          Lastly, study by the MSF will use digital video and data devices to
          confirm rider performance in an effort to see what riders do to evade
          crashes and what they do incorrectly when they smash up.
        </p>
        <p>
          It's a new year filled with new laws, especially in regards to
          motorcycle safety. California is unique in that this state has a
          completely different set of rules than the rest of the country.
        </p>
        <h2>Among the new legislation that has gone into effect:</h2>
        <p>
          A new, required motorcycle safety course has been instituted for
          under-21 motorcycle permit applicants. This new law requires any
          individual under the age of 21 to fully attend and complete a state
          regulated motorcycle safety course. This must be completed and
          certified  before the person is issued a permit with which they can
          practice driving a motorcycle. It further requires the permit must be
          held for a full six months before the person can be issued a
          California class M motorcycle driver license. Currently over 6,000
          drivers who are 19 years and younger are licensed to ride a motorcycle
          in California.
        </p>
        <p>
          There are a series of new oversights regarding the flood of online
          traffic violator schools. Prior to this new law the DMV only licensed
          "brick and mortar" classroom traffic violator schools. This new law
          will bring court-approved courses, such as online and home study
          traffic schools into the DMV Traffic Violator School licensing program
          and implements a number of preferences included in a DMV study of
          traffic violator school issues. This law will have a 3-year
          implementation process.
        </p>
        <p align="center">
          <strong> New Laws to be aware of.</strong> Remember, there are new
          traffic laws that will affect you.{" "}
          <u>Text directly from the State of California Site.</u>
        </p>
        <ul>
          <li>
            <em>
              <strong>Organ Donation </strong>(SB 1395 / Alquist) Starting July
              1, the language on the DMV's application offers the driver license
              applicant the option to register as a prospective organ donor in
              the organ donor program or to defer that decision to a later time.
              This change will now include in the organ donor registration field
              of the application check boxes indicating (A) Yes, add my name to
              the donor registry or (B) I do not wish to register at this time,
              thus ensuring that the applicant's failure to check the box is not
              simply an oversight.
            </em>
          </li>
          <li>
            <em>
              <strong> Local Traffic Ordinances</strong> (SB 949 / Oropeza) On
              July 1, in an effort of ensuring that traffic convictions are
              recorded by the Department of Motor Vehicles, local authorities
              may not enact or enforce a local ordinance on any matter covered
              by the California Vehicle Code.
            </em>
          </li>
          <li>
            <em>
              <strong> Driving Under the Influence</strong> (AB 1601 / Hill)
              Effective in January 1, 2012, this bill authorizes a court to
              order a 10-year revocation of the driver license of a person
              convicted of a third or subsequent DUI violation, with possible
              reinstatement after five years if specified conditions are met.
            </em>
          </li>
        </ul>
        <p>
          If you have been involved in a motorcycle accident and have suffered
          severe motorcycle accident injury, then you will want to contact the
          experienced Long Beach motorcycle accident attorneys at Bisnar Chase
          Personal Injury Attorneys. You may call us at 949-203-3814 or{" "}
          <Link to="/contact">contact us </Link> for a free consultation and
          more information regarding your situation. But don't delay! While you
          will want to spend time choosing an attorney, you still have
          limitations on the amount of time that you have to file your case. The
          amount of time varies between different types of cases, but to find
          out what applies to you, you will want solid advice from an
          experienced Long Beach motorcycle accident lawyer.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
