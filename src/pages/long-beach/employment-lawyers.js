// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Employment Lawyer - Employment Attorney Long Beach"
        pageDescription="Wrongfully terminated? Call 323-238-4683 for top Long Beach employment attorneys. Highest quality client care and free consultations since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Employment Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Sexual harassment is a unique type of discrimination at the workplace,
          which involves unwelcome sexual attention. There are many acts of
          harassment that are unacceptable at the workplace and this type of
          abuse can affect an employee emotionally and psychologically.
        </p>
        <p>
          Anyone who has experienced sexual harassment on the job would be well
          advised to discuss the circumstances of their case with an experienced
          Long Beach{" "}
          <Link to="/employment-law">
            <strong> employment lawyer</strong>
          </Link>{" "}
          who can not only help put a stop to the employer's wrongdoing, but
          also help the affected employees receive compensation for their
          mistreatment. Please{" "}
          <strong> call 323-238-4683 for a free consultation</strong>.
        </p>
        <h2>Sexual Harassment</h2>
        <p>
          {" "}
          <Link to="/employment-law/sexual-harassment">
            Sexual harassment
          </Link>{" "}
          can be verbal or physical. Examples of acts that may be considered
          harassment at the workplace include:
        </p>
        <ul>
          <li>
            Lewd comments including repeated sexual innuendo, off-color jokes,
            slurs, lewd language and other types of comments that are sexual in
            nature.
          </li>
          <li>
            Content in letters, notes, faxes or email that is sexually
            suggestive or abusive
          </li>
          <li>
            Making sexual propositions such as offering benefits or raises in
            exchange for sexual contact or favors.
          </li>
          <li>Persistent sexual advances or unwanted romantic overtures</li>
          <li>
            Displaying pornographic pictures or other sexual material in the
            workplace
          </li>
          <li>
            Unwelcome touching such as patting, slapping, massaging, fondling,
            stroking or tickling
          </li>
          <li>Coerced sexual intercourse or rape</li>
        </ul>
        <p>
          Women commonly file sexual harassment suits against men, but there are
          instances of female bosses harassing their male employees as well. All
          forms of harassment are wrong and should be taken seriously.
          Businesses that allow this type of behavior or that fosters such a
          hostile work environment should be held accountable for their
          wrongdoing.
        </p>
        <h2>Sexual Harassment Statistics</h2>
        <ul>
          <li>
            1,367 sexual harassment cases resulted in a settlement. That means
            approximately 10.9 percent of all sexual harassment complaints
            resulted in a settlement between the two parties.
          </li>
          <li>
            1,150, or 9.1 percent of sexual harassment lawsuits resulted in
            withdrawals with benefits.
          </li>
          <li>
            2,635 or 21 percent of claims resulted in an administrative closure.
            This type of resolution may involve a failure to locate the charging
            party, the charging party refused to accept full relief or there was
            a closure because of the outcome of related litigation.
          </li>
          <li>
            Over half of all complaints, 6,658 cases (53 percent) found that the
            claimant had no reasonable cause.
          </li>
          <li>
            $ 52.3 million was obtained through sexual harassment litigation in
            2011
          </li>
        </ul>
        <h2>Who is the Harasser?</h2>
        <p>
          A person does not have to be someone's boss or supervisor to be guilty
          of sexual harassment. Anyone with whom the victim has contact with in
          the workplace can be guilty of sexual harassment. This includes a
          supervisor, a co-worker or even a client. The victim does not even
          have to be the person at whom the inappropriate comments are directed.
          A victim is anyone who finds certain behavior offensive and is
          therefore affected by the harassment. The harasser does not have to be
          of the opposite sex and the age of the harasser is not relevant. The
          harasser does not even have to be aware that his or her behavior was
          offensive to be found at fault.
        </p>
        <h2>Hostile Environment and Quid Pro Quo</h2>
        <p>
          Sexual harassment cases involving a{" "}
          <Link to="/employment-law/hostile-work-environment-attorney">
            hostile work environment
          </Link>{" "}
          may include unwelcome conduct of bosses, vendors, customers or anyone
          else in the company. It may involve threats, advancements or physical
          altercations. Quid pro quo is commonly referred to as &ldquo;this for
          that.&rdquo; An example of quid pro quo harassment may involve a
          supervisor offering a promotion in exchange for sexual cooperation or
          the threat of termination.
        </p>
        <h2>Discrimination at the Workplace</h2>
        <p>
          There are a number of ways in which an employee can be discriminated
          against:
        </p>
        <ul>
          <li>
            <strong> Age Discrimination:</strong> The Federal Age Discrimination
            in Employment Act and California's Fair Employment and Housing Act
            (FEHA) prevents discriminatory treatment of workers based on their
            age. Decisions regarding promotions, training opportunities and
            terminations cannot be based solely on the age of the employee.
          </li>
          <li>
            <strong> Race Discrimination:</strong> It is a tragic reality that
            race discrimination still takes place in the workplace. A skilled
            employment lawyer will be needed to prove that an employer made an
            important workplace decision based on the race of the employee or
            job applicant.
          </li>
          <li>
            <strong> Disability Discrimination:</strong> There are some jobs in
            which employers can reasonably say that a disabled applicant could
            not have performed the tasks required for the position. In many
            cases, however, an employer simply does not want to hire or promote
            an individual because of his or her disabilities. The Americans with
            Disabilities Act (ADA) and California's FEHA protect people with
            disabilities from adverse employment actions.
          </li>
          <li>
            <strong> Religious Discrimination:</strong> Everyone has a right to
            choose whatever religion they want to practice. Freedom of religion
            is a right afforded to every American by the U.S. Constitution.
            Religion has nothing to do with the workplace, and it should
            therefore not play a role in any decisions made at the office.
            Anyone who is discriminated against, insulted, ridiculed or
            otherwise mistreated because of his or her religion can file a claim
            under the federal Civil Rights Act or the FEHA.
          </li>
          <li>
            <strong> Gender Discrimination:</strong> No employment decisions
            must be based on the sex of the applicant or employee. A skilled
            employment lawyer will have experience fighting against equal pay
            act discrimination, sexual harassment and pregnancy discrimination.
          </li>
          <li>
            <strong> Pregnancy Discrimination:</strong> There are cases in which
            an employee will let a woman go after learning that she is pregnant.
            This is a violation of the federal Pregnancy Discrimination Act.
          </li>
          <li>
            <strong> Sexual Orientation Discrimination:</strong> Anyone who has
            been ridiculed or insulted because of his or her sexual orientation
            should discuss his or her rights with an employment attorney. Some
            employees have to suffer through hostility, harsh judgment or even
            violence because of their sexual orientation.
          </li>
        </ul>
        <h2>Workplace Harassment</h2>
        <p>
          Employees are protected under California's Fair Employment and Housing
          Act as well as by Title VII of the federal Civil Rights Act from
          discrimination based on gender. This protection extends to cases
          involving sexual harassment. Sexual harassment is when an employee is
          subject inappropriate comments, unwanted touching or sexual advances.
          It also includes situations where an employee is threatened with job
          loss, retaliation or is refused a promotion because he or she declined
          a supervisor's sexual advances.
        </p>
        <h2>Wrongful Termination</h2>
        <p>
          Just because California is an at-will state, employers do not have the
          right to fire an employee based on matters such as race, religion,
          age, nationality, gender or sexual orientation. If the decision to let
          the employee go was based on any type of discrimination, then, the
          employer can be held accountable for{" "}
          <Link to="/employment-law/wrongful-termination">
            wrongful termination
          </Link>
          .
        </p>
        <h2>Your Rights</h2>
        <p>
          There are many forms of discrimination that can occur in a Long Beach
          workplace in addition to harassment. Job applicants are sometimes not
          given due consideration because of their race. Experienced workers
          with an excellent track record may not get the promotion they clearly
          deserve because of their age. A woman may not receive equal pay for
          her services simply because of her gender. These are all forms of
          discrimination that are unacceptable at any level in any workplace.
        </p>
        <p>
          If you believe that your current or former employer has violated your
          rights or if you have been a victim of sexual harassment, the
          experienced and knowledgeable Long Beach employment lawyers at Bisnar
          Chase Personal Injury Attorneys can help you better understand your
          legal rights and options. Please call us at 323-238-4683 or{" "}
          <Link to="/contact">contact us </Link> today for a free, comprehensive
          and confidential consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
