// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Long Beach Dog Bite Lawyers - Dog Bite Attorney in Long Beach"
        pageDescription="Looking for an experienced dog bite lawyer in Long Beach? Call 949-203-3814. We can help you get maximum compensation for your injuries. Top client care and legal service since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Long Beach Dog Bite Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Following a serious dog bite injury, you are going to need a Long
          Beach <Link to="/dog-bites">dog bite lawyer </Link> with experience
          and a history of success. At Bisnar Chase, we have developed a formula
          for success that has worked for 96% of our clients. We have some of
          the top experts in dog bite law working together to provide you with a
          first-className client experience. Since 1978, we have obtained
          hundreds of millions of dollars in verdicts and settlements while
          assisting over 12,000 clients. Bisnar Chase Personal Injury Attorneys
          know how to get your case started off right and will begin
          investigating the moment you sign with our firm.
        </p>
        <h2>California Dog Bite Lawyers Set National Expectations</h2>
        <p>
          John Bisnar and Brian Chase, Bisnar Chase partners, have set the
          national standard for how personal injury law firms should be run.
          John Bisnar speaks at seminars to help law firms create a better
          client experience as well as manage their office in a more efficient
          manner. Brian Chase has made his mark in the courtroom, obtaining
          millions of dollars for his clients in the last year alone. In August
          of 2011, Brian obtained a record-breaking 24.7 million dollar verdict
          for a client who suffered personal injuries. Both partners are
          passionate about obtaining justice for their clients and their passion
          is instilled in every Bisnar Chase Personal Injury Attorneys staff
          member.
        </p>
        <h2>Dog Bite Attorneys in Long Beach Who Care</h2>
        <p>
          Being involved in a serious dog attack is traumatic; dog bite injuries
          are very painful and can leave permanent scars. We have assisted dog
          bite victims for decades and understand the trauma associated with
          being bitten by a vicious canine. Many of our clients must undergo
          intense therapy and painful revision surgeries to help repair the
          damage caused by their attack. We will hold your hand throughout the
          entire process and keep you informed of your case status on a regular
          basis. We know that your Long Beach dog bite lawsuit is important to
          you and that you have a lot counting on a favorable outcome. Our dog
          bite attorneys will help you feel at ease and give you the peace of
          mind to concentrate on your treatment.
        </p>
        <p>
          Our dedication to serving those who are unable to help themselves goes
          beyond the courtroom; our entire staff spends some of their free time
          participating in fundraising events that help injury victims in the
          state of California. The money that we raise as a team is matched by
          the firm partners who help us to be amongst the top contributors for
          some of the causes in which we participate. The following is a list of
          ways that we give back to the community.
        </p>
        <ul>
          <li>Turkey Giveaway</li>
          <li>Adopt A Family</li>
          <li>Walk Like M.A.D.D. campaigns</li>
          <li>Boys & Girls Club Chili Cook Off</li>
          <li>AIDS Bicycle Fundraiser</li>
          <li>Hit & Run Reward Program</li>
        </ul>
        <h2>Dog Bite Statistics</h2>
        <p>
          Dog bites in the United States are on the rise, and the injury
          statistics are looking quite bleak as well. The following are dog bite
          statistics that you may find interesting.
        </p>
        <ul>
          <li>
            Dog bite cases have increased more rapidly than the national
            increase in dog ownership.
          </li>
          <li>
            On average, fifteen dog bite victims die per year with the majority
            of them being children.
          </li>
          <li>
            There are 4.5 million dog bite victims per year. Of those, nearly
            one million require medical attention
          </li>
        </ul>
        <p>
          If you or a loved one has suffered injuries as a result of a dog
          attack in Long Beach, it is in your best interest to contact an
          experienced attorney immediately.
        </p>
        <h2>Dog Bite Attorneys in Long Beach</h2>
        <p>
          If you are looking for an experienced attorney, contact a dog bite
          lawyer in Long Beach at Bisnar Chase Personal Injury Attorneys to get
          the expert assistance you've been looking for. Our attorneys have
          experience assisting victims of almost all types of dog bites. Call
          today to take advantage of our free consultation.
        </p>
        <p>Call 949-203-3814 NOW to get FREE LEGAL ASSISTANCE</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
