// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/nursing-home-abuse-banner-gradient-1.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Nursing Home Abuse Attorney - Bisnar Chase"
        pageDescription="Contact the Orange County Nursing Home Abuse lawyers for a free consultation. Our nursing home abuse attorney offers a no win - no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Nursing Home Abuse Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Nursing Home Abuse Lawyer"
            title="Orange County Nursing Home Abuse Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have a loved one in an{" "}
          <strong> Orange County Care Facility</strong> you may assume that he
          or she is well cared for and treated properly. Even so, it may be in
          your best interest to constantly watch for signs of elder abuse and
          neglect. If you or a loved one has been injured, contact a{" "}
          <strong> Bisnar Chase</strong>{" "}
          <strong> Orange County Nursing Home Abuse Lawyer</strong> as soon as
          possible.
        </p>
        <p>
          Facilities that foster unsafe living conditions or companies that hire
          employees without conducting proper background checks should be held
          accountable for their actions. If you feel that your loved one has
          been mistreated in a nursing home it is essential to contact an{" "}
          <strong> Orange County </strong>nursing home abuse attorney
          <strong> &nbsp;immediately</strong>
          <em>. </em>
          <strong> Call 949-203-3814</strong> to see how we can help you.
        </p>
        <h2>Make Sure Your Loved One is Not a Statistic</h2>
        <ul>
          <li>
            In the U.S. there are over 3.2 million adults living in nursing
            homes and other long term care facilities.
          </li>
          <li>
            As many as 40% of all adults will enter a nursing home at some point
            during their lives and as the U.S. population ages, the number of
            nursing home residents is expected to grow.
          </li>
          <li>
            Up to 1 in 6 nursing home residents may be the victim of abuse or
            neglect every year.
          </li>
          <li>
            Seniors who have been abused have a 300 percent greater chance of
            death in the 3 years following the abuse compared to those who are
            not abused.
          </li>
          <li>
            44 percent of nursing home residents reported that they had been
            abused at some time and 95 percent of nursing home residents
            reported they had seen another resident neglected.
          </li>
        </ul>
        <p>
          At <strong> Bisnar Chase</strong>, our team of highly skilled{" "}
          <strong> Orange County </strong>nursing home abuse attorneys will
          ensure justice is served and your loved ones are in safe and loving
          care. Those who neglect and <strong> abuse </strong>the elderly or
          those in <strong> assisted care </strong> must be held accountable to
          the <strong> maximum extent of the law</strong>.
        </p>
        <p>
          To learn more about nursing home abuse and neglect statistics in the
          U.S. visit{" "}
          <Link to="http://www.nursinghomeabuseguide.org/" target="new">
            Nursing Home Abuse Guide
          </Link>
          .
          <LazyLoad>
            <img
              src="/images/text-header-images/neglect-abuse-family-nursing-home-assisted-living-attorneys-lawyer.jpg"
              width="100%"
              alt="nursing home neglect lawyer"
            />
          </LazyLoad>
        </p>

        <h2>Signs of Nursing Home Abuse and Negligence in Orange County</h2>

        <p>
          There are many potential signs of nursing home abuse or neglect.
          Unfortunately, it is common for the elderly to suffer injuries as a
          result of slip and fall accidents or to experience health issues. So
          it is often difficult to distinguish between signs of abuse and the
          aging process.
        </p>
        <p>
          If, however, your loved one tells you about the abuse or if you see
          signs of neglect or abuse, it would be in your best interest to report
          it to the appropriate authorities and hire an
          <strong> Orange County Nursing Home Abuse Lawyer</strong>. A few of
          the most common signs of nursing home abuse or neglect include:
        </p>
        <ul>
          <li>
            <strong> Unnatural bruising</strong>: There are certain bruises that
            may indicate restraint or abuse. Bruising on the wrists or elbows,
            for example, may be a sign that a worker was forceful with your
            loved one. Some facilities use unnecessary restraints to hold a
            patient in their bed. In such cases, there may be bruising around
            the wrists or ankles.
          </li>
          <li>
            <strong> Chemical restraint</strong>: This has become a common
            problem in California's nursing homes. Chemical restraint is when
            nursing home staff unnecessarily drug residents. Often times,
            residents are drugged because nursing home staff simply does not
            want to deal with them or be bothered by them.
          </li>
          <li>
            <strong> Broken bones</strong>: The elderly are prone to falls, but
            broken bones may be proof of wrongdoing.
          </li>
        </ul>
        <p>
          The CBS Video shows how your loved ones can be mistreated, right under
          your noses. Thankfully, justice was served in these circumstances.
        </p>
        <ul>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/WJRxjgaG_Vg"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>

          <li>
            <strong> Changes in finances</strong>: Financial abuse involves
            individuals stealing money or defrauding the elderly. Are there
            missing items or checks from your loved one's room? Has his or her
            will or deed been altered recently?
          </li>
          <li>
            <strong> Personality change</strong>: Victims of abuse often become
            withdrawn, nervous or shy. They may be unwilling to see their family
            out of embarrassment about their situation.
          </li>
          <li>
            <strong> Bedsores</strong>: It is common for individuals who spend a
            lot of time in bed or in a wheel chair to suffer from minor
            bedsores. If, however, these bedsores persist or they get worse or
            become infected, it could be a sign that the nursing home is not
            giving them the attention they need. Patients should be shifted and
            moved throughout the day to{" "}
            <Link to="/nursing-home-abuse/bedsores" target="new">
              minimize bedsores
            </Link>
            . Failure to do so is neglect.
          </li>
          <li>
            <strong> Dehydration and malnutrition</strong>: Has your loved one
            lost weight recently? Care facilities must not only provide adequate
            meals but also ensure that the residents eat the food as well. They
            cannot force them to eat, but they should do everything possible to
            ensure that the resident is receiving proper care and nourishment.
          </li>
          <li>
            <strong> Fear</strong>: Victims of abuse may show signs of fear
            around certain individuals. Other times they may become depressed or
            angry.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Value of my personal injury claim"
            style={{
              backgroundImage:
                "url('/images/text-header-images/steps-to-avoid-nursing-home-abuse (1).jpg')"
            }}
          >
            <h2>Steps You Can Take To Prevent Neglect</h2>
          </div>
        </LazyLoad>
        <p>
          If you observe any <strong> signs of neglect</strong>, it may be in
          your best interest to discuss them with your loved one and to express
          your concerns with the facility's administrators. If your concerns are
          not being addressed, or worse, if they are ignored, then, it may be in
          your best interest to move your loved one to another facility.
        </p>
        <p>
          If your loved one has been injured or if he or she had died as a{" "}
          <strong> result of neglect</strong>, please remember that you have
          rights. It is important that nursing homes that put profits before
          people be <strong> held accountable</strong>.
        </p>
        <p>
          You can also contact the <strong> Eldercare Locater</strong>, at{" "}
          <strong> 1-800-677-1116</strong> for{" "}
          <strong> state information</strong>.
        </p>

        <h2>
          Seeking The Best Legal Help in Orange County for Your Loved Ones
        </h2>
        <p>
          The experienced{" "}
          <strong> Orange County Nursing Home Abuse Lawyers </strong>at
          <strong> Bisnar Chase</strong> have helped victims of nursing home
          abuse and neglect and their families obtain fair compensation for
          their damages and losses.
        </p>
        <p>
          Civil litigation also helps victims and their families obtain and
          sense of justice and it gives them the satisfaction of having done
          their part to hold the wrongdoers accountable and ensure that others
          are not victimized in the same manner.
        </p>
        <p>
          If you or a loved one has suffered neglect or abuse in an{" "}
          <strong> Orange County Nursing Home</strong>, please{" "}
          <Link to="/contact">contact us</Link> to obtain more information about
          pursuing your legal rights. <strong> Call 949-203-3814</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
