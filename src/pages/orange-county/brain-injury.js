// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/review-medical-physician-orange-county-brain-injury-attorneys-lawyers-accidents.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Brain Injury Lawyer - 949-203-3814"
        pageDescription="if your brain injury was caused by someone else's negligence, you or your family should not have to suffer. The experienced Orange County brain injury attorneys at Bisnar Chase can help you pursue compensation for your damages. Please call us at 949-203-3814 for a free, comprehensive consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Brain Injury Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="orange county brain injury attorney"
            title="orange county brain injury attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <img
          className="imgleft-fixed"
          src="/images/attorney-chase.jpg"
          height="141"
          alt="Brian Chase"
        />

        <p>
          If your brain injury was caused by someone else's negligence, you or
          your family should not have to suffer. The experienced{" "}
          <strong> Orange County Brain Injury Lawyers</strong> at Bisnar Chase
          can help you pursue compensation for your damages. Please call us at{" "}
          <strong> 949-203-3814</strong> for a <strong> free</strong>,{" "}
          <strong> comprehensive</strong> <strong> and</strong>
          <strong> confidential</strong> <strong> consultation</strong>.
        </p>
        <p>
          Our team of highly skilled{" "}
          <Link to="/orange-county" target="new">
            Orange County Personal Injury Lawyers{" "}
          </Link>{" "}
          understand how to be there for you through this difficult time and
          ensure you receive maximum compensation for you and your family.
        </p>

        <p className="clearfix">
          <span>
            <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
            Partner:
          </span>{" "}
          “We've helped some great people recover their lives after a
          catastrophic brain injury and that feels great.”
        </p>
        <h2>Facing Challenges with You</h2>
        <LazyLoad>
          <img
            src="/images/brain-injury/Brain_2.jpg"
            alt="brain injury lawyers in Orange County"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Living with a brain injury can be extremely challenging not only for
          victims, but also their family members. In addition to what these
          victims and families go through emotionally, they are also faced with
          significant financial burdens. Medical expenses, hospitalization,
          surgery, rehabilitation and other forms of therapy can add up very
          quickly and push families into debt.
        </p>

        <p>
          A traumatic brain injury can become a catastrophic injury. What this
          means is that victims of such traumatic injuries are often left with
          lifelong disabilities that prevent them from going back to the lives
          they had prior to the injury.
        </p>
        <p>
          Most of them are not able to return to the jobs they had prior to the
          injury or are even unable to re-join the workforce. If you or a loved
          one has suffered a traumatic brain injury as a result of someone
          else's negligence or wrongdoing, an experienced{" "}
          <strong> Orange County </strong>brain injury attorney can help you
          better understand your legal rights and options.
        </p>
        <h2>Some of our Recent Orange County Case Results</h2>
        <ul>
          <li>
            <strong> $38,650,000</strong> - Motorcycle Accident
          </li>
          <li>
            <strong> $24,744,764</strong> - Auto defect
          </li>
          <li>
            <strong> $23,091,098</strong> - Product liability
          </li>
          <li>
            <strong> $16,444,904</strong> - Dangerous road condition, driver
            negligence
          </li>
          <li>
            <strong> $10,030,000</strong> - Premises negligence
          </li>
          <li>
            <strong> $9,800,000</strong> - Motor vehicle accident
          </li>
          <li>
            <strong> $8,500,000</strong> - Motor vehicle accident - wrongful
            death
          </li>
          <li>
            <strong> $8,250,000</strong> - Premises liability
          </li>
          <li>
            <strong> $7,998,073</strong> - Product liability - motor vehicle
            accident
          </li>
        </ul>
        <p>
          Although brain injuries can range from mild to severe, there are many{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="new">
            Different Types of Brain Injuries
          </Link>
          .
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Value of my personal injury claim"
            style={{
              backgroundImage:
                "url('/images/text-header-images/head-pain-brain-injury-orange-county.jpg')"
            }}
          >
            <h2>Causes of Serious Head Injuries in Orange County</h2>
          </div>
        </LazyLoad>

        <p>
          They are many ways in which a <strong> brain injury</strong> can
          occur. Most brain injuries are caused by an impact to the head and the
          brain. Some head injuries or brain injuries can be critical or life
          threatening. Others are moderate or even minor. However, regardless of
          the severity of the injuries, victims should obtain prompt medical
          attention to prevent the situation from worsening.
        </p>
        <p>
          A minor or moderate head injury can take a turn for the worse without
          prompt treatment. There are several possible causes for brain injuries
          such as:
        </p>
        <p>
          <strong> Motor Vehicle Accidents:</strong> About one-third of all
          brain injuries in the United States are caused by auto accidents.
          These include car accidents, bike crashes, motorcycle accidents, truck
          collisions, pedestrian accidents and bus crashes to mention a few.
          When motor vehicle accidents cause serious brain injuries, it is
          important to determine who was at fault and who should be held liable.
        </p>
        <p>
          <strong> Falls:</strong> In the United States, falls are the leading
          cause of all traumatic brain injuries, according to the Centers for
          Disease Control and Prevention (CDC).  Falls could occur in nursing
          homes, at work areas such as construction sites or as the result of a
          slip-trip-and-fall accident. Depending on the circumstances of the
          fall, the at-fault party can be held liable. For example, if a nursing
          home resident suffers a brain injury as the result of an employee's
          negligence, the nursing home can be held liable for the injuries. If a
          visitor to a shopping center slips and falls on liquid spilled on the
          floor that was not cleaned up in a timely manner, the property owner
          could be held liable for the victim's injuries.
        </p>
        <p>
          <strong> Acts of Violence:</strong> An assault with a weapon such as a
          gun, knife or even a baseball bat can result in a traumatic brain
          injury. In such cases, in addition to criminal prosecution, the
          at-fault party can also be held civilly liable for their wrongdoing.
        </p>
        <p>
          <strong> Sports-Related Injuries:</strong> Head injuries such as
          concussions are extremely common in contact sports. However, when a
          serious brain injury occurs because of someone's negligence, there may
          be a basis for a lawsuit. Assumption of risk is often a factor in such
          cases. This means that athletes assume a certain element of risk when
          they play sports. An experienced Orange County brain injury lawyer
          will be able to determine fault and liability in such complex cases.
        </p>
        <p>
          <strong> Medical Negligence:</strong> There are cases when a doctor or
          medical staff's negligence can cause a brain injury. For example, if
          oxygen is cut off to the brain of an unborn child during labor or
          delivery, the child may suffer a brain injury or irreversible brain
          damage. In such cases, a doctor who failed to recognize symptoms of
          fetal distress could be held liable.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/concussion-treatment-orange-county-.jpg"
            width="100%"
            alt="brain injury law firms orange county"
          />
        </LazyLoad>

        <h2>What are Symptoms of a Traumatic Brain Injury?</h2>
        <p>
          Symptoms can range from mild to severe and can last for hours, days,
          weeks, or even months which can include:
        </p>
        <ul>
          <li>Unclear thoughts</li>
          <li>Difficulty remembering new information</li>
          <li>Headaches, vision problems and dizziness</li>
          <li>Depression, anxiety and anger</li>
          <li>Sleeping more or less than usual</li>
        </ul>

        <h2>How are Traumatic Brain Injuries Diagnosed?</h2>

        <p>
          Every head and brain injury vary from person to person. Typically, a
          doctor will begin by asking basic questions about the injury and how
          it took place, followed with questions that will test your ability to
          pay attention, learn, remember and solve problems.
        </p>
        <p>
          An examination for physical signs of a brain injury by checking your
          reflexes , strength, balance, coordination and sensation. CT scans and
          MRI's may be ordered to make sure your brain is not bruised or
          bleeding. Additional tests to see if proper brain function is present
          may be necessary. You can also call a <strong> Orange County</strong>{" "}
          brain injury attorney to gain more knowledge of your legal options for
          your case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/gny8lZ1Ds3g"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Treatment and Rehabilitation in Orange County</h2>
        <p>
          If you experienced brain damage, rehabilitation and treatment can be
          tremendously beneficial, whether short term or longterm.{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="new">
            Types of brain injury treatments{" "}
          </Link>{" "}
          can include the following:
        </p>
        <ul>
          <li>Physical therapy</li>
          <li>Occupational therapy</li>
          <li>Speech and language therapy</li>
          <li>Counseling for you, family and loved ones</li>
          <li>Social support and support groups</li>
          <li>Medicine and holistic alternatives</li>
        </ul>
        <p>
          To learn more specific information about brain injuries,{" "}
          <Link
            to="http://www.webmd.com/brain/tc/traumatic-brain-injury-topic-overview#1"
            target="new"
          >
            click here
          </Link>
          .
        </p>
        <h2>Seeking Legal Help For an Orange County Brain Injury</h2>
        <p>
          Immediately call an experienced and reputable{" "}
          <strong> Orange County Brain Injury lawyer</strong> for a free
          consultation at 949-203-3814. You may be entitled to compensation for
          your injuries including medical bills. Our results speak for
          themselves. We've helped thousands obtain justice.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
