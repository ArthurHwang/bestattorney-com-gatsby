// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-motorcycle-accidents-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Motorcycle Accident Attorney - Bisnar Chase"
        pageDescription="Call 949-203-3814 for an experienced Orange County Motorcycle Accident Lawyer. Winning biker's rights attorneys for three decades. Free case review & a 96% success rate in and out of court."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Motorcycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Motorcycle Accident Lawyer"
            title="Orange County Motorcycle Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you've been injured in a motorcycle accident, contact our{" "}
          <strong> Orange County Motorcycle Accident Lawyer</strong> for a no
          obligation case review. We've been representing motorcycle accident
          plaintiffs for over <strong> 40 years</strong>. We've collected and
          settled over <strong> $500 Million</strong> for our clients. To see if
          you're entitled to compensation, give our team of highly skilled and
          experienced accident lawyers at <strong> 949-203-3814</strong>.
        </p>
        <p>
          At{" "}
          <strong>
            {" "}
            <Link to="/">Bisnar Chase</Link>
          </strong>{" "}
          we deal with{" "}
          <Link to="/orange-county/car-accidents">vehicle accidents</Link>{" "}
          nearly every day and over the years many of them have been motorcycle
          accidents throughout Orange County. With a massive population, plenty
          of money to spend on the newest and fastest bikes and increasing
          traffic stretching from the 22 freeway, east on the 91 freeway, and
          all the way down the 5 freeway, motorcycle accidents are bound to
          happen.
        </p>
        <p>
          If you or a loved one has been injured in a motorcycle accident,
          contact our highly skilled and experienced{" "}
          <Link to="/orange-county" target="new">
            Orange County Personal Injury Attorneys
          </Link>{" "}
          for a free consultation.
        </p>
        <h2>
          Seeking Compensation for Your Losses in a Orange County Motorcycle
          Accident
        </h2>
        <p>
          Injured victims of motorcycle accidents can seek compensation from the
          at-fault parties for damages such as medical expenses, lost wages,
          cost of hospitalization, rehabilitation, pain and suffering, permanent
          injury and emotional distress.
        </p>
        <p>
          When an individual is killed in a motorcycle accident, his or her
          family members can file a wrongful death claim seeking compensation
          for damages as well. In the case of hit-and-run motorcycle accidents
          or collisions involving uninsured drivers, injured victims can seek
          compensation through the uninsured motorist clause of their auto
          insurance policies.
        </p>
        <p>
          The knowledgeable{" "}
          <strong>
            {" "}
            Orange County Motorcycle Accident Lawyers at Bisnar Chase
          </strong>{" "}
          have over 40 years of experience representing injured accident victims
          and their families. We know what it takes to fight insurance
          companies, corporations and other entities that make it challenging
          for our injured clients to seek the compensation they rightfully
          deserve.
        </p>
        <p>
          Riding a motorcycle can be exhilarating and cost-effective. It offers
          commuters an easier way to make their way through Orange County's
          congested freeways. However, riding a motorcycle also comes with
          significant risks. Motorcycles are smaller vehicles that are easier to
          overlook than cars, trucks or SUVs. They also provide very little
          protection to riders in the event of a crash. Even the most skilled
          Orange County riders can get seriously injured in a traffic collision.
        </p>

        <h2>Staying Safe on Motorcycles in Orange County</h2>
        <p>
          Motorcycles are known to be extremely dangerous. It isn't the
          motorcycle itself that is dangerous, but the factors surrounding the
          motorcycle.
        </p>
        <p>
          A motorcycle rider can practice all of the following safety
          suggestions and still encounter hazards:
        </p>
        <ul>
          <li>
            <strong> Proper Safety Gear</strong>: Helmet, protective clothing
            (padded jackets and pants), gloves, boots, eye safety, etc.
          </li>
          <li>
            <strong> Motorcycle Condition</strong>: Ensuring all parts are
            functioning properly, lubricated, nothing hindering their movements
            or potentially creating a hazardous situation.
          </li>
          <li>
            <strong> Road Ready</strong>: Turn signals, headlights, brake
            lights, no bulbs out, license plate, etc.
          </li>
        </ul>
        <p>
          Making sure you're taking every precaution towards safe motorcycle
          transportation is a must, for those around you, yourself, and your
          family.
        </p>
        <p>
          Although being a safe motorcycle rider is always encouraged, it
          doesn't always promise your safety on the road. The majority of
          motorcycle accidents are caused by other vehicles causing the
          accident.
        </p>
        <p>
          Cars pulling out in front of a motorcycle in motion, stopping
          abruptly, smashing into them, or by mistakenly not seeing them due to
          their small profile and less significant sources of light can cause
          serious injury and dea th.
        </p>
        <p>
          If you have or a loved one has been injured or killed in a motorcycle
          accident, contact our{" "}
          <strong> Orange County Motorcycle Accident Attorneys</strong> for a
          <strong> Free Case Evaluation</strong>.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/dog-in-motorcycle-helmet-safety-family-accidents-attorneys-lawyer.jpg"
            width="100%"
            alt="dog in a motorcycle helmet promoting safety orange county"
          />
        </LazyLoad>

        <h2>Orange County Motorcycle Accidents Statistics</h2>
        <p>
          Hundreds of motorcyclists are injured while riding in Orange County
          each year. According to{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="new"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System (SWITRS)
          </Link>
          , 18 motorcyclists were killed and 796 were injured in Orange County
          accidents in one year alone.
        </p>
        <ul>
          <li>
            Anaheim led all Orange County cities with 98 motorcycle injuries
          </li>
          <li>Santa Ana with 67</li>
          <li>Huntington Beach with 59</li>
          <li>
            5 fatalities and 34 injuries were reported as a result of motorcycle
            accidents in the unincorporated areas of Orange County
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9nqLzrBI8FE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Causes of Motorcycle Accidents in Orange County</h2>
        <p>
          Motorcycle accidents, like many other types of traffic accidents, can
          happen in a split second. It only takes a moment of distraction or
          inattention. Some of the most common causes of motorcycle accidents in
          Orange County include:
        </p>
        <ul>
          <li>
            Failure to look for motorcyclists before changing lanes, turning
            left or entering traffic
          </li>
          <li>Running a red light or stop sign at an intersection</li>
          <li>Driving under the influence of drugs and/or alcohol</li>
          <li>
            Exceeding the speed limit or driving at a speed that is unsafe for
            traffic, roadway or weather conditions
          </li>
          <li>Driving while distracted</li>
          <li>Reckless or aggressive driving</li>
          <li>
            Dangerous or defective roadways and poorly designed street
            intersections
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Ambulance rushing to scene"
            style={{
              backgroundImage:
                "url('/images/text-header-images/medical-care-motorcycle-accidents.jpg')"
            }}
          >
            <h2>What To Do If You Have Been Injured</h2>
          </div>
        </LazyLoad>
        <p>
          Immediately following the accident, seek medical attention as soon as
          possible. This is not only important for your health and safety, but
          it allows a doctor or medical personal to legally document your
          injuries with details and a date of occurrence. Later on down the
          line, if you do not have any medical documentation of injuries
          inflicted from the accident, they "never happened," and can
          dramatically affect your case.
        </p>
        <p>
          Contact an experienced{" "}
          <strong> Orange County Motorcycle Accident attorney </strong> at
          Bisnar Chase immediately for a free consultation at{" "}
          <strong> 949-203-3814</strong>. We may be able to preserve your rights
          and fight for compensation.
        </p>
        <h2>Types of Motorcycle Accident Injuries in Orange County</h2>
        <p>
          In a motorcycle crash, almost always, it is the rider and his or her
          passenger who suffer the more serious or catastrophic injuries. Some
          of the most common injuries suffered in motorcycle accidents include:
        </p>
        <ul>
          <li>Traumatic brain injury or head injuries</li>
          <li>Spinal cord trauma - neck and back injuries</li>
          <li>Internal organ damage</li>
          <li>Broken bones or fractures</li>
          <li>Lacerations or amputations</li>
          <li>Road rash</li>
          <li>Burn injuries</li>
        </ul>
        <p>
          The <strong> Orange County Motorcycle Accident Lawyers</strong> of
          Bisnar Chase has represented California clients since 1978 including
          many California motorcycle accident victims who had been told by other
          law firms that they didn't have a case. Let us see what we can do for
          you. Call for a{" "}
          <Link to="/contact">no obligation free consultation</Link> with our
          award winning Orange County personal injury attorneys at{" "}
          <strong> 949-203-3814</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
