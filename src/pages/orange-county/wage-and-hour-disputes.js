// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/wage-employment-disagreement-jobs-attorney-lawyer-banner-orange-county.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Wage and Hour Lawyer - Unpaid Wages Attorneys"
        pageDescription="Call 949-203-3814 to receive a free consultation from experienced Orange County wage and hour violation attorneys. All California victims receive free information with no obligation. Your case may be worth retaining an unpaid wages lawyer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Unpaid Wages Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Wage and Hour Attorney"
            title="Orange County Wage and Hour Attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you feel that your employer has violated your rights, or would like
          to receive a <strong> Free Consultation</strong> to find out if they
          have, <strong> Call </strong> our highly experienced team of skilled{" "}
          <strong> Unpaid Wages Attorneys </strong>at{" "}
          <strong> 949-203-3814</strong>.
        </p>
        <p>
          These days, far too many employees are being taken advantage of by
          greedy employers. California workers are being asked to work unfair
          hours at low wages with no breaks and little respect.
        </p>

        <h2>Minimum Wage Violations</h2>
        <p>
          According to the{" "}
          <Link to="https://www.dol.gov/" target="new">
            Department of Labor
          </Link>{" "}
          there were over $246 Million in back wages for 2015 compared to $240
          Million the year before.
        </p>
        <p>
          The DOL received over 22,000 complaints in 2015 alone about back wages
          and minimum wage violations. Employers who are found to violate
          minimum wage laws may be held liable for substantial settlements and
          verdicts if investigated properly.
        </p>
        <p>
          One of the most important aspects of an employment lawsuit is proper
          investigation by attorneys who know what to look for. Many Bisnar
          Chase clients who received large verdicts and settlements were unaware
          of their employer's most important employment violations.
        </p>
        <p>
          The <strong> Orange County Minimum Wage Attorneys</strong> at{" "}
          <strong> Bisnar Chase</strong> know what questions to ask and how to
          ask them. You can rest assured that they will uncover every violation
          and hold your employer accountable.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/414yNyALsPo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Overtime Violations and Lost Wages</h2>
        <p>
          It seems like common sense, but employees who work more than full time
          hours are entitled to overtime pay.
        </p>
        <p>
          Some employers throughout California "conveniently" overlook such
          dedication and steal money directly from their employee's pockets
          through their refusal to pay fair wages.
        </p>
        <ul>
          <li>
            California state law requires 1 1/2 times the regular wage for daily
            overtime following 40 hours worked in a week.
          </li>
          <li>8 hours in a day for all non-exempt employees.</li>
          <li>
            If a non-exempt employee works more than 12 hours in a day, their
            employer must pay them 2 times their regular wage.
          </li>
        </ul>
        <p>
          In 2008, nearly $12.8 million in back wages was awarded to more than
          9,500 employees due to{" "}
          <Link
            to="https://www.dol.gov/whd/overtime/regulations.pdf"
            target="new"
          >
            violations of the Overtime Security regulations (29 C.F.R. Part 541)
          </Link>
          .
        </p>
        <p>
          Although cited in substantially fewer cases, back wages that resulted
          from determinations that some employees were unable to meet the duties
          test for exempt employees was close to $4 million and was felt by
          nearly 2,900 employees.
        </p>
        <p>
          Overtime violations still run rampant today, and many victims have yet
          to come forward to pursue claims for compensation.
        </p>
        <h2>Meals and Break Periods</h2>
        <p>
          No matter how dedicated you are to your job, your employer is required
          to make sure that you take a break every so often. Employees these
          days are under a substantial amount of pressure to outperform and
          outwork their coworkers, but employers are supposed to step in and
          make sure that we are not working ourselves to death.
        </p>
        <p>
          Employers are required to give their employees 30 minute unpaid meal
          break period for every 5 hours of work performed in a day. In
          addition, 10 minutes of paid rest breaks every four hours are
          required.
        </p>
        <p>
          These are not guidelines or suggestions, these are rules enforced by
          the state of California to make sure that we are working under
          conditions that will keep the average employee healthy and happy.
        </p>
        <p>
          Violations may not seem to warrant extravagant monetary compensation,
          but results prove otherwise.
        </p>
        <p>
          To find out if you are entitled to compensation for a employment
          violation please <strong> call 949-203-3814</strong> for a{" "}
          <Link to="/contact" target="new">
            Free Case Evaluation
          </Link>
          .
        </p>

        <p>
          The Orange County{" "}
          <Link to="/employment-law/wage-and-hour">wage and hour lawyers</Link>{" "}
          at <strong> Bisnar Chase</strong> have been working with satisfied
          clients for years and have successfully tried several cases that have
          resulted in multi million dollar settlements and verdicts.
        </p>

        <h2>Superior Client Representation by Aggressive Attorneys</h2>

        <p>
          <strong> Bisnar Chase</strong> will find out what violations your
          employer is guilty of and give you top-notch legal representation
          throughout the duration of your case.
        </p>
        <p>
          Our <strong> Wage and Hour Lawyers</strong> have been practicing in
          California for over <strong> 39 Years</strong>, since 1978 and have a
          long history of satisfied clients. Establishing a
          <strong> 96% Success Rate</strong>, <strong> Bisnar Chase </strong>has
          won over <strong> $500 Million</strong> for our clients.
        </p>
        <p>
          <strong> Call 949-203-3814</strong> for a{" "}
          <strong> Free Employment Rights Consultation </strong>and{" "}
          <strong> Complimentary Case Evaluation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
