// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-product-liability-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Product Liability Lawyers - Bisnar Chase"
        pageDescription="Trusting the products we buy & use for our family & ourselves can be a dangerous decision. Negligent maintenance, manufacturing & advertising can contribute to the unsafe sales of hazardous or recalled products. Call our Orange County Product Liability Lawyers for your Free consultation at 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Product Liability Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Product Liability Lawyers"
            title="Orange County Product Liability Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Our skilled and experienced team of{" "}
          <strong> Orange County Product Liability Lawyers</strong> at{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          understand what health and safety means, especially to you and your
          family. When that trust is devalued and negligence causes personal
          injury, justice must be served, holding those to blame accountable.
        </p>
        <p>
          Making sure that you can put your trust and faith in you legal
          representation is of extreme importance, and the{" "}
          <Link to="/orange-county" target="new">
            Orange County Personal Injury Lawyers
          </Link>{" "}
          at <strong> Bisnar Chase</strong> have the experience and resources to
          win your case, or you do not pay.
        </p>
        <p>
          If you or a loved one has experienced a personal injury or traumatic
          experience as result of a defective product, product recall or
          mislabeled product, call our team of legal professionals for your{" "}
          <strong> Free consultation</strong> and{" "}
          <strong> case evaluation</strong>, at <strong> 949-203-3814</strong>.
        </p>
        This page will cover the following:
        <div className="mb">
          <Link to="/orange-county/product-liability-lawyers#header1">
            <DefaultGreyButton
              type="button"
              data-elementid="header1"
              className="btn btn-primary active nav-button"
            >
              What is a Product Defect?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header2">
            <DefaultGreyButton
              type="button"
              data-elementid="header2"
              className="btn btn-primary active nav-button"
            >
              Types of Product Defects{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header3">
            <DefaultGreyButton
              type="button"
              data-elementid="header3"
              className="btn btn-primary active nav-button"
            >
              Top 5 Most Infamous Product Defects{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header4">
            <DefaultGreyButton
              type="button"
              data-elementid="header4"
              className="btn btn-primary active nav-button"
            >
              How to Prove a Product Liability Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header5">
            <DefaultGreyButton
              type="button"
              data-elementid="header5"
              className="btn btn-primary active nav-button"
            >
              Recalled Products?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header6">
            <DefaultGreyButton
              type="button"
              data-elementid="header6"
              className="btn btn-primary active nav-button"
            >
              How To Check Your Home for Product Defects & Recalls{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header7">
            <DefaultGreyButton
              type="button"
              data-elementid="header7"
              className="btn btn-primary active nav-button"
            >
              Defective Products Class Action Lawsuits{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header8">
            <DefaultGreyButton
              type="button"
              data-elementid="header8"
              className="btn btn-primary active nav-button"
            >
              Why It's Important To Hire an Orange County Product Liability
              Lawyer <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header9">
            <DefaultGreyButton
              type="button"
              data-elementid="header9"
              className="btn btn-primary active nav-button"
            >
              Contingency Fee - You Don't Pay If We Don't Win{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>

          <Link to="/orange-county/product-liability-lawyers#header10">
            <DefaultGreyButton
              type="button"
              data-elementid="header10"
              className="btn btn-primary active nav-button"
            >
              Our Product Liability Law Firm is Ready to Win Your Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>What is a Product Defect?</h2>
        <p>
          When we purchase items, we expect them to work in a specific way,
          generally speaking. If you buy a blender, you expect it to blend your
          food without having to worry about a significant possibility that it
          is going to cause a house fire or explode shrapnel all over your
          kitchen.
        </p>
        <p>
          Often, due to negligent manufacturing, low-cost quality and of course
          abnormal defects, products can become defective and not work as
          expected. Having extra or missing parts, cracks, seems or irregular
          structures can offer more hazardous possibilities as time goes on.
        </p>
        <p>
          If you have noticed that something you have purchased has a defect or
          irregularity, return to the place of purchase and notify them of the
          issue before someone gets hurt or property is damaged.
        </p>
        <div id="header2"></div>
        <h2>Types of Product Defects</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/types-of-product-defects-orange-county.jpg"
            width="100%"
            alt="orange county product liability attorneys"
          />
        </LazyLoad>
        <p>
          All products are at risk to becoming defective, especially when
          negligence is a factor. It's only a matter of where the negligence is
          a factor. Once a customer experiences the product defect, who, what,
          where and why become important. There is an endless amount of products
          in this world, but there are a select few that consistently experience
          problems with defects. Here are the 3 common stages where defects can
          develop:
        </p>
        <ul>
          <li>
            <strong> Manufacturing Defects:</strong> During the process of
            manufacturing, a plethora of issues are always possible.
            Malfunctioning machines, calibration errors and irregularities in
            assembly line production. Having the wrong screw used, not tightened
            enough or over tightened, wrong parts being used or unused.
          </li>
          <li>
            <strong> Design Defects: </strong>In these scenarios, it falls back
            to the drawing-boards. Where engineers, entrepreneurs and designers
            fail, overlook or miscalculate design aspects that lead to defects,
            dangerous products and product recalls.
          </li>
          <li>
            <strong> Marketing Defects:</strong> Failing to warn potential
            customers of the product's possible dangers, ingredients, contents
            and hazards can lead to serious injuries, deaths, expensive lawsuits
            and media coverage, which is not good for a company's reputation.
            This can fall under false advertising as well.
          </li>
        </ul>
        <div id="header3"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="orange county product liability lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/top-5-most-infamous-product-defects.jpg')"
            }}
          >
            <h2>Top 5 Most Infamous Product Defects</h2>
          </div>
        </LazyLoad>
        <p>
          Throughout history, there have been a wide variety of product defects
          and recalls that made their way into the media. Whether the media
          coverage was popular based on relevancy or strangeness, here is a list
          of the <strong> Top 5 Most Infamous Product Defects:</strong>
        </p>
        <ol>
          <li>
            <strong> Hasbro Easy Bake Ovens:</strong> With 249 reports of
            hazardous situations of children injuring themselves or seeking
            medical or professional attention, Hasbro's Easy Bake Oven's were
            recalled. Hasbro recalled 1 million easy bake ovens and felt it for
            about a year until their stock price recovered. Children were
            getting their fingers and hands stuck inside the oven, where they
            could be burned and suffer injuries from trying to pry their hand
            out of the oven.
          </li>
          <li>
            <strong> Toyota Motors:</strong> You may remember Toyota taking a
            few punches from the media in 2010, when their stock price dropped
            during a 2.3 million vehicles due to the gas pedal sticking.
            Affected models were the Camry, Highlander, Rav4, Corolla, Matrix,
            Avalon, Sequoia and the Tundra.
          </li>
          <li>
            <strong> Graco High Chairs:</strong> You want your kids to be safe,
            right? So did the parents who bought Graco high chairs. After
            receiving 24 reports of injured children, Graco recalled about 1
            million high chairs, specifically the harmony model due to being
            prone to tipping over. The company did not suffer from this recall.
          </li>
          <li>
            <strong> Dell Batteries:</strong> In 2006 there was a recall of over
            4 million lithium-ion batteries made by Sony. The batteries were
            failing, causing overheating and fires. Although the stock price
            dropped, they made a quick come back.
          </li>
          <li>
            <strong> Vioxx/Merck: </strong>An arthritis drug that was
            manufactured and sold after the FDA approved it in 1999. The drug
            received lots of reports stating heart attacks and strokes were
            experienced as result of taking the drug. After $4.85 billion to
            settle, with over 27,000 lawsuits related to incidents of stroke and
            heart attack, MRK's stock price fell.
          </li>
        </ol>
        <div id="header4"></div>
        <h2>How To Prove a Product Liability Case</h2>
        <p>
          There are many ways of going about your case, steps you should take
          before beginning your case and even important things to remember after
          your case. But first things first, lets prove and win your case.
        </p>
        <p>
          With all litigation and accident/situation cases, documentation and
          evidence are key. Proving that your injuries, pain and suffering,
          property damage and so on were caused directly by the offending party
          or situation are your biggest things to remember.
        </p>
        <p>
          Having a police report at the scene of the accident or situation,
          seeking and documenting your injuries or symptoms, especially in
          product defect cases. Later down the line when trying to prove your
          side of the case, if you are unable to show concrete evidence that
          your injuries were the result of the incident, it is going to be very
          difficult to prove that you are entitled to compensation and a
          victorious settlement.
        </p>
        <p>
          If indeed you are seeking legal representation for a case that you are
          the victim of, gathering such evidence is possible and we are here to
          help you every step of the way.
        </p>
        <p>
          If you have experienced a personal injury, symptoms, pain and
          suffering, property damage or any other issues related to a defective
          product, call our skilled team of Orange County Product Defect
          Attorneys at <strong> 949-203-3814</strong> for your Free consultation
          and case evaluation.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/JjAc3yieVGo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <div id="header5"></div>
        <h2>Recalled Products?</h2>
        <p>
          A product recall is when a company or organization acts on a discovery
          or user reports, usually regarding a dangerous or defective product.
          When defective and potentially hazardous products are discovered,
          recalls are notices to return the product to it's place of purchase or
          warranty for a refund or repair.
        </p>
        <p>
          This is to entitle the purchaser to a healthy and safe use of the
          product. But what happens when not everyone is notified?
        </p>
        <p>
          Advertisements, magazine ads, tv and radio commercials only reach so
          many people. But how do you notify customers who don't have access to
          tv or radio or whatever means of notifying the customers the company
          has acted on?
        </p>
        <p>
          There's no way of making sure everybody involved in a recall or who
          could be affected by a recall can be reached. This is dangerous and
          can be deadly.
        </p>
        <p>
          Companies who request email or contact information upon purchase of
          their product have paved their way into the future in case the need
          arises to contact purchasers of any defects or information they would
          need to know, especially when it comes to them and their family's
          safety.
        </p>
        <div id="header6"></div>
        <h2>How To Check Your Home for Product Defects & Recalls</h2>
        <p>
          If you suspect or just want to ensure the safety of your family, there
          are ways to check to see if there are any products in your house that
          currently have product recalls or known defects by just doing some
          basic search engine research and visiting the{" "}
          <Link to="https://www.recalls.gov/" target="new">
            recalls.gov
          </Link>{" "}
          page where you can view recent recalls, search for recalls or sign up
          for their email list.
        </p>
        <p>
          Often times you will see evening news discussing the most current
          product defects and recalls, the commercials during prime evening
          programming, as well as radio commercials on your going to and coming
          home commute from work. Billboards displaying local attorneys trying
          to get cases for popular recalls and class actions are also another
          avenue of seeing whats out there.
        </p>
        <p>
          If you have experienced a product defect and feel others could be at
          risk, you can visit SaferProducts.gov and{" "}
          <Link
            to="https://www.saferproducts.gov/CPSRMSPublic/Incidents/ReportIncident.aspx"
            target="new"
          >
            file a product defect report
          </Link>
          .
        </p>
        <div id="header7"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="orange county product liability law firm"
            style={{
              backgroundImage:
                "url('/images/text-header-images/product-defect-class-action-lawsuits.jpg')"
            }}
          >
            <h2>Defective Products Class Action Lawsuit</h2>
          </div>
        </LazyLoad>
        <p>
          Often times, individuals are not the only ones affected by a singular
          product defect, and in fact are just one of many people experiencing
          symptoms, injuries or issues.
        </p>
        <p>
          These situations are when class action lawsuits become appropriate. A
          class action lawsuit is when multiple parties have experienced some
          sort of issue from the same defect or accident, collectively pursuing
          litigation in one case. This involves cases such as:
        </p>
        <ul>
          <li>
            Dell Batteries selling defective batteries to many customers that
            resulted in fires
          </li>
          <li>Sephora employees suing for employment violation issues</li>
          <li>
            PPV interruptions and outages during "money fight" with Conor
            McGregor and Floyd Mayweather
          </li>
        </ul>
        <p>
          In class action cases, the payouts can be huge and into 7 and 8
          figures, but depending on how many people are involved, when
          distributed among everyone can turn out to be only dollars in each
          persons pocket.
        </p>
        <p>
          Although class action lawsuits usually don't have massive payouts,
          they can have large payouts depending on the circumstances and amount
          of people involved. But the whole point of a class action lawsuit is
          to collect everyone involved and fight collectively, showcasing the
          importance of the issue everyone experienced and that being held
          accountable is a must.
        </p>
        <div id="header8"></div>
        <h2>
          Why It's Important To Hire an Orange County Product Liability Lawyer
        </h2>
        <p>
          Although many individuals are capable of representing themselves in
          certain situations, it is strongly advised that you hire an attorney
          who specializes in the field of legal representation you need
          assistance with.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/importance-of-hiring-product-defect-attorney.jpg"
            width="100%"
            alt="orange county product liability legal representation"
          />
        </LazyLoad>
        <p>
          When you hire a skilled and experienced lawyer, not only are you
          reserving exclusive legal representation, but a professional that has
          years of practice, studies consistently, is experienced in actual
          legal circumstances and has the resources to choose from to better
          your odds of a favorable outcome, winning verdict and maximum
          compensation.
        </p>
        <div id="header9"></div>
        <h2>Contingency Fee - You Don't Pay If We Don't Win</h2>
        <p>
          Are you worried or have concerns with affording a lawyer? At Bisnar
          Chase, we promise to win your case with maximum compensation, or you
          do not pay. Now, many other firms claim to have this same promise, but
          have hidden fees that you must pay even if they lose your case.
        </p>
        <p>
          Do you need financial assistance or have medical bills you need to
          pay? We offer financial advancement, where we will help you take care
          of financial obligations you need to pay, during your case, before it
          has been settled.
        </p>
        <p>
          Depending on your case, situation and the complexity of the overall
          circumstances, limitations, restrictions and perimeters will be set in
          plain view before the case is signed, ensuring that you know exactly
          what you are going to get throughout the process of the case, because
          there is nothing worse than having high expectations crushed.
        </p>
        <p>
          We set everything out in plain view. No hidden fees, penalties or
          additional costs. Plus, you don't pay if we don't win, even if
          financial support was provided by our firm.
        </p>
        <p>
          Other credible firms decline many cases on a consistent basis that we
          end up signing and winning, many times being refereed to us from
          competitor firms, because they know we are capable of taking on
          complex and difficult cases and extracting a winning settlement and or
          verdict.
        </p>
        <div id="header10"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="orange county defective product lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-orange-county-product-liability-lawyers.jpg')"
            }}
          >
            <h2>Our Product Liability Law Firm is Ready to Win Your Case</h2>
          </div>
        </LazyLoad>
        <p>
          The skilled<strong> Orange County Product Liability Lawyers</strong>{" "}
          of <strong> Bisnar Chase</strong> have been representing and winning
          our client's cases for over <strong> 40 years</strong> and want to
          help you become one of our victories.
        </p>
        <p>
          Our law firm has won over<strong> $500 Million </strong>and have
          established a <strong> 96% success rate</strong>. If you want to hear
          from our past clients and their experience hiring Bisnar Chase
          Personal Injury Lawyers, visit our{" "}
          <Link to="/about-us/testimonials" target="new">
            Client Testimonials
          </Link>{" "}
          to hear the stories of families and individuals we have helped over
          the years.
        </p>
        <p>
          If you have experienced a personal injury, symptoms, pain and
          suffering, property damage or any other issues related to a defective
          product, call and discuss your case with an Orange County Product
          Defect Attorney at <strong> 949-203-3814</strong> for your Free
          consultation and case evaluation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  /* .nav-button {
    height: 35px;
    font-size: 1.4rem;
    width: 100%;
    margin-bottom: 1rem;
    margin: 1rem auto;
    display: flex;
    justify-content: space-between;

    .arrow-right {
      position: relative;
      left: 4px;
      top: 2px;

      @media (max-width: 1340px) {
        display: block;
        margin: 0 auto;
      }
    }

    @media (max-width: 1340px) {
      height: auto;
      display: block;
      padding: 1rem;
    }

    @media (max-width: 700px) {
      font-size: 1.2rem;
    }
  } */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
