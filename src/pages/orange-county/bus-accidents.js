// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/bus-over-freeway-orange-county-bus-accident-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Bus Accident Lawyer - Bisnar Chase"
        pageDescription="call 949-203-3814 for Orange County bus accident lawyers with over 3 decades of success. Free consultation with top rated injury attorneys who specialize in bus and public transportation crashes and serious injuries."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Bus Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Bus Accident Lawyer"
            title="Orange County Bus Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          When you or a loved one has been seriously injured in a bus accident,
          it is important that you get an experienced{" "}
          <strong> Orange County</strong>{" "}
          <strong> Bus Accident Attorney</strong> on your side right away. Bus
          companies and governmental agencies will have their people on site
          within minutes in order to protect their own interests. You need an
          honest, aggressive and committed bus injury attorney on your side,
          fighting for your rights and safeguarding your best interests.
        </p>
        <p>
          If you have been injured in a bus accident or if you have lost a loved
          one, please contact an experienced <strong> Orange County</strong> Bus
          Accident Lawyer to better understand your legal rights and options.{" "}
          <strong> Call for a free case review at 949-203-3814.</strong>
        </p>
        <h2>Recent Orange County Bus Accident Case Results</h2>
        <ul>
          <li>$7,998,073.00</li>
          <li>$3,075,000.00</li>
          <li>$1,500,000.00</li>
        </ul>
        <p>
          Experienced in catastrophic and serious personal injuries, Bisnar
          Chase has decades of trial experience to get you the maximum
          compensation. Our
          <strong> Orange County Bus Accident Lawyers</strong> will work
          tirelessly for you to recover fair compensation including medical and
          hospital bills, lost wages, recovery and future disability.
        </p>
        <h2>Types of Orange County Bus Accidents</h2>
        <p>
          Bus accident claims can be complex, regardless of whether they involve
          school buses, commercial buses or transit buses run by cities or
          governmental agencies. In Orange County, we have both public and
          private bus companies. It is important to understand the different
          types of bus accidents in order to help your{" "}
          <strong> Orange County Bus Accident Lawyer</strong> with your case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/I6P4MXquWzg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          <strong> Orange County School Bus Accidents:</strong> School bus
          drivers have the important responsibility of safely transporting our
          children. When school bus drivers operate negligently, they put the
          safety of our children at risk. When the negligence of a school bus
          driver causes an accident, the driver and his or her employer and the
          school district can be held liable. Many school districts in Orange
          County also have to make the important decision of whether to equip
          their school buses with seatbelts. Not all school boards vote in favor
          of seatbelts in buses given the tough budget constraints facing our
          school districts. Many Orange County school districts have not updated
          their fleet of buses leaving these large vehicles poorly maintained
          and sometimes, neglected.
        </p>
        <p>
          <strong> Crashes Involving Transit Buses:</strong> Riding the bus to
          work and school is quickly becoming a way of life in Orange County.
          Riders depend on mass transit bus systems to get to their daily
          destinations. It is important that the transit authorities and
          governmental agencies that run these vehicles maintain them in good
          shape. Failure to do so can place their passengers in grave danger.
        </p>
        <p>
          <strong> Tour Bus Accidents:</strong> Hundreds of Orange County
          residents, especially seniors, depend on tour buses to get to
          entertainment destinations such as resorts and casinos. Tour bus
          companies owe it to passengers to hire qualified and licensed drivers
          who have a clean record. Bus companies are also required to screen
          their drivers randomly for alcohol and drug use. Driver fatigue is
          often a factor in tour bus crashes. It is critical that drivers
          receive sufficient rest before they get on the road. Proper vehicle
          maintenance is also a must in order to avoid major injury crashes.
        </p>

        <h2>California Bus Accident Investigations</h2>
        <p>
          Investigation of bus accidents can get extremely complex. Usually,
          local law enforcement agencies investigate these crashes. However, in
          the case of major bus crashes, the{" "}
          <Link to="http://www.ntsb.gov/Pages/default.aspx" target="new">
            National Transportation Safety Board (NTSB){" "}
          </Link>{" "}
          may get involved. These investigations often continue for months
          before a determination is made. Investigators will likely look into
          several factors as they examine the cause of the accident, injuries or
          fatalities such as:
        </p>
        <ul>
          <li>Seatbelt use</li>
          <li>Crash-worthiness of the vehicle</li>
          <li>Qualifications of the bus driver</li>
          <li>Driver impairment</li>
          <li>Fatigue</li>
          <li>Defective products such as brakes, tires, etc.</li>
          <li>Driver records and bus maintenance logs</li>
        </ul>

        <h2>Seeking Legal Advice After an Orange County Bus Accident</h2>
        <p>
          Our <strong> Orange County Bus Accident Attorneys</strong> are skilled
          and experienced. We've taken on some of the toughest cases for our
          clients with <Link to="/case-results">great results</Link>.
        </p>
        <p>
          With over <strong> 40 Years </strong>of experience and a{" "}
          <strong> 96% Success Rate</strong>, the legal team at{" "}
          <strong> Bisnar Chase</strong> have won over{" "}
          <strong> $500 Million</strong> for our clients and may be able to help
          you too.
        </p>
        <p>
          If you've been seriously injured in an Orange County bus accident call
          today for a free consultation. <strong> Call 949-203-3814.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
