// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/amusement-park-orange-county-banner (1).jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Amusement Park Accident Attorney - 949-203-3814."
        pageDescription="We've helped over 12,000 injured clients with many of them living in Orange County. We are familiar with local courts and defense teams. Call 949-203-3814 for a Free Consultation with an Orange County theme park injury lawyer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Amusement Park Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="orange county amusement park accident lawyer"
            title="orange county amusement park accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Bisnar Chase, California Amusement Park Accident Lawyers, a prominent
          and respected personal injury law firm serving the people of
          California since 1978, has successfully represented many people who
          have been{" "}
          <Link
            to="/blog/category/personal-injury/amusement-park-accidents"
            target="new"
          >
            injured in amusement parks{" "}
          </Link>{" "}
          and on amusement park rides. Our trial lawyers have successfully
          collected over <strong> $500 Million</strong> in verdicts and
          settlements.
        </p>

        <p>
          We've helped over 12,000 clients with many of them living in Orange
          County. We are familiar with local courts and defense teams.{" "}
          <strong> Call 949-203-3814 </strong>for a
          <strong> Free Consultation </strong>with a theme park injury lawyer.
        </p>
        <h2>Deaths and Injuries Increase</h2>
        <p>
          Amusement park rides are described as any mechanical device which
          carries or conveys passengers along, around or over a fixed or
          restricted route within a defined area for the purpose of giving its
          passengers amusement. Amusement parks such as Disneyland, Magic
          Mountain, Knott's Berry Farm, etc., are a favorite form of
          entertainment for families and young people in California. Millions of
          people from all over the world come to California specifically to
          visit our amusement parks and ride our roller coasters.
        </p>
        <h2>Disturbing Amusement Park Statistics</h2>
        <p>
          Amusement parks can be fun for the whole family, friends, celebrations
          and weekend get togethers, but they can become dangerous when
          negligent park management, reckless behavior and other factors are
          introduced.
        </p>
        <p>
          Here are some statistics that will make you want to pay closer
          attention when enjoying an amusement parks amenities.
        </p>
        <ul>
          <li>
            Unfortunately, serious injuries and deaths have been steadily
            increasing since 1998.
          </li>
          <li>
            The Consumer Product Safety Commission reports that emergency room
            injuries from amusement park rides rose almost 87 percent in the
            last five years.
          </li>
          <li>
            In 1998 there were 9,200{" "}
            <strong> amusement park related injuries</strong> nationwide that
            required <strong> emergency room visits</strong> with an estimated
            2,100 of these injuries occurring on
            <strong> mobile amusement rides</strong> and 4,500 injuries involved{" "}
            <strong> fixed rides</strong>.
          </li>
          <li>
            Children between the ages of 10 and 14 were most at risk, and women
            were one and a half times more likely to be injured.
          </li>
        </ul>
        <h2>Top 15 Most Dangerous Amusement Park Rides</h2>
        <p>
          Amusement Parks throughout Orange County can range from ride to ride.
          Each individual adrenalin junkie has his or her own preference of
          thrill ride, but each type of ride has there own danger factor. Here
          is a list of the{" "}
          <strong> Top 15 Most Dangerous Amusement Park Rides </strong>in
          <strong> Orange County, California.</strong>
        </p>
        <p>
          Each ride type is in association with reported injuries (# of injuries
          reported per that ride during 1 recorded year):
        </p>
        <ol>
          <li>
            <strong> Steel Coasters</strong>: 495
          </li>
          <li>
            <strong> Water Slides</strong>: 438
          </li>
          <li>
            <strong> Boat Rides</strong>: 93
          </li>
          <li>
            <strong> Wooden Coasters</strong>: 87
          </li>
          <li>
            <strong> Water Park Playgrounds</strong>: 84
          </li>
          <li>
            <strong> Alpine Sliders</strong>: 72
          </li>
          <li>
            <strong> Car Rides</strong>: 72
          </li>
          <li>
            <strong> Log Rides</strong>: 72
          </li>
          <li>
            <strong> Slides</strong>: 69
          </li>
          <li>
            <strong> Carousels</strong>: 60
          </li>
          <li>
            <strong> Rafting</strong> <strong> Rides</strong>: 60
          </li>
          <li>
            <strong> Train</strong> <strong> Rides</strong>: 57
          </li>
          <li>
            <strong> Rides</strong> <strong> Operating</strong>{" "}
            <strong> in</strong> <strong> the</strong> <strong> Dark</strong>:
            54
          </li>
          <li>
            <strong> Go-Karts</strong>: 51
          </li>
          <li>
            <strong> Simulators</strong>: 51
          </li>
        </ol>

        <LazyLoad>
          <img
            src="/images/text-header-images/family-roller-coaster-oc.jpg"
            width="100%"
            alt="theme park accident attorneys"
          />
        </LazyLoad>

        <h2>Compensating You</h2>
        <p>
          If you or a loved one has been injured on an amusement park ride, you
          may be able to file a claim for negligence against the park owners,
          ride manufacturers and ride operators. Contact an experienced{" "}
          <Link to="/orange-county">Orange County personal injury lawyer </Link>{" "}
          immediately to learn about your options.
        </p>
        <p>
          <strong> Bisnar Chase</strong> and their team of experienced attorneys
          are dedicated to their clients and have committed themselves to
          getting the best possible results in each and every case. In every
          personal injury case there are significant costs associated with
          providing the best representation. <strong> Bisnar Chase</strong> have
          the financial resources available and can afford to hire the best
          experts in the field at no cost to you. We are aggressive,
          accomplished and feared by insurance companies, corporate wrongdoers
          and their law firms. We guide our clients through the legal and
          financial challenges they encounter on their way to recovery from
          their accidents and we achieve results.
        </p>
        <p>
          Whenever there is an accident that results in injury, whether in an
          automobile, boat or roller coaster, it is crucial that you document
          the injury and the circumstances surrounding the accident.
        </p>
        <ul>
          <li>
            Make sure you report the injury to the amusement park authorities
            immediately and insist that they write a report and get a copy.
          </li>
          <li>
            Record the names, addresses and phone numbers of any witnesses.
          </li>
          <li>
            Seek medical attention as soon as possible. Even if you feel that
            your injuries may be minor, they could lead to significant
            impairment in the future.
          </li>
          <li>
            Get the advice of an experienced amusement park accident attorney
            without delay.
          </li>
          <li>
            Do not volunteer any information regarding the accident to anyone
            other than your attorney and refer all inquiries directly to your
            lawyer.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/LyE142IiQ6U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Top 10 Most Common Injuries from Roller Coasters</h2>
        <p>
          Roller Coasters and other theme park thrill rides are meant to get
          your adrenalin pumping. Many people will not go on thrill rides
          because of fear, anxiety and the worry of something going horribly
          wrong.
        </p>
        <p>
          For the most part, amusement park rides are generally safe. Your odds
          of getting injured on a roller coaster are 1 in 24 Million, while
          emergency room visits see twice as many injuries from carousel rides.
        </p>
        <p>
          The <strong> Top 10 Most Common Injuries from roller coasters</strong>{" "}
          and other thrill rides consist of:
        </p>
        <ol>
          <li>
            <strong> Head, Neck and Back Injuries</strong>
          </li>
          <li>
            <strong> Bruises, Torn Ligaments and Lacerations</strong>
          </li>
          <li>
            <strong> Broken Bones</strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/orange-county/brain-injury" target="new">
                Traumatic Head Injuries{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/food-poisoning/foodborne-illness" target="new">
                Food Poisoning{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong> Aggressive or Violent Assault</strong>
          </li>
          <li>
            <strong> Stroke from Trauma to Ligaments in the Neck</strong>
          </li>
          <li>
            <strong> Drowning and Suffocation</strong>
          </li>
          <li>
            <strong> Limb Amputation and Dismemberment</strong>
          </li>
          <li>
            <strong> Internal Bleeding</strong>
          </li>
        </ol>
        <h2>Let Bisnar Chase Represent Your Case</h2>
        <p>
          Let our Orange County Amusement Park Lawyers represent your case. With
          over <strong> 39 Years </strong>of experience, over{" "}
          <strong> $500 Million </strong>won for clients and an established
          <strong> 96% Success Rate</strong>, our{" "}
          <strong>
            {" "}
            <Link to="/about-us/testimonials" target="new">
              Client Reviews and Testimonials{" "}
            </Link>
          </strong>{" "}
          may prove we are the ideal choice for your legal representation.
        </p>
        <p>
          At{" "}
          <strong>
            {" "}
            <Link to="/">Bisnar Chase </Link>
          </strong>{" "}
          we will promptly investigate the accident, determine fault, and seek
          to recover all legal damages. Call
          <strong> 949-203-3814</strong> today.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
