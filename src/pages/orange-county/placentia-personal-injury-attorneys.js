// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/placentia-car-accident-orange-county-personal-injury-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Placentia",
      population: 52206,
      totalAccidents: 1198,
      intersection1: "Imperial Hwy & Rose Dr",
      intersection1Accidents: 46,
      intersection1Injuries: 32,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 126.3,
      city1Name: "Brea",
      city1Index: 185.6,
      city2Name: "Anaheim",
      city2Index: 243.9,
      city3Name: "Yorba Linda",
      city3Index: 71.3,
      city4Name: "Fullerton",
      city4Index: 214.7
    },
    locationBox3: {
      intersection2: "Bastanchury Rd & Placentia Ave",
      intersection2Accidents: 28,
      intersection2Injuries: 29,
      intersection2Deaths: 0,
      intersection3: "Yorba Linda Blvd & Palm Dr",
      intersection3Accidents: 29,
      intersection3Injuries: 23,
      intersection3Deaths: 0,
      intersection4: "Kraemer Blvd & Yorba Linda Blvd",
      intersection4Accidents: 54,
      intersection4Injuries: 25,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Placentia Personal Injury Attorneys - Orange County, CA"
        pageDescription="Contact the top rated award winning personal injury attorneys in Placentia California. For over 4 decades we have recovered hundreds of millions. Free consultation and a no win, no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Placentia Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            title="Placentia Personal Injury Lawyers"
            alt="Placentia Personal Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          There are many types of accidents that can occur as a result of
          someone else's negligence, and you will want to contact an experienced
          <strong> Placentia Personal Injury Attorney</strong>.
        </p>
        <p>
          In such cases, victims may pursue financial compensation for their
          losses by holding the at-fault party accountable with a personal
          injury claim. This process can be complicated but it is often the only
          way for seriously injured victims to receive fair compensation for the
          losses they have suffered.
        </p>
        <p>
          If you or a loved one has been injured because of someone else's
          negligence, it is crucial that you first understand your legal rights
          and options.
        </p>
        <p>
          For immediate assistance <strong>Call 949-203-3814</strong> and
          receive a <strong>Free Consultation</strong> and{" "}
          <strong>Discuss Your Case</strong> with award winning{" "}
          <Link to="/orange-county">
            personal injury attorneys in Orange County
          </Link>
          .
        </p>

        <h2>Placentia Car Accidents</h2>
        <p>
          If you have been injured in a Placentia car accident, you may be able
          to pursue financial compensation for your medical bills, time away
          from work, loss of property and other related damages.
        </p>
        <p>
          You will have to prove that the other drivers negligence contributed
          to the accident. Was the other driver speeding, distracted, fatigued,
          under the influence or otherwise negligent? Was he or she under the
          influence of drugs or alcohol? Did the other motorist run through a
          red light or stop sign?
        </p>
        <p>
          In such cases, victims would be well advised to exchange information
          with the at-fault driver, contact the authorities, seek medical
          attention and call a skilled
          <strong> Placentia Personal Injury Attorney</strong>.
        </p>
        <h2>Placentia Slip-and-Fall Accidents</h2>
        <p>
          There are a number of unsafe conditions that can make properties
          dangerous for visitors. Cracked floors, wet walkways and damaged
          stairs are all examples of hazardous conditions that can cause a
          devastating slip-and-fall accident.
        </p>
        <p>
          It is the legal responsibility of all property owners to post warning
          signs, cones or tape around these types of hazardous locations so that
          visitors don't slip and fall.
        </p>
        <p>
          When a property owner fails to warn visitors of a hazardous condition
          and an accident occurs, a premises liability claim can help hold the
          owner accountable for the losses suffered by the victim.
        </p>
        <h2>Dog Bites and Attacks in Placentia</h2>

        <LazyLoad>
          <img
            src="/images/text-header-images/Orange-County-Dog-Bites-Banner-Image (1).jpg"
            width="100%"
            alt="Placentia personal injury attorneys"
          />
        </LazyLoad>
        <p>
          Under California's strict liability statute, dog owners can be held
          accountable for the damages their pets cause.{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=3342."
            target="new"
          >
            California's Civil Code Section 3342
          </Link>{" "}
          states: "The owner of any dog is liable for the damages suffered by
          any person who is bitten by the dog while in a public place or
          lawfully in a private place, including the property of the owner of
          the dog, regardless of the former viciousness of the dog or the
          owner's knowledge of such viciousness."
        </p>
        <p>
          If you have been injured by a dog bite, it's imperative to contact an{" "}
          <strong> Orange County Dog Bite Lawyer</strong> to help determine what
          your options are.
        </p>
        <h2>Placentia Defective Product Accidents</h2>
        <p>
          There are a number of different ways in which a product can be
          considered defective. Defective products may fail to work as
          advertised. Defective tires may experience tire tread separation on
          the roadway.
        </p>
        <p>
          Defective medications may cause adverse side effects that are not on
          the warning label. Defective toys may break off and become choking
          hazards.
        </p>
        <p>
          If a potentially defective product has injured you or a loved one, you
          would be well advised to preserve the item in question and contact an
          experienced
          <strong> Placentia Defective Product Attorney</strong>.
        </p>
        <h2>Other Types of Personal Injury</h2>
        <p>
          There are many other types of accidents that can result in injuries
          and civil litigation. Nursing homes who abuse or neglect the residents
          they are supposed to serve should be held accountable for their
          actions. The same is true for medical professionals who commit
          inexcusable or egregious errors.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/ajAkXU794Ko"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Protecting your Rights in California</h2>
        <p>
          If you or a loved one has been injured as the result of someone else's
          negligence or wrongdoing, the experienced Placentia personal injury
          lawyers at
          <strong> Bisnar Chase Personal Injury Attorneys</strong> can help
          protect your rights and hold the negligent parties accountable.
        </p>
        <p>
          If you are an innocent victim, you should not have to suffer the
          consequences of a situation in which someone else placed you.{" "}
          <Link to="/contact" target="new">
            Contact us
          </Link>{" "}
          to obtain more information about pursuing your legal rights.
        </p>
        <p>
          For immediate assistance contact a{" "}
          <strong> Placentia Personal Injury Lawyer </strong>at{" "}
          <strong> 949-203-3814</strong> and receive a{" "}
          <strong> Free Case Evaluation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
