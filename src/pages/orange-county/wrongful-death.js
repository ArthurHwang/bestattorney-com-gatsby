// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/mourning-girl-candles-rose-wrongful-death-orange-county-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Wrongful Death Attorney - Bisnar Chase"
        pageDescription="Contact the Orange County Wrongful death lawyers to see if you have a case. We've been helping family members for over 40 years and have taken on many wrongful death cases throughout California. Top rated trial attorneys with a 97% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Wrongful Death Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Wrongful Death Lawyers"
            title="Orange County Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Devastation. Grief. Anger.</strong> These are all emotions
          that can accompany the untimely passing of a loved one. At{" "}
          <strong> Bisnar Chase</strong>, we can provide you with an experienced{" "}
          <strong> Orange County Wrongful Death Lawyers</strong> to reduce the
          burden and stress after such a tragedy.
        </p>
        <p>
          What if your beloved spouse or relatives death could have been
          prevented? What if it was caused by a negligent property owner,
          employer, or through the wrongful actions of another?
        </p>
        <p>
          The highly skilled and experienced wrongful death lawyers at Bisnar
          Chase can provide answers to your questions.
          <strong> For immediate assistance call 949-203-3814.</strong>
        </p>
        <h2>
          Orange County Wrongful death can occur from many circumstances,
          including:
        </h2>
        <ul>
          <li>
            <strong> Defective products </strong>
          </li>
          <li>
            <strong> Poisoning</strong>
          </li>
          <li>
            <strong> Assault </strong>
          </li>
          <li>
            <strong> Car and truck accidents </strong>
          </li>
          <li>
            <strong> Slip and fall injuries</strong>
          </li>
          <li>
            <strong> Accidents in the workplace and other mishaps</strong>
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IUvwlj6ew0I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            John Bisnar discusses types of wrongful death claims and related
            information
          </blockquote>
        </center>

        <h2>Some of Our Recent Victories in Orange County</h2>
        <ul>
          <li>
            <strong> $16,444,904</strong> - Dangerous road condition, driver
            negligence
          </li>
          <li>
            <strong> $10,030,000</strong> - Premises negligence
          </li>
          <li>
            <strong> $9,800,000</strong> - Motor vehicle accident
          </li>
          <li>
            <strong> $8,500,000</strong> - Motor vehicle accident - wrongful
            death
          </li>
          <li>
            <strong> $7,998,073</strong> - Product liability - motor vehicle
            accident
          </li>
          <li>
            <strong> $5,000,000</strong> - Auto Defect
          </li>
          <li>
            <strong> $4,250,000</strong> - Product Liability
          </li>
        </ul>
        <h2>Mourning Makes Us Human</h2>
        <p>
          The family at Bisnar Chase are caring and respectful people who are
          always just a phone-call away. If you have experienced the loss of a
          loved one, we understand that this is a tremendously difficult time in
          your life.
        </p>
        <p>
          When it seems that nothing will give you back what you had and it
          feels like the world is coming to an end, remember tomorrow is a brand
          new day and that there is light at the end of the tunnel.
        </p>
        <p>
          There are typically 5 key emotional stages we go must experience as
          human beings when mourning the loss of a loved one, friend and or
          family member.
        </p>
        <p>
          Completing the process of mourning is important to us as individuals
          to acquire the strength to continue moving forward in our own lives.
          These stages include:
        </p>
        <ul>
          <li>
            <strong> Denial</strong> - The gravity of the situation hasn't hit
            and does not feel like it is reality
          </li>
          <li>
            <strong> Anger</strong> - When reality begins to set in but you're
            not ready to accept it
          </li>
          <li>
            <strong> Bargaining</strong> - Feelings of helplessness and
            vulnerability is often a need to regain control
          </li>
          <li>
            <strong> Depression</strong> - Relating to the loss and preparing
            for acceptance
          </li>
          <li>
            <strong> Acceptance</strong> - Understanding that reality is what it
            is and having the ability to move on in life
          </li>
        </ul>

        <p>
          If the circumstances of your loved one's death resemble any of the
          previous scenarios, you may be eligible to file a wrongful death
          lawsuit as a surviving relative of the deceased.
        </p>
        <p>
          Contact our <strong> Orange County wrongful death lawyers</strong> to
          see if you have a wrongful death case. Depending on circumstances you
          may be entitled to pain, suffering and loss.
        </p>
        <p>
          <strong> Call 949-203-3814</strong> for a{" "}
          <strong> Free Consultation</strong>.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/family-mourning-wrongful-death-orange-county-lawyers-attorneys.jpg"
            width="100%"
            alt="wrongful death lawyera orange county"
          />
        </LazyLoad>

        <h2>Orange County Wrongful Death Cases</h2>
        <p>
          The thought of a legal battle can seem daunting, frightening or
          burdensome. But it's the responsibility of survivors to challenge and
          fight back against negligent individuals or companies who cause death.
        </p>
        <p>
          Filing, fighting and winning a wrongful death lawsuit can send a
          powerful message and force negligent parties not only to pay for their
          actions, but to better protect others in the future.
        </p>
        <p>
          Call us to speak with one of our{" "}
          <strong> Orange County wrongful death lawyers</strong> today.
        </p>
        <p>
          If you can prove in court that the party responsible for the
          decedent's safety caused their wrongful death, you could be eligible
          to collect compensation not only for the loss of your family member
          and its emotional toll, but for the loss of the family member's future
          contribution to household finances.
        </p>
        <h2>Experienced Orange County Legal Support</h2>
        <p>
          Every wrongful death lawsuit is different, and every case has its own
          set of unique circumstances and legal challenges.
        </p>
        <p>
          As the family member of a loved one who fell victim to wrongful death,
          you'll need competent, discreet and compassionate legal counsel at
          your side as you seek justice.
        </p>
        <p>
          The <strong> Orange County</strong>{" "}
          <strong> wrongful death Lawyers</strong> at{" "}
          <strong> Bisnar Chase</strong> have been helping victims of wrongful
          death since 1978. Their offices have the resources you need to not
          only fight a wrongful death lawsuit, but to win it.
        </p>

        <p>
          The experienced lawyers at <strong> Bisnar Chase</strong> is not just
          here to win your case, but to offer our services on an emotional and
          supportive level as well.
        </p>
        <p>
          Our caring team of educated lawyers understand the pain of losing
          someone you love and were not ready to say goodbye to.
        </p>
        <p>
          <strong> Call us</strong> to speak with an <strong> lawyer</strong> or{" "}
          <strong> intake</strong> specialist immediately about any questions or
          concerns you may have about your <strong> case</strong>, at{" "}
          <strong> 949-203-3814</strong>.
        </p>
        <p>
          Don't suffer through the grief of <strong> wrongful death</strong>{" "}
          alone. Do it with the help of an experienced wrongful death attorney
          who can help you find closure - and justice.
        </p>
        <p>
          Contact <strong> the Orange County </strong>wrongful death attorneys
          <strong> Bisnar Chase</strong> today for more information on whether
          you might have a valid
          <strong> wrongful death claim</strong> - your{" "}
          <Link to="/contact" target="new">
            Free Consultation
          </Link>{" "}
          is <strong> no-obligation</strong> and
          <strong> completely</strong> <strong> confidential</strong>.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Orange county wrongful death attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/why-are-motorcycles-dangerous-los-angeles.jpg')"
            }}
          >
            <h2>Skilled, Experienced and Winning Wrongful Death Lawyers</h2>
          </div>
        </LazyLoad>

        <p>
          The team of attorneys at Bisnar Chase has over{" "}
          <strong> 40 years</strong> of experience and a{" "}
          <strong> 96% success rate</strong>.The skilled and experienced
          <strong> Orange County Wrongful Death Attorneys </strong>at{" "}
          <strong> Bisnar Chase </strong>have won over{" "}
          <strong> $500 Million</strong> for our clients and may be able to help
          you and your loved ones too.
        </p>
        <p>
          <strong> Call us for immediate assistance at 949-203-3814 </strong>for
          a free, no-hassle, confidential, no-obligation wrongful death lawyer
          consultation in Orange County.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
