/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "orange-county",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "",
    HTML: ``
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Orange County Injury Information",
    links: [
      {
        linkName: "Auto Defects",
        linkURL: "/orange-county/auto-defects"
      },
      {
        linkName: "Bicycle Accidents",
        linkURL: "/orange-county/bicycle-accidents"
      },
      {
        linkName: "Brain Injury",
        linkURL: "/orange-county/brain-injury"
      },
      {
        linkName: "Bus Accidents",
        linkURL: "/orange-county/bus-accidents"
      },
      {
        linkName: "Car Accidents",
        linkURL: "/orange-county/car-accidents"
      },
      {
        linkName: "Dog Bites",
        linkURL: "/orange-county/dog-bites"
      },
      {
        linkName: "Employment Law",
        linkURL: "/orange-county/employment-lawyers"
      },
      {
        linkName: "Hit and Run Accidents",
        linkURL: "/orange-county/hit-and-run-accidents"
      },
      {
        linkName: "Hoverboard Injuries",
        linkURL: "/orange-county/hoverboard-injury-lawyers"
      },
      {
        linkName: "Motorcycle Accidents",
        linkURL: "/orange-county/motorcycle-accidents"
      },
      {
        linkName: "Nursing Home Abuse",
        linkURL: "/orange-county/nursing-home-abuse"
      },
      {
        linkName: "Parking Lot Accidents",
        linkURL: "/orange-county/parking-lot-accidents"
      },
      {
        linkName: "Pedestrian Accidents",
        linkURL: "/orange-county/pedestrian-accidents"
      },
      {
        linkName: "Product Liability",
        linkURL: "/orange-county/product-liability-lawyers"
      },
      {
        linkName: "Slip and Fall Accidents",
        linkURL: "/orange-county/slip-and-fall-accidents"
      },
      {
        linkName: "Swimming Pool Accidents",
        linkURL: "/orange-county/swimming-pool-accidents"
      },
      {
        linkName: "Truck Accidents",
        linkURL: "/orange-county/truck-accidents"
      },
      {
        linkName: "Amusement Park Accidents",
        linkURL: "/orange-county/amusement-park-accident-lawyer"
      },
      {
        linkName: "Wage and Hour Disputes",
        linkURL: "/orange-county/wage-and-hour-disputes"
      },
      {
        linkName: "Wrongful Death",
        linkURL: "/orange-county/wrongful-death"
      },
      {
        linkName: "Contact OC Location",
        linkURL: "/orange-county/contact"
      },
      {
        linkName: "Orange County Home",
        linkURL: "/orange-county"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "What is my Case Worth?",
      videoUrl: "IuD-S72PMxk"
    },
    {
      videoName: "Do I Owe Money If My Case Is Lost?",
      videoUrl: "D3c_pxDeswg"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: false,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
