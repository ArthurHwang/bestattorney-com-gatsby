// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-car-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Car Accident Lawyer - Call (949) 203-3814 Now!"
        pageDescription="The top-rated Orange County car accident lawyers of Bisnar Chase have been winning cases for 40 years, maintaining a 96% success rate and recovering more than $500M. Call now for a free consultation with an expert car crash attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Car Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Car Accident Lawyers"
            title="Orange County Car Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Finding a qualified Orange County car accident lawyer to handle your
          case can be overwhelming. What will they charge, how long will it
          take, and what is fair compensation? These are all questions that you
          are likely to have.
        </p>

        <p>
          At{" "}
          <Link to="/" target="new">
            Bisnar Chase{" "}
          </Link>{" "}
          we specialize in dealing with all kinds of vehicle accident cases –
          from car crashes to
          <Link to="/orange-county/truck-accidents" target="new">
            truck accidents{" "}
          </Link>
          . We have built up an outstanding success rate over 40 years of
          helping car accident victims fight for the compensation they deserve.
        </p>

        <p>
          Our skilled attorneys are also trial lawyers and have a wealth of
          experience in Orange County courtrooms, helping more than 12,000
          clients and taking on even the most complex car accident cases.
        </p>

        <p>
          Our passion for plaintiff justice goes beyond compensation. We set you
          and your family on a path to physical, emotional, and financial
          recovery from the moment we receive your online inquiry or phone call.
        </p>

        <p>
          For immediate assistance from a top-quality{" "}
          <Link to="/orange-county" target="new">
            Orange County personal injury lawyer
          </Link>
          , call us on <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>.
        </p>

        <h2>The Most Dangerous Roads in Orange County</h2>

        <p>
          With so many drivers on the roads in and around Orange County, driving
          can be dangerous. A huge range of driving dangers are present on the
          streets, on highways, and in{" "}
          <Link to="/orange-county/parking-lot-accidents">parking lots </Link>{" "}
          on a daily basis. These can include heavy traffic, car accidents,
          breakdowns, grand theft auto, speeding, reckless driving, and many
          other potentially dangerous activities.
        </p>
        <p>The most dangerous roads in Orange County include:</p>
        <ol>
          <li>Ortega Highway</li>
          <li>Laguna Canyon Road</li>
          <li>Beach Boulevard (State Route 39)</li>
        </ol>
        <p>
          Each of these roadways accounts for a high volume of the accidents in
          Orange County. According to
          <Link to="https://www.ortega74.com/logs" target="new">
            Ortega.com
          </Link>
          , there were 212 accidents on Ortega Highway through the first four
          months of 2019 alone, resulting in three fatalities.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d602608.2483990466!2d-117.34516188352826!3d33.459231033524745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sortega+highway!5e0!3m2!1sen!2sus!4v1554829826971!5m2!1sen!2sus"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Car Accident and Safety Statistics</h2>

        <p>
          There are a lot of safe drivers on the road, but there are also plenty
          of reckless and dangerous drivers as well. Here are some statistics
          covering the main{" "}
          <Link to="/resources/top-causes-of-car-accidents" target="new">
            causes of car accidents
          </Link>{" "}
          in Orange County to help you steer clear of road dangers:
        </p>

        <ul>
          <li>
            There is an average of 32,500 car accidents every year in Orange
            County, according to our comprehensive report on{" "}
            <Link to="/resources/car-accident-statistics" target="new">
              Orange County auto accident statistics
            </Link>
            .
          </li>
          <li>2.6% of these accidents involved pedestrians.</li>
          <li>3.1% involved motorcycles.</li>
          <li>4.1% involved bicycles.</li>
          <li>3.9% involved trucks.</li>
          <li>
            An unbelievable 10.9% of all crashes{" "}
            <Link to="/dui">involved alcohol </Link> or another substance.
          </li>
        </ul>
        <p>
          Though the amount of crashes in the past years has been declining, a
          dangerous traffic situation can happen at any time and change you and
          your family's lives forever.
        </p>

        <h2>Top-10 Most Common Car Accident Causes in Orange County</h2>

        <p>
          Some of the most common causes of car accidents in Orange County are:
        </p>
        <ol>
          <li>
            <span>Speeding</span>
          </li>
          <li>
            <span>Running red lights</span>
          </li>
          <li>
            <span>Distracted driving</span>
          </li>
          <li>
            <span>Failing to stop at stop signs</span>
          </li>
          <li>
            <span>Tailgating</span>
          </li>
          <li>
            <span>Dangerous lane changes – often without looking</span>
          </li>
          <li>
            <span>Vehicle faults and blown tires</span>
          </li>
          <li>
            <span>Driving while intoxicated by alcohol or drugs</span>
          </li>
          <li>
            <span>Driver error due to high volumes of traffic</span>
          </li>
          <li>
            <span>Road rage</span>
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Broken window car accident"
            style={{
              backgroundImage:
                "url('/images/text-header-images/orange-county-car-accident-attorney.jpg')"
            }}
          >
            <h2>Common Injuries Suffered in Orange County Car Accidents</h2>
          </div>
        </LazyLoad>

        <p>
          Car crash victims in Orange County can sustain a wide range of
          injuries. It will depend on the type of accident they are involved in,
          where their car was impacted, and the force of the collision, among
          other factors.
        </p>
        <p>Some of the most likely car accident injuries include:</p>
        <ul>
          <li>Cuts and bruises</li>
          <li>Broken bones</li>
          <li>Whiplash</li>
          <li>
            <Link to="/head-injury" target="new">
              Head injuries{" "}
            </Link>
          </li>
          <li>Internal bleeding</li>
          <li>
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              Spinal Cord Injuries{" "}
            </Link>
          </li>
          <li>Emotional pain and anxiety</li>
          <li>Death</li>
        </ul>
        <p>
          Car crashes have a wide range of potential outcomes and can leave
          victims with life-changing injuries.
        </p>

        <h2>What To Do If You Have Been in a Car Crash in Orange County</h2>

        <ol>
          <li>
            <span>
              <b>Make sure you get to safety.</b> Experts say that staying next
              to your car after a crash can be dangerous, depending on where it
              happens.
            </span>
          </li>

          <li>
            <span>
              <b>Get medical attention.</b> Get the treatment you need as soon
              as possible after a crash, either from a paramedic at the scene,
              or later from your doctor. This is vital for your own health and
              wellbeing, as well as helping any future legal claim.
            </span>
          </li>

          <li>
            <span>
              <b>Call the police.</b> If there has been a car crash, the police
              should be involved. You can also request a copy of the police
              report at a later date to support a fault claim.
            </span>
          </li>

          <li>
            <span>
              <b>Document the scene and preserve evidence.</b> When you have
              been in a car accident in Orange County, you should document
              everything. Take pictures or videos of the scene and vehicles
              using your phone, record license plate numbers, and secure witness
              statements or contact details.
            </span>
          </li>

          <li>
            <span>
              <b>Organize your evidence.</b> Make sure the evidence you have
              collected is well organized. Request copies of police reports and
              proof of medical treatment and bills.
            </span>
          </li>

          <li>
            <span>
              Contact an <strong> Orange County Car accident lawyer</strong> who
              specializes in OC crash cases for help.
            </span>
          </li>
        </ol>

        <h2>Why Hire Bisnar Chase?</h2>

        <p>
          Following a serious car wreck, you are going to need the best legal
          representation available. You will need a car accident lawyer in
          Orange County who knows how to play hard-ball with insurance adjusters
          and has a{" "}
          <Link to="/case-results" target="new">
            proven winning record{" "}
          </Link>{" "}
          in Orange County courtrooms.
        </p>
        <p>
          Some car accidents are relatively minor, but can still leave a lasting
          impact on the victim. Other incidents will leave crash victims with
          serious injuries and disabilities. For many people, this results in a
          major adjustment.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/orange-county-car-accident.jpg"
            width="50%"
            className="imgleft-fluid"
            alt="The front of a badly-damaged car which has been wrecked in a serious accident."
          />
        </LazyLoad>

        <p>
          Bisnar Chase aims to take the strain off the victim. We have a very
          skilled Orange County legal team with an outstanding record of success
          in local courtrooms. Our goal is to ease the burden on those who have
          been injured in car collisions and make sure they get the justice and
          compensation that they deserve.
        </p>
        <p>
          Our success rate, long history of highly-satisfied clients, and
          hundreds of millions of dollars recovered, make us the go-to car
          accident attorneys in Orange County.
        </p>
        <p>
          We also offer a 'No Win, No Fee' guarantee, to make sure our clients
          do not have to pay a dime unless their case is successful. We have
          built a reputation on providing a client experience which is second to
          none, and have been recognized by Best Lawyers, Super Lawyers, Lexis
          Nexis, Avvo, and many more, as well as earning an{" "}
          <Link
            to="http://www.bbb.org/sdoc/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-in-newport-beach-ca-100046710/"
            target="new"
          >
            A+ rating
          </Link>{" "}
          with the Better Business Bureau. Our caring Orange County car accident
          attorneys here to serve you.
        </p>

        <h2>Some of Our Orange County Car Accident Case Results</h2>

        <ul>
          <li>
            <b>$16,444,904</b> - Dangerous road condition, driver negligence
          </li>
          <li>
            <b>$9,800,000</b> - Motor vehicle accident
          </li>
          <li>
            <b>$8,500,000</b> - Motor vehicle accident - wrongful death
          </li>
          <li>
            <b>$5,000,000</b> - Auto Defect
          </li>
          <li>
            <b>$3,000,000</b> - Motor vehicle accident (Insurance offered $100k,
            we won $3 million)
          </li>
        </ul>

        <p>
          These are just some of the major payouts we have secured for our
          clients. We strive to make sure all clients get the justice they
          deserve.
        </p>

        <h2>Orange County Car Accident Compensation</h2>

        <p>
          How much are you likely to receive if you have been the victim of a
          car crash in Orange County?
        </p>
        <p>
          The frustrating answer is that it will always depend on the
          circumstances of the specific accident. Some of the factors which will
          affect the value of a claim include:
        </p>
        <ul>
          <li>Vehicle or property damage caused</li>
          <li>Extent of injuries</li>
          <li>Medical bills and expenses</li>
          <li>Costs of ongoing rehabilitation and care</li>
          <li>Pain and suffering</li>
          <li>Lost wages and work opportunities</li>
        </ul>

        <h2>Navigating Orange County Courts</h2>

        <p>
          The car accident lawyers at Bisnar Chase are equally adept at
          negotiating out of court settlements and battling for our clients in
          the courtroom. We are highly effective trial lawyers, and will not
          hesitate to go to trial if we feel that is the best choice for our
          clients.
        </p>
        <p>
          It is important to hire an Orange County car crash lawyer who has a
          good knowledge of the OC courts. Having the advantage of knowing the
          rules and procedures of local jurisdictions is an edge that locally
          planted attorneys always have.
        </p>
        <p>
          You'll want an attorney that knows the judges, defense attorneys,
          court personnel, and culture of those particular courtrooms.
        </p>
        <p>
          Orange County, California, is a large area which covers several major
          cities. We can provide top-quality representation to residents of:
        </p>
        <ul>
          <li>Irvine</li>
          <li>Huntington Beach</li>
          <li>Newport Beach</li>
          <li>Santa Ana</li>
          <li>Anaheim</li>
          <li>Fullerton</li>
          <li>Costa Mesa</li>
          <li>Any other area of Orange County, and even further afield</li>
        </ul>
        <p>
          You can review our{" "}
          <Link to="/resources/case-timeline" target="new">
            interactive timeline of a typical car accident case{" "}
          </Link>{" "}
          to get a better understanding of what a car accident attorney in
          Orange County can do for you.
        </p>

        <h2>Top 11 Questions to Ask Your Orange County Car Accident Lawyer</h2>

        <p>
          Here are some of the top questions that your car accident attorney can
          help you answer after a crash:
        </p>

        <ol>
          <li>
            <span>
              Do I contact my insurance company or the insurance company of the
              person who hit me?
            </span>
          </li>
          <li>
            <span>
              How do I get compensated for lost time and income from work?
            </span>
          </li>
          <li>
            <span>
              Who will pay my medical bills and when will they pay them?
            </span>
          </li>
          <li>
            <span>What information should I give to insurance adjusters?</span>
          </li>
          <li>
            <span>
              What should I do if the person who hit me is uninsured or the
              accident is a hit and run?
            </span>
          </li>
          <li>
            <span>Should I let an insurance adjuster record what I say?</span>
          </li>
          <li>
            <span>
              What should I do if the person who hit me did not carry enough
              insurance for my damages?
            </span>
          </li>
          <li>
            <span>Where should I go to get my car repaired or replaced?</span>
          </li>
          <li>
            <span>What is my car accident case worth?</span>
          </li>
          <li>
            <span>
              Should I hire a consumer attorney or car accident attorney?
            </span>
          </li>
          <li>
            <span>What does an attorney cost and should I pay a retainer?</span>
          </li>
        </ol>

        <h2>The Insurance Adjuster is Not Your Friend</h2>
        <LazyLoad>
          <img
            src="/images/lawyers/john-bisnar-orange-county-car-accident-lawyer.jpg"
            alt="Orange County car accident lawyer John Bisnar"
            className="imgleft-fixed mb"
          />
        </LazyLoad>

        <p>
          It is best to place your trust in a lawyer who is working for you –
          with your best interests in mind – rather than an insurance agent
          whose job is to save money for their firm.
        </p>

        <p>
          <span>
            California car accident attorney{" "}
            <Link to="/attorneys/john-bisnar" target="new">
              John Bisnar{" "}
            </Link>{" "}
            said:{" "}
          </span>
          "Thinking the insurance company is on your side is a big mistake. Car
          insurance companies give bonuses and pay raises to adjusters who
          settle claims in the quickest time and for the least money. The
          insurance adjuster is not your friend."
        </p>

        <h2>Finding the Right Orange County Car Accident Lawyer</h2>

        <p>
          Choosing the right lawyer for your injury claim can be a big decision,
          and one which you should not take lightly. The success of your claim
          can depend on the representation provided by your attorney, so it is
          essential that you choose a firm with a great reputation.
        </p>
        <p>
          You should ask these questions before hiring an attorney or law firm:
        </p>
        <ul>
          <li>How long have they been in business?</li>
          <li>How long has the attorney handling your case been licensed?</li>
          <li>Have they worked on cases similar to your accident?</li>
          <li>Are they flexible and considerate to your needs?</li>
          <li>
            Does the attorney seem like a good listener or do they talk over
            you?
          </li>
          <li>Do they go to court if need be (trial lawyers)?</li>
          <li>
            <Link
              to="https://hirealawyer.findlaw.com/choosing-the-right-lawyer/researching-attorney-discipline.html"
              target="new"
            >
              Check their legal standing{" "}
            </Link>{" "}
            with the Bar Association of their state
          </li>
          <li>
            Do they truly offer a free consultation and advance all costs?
          </li>
          <li>Is the accident lawyer near me or close to my work?</li>
        </ul>

        <h2>Superior Representation with Bisnar Chase</h2>

        <p>
          The Orange County car accident lawyers of Bisnar Chase can provide the
          skill and support needed for crash victims to find justice.
        </p>
        <p>
          Our firm has been a part of the Orange County community for more than
          40 years. Over that time, we have established a{" "}
          <b>96% success rate</b> and won more than
          <b>$500 million</b> for our clients.
        </p>
        <p>
          For immediate help,{" "}
          <Link to="tel:+1-949-203-3814">call (949) 203-3814</Link>, or visit
          our Orange County law offices, based in Newport Beach. You can also
          click to{" "}
          <Link to="/orange-county/contact" target="new">
            contact us
          </Link>
          .
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
