// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/hoverboard-accidents-dangers-orange-county-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Hoverboard Injury Lawyer - Bisnar Chase"
        pageDescription="Orange County hoverboard injury lawyers with 3 decades of success. Call 949-203-3814 for a free case review from top rated injury attorneys"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Hoverboard Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Hoverboard Injury Lawyers<"
            title="Orange County Hoverboard Injury Lawyers<"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Those who suffer injuries as a result of hoverboards that explode or
          catch fire can seek compensation for damages including medical
          expenses, lost wages, hospitalization, rehabilitation costs, scarring
          or disfigurement, permanent injuries, disabilities, pain and suffering
          and emotional distress.
        </p>
        <p>
          The knowledgeable{" "}
          <strong>
            {" "}
            Orange County Hoverboard Fire and Burn Attorneys at Bisnar Chase
          </strong>{" "}
          have extensive experience and a{" "}
          <Link to="/case-results" target="new">
            successful track record
          </Link>{" "}
          financially pursuing manufacturers who put{" "}
          <Link to="/orange-county/product-liability-lawyers">
            defective products{" "}
          </Link>
          in the market that cause serious injury and harm to consumers.
        </p>
        <p>
          If you have been injured as a result of a lithium-ion explosion or
          hoverboard fire, please call us at <strong> (949) 203-3814</strong>{" "}
          for a free consultation and a comprehensive case evaluation with an{" "}
          <strong> Orange County Personal Injury Lawyer</strong>.
        </p>

        <div className="well" role="alert">
          <h2>Do You Have A Case?</h2>
          <p>
            Hoverboard manufacturers are obligated to make sure their products
            are safe and are considered negligent if their products are prone to
            explode and injure anyone nearby. However,
            <span>
              injuries that occur from simple interaction with a hoverboard such
              as falls or wipeouts
            </span>{" "}
            are not considered negligence on behalf of the manufacturer, and
            therefore <span>are not eligible for a lawsuit.</span>
          </p>
          <p>
            <strong> Call (949) 203-3814</strong>
          </p>
        </div>

        <h2>Hoverboards a Danger</h2>
        <LazyLoad>
          <img
            src="/images/hoverboard-fires.jpg"
            width="250"
            alt="orange county hoverboard injury lawyers"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Self-balancing hoverboards, once in high demand as the latest trend in
          electronic skateboards, are now the focus of a safety investigation in
          the United States.
        </p>
        <p>
          Airlines have banned these devices from their cargo areas, colleges
          have prohibited them on their campuses and some online retailers like{" "}
          <Link
            to="http://www.today.com/news/amazon-pulls-hoverboards-site-amid-reports-them-catching-fire-t61521"
            target="new"
          >
            Amazon are pulling certain brands of hoverboards
          </Link>{" "}
          off their inventories following several incidents nationwide where
          these contraptions either caught fire or exploded.
        </p>
        <p>
          Experts say what is causing these fires and explosions are shoddily
          manufactured lithium-ion batteries. Exploding lithium-ion batteries
          have the potential to cause major or even catastrophic burn injuries.
          If you have been injured by some of these dangerous and defective
          products, it is important that you explore your legal rights and
          options.
        </p>
        <h2>What are Lithium-Ion Batteries?</h2>
        <p>
          Lithium-ion batteries given power to so many of the electronic gadgets
          we use today from cell phones and laptops to power tools and
          children's toys. Lithium is hazardous by itself. It can explode if it
          comes into contact with water or oxygen. But the companies that make
          these batteries have to minimize the danger of an explosion. So, with
          all the new technology we have today, the battery cell containing
          lithium is usually not a problem. But the electronic circuitry
          surrounding the cell can cause problems if the battery is not properly
          manufactured. Lithium technology is safe, but only if the user doesn't
          overcharge the battery or doesn't let it get it too hot.
        </p>
        <h2>Why Do The Batteries Explode?</h2>
        <p>
          Lithium-ion batteries in reputed brands of laptops and cell phones are
          often built according to strict standards. However, counterfeit
          lithium-ion batteries tend to lack much needed safety features. So, if
          a faulty lithium-ion battery is overcharged or overheats, when a
          person is using it in a hoverboard or has it plugged to a charger, the
          ions can gather in one spot and be deposited and metallic lithium
          within the battery. Once the oxygen bubbles inside the battery reaches
          the lithium, it causes an ignition and explosion. Well-made lithium
          batteries on the other hand will automatically turn off if they get
          too hot. Using the wrong charger can also cause such a reaction.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/young-adult-cruising-on-hoverboard-sidewalk.jpg"
            width="100%"
            alt="hoverboard injuries"
          />
        </LazyLoad>

        <h2>Understanding the Dangers of Hoverboards</h2>
        <p>
          The U.S. Consumer Product Safety Commission is investigating a number
          of hoverboard fires across the country where the lithium-ion batteries
          in hoverboard caught fire destroying bedrooms and even entire homes.
          Some hoverboards exploded while charging, others when riders were on
          it. There is no single reason why these hoverboards are exploding.
          There is also no particular brand of hoverboard to avoid. They seem to
          come from a number of different factories in China.
        </p>

        <p>
          Nearly all of these toys are made in China, many in the cheap
          tech-manufacturing hub of Shenzen. Millions of hoverboards have been
          shipped out of China to keep up with the burgeoning demand, especially
          in the United Kingdom and the United States. The{" "}
          <Link to="http://www.nationaltradingstandards.uk/" target="new">
            UK's National Trading Standards
          </Link>{" "}
          inspected
          <strong> 17,000 hoverboards</strong> recently and found that of the
          lot they inspected <strong> 88 percent</strong> are unsafe because of
          issues with the plug, cabling, charger, battery or the cut-off switch
          within the board, which often fails.
        </p>
        <h2>Hoverboard Injuries</h2>
        <p>
          Lithium-ion explosions occur with such power that they can result in
          severe injuries. In addition to the riders themselves, others who may
          be in the vicinity could also get seriously injured. Some of the
          common injuries that are suffered in hoverboard accidents include:
        </p>
        <ul>
          <li>
            <strong> Burn injuries:</strong> These injuries could range from
            first- and second-degree burns to severe or even catastrophic burns
            that could cause permanent scarring disfigurement, nerve and tissue
            damage. These types of injuries can be extremely costly to treat,
            not to mention, painful and traumatic for victims who suffer them.
          </li>
          <li>
            <strong> Bone fractures:</strong> Explosions could also result in
            multiple bone fractures, which could take a long time to heal and
            may require extensive rehabilitation.
          </li>
          <li>
            <strong> Head injuries:</strong> An explosion is likely to throw the
            rider off the hoverboard causing him or her to suffer traumatic
            brain injuries, particularly if he or she is not wearing a helmet.
            Such injuries could result in permanent disabilities.
          </li>
          <li>
            <strong> Amputations:</strong> Severe explosions could result in
            riders losing their fingers, toes or even limbs.
          </li>
          <li>
            <strong> Facial injuries:</strong> Depending on how and where the
            explosion occurs, victims could suffer severe facial injuries. They
            may suffer eye and ear injuries that could lead to loss of vision or
            hearing, broken nose or teeth or severe scarring.
          </li>
        </ul>

        <p>
          This{" "}
          <Link to="http://www.cbsnews.com/cbs-this-morning/" target="new">
            CBS
          </Link>{" "}
          Video shows just how dangerous and deadly the{" "}
          <Link
            to="/blog/faa-issues-warning-about-lithium-ion-batteries"
            target="new"
          >
            Lithium-Ion Batteries
          </Link>{" "}
          inside hoverboards and other products can be.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/o27DLvSDITw"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Product Liability Issues</h2>
        <p>
          All product manufacturers have a legal obligation to make sure their
          products are safe to use. Those who make defective products can be
          held liable for the injuries caused by those products. Products may be
          faulty due to a manufacturing defect that occurs during the
          manufacturing process. This may happen due to inexperienced workers or
          substandard materials used in the production of the items. The product
          may be defectively designed. This means that the product is inherently
          dangerous because of the manner in which it is designed. So, it is
          dangerous regardless of how it is manufactured.
        </p>
        <p>
          A marketing defect or "failure to warn" occurs when the manufacturer
          does not caution consumers about dangers that can be avoided when the
          user is made aware of them. For example, hoverboard manufacturers
          should warn consumers about the dangers of overcharging the devices or
          using incompatible chargers. This could help avoid potentially lethal
          lithium-ion explosions.
        </p>
        <p>
          A number of different parties could be held potentially liable for
          injuries caused by hoverboard lithium-ion battery explosions including
          the manufacturer, the distributor, the retailer, the manufacturers of
          accessories such as chargers, etc. An experienced{" "}
          <Link to="/orange-county" target="new">
            Orange County personal injury lawyer
          </Link>{" "}
          will be able to identify responsible parties and hold them
          accountable.
        </p>
        <h2>Seeking Injury Compensation for a Hoverboard Injury</h2>
        <p>
          Please contact our{" "}
          <strong> Orange County Hoverboard Fire and Injury Lawyers</strong>{" "}
          today if you have suffered injuries from a hoverboard product. Our
          dedicated legal team will evaluate your case and advise you to whether
          you are entitled to compensation. <strong> Call 949-203-3814</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
