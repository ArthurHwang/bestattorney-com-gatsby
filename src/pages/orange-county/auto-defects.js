// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/mechanic-auto-defect-banner-oc.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Auto Defect Attorney - Bisnar Chase"
        pageDescription="If you have been injured in a car accident that you feel may have been caused by an auto defect, please call our experienced team of Orange County auto defect attorneys for a free consultation. Call 949-203-3814 to reach our legal team."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Auto Defect Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="orange county auto defect lawyer"
            title="orange county auto defect lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been injured in a{" "}
          <Link to="/orange-county/car-accidents">car accident </Link> that you
          feel may have been caused by an auto defect, please call our
          experienced team of<strong> Orange County Auto Defect Lawyers</strong>{" "}
          for a free consultation. <strong> Call 949-203-3814</strong> to reach
          our legal team.
        </p>
        <p>
          Bisnar Chase has spent over three decades representing those injured
          by auto defects or dangerous design. Our legal team is comprised of
          experienced Orange County auto defect attorneys with decades of trial
          experience fighting big automakers. We've collected more than{" "}
          <strong> $500 Million</strong> for our clients and may be able to
          represent you too.
        </p>
        <h2>Recent Orange County Auto Defect Victories</h2>
        <ul>
          <li>
            <strong> $32,698,073</strong> - Auto Defect - Seat manufacturers,
            Johnson Controls
          </li>
          <li>
            <strong> $5,000,000.00</strong> - Auto Defect
          </li>
          <li>
            <strong> $3,075,000.00</strong> - Product Defect - motor vehicle
            accident
          </li>
          <li>
            <strong> $2,600,000.00</strong> - Auto Defect
          </li>
          <li>
            <strong> $2,250,000.00</strong> - Auto Defect - motor vehicle
            accident
          </li>
        </ul>

        <h2>Litigating Orange County Auto Defect Cases</h2>
        <LazyLoad>
          <img
            src="/images/attorney-chase.jpg"
            height="141"
            className="imgright-fixed"
            alt="Orange County auto defect lawyer, Brian Chase"
          />
        </LazyLoad>

        <p>
          <span>
            <Link
              to="/attorneys/brian-chase"
              title="Brian Chase"
              target="new"
              alt="Auto defect lawyer"
            >
              Brian Chase
            </Link>
            , Senior Partner:
          </span>{" "}
          “Automakers need to be held accountable for the dangers and risks they
          pose to the public.”
        </p>
        <p>
          For the <strong> Orange County Auto Defect Lawyers</strong> at Bisnar
          Chase, litigating auto product liability cases is very rewarding, both
          personally and professionally. These are real-life David and Goliath
          scenarios where we, representing the common man, take on corporate
          giants that routinely put profit over safety. The satisfaction in
          these cases also comes from obtaining a great result for the client in
          the form of monetary compensation and better healthcare that help
          enhance his or her quality of life. But, we also know that auto defect
          cases, while rewarding, can be grueling, intense and sometimes,
          frustrating.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/brian-red-car-orange-county-auto-defect-accident-lawyers-attorneys.jpg"
            width="100%"
            alt="Defective vehicle attorney Brian Chase"
          />
        </LazyLoad>

        <p>
          <b>
            Any attorney who takes on auto defect cases must act decisively on
            the following three matters right away:
          </b>
        </p>
        <ul>
          <li>
            <b>Assessing damages</b>: Auto defect cases can be expensive to
            prosecute. This means that the damages must be significantly large.
            Our Orange County auto defect law firm invests hundreds of thousands
            of dollars in just one claim, which means that the damages should
            reach the seven-figure-plus mark to make the case economically
            feasible for both the client and the firm. This often means that we
            cannot take on every auto defect cause. But, by taking cases
            involving those who are severely injured, we can have an impact on
            the way manufacturers design their vehicles.
          </li>
          <li>
            <b>Preserving the vehicle</b>: The vehicle is often the most
            important piece of evidence in an auto defect case. So, it is
            important that the vehicle is preserved immediately. There is
            usually an abundance of forensic evidence in or on a vehicle that
            must be examined in order to determine the cause of the accident or
            injuries as well as liability. Without the vehicle, there is
            virtually no basis on which a case can be built. For example, when
            seatbelt failure is suspected, the restraint system is examined for
            evidence of "loading" to support an opinion that the occupant was
            properly restrained at the time of the crash.
          </li>
          <li>
            <b>Access to experienced experts</b>: A knowledgeable and
            resourceful auto defects lawyer will also be able to retain
            experienced experts, who obviously have the expertise to testify in
            these cases and convince jurors that the injuries were caused by the
            defective vehicles or parts.
          </li>
        </ul>
        <p>
          Brian Chase discussed our{" "}
          <Link to="/case-results/jaklin-romine" target="new">
            $24 Million Auto Defect Case with Jaklin Romine
          </Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/PFYyZrS5BLY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What Constitutes an Orange County Auto Defect Case ?</h2>
        <p>
          It is quite common for someone to be involved in an auto defect case
          and not even know it. A case involving a defect, but not a serious
          injury, is not a case that is economically feasible to pursue for
          either the client or the law firm. Here are just a few examples of the
          types of cases our <strong> Orange County Auto Defect Lawyers</strong>{" "}
          have pursued successfully:
        </p>
        <ul>
          <li>
            An SUV rollover crash that involves more than driver error. For
            example, it may involve a weak roof that crushed the occupants or an
            electronic stability control (ESC) system that failed.
          </li>
          <li>
            When someone who is buckled up gets partially or fully ejected, the
            case may involve a defective restraint system. It is important to
            remember that ejection from a vehicle does not necessarily mean that
            the occupant was not wearing the seatbelt.
          </li>
          <li>
            If someone's car catches fire, the case may involve a defective gas
            tank or a faulty part that overheated.
          </li>
          <li>
            When a car seat collapses backward in an accident, it may be a{" "}
            <Link to="/auto-defects/seatback-failure" target="new">
              seatback failure{" "}
            </Link>{" "}
            case.
          </li>
          <li>
            A tire may fail due to tread separation or other manufacturing
            defects.
          </li>
          <li>
            If someone is ejected through a door, the door latch may have been
            defective.
          </li>
          <li>
            When airbags fail to deploy or deploy suddenly without warning, the
            consequences can be devastating.
          </li>
        </ul>
        <p>
          <Link
            to="https://www-odi.nhtsa.dot.gov/recalls/recallprocess.cfm"
            target="new"
          >
            What every vehicle owner should know{" "}
          </Link>{" "}
          from safercar.gov.
        </p>
        <p>
          <em>Brian Chase discusses why there are so few vehicle recalls.</em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/pb3gVUmYTCY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Catastrophic Injuries in California Auto Defect Cases</h2>
        <p>
          A number of California auto defect cases we handle tend to be{" "}
          <Link to="/catastrophic-injury" target="new">
            catastrophic injury{" "}
          </Link>{" "}
          or wrongful death cases. These are cases where someone has lost a
          spouse, a child or a loved one as the result of a defective auto.
          There are cases where individuals have suffered irreversible brain
          damage or have been paralyzed in a car crash. For us, these are not
          just cases. These people we represent are human beings, not numbers or
          statistics. Their personal stories remind us about how quickly life
          can change and how suddenly the world we once knew and cherished could
          vanish or be turned upside down.
        </p>
        <p>
          In these types of cases, we know that money cannot take the place of a
          lost limb, a functioning brain or a loved one. But, what a recovery
          does in these cases is provide a sense of justice on the victim's
          behalf. It pays for necessary medical care, living expenses if the
          injured victim is unable to work and other forms of tangible relief
          for survivors.
        </p>
        <p>
          In cases where victims have suffered catastrophic injuries, for
          example, they may need round-the-clock nursing care. An amputee may
          need money for prosthetics. Someone who has suffered a brain injury or
          has been rendered paraplegic may need the money to buy a special
          wheelchair or for extensive rehabilitation. The cost of continuing
          treatment and care for catastrophic injuries can add up to several
          millions of dollars during a person's lifetime.
        </p>
        <p>
          In wrongful death cases, victims' families can seek a settlement that
          covers a wide range of losses including medical expenses, funeral
          costs, pain and suffering and loss of love and companionship. A
          successfully litigated auto products case can provide necessary relief
          to east the pain, suffering and financial devastation that often
          result from an accident.
        </p>
        <h2>How the Orange County Injury Attorneys of Bisnar Chase Can Help</h2>
        <p>
          It is evident that auto defect cases pose a formidable challenge to
          even the most experienced and resourceful law firms in the United
          States. This is because automakers use every strategy and tactic at
          their disposal to thwart the efforts of plaintiffs and plaintiffs'
          attorneys. But, at{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>
          , we are ready for the challenge. Our track record against the largest
          automakers in the United States and the world, speaks for itself. We
          are thankful for a civil justice system that levels the playing field.
          The courtroom is where the corporation and the common man can duel and
          the average Joe can win. This is one of the greatest triumphs of the
          American justice system. In principle, we all stand on equal footing.
        </p>
        <p>
          At <strong> Bisnar Chase</strong>, our{" "}
          <strong> Orange County </strong>
          <Link to="/auto-defects" target="new">
            Auto Defect Lawyers{" "}
          </Link>{" "}
          are ready to fight for your rights and hold wrongdoers accountable. If
          you have been injured or have lost a loved one as the result of an
          auto defect, please <strong> call us at (949) 203-3814</strong> for a
          free, comprehensive and confidential consultation.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
