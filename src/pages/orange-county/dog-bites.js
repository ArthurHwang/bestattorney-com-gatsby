// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite",
  sidebarLinks: {
    title: "Dog Bite Injury Information",
    links: [
      {
        linkName: "Dangerous Breeds",
        linkURL: "/dog-bites/dangerous-dog-breeds"
      },
      {
        linkName: "Factors Causing Canine Agression",
        linkURL: "/dog-bites/factors-causing-canine-aggression"
      },
      {
        linkName: "First Aid For Dog Bites",
        linkURL: "/dog-bites/first-aid-dog-bites"
      },
      {
        linkName: "Injury & Infections from Dog Bites",
        linkURL: "/dog-bites/injury-infections-dog-bites"
      },
      {
        linkName: "Dog Bites",
        linkURL: "/orange-county/dog-bites"
      },
      {
        linkName: "Dog Bite Prevention",
        linkURL: "/dog-bites/prevention-tips"
      },
      {
        linkName: "Dog Bite Statistics",
        linkURL: "/dog-bites/statistics"
      },
      {
        linkName: "Victim's Rights",
        linkURL: "/dog-bites/victim-rights"
      },
      {
        linkName: "Why Dogs Bite",
        linkURL: "/dog-bites/why-dogs-bite"
      },
      {
        linkName: "California Dog Bite Laws",
        linkURL: "/dog-bites/california-dog-bite-laws"
      },
      {
        linkName: "Criminal Penalties",
        linkURL: "/dog-bites/criminal-penalties"
      },
      {
        linkName: "Insurance Breed Bans",
        linkURL: "/dog-bites/insurance-ban-breeds"
      },
      {
        linkName: "Orange County Home",
        linkURL: "/orange-county"
      }
    ]
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "dog-bites/orange_county_dog_bite_attorneys_banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Dog Bite Attorney - Dog Attack Injury Lawyer"
        pageDescription="Call 949-203-3814 for top rated Orange County dog bite lawyers specializing in serious injuries from California dog attacks. Bisnar Chase is a trusted injury law firm for over 40 years with a 96% success rate while winning hundreds of millions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Dog Bite Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Dog Bite Lawyers"
            title="Orange County Dog Bite Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Have you been the victim of a serious{" "}
          <strong> dog bite in Orange County?</strong> Our law firm has over
          four decades in personal injury and our lawyers have won hundreds of
          millions for our clients throughout Orange County California.
        </p>
        <p>
          If you are seeking compensation for a dog bite injury be sure to
          consult a qualified Orange County Injury Attorney before you talk to
          the at-fault party or insurance companies. We can help determine how
          much compensation you are entitled to including time lost on the job,
          medical bills, and emotional damages.{" "}
          <strong>
            For immediate assistance, call us at 949-203-3814 for a free,
            no-hassle, confidential consultation with an experience Orange
            County <Link to="/dog-bites">Dog Bite Lawyer</Link>.
          </strong>
        </p>
        <h2>Some of Our Dog Bite Case Results</h2>
        <p>
          Bisnar Chase represents a variety of personal injury clients. From
          serious dog bite cases to catastrophic injuries due to a defective
          product; our attorneys have a vast knowledge of California injury laws
          and will fight to the end for our client's right to fair compensation.
        </p>
        <ul>
          <li>$1,478,819.00</li>
          <li>$1,050,000.00</li>
          <li>$900,000.00</li>
          <li>$600,000.00</li>
        </ul>

        <h2>Why Dogs Attack</h2>
        <p>
          Here in Orange County, on average{" "}
          <strong> 3 to 5 dog bites are legally reported every day</strong>, not
          to mention all of the incidents that are
          <Link to="http://www.ocpetinfo.com/services/bite" target="_blank">
            {" "}
            not reported
          </Link>
          . While some believe only specific types of dog breeds are the{" "}
          <Link to="/dog-bites/dangerous-dog-breeds">dangerous biters</Link>,
          every dog is a risk, even with no prior history of
          <Link to="/dog-bites/factors-causing-canine-aggression">
            aggressive behavior
          </Link>
          . The majority of cases
          <strong> Orange County Dog Bite Attorneys </strong>deal with are
          involving dogs that have never shown any aggression prior to the bite
          incident.
        </p>
        <p>
          Some key factors to consider before stereotyping breeds - reasons for
          dog attack situations can result from any of the following:
        </p>
        <ul>
          <li>Reaction to a stressful situation</li>
          <li>Scared or threatened</li>
          <li>Protecting themselves, their puppies or their owner</li>
          <li>Startled or not feeling well</li>
          <li>Over-excitement, especially during rough play</li>
        </ul>
        <LazyLoad>
          <img
            src="/images/text-header-images/beware-of-dog-orange-county-dog-attacks-accidents-attorneys-lawyers.jpg"
            width="100%"
            alt="orange county dog bite attorney"
          />
        </LazyLoad>

        <h2>Dog Bite Fatalities</h2>
        <p>
          Out of a total 78 million dogs throughout the nation, 4.5 million dog
          bites were recorded last year, with 41 U.S. fatalities.
        </p>
        <p>
          All breeds have the capability to bite or attack at random or in
          result of a specific situation. Statistic information reports these
          breeds to be accountable for last years annual report:
        </p>
        <ul>
          <li>
            <strong> Pit bulls</strong> contributed to{" "}
            <strong> 71 percent of these deaths</strong>, with{" "}
            <strong> 22 of the 41 deaths recorded</strong>
          </li>
          <li>
            <strong> Labradors</strong> made up{" "}
            <strong> 3 of the 41 deaths</strong>
          </li>
          <li>
            <strong> Rottweilers</strong>, <strong> American Bulldogs</strong>,{" "}
            <strong> Belgian Malinois</strong>,{" "}
            <strong> Doberman Pinschers</strong>,
            <strong> German Shepards</strong> and <strong> mixed breeds</strong>{" "}
            <strong> each</strong> contributed to 2 deaths{" "}
            <strong> per breed</strong>
          </li>
        </ul>
        <p>These fatalities included:</p>
        <ul>
          <li>31 percent were infants ages 3-6 days old</li>
          <li>
            42 percent or 13 of the deaths were children 9 years and younger
          </li>
          <li>58 percent or 18 of the deaths were adults ages 30 and older</li>
        </ul>
        <h2>Danger for Postal Carriers in California</h2>
        <p>
          Southern California is a hotbed for dog attacks on postal carriers.
          Los Angeles to San Diego ranks top 4 in the entire nation for reported{" "}
          <Link
            to="/blog/los-angeles-leads-nation-in-highest-number-of-postal-worker-dog-bites"
            target="new"
          >
            postal carrier dog bites
          </Link>
          .
        </p>
        <p>
          &ldquo;Dogs are protective in nature and may view our letter carriers
          Houston, where postal employees suffered 77 attacks, more than any
          other city. Fifty-one cities make up the top 30 rankings. See the
          national statistic report for postal carrier-related dog bites{" "}
          <Link
            to="https://about.usps.com/news/national-releases/2016/pr16_039.htm"
            target="new"
          >
            here
          </Link>
          .
        </p>
        <p>
          If you are a postal carrier or delivery serviceman and have been
          injured due to a dog attack, contact our{" "}
          <strong> Orange County Dog Bite Lawyers </strong>at
          <strong> 949-203-3814 </strong>for immediate assistance and a free
          consultation.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/6wAqpIBOw6A"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What You Need to Show to Have a Solid Legal Dog Bite Case</h2>
        <ul>
          <li>The defendant owned the dog</li>
          <li>The dog bit the plaintiff</li>
          <li>
            The plaintiff was <strong> lawfully</strong> on the premises where
            the bite or attack took place
          </li>
          <li>The dog bite caused the plaintiff injuries</li>
        </ul>
        <p>
          Once these prerequisites are met, further action can be taken. A
          skilled dog bite lawyer can put all the facts of the case together to
          attain fair compensation for you. We have had dog bite cases where the
          at-fault party (or their insurance company) offered a few thousand
          dollars but through our pre-litigation negotiations we were able to{" "}
          <Link to="/case-results">collect much more for our clients</Link>.
        </p>
        <h2>Preserving Evidence to Support Your California Dog Bite Claim</h2>
        <p>
          The most important aspect of prosecuting a California dog attack case
          is to secure evidence as quickly as possible. We have our investigator
          on the scene, usually within an hour of notification, in order to
          gather and preserve evidence. The best evidence to gather is to obtain
          the identity of the dog and its owner, photographs of the dog,
          identity of witnesses, witness statements, and photographs of the
          injuries and location of the attack.
        </p>
        <h2>How Much is an Orange County Dog Bite Claim Worth?</h2>
        <p>
          Orange County Dog Bite Lawyer John Bisnar: John has decades of
          experience prosecuting dog bite cases for the no-fault party. John
          understands the ins and outs of the Orange County Courts and local
          defense teams.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/LCaC1XWmRGU"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>California Ranks Top for Canine Attacks</h2>
        <p>
          It may surprise you that California is ranked first in the nation in
          fatal dog attacks. Insurers nationwide pay out over $1 billion in
          claims related to animal bites annually. One-third of the money paid
          out through homeowners' liability claims are related to dog attacks.
        </p>
        <p>
          At Bisnar Chase, our <em>Orange County dog bite lawyers</em> handle
          numerous California dog bite cases each year. We can use our knowledge
          and experience to represent you and achieve great results. In our
          experience, the most dangerous month for dog attacks is August. The
          days are long, it's been hot for months, the gardeners are coming and
          going and children are generally home from school; a dangerous
          combination.
        </p>
        <h2>
          Contact an Orange County Injury Lawyer Who Specializes in Dog Attacks
        </h2>
        <p>
          If you or a loved one has suffered from a dog bite or animal attack,
          our team of highly skilled{" "}
          <strong> Orange County Dog Bite Attorneys </strong>at
          <strong> Bisnar Chase</strong> for a{" "}
          <strong> Free Consultation</strong>. We will use our experience,
          knowledge and resources to achieve the best possible results for you
          and your family. Hire an attorney with a long history of pursuing dog
          attack cases in <strong> California</strong>. You don't pay anything
          until we win your case.
        </p>
        <p>
          For immediate assistance, <strong> call us at 949-203-3814</strong>{" "}
          for a <strong> Free</strong>, <strong> No-Hassle</strong>,{" "}
          <strong> Confidential Consultation</strong> with an{" "}
          <strong> Experienced Lawyer</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
