// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/interview-line-up-orange-county-employment-lawyers-banner (1).jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Employment Lawyers - Bisnar Chase"
        pageDescription="There are a number of rights that all employees have under California and Federal Law. Contact our experienced Orange County employment attorneys if you were unlawfully terminated. Call 949-203-3814. Free Legal Consultation for employees."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Employment Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Employment lawyers"
            title="Orange County Employment lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Call our team of skilled{" "}
          <strong> Orange County Employment Lawyers</strong> at{" "}
          <b>949-203-3814</b> for immediate assistance and a{" "}
          <strong> Free Consultation</strong> to discuss your legal options.
        </p>
        <p>
          There are a number of rights that all employees have under{" "}
          <strong> California</strong> and <strong> Federal Law</strong>. When
          an employee's rights are violated, he or she would be well advised to
          discuss his or her legal options with an experienced{" "}
          <Link to="/orange-county">Orange County personal injury lawyer</Link>.{" "}
        </p>
        <p>
          In cases where a number of employees have been mistreated by the same
          employer, the workers can combine their claims into one class action
          lawsuit against the company.
        </p>

        <h2>What Types of Employment Cases Are There in Orange County?</h2>
        <ul>
          <li>Unlawful termination</li>
          <li>Unfair wage and hour</li>
          <li>Class action</li>
          <li>Harassment on the job</li>
          <li>Hostile work environments</li>
          <li>Fair severance packages</li>
        </ul>
        <p>
          We represent employees in Orange County. Our Newport Beach office is
          centrally located near you and we offer a free consultation and a{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer" target="new">
            no win, no fee guarantee
          </Link>
          .
          <strong>
            {" "}
            Our lawyers have recovered over $500 Million for our clients
          </strong>{" "}
          and we may be able to help you too.{" "}
          <strong> Call 949-203-3814</strong> for a free consultation with our
          highly skilled <strong> employment lawyer in Orange County.</strong>
        </p>
        <p>
          Like many states California is an <strong> "at-will"</strong> state,
          which means that an employer can fire an employee without
          repercussions. That does not, however, give them the supreme right to
          let go of an employee, especially if they fired a worker by violating
          state or federal labor codes.
        </p>
        <p>
          In cases where an employee's rights are violated, an experienced{" "}
          <Link
            to="/orange-county/wage-and-hour-disputes"
            title="Wage and Hour Disputes"
            target="new"
          >
            Orange County Wage and Hour Attorney
          </Link>{" "}
          can help determine the victims' legal options while protecting their
          rights and best interests. If you have an employment issue please call
          our legal team for a free consultation. An
          <strong> employment lawyer</strong> may be able to help you recover
          compensation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Orange County employment lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/unlawful-termination-orange-county-employment-attorneys.jpg')"
            }}
          >
            <h2>Examples of Unlawful Termination in Orange County</h2>
          </div>
        </LazyLoad>

        <p>
          Despite California being an at-will state, there are a number of acts
          that may be considered as "wrongful or unlawful termination." Under
          California law, an employer may not fire an employee for:
        </p>
        <ul>
          <li>Taking time off for jury duty (230.a)</li>
          <li>
            Being a victim of a crime or taking time off to appear in court in
            accordance with a subpoena (230.b)
          </li>
          <li>Taking time off to perform emergency duty (230.3.a)</li>
          <li>
            Taking off up to 40 hours each year, not exceeding eight hours in
            any calendar month of the year, to participate in activities of the
            school or licensed child day care facility of any of his or her
            children. (230.8.a)
          </li>
          <li>Disclosing the amount of his or her wages (232.c)</li>
          <li>
            Disclosing information regarding the employer's working conditions
            (232.5)
          </li>
          <li>
            Using, or attempting to exercise the right to use sick leave for a
            child, parent or spouse. (233.c)
          </li>
        </ul>
        <p>
          <strong>
            {" "}
            It is also against the law to fire someone because of their race,
            religion, sex, sexual orientation or age.{" "}
          </strong>
          Have our high skilled and successful team of
          <strong> Orange County Employment Lawyers </strong>represent you and
          your case.
        </p>
        <h2>
          Speak Up Against Wrongful Employment Taking Place in Orange County
        </h2>
        <p>
          Many workers and employees fail to report or speak up when the job
          becomes hazardous and caused them an injury or other harmful aspect in
          result of their duties. Usually, excuses consist of the following:
        </p>
        <ul>
          <li>Accepting pain as part of the job</li>
          <li>Not wanting to be labeled a &ldquo;complainer&rdquo;</li>
          <li>Believing home treatment would be sufficient</li>
          <li>Not being sure if the injury was work-related</li>
          <li>Fearing the loss of future or current jobs</li>
          <li>Not being able to afford time off without pay to see a doctor</li>
          <li>
            Not wanting to lose out on the safety incentive for no lost work
            time
          </li>
        </ul>
        <p>
          If you have been injured on the job you should report the situation to
          your supervisor, seek immediate medical attention and contact a
          skilled Orange County Employment Attorney. For immediate assistance
          call <strong> 949-203-3814</strong>.
        </p>

        <p>
          At Bisnar Chase, we strive to have the best work environment possible
          for our staff, lawyers, clients and our visitors and guests, because a
          healthy, positive and productive environment is what everyone
          deserves.
        </p>
        <p>
          <em>
            In the video below, founder, John Bisnar, describes our office
            environment and it's importance.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/414yNyALsPo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Additional Labor Codes Pertaining to CA Terminations</h2>
        <p>
          Employees who are in the process of being let go still have a number
          of rights. A few important labor codes to remember include:
        </p>
        <li>
          201. (a): "If an employer discharges an employee, the wages earned and
          unpaid at the time of discharge are due and payable immediately." It
          is important that recently discharged employees are paid promptly.
        </li>
        <li>
          201. (c): "When the state employer discharges an employee, the
          employee may, at least five workdays prior to his or her final day of
          employment, submit a written election to his or her appointing power
          authorizing the state employer to defer into the next calendar year
          payment of any or all of the employee's unused or accumulated
          vacation, annual leave, holiday leave, or time off to which the
          employee is entitled by reason of previous overtime work where
          compensating time off was given by the appointing power." Workers
          deserve to get the benefits they earned while working.
        </li>
        <li>
          208: "Every employee who is discharged shall be paid at the place of
          discharge, and every employee who quits shall be paid at the office or
          agency of the employer in the county where the employee has been
          performing labor."
        </li>
        <p>
          If you've lost your job, you have certain rights, such as the right to
          continue your health care coverage and, in some cases, the right to
          unemployment compensation.
        </p>
        <p>
          Visit the{" "}
          <Link to="https://www.dol.gov/general/topic/termination" target="new">
            United States Department of Labor
          </Link>{" "}
          to learn more info, and if you believe you are ready to take strong
          legal action, contact the highly skilled{" "}
          <strong> Orange County Employment Attorneys</strong> at Bisnar Chase.
        </p>
        <h2>Top Employment Lawyers Orange County</h2>
        <p>
          <Link
            to="/attorneys/jerusalem-beligan"
            title="Orange County Employment Lawyer, J. Beligan"
          >
            Jerusalem Beligan
          </Link>{" "}
          and{" "}
          <Link
            to="/attorneys/ian-silvers"
            title="Employment Attorney, Ian Silvers"
          >
            Ian Silvers
          </Link>{" "}
          are top rated employment lawyers in Orange County, CA. Both attorneys
          aggressively represent those affected by wage and hour claims,
          employee class actions and harassment in the workplace. Labor laws are
          very strict in California and you'll need a very experienced labor law
          legal team to fight for your rights and prove your case.
        </p>
        <h3>What Does an Employment Lawyer do</h3>
        <p>
          There are lawyers that represent both employees and employers. Bisnar
          Chase only represents the employees in any given case. Our job is to
          determine if you were treated fairly and in a considerate manner; one
          that does not violate your rights. California Labor laws can be
          complicated and an experienced employment law firm will have a great
          deal of experience in this specialized area. It is for this reason
          that you should always look for an attorney with this specialized area
          of training who understands employment law and regulations.{" "}
        </p>
        <p>
          The employment lawyer will begin ordering documents and records
          pertaining to your case to build a foundation to support your claims.
          It's a good idea to hire one that has trial experience. You don't want
          your case handed off to another law firm should a trial option arise.
        </p>
        <h3>When Should You Hire an Employment Attorney?</h3>
        <p>
          If you think you've been discriminated against, had your rights
          violated or know that your employer violated labor laws. You should
          immediately begin protecting yourself and keep a journal of every
          action that took place that leads you to believe there has been a
          violation of your rights in California when it comes to labor and
          employment. Seek out a qualified labor law attorney and make sure they
          are in good standing with the{" "}
          <Link to="http://www.calbar.ca.gov/" target="_blank">
            {" "}
            California Bar
          </Link>
          .{" "}
          <strong>
            {" "}
            Bisnar Chase does NOT charge a fee to evaluate your case and we are
            only paid if you win.
          </strong>
        </p>
        <h3>What Should I bring to My Appointment with the Law Firm?</h3>
        <p>
          Bring in anything you feel may have an impact on your case. This can
          include;
        </p>
        <ul>
          <li>Witness statements (other employees, supervisors etc)</li>
          <li>Employer communications (email, written, phone recordings)</li>
          <li>Employee to employee communications</li>
          <li>
            Written statements that lay out the facts of your case or why you
            are taking action
          </li>
          <li>All performance reports, job descriptions</li>
          <li>
            Pay stubs, reimbursement invoices, employee handbooks, written
            policies{" "}
          </li>
        </ul>
        <h2>
          Do You Believe Your Orange County Employment Rights Were Violated?
        </h2>
        <p>
          If you have recently lost your job and you believe your rights as an
          employee were violated, it may be in your best interest to take
          extensive notes regarding the reasons you were told you were let go.
        </p>
        <p>
          Keep any information you were given at the time of your termination
          and write out any concerns you had about your employment. Then,
          contact an experienced Orange County Employment Attorney who has a
          successful track record handling similar cases.
        </p>
        <p>
          Our knowledgeable employment law staff have successfully fought for
          the rights of employees for decades. We have a successful track record
          of standing up against large corporations on behalf of the "little
          guy" who has either been hurt by the corporation's negligence or has
          been mistreated.
        </p>
        <p>Remember, you do have rights as an employee.</p>
        <p>
          Contact our<strong> Orange County Employment Lawyers </strong>today
          for a{" "}
          <strong>
            {" "}
            Free, No-Obligation, Case Consultation. Call 949-203-3814.
          </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
