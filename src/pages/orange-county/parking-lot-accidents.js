// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-parking-lot-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Parking Lot Attorneys - Parking Lot Accident Lawyers"
        pageDescription="Experienced Orange County Parking Lot Accident Lawyers who have won over $500 Million for our clients. Call 323-238-4683 for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Parking Lot Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="orange county parking lot accident lawyers"
            title="orange county parking lot accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Orange County Parking Lot Accident Lawyers</strong> have over{" "}
          <strong> 39 Years of Experience</strong> and have won over{" "}
          <strong> $500 Million</strong> for our clients. Having established a{" "}
          <strong> 96% Success Rate</strong>, our team of skilled attorneys are
          ready to fight and win.
        </p>
        <p>
          The parking lot of a grocery store, beach, mall and anywhere else
          people park their cars can be hazardous and chaotic places.
        </p>
        <p>
          Filled with cars, trucks, motorcycles and people wanting to get in and
          out quickly can establish a zone for traumatic experiences and
          expensive damages.
        </p>
        <p>
          If you or a loved one has experienced a{" "}
          <Link to="/orange-county/car-accidents">car accident</Link>,
          fender-bender or personal injury in result of a
          <strong> parking lot accident</strong>, call our experience team of
          <strong>
            {" "}
            <Link to="/orange-county" target="new">
              Orange County Personal Injury Attorneys
            </Link>
          </strong>{" "}
          for a <strong> Free Consultation </strong>at
          <strong> 949-203-3814</strong>.
        </p>
        <h2>Top 6 Dangers in the Parking Lot</h2>
        <p>
          Parking lots can consist of small spaces, congested driving and
          parking areas and an increased risk factor for for many potentially
          dangerous situations.
        </p>
        <p>
          Here is a list of the{" "}
          <strong> Top 6 Dangers in the Parking Lot</strong>:
        </p>
        <ol reversed>
          <li>
            <strong> Property Damage</strong>
          </li>
          <li>
            <strong> Personal Injury</strong>
          </li>
          <li>
            <strong> Pedestrian Accidents</strong>
          </li>
          <li>
            <strong> Car Accidents</strong>
          </li>
          <li>
            <strong> Hit and Runs</strong>
          </li>
          <li>
            <strong> Wrongful Death</strong>
          </li>
        </ol>
        <p>
          Even when pedestrians and drivers are fully alert, serious
          circumstances can lead to deadly encounters in a matter of
          milliseconds.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="orange county parking lot accident statistics"
            style={{
              backgroundImage:
                "url('/images/text-header-images/parking-lot-accident-statistics.jpg')"
            }}
          >
            <h2>Parking Lot Accident Statistics</h2>
          </div>
        </LazyLoad>

        <p>
          With the low speeds and confined space of parking lots limiting the
          ability of reckless drivers, the congested confines and
          heavy-pedestrian foot traffic of the parking lot, it can be one of the
          most dangerous places to be, for drivers and pedestrians.
        </p>
        <p>
          Here are some scary statistics that will make you look twice, even
          three times before walking from your parking spot to the grocery store
          entrance.
        </p>
        <p>
          According to a{" "}
          <Link
            to="https://www.cbsnews.com/news/parking-lot-accidents-distracted-drivers-national-safety-council/"
            target="new"
          >
            report by CBS News
          </Link>
          :
        </p>
        <ul>
          <li>2 out of 3 drivers in the parking lot are distracted</li>
          <li>1 out of 5 of all car accidents take place in a parking lot</li>
        </ul>
        <p>
          Statistical information provided by{" "}
          <Link to="http://www.nsc.org/pages/home.aspx" target="new">
            The National Safety Council
          </Link>
          :
        </p>
        <ul>
          <li>60,000 injuries per year from parking lot accidents</li>
          <li>500+ deaths per year from parking lot accidents</li>
          <li>50,000+ crashes per year from parking lot accidents</li>
        </ul>
        <p>
          Even though slower speeds give drivers the false-sense of having the
          ability to multi-task safely, it is just as distracting at 5 mph as it
          is at 50 mph to multi-task.
        </p>
        <p>
          <strong>
            {" "}
            While driving through a parking lot, drivers feel comfortable:
          </strong>
        </p>
        <ul>
          <li>Making phone calls - 66%</li>
          <li>Texting - 56%</li>
          <li>Emailing - 50%</li>
          <li>Video Chatting - 42%</li>
          <li>
            Social media, games and other electronic distractions are also
            prevalent
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/AH5jkmpq_Jc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Top 4 Most Common Parking Lot Accidents</h2>
        <p>
          There are many different types of vehicles and types of drivers you
          may encounter in a parking lot or parking garage, each with their own
          strengths, weaknesses and distractions.
        </p>
        <p>
          Here is the <strong> Top 4 Most Common Parking Lot Accidents</strong>:
        </p>
        <ol>
          <li>
            <strong> Wrongful Deaths </strong>- Even though traffic speed in a
            parking lot is considerably lower than speeds on streets and
            highways, the size, weight and potential danger of a vehicle is
            still extreme at any speed. A slow impact or fast hit can result in
            a casualty or fatal injury and can be experienced while walking to
            and from your car, waiting for a parking spot in your car, and
            generally anywhere, especially when dealing with negligent and
            reckless drivers.
          </li>
          <li>
            <strong> Pedestrian Accidents</strong> - Whether a pedestrian feels
            entitles or is just unaware of their surroundings, vehicles,
            motorcycles and even bicycles and other forms of transportations
            like skateboard and rollerblades can be very dangerous. Pedestrians
            who are on their cell phones, electronic devices and those who are
            wearing headphones, possibly listening to music, talking on the
            phone or just muffling the load outside parking lot sounds, can be
            an increased danger to themselves, drivers and other innocent and
            vulnerable lives standing nearby.
          </li>
          <li>
            <strong> Hit-and-Runs</strong>: Hit and runs are a very common
            factor of pedestrian accidents, car accidents and wrongful death
            situations. Unfortunately, hit and runs are usually a reaction of a
            driver who does not want to deal with the situation, has prior
            history with the law and does not want it to impact him or her or is
            negligent and oblivious to the severity of the circumstances. If you
            witness a hit and run accident, try and get a license plate, color,
            year, make and model of the hit and run vehicle. If possible, photos
            and videos can be very helpful, especially when identifying the
            suspect. It is in no way indicated that Bisnar Chase is advising you
            to gather this content and information based on the potential danger
            you could be subjected to when doing so. If you happen to have any
            of this information throughout the process of witnessing a hit and
            run accident or any other crime, such information will be
            beneficial. If witnessing a ht and run, pedestrian, car accident or
            any other type of accident or crime, call 911, seek medical
            attention if you were harmed and do not move, touch or contaminate
            the crime scene in any way. Remember to stay alert, safe and take
            your own health and safety as a number one priority at all times.
          </li>
          <li>
            <strong> Fender-Benders</strong>: Fender-benders can be hazardous to
            drivers, passengers as well as by-standers. Flying debris, violent
            impacts, serious and fatal injuries are major possibilities. Being
            rear-ended, side impacts,{" "}
            <Link
              to="/auto-defects/sudden-unintended-acceleration"
              target="new"
            >
              sudden acceleration
            </Link>
            , head ons, brake, windsheild wiper and other{" "}
            <Link to="/orange-county/auto-defects" target="new">
              auto defects
            </Link>{" "}
            can cause fender-benders and car accidents, pedestrian accidents and
            can result in serious injury and death.
          </li>
        </ol>

        <h2>Parking Garage and Parking Structure Hazards</h2>
        <p>
          As if parking lots were not dangerous enough, parking garages, parking
          structures and other types of enclosed parking areas have an increased
          amount of hazards and dangers consisting of:
        </p>
        <ul>
          <li>Low-hanging ceilings</li>
          <li>Steel Cables</li>
          <li>Tight and narrow surface area</li>
          <li>Concrete beams</li>
          <li>Elevated levels</li>
          <li>Poles, posts and other metal structures</li>
        </ul>
        <p>Other hazards pose the increased risk of:</p>
        <ul>
          <li>Failure of structure or support beams</li>
          <li>Collapse; especially in areas prone to earthquakes</li>
          <li>Premise liabilities</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="orange county parking garage accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-orange-county.jpg')"
            }}
          >
            <h2>Experienced Legal Representation that Wins</h2>
          </div>
        </LazyLoad>

        <p>
          Our team of dedicated and skilled{" "}
          <strong> Orange County Parking Lot Accident Lawyers </strong>have
          established a <strong> 96% Success Rate </strong>and havewon over
          <strong> $500 Million </strong>for our clients. With over{" "}
          <strong> 39 Years of Experience</strong>, our lawyers, paralegals,
          legal advisors and staff have the resources, ability and know-how to
          take a complex case, and win.
        </p>
        <p>
          Another reason to call <strong> Bisnar Chase</strong>, if we don't win
          your case, <strong> You Don't Pay</strong>.
        </p>
        <p>
          Call and get a <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation</strong>, and received answers to the questions
          you have.
        </p>

        <center>
          <p>
            For immediate assistance <strong> Call 949-203-3814</strong>.
          </p>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
