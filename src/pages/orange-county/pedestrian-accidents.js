// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/pedestrians-in-the-streets-orange-county-accident-attorney-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Pedestrian Accident Attorney - Bisnar Chase"
        pageDescription="Call 949-203-3814 for a qualified Orange County pedestrian accident attorney who has won hundreds of millions for injured clients throughout California. The experienced injury Lawyers of Bisnar Chase have helped thousands and offer a free consultation. Call 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Pedestrian Accident Lawyer"
            title="Orange County Pedestrian Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The experienced <strong> Orange County</strong>{" "}
          <strong> Pedestrian Accident Lawyers at Bisnar Chase</strong> have
          helped injured victims in Orange County seek compensation for their
          injuries, damages and losses since 1978. We will fight for your rights
          and ensure that justice is served.
          <strong>
            {" "}
            Please call us at 949-203-3814 for a free, comprehensive and
            confidential consultation
          </strong>{" "}
          with our experienced legal team.
        </p>
        <h2>Recent Orange County Injury Verdicts</h2>
        <LazyLoad>
          <img
            className="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Orange County pedestrian accident attorney Brian Chase"
          />
        </LazyLoad>
        <p>
          The following is a list of the winnings our Orange County Pedestrian
          Accident Lawyers have obtained:
        </p>
        <ul className="bullet-inside">
          <li>$3,000,000.00 - Dangerous road design - Trolly vs. pedestrian</li>
          <li>$16,444,904.00 - Dangerous road condition, driver negligence</li>
          <li>$2,815,958.00 - Negligent road design</li>
        </ul>
        <p>
          <span>Brian Chase, Senior Partner:</span>
          “I went to law school knowing I wanted to be a personal injury
          attorney. I wanted my life’s work to have a positive impact on other
          people’s lives.”
        </p>
        <h2>Compensation for Orange County Pedestrian Injuries</h2>
        <p>
          Pedestrians often suffer serious injuries in collisions such as
          traumatic <Link to="/orange-county/brain-injury">brain injuries</Link>
          , spinal cord damage, broken bones and internal injuries. When a
          pedestrian accident is caused by someone else's negligence or
          wrongdoing, injured victims can seek compensation for damages
          including medical expenses, lost wages, hospitalization,
          rehabilitation and pain and suffering. Your injury lawyer should be
          familiar with laws and courts specific to California, especially
          Orange County if that is where the accident occurred.
        </p>
        <p>
          When there is a collision between a vehicle and a pedestrian, it is
          always the pedestrian who is injured. If you have been injured in a
          crash contact the
          <strong> Orange County Pedestrian Accident Lawyers </strong>of{" "}
          <Link to="/">Bisnar Chase</Link> today to obtain more information
          about pursuing your legal rights for compensation.
        </p>
        <h2>It's Not Always a Car that Endangers Orange County Pedestrians</h2>
        <p>
          You're taught at a young age to look both ways before crossing a
          street, to avoid being struck by a vehicle. But what about bicycles?
          Cyclists are sharing the road with vehicles, with the same rules as
          cars and motorcycles. They must stop at stop lights and stop signs,
          obey traffic laws and signs as well as the speed limit.
        </p>
        <p>
          The{" "}
          <Link to="http://www.cbs.com/" target="new">
            CBS
          </Link>{" "}
          Video below gives a perfect example of how dangerous bicycles can be
          to pedestrians.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/g0k9kcXk77U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Society is also becoming more and more aware of the dangers of
          distracted transportation, whether in a vehicle, motorcycle, bicycle
          or even walking or running. Usually, cell phones and other electronic
          devices are the main focus of distracted transportation and distracted
          driving.
        </p>
        <p>
          Texting, phone calls, video games, emails and social media are
          extremely distracting and can lead to potentially deadly situations,
          like checking your Twitter and walking in front of a bus. If you have
          been a victim of crash due to someone texting and driving call the{" "}
          <strong> Orange County Pedestrian Accident Lawyers</strong> of Bisnar
          Chase at <strong> 949-203-3814</strong>.
        </p>
        <h2>Pedestrian Accident Laws in California</h2>
        <p>
          <strong> Crosswalk laws:</strong> Under{" "}
          <Link to="/california-motor-vehicle-code/crosswalks-21950">
            California Vehicle Code Section 21950
          </Link>{" "}
          (a), motorists are required to yield the right of way to pedestrians
          who are walking in marked or unmarked crosswalks. The same section
          also states: "This section does not relieve a pedestrian from the duty
          of using due care for his or her safety. No pedestrian may suddenly
          leave a curb or other place of safety and walk or run into the path of
          a vehicle that is so close as to constitute an immediate hazard. No
          pedestrian may unnecessarily stop or delay traffic while in a marked
          or unmarked crosswalk."
        </p>
        <p>
          <strong> Motorists' responsibility:</strong> Even when pedestrians are
          not crossing in a crosswalk or are jaywalking, it does not excuse a
          driver from exercising due care.{" "}
          <Link to="/california-motor-vehicle-code/driving-near-pedestrians-outside-crosswalks-cmvc-21954">
            California Vehicle Code Section 21954
          </Link>{" "}
          states: "Every pedestrian upon a roadway at any point other than
          within a marked crosswalk or within an unmarked crosswalk at an
          intersection shall yield the right-of-way to all vehicles upon the
          roadway so near as to constitute an immediate hazard." The same
          section also states: "The provisions of this section shall not relieve
          the driver of a vehicle from the duty to exercise due care for the
          safety of any pedestrian upon a roadway."
        </p>
        <p>
          <strong> Right-of-Way:</strong> Drivers pulling out of public or
          private driveways must yield right-of-way to pedestrians and oncoming
          vehicles. According to California{" "}
          <Link to="/california-motor-vehicle-code/entering-highway-21804">
            Vehicle Code Section 21804
          </Link>{" "}
          (a): "The driver of any vehicle about to enter or cross a highway from
          any public or private property, or from an alley, shall yield the
          right-of-way to all traffic." Vehicles entering traffic "shall
          continue to yield the right-of-way to that traffic until he or she can
          proceed with reasonable safety."
        </p>
        <h2>Common Pedestrian Accident Causes in Orange County</h2>
        <p>
          The most common causes of pedestrian accidents in Orange County
          include:
        </p>
        <ul>
          <li>Driving under the influence</li>
          <li>Distracted driving</li>
          <li>Failure to yield the right-of-way</li>
          <li>Running a red light</li>
          <li>Inattention</li>
          <li>Dangerous roadway or intersection</li>
        </ul>
        <h2>Seeking A Qualified Orange County Pedestrian Accident Attorney</h2>
        <p>
          After an accident you'll have many questions. Who will pay your
          medical bills, who's liable for the damages, can you collect for pain
          and suffering, what are your time limits to file a claim? Our
          experienced <strong> Orange Pedestrian Accident Lawyer</strong> can
          answer those questions for you. If we accept your case, we will not
          charge you a fee or percentage until we win. To find out more about
          your rights <strong> call 949-203-3814</strong> for a{" "}
          <Link to="/contact">free consultation</Link>.
        </p>{" "}
        <div className="mini-header-image">
          <LazyLoad>
            <img
              alt="Orange County Pedestrian Accident Lawyer"
              src="../images/text-header-images/pedestrians-in-the-streets-orange-county-accident-attorney-lawyer-banner.jpg"
            />
          </LazyLoad>
        </div>
        <p>
          The experienced <strong> Orange County</strong>{" "}
          <strong> Pedestrian Accident Lawyers at Bisnar Chase</strong> have
          helped injured victims in Orange County seek compensation for their
          injuries, damages and losses since 1978. We will fight for your rights
          and ensure that justice is served.
          <strong>
            {" "}
            Please call us at 949-203-3814 for a free, comprehensive and
            confidential consultation
          </strong>{" "}
          with our experienced legal team.
        </p>
        <h2>It's Not Always a Car that Endangers Orange County Pedestrians</h2>
        <p>
          You're taught at a young age to look both ways before crossing a
          street, to avoid being struck by a vehicle. But what about bicycles?
          Cyclists are sharing the road with vehicles, with the same rules as
          cars and motorcycles. They must stop at stop lights and stop signs,
          obey traffic laws and signs as well as the speed limit.
        </p>
        <p>
          The{" "}
          <Link to="http://www.cbs.com/" target="new">
            CBS
          </Link>{" "}
          Video below gives a perfect example of how dangerous bicycles can be
          to pedestrians.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/g0k9kcXk77U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Society is also becoming more and more aware of the dangers of
          distracted transportation, whether in a vehicle, motorcycle, bicycle
          or even walking or running. Usually, cell phones and other electronic
          devices are the main focus of distracted transportation and distracted
          driving.
        </p>
        <p>
          Texting, phone calls, video games, emails and social media are
          extremely distracting and can lead to potentially deadly situations,
          like checking your Twitter and walking in front of a bus. If you have
          been a victim of crash due to someone texting and driving call the{" "}
          <strong> Orange County Pedestrian Accident Lawyers</strong> of Bisnar
          Chase at <strong> 949-203-3814</strong>.
        </p>
        <h2>Pedestrian Accident Laws in California</h2>
        <p>
          <strong> Crosswalk laws:</strong> Under{" "}
          <Link to="/california-motor-vehicle-code/crosswalks-21950">
            California Vehicle Code Section 21950
          </Link>{" "}
          (a), motorists are required to yield the right of way to pedestrians
          who are walking in marked or unmarked crosswalks. The same section
          also states: "This section does not relieve a pedestrian from the duty
          of using due care for his or her safety. No pedestrian may suddenly
          leave a curb or other place of safety and walk or run into the path of
          a vehicle that is so close as to constitute an immediate hazard. No
          pedestrian may unnecessarily stop or delay traffic while in a marked
          or unmarked crosswalk."
        </p>
        <p>
          <strong> Motorists' responsibility:</strong> Even when pedestrians are
          not crossing in a crosswalk or are jaywalking, it does not excuse a
          driver from exercising due care.{" "}
          <Link to="/california-motor-vehicle-code/driving-near-pedestrians-outside-crosswalks-cmvc-21954">
            California Vehicle Code Section 21954
          </Link>{" "}
          states: "Every pedestrian upon a roadway at any point other than
          within a marked crosswalk or within an unmarked crosswalk at an
          intersection shall yield the right-of-way to all vehicles upon the
          roadway so near as to constitute an immediate hazard." The same
          section also states: "The provisions of this section shall not relieve
          the driver of a vehicle from the duty to exercise due care for the
          safety of any pedestrian upon a roadway."
        </p>
        <p>
          <strong> Right-of-Way:</strong> Drivers pulling out of public or
          private driveways must yield right-of-way to pedestrians and oncoming
          vehicles. According to California{" "}
          <Link to="/california-motor-vehicle-code/entering-highway-21804">
            Vehicle Code Section 21804
          </Link>{" "}
          (a): "The driver of any vehicle about to enter or cross a highway from
          any public or private property, or from an alley, shall yield the
          right-of-way to all traffic." Vehicles entering traffic "shall
          continue to yield the right-of-way to that traffic until he or she can
          proceed with reasonable safety."
        </p>
        <h2>Common Pedestrian Accident Causes in Orange County</h2>
        <p>
          The most common causes of pedestrian accidents in Orange County
          include:
        </p>
        <ul>
          <li>Driving under the influence</li>
          <li>Distracted driving</li>
          <li>Failure to yield the right-of-way</li>
          <li>Running a red light</li>
          <li>Inattention</li>
          <li>Dangerous roadway or intersection</li>
        </ul>
        <h2>Seeking A Qualified Orange County Pedestrian Accident Attorney</h2>
        <p>
          After an accident you'll have many questions. Who will pay your
          medical bills, who's liable for the damages, can you collect for pain
          and suffering, what are your time limits to file a claim? Our
          experienced <strong> Orange Pedestrian Accident Lawyer</strong> can
          answer those questions for you. If we accept your case, we will not
          charge you a fee or percentage until we win. To find out more about
          your rights <strong> call 949-203-3814</strong> for a{" "}
          <Link to="/contact">free consultation</Link>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
