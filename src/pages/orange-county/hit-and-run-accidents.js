// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/single-car-hit-and-run-accidents-attorneys-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Hit and Run Attorney - Bisnar Chase"
        pageDescription="You may be entitled to compensation. Contact our Orange County Hit and Run lawyers for a free consultation. Best client care since 1978 and hundreds of millions recovered for thousands of clients. Call 949-203-3814 "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Hit and Run Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Hit and Run Lawyers"
            title="Orange County Hit and Run Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          We represent people who've been injured in a hit and run accident in
          Orange County. Bisnar Chase has spent the last 39 years helping Orange
          County residents receive maximum compensation for their injuries. Our
          plaintiff hit and run attorneys are{" "}
          <Link to="/about-us/lawyer-reviews-ratings" target="new">
            top rated
          </Link>{" "}
          trial lawyers who've collected over
          <strong> $500 Million </strong> in verdicts and settlements.
        </p>
        <p>
          If you've been injured by a hit and run driver call our experienced{" "}
          <Link to="/orange-county" target="new">
            Orange County Personal Injury Attorneys
          </Link>{" "}
          today for a free consultation. <strong> Call 949-203-3814</strong>.
        </p>

        <h2>Hit and Runs are More Common Than You Think</h2>

        <p>
          Over <strong> 3.1 million people live in Orange County</strong> which
          is only 948sq mi. Many roadways do not have crosswalks, have poorly
          lit streets and a high amount of vehicles. Consider the following:
        </p>
        <ul>
          <li>1 in 5 of all pedestrian fatalities are hit-and-runs</li>
          <li>60 percent of hit and runs involve pedestrians</li>
          <li>
            Southern California has the highest percentages of hit and runs in
            the entire country
          </li>
          <li>
            Individuals convicted of felony hit and runs that injured someone
            face up to a year in prison, $10,000 fine and points on their DMV
            record
          </li>
        </ul>
        <p>
          To learn more about Orange County Hit and Run Statistics,{" "}
          <Link
            to="http://www.ots.ca.gov/Media_and_Research/Rankings/default.asp"
            target="new"
          >
            click here
          </Link>
          .
        </p>
        <h2>Why Do Hit and Runs Occur?</h2>
        <p>
          There are many reasons why people who run down a pedestrian or are
          involved in an accident flee from the scene. Reasons can range from
          plain fear of getting in trouble with the law, preexisting records, to
          just not caring or wanting to deal with the situation.
        </p>
        <p>
          A high number of hit and runs are alcohol or drug related, which sends
          a clear message to all pedestrians and safe drivers to be on the
          lookout at all times to avoid the following:
        </p>
        <ul>
          <li>Drunk drivers</li>
          <li>Distracted drivers</li>
          <li>Reckless driver</li>
          <li>
            <Link to="/orange-county/car-accidents">Car accidents</Link> that
            involve unsuspecting people
          </li>
          <li>Mechanical error</li>
          <li>Pedestrians or cyclist in unmarked areas or blindspots</li>
        </ul>
        <p>
          The experienced Hit and Run Attorneys at Bisnar Chase will make sure
          your case receives maximum compensation.
        </p>
        <p>
          <em>
            In the Video below, an act of road rage and hit and run situation is
            captured from a vehicles dashcam.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/Th6XR02YllI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>How Do You Collect on a Hit and Run in Orange County?</h2>
        <p>
          Just because the person left the scene doesn't mean they won't be
          caught. If the hit and run motorist is located, you may be able to
          file a claim against their insurance company for compensation. If they
          are not located, then you can file a claim with your own insurance
          company under your UM - uninsured motorist portion of the policy.
        </p>
        <p>
          You should take precautions not to discuss the accident with your
          insurance company before consulting an Orange County Hit and Run
          Lawyers so that you may preserve your rights. The personal injury
          settlement process should be guided by an experienced attorney who
          understands UM policies.
        </p>
        <h2>What is a UM Policy? (Uninsured Motorist)</h2>
        <p>
          {" "}
          <Link
            to="https://www.esurance.com/info/car/uninsured-and-underinsured-coverage"
            target="_blank"
          >
            {" "}
            Uninsured or underinsured coverage
          </Link>{" "}
          protects you when the other driver fails to have adequate coverage. In
          the case of a <strong> hit and run</strong>, whether as a pedestrian
          or in another vehicle, your UM coverage of your policy can cover your
          medical costs and related expenses. Most people carry a UM clause in
          their policy but to be sure you can review your policy rider for full
          details. It's crucial to make sure your own insurance company gives
          you a fair settlement.
        </p>
        <p>
          Remember, the insurance companies make money when they pay out less
          claims so even if you are their customer they'll want to avoid paying
          the claim if possible. An
          <strong> Orange County Hit and Run Attorney</strong>
          <strong> </strong>can help you with this. Bisnar Chase has been
          representing hit and run victims -- both in vehicle and pedestrian
          accidents for over three decades.
        </p>
        <h2>Who Will Decide My Case?</h2>
        <p>
          UM benefits are typically handled outside of a courtroom. A third
          party arbitrator will decide the case and there are no appeals,
          generally speaking. You, your insurance company and the arbitrator
          will come to an agreement. You still reserve the right to sue the
          individual in court for your injuries (if they are located), but if
          they have no assets or insurance it's typically not a good idea.
        </p>

        <h2>Contact an Experienced Orange County Hit and Run Attorney</h2>
        <p>
          We offer a no obligation consultation regarding your accident. You can
          discuss your options with one of our Orange County Hit and Run Lawyer
          free of cost to find out your options. If we decide to take your case
          you will not pay unless we win. We also advance all costs during the
          case. <strong> Call 949-203-3814</strong> for{" "}
          <strong> Free Information</strong> about an
          <strong> Orange County Hit and Run Injury</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
