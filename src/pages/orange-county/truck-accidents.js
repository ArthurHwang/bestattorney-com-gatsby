// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/semi-truck-flipped-over-orange-county-truck-accident.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Truck Accident Lawyer - Bisnar Chase"
        pageDescription="Call the Orange County Truck Accident Attorneys today for a free consultation. Top rated injury lawyers with major wins throughout California and a 96% success rate. You may be entitled to compensation for a commercial truck accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Truck Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County truck accident lawyers"
            title="Orange County truck accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p align="left">
          Accidents involving large trucks, 18-wheelers, big rigs and semi
          trucks can be extremely dangerous, very damaging and result in serious
          injury or death. If you or someone you know has been involved in an
          accident with a large truck and has experienced costly damage, serious
          injury or has lost a loved one,{" "}
          <strong> call for immediate help </strong>at
          <strong> 949-203-3814 </strong>to speak with an
          <strong> Orange County Truck Accident Lawyer </strong>for
          <strong> free.</strong> Our team of highly skilled lawyers will ensure
          you get the best options and maximum compensation for your truck
          accident case.
        </p>
        <h2>Semi-Truck Driver Negligence in Orange County</h2>
        <p>
          Since 1978, the law office of Bisnar Chase has been successfully
          resolving claims against big rig drivers and the companies that hire
          them. Semi-truck drivers and the companies they work for have a duty
          to operate their vehicles in a safe and conscientious manner and
          failing to do so can be catastrophic. Truck driver negligence can
          consist of the following:
        </p>
        <ul>
          <li>Unsafe driving</li>
          <li>Loads which are too heavy</li>
          <li>Substandard equipment</li>
          <li>Unsafe tires</li>
          <li>Drug and alcohol use</li>
          <li>Sleep deprivation</li>
          <li>
            and other dangerous or negligent practices which endanger the lives
            of innocent motorists and pedestrians
          </li>
        </ul>
        <p align="center">
          <em>
            Truck accident attorneys John Bisnar and Brian Chase discuss truck
            injury issues including injured victims rights from truck driver
            negligence and auto defects in the trucking industry.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/mX8wMo8QTzo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>We Know the Facts</h2>

        <p>
          We've recovered over <strong> $500 Million</strong> for over{" "}
          <strong> 12,000 clients</strong>, many of them involving large trucks,
          semi-trucks, big rigs, commercial freightliners, etc.
          <strong> Bisnar Chase</strong> has over <strong> 40 years</strong> of
          experienced and has developed a distinguishing 96% success rate.
        </p>
        <p>
          Our <strong> Orange County Truck Accident Lawyers</strong> know what
          to search and investigate for when analyzing trucking accidents in
          California. We are familiar with the distinctive issues that arise,
          the federal regulations and commercial insurance issues that are
          involved, and the accident reconstruction problems that are exclusive
          to big-rig accidents.
        </p>
        <p>
          If you or someone you know has been involved in an accident with a
          large truck and has experienced costly damage, serious injury or has
          lost a loved one,{" "}
          <strong>
            {" "}
            call for immediate help at 949-203-3814 to speak with an Orange
            County{" "}
          </strong>
          truck accident attorney<strong> for free.</strong>
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Truck accident aftermath"
            style={{
              backgroundImage:
                "url('/images/text-header-images/orange-county-truck-accident-statistics.jpg')"
            }}
          >
            <h2>Truck Accident Statistics in Orange County</h2>
          </div>
        </LazyLoad>

        <ul>
          <li>
            Over $700 billion was made in trucking revenues this past year yet
            the average cost of truck crashes can range from $65,000 to hundreds
            of thousands. Accidents involving large commercial trucks are not
            only deadly, but can be very costly. They also cost many commuters
            many hours of their daily lives. According to the 2000 census,
            trucking accidents alone accounted for 98,000 hours of traffic
            nationwide. Most importantly, the truck accident epidemic in Orange
            County California and nation-wide has cost us deaths and seriously
            injured family and friends.
          </li>
          <li>
            A big semi-truck and fully-loaded trailer can weigh as much as
            80,000 pounds and be as much as 75 feet in length.
          </li>
          <li>
            Between 5,000 fatalities a year in the United States occur due to
            truck accidents.
          </li>
          <li>
            The vast difference in size and weight between trucks and passenger
            cars often spells disaster for motorists. In fact, it has been
            estimated that as many as 10% of all fatal accidents are caused by
            semi-trucks.
          </li>
          <li>
            In crashes involving a truck or tractor trailer and a passenger
            vehicle, the occupants of the passenger vehicle are 50 times more
            likely to be killed than the truck driver, or in other words, 98% of
            fatalities happen to people in the passenger vehicle.
          </li>
        </ul>
        <h2>Truck Accident Causes in Orange County</h2>
        <p>
          There are many different aspects to consider when handling a truck
          accident case, especially when the following are a factor:
        </p>
        <ul>
          <li>
            <strong> Long hours in service/ sleepiness</strong>
          </li>
          <li>
            <strong> Intoxicated or distracted Drivers</strong>
          </li>
          <li>
            <strong> Substandard Inspections</strong>
          </li>
          <li>
            <strong> Longer Combination Vehicles</strong>
          </li>
          <li>
            <strong> Hazardous Materials</strong>
          </li>
          <li>
            <strong> Driver error or bad judgment</strong>
          </li>
        </ul>
        <p>
          The enormous size and configuration of semi-trucks, when traveling on
          city streets, present serious risks to both pedestrians and motorists
          when the trailer swings widely and many times entraps pedestrians,
          smaller cars and bicyclists who are simply observing the right of way.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Orange County truck accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/road-debris-from-large-trucks-saving-money-and-not-lives.jpg')"
            }}
          >
            <h2>Saving Money and Not Lives</h2>
          </div>
        </LazyLoad>

        <p>
          The trucking industry has long put profit over safety, using
          under-qualified drivers or making them work unsafe amount of hours
          where they are sleep-deprived. Studies show how sleep-deprived driving
          can be even more dangerous than drinking and driving.
        </p>
        <p>
          Many times companies don't replace faulty or old equipment that their
          drivers use, putting everyone at risk for truck accidents. There are
          many reports of rocks, metal, sand and other debris that either fall
          off the back or get kicked up from large trucks and often times damage
          vehicles, injury drivers, or can even cause a catastrophic and fatal
          situation.
        </p>
        <p>
          Failure to provide trucks with the proper accessories like mud flaps,
          debris nets and other safety devices to avoid potential injury, damage
          and potentially death, are many times unreported and justified.
        </p>
        <p>
          If you have experienced property damage, personal injury or have lost
          a loved one due to <strong> road debris from large trucks</strong>,
          you could be entitled to generous compensation and should call our
          highly skilled <strong> Orange County Truck Accident Lawyers</strong>{" "}
          at <strong> 949-203-3814</strong>.
        </p>
        <h2>Experienced Legal Representation in Orange County</h2>
        <p>
          The truck accident attorneys at Bisnar Chase are passionate and
          experienced lawyers who have represented truck and other auto accident
          victims for over thirty nine years. We have a track-record of success
          including:
        </p>
        <ul>
          <li>
            $250,000 for a forklift injured a delivery truck driver on a loading
            dock.
          </li>
          <li>
            A confidential settlement for the driver of a freight-liner cement
            truck whose brakes failed. The truck careened out of control for
            over a quarter-mile and the driver was unfortunately permanently
            disabled. We were able to provide the victim with an extremely
            significant compensation.
          </li>
          <li>
            A U-Haul that caused two Ford Explorers to roll over, killing a
            child and injuring a child whose seatbelt failed. We fought for the
            victims and received a substantial confidential settlement for them.
          </li>
          <li>
            $200,000 for a woman who was injured when her car crashed into an
            illegally parked semi truck.
          </li>
          <li>$1,950,000 for a Truck Accident Case</li>
          <li>$3,476,250 for a Defective Truck Case</li>
        </ul>
        <p>
          If you need legal advice on a truck accident you were injured in call
          us today for a <strong> Free Consultation</strong>. Our highly skilled
          and experienced
          <strong> Orange County Truck Accident Lawyers</strong> are ready to
          discuss your case at no cost. You don't pay anything unless we win
          your case.
        </p>
        <p>
          For immediate assistance call <strong> 949-203-3814</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
