// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-swimming-pool-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Swimming Pool Accident Lawyers - Bisnar Chase"
        pageDescription="The Orange County swimming pool accident lawyers of Bisnar Chase excel in handling pool injuries and wrongful deaths. Contact us now for expert help with drownings, diving accidents, slip-and-falls and more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Swimming Pool Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County swimming pool accident lawyer"
            title="Orange County swimming pool accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Using a swimming pool is a great way of getting exercise and staying
          cool. Public and personal swimming pools are common across California
          – but so are pool accidents. In cases where negligence has played a
          part in a pool accident or injury, an experienced and highly-skilled{" "}
          <strong>Orange County swimming pool accident lawyer</strong> might be
          able to help.
        </p>

        <p>
          Tragic swimming pool deaths and injuries can occur in backyard pools,
          hotels, schools, country clubs, pater parks and more. If a pool has
          unsafe conditions or the swimmers are not properly supervised, the
          pool-goers will be at risk.
        </p>

        <p>
          Common pool incidents such as drownings, near-drownings, slips, and
          diving accidents can all carry serious and even life-threatening
          effects.
        </p>

        <p>
          When a person suffers an injury or death as a result of a pool
          incident, turning to a swimming pool accident lawyer in Orange County
          might be the best path to justice.
        </p>

        <h2>Getting the Pool Accident Support You Need</h2>
        <hr />
        <p>
          Swimming is not just a summer activity in Orange County. The
          year-round good weather that we enjoy makes it an activity enjoyed by
          countless people of all ages throughout the year.
        </p>

        <p>
          If your child has been injured in a swimming pool accident or
          near-drowning incident, either on another person's property or on
          someone else's watch, it is vital that you understand your legal
          rights and options.
        </p>

        <p>
          Contact Bisnar Chase now for expert help. We offer a free consultation
          with no obligations. Our personal injury attorneys will be able to
          assess your case and provide you with the guidance you need to make an
          informed decision on your next step.
        </p>

        <p>
          Call <Link to="tel:+1-949-203-3814">(949) 203-3814</Link> for
          immediate help from one of our swimming pool accident attorneys.
        </p>

        <h2>Types of Swimming Pool Accidents</h2>
        <hr />
        <p>
          <i>
            Swimming pools can be dangerous, and pool injuries can occur in a
            whole variety of ways. These include:
          </i>
        </p>

        <ul>
          <li>
            <b>Drownings –</b> Swimming pool drownings are unfortunately far too
            common. Drowning deaths occur when a person is unable to breathe
            because he or she is submerged in water. This can happen due to a
            wide range of reasons, including a victim being unable to swim, or
            being too tired to do so.
          </li>
          <li>
            <b>Near-drownings –</b> This refers to people who suffer the effects
            of drowning, but survive the experience. Victims of near-drownings
            can be left with permanent effects, such as catastrophic brain
            damage due to oxygen deprivation.
          </li>
          <li>
            <b>Slip-and-fall accidents –</b> Pool injuries can occur, even if
            you are not in the water. Pool water is easily splashed over the
            surrounding ground, making it wet and slippery. This can easily lead
            to a{" "}
            <Link to="/orange-county/slip-and-fall-accidents" target="new">
              slip accident
            </Link>
            .
          </li>
          <li>
            <b>Chemical issues –</b> Swimming pools have to use chemicals in the
            water to keep it clean and safe to swim in. However, they can also
            be dangerous. Pool water needs to contain the right amount of
            chemicals and be filtered properly. If not, injuries can occur.
          </li>
          <li>
            <b>Diving accidents –</b> Swimming pool diving accidents are a major
            cause of serious injuries. The key causes of these injuries are
            people diving into water that is not deep enough, and faulty
            equipment such as a diving board. Diving accidents can cause severe
            issues such as{" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              spinal cord injuries
            </Link>
            .
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Value of my personal injury claim"
            style={{
              backgroundImage:
                "url('/images/text-header-images/drowning-swimming-pool-accidents-calling-for-help-signs.jpg')"
            }}
          >
            <h2>The Signs of Drowning</h2>
          </div>
        </LazyLoad>

        <p>
          Drowning incidents are hard to prevent because the signs of drowning
          can be undetectable. Someone who is about to drown is not always loud
          and distressed. Sometimes, a victim may simply have his or her head
          low in the water.
        </p>

        <p>
          A person drowning may not look like it does on TV, when the victim
          flails and calls for help before going underwater. It may simply look
          like someone slowly sinking without much struggle. This is because a
          real drowning victim will be tired and gasping for breath.
        </p>

        <p>
          <i>Signs of drowning include:</i>
        </p>

        <ul>
          <li>Silence, with a look of panic on their face.</li>
          <li>Treading water with head tilted back.</li>
          <li>Scrambling to keep hold of a float, pool wall, or pool line.</li>
          <li>Floating face down.</li>
        </ul>

        <p>
          Other swimming pool drowning accidents occur when someone has tripped
          and fallen into a pool. This is why pool owners must keep walkways
          clear of debris.
        </p>

        <p>
          Individuals who strike their head on the ground before falling into a
          pool are in particular danger of drowning if they are not immediately
          pulled from the water. They may have lost consciousness and be unable
          to pull themselves up.
        </p>

        <p>
          <i>
            Anyone who sees someone drowning should try to help without putting
            themselves in danger. They should:
          </i>
        </p>

        <ul>
          <li>
            Throw a flotation device to the person or swim out to them if it is
            safe to do so.
          </li>
          <li>Call emergency services.</li>
          <li>Perform CPR.</li>
        </ul>

        <h2>Swimming Pool Accident Statistics</h2>
        <hr />

        <p>
          <i>
            These are some of the most alarming swimming pool statistics,
            including information from the{" "}
            <Link to="https://www.cdc.gov/" target="new">
              Centers for Disease Control and Prevention (CDC)
            </Link>
            , as well as other safety watchdogs:
          </i>
        </p>
        <ul>
          <li>
            There was an average of 3,533 fatal unintentional drowning incidents
            that were not boat-related every year between 2005 and 2009. That
            means that about 10 people drown in the United States every day.
          </li>

          <li>
            About one-fifth of the fatally-injured victims were children below
            the age of 15, and for every child who dies from drowning, another
            five require emergency care.
          </li>

          <li>
            Statistics indicate that more than 3,000 youngsters require
            emergency hospital care for drowning-related injuries every year.
          </li>

          <li>
            According to statistics compiled by the{" "}
            <Link to="http://www.ocfa.org/" target="new">
              Orange County Fire Authority
            </Link>
            , officials got 74 drowning calls in 2013. Of those calls, 35
            resulted in fatalities and many other cases, victims suffered
            irreversible{" "}
            <Link to="/head-injury/traumatic-brain-injury" target="new">
              brain damage
            </Link>
            .
          </li>

          <li>
            In 2018, California had the third-highest rate of children aged
            under 15 drowning in pools and spas (behind Florida and Texas).
          </li>

          <li>
            Pool drowning is the single most common cause of death for children
            under the age of 14 in California.
          </li>
        </ul>

        <h2>Who is Liable in Swimming Pool Accident Cases?</h2>
        <hr />

        <p>
          Who is responsible for a pool accident in California? It can vary from
          case to case, depending on the circumstances. When a pool incident
          leads to an injury or wrongful death, the blame can lie with a number
          of different parties.
        </p>

        <ul>
          <li>
            <b>
              <u>The owner or manager of a swimming pool.</u>
            </b>{" "}
            In many cases, pool accidents are a result of premises liability.
            This means that the person in charge of the pool is at fault,
            whether it is a personal, public, or government-run swimming pool.
            <p>
              At-fault parties could include a homeowner with a pool in their
              yard, as well as operators of public pool facilities, or owners of
              hotels, motels, gyms and more. However, an owner is not always at
              fault for an accident happening at their pool. Some factors which
              could contribute to a pool accident claim include:
            </p>
            <ul>
              <li>
                The owner may have failed to employ staff or lifeguards to
                supervise swimmers.
              </li>
              <li>Sub-par maintenance leading to dangerous pool conditions.</li>
              <li>
                A lack of warning signs – for instance, indicating pool depth.
              </li>
            </ul>
          </li>

          <li>
            <b>
              <u>Employees.</u>
            </b>{" "}
            Even if the pool owner did everything right, their employees might
            be negligent. It is their responsibility to ensure the safety of
            pool users. The negligent staff could include lifeguards who do not
            pay attention, or a school teacher supervising a swim class.
          </li>
          <li>
            <b>
              <u>Construction or maintenance teams.</u>
            </b>{" "}
            It is important that swimming pools are constructed properly and
            inspected according to safety guidelines. If not, crews could be
            held liable for resulting accidents.
          </li>
          <li>
            <b>
              <u>Manufacturers.</u>
            </b>{" "}
            Accidents can happen at swimming pools as a result of faulty parts.
            For instance, a poorly-constructed water slide or diving board could
            lead to injury – and a legal claim. If anyone is hurt in or around a
            pool, they should seek an experienced swimming pool accident lawyer
            in Orange County who can guide them in taking legal action.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/swimming-pool-accident-attorneys.jpg"
            width="100%"
            alt="A group of six children playing in a swimming pool."
          />
        </LazyLoad>

        <h2>
          What Should You Do If You Have Been Involved in a Pool Accident?
        </h2>
        <hr />
        <p>
          If you, or someone you know, has been hurt in a swimming pool accident
          in Orange County, there are a few steps you should take. These steps
          will boost your claim if you hire a swimming pool accident attorney to
          help you take legal action.
        </p>

        <ol>
          <li>
            <span>
              <b>Seek medical attention.</b>
              <p>
                Whether you have been injured in a pool slip-and-fall, or a
                near-drowning incident, make sure you get the appropriate
                medical attention.
              </p>
            </span>
          </li>

          <li>
            <span>
              <b>Document the accident.</b>
              <p>
                When it is safe to do so, you should form a clear picture of the
                events.
              </p>
            </span>
          </li>
          <ul>
            <li>
              Write a first-hand account of the accident and how it came about.
            </li>
            <li>
              Talk to witnesses, and take down their contact details. Ask them
              for statements on what happened if possible.
            </li>
            <li>
              Take pictures of the scene, including any potential causes of the
              accident.
            </li>
          </ul>

          <li>
            <span>
              <b>Preserve any evidence relating to the incident.</b>
              <p>
                If your pool injury was caused by a piece of faulty equipment,
                such as a dangerous diving board, make sure you keep it.
              </p>
            </span>
          </li>

          <li>
            <span>
              <b>Assemble and organize your evidence.</b>
              <p>
                This should include anything relating to your pool accident.
                Request a copy of your police report, if police were involved.
                Obtain copies of related medical paperwork, including:
              </p>
            </span>
          </li>
          <ul>
            <li>Medical records and treatments due to the pool incident.</li>
            <li>Proof of doctor's appointments and physio treatments.</li>
            <li>Proof of bills and expenses.</li>
            <li>
              You should also assemble your photos of the scene and your
              injuries, as well as your eye-witness statements, and proof of
              lost wages if you have had to miss work.
            </li>
          </ul>

          <li>
            <span>
              <b>Contact an Orange County swimming pool accident lawyer.</b>
              <p>
                If a family is grieving a pool drowning death, or a person is
                struggling with the injuries they sustained in a swimming pool,
                it can be tough to turn your mind to legal proceedings. But this
                is an important step. A high-quality swimming pool injury lawyer
                can provide the support you need during this difficult time.
                Make sure you only discuss your case with the opposing insurance
                firm when your attorney is with you.
              </p>
            </span>
          </li>
        </ol>
        <h2>Financial Support and Compensation for a Swimming Pool Injury</h2>
        <hr />
        <p>
          Swimming pool accidents can and do happen in Orange County. When they
          do occur, it is important that pool incident victims and their
          families are offered fair compensation for their injuries or losses.
        </p>
        <p>
          There is no set level of compensation for pool injury victims. Each
          case must be evaluated on its own merits to determine who was
          responsible, how the accident could have been prevented, and how much
          compensation the victim should get.
        </p>
        <p>
          It could include funeral costs in the case of a fatal drowning
          incident, or money to help with ongoing care if a victim has suffered
          permanent brain damage or disability.
        </p>
        <p>
          Swimming pool accident lawyers in Orange County will fight to make
          sure their clients are made whole.
        </p>
        <p>
          <i>Some of the other factors impacting compensation include:</i>
        </p>
        <ul>
          <li>How bad the injury was, and the victim's pain and suffering.</li>
          <li>Level of negligence causing the accident.</li>
          <li>Medical bills and expenses.</li>
          <li>Future treatment and care costs.</li>
          <li>Lost wages and earning potential.</li>
          <li>Wrongful death and loss.</li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/GDYVF2qtxqM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Swimming Pool Accident Prevention in Orange County</h2>
        <hr />
        <p>
          <i>
            There are steps that swimmers and pool owners can take to prevent
            swimming pool accidents. These include:
          </i>
        </p>
        <ul>
          <li>
            Taking swimming lessons. It is important for people of all ages to
            be able to swim in order to be in a swimming pool safely.
          </li>
          <li>
            Post signs indicating whether a lifeguard is on duty. This is a
            legal{" "}
            <Link to="http://ochealthinfo.com/eh/water/pool" target="new">
              requirement
            </Link>{" "}
            in Orange County.
          </li>
          <li>Have appropriate safety gear on hand, such as a life ring.</li>
          <li>
            Make sure pools are surrounded by fencing (in-keeping with the law).
          </li>
          <li>Have a lifeguard on duty at peak times.</li>
          <li>
            Property owners can install door alarms near home pools to prevent
            young children from falling in accidentally.
          </li>
        </ul>

        <h2>How Do I Find the Best Swimming Pool Injury Lawyer Near Me?</h2>
        <hr />
        <p>
          If you have lost a loved one or been injured in a swimming pool
          accident, involving negligence, contact Bisnar Chase. Our team of
          personal injury attorneys takes pride in offering superior
          representation.
        </p>
        <p>
          Our experienced <strong> swimming pool accident lawyers</strong> take
          on cases from Newport Beach, Santa Ana, across Orange County and even
          further afield.
        </p>
        <p>
          Bisnar Chase has earned its outstanding reputation over 40 years in
          business. Our firm has collected more than <b>$500 million</b> for our
          clients, with a <b>96% success rate</b>.
        </p>
        <p>
          Above all, our lawyers care. We are passionate about building trusting
          attorney-client relationships, and ensuring that our clients get the
          justice they deserve.
        </p>
        <p>
          Schedule a free consultation now. Call{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814</Link> or click to
          contact us.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
