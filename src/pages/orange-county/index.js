// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreenButton } from "../../components/utilities"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/orange-county-personal-injury-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Personal Injury Lawyers - Bisnar Chase"
        pageDescription="The Orange County personal injury lawyers of Bisnar Chase have been victims with superior representation for more than 40 years. If you have suffered an injury in Orange County, call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Orange County Personal Injury Attorneys"
            title="Orange County Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been injured, your best option may be to contact the
          experienced Orange County personal injury lawyers of Bisnar Chase for
          immediate assistance.
        </p>
        <p>
          Whether you were hurt by a car crash or a dog bite, you may be able to
          seek justice and compensation by filing a legal claim. In order to
          protect yourself and improve your claim, it is always best to speak to
          a personal injury attorney before discussing the accident with an
          at-fault party or insurance agent.
        </p>
        <p>
          Protecting your rights after an injury is vital. What you say to law
          enforcement and the insurance adjusters can highly impact your case.
          Our team can help protect those rights and fight for you through the
          legal process.
        </p>
        <p>
          Bisnar Chase has been helping Orange County residents for more than 40
          years. If you have been hurt and need help, contact our Orange County
          personal injury lawyers now. Go to our{" "}
          <Link to="/contact">contact page</Link> or{" "}
          <Link to="tel:+1-949-203-3814">call (949) 203-3814</Link> for a free
          consultation.
        </p>

        {/* <center className="mb">
          <div className="button_cont" align="center">
            <Link className="example_b" to="/contact" target="_blank">
              {" "}
              <DefaultGreenButton>Contact Bisnar Chase Now!</DefaultGreenButton>
            </Link>
          </div>
        </center> */}

        <h2>Types of Personal Injury Cases in Orange County</h2>

        <p>
          Our law firm takes on a wide variety of personal injury lawsuit cases.
          People can be hurt anywhere – from crossing the road to slipping on a
          wet store floor. We have been based in Orange County, California, for
          more than four decades, and have built up a huge level of expertise in
          that time.
        </p>
        <p>Here are some of our most common personal injury practice areas:</p>
        <ul>
          <li>
            Auto accidents – including:{" "}
            <Link
              to="/orange-county/car-accidents"
              target="new"
              title="Orange County Car Accident Lawyer"
            >
              car accidents
            </Link>
            ,{" "}
            <Link
              to="/orange-county/motorcycle-accidents"
              target="new"
              title="Orange County Motorcycle Accident Lawyer"
            >
              motorcycle crashes
            </Link>
            ,{" "}
            <Link
              to="/orange-county/bus-accidents"
              target="new"
              title="Orange County Bus Accident Lawyer"
            >
              bus incidents
            </Link>
            , and{" "}
            <Link
              to="/orange-county/truck-accidents"
              target="new"
              title="Orange County Truck Accident Lawyer"
            >
              truck accidents{" "}
            </Link>
          </li>
          <li>
            <Link
              to="/orange-county/auto-defects"
              target="new"
              title="Orange County Auto Defect Lawyer"
            >
              Auto defects{" "}
            </Link>
          </li>
          <li>
            <Link
              to="/orange-county/bicycle-accidents"
              target="new"
              title="Orange County Bicycle Accident Lawyer"
            >
              Bicycle accidents{" "}
            </Link>{" "}
            and{" "}
            <Link
              to="/orange-county/pedestrian-accidents"
              target="new"
              title="Orange County Pedestrian Accident Lawyer"
            >
              pedestrian injuries
            </Link>
          </li>
          <li>
            <Link
              to="/orange-county/hit-and-run-accidents"
              target="new"
              title="Orange County Hit-and-Run Lawyer"
            >
              Hit-and-run accidents{" "}
            </Link>
          </li>
          <li>
            <Link
              to="/orange-county/dog-bites"
              target="new"
              title="Orange County Dog Bite Lawyer"
            >
              Dog bites{" "}
            </Link>
          </li>
          <li>
            Premise liability – including{" "}
            <Link
              to="/orange-county/slip-and-fall-accidents"
              target="new"
              title="Orange County Slip-and-Fall Lawyer"
            >
              slip-and-fall accidents
            </Link>
          </li>
          <li>
            Defective products and{" "}
            <Link
              to="/orange-county/product-liability-lawyers"
              target="new"
              title="Orange County Product Liability Lawyer"
            >
              {" "}
              product liability
            </Link>{" "}
            injuries
          </li>
          <li>
            <Link
              to="/orange-county/swimming-pool-accidents"
              target="new"
              title="Orange County Swimming Pool Accident Lawyer"
            >
              Swimming pool injuries
            </Link>
          </li>
          <li>
            <Link
              to="/orange-county/nursing-home-abuse"
              target="new"
              title="Orange County Nursing Home Abuse Lawyer"
            >
              Nursing home abuse and neglect{" "}
            </Link>
            cases
          </li>
          <li>
            Catastrophic injury claims, including{" "}
            <Link
              to="/orange-county/brain-injury"
              target="new"
              title="Orange County Brain Injury Lawyer"
            >
              brain injuries
            </Link>
            ,
            <Link
              to="/catastrophic-injury/spinal-cord-injury"
              target="new"
              title="Spinal Cord Injury Lawyer"
            >
              spinal cord injuries
            </Link>
            , and paralysis
          </li>
          <li>
            <Link
              to="/orange-county/employment-lawyers"
              target="new"
              title="Orange County Employment Law Attorney"
            >
              Employment law{" "}
            </Link>{" "}
            – such as{" "}
            <Link
              to="/orange-county/wage-and-hour-disputes"
              target="new"
              title="Orange County Wage and Hour Dispute Lawyer"
            >
              wage and hour disputes
            </Link>
          </li>
        </ul>

        <h2>Seeking Justice – What is Fair Compensation?</h2>

        <p>
          Personal injury law covers a huge range of accident and injury types.
          The amount of money that might be awarded to a client in compensation
          will always depend on how they were hurt.
        </p>
        <p>
          Bisnar Chase is a plaintiff's personal injury law firm in Orange
          County which is passionate about the pursuit of justice. We strive to
          make sure people who have suffered harm – especially those with
          life-altering injuries due to acts of negligence and wrongdoing – get
          the help they need.
        </p>
        <p>
          These are some of the factors which affect the level of compensation a
          person might get:
        </p>
        <ul>
          <li>Pain and suffering</li>
          <li>The type of injury</li>
          <li>Medical bills</li>
          <li>Long-term effects of the injury</li>
          <li>Lost wages and impact on future employment</li>
          <li>Cost of ongoing care</li>
        </ul>

        <h2>What to do After Suffering a Personal Injury in Orange County</h2>

        <p>
          The most important step for a person who has been injured in Orange
          County is to get medical treatment and alert authorities.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/orange-county-personal-injury.jpg"
            width="100%"
            alt="Personal injury victim in a hospital bed being treated by a doctor"
            title="Lawyer for Injury Victim Orange County"
          />
        </LazyLoad>

        <p>
          Many people who are injured – whether they are in a car accident or
          hurt in a slip-and-fall incident – will downplay the severity of the
          harm they have suffered. This is not a smart or safe thing to do.
        </p>
        <p>
          Sometimes head injuries and traumatic brain damage can happen without
          the victim even realizing. It is absolutely vital to seek appropriate
          treatment after an accident, no matter how minor you think your
          injuries are.
        </p>
        <p>
          This has dual benefits. The most important is that it ensures the
          victim is safe and properly treated. But it also provides proof of
          injury if a legal claim is filed.
        </p>
        <p>
          Once a victim has been properly treated, they can gather evidence and
          contact an effective Orange County personal injury attorney to take on
          their case.
        </p>

        <h2>
          Do Not Speak to Insurance Agents Without Your Personal Injury Lawyer
        </h2>

        <p>
          When you are making a claim after suffering an injury, it is important
          that you do not discuss your case with an insurance agent without your
          Orange County personal injury lawyer present.
        </p>
        <p>
          Insurance adjusters will usually be pleasant and personable, but their
          job is to settle cases quickly and for the lowest amount possible.
          They will use any information you give them to do this.
        </p>
        <p>
          When possible, do not enter into discussions with insurance agents –
          either your own or those representing an at-fault party. Let your
          attorney handle the negotiations for you.{" "}
          <Link
            to="/resources/dealing-with-an-insurance-adjuster"
            target="new"
            title="Dealing With an Insurance Agent"
          >
            Click here{" "}
          </Link>{" "}
          for more information on dealing with insurance adjusters.
        </p>

        <h2>
          What Evidence is Needed to Win an Orange County Personal Injury Case?
        </h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/personal-injury-attorneys-in-orange-county.jpg"
            width="75%"
            className="imgcenter-fluid"
            alt="Car accident victim taking a picture of their damaged vehicle as evidence for a personal injury lawsuit"
            title="Best Personal Injury Lawyers Orange County"
          />
        </LazyLoad>

        <p>
          Winning your case is our ultimate goal. In order to do so, your Orange
          County personal injury attorney will build the strongest case
          possible. It is best to support your legal claim with as much relevant
          evidence as possible.
        </p>
        <p>
          One of the first things a personal injury victim should look to do
          once they are no longer in danger is to compile as much evidence as
          possible.
        </p>
        <p>These are some of the items which can prove most useful:</p>

        <ul>
          <li>
            A clear account of the incident, including the location, those
            involved, what happened, the time it occurred, etc.
          </li>
          <li>Medical records and documentation of your injuries.</li>
          <li>Witness statements and their contact information.</li>
          <li>
            Photos and or videos of the incident, injuries, those involved, etc.
          </li>
        </ul>
        <p>
          Bisnar Chase Orange County Personal Injury Lawyers and staff are here
          to win your case and offer compassionate support.
        </p>
        <p>
          We know that fighting a legal battle can be a tough process,
          especially if you have suffered an injury or lost a loved one. We will
          walk you through the process and help in any way we can.
        </p>

        <h2>How Much Does an Orange County Personal Injury Attorney Cost?</h2>

        <p>
          Many people are held back from filing a personal injury claim due to
          financial concerns. They believe they will have to pay a huge fee to
          an Orange County personal injury attorney to take their case on – and
          it is true that some high-priced lawyers are only interested in
          getting as much commission as possible.
        </p>
        <p>
          This does not apply to Bisnar Chase though. We believe in our skills
          and services being open to all victims, including those most in need.
          We offer a 'No Win, No Fee' guarantee, which ensures that we are only
          paid if we win your case.
        </p>
        <ul>
          <li>
            We have handled the cases of more than 12,000 accident victims under
            our no-fee guarantee.
          </li>
          <li>We advance all costs necessary to pursue your claim.</li>
          <li>
            We charge no upfront fees and it costs you nothing to get your case
            started.
          </li>
          <li>
            We also have a unique protection plan that will shield you from
            having to repay advance costs.
          </li>
          <li>If your case is not won, you don't pay.</li>
        </ul>
        <p>
          At Bisnar Chase, we really have you covered. If you or someone you
          love has been injured due to negligence or wrongdoing, call for a free
          no-strings-attached consultation to see if one of our Orange County
          personal injury attorneys can take your case.
        </p>

        <h2>
          Bisnar Chase: Passionate Personal Injury Lawyers Serving Orange County
        </h2>

        <p>
          Our law firm has the conviction, resources, and experience it takes to
          investigate to take on personal injury cases of all types for our
          clients.
        </p>
        <p>
          We understand the physical, emotional, and financial struggles that
          can face injury victims. The personal injury lawyers of Bisnar Chase
          excel when it comes to securing the best results possible for our
          clients. We know the Orange County courtrooms inside out and are ready
          to go to trial for our clients, or to negotiate with insurance
          companies and defense teams out of court.
        </p>
        <p>It is our achievements and track record that set us apart:</p>
        <ul>
          <li>
            Personal injury law firm founded in 1978, based in Orange County.
          </li>
          <li>President of the Orange County Trial Lawyers Assoc., 2004.</li>
          <li>Multiple "Attorney of the Year" awards.</li>
          <li>
            Highest possible "AV" and{" "}
            <Link
              to="https://www.avvo.com/attorneys/92660-ca-brian-chase-339392.html"
              target="new"
            >
              Avvo attorney ratings
            </Link>
            .
          </li>
          <li>
            "Best Places to Work in Orange County" awards in 2012 and 2013,
            2014, 2015, 2016, 2017 and 2018.
          </li>
          <li>
            A+ Rating with the{" "}
            <Link
              to="https://www.bbb.org/us/ca/newport-beach/profile/lawyers/bisnar-chase-personal-injury-attorneys-1126-100046710"
              target="new"
            >
              Better Business Bureau
            </Link>
            .
          </li>
        </ul>
        <p>
          Our attorneys have received top ratings from reputed professional
          associations such as Avvo and SuperLawyers. Our stellar track record
          and unique approach to client care has not only set us apart but has
          also earned us the respect of our peers.
        </p>
        <p align="center">
          Founding partner John Bisnar discusses what it is about our team of
          lawyers and staff that makes such a difference:
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/qKLuqDKoAKQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Bisnar Chase: Outstanding Case Results</h2>

        <p>
          Bisnar Chase has an outstanding legal team and is prepared to take on
          cases involving all kinds of personal injuries, from wounds caused by
          a vicious dog bite to catastrophic car crash injuries.
        </p>
        <p>
          We have had great success in winning compensation for our clients. We
          also often get referrals from other law firms due to our ability to
          handle complex cases requiring skill, resources, and experience.
        </p>
        <p>
          Some of our personal injury claim successes in Orange County include:
        </p>
        <ul>
          <li>$16,444,904 – Dangerous road condition, driver negligence</li>
          <li>$10,030,000 – Premises negligence</li>
          <li>$9,800,000 – Motor vehicle accident</li>
          <li>$8,500,000 – Motor vehicle accident – wrongful death</li>
          <li>$8,250,000 – Premises liability</li>
          <li>$7,998,073 – Product liability – motor vehicle accident</li>
          <li>$5,000,000 – Auto Defect</li>
          <li>$4,250,000 – Product Liability</li>
        </ul>

        <h2>Client-oriented Support for Personal Injury Victims</h2>

        <p>
          When it comes to personal injury lawyers in Orange County, not all are
          equal. Bisnar Chase is proud to be considered among the elite firms in
          Southern California.
        </p>
        <p>
          What sets us apart is our dedication to client-first service. Our
          priority is to provide first-class support to victims so that they can
          focus on recovery while we seek justice for them.
        </p>
        <p>
          It is important to us that we deliver outstanding results. But we also
          believe in providing superior representation with a personal touch.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/bjNAkh0v6QQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Legal Help Across Orange County</h2>

        <p>
          Bisnar Chase is proud to be based in Orange County. Whether you are a
          resident of Orange County, or your accident happened here, we can take
          your case. We specialize in the following areas:
        </p>
        <ul>
          <li>Anaheim</li>
          <li>Newport Beach</li>
          <li>Santa Ana</li>
          <li>Huntington Beach</li>
          <li>Irvine</li>
          <li>Laguna Hills</li>
          <li>Westminster</li>
        </ul>
        <p>
          Whether you need a personal injury attorney in Newport Beach or a
          lawyer who excels in Santa Ana cases, we can help.
        </p>

        <h2>
          How to Find the Best Orange County Personal Injury Lawyer Near Me
        </h2>

        <p>
          The Orange County personal injury lawyers of Bisnar Chase are
          compassionate and knowledgeable, with more than 40 years of trial
          experience.
        </p>
        <p>
          Our team of legal professionals is of the highest quality. We have
          developed an outstanding <b>96% success rate</b>, collecting more than{" "}
          <b>$500 million</b> for our clients in and around Orange County.
        </p>
        <p>
          We have the resources and skills to get the best settlements and
          verdicts for our clients. Whether your case is worth $5,000 in damages
          or $500,000, we treat everyone like they are our only client.
        </p>
        <p>
          If you are looking for the best personal injury lawyers in Orange
          County, look no further than Bisnar Chase. Contact us for a free
          consultation and let us work to get you the compensation you deserve.
        </p>
        <div className="contact-us-border">
          <p>
            <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> for
            immediate help, visit our law offices, or click to{" "}
            <Link to="/contact" target="new">
              contact us
            </Link>
            .
          </p>
        </div>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
