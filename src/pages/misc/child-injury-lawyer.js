// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/child-injury-lawyer-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Child Injury Lawyers- Orange County Child Accident Attorneys"
        pageDescription="If your child has suffered from a serious injury contact the million dollar child injury lawyers of Bisnar Chase at 949-203-3814. Negligent parties need to be held accountable. Call now."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Child Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="child injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Children who have suffered at the hands of negligent party deserve to
          be compensated for their devastating injuries. If a child has
          experienced serious physical or emotional harm, these ailments can
          lead to long-lasting disabilities. The{" "}
          <strong> California Child Injury Lawyers</strong> of Bisnar Chase are
          passionate about winning the finances your child needs to heal from a
          traumatic incident.
        </p>
        <p>
          Since 1978, Bisnar Chase's Personal Injury Law Firm has been dedicated
          to earning the maximum compensation our accident clients need to
          recover. We have obtained a{" "}
          <strong> 96% success rate at winning cases</strong> and show no signs
          of stopping.{" "}
        </p>
        <p>
          If your child has been wronged due to someone's carelessness contact
          the California Child Injury Attorneys of Bisnar Chase for a{" "}
          <strong> free consultation at 949-203-3814</strong>. You shouldn't
          have to pay out-of-pocket for your child's medical expenses.
        </p>
        <p>
          Contact the law group of Bisnar Chase and speak to a top-notch legal
          expert today.
        </p>
        <h2>California Child Injury Statistics</h2>
        <p>
          The{" "}
          <Link
            to="https://www.cdc.gov/safechild/child_injury_data.html"
            target="_blank"
          >
            {" "}
            Center for Disease Control and Prevention
          </Link>{" "}
          reported that 12,175 minors died from accidental injuries. Types of
          injuries that led to these deaths included vehicle collisions,
          pedestrian accidents and also bike crashes. The primary cause of
          deaths for children 1-4 was suffocation and drowning.{" "}
        </p>
        <p>
          In California alone, exactly 35,277 children and teenagers were
          admitted to the emergency room. Studies show that different age groups
          suffer from specific injuries. For example, the{" "}
          <Link
            to="https://www.ncbi.nlm.nih.gov/pubmed/11533363"
            target="_blank"
          >
            {" "}
            US National Library of Medicine National Institutes of Health
          </Link>{" "}
          stated that it is common for children ages 5-9 to suffer from injuries
          sustained on a playground. The main cause of injuries for adolescents
          was associated with car crashes.{" "}
        </p>
        <h2>9 Common Child Accident Injury Claims </h2>
        <p>
          A child suffering from a catastrophic injury can damage a family
          entirely. Many children experience great pain and suffering from the
          incident and may be affected for years to come. Many families are
          confused about whether or not their child has a case though. When you
          speak to a California child injury attorney they can offer you legal
          advice on your child's accident case.
        </p>
        <p>
          <strong> Common child injury claims are:</strong>
        </p>
        <ol>
          <li>
            <strong> Defective products:</strong> The{" "}
            <Link
              to="https://www.cpsc.gov/s3fs-public/Toy_Report_2018.pdf?qIO1DVoYhV6lzYgcLa04K28yF28BOgdS"
              target="_blank"
            >
              {" "}
              Consumer Product Safety Commission{" "}
            </Link>{" "}
            stated that there were 251,700 toy-related injuries that took place
            in one year. The types of injuries that were caused by these toys
            included fractures, sprains, cuts and contusions. Reports have also
            concluded that choking hazards are one of the most frequent
            incidents that take place amongst children ages four and younger.{" "}
            <Link
              to="https://www.parents.com/baby/injuries/choking/choking-hazards-and-your-baby/"
              target="_blank"
            >
              {" "}
              Compartment{" "}
            </Link>{" "}
            wrote that "
            <em>
              Any toy that is small enough to fit through a 1-1/4-inch circle or
              is smaller than 2-1/4 inches long is unsafe for children under 4
              years old.
            </em>
            " Children's products that are defective are not always associated
            with toys. At times, they are related to products that were intended
            to keep the child safe such as flotation devices.{" "}
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/car-accidents">Car accidents </Link>:
            </strong>{" "}
            The stature and the height of a child make them vulnerable to
            multiple catastrophic injuries in a vehicular collision. Data
            provided by the{" "}
            <Link
              to="https://www.iihs.org/iihs/topics/t/child-safety/fatalityfacts/child-safety"
              target="_blank"
            >
              {" "}
              Insurance Institute for Highway Safety{" "}
            </Link>{" "}
            concluded that 677 children lost their lives in car crashes. Taking
            precautions for{" "}
            <Link
              to="https://www.workingmother.com/keeping-your-kids-safe-important-car-safety-tips-for-your-kids"
              target="_blank"
            >
              {" "}
              keeping your kids{" "}
            </Link>{" "}
            safe in a motor vehicle is important. Experts say that parents
            should pay attention to the expirations of car seats, always put a
            seatbelt on your child when driving and never leave your child alone
            in a car especially in hot weather. Parents or guardians who lose a
            child due to their own negligence can be charged with manslaughter
            or a family member can file a{" "}
            <Link to="/wrongful-death">wrongful death claim </Link> against
            them.{" "}
          </li>
          <LazyLoad>
            <img
              src="../images/catastrophic-injuries/california-child-injury-lawyer-boy-on-bike.jpg"
              width="100%"
              alt="Little boy on bike in the street"
            />
          </LazyLoad>
          <li>
            <strong> Bicycle collisions: </strong>Stanford Children's Health
            reported that in one year 254,000 children were injured in
            bicycle-related accidents. Each year over 100 children are killed in
            bike collisions as well. There are multiple factors that can cause a
            child to be injured in a bicycle accident. Some of these causes
            include a car rear-ending a bicycle, drivers turning right and into
            the bike lane and also distracted driving. Small children who are
            still learning how to ride a bicycle should be supervised by a
            parent. Parents should also make sure that their children have the
            proper safety gear such as a helmet when riding a bicycle.{" "}
          </li>
          <li>
            <strong> Swimming pool accidents:</strong> According to the CPSC, in
            one year over 148 children drowned in swimming pools. California is
            one of the states with the most fatalities in the summer. Most of
            the victims who died due to swimming pool accidents were ages 1-3
            years old. It only takes a few minutes for a child to lose their
            life when drowning. At times it can take 20 seconds or less for a
            toddler to drown. Experts suggest that if your child is still
            learning how to swim putting a life vest won't suffice. Be in the
            pool with them in order to ensure that they are safe in the water.
            Parents can take their children to{" "}
            <Link
              to="https://www.babycenter.com/404_when-can-my-baby-take-swimming-lessons_1368527.bc"
              target="_blank"
            >
              {" "}
              swimming lessons{" "}
            </Link>{" "}
            as early as six months old.{" "}
          </li>
          <li>
            <strong> School bus collisions: </strong>Most school bus injuries
            that children experience are not when the bus is in route. Children
            are more likely to experience injuries by coming off and on a bus.
            Most children when getting off the bus do not use the rails to
            retain a steady balance. Since the stairs of a bus are steep then
            this can lead to a small child slipping and falling. Injuries that
            children can sustain when boarding the bus or coming off of a bus
            include sprains, broken bones or depending on the severity of the
            fall, brain damage.{" "}
          </li>
          <li>
            <strong> Playground injuries: </strong>The number one cause for
            playground injuries are falls. To prevent serious injuries such as
            head injuries, playgrounds are suggested to have surfaces that can
            cushion a fall. Ground surfaces like mulch, wood chips or sand make
            for a safer environment for a playground. There are instances when a
            city has been negligent or careless with playground equipment.
            Public parks should make sure that all playground equipment is
            well-maintained and safe to play on. For example, if a child falls
            off of a swing due to the chains being rusted and weak, the parents
            of the child can file an injury claim on their behalf.
          </li>
          <li>
            <strong> Medical Malpractice: </strong>Parents take their children
            to doctors trusting that their children will receive the best
            medical care and treatment. There are times that when a child is
            treated for an ailment though, that child's condition worsens.
            Pediatric malpractice involves a child suffering from injuries or
            losing their life due to the negligence of medical care
            professional. Medical care professionals do not only include doctors
            but also can include nurses. One of the most common types of medical
            malpractice claims are doctors misdiagnosing appendicitis in young
            girls for a urinary tract infection. If you believe that your child
            has been wronged due to a misdiagnoses speak to a child injury
            lawyer for information.{" "}
          </li>
          <li>
            <strong> Dog bites: </strong>Dogs can be very unpredictable at times
            especially if the dog has not been trained or socialized at a young
            age. Over 300,000 children are admitted to the emergency room with
            severe dog bite injuries. Children who suffer from animal attacks
            can be left with serious scarring, dismembered limbs and if the
            child is in infancy can even be killed by a dog. Two-year-olds are
            the biggest demographic to be attacked by dogs. Animals are less
            likely to be aggressive towards a child if they are accompanied by
            an adult. Do not leave a child unsupervised with a dog it is not
            familiar with.{" "}
          </li>
          <li>
            <strong> Poisoning accidents</strong>: Poisoning among children ages
            1-3 years old is very common. Children who are unsupervised can get
            into cabinets and can either ingest or spray themselves accidentally
            with a toxic chemical. Sometimes children are poisoned due to
            unknown substances being in the air. Common chemicals that children
            are usually poisoned by are pain-relieving medications, paint
            primers, cleaning products such as bleach and other household
            products such as pesticides or even essential oils. If you believe
            your child has been poisoned seek medical attention immediately.
            Symptoms that your child has been poisoned include vomiting,
            fainting or if they are expressing severe pain in their abdomen.{" "}
          </li>
        </ol>
        <h2>Child Injury Prevention </h2>
        <p>
          Although accidents do happen and a child sustaining an injury can be
          out of a person's control it does not mean that one should not take
          preventive measures to protect a child. Since children are not fully
          developed their injuries can be life-threatening and may follow them
          later down the road. Depending on the location and situation there are
          steps you can take to make sure your child is safeguarded.
        </p>
        <ul>
          <li>
            <strong> Do not leave your child unsupervised</strong>
          </li>
          <li>
            <strong> Safeguard an area (such as a pool) with a fence </strong>
          </li>
          <li>
            <strong>
              {" "}
              Make sure the location in which your child is playing in is safe
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Dangerous objects such as sharp objects and chemicals need to be
              locked away
            </strong>
          </li>
          <li>
            <strong> Have fire alarms installed in your home </strong>
          </li>
          <li>
            <strong>
              {" "}
              Use the correct car seat appropriate for your child's age
            </strong>
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Statue of lady justice infront of books"
            style={{
              backgroundImage:
                "url('/images/catastrophic-injuries/california-child-accident-attorneys-lady-justice.jpg')"
            }}
          >
            <h2>If Your Child Was Injured in California...</h2>
          </div>
        </LazyLoad>

        <p>
          Fighting for compensation for a minor differs from that of an adult.
          There are certain aspects that are unique to a California child injury
          case lawsuit. First off, a child can not file a claim for themselves.
          Parents or a guardian would have to pursue legal action on the behalf
          of a minor who is 18 years or younger. If the parent was involved in
          the incident along with the child then the parents would need to file
          a separate legal claim.{" "}
        </p>
        <p>
          The parent also has up to only two years to file a claim for their
          child. The sooner you hire a child injury lawyer though the sooner you
          can receive compensation for medical bills and the lost wages you
          experienced from taking time off of work to care for your child.{" "}
        </p>
        <p>
          There is not a set settlement amount that a child will receive after a
          harmful incident. Elements that will be factored into the winnings
          include whether the child's injuries will affect them later on in
          life, if the child will require continuous care because of these
          injuries and will the injuries eliminate their ability to financially
          support themselves in adulthood.{" "}
        </p>
        <h2>Contact the California Child Injury Lawyers of Bisnar Chase</h2>
        <p>
          For over 40 years, our <Link to="/">personal injury lawyers </Link>{" "}
          have been winning millions of dollars for child injury lawsuits. At
          the law firm of Bisnar Chase, we believe that irresponsible entities
          should compensate seriously injured children. Our child accident
          lawyers have sustained a 96% success rate at earning compensation for
          accident victims and we make sure our clients are aware of what is
          happening in their children's case every step of the way.
        </p>
        <p>
          <strong> Our Mission Statement: </strong>
          <em>
            "To provide superior client representation in a compassionate and
            professional manner while making our world a safer place."
          </em>
        </p>
        <p>
          Call our law offices today to get free legal advice on your child's
          injury claim. <strong> Contact us at 949-203-3814</strong>.
        </p>
        <p>
          Don't wait, seek justice and start fighting for what your child
          deserves today.{" "}
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St, #120 Newport
          Beach, CA 92660
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
