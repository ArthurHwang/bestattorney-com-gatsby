// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  extraSidebar: {
    title: "TCPA Attorneys",
    HTML: `
   <ul className='bpoint'>
      <li>Illegal Actions from Telemarketers include: </li>
      <li>Calling before 8am or after 9pm</li>
      <li>Not respecting the Do Not Call List</li>
      <li>Calling after a customer requests that they stop</li>
      <li>If you have received illegal phone calls from telemarketers, know your rights and <Link to='/contact'>contact an attorney </Link> today.</li>
  </ul>
    `
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "images/telephone-consumer-protection-act-lawyers-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Telephone Consumer Protection Act-TCPA Attorneys"
        pageDescription="Award-winning Telephone Consumer Protection Act Lawyers of Bisnar Chase. Call 800-561-4887 for Your Free Case Evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Telephone Consumer Protection Act Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California consumer protection act lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            If you have been harassed and verbally attacked by aggressive
            telemarketers and robocalls, you should call the highly skilled and
            experienced
            <strong> Telephone Consumer Protection Act Lawyers</strong> of{" "}
            <strong> Bisnar Chase</strong> for your{" "}
            <strong> Free Case Evaluation </strong>and{" "}
            <strong> Consultation</strong>.
          </p>
          <p>
            The trusted and skilled personal injury attorneys of{" "}
            <strong> Bisnar Chase</strong> will fight for you and never give up
            on difficult cases. To see if your situation qualifies, contact us
            and receive a <strong> Free Consultation. </strong>
          </p>
          <p>
            For immediate assistance <strong> Call 1-800-561-4887</strong>.
          </p>
        </div>
        <h2>What is the Telephone Consumer Protection Act?</h2>
        <p>
          The{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Telephone_Consumer_Protection_Act_of_1991"
            target="new"
          >
            TCPA (Telephone Consumer Protection Act of 1991)
          </Link>
          , restricts telephone solicitations (i.e., telemarketing) and the use
          of automated telephone equipment. The <strong> TCPA</strong> limits
          the use of automatic dialing systems, artificial or prerecorded voice
          messages, SMS text messages, and fax machines.{" "}
        </p>
        <p>
          Solicitation calls consist of telemarketers calling residence and
          business phone numbers using automated recording systems and live
          persons in real time on the phone. Not all telemarketing and
          soliciting companies abide by the rules, but are subtle about it, and
          many times, people don't even realize they have been victim of TCPA
          violations.
        </p>
        <h2>Types of Telephone Consumer Protection Act Violations</h2>
        <h3>1. Prohibited Acts</h3>
        <p>
          According to{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Telephone_Consumer_Protection_Act_of_1991"
            target="new"
          >
            Wiki
          </Link>
          , unless you have given the telemarketer prior consent, the
          <strong> Telephone Consumer Protection Act </strong> and{" "}
          <strong> Federal Communications Commission (FCC) </strong>rules under
          the <strong> TCPA </strong>generally prohibits or requires the
          following:
        </p>
        <ul>
          <li>
            Prohibits solicitation calls to residences before 8am or after 9pm
          </li>
          <li>
            Must keep company-specific "
            <strong>Do-Not-Call" list or DNC</strong>, and must be honored for 5
            years
          </li>
          <li>
            Must honor{" "}
            <strong>
              {" "}
              <Link
                to="https://en.wikipedia.org/wiki/National_Do_Not_Call_Registry"
                target="new"
              >
                National Do Not Call Registry{" "}
              </Link>
            </strong>
          </li>
          <li>
            Solicitors must provide their name, the name of the person or entity
            for who the call is being made for and a telephone number or address
            where the person or entity may be contacted
          </li>
          <li>
            Prohibits the use of artificial voices or recorded messages for
            solicitation calls to residences
          </li>
          <li>
            Prohibits any call made using automated telephone equipment or an
            artificial or prerecorded voice to an emergency line ("911"), a
            hospital emergency number, a physician's office, a hospital/health
            care facility/elderly room, a cellular telephone, or any service for
            which the recipient is charged for the call
          </li>
          <li>
            Prohibits auto dialed calls that engage two or more lines of a
            multi-line business.
          </li>
          <li>Prohibits unsolicited advertising faxes.</li>
          <li>
            In the event of a violation of the TCPA, a subscriber may (1) sue
            for up to $500 for each violation or recover actual monetary loss,
            whichever is greater, (2) seek an injunction, or (3) both
          </li>
          <li>
            In the event of a willful violation of the TCPA, a subscriber may
            sue for up to three time the damages, i.e. $1,500, for each
            violation
          </li>
        </ul>
        <h3>2. Top 15 Most Common Telemarketing Scams</h3>
        <p>
          If you've ever received a call that sounds similar to any of the
          listed types below, it's not uncommon.
        </p>
        <p>
          Here is a list of the{" "}
          <strong> Top 15 Most Common Telemarketing Scams:</strong>
        </p>
        <ol>
          <li>Counterfeit checks from phony lottery & sweepstakes companies</li>
          <li>Payment processor or &ldquo;money mule&rdquo; scam</li>
          <li>&ldquo;Grandma/grandpa, it's me!&rdquo;</li>
          <li>Guaranteed government grants</li>
          <li>Sweetheart scams (INTERNET and telephone)</li>
          <li>Advance fee loan scams</li>
          <li>Credit card or identity theft insurance</li>
          <li>Secret shopper scam</li>
          <li>Utility company cut-off scam</li>
          <li>Check processing & check overpayment scams</li>
          <li>Overseas money transfers (Nigerian scams)</li>
          <li>"Your distant relative has died in our country"</li>
          <li>Predatory mortgage lending</li>
          <li>Sound-alike charities and law enforcement groups</li>
          <li>
            Other alarming messages from the bank (phishing and vishing scams)
          </li>
        </ol>
        <p>
          To find a detailed summary for each of these tele-scams, visit{" "}
          <Link
            to="http://www.ncdoj.gov/Protect-Yourself/Stop-Telemarketers/Common-Telemarketing-Tricks.aspx"
            target="new"
          >
            ncdoj.gov
          </Link>
          .
        </p>
        <p>
          If you have been the victim of any of the scams listed above, contact
          the <strong> Telephone Consumer Protection Lawyers</strong>
          of Bisnar Chase. You could be entitled to compensation.
        </p>
        <h3>3. Large Corporations Breaking the Law Using Telemarketing</h3>
        <p>
          Over the last decade, companies have been consistently abusing the
          rules and laws intended to govern telemarketing boundaries, and the
          awareness is beginning to multiply throughout society.
        </p>
        <p>
          The following is a list of corporate giants that have allegedly broken
          and abused the TCPA, and have settled these claims instead of face
          hundreds of millions of dollars in exposure:
        </p>
        <ul>
          <li>
            Capital One for <strong> $75 Million</strong>
          </li>
          <li>
            JPMorgan Chase for <strong> $34 Million</strong>
          </li>
          <li>
            AT&T for <strong> $45 Million</strong>
          </li>
          <li>
            MetLife for <strong> $23 Million</strong>
          </li>
          <li>
            Bank of America for <strong> $32 Million</strong>
          </li>
          <li>
            Papa John's Pizza for <strong> $16 Million</strong>
          </li>
          <li>
            Walgreen's Pharmacy for <strong> $11 Million</strong>
          </li>
        </ul>
        <h2>Telemarketing is a Problem</h2>
        <p>
          According to the{" "}
          <Link
            to="https://www.sba.gov/managing-business/running-business/marketing/telemarketing-laws"
            target="new"
          >
            U.S. Small Business Administration
          </Link>
          ,<strong> Telemarketing</strong> is regulated at the federal level by
          two statutes: the Telephone Consumer Protection Act of 1991 (TCPA) and
          the <strong> Telemarketing</strong> Sales Rule (TSR).
        </p>
        <p>
          The Federal Communications Commission (FCC) derives its regulatory
          authority from TCPA, while the Federal Trade Commission (FTC) is
          responsible for enforcing TSR.
        </p>
        <p>
          The biggest problem is the enforcement of telemarketers and how they
          abide by the laws and guidelines set to ensure the safety and prohibit
          harassment of any kind.
        </p>
        <p>
          Our highly experienced{" "}
          <strong> Telephone Consumer Protection Act Lawyers </strong>can decide
          if you have a case over the phone during our <strong> Free</strong>
          <strong> and Confidential Consultation.</strong>
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/O13Vv4n__J8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>
          Overwhelming Statistics on Telephone Marketing Strategies and
          Techniques
        </h2>
        <p>
          Everyone is persistently provoked by bothersome telemarketers,
          especially around dinner time or when we are relaxing before bed, but
          the numbers of complaints and reports that are received every day is
          astounding.
        </p>
        <p>
          Many Lawyers will reject person after person who complain about being
          harassed and tormented by persistent telemarketers, scam artists and
          thieves, but in order to fight these types of cases you need an
          aggressive and highly skilled attorney, like the team at{" "}
          <strong> Bisnar Chase.</strong>
        </p>
        <ul>
          <li>
            <strong> TCPA</strong> complaints are the largest category of
            informal complaints received by the{" "}
            <Link to="https://www.fcc.gov/" target="new">
              FCC (Federal Communications Commission)
            </Link>
            , averaging over 10,000 complaints every month.
          </li>
          <li>
            {" "}
            <Link to="https://www.ftc.gov/" target="new">
              The Federal Trade Commission (FTC){" "}
            </Link>{" "}
            reports an average of about 63,000 complaints of illegal{" "}
            <Link
              to="https://www.consumer.ftc.gov/features/feature-0025-robocalls"
              target="new"
            >
              {" "}
              robocalls
            </Link>
            . By the end of the 4th quarter complaints can spike to over
            200,000.
          </li>
        </ul>

        <h2>How Do Call Centers Work?</h2>
        <p>
          Telemarketing centers, call centers, inside marketing or telesales is
          a method of direct marketing, where salespeople obtain leads or lists
          of potential customers and solicit products and or services, usually
          pushed by companies and businesses paying for the telemarketers
          services.
        </p>
        <p>
          Although this can often be a great channel of advertising and
          marketing, it can lead to very upset individuals, a bad reputation for
          the company and product or service, and can many times lead to a
          lawsuit, where the plaintiff is compensated.
        </p>
        <p>
          If you have experienced an aggressive, abusive or otherwise unsettling
          sales call, contact us at <strong> 800-561-4887</strong>, and receive
          a <strong> Complimentary Consultation </strong>today.
        </p>
        <h2>Know When You're Being Played</h2>
        <p>
          Often times when you receive a phone call from an unfamiliar number,
          you don't usually know what to expect.
        </p>
        <p>
          Sometimes it could be a serious family emergency, bill collectors, a
          wrong number, even a local carpet cleaning company trying to win your
          business.
        </p>
        <p>
          But when it sounds too good or too scary to be true, it usually is.
          Getting a call saying you've won a private lottery, or that you own
          $5,000 for an outstanding car payment or house mortgage are red flags.
        </p>
        <p>
          Many of these tricks and scams used by telephone marketers are usually
          aimed at vulnerable and susceptible elderly individuals.
        </p>
        <h2>Aggressive and Inappropriate Telemarketers</h2>
        <p>
          There is an unacceptable amount of telemarketers that are hostile and
          harassing. Many times the phone call can be unsettling, disturbing,
          threatening and violating. You should not have to endure endless phone
          calls of unpleasant and discomforting qualities.
        </p>
        <p>
          A telephone marketer's job is to sell products and services through
          the phone, and has a very high rejection rate. Over time,
          telemarketers can become frustrated by not making any sells and can
          become hostile. It can also be in result of an inexperienced
          telemarketer that has not learned how to deal with rejection, does not
          like or care for their job, or many other reasons.
        </p>
        <p>
          There is never an acceptable justification for telemarketers to be
          disrespectful, impolite, threatening or
          <span data-syllable="mis·chie·vous">
            mischievous, and if encountered should be reported and held
            accountable.
          </span>
        </p>
        <h2>4. Let Our Skilled and Assertive Attorneys Represent You</h2>
        <p>
          The distinguishing quality of our Telephone Consumer Protection Act
          Lawyers have a prestigious reputation in the courtroom and have been
          helping victims and their families for over
          <strong> 39 Years. Bisnar Chase </strong>has won over{" "}
          <strong> $500 Million </strong>for our deserving clients and have
          established a <strong> 96% Success Rate.</strong>
        </p>
        <p>
          If you have been the victim of an aggressive, hostile or inappropriate
          phone call associated to telemarketing, call{" "}
          <strong> 800-561-4887</strong> and receive a
          <strong> Free Case Evaluation</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
