// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "electrocution/california-electrocution-injury-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Electrocution Injury Lawyers – Bisnar Chase"
        pageDescription="The California electrocution injury lawyers of Bisnar Chase can help electric shock accident victims seek compensation and justice. An electrocution accident can cause severe injuries and fatalities. Call now for expert help from our electrocution injury attorneys - 96% success rate, $500 million won."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Electrocution Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Electrocution Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Coming into contact with an electrical current can be deadly to the
          human body. A shock can cause serious injuries – from burns to heart
          problems – and could even prove fatal. If you or a loved one have been
          harmed by an electric shock, it is time to contact the{" "}
          <strong> California electrocution injury lawyers</strong> of Bisnar
          Chase.
        </p>
        <p>
          The world relies on electricity. Every home and workplace is filled
          with appliances and electrical sockets. In most cases, these are
          perfectly safe. But accidents and malfunctions can happen, and there
          are plenty of examples of safety negligence resulting in severe
          electrocution injuries. This is where a skilled and experienced
          electric shock injury lawyer comes in.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">
            Call us now on (949) 203-3814
          </Link>{" "}
          for immediate help, or click the button below to arrange a free
          consultation with an expert California electrocution injury attorney.
        </p>
        {/* <center>
          <div className="button_cont" align="center">
            {" "}<Link className="example_b" to="/contact.html" target="_blank">
              {" "}
              Contact Bisnar Chase Now!{" "}
            </Link>
          </div>
        </center> */}
        <p>
          At Bisnar Chase we have a 96% success rate and have collected more
          than $500 million for our clients.
        </p>
        <p>
          If you have been hurt by an electric shock due to the fault of another
          person or company, you deserve to be compensated. Trust our{" "}
          <Link to="/" target="new" title="California Personal Injury Attorney">
            California personal injury lawyers
          </Link>{" "}
          to fight for you.
        </p>
        <div className="mb">
          {" "}
          <Link to="/misc/electrocution-injury-lawyer#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is Electrocution? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Common Causes of Electric Shock Lawsuits{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Electrocution Injuries and Effects <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Electrocution Injury Statistics <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What to Do If You Are Electrocuted <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Who is Liable for An Electrocution Injury Claim?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Finding the Best Electrocution Injury Attorney Near Me{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Electrocution Injury Damages and Compensation in California{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/electrocution-injury-lawyer#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Trust Bisnar Chase with your Electrocution Case{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>What is Electrocution?</h2>
        <hr />
        <p>
          Electrocution refers to a person coming into contact with an electric
          current. An accident will involve the electric current passing through
          the body of the victim and causing shock injuries.
        </p>
        <h3>
          <b>Electrocution vs Electric Shock – Is there a difference?</b>
        </h3>
        <p>
          Technically, electrocution refers specifically to a fatal shock which
          results in the death of the victim. The word originates from a
          combination of ‘electricity’ and ‘execution’. However, informal use of
          the word is now used to describe a shock which results in death or
          injury.
        </p>
        <p>
          Electric shock is a wider term which can mean a minor or serious
          incident. It will still involve an electric charge coming into contact
          with the body, but may not lead to injury.
        </p>
        <div id="header2"></div>
        <h2>Common Causes of Electric Shock Lawsuits</h2>
        <LazyLoad>
          <img
            src="/images/electrocution/electrocution-lawyers-in-california.jpg"
            width="350"
            alt="A malfunctioning plug exploding from a wall socket in a flash of electricity."
            className="imgright-fixed"
            title="California Electrocution Accident Lawyer"
          />
        </LazyLoad>
        <p>
          There are several common causes of electrocution accidents in
          California which can leave people with severe injuries. These include:
        </p>
        <ul>
          <li>
            Contact with frayed or damaged power cords, extension leads, and
            chargers
          </li>
          <li>Faulty electrical items and appliances</li>
          <li>Overuse or misuse of extension cords</li>
          <li>Damaged power lines</li>
          <li>Old or poorly maintained wiring within a home or workplace</li>
          <li>Live wires at construction sites and renovation projects</li>
          <li>Exposed electricals near water</li>
        </ul>
        <p>
          When you come into contact with an electrical current through any of
          these sources, you could receive a significant shock resulting in
          serious injuries.
        </p>
        <p>
          If you believe you have an electrocution injury legal claim, contact
          our personal injury attorneys for expert help and guidance in
          navigating the California courts and laws.
        </p>
        <div id="header3"></div>
        <h2>Electrocution Injuries and Effects</h2>
        <hr />
        <p>
          There are several factors which can impact how severe an{" "}
          <Link
            to="https://www.webmd.com/a-to-z-guides/electric-shock#2-3"
            target="new"
            title="Electric Shock Injury Symptoms"
          >
            {" "}
            electrocution injury
          </Link>{" "}
          is. These include:
        </p>
        <ul>
          <li>The type and strength of the electric current</li>
          <li>
            The length of time a person comes into contact with a high voltage
            current
          </li>
          <li>The person’s health before the incident</li>
        </ul>
        <p>
          The type of injuries that can be caused by an electrocution accident
          include:
        </p>
        <ul>
          <li>Burns</li>
          <li>Muscle spasming</li>
          <li>Heart problems including cardiac arrest</li>
          <li>Loss of consciousness or coma</li>
          <li>Headaches and loss of hearing or vision</li>
          <li>Seizures</li>
          <li>Loss of feeling in extremities</li>
          <li>Death</li>
        </ul>
        <p>
          These common electric shock injuries show just how dangerous an
          accident can be. The most serious injuries are considered to be
          cardiac issues and burns. In many cases, electrocution burn injuries
          may appear superficial, but could have had a devastating impact
          internally.
        </p>
        <p>
          If you have suffered an injury, you should seek medical attention as
          soon as possible, before turning to an electrocution injury attorney
          for legal help.
        </p>
        <div id="header4"></div>
        <h2>Electrocution Injury Statistics</h2>
        <LazyLoad>
          <img
            src="/images/electrocution/california-electric-shock-attorney-near-me.jpg"
            width="40%"
            alt="A graphic showing exposed wires shooting blue electricity into the air, demonstrating electric shock dangers."
            title="California Electric Shock Injury Lawyers"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Most people think that they will never be injured by a serious
          electric shock. But even if you know the risks and take all reasonable
          precautions, faulty wiring or equipment could leave you with an
          injury.
        </p>
        <p>
          The following{" "}
          <Link
            to="https://www.ncbi.nlm.nih.gov/books/NBK448087/"
            target="new"
            title="Electric Shock Accident Statistics"
          >
            {" "}
            electrocution accident statistics
          </Link>{" "}
          show how dangerous an electric shock can be to victims:
        </p>
        <ul>
          <li>
            There are more than 1,000 electrocution deaths every year, including
            400+ high-voltage shock deaths
          </li>
          <li>
            At least 30,000 electric shock accidents leave victims with
            non-fatal but serious injuries each year
          </li>
          <li>
            Such incidents account for about 5% of all hospital burn unit
            admissions
          </li>
          <li>
            Many electric shock accidents happen in the workplace. They are the
            fourth largest cause of traumatic deaths in the workplace
          </li>
          <li>
            A huge 80% of injuries occur to adults, with children accounting for
            20% of electrical injuries
          </li>
        </ul>
        <p>
          These statistics show that electric shock injuries are not restricted
          to careless people sticking silverware into the toaster. It is a
          serious issue, often arising from negligent acts in electrical safety,
          manufacturing, and maintenance.
        </p>
        <div id="header5"></div>
        <h2>What to Do If You Are Electrocuted</h2>
        <hr />
        <p>
          If you have suffered an electric shock and believe you have a legal
          claim, take the following steps:
        </p>
        <ol>
          <li>
            <span>
              <strong> Call 911.</strong>Make sure you receive medical treatment
              if needed. Medical reports can be requested and later used as
              evidence.
            </span>
          </li>

          <li>
            <span>
              <strong> Document the accident.</strong>Take pictures of the
              unsafe scene, as well as any visible injuries.
            </span>
          </li>

          <li>
            <span>
              <strong> Stay away from the source of the shock.</strong>Steer
              clear if possible, and use non-conductive materials if you need to
              get close to the shock source.
            </span>
          </li>

          <li>
            <span>
              <strong> Speak to witnesses.</strong>Get signed witness accounts
              of the accident, as well as their contact details so you can reach
              out to them again if needed.
            </span>
          </li>

          <li>
            <span>
              <strong> Write your own account.</strong>Write down a detailed
              description of the incident from your own point of view while the
              details are still fresh in your mind.
            </span>
          </li>

          <li>
            <span>
              <strong> Contact Bisnar Chase.</strong>Approach an experienced
              electrocution injury attorney in California for expert advice on
              how to proceed.
            </span>
          </li>
        </ol>
        <p>
          Click for more information on{" "}
          <Link
            to="https://www.healthline.com/health/electric-shock#first-aid"
            target="new"
            title="Electrocution First Aid Tips"
          >
            {" "}
            electric shock first aid
          </Link>
          , and how to react after an accident has happened.
        </p>
        <div id="header6"></div>
        <h2>Who is Liable for An Electrocution Injury Claim?</h2>
        <hr />
        <p>
          It is important to work with your electrocution lawyer to determine
          who was responsible for an electric shock injury. Liability will
          depend on when and where the accident happened.
        </p>
        <p>
          There are several areas of California law which can be used to sue a
          negligent party after an electrocution accident.
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              <u>Premises Liability</u>
            </strong>

            <p>
              If the owner or manager of a property either knew about an
              electrical problem and failed to fix it, or should have known
              about the problem, they could be{" "}
              <Link
                to="/premises-liability"
                target="new"
                title="California Premises Liability Lawyer"
              >
                liable for an injury or incident{" "}
              </Link>
              . For instance, if a building owner knew that their wiring was
              unsafe but failed to act, they could be sued in an electrocution
              lawsuit.
            </p>
          </li>

          <li>
            <strong>
              {" "}
              <u>Lack of Training</u>
            </strong>

            <p>
              Employers have a responsibility to train workers in electrical
              safety, as well as how to properly use potentially dangerous
              electric equipment. If proper training is not conducted or
              enforced, the employer could be held responsible for a resulting
              accident.
            </p>
          </li>

          <li>
            <strong>
              {" "}
              <u>Negligent Supervision</u>
            </strong>

            <p>
              Negligent supervision is most often applied to cases in which
              children suffer electric shock injuries. For instance, daycare
              workers must watch youngsters to make sure they are not exposed to
              dangerous electrical settings. Fault could also apply if a child
              was allowed to swim in an outdoor pool during a lightning strike
              while under another person’s care.
            </p>
          </li>

          <li>
            <strong>
              {" "}
              <u>Vicarious Liability</u>
            </strong>

            <p>
              If an employee acts in a careless or negligent way, they may be
              held liable. However, vicarious fault could also put some level of
              responsibility on their employer, who is accountable for their
              workplace actions.
            </p>
          </li>

          <li>
            <strong>
              {" "}
              <u>Product Liability</u>
            </strong>

            <p>
              Anyone involved in the manufacturing or production of a product is
              responsible for taking all reasonable care to ensure its safety.
              If someone is shocked or suffers an electrocution injury from a
              defective or{" "}
              <Link
                to="/defective-products"
                target="new"
                title="Califronia Defective Product Lawyer"
              >
                malfunctioning product{" "}
              </Link>
              , a lawsuit could follow.
            </p>
          </li>
        </ul>
        <div id="header7"></div>
        <h2>Finding the Best Electrocution Injury Attorney Near Me</h2>
        <hr />
        <LazyLoad>
          <img
            src="/images/electrocution/best-electrocution-accident-lawyer-in-california.jpg"
            width="300"
            alt="A dangerously overloaded power strip with many plugs creating an electrocution risk."
            title="Electric Shock Injury Lawyers Near You"
            className="imgleft-fixed"
          />
        </LazyLoad>

        <p>
          At Bisnar Chase we pride ourselves on offering top-quality
          representation to electric shock injury victims.
        </p>
        <p>
          An electrocution injury lawsuit can have a number of benefits. First
          and foremost, it can hold the culprit accountable for their negligent
          actions. We believe that this is important. No one should have their
          lives put at risk due to carelessness or negligence.
        </p>
        <p>
          An electric shock lawsuit may also lead to safety improvements. This
          can stop other people suffering from the same dangers.
        </p>
        <p>
          In addition, an injury claim in California can result in compensation
          being paid to the victim. Our electric shock injury lawyers are
          experts when it comes to negotiating settlements with defense teams
          and insurance firms. But they are also skilled trial lawyers who will
          not hesitate to take a case before a jury if needed.
        </p>
        <p>
          Our electrocution injury lawyers are based in Orange County and have
          secured spectacular results for clients across Southern California.
          The areas we serve include:
        </p>
        <ul>
          <li>Newport Beach</li>
          <li>Santa Ana</li>
          <li>Irvine</li>
          <li>Huntington Beach</li>
          <li>Los Angeles</li>
          <li>Anaheim</li>
        </ul>
        <p>
          You can trust our California electrocution injury attorneys to fight
          for you and secure the maximum compensation possible.
        </p>
        <div id="header8"></div>
        <h2>Electrocution Injury Damages and Compensation</h2>
        <hr />
        <p>
          The amount of compensation a victim will receive in a personal injury
          case will depend on a wide range of variables.
        </p>
        <p>
          These are some of the factors which can affect the amount of
          compensation a victim receives:
        </p>
        <ul>
          <li>The severity of their injury</li>
          <li>Any long-term effects caused by the electric shock</li>
          <li>Pain and suffering</li>
          <li>Lost wages</li>
          <li>
            Loss of ability to work in a chosen career/loss of earning potential
          </li>
          <li>
            The level of negligence associated with the electric shock accident
          </li>
          <li>Medical bills and care costs</li>
          <li>
            In the case of wrongful death – funeral/burial costs, loss of life
            and companionship
          </li>
        </ul>
        <p>
          An experienced California electrocution injury attorney will be able
          to work with you to evaluate the potential value of your claim.
        </p>
        <div id="header9"></div>
        <h2>Trust Bisnar Chase with your Electrocution Case</h2>
        <hr />
        <LazyLoad>
          <img
            src="/images/electrocution/fatal-electrocution-attorney-california.jpg"
            width="200"
            alt="A warning sign saying: Keep Off - Death Risk By Electrocution."
            title="Lawyers for Electrocution Death and Injury Cases"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Electrocution is extremely dangerous and can result in serious injury
          or death. Whether you have suffered an injury in an electric shock
          accident at work, or been hurt by a defective electrical product, we
          can help you with your case.
        </p>
        <p>
          Bisnar Chase takes pride in helping victims and family members secure
          the maximum compensation possible from electric shock injury claims.
          Our skilled electrocution attorneys have built up a{" "}
          <b>96% success rate</b>. They have also collected more than{" "}
          <b>$500 million</b> for our clients over the last 40 years.
        </p>
        <p>
          Our law firm is also proud to offer a ‘No Win, No Fee’ guarantee. This
          allows us to advance all costs for our clients. We only get paid if we
          win your case.
        </p>
        <center>
          <div className="contact-us-border">
            <p>
              Contact our{" "}
              <strong> California electrocution injury lawyers</strong> now!{" "}
              <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> for
              immediate help, visit our law offices in Newport Beach, or{" "}
              <Link
                to="/contact.html"
                target="new"
                title="Contact Bisnar Chase Personal Injury Attorneys"
              >
                click to arrange a free consultation{" "}
              </Link>
              now.
            </p>
          </div>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="streetAddress">1301 Dove St., #120</span>{" "}
                  <span itemProp="addressLocality">Newport Beach</span>,{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <span itemProp="telephone">(949) 203-3814</span>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1301+Dove+St.,+Newport+Beach,+CA,+92660&sll=37.0625,-95.677068&sspn=33.02306,56.337891&ie=UTF8&ll=33.680783,-117.858582&spn=0.068011,0.110035&z=13&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px;
    border-radius: 6px;

    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -140px;

    opacity: 0;
    transition: opacity 0.3s;
  }

  .tooltip .tooltiptext::before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
