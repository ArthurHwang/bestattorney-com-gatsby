// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Aviation Accident Attorneys - Plane Crash Lawyers"
        pageDescription="California Aviation Accident Lawyers can help plane crash victims get the compensation they deserve. No fee, no win promise, representing accident victims for 40 years with a 96% success rate and hundreds of millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Aviation Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          If you would like to file a claim against an airline or the
          manufacturer of a defective product to recover damages suffered by you
          and your family, it is critical that you seek the counsel of an
          experienced <Link to="/">California personal injury lawyer</Link>, who
          is knowledgeable about aviation law and the financial resources to
          represent you from start to finish. Bisnar Chase not only has the
          experience, resources and the necessary fire power to accomplish the
          individual goals of our clients, but we also understand that a
          properly prepared case against those responsible will ultimately save
          lives and prevent future tragedies.
        </p>
        <p>
          If you have been involved in a serious airline accident, you are
          entitled to seek compensation for your injuries, damages and losses.
          The experienced aviation accident attorneys at Bisnar Chase can guide
          you through the process of pursuing justice and help you get the best
          results in your personal injury or wrongful death lawsuit.
          <strong>
            {" "}
            For a free consultation, please call us today at 949-203-3814
          </strong>
          .
        </p>
        <p>
          <em>
            A plane crash death can fall under a wrongful death claim in injury
            law. Here, personal injury attorney John Bisnar describes wrongful
            deaths in California.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_okWKolf9qc?list=UURMtvG2jyspSYrDgoXo7EVA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>California Aviation Accidents</h2>
        <p>
          Aviation accidents occur rarely, but cause unspeakable tragedy and
          devastation when they do happen. The families that have lost their
          loved ones as well as the flying public will have a number of
          questions after a catastrophic aviation accident and they deserve
          answers.
        </p>
        <p>
          Uncovering the truth regarding what caused an aviation accident is
          never easy. Investigations into aviation accidents, which are usually
          carried out by the National Transportation Safety Board (
          <Link
            to="http://www.ntsb.gov/publications/Pages/default.aspx"
            target="_blank"
          >
            NTSB
          </Link>
          ), are complicated and lengthy.
        </p>
        <p>
          It takes painstaking research and investigation by an experienced
          California aviation accident attorney to get to the bottom of such
          incidents and determine precisely what happened and why.
        </p>
        <h2>Aviation Accident Statistics</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/aviation-accidents/airplane.jpg"
              alt="aviation accidents"
              className="imgright-fixed"
            />
          </LazyLoad>
          According to the Review of U.S. Civil Aviation Accidents by the
          National Transportation Safety Board, there were 1,500 civil aviation
          accidents in the year 2010. In those aviation accidents, 470 people
          were killed.
        </p>
        <p>
          The majority of aviation accidents involve personal aircraft, but
          there are a few commercial airline incidents that occur each year as
          well. In the year 2010, commercial airlines were involved in one major
          accident, 13 non-fatal accidents with at least one serious injury and
          14 crashes where no one was killed or seriously injured, but the
          aircraft was substantially damaged.
        </p>
        <h2>Causes of Aviation Accidents</h2>
        <p>
          Whether you are in a large commercial plane, a helicopter or a small
          private plane, it will be necessary to determine how the accident
          occurred and who or what was responsible. The NTSB reports that 89
          percent of personal flying accidents involved single-engine airplanes.
          The majority of those accidents involved a loss of control in flight.
          The most common causes of California aviation accidents include:
        </p>
        <ul>
          <li>
            <strong> Pilot error</strong>: Was the pilot fatigued, distracted,
            impaired or inexperienced?
          </li>
          <li>
            <strong> Mechanical failure</strong>: This could include engine and
            component failure. Was the part improperly installed, negligently
            maintained or defectively designed?
          </li>
          <li>
            <strong> Runway defects</strong>: Did an uneven surface force the
            vehicle to crash?
          </li>
          <li>
            <strong> Inadequate maintenance</strong>: Did the airline
            negligently avoid necessary repairs to save money? Did they fail to
            carry out the required inspections before take off? Were there any
            safety violations
          </li>
          <li>
            <strong>
              {" "}
              Violation of Federal Aviation Administration (FAA) regulations
            </strong>
            : There are many rules and regulations set forth by the FAA that
            must be followed at all times. Companies that put passengers in
            harm's way may be held accountable for their wrongdoing.
          </li>
          <li>
            <strong> Design or structural problems</strong>: Did the accident
            occur due to defective design?
          </li>
          <li>
            <strong> Federal Air Traffic Controller error</strong>: According to
            March 4, 2013 report in The Washington Post, there has been a sharp
            increase in air traffic controller errors in recent years. Runway
            incursions and potential crashes could occur if air traffic
            controllers fail to do their jobs.
          </li>
        </ul>
        <h2>Aviation Litigation</h2>
        <p>
          Most plane crashes are general aviation accidents involving small
          planes. These types of crashes almost always result in catastrophic
          injury or wrongful death. When the victim suffers multiple bone
          fractures, traumatic brain injuries or spinal cord trauma, he or she
          may suffer long-term physical, cognitive and emotional consequences.
          In some cases, a pilot or passenger may suffer permanent brain damage
          or paralysis resulting in a loss of ability to work or provide for
          their families again.
        </p>
        <p>
          Victims of charter, tourist or commercial plane accidents can pursue
          financial compensation for their losses from the at-fault parties.
          Therefore, determining liability for the crash is an important part of
          the investigation process. Proving the cause of the crash can help
          establish who should be held accountable for the injuries and losses
          suffered by victims and/or their families. Often times, there may be
          several liable parties involved.
        </p>
        <h2>Negligence and Wrongdoing</h2>
        <p>
          Many accidents are the direct result of pilot error, weather
          conditions and errors on the part of air traffic controllers. Whatever
          the cause, it will be necessary to prove if the at-fault party was
          behaving reasonably before the accident or if negligence was a
          contributing factor in the crash. Was the pilot fatigued or
          distracted? Was the air-traffic controller distracted? Did the airline
          company fail to properly monitor the actions of the pilot? Was alcohol
          a contributing factor in the accident? These are all forms of
          negligence. If a defective product caused the aviation accident, the
          manufacturer of that product can also be held liable.
        </p>
        <p>
          Injured victims of aviation accidents can pursue financial
          compensation for damages including medical bills, the cost of
          rehabilitation, lost wages, physical pain, mental anguish, loss of
          earning potential and other related damages. The family of someone
          killed in an aviation accident can file a{" "}
          <Link to="/wrongful-death">wrongful death </Link> claim to pursue
          financial compensation for their losses as well. In such cases,
          monetary support may be available for funeral expenses, lost future
          wages, pain and suffering and loss of companionship.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
