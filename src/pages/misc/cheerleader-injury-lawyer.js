// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cheerleader injury lawyers - Bisnar Chase"
        pageDescription="Contact the cheerleading injury lawyers of Bisnar Chase if your child has been harmed during cheer practice or while cheering for the school. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cheerleader Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Injuries to high school cheerleaders serve as an intriguing
          intersection of many conflicting legal rights and duties. An
          interesting stew is created with evaluating the balance of whether a
          participant has assumed the risks inherent in cheerleading, against
          the legal duty that a school district may have to protect the safety
          of its students, leavened with the effect of the release or waiver of
          liability that a parent has probably signed on behalf of that student
          as a condition of the student's participation in cheerleading. How
          satisfying that stew is to the interests of the injured cheerleader
          depends on the laws of the state where the injury occurred.
        </p>
        <p>
          If your child has been injured in a cheerleading accident and you
          believe it has special circumstances, contact our{" "}
          <Link to="/">personal injury lawyers </Link> to review your rights.
          You may be entitled to compensation including medical bills and pain
          and suffering.
        </p>
        <h2>Assumption of the Personal Injury Risk</h2>
        <p>
          Cheerleading today is much more than girls and boys with pom pons,
          megaphones, sweaters, and spirit. Elements of gymnastics now dominate
          the "performance" given by squads at games and other events.
          Competition is usually the name of the game, just as competitive as
          the activity being contested by the athletes at the same time. As a
          result of the increasingly athletic activity inherent in cheerleading,
          it is more easily seen as a "sport" just like football, basketball,
          soccer, and others. In most states, then, an analysis of the risk of
          personal injury or other harm assumed by a participant in cheerleading
          is conducted in the same way that a court would analyze the issue
          presented by the injury suffered by a football player.
        </p>
        <p>
          There is a legal concept known as "assumption of the risk" which holds
          that a person may, either expressly or impliedly, agree that other
          people involved in an activity with him owe him less or no duty of
          care to not cause him personal injury. One might expressly assume the
          risk of personal injury by agreeing that the others will not be
          legally responsible for him -- usually memorialized by a "release" or
          "waiver" of liability. When a person voluntarily takes part in an
          activity which has known inherent risks of injury or harm, she might
          impliedly accept the risks of that harm. Whether a person plays
          football, snow skis, rides a wakeboard, "car surfs," or performs a
          basket toss in a cheerleading routine, there are risks of personal
          injury inherent in these activities.
        </p>
        <p>
          In the states which have not adopted assumption of the risk, ordinary
          negligence standards apply in deciding whether or not a co-participant
          owed a duty of due care to the injured person.
        </p>
        <p>
          How completely the application of assumption of the risk prevents or
          limits a recovery from other participants is usually broken into two
          categories: complete ("primary") or partial ("secondary"). In
          activities where the risk of personal injury is so inherent in the
          activity that it cannot be performed, or performed well, without those
          risks, the assumption of the risk is primary -- as long as the injury
          resulted from conduct which is inherent in the activity, no one owes
          another participant a duty to protect him from that injury. When the
          risk of personal injury is a part of the activity, but not inherent in
          all conduct of the activity (such as driving or traveling by car) the
          other participants have some duty to protect others from injury but
          the participant must also protect himself from injury.
        </p>
        <p>
          In most states which have adopted assumption of the risk,
          participation in "sport" is a circumstance in which primary assumption
          of the risk can be applied. "Sport" is vigorous physical activity,
          usually performed with others in a competitive environment. Most sport
          involves the possibility of physical contact between participants,
          often forceful physical contact. Sport also involves the performance
          of physical activity which pushes the participant's body into
          stressful or straining conditions (running, twisting, tumbling,
          pitching) often with a risk of personal injury if the participant does
          not perform the activity as expected (diving from a high dive, a
          gymnast on the rings or pommel horse or high beam). Sport also
          involves coaches, trainers, spotters, and others who help develop the
          more active participants' expertise in the activity, and therefore are
          "participants". Sport also involves the owners, operators, and
          maintenance people of the ski slopes or fields or floors where the
          activities occur. If people were legally liable for the injuries
          caused to others in these activities, or a coach or gym was
          responsible when an athlete did not perform a routine safely, there
          would be a damper on the full enjoyment of these activities; people
          would have to hold back or limit what they do. As the California
          Supreme Court wrote in Kahn v. East Side Union High School (2003) 31
          Cal.4th 990, 1004, 4 Cal.Rptr.3d 103.
        </p>
        <blockquote>
          Principles of the doctrine of primary assumption of the risk of injury
          inherent to certain sporting activities exist because, as a matter of
          policy, it would not be appropriate to recognize a duty of care when
          to do so would require that an integral part of the sport be
          abandoned, or would discourage vigorous participation in sporting
          events.
        </blockquote>
        <p>
          Imposing legal liability for personal injuries would change the sport.
          Therefore, these states hold that a participant impliedly assumes the
          risks of personal injury that are inherent in a sporting-type
          activity. In many of these states cheerleading is seen as a sport, and
          so a cheerleader impliedly assumes the risks of personal injury which
          are inherent in modern cheerleading.
        </p>
        <p>
          In California (one of the states which has adopted primary assumption
          of the risk, applied it to sports, and considers cheerleading a
          sport), the analysis of these issues is a two-step process. First is
          to decide whether or not the injury was caused by conduct which is
          inherent in and an integral part of the activity. If not (such as a
          case where a middle school student was hit in the head by a golf club
          during PE), the analysis stops and returns to general negligence
          analysis. If the risk is inherent (such as diving too deeply from
          blocks in competitive swimming and injuring yourself when you hit the
          bottom of the pool), then the court moves to the next step. In that
          step, the court must evaluate whether the person or entity who is the
          defendant in the case, as a co-participant in the activity,
          "intentionally injured the participant or engaged in conduct that is
          so reckless as to be totally outside the range of ordinary activity
          involved in the sport" (Knight vs. Jewett (1992) 3 Cal.4th 296, 320,
          11 Cal.Rptr.2d 2.) which increased the risk of, and contributed to,
          the at-issue injury.
        </p>
        <p>
          When it comes to a coach or instructor, the question is better phrased
          as whether the defendant acted with the intent to cause the injury or
          acted recklessly in his supervision or instruction which increased the
          risks of injury inherent in the activity. How to judge whether
          supervision or instruction was "reckless" remains an open question,
          with one formulation being instruction "which departs from the range
          of ordinary instructional activities, increasing the risks of injury
          beyond those inherent in teaching a sport, and is therefore subject to
          liability, when his or her conduct constitutes a gross or extreme
          departure from the instructional norms." (Kahn v. East Side Union High
          School District, supra, concurring opinion of Justice Werdegar at page
          1019.)
        </p>
        <p>
          Looking now at cheerleading injuries, we have potential claims against
          co-participants along two dimensions: other members of the squad, and
          the teacher/coach of the squad. To determine whether or not a claim
          might be allowed against a team member, we must evaluate whether the
          conduct of that team member unreasonably increased the risk of injury
          by conduct outside of the ordinary gymnastics/cheerleading activity.
          This does not mean to be without skill, this does not include making a
          mistake in the performance of the maneuver or not understanding how to
          do it. To be possibly liable the team member must have stepped far
          outside of the bounds of normal performance in causing the injury. To
          evaluate the possible liability of a coach or instructor, we must look
          beyond being untrained or lesser trained: the coach's instructions or
          the failure to properly supervise must be an extreme departure from
          the norms for coaches of the activity. As you can imagine, these
          determinations are very fact dependent and often require the
          resolution of disputed factual issues.
        </p>
        <h2>Duty of School Instructors</h2>
        <p>
          In most states, schools and their staff have a special relationship
          with their students. Since a major element of that relationship is the
          supervision and guidance of young people, teachers have a higher duty
          to protect students from harm while the students are under their
          supervision. In many states this duty is described or imposed by
          statute. This duty of care is characterized in California as being the
          duty of a "prudent person" in like or similar situations, rather than
          an "ordinary" person.
        </p>
        <p>
          In states which would not be applying a primary assumption of the risk
          analysis to the injured cheerleader's claims, this higher duty of care
          would make it easier for the cheerleader to be successful in a case
          against the school district or its employees for negligent supervision
          or coaching.
        </p>
        <p>
          What happens when this higher duty of care is met by a primary
          assumption of the risk circumstance (no duty or greatly reduced duty)?
          States will answer this differently. Most states would not use the
          higher duty of care in its assumption of the risk analysis -- this
          would negate the doctrine in these cases -- but would apply the higher
          duty if the court finds that the injury did not result from a risk
          inherent in the activity. Your state may, or may not, apply the
          "prudent man" standard in deciding the second prong of the test,
          whether the conduct of the participant was reckless and unjustly
          increased the risk of injury. It will be easier to establish
          recklessness if the co-participant will be held to a prudent man
          standard of care.
        </p>
        <h2>Legal Effect of Release/Waiver of Liability Signed by Parent</h2>
        <p>
          The participants in most sponsored risky activities are required to
          sign a Waiver of Liability as a condition of being allowed to
          participate. In such a waiver, a participant will agree that the
          participant will not be able to sue the sponsoring entity and its
          employees because of an injury suffered because of the participation.
          A minor does not have legal capacity to sign such a waiver, so the
          minor's parent(s) must sign on her behalf. Is this signed waiver an
          express assumption of the risk of injury on behalf of the minor
          cheerleader and, if so, how far does this protect the sponsoring
          entity and its employees?
        </p>
        <p>
          As you might have already guessed, it depends on your state. Most
          states will enforce the waiver against the claims of the cheerleader,
          even when signed by a parent. Because participation is seen as a
          necessity for the minor and this signature as a requirement for
          participation, it will be enforced. Other states will not enforce the
          waiver.
        </p>
        <p>
          To what extent the claims against the sponsoring entity are waived
          depends on the terms of the waiver document and state law. These
          documents are strictly construed against the sponsoring entity, and in
          the light of any uncertainty about whether a claim is waived the
          waiver would not be enforced. Most if not all states have a public
          policy which will not enforce a term which purports to allow a waiver
          of the consequence of acts which were intended to cause personal
          injury, or which were caused by reckless or grossly negligent conduct.
        </p>
        <p>
          In California, a waiver signed by a parent is enforceable against the
          cheerleading participant. The waiver can properly be given to a
          government entity like a school district. If properly worded, a waiver
          might be enforced in favor of the school district. However, since
          California imposes a higher duty of care for students on schools and
          their employees, and liability would not imposed unless the teacher or
          coach was reckless in her coaching, such a waiver would not bar an
          action against a coach or her employer if the claim were to survive
          the two-part analysis of the application of primary assumption of the
          risk.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
