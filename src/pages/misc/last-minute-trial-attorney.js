// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "trial-attorneys/california-last-minute-trial-attorneys.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Last-minute Trial Lawyers - Bisnar Chase"
        pageDescription="A last-minute trial lawyer can step in to provide expert courtroom representation at short notice. Bisnar Chase has an outstanding trial track record - contact us for a free consultation now."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Last-minute Trial Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California last-minute trial attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The trial lawyers of Bisnar Chase take pride in their mission – to
          make sure every client gets the compensation and justice they deserve.
          But not every client starts out by hiring our experienced personal
          injury attorneys. When a client is looking for new representation or
          additional courtroom expertise, Bisnar Chase is also adept at stepping
          in to provide highly-effective
          <b> last-minute trial lawyer</b> services.
        </p>
        <p>
          Bisnar Chase has an outstanding success rate, built on the skills of
          its dedicated attorneys. If your case is coming to trial and you need
          new or additional legal counsel, Bisnar Chase can step in to provide
          superior representation in the courtroom.
        </p>
        <div className="mb">
          {" "}
          <Link to="/misc/last-minute-trial-attorney#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is a Last-Minute Trial Attorney? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Do Clients Look for Last-Minute Trial Attorneys?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Can You Change Lawyers During a Trial?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              When is it Too Late to Hire a Last-Minute Trial Attorney?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Does a Last-Minute Trial Attorney Do?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Qualities of the Best Last-minute Trial Attorneys{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Will Bisnar Chase Take Your Late-Notice Case?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/misc/last-minute-trial-attorney#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Bisnar Chase Can Provide Outstanding Last-Minute Representation{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>

        <div id="header1"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="What is a Last-Minute Trial Attorney?"
            style={{
              backgroundImage:
                "url('/images/trial-attorneys/last-minute-trial-lawyer.jpg')"
            }}
          >
            <h2>What is a Last-Minute Trial Attorney?</h2>
          </div>
        </LazyLoad>
        <p>
          A last-minute trial attorney is a lawyer who steps in to take on a
          case which involves an impending trial.
        </p>
        <p>
          They might replace a fired attorney, or join an existing legal team to
          add trial experience on relatively short notice.
        </p>
        <p>
          This is not a service that every lawyer or firm offers. It requires a
          great deal of courtroom skill, as well as the resources to get up to
          speed at short notice.
        </p>
        <p>
          It is important to note that the title of ‘last-minute trial attorney’
          is slightly misleading. No lawyer can take over a case on the eve of
          the trial and expect to be successful. The best trial attorneys are
          well prepared and know the case better than anyone else.
        </p>
        <p>
          Read on for a full rundown of the time frames that last-minute legal
          representation can operate under.
        </p>

        <div id="header2"></div>
        <h2>Why Do Clients Look for Last-Minute Trial Attorneys?</h2>
        <LazyLoad>
          <img
            src="/images/trial-attorneys/best-last-minute-trial-lawyers.jpg"
            width="40%"
            className="imgright-fluid"
            alt="best last minute trial lawyers"
          />
        </LazyLoad>
        <p>
          There can be a wide range of reasons for a client to hire a
          last-minute trial lawyer.
        </p>
        <p>
          Changing lawyers for trial is not ideal. However, it could be the best
          course of action if the new attorney is a better fit for the client
          and their case.
        </p>

        <p>
          In some situations, a client may decide that they need a different
          attorney to handle their case. Perhaps a client and lawyer experience
          a personality clash, or the client strongly disagrees with the
          attorney’s strategy. In other cases, a lawyer might have been
          unhelpful or non-responsive.
        </p>
        <p>
          If an attorney switch is made before a trial, it can be as simple as
          firing one firm, requesting your case file, and handing it over to the
          replacement. If it is done during a trial, a notification must be
          filed to the court first.
        </p>
        <p>
          In some cases, a client may not be looking for a change of lawyer at
          all. A <b>California last-minute trial attorney</b> may be sought by
          the current legal team if it has limited trial experience. If a legal
          team does not feel comfortable in going to trial, a more experienced
          trial lawyer could be brought in to take the lead in the courtroom.
          This is also a role that the personal injury trial lawyers at Bisnar
          Chase are comfortable in filling.
        </p>
        <div id="header3"></div>
        <h2>Can You Change Lawyers During a Trial?</h2>
        <p>
          It is rare for a client to change lawyers in the middle of a personal
          injury trial. However, it can happen.
        </p>
        <p>
          For instance, if an attorney falls ill or is otherwise taken out of
          commission during a trial, a replacement may be brought in and allowed
          extra time to get up to speed. If this was the case, another lawyer
          may consider taking over. However, it is more likely that a judge
          would declare a mistrial in this scenario.
        </p>
        <p>
          It is highly unlikely that Bisnar Chase would take over a case and
          step into a trial while it is already in progress. This would only
          happen in exceptional circumstances.
        </p>

        <div id="header4"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="When is it Too Late to Hire a Last-Minute Trial Attorney?"
            style={{
              backgroundImage:
                "url('/images/trial-attorneys/replacement-attorney.jpg')"
            }}
          >
            <h2 id="header4">
              When is it Too Late to Hire a Last-Minute Trial Attorney?
            </h2>
          </div>
        </LazyLoad>
        <p>
          Bringing in a late-stage case lawyer does not mean hiring someone on
          your way into the courtroom.
        </p>
        <p>
          Some law firms might promise the ability to step into a personal
          injury trial at a moment’s notice and achieve positive results. The
          truth is that it will rarely work out like that. Even the
          <b> best last-minute trial attorneys</b> need time to prepare if they
          are to provide top-quality representation to the client.
        </p>
        <p>So, how late is too late?</p>
        <p>
          At Bisnar Chase, we look at each case carefully before deciding
          whether to take it on. As such, there are no ‘strict’ timelines – but
          we do have timeframe guidelines.
        </p>
        <ul>
          <li>
            <b>Preferred –</b> 90+ days until trial
          </li>
          <li>
            <b>Probable –</b> 60 days until trial
          </li>
          <li>
            <b>Possible (heavily dependent on circumstance) –</b> Within 30 days
            of trial
          </li>
        </ul>
        <p>
          As always, the more time we are given to prepare before a trial, the
          better. However, we know that this is not always possible in the
          search for a last-minute trial lawyer.
        </p>
        <p>
          The timeline will depend on a range of factors, such as how much work
          and pre-trial preparation has been done by the client’s previous
          representation. If thorough preparation has been done, our attorneys
          will need less time to get up to speed.
        </p>
        <p>
          Some cases are also more complicated than others. The timeline will
          depend on how complex or technical the subject matter is, and how hard
          it is to prove damages and liability.
        </p>
        <p>
          In most cases, at least <b>90 days</b> is preferred for preparation
          before going to trial.
        </p>
        <p>
          However, the last-minute trial attorneys of Bisnar Chase can handle
          most cases within a <b>60-day window</b> of the trial. Some cases can
          even be taken on
          <b> within 30 days</b> of a trial.
        </p>
        <p>It will always depend on the individual case.</p>

        <div id="header5"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Does a Last-Minute Trial Attorney Do?"
            style={{
              backgroundImage:
                "url('/images/trial-attorneys/last-minute-legal-representation.jpg')"
            }}
          >
            <h2>What Does a Last-Minute Trial Attorney Do?</h2>
          </div>
        </LazyLoad>
        <p>
          The role that last-minute lawyers play will depend on when they are
          brought in, and whether they will be the sole representation, or
          assisting another attorney.
        </p>
        <p>Some of the details which go into preparing for a trial include:</p>
        <ul>
          <li>Learning the background of the case</li>
          <li>Developing opening and closing arguments</li>
          <li>
            Lining up evidence and exhibits showing liabilities and damages
          </li>
          <li>Preparing witnesses and cross-examinations</li>
          <li>And much more!</li>
        </ul>
        <p>
          A huge amount of work goes into preparing an effective case for trial.
        </p>
        <p>
          In the most extreme short-notice cases, it would be necessary for the
          previous or current legal team to have already arranged the witnesses
          and evidence needed for the trial. These things take time.
        </p>
        <p>
          Even when you are working to a slightly longer timeframe, some of this
          trial preparation may have already been completed by the client’s
          previous legal representation. The new
          <b> last-minute attorney </b>
          will have to review the work, arrange for any evidentiary shortfalls
          to be filled, and ensure that they are ready to argue the case in
          court.
        </p>
        <p>
          If the pre-trial prep has not been completed by the previous legal
          team, the new representation will have to handle it. That will mean
          they need more time before going to trial.
        </p>
        <p>
          When a last-minute trial attorney joins forces with the existing team,
          it is expected that the preparation will be at an advanced stage. This
          scenario usually occurs when the current representation is good at
          preparing a case, but does not feel confident in achieving the best
          result possible at trial. Bringing a more experienced trial attorney
          onboard to assist can lead to a better result for the client.
        </p>
        <div id="header6"></div>
        <h2>Qualities of the Best Last-minute Trial Attorneys</h2>
        <p>
          The best last-minute trial lawyers excel in the courtroom. Quality
          courtroom representation is an art form – a great skill developed
          through experience. The best trial lawyers will:
        </p>
        <ul>
          <li>
            <b>Be prepared.</b> Planning and preparation are the keys to trial
            success. The best trial attorneys will know their case better than
            anyone else.
          </li>

          <li>
            <b>Respect the court.</b> Showing respect to the judge, jury and
            process is important and helps to project the right impression.
          </li>

          <li>
            <b>Communicate well.</b> It is important for a trial attorney to
            communicate with their client throughout a trial, as well as their
            legal support team. This is especially true when a last-minute trial
            lawyer has joined forces with another legal team which will have
            handled the majority of case preparation.
          </li>

          <li>
            <b>Build a rapport.</b> A good trial lawyer will have great people
            skills. They will build a rapport with the room and know how to
            speak to people, based on their past experience.
          </li>

          <li>
            <b>Tell a story.</b> The best trial lawyers are great storytellers.
            They know how to display a case in its best form, speaking clearly
            and concisely, while showing passion and authenticity.
          </li>
        </ul>
        <div id="header7"></div>
        <h2>Will Bisnar Chase Take Your Late-Notice Case?</h2>
        <LazyLoad>
          <img
            src="/images/trial-attorneys/late-stage-case-lawyers.jpg"
            width="40%"
            className="imgright-fluid"
            alt="late stage case lawyers"
          />
        </LazyLoad>
        <p>
          There is no guarantee that Bisnar Chase will be able to take your case
          on short notice as the trial approaches.
        </p>
        <p>
          Last-minute trial attorneys still need time to prepare. They also need
          to have the resources available to help. This means that the firm’s
          ability to take on a last-minute case is impacted by the other cases
          it has in progress at the time. A last-minute case gives no time for
          the firm to plan ahead.
        </p>
        <p>
          This is why it is most common for Bisnar Chase's{" "}
          <Link to="/orange-county" target="new">
            personal injury lawyers
          </Link>{" "}
          to join forces with a current legal team to help them in the trial
          phase of the case. In these situations, the leg work and preparation
          will already have been completed, and the Bisnar Chase trial lawyers
          can provide the courtroom expertise that the client and their legal
          team need.
        </p>
        <p>
          While Bisnar Chase cannot guarantee its services, the firm believes in
          helping people. If someone has a personal injury case due to the
          negligence of another party, we want to make sure they get the justice
          and compensation they deserve.
        </p>
        <p>
          If we believe we have the opportunity to provide the client with
          superior representation, we will always strive to do so.
        </p>

        <div id="header8"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Last minute trial attorneys"
            style={{
              backgroundImage:
                "url('/images/trial-attorneys/last-minute-trial-attorneys.jpg')"
            }}
          >
            <h2>
              Bisnar Chase Can Provide Outstanding Last-Minute Representation
            </h2>
          </div>
        </LazyLoad>
        <p>
          {" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          is made up of a team of exceptional California trial attorneys and
          staff. Through 40 years in business, our law firm has made a huge
          difference to the lives of our clients. We are proud of our{" "}
          <b>96% success rate</b> and have a strong track record in{" "}
          <Link to="https://www.courts.ca.gov/" target="new">
            {" "}
            courtrooms across California
          </Link>
          . We have also collected <b>$500 million</b> for our clients.
        </p>
        <p>
          We strive to provide superior client representation and offer a ‘No
          Win, No Fee’ guarantee. This ensures that our services are open and
          accessible to everyone.
        </p>
        <p>
          We know that monetary compensation is important, and give our clients
          the best chance possible of trial success. But we also put a focus on
          the experience and personal touch that we offer. The Bisnar Chase team
          will do everything possible to make the legal process smooth and
          stress-free for their clients.
        </p>
        <p>
          Those in need of a last-minute trial attorney in California can
          contact Bisnar Chase now. Our lawyers have the skills necessary to
          step in at short notice and achieve excellent results for their
          clients. Call <Link to="tel:+1-800-561-4887">(800) 561-4887 </Link>{" "}
          for a free consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
