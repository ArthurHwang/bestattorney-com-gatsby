// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California ATV Accidents Lawyer - All Terrain Vehicle Crash Attorney"
        pageDescription="Call 800-561-4887 for California ATV accident lawyer representation. No win, no fee all terrain vehicle crash attorneys for justice."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California ATV Accidents Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          Injured victims can seek compensation for damages including medical
          expenses, lost wages, hospitalization, surgeries, rehabilitation,
          therapy and pain and suffering. A personal injury claim may be filed
          against an at-fault party such as the driver of another vehicle. In
          cases where a defective ATV caused or contributed to the accident and
          injuries, the manufacturer of the ATV can be held accountable.
        </p>
        <p>
          Our ATV accident lawyers have been helping win cases since 1978.
          Bisnar Chase has recovered over $500 Million and we take on some of
          the most complex cases. You may be entitled to compensation so please
          contact our <Link to="/">top rated personal injury attorneys </Link>{" "}
          at 800-561-4887 for a free and confidential consultation.
        </p>
        <p>
          <img
            src="/images/atv-accidents.jpg"
            className="imgright-fixed"
            alt="atv accidents"
          />
          All Terrain Vehicles (ATVs) are fast and fun to ride, but they can
          also be unstable and dangerous. ATVs are commonly involved in rollover
          accidents due to the way they are designed and because of the fact
          that they are usually driven on rugged or uneven terrain. ATV
          accidents may occur as a result of driver error, product defect or
          faulty vehicle design.
        </p>
        <p>
          Regardless of how or why these accidents occur, they have the
          potential to cause catastrophic or even fatal injuries. If you or a
          loved one has been seriously injured in an ATV accident, the
          experienced California ATV accident lawyers at Bisnar Chase can help
          you better understand your legal rights and options. We are committed
          to helping our clients achieve the best possible results.
        </p>
        <h2>ATV Accident Statistics</h2>
        <p>
          Thousands of people are injured in ATV accidents each year. According
          to the U.S. Consumer Product Safety Commission (CPSC), 726 people died
          and 115,000 were injured as a result of ATV accidents nationwide in
          the year 2010. A number of these injuries and fatalities involve
          children and teens. In the year 2011, 57 deaths and 29,000 injuries
          caused by ATVs involved children under 16 years of age. Approximately
          17 percent of all fatalities and 27 percent of all injuries caused by
          ATV accidents involve children under the age of 16.
        </p>
        <h2>Types of ATV Accidents</h2>
        <p>
          Two of the most devastating and common types of ATV accidents involve
          collisions and rollovers. Both of these types of accidents can result
          from design flaws or mechanical malfunctions. For example, if an ATV
          has defective brakes or steering, it is more likely that a collision
          will occur. Other ATVs are inherently dangerous because of the manner
          in which they are designed.
        </p>
        <h2>If You Have Been Involved in an ATV Accident</h2>
        <p>
          The steps you take immediately following the crash can affect your
          ability to pursue financial compensation for your considerable losses.
          Here are a few steps to take after being involved in an ATV accident:
        </p>
        <p>
          Seek medical attention right away. ATV accidents often result in
          devastating injuries that require immediate treatment. Even if you are
          only sore, it is still important to see a doctor to make sure that you
          are not injured.
        </p>
        <p>
          Gather information about the crash. It will be helpful to have the
          name, number and the address of anyone who witnessed the accident. You
          should also write down the precise location of the crash, information
          about the ATV and insurance information of everyone involved.
        </p>
        <p>
          Call the authorities. It is important to report any accident that
          resulted in an injury even if the only vehicle involved is an ATV.
        </p>
        <p>
          Preserve the ATV. You may feel inclined to repair your vehicle, but it
          is important that you keep it in its damaged state for a thorough
          inspection by a qualified expert, who will look for any defects or
          malfunction.
        </p>
        <p>
          Do not talk to an insurance provider. Everything you tell an insurance
          adjuster or investigator can hurt your personal injury claim. Before
          you speak to an insurance provider or sign any documents, it is
          advisable to call an experienced California personal injury lawyer.
        </p>
        <p>
          Consult with an ATV accident attorney. A California personal injury
          lawyer can make sure that your rights are protected throughout the
          legal proceedings. It can be challenging to deal with insurance
          companies and ATV manufacturers. Your lawyer can make your life easier
          by negotiating on your behalf. A knowledgeable California ATV accident
          lawyer can help protect your legal rights and options every step of
          the way.
        </p>
        <h2>Preventing ATV Accidents</h2>
        <p>
          Before even buying an ATV, it is important to do your research. It is
          best to avoid certain models that have been recalled numerous times or
          to refrain from purchasing a model that has been involved in a number
          of crashes. If you have already purchased an ATV, it may be in your
          best interests to stay abreast of ATV part recalls.
        </p>
        <p>
          Get your ATV regularly looked at by a qualified mechanic. Any broken,
          worn down or improperly installed part can cause you to lose control.
          Make sure your vehicle is in proper working order before riding it
          anywhere.
        </p>
        <p>
          It is also important to receive proper training. These vehicles may
          seem fun and harmless, but they can be extremely dangerous.
          Inexperienced, untrained drivers have an increased risk of crashing.
          Ride at speeds that you feel comfortable with, avoid fast turns on
          hills, travel slowly on trails and paths that you are unfamiliar with
          and never ride while intoxicated.
        </p>
        <p>
          Do not let young children handle ATVs. Having additional passengers on
          an ATV can also cause the vehicle to become unstable and roll over. If
          you would not allow your child to drive a car, you should not allow
          him to operate an ATV that could reach speeds of 80 to 100 mph.
          Wearing proper safety gear such as a helmet is critical to prevent
          major injuries.{" "}
          <Link to="/blog/atvs-child-injuries">
            ATV accidents involving children
          </Link>{" "}
          are usually accompanied by serious injuries to their small bones.
        </p>
        <ul>
          <li>Injuries and Damages</li>
          <li>ATV accidents can result in major injuries including:</li>
          <li>Brain injuries including concussions</li>
          <li>Spinal cord damage</li>
          <li>Broken bones</li>
          <li>Lacerations</li>
          <li>Internal injuries</li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
