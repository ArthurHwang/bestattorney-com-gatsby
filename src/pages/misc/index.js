// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Misc. Personal Injuries - Bisnar Chase"
        pageDescription="Bisnar Chase represents people injured in a variety of different accidents throughout California. We take on serious to catastrophic injury cases including car accidents, truck rollovers, serious dog bite cases & auto defects. Call for a Free consultation at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Misc. Injuries</h1>
        <BreadCrumbs location={location} />

        <p>
          Bisnar Chase Personal Injury Attorneys represent people injured in a
          variety of accidents. From swimming pool injuries or death to an
          aviation accident, our trial lawyers have the experience and knowledge
          to represent some of the most difficult cases.
        </p>

        <ul>
          <li>
            {" "}
            <Link to="/misc/train-accidents">Train Accidents </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/telephone-consumer-protection-act">
              Telephone Consumer Protection Act Violations{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/last-minute-trial-attorney">
              Last-minute Trial Lawyer{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/cruise-ship-injury-lawyer">
              Cruise Ship Injury Lawyer{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/child-injury-lawyer">Child Injury Lawyer </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/cheerleader-injury-lawyer">
              Cheerleader Injury Lawyer{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/aviation-accident-lawyers">
              Aviation Accident Lawyer{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/misc/atv-accident-lawyers">ATV Accident Lawyer </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
