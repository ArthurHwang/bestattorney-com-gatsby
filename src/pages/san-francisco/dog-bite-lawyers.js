// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-dog-bite-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Dog Bite Lawyer - Dog Attack Attorney"
        pageDescription="Dog bites can be very traumatic & life changing, not to mention deadly. Serious injuries, life long deformations & permanent disabilities happen every day from animal attacks. Our San Francisco Dog Bite Attorneys are here to fight & win for you. Contact our skilled lawyers for your Free consultation at 415-358-0723."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san francisco dog bite attorney"
            title="san francisco dog bite attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> San Francisco Dog Bite Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> have been representing dog bite victims
          and winning their cases for over <strong> 40 years</strong>. From
          infants to toddlers, children, teens, young adults, adults to the
          elderly, dogs do not discriminate.
        </p>
        <p>
          Our law firm has a high level of skill when it comes to dog bites,
          because of our dedication and passion to helping injured victims. Dog
          bites can be especially traumatizing, as well as causing severe and
          catastrophic injuries, resulting in permanent disabilities,
          disfigurements and serious health problems.
        </p>
        <p>
          First and foremost, your best way to go about experiencing a dog bite
          or dog attack is to seek medical attention immediately, having your
          injuries documented immediately and contacting a skilled and
          experienced{" "}
          <Link to="/san-francisco" target="new">
            San Francisco Personal Injury Attorney
          </Link>
          . Bisnar Chase offers a Free consultation at
          <strong> 415-358-0723</strong>.
        </p>
        <p>This page will cover:</p>
        <ul>
          <li>The Importance of Hiring a Dog Bite Attorney</li>
          <li>Top 10 States for Dog Bite Claims</li>
          <li>Dog Bite Statistics</li>
          <li>Dogs With and Without a History of Aggressive Behavior</li>
          <li>Top 10 Most Dangerous Dog Breeds</li>
          <li>California Dog Bite Laws</li>
          <li>Why You Must Seek Medical Attention Immediately</li>
          <li>Documenting Your Injuries in Affiliation to the Dog Bite</li>
          <li>How to Avoid Future Dog Attack Situations</li>
          <li>What To Do in the Event of a Dog Attack</li>
          <li>PTSD and Affects Dog Bites Have on Victims</li>
          <li>Details on Our Contingency-Based Law Firm</li>
        </ul>

        <h2>The Importance of Hiring a Dog Bite Attorney</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/handling-your-own-dog-bite-case.jpg"
            width="100%"
            alt="san francisco dog bite lawyer"
          />
        </LazyLoad>
        <p>
          Handling your own case seems possible and often the only choice due to
          financial issues, but are often overwhelming, confusing and many times
          impossible to do so in complex situations and difficult cases.
        </p>
        <p>
          Many people seem to think that dog bites are not serious unless a
          severe injury is inflicted, but this is not always true. As dog bites
          can leave a lasting impression, so can your following actions, from a
          legal perspective.
        </p>
        <p>
          Everything you do, from before, during and immediately after the dog
          attack occurred, is crucial and directly affiliated with the incident
          and your case.
        </p>
        <p>
          Thinking that you are capable of taking on your case as your own
          personal legal representative throughout the litigation process is
          ambitious and shows confidence.
        </p>
        <p>
          But this can lead to much stress, higher fees in other areas, case
          settlement taking much longer than efficiently necessary, and the high
          potential of you losing your case, due to lack of legal experience.
        </p>
        <p>
          Our dog bite attorneys have been representing and winning cases for
          maximum compensation for over 40 years, and understand what it takes
          to win your case in the shortest time possible, and for as much
          compensation return to the client as possible.
        </p>
        <p>
          Not only dog bite cases, but the majority of all legal cases are
          fought back by aggressive defense attorneys and legal battles can
          become very demanding.
        </p>
        <p>
          That's why <Link to="/">Bisnar Chase</Link> dog bite injury lawyers
          are here, and that is to fight back with the most aggression,
          sophistication and professionalism, in order to be the "top dog" and
          win your case, hands down, for maximum compensation, or{" "}
          <strong> you don't pay</strong>.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco dog bite attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dog-walkers-san-francisco-dog-bite-lawyers.jpg')"
            }}
          >
            <h2>Top 10 States for Dog Bite Claims</h2>
          </div>
        </LazyLoad>

        <p>
          Dog bites occur every single day, in cities all across America. But
          due to environment, weather, population and transportation, some
          states have high dog attack rates than others. According to{" "}
          <Link
            to="http://www.propertycasualty360.com/2017/04/07/top-10-states-for-dog-bite-claims-in-2016"
            target="new"
          >
            PropertyCasualty360.com
          </Link>
          , here is a list of the
          <strong> Top 10 States for Dog Bite Claims</strong>:
        </p>
        <ol reversed>
          <li>
            <strong> Georgia</strong>: With over 460 claims, Georgia starts our
            countdown of Top 10 states for dog bites. Claims averaged just over
            $29,000 each, coming in at over $13.6 million paid out.
          </li>
          <li>
            <strong> New Jersey</strong>: Over 530 claims, averaging over
            $53,000 each and almost $29 million paid out.
          </li>
          <li>
            <strong> Michigan</strong>: Over 780 claims, averaging almost
            $28,000 each and nearly $22 million paid out.
          </li>
          <li>
            <strong> Ohio</strong>: 850 claims, averaging over $34,000 each,
            paying out over $29 million.
          </li>
          <li>
            <strong> Illinois</strong>: 910 claims, averaging nearly $43,000
            each and paying out $39 million.
          </li>
          <li>
            <strong> Texas</strong>: Over 920 claims, almost $22,000 claims and
            over $20 million paid out.
          </li>
          <li>
            <strong> Pennsylvania</strong>: 988 claims, averaging nearly $25,000
            and over $24.6 million paid out.
          </li>
          <li>
            <strong> New York</strong>: Well over 1,000 claims, averaging over
            $55,500 each and paying out over $58 million.
          </li>
          <li>
            <strong> Florida</strong>: Over 1,300, each averaging about $37,000
            and paying out over $49.5 million.
          </li>
          <li>
            <strong> California</strong>: Coming in number one, are the dog
            lovers of California. With over 1,930 claims, averaging over
            $39,400, California dog bite settlement pay outs reached over $76.3
            million.
          </li>
        </ol>

        <h2>Dog Bite Statistics</h2>
        <p>
          It can be alarming, that California has the highest volume of dog
          bites in the entire country. With the dense population of the Bay
          area, high volume of foot traffic and congested areas, dog bites are
          going to be inevitable as long as dogs are present, negligence does
          not have to be a factor.
        </p>
        <ul>
          <li>In recent years, there were 35 dog bite fatalities</li>
          <li>1 out of 5 dog bites will get infected</li>
          <li>
            Untreated animal bites can lead to muscle, bone and nerve damage
          </li>
          <li>
            MRSA, pasteurella, tetanus and rabies can be transmitted by dog
            bites
          </li>
          <li>Out of almost 5 million dog bite victims every year</li>
          <li>850 thousand of these incidents need medical attention</li>
          <li>10-20% of dig bite victims</li>
          <li>Children make up the majority of that percentage</li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/group-of-dogs-with-or-without-aggresive-history.jpg"
            width="100%"
            alt="san francisco dog bite injuries"
          />
        </LazyLoad>

        <h3>Dogs with or without an aggressive history</h3>
        <p>
          People like to stereotype breeds and types of dogs into vicious
          monsters, while passing a couple down the street with a growling
          chihuahua who say, "don't worry, it's all bark, our precious Fiona
          wouldn't hurt you..."
        </p>
        <p>
          It's not what we say here at Bisnar Chase as dog bite attorneys and
          animal attack lawyers, it is reality. Dogs, cats and animals of every
          kind have the potential of becoming aggressive at any moment, even if
          that animal has had no prior history of hostile or violent behavior in
          the past.
        </p>
        <p>
          There are many reasons a dog with no prior aggressive past can become
          vicious:
        </p>
        <ul>
          <li>Feeling threatened or suddenly in danger</li>
          <li>Suffering from an illness, disease, infection or injury</li>
          <li>On a heat cycle</li>
          <li>Protecting their young</li>
          <li>Abused animals showing dominance to avoid future neglect</li>
          <li>A puppy or young animal turning into an adult</li>
          <li>Teething</li>
          <li>Environment</li>
          <li>Genes</li>
        </ul>
        <p>
          Although the last bullet is usually a very controversial topic, it is
          true regarding certain aspects. Genes are strong characteristics
          living things have no control over having. Defined by U.S. National
          Library of Medicine, "
          <em>
            A gene is the basic physical and functional unit of heredity. Genes,
            which are made up of DNA, act as instructions to make molecules
            called proteins.
          </em>
          " You can learn more about genes and the{" "}
          <Link
            to="https://ghr.nlm.nih.gov/primer/hgp/description"
            target="new"
          >
            Human Genome Project
          </Link>{" "}
          at{" "}
          <Link to="https://ghr.nlm.nih.gov/primer/basics/gene" target="new">
            U.S. National Library of Medicine
          </Link>{" "}
          for more info.
        </p>
        <h2>
          <strong> Top 10 Most Dangerous Dog Breeds</strong>
        </h2>
        <p>
          Small dogs, medium dogs, large dogs and huge dogs; it doesn't matter
          how big the bark, it's all about the bite. Dangerous dog breeds is a
          very controversial topic, and can get heated quickly, due to the
          passion and dedication dog owners have towards their beloved pup.
        </p>
        <p>
          But the fact of the matter, all dogs, regardless or their nurturing
          upbringing, from a scientific and logical approach, do in fact carry
          specific traits, genes and characteristics in their DNA, in which they
          are unable to control.
        </p>
        <p>
          Some of these genes, like the need for excess exercise and running due
          to an intense hyperactive amount of energy, like cattle dogs and
          herders, or the need to fetch and retrieve, like the family favorite
          Golden Retrievers and Labradors have.
        </p>
        <p>
          But within nature holds the DNA of warriors, masters of the hunt,
          offense, and need the to be the alpha, dominator, and powerhouse of
          the family. In these breeds carry some of the most powerful, fickle,
          hard to read, and potentially deadly characteristics of all the dog
          breeds, making them,{" "}
          <strong> The Top 10 Most Dangerous Dog Breeds</strong>:
        </p>
        <ol>
          <li>
            <strong> Caucasian Shepherd</strong>
          </li>
          <li>
            <strong> Pit Bull</strong>
          </li>
          <li>
            <strong> Perro de Presa Canario (A.K.A  Canary Mastiff)</strong>
          </li>
          <li>
            <strong> Rottweilers</strong>
          </li>
          <li>
            <strong> Fila Brasileiro (A.K.A. Brazilian Mastiff)</strong>
          </li>
          <li>
            <strong> Alaskan Malamute</strong>
          </li>
          <li>
            <strong> Wolfdog (Domestic & Wolf hybrid)</strong>
          </li>
          <li>
            <strong> Bullmastiff</strong>
          </li>
          <li>
            <strong> Husky</strong>
          </li>
          <li>
            <strong> Doberman Pinschers</strong>
          </li>
        </ol>

        {/* <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/5omi6LPh6dg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad> */}

        <h2>California Dog Bite Laws</h2>
        <p>
          Many people wonder if the event they experienced was harmful enough to
          sue or press legal charges. Whether the chihuahua next door who nipped
          their ankles, Buddy the lab who playfully bit the neighbors are a
          little too rough while playing fetch (non-aggressive accidental bite),
          to the vicious pitbull or Rottweilers who got out of their harness or
          gated area and mauled someone.
        </p>
        <p>
          A dog biting someones arm and not breaking the skin is still enough
          for the victim to press charges. While California has a strict
          liability dog bite statute and is applied to the animal's history,
          other laws make it possible for taking appropriate steps to reducing
          or depleting the possibility of that animal harming someone ever
          again.
        </p>
        <p>
          When a dog has bitten a human two separate times, there is protocol
          that is taken to ensure it does not happen again. To dogs that have
          been trained to attack, fight and kill that have harmed humans on
          multiple occasions, the animal's destruction is a possible result, if
          confinement is not a solution. (
          <Link to="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=3342.5">
            Cal. Civil Code § 3342.5
          </Link>
          ).
        </p>
        <p>
          In San Francisco you must be able to provide registration and
          vaccinations if your dog is 4 months or older and when in public, your
          dog must be on a leash, and the leash must be in your hand, not tied
          to a post, belt loop or other structure; in your hand.
        </p>
        <p>
          The law has some limits, and can get very complex, but for the most
          part, the dog owner is liable if the injured person was bitten and in
          a public place, or legally in a private place at the time of the
          accident.
        </p>
        <p>
          To learn more about California dog bites you can visit and read
          through the{" "}
          <Link
            to="http://www.theanimalcouncil.com/files/CALIFORNIA_CIVIL_DAMAGES.pdf"
            target="new"
          >
            California Civil Code - Damages PDF
          </Link>
          .
        </p>
        <h2>Why You Must Seek Medical Attention Immediately</h2>
        <p>
          Health and safety should be everyone's first priority when face to
          face with a dog bite or dog attack situation. Dog and other animal
          bites can become infected and cause serious and often fatal injuries
          if not properly treated right away.
        </p>
        <h3>
          Why is it important to seek medical attention immediately following a
          dog bite or attack?
        </h3>
        <p>
          There are other reasons to seeking medical attention right away.
          Usually in the case of a dog bite, when someone seeks medical
          attention, the injuries are documented and properly treated. The major
          benefit of having your injuries documented will help you and your case
          as it gets deeper and deeper into the litigation process.
        </p>
        <h2>Documenting Your Injuries in Affiliation to the Dog Bite</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/dog-bite-injury-san-francisco.jpg"
            width="100%"
            alt="san francisco dog attack lawyer"
          />
        </LazyLoad>
        <p>
          From time to time, there are "fraudulent victims" just looking for
          compensation. Fake slip and falls, saying someone's dog bit them when
          they didn't, and so on. This is why it is important to have your
          injuries documented in affiliation to the dog bite or attack, to prove
          that the injuries did occur as the direct result of the incident.
        </p>
        <p>
          When someone moves forward with a dog bite case, who has "injuries
          that were caused by a dog bite," it is difficult to prove that the
          injuries were in fact the result of the actual dog bite the case is
          actually about. If there is no medical documentation of your injuries
          happing from the situation, often times it can make or break the case.
        </p>
        <h2>How to Avoid Future Dog Attack Situations</h2>
        <p>
          If you have already experienced a dog bite or dog attack, you should
          consult with one of our skilled dog bite attorneys who have over 40
          years experience in representing and winning dog attack cases,
          otherwise, here are some steps you can take to better avoid dangerous
          situation events with dogs and other animals that could attack.
        </p>
        <p>
          First, remember that common sense and safety must be your top
          priority.
        </p>
        <h3>Avoiding Provoked Attacks</h3>
        <ul>
          <li>
            Never approach an unfamiliar animal, even if restrained to a leash,
            gated yard or indoors
          </li>
          <li>Always slowly let a dog or animal sniff your hand</li>
          <li>Never antagonize a restrained or loose animal</li>
          <li>
            Do not run from, chase or intimidate a restrained or loose animal
          </li>
          <li>
            Don't irritate, poke at or bother an animal while they are eating,
            sleeping or going to the bathroom
          </li>
          <li>
            Never pressure, bother or mess with an animal and their babies
          </li>
          <li>Never put your face near another animals face</li>
          <li>
            Don't waste your time with a growling or obviously irritated animal
          </li>
        </ul>
        <h3>Staying Safe During an Attack</h3>
        <p>
          Many times, dog bites and animal attacks are unprovoked, meaning you
          did nothing to issue the engagement of the attack. This could be due
          to an animal that is already angered, in self defense mode, scared,
          traumatized, confused as in misidentifying you for prey or a threat,
          deranged and suffering from some sort of illness or disease.
        </p>
        <p>
          If you find your self in a situation where a dog is chasing you or
          about to charge, here are some tips you can use to better the
          situation and possibly save your life and limbs, even loved ones,
          friends or strangers near you.
        </p>
        <ul>
          <li>Don't panic</li>
          <li>Don't run</li>
          <li>Try and look and sound bigger and louder than the dog</li>
          <li>
            Look for something to jump onto in which you can keep the dog or
            animal off of
          </li>
          <li>
            Save younger children and women; they could be more prone to
            becoming more severely injured or killed
          </li>
          <li>
            If possible, run inside, jump into a car, or over a fence in which
            there are no other dogs, and make sure the door is secure
          </li>
        </ul>
        <h3>In the Event of Being Attacked</h3>
        <p>
          If you were not able to run or find safe shelter and are now
          experience physical confrontation with the dog or animal, it is
          important to keep vital parts of your body covered, protected and safe
          such as:
        </p>
        <ul>
          <li>Neck</li>
          <li>Face</li>
          <li>Eyes</li>
          <li>Wrists</li>
          <li>Areas prone to extreme arterial bleeding</li>
        </ul>
        <p>
          For more information on how to avoid dog bites and other statistics,
          visit{" "}
          <Link
            to="https://www.caninejournal.com/dog-bite-statistics/"
            target="new"
          >
            CanineJournal.com
          </Link>
          .
        </p>
        <p>
          If you or a loved one has experienced a dog attack, call our{" "}
          <strong> San Francisco Dog Bite Lawyers </strong>for your Free
          consultation, and see if your situation qualifies.
        </p>
        <p>
          <strong> Bisnar Chase </strong>has signed and agreed to many cases{" "}
          <strong> other law firms have denied</strong>, because at Bisnar
          Chase, our personal injury attorneys have the skill, experience,
          resources and ability to take on complex cases, represent our injured
          victims and fight until the end, winning maximum compensation, or{" "}
          <strong> you do not pay</strong>.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco dog attack attorney"
            style={{
              backgroundImage:
                "url('/images/text-header-images/ptsd-dog-bite-san-francisco.jpg')"
            }}
          >
            <h2>PTSD and Affects Dog Bites Have on Victims</h2>
          </div>
        </LazyLoad>

        <p>
          Dog bites and animal attacks are very traumatizing and can often have
          long lasting if not permanent damage on the victim's emotions and
          metal health, not to mention severe and permanently disabling
          injuries.
        </p>
        <p>
          <strong> PTSD</strong>, or post traumatic stress disorder is common
          after a victim experiences a dog bite or attack. The event can shake a
          person emotionally and mentally so violently, that they can re-live,
          consistently visualize, suffer from nightmares and the constant fear
          and anxiety of it reoccurring.
        </p>
        <p>Symptoms may include and are not limited to:</p>
        <ul>
          <li>Insomnia</li>
          <li>Change in appetite</li>
          <li>Behavior problems</li>
          <li>Increased anger or depression</li>
          <li>High levels of anxiety and stress</li>
        </ul>
        <p>
          This amount of stress can lead to weight loss, severe anxiety
          problems, which can lead to physical medical conditions. Fear of
          living life day to day as normal, going outside and spending time or
          going places with friends, families and loved ones can become
          overwhelming and take over the victim's life.
        </p>
        <p>
          If you have experienced a traumatic dog bite or animal attack, call
          our dedicated and passionate San Francisco dog bite lawyers who can
          help determine the best route for you to take, a Free consultation and
          even help find solutions such as therapy, counseling and activities to
          get your life back where it used to be.
        </p>
        <p>
          To learn more about PTSD after a dog attack, visit{" "}
          <Link
            to="http://dogattackinjury.com/dog-bite-injuries/ptsd/"
            target="new"
          >
            DogAttackInjury.com
          </Link>
          .
        </p>
        <h2>You Don't Pay If We Don't Win</h2>
        <p>
          Our law firm has been representing clients of personal injuries, car
          accidents and many other situations in which a person or group of
          people were injured, ranging from minor injuries, to severe injuries,
          permanent disabilities and wrongful deaths. For over 40 years we have
          established a 96% success rate because of our legal team's dedication,
          passion for helping our clients, and making you and your case our top
          priority.
        </p>
        <p>
          Bisnar Chase is a <strong> contingency-based law </strong>firm, which
          means <strong> we will advance costs throughout your case</strong>,
          provide superior client representation, and if we end up not winning
          your case you do not pay anything. To learn more about contingency
          fees and how they work, visit{" "}
          <Link
            to="https://www.americanbar.org/groups/public_education/resources/law_issues_for_consumers/lawyerfees_contingent.html"
            target="new"
          >
            AmericanBar.org
          </Link>
          .
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco car accident law firm attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-san-francisco.jpg')"
            }}
          >
            <h2>Bisnar Chase Will Fight For You</h2>
          </div>
        </LazyLoad>

        <p>
          Dog bites are very serious and should have the best legal
          representation. <strong> San Francisco Dog Bite Attorneys </strong>of{" "}
          <strong> Bisnar Chase </strong>have been representing and winning
          these cases for over <strong> 40 years </strong>and have established a
          recognized and impressive <strong> 96% success rate</strong>.
        </p>
        <p>
          Our lawyers, paralegals, negotiators, litigation team and other staff
          members have won over <strong> $500 Million </strong>for our clients
          and have changed the lives of so many. Our law firm wants to help
          injured victims get their lives back, receive maximum compensation and
          all while having the support and care of our staff here at{" "}
          <strong> Bisnar Chase</strong>.
        </p>
        <p>
          For a <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation Call 415-358-0723</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
