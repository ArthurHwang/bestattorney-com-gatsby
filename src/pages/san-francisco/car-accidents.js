// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-car-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Car Accident Lawyer - Bisnar Chase"
        pageDescription="San Francisco Car Accident Lawyers with over 40 years experience & over $500 Million won will take complex cases. Call for a Free Consultation at 415-358-0723."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san francisco car accident lawyers"
            title="san francisco car accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> San Francisco Car Accident Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong>are here to fight and win for you. Our
          law firm has over <strong> 39 years of experience </strong>and has won
          over <strong> $500 Million </strong>for our clients. Become one of our
          successes and we guarantee maximum compensation.
        </p>
        <p>
          Obtaining the wise counsel of a skilled and experienced{" "}
          <strong>
            {" "}
            <Link to="/san-francisco" target="new">
              San Francisco Personal Injury Lawyers
            </Link>
          </strong>{" "}
          beneficial and can dramatically increase your chances of receiving
          much more compensation and in a shorter amount of time.
        </p>
        <p>
          Car accidents can be very traumatic experiences. They can be
          financially devastating, emotionally draining and a mental frenzie.
          Between dealing with your own issues, negotiating with insurance
          adjusters, overwhelming legal information and every other aspect
          included, it can many times be a painful and chaotic time.
        </p>
        <p>
          The high possibility of not understanding all of the legal terminology
          and processes can be a failing factor towards being compensated and
          winning your settlement or case.
        </p>
        <p>
          If you have been injured or have experienced a car accident, once you
          have sought medical attention, contact a law firm that knows how to
          win. Choose a car accident firm that cares about their clients and
          always puts them first and their number one priority.
        </p>
        <p>
          <strong> Call</strong> successful, reputable and trusted{" "}
          <strong> San Francisco Car Accident Lawyers </strong>at{" "}
          <strong> Bisnar Chase</strong> at <strong> 415-358-0723</strong>.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco car accident attorneys top 20"
            style={{
              backgroundImage:
                "url('/images/text-header-images/car-accident-san-francisco-attorneys-top-20-causes.jpg')"
            }}
          >
            <h2>Top 20 Most Common Causes of Car Accidents in San Francisco</h2>
          </div>
        </LazyLoad>

        <p>
          San Francisco is know for a number of things, like the Golden Gate
          Bridge, Alcatraz and it's culturally diverse population, food and art
          scene, but it is also known for its traffic and fender-bender-prone
          downtown and suburb streets.
        </p>
        <p>
          Here is a list of the{" "}
          <strong>
            {" "}
            Top 20 Most Common Causes of Car Accidents in San Francisco
          </strong>
          :
        </p>
        <ol>
          <li>Night Driving</li>
          <li>Teen Drivers</li>
          <li>Fog</li>
          <li>Pot Holes</li>
          <li>Tire Blowouts</li>
          <li>Road Rage</li>
          <li>Deadly Curves</li>
          <li>Drowsy Driving</li>
          <li>Wrong-way driving</li>
          <li>Snow/Ice/Rain</li>
          <li>Improper Turns</li>
          <li>Tailgating</li>
          <li>Unsafe Lane Changes</li>
          <li>Auto Defects</li>
          <li>Running Red Lights and Stop Signs</li>
          <li>Animal Crossings</li>
          <li>Reckless Driving/Street Racing</li>
          <li>Drunk Driving (Drugs/Intoxicated Driving)</li>
          <li>Speeding</li>
          <li>Distracted Driving</li>
        </ol>

        <h2>Car Accidents Statistics in San Francisco</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/car-accident-statistics-san-francisco.jpg"
            className=""
            width="100%"
            alt="san francisco car accident statistics lawyers"
          />
        </LazyLoad>
        <p>
          With San Francisco's high population in a dense city environment, lots
          of cars and consistent foot and vehicle travel, accidents are
          inevitable.
        </p>

        <p>
          In a recent years study, the{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="new"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System (SWITRS)
          </Link>
          indicated the following:
        </p>

        <ul>
          <li>31 people were killed</li>
          <li>3,740 were injured in San Francisco car crashes</li>
          <li>16 pedestrians lost their lives</li>
          <li>942 were injured in car collisions</li>
          <li>
            Car accidents also took the life of 2 bicyclists and injured 658
          </li>
          <li>Motorcycle accidents killed nine and injured 398</li>
          <li>
            {" "}
            <Link to="/dui" target="new">
              Impaired-driver car crashes
            </Link>{" "}
            resulted in 12 fatalities and 293 injuries
          </li>
        </ul>
        <h2>Insurance Companies Are Not Your Friend</h2>
        <p>
          When you are in a car accident and need help from insurance companies,
          you may find that everything you thought was once promised, wasn't
          actually available to you.
        </p>
        <p>
          Insurance companies are in this business for a reason; to make money.
          Sure, insurance is great to have and they can do great things, but
          when it comes to you receiving max compensation, they will do
          everything in their power to make sure they save as much money as
          possible.
        </p>
        <p>
          If you have already contacted your insurance provider and have noticed
          suspicious, malicious, negligent or wrongdoing, bring this up to your
          car accident attorney immediately.
        </p>
        <p>
          To learn more about fraud, scams and insurance negligence visit the{" "}
          <Link
            to="http://www.insurancefraud.org/scam-alerts-agent-and-insurer.htm"
            target="new"
          >
            Coalition Against Insurance Fraud
          </Link>
          .
        </p>
        <p>
          Once you have properly sought medical attention, contact an
          experienced car accident attorney immediately.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/aGdMrmlKJNE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Medical Attention and Documentation</h2>
        <p>
          It is common sense to seek medical attention immediately following a
          car accident or personal injury. Some people do not, and this is a bad
          decision for a few reasons.
        </p>
        <p>
          When you experience a car accident or personal injury, seeking medical
          attention has the following benefits for you and your case:
        </p>
        <ul>
          <li>
            Increases your chances of avoiding long-term injuries and
            disabilities
          </li>
          <li>Lowers your chances of infection and disease</li>
          <li>Documents the injuries</li>
          <li>Associates your injuries with the accident</li>
        </ul>
        <p>
          The ability to prove your injuries were in result of the accident is
          crucial, and a wining or losing factor. Seeking medical attention
          immediately is the best way of doing this.
        </p>
        <p>Proper medical attention consists of:</p>
        <ul>
          <li>Ambulance/Emergency First Responders</li>
          <li>Emergency Room/Hospital</li>
          <li>Urgent Care</li>
        </ul>
        <p>
          Ensure that where you seek medical attention are licensed and
          currently practicing. Having a friend, family member or loved one is
          better than no medical attention, but we suggest that anyone who has
          experienced a personal injury or some type of accident should seek
          professional care.
        </p>
        <p>
          Professional medical care is also the most legitimate form of
          documentation you can use towards winning your case, settlement and or
          compensation.
        </p>
        <h2>Don't Connect Past Injuries with Current Car Accident</h2>
        <p>
          If you get into a car accident, and subsequently seek compensation for
          your medical losses, knowledgeable San Francisco car accident lawyers
          advise not to exaggerate your losses.
        </p>
        <p>
          If you already suffer from medical conditions before your accident
          occurred, never connect the symptoms of any existing medical problems
          with your car accident. Doing so will only cause the claims adjuster
          or defense attorney to view your claim as exaggerated and disbelieve
          anything you say about your injuries and treatment.
        </p>
        <p>
          In most instances, such a case will have to go to trial, and not the
          settlement table, before you have the opportunity to recovery that you
          believe is "fair."
        </p>
        <p>
          The best San Francisco car collision lawyers will tell you that these
          complications can be avoided by simply keeping an accurate written
          record of the medical losses that relate directly to your car
          accident.
        </p>
        <p>
          Your credibility will be enhanced at settlement time, resulting in
          much higher dividends.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco car accident law firm attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-san-francisco.jpg')"
            }}
          >
            <h2>Let Bisnar Chase Represent You and Win</h2>
          </div>
        </LazyLoad>

        <p>
          Our skilled and highly experienced{" "}
          <strong> San Francisco Car Accident Lawyers </strong>have won over{" "}
          <strong> $500 Million </strong>for our clients and have established an
          impressive
          <strong> 96% Success Rate</strong>.
        </p>
        <p>
          With over <strong> 39 years of experience</strong>, the attorneys,
          paralegals, legal advisors, mediators and other staff at the law
          offices of <strong> Bisnar Chase </strong>have a solid and reputable
          name in the legal world.
        </p>
        <p>
          We are trusted and feared by many, and strive to find justice in all.
          We will fight for you regardless of the uphill battles, until we win
          maximum compensation for you, or you do not pay anything.
        </p>
        <p>
          If you have been injured in or have experienced a car accident, don't
          wait for statute of limitations, and call us today.
        </p>
        <p>
          For a <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation Call 415-358-0723</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
