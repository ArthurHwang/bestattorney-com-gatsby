// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-pedestrian-accident-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Pedestrian Accident Lawyer - Bisnar Chase"
        pageDescription="Joggers, bicycle riders and tourists are constantly roaming the streets of the Bay Area, whether on their daily commute or site-seeing. The San Francisco Pedestrian Accident Lawyers will fight & win for you or you do not pay. Call our skilled injury attorneys for your Free consultation at 415-358-0723."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Pedestrian Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san francisco pedestrian accident lawyers"
            title="san francisco pedestrian accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong> San Francisco Pedestrian Accident Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> have been representing injured victims
          and winning cases for over <strong> 40 years</strong>.
        </p>
        <p>
          We often take for granted the safety of walking up and down streets,
          to go shopping, dropping our children off at school, or a date night
          with your loved one.
        </p>
        <p>
          But when the unexpected happens, it can prove fatal. Serious to
          catastrophic injuries resulting in permanent disabilities can happen
          in the blink of an eye, and if it does happen, Bisnar Chase is in the
          Top 1% of{" "}
          <Link to="/san-francisco" target="new">
            {" "}
            Personal Injury law firms
          </Link>{" "}
          in the Country with aggressive attorneys who will win your case for
          maximum compensation, or you do not pay.
        </p>
        <p>This page will cover the following:</p>
        <ul>
          <li>Top 10 Most Common Pedestrian Accident</li>
          <li>San Francisco Pedestrian Accident Statistics</li>
          <li>Top 5 Most Common Pedestrian Accident Injuries</li>
          <li>Why Is It Important To Hire a Personal Injury Attorney?</li>
          <li>Receiving Medical Treatment</li>
          <li>Documenting Injuries in Affiliation to the Accident</li>
          <li>If We Don't Win, You Don't Pay</li>
        </ul>
        <p>
          None of us think we will ever get into the situation of behind hit,
          ran over or injured by a vehicle while walking down the street or
          waiting at a crosswalk, but neither do the countless victims who are
          severely injured or killed while doing so.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco pedestrian accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/pedestrians-top-10-most-common-accident-san-francisco.jpg')"
            }}
          >
            <h2>Top 10 Most Common Pedestrian Accident</h2>
          </div>
        </LazyLoad>
        <p>
          There are an endless amount of ways people can become injured or
          killed by vehicles and other transportation. Here is a list of the{" "}
          <strong> Top 10 Most Common Pedestrian Accidents</strong>:
        </p>
        <ol>
          <li>
            <strong> Jaywalking</strong>: Crossing the street in an undesignated
            area can be not only dangerous, but fatal. Passing
            transportation/vehicles are not expecting to see a pedestrian in the
            street, other than designated crosswalks.
          </li>
          <li>
            <strong>
              {" "}
              Jogging or running at night (especially in dark clothing)
            </strong>
            : You are taking a risk whenever you step into the street. Even when
            you are in a jog or biking lane, a car could veer off by negligence,
            distractions or having to avoid a potential hazard and protecting
            themselves from danger, possibly hurting or killing you by accident.
            Wearing dark clothes makes it even more difficult to see you. If you
            are going to run or jog outside, it is advised that you wear bright
            and reflective clothing, rather than dark and dim clothing.
          </li>
          <li>
            <strong> Road Cycling</strong>: As stated above, stepping into the
            street, whether walking, jogging, skateboarding, cycling on a
            bicycle or bike or any other means of transportation is risky. Take
            extreme precautions when doing so.
          </li>
          <li>
            <strong> Stop sign and stop light runners</strong>: Not all drivers
            are safe drivers who follow the rules and laws of the road. Some
            people want to get places quicker, or could possibly themselves be
            in a hurry to the hospital. When attempting to cross the street at a
            designated crosswalk, look both ways to make sure there is no
            oncoming traffic, and ensure they come to a complete stop before
            walking out in front of their car.
          </li>
          <li>
            <strong> Intoxicated or Distracted Drivers</strong>: Driving under
            the influence of drugs, alcohol or other substances, such as
            prescription pain medication, can have catastrophic results. Also
            using a cell phone, changing the radio station or doing some sort of
            other act that distracts or diverts your attention from driving your
            vehicle or motorcycle can be the last thing you do being the driver,
            and could easily take out a group of small children crossing the
            street to school. Think twice before responding or reading that text
            message, and make sure all cars approaching you are fully aware, and
            even signal you to cross in front of them, once to a complete stop.
          </li>
          <li>
            <strong> Driver Error</strong>: Not all drivers are created equal.
            There are good drivers, then there are really bad drivers. Some
            people have a hesitant driving style, swerve, inability to properly
            judge the distance between stop signs, stop lights and cross walks,
            or don't pay attention to other drivers or pedestrians on the road.
            These characteristics of a bad driver plus others can almost
            guarantee a problem at some point, potentially fatally injuring
            someone if the driving is not appropriate in their environment.
          </li>
          <li>
            <strong> Auto Defect</strong>: It's not always the drivers fault.
            Sometimes the vehicle actually becomes subject to a flaw, whether it
            is the brake system, steering, stuck accelerator, etc. This can
            result in catastrophic results for the driver, pedestrians, other
            drivers on the roads and others standing by in the path of the
            faulty vehicle and flying debris.
          </li>
          <li>
            <strong> Premise Liability</strong>: Even if you are a safe driver,
            sometimes a bump in the road or sudden fallen object landing in the
            road can cause a severe situation, such as a fallen stop light or
            power pole, a vehicle striking it, and sending materials and even
            the car into a group of skateboarders waiting to cross the street.
          </li>
          <li>
            <strong> Negligence or reckless driving</strong>: Speeding drivers
            racing around, attempting donuts, wheelies or just swerving out of
            control can cause them to lose control of their vehicles,
            potentially sending their car careening into a roadside cafe or
            group of innocent bystanders.
          </li>
          <li>
            <strong> Wrong-doing</strong>: As in recent terror attacks,
            wrong-doing could be the case of a catastrophic event. Someone
            plowing their car into a crowded area or restaurant, attempting to
            cause catastrophic damage, or an angry person with road rage can
            lead to a{" "}
            <Link to="/wrongful-death/" target="new">
              wrongful death
            </Link>
            .
          </li>
        </ol>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/A1NORwVGQmI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>San Francisco Pedestrian Accident Statistics</h2>
        <p>
          As in all densely populated cities, accidents, injuries and fatalities
          are inevitable. In San Francisco and other parts of the Bay Area,
          cycling, jogging, other forms of transportation and
          touring/sight-seeing activities are very popular.
        </p>
        <p>In recent year's study:</p>
        <ul>
          <li>Out of approximately 35 traffic fatalities</li>
          <li>About 5-10% were cyclist</li>
          <li>20-35% were driving</li>
          <li>50%+ were walking or pedestrians</li>
          <li>Since 2009, traffic fatalities began to spike</li>
          <li>The spike peaked in 2013</li>
          <li>Traffic fatalities have been decreasing since</li>
        </ul>
        <p>
          To learn more about San Francisco Pedestrian Accidents, Injuries and
          statistics visit{" "}
          <Link
            to="http://sfgov.org/scorecards/traffic-fatalities"
            target="new"
          >
            sfgov.org
          </Link>
          .
        </p>
        <h2>
          <strong> Top 5 Most Common Pedestrian Accident Injuries</strong>
        </h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/cyclist-pedestrian-hit-by-car-san-francisco.jpg"
            width="100%"
            alt="san francisco pedestrian accident lawyer"
          />
        </LazyLoad>
        <p>
          There are many ways people can get hurt in the city. Just doing
          everyday activities, at work, recreational, sporting and so on.
          Regardless, we should always have our eyes open and watching for
          potential dangers and threats to our health and safety, as well as
          others around us.
        </p>
        <p>
          Here are the{" "}
          <strong> Top 5 Most Common Pedestrian Accident Injuries</strong> that
          people experience in <strong> San Francisco, CA</strong>:
        </p>
        <ol>
          <li>
            <strong> Bone Fractures</strong> <strong> & Sprains</strong>: Will
            all of the stairways, city sidewalks, curbs and uneven pavement
            foundations, twisted ankles, broken arms and leg injuries are
            prevalent in the up and down streets of San Fran.
          </li>
          <li>
            <strong> Head & Brain Injuries</strong>: Again, due to hills,
            slopes, inclines and ramps, wet surfaces can become slippery due to
            mildew, water and other substances, causing an ideal environment for
            slips and falls, concussions and even skull fractures and
            hemorrhages.
          </li>
          <li>
            <strong> Back & Spinal Injuries</strong>: Going hand and hand with
            slips and falls, inclines and declines, losing balance backwards or
            forwards can result in landing on ones neck, or back, potentially
            resulting in spinal injuries, paralysis and even more serious and
            catastrophic injuries.
          </li>
          <li>
            <strong> Cuts & Lacerations</strong>: San Francisco is an older
            city, full of beautiful design, architecture and original
            structures. Unfortunately, over time, these areas begin to degrade
            and become dangerous and more of a premise liability, than a piece
            of art. Rusty and sharp handrails, metal spines and rot-iron
            protruding from the concrete and other dangerous objects that can
            cause severe cuts, scrapes, lacerations and wounds, that have the
            ability to result in serious infection and even death.
          </li>
          <li>
            <strong> Internal Injuries</strong>: Besides falling from a ledge,
            down a stairwell or off a curb, the vast quantities of speeding,
            distracted and recklessly driving vehicles provides a deadly hazard
            of its own. One small step into the busy city streets can land you
            waking up in the hospital, unknowing of what happened, but with
            severe internal bleeding, a concussion, multiple broken bones and
            very high medical costs.
          </li>
        </ol>
        <p>
          The city of San Francisco and Bay Area can be a beautiful place to
          live and visit, as long as you stay alert and safe, as anywhere else
          you may go.
        </p>
        <h2>
          <strong>
            {" "}
            Why Is It Important To Hire a Personal Injury Attorney?
          </strong>
        </h2>
        <p>
          While many people feel they are capable of dealing with their case
          throughout the litigation process on their own, you never know what
          kind of dead ends or speed bumps you may face in the process.
        </p>
        <p>
          Having a skilled and experienced pedestrian accident lawyer or
          personal injury attorney represent you is most advised, to protect you
          and the outcome of your case. If you want maximum compensation, hire a
          trained and experienced top rated legal team.
        </p>
        <p>
          At Bisnar Chase, our legal staff have been representing injured
          victims for over <strong> 40 years</strong>, have won over{" "}
          <strong> $500 Million </strong>for their clients and have established
          a<strong> 96% success rate</strong>.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/pedestrian-accident-stress-san-francisco-lawyers.jpg"
            width="100%"
            alt="san francisco pedestrian accident attorney"
          />
        </LazyLoad>
        <p>
          The legal system can throw curve balls when you least expect it, and
          having a legal expert who understands the ins-and-outs, techniques and
          strategies. A skilled lawyer who has the resources to establish a
          solid win and maximum compensation for you is your best bet towards
          victory, finding justice, and holding those accountable for their
          negligence, carelessness and or wrong-doing.
        </p>
        <h2>
          <strong> Receiving Medical Treatment</strong>
        </h2>
        <p>
          Whether you were hit by a car crossing a crosswalk, a drunk driver
          jumping the curb and smashing you into a wall, or a car reversing out
          of a parking spot and hitting you on your bicycle, you are going to
          need medical attention. Depending on the severity of the event, EMS
          should evaluate you and taken to see either an emergency room, urgent
          care or your primary care physician for a follow up on your injuries.
        </p>
        <p>
          There are injuries that are consistent with pedestrian accident that
          seem to be mild but can actually be silent and extremely dangerous, if
          not deadly, such as a concussion or internal bleeding. It is better to
          be safe than sorry in these circumstances.
        </p>
        <p>
          Besides ensuring your health and safety are up to par, another key
          aspect to seeking medical attention immediately following your
          pedestrian accident is documenting your injuries in accordance to the
          situation.
        </p>
        <h2>
          <strong> Documenting Injuries</strong> in Affiliation to the Accident
        </h2>
        <p>
          Getting your injuries documented as soon as possible immediately
          following your accident is very important to the outcome of your case.
          Not being able to prove that your injuries were a direct result of the
          incident can make or break your case.
        </p>
        <p>
          Photos, videos, witnesses and any other content you can gather as soon
          as possible will only better your chances at having a successful and
          victorious case result.
        </p>
        <p>
          If you are not able to prove that your injuries did in fact result
          from the incident, the case could sway into arguing your injuries were
          already there, or happened after the event, and even possibly used to
          look like the accident was more severe than it truly was.
        </p>
        <h2>If We Don't Win Your Case, You Do Not Pay</h2>
        <p>
          Our personal injury law firm is a contingency-based legal practice,
          which means we will advance costs throughout your case to make sure
          you get what you need and deserve.
        </p>
        <p>
          If in fact by the end of your case we do not successfully win your
          case, you do not owe anything. No hidden fees or costs, and everything
          we advanced will be covered by us, and only our loss.
        </p>
        <p>
          We are so confident that if we sign your case, our team of pedestrian
          accident lawyers and top rated personal injury attorneys have what it
          takes to win your case through skill, experience and resources
          obtained throughout over four decades of experience.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco car accident law firm attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-san-francisco.jpg')"
            }}
          >
            <h2>Bisnar Chase Will Fight For You</h2>
          </div>
        </LazyLoad>
        <p>
          The <strong> San Francisco Pedestrian Accident Attorneys </strong>of{" "}
          <strong> Bisnar Chase </strong>have been representing and winning
          cases <strong> </strong>and have established an impressive
          <strong> 96% success rate</strong>, while winning over{" "}
          <strong> $500 Million</strong> for our clients.
        </p>
        <p>
          The legal staff at our law firm are dedicated to you and your case,
          and do everything in their ability to ensure you get the best legal
          representation possible.
        </p>
        <p>
          We want to help you, your loved ones and family get their lives back,
          maximum compensation, justice served and those held accountable for
          their negligence and or wrong doing.
        </p>
        <p>
          Call for your <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation Call 415-358-0723</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
