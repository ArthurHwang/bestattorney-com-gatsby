// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-auto-defect-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Auto Defect Lawyer - Bisnar Chase"
        pageDescription="You shouldn't have to worry if your vehicle is safe to drive your children to school. The San Francisco Auto Defect Lawyers fight to ensure the safety of our community through enforcing accountability on negligent circumstances & serious injuries. Call our skilled attorneys for your Free consultation at 415-358-0723."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Auto Defect Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="San Francisco Auto Defect Lawyers"
            title="San Francisco Auto Defect Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> San Francisco Auto Defect Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> have been representing injured victims
          and winning their cases for over <strong> 40 years</strong>. From
          sudden vehicle acceleration to malfunctioning airbag complications,
          our legal team has the resources and skill to take on, represent and
          win your case.
        </p>
        <p>
          If you want to win maximum compensation for the traumatic experiences,
          injuries, pain and suffering and more you or your loved ones have
          experienced, call our{" "}
          <Link to="/san-francisco" target="new">
            San Francisco Personal Injury Lawyers
          </Link>{" "}
          for your <strong> Free consultation</strong> at{" "}
          <strong> 415-358-0723</strong>.
        </p>
        <p>This page will cover the following:</p>

        <div className="mb">
          <Link to="/san-francisco/auto-defect-lawyers#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is an Auto Defect?
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Are Auto Defects Dangerous?
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Top 5 Most Infamous Auto Defect
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How to Report Safety Problem to NHTSA
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Recall Information
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Check If Your Car Is Safe
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Class Action Auto Defects
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why It's Important To Hire an Auto Defect Lawyer
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Contingency Fee - You Don't Pay If We Don't Win
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/auto-defect-lawyers#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Let Top Ranked Auto Defect Lawyers Represent You
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">What is an Auto Defect?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/auto-defect-close-up-car-parts.jpg"
            width="100%"
            alt="San Francisco Auto Defect Attorneys"
          />
        </LazyLoad>
        <p>
          Auto defects can result from many aspects of manufacturing errors,
          engineering issues or the misuse or lack of the correct part or system
          function. Auto defects are very dangerous and can be slight in
          appearance, but cause catastrophic situations and horrific accidents.
        </p>
        <p>
          Some auto defects happen but are not a safety issue, such as issues
          with:
        </p>
        <ul>
          <li>Radio</li>
          <li>Air conditioner</li>
          <li>Rust on body panel</li>
          <li>Cosmetic</li>
        </ul>
        <p>
          More serious auto defects that can result in an accident, serious
          injury or fatal crash:
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="/blog/harley-davidson-recalls-about-250000-motorcycles-for-brake-issues"
              target="new"
            >
              Brake failure
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/auto-defects/defective-seatbelts/types-of-seat-belt-injuries"
              target="new"
            >
              Seat belt failure
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/auto-defects/roof-crush" target="new">
              Roof crush
            </Link>
          </li>
          {/* <!-- Touched by Arthur 10/23/18.  Took out target=new attribute. -->
      <!-- <li>{" "}<Link to="/auto-defects/seatback-failure/seat-back-injuries" target="new">Seat-back failure</Link></li> --> */}
          <li>
            {" "}
            <Link to="/auto-defects/seatback-failure" target="new">
              Seat-back failure
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/auto-defects/airbag-failures" target="new">
              Airbag malfunction
            </Link>
          </li>
        </ul>
        <p>
          For more types of auto defects, explore our search bar using the
          magnifying glass at{" "}
          <Link to="/" target="new">
            bestatto-gatsby.netlify.app
          </Link>
          .
        </p>

        <h2 id="header2">Why Are Auto Defects Dangerous?</h2>
        <p>
          Problems or defects that exist in the vehicle somewhere, can cause
          seriously devastating problems in the blink of an eye, without any
          warning, when you least expect it.
        </p>
        <p>
          When driving down the mountain from a weekend in the snow with the
          family and the brakes fail, or in a car accident when the seatbelts
          fail, a tire blows out due to a flaw in the wall of the tire, these
          are all possible situations that do happen from time to time that
          could cause serious injuries resulting in fatalities, property damage
          and compensation for survivors and injured victims.
        </p>

        <div id="header3"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="San Francisco Auto Defect Lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/ford-pinto-auto-defect-san-francisco.jpg')"
            }}
          >
            <h2 id="header3">Top 5 Most Infamous Auto Defect</h2>
          </div>
        </LazyLoad>

        <p>
          Over the years, many different auto makers and auto part manufacturers
          have experienced auto defects and product recalls due to the dangers
          of accidents, serious, catastrophic and fatal injuries occurring. Here
          is a list of the <strong> Top 5 Most Infamous Auto Defects </strong>in
          America.
        </p>
        <ol>
          <li>
            Ford's faulty cruise control deactivation switch could short and
            ignite. A total of 14.9 million vehicles were recalled from years
            1992-2004. The company that manufactured the defective switch was{" "}
            <Link to="http://www.ti.com/" target="new">
              Texas Instruments
            </Link>
            .
          </li>
          <li>
            Ford Pinto - years 1971-1976; the gap between the gas tank in the
            rear of the vehicle to a set of bolts that could pierce the gas tank
            in the event of a rear-end collision could cause an explosion and
            fire, fueled a media frenzy when Ford decided it was cheaper to pay
            off lawsuits and settlements to burn victims of the auto defect than
            to recall the vehicle and fixing the problem.
          </li>
          <li>
            1970-1980 - Ford's park to reverse automatic transmission defect
            would have been the largest recall in history if the{" "}
            <Link to="https://www.transportation.gov/" target="new">
              Department of Transportation (DOT)
            </Link>{" "}
            hadn't agreed to a settlement in 1980 when the defect was found.
            After receiving more than 23,000 complaints, 6,000 accidents, over
            1,700 injuries with 98 attributed to the defect in which when the
            vehicle appeared to be in "park," could slip into reverse, causing
            serious consequences, as described by the{" "}
            <Link to="https://www.autosafety.org">
              Center for Autosafety (CAS)
            </Link>
            .
          </li>
          <li>
            9 million vehicle recalls and billions of dollars later, Toyota and
            Lexus experienced gas pedals that stuck to the floor, resulting in
            "runaway acceleration." After trying to blame{" "}
            <Link
              to="http://www.weathertech.com/floor-mats-all-weather/"
              target="new"
            >
              All-Weather
            </Link>{" "}
            floor mats, they had to redesign in order to ensure safety, after
            fatalities resulted from the defect. This auto defect took place in
            Toyota years 2004-2010, and Lexus years 2006-2010.
          </li>
          <li>
            Around 70 million Takata airbags have been recalled due to the
            airbag inflater exploding, and sending shrapnel into the face and
            surrounding cabin of the vehicle, resulting in many deaths every
            single year, throughout a variety of vehicle makes, models and
            years. See below how to check and see if your car has a Takata
            Airbag that could be defective.
          </li>
        </ol>
        <p>
          Over time there have been many different auto defects that have gone
          viral, or widely talked about. Whether they happen in large quantities
          with a new and popular car, or caused a serious or fatal car crash of
          a famous celebrity.
        </p>

        <h2 id="header4">How to Report Safety Problem to NHTSA</h2>
        <p>
          If you experience an auto defect or issue with your vehicle that is
          not or the norm, you can report it at{" "}
          <Link
            to="https://www-odi.nhtsa.dot.gov/VehicleComplaint/"
            target="new"
          >
            Safercar.gov (NHTSA)
          </Link>
          .
        </p>
        <p>
          When you file a complain and report an auto defect, you could be
          saving millions of lives that could be affected by the same auto
          defect or issue you have been subjected to. It is important to speak
          up and let your voice be heard. It makes a difference.
        </p>
        <p>
          There are many different ways to identify an auto defect, some of
          which may consist of:
        </p>
        <ul>
          <li>
            Noticing a sudden change to the way your vehicle drives,
            accelerates, reverses, steers, etc
          </li>
          <li>
            Unusual noises coming from areas where noise was not heard from
            before
          </li>
          <li>
            Cracks in metal, steel, engine parts, other vital car areas, etc
          </li>
          <li>
            Rattling, shaking or bolts loosening all of a sudden/unable to
            tighten
          </li>
          <li>
            Smoke (dark or light) coming from engine compartment, or excessive
            exhaust fumes
          </li>
          <li>Exhaust fumes inside the occupant cabin</li>
          <li>Parts of the vehicle that are easy to move, shake or rattle</li>
        </ul>
        <p>
          If you were injured when experiencing this defect, call our trusted
          San Francisco Auto Defect Attorneys for your Free consultation at{" "}
          <strong> 415-358-0723</strong>.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/reporting-your-cars-auto-defect.jpg"
            width="100%"
            alt="San Francisco Auto Defect Attorney"
          />
        </LazyLoad>

        <h2 id="header5">Recall Information</h2>
        <p>
          Even though auto defects are lying dormant in your car until you
          experience a sudden issue, sometimes you get a warning or recall, with
          the opportunity to have it fixed before the auto defect affects you.
        </p>
        <p>
          A recall is usually sent out by email, mail, on advertisements online
          or on the morning or evening news. For example, a recall notice on the
          evening news while you are cooking dinner may pop up as the highlight
          segment of the evening;
        </p>
        <p>
          "ABC Car company has begun urging people who have purchased or leased
          models 2016-2018 XYZ model SUV, to bring in to be service and have a
          specific item replaced, fixed or exchanged for the correct and
          non-defected material or part."
        </p>
        <p>
          This is a disclaimer to avoid lawsuits or potential charges brought
          against their company and affiliate manufacturing companies.
        </p>
        <p>
          The problem is, not everyone is able to catch that evening news
          segment on the newly found and dangerous auto defect, or didn't see
          the advertisement or email warning of the potentially hazardous auto
          defect in their new vehicle.
        </p>
        <p>
          But, there is a way that you can check and see if your exact vehicles
          has any warnings, recalls or serviced needed on your exactly car, see
          below.
        </p>

        <h2 id="header6">How To Check If Your Car Is Safe</h2>
        <p>
          Ensuring that your vehicle is safe for you and your loved ones to
          drive in is important, and can give you a piece of mind. All you have
          to do is check and see online, for free.
        </p>
        <p>
          All you need is your vehicles 17 character VIN (vehicle identification
          number), found on the lower driver-side corner of your vehicles front
          windshield, or on the side panel of your driver side door.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/where-to-find-vin-information-on-vehicle.jpg"
            width="100%"
            alt="San Francisco Auto Defect Law Firm"
          />
        </LazyLoad>

        <p>
          Go to{" "}
          <Link
            to="https://www.nhtsa.gov/recalls?gclid=Cj0KCQiA_JTUBRD4ARIsAL7_VeXeH0vesIFrT55LtR6_ozfvumlomWx4BhcT22NFy--rw7cIYHu0UpQaAjx9EALw_wcB"
            target="new"
          >
            NHTSA.gov.recalls
          </Link>
          , and type in your VIN number, and you will instantly get a report of
          any recalls, system alerts, auto defects pertaining to your exact
          vehicle.
        </p>
        <p>
          If you do see that you have a safety recall or auto defect warning
          that urges you to get it fixed as soon as possible, do not panic. Just
          schedule an appointment with your auto dealer and tell them the matter
          of service you need, express to them that you do not feel safe driving
          your car until this defect is fixed, and they are usually very
          responsive.
        </p>
        <p>
          You can even have your car towed to the dealership if you are not
          comfortable driving it with the safety issues you find.
        </p>
        <p>
          Don't worry, it's a great thing you found it, and once it is fixed,
          you will have that peace of mind that your vehicle is as safe as can
          be for you, your family and your loved ones.
        </p>

        <h2 id="header7">Class Action Auto Defects</h2>
        <p>
          Throughout time there have been products that are sold in mass
          quantities, like cars, trucks and SUV's. The problem with mass
          production, is the fact that if you can not ensure that a vehicle is
          manufactured and has zero defects or issues, how can you do so on 1
          million vehicles?
        </p>
        <p>
          This is where we get into class action cases, where many different
          people experience the same issue or auto defect in the same line or
          model of vehicle, which can be dangerous for companies and their
          financial stock holders, reputation and existence, but safety is more
          important than corporate success.
        </p>
        <p>
          Thats why the class action auto defect lawyers of{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          take these matters so seriously, and with such dedication and
          precision. We believe that large auto makers and auto part
          manufacturers need to uphold the safety, quality and consistency of
          their products so that nobody drives off in a vehicle that is going to
          fail on the road, possibly leading to a multiple fatality car
          accident.
        </p>
        <p>
          <em>
            The video below elaborates on the specifics of the Takata Airbag
            Recall/Auto Defect, courtesy of{" "}
            <Link to="https://www.pbs.org/newshour/" target="new">
              PBS News Hour
            </Link>
            .
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/jGJN6Un0d7g"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2 id="header8">Why It's Important To Hire an Auto Defect Lawyer</h2>
        <p>
          Many people are under the impression they can take on a legal battle
          themselves, when in fact it is not totally impossible, but can
          definitely be a headache and cost you a few extra dimes if not handled
          correctly or appropriately.
        </p>
        <p>
          The lawyers of Bisnar Chase have been studying law and representing
          injured victims for over 40 years, which as result shows in won cases,
          at an impressive 96% success rate.
        </p>
        <p>
          The problem with not hiring an attorney is having to consistently
          staying on top of all deadlines, due dates, progress of the current
          status of your case and all aspects that are attributed to handling a
          case, all while recuperating, physical therapy, recovering from
          injuries or whatever you are dealing with as direct result of your
          accident.
        </p>
        <p>
          Auto Defect Attorneys at Bisnar Chase have the resources to take on
          complex cases with ease, and the ability to devote their passion
        </p>

        <h2 id="header9">Contingency Fee - You Don't Pay If We Don't Win</h2>
        <p>
          Many people are worried that they do not have the money to hire a
          lawyer, so they go without, end up not going through the legal process
          or attempting to do it on their own and losing.
        </p>
        <p>
          You do not want to miss out on compensation, and at Bisnar Chase, you
          won't. Bisnar Chase is a personal injury and auto defect law firm that
          advances all costs and helps their clients get through the case
          without financial burden. If we do not win your case, you do not pay
          us.
        </p>
        <p>
          The auto defect attorneys of Bisnar Chase have been perfecting the art
          of winning auto defect cases against large corporations and will get
          you maximum compensation, or you do not pay.
        </p>

        <div id="header10"></div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco car accident law firm attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/san-francisco-personal-injury-lawyers-auto-defects.jpg')"
            }}
          >
            <h2>Let Top Ranked Auto Defect Lawyers Represent You</h2>
          </div>
        </LazyLoad>

        <p>
          The <strong> San Francisco Auto Defects Lawyers </strong>of{" "}
          <strong> Bisnar Chase </strong>have been representing and winning
          these cases for over <strong> 40 years</strong>, and want to help get
          your life back. Our dedicated legal staff, personal injury lawyers and
          aggressive attorneys have won over <strong> $500 Million </strong>for
          our clients and have established an impressive
          <strong> 96% success rate</strong>.
        </p>
        <p>
          Our firm is dedicated to helping those in need and if you visit our{" "}
          <Link to="/about-us/testimonials" target="new">
            client testimonial and video testimonials page
          </Link>
          , you can see what our clients had to say about our services,
          representation and there overall experience working with us.
        </p>
        <p>
          Our legal staff at the Bisnar Chase Law Firm are passionate about what
          they do, each and every single day. We understand that we are making
          the world a safer place, one client at a time.
        </p>
        <p>
          If you have been injured due to an auto defect contact our skilled{" "}
          <strong> San Francisco Auto Defect Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> for a{" "}
          <strong> Free consultation</strong> at
          <strong> 415-358-0723</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
