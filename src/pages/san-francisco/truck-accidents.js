// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-semi-truck-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Truck Accident Lawyers - Bisnar Chase"
        pageDescription="Our San Francisco Truck Accident Lawyers have over 39 years of experience & over $500 Million Won for our Clients. Call for a Free Consultation at 415-358-0723."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Truck Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san francisco truck accident lawyers"
            title="san francisco truck accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> San Francisco Truck Accident Lawyers</strong> have over{" "}
          <strong> 39 years of experience</strong> in winning cases for injured
          people who are in need of help. With over
          <strong> $500 Million</strong> won for our clients, we have
          established a <strong> 96% Success Rate</strong> and a trusted name in
          the world of legal representation.
        </p>
        <p>
          In any personal injury lawsuit, it is necessary for the plaintiff to
          prove fault and liability. These are two important components when it
          comes to any injury claim. San Francisco truck accidents are no
          exception.
        </p>
        <p>
          If you have been injured in a{" "}
          <strong> San Francisco truck accident</strong>, you will need to prove
          fault and liability in order to pursue compensation for your losses.
          Contact our team of experienced{" "}
          <strong>
            {" "}
            <Link to="/san-francisco" target="new">
              San Francisco Personal Injury Lawyers
            </Link>
          </strong>{" "}
          who are dedicated to making sure you receive maximum compensation and
          the best professional legal representation you can get.
        </p>
        <p>
          Unfortunately, it is common for truck drivers to deny responsibility
          and for their insurance providers to fight any and all claims made by
          the victims.
        </p>
        <p>
          An experienced San Francisco truck accident lawyer can help gather
          evidence, determine liability, file a personal injury claim and help
          victims obtain the compensation they need and rightfully deserve.
        </p>

        <h2>Gathering Evidence To Win Your Case</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/crash-site-investigation-evidence-san-francisco.jpg"
            alt="san francisco truck accident lawyers"
            className="imgleft-fixed"
            width="265"
          />
        </LazyLoad>

        <p>
          The physical and circumstantial evidence you gather in the aftermath
          of a truck accident can strengthen your case tremendously. In addition
          to gathering the contact information from the truck driver and any
          witnesses, there are a number of steps you can take to collect and
          preserve valuable evidence at the scene.
        </p>

        <p>
          If you do not have a camera in your car, or a cell phone with a
          camera, call a friend to visit the crash site and take photos. The
          more photos you can take, the better. You will want proof of where the
          crash occurred, how the vehicles were positioned, what the damage to
          the vehicles looked like and what your injuries looked like
          immediately after the accident.
        </p>
        <p>
          In addition to photos, you should write out your recollections of the
          accident. Having your written notes of exactly when, where and how the
          accident occurred can help. It will also be in your best interest to
          obtain a copy of the police report.
        </p>
        <p>
          A skilled <strong> truck accident attorney</strong> will also be able
          to request an inspection of the truck and the trailer, collect
          evidence from the truck's data recorder, and examine the trip
          inspection report as well as the driver's log.
        </p>
        <h2>Determining Liability</h2>
        <p>
          There are a number of ways in which your case can be bolstered during
          the investigation. It will be necessary to prove that the truck driver
          was negligent at the time of the accident. If, for example, the
          authorities cite the driver for being under the influence or for a
          traffic violation, it will help you case.
        </p>
        <p>
          A knowledgeable truck accident lawyer will know how to get to the
          bottom of a number of important liability questions:
        </p>
        <ul>
          <li>When was the last time the truck was properly inspected?</li>
          <li>
            Were the tires, brakes and other components properly maintained?
          </li>
          <li>Who was responsible for loading the cargo?</li>
          <li>
            Did the trucking company properly review the truck driver's
            experience, driving record and criminal background before hiring
            him?
          </li>
          <li>When was the last time the truck driver took a break?</li>
          <li>
            Did the truck driver take a break for over 10 hours before returning
            to work?
          </li>
          <li>
            Did the trucking company encourage the driver to violate federal
            regulations to meet unreasonable deadlines?
          </li>
          <li>
            Was there a pattern of falsifying the number of hours worked in
            order to meet unrealistic deadlines?
          </li>
        </ul>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/mX8wMo8QTzo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Insurance Claims and Civil Litigation</h2>
        <p>
          Ideally, injured San Francisco truck accident victims will receive
          fair compensation for all of the losses sustained such as medical
          expenses and lost wages.
        </p>
        <p>
          Unfortunately, simply requesting fair compensation through an
          insurance claim rarely results in an adequate settlement offer,
          especially in serious injury cases.
        </p>
        <p>
          It is common for inadequate settlements to be offered or for a claim
          to be denied completely. In such cases, the injured victim may have to
          file a civil lawsuit against the at-fault parties.
        </p>
        <p>Many cases result in lawsuits against many liable parties.</p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco truck accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-san-francisco.jpg')"
            }}
          >
            <h2>Bisnar Chase Will Win or You Don't Pay</h2>
          </div>
        </LazyLoad>

        <p>
          The experienced <strong> San Francisco truck accident lawyers</strong>{" "}
          at <strong> Bisnar Chase </strong> have a long and successful track
          record of helping injured victims and their families secure fair
          compensation for their losses.
        </p>
        <p>
          To ensure you have the best chance for winning and receiving maximum
          compensation, contact and hire a skilled and experienced San Francisco
          Truck Accident Lawyer.
        </p>
        <p>
          Our law firm has dedicated attorneys and legal staff that will always
          go the extra mile regardless of the complexity of the case, to make
          sure you are taken care of and find justice.
        </p>
        <p>
          With over <strong> 39 years</strong> <strong> of experience</strong>{" "}
          and a <strong> 96% Success Rate</strong>,{" "}
          <strong> Bisnar Chase </strong>has won over{" "}
          <strong> $500 Million </strong>for our clients and want to help you.
        </p>
        <p>
          We will help hold negligent truck drivers and trucking companies
          accountable. For your <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation</strong>,<strong> Call 415-358-0723</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
