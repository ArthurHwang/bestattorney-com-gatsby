// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-francisco-bus-accident-lawyers-banner-new.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Bus Accident Lawyers - Bisnar Chase"
        pageDescription="Bus accidents are very common and happen every day. Need a public transportation accident attorney? If you have been injured in a bus accident, contact our San Francisco Bus Accident Lawyers at Bisnar Chase for your Free consultation at 415-358-0723. You could be entitled to compensation. You don't pay if we don't win."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Bus Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san francisco bus accident lawyers"
            title="san francisco bus accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> San Francisco Bus Accident Lawyers</strong> of{" "}
          <strong> Bisnar Chase</strong> have been representing cases for over{" "}
          <strong> 40 years</strong>, winning over
          <strong> $500 Million </strong>for our clients. Having established an
          impressive <strong> 96% success rate</strong>, our team of legal
          professionals are ready to fight for you.
        </p>
        <p>
          Public transportation, school buses, shuttles and other types of large
          multi-passenger vehicles can make things easier on schedules and
          budgets, or even for a good time out on the town, like bachelor and
          bachelorette parties, prom transportation and more. But when bad
          situations become a reality in these large moving blocks of metal,
          severe consequences can result, especially if negligence or wrong
          doing is a factor.
        </p>
        <p>
          If you have been injured or have experienced a traumatic injury, call
          our{" "}
          <Link to="/san-francisco" target="new">
            San Francisco Personal Injury Lawyers
          </Link>{" "}
          for your
          <strong> Free consultation</strong> at <strong> 415-358-0723</strong>.
        </p>
        <p>This page will cover the following:</p>

        <div className="mb">
          <Link to="/san-francisco/bus-accident-lawyers#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Public Transportation in San Francisco{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are San Francisco Public Buses Safe? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Top 5 Most Dangerous Types of Buses <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              San Francisco Bus Accident Statistics <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              No Seat Belts in School Buses? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Protect Yourself in a Bus Crash{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Seeking Medical Attention Immediately <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Documenting Injuries is Important <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>

          <Link to="/san-francisco/bus-accident-lawyers#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Quality Legal Representation That Wins{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>

        <div id="header1"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco bus accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/public-transportation-san-francisco.jpg')"
            }}
          >
            <h2>Public Transportation in San Francisco</h2>
          </div>
        </LazyLoad>

        <p>
          The <strong> San Francisco Bus Accident Lawyers </strong>understand
          that everyone needs transportation of some kind at some point in their
          life, the only problem is that transportation of any kind is
          dangerous.
        </p>
        <p>
          Whether it's a family taking a bus downtown, a taxi from one city to
          another, subway, train, airplane, Uber, Lyft, whatever it is, they all
          have their hazards. With Buses, the vehicle themselves are very large,
          very heavy, and transport large number of people at a time, increasing
          both the overall weight and risk of large catastrophic injuries or
          casualties.
        </p>
        <p>
          Why is weight a factor? The weight of any vehicle increases or
          decreases its ability to maneuver, steer, accelerate and brake in an
          efficient and safe manner. Increasing the weight with large numbers of
          people and their luggage can significantly increase the dangers of
          travel.
        </p>
        <p>
          At{" "}
          <strong>
            {" "}
            <Link to="/" target="new">
              Bisnar Chase
            </Link>
          </strong>
          , our legal team encounters individuals who become victims of public
          transportation, and are here to help.
        </p>
        <p>
          If you have experienced a personal injury or traumatic event riding a
          public bus, contact our San Francisco Personal Injury Attorneys for a{" "}
          <strong> Free consultation</strong> and
          <strong> case evaluation</strong> at <strong> 415-358-0723</strong>.
        </p>

        <h2 id="header2">Are San Francisco Public Buses Safe?</h2>
        <p>
          With over a million individuals killed in car accidents every single
          year, road travel is very dangerous. But are public buses safe?
        </p>
        <p>
          Generally speaking, public bus travel is safe. They travel at lower
          speeds than most road vehicles, are not as prone to distracted,
          intoxicated or reckless driving behavior, due to strict laws, rules
          and guidelines set on bus driver safety, and are set to an assigned
          schedule.
        </p>
        <p>
          Buses are supposed to be maintained by government personal and staff,
          ensuring the safety of each and every bus. Brakes, tire pressure and
          quality, engine and acceleration attention as well as many other
          aspects that could potentially endanger passenger's well-being.
        </p>
        <div id="header3"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco bus accident lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/top-5-most-dangerous-buses.jpg')"
            }}
          >
            <h2>Top 5 Most Dangerous Types of Buses</h2>
          </div>
        </LazyLoad>

        <p>
          School buses, charter buses, double-decker buses, Greyhound buses -
          What type of buses are the most dangerous? Where you thinking about
          the danger you were in the last time you were riding on a bus?
        </p>

        <p>
          Here is a list of the{" "}
          <strong> Top 5 Most Dangerous Types of Buses:</strong>
        </p>

        <p>
          <strong>1. Articulated Bus (accordion): </strong>
          You may not be familiar with these types of buses, but if you ever get
          an opportunity to ride in one of these vehicles, just stay aware of
          the dangers. These buses, depending on overall safety and maintenance,
          are generally safe for the most part. The dangers of these buses
          specifically are in turns and bends. Passengers need to watch for
          being pinned, pinched or crushed in the conjunction areas, in the
          accordion-zones. Also if the bus looses control, or experiences a side
          impact, the conjoining areas are weak points that can become "sudden
          and unexpected ejection points" for passengers.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/bendy-articulated-bus-most-dangerous.jpg"
            width="100%"
            alt="san francisco city bus crash lawyers"
          />
        </LazyLoad>

        <p>
          <strong>2. Double-Decker Bus:</strong>
          Famous for being seen in England, these double-decker and two story
          buses can be found in many tourist areas. Great for touring and
          site-seeing, they can be extremely dangerous in the event of an
          accident. If one of these buses happen to flip, roll or tip over,
          riders on the top deck are at risk for extreme injury or death. Low
          bridges also become a severe risk, if the driver is not paying
          attention or a bridge is unmarked.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/double-decker-bus-dangerous-buses.jpg"
            width="100%"
            alt="san francisco bus crash attorneys"
          />
        </LazyLoad>

        <p>
          <strong>3. Greyhound (cross-country):</strong>
          Cross country buses and in no specific isolation to the Greyhound
          company, can be dangerous to ride on, due to higher risks of
          experiencing more hazardous situations. Cross country buses travel
          long distances, getting passengers from one city to the next, in many
          cases from one side of the country to the other. Experiencing heavy
          rain, dangerous conditions, snow, ice, high elevations, dangerous
          roads and many other risk factors is common. Cross country bus drivers
          need to be properly trained and disciplined in their driving
          techniques and strategies.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/greyhound-bus-dangerous-buses.jpg"
            width="100%"
            alt="san francisco bus crash lawyer"
          />
        </LazyLoad>

        <p>
          <strong> 4. City Buses:</strong> There are many buses in the downtown
          areas, city streets and outlining suburban areas. City buses encounter
          cramped conditions, heavy traffic and thick congested streets every
          day. Lots of foot traffic, pedestrians and cyclist are constant
          dangers and offer extra risk factors. In downtown areas and low
          poverty-prone areas, violence, assaults and dangerous individuals tend
          to ride city buses as a cheap way of getting around, which can lead to
          attacks, muggings and robbery. Stay alert, stay prepared.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/city-bus-dangerous-buses.jpg"
            width="100%"
            alt="san francisco bus crash attorney"
          />
        </LazyLoad>

        <p>
          <strong> 5. School Buses:</strong> We trust that the transportation we
          allow our children to ride in is safe and reliable. Yet, school buses
          have no seat belts. School buses have rules and guidelines that
          children and teens must follow, but is poorly enforced. School buses
          are large and heavy vehicles that when out of control can be extremely
          dangerous. The largest percentage of injuries and fatalities involving
          school buses are pedestrians, cyclists and others outside of the bus.{" "}
          <Link to="/san-francisco/pedestrian-accident-lawyers" target="new">
            Pedestrian accidents
          </Link>{" "}
          can be very severe when involving buses.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/school-bus-dangerous-buses.jpg"
            width="100%"
            alt="san francisco bus crash law firm"
          />
        </LazyLoad>

        <h2 id="header4">
          <strong> San Francisco Bus Accident Statistics</strong>
        </h2>
        <p>
          Like semi truck accidents and other heavy vehicle collisions, bus
          accidents can be catastrophic and cause severe damage. Here are some
          statistical information to give you a better sense of the safety,
          dangers, pros and cons of bus transportation.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.nhtsa.gov/" target="new">
            NHTSA
          </Link>
          , in an{" "}
          <Link
            to="https://www.usatoday.com/story/news/2015/09/15/school-bus-safety-statistics/72318198/"
            target="new"
          >
            article posted by USA Today
          </Link>
          : "on average/per year":
        </p>
        <ul>
          <li>
            Approximately 130+ fatalities in school vehicle related crashes
            every year
          </li>
          <li>8% of those crashes are in buses</li>
          <li>
            Bicyclist, pedestrians and others outside of the bus make up 21% of
            those fatalities
          </li>
          <li>The most of these fatalities are people in other vehicles</li>
          <li>
            Seat belts are not required on school buses in: California, Florida,
            Louisiana, New Jersey, New York, and Texas
          </li>
        </ul>
        <p>
          To learn more about school bus safety, visit{" "}
          <Link
            to="https://www.nhtsa.gov/road-safety/school-bus-safety"
            target="new"
          >
            NHTSA/School-Bus-Safety
          </Link>
          .
        </p>

        <h2 id="header5">No Seat Belts in School Buses?</h2>
        <p>
          We have instilled in our minds, that it is important to wear seat
          belts when traveling. Whether you are wearing a seat belt in a car,
          train, plain or semi truck, it's the law, it saves lives, it's the
          smartest thing to do.
        </p>
        <p>
          But why is it not a federal law for school buses to require seat belts
          in school buses that our young and innocent children ride in every
          single day to school and on field trips?
        </p>
        <p>
          Researchers and the NHTSA state that it will cost between
          $10,000-12,000 per bus, to equip essential and efficient safety belts,
          which will come to millions if not billions throughout all of the
          buses in the country.
        </p>
        <p>
          It sounds as if it is money over safety is the concluding factor, but
          time will tell. Seat belts are not required on school buses in:
          California, Florida, Louisiana, New Jersey, New York, and Texas. How
          do you feel about seat belts not being required on your child's bus to
          school?
        </p>
        <p>
          Was your son or daughter injured in a school bus accident either on
          their way to school or coming home? What about when on a field trip
          with a class or after-school program?
        </p>
        <p>
          If you or your loved ones have experienced a personal injury or
          traumatic event, contact the San Francisco Bus Accident Attorneys for
          your Free case evaluation and consultation.
        </p>
        <p>
          You could be entitled to compensation, and Bisnar Chase will win you
          maximum compensation, or you do not pay.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/bkERcx44nqA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2 id="header6">How To Protect Yourself in a Bus Crash</h2>
        <p>
          If only we could prevent the worse from happening - How can you know
          when danger is going to strike when riding in a bus?
        </p>
        <p>
          Obviously, you won't be able to guarantee that you can protect
          yourself from danger, every time it strikes, but being prepared, in
          the case of having the ability to, will help.
        </p>
        <ul>
          <li>
            Don't get on a bus if you notice the driver is intoxicated or
            noticeably negligent. This can be very dangerous to you, the other
            passengers, other drivers on the road, as well as pedestrians,
            cyclist and property. Instead, call 911 and report the driver.
          </li>
          <li>
            If you are already on the bus and notice negligence or wrong-doing
            of the bus driver, call 911 and report the situation immediately.
          </li>
          <li>
            Don't get on a bus if you notice visible or audible damage. Instead,
            let the driver know there is something wrong, and report the
            situation.
          </li>
          <li>
            If driving conditions are extreme, such as heavy rain, snow, hail or
            unsafe driving conditions, such as flooding, icy roads and so forth,
            find another way of transportation.
          </li>
          <li>
            If you are on a bus and notice a car approaching the rear, front or
            sides of the bus in a non-stopping motion, warn the driver and
            nearby passengers, move from the area of soon-to-be-impact, find
            cover and brace - this is not a guarantee for health and safety, but
            will prove better than staying in the line of impact and waiting to
            see what happens.
          </li>
        </ul>
        <p>
          For more on how to stay safe on buses, you can visit an article on the{" "}
          <Link
            to="http://www.dailymail.co.uk/health/article-146201/How-stay-safe-trains-buses.html"
            target="new"
          >
            DailyMail.com/How-to-Stay-Safe-on-Trains-and-Buses
          </Link>
          .
        </p>

        <div id="header7"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="san francisco bus accident attorney"
            style={{
              backgroundImage:
                "url('/images/text-header-images/seeking-medical-attention-san-francisco-bus-accident-attorneys.jpg')"
            }}
          >
            <h2>Seeking Medical Attention Immediately</h2>
          </div>
        </LazyLoad>

        <p>
          After experiencing any type of accident, hit or traumatic situation,
          it is best to be evaluated by a health care professional. Whether you
          seek medical attention at an urgent care facility, take ambulance
          transportation or visit the emergency room of a hospital, ensuring
          that your health and safety is 100% is always the best practice.
        </p>
        <p>
          Often, injuries go untreated due to the victim being unaware of the
          injuries in the first place. Common injuries that go untreated,
          causing more serious and potentially life-threatening conditions
          consist of:
        </p>
        <ul>
          <li>
            <strong> Head injuries</strong>
          </li>
          <li>
            <strong> Traumatic brain injuries</strong>
          </li>
          <li>
            <strong> Internal bleeding</strong>
          </li>
          <li>
            <strong> Internal lacerations</strong>
          </li>
          <li>
            <strong> Bone fractures</strong>
          </li>
          <li>
            <strong> Infections</strong>
          </li>
        </ul>
        <p>
          Seeking medical attention has it's benefits which can also help{" "}
          <strong> win your case</strong>.
        </p>

        <h2 id="header8">Why Documenting Injuries is Important</h2>
        <p>
          Making sure that your injuries are documented in affiliation with the
          incident or accident is a must. Not having or having your injuries
          documented can make or break your case.
        </p>
        <p>
          Later down the line throughout the process of your case, not having
          legally documentation of your injuries can bring questions to if the
          injuries were preexisting or happened after the fact. It can also be
          argued that the victim inflicted the injuries upon themselves in order
          to look like they were more injured than they were at the time of the
          accident, with the resulting injuries of the accident.
        </p>
        <h3>How Can You Legally Document Your Injuries?</h3>
        <ul>
          <li>
            Seek medical attention (Dr's notes, hospital reports, x-ray,
            treatments, etc.)
          </li>
          <li>Police reports</li>
          <li>
            Photos and video (of the accident, location of the accident,
            placement of vehicles, peoples, etc.)
          </li>
          <li>
            Audio with digital time stamp (iPhones/Androids - shows what time
            the audio was recorder. Although this is not the best method,
            anything is better than nothing - usually only used if the only
            camera available is broken)
          </li>
          <li>
            Witnesses - having people who saw the accident or who were involved
            in the accident giving information of what they saw, when they saw
            it, how it happened and any other information they can give about
            your injuries at the time of the accident - usually in writing,
            audio, photos and video.
          </li>
        </ul>

        <p>
          Any documentation is good documentation. No documentation can break
          your case to the argument that your story was exaggerated and of false
          happenings.
        </p>

        <h2 id="header9">Quality Legal Representation That Wins</h2>

        <p>
          The team of skilled and dedicated{" "}
          <strong> San Francisco Bus Accident Attorneys </strong>have over{" "}
          <strong> 40 years of experience</strong>. Having established a{" "}
          <strong> 96% success rate</strong>, the legal professionals of{" "}
          <strong> Bisnar Chase </strong>want to be there for you every step of
          the way.
        </p>
        <p>
          You can never ask to many questions and we will never put you or your
          case on the back-burner. When you are a client of ours, you become
          family, and we will always have your back, even after your case is
          settled.
        </p>
        <p>
          To hear it what its like to be represented by Bisnar Chase, visit our
          client testimonial and video testimonials page. Become one of our
          victories and success stories, part of the
          <strong> $500 Million </strong>we have won for our clients, and get
          your life back.
        </p>
        <p>
          {" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          wants to help and represent you and your case, to win and get you
          maximum compensation. Call us for your
          <strong> Free consultation</strong> and{" "}
          <strong> Case Evaluation</strong> at <strong> 415-358-0723</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
