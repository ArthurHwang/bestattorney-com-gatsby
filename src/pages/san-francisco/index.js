// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/san-diego-personal-injury-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Francisco",
      population: 837442,
      totalAccidents: 23771,
      intersection1: "Potrero Ave & 16th St",
      intersection1Accidents: 50,
      intersection1Injuries: 60,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 489.5,
      city1Name: "Daly City",
      city1Index: 158.4,
      city2Name: "Broadmoor",
      city2Index: 185.1,
      city3Name: "Brisbane",
      city3Index: 127.9,
      city4Name: "Colma",
      city4Index: 686.0
    },
    locationBox3: {
      intersection2: "Market St & 5th St",
      intersection2Accidents: 65,
      intersection2Injuries: 63,
      intersection2Deaths: 0,
      intersection3: "4th St & Harrison St",
      intersection3Accidents: 61,
      intersection3Injuries: 52,
      intersection3Deaths: 0,
      intersection4: "Market St & Octavia St",
      intersection4Accidents: 54,
      intersection4Injuries: 50,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Francisco Personal Injury Lawyers - Bisnar Chase Attorneys"
        pageDescription="Bisnar Chase serves personal injury victims in San Francisco, CA. We specialize in car accidents, wrongful death & auto defects and have won over $500 Million for thousands of clients. Free consultation, no win, no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Francisco Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="San Diego personal injury lawyers"
            title="San Diego personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> San Francisco Personal Injury Lawyers</strong> at
          <strong> Bisnar Chase</strong> are award-winning attorneys with over
          <strong> $500 Million won</strong> and a
          <strong> 96% Success Rate</strong>. With over
          <strong> 39 years of experience</strong>, our law firm strives every
          day to make the world a better place.
        </p>
        <p>
          <strong> Bisnar Chase</strong> understands that experiencing a
          traumatic situation or helping a friend, family or loved one who has
          is very difficult on a financial, emotional and mental level. Bisnar
          Chase has helped clients find justice, maximum compensation and help
          make the rest of their lives better from all over the country
        </p>
        <p>
          We want to help you, like all of the clients we have helped before.
        </p>
        <p>
          If you or a loved one has experienced a personal injury, please call
          for your <strong> Free Case Evalutation </strong> and
          <strong> Consultation</strong>. Our team of attorneys, paralegals and
          other legal staff are always available to answer your questions and
          concerns during our normal business hours.
        </p>
        <p>
          For immediate assistance <strong> Call 415-358-0723</strong>.
        </p>
        <h2>San Francisco Has Its Dangers</h2>

        <p>
          San Francisco is one of the world's centers of culture, technology,
          media, business, and international trade.
        </p>
        <p>
          It is home to renowned institutions covering a broad range of
          professional and cultural fields, and it is one of the most
          substantial economic engines of the United States.
        </p>
        <p>
          San Francisco is also, unfortunately, a place where hundreds of
          needless accidents occur each month.
        </p>
        <p>
          With the amount of traffic and dense population, it is no wonder that
          there are multiple{" "}
          <Link to="/san-francisco/car-accidents" target="new">
            San Francisco car accidents
          </Link>{" "}
          daily.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/san-francisco-sky-line-city.jpg"
            alt="san francisco personal injury attorneys"
            width="100%"
          />
        </LazyLoad>

        <p>
          The <strong> San Francisco personal injury attorneys</strong> at{" "}
          <strong> Bisnar Chase Personal Injury Attorneys</strong> offer these
          San Francisco resources for your convenience. Our law firm offers a{" "}
          <strong> Free Consultation </strong>and{" "}
          <strong> Free Case Evaluation </strong>so that we can figure out how
          we can best help you.
        </p>
        <p>
          If you are in need of an San Francisco personal injury lawyer, please
          call us at <strong> 415-358-0723</strong>. Even though our main office
          is located in Newport Beach, we will travel to meet you in our San
          Francisco offices.
        </p>
        <h2>Personal Injury Statistics</h2>
        <p>
          We have all encountered some sort of minor or major injury or know
          someone who has at some point in their life. But depending on how it
          happened, where it happened and why it happened can vary drastically.
        </p>
        <ul>
          <li>
            According to the health statistics by the Center for Disease Control
            (CDC) that about 37 million people seek medical attention due to an
            unintentional injury or poisoning every year.
          </li>
          <li>37 Million makes up 34 % of emergency room visits.</li>
          <li>
            Fatalities as a result of an unintentional accident totalled nearly
            11,000 in a recent years study.
          </li>
          <li>
            San Francisco reported 261 deaths in this recent years study as
            well.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          We have successfully concluded hundreds of personal injury cases in
          the San Francisco Bay area in the past.
        </p>
        <p>
          Want aggressive representation by local attorneys who are passionate
          about their clients and their causes?
        </p>
        <p>
          Attorneys who give you straight-talk and get results? Attorneys that
          love representing the common person against the powerful?
        </p>
        <p>
          The Bisnar Chase Personal Injury Attorneys exclusively represents
          injured people and wronged consumers.
        </p>
        <p>
          For your <strong> Free Case Evaluation </strong>and
          <strong> Consultation Call 415-358-0723</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          <div className="gmap-addr">
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">5139 Geary Blvd</div>
                <div>
                  <span itemProp="addressLocality">San Francisco</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">94118</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(415) 358-4887</div>
            </div>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&sll=33.660639,-117.873344&sspn=0.068011,0.110035&ie=UTF8&ll=37.790388,-122.470522&spn=0.02374,0.036478&z=14&output=embed"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
