// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { FaAccessibleIcon, FaExclamationCircle } from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { Contact } from "../components/elements/forms/Form_Contact"
import { Reviews } from "../components/elements/home/Home_Reviews"
import { SEO } from "../components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ContactPage({ location }) {
  return (
    <LayoutDefault {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Contact Bisnar Chase Personal Injury Attorneys"
        pageDescription="Contact the California personal injury attorneys of Bisnar Chase."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Contact Bisnar Chase</h1>
        <BreadCrumbs location={location} />
        <div className="address-wrapper">
          <h2>BISNAR | CHASE PERSONAL INJURY ATTORNEYS</h2>
          <p>1301 Dove St. #120, NEWPORT BEACH, CA 92660</p>
          <p>
            <span className="orange">LOCAL:</span> (949) 203-3814{" "}
            <span className="orange">TOLL FREE:</span> (800) 561-4887
          </p>
        </div>
        <div className="grid-wrapper">
          <Contact />
          <div className="iframe-holder">
            <iframe
              width="100%"
              height="100%"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d53131.77654620503!2d-117.86867405240199!3d33.66400091536968!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1423772603989"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </div>
        </div>
        <div className="text-wrapper">
          <div className="warning">
            <FaExclamationCircle className="warning-icon" />
            <h3 className="handicap-warning">
              No attorney client relationship is established by submitting this
              initial contact information to our office.The forms are intended
              to assist Bisnar Chase with a preliminary evaluation of your case.
              You may reach our office Monday- Friday 8:30 am to 5:00pm.
            </h3>
          </div>
          <div className="handicap">
            <FaAccessibleIcon className="handicap-icon" />
            <div className="handicap-text-wrap">
              <h3>Unable to Come In? We'll Come To You!</h3>
              <p>
                In the event that your injury makes traveling difficult, we can
                arrange to visit you in your home or hospital.
              </p>
            </div>
          </div>
        </div>
        <Reviews invert={true} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .iframe-holder {
    background-color: #eee;

    iframe {
      @media (max-width: 700px) {
        height: 100%;
      }
    }
  }

  .address-wrapper {
    text-align: center;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-weight: 600;
    }

    h2 {
      border-bottom: none;
      font-size: 2rem;
      margin-bottom: 0.3rem;

      @media (max-width: 500px) {
        font-size: 1.2rem;
      }
    }
    p {
      text-align: center;
      font-weight: 600;

      @media (max-width: 500px) {
        font-size: 1rem;
      }
      &:nth-of-type(1) {
        margin-bottom: 0;
      }
    }
  }
  .grid-wrapper {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;

    @media (max-width: 1024px) {
      grid-template-columns: 1fr;
      grid-template-rows: 1fr 0.5fr;
    }
  }

  div.text-wrapper {
    border: thick double ${({ theme }) => theme.colors.grey};
    margin-top: 2rem;
    display: grid;
    grid-template-columns: 1fr 1fr;
    padding: 2rem;

    @media (max-width: 1024px) {
      grid-template-columns: 1fr;
      grid-template-rows: auto auto;
      align-items: center;
    }

    @media (max-width: 550px) {
      padding: 0;
    }

    .warning {
      display: grid;
      grid-template-columns: 1fr 8fr;
      align-items: center;
      padding: 2rem 0;

      @media (max-width: 550px) {
        grid-template-columns: 1fr;
        grid-template-rows: auto auto;
        justify-items: center;
        padding: 2rem 0 !important;
      }

      @media (max-width: 1024px) {
        padding: 0 2rem;
      }

      .warning-icon {
        font-size: 10rem;
        color: red;

        @media (max-width: 550px) {
          font-size: 8rem;
        }
      }
    }

    .handicap-warning {
      padding: 2rem;
      text-align: justify;
    }

    .handicap {
      display: grid;
      grid-template-columns: 1fr 8fr;
      align-items: center;
      padding: 2rem 0;

      @media (max-width: 550px) {
        grid-template-columns: 1fr;
        grid-template-rows: auto auto;
        justify-items: center;
        padding: 2rem 0 !important;
      }

      @media (max-width: 1024px) {
        padding: 0 2rem;
      }

      .handicap-text-wrap {
        padding: 2rem;
      }

      .handicap-icon {
        font-size: 10rem;
        color: #004c97;

        @media (max-width: 550px) {
          font-size: 8rem;
        }
      }
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
