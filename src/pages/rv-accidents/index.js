// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "rv-motorhome-lawyers-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California RV & Motorhome Accident Lawyers | The Law Firm of Bisnar Chase"
        pageDescription="If you have been severely injured in a motorhome accident contact the RV & Motorhome Lawyers of Bisnar Chase today. The Law Firm of Bisnar Chase can earn you the compensation you need for medical bills and lost wages. Call 1-800-561-4887 to receive a free consultation from a top-notch legal team member. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California RV & Motorhome Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="RV/Motorhome Accident Lawyers"
            title="RV/Motorhome Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> California RV & Motorhome Accident Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          have been helping families recuperate after their disastrous
          recreational vehicle crash for <strong> 40 years</strong>.
        </p>
        <p>
          Families nationwide find that renting a motorhome is a less expensive
          alternative for a vacation. Unfortunately, many families are not
          well-experienced in driving RV's and put their passengers and other
          drivers on the road at risk.
        </p>
        <p>
          The injury attorneys of Bisnar Chase have brought reckless RV drivers
          to justice and{" "}
          <strong> continue to win millions in compensation for victims</strong>{" "}
          who have been involved in a motorhome crash. Bisnar Chase's expert
          legal team members are on standby to take your call.
        </p>
        <p>
          <strong>
            {" "}
            Upon your call you will also receive a thorough case evaluation for
            free{" "}
          </strong>
          .
        </p>
        <p>
          <strong> Call 1-800-561-4887 </strong>.
        </p>
        <p>
          Our professionals will be here to answer any of your questions
          throughout the legal process. Contact us today.
        </p>
        <h2>Do I Have a Motorhome Accident Case?</h2>
        <p>
          If there is a substantial amount of evidence to prove that there was
          carelessness on the behalf of the RV driver the claim can qualify for
          a strong RV accident case.
        </p>
        <LazyLoad>
          <img
            src="/images/driving-motor-home.jpg"
            width="100%"
            alt=" Motorhome injury lawyers in California"
          />
        </LazyLoad>
        <p>
          One of the important elements most motorhome drivers do not know is
          that they must possess a specific license when driving the vehicle.
        </p>
        <p>
          The type of license that an RV driver must possess is a Class B
          license. A Class B license states that the driver is certified to
          operate a vehicle that weighs 26,001 pounds or more. If the driver was
          not certified to drive the motorhome then this can prove to be a form
          of negligence.
        </p>
        <p>
          <strong>
            {" "}
            Other factors that can strengthen your RV claims is if you prove the
            driver was
          </strong>
          :
        </p>

        <ul>
          <li>Recklessness with turns</li>
          <li>Failed to pay attention to an RV's blind spot</li>
          <li>Overloaded the motorhome</li>
          <li>Speeding</li>
          <li>Drunk driving</li>
        </ul>
        <h2>Legal Steps to Take After a Recreational Vehicle Crash </h2>
        <p>
          The average motorhome weighs 5,200 pounds. A vehicle this big can
          cause extreme damage to the surrounding property and leave many
          victims to endure severe pain and suffering. Data reports that over 10
          million families own an RV putting not only adults at risk but
          children as well. There are actions that can be taken to ensure that
          you and your families rights are protected.
        </p>
        <p>
          <strong> Legal actions to take after a motorhome accident</strong>:
        </p>
        <p>
          <strong> File a claim</strong>: You need to file a claim with your
          insurance company before leaving the scene of the accident. This is
          the first step you need to take when beginning the legal process. The
          way in which you can strengthen your claim is by giving the insurance
          agency the name, driver license number and insurance company of the
          party that was also involved in the accident. What will also help your
          claim is if you had taken any pictures of the crash or have gathered
          any witnesses at the scene of the accident.
        </p>
        <p>
          <strong>
            {" "}
            Follow up with your insurance agent with the process of your claim
          </strong>
          : The length that the claim will take to process depends on how much
          property was damaged and the severity of injuries the victims of the
          crash have suffered. Stay involved in the process of your case so you
          have a better understanding of the amount that you will be compensated
          or what you will have to pay out of pocket.
        </p>
        <p>
          <strong>
            {" "}
            Contact a California RV & Motorhome Accident Attorney
          </strong>
          : If you feel that you are not receiving the compensation you deserve
          from your motorhome crash contact the
          <strong> California RV & Motorhome Accident Lawyers</strong> of Bisnar
          Chase. We have taken on the most complex cases and navigated through
          every challenge to get you the earnings that you need to recover from
          your massive wreck. <strong> Call 1-800-561-4887</strong> to further
          your knowledge on how you can obtain the medical bills and other
          financial earnings you are entitled to.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="RV collsion attorney in California"
            style={{
              backgroundImage: "url('/images/doctor-x-ray-photo-image.jpg')"
            }}
          >
            <h2>Compensation for Injuries</h2>
          </div>
        </LazyLoad>
        <p>
          The{" "}
          <Link to="https://www.fmcsa.dot.gov/" target="_blank">
            {" "}
            Federal Motor Carrier Safety Administration
          </Link>{" "}
          reported that 26 deaths take place annually due to motorhome crashes.
          Data has also shown that in one year, 75,000 people had experienced an
          RV accident-related injury. Injuries from these incidents may need
          intensive surgery, rehabilitation or continuous at-home care.
        </p>
        <p>
          <strong>
            {" "}
            The following are injuries that can be compensated for after a
            recreational vehicle wreck
          </strong>
          .
        </p>
        <p>
          <strong> Brain damage</strong>: A blow to the head from an auto
          accident can cause irreparable damage to the brain. It can lead to
          victims to live with disabilities for all their lives or victims can
          be left in a vegetative state. Common symptoms of minor head injuries
          include depression, forgetfulness and extreme mood swings. Seek
          medical attention for minor signs of a head injury. Side effects from
          minor brain injuries can lead to extreme disturbances later in life.
        </p>
        <p>
          <strong>
            {" "}
            <Link to="/catastrophic-injury/nerve-damage" target="_blank">
              {" "}
              Nerve Damage
            </Link>
          </strong>
          : Typical signs of nerve damage include a tingling sensation in
          extremities, the inability to speak or paralysis. Types of nerve
          damage are sectioned into three different categories which include
          autonomic, motor and sensory nerve damage.
        </p>
        <p>
          <em>Each serves a different function in the body</em>:
        </p>
        <p>
          <em>Sensory nerves</em> command the sensations, for example, being
          able to feel a prick in the finger.
        </p>
        <p>
          <em>Autonomic nerves</em> are responsible for the functioning of
          systems of the body which include blood pressure and heart rate.
        </p>
        <p>
          <em>Motor nerves</em> govern the movement of the body. It is
          responsible for signals transmitting through the spinal cord and
          brain.
        </p>
        <p>
          <strong> Back injuries</strong>: In the United States injury victims
          spend an average amount of $50 billion dollars on back pain. People
          who suffer from severe back pain do not only experience pain in their
          back but also in their neck, legs or arms. Treatment for back injuries
          can involve at home remedies such as leaving hot/cold compressions on
          the area of pain, physical therapy and if necessary, surgery.
        </p>
        <p>
          <strong> Broken bones</strong>: Bones that are commonly broken in
          accidents are ribs, arms and legs. Since the weight of the motorhome
          is heavier than that of a commercial vehicle, the body is more likely
          to suffer more damage from an accident.
        </p>
        <p>
          <strong> Dental injuries</strong>: It is not abnormal for injury
          victims to experience damage to their teeth after a motorhome crash.
          Teeth can either become chipped or entirely lost in a RV collision.
          Once a tooth has been loosened the victim has a two-hour window to
          receive treatment to recover the tooth. Cosmetic procedures on the
          mouth can be costly and most insurance companies do not cover it. The
          injury attorneys of Bisnar Chase can gain you the finances you need to
          cover your dental cosmetic procedures. Contact us at 1-800-561-4887 to
          receive a free case analysis.
        </p>
        <h2>Common Causes for a Recreational Vehicle Accident</h2>
        <LazyLoad>
          <img
            src="/images/motorhome-rv-image.jpg"
            width="300"
            className="imgright-fluid"
            alt="California RV Crash Lawyers"
          />
        </LazyLoad>
        <p>
          Due to its size a motorhome can be difficult to operate. It is
          important that drivers proceed with care and caution to prevent a
          disastrous accident. If not, many people can end up with serious
          injuries. Below are the primary elements that cause recreational
          vehicle collisions.
        </p>

        <p>
          <strong> Common causes for a recreational vehicle accident</strong>:
        </p>

        <ul>
          <li>Amateur drivers</li>
          <li>Speeding</li>
          <li>Drowsy driving</li>
          <li>Miscalculated turns</li>
          <li>
            Under or Overestimating the stopping distance before an intersection
          </li>
          <li>Defective motorhome parts</li>
        </ul>

        <h2>RV's and the California Lemon Law</h2>
        <p>
          Just like commercial drivers have laws placed for them, so do avid
          motorhome drivers. The California{" "}
          <Link
            to="https://www.dmv.org/ca-california/automotive-law/lemon-law.php"
            target="_blank"
          >
            {" "}
            lemon law
          </Link>{" "}
          serves to protect newly purchased recreational vehicles that have
          defective parts. The Lemon Law states that the manufacture of the
          vehicle must replace the defective car parts or repurchase the
          motorhome back from the consumer.
        </p>
        <p>
          If a California resident has purchased their car from a different
          state, the lemon law would still apply to that defective vehicle.
        </p>
        <h2>Superior Legal Representation</h2>
        <LazyLoad>
          <img
            src="/images/office-meeting.jpg"
            className="imgright-fixed"
            alt=" RV incident attorneys in California"
          />
        </LazyLoad>
        <p>
          For over <strong> 40 years the RV & Motorhome crash attorneys</strong>{" "}
          of Bisnar Chase have been bringing negligent parties to justice and
          have won millions in compensation for our clients.
        </p>
        <p>
          You and your family members should not have to pay out-of-pocket for
          someone else's carelessness.
        </p>
        <p>
          The law firm of Bisnar Chase{" "}
          <strong> holds a 96% success rate of winning cases</strong> and we do
          not show signs of stopping.
        </p>
        <p>
          When you call today you will receive a{" "}
          <strong> free consultation for your injury case</strong>.
        </p>
        <p>Your suffering should end at your accident.</p>
        <p>
          <strong> Call 1-800-561-4887</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br></br>1301 Dove St. #120{" "}
          <br></br>Newport Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
