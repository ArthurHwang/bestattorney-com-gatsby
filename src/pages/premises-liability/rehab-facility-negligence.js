// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "facility-negligence/california-drug-and-rehab-negligence-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Rehab Facility Negligence Lawyers – Bisnar Chase"
        pageDescription="The California drug and rehab facility negligence lawyers of Bisnar Chase are here to help anyone who has suffered due to the negligence of rehab centers and staff. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Rehab Facility Negligence Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California drug and rehab negligence lawyer"
            title="California drug and rehab negligence lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <b>California drug and rehab facility negligence lawyers</b> of
          Bisnar Chase are dedicated to making sure justice is done in rehab
          harm cases. When a person has suffered due to the negligence of a
          rehab facility or its staff, we can help.
        </p>

        <p>
          As a person is checked into a rehabilitation center, they are placing
          their life in the hands of the staff. That clinic has a vital duty of
          care. But if they are negligent in their duties, it can have
          catastrophic results.
        </p>

        <p>
          Negligence can take different forms, but it boils down to a treatment
          facility failing in its duty to patients.
        </p>

        <p>
          For expert help and support, contact our California drug and rehab
          facility negligence attorneys. They treat every case and client with
          the compassion they deserve, and are dedicated to seeing justice done.
        </p>

        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814</Link> now for
          immediate help, or click here to contact{" "}
          <Link to="/" target="new">
            Bisnar Chase Personal Injury Attorneys
          </Link>
          .
        </p>

        <div className="mb">
          <Link to="/premises-liability/rehab-facility-negligence#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Rehab Facility Dangers <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is Rehab Facility Negligence? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Dangerous is Rehab Facility Negligence?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Types of Rehab Facility Negligence <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Case Study: $11 Million Verdict After Rehab Center Suicide{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Is Rehab Center Negligence Common? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Rehab Center Negligence: What is Narconon?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              When to Sue a Drug or Alcohol Rehab Clinic{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Information Does Your Rehab Facility Negligence Lawyer Need?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
          <Link to="/premises-liability/rehab-facility-negligence#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Bisnar Chase is the Right Firm for You{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">Rehab Facility Dangers</h2>

        <p>
          Before discussing rehab center negligence – and how a skilled rehab
          negligence lawyer can bring wrongdoers to justice – it is important to
          know what a rehab center does.
        </p>

        <p>
          When a person is struggling with an addiction and seeking treatment,
          they may turn to supposed experts for help.
        </p>

        <p>
          There are different kinds of rehab centers, designed to meet different
          needs. The two main types are:
        </p>

        <p>
          <b>
            <u>Inpatient facilities</u> –
          </b>{" "}
          A type of live-in center which patients will stay at 24 hours per day
          for the duration of their treatment. Patients can check themselves in,
          or may be checked in by another person in extreme circumstances.
        </p>

        <p>
          <b>
            <u>Outpatient Facilities</u> –
          </b>{" "}
          Patients can live normal lives while visiting an outpatient center for
          scheduled treatments. Many patients start out by staying at an
          inpatient facility, before transitioning to outpatient clinic care.
        </p>

        <p>
          Some of the steps for recovery that you are likely to experience at an
          inpatient rehab center in California include:
        </p>

        <ul>
          <li>
            <b>Detox –</b> Short for detoxification. This is the process of
            clearing a person's body of an addictive drug or substance.
          </li>
          <li>
            <b>Rehabilitation –</b> Designed to change the patient’s mindset and
            get to the root of their harmful substance use. Can include
            individual and group therapy sessions, sports, activities and more.
          </li>
          <li>
            <b>Aftercare –</b> Ongoing support, designed to develop coping
            mechanisms and reinforce the progress made through rehabilitation.
          </li>
        </ul>

        <p>
          Most lawsuits are launched due to facility negligence while patients
          are going through detox.
        </p>

        <p>
          When patients are detoxing they are often at their most vulnerable.
          This is when they need the most support. Being in the care of a
          negligent facility could put that patient in real danger.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="580"
            src="https://uw-media.usatoday.com/embed/video/1379092002?placement=snow-embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2 id="header2">What is Rehab Facility Negligence?</h2>

        <p>
          Rehab facility negligence refers to the failure of staff members to
          provide the proper level of care for their patients.
        </p>

        <p>
          This can include failures in care, supervision, medicating and more.
        </p>

        <p>
          California has strict laws in place to protect patients while they are
          living in a 24-hour rehab facility.
        </p>

        <p>
          The Elder Abuse and Dependent Adult Civil Protection Act is a long and
          complicated piece of legal legislation. But it has a couple of key
          guidelines that relate specifically to rehab clinics. As far as the
          law is concerned, those who are living in rehab facilities are classed
          as ‘dependent adults.’
        </p>

        <p>
          The law states that dependent adults who are receiving treatment
          should not be subjected to:
        </p>

        <ul>
          <li>
            <i>
              Abuse, neglect, abandonment, or any other behavior which could
              cause physical or mental harm.
            </i>
          </li>
          <li>
            <i>
              A carer or member of staff depriving them of goods or services
              that are necessary to avoid physical or mental harm.
            </i>
          </li>
        </ul>
        <p>
          If anyone deprives a patient of the services they need to detox
          safely, it is a clear case of negligence.
        </p>

        <div className="rehab-negligence-border">
          <p>
            <b>Medical Malpractice:</b> It is important to note that rehab
            facility negligence refers to the actions and mistakes of rehab
            center staff. It does not cover medical malpractice. This falls
            under a different type of lawsuit which Bisnar Chase does not
            handle.
          </p>

          <p>
            If you are unsure whether your case is a result of negligence or
            medical malpractice, call our California drug and rehab facility
            lawyers for guidance.
          </p>
        </div>

        <h2 id="header3">How Dangerous is Rehab Facility Negligence?</h2>

        <p>Rehab facility negligence can be fatal.</p>

        <p>
          It is a huge step for a person to take the plunge and accept the help
          of a rehab center. Different people become aware that they need help
          in fighting addiction at different moments.
        </p>

        <p>
          Some people might check themselves into a rehab center once they
          become aware that they are losing control, or are dependent on an
          addictive substance. Others need to hit rock bottom before they will
          accept help.
        </p>

        <p>
          When patients are in this helpless state, they need the highest
          quality of support, supervision and care to get through it.
        </p>

        <p>
          When a rehab facility’s staff is negligent in its duty to provide that
          care, it can have disastrous consequences. These may include
          self-harm, suicide, and other serious side effects.
        </p>

        <h2 id="header4">Types of Rehab Facility Negligence</h2>

        <p>
          There are several types of negligence which can occur in California
          drug and alcohol rehab facilities. These include:
        </p>

        <ul>
          <li>
            <b>Sub-par Patient Evaluation</b>

            <p>
              When a patient is checked into a rehabilitation facility, the
              staff should be fully aware of their personal state and situation.
              This will allow them to provide the care and treatment needed. A
              failure to do so constitutes negligence and can result in a
              dangerous situation for the patient.
            </p>
          </li>

          <li>
            <b>Supervision</b>

            <p>
              Rehab patients are often in a particularly vulnerable state. In
              some cases, they may even be suicidal. Whether they are at a drug
              or alcohol rehabilitation center, it is vital that such patients
              are given proper support and supervision for their own safety.
            </p>
          </li>

          <li>
            <b>Failure to provide a safe environment</b>

            <p>
              A rehab facility must be kept in a safe condition for its
              patients. It must be kept clean and hygienic, with steps taken to
              minimize situational risks. These can include slip-and-fall
              accidents and hostile patient interactions. This could also mean
              keeping patients with a tendency for harm or self-harm away from
              potentially-dangerous objects.
            </p>
          </li>

          <li>
            <b>Unfit Staff</b>

            <p>
              Every rehab center must make sure it has the right staff in place
              to provide a safe and caring environment for patients. Staff must
              be properly qualified and trained. If a facility does not have
              suitable staff in place, it may be subject to vicarious liability.
            </p>
          </li>

          <li>
            <b>Abuse</b>

            <p>
              Drug and alcohol rehab center patients can also be harmed through
              abuse. This is not a product of negligence, but is still a serious
              danger. It can involve physical, mental, or even sexual abuse.{" "}
              <Link to="/nursing-home-abuse" target="new">
                Nursing home abuse
              </Link>{" "}
              has long been a problem in elderly care facilities, and those same
              issues apply in rehab centers. As with nursing home neglect and
              abuse, such rehab harm can deeply affect patients, causing
              physical and emotional distress.
            </p>
          </li>
        </ul>

        <h2 id="header5">
          Case Study: Bisnar Chase Wins $11 Million Verdict After California
          Rehab Center Suicide
        </h2>
        <LazyLoad>
          <img
            src="/images/facility-negligence/california-rehab-negligence-attorney.jpg"
            width="40%"
            className="imgleft-fluid"
            alt="Personal injury lawyer H. Gavin Long of Bisnar Chase law firm."
          />
        </LazyLoad>
        {/* <div className="item">
          <span className="caption">
            <i>Personal injury trial lawyer H. Gavin Long of Bisnar Chase.</i>
          </span>
        </div> */}

        <p>
          Bisnar Chase won a huge $11 million verdict for the family of a man
          who died by suicide while attending rehab. His{" "}
          <Link
            to="https://www.prlog.org/12768353-bisnar-chase-secures-11-million-jury-verdict-for-wife-and-children-of-man-who-died-by-suicide-while-in-rehab.html"
            target="new"
          >
            death was caused by facility negligence
          </Link>
          .
        </p>

        <p>
          John Cunningham, 58, was checked in to a residential rehab facility in
          California in 2015. He had been battling a dependence on prescription
          drugs such as Ativan and Xanax for about four years.
        </p>

        <p>
          The facility was informed of Mr. Cunningham’s history of suicidal
          thoughts and hospitalizations. However, serious negligence was shown
          in the handling of his detox period.
        </p>

        <p>
          Mr. Cunningham was taken to hospital three times in five days due to
          the severity of his withdrawal symptoms. He was kept isolated while
          detoxing and was deprived of his depression medication. Staff failed
          to properly supervise or support the patient, and Mr. Cunningham
          committed suicide after being left alone for several hours.
        </p>

        <p>
          The rehab center was linked to the Church of Scientology and used the
          practice of Narconon. This is a method of addiction treatment which is
          widely regarded as dangerous by medical experts. Mr. Cunningham’s
          family, which had been unaware of the Scientology link before his
          death, contacted the rehab facility negligence lawyers of Bisnar Chase
          for help in seeking justice.
        </p>

        <p>
          John Cunningham’s wife and daughters were offered settlements of
          $100,000 and $350,000. But personal injury lawyer Gavin Long, of
          Bisnar Chase, decided to take the case to trial.
        </p>

        <p>
          He argued the case in front of a jury in 2019 and recorded a major
          victory. The jury returned a verdict awarding $11 million to the
          victim’s family members.
        </p>

        <p>
          Long said the verdict finally gives Mr. Cunningham’s family justice.
          It also sends a message to treatment facilities that they will be held
          accountable if they do not provide proper care to their patients.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wgdmqv922z8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <div className="snippet">
          <p>
            <span>
              Lawyer <Link to="/attorneys/gavin-long">H. Gavin Long</Link>,
              said:{" "}
            </span>
            “It is the job of treatment and rehab centers to care for people
            when they are at their most vulnerable. These facilities have a very
            strict duty of care for their patients. When they fall short of that
            duty, it can result in tragic consequences. This happens far too
            often, and it is vital that we hold them accountable.”
          </p>
        </div>

        <h2 id="header6">Is Rehab Center Negligence Common?</h2>

        <p>
          Southern California is a hotbed for drug and alcohol addiction
          treatment centers. In Orange County alone there are more than 300
          rehab clinics. That is just the tip of the iceberg, with many others
          running throughout Los Angeles County, Riverside County, and San
          Bernardino County.
        </p>

        <p>
          Southern California has a total of more than 1,100 registered rehab
          centers. That does not include the high numbers of unlicensed ‘sober
          living homes’ which continue to spring up.
        </p>

        {/* <iframe
          id="rehab-map"
          src="https://www.google.com/maps/d/embed?mid=1MRgAOPiBgCMirbc_YYiM09Y_hMM"
          width="640"
          height="480"
          width="100%"
        ></iframe>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/d/embed?mid=1MRgAOPiBgCMirbc_YYiM09Y_hMM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad> */}

        <p>
          The majority of these care facilities provide important services and
          expert help for those trying to beat a dependence on drugs or alcohol.
        </p>

        <p>
          But there are some rehab centers which fall far short of these
          expected standards. There are far too many examples of rehab facility
          negligence putting patients at risk of further harm – and even
          wrongful death.
        </p>

        <p>
          The Orange County Register produced a series of reports on the issues
          discovered at some{" "}
          <Link to="https://www.ocregister.com/rehab-riviera/" target="new">
            California rehab centers
          </Link>
          . Their findings show the extreme risk that rehab negligence victims
          face.
        </p>

        <p>
          A lack of regulation and inspection is blamed for the terrifying rise
          in poor-quality rehab centers putting patients at risk.
        </p>

        <ul>
          <li>
            Someone dies every two weeks on average while under the care of a
            licensed California rehab center.
          </li>
          <li>
            More than 500 complaints are launched regarding rehab clinic care
            every year in California.
          </li>
          <li>
            It is estimated that about 140 people died during detox at
            California rehab centers from 2012-2017.
          </li>
        </ul>

        <p>
          A California rehab facility negligence lawyer can help those who have
          been impacted by poor rehab practices.
        </p>
        <div className="rehab-negligence-border mb">
          <b>Did You Know?...</b> Costa Mesa has one of the highest per-capita
          rehab center rates, with more than 100 clinics for a population of
          112,000.
        </div>

        <h2 id="header7">Rehab Center Negligence: What is Narconon?</h2>

        <p>
          {" "}
          <Link to="https://en.wikipedia.org/wiki/Narconon" target="new">
            Narconon
          </Link>{" "}
          is a term which has been linked with some rehab center negligence
          cases.
        </p>

        <p>
          Narconon is an organization which runs dozens of residential rehab
          centers across the world, using a specific method of addiction
          treatment. It is strongly associated with the Church of Scientology
          and has proven to be extremely controversial.
        </p>

        <p>
          The Narconon method is based on the theories of L. Ron Hubbard – the
          founder of Scientology. It claims that addiction is caused by traces
          of a drug being stored in the body. Narconon believes that addiction
          can be cured by flushing those traces out.
        </p>
        <LazyLoad>
          <img
            src="/images/facility-negligence/rehab-negligence-lawyers-in-california.jpg"
            width="300"
            alt="The web page for the Church of Scientology, which is closely linked to Narconon addiction treatment."
            className="imgleft-fixed"
          />
        </LazyLoad>

        <p>
          But experts say no research exists backing Narconon as an effective
          practice. They say that the Narconon method is not based on scientific
          fact and can be medically unsafe.
        </p>

        <p>
          Several Narconon facilities in California have been cited by
          inspectors. Their violations include giving patients medication
          without being authorized to do so, keeping alcohol on-site, and
          failing to provide proper bedding.
        </p>

        <p>
          In addition, there have been several publicized deaths at Narconon
          facilities, as well as a host of lawsuits against the organization.
        </p>

        <p>
          Many cases are settled out of court, but Gavin Long of Bisnar Chase is
          one of the few drug rehab negligence lawyers in California to
          challenge Narconon in court – winning a large jury verdict for the
          family of John Cunningham.
        </p>

        <h2 id="header8">When to Sue a Drug or Alcohol Rehab Clinic</h2>

        <p>
          A rehab facility negligence attorney should be hired when a patient
          has been harmed due to the negligent action or inaction of a rehab
          center’s staff.
        </p>

        <p>
          As we have outlined, negligence can occur in a wide range of ways. No
          matter what form it takes, it can seriously impact patients. When this
          happens, the patient or their family can seek justice through the
          legal system.
        </p>

        <p>
          A skilled California rehab negligence lawyer can help clients secure
          compensation and make sure these facilities are held liable for their
          failings.
        </p>

        <h2 id="header9">
          What Information Does Your Rehab Facility Negligence Lawyer Need?
        </h2>

        <p>
          The law firm of Bisnar Chase will always assess the details of a case
          before taking it on.
        </p>

        <p>
          Our expert rehab negligence attorneys will look to build a clear
          picture of how the detox/rehab death or injury occurred. This is vital
          in developing an effective legal case.
        </p>

        <p>They will need to know:</p>

        <ol>
          <li>
            <span>The rehab or detox facility involved.</span>
          </li>
          <li>
            <span>How the victim came to be harmed.</span>
          </li>
          <li>
            <span>
              How the victim came to be at the facility. Was it a voluntary
              check-in, and was payment involved?
            </span>
          </li>
          <li>
            <span>The background of the victim:</span>
          </li>
          <ul>
            <li>Family life</li>
            <li>How they came to be addicted</li>
            <li>How the addiction was sustained</li>
            <li>Did they have any previous rehab or detox facility history?</li>
            <li>
              Have they previously had suicidal thoughts or attempted suicide?
            </li>
          </ul>
        </ol>

        <p>
          This information will allow our specialists to properly assess your
          case and offer expert help and guidance.
        </p>

        <h2 id="header10">Bisnar Chase is the Right Firm for You</h2>

        <p>
          Bisnar Chase has established itself as a premier California law firm
          over the last 40 years. Our skilled attorneys are extremely successful
          in securing justice for their clients. They have collected more than
          $500 million for their clients, with an outstanding 96% success rate.
        </p>

        <p>
          Beyond compensation, our firm believes in providing superior
          representation with a personal touch. We understand that addictions
          and rehab can be sensitive subjects that deeply impact families and
          individuals.
        </p>

        <p>
          Our California rehab negligence lawyers treat every case and client
          with the compassion that they deserve. They build strong
          attorney-client relationships and provide the support needed to get
          through their ordeal.
        </p>

        <p>
          Call Bisnar Chase now on{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>, or click here to{" "}
          <Link to="/contact" target="new">
            contact us
          </Link>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
