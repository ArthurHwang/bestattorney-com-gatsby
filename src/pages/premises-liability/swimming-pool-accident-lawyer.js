// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-swimming-pool-accident-attorneys.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Swimming Pool Accident Attorneys - Drowning Lawyers"
        pageDescription="Call 949-203-3814 for top-rated California Swimming Pool Accident Attorneys. Our drowning lawyers have the reputation, assets and experience to handle serious pool injury and drowning cases. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Swimming Pool Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california swimming pool accident attorneys"
            title="california swimming pool accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          At Bisnar Chase, we have more than three decades of experience
          protecting the rights of fatal and non-fatal swimming pool victims. We
          have recovered hundreds of millions of dollars in compensation for
          individuals and families that have been deeply affected by another
          person or entity's negligence or wrongdoing.
        </p>
        <p>
          If you or someone you love has been injured in a swimming pool
          accident, please contact our experienced California drowning lawyers
          at <strong> 800-561-4887</strong> for a free and comprehensive
          consultation. Absolute privacy and compassion guaranteed.
        </p>
        <h2>Drowning Accidents in California</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/swimming-pool-accident.jpg"
              alt="swimming pool accident lawyer"
              className="imgright-fixed"
            />
          </LazyLoad>
          There are few activities as refreshing and fun as taking a dip in a
          swimming pool. It is so much fun that we often forget just how
          dangerous swimming can be.
        </p>
        <p>
          Hundreds of people, especially young children, are killed in{" "}
          <Link
            to="https://www.cdc.gov/homeandrecreationalsafety/water-safety/waterinjuries-factsheet.html"
            target="new"
          >
            unintentional drowning accidents
          </Link>{" "}
          in California each year.
        </p>
        <p>
          In fact, drowning is the most common cause of unintentional death for
          children between 0 and 4 years old in California. In addition to
          tragic drowning fatalities, swimming pool accidents can also result in
          near-drowning, which can leave a victim brain-injured or severely
          disabled.
        </p>
        <p>
          Fault and liability issues in swimming pool accident cases can be
          complex and challenging. If you or someone you love has been injured
          or has died as the result of someone else's negligence, it is
          important that you understand your legal rights and options.
        </p>
        <p>
          An experienced California swimming pool accident attorney may be able
          to help you get compensation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california swimming pool accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/swimming-pool-accident-statistics.jpg')"
            }}
          >
            <h2>California Swimming Pool Accident Statistics</h2>
          </div>
        </LazyLoad>

        <p>
          According to the{" "}
          <Link to="https://www.cdph.ca.gov/" target="new">
            California Department of Public Health
          </Link>
          :
        </p>
        <ul>
          <li>
            There were 396 drowning deaths in California in the year 2010.
          </li>
          <li>
            During that same year, 1,393 people were either hospitalized or
            treated and released for swimming pool accidents in California.
          </li>
        </ul>
        <p>
          {/* <!-- {" "}<Link to="/child-injury-lawyer.html" target="_blank"> Young children make up a significant portion of the fatally and seriously injured victims</Link>. --> */}
        </p>
        <ul>
          <li>
            In the year 2010, children under 10 accounted for about half of the
            injuries suffered in California swimming pool accidents.
          </li>
          <li>
            In the following year, 30 children 5 years of age or younger were
            killed.
          </li>
          <li>
            Another 18 children under 6 years of age were killed in non-pool
            drowning accidents involving:
            <ul>
              <li>Hot tubs</li>
              <li>Bathtubs</li>
              <li>Toilets</li>
              <li>Buckets of water</li>
              <li>Other bodies of water.</li>
            </ul>
          </li>
        </ul>
        <h2>Top 10 Symptom of Near Drowning</h2>
        <p>
          Not all victims of drowning accidents are killed. Near drowning is
          when a victim is rescued before suffering fatal injuries. There are
          many symptoms of near drowning, including:
        </p>
        <ol reversed>
          <li>Confusion</li>
          <li>Coughing up pink sputum</li>
          <li>Lethargy</li>
          <li>Restlessness</li>
          <li>Gasping respirations</li>
          <li>Unconsciousness</li>
          <li>Vomiting</li>
          <li>Irritability</li>
          <li>Abdominal distention</li>
          <li>Traumatic Brain Injury</li>
        </ol>
        <p>
          If the drowning victim stopped breathing and is revived, it is
          possible that he or she has suffered permanent brain damage because of
          the lack of oxygen. These types of nonfatal drowning injuries, in
          addition to severe brain damage, may also result in long-term
          disabilities such as memory issues, learning disabilities and
          permanent loss of basic functioning, also known as permanent
          vegetative state.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/YEZ7XBr1IiM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Dry Drowning</h2>
        <p>
          Dry drowning, otherwise known as secondary drowning, occurs at least
          1-24 hours after water inhalation. Secondary Drowning (dry drowning)
          takes place when water seeps into the lungs and then induces swelling
          that makes it difficult for oxygen to flow.
        </p>
        <p>
          {" "}
          <Link
            to="https://www.popsci.com/secondary-and-dry-drowning#page-3"
            target="_blank"
          >
            {" "}
            What parents need to know about dry drowning
          </Link>{" "}
          is that children are usually the victims of this rare occurrence and
          can lead to a child to having trouble breathing and may even result in
          death. Over 1%-2% of children experience dry drownings and even though
          it doesn't happen often, it is strongly suggested that you pay close
          attention to the signs.
        </p>
        <p>
          <strong> 5 signs of dry drowning</strong>:
        </p>
        <ol>
          <li>
            <strong> Coughing</strong>: Your child coughing excessively is a
            sign that his/her lungs are struggling to let oxygen pass through
            their airwaves. Coughing is related to your child’s difficulty to
            breath.
          </li>

          <li>
            <strong> Exhaustion</strong>: If your child was energetic in the
            pool but suddenly is sleepy or tired this is a prominent sign of dry
            drowning. Your child being drained abruptly of energy means that
            they are not receiving oxygen to the blood.
          </li>

          <li>
            <strong> Vomiting</strong>: When the body is lacking oxygen and is
            suddenly experiencing inflammation this is a symptom of dry
            drowning. Gagging and coughing is a sign that the body is under
            extreme stress.
          </li>

          <li>
            <strong> Confusion</strong>: Whenever your child is acting out of
            character and is not remembering much, again this could be a warning
            that not enough oxygen is going to the brain. The child may also
            feel dizzy or sick.
          </li>

          <li>
            <strong> Heavy breathing</strong>: Nostrils flaring up and fast
            shallow breathing means that the lungs are working harder than
            normal. If your child is breathing so deeply to point that his/her
            ribs and collarbone are being exposed, you need to seek medical
            attention immediately.
          </li>
        </ol>
        <p>
          <strong>
            {" "}
            If your child is displaying any of these symptoms call 911 or go to
            the emergency room immediately.
          </strong>
        </p>
        <p>
          Preventive measures of dry drowning comes in supervision. Paying
          special attention to your child while swimming is important. Also if
          your child is learning how to swim take safety precautions by
          providing flotation devices and making sure that they stay in the
          shallow part of the pool.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/XmbOZo0Gsho"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Drowning Prevention</h2>
        <p>
          Under California law, all pool owners and landlords must put up an
          enclosure or fence around their pool. The fence has to be at least 5
          feet high and it must completely surround the pool.
        </p>
        <p>
          Gates must also have self-locking and self-latching gates. This will
          help prevent young children from wandering off and falling into the
          pool. Here are a few other important safety tips for all pool owners
          and swimmers:
        </p>
        <ul>
          <li>
            <strong> Never leave children unsupervised by a pool</strong>. Even
            if they are excellent swimmers, it is important that someone is
            nearby. If you are the supervising adult, remain focused on the
            children. Do not turn away to talk to someone or take a phone call.
          </li>
          <li>
            <strong> Avoid alcohol around the pool</strong>. The heat from the
            sun can increase the effects of alcohol. Individuals who are
            drinking are much more likely to drown.
          </li>
          <li>
            <strong> Take a water safety course</strong>. Swimmers of all
            abilities would be well advised to take a formal swimming lesson.
          </li>
          <li>
            <strong> Remain in arm's length</strong>. When there are young
            swimmers, it is necessary to be alongside them in the pool. Staying
            within arm's reach of an inexperienced swimmer could help save his
            or her life.
          </li>
          <li>
            <strong> Install pool and door alarms</strong>. Property owners
            would be well advised to install an alarm on their backdoor and on
            the pool fence gate.
          </li>
          <li>
            <strong> Learn CPR and have a phone nearby</strong>. In case of an
            emergency, it will be necessary to call for help and to perform CPR
            immediately.
          </li>
          <li>
            <strong> Use life jackets</strong>. Young and old inexperienced
            swimmers should be given life jackets instead of inflatable
            armbands. Floating devices can deflate and become useless.
          </li>
          <li>
            <strong> Properly maintain the pool area</strong>. Uneven paving and
            broken ladders can result in devastating falling accidents and
            drowning incidents.
          </li>
        </ul>
        <h2>Swimming Pool Liability Claims</h2>
        <p>
          Owning a pool comes with a lot of responsibility. It is possible to
          hold a pool owner liable for any injuries or fatalities that result
          from their failure to follow safety regulations.
        </p>
        <p>
          For example, if a child drowns in a pool that does not have proper
          fencing surrounding it, then, that pool owner can be held civilly
          liable for an accident that occurs. A pool owner cannot, however, be
          held accountable for every incident that occurs there.
        </p>
        <p>
          The victim or the victim's family will have to prove that the pool
          owner's negligence was a contributing factor in the accident.
        </p>
        <p>
          In general, there are three categories of swimmers that affect
          liability: invitee, licensee and trespasser. Property owners are
          legally responsible for providing reasonably safe conditions for all
          guests to their property. If an invitee to the premises is injured
          because of the negligence of the property owner, then, the victim may
          file a claim.
        </p>
        <p>
          Property owners are also required to provide reasonably safe
          conditions to persons who are allowed to enter the pool area for
          business reasons. Trespassers, however, do not have permission to be
          on the property and will therefore find it challenging to hold the
          property owners responsible for their injuries.
        </p>
        <p>
          Other potentially liable parties in swimming pool accidents may
          include private clubs, hotels, daycare programs, childcare centers and
          governmental agencies in charge of maintaining public pools. Depending
          on the circumstances of the incident, the at-fault party or entity can
          be held liable for the injuries, damages and losses.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/pool-accident-statistics.jpg"
            width="100%"
            alt="california pool accident law firm"
          />
        </LazyLoad>

        <h2>Compensation for Victims</h2>
        <p>
          Swimming pool accident victims can recover damages by filing a
          personal injury lawsuit. They can recover compensation for the
          following damages, depending on the nature and circumstances of the
          incident:
        </p>
        <ul>
          <li>
            <strong> Medical expenses</strong>: Serious injuries may end up
            costing millions for victims and their families. In some cases, the
            victim's insurance company may not cover the full cost of medical
            treatment and rehabilitation. A personal injury claim can seek
            compensation for all medical expenses and the cost of any future
            medical and rehabilitative treatment.
          </li>
          <li>
            <strong> Lost wages</strong>: Injured victims may be forced to miss
            significant time at work while recovering.
          </li>
          <li>
            <strong> Pain and suffering</strong>: These are non-economic damages
            including physical pain, emotional trauma, permanent injuries and
            disabilities and loss of life's enjoyment.
          </li>
          <li>
            <strong> Wrongful death</strong>: When an individual dies in a
            swimming pool accident, his or her family members may be able to
            file a wrongful death lawsuit against the at-fault party. Such
            claims usually cover medical expenses, funeral costs, lost future
            income and pain and suffering.
          </li>
        </ul>
        <p>
          <strong>
            {" "}
            To speak with a lawyer that specializes in near drownings and fatal
            swimming pool accidents, call toll free 949-203-3814 for free case
            review.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
