// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Slip & Fall Help for California Injury Victims - Bisnar Chase Law Firm"
        pageDescription="Slip & fall help for California injury victims. Find out steps to take after a slip and fall accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>What to Do if you Have Been Injured in a Slip-and-fall Accident</h1>
        <BreadCrumbs location={location} />
        <p>
          {/* <img
            src="/images/slip-and-fall-caution-sign.jpg"
            alt="caution! Wet floor sign"
            className="imgright"
          /> */}
          The experienced{" "}
          <Link to="/premises-liability/slip-and-fall-accidents">
            California slip-and-fall attorneys
          </Link>{" "}
          at Bisnar Chase have a long and successful track record of helping
          injured victims and their families obtain fair and full compensation
          for their damages and losses.
        </p>
        <p>
          If you or a loved one has been injured in a slip-and-fall accident,
          please contact us for a free comprehensive consultation at{" "}
          <strong> 949-203-3814.</strong>
        </p>
        <p>
          A slip-and-fall accident may result in serious or even catastrophic
          injuries and permanent disabilities. Slip-and fall victims may have to
          spend several days in the hospital and possibly months in
          rehabilitation and physical therapy. They may also incur tens of
          thousands of dollars in medical expenses and even more in lost wages
          due to time they must take off work to recuperate from their injuries.
          If you have suffered injuries in a slip-and-fall accident, there are
          several steps you can take to ensure that your legal rights and best
          interests are protected.
        </p>
        <h2>Measures to Take</h2>
        <p>
          The actions you take after being injured on someone else's property
          will directly affect your ability to receive financial compensation
          for your medical bills and other related damages. Immediately after
          falling, take a moment to assess the situation. Have you been hurt?
          Can you move without causing additional injuries? If you are only
          suffering from a minor bump that will not require medical attention,
          you should report the hazardous condition to the property owner to
          prevent future incidents. If, however, you have suffered a significant
          injury, there are a number of steps you should take to protect your
          rights:
        </p>
        <ul>
          <li>
            <strong> Call for help</strong>. If you have hurt you head, neck,
            back or legs, you should not risk getting up. Stay where you are and
            call out for help. If you have a cell phone, call 911.
          </li>
          <li>
            <strong> Notify the authorities</strong>. An ambulance should be
            called and the authorities should be notified that an injury
            accident occurred on the premises.
          </li>
          <li>
            <strong> File a report with management</strong>. If the accident
            occurred at a place of business, the property manager should be told
            right away. Ask to fill out an injury report and obtain a copy of
            the report before you leave.
          </li>
          <li>
            <strong> Collect contact information</strong>. Request the name,
            address and phone number of anyone who witnessed the accident. If
            you choose to file a claim, it could prove useful to have first-hand
            reports that back up your account of what caused the accident.
          </li>
          <li>
            <strong> Take photos</strong>. If you have a camera or a smart
            phone, take photos of where the accident occurred. Make sure to
            photograph the hazardous conditions. Was the floor wet? Did you trip
            on debris? Was the carpeting uneven? Did you trip on a broken stair?
            Were there signs, cones or caution tape around the location? If not,
            take a wide enough photo to show that no warnings were posted. It is
            also advisable to photograph your injuries.
          </li>
          <li>
            <strong> Seek immediate medical attention</strong>. Seeing a doctor
            right away will increase your chances of a full recovery.
            Furthermore, your medical records are legal documents that can be
            used in court to prove that you were injured.
          </li>
          <li>
            <strong> Keep your records</strong>. It is important to keep
            organized records of all the financial losses you have suffered
            after being injured. Keep track of your medical expenses as well as
            your prescription drug bills, lost wages and other related damages.
          </li>
          <li>
            <strong> Maintain a journal</strong>. If you sustained a serious
            injury, you may be able to receive financial compensation for the
            pain and suffering you undergo during the recovery process. Keep a
            daily journal of your struggles since being injured. Are there
            important activities that you are no longer able to participate in
            because of your injuries? How has your life and livelihood been
            affected? Are you more stressed, depressed or fatigued since the
            accident? Have you had to hire a housekeeper or a gardener because
            you have been unable to cook or maintain the house? These are
            important pieces of information you would be well advised to
            document.
          </li>
        </ul>
        <h2>Liability in Slip-and-Fall Cases</h2>
        <p>
          Property owners and managers are responsible for keeping walkways
          clear and dry. They must repair damaged steps and railings. They are
          required to post warning signs around areas that are under repair.
          Property owners must also act within a reasonable amount of time to
          fix a hazardous location. Failure to do so is a form of negligence.
        </p>
        <p>
          In order to determine liability for a slip and fall accident, injured
          victims in California must prove that:
        </p>
        <ul>
          <li>The owner created the hazardous condition.</li>
          <li>The owner knew the condition existed and failed to fix it.</li>
          <li>
            The condition existed for a long enough period of time that the
            owner should have discovered it and corrected it before the
            slip-and-fall accident occurred.
          </li>
        </ul>
        <h2>Receiving Fair Compensation</h2>
        <p>
          It is common for property owners and insurance providers to deny
          responsibility for these types of accidents and for victims to
          struggle to get the compensation they need. An experienced California
          personal injury lawyer will fight for the victim's right to fair
          compensation for medical bills, lost wages, mental anguish, the cost
          of rehabilitation services and other related damages.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
