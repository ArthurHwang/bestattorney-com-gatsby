// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Making the Right Decisions When It Comes to Your Slip and Fall Claim"
        pageDescription="Every day people suffer serious slip and fall injuries. Find out what compensation you are entitled to and how to pursue a slip and fall claim in California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Making the Right Decisions When It Comes to Your Slip and Fall Claim
        </h1>
        <BreadCrumbs location={location} />
        <p>
          Every day, 25,000 people suffer a "
          <Link to="/premises-liability/slip-and-fall-accidents">
            slip and fall" injury
          </Link>
          . Over a half million "slip and fall" injuries a year require hospital
          care. On average, 55 people a day, die from a "slip and fall" event.
          (North American statistics). A winning "slip and fall" claim has four
          elements:
        </p>
        <p>
          (1) A fall resulting in significant injuries; (2) A dangerous
          condition that caused the fall; (3) A responsible party that has
          substantial assets or insurance; and (4) Economic reality.
        </p>
        <h2>Significant Injuries</h2>
        <p>
          A personal injury claim is the pursuit of a money recovery to
          compensate for injuries and losses that were suffered. The more
          serious the injury, the more money it will take to compensate the
          injured person. The more medical bills that were incurred and the more
          wages that were lost, the more it will take to compensate the injured
          person. The more serious the injuries and losses the more valuable the
          claim is.
        </p>
        <h2>Dangerous Condition</h2>
        <p>
          In order to hold someone responsible for your "slip and fall" losses,
          there must have been a "dangerous condition" that caused your fall.
          The dangerous condition must be specifically identified. A reasonable
          person seeing the condition must judge it to be unreasonably
          dangerous. The condition cannot be so obvious that a reasonable person
          would have appreciated the condition and avoided it.
        </p>
        <h2>Responsible Party</h2>
        <p>
          In order to collect damages for your "slip and fall" injury the
          responsible party will need to have the ability to pay a judgment.
          This usually means they have "premises liability" insurance or they
          have assets so substantial that they can pay the judgment themselves
          (like a major grocery store chain). A homeless person who creates a
          dangerous condition on the sidewalk that causes you a serious "slip
          and fall" injury is not going to compensate you for the losses you
          have sustained.
        </p>
        <h2>Economic Reality</h2>
        <p>
          A personal injury claim is the pursuit of compensation for losses
          suffered. The value of the losses must be substantially greater than
          value of the time, money and emotion that will be spent in the pursuit
          of the compensation. Most business establishments and insurance
          companies fight "slip and fall" cases vigorously. They have been known
          to spend more to defeat a claim than they could settle it for.
        </p>
        <p>
          California juries are generally skeptical of "slip and fall" claims.
          Unless the jury believes the conditions that caused the fall are truly
          dangerous, they aren't likely to rule in a claimant's favor. And if
          they do, they rarely award significant amounts unless they really
          believe the claimant deserves it and the defendant deserves to have to
          pay it.
        </p>
        <p>
          You can expect $25,000 in costs and hundreds of hours of law firm time
          to take a "slip and fall" case to trial, at a minimum. It takes one to
          five years from filing a lawsuit to get to trial, depending upon the
          county where the case is filed.
        </p>
        <p>
          For our law firm to take on a "slip and fall" case, the value of the
          case must exceed $100,000. There are law firms that will take smaller
          cases hoping to settle them without significant work or expenses. Most
          law firms cannot afford to take a small case to trial.
        </p>
        <p>
          An injured person can represent himself or herself in a claim in an
          attempt to reach a settlement with the responsible party. They also
          have the option of taking their case to small claims court. Small
          claims court is quick and relatively easy. There are plenty of
          websites that will assist you in processing your small claims case.
        </p>
        <h2>What is A "Slip and Fall" Injury?</h2>
        <p>
          A "slip and fall" injury is one that is caused when a person falls
          down, slips, or trips due to the unsafe condition of the property
          owned or controlled by another individual or business. It may include
          a fall caused by water, ice, snow, or a foreign object. Other times,
          falls may be caused by slippery or uneven sidewalks or pavement. No
          matter what the cause of the fall, the fall must have been caused by
          the negligence of another in order to have a claim for which the
          property owner or possessor will be responsible for the damages caused
          by your injury from the fall.
        </p>
        <p>
          Slip and fall is a broad category that is used to describe one of four
          kinds of fall down accidents: (1) Trip and fall, occurs when a foreign
          object is in the pathway (2) Stump and fall, occurs when an impediment
          is on the walking surface, such as an unexpected bump (3) Step and
          fall, occurs when an unexpected hole, such as an uncovered manhole, is
          in the walkway (4) Slip and fall, occurs when the surface of the floor
          causes you to slip
        </p>
        <p>
          A slip and fall injury is when any of the four types of situations
          above occurs. Some common causes of slip and fall accidents are as
          follows:
        </p>
        <p>
          " Slippery or uneven sidewalks, cobblestones, or pavement " Potholes "
          Steeply sloping driveways " Slippery floor surfaces or floor coverings
          " Oil, grease, water, liquids, or food on the floor " Uneven stairs or
          inadequate stair rails " Mats or rugs which become unanchored or loose
          " Rain, snow, or ice " Blocked store aisles " Bridge construction
          hazards, including falling debris " Unsafe balconies or railings "
          Poor lighting
        </p>
        <p>
          If you have been injured because of one of the hazards listed above
          you may have a slip and fall case.
        </p>
        <h2>Slip and Fall Facts:</h2>
        <p>
          " Approximately 25,000 people a day are victims to slip and fall
          accidents. " The average cost of a slip related injury exceeds
          $12,000. " The average cost to defend a slip and fall lawsuit is
          $50,000. " 65% of all lost workdays are due to slip and fall
          accidents, this results in 95 million lost workdays per year. " The
          food service industry's leading cause of employee injury is slipping
          and falling. " The average restaurant has 3-9 slip and fall accidents
          each year. " Slips and falls are the #1 cause of accidents in hotels,
          restaurants, & public buildings, 70% of these accidents occur on flat
          / level surfaces. " Our Nation's $494 Billion grocery store industry
          spends $450 million annually to defend slip and fall claims. " Slip
          and falls account for over 20,000 fatalities per year in North
          America; 55 persons per day. " Slip and falls are the second leading
          cause of accidental death and disability after automobile accidents. "
          Over 540,000 slip-fall injuries, requiring hospital care, occur in
          North America each year. " Slip-falls account for over 300,000
          disabling injuries per year in North America. " Slip-falls kill more
          workers than all other combined forms of workplace accidents.
        </p>
        <h2>Slip and Fall Case Facts:</h2>
        <p>
          1. Slip and fall injuries can occur in businesses, on public property,
          or in private homes: Most slip and falls occur in stores or public
          areas, but if you do slip and fall in someone's house and the owner
          has created an unsafe condition or allowed the continued threat of a
          known unsafe condition, he could be liable for your injuries. 2. The
          standard for deciding whether the responsible person has breached his
          duty of due care for you differs depending on where you are: the duty
          that is owed to you varies depending on whether you are in a store, in
          someone's home, or you are trespassing on someone's property. The more
          foreseeable and invited your presence, the more active the property
          owner must be to keep you safe from a known or knowable dangerous
          condition of his property. 3. A personal injury attorney can help you
          bring a civil case: your personal injury attorney will help you gather
          the proper evidence to help you carry your heavy burden of proving
          liability and establishing the damages you are able to recover for
          your injuries, and will present your case to an insurance claims
          adjuster, the court, or a jury to generate compensation for your
          injuries. 4. It is your burden to prove negligence: If you fall or
          hurt yourself, the other party is liable only if you can prove that
          his actions were a breach of a legal duty of due care owed to you that
          resulted in your fall. As the plaintiff, it is your burden to prove
          this. 5. You will have to prove damages: You have to have suffered
          some type of harm that you can be compensated for. 6. Your recovery
          can be reduced if you contributed to your injury: In most states you
          can still recover damages if you were partially responsible for your
          injuries. Your damages will simply be reduced by the percentage of
          responsibility attributed to your actions. 7. Your damages will be
          based on the extent of your actual damages: The more severe your
          damages, the more money you can expect in compensation. 8. Emotional
          distress damages may also be possible in most cases if you have
          suffered physical injury: If you experienced stress or fear as a
          result of your slip or fall, this might be compensable as well. 9.
          Most slip and fall cases settle out our court: Most of the time you
          will not have to go to court and the defendant will offer you a lump
          sum payment to resolve the case.
        </p>
        <h2>How You Can Win Your Slip and Fall Case:</h2>
        <p>
          To bring your slip and fall personal injury case to a profitable end,
          you need to show that the property owner or tenant was negligent,
          meaning that some action he took or failed to take caused the fall. In
          order to prove that a property owner was negligent in the maintenance
          of his/her property you must show:
        </p>
        <p>
          1. That he owned/leased/occupied/controlled the property; 2. That he
          was negligent in the use or maintenance of the property; 3. That you
          were harmed; and 4. That his negligent construction, repair, or
          maintenance of the property was the cause of your harm.
        </p>
        <h2>Duty:</h2>
        <p>
          The general rule for a property owner is that he is responsible not
          only for his willful acts, but also for his lack of ordinary care or
          skill in the management of his property. The exception to this is if
          you have brought the injury upon yourself by your own actions or lack
          of ordinary care. More specifically, a person who owns, leases,
          occupies, or controls a property must use reasonable care to keep the
          property in a reasonably safe condition. He must use reasonable care
          to discover any unsafe conditions and to repair, replace, or give
          adequate warning of anything that could be expected to harm others.
          There are a number of factors that the court uses in order to
          determine if the owner used reasonable care. Among others, the court
          uses the following:
        </p>
        <p>
          (1) The location of the property (2) The likelihood that someone would
          come onto the property in the same manner as you did; (3) The
          likelihood of harm; (4) The probable seriousness of such harm; (5)
          Whether the owner knew or should have known of the condition that
          created the risk of harm; (6) The difficulty of protecting against the
          risk of such harm; and (7) The extent of the owner's control over the
          condition that created the risk of harm.
        </p>
        <p>
          The court may also use any other factor that it considers relevant.
          The main question the court will ask is if in the management of
          his/her property, the owner of land acted as a reasonable person would
          under all the circumstances. To determine if the owner acted as a
          reasonable person you need to ask what a normal, ordinary, prudent
          owner of land would do under the circumstances.
        </p>
        <p>
          An owner must make reasonable inspections of the property to discover
          dangerous conditions. The court will need to decide if the condition
          that created your harm was the type and existed long enough so that it
          would have been discovered by an owner using reasonable care. If an
          inspection was not made within a reasonable time before the accident,
          this may show that the condition existed long enough so that a store
          owner using reasonable care would have discovered it.
        </p>
        <p>
          An owner of land does not insure everyone's safety, but they must use
          reasonable care to keep the property in a reasonably safe condition
          and must give warning of any hidden or concealed hazards that could
          cause harm. If the court determines that the hazardous condition which
          caused you harm was so obvious that a person should reasonably be
          expected to observe it, then the owner does not have to warn about the
          dangerous condition.
        </p>
        <h2>What You Will Need to Prove to Win:</h2>
        <p>
          In order for you to prevail on your slip and fall case one of the most
          important things you will have to prove is that the owner of the
          property where you fell either created, had prior knowledge of, or
          through the use of reasonable diligence should have gained knowledge
          of the hazardous condition and the negligence of the store. These are
          often the most difficult things to prove as well. This is due to the
          fact that the evidence that you need to prove these elements will
          often be in control of the store and will be more accessible to them.
        </p>
        <p>
          What you want to show is that the store either had actual knowledge of
          the dangerous condition (they were told of the condition), that they
          had constructive knowledge (evidence that the store should have
          discovered the condition through reasonable care) or they created the
          dangerous condition. To establish constructive knowledge you want to
          show that the condition existed for a long enough time period where
          the owner or his/her employee should have known about if he/she had
          been acting in a reasonable manner (such as conducting timely
          inspections of the store).
        </p>
        <h2>What the Owner of the Store Will Say About Your Claim:</h2>
        <p>
          When the owner of the store or the owner of the land finds out about
          your claim against him/her they will defend against your claim. Most
          likely they will assert one of the following defenses in order to
          avoid or lessen their liability. 1. The condition at the time and
          place of your fall was not unreasonable dangerous. He might say,
          "There was nothing dangerous or there was not a defective condition on
          the premises." 2. The store had no prior knowledge of the hazardous
          condition that allegedly caused your injuries (or the store only had a
          one minute notice of the condition before you fell). They will say,
          "Even if there was a dangerous or defective condition, we didn't have
          time to take action to remedy the situation." 3. The store exercised
          reasonable care to protect you from injury (such as inspecting store
          every 20 minutes). They will say, "We just did a safety check of that
          area and didn't see any dangerous or defective conditions." 4. Your
          injuries were not a result of the slip and fall that you had in the
          store. They will say, "The fall didn't cause any new injuries nor
          aggravate any preexisting conditions." 5. Your own negligence was a
          contributing factor to the injuries. Basically, you have some fault as
          well in the slip and fall. You should have been more careful. They
          will say, "The injured party was careless or negligent in failing to
          observe the dangerous or defective condition (such as loose carpet,
          clear liquids on floor, rotting wood, etc.)
        </p>
        <h2>Tips to Follow After Your Slip and Fall</h2>
        <p>
          After you suffer a slip or a fall time is of the essence. Your case
          might be won or lost depending on how quickly you gather information
          that can establish proof that the owner violated their duty to keep
          you safe. The following are important actions you will want to take
          immediately after your slip or fall.
        </p>
        <p>
          1. If there were witnesses to your slip or fall, get their names and
          addresses. This includes the name and addresses of store employees
          that saw the slip or fall. 2. Have the store or property owner make a
          report of the incident and get a copy of the report. 3. If possible,
          take photographs of the area where you fell, as near in time to the
          accident that you can. If you have a cell phone with you, take
          photographs immediately of the area and the substance that you slipped
          on. If you are in too much pain to take a photo, ask a witness or
          anyone at the scene to take the photos for you. Make sure to try to
          document in the picture the date and time of the picture, use an
          automatic time and date stamp if that feature is included with your
          camera. 4. Contact the property owner and get contact information for
          his insurance company claims department, the policy number and the
          name of the insured person or company. 5. When you contact the
          insurer, ask specifically about no-fault medical payments assistance,
          which can get your bills paid initially. Accepting medical payments
          will not jeopardize your claim. (Insurance representatives of the
          property owner are trained to be extremely nice and pleasant. Don't
          put too much stock in their kindness to you. Their mission is to save
          as much money as they can for their company by not paying you as much
          or anything for your claim. It is also their mission to gather as much
          information that they can use against you in your claim. If you say
          too much to the adjuster, it could jeopardize your claim. Be extremely
          cautious about giving the insurance representative an interview or
          recorded statement about your accident without first consulting an
          experienced slip and fall attorney. 6. Seek immediate medical help for
          your slip and fall injuries. In the case of a severe injury have an
          ambulance summoned to transport you to the hospital. It is often the
          case that severe injuries resulting from a slip or fall do not
          manifest themselves for days or weeks. This makes it all the more
          important to be examined promptly by a medical provider. 7. Request
          that the property owner keep, as evidence, any video recording of your
          fall. Best to confirm this request in writing.
        </p>
        <h2>What Damages Can I Recover From My Slip and Fall Injury?</h2>
        <p>
          If you can prove that your slip and fall was due to the negligence of
          the property owner or his/her employee then you would be able to
          recover damages for: " Pain and Suffering " The reasonable value of
          Medical bills that you have incurred to treat you for the consequences
          of the injury caused by your fall " Future medical care; " Lost wages
          for the time you were unable to work because of your injury " Any
          reduction in future earning capacity because of your injury " Punitive
          damages ONLY IF it can be proven that the owner created the dangerous
          condition on purpose of if you can show that the owner had reckless
          disregard for safety (he/she egregiously ignored a known safety hazard
          that caused your fall).
        </p>
        <h2>Always Document Your Claim:</h2>
        <p>
          With any personal injury claim it is always important to get whatever
          medical care you need.
        </p>
        <p>
          As you get your needed care, document everything through billing
          statements, pain diaries, and even medical records. Medical records
          can be obtained by contacting your physician or the hospital where you
          were seen. Oftentimes you can simply call the hospital or Doctor's
          office and ask for their medical records department. They will be able
          to assist you in getting all records pertaining to your injury.
        </p>
        <p>
          It is also extremely important to document your loss of earnings and
          any out of pocket expenses you may have incurred. If you have missed
          work due to doctor's appointments, because you are in too much pain,
          if your ability to perform your usual duties is impaired and you
          cannot work or must perform modified duties, or if you have been laid
          off due to your injuries, keep track of the total dollar amount in
          earnings you have lost. These are damages you may be able to recover!
        </p>
        <h2>Get A Consultation</h2>
        <p>
          If you have a serious injury, you want to hire the best personal
          injury attorney you can. If your injuries are not serious and you want
          to handle your claim on your own, it is still a good idea to discuss
          your claim with a <Link to="/">personal injury attorney</Link>. The
          best personal injury attorneys provide free consultations. Ask about
          tactics, pitfalls and the value of your claim.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
