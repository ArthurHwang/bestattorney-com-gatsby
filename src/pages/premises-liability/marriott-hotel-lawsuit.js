// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Marriott Responsible For Infecting A Tourist With Legionnaires' Disease"
        pageDescription="California wrongful death attorney weighs in on lawsuit (Case number: 012-M1-109345) filed by special administrator of a man who died from Legionnaire's disease."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase: Lawsuit Alleges Marriott was Responsible for Fatally
          Infecting an Irish Tourist with Legionnaires' Disease
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            California wrongful death attorney weighs in on lawsuit (Case
            number: 012-M1-109345) filed by special administrator of a man who
            died from Legionnaire's disease. According to a Nov. 2 news report
            in the Chicago Tribune, the lawsuit alleges that the man contracted
            the disease when he inhaled aerosolized water vapor that was
            contaminated with the deadly bacteria.
          </strong>
        </p>
        <p>Newport Beach, CA (PRWEB) November 07, 2012</p>
        <p>
          The special administrator for 66-year-old Thomas Joseph Keane, a
          tourist from Ireland, has filed a wrongful death lawsuit (Case number:
          012-M1-109345) against Marriott International alleging that Keane
          became infected with the deadly bacteria when he dined there.
          According to a Nov. 2 Chicago Tribune article, the lawsuit, which was
          filed in Cook County Circuit Court states that Keane was having dinner
          at the JW Marriott Hotel on West Adams Street on July 27 when he
          somehow inhaled "dangerous aerosolized water vapor contaminated with
          Legionella bacteria."
        </p>
        <p>
          <img
            src="/images/gI_83231_marriot-hotel.jpg"
            align="right"
            alt="Marriott Hotel shown here in Chicago"
            className="imgright-fixed"
          />{" "}
          The bacteria were found in a decorative fountain of the main lobby,
          which has since been removed after it infected other visitors as well,
          the article states. Keane was diagnosed with Legionnaire's disease and
          died Aug. 29, according to the Tribune report. The lawsuit alleges
          that the hotel failed to create and implement proper measures to
          ensure that the decorative fountain was bacteria-free and failed to
          maintain proper water temperatures and biocide levels in the fountain.
        </p>
        <p>
          In addition, the lawsuit alleges that the hotel failed to warn patrons
          about the contaminated fountain between July 27 and Aug. 3. The suit
          seeks damages for pain and suffering and various medical expenses. The
          Tribune report states that health officials, who conducted inspections
          at the hotel, also found the bacteria in the hotel's swimming pool,
          the spa's whirlpool and the men's and women's locker rooms.
        </p>
        <p>
          Hotels have a responsibility to maintain their premises properly and
          ensure the safety of their guests and patrons, said John Bisnar,
          founder of the Bisnar Chase personal injury law firm. "When a business
          such as a hotel knows about hazards that are present on its premises,
          it must take steps as quickly as possible to fix the problem. If that
          is not possible, the business must warn its patrons and guests about
          the existing danger so they can take the necessary steps to protect
          themselves."
        </p>
        <p>
          In cases where negligence is involved, families of deceased victims
          can seek compensation for damages, Bisnar said. "
          <Link to="/wrongful-death">Wrongful death lawsuits</Link> are usually
          filed by immediate family members and seek damages such as medical and
          funeral expenses, lost future income, loss of love and companionship
          and pain and suffering. It is important that victims' families in such
          cases understand their legal rights so they can make an informed
          decision about pursuing them."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The{" "}
          <Link to="/wrongful-death">California wrongful death attorneys</Link>{" "}
          of Bisnar Chase represent victims of auto accidents, defective
          products, dangerous roadways, and many other personal injuries. The
          firm has been featured on a number of popular media outlets including
          Newsweek, Fox, NBC, and ABC and is known for its passionate pursuit of
          results for their clients. Since 1978, Bisnar Chase has recovered
          millions of dollars for victims of auto accidents, auto defects and
          dangerously designed and/or maintained roadways. For more information,
          please call 949-203-3814 or visit / for a free consultation.
        </p>
        <p>
          <em>
            Source:
            http://www.chicagotribune.com/news/local/ct-met-legionnaires-death-lawsuit-1102-20121102%2c0%2c3208797.story
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
