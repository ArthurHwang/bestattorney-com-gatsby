// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Garden Grove Employment Lawyers - Employment Attorneys in Garden Grove"
        pageDescription="If you have been involved in a Garden Grove employment issue,Call 949-203-3814 to discuss your case with one of our employment attorneys. We specialize in discrimination,sexual harassment and wrongful termination. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Garden Grove Employment Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Even though great advancements have been made over the years, gender
          discrimination is still a very real issue for many Garden Grove
          employees. Gender discrimination is when someone is not offered a
          promotion, an opportunity for employment or equal pay because of their
          sex. Individuals who have faced violations of their rights because of
          their sex would be well advised to contact an experienced Garden Grove
          employment lawyer with experience helping wronged employees get the
          support they need.
        </p>
        <h2>Gender Discrimination</h2>
        <p>
          It is wrong for a supervisor or an employer to make decisions based
          solely on the sex of the applicant or employee. Victims of sexism will
          have to prove that there are not only differences in policies for
          males and females but also that the differences are unfair. For
          example, forcing men and women to use different bathrooms is not
          gender discrimination under the law. If, however, the hiring policies
          or working conditions for the women and men at the workplace is
          different, then discrimination may be taking place. Many of the
          protections for employees regarding gender discrimination come from
          the Civil Rights Act of 1964 and the Equal Pay Act.
        </p>
        <h2>Civil Rights Act and Gender Discrimination</h2>
        <p>
          Title VII of the Civil Rights Act includes a prohibition of workplace
          discrimination not only based on race but also on gender. Under the
          Civil Rights Act, employers may not make decisions regarding hiring or
          firing based solely on the gender of the applicant or employee.
          Choices made regarding promotions and opportunities must be made
          without relation to the gender of the employee as well. Other rights
          are provided within the Equal Pay Act.
        </p>
        <h2>Equal Pay Act and Discrimination</h2>
        <p>
          Under the Equal Pay Act, employers must pay male and female employees
          equal salaries for equal work. Equality of pay does not have to
          involve identical work but it does have to occur where there is
          substantially equal work being performed. Additionally, both the Equal
          Pay Act and the Civil Rights Act makes it unlawful for an employer to
          retaliate against an employee who chooses to oppose discriminatory
          practices.
        </p>
        <h2>Pregnancy Discrimination Act</h2>
        <p>
          Another important law that affects gender discrimination in the
          workplace is the Pregnancy Discrimination Act of 1978. Under this act,
          the protections granted to women were expanded. Women affected by
          pregnancy, childbirth or related medical conditions, must be treated
          in the same manner as other employees at the company with regard to
          their employment-related benefits.
        </p>
        <h2>Additional Forms of Discrimination</h2>
        <p>
          Skilled Garden Grove employment lawyers handle all types of
          discrimination cases in Orange County. There are many forms of
          discrimination that still exist in the workplace. Examples of wrongful
          discrimination at the workplace include age discrimination, race
          discrimination, disability discrimination and discrimination based on
          sexual preference or religious practice. All forms of discrimination
          are wrong and when they occur in the workplace, the victim can choose
          to hold his or her employer accountable for their actions.
        </p>
        <p>
          Other workers who may have a valid claim against their employers
          include employees who were not provided adequate pay for their hours
          worked. It is common for companies to cut corners or try to save money
          by asking employees to work over eight hours a day or over 40 hours in
          a week without additional pay. There are a few exceptions to this
          rule, but in general, individuals working over eight hours a day or
          over 40 hours a week must be paid time and a half. When one person is
          not being paid fair compensation for their hours worked, it is likely
          that many other employees are being mistreated as well. It is
          important that employers who violate the rights of their employees are
          held accountable for their actions.
        </p>
        <p>
          The reputed Garden Grove employment lawyers at Bisnar Chase Personal
          Injury Attorneys can help protect the legal rights and best interests
          of injured workers and their families. Please call us at 949-203-3814
          or <Link to="/contact">contact our law offices </Link> for a free and
          comprehensive consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
