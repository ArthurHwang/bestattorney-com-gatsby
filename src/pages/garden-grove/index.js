// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/garden-grove-personal-injury-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Garden Grove",
      population: 175140,
      totalAccidents: 5971,
      intersection1: "Brookhurst St & Westmister Ave",
      intersection1Accidents: 138,
      intersection1Injuries: 79,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 181.0,
      city1Name: "Stanton",
      city1Index: 197.5,
      city2Name: "Westminster",
      city2Index: 229.8,
      city3Name: "Santa Ana",
      city3Index: 200.6,
      city4Name: "Fountain Valley",
      city4Index: 153.0
    },
    locationBox3: {
      intersection2: "Trask Ave & Brookhurst St",
      intersection2Accidents: 94,
      intersection2Injuries: 61,
      intersection2Deaths: 0,
      intersection3: "Harbor Blvd & Trask Ave",
      intersection3Accidents: 105,
      intersection3Injuries: 49,
      intersection3Deaths: 0,
      intersection4: "Garden Grove Blvd & Harbor Blvd",
      intersection4Accidents: 97,
      intersection4Injuries: 44,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Garden Grove Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Contact the award winning Garden Grove personal injury lawyers of Bisnar Chase. Attorneys who fight for you & have a 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Garden Grove Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Garden Grove Personal Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            For nearly four decades, the personal injury lawyers at Bisnar Chase
            have served and continue to serve as strong advocates for personal
            injury victims in Garden Grove, Orange County and throughout
            California. Our skilled attorneys have secured millions of dollars
            in verdicts and settlements on behalf of those who have been injured
            as a result of auto accidents and defective products. Our law firm
            has the experience and resources to handle any type of personal
            injury claim, regardless of the challenges or complexities involved.
            <strong> Contact us at 1-800-561-4887</strong> to obtain more
            information about pursuing your legal rights.
          </p>
          <h2>Do You Need a Personal Injury Attorney?</h2>
          <p>
            Not every accident or injury claim needs a personal injury attorney.
            The facts and circumstances of your case will often determine
            whether or not you need to retain an injury lawyer. For example, if
            you were involved in a{" "}
            <Link to="/garden-grove/car-accidents">
              car accident in Garden Grove{" "}
            </Link>{" "}
            in which you suffered minor injuries that didn't require
            hospitalization or treatment and your car was totaled, you might
            just need to work with the insurance company to negotiate and settle
            your claim.
          </p>
          <p>
            However, if you suffered injuries serious enough to require
            hospitalization, treatment and take days off work to recover, then,
            you will likely need a personal injury lawyer on your side who can
            make an accurate assessment of the damages and losses you have
            suffered and can go after the at-fault parties to ensure that you
            receive maximum compensation for your losses.
          </p>
          <h2>Dealing with Insurance Companies</h2>
          <p>
            When you have suffered an injury due to someone else's fault, you
            have to deal with insurance companies for other parties or with your
            own insurance carrier. Often, this can be challenging or even
            tricky. Many of us tend to believe that the insurance company is our
            friend and that they have our best interests at heart. However, as
            personal injury attorneys, we know this is not true. We often fight
            insurance companies on behalf of injured clients who struggle
            against these corporations just to get what they rightfully deserve.
            It is fairly common for insurance adjusters to lowball you with an
            offer that is not fair, given the magnitude of the injuries and
            damages you have suffered.
          </p>
          <p>
            If you have been injured and must deal with an insurance company, be
            sure they are not recording anything you say. Don't admit fault or
            make any statements that are not well thought out. They can and will
            use those statements against you during the claims process. Do not
            accept the first settlement offer. In fact, don't take an offer to
            settle until you have had a chance to sit down and go over all your
            damages and losses with your injury lawyer.
          </p>
          <p>
            Insurance companies are more concerned about protecting their
            profits and bottom line. Insurance adjusters are trained to maximize
            profits for their employer and minimize the company's payout to you.
            In such cases, injured victims need an experienced injury lawyer on
            their side who will fight for their rights and look out for their
            best interests. You can be certain that the insurance company is not
            doing that for you.
          </p>
        </div>
        <h2>How Do You Find the Right Personal Injury Lawyer?</h2>
        <p>
          Choosing the right personal injury lawyer to represent you is critical
          to the success of your claim. Here are a few things to consider before
          you retain the services of a personal injury lawyer in Garden Grove:
        </p>
        <ul type="disc">
          <li>
            <strong> The right experience:</strong> The value of an attorney's
            experience when it comes to evaluating and investigating a claim can
            never be underestimated. Choose an attorney who has not just been
            practicing personal injury law for many years, but also someone who
            has handled cases similar to yours and has an undisputable track
            record of success.
          </li>
          <li>
            <strong> Focus:</strong> An attorney who focuses his or her practice
            on personal injury is much better equipped to handle your case
            rather than a lawyer who dabbles in different areas of the law. A
            majority of injury lawsuits are settled outside the courtroom. So
            you need a skilled negotiator to handle your case. It is also
            important to ensure that your lawyer has the necessary skills and
            experience to take the case to trial if that becomes necessary.
          </li>
          <li>
            <strong> Reputation:</strong> Personal injury law is one of those
            fields when an attorney's reputation can go a long way in resolving
            your case quickly and fairly. Insurance companies deal with lawyers
            all the time and they know which attorneys they cannot mess with.
            When you hire a lawyer with a reputation that precedes him or her,
            you put yourself at an advantage and in the best possible situation
            to recover maximum compensation for your losses.
          </li>
        </ul>
        <h2>Why Bisnar Chase?</h2>
        <p>
          Our Garden Grove personal injury attorneys have{" "}
          <strong> won over $500 Million dollars</strong> in verdicts and
          settlements for our clients. Our firm has more than 35 years of
          experience in personal injury law. We have stellar ratings from
          professional associations such as AVVO, Trial Lawyers of America and
          SuperLawyers. We believe that our reputation, the quality of legal
          representation we provide to every client we take on and the superior
          level of customer service we provide to every person who walks through
          our doors makes us truly unique.
        </p>
        <p>
          The personal injury lawyers of Bisnar Chase have represented{" "}
          <strong> over 12,000 clients with a 96 percent success</strong> rate
          since 1978. We have a no-win, no-fee guarantee, which means you don't
          pay anything unless we recover compensation for you.{" "}
          <strong>
            {" "}
            Call us at 1-800-561-4887 to schedule your free, comprehensive and
            confidential consultation.
          </strong>
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
