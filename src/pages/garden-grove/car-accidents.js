// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Garden Grove Car Accident Lawyers - Bisnar Chase"
        pageDescription="If you have a car accident please call our Garden Grove car accident attorney for a Free consultation. 96% success rate, millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Garden Grove Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          When you have been injured in a Garden Grove car accident, having a
          skilled negotiator and trial lawyer on your side is imperative. The
          experienced car accident lawyers at Bisnar Chase are committed to
          fighting and winning on your behalf. We strive to help you secure the
          maximum possible compensation for your injuries damages and losses. We
          have been representing auto accident victims since 1978 and have
          helped injured clients secure{" "}
          <strong> more than $500 Million in verdicts and settlements</strong>.
          Call us at (949) 203-3814 to find out how we can help you.
        </p>
        <p>
          Our <Link to="/garden-grove">personal injury attorneys </Link> are
          highly skilled and have represented thousands of Orange County
          residents. If you are seeking a reliable team of expert lawyers, our
          firm should be at the top of your list. We have a{" "}
          <strong> 96% success rate</strong> and client care is always our top
          priority.
        </p>
        <h2>Have You Been Injured in a Car Accident?</h2>
        <p>
          If you have been injured in a Garden Grove car accident, there are a
          number of steps you would be well advised to take in order to protect
          your rights:
        </p>
        <ul>
          <li>
            Stop immediately and remain at the scene of the crash. Wait for the
            authorities to arrive. The law requires you to do so.
          </li>
          <li>
            File a police report and obtain a copy of the report for your
            records. A police report puts you at the scene of the accident and
            can help get your account of the incident into the official report.
          </li>
          <li>
            Gather as much evidence as possible from the accident scene. A
            picture is worth a thousand words. Take photographs of the scene
            including the position of the vehicles and injuries sustained. Get
            contact information for not just the parties involved in the
            collision, but also anyone who might have witnessed the crash and
            can corroborate your account. If you are unable to gather evidence
            because you were injured and had to get to the hospital, have a
            friend or family member collect that information for you.
          </li>
          <li>
            Get prompt medical attention, treatment and care for your injuries.
            This not only gives you a good shot at making a speedy recovery, but
            also creates documentation for the type of injuries you sustained
            and the treatment you received.
          </li>
          <li>
            Contact an experienced Garden Grove car accident lawyer who will
            help protect your rights every step of the way and hold the at-fault
            parties accountable for their negligence.
          </li>
        </ul>
        <h2>The Issue of Negligence</h2>
        <p>
          There are several reasons why car accidents occur. More than{" "}
          <strong>
            {" "}
            90 percent of car accidents occur as a result of driver error or
            negligence.
          </strong>{" "}
          Some of the most common causes of car accidents include driving under
          the influence, distracted driving, fatigued or drowsy driving, and
          failure to follow the rules of the road of which examples include
          running a red light, failing to yield the right of way to vehicles,
          pedestrians and bicycles, and making unsafe turns or lane changes.
        </p>
        <p>
          Negligence is probably the most important component of a personal
          injury case. In any car accident case, particularly those  involving
          two or more vehicles, liability will depend on fault. This means that
          the driver who was determined to have been negligent will be held
          liable for the damages caused. In cases where there is fault on both
          sides, a degree of fault will be assigned to each party. For example,
          if the other driver is found to be 70 percent responsible and you are
          held 30 percent responsible, you may seek up to
          <strong>
            {" "}
            70 percent of the settlement from the other driver's auto insurance
            policy.
          </strong>
        </p>
        <h2>> What if the Other Driver is Uninsured?</h2>
        <p>
          Nationwide, 12.6 percent of drivers, about 1 in 8, are uninsured,
          according to the latest data report from the Insurance Research
          Council. California has the highest number of uninsured motorists in
          the country at 4.1 million. So, what do you do if you are injured by
          an uninsured motorist? When this happens, the uninsured motorist
          clause of your auto insurance kicks in and helps pay for your losses.
          This coverage also helps those who are faced with an at-fault driver
          who lacks sufficient auto insurance coverage to pay for all their
          losses. While liability insurance coverage protects other drivers,
          uninsured motorist protects you and the members of your household.
        </p>
        <h2>What Damages Can You Claim?</h2>
        <p>
          Car accidents often result in significant injuries or sometimes, even
          fatalities. Victims of car accidents often suffer severe injuries
          including brain and spinal cord trauma, multiple broken bones and
          internal injuries. A majority of these injuries involve
          hospitalization, surgeries and rehabilitative therapy. The cost of
          such treatments could add up very quickly. Car accident victims also
          lose valuable income because they have to take time off work to
          recover from their injuries. If you have been injured in a Garden
          Grove car accident, you could seek compensation for damages including
          but not limited to medical expenses, lost income, hospitalization,
          rehabilitation, permanent injuries, disabilities, pain and suffering
          and emotional distress.
        </p>
        <h2>Contacting an Experienced Lawyer</h2>
        <p>
          An experienced Garden Grove car accident lawyer can make the
          difference between a successful claim and a severe financial setback.
          The compensation you receive after a car accident can affect the
          treatment you receive, the speed of your recovery and your financial
          future. You need a lawyer who will give voice to your concerns, make
          sure you are treated fairly and fight hard to ensure that your legal
          rights are protected.
        </p>
        <p>
          At Bisnar Chase, we've set the bar for superior client representation
          and customer service over the last 35 years. We have represented more
          than
          <strong>
            {" "}
            12,000 clients with a{" "}
            <Link to="/case-results">96 percent success rate </Link>
          </strong>
          . If you or a loved one has been injured in a Garden Grove car
          accident, call us at <strong> (949) 203-3814</strong> for a no-cost
          consultation and comprehensive case evaluation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
