// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Garden Grove Dog Bite Lawyers - Dog Bite Attorneys in California"
        pageDescription="Suffered injuries from a dog bite in Garden Grove? Call 949-203-3814 for Garden Grove dog bite attorneys that care. Top client care since 1978. We can help today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Garden Grove Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/gavel and scales personal injury.jpg"
          width="250"
          alt="Dog Bite Attorneys in Garden Grove"
          className="imgleft-fixed"
        />
        <p>
          If you have suffered injuries from a dog attack, you are going to need
          an experienced attorney with a history of success. Our{" "}
          <Link to="/about-us"> Garden Grove dog bite attorneys </Link> have
          been assisting victims of dog bites since 1978, and have developed a
          formula for success that has made us one of the top personal injury
          law firms in the nation. Many dog bite victims suffer from more than
          just scrapes and bruises; dog attacks can leave victims with permanent
          injuries and deformities that alter the way in which they are able to
          enjoy life. The injuries sustained by dog bite victims can affect
          every aspect of their lives, leaving them with serious injuries and
          the challenge of adjusting to a lifestyle that works with their new
          handicaps.
        </p>
        <p>
          Your dog bite lawsuit is more than a monetary award for damages
          sustained, but a vital boost to help you recover from your traumatic
          experience financially and emotionally. Our Orange County dog bite
          lawyers give all of their clients a personal experience and keep them
          informed throughout the duration of their case. Contact us at
          949-203-3814 NOW to take advantage of our free consultation and get
          the information needed to make the best decision for you and your
          family.
        </p>

        <h2>U.S. Dog Bite Statistics</h2>
        <p>
          California has one of the highest rates for fatal dog attacks. In
          2010, 34 people lost their lives as a result of vicious dog bites and
          350,000 people sought treatment from emergency rooms after being
          bitten by a dog in the United States. On a yearly basis, damages
          sustained as a result of dog attacks exceed 1 billion dollars, and
          those who suffer these injuries have little means of covering their
          expenses. The following are dog bite statistics gathered from the CDC,
          Michigan Association of Insurance Agents, and U.S. Postal Service dog
          attack data.
        </p>
        <ul>
          <li>
            There are over four million dog bites that occur on a yearly basis
            nationwide.
          </li>
          <li>
            The number of dog bite attacks requiring immediate medical
            assistance has increased fifteen times faster than the increase in
            dog ownership.
          </li>
          <li>
            Letter carriers suffer about 3,000 dog bites annually as they
            deliver the mail.
          </li>
        </ul>
        <h2>Why Choose Bisnar Chase</h2>
        <p>
          Our clients know that we are more than just a Garden Grove dog attack
          law firm; we are passionate about helping dog bite victims. Not only
          have we sustained a 96% success rate for more than 35 years, but we
          have created an experience which gives our clients the confidence to
          trust their case in our hands. This allows our clients to focus more
          on their recovery, rather than enduring sleepless nights worrying
          about the outcome of their case. Our entire staff enjoys what they do,
          and are very active in the community helping others get back on their
          feet.
        </p>
        <p>
          Bisnar and Chase partners, John Bisnar and Brian Chase, have instilled
          a dedication to assisting those who are less fortunate in every one at
          the firm. Whether we are giving away turkeys, making group donations
          to charities, sponsoring local fundraisers, or participating in
          conjunction with M.A.D.D. to help raise drunk driving awareness, our
          firm is actively working to better the community. Take a look at our
          Bisnar and Chase giving spotlight to see how we work together to help
          struggling Orange County locals.
        </p>
        <h2>Why Children are Prone to Garden Grove Dog Attacks</h2>
        <p>
          Unfortunately, many of those who suffer serious injuries in Orange
          County dog attacks are small children. Among children, injuries
          related to dog bites are substantially higher for those between ages
          five and nine. Due to the child's height and weight, aggressive dogs
          can find them to be easy targets and threats. When dogs to attack
          children, the injuries are usually substantial and years of
          reconstructive surgery, as well as intense therapy, are needed to help
          the child recover from their traumatic experience. If your child was
          attacked by a vicious dog, speaking with an experienced Orange County
          dog attack attorney is in your best interest. The long-term damage
          associated with dog attacks on children can be difficult to bear, but
          with a lack of compensation, getting your child the help they need
          will be much more difficult.
        </p>
        <h2>Dog Bite Attorneys in Garden Grove</h2>
        <p>
          If you are looking for the best legal representation available, you
          need to speak with our law firm today. There is a limited amount of
          time in which one can pursue compensation for a dog bite claim, so it
          is important to seek a case evaluation as soon as possible. Contact a
          Bisnar and Chase dog bite attorney in Garden Grove today to get expert
          answers from top personal injury attorneys in Orange County.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d423896.5453989271!2d-118.0646!3d33.897591517411584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1398360778839"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
