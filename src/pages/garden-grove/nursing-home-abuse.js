// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Garden Grove Bisnar Chase Motorcycle Accident Lawyers"
        pageDescription="Call 949-203-3814 if you or a loved one have experienced nursing home abuse in Garden Grove.Our nursing home attorneys will give you a free case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Garden Grove Nursing Home Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          It is common for Garden Grove nursing homes to cut corners by under
          staffing their facilities. Unfortunately, the consequence of cutting
          staff or hiring unqualified or under-qualified staff is poor care, and
          sometimes can lead to nursing home abuse. It is the patients who get
          shortchanged in the process. Residents of nursing homes who are
          neglected often have an elevated risk of ailments and injuries. There
          are many potential signs of nursing home neglect.
        </p>
        <h2>Dehydration and Malnutrition</h2>
        <p>
          Food and water are the most basic needs for all human beings. All care
          facilities are required to provide proper nutrition to their residents
          and to make sure that the food provided is actually consumed. Although
          nursing homes cannot force a person to eat, they are required to
          notify a patient's family when he or she is refusing food or drink.
          Therefore, when a resident is losing weight and appears malnourished,
          it may be a sign that he or she is being neglected.
        </p>
        <p>
          Patients with dementia are particularly in danger of dehydration
          because they often do not or cannot take care of their own basic
          needs. These types of cases require a rigid schedule that addresses
          the dietary needs of the patient. Malnutrition can result in weakness
          and fragility of bones. Victims of malnutrition may also suffer from a
          compromised immune system making them prone to illnesses and
          infections. Those with weakened bones may also suffer devastating and
          debilitating injuries when they fall.
        </p>
        <h2>Bedsores</h2>
        <p>
          A patient who spends extended periods of time in bed or in a
          wheelchair may be prone to suffering from bedsores. Bedsores, also
          known as pressure ulcers or pressure sores, may start off mild, but
          they can get serious very quickly especially when they become infected
          or are not treated properly. More severe cases of bedsores involve
          wounds that penetrate the skin and the muscle. If these bedsores
          result in infections, it may be a sign that the resident is being
          neglected. Care facilities that are providing proper care and
          attention to their residents can help prevent the spread of bedsores
          through cleaning and by moving the patient throughout the day to
          relieve pressure.
        </p>
        <h2>Falls</h2>
        <p>
          Falling is one of the most common causes of injury for elderly
          residents. Not all falling accidents are preventable, but attentive
          nursing staff members can help reduce the number of falling incidents.
          Nursing homes must provide adequate supervision to their residents.
          Patients who are particularly fragile should be given added attention
          whenever they move through the facility. If a victim suffers an injury
          in a fall and goes unnoticed for a prolonged period of time, it may be
          a sign of neglect. Elderly victims of falling accidents require
          immediate medical attention. It is crucial that facilities provide
          immediate care for residents who have hurt themselves and that the
          family is notified of the incident.
        </p>
        <h2>Choking Accidents</h2>
        <p>
          Recently, there have been several incidents in Orange County nursing
          homes where residents have choked on food and died. Some patients are
          unable to chew and swallow their food because of medical conditions.
          Nursing staff members are required to follow doctor's orders in such
          cases to provide residents with semi-solid, or in some cases, liquid
          food. Such patients who are prone to choking on their food also need
          to supervised at mealtime. Nursing homes that fail to provide proper
          supervision or properly prepared food for their residents can be held
          liable for incidents that cause injury or death.
        </p>
        <h2>If You Suspect Neglect</h2>
        <p>
          If you worry that your loved one has been neglected in some way, it
          may be in your best interest to contact an experienced Garden Grove
          nursing home lawyer. It may be possible to hold the care facility
          accountable for their negligence. Additionally, if your loved one has
          been neglected, it is possible that others at the facility are in
          danger as well. Stepping forward to hold the neglected nursing home
          liable for their actions can help prevent similar cases in the future.
        </p>
        <p>
          The experienced Garden Grove nursing home abuse and neglect attorneys
          at Bisnar Chase Personal Injury Attorneys have a successful track
          record of holding negligent nursing homes accountable for their
          actions. If your loved one has been neglected or abused in a nursing
          home, please call us at 949-203-3814 or contact us for a free
          consultation and more information about how we can help you.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
