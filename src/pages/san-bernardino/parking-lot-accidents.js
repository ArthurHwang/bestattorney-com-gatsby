// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/parking-lot-san-bernardino-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Bernardino Parking Lot Accident Lawyers - Bisnar Chase"
        pageDescription="Experienced Riverside Parking Lot Accident Lawyers who have won over $500 Million for our clients. Call 909-253-0750 for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Bernardino Parking Lot Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san bernardino parking lot accident lawyers"
            title="san bernardino parking lot accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Our skilled team of{" "}
          <strong> San Bernardino Parking Lot Accident Lawyers </strong>have
          been winning cases for over <strong> 40 years</strong>. Having
          established an impressive
          <strong> 96% Success Rate</strong>, we have won over{" "}
          <strong> $500 Million </strong>for our clients and continue to do so.
        </p>
        <p>
          The legal team of paralegals, attorneys, mediators legal advisors and
          other staff here at <strong> Bisnar Chase </strong>are dedicated to
          ensuring you and your case have top priority and that you are never
          left guess or wondering what the status of your situation is.
        </p>
        <p>
          We strive to help those in need and hold those accountable for their
          wrongdoing, negligence or for causing the accident. If we don't win
          your case, you don't pay anything. That's the Bisnar Chase promise.
        </p>
        <p>
          If you or a loved one has experienced an accident or have been injured
          in a parking lot accident, contact us for a{" "}
          <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation</strong>.
        </p>
        <p>
          For immediate assistance <strong> Call 909-253-0750</strong>.
        </p>
        <h2>Dangerous Parking Lot Conditions</h2>
        <p>
          Going to the store or mall and parking our car is an everyday thing.
          That's because we haven't realized or experienced what can actually
          take place somewhere like a parking lot or parking garage.
        </p>
        <p>
          Due to the congested vehicle and pedestrian traffic, accidents are
          inevitable. With distracted drivers and walkers paying more attention
          to their cell phones rather than the road and environment around them,
          behind struck by a vehicle or running someone over is very likely.
        </p>
        <p>Parking lots are everywhere we go:</p>
        <ul>
          <li>Grocery Stores</li>
          <li>Shopping Malls</li>
          <li>Skateparks</li>
          <li>Beaches</li>
          <li>Gas Stations</li>
          <li>Restaurants</li>
          <li>Sporting Events</li>
          <li>Concerts</li>
          <li>Theme Parks</li>
        </ul>
        <p>
          With the high number of parking lots and high percentage of parking
          lot usage, it shouldn't be surprizing at how common and often
          accidents, injuries and even deaths occur on a daily basis.
        </p>
        <h2>Top 3 Most Common Parking Lot Accidents</h2>
        <ul>
          <li>
            <strong> Hit-and-Runs</strong> - Hit and run accidents are usually
            caused by a car accident or pedestrian accident gone bad. The
            suspect is usually someone with prior history with the law and wants
            to avoid getting in deeper trouble, so they try and run, but this
            will only get them in more trouble and increase their punishment,
            penalties and fines. If you witness a hit and run, as long as safety
            permits, call 911.{" "}
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/san-bernardino/pedestrian-accidents">
                Pedestrian Accidents
              </Link>{" "}
              -{" "}
            </strong>
            In the case of a pedestrian being hit, this can consist of people
            walking or jogging, bicycle riders, skateboarders and many other
            types of transportation, given they are not driving a car or
            motorcycle. Distracted driving, driving under the influence of drugs
            and or alcohol, and also speeding are three major factors for
            pedestrian accidents.
          </li>
          <li>
            <strong> Fender-Benders</strong> - Being rear-ended, side-swiped,
            bumping into another can and head-on collisions are all types of
            fender-benders. They can result from minor with no injuries and very
            little damage, to very severe and catastrophic. Never down play a
            fender-bender, seek medical attention immediately and contact a
            skilled and experienced parking lot attorney.
          </li>
        </ul>
        <p>
          If you or a loved one has been injured or has experienced a parking
          lot accident, contact our{" "}
          <strong> San Bernardino Law Firm Office</strong>. For immediate
          assistance
          <strong> Call 909-253-0750</strong>.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/igNp5SthN80"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Insurance Companies Want Your Money</h2>
        <p>
          This is why they are in the business; to make money off of you.
          Although insurance companies advertising and say they are always there
          for you and will help you through tough times, it all comes down to
          them wanting your money, and finding loopholes and ways of saving as
          much as possible on their end.
        </p>
        <p>
          Insurance companies are definitely helpful and beneficial in many
          ways, but our advice is to contact a lawyers first before you start
          settling with your insurance companies.
        </p>
        <p>
          Our advice is to contact a parking lot accident lawyer and discuss
          your situation, so that a paralegal, legal advisor, mediator or other
          law firm staff can negotiate and counteract insurance hard-ball
          techniques to ensure you receive maximum compensations and
          reimbursement for whatever it is that you are entitled to.
        </p>
        <p>
          For a <strong> Free Consultation </strong>and{" "}
          <strong> Free Case Evaluation</strong>, contact the{" "}
          <strong> San Bernardino Parking Lot Attorneys </strong>who have over
          <strong> 40 Years of Experience Winning Cases </strong>for our
          clients.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  473 E Carnegie Dr. 2nd Floor, Room 221
                </div>
                <div>
                  <span itemProp="addressLocality">San Bernardino</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92408</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(909) 253-0750</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <p>
          <strong>
            {" "}
            You can also reach us at our{" "}
            <Link to="/los-angeles/contact">Los Angeles</Link> or{" "}
            <Link to="/orange-county/contact">Orange County</Link> law offices.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
