// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-bernardino-wrongful-death-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Bernardino Wrongful Death Lawyers - Bisnar Chase"
        pageDescription="Call our San Bernardino Wrongful Death Lawyers for a Free Case Evaluation at 909-253-0750. We have won over $500 Million for the families who have experienced the loss of a loved one. In your time of sorrow and grief reach for help. Call our Inland Empire attorneys today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Bernardino Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="San Bernardino wrongful death lawyers"
            title="San Bernardino wrongful death lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> The San Bernardino Wrongful Death Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong>are here for you. Our compassionate and
          dedicated team of attorneys have a<strong> 96% Success Rate </strong>
          and will fight for you while standing by your side, every step of the
          way.
        </p>
        <p>
          In San Bernardino, a <strong> wrongful death claim</strong> arises
          when one person dies as the result of the negligence or wrongdoing of
          another individual or entity.
        </p>
        <p>
          A wrongful death claim is a civil action, which is brought to court by
          the survivors of the decedent. Relief is offered solely in terms of
          monetary damages where the court orders the defendant to pay the
          survivors of the deceased person.
        </p>

        <p>
          The San Bernardino personal injury attorneys at Bisnar Chase have been
          representing clients who have experienced the death of a loved one for
          over 40 years. We handle each and every one of our cases with
          compassion and sensitivity.
        </p>
        <p>
          We are here to help you through this difficult time and do everything
          we can to help you get justice and fair compensation for your losses.
        </p>
        <p>
          <strong>
            {" "}
            If you have a wrongful death case and you need professional
            representation, please contact us at 909-253-0750.
          </strong>
        </p>

        <p>
          Essentially, a wrongful death claim is different from a criminal
          murder, homicide or manslaughter case because in those cases a
          defendant would have to be found "guilty beyond a reasonable doubt."
        </p>
        <p>
          In a civil wrongful death case, the defendant is found liable by the
          jury and told to compensate the victim's survivors.
        </p>

        <p>
          The experienced San Bernardino wrongful death lawyers at Bisnar Chase
          understand the nuances and complexities of the law, which is required
          to bring forth a successful claim.
        </p>
        <p>
          We are extremely sensitive to the grief and pain families feel when
          they lose a loved one. It is our goal to provide you with the
          information, resources and help you need to protect your legal rights
          and your financial future, during a very difficult and traumatic time
          in your life.
        </p>
        <p>
          Contact the Inland Empire injury lawyers today for a free initial
          consultation.{" "}
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IUvwlj6ew0I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Who Can File a Wrongful Death Claim?</h2>

        <p>
          California law allows only certain individuals to file a{" "}
          <strong> wrongful death lawsuit</strong>. Here are examples of some of
          the individuals who may be able to file a wrongful death lawsuit in
          San Bernardino:
        </p>

        <ul>
          <li>
            The spouse or children of the deceased person including a putative
            spouse and children.
          </li>
          <li>A domestic partner of the deceased person.</li>
          <li>The decedent's parents, surviving siblings or grandparents.</li>
          <li>
            Anyone who was financially dependent on the deceased person at the
            time of death.
          </li>
        </ul>
        <p>
          Statistics provided by the{" "}
          <Link
            to="http://www.injuryinformation.com/accidents/wrongful-death.php"
            target="new"
          >
            U.S. Department of Justice
          </Link>
          .
        </p>
        <h2>Elements of a Wrongful Death in San Bernardino, CA</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/couple-comforting-eachother-wrongful-death.jpg"
            alt="wrongful death lawyer san francisco"
            width="100%"
            id="couple"
            malt="couple-comforting-each-other-san-bernardino"
          />
        </LazyLoad>
        <p>
          It is important to remember that not all fatal incidents result in
          wrongful death claims.
        </p>
        <p>
          Whether a wrongful death claim is warranted or not depends, of course,
          on the facts and circumstances of the case.
        </p>

        <p>
          In order to be successful with a wrongful death case, whether it be
          associated with a car accident or medical malpractice, the plaintiffs
          must be able to prove the following:
        </p>

        <ul>
          <li>
            The victim's death was caused by the defendant. For example, if an
            individual was a pedestrian who was killed in a{" "}
            <Link to="/san-bernardino/car-accidents">car accident</Link> by a
            drunk driver, then, the plaintiff can show that the impaired driver
            caused the decedent's death.{" "}
          </li>
          <li>
            The victim's death was the direct result of the defendant's
            negligence. For example, if an individual's fatal injuries were
            caused by the fact that he was ejected from a vehicle as the result
            of a defective seatbelt, the manufacturer of the vehicle and/or the
            faulty seatbelt can be held liable for the victim's wrongful death.
          </li>
          <li>
            The decedent's death has affected surviving family members. There
            are many ways in which immediate family members are affected by the
            loss of a loved one. A wife may miss her husband's love and
            companionship. Children may miss a father's love and mentorship. The
            family as a whole is often affected financially not only because of
            the expenses they face, but also because they have lost a primary or
            sole wage earner. Such a death could financially devastate families.
            So, in the case of a wrongful death in San Bernardino, CA,
            plaintiffs are required to show evidence of how the victim's death
            has affected them.
          </li>
          <li>
            The victim's death resulted in monetary damages. This often includes
            lost future income, lost benefits such as health, dental or other
            insurance, pensions, etc.
          </li>
        </ul>
        <p>
          To learn more about specifics on wrongful deaths, visit{" "}
          <Link
            to="https://www.cdc.gov/nchs/fastats/accidental-injury.htm"
            target="new"
          >
            CDC.gov
          </Link>
          .
        </p>

        <h2>Damages in San Bernardino County Wrongful Death Lawsuits</h2>

        <p>
          The settlement for a wrongful death in San Bernardino,CA will vary
          depending on the individual case. Some of the damages typically
          awarded in a wrongful death lawsuit include:
        </p>

        <ul>
          <li>Medical and hospital bills</li>
          <li>Funeral and burial costs</li>
          <li>
            Lost income including potential income the decedent would have
            earned in the future had he or she lived
          </li>
          <li>Value of household services</li>
          <li>
            Loss of love, community, attention, affection, moral support and
            guidance
          </li>
          <li>Pain and suffering</li>
          <li>Punitive damages, in egregious cases.</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san bernardino wrongful death attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/mourning-in-san-bernardino.jpg')"
            }}
          >
            <h2>Hire A Top-Notch Wrongful Death Attorney in San Bernardino</h2>
          </div>
        </LazyLoad>

        <p>
          The experienced{" "}
          <strong> San Bernardino wrongful death lawyers</strong> at{" "}
          <strong> Bisnar Chase</strong> have more than{" "}
          <strong> 40 years of experience</strong> successfully handling complex
          cases.
        </p>
        <p>
          We handle each and every one of our personal injury law cases with
          compassion and sensitivity. We are here to help you through this
          difficult time and do everything we can to help you get justice and
          fair compensation for your losses.
        </p>

        <p>
          If you have lost a loved one due to someone else's negligence or
          wrongdoing, it is crucial that you get a knowledgeable a wrongful
          death attorney in San Bernardino on your side right away.
        </p>
        <p>
          We can take care of paperwork and procedures, answer your questions
          and address your concerns.
        </p>
        <p>
          Please call our office for your <strong> Free consultation</strong>{" "}
          with one of our local wrongful death attorneys.
        </p>
        <p>
          For immediate assistance <strong> Call 909-253-0750</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  473 E Carnegie Dr. 2nd Floor, Room 221
                </div>
                <div>
                  <span itemProp="addressLocality">San Bernardino</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92408</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(909) 253-0750</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <p>
          <strong>
            {" "}
            You can also reach us at our{" "}
            <Link to="/los-angeles/contact">Los Angeles</Link> or{" "}
            <Link to="/orange-county/contact">Orange County</Link> law offices.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
