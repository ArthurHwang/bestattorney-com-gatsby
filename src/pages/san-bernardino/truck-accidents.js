// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/semi-truck-san-bernardino-accident-truck-lawyers-attorneys.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Bernardino Truck Accident Lawyers - Inland Empire Accident Attorneys"
        pageDescription="San Bernardino Truck Accident Lawyers at Bisnar Chase will fight and win for you. $500 Million Won & a 96% Success Rate. Free Consultation. Call 909-253-0750."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Bernardino Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san bernardino truck accident lawyers"
            title="san bernardino truck accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Our team of <strong> San Bernardino Truck Accident Lawyers</strong>{" "}
          have over <strong> 40 years of experience </strong>and understand what
          it takes to win your case.
        </p>
        <p>
          If you've been involved in a truck accident contact our San Bernardino
          accident lawyers for a free consultation and to find our more on how
          you can be compensated for your medical bills and lost wages.
        </p>
        <p>
          <strong> The Personal Injury Law Firm of Bisnar Chase </strong>has
          represented no-fault drivers for decades<strong> </strong> and have
          over <strong> $500 Million</strong> in settlements and verdicts. Our
          approach is driven by trust, passion, and results for our clients.
        </p>

        <p>
          There are many potential causes of a truck accident and having a basic
          understanding of what causes truck accidents can help you avoid being
          involved in a collision.
        </p>
        <p>
          In general, it is in the best interest of all motorists to keep a safe
          distance from tractor-trailers and to drive defensively around big
          rigs.
        </p>
        <p>
          If you or a loved one has been injured in a truck accident, it is also
          important that you contact a San Bernardino Personal Injury Lawyer so
          that you can understand what your legal rights and options are.
        </p>

        <p>
          {" "}
          <Link to="/san-bernardino/contact">Contact us</Link> for a{" "}
          <strong> Free </strong> <strong> Consultation</strong>. For immediate
          assistance <strong> Please Call 909-253-0750</strong>.
        </p>

        <h2>Examples of Driver Negligence Throughout San Bernardino County</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/semi-truck-driving-in-snow.jpg"
            width="100%"
            id="snow-truck"
            alt="san bernardino truck accident attorneys"
          />
        </LazyLoad>

        <p>
          Inland empire traffic accidents may be caused by negligence on the
          part of a truck driver or other drivers.
        </p>
        <p>
          Here are some of the most common types of driver negligence we see in
          the most serious San Bernardino truck accidents:
        </p>

        <p>
          You can report dangerous and negligent truck and commercial drivers at{" "}
          <Link
            to="https://www.fmcsa.dot.gov/consumer-protection/report-safety-violations"
            target="new"
          >
            FMCSA.gov
          </Link>
          .
        </p>

        <ul type="disc">
          <li>
            <strong> Drunk driving:</strong> All commercial drivers in
            California are considered over the legal limit if they have a blood
            alcohol concentration of .04 percent. Driving a large truck while
            under the influence is extremely dangerous and it can have
            catastrophic consequences.
          </li>
          <li>
            <strong> Speeding:</strong> Truck drivers must not only obey the
            speed limit, but also adjust their speed according to the cargo they
            are hauling, roadway conditions and traffic flow. Tailgating is also
            a common practice among truckers, which can be extremely dangerous.
            A common type of accident that occurs is a large truck rear-ending a
            passenger vehicle because the truck driver was traveling at an
            unsafe rate of speed or was following too closely and could not stop
            in time to avoid a collision. Such rear-end accidents often result
            in catastrophic injuries or fatalities.
          </li>
          <li>
            <strong> Distracted driving:</strong> Commercial truck drivers
            should never take their eyes off the road, their hands off the wheel
            or their focus off the task at hand, which is to drive their truck
            safely. Distracted driving is a common cause of injury accidents and
            distracted truck drivers have been linked to a number of tragic San
            Bernardino accidents. Common examples of distracted truck driving
            include talking on the cell phone, texting, using a laptop or
            reading while driving, eating or drinking or using other
            communication devices.
          </li>
          <li>
            <strong> Dangerous lane changes:</strong> It is crucial that truck
            drivers only change lanes when it is safe to do so. Truck drivers
            must use their signals and they must check their rear-view and
            side-view mirrors multiple times before changing lanes.
          </li>
          <li>
            <strong> Fatigued driving:</strong> Drowsy driving is a serious
            problem on our roadways. Truck drivers who have worked long hours
            and feel drowsy should pull over and rest. Drivers who attempt to
            push through fatigue to meet a deadline have an increased chance of
            being involved in a San Bernardino truck accident.{" "}
          </li>
          <li>
            <strong> Unsafe turns:</strong> Tractor-trailers or large trucks
            have to make wide and slow turns. It is crucial that they only
            attempt a turn when there is no oncoming traffic and when vehicles
            are not so close to them that they will be crushed under the
            trailer. It is also important that all truck drivers obey traffic
            control devices such as stop signs and traffic lights. Truck drivers
            must not only look for other larger vehicles, but also for
            bicyclists and pedestrians especially at street intersections when
            they are about to turn.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/Gd0KQv57Rq0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>
          Lack of Vehicle Maintenance On Older Cars Throughout San Bernardino,
          California
        </h2>
        <p>
          All truck drivers must inspect their vehicles before every trip. It is
          particularly important to check the truck's brakes, lights and tires.
        </p>
        <p>
          When the brakes are worn or defective there is the potential for the
          trailer to swing out of control or jackknife. When the lights are not
          working properly, the truck may not be visible to other motorists when
          it is dark outside.
        </p>
        <p>
          When the tires are worn or faulty, there is the potential for a tire
          blowout, which can also cause the truck to go out of control.
        </p>
        <p>
          There are strict guideline, rules, regulations and laws set for the
          maintenance of semi trucks and other commercial vehicles, set by OSHA,
          to insure safe travel for commercial drivers and others on the roads,
          at all times.
        </p>
        <p>
          You can read more at{" "}
          <Link
            to="https://www.osha.gov/SLTC/trucking_industry/vehiclemaintenance.html#General"
            target="new"
          >
            OSHA.gov/SLTC/trucking-industry
          </Link>
          .
        </p>
        <h2>Improperly Loaded Cargo Traveling Through San Bernardino</h2>
        <p>
          When the trailer is loaded incorrectly, it can cause the truck to
          easily veer out of control. These types of Inland Empire traffic
          accidents may involve a runaway trailer or a jackknife accident.
        </p>
        <p>
          In either case it must be determined who failed to properly inspect
          the trailer and who was responsible for the way the cargo was loaded.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san bernardino truck accident law firm"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-san-bernardino.jpg')"
            }}
          >
            <h2>Hire an Accident Attorney in San Bernardino</h2>
          </div>
        </LazyLoad>

        <p>
          If you have been injured in a collision with a big rig, the
          experienced <strong> San Bernardino truck accident lawyers</strong> at{" "}
          <strong> Bisnar Chase</strong> can help you better understand your
          legal rights and options.
        </p>
        <p>
          <strong> The Southern California Law Offices of Bisnar Chase </strong>
          has won over <strong> $500 Million </strong>for our clients and have
          established a <strong> 96% Success Rate. We</strong> will put more
          than
          <strong> 40 years of experience</strong> to work for you and fight for
          your rights every step of the way.
        </p>

        <center>
          <p>
            Contact our personal injury attorneys to schedule a{" "}
            <strong> Free Case Evaluation.</strong>
          </p>
        </center>

        <p>
          For immediate assistance <strong> Call 909-253-0750</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  473 E Carnegie Dr. 2nd Floor, Room 221
                </div>
                <div>
                  <span itemProp="addressLocality">San Bernardino</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92408</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(909) 253-0750</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <p>
          <strong>
            {" "}
            You can also reach us at our{" "}
            <Link to="/los-angeles/contact">Los Angeles</Link> or{" "}
            <Link to="/orange-county/contact">Orange County</Link> law offices.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
