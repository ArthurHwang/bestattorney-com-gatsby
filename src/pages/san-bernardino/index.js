// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/san-bernindino-personal-injury-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Bernardino Personal Injury Lawyers - Inland Empire Accident Lawyers"
        pageDescription="Contact the San Bernardino Personal Injury Attorneys of Bisnar Chase if you have been injured in a car accident, auto defect crash or dog bite. One of our America's Top 100 Trial Lawyers may win you the millions you deserve for your accident. Call 909-253-0750 for a Free consultation"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Bernardino Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="San Bernardino personal injury lawyers"
            title="San Bernardino personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            The <strong> San Bernardino Personal Injury Lawyers</strong> of
            Bisnar Chase have won over
            <strong> $500 million in compensation</strong> from those who have
            experienced pain and suffering from a catastrophic incident.
          </p>
          <p>
            Call <strong> 909-253-0750</strong> to speak with one of our legal
            team members.
          </p>
          <p>
            We have <strong> over 40 years of experience</strong> and have
            established a{" "}
            <strong>
              {" "}
              96% Success Rate in winning recovery for our clients
            </strong>
            .
          </p>

          <p>
            If you've been injured by no fault of your own and are asking
            questions such as "how do I find a lawyer near me?", contact our San
            Bernardino injury attorneys nearby for a{" "}
            <strong> free consultation</strong>.
          </p>
        </div>
        <h2>Personal Injuries in San Bernardino You Can Sue For</h2>
        <p>
          There are approximately 2.1 million residents in{" "}
          <Link
            to="https://factfinder.census.gov/faces/nav/jsf/pages/community_facts.xhtml?src=bkmk"
            target="_blank"
          >
            {" "}
            San Bernardino County
          </Link>
          , which is only 20,105 square miles. With less than 5 major freeways,
          many side streets, stop lights, intersections and other roadways,
          accidents can be inevitable. In San Bernardino County, there are some
          incidents that occur more frequently than others though.
        </p>
        <p>
          <strong> Common personal injuries in San Bernardino include</strong>:
        </p>
        <ol>
          <li>
            <strong> Car accidents</strong>: In one year, an estimated amount of
            1,604 drivers were killed in a car accident. Victims of vehicle
            collisions, who do survive are left with catastrophic injuries such
            as broken bones, traumatic brain injuries and some may even end up
            paralyzed for the rest of their lives. If you have been involved in
            a motor vehicle accident the best way to maximize the car accident
            compensation you will receive is by providing your personal injury
            accident lawyer or car accident lawyer in San Bernardino with
            evidence such as photos, hospital bills and witness accounts.
          </li>
          <li>
            {" "}
            <Link to="/san-bernardino/auto-defects">
              <strong> Auto defects</strong>
            </Link>
            : Victims who were involved in accidents that were caused by a
            seller’s or manufacturer’s negligence have the right to file an auto
            defect lawsuit. Typical claims can involve defects with the brake
            system, electrical system or transmission. California enforces a
            "strict product liability" regulation that states that if a consumer
            was injured from the use of a product, the designer, seller or
            manufacturer of the product is responsible for the damages that
            follow that accident. If a car defect becomes common amongst a
            specific organization, this can lead to an auto recall. A San
            Bernardino injury lawyer can inform you more on your legal options
            as an auto defect accident victim.
          </li>

          <li>
            <strong> Premise liability</strong>: The term premise liability is
            known as a personal injury that took place due to poor conditions on
            an owner’s property. Property owners have the responsibility to
            maintain the upkeep of their estate, failure to do so may compel an
            injury victim to find a lawyer so they can explore their legal
            options. The most common premise liability cases involve a person
            slipping and falling due to broken stairways, slippery floors or
            uneven concrete. Seek medical attention right away to not only
            receive treatment for property injuries you have experienced but to
            also begin collecting documents for your personal injury claim.
          </li>
          <LazyLoad>
            <img
              src="../images/personal-injury/slippery-floor-premise-liability.jpg"
              width="100%"
              alt="personal injury lawyers in San Bernardino"
            />
          </LazyLoad>
          <li>
            <strong> Work-related injuries</strong>: The{" "}
            <Link
              to="https://www.nsc.org/work-safety/tools-resources/infographics/workplace-injuries"
              target="_blank"
            >
              {" "}
              National Safety Council
            </Link>{" "}
            reported that every seven seconds an employee is injured while
            working. Employees who have been injured on the job, need to inform
            their direct manager or supervisor right away. An on-the-job injury
            can hurt an employee financially due to losing wages from time off
            of work. One option that injured workers have to gain recovery from
            their accident is to file a worker’s compensation claim. If the
            claim is denied, then hiring a work injury lawyer in San Bernardino
            would be in an employee’s best interest.
          </li>
          <li>
            <strong> Bicycle accidents</strong>: There are many congested areas
            in San Bernardino. Heavy traffic can often lead to an immense amount
            of bicycle accidents taking place. Common causes of a bike collision
            in San Bernardino can include drivers hitting cyclists when turning
            left, putting a vehicle in reverse without paying attention and
            opening a car door while parked in a bicycle lane. Motorists can
            avoid a bike crash by being attentive and refraining from texting
            while driving. Experts say that a person that is texting driving is
            23 times more likely to be involved in an accident.
          </li>
          <li>
            <strong> Traumatic brain injury</strong>: Catastrophic head injuries
            are one of the most frequent traumas that take place in an accident.
            The{" "}
            <Link
              to="https://www.cdc.gov/traumaticbraininjury/data/tbi-ed-visits.html"
              target="_blank"
            >
              {" "}
              Centers for Disease Control and Prevention
            </Link>{" "}
            reported that in one year over 2.5 million people were admitted into
            the emergency room for brain injuries. What many do not know is that
            brain injuries can progress and worsen over time. You need to keep a
            journal of your symptoms so that your lawyer can adjust the
            compensation that is needed for you to recover. Common head injuries
            from a vehicle collision can include whiplash, a concussion or an
            open head wound.{" "}
          </li>
          <li>
            <strong> Pedestrian accident:</strong>Drivers who failed to pay
            attention to crosswalks should be held accountable for their
            negligence. Pedestrian accidents happen more frequently than people
            know. According to the{" "}
            <Link
              to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/811888"
              target="_blank"
            >
              {" "}
              National Highway Traffic Safety Association
            </Link>
            , an estimated amount of 5,000 people die in pedestrian incidents
            every year. Drivers have the legal duty to adhere to the rules of
            the road. If they fail to do so and you have experienced pain and
            suffering because of it, seek legal representation to receive
            recovery for your losses.{" "}
          </li>
          <li>
            <strong> Motorcycle accidents:</strong> Motorcycle riders are more
            vulnerable to serious injuries on the road due to the lack of
            protection a bike has. Experts urge cyclists to wear{" "}
            <Link
              to="https://www.motorcyclelegalfoundation.com/how-to-choose-motorcycle-safety-gear/"
              target="_blank"
            >
              {" "}
              motorcycle safety gear
            </Link>{" "}
            such as a helmet, a leather protective jacket and strong protective
            pants. This does not mean that a rider is completely protected from
            negligent drivers though. If a drunk driver or reckless driver
            merged into a lane in a hazardous manner, riders can end up with
            serious road rash, broken bones or even end up dead.
          </li>
          <li>
            <strong> Dog bite</strong>: Children are the number one dog bite
            victims in the United States. Small children who experienced a dog
            bite have been admitted to the emergency room for torn skin on the
            arm and even the face. It can take many surgeries to fix the skin
            that had been punctured on the face or the body. Surgeries and
            medical treatment can be costly and many parents have to miss days
            of work to care for their child. Medical bills and lost wages can be
            included in the compensation you and your child can receive.{" "}
          </li>
        </ol>
        <p>
          To view more statistics for San Bernardino County, visit{" "}
          <Link
            to="http://www.ots.ca.gov/Media_and_Research/Rankings/default.asp"
            target="new"
          >
            OTS.CA.GOV
          </Link>{" "}
        </p>
        <h2>California Wrongful Death Cases</h2>
        <p>
          A wrongful death case involves a family member, spouse or surviving
          parent filing a claim on the behalf of their deceased loved one. A
          person who has lost their life due to another person's negligence can
          face serious jail time.
        </p>
        <p>
          Over 13,840 accidents occurred in one year in the state of California.
          Accidents prove to be one of the top leading causes of wrongful deaths
          in California. Although accidents are labeled as unintentional
          injuries this does not mean they could not have been prevented. People
          who have suffered the loss of a loved one do to a negligent employer,
          reckless driver or defective product should speak to a wrongful death
          attorney in San Bernardino.{" "}
        </p>
        <h2>Important Factors You Must Prove in a Personal Injury Lawsuit</h2>
        <p>
          Catastrophic personal injuries happen every day. Even the most{" "}
          <em>minor injuries</em> can accumulate a substantial amount of medical
          expenses. There are key elements in a personal injury case that must
          be proven to gain the maximum amount of compensation for your
          injuries. Those factors are the "duty of care" and the "breach of
          duty".
        </p>
        <p>
          An example of a breach of duty would be if a supervisor neglects to
          put a caution sign on a slippery floor after being informed of a leaky
          fridge. The supervisor has the duty to keep customers safe and
          breached that duty when he failed to take preventive measures for the
          incident.
        </p>
        <p>
          Each injury case differs from the next but there are some important
          aspects that your San Bernardino injury attorney must determine:
        </p>
        <ul>
          <li>Responsibility and who is at fault</li>
          <li>Reason the circumstances were what they were</li>
          <li>
            Did any person or persons flee or leave the scene of the accident of
            location of injury before contact and insurance information was
            exchanged? (this means everyone involved in the situation)
          </li>
          <li>Photos, videos and any other proof or evidence collected</li>
          <li>Accident and injury information documented</li>
          <li>Medical attention received and or documented</li>
          <li>
            Excessive aggression, negligence or wrongdoing involved in the
            situation
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="San Bernardino injury attorneys"
            style={{
              backgroundImage:
                "url('../images/text-header-images/san-bernardino-text-header.jpg')"
            }}
          >
            <h2>What Can I Win From My Personal Injury Case?</h2>
          </div>
        </LazyLoad>
        <p>
          There are two kinds of damages that can be claimed in a personal
          injury lawsuit which are monetary and punitive damages. Monetary
          damages are defined as financial losses that you have faced because of
          the accident. Unlike monetary damages, punitive damages are difficult
          to determine. A punitive damage is the actual punishment that the jury
          will implement on the wrongdoer.
        </p>
        <p>
          <strong>
            {" "}
            When you hire a personal injury accident lawyer you can receive
            compensation for damages such as:{" "}
          </strong>
        </p>
        <p>
          <strong> Medical bills</strong>: Seeking medical care or attention
          immediately following a car accident or other personal injury is
          incredibly important. Mostly to your health and well-being, but to
          your case and its outcome, which decides whether you are compensated
          or not. Receiving medical attention and care gives the opportunity to
          have your injuries recorded and documented legally, to prove they are
          in fact related and found as a result of the accident. Not having
          medically documented records can cause major issues and can make it
          impossible to win, continue further, or even start a case.
        </p>
        <p>
          <strong> Pain and suffering</strong>: Accident victims who experience
          pain and suffering usually do not receive this type of compensation
          from the defendant’s insurance company. The way in which your personal
          injury attorney in San Bernardino can prove your pain and suffering is
          through witness accounts from friends or family and a mental health
          expert diagnosing you with conditions like anxiety. If you have dealt
          with pain and suffering from your accident, the compensation you win
          from your case can go to the costs for therapy or medication. Experts
          strongly encourage auto collision victims to seek legal representation
          from emotional distress lawyers near you.{" "}
        </p>
        <p>
          <strong> Lost wages</strong>: In personal injury lawsuits lost wages
          are the amount the injured would have collected if they hadn't been
          harmed in the accident. In work injury suits these are named "back pay
          wages". In California, victims of personal injuries who have lost
          wages after an injury can also lose future earnings (lost earning
          capacity). Under the strict liability rule, the negligent party must
          compensate the victim for the financial setbacks they have faced since
          the incident. Lost wages that can be compensated for can include hour
          or salary pay, commissions, overtime pay, vacation or sick days and
          income from being self-employed.
        </p>
        <h2>The Cost of A San Bernardino, CA Lawyer</h2>
        <p>
          We understand California courts and have a long history fighting
          insurance companies to get you fair compensation. Our law firm offers
          a contingency plan where you not only pay zero if we don't win your
          case, but we shield you from being liable for costs while we handle
          your case. We actually absorb all your costs until the end. And if in
          the end, we don't win, you will not be billed for the fees and costs
          of your case.
        </p>
        <p>
          <strong>Bisnar Chase</strong> is committed to "Excellent
          Representation" of their personal injury clients and is focused on
          recovering the maximum monetary recovery possible for them. We've
          recovered millions of dollars for our Southern California personal
          injury clients.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>How Long Will It Take to Reach a Settlement?</h2>
        <p>
          Personal injuries settlements differ from each accident case. There is
          no set date into which a settlement will be reached, an injury lawsuit
          can take months and in some cases years. The opposing party's
          insurance company will offer you a settlement quickly, but it is
          critical that you receive advice from a legal expert before accepting
          the offer. There may also be delays on receiving your compensation
          when the case has closed.
        </p>
        <p>
          The reason for the delays can be due to if the case involves a large
          amount of compensation, the courts still are determining the amount of
          medical treatment you would need in the future or proving that the
          defendant was, in fact, responsible for the accident.
        </p>
        <p>
          Even though it is tempting, do not hurry to settle your car accident
          case. Many accident victims need to understand why premature
          settlements can be an irreparable mistake.{" "}
        </p>
        <h2>Hire Our San Bernardino Injury Lawyers</h2>
        <p>
          If you have been seriously injured, or suffered damages, you should
          seek the advice of an experienced{" "}
          <strong> San Bernardino Personal Injury Lawyer</strong> before time
          runs out and the statute of limitations makes it impossible for you to
          ever take legal action.
        </p>
        <p>
          <strong> Bisnar Chase </strong> has had the honor of representing many
          individuals and who have fallen victim to personal injuries, car
          accidents and other horrible situations caused by an accident,
          negligence or wrong doing.
        </p>
        <p>
          Our team of skilled personal injury accident lawyers in San Bernardino
          have a reputation for stunning success in accident cases. We have{" "}
          <strong> over 40 years of winning </strong>compensation for clients
          after experiencing a personal injury in California.
        </p>
        <p>
          If you have not received medical care yet in result of an accident or
          other personal injury, you can{" "}
          <Link to="/san-bernardino/contact">contact us</Link> at
          <strong> 909-253-0750 </strong>for immediate assistance, and receive a{" "}
          <strong> Free Consultation.</strong>
        </p>
        <p>Don't miss out on compensation and the justice you deserve.</p>
        <p>
          <strong>
            {" "}
            Let Personal Injury Law Firm of Bisnar Chase Represent You.{" "}
          </strong>
        </p>
        <div
          className="google-maps"
          data-map="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
        ></div>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  473 E Carnegie Dr. 2nd Floor, Room 221
                </div>
                <div>
                  <span itemProp="addressLocality">San Bernardino</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92408</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(909) 253-0750</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        <p>
          <strong>
            {" "}
            You can also reach us at our{" "}
            <Link to="/los-angeles/contact">Los Angeles</Link> or{" "}
            <Link to="/orange-county/contact">Orange County</Link> law offices.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
