// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/san-bernardino-auto-defect-car-accident-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Bernardino Auto Defect Lawyers - Free Consultation"
        pageDescription="San Bernardino Auto Defect Lawyers - Free Case Evaluation - Our Auto Defect Attorneys will Win or You Don't Pay. Call 909-253-0750."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Bernardino Auto Defect Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="san bernardino auto defect lawyers"
            title="san bernardino auto defect lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> San Bernardino Auto Defect Lawyers</strong> here at Bisnar
          Chase have won over <strong> $500 Million</strong> for our clients.
          With over
          <strong> 40 years of experience</strong>, Bisnar Chase has established
          a <strong> 96% Success Rate</strong> and want to represent you.
        </p>
        <p>
          Automobiles are an integral part of our lives. This is true especially
          in Southern California where everyday life is nearly impossible
          without a car.
        </p>
        <p>
          The average person in the United States takes four car trips a day.
          Automakers advertise their vehicles as "safe havens" or as a "refuge
          for the entire family."
        </p>
        <p>
          But are cars always designed and manufactured with as much safety in
          mind as is reasonably and technologically possible. When buying a new,
          used or certified car, you should not have to be concerned about
          whether or not the vehicles' <strong> auto recalls</strong> or{" "}
          <strong> auto defects</strong> have been fixed or not.
        </p>
        <p>
          By law, dealerships must fix any active and current auto recalls and
          or defects before the vehicle is sold to a customer.
        </p>
        <p>
          If you look at the number of vehicles that are recalled each year, you
          will realize that vehicles are not always safe and automakers are not
          always diligent about investing in vehicle safety.
        </p>
        <p>
          If you have been involved in a car accident or have experienced a
          personal injury in result of an auto defect, contact our team of
          skilled
          <strong>
            {" "}
            <Link to="/san-bernardino">personal injury attorneys</Link> in San
            Bernardino{" "}
          </strong>
          for a <strong> Free Consultation</strong>and{" "}
          <strong> Case Evaluation</strong>.
        </p>
        <p>
          For immediate assistance <strong> Call 909-253-0750</strong>.
        </p>

        <h2>Why Choose the San Bernardino Law Offices of Bisnar Chase?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/san bernardino brian auto defect safe car crash accidents test attorneys lawyers personal injury.jpg"
            width="180"
            className="imgright-fluid"
            alt="san bernardino auto defect lawyer brian chase"
          />
        </LazyLoad>
        <p>
          The <strong> San Bernardino auto defect lawyers</strong> at{" "}
          <strong> Bisnar Chase</strong> have a long and successful track record
          of handling these types of cases. We have the knowledge, skill and
          tenacity to stand up and fight against large automakers.
        </p>
        <p>
          We are not intimidated by them. Instead, we are driven by the passion
          to get justice and fair compensation for our clients.
        </p>
        <p>
          We hope that our work in the area of product liability will help
          change the shape of cars in the future so they are safer for all
          consumers, not just those who can pay more to get better safety
          features. Auto safety should not be a luxury, it should be deemed a
          necessity.
        </p>
        <p>
          If you have been injured due to a{" "}
          <strong> defective automobile</strong>, please contact us at{" "}
          <strong> Call 909-253-0750</strong> to obtain more information about
          pursuing your legal rights.
        </p>
        <h2>Is it Always about Driver Error?</h2>
        <p>
          When a catastrophic or fatal accident occurs, the tendency on the part
          of most people is to conclude that the drivers involved did something
          wrong. While in many cases, it is true that drivers are responsible
          for crashes, it is also often true that a{" "}
          <strong> product defect</strong> also contributed to the accident and
          or injuries.
        </p>
        <p>
          For example, a driver may have overcorrected and caused an accident or
          rollover. However, if the roof is well designed so it does not crush
          or the seatbelt buckle is designed so it is strong and does not
          unlatch, then, the vehicle's occupants will survive the accident and
          come out of it with minimal injuries.
        </p>
        <p>
          However, in far too many cases, the role of a defective auto part is
          underplayed or never identified, the driver is blamed, the case is not
          litigated properly and the victim and his or her family do not get the
          justice they deserve.
        </p>
        <p>
          Driver error is also a common excuse automakers freely use to escape
          liability for their defective products.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="san bernardino car accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/working-on-car-auto-defect-san-bernardino.jpg')"
            }}
          >
            <h2>What Constitutes an Auto Defect?</h2>
          </div>
        </LazyLoad>

        <p>
          Federal law defines auto safety as the performance of a vehicle or
          equipment in a way that protects consumers against the "unreasonable
          risk of accidents" that occur due to the design, construction or
          performance of a motor vehicle, and against the unreasonable risk of
          death or injury in an accident.
        </p>
        <p>
          A safety defect is a problem that exists in a motor vehicle or vehicle
          part that poses a risk to motor vehicle safety and may exist in a
          group of vehicles or vehicle parts of the same design or manufacture.
        </p>
        <p>
          Auto Defects that can cause accidents, injuries or worsen the outcome
          of the situation consist of but are not limited to:
        </p>
        <ul>
          <li>Defective Seatbelts</li>
          <li>Faulty Airbag Deployment</li>
          <li>Weak Roof and Roof Crush</li>
          <li>Carbon Monoxide Poisoning</li>
          <li>Loss of Power Steering</li>
          <li>Brake Failure</li>
          <li>Loss of Engine or Transmission Power</li>
        </ul>
        <h2>Auto Recalls</h2>
        <p>
          A vehicle or part recall becomes necessary when a motor vehicle or a
          part, including tires, does not comply with the Federal Motor Vehicle
          Safety Standard. Recalls are issued when there is a safety-related
          defect in the vehicle or equipment as well.
        </p>
        <p>
          Automakers are required to report any safety defect relating to a
          vehicle or part within five days of finding out about it. However,
          automakers are not always diligent about following the law.
        </p>
        <p>
          We have seen several examples of such lack of diligence including
          Toyota, which stalled for years before recalling more than 10 million
          vehicles for sudden acceleration issues.
        </p>
        <p>
          Automakers, after they issue a recall, are required to fix the problem
          for free. This often costs millions of dollars, which is why these
          manufacturers try to avoid or stall a recall as much as possible.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/s1Bf3UA5re0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Putting Profits Before People</h2>
        <p>
          Often, we find that automakers do not need to make a major financial
          commitment to make vehicles and safety systems safer. For example,
          General Motors could have fixed its defective ignitions for under $1
          per vehicle. Instead, the company chose not to act on the matter.
        </p>
        <p>
          Today, 13 people are dead as a result of that negligence. Many cars
          today have seats that are no better than lawn chairs. It would not
          take much to make them stronger. But automakers don't act on it. The
          same goes for safer windshields, window glass, airbags, seatbelts and
          stronger roofs.
        </p>
        <p>
          The question is: When will safety become a priority for these
          automakers?
        </p>
        <h2>Preserving the Evidence</h2>
        <p>
          If you suspect that you have been involved in an accident or injured
          as the result of a faulty vehicle or vehicle parts, it is crucial that
          you preserve the vehicle in its current state, unaltered.
        </p>
        <p>
          It is absolutely critical that an expert thoroughly examines the
          vehicle for evidence of manufacturing defects, malfunctions and design
          flaws.
        </p>
        <p>
          An experienced auto defect law firm will be able to send out a
          spoliation letter to the parties involved so that the vehicle is
          preserved. The vehicle is the single most important piece of evidence
          in an auto defect case.
        </p>

        <h2>Let Bisnar Chase Represent You</h2>
        <p>
          Our team of skilled and experienced
          <strong> San Bernardino Auto Defect Lawyers </strong>have over{" "}
          <strong> 40 years of experience</strong>, having won over{" "}
          <strong> $500 Million </strong>for our clients and establishing a{" "}
          <strong> 96% Success Rate</strong>.
        </p>
        <p>
          There is no corporation too big or too powerful for Bisnar Chase. We
          have taken on and won complex cases that other well-known and
          reputable law firms have turned down due to lack of confidence,
          resources and inability to do so.
        </p>
        <p>
          Auto manufacturers, auto corporations and auto part companies need to
          be held accountable for the negligence of safety protocol their
          customers vulnerably trust in them. When something goes wrong and
          someone is injured, killed or property is damaged, repercussions will
          be met with justice, compensation to the victims and Bisnar Chase has
          the proven track record of doing so.
        </p>
        <p>
          If you or a loved one has been injured due to an{" "}
          <strong> auto defect</strong>, call us and receive a{" "}
          <strong> Free Consultation </strong>and{" "}
          <strong> Case Evaluation</strong>. For immediate assistance,{" "}
          <strong> Call 909-253-0750</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  473 E Carnegie Dr. 2nd Floor, Room 221
                </div>
                <div>
                  <span itemProp="addressLocality">San Bernardino</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92408</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(909) 253-0750</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3305.0657484712356!2d-117.2768493842638!3d34.067828780601744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcac84b6d6217b%3A0xbd25156f9c9a598f!2s473+E+Carnegie+Dr+%23221%2C+San+Bernardino%2C+CA+92408!5e0!3m2!1sen!2sus!4v1505230188886"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        <p>
          <strong>
            {" "}
            You can also reach us at our{" "}
            <Link to="/los-angeles/contact">Los Angeles</Link> or{" "}
            <Link to="/orange-county/contact">Orange County</Link> law offices.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
