// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Oxnard Personal Injury Lawyers - Bisnar Chase"
        pageDescription="If you've been injured by a car accident, dog bite, or other negligence, contact Bisnar Chase to get fair compensation for your injuries and bills!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Oxnard Personal Injury Accidents Happen All the Time</h1>
        <BreadCrumbs location={location} />

        <p>
          Oxnard, California is the largest city in Ventura County and known as
          an one of the world's most important agricultural hubs. The city
          connects many forms of transportation, including Amtrak, Union
          Pacific, Metrolink, Greyhound and more, so the approximately 200,000
          Oxnard residents have seen a variety of personal injury accidents over
          the years.
        </p>
        <p>
          According to city-data statistics, Oxnard's fatal accident count is
          about equal to the California average in recent years. The California
          average, however, is relatively high with many DUI accidents and
          hit-and-runs. Oxnard has seen it's fair share of these and local law
          enforcement is doing their best to crack-down on fatal car accidents
          involving driving under the influence. Just recently, the Ventura
          County Star news source reported that the police made 108 traffic
          stops one evening in January 2010 as part of a procedure to apprehend
          risky driving. They found seven DUIs and four of these people had a
          blood alcohol content (BAC) over twice the legal limit. Luckily, they
          were able to prevent <Link to="/dui">drunk driving accidents</Link>{" "}
          before they took place.
        </p>
        <p>
          Unfortunately, hit-and-run is another issue. In 2008, a 19-year-old
          man was charged with murder, second-degree burglary of a vehicle, and
          hit-and-run after he caused a traffic accident that caused the death
          of a mother of three. The police reported that the young man ran a red
          light and broadsided a pick-up truck, killing a passenger, and
          seriously injuring the driver and one of the children in the car. He
          then fled from the scene and was later found.
        </p>
        <h2>Defective Toyotas Worry Oxnard Residents</h2>
        <p>
          A recent announcement warns Toyota drivers of possible defects in
          certain models' accelerators. Drivers of Toyotas and car dealers alike
          are concerned because it is not clear what should be done. Toyota
          manufacturers have recalled many cars, but there are thousands more
          Toyotas on the road still, the drivers of which do not know whether to
          expect dangerous situations.
        </p>
        <p>
          Any{" "}
          <Link to="/locations/oxnard-car-accidents">
            Oxnard personal injury attorney
          </Link>{" "}
          will tell you that this is a sticky situations, and that Toyota
          drivers should take necessary precautions to ensure safety.
        </p>
        <p>
          It seems many people are awaiting more information about their
          vehicles, but in the mean time, Safety Research Strategies reports
          that 2,274 incidents of "sudden unintended acceleration in Toyota
          vehicles" are known and have caused at least 275 crashes and 18 deaths
          since 1999.
        </p>
        <h2>Oxnard Smells Personal Injury Risk on the Job</h2>
        <p>
          The Ventura County Probation Agency and Human Services Agency offices
          building was recently evacuated after smelling gas. This is the second
          time people have had to evacuate the building for this reason.
          Firefighters came in to investigate and could not find the exact
          source of the gas odor, and they believe it is not in the building,
          but many employees in the building complained of feeling sick from the
          smell.
        </p>
        <p>
          A gas leak in a work environment could lead to{" "}
          <Link to="/job-injuries">Oxnard job injury</Link> and put people at
          risk for illness. Though the gas leak may not be in the building, it
          may be somewhere outside and the fumes are blown into the building by
          wind, and could continue to be detrimental for affected workers.
        </p>
        <h2>
          Oxnard Personal Injury Lawfirms Know How to Represent Your Needs
        </h2>
        <p>
          If you or someone you love has ever been the victim of a drunk driving
          or hit-and-run accident, a train accident, an auto defect, an unsafe
          workplace, or anything else, you owe it to yourself to get in contact
          with a good personal injury lawyer.
        </p>
        <p>
          <strong>Los Angeles Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th Fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">90015</span>
            <br />
            <span className="tel">(323) 238-4683</span>{" "}
          </div>
        </div>
        <h2>Oxnard Legal Resources</h2>
        <p>
          See <Link to="/car-accidents">California car accident lawyer</Link>,{" "}
          <Link to="/">California personal injury lawyer</Link> and{" "}
          <Link to="/locations/oxnard-car-accidents">
            Oxnard car accident lawyer
          </Link>{" "}
          for more information.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
