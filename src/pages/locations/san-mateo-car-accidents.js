// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Mateo",
      population: 101128,
      totalAccidents: 2192,
      intersection1: "3rd Ave & El Camino Real ",
      intersection1Accidents: 24,
      intersection1Injuries: 18,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 178.1,
      city1Name: "Hillsborough",
      city1Index: 31.9,
      city2Name: "Foster City",
      city2Index: 62.0,
      city3Name: "Belmont",
      city3Index: 135.8,
      city4Name: "Burlingame",
      city4Index: 138.2
    },
    locationBox3: {
      intersection2: "Hillsdale Blvd & Norfolk St",
      intersection2Accidents: 21,
      intersection2Injuries: 18,
      intersection2Deaths: 0,
      intersection3: "El Camino Real & 31st Ave",
      intersection3Accidents: 18,
      intersection3Injuries: 16,
      intersection3Deaths: 0,
      intersection4: "El Camino Real & 20th Ave",
      intersection4Accidents: 14,
      intersection4Injuries: 11,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Mateo Car Accident Lawyer - Bisnar Chase 800-561-4887"
        pageDescription="Contact our San Mateo car accident attorneys for a free consultation and no fee, no win promise. Over $500 Million in wins for our clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Mateo Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          If you've been injured in a car accident please call our San Mateo{" "}
          <Link to="/car-accidents">car accident attorneys</Link> for a free
          consultation. We offer a no win, no fee guarantee and advance all
          costs until your case closes. With over thirty five years in personal
          injury law our team has the skills and resources to represent some of
          the toughest cases.
        </p>

        <p>
          The city of San Mateo in San Mateo County, California has nearly
          96,000 people. As one of the larger suburbs on the San Francisco
          Peninsula, the city has a lower than average accident rate. San Mateo
          injury attorneys attribute this to the city's comprehensive traffic
          enforcement programs.
        </p>
        <p>
          According to the National Highway Traffic Safety Administration,
          accident statistics from 2001 to 2003 show that San Mateo had six
          fatal car collisions. This averages out to 0.6 fatal San Mateo car
          crashes per 12,000 people--far below the national norm.
        </p>
        <p>
          In 2005, San Mateo implemented a Red-Light Photo Enforcement program.
          The city installed red-light cameras at major crash-prone
          intersections. These included cameras at Hillsdale and Saratoga Drive,
          Hillsdale and Norfolk Street, 4th Avenue and Humboldt Street, El
          Camino Real and Ralston Avenue, and Ralston Ave and Old County Road.
        </p>
        <p>
          To address residents' concerns related to speeding, and to further
          bring down its accident rate, San Mateo adopted a number of Traffic
          Calming Procedures. Calming efforts included installing speed humps on
          Edinburgh Street between Virginia Avenue and Fordham Road. This was
          based on recent speed surveys that indicated speed humps slowed
          traffic on Edinburgh by from 32 mph to 25 mph.
        </p>
        <p>
          The San Mateo Police Department kept the pressure on speeders with
          advanced laser equipment and new radar guns. The city was determined
          to slow down motorists and reduce the number of crash injuries.
          Statistics compiled by the city showed that speed is a major factor in
          injury crashes.
        </p>
        <p>
          Another major factor in car crashes is the drunk driver. To keep these
          impaired drivers off the road, San Mateo County swung into action with
          its Avoid the 23 campaign. The effort united the resources of 23
          county law enforcement agencies to come down on drunk drivers using
          such tools as public education, community help and strict, responsive
          enforcement.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
