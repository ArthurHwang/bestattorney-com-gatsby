// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Fontana",
      population: 203003,
      totalAccidents: 5320,
      intersection1: "Valley Blvd & Sierra Ave",
      intersection1Accidents: 354,
      intersection1Injuries: 65,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 223.5,
      city1Name: "Rialto",
      city1Index: 298.6,
      city2Name: "Rancho Cucamonga",
      city2Index: 185.4
    },
    locationBox3: {
      intersection2: "Cherry Ave & Valley Blvd",
      intersection2Accidents: 354,
      intersection2Injuries: 65,
      intersection2Deaths: 0,
      intersection3: "Slover Ave & Cherry Ave",
      intersection3Accidents: 142,
      intersection3Injuries: 17,
      intersection3Deaths: 0,
      intersection4: "Foothill Blvd & Cherry Ave",
      intersection4Accidents: 65,
      intersection4Injuries: 8,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fontana Car Accident and Injury Lawyers - Bisnar Chase"
        pageDescription="If you need an experienced Fontana car accident lawyer, Bisnar Chase will be there to walk you through each step. Learn more now! "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fontana Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            An Experienced injury attorney can be especially helpful
          </strong>{" "}
          if you get into a car accident in Fontana. Getting fair compensation
          from insurance companies is difficult to do when you don't know the
          tricks they use to lower your claim value. Bisnar Chase as experience
          with car accident cases in Fontana and all over California. We are
          also skilled auto and product defect lawyers, and have represented
          many clients in dog bite and premises liability cases. For a free
          consultation to see whether you have a case, fill out our form or call
          us!
        </p>
        <h2>Some Revealing Statistics About Fontana Car Accidents</h2>
        <p>
          The California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) shows some interesting data about Fontana car
          accidents. From 2009-2014, 15,000 accidents were reported, but
          accidents that happened at the most dangerous intersections in Fontana
          did not result in many injuries. The amount of car accidents in
          Fontana is about average for the population, but fortunately, it seems
          as if Fontana car accidents are much less likely to result in an
          injury, either due to good road conditions or good city
          infrastructure.
        </p>
        <h2>Useful Advice from One Car Accident Lawyer</h2>
        <p>
          Knowledgeable Fontana car accident lawyers can provide some very
          useful advice to car accident victims. They can prevent you from
          making the common mistakes that so often rob car accident victims of
          full compensation. Here's just one example: If your damaged car is
          being repaired, and you choose to rent a car through your insurance
          plan, don't immediately accept the $20 or $25 per day rental
          allowance. Consider first the fair rental value of your damaged car.
          If it's an SUV or luxury sedan, it probably rents for far more than
          the insurance firm's allowance. If this is the case, document the
          difference and claim that difference as part of your settlement.
        </p>
        <p>
          Skilled Fontana personal injury attorneys further advise that if your
          car is in a towing yard, and you're fairly certain that the other
          driver was responsible for the accident, you may still be hit with the
          storage fees if the other motorist is not fully insured. In this
          applies to you, you should pull your car out of the yard as soon as
          possible, because you may not be reimbursed for this expense.
        </p>
        <p>
          When dealing with economic damages, be sure to eliminate any
          unnecessary expenses, especially any costs that don't qualify for
          reimbursement. By the same token, you'll want to include every item
          that qualifies for reimbursement.
        </p>
        <h2>
          The Best Car Accident Lawyers offer No Charge, No Pressure
          Consultations
        </h2>
        <p>
          Trustworthy Fontana car crash lawyers will offer no charge, no
          pressure consultations to people who have suffered a car accident.
          Going it alone in the face of today's complicated personal injury law
          can be risky. A skilled car accident lawyer can tell you things you
          need to know if you want to be justly and fully compensated for your
          car accident expenses.
        </p>
        <p>
          For more useful advice, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar.
        </p>
        <p align="center">Call 949-203-3814 Today!</p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
