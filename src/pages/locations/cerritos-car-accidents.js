// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Cerritos",
      population: 49707,
      totalAccidents: 3039,
      intersection1: "Gridley Rd & South St",
      intersection1Accidents: 158,
      intersection1Injuries: 58,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 232.5,
      city1Name: "Artesia",
      city1Index: 235.5,
      city2Name: "La Palma",
      city2Index: 131.2,
      city3Name: "Hawaiian Gardens",
      city3Index: 198.4,
      city4Name: "Norwalk",
      city4Index: 242
    },
    locationBox3: {
      intersection2: "Carmenita Rd & 183rd St",
      intersection2Accidents: 70,
      intersection2Injuries: 45,
      intersection2Deaths: 0,
      intersection3: "183rd St & Bloomfield Ave",
      intersection3Accidents: 68,
      intersection3Injuries: 42,
      intersection3Deaths: 0,
      intersection4: "South St & Studebaker Rd",
      intersection4Accidents: 77,
      intersection4Injuries: 33,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cerritos Car Accident Lawyer - Bisnar Chase - 323-238-4683"
        pageDescription="Call 323-238-4683 for a Cerritos car accident attorney specializing in serious car accidents. over $500 Million in wins for our clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cerritos Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          A skilled Cerritos car accident attorney will advise you to be concise
          and detailed about the damages and losses you sustained in a car
          crash. Fail to do so and you may not be fully compensated by an
          insurance company. Bisnar Chase has represented accident and injury
          cases in Cerritos with great success and we take care of all of our
          clients' needs. <Link to="/los-angeles/contact">Contact us</Link> to
          see if you have a case for your injury!
        </p>

        <h2>A Car Accident Lawyer Can Assist You With Accident Claims</h2>
        <p>
          Highly{" "}
          <Link to="/los-angeles/car-accidents">
            experienced car accident lawyers
          </Link>{" "}
          will tell their clients to make a comprehensive list of each item that
          qualifies for reimbursement with their insurance company. "This should
          be part of the letter of damages send to the insurance adjuster," says
          John Bisnar. "Unfortunately, many car crash victims try to settle
          their property damage claims without first consulting a car accident
          lawyer. Most lawyers advise against doing this. That's because car
          accident victims are typically overwhelmed by their accident and fail
          to consider anything but the loss of their vehicle. Many times, they
          won't list expensive navigation systems, CD and DVD players, custom
          wheels and other pricey aftermarket accessories."
        </p>
        <p>
          If the damaged car was an expensive Porsche, Mercedes or other
          high-end vehicle that had to be repaired, these vehicles will suffer
          considerably in resale value. Keep in mind that insurance companies
          are driven to cut costs and offer you only minimal reimbursements for
          your damages. As soon as you sign a release of property damage claims,
          you can't claim any more items you may have forgotten to list. It's
          the same with the depreciation of your repaired vehicle. If it's not
          on the list of damages, it's your loss.
        </p>
        <h2>Consult an Experienced Car Accident Lawyer</h2>
        <p>
          A savvy personal injury lawyer knows how to get the most out of
          insurance companies. He or she can make sure you are fairly
          compensated for your losses. Some of the best lawyers offer no charge,
          no pressure consultations. And these lawyers work on a contingency
          basis (you pay fees only if they win or settle your case).
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
