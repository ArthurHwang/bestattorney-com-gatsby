// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {
  location: "orange-county"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Aliso Viejo Brain Injury Attorneys of Bisnar Chase."
        pageDescription="Contact the Aliso Viejo brain injury lawyers. Best client support, directed medical care and no obligation. Success for over 35 years in Orange County."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Aliso Viejo Brain Injury Attorney</h1>
        <BreadCrumbs location={location} />

        <p>
          <img
            src="/images/brain-injury/Brain_2.jpg"
            alt="Aliso viejo brain injury lawyers"
            className="imgright-fixed"
          />
          Hope and healing are the cornerstones to recovering from a brain
          injury. Protecting your rights after the accident is the cornerstone
          of our personal injury law firm. Our Aliso Viejo{" "}
          <Link to="/head-injury">brain injury attorneys</Link> are caring,
          compassionate and driven to help you obtain compensation for an injury
          at the fault of another. We've been serving Orange County and Southern
          California for over three decades and are committed to your rightful
          recovery.
        </p>

        <p>
          Brain injuries are often the very worst type of physical injuries
          simply because there is no way to predict exactly how the injury will
          affect someone or how long the symptoms will last.
        </p>
        <p>
          Many brain traumas are permanent, while others heal slowly or quickly.
          Much depends on the individual patient, the exact nature of the
          injury, and how the head trauma is handled.
        </p>
        <h2>Medical Treatment for a Brain Injury</h2>
        <p>
          Generally speaking, the more rapidly the victim receives treatment
          after the accident, the better the ultimate outcome. However, even
          this is not a hard and fast rule; some victims receive immediate
          treatment and still suffer terrible, long-term effects from brain
          damage, while others blithely walk away from an accident with a
          concussion and never realize there is anything wrong.
        </p>
        <p>
          Brain damage is very unpredictable in its progress, and even the best
          doctors may not be able to fix everything. While the treatments for
          severe concussions and traumatic brain damage have improved greatly in
          recent years, there are still many things doctors do not understand
          about the brain and how it heals.
        </p>
        <p>
          As research develops in this area, patients are seeing a better way to
          treat head trauma including severe brain injury,{" "}
          <Link
            to="https://www.brainandspinalcord.org/traumatic-brain-injury-types/anoxic-brain-injury/index.html"
            target="_blank"
          >
            anoxic brain trauma
          </Link>{" "}
          and long term concussions. The key however, is finding the right
          medical care. Our injury attorneys and connections in the legal field
          for over 35 years puts some of the best doctors at our disposal. Part
          of treating your brain injury claim is making sure you get the best
          medical care.
        </p>
        <h2>Brain Injury Claim</h2>
        <p>
          Besides the obvious need to collect funds to pay for medical care,
          brain injury victims and their families are entitled to payments for
          their pain and suffering and the loss of lifestyle they have
          experienced. Some serious head injury victims will never recover; they
          may have lost cognitive ability or physical function, and that may not
          return. It is unfair to ask the victim to bear the brunt of this loss
          without compensation from the person who caused the accident.
        </p>
        <p>
          A legal team needs to provide you or your family with the tools you
          need to seek compensation from the at-fault party. If you've been
          involved in an accident or injury resulting in severe or long term
          brain injuries like long term concussions, traumatic brain injury, or
          a similar head injury, contact our offices for a{" "}
          <Link to="/contact">free case evaluation</Link>.
          <strong> Call 949-203-3814.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
