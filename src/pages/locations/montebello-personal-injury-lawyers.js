// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Montebello",
      population: 63495,
      totalAccidents: 2455,
      intersection1: "Via Campo & Wilcox Ave",
      intersection1Accidents: 64,
      intersection1Injuries: 64,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 207.0,
      city1Name: "Pico Rivera",
      city1Index: 232.1,
      city2Name: "Commerce",
      city2Index: 264.1,
      city3Name: "Monterey Park",
      city3Index: 159.3,
      city4Name: "Bell Gardens",
      city4Index: 194.5
    },
    locationBox3: {
      intersection2: "Via Campo & Garfield Ave",
      intersection2Accidents: 65,
      intersection2Injuries: 51,
      intersection2Deaths: 0,
      intersection3: "Montebello Blvd & Beverly Blvd",
      intersection3Accidents: 53,
      intersection3Injuries: 52,
      intersection3Deaths: 0,
      intersection4: "Greenwood Ave & Washington Blvd",
      intersection4Accidents: 55,
      intersection4Injuries: 41,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Montebello Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Have you been injured in a Montebello accident, dog bite, or premises case? Bisnar Chase fights for injured victims to get their fair compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Montebello Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            When you have been seriously injured in a preventable accident
          </strong>{" "}
          that was caused by someone else's negligence or wrongdoing, the
          consequences for victims can be devastating physically, emotionally
          and financially. You may find yourself temporarily disabled or with
          permanent injuries and disabilities, not to mention dealing with the
          stress and emotions of a traumatic incident. Such injuries also result
          in significant expenses. You may be looking at mounting medical
          expenses, lost income, and other costs relating to the incident and
          injury.
        </p>
        <p>
          The experienced Montebello personal injury attorneys at Bisnar Chase
          have a long and successful track record of helping individuals who
          have suffered serious and catastrophic injuries as a result of car
          accidents, defective products and other incidents caused by the
          negligence of others. We can help you get back on your feet by
          securing just compensation for your losses and holding the at-fault
          parties accountable. Contact us to see if you have a case today!
        </p>

        <h2>Taking Prompt Action</h2>

        <p>
          When you have been injured in a car accident or any other type of
          traumatic incident, there are several steps you can take to ensure
          that your rights and best interests are protected. Make sure that you
          file a report with those in charge. For example, if you've been
          injured in a car accident, be sure to file a police report and get a
          copy of the report for your records.
        </p>

        <p>
          Be sure to get prompt medical attention for any injuries you may have
          sustained. This not only helps put you on the road to recovery, but
          also creates a record of your injuries and the treatment and care you
          receive. Save all receipts and invoices for expenses you incurred as a
          result of the incident and the injury. Contact an experienced
          Montebello personal injury lawyer who will remain on your side, fight
          for your rights and ensure that you are fairly compensated for all
          your losses.
        </p>

        <h2>How an Experienced Lawyer Can Help</h2>

        <p>
          Victims of car accidents and other serious personal injuries are often
          victimized by insurance companies who deny valid claims and pounce on
          any mistakes unsuspecting victims might make during a time when they
          are most traumatized and vulnerable. For more than three decades, the
          experienced Montebello personal injury attorneys at Bisnar Chase have
          set the bar high for superior customer service and quality legal
          representation. Our attorneys know how to deal with insurance
          companies. We are passionate in our pursuit of justice and
          compensation for our injured clients. Call us at (800) 561-4887 for a
          free consultation and comprehensive case evaluation.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
