// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Compton",
      population: 97877,
      totalAccidents: 3243,
      intersection1: "Artesia Blvd & Wilmington Ave",
      intersection1Accidents: 147,
      intersection1Injuries: 42,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 545.4,
      city1Name: "Lynwood",
      city1Index: 314.7,
      city2Name: "Paramount",
      city2Index: 304.2,
      city3Name: "South Gate",
      city3Index: 322.7,
      city4Name: "Torrance",
      city4Index: 136.2
    },
    locationBox3: {
      intersection2: "Central Ave & Compton Blvd",
      intersection2Accidents: 66,
      intersection2Injuries: 42,
      intersection2Deaths: 1,
      intersection3: "Santa Fe Ave & Artesia Blvd",
      intersection3Accidents: 62,
      intersection3Injuries: 36,
      intersection3Deaths: 0,
      intersection4: "Wilmington Ave & Compton Blvd",
      intersection4Accidents: 58,
      intersection4Injuries: 32,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Compton Car Accident Lawyer - Los Angeles County Auto Accident Attorney"
        pageDescription="Call 949-203-3814 for a Comptom car accident lawyer serving Compton and all of southern California.  Talk to an expert before you make any decisions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Compton Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Truly successful injury attorneys understand the critical nature of
            timing
          </strong>{" "}
          in settling car accident injury claims. However, before delving into
          the details of when to file and some insurance company tactics, a
          quick glance at car accident statistics in Compton, California may be
          helpful. If you've been injured in a car accident or think you have a
          personal injury case, read our{" "}
          <Link to="/resources">personal injury resources</Link> or call us for
          a free consultation!
        </p>
        <h2>Car Accidents in Compton Rise to Troubling Levels</h2>
        <p>
          A look at the California Highway Patrol's Statewide Integrated Traffic
          Records System (SWITRS) revealed that in 2006, nine people died and
          359 people were injured in Compton car crashes. Three pedestrians were
          killed and 27 were injured in motor vehicle mishaps. And bicycle
          accidents injured 18. In addition, motorcycle accidents killed one and
          injured 20 people. DUIs caused four deaths and 38 injuries. In 2007,
          eight car accidents resulted in as many fatalities. The following
          year, nine car collisions caused nine fatalities.
        </p>
        <h2>Don't Wait Too Long to Settle Car Accident Injury Claims</h2>
        <p>
          "Savvy Compton car collision attorneys realize that timing can mean
          the difference between a just compensation settlement, an unfair one,
          or no award at all," notes John Bisnar. "Although you want to be sure
          to have your injuries thoroughly diagnosed and treated, you don't want
          the statute of limitations -- two years in California -- to run out.
          If this happens, you'll forfeit your rights, and your insurance
          adjuster will tell you that his company won't have to pay you for your
          losses and injuries."
        </p>
        <p>
          If you wait too long before filing your lawsuit, you may be vulnerable
          to another insurance company tactic: The insurance adjustor becomes
          difficult to work with and hard to contact -- especially during the
          last 90 days before the statute of limitations expires. The adjuster
          is counting on your impatience to settle as the days count down, and
          that you'll be more amenable to accept a much lower settlement. They
          also hope that you'll just give up and lose the personal fortitude to
          even file a lawsuit.
        </p>
        <p>
          "This 'grind 'em down' tactic is most often used when insurance
          companies deal with clients who are unrepresented by a car accident
          lawyer or with law firms that seldom go to trial," says John Bisnar.
          "Adjusters know which law firms settle out of court and they quickly
          become hard nosed and hard to reach."
        </p>
        <h2>Consult a Car Accident Lawyer on When and How to File</h2>
        <p>
          An experienced Compton car crash lawyer knows the smartest way to
          apply the statutes and notice requirements to your benefit. Going it
          alone can be tough. You need legal knowledge, an awareness of
          insurance company tactics and patience. The best lawyers offer no
          charge, no pressure consultations. It's wise to consult a car crash
          lawyer, for they can help you avoid costly mistakes and make sure you
          are fairly compensated for your various losses.
        </p>
        <p>
          For more advice, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
