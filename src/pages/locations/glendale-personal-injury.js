// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Glendale",
      population: 196021,
      totalAccidents: 6122,
      intersection1: "N Brand Blvd & Goode Ave ",
      intersection1Accidents: 143,
      intersection1Injuries: 93,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 108.6,
      city1Name: "La Canada Flintridge",
      city1Index: 95.9,
      city2Name: "Burbank",
      city2Index: 148.1,
      city3Name: "South Pasadena",
      city3Index: 159.9,
      city4Name: "Pasadena",
      city4Index: 223.9
    },
    locationBox3: {
      intersection2: "Central Ave & Goode Ave ",
      intersection2Accidents: 85,
      intersection2Injuries: 51,
      intersection2Deaths: 0,
      intersection3: "Broadway & Glendale Ave",
      intersection3Accidents: 95,
      intersection3Injuries: 46,
      intersection3Deaths: 0,
      intersection4: "Colorado St & Pacific Ave ",
      intersection4Accidents: 106,
      intersection4Injuries: 38,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Glendale Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Have a personal injury case? Read some of our Glendale resources about getting compensation for your injuries!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Glendale Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Glendale has remained relatively static in terms of population and
          housing prices over the past 15 years, and though the yearly car
          accidents have decreased from 3,021 in 2009 to 2,546 in 2014, the
          yearly deaths from car crash incidents have actually increased
          significantly. Bisnar Chase represents victims of car accidents who
          are fighting insurance companies to be compensated fairly. We'll take
          you through the process so you don't have to worry about a thing.
          Contact us to see if you have a case today!
        </p>

        <p>
          Glendale is famous for having helped father the emerging age of
          aviation, with its now defunct Grand Central Airport. Forest Lawn
          Memorial Park Cemetery, located in Glendale, contains the remains of
          many celebrities and local residents. It is famous as the pioneer of a
          new style of cemetery.
        </p>
        <p>
          The <strong>Glendale personal injury attorneys</strong> at Bisnar
          Chase Personal Injury Attorneys offer these Glendale resources for
          your convenience. If you are in need of an Glendale personal injury
          lawyer, please call us at 949-203-3814.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="https://www.glendaleca.gov/" target="_blank">
              City of Glendale, CA
            </Link>
          </li>

          <li>
            {" "}
            <Link
              to="https://www.glendaleca.gov/government/departments/police-department"
              target="_blank"
            >
              Glendale Police Department
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://gusd.net/" target="_blank">
              Glendale Unified School District
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.dmv.ca.gov/portal/dmv/detail/fo/offices/fieldoffice?number=510"
              target="_blank"
            >
              Glendale DMV Office
            </Link>
          </li>
        </ul>
        <p>
          Contact our Glendale Personal Injury Attorneys at our Los Angeles
          office:
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>
            <span className="postal-code">90045</span>
            <br />
            <span className="tel">323-238-4683</span>{" "}
          </div>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
