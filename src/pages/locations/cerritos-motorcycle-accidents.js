// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cerritos Motorcycle Accident Attorneys - 323-238-4683"
        pageDescription="Call 323-238-4683 for Cerritos motorcycle accident attorneys who can help. Free consultations, millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cerritos Motorcycle Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <p>
          The city of Cerritos is located in the southwestern corner of Los
          Angeles County and has a population of around 49,000 people.  One of
          the &ldquo;gateway cities&rdquo; into Los Angeles proper, Cerritos,
          which means &ldquo;little hills,&rdquo; was once known as Dairy Valley
          due to the high preponderance of dairy farms in the area.  Today,
          Cerritos is a pleasant residential area and considered a suburb of Los
          Angeles.
        </p>
        <p>
          Cerritos{" "}
          <Link to="/los-angeles/motorcycle-accidents">
            motorcycle accident lawyers
          </Link>{" "}
          stand behind the victims of motorcycle collisions and help them to
          obtain fair treatment and a reasonable settlement for their costs. 
          The costs of a motorcycle wreck can be very high, and may include
          medical treatment, pain and suffering, and an increase in living
          expenses.  In some cases, motorcycle collision victims may find that
          their living expenses increase significantly while their income
          dwindles due to their inability to work.  A further problem these
          victims face is the fact that some of these injuries may be permanent
          and life-changing.
        </p>
        <h2>Get Fair Compensation</h2>
        <p>
          With the help of motorcycle accident attorneys in Los Angeles,
          however, the future does not have to be bleak.  While no attorney can
          undo the damage done by a motor vehicle accident, a professional
          lawyer who deals with recovering money from at-fault parties when
          damages do occur is able to help victims receive the treatment they
          need to obtain as much of a return to normalcy as possible.
        </p>
        <p>
          Some injuries cannot be cured, and payments for permanent disabilities
          are also part of the damages a motorcycle accident lawyer will assess
          in evaluating your case.  It is impossible to put a price tag on the
          loss of use of a limb, for example, but you can collect monetary
          damages to compensate you for this loss under California law.
        </p>
        <h2>Don't be Bullied by the Other Party</h2>
        <p>
          It costs nothing to consult a motorcycle attorney about your case. 
          These professional lawyers offer a no-cost consultation to any victim
          who believes he or she might have a case.  Even if you do not think
          you can collect anything on your motorcycle collision, you should
          still make an appointment and talk to a lawyer before you agree to any
          type of settlement or accept the fact that you will not receive
          compensation.  Many at-fault parties and insurance companies are
          dedicated to the concept of denying a victim any compensation at all
          unless and until the victim pursues the matter, and then offering the
          victim a small amount hoping the case will go away.  With the help of
          an attorney, you do not have to settle for this type of treatment. 
          You can, instead, assert your rights and collect a fair settlement
          from the person responsible for your accident.
        </p>
        <h2>Seek Immediate Legal Help</h2>
        <p>
          Immediately after your motorcycle accident its a good idea to{" "}
          <Link to="/los-angeles/contact">reach out to a local attorney</Link>.
          One who is familiar with the city, courts and defense attorneys. You
          potential compensation may rely on it. Let an experienced motorcycle
          injury lawyer be your eyes and ears. Their years of experience in and
          out of the courts will put you at an advantage against the greedy
          insurance companies. Call for a free case evaluation. Toll free
          800-561-4887 or local at 323-238-4683
        </p>
        <p>&nbsp;</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
