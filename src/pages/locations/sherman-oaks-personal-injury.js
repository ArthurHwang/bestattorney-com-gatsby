// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sherman Oaks Personal Injury Attorneys - Los Angeles County, California"
        pageDescription="Call 323-238-4683 for highest-rated personal injury attorneys, serving Sherman Oaks, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sherman Oaks Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you've been injured in a car accident, work injury or auto defect
          accident, call the Sherman Oaks personal injury attorneys at Bisnar
          Chase. We provide a free consultation and advance all costs in your
          case. If in the end, we don't win your case, you don't pay anything.
          Our client care is a big part of what makes us so different. When John
          Bisnar created this law firm 35 years ago he was determined to provide
          the best client care possible. Contact our receptionist, paralegal or
          attorney and you'll see firsthand how we treat every client.
        </p>
        <p>
          In addition to having a client care second to none, we have pretty
          amazing attorneys. The firm has received top honors from organizations
          such as Avvo, SuperLawyers and NADC for Top 1% Lawyers nationwide. You
          may have questions about a serious car accident or even a dog bite
          case. Let us help you determine if you have a case and are entitled to
          compensation. We are committed to the successful closure of your case.{" "}
        </p>
        <p>
          Sherman Oaks, close to two major freeways, serves as a gateway to the
          San Fernando Valley. The community is perhaps best known as the home
          of the Sherman Oaks Galleria, considered by many to be the undisputed
          residence of the Valley Girl, a cultural label popularized by the song
          and 1983 movie of the same name. Two large shopping malls in the area,
          numerous smaller boutiques and plazas line Ventura Boulevard,
          providing residents and visitors with endless window-shopping
          opportunities.
        </p>
        <p>
          The <strong>Sherman Oaks personal injury attorneys</strong> at Bisnar
          Chase offer these Sherman Oaks resources for your convenience. If you
          are in need of a{" "}
          <Link to="/los-angeles">Sherman Oaks personal injury lawyer</Link>,
          please call us at 800-561-4887 or 323-238-4683.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="https://www.shermanoakschamber.org/" target="_blank">
              Sherman Oaks Chamber of Commerce
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.lapdonline.org" target="_blank">
              Sherman Oaks Police Department
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://lafd.org/" target="_blank">
              Sherman Oaks Fire Department
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.lausd.net" target="_blank">
              Sherman Oaks School District
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.lapl.org/branches/Branch.php?bID=51"
              target="_blank"
            >
              Sherman Oaks Branch Library
            </Link>
          </li>
        </ul>
        <p>
          <strong>
            Contact our Sherman Oaks Personal Injury Attorneys at our Los
            Angeles office:
          </strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">90045</span>
            <br />
            <span className="tel">323-238-4683</span>{" "}
          </div>
        </div>

        <p>
          For Los Angeles County Car Accident Attorneys call 323-238-4683 for a
          free consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
