// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Brea",
      population: 40963,
      totalAccidents: 2046,
      intersection1: "Imperial Highway & State College Blvd",
      intersection1Accidents: 212,
      intersection1Injuries: 102,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 185.6,
      city1Name: "Placentia",
      city1Index: 126.3,
      city2Name: "La Habra",
      city2Index: 141.6,
      city3Name: "Fullerton",
      city3Index: 214.7,
      city4Name: "La Habra Heights",
      city4Index: 85.5
    },
    locationBox3: {
      intersection2: "Lambert Rd & State College Blvd",
      intersection2Accidents: 133,
      intersection2Injuries: 40,
      intersection2Deaths: 0,
      intersection3: "Associated Rd & Imperial Highway",
      intersection3Accidents: 84,
      intersection3Injuries: 39,
      intersection3Deaths: 0,
      intersection4: "Imperial Highway & Berry St",
      intersection4Accidents: 71,
      intersection4Injuries: 39,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brea Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 949-203-3814 for experienced Brea car accident attorneys whove won over $500 Million for their clients and have served Orange County since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Brea Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div>
          <img
            class="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase"
          />
        </div>
        <p>
          Bisnar Chase Personal Injury Attorneys represent clients who have been
          injured in car accidents in Brea and around Orange County, CA. We have
          the experience and the determination to make sure your insurance
          companies compensate you fairly for your medical bills, property
          damage, lost wages, and general inconvenience. If you've been in a car
          accident and you're not happy with the settlement that they've offered
          you, or if you just want someone to walk you through the experience to
          make sure you're getting the best recovery you can get from the
          insurance companies, contact Bisnar Chase Today! Our top rated{" "}
          <Link to="/car-accidents"> car accident lawyers </Link> may be able to
          provide you with valuable information that supports your case.
        </p>
        <div className="snippet">
          <p>
            <span>
              <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
              Partner:
            </span>{" "}
            “I went to law school knowing I wanted to be a personal injury
            attorney. I wanted my life’s work to have a positive impact on other
            people’s lives.”
          </p>
        </div>

        <p>
          Brea car accident lawyers agree that car collisions are pretty much
          unavoidable in a city of over 40,000. A busy retail center with a
          large shopping mall and the recently revitalized downtown area, Brea
          in Orange County, California, set out to reduce car accidents within
          its borders.
        </p>
        <h2>Brea Car Accident Statistics</h2>
        <p>
          {" "}
          Brea is a popular destination for Orange County transplants and
          especially college students. The city carries a big burden however,
          with the number of young people who travel through the city to
          Fullerton and back from local bars and pubs. In the last five years
          there has been 4 dangerous intersections that see the most accidents.
          Imperial Highway and State College had the most car accidents at 212.
          Lambert Road and State College saw 133 car accidents and Associated
          Road and Imperial saw 84. Of those, there were about 202 injuries.
          There were an additional 71 car accidents at Imperial Highway and
          Berry St. 39 of those resulted in injuries.{" "}
        </p>
        <p>
          Another troubling statistic is the fact that Brea has the highest rate
          of truck accidents of any city in Orange County, at more than 9 truck
          accidents per every 1,000 people in 2009-2014, as noted from our{" "}
          <Link to="/resources/car-accident-statistics">
            Orange County Car Accident Statistics Report
          </Link>
          . Even more surprising is that the truck traffic through Brea (as
          reported by City-Data.com) is half of the next city on the list, Seal
          Beach (95,960 vs 184,497).
        </p>
        <h2>
          Kudos to Traffic Division for Keeping Brea Car Collisions in Check
        </h2>
        <p>
          Serving both Brea and the city of Yorba Linda, the Brea Police
          Department's Traffic Division is charged with maintaining the safe
          traffic flow for residents and commuters. Brea and Yorba Linda are
          accessed by two major freeways, the Orange (57) and the Riverside (91)
          and a major highway, Imperial (90), which contribute heavy daily
          commuter traffic. Situated in the foothills of north Orange County,
          these cities comprise a daily population of about 160,000. So
          controlling traffic is no small task.
        </p>
        <h2>Sobriety Checkpoints Help Reduce Brea Car Crashes</h2>
        <p>
          To reduce Brea's car crash rate, the city regularly sets up Sobriety
          and Driver's License Checkpoints at key accident-prone intersections
          and streets. These include checkpoints stretched along State College
          Blvd., Birch Street near the Brea Mall, Lambert Road near the 57
          freeway, and Imperial Highway (90) between Kraemer Blvd. and Harbor
          Blvd.
        </p>
        <p>
          Sobriety checkpoints help take drunk drivers off city streets. In
          addition to issuing citations and making DUI arrests, sobriety
          checkpoints help educate the public on the dangers of driving while
          intoxicated. They also encourage the use of sober designated drivers.
          Virtually all Brea car collision lawyers will tell you that sobriety
          checkpoints can add to the safety of city streets and highways. The
          goal is to remove reckless and irresponsible drivers off Brea streets,
          to save lives and prevent needless injuries.
        </p>

        <h2>Contact Our Top Rated Injury Attorneys</h2>
        <p>
          Immediately call an experienced and reputable Brea Car Accident Lawyer
          for a free consultation at 949-203-3814 if you have been injured. We
          exclusively represent plaintiffs and can help determine if you have a
          case. You may be entitled to a substantial recovery.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
