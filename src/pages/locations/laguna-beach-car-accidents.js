// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Laguna Beach",
      population: 23250,
      totalAccidents: 1068,
      intersection1: "Laguna Canyon Rd & Canyon Acres Dr",
      intersection1Accidents: 82,
      intersection1Injuries: 29,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 144.6,
      city1Name: "Laguna Niguel",
      city1Index: 73.2,
      city2Name: "Aliso Viejo",
      city2Index: 47.5,
      city3Name: "Dana Point",
      city3Index: 145.4,
      city4Name: "Laguna Woods",
      city4Index: 54.2
    },
    locationBox3: {
      intersection2: "Laguna Canyon Rd & El Toro Rd",
      intersection2Accidents: 46,
      intersection2Injuries: 28,
      intersection2Deaths: 0,
      intersection3: "Route 1 & Cleo St",
      intersection3Accidents: 44,
      intersection3Injuries: 17,
      intersection3Deaths: 1,
      intersection4: "Route 1 & Vista Del Sol",
      intersection4Accidents: 30,
      intersection4Injuries: 17,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Beach Car Accident Lawyer - Auto Accident Attorneys"
        pageDescription="Call 949-203-3814 to speak with a Laguna Beach car accident lawyer. Receive a free consultation from experienced auto accident attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Beach Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Laguna Beach is a big tourist destination in Orange County,
          </strong>{" "}
          and this leads to much more car and foot traffic than there normally
          would be for a town with a population of 24,000. Bisnar Chase knows
          that Laguna Beach has its' own special traffic problems, and that this
          information can be important when it comes time to seek a fair
          settlement in a{" "}
          <Link to="/car-accidents">car accident injury case</Link>. We have
          experience in representing car accident victims in Orange County and
          all over California. We'll fight for you! Contact us to see if you
          have a case!
        </p>
        <h2>An Overview of Laguna Beach Accident Statistics</h2>
        <p>
          In our report on{" "}
          <Link to="/resources/car-accident-statistics">
            Orange County car accident statistics
          </Link>
          , we found that Laguna Beach has the highest rate of pedestrian
          accidents out of any city in Orange County, as well as the
          second-highest rate of motorcycle accidents, third highest rate of
          truck accidents, and second highest rate of DUI accidents. This makes
          Laguna Beach a fairly dangerous place to be in, considering it has
          such a high rate of accidents in such a small area.
        </p>
        <h2>The Unique Traffic Problems of Laguna Beach</h2>
        <p>
          Experienced Laguna Beach accident injury lawyers have long known that
          Laguna Canyon Road is one of the deadliest in the U.S. Daily commuters
          who have become very familiar with this road will often speed and
          tailgate newcomers. Unsafe passing into oncoming traffic has caused
          many tragic car crashes.
        </p>
        <p>
          Another area deemed hazardous, especially for pedestrians, bicyclists
          and skateboarders, is Park Avenue at Third Street. Crossing this
          intersection can be very risky. One other dangerous are is the stretch
          of Pacific Coast Highway that cuts through Laguna Beach. Tourists who
          are not familiar with PCH's hectic, speeding traffic, often try to
          jaywalk or ignore traffic lights and end up getting struck by passing
          motorists.
        </p>
        <p>
          PCH can also be especially hazardous for bicyclists. Vehicles moving
          at or over the speed limit typically leave little room between parked
          cars for bicyclists. As one cyclist complained, "If someone opens a
          parked car door at the wrong time, it's all over."
        </p>
        <h2>
          Seek the Advice of an Experienced Laguna Beach Car Accident Lawyer
        </h2>
        <p>
          If you should suffer a car accident injury in Laguna Beach, consult an
          expert who knows the city. A knowledgeable car crash lawyer who
          understands the dangers and accident statistics of Laguna Beach can
          tell you if your case has merit and what to do next. The best Laguna
          Beach car collision lawyers offer free, no obligation consultations to
          accident victims. The most successful work on a contingency basis
          (which means you only pay their fees if they win your case or settle
          successfully).
        </p>
        <p>
          <strong>Orange County Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">1301 Dove St. #120</div>
            <span className="locality">Newport Beach</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">92660</span>
            <br />
            <span className="tel">949-203-3814</span>{" "}
          </div>
        </div>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
