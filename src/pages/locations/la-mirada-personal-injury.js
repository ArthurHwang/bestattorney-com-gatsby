// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "La Mirada",
      population: 49133,
      totalAccidents: 1315,
      intersection1: "Imperial Hwy & La Mirada Blvd ",
      intersection1Accidents: 109,
      intersection1Injuries: 40,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 143.6,
      city1Name: "Buena Park",
      city1Index: 220.5,
      city2Name: "La Habra",
      city2Index: 141.6,
      city3Name: "Cerritos",
      city3Index: 232.5,
      city4Name: "Santa Fe Springs",
      city4Index: 295.0
    },
    locationBox3: {
      intersection2: "Santa Gertrudes Ave & Imperial Hwy",
      intersection2Accidents: 101,
      intersection2Injuries: 36,
      intersection2Deaths: 0,
      intersection3: "Beach Blvd & Rosecrans Ave",
      intersection3Accidents: 75,
      intersection3Injuries: 41,
      intersection3Deaths: 0,
      intersection4: "Firestone Blvd & Valley View Ave",
      intersection4Accidents: 96,
      intersection4Injuries: 34,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Mirada Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Injured in an accident, dog bite, or premises case? Bisnar Chase will walk with you to make sure you get the compensation you deserve! Get a free case evaluation now!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Mirada Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>La Mirada is a relatively small city of 50,000 people</strong>{" "}
          with a high population density, meaning that despite about 10% of the
          population commuting out of the city for work, there are still a lot
          of cars on the road and increased chances of car accidents.
          Experiencing any sort of catastrophic injury, whether from a car
          accident or dog bite, or even from a defective product, can be life
          changing. Bisnar Chase Personal Injury Attorneys are advocates for
          injured victims - we fight for compensation when our clients are
          injured by negligent parties. See if you can get compensated for your
          medical bills and general damages by contacting us for a free case
          evaluation!
        </p>
        <h2>La Mirada - A Relatively Safe City</h2>
        <p>
          The City of La Mirada was named a "Best Place to Live" by CNN Money
          Magazine in 2008. La Mirada placed 34th on the list, with the La
          Mirada Theatre for the Performing Arts and the Splash! La Mirada
          Regional Aquatics Center as two of its main attributes. La Mirada can
          also be considered a good place to live due to its’ low rate of car
          accidents and DUIs compared to the rest of California. Added to that,
          the close proximity to amusement parks like Knott’s Berry Farm and
          Disneyland and family-friendly shows in Buena Park like Medieval
          Times, make La Mirada a great place to raise a family.
        </p>
        <p>
          Personal Injury, however, can strike any La Mirada family without
          warning – from car accidents to dog bites and injuries due to
          negligent property owners. In an instant, your situation can change
          from being at peace to being stressed and afraid – dealing with your
          families or your own injuries and medical bills. Our goal at Bisnar
          Chase Personal Injury Attorneys is to provide relief from that stress
          and fear. Injury Attorneys in general are usually able to get a much
          higher settlement from insurance companies and guilty parties because
          they know the ins and the outs of the system, and can spot any tricks
          that companies use to avoid paying out insurance money. If you have
          decided that you need an attorney, we recommend reading our guide to
          learn{" "}
          <Link to="/resources/hiring-an-injury-lawyer">
            how to hire a good lawyer.
          </Link>
        </p>

        <h2>Other Injury Resources for La Mirada Residents</h2>
        <p>
          The idea of taking a case to Los Angeles County court can be
          intimidating, especially for those who aren’t acquainted with the
          legal system. Bisnar Chase has put together several resources to
          assist injury victims and to help them understand the process will go
          through as a personal injury attorney client. Our{" "}
          <Link to="/faqs">frequently asked questions</Link> contains the most
          sought after information, and we also suggest reading about{" "}
          <Link to="/resources/taking-your-case-to-small-claims-court">
            taking your case to small claims court
          </Link>{" "}
          if you are confident enough in representing yourself and not retaining
          a lawyer.{" "}
        </p>

        <h2>Bisnar Chase Sides with the Injured</h2>
        <p>
          As you prepare to negotiate a settlement with your insurance company
          for your personal injury claim, remember that these insurance
          adjusters have the job of paying out as little as possible in
          insurance money. Because of this, there is inherently a bias on the
          adjuster’s part to not want to pay out your claim. Bisnar Chase on the
          other hand, is on your side every step of the way. When you sign your
          case with us, we handle the communication with insurance and medical
          billing companies so that you don’t have to. We are paid on a
          contingency basis, so you if we don’t win your case, you don’t pay!
          You can read more about our{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            no-win, no-fee guarantee here.
          </Link>{" "}
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
