// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Sacramento",
      population: 495200,
      totalAccidents: 23728,
      intersection1: "Bruceville Rd & Cosumnes River Blvd",
      intersection1Accidents: 72,
      intersection1Injuries: 119,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 356.5,
      city1Name: "Fontana",
      city1Index: 223.5,
      city2Name: "Colton",
      city2Index: 302.7,
      city3Name: "San Bernardino",
      city3Index: 555.0,
      city4Name: "Grand Terrace",
      city4Index: 224.8
    },
    locationBox3: {
      intersection2: "Valley Hi Dr & Mack Dr",
      intersection2Accidents: 76,
      intersection2Injuries: 117,
      intersection2Deaths: 0,
      intersection3: "24th St & Florin Rd",
      intersection3Accidents: 75,
      intersection3Injuries: 91,
      intersection3Deaths: 1,
      intersection4: "Howe Ave & Fair Oaks Blvd",
      intersection4Accidents: 52,
      intersection4Injuries: 55,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sacramento Personal Injury Lawyers of Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Sacramento personal injury attorneys. We Specialize in catastrophic injuries and car accidents.  No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sacramento Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Contact the Sacramento personal injury attorneys of Bisnar Chase for a
          free private consultation. Our attorneys are highly skilled and have
          collected hundreds of millions for our clients. If you've been hurt in
          a car accident, work injury or even serious dog bite, we may be able
          to get you fair compensation. Before you talk to the insurance
          companies make sure you consult a qualified Sacramento personal injury
          lawyer.
        </p>
        <p>
          As California's capital and the seat of local government, Sacramento
          would seem like an organized and well-run place to live and work.
          However, many Sacramento personal injury lawyers will tell you that if
          you drive in Sacramento, you'd best buckle up and stay on guard.
        </p>
        <p>
          Recent studies show that Sacramento is the most crash-prone city in
          the state, with its figures even topping Los Angeles and San
          Francisco, which have significantly larger populations.
        </p>
        <p>
          Data showed that more than 4,000 people suffered serious personal
          injuries in Sacramento car accidents in 2007 while 49 others were
          killed.
        </p>
        <h2>Large Roads, Small Population Encourage Reckless Driving</h2>
        <p>
          Some{" "}
          <Link to="/locations/sacramento-car-accidents">
            Sacramento car accident attorneys
          </Link>{" "}
          say the city's relatively small population might actually be factoring
          into its susceptibility to car, bike, motorcycle and pedestrian
          accidents. For example, research has shown that drivers in highly
          congested cities tend to drive more carefully, knowing that they must
          always be aware of their surroundings. Drivers in cities with smaller
          populations, such as Sacramento for example (which has a little less
          than 500,000 people) may be too relaxed for comfort. For example, one
          person reported once seeing a driver brushing his teeth while behind
          the wheel.
        </p>
        <p>
          Safety officials also say that Sacramento's street designs, traffic
          patterns and traffic enforcement may also play a significant role in
          car accident rates.
        </p>
        <p>
          One expert noted that many of the streets where most of Sacramento's
          car accidents have taken place have a lot in common. For one, most are
          oversized with four to six lanes which tempt drivers into pushing the
          pedal to the metal, especially as there are long stretches between
          signal lights. A Sacramento anti-drunken-driving organization has also
          blamed the city's wide roads with giving intoxicated drivers more room
          to make deadly mistakes.
        </p>
        <p>
          The street system meantime in Sacramento is also not as tight as in
          other cities. Instead of street grids like those found in San
          Francisco, Sacramento planners favored a system that sees high-volume
          corridors leading drivers onto large intersections, which experts say
          are the most dangerous sites in urban areas.
        </p>
        <p>
          In a bid to lessen car accidents, city engineers said they are
          building streets with fewer lanes and more pedestrian space and
          bicycle lanes. Other safety measures include tweaking some signal
          lights so that all vehicles in all four directions of an intersection
          have a red light at the same time for a few seconds before one
          direction gets the go signal.
        </p>
        <h2>Waterway Accidents Concern Sacramento Personal Injury Attorneys</h2>
        <p>
          Aside from dangerous roads, many accident injuries can also occur on
          Sacramento's open waterways. While the waterways are a popular
          attraction especially during the holiday season Sacramento personal
          injury law firms say they deal with a number of boating injuries each
          year.
        </p>
        <p>
          Studies show that 14 people including three children under the age of
          13 drowned in Sacramento waterways in 2008.
        </p>
        <p>
          In July 2009, a father and son boating on the Sacramento River as part
          of a Boy Scout Group nearly died when their canoe overturned after
          becoming trapped against a sunken tree. Another boater meantime was
          also thrown into the water after he tried to rescue them. Thankfully,
          a patrol boat was able to rescue the three of them.
        </p>
        <p>
          While patrol boats do their best to ensure the safety of people on
          Sacramento's waterways, they warn that budget cuts are severely
          hampering their resources. For example, in August 2009, an abandoned
          boat sunk and floated downstream in Snodgrass Slough. Authorities took
          two days to respond due to manpower problems.
        </p>
        <p>
          Despite the lack of manpower, Sacramento officials said they are
          making progress in waterway safety. They said that so far in 2009, no
          children under the age of 13 have drowned. They attribute this to
          heightened awareness and use of lifejackets as a result of an
          education campaign by the city.
        </p>
        <h2>Contact a Sacramento Personal Injury Lawyer</h2>
        <p>
          If you have been involved in an accident of any kind, please consult a
          Sacramento personal injury lawyer.
        </p>
        <p>
          Your accident attorney will explain the legal issues of your personal
          injury case and fight for any damages to which you may be entitled.
          They will also ensure that any guilty parties involved in the accident
          will be held accountable. While there are many personal injury and
          accident lawyers claiming to have your best interests at heart, it is
          helpful to keep in mind that the best law firms will always offer a
          free consultation on your case.
        </p>
        <p>
          Law firms with a{" "}
          <Link to="/case-results">
            {" "}
            strong record of successfully handling personal injury lawsuits
          </Link>{" "}
          may also offer a no-fee guarantee meaning that you don't pay anything
          unless they win your case.
        </p>
        <p>
          Please call our Sacramento personal injury lawyers at 949-203-3814.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
