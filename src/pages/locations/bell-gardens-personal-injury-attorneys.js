// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Bell Gardens",
      population: 42889,
      totalAccidents: 755,
      intersection1: "Eastern Ave & Lubec St",
      intersection1Accidents: 42,
      intersection1Injuries: 32,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 194.5,
      city1Name: "Cudahy",
      city1Index: 205.8,
      city2Name: "Bell",
      city2Index: 246.9,
      city3Name: "Maywood",
      city3Index: 212.7,
      city4Name: "Commerce",
      city4Index: 264.1
    },
    locationBox3: {
      intersection2: "Florence Ave & Eastern Ave",
      intersection2Accidents: 77,
      intersection2Injuries: 24,
      intersection2Deaths: 0,
      intersection3: "Eastern Ave & Clara St",
      intersection3Accidents: 39,
      intersection3Injuries: 20,
      intersection3Deaths: 0,
      intersection4: "Garfield Ave & Loveland St",
      intersection4Accidents: 49,
      intersection4Injuries: 16,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bell Gardens Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Call 323-238-4683 for personal injury attorneys in Bell Gardens, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects. Free no-obligation legal consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bell Gardens Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          A traumatic event such as a car accident that leaves you or a loved
          one with a serious personal injury can give rise to a number of
          questions. You may wonder who will help pay for your mounting medical
          expenses and income lost as you recover from your injuries. You may
          also be looking at continuing out-of-pocket expenses for
          rehabilitative treatment and care. It is not an exaggeration to state
          that families in Bell Gardens face significant financial burdens as
          they care for injured family members.
        </p>

        <p>
          Reviewing your case with experienced Bell Gardens{" "}
          <Link to="/"> personal injury attorneys </Link> can help prepare you
          for what can be a complex claims process. At Bisnar Chase, we
          understand how frustrating it can be for injured victims and their
          families to deal with insurance adjusters at a time when they are
          still vulnerable and coping with a myriad of other challenges. Bisnar
          Chase Personal Injury Attorneys will help you seek and obtain the
          resources and support you need during this difficult time in your
          life. Fill out our form above or call us at{" "}
          <strong>(323) 238-4683</strong>
        </p>

        <h2>Helping You Avoid the Pitfalls</h2>

        <p>
          Our attorneys can help you avoid the costly mistakes that could
          jeopardize your personal injury claim. One of the biggest mistakes
          injured victims make is to rush into a settlement with the insurance
          company without fully analyzing their options. This could prove
          extremely costly because once you sign a settlement agreement your
          case will be closed. Victims cannot seek additional compensation in
          the future even if they incur expenses relating to the incident or the
          injury in question. This is why it would be in your best interest to
          discuss your case with your injury lawyer who will fight hard to
          protect your rights and look out for your best interests.
        </p>

        <h2>Dealing with Insurance Companies</h2>

        <p>
          Another common mistake personal injury plaintiffs make is assume that
          the insurance company is their friend. This is not true. The insurance
          company is in business to make money and maximize profits and the
          insurance adjuster is an individual who is employed by the insurance
          company to ensure that the payouts are minimal. An insurance adjuster
          may seem cordial and cooperative.
        </p>

        <p>
          But, his or her job is to coax you into accepting a quick and low
          offer, which is obviously in the insurance company's best interest.
          This is why you need a Bell Gardens personal injury attorneys on your
          side, advocating for your rights and making sure that you receive
          maximum compensation for all your injuries, damages and losses. Call
          us at (800) 561-4887 for a free, comprehensive and confidential
          consultation.
        </p>
        <h2> Get Immediate Legal Help</h2>
        <p>
          {" "}
          The injury attorneys of Bisnar Chase have a long and{" "}
          <Link to="/case-results"> impressive record of wins.</Link> We have
          represented thousands of people throughout Los Angeles and have won
          hundreds of millions for our clients. A good personal injury attorney
          can mean the difference between a good settlement and a great
          settlement. Call today to discuss your case free. 323-238-4683.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
