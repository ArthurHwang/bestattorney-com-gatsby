// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Dana Point",
      population: 34062,
      totalAccidents: 965,
      intersection1: "Pacific Coast Hwy & Doheny Park Rd",
      intersection1Accidents: 80,
      intersection1Injuries: 49,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 145.4,
      city1Name: "San Juan Capistrano",
      city1Index: 107.5,
      city2Name: "Laguna Niguel",
      city2Index: 73.2,
      city3Name: "San Clemente",
      city3Index: 92.6,
      city4Name: "Laguna Beach",
      city4Index: 144.6
    },
    locationBox3: {
      intersection2: "Stonehill Dr & Del Obispo St ",
      intersection2Accidents: 68,
      intersection2Injuries: 34,
      intersection2Deaths: 0,
      intersection3: "Pacific Coast Hwy & Crown Valley Pkwy",
      intersection3Accidents: 66,
      intersection3Injuries: 32,
      intersection3Deaths: 0,
      intersection4: "Pacific Coast Hwy & Del Obispo St",
      intersection4Accidents: 58,
      intersection4Injuries: 31,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dana Point Car Accident Lawyer - Bisnar Chase"
        pageDescription="Have you been injured in a car accident in Dana Point, CA? If so, you'll need the help of an experienced Car Accident Lawyer. Call 949-203-3814 today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Dana Point Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>If you've been injured in a car accident,</strong> you and
          your spouse may be entitled to recover a host of damages. You can be
          compensated for your medical bills, loss of wages and time, property
          damage, and general inconvenience - especially if you have experienced
          a catastrophic injury. Call Bisnar Chase to learn more and see if you
          have a case!
        </p>
        <h2>Car Accidents in Dana Point Take Their Annual Toll</h2>
        <p>
          Car accident statistics reported by the California Highway Patrol's
          Statewide Integrated Traffic Records System (SWITRS) revealed that in
          2006, two people were killed and 94 were injured in Dana Point car
          collisions. DUIs were responsible for one death and 22 crash injuries.
          Pedestrian accidents injured four and motorcycle accidents injured
          seven. Bicycle accidents killed two and injured six. In 2007, two car
          accidents resulted in two deaths.
        </p>
        <h2>Car Accident Victims May Be Entitled to Recovery in Many Areas</h2>
        <p>
          Expert Dana Point car accident lawyers know car accident victims often
          face a gauntlet of medical bills that seem to go on and on. "If you
          are seriously injured, you may seek recovery for physical and mental
          pain and suffering, medical and rehabilitative expenses, permanent
          impairment and permanent disfigurement, and past and future lost
          income," notes John Bisnar. "You may also be compensated for the loss
          of enjoyment of life. You may even be compensated for re-injuring or
          aggravating an injury you sustained months or years before your car
          accident." Car accident lawyers advise, however, that to be fairly
          compensated for your injuries, it helps to keep a daily diary of how
          the accident has impacted you, your life and your family.
        </p>
        <h2>Family Members May Also Recover For Damages</h2>
        <p>
          Astute Dana Point car collision lawyers know that you or your spouse
          may be entitled to recover for damage or loss to the marital
          relationship. This claim, called "loss of consortium," can be either
          temporary or permanent. Damages in this category are characterized by
          any negative effect the accident has had on the marital relationship.
          This can include loss of the spouse's love, companionship, comfort,
          affection, solace, moral support, sexual relations, ability to have
          children, and physical assistance in the maintenance and operation of
          one's home.
        </p>
        <h2>Unclear About Damages? Seek the Advice of a Car Accident Lawyer</h2>
        <p>
          A trustworthy Dana Point car crash lawyer can help you itemize the
          damages caused by your car accident. He or she can help you assign a
          dollar amount to such damages as pain and suffering, and offer
          guidance in seeking just compensation for your injuries. Some of the
          finest car accident lawyers offer no charge, no pressure
          consultations. And fees are typically paid after your case settles.
        </p>
        <p>
          For additional advice, please read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar. If you are injured
          and in need of legal assistance, please{" "}
          <strong>call toll free at 949-203-3814</strong> to receive your free
          consultation today.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
