// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "West Covina",
      population: 107740,
      totalAccidents: 4102,
      intersection1: "Amar Rd & Azusa Ave",
      intersection1Accidents: 104,
      intersection1Injuries: 71,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 231.5,
      city1Name: "La Puente",
      city1Index: 199.2,
      city2Name: "Covina",
      city2Index: 235.2,
      city3Name: "Industry",
      city3Index: 598.0,
      city4Name: "Encinitas",
      city4Index: 150.3
    },
    locationBox3: {
      intersection2: "Lark Ellen & Puente Ave",
      intersection2Accidents: 43,
      intersection2Injuries: 50,
      intersection2Deaths: 0,
      intersection3: "Rowland Ave & Azusa Ave",
      intersection3Accidents: 50,
      intersection3Injuries: 38,
      intersection3Deaths: 1,
      intersection4: "Glendora Ave & Cameron Ave",
      intersection4Accidents: 53,
      intersection4Injuries: 37,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="West Covina Car Accident Lawyer - Auto Accident Attorney"
        pageDescription="Contact our West Covina car accident lawyers for a free consultation. 96% success rate and we advance all costs. Call 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>West Covina Auto Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          Unforeseen events have perhaps brought you here. We've been
          representing West Covina car accident victims since 1978. Our legal
          team is passionate about your case, and your success is our success.
          If you've been injured in a car accident by a negligent party call
          today for a free consultation.
        </p>
        <p>
          <strong>Trust. Passion. Results.</strong>
        </p>
        <p>
          We can make all the difference in your car accident case. You'll be
          treated fairly and we will work hard to get the best compensation
          possible. Stop worrying about your legal case and let us take over.{" "}
        </p>
        <ul>
          <li> No fee guarantee</li>
          <li> Maximum compensation</li>
          <li> Earn your trust</li>
          <li> Talk to a lawyer for free</li>
        </ul>
        <p>
          An experienced West Covina car accident lawyer can help you stay clear
          of all sorts of problems when you try to negotiate compensation
          amounts after a car accident. Granted, you drive safe and probably
          will avoid getting into a car accident, but there's still that outside
          chance that you could be involved in a car accident either by an auto
          defect or negligence of another driver.
        </p>

        <h2>Insurance Companies Driven by Profit Motive</h2>
        <p>
          If you ever have the misfortune of getting into a car accident, you
          may find it increasingly difficult to get compensated for your
          injuries and expenses. An experienced West Covina car crash attorney
          knows that only through skill and tenacity can you prevail against big
          insurance companies. The fact is, these companies and the people they
          employ to look into your accident are mainly driven by the profit
          mantra.{" "}
        </p>
        <p>
          Did you know that it's much more profitable for insurance companies to
          minimize your car accident settlement than it is to recruit new
          clients and collect their premiums? Any seasoned car accident lawyer
          will admit that every dollar a casualty insurance firm saves on a
          claim is 100 percent profit. So before you talk to your insurance
          company or their adjusters or attorneys, consult a car accident lawyer
          about your claim.{" "}
        </p>
        <h2>Consult a Car Accident Lawyer Who Will Stand Up for Your Rights</h2>
        <p>
          You can begin to see why insurance companies hire the most expensive
          lawyers they can afford. Their goal is to limit your compensation
          amounts or pay you nothing at all. And they'll reluctantly give in
          only if they meet their match in court or the settlement table--with a
          car accident attorney that skillfully represents your case.
        </p>
        <p>
          The <Link to="/car-accidents">experienced car accident lawyer</Link>{" "}
          you retain will not only stand up for your rights, but help you with
          the paperwork and critical negotiations that can mean the difference
          between victory and defeat.{" "}
        </p>
        <p>
          Some accident victims say they can't afford a good car accident
          lawyer. What they don't realize is that many injury law firms offer
          free initial consultations and will gladly advise them of all their
          options. At Bisnar Chase, we not only provide a free evaluation of
          your case, we also have a no win, no fee guarantee, so you don't pay
          unless your lawyer wins your case.
        </p>
        <p>
          Were you injured in a West Covina auto accident? An auto accident
          attorney is an essential member of your team.
        </p>
        <p>
          Call us today to schedule your free no-obligation consultation.{" "}
          <strong>800-561-4887.</strong>
        </p>

        <h2>
          West Covina Car Accidents Attributed to Dangerous Intersections and
          Red-Light Runners
        </h2>
        <p>
          With a diverse population of well over 112,000 and motorists
          increasingly vying for space on crowded city streets, West Covina car
          accident lawyers see it as no surprise that traffic accidents continue
          to mount. Heading up the list are accident-prone intersections that
          have contributed to tragic car collisions. Symptomatic of the city's
          crossroads crisis is the intersection of Cameron and Sunset avenues,
          one of 11 intersections considered the worst in Los Angeles County for
          red-light runners.
        </p>
        <p>
          "Motorists who run red lights are 'rolling the dice' with their lives
          and the lives of the drivers crossing their path," said John Bisnar, a
          car accident lawyer with Bisnar Chase. "Stepped-up traffic enforcement
          can help convince these dangerous drivers that traffic laws must be
          respected."
        </p>
        <p>
          Adding to the West Covina's woes is the tendency of many people to
          drive while under the influence of drugs or alcohol.
        </p>
        <p>
          Underscoring West Covina's traffic problems are the city's total car
          accidents. According to the California Highway Patrol's Statewide
          Integrated Traffic Records System (SWITRS), nine people lost their
          lives in West Covina car accidents and 628 were injured. Motorcycle
          and bicycle accidents injured 29 people in each category. DUI
          accidents killed three and injured 71. Two pedestrians were killed and
          35 were injured in traffic accidents. The following year, 20 vehicles
          were involved in fatal car accidents that took 10 lives.
        </p>
        <p>
          If you have been injured, Please call Bisnar Chase today. We have been
          helping injured car accident victims since 1978. You have nothing to
          lose and probably a lot more to gain than you realize.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
