// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Whittier",
      population: 86635,
      totalAccidents: 3317,
      intersection1: "Painter Ave & Lambert Rd",
      intersection1Accidents: 50,
      intersection1Injuries: 63,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 227.6,
      city1Name: "Santa Fe Springs",
      city1Index: 295.0,
      city2Name: "Pico Rivera",
      city2Index: 232.1,
      city3Name: "La Habra Heights",
      city3Index: 85.5,
      city4Name: "Downey",
      city4Index: 264.2
    },
    locationBox3: {
      intersection2: "Mills Ave & Whittier Blvd",
      intersection2Accidents: 59,
      intersection2Injuries: 59,
      intersection2Deaths: 0,
      intersection3: "Colima Rd & Whittier Blvd",
      intersection3Accidents: 73,
      intersection3Injuries: 37,
      intersection3Deaths: 0,
      intersection4: "Painter Ave & Whittier Blvd",
      intersection4Accidents: 52,
      intersection4Injuries: 26,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Whittier Car Accident Lawyers - Bisnar Chase"
        pageDescription="Call 800-561-4887 for a top-rated Whittier car accident lawyer. No fees unless we win. $500 Million won for our clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Whittier Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <h2> Personal Attention, Experience, Passion.</h2>
        <div>
          <img
            class="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase"
          />
        </div>
        <p>
          Bisnar Chase Personal Injury Lawyers has been helping Whittier
          residents for over 35 years. Our team of experienced attorneys have
          recovered over $500 Million and cared for over 12,000 clients since
          1978. A knowledgeable Whittier{" "}
          <Link to="/car-accidents">car accident lawyer</Link> advises that when
          negotiating a motor vehicle injury settlement with an insurance
          adjuster, the more informed you are, the more likely you'll reach a
          fair compensation figure.
          <div className="snippet">
            <p>
              <span>
                <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
                Partner:
              </span>{" "}
              “I went to law school knowing I wanted to be a personal injury
              attorney. I wanted my life’s work to have a positive impact on
              other people’s lives.”
            </p>
          </div>
        </p>
        <p>
          <b>Passion. Trust. Results.</b>
        </p>
        <ul>
          <li> 96% Success rate</li>
          <li> $500 Million in verdicts and settlements</li>
          <li> Serving Whittier since 1978</li>
          <li> Highest Possible AVVO Rating of Superb</li>
        </ul>
        <p>
          If you've been injured in a car accident from no fault of your own,
          please call our Whittier car accident attorneys today for a
          confidential and free consultation. The call is free, the advice may
          be priceless.
        </p>
        <h2>Ask Questions, Take Careful Notes</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/whittier-car-accident-lawyer.jpg"
              width="230"
              alt="Whittier Car Accident Lawyer"
              className="imgleft-fixed"
            />
          </LazyLoad>
          You will want to start by asking an insurance adjuster what specific
          amounts he's allocated to reimburse you for each item of damage. "Your
          itemized list should match his in terms of expenses and losses," says
          John Bisnar. "You should also ask how he arrived at the amounts
          allocated for your pain and suffering." This is one area that can get
          pretty complicated. If you don't fully understand something, ask for a
          detailed explanation. And write down the answer to every question. The
          details of this Q&A session will be valuable in later negotiations.{" "}
        </p>
        <p>
          Remember, you'll want to learn as much as you can about the whole
          compensation process. A Whittier car accident lawyer will know that
          the claims process can get pretty complex. He also knows that
          insurance company negotiators are skilled professionals who do this on
          a daily basis.
        </p>
        <p>
          Most accident victims are novices when it comes to negotiating in this
          area, and they also have many other concerns deal with, so it's easy
          to get confused with insurance policy jargon. It pays to ask questions
          because being informed boosts your bargaining position. You'll be less
          likely to be surprised by a negotiator's language and tactics.{" "}
        </p>
        <h2>Negotiating Tactics</h2>
        <p>
          "Through years of experience, the most successful car crash lawyer
          will have gained vital negotiating skills," notes John Bisnar. "These
          tactics can help you sidestep costly mistakes during the negotiating
          process." The most reputable lawyers offer no charge, no pressure
          consultations. You owe it to yourself to seek their wise counsel
          before tackling car crash negotiations on your own."
        </p>
        <h2>Whittier Car Accidents Linked to DUI Offenders</h2>
        <p>
          As many car accident lawyers know, Whittier has experienced
          considerable growth, especially since the 1990s and currently has a
          population that is soon to exceed 90,000. Regrettably, this growth has
          also created the climate for more car accidents. As one might expect,
          many of these car accidents are caused by those driving under the
          influence.{" "}
        </p>
        <p>
          A city's rapid growth, combined with scofflaws who insist on driving
          drunk, can result in some very tragic car crashes. In its pro-active
          efforts to remove drunks and other traffic violators from city streets
          and prevent tragic car collisions, the Whittier Police Department set
          up a Driving Under the Influence Checkpoint that operates periodically
          between 5:00 p.m. and 3:00 a.m. during weekends. By having the DUI
          checkpoint police and local officials hope to decrease the number of
          car accidents in Whittier.{" "}
        </p>
        <h2> Reputation and Results</h2>
        <p>
          Bisnar Chase has been very successful{" "}
          <Link to="/case-results">
            collecting hundreds of millions for our clients
          </Link>
          . We will work hard to make sure you receive the largest settlement
          possible. We'll handle all the insurance and paperwork for you and
          even help getting you proper medical treatment. If you have been
          injured as a result of a car accident due to the negligence of
          another, please call 800-561-4887 for top-rated Whittier car accident
          lawyers who can help you. We provide free no-obligation consultations.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
