// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Rancho Cucamonga",
      population: 171386,
      totalAccidents: 3582,
      intersection1: "Foothill Blvd & Day Creek Blvd",
      intersection1Accidents: 113,
      intersection1Injuries: 39,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 185.4,
      city1Name: "Upland",
      city1Index: 235.3,
      city2Name: "Ontario",
      city2Index: 236.9,
      city3Name: "Fontana",
      city3Index: 223.5,
      city4Name: "Montclair",
      city4Index: 395.6
    },
    locationBox3: {
      intersection2: "Foothill Blvd & Vineyard Ave",
      intersection2Accidents: 82,
      intersection2Injuries: 25,
      intersection2Deaths: 0,
      intersection3: "Haven Ave & Foothill Blvd",
      intersection3Accidents: 78,
      intersection3Injuries: 22,
      intersection3Deaths: 0,
      intersection4: "Foothill Blvd & Hermosa Ave",
      intersection4Accidents: 65,
      intersection4Injuries: 22,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rancho Cucamonga Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call the Rancho Cucamonga car accident attorneys of Bisnar Chase. We specialize in serious car accidents in your area. Call 800-561-4887. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rancho Cucamonga Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          If you are injured in a car accident, it would be wise to seek the
          advice of a skilled Rancho Cucamonga car accident attorney. You will
          more than likely need their skill, knowledge and expertise to ensure
          that you are justly and adequately compensated for your disability.
          For those who think car accidents will never happen to them, Rancho
          Cucamonga was named one of the "Most Dangerous California Cities for
          Pedestrians." Fortunately, auto collisions with pedestrians have been
          on a slow decline since 2009, but car accidents still average about
          1500 every year. If you've been injured in a car accident and you need
          someone to walk you through the process of dealing with your insurance
          company to make sure that you get your fair compensation,{" "}
          <Link to="/contact"> contact Bisnar Chase </Link> today, and see if
          you have a case for yor injuries!
        </p>
        <h2>Car Accident Can Affect Future Earning Capacity</h2>
        <p>
          Knowledgeable Rancho Cucamonga car accident lawyers advise their
          clients that they should seek fair and adequate compensation for all
          economic damages they suffered in a car accident. A major economic
          damage to consider is how an accident-related injury can affect one's
          future earning capacity, should they become disabled.
        </p>
        <h2> Establishing a Strong Case</h2>
        <p>
          Car accident attorneys advise that unless these losses can be
          established in a straightforward and simple manner, they should be
          substantiated by using the expert opinion of a forensic accountant.
          These professionals establish a specific level of income and benefits
          that a disabled accident victim is expected to require. The forensic
          accountant's opinion may be augmented by an expert in vocational
          rehabilitation to draw up a recovery timeline and list of expenses
          faced by the disabled victim.
        </p>
        <h2>Proving Car Accident Losses</h2>
        <p>
          This is often difficult work as these damages must be proved to
          insurance adjusters and, if necessary, arbitrators, judges and juries.
          Savvy Rancho Cucamonga{" "}
          <Link to="/car-accidents"> car collision lawyers </Link> will tell you
          that recovering a just settlement that truly reflects an accident
          victim's future lost income often requires extensive research. All the
          more reason to consult an experienced car accident attorney, someone
          who can summon a team of professionals to credibly substantiate your
          claim.
        </p>
        <p>
          The best Rancho Cucamonga car crash lawyers, now offer no charge, no
          pressure consultations to advise you regarding the complex issue of
          car accident disability compensation.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
