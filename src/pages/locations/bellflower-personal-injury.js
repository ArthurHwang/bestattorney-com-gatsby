// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Bellflower",
      population: 77593,
      totalAccidents: 2672,
      intersection1: "Downey Ave & Artesia Blvd",
      intersection1Accidents: 103,
      intersection1Injuries: 63,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 249.8,
      city1Name: "Paramount",
      city1Index: 304.2,
      city2Name: "Norwalk",
      city2Index: 242.0,
      city3Name: "Lakewood",
      city3Index: 228.6,
      city4Name: "Artesia",
      city4Index: 235.5
    },
    locationBox3: {
      intersection2: "Artesia Blvd & Clark Ave",
      intersection2Accidents: 96,
      intersection2Injuries: 39,
      intersection2Deaths: 0,
      intersection3: "Woodruff Ave & Alondra Blvd",
      intersection3Accidents: 121,
      intersection3Injuries: 38,
      intersection3Deaths: 0,
      intersection4: "Bellflower Blvd & Rosecrans Ave",
      intersection4Accidents: 83,
      intersection4Injuries: 32,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bellflower Personal Injury Attorneys - Los Angeles, California"
        pageDescription="Call (323)238-4683 for award-winning Bellflower personal injury attorneys. Free consultations. No win, no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bellflower Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          When you are injured in a traumatic event such as a car accident, a
          slip-and-fall accident, dog attack or an incident caused by a
          dangerous or defective product, the aftermath can be devastating not
          just physically, but also emotionally and financially. As they recover
          from their injuries, victims must also deal with medical expenses,
          rehabilitation and other out-of-pocket costs and income lost as they
          heal.
        </p>
        <p>
          If the injury is catastrophic, victims must also deal with job loss or
          even loss of livelihood. The Bellflower
          <Link to="/"> personal injury lawyers</Link> at Bisnar Chase have more
          than three decades of experience representing victims of serious and
          catastrophic injuries.
        </p>
        <p>
          We offer our unique brand of superior customer service and quality
          legal representation, seeking and obtaining maximum compensation for
          our injured clients and their families. We have the knowledge,
          resources and expertise to go after the big guys, whether they are
          large automakers, industry giants or insurance companies.
        </p>

        <h2>Your Right to Financial Recovery</h2>
        <p>
          If you have been injured in an accident as a result of someone else's
          negligence or wrongdoing, you have the right to be compensated for
          your injuries, damages and losses. Personal injury lawsuits are
          handled in civil courts and the burden of proof in a personal injury
          case rests on the plaintiff. A plaintiff in a car accident case, for
          example, must prove that a driver's negligence, carelessness or
          recklessness caused the accident and the resulting injuries. If the
          driver was under the influence, distracted, fatigued or speeding,
          those are all examples of negligence on the part of the motorist.
        </p>

        <p>
          Some of the losses that injured victims can recover through a personal
          injury civil lawsuit include:
        </p>

        <ul>
          <li>
            Compensation for all medical and health-related expenses such as
            emergency room costs, hospitalization, rehabilitative therapy and
            cost of medication and medical equipment.
          </li>
          <li>
            Lost wages and loss of future income due to the victim's inability
            work. This also includes any lost earning potential.
          </li>
          <li>Pain and suffering</li>
          <li>Emotional distress</li>
          <li>
            Punitive damages in cases where the defendant displayed wanton or
            reckless disregard for the victim's life and health.
          </li>
        </ul>
        <h2> Over 35 Years Serving Los Angeles County</h2>
        <p>
          {" "}
          Bisnar Chase has been serving Los Angeles residents including
          Bellflower for over three decades. We are always committed to the best
          client experience and providing you with passionate legal
          representation. Our personal injury attorneys are top rated
          compassionate members of the Bisnar Chase family. We treat every
          client as if they were the only one.{" "}
        </p>

        <h2>Our No-Fee Guarantee</h2>
        <p>
          {" "}
          At Bisnar Chase, we give each and every client the benefit of our{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            {" "}
            No-Fee Guarantee.
          </Link>{" "}
          This means that we front all costs and fees and you do not pay
          anything until we win for you. We only charge a percentage of what you
          receive as compensation. Our knowledgeable attorneys and skilled
          negotiators are here to help you through what can be a complex claims
          process. We will handle complex paperwork and take care of trial
          procedures and courtroom protocols so you can focus on recovering from
          your injuries. To schedule a no-cost, no-obligation consultation with
          one of our experienced Bellflower personal injury lawyers, call (800)
          561-4887.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
