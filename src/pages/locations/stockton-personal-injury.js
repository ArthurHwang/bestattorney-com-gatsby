// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Stockton",
      population: 298118,
      totalAccidents: 8657,
      intersection1: "N Pershing Ave & W March Ln",
      intersection1Accidents: 93,
      intersection1Injuries: 91,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 610.5,
      city1Name: "Napa",
      city1Index: 187.9,
      city2Name: "Yountville",
      city2Index: 130.5,
      city3Name: "Cotati",
      city3Index: 205.2,
      city4Name: "Windsor",
      city4Index: 157.1
    },
    locationBox3: {
      intersection2: "Hammer Ln & West Ln",
      intersection2Accidents: 127,
      intersection2Injuries: 79,
      intersection2Deaths: 0,
      intersection3: "March Ln & Quail Lakes Dr",
      intersection3Accidents: 78,
      intersection3Injuries: 69,
      intersection3Deaths: 0,
      intersection4: "Hammer Ln & Tam O Shanter Dr",
      intersection4Accidents: 72,
      intersection4Injuries: 65,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Stockton Personal Injury Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Stockton personal injury lawyers. No win, no-fee lawyers. Get a free no-obligation legal consultation today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Stockton Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Stockton personal injury attorneys of Bisnar Chase offer a free case
          review. If we take your case you will not be charged unless the case
          is won. If we don't win, you don't pay. Let our professional personal
          injury team help you get the compensation you deserve.
        </p>
        <p>
          The Stockton personal injury lawyers at Bisnar Chase have been helping
          seriously inured victims since 1978. We have helped those victims
          recover more than 500 million dollars in settlements.
        </p>
        <p>
          Our personal injury attorneys handle a wide range of cases, but most
          notably{" "}
          <Link to="/locations/stockton-car-accidents">car accidents</Link>,
          product liability, dog bites, wrongful death, and slip and fall or
          premises liability cases.
        </p>
        <h2>Stockton Personal Injury Cases</h2>
        <p>
          The most common types of personal injury cases we see in Stockton
          besides car accidents are water related. With boating being a favorite
          activity, Stockton's waterways see many drownings and personal
          injuries due to boat accidents each year.
        </p>
        <p>
          Stockton boating safety officials urge people who take to the water to
          follow these safety precautions: be aware of weather; bring a portable
          radio, flashlight, and first aid kit; tell someone where you are going
          and how long you will be away; check your boat, equipment, boat
          balance, engine and fuel supply before setting off; know how to swim;
          wear a life jacket; and do not consume alcohol while on the water.
        </p>
        <h2>Further Water Dangers Include Pollution</h2>
        <p>
          Aside from recreational accidents, Stockton's waterways have been
          plagued by dangerous levels of pollution. In December of 2009, federal
          regulators and the Port of Stockton decided to reduce the level of
          polluted storm water that dumps into the Stockton Deep Water Channel
          since the EPA found deficiencies in the port nearly two years prior.
        </p>
        <p>
          The agreement to ameliorate the pollution situation comes better late
          than never as Stockton streets and roads were not properly swept,
          increasing the chance that dangerous substances would flow into the
          Delta during the rainy season. This is a costly health hazard, costing
          about $200,000 to improve through monitoring and testing.
        </p>
        <p>
          Other findings of the inspection include fertilizer residue in storm
          drains, soil from construction sites washing away, and tenants
          producing a large amount of storm water runoff. Since residents go in
          the water, it is imperative that the water be kept clean. Water
          pollution contaminants can have serious health effects like the
          spreading of typhoid and waterborne intestinal parasites such as
          amoebiases, giardiases, ascariasis, and hookworm.
        </p>
        <h2>Stockton Personal Injury Lawyers Understand</h2>
        <h3>
          What separates Bisnar Chase from other Stockton accident injury
          attorneys?
        </h3>
        <p>
          Empathy and compassion. Our attorneys have had personal experiences
          with injury related accidents whether it was our own or that of
          someone close to us. This is what drives our passion to help others.
          We understand what you are going through. Also while there are many
          personal injury and accident lawyers claiming to have your best
          interests at heart, it is helpful to keep in mind that the best law
          firms will always offer a free consultation on your case.
        </p>
        <h2>Our No Win, No Fee Guarantee</h2>
        <p>
          Because Bisnar Chase understands your situation and knows about pain
          and suffering, we offer a no win, no fee guarantee. We take care of
          the costs up front for you. If we do not win your case you do not pay.
          There is another side to this. We need to know that our prospective
          clients are being honest with us about the facts of their claim. We
          will not take on a case if there is any deception involved. That said,
          if we take on your case we will fight to the end to get you what you
          are entitled to.
        </p>
        <p>
          If you or someone you know has sustained personal injuries or sickness
          from a boating accident, or have been subject to water contaminants,
          or any other type of accident injury, a knowledgeable Stockton
          personal injury attorney from Bisnar Chase can help you clean up your
          case.
        </p>
        <p>
          If you are the victim of a personal injury, you owe it to yourself to
          get a <strong>Free Case Consultation</strong> from a skilled and
          experienced Stockton personal injury lawyer. Call one of our experts
          now and we will schedule a consultation the same day or by the next
          business day at the latest.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
