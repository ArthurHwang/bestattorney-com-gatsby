// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Escondido",
      population: 148738,
      totalAccidents: 6629,
      intersection1: "Valley Pkwy & Midway Dr",
      intersection1Accidents: 80,
      intersection1Injuries: 105,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 272.0,
      city1Name: "San Marcos",
      city1Index: 175.5,
      city2Name: "Vista",
      city2Index: 247.8,
      city3Name: "Oceanside",
      city3Index: 248.4,
      city4Name: "San Marcos",
      city4Index: 175.5
    },
    locationBox3: {
      intersection2: "Valley Pkwy & Rose St",
      intersection2Accidents: 66,
      intersection2Injuries: 94,
      intersection2Deaths: 0,
      intersection3: "Ash St & Valley Pkwy",
      intersection3Accidents: 51,
      intersection3Injuries: 86,
      intersection3Deaths: 0,
      intersection4: "El Norte Pkwy & Broadway",
      intersection4Accidents: 53,
      intersection4Injuries: 72,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Escondido Car Accident Lawyers - Bisnar Chase "
        pageDescription="If you have been in an accident in Escondido, view our resources to learn what your claim value is."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Escondido Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Escondido intersections can be dangerous, especially the intersection
          at Valley Pkwy and Midway Dr, which has registered 1.3 injuries for
          every accident that occurs there. This is an extremely high ratio and
          demonstrates how dangerous the intersection is. Bisnar Chase Personal
          Injury Attorneys are car accident victim advocates - we fight for
          those who have been injured and who can't fight for themselves against
          insurance companies and guilty parties. Contact us to see if you have
          a case today!
        </p>
        <h2>Car Accidents Are on the Rise in Escondido</h2>
        <p>
          Escondido car accident lawyers are aware that the city is plagued with
          an alarming rise in car accidents. Just last year, an older teen
          driving a pickup truck led police on a circuitous chase through a
          residential neighborhood in Escondido. When the driver lost control of
          his truck on Rose Street near Riva Lane, it rolled over in the middle
          of the street. Fortunately, no one was seriously injured, but the teen
          was arrested on suspicion of driving under the influence (DUI) and
          felony evading arrest.
        </p>
        <p>
          According to car accident injury and fatality statistics released by
          the California Office of Traffic Safety, Escondido faired poorly in
          2007. The city ranked third highest among 52 cities of similar size in
          alcohol-related accidents per vehicle mile. It ranked number one in
          deaths and injuries caused by underage drunk drivers, fourth in
          hit-and-run related injuries, and fourth out of 52 cities for its rate
          of traffic-injuries. These sobering statistics presented city
          officials and local traffic enforcement with some immediate
          challenges.
        </p>
        <p>
          "There's no doubt that Escondido had a real problem on its hands,"
          observed John Bisnar. "DUIs and hit-and-run motor car accidents were
          rising at an alarming rate, and people were being killed and seriously
          injured. Aggressive and immediate action had to be taken."
        </p>
        <h2>DUIs Make Up Half of Car Accidents in Escondido</h2>
        <p>
          Statistics culled by the California Highway Patrol's Statewide
          Integrated Traffic Records System (SWITRS) further underscored
          Escondido's problems. The agency revealed that from 2010-2015, 54
          people lost their lives and 6000 were injured in the Escondido car
          collisions. DUI accidents were responsible for about half of those
          deaths, and 650 of the injuries. Eleven motorcyclists were killed and
          342 were injured in traffic collisions. There were 15 pedestrian car
          accident fatalities and 356 traffic injuries.
        </p>
        <h2>The City of Escondido Responds to Rising Rates of Car Accidents</h2>
        <p>
          Escondido acted quickly to stem its growing car accident problem. The
          city increased its traffic enforcement efforts, including driver's
          license/DUI checkpoints and increased seat-belt enforcement. The
          result: injury accidents dropped by more than 20 percent from 2005 to
          2008. Escondido's DUI enforcement ranking has also gone up. In 2007,
          the city arrested 911 drunken-drivers or 1.06 percent of all its
          licensed drivers (compared to 0.8 percent of licensed drivers for the
          average city). As a result, the city recently ranked 47 out of 52
          cities in DUI enforcement (the higher the ranking, the better the
          enforcement).
        </p>
        <p>
          "Faced with high car accident statistics, Escondido took the 'bull by
          the horns' and acted quickly with stepped-up enforcement efforts,"
          said John Bisnar. "Hopefully, these actions will continue to reduce
          the number of motor vehicle accidents in the city."
        </p>
        <h2>San Diego Car Accident Attorneys You Can Trust</h2>
        <p align="center">
          Immediately call an experienced and reputable{" "}
          <strong>Escondido Car Accident Lawyer</strong> for a free consultation
          at 949-203-3814 or contact the{" "}
          <Link to="/contact">San Diego attorneys</Link> at Bisnar Chase
          Personal Injury Attorneys.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
