// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Fremont",
      population: 224922,
      totalAccidents: 5349,
      intersection1: "Mowry Ave & Fremont Blvd",
      intersection1Accidents: 58,
      intersection1Injuries: 53,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 141.4,
      city1Name: "Newark",
      city1Index: 218.6,
      city2Name: "Union City",
      city2Index: 220.5,
      city3Name: "Hayward",
      city3Index: 322.8,
      city4Name: "Milpitas",
      city4Index: 191.4
    },
    locationBox3: {
      intersection2: "Auto Mall Pkwy & Grimmer Blvd",
      intersection2Accidents: 42,
      intersection2Injuries: 44,
      intersection2Deaths: 0,
      intersection3: "Fremont Blvd & Stevenson Blvd ",
      intersection3Accidents: 34,
      intersection3Injuries: 33,
      intersection3Deaths: 1,
      intersection4: "Mowry Ave & Farwell Dr",
      intersection4Accidents: 35,
      intersection4Injuries: 25,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fremont Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Fremont"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fremont Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>The City of Fremont has grown by about 10%</strong> in the
          last decade, and in that time housing prices have almost doubled, and
          crime rates have dropped. A higher population density, however, means
          that there are more cars on the road, and more dangerous situations
          that can result in an accident. Bisnar Chase represents clients who
          have been injured in an accident or under other circumstances when the
          guilty party was negligent, such as dog bites and premises liability
          cases. We've helped many clients in Northern California with their
          personal injury cases and Fremont is no stranger to us. Call us to
          learn more about our firm and to see if you have a case with us!
        </p>
        <p>
          Fremont, California is located in the Bay Area just outside of San
          Francisco. The city's 213,000 people reside in the largest suburb of
          the east bay. Though suburban streets are thought to be slow and safe
          compared to big-city streets, Fremont residents must be aware of the
          numerous personal injury accidents that happen everyday.
        </p>
        <p>
          Like much of the rest of California, Fremont has seen its fair share
          of drunk driving car accidents and hit-and-runs. In 2005, a driver
          under the influence of alcohol hit a Fremont police officer. The
          officer sustained minor injuries and the driver was arrested.
        </p>
        <p>
          A couple years later a more tragic accident occurred in Fremont where
          a 22-year-old student was pulled over on the shoulder of southbound
          Interstate 880 just past Mission Boulevard. His car had broken down
          and he was calling for help, when a driver in a Lexus came speeding
          toward the exit ramp and broadsided the student in his Toyota. The
          impact caused both cars to slide about 100 yards, the Lexus landing on
          top of the Toyota.
        </p>
        <p>
          It took Fremont firefighters 45 minutes to retrieve the victim's body
          from the wreckage. The Lexus driver had fled the scene in the mean
          time. Any{" "}
          <Link to="/pedestrian-accidents/hit-and-run">
            {" "}
            hit-and-run attorney
          </Link>{" "}
          will stress the importance of being a reliable driver, obeying the
          speed limits and DUI laws, and being responsible toward your fellow
          drivers, especially if an accident has occurred.
        </p>
        <p>
          Another string of hit-and-run accidents happened in the bay area in
          2006, leaving the Fremont and San Francisco communities concerned
          about the risk of hit-and-run car accidents on their streets. Around
          14 people were injured by a motorist who was seen deliberately running
          people down in San Francisco. Police linked these accidents to the one
          that had occurred around noon in Fremont and left a 54-year-old man
          dead.
        </p>
        <p>
          According to the San Francisco Chronicle, a police spokesman said the
          attacks in the city occurred at 12 locations over the duration of a
          20-minute period. Witnesses recalled the driver as "zombie-like", as
          if he had no fear about the consequences in store for the acts he had
          committed.
        </p>
        <h2>Fremont Police Tasers Don't Just Stun</h2>
        <p>
          Fremont personal injury lawyers were concerned to hear about the two
          instances of police officer's Tasers killing those they were used
          against. The first incident took place in San Jose, California, and
          the second in Fremont. Apparently similar stories had popped up in
          Santa Rosa, Pacifica, Vallejo and Salinas.
        </p>
        <p>
          After attempting to escape from Fremont police by scaling a wall near
          a motel, the pursued man was shot numerous times with a Taser gun. The
          suspect received between seven and nine shots from the Taser, was held
          by the police, and taken to the hospital. Twenty to 25 minutes later
          he fell unconscious and died. Taser guns are used by police to deliver
          a 50,000-volt jolt to stop a person with brief incapacitation. Taser
          guns are used instead of real firearms when the intent is not to kill
          the suspect.
        </p>
        <p>
          It is possible that the suspect was under the influence of drugs at
          the time, raising concern as to the effect of Taser guns on those with
          drugs in their systems. Considering the ubiquity of Taser death cases,
          totaling 140 across the nation, the event is of further concern to
          civil rights attorneys and Fremont product liability lawyers. It seems
          the Taser should not be used if it fails to function properly or is
          used improperly. If the Taser gun is not meant to kill someone, there
          should be no margin of error.
        </p>
        <h2>Fremont Personal Injury Law Firms are Watching Out for You</h2>
        <p>
          If you have been injured in a car accident, hit-and-run accident,
          drunk-driving accident, or any other kind of personal injury accident,
          you owe it to yourself to seek help from a good Fremont personal
          injury lawyer.
        </p>
        <p>
          If you are the victim of a personal injury, you owe it to yourself to
          get a copy of the book,{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim"
          </Link>
          . If you need help immediately, call one of our expert personal injury
          lawyers now at 949-203-3814 and we will schedule a consultation the
          same day or by the next business day at the latest.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
