// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Oakland",
      population: 406253,
      totalAccidents: 16923,
      intersection1: "International Blvd & 82nd Ave",
      intersection1Accidents: 57,
      intersection1Injuries: 47,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 970.9,
      city1Name: "Piedmont",
      city1Index: 221.8,
      city2Name: "Alameda",
      city2Index: 201.1,
      city3Name: "Emeryville",
      city3Index: 1068.2,
      city4Name: "Berkeley",
      city4Index: 388.2
    },
    locationBox3: {
      intersection2: "International Blvd & 35th Ave",
      intersection2Accidents: 68,
      intersection2Injuries: 49,
      intersection2Deaths: 0,
      intersection3: "27th St & Northgate Ave ",
      intersection3Accidents: 70,
      intersection3Injuries: 44,
      intersection3Deaths: 1,
      intersection4: "Martin Luther King Jr Way & 52nd St",
      intersection4Accidents: 67,
      intersection4Injuries: 40,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Oakland Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 for highest-rated personal injury lawyer, serving Oakland California. Specialize in catastrophic injuries, car accidents, wrongful death"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Oakland Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Oakland, with its population of 400,000 people,</strong> is
          the 8th largest city in California and one of the country's most
          diverse cities. <Link to="/">Oakland personal injury lawyers</Link>{" "}
          say that the city is also known for reckless drivers who cause a
          number of serious car accidents every year, and it's extremely high
          crime rate. Whether its car accidents or dog bites, premises
          negligence or product defects, whenever you're injured because of
          negligence, it takes a toll on you and those close to you. Bisnar
          Chase helps Oakland victims to get fair compensation for their
          injuries, while negating the stress and effort it takes to fight big
          insurance companies. Call us today to see if you have a case!
        </p>
        <h2>Young Students Most Vulnerable in Oakland Car Accidents</h2>
        <p>
          With about 46,000 students or more than 10 percent of the population,
          Oakland's youth is a particularly vulnerable demographic when it comes
          to car accidents.
        </p>
        <p>
          In October 2009, three students were involved in car accidents
          throughout Oakland in one day. They included an elementary student, a
          middle schools student and a high school student. The middle school
          student, 11-year-old Alana Williams, was killed when she was struck by
          a hit-and-run driver while on a crosswalk near the corner of Foothill
          Boulevard and 64th Avenue, in front of Frick Middle School. Officials
          said this was the same area where a teacher at the same school lost
          part of her lower leg after also getting struck by a hit-and-run
          driver. In response to the latest incident, officials recently
          installed a crossing guard at the intersection.
        </p>
        <p>
          In the same day, an elementary student was hit by a school bus while
          riding his bicycle near Jefferson Elementary School at the corner of
          40th Avenue and Mera Street, while an Oakland High School student on a
          skateboard ran into a car on MacArthur Boulevard. Thankfully, in both
          those incidents, the drivers stopped their vehicles and helped the
          young students who sustained minor accident injuries.
        </p>
        <h2>
          Oakland Teachers, Safety Officials Encourage Pedestrian-Safety
          Education
        </h2>
        <p>
          Teachers said that in addition to gang violence and earthquake
          preparedness, more attention must be paid to Oakland car accidents and
          have suggested that safety begins in schools. They say that a number
          of undocumented residents who cannot get a driver's license still get
          behind the wheel of a car every day, possibly leading to an increase
          in the number of{" "}
          <Link to="/pedestrian-accidents/hit-and-run-laws">
            hit-and-run incidents
          </Link>{" "}
          such as the one that befell the Williams family.
        </p>
        <p>
          With the goal of improving safety on Oakland's streets, teachers have
          proposed the following measures. They include increased campaigns on
          pedestrian awareness and an injury prevention curriculum in schools
          that would teach students the importance of wearing protective gear
          while biking or skateboarding.
        </p>
        <p>
          Meantime, the Oakland Pedestrian Safety Project also promotes
          pedestrian safety in the city. They have conducted major projects such
          as the Pedestrian Master Plan whose goal is to encourage more people
          to put on their walking shoes and leave their car keys behind by
          promoting a pedestrian-friendly environment. The Walk Oakland! Map
          Guide meantime is made available to visitors and residents and
          includes bikeways and information on pedestrian and cyclists' safety.
          Also, the OPSP has created a Safe Routes to School Toolkit and hosts
          an annual Walk to School Day.
        </p>
        <h2>
          Oakland Personal Injury Attorneys Protect Pedestrians, Car Accident
          Victims
        </h2>
        <p>
          If you or a loved one has been involved in an Oakland accident of any
          kind, please consult an Oakland personal injury attorney.
        </p>
        <p>
          Your accident attorney will explain the legal issues of your personal
          injury case and fight for any damages to which you may be entitled.
          They will also ensure that any guilty parties involved in the accident
          will be held accountable. While there are many personal injury and
          accident lawyers claiming to have your best interests at heart, it is
          helpful to keep in mind that the best law firms will always offer a
          free consultation on your case.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
