// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Salinas Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for Salinas car accident attorneys. Specialize in catastrophic injuries. No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Salinas Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Please call 800-561-4887 to reach a Salinas{" "}
          <Link to="/car-accidents">car accident lawyer</Link> for a free
          private consultation. Bisnar Chase has collected over $500 Million in
          verdicts and settlements for our clients and have been serving
          California since 1978.
        </p>
        <h2>Tragic Injuries in Salinas</h2>
        <p>
          The nearly 200,000 residents of Salinas, CA have seen a handful of
          tragic car accidents occur since the new year.
        </p>
        <p>
          On New Year's Day, a recent Salinas High School graduate died in a
          suspected drunk driving accident. The loss of this young man has
          really shaken up the whole community, emphasizing the destruction that
          occurs when people drive drunk.
        </p>
        <p>
          Only a couple months later another car accident claimed the lives of
          two Salinas teens. The 16-year-old driver and two 17-year-old
          passengers had just dropped off a friend, when the driver ran a red
          light at Las Palmas Parkway and River Road. The Lexus the 16-year-old
          was driving hit a Mazda pickup that had legally entered the
          intersection, and the driver and one 17-year-old passenger of the
          Lexus died upon impact. The other passenger in the Lexus sustained
          minor injuries.
        </p>
        <p>
          The Lexus was so smashed that police had to use the Jaws of Life to
          get the passengers out of the car. "It's just sad to see two loving
          girls die over just a red light. It's just not fair. It's not fair at
          all," said a friend of the victims.
        </p>
        <h2>Salinas Responds</h2>
        <p>
          California has struggled with drunk driving accidents in recent years
          and the number of drivers who flee the scene of a drunk driving
          accident they've caused has also risen. These hit-and-runs are making
          it hard to locate those responsible for drunk driving crimes, but are
          making Californians move to change the laws that punish those who
          commit drunk driving and hit-and-run.
        </p>
        <p>
          Further, because of these drunk and reckless driving accidents, the
          California Highway Patrol has begun a program in Salinas aimed to
          educate teen drivers of road safety and responsibility. The program,
          entitled "Start Smart", brought about two dozen teenagers and parents
          to a meeting which discussed these issues.
        </p>
        <p>
          A father who lost his 16-year-old daughter in a crash spoke to the
          group about the terrible loss his family has suffered. The CHP
          officers who hosted the event also spoke about this seeming epidemic
          of teen drivers killing and being killed.
        </p>
        <p>
          "We've had a lot of rain and a lot of change in the weather
          conditions. Summer time is coming up, so there's going to be a lot
          more traffic. People need to slow down and drive a little more
          carefully," advised a CHP officer.
        </p>
        <h2>Salinas Car Accident Attorneys Can Help</h2>
        <p>
          If you have lost a loved one, or have been injured in a car accident,
          you should contact a Salinas car accident lawyer who can sort out your
          case.
        </p>
        <p>Please call 800-561-4887.</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
