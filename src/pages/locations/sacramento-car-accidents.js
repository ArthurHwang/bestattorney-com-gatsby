// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sacramento Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call the Sacramento car accident lawyers of Bisnar Chase. We specialize in catastrophic injuries of car accidents, wrongful death & auto defects.  No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sacramento Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Contact our top rated and highly experienced Sacramento{" "}
          <Link to="/car-accidents">car accident attorneys</Link> for a free
          case evaluation. We can discuss your right to compensation and help
          take the burden off of you. There is NO fee unless we win your case.
          We specialize in catastrophic injuries from serious car accidents.
        </p>
        <h2>Sacramento A Dangerous Area</h2>
        <p>
          Residents believe Sacramento's dubious distinction is well deserved.
          Besides careless youth and drunken drivers, they blame the high
          accident rates on the city's troublesome roads and intersections.{" "}
        </p>
        <p>
          California Highway Patrol officials have tried to cut down auto
          accident rates in major accident-prone zones like Watt Avenue, It's
          easily the most auto accident-prone site in Sacramento ,with at least
          7,800 crashes in the past decade. CHP and traffic enforcement officers
          have tried everything from widening the roads to changes in traffic
          flow with little success. Watt Avenue stubbornly remains one of the
          most dangerous roads in the state. Road widening efforts have
          encountered stiff resistance from city residents who fear their street
          could become another busy Los Angeles freeway.
        </p>
        <h2>Efforts to Reduce Auto Accidents</h2>
        <p>
          Efforts are underway to reduce traffic levels and make Watt Avenue
          friendlier to pedestrians and bicyclists. This is a street where the
          posted speed limit is 40 mph, yet motorists routinely zip along at a
          heady 55 mph. Accommodating the heavy traffic from neighboring Elk
          Grove and Roseville presents an added challenge. Still another factor
          contributing to Watt Avenue's congestion is heavy truck traffic. Add
          to this the large number of shopping mall driveways with cars going in
          and out and you have an accident waiting to happen.
        </p>
        <p>
          Another accident-prone site is Highway 99. Nicknamed Blood Alley, the
          CHP says that Highway 99 is a perfectly good road, and that it's the
          drivers who make it dangerous. Although some would argue that H99's
          lack of traffic lights, varying roadway widths, sweeping curves,
          frequent left-turn lanes and cross streets have contributed to Blood
          Alley's reputation. Drivers who frequent this highway have seen the
          impatient motorists who won't hesitate to sneak right up to the bumper
          of a car they feel is going too slow, then try to pass it using the
          oncoming traffic lane. A report published by AAA found that Sacramento
          drivers think Highway 99 has some of the "meanest and most aggressive
          drivers" in the North State. It's no wonder changes are in the works
          to make parts of this highway safer.
        </p>
        <h2>Get Immediate Legal Help</h2>
        <p>
          To find out if you are entitled to compensation for your injuries
          please call our Sacramento auto accident lawyers toll free at
          800-561-4887. The call is free, the information may be priceless.
        </p>
        <p>
          <strong>Northern California Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">5139 Geary Boulevard</div>
            <span className="locality">San Francisco</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">94118</span>
            <br />
            <span className="tel">800-561-4887</span>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
