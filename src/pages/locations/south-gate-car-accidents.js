// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "South Gate",
      population: 95677,
      totalAccidents: 2943,
      intersection1: "Firestone Blvd & Garfield Ave",
      intersection1Accidents: 184,
      intersection1Injuries: 76,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 322.7,
      city1Name: "Lynwood",
      city1Index: 314.7,
      city2Name: "Cudahy",
      city2Index: 205.8,
      city3Name: "Bell",
      city3Index: 246.9,
      city4Name: "Bell Gardens",
      city4Index: 194.5
    },
    locationBox3: {
      intersection2: "Firestone Blvd & Long Beach Blvd",
      intersection2Accidents: 72,
      intersection2Injuries: 55,
      intersection2Deaths: 0,
      intersection3: "Firestone Blvd & Rayo Ave",
      intersection3Accidents: 98,
      intersection3Injuries: 50,
      intersection3Deaths: 0,
      intersection4: "California Ave & Tweedy Blvd",
      intersection4Accidents: 58,
      intersection4Injuries: 50,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="South Gate Car Accident Lawyers - Bisnar Chase "
        pageDescription="Call 800-561-4887 for South Gate car accident lawyers who've recovered over $500 Million for our clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>South Gate Car Accidents</h1>
        <BreadCrumbs location={location} />

        <p>
          Contact our South Gate car accident attorneys for information
          regarding a no-fault car accident. You may be entitled to compensation
          including medical bills. We've been helping Los Angeles residents for
          over thirty five years. The consultation is free and we advance all
          costs.
        </p>
        <p>
          {" "}
          <Link to="/car-accidents">Car accident lawyers </Link> will tell you
          that with a total population of nearly 103,000, South Gate in
          southeastern Los Angeles County is like most cities in southern
          California. Rush-hour commuters and inattentive drivers can create
          South Gate car collisions that result in fatalities and injuries.
        </p>
        <p>
          A retrospective view from 2001 to 2003 shows that South Gate
          experienced a total of 16 fatal auto collisions, according to the
          National Highway Traffic Safety Administration. This computes to 1.7
          fatal South Gate car crashes per 12,000 people. These numbers are
          below the national norm for a city the size of South Gate.
        </p>
        <p>
          Jumping ahead to 2006, statistics compiled by the California Highway
          Patrol's Statewide Integrated Traffic Records System (SWITRS) revealed
          that seven people were killed and 340 people were injured in South
          Gate car crashes. Bicycle accidents killed one and injured 25. Three
          pedestrians were killed and 44 were injured in motor vehicle mishaps.
          And motorcycle accidents injured seven people. Drunk drivers caused
          one death and 35 injuries. During the following year, seven car
          accidents resulted in eight deaths.
        </p>
        <p>
          Increasing numbers of South Gate car collision lawyers have observed
          that red-light cameras are a factor in reducing car accidents at
          dangerous intersections. It seems that red-light violators are not so
          eager to "beat the light" once they spot the words "photo enforced."
          Another factor to consider is that a number of photo enforcement
          systems create both motion video and still photos to prove that a
          violation took place. Then there's the hefty fine to pay--typically
          $361 for gaining just a few minutes in traffic.
        </p>
        <p>
          Having proved themselves in many dangerous intersections across the
          country, red-light cameras have been adopted by cities throughout Los
          Angeles County. South Gate adopted its own Red-Light Photo Enforcement
          program, installing cameras at critical crash-prone intersections.
          Among these are including Atlantic and Firestone, Atlantic and Tweedy
          Blvd., Atlantic Avenue and Imperial Hwy, California and Firestone,
          Imperial Hwy and Garfield Avenue, Firestone and Garfield, and Garfield
          and Southern.{" "}
        </p>
        <p>
          South Gate also implemented another effective measure to help reduce
          car accidents--the sobriety checkpoint. On December 14th, 2007 one
          checkpoint was conducted at 11 pm. on Firestone Blvd. DUI checkpoints
          help keep impaired drivers off city streets. They also help prevent
          unsafe cars and unlicensed drivers from hurting people. In short, they
          have saved many lives. In fact, pro-active efforts like these have
          kept DUI fatalities down throughout Los Angeles County during the 2008
          holiday season compared to the same period in 2007.{" "}
        </p>
        <p align="center">
          Immediately call an experienced and reputable South Gate Car Accident
          Lawyer for a free consultation at 800-561-4887.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
