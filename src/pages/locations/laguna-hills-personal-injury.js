// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Laguna Hills",
      population: 30880,
      totalAccidents: 1471,
      intersection1: "Alicia Pkwy & Paseo De Valencia ",
      intersection1Accidents: 100,
      intersection1Injuries: 48,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 119.1,
      city1Name: "Laguna Woods",
      city1Index: 54.2,
      city2Name: "Aliso Viejo",
      city2Index: 47.5,
      city3Name: "Mission Viejo",
      city3Index: 70.0,
      city4Name: "Lake Forest",
      city4Index: 97.2
    },
    locationBox3: {
      intersection2: "Avenida De La Carlota & El Toro Rd ",
      intersection2Accidents: 107,
      intersection2Injuries: 48,
      intersection2Deaths: 0,
      intersection3: "Laguna Hills Dr & Paseo De Valencia ",
      intersection3Accidents: 33,
      intersection3Injuries: 34,
      intersection3Deaths: 1,
      intersection4: "Lake Forest Dr & Avenida De La Carlota ",
      intersection4Accidents: 51,
      intersection4Injuries: 38,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Hills Personal Injury Lawyers - 949-203-3814"
        pageDescription="Call 949-203-3814 for highest-rated Laguna Hills Personal Injury Lawyers.  Specialize in catastrophic injuries, car accidents. No fee, No win"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Hills Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Suffering a personal injury can turn an individual's life or even an
            entire family's life upside down.
          </strong>{" "}
          Families are burdened with the emotional trauma of seeing loved ones
          severely injured, hospitalized, sometimes fighting to stay alive. They
          must also deal with the financial challenges of a wage earner in the
          family who is unable to work and mounting medical and other daily
          expenses.{" "}
        </p>
        <p>
          When a personal injury occurs as a result of someone else's negligence
          or wrongdoing, victims can seek compensation for damages such as
          medical expenses, lost income, cost of hospitalization, rehabilitation
          and even pain and suffering. The{" "}
          <Link to="/about-us">
            Laguna Hills personal injury lawyers at Bisnar Chase
          </Link>{" "}
          have a long and successful track record helping seriously injured
          victims secure fair and full compensation for their losses. Contact us
          to see if you have a case today!
        </p>
        <h2>Contacting an Experienced Injury Lawyer</h2>
        <p>
          The Laguna Hills personal injury attorneys at Bisnar Chase offer a
          no-fee guarantee to all its clients. This means that{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">you pay us nothing</Link>{" "}
          – fees or costs – unless you have won your case and have collected on
          your claim. Our attorneys provide quality legal representation and
          superior customer service that is tempered with compassion and
          sensitivity to our clients' needs. Please contact us at 949-203-3814
          if you would like to obtain more information about pursuing your legal
          rights.
        </p>
        <h2>Have You Suffered an Injury? </h2>
        <p>
          Laguna Hills is a wealthy, suburban community in South Orange County,
          which is much sought after for its proximity to beautiful beaches.
          But, no community, regardless of how safe or upscale it may be, is
          immune from the everyday hazards that result in serious injury or
          death. Those who live and work in Laguna Hills are vulnerable to a
          host of potentially traumatic incidents including auto accidents,
          slip-and-fall accidents, dog bites and other traumatic incidents that
          could result in serious personal injuries.{" "}
        </p>
        <h2>What to Do If You Have Been Injured</h2>
        <p>
          There are a number of steps you can take to protect your rights if you
          have been injured as a result of someone else's negligence or
          wrongdoing:
        </p>
        <ul type="disc">
          <li>
            <strong>File a complaint/report.</strong> If you have been injured
            in an auto accident, for example, make sure you file a police report
            and obtain a copy. If you have been injured in a slip-and-fall
            accident, make sure you register a complaint with the property owner
            and/or manager. This ensures that there is some documentation of
            incident and when and where it occurred.
          </li>
          <li>
            <strong>Get medical attention right away.</strong> Regardless of how
            and where you are injured, you may be in shock immediately
            afterward. You may not feel pain or experience all the symptoms of
            the injury right away. Some of the symptoms might not even surface
            for days after the incident. But, it would be in your best interest
            to see a doctor right away and get the necessary treatment and care.
            This will not only help document your injuries, but also give you
            your best shot at a quick and complete recovery.
          </li>
          <li>
            <strong>Gather important evidence.</strong> Take photographs of
            where the accident occurred, the different parties and your
            injuries. Obtain insurance and contact information from the other
            parties involved. If anyone saw the incident occur, be sure to get
            their contact information because eyewitness testimony could prove
            invaluable in a personal injury case.
          </li>
          <li>
            <strong>Preserve evidence.</strong> Don't get rid of physical
            evidence such as a damaged vehicle, malfunctioned part or torn or
            bloody clothing. These can be extremely important pieces of evidence
            that will help bolster your injury claim.
          </li>

          <li>
            <strong>Get the counsel and guidance you need.</strong> Contact an
            experienced Laguna Hills personal injury lawyer who will remain on
            your side, fight for your rights and ensure that you receive just
            compensation for all your losses.
          </li>
        </ul>
        <h2>What is Your Injury Claim Worth?</h2>
        <p>
          The amount of compensation you receive will be directly tied to the
          nature and extent of your injuries and the losses you have suffered.
          To determine what your case is really worth, you need to assess the
          damages and figure out what your injuries have cost you physically,
          financially and emotionally. Typically, the damages that are sought in
          a personal injury case include:
        </p>
        <p>
          <strong>Medical expenses:</strong> This includes reimbursement for
          treatment you have already received as well as compensation for the
          estimated cost of the care and treatment you will need in the future
          because of the accident.
          <br />
          <strong>Lost income:</strong> You may be able to receive compensation
          for the impact your injuries have had on your salary and wages. If you
          have lost your job or the ability to earn a livelihood as a result of
          your injuries, you can seek compensation for those losses as well.
          <br />
          <strong>Rehabilitation costs:</strong> If you have suffered injuries
          such as a brain injury or multiple broken bones, you may need
          long-term rehabilitation, which is often not covered by insurance.
          These expenses are also typically included in a personal injury claim.
          <br />
          <strong>Permanent injury and disability:</strong> Examples of
          permanent injury is where a person had to have a leg amputated or
          suffered severe mental disabilities because of a brain injury. <br />
          Pain and suffering: This refers to the physical pain and discomfort
          victims suffer during the accident and its aftermath. Some may suffer
          from chronic pain for the rest of their lives.
          <br />
          <strong>Emotional distress:</strong> Accidents and injuries, no doubt,
          take an emotional toll not just on victims but also their families.
          <br />
          <strong>Loss of consortium:</strong> These damages typically relate to
          the impact the injuries have on the plaintiff's relationship with his
          or her spouse. Loss of companionship refers to the plaintiff's
          inability to maintain a sexual relationship.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=1301+Dove+St.,+Newport+Beach,+CA,+92660&amp;sll=37.0625,-95.677068&amp;sspn=33.02306,56.337891&amp;ie=UTF8&amp;ll=33.680783,-117.858582&amp;spn=0.068011,0.110035&amp;z=13&amp;output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <Link to="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=1301+Dove+St.,+Newport+Beach,+CA,+92660&amp;sll=37.0625,-95.677068&amp;sspn=33.02306,56.337891&amp;ie=UTF8&amp;ll=33.680783,-117.858582&amp;spn=0.068011,0.110035&amp;z=13">
          View Larger Map
        </Link>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
