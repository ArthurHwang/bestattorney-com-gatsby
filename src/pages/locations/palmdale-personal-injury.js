// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Palmdale Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated personal injury attorneys, serving Palmdale California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Palmdale Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Palmdale is known as a family-oriented community with a high quality
          of life. A first-className medical campus called Palmdale Regional
          Medical Center includes the region's largest emergency department, a
          helipad, medical office towers, and a senior housing complex.
        </p>
        <h2>Fair Compensation</h2>
        <p>
          If you've been injured in a personal injury accident our Palmdale
          personal injury lawyers may be able to help you. We've helped over
          12,000 Californians and have a 96% success rate. Some of our practice
          areas include;
        </p>
        <ul>
          <li>car accidents</li>
          <li>serious dog bites</li>
          <li>product defects</li>
          <li>employment discrimination</li>
          <li>catastrophic personal injuries</li>
        </ul>
        <p>
          The legal team at Bisnar Chase will work hard to make sure you get top
          notch representation with experienced trial attorneys. Every client we
          represent is unique and we make sure your needs are met. We have great
          negotiators on our legal team looking out for you and your best
          interest when it comes to compensation and representation.
        </p>
        <h2>Experience and Trust</h2>
        <p>
          Over thirty five years serving California. Since we specialize in
          injuries you can be sure that our team will work together very closely
          on your case. Six trial attorneys and a large pre-litigation and
          litigation team all working together to get the best possible
          settlement for you.
        </p>
        <h2>Get Immediate Legal Help</h2>
        <p>
          Call today to speak with a Palmdale Personal Injury Lawyer. The
          consultation is confidential and free.
        </p>
        <p>
          Contact our Palmdale Personal Injury Attorneys at our Los Angeles
          office:
        </p>
        <p>
          6701 Center Drive West, 14th fl.
          <br />
          Los Angeles, CA 90045
          <br />
          (323) 238-4683
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
