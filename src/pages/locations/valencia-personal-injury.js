// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Valencia Personal Injury Lawyer - Accident Attorney"
        pageDescription="Call 949-203-3814 for a free consultation with Valencia personal injury attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Valencia Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          The <strong>Valencia personal injury lawyers</strong> at Bisnar Chase
          are dedicated personal injury attorneys with 30 years of combined
          experience. If you are in need of a Valencia personal injury attorney,
          please call us at <strong>800-561-4887</strong> for immediate
          assistance.
        </p>

        <p>
          There are many types of personal injuries, but the most common are{" "}
          <Link to="/locations/valencia-car-accidents">car accidents</Link>, dog
          bites, swimming pool accidents, and product defects, especially auto
          defects. Other personal injury cases we handle are amusement and theme
          park accidents, pedestrian accidents, premise liability and employment
          lawsuits.
        </p>
        <p>
          Often times when a personal injury is the result of a car accident,
          the victim might be tempted to settle for whatever the insurance
          company decides is the cap. This should never be the case. If the
          victim has exorbitant medical bills in addition to the cost of car
          repairs, usually what the insurance company is offering is not enough.
        </p>

        <p>
          Whether it is a car accident, an auto defect, or a slip and fall
          accident, it is in your best interest to consult with a Valencia
          personal injury lawyer before you talk to anyone else. In fact your
          doctor is the only one you should discuss your situation with until
          you get legal assistance.
        </p>

        <h2>Experienced Valencia Personal Injury Lawyers</h2>

        <p>
          The California personal injury attorneys at Bisnar Chase have tried
          hundreds of cases since 1978. We have gone to bat for victims of
          seatbelt defects, seatback failure, premises liability due to
          negligence, wrongful termination, dog bites, nursing home abuse, and
          wrongful death. One of these ended up being a 24 million dollar
          settlement. We are not only experienced, we are passionate and
          dilligent about winning. No one should have to suffer at the hands of
          someone elses negligence.
        </p>

        <h2>What Makes Us Unique?</h2>
        <h3>
          Besides experience it is the compassion, the passion, the
          professionalism and the availability.
        </h3>
        <p>
          From the initial intake you will notice how committed Bisnar Chase is
          to client care. Our compassionate staff of paralegals understands what
          you are going through. Once we get the details of the case from you,
          we take care of everything else. All we want you to do is heal.
        </p>
        <p>
          Communication and availability is very impotant to us. Bisnar Chase
          personal injury lawyers make it a habit to always keep clients to date
          on the progress of their case.
        </p>
        <p>
          Diligence comes from that passion for what we do. We fight hard to get
          our clients what they deserve. We have been very successful at getting
          our clients even more than they were expecting.
        </p>
        <p>
          Trust comes from all of the above and trust is very important to us.
          We can only succeed with trust. We have years of successful
          experience. We are professional and ethical and it shows in the
          glowing testimonials we receive.
        </p>
        <p>
          Bisnar Chase is a{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            contigent fee law firm
          </Link>
          . We have a no win, no fee guarantee.
        </p>
        <p>
          If you were injured in an accident in Valencia due to someone elses
          negligence, please call our Valencia Personal Injury Lawyers for a
          free, no-obligation consultation. (800) 561-4887
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
