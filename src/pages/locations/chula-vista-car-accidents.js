// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Chula Vista Car Accident Lawyers - Bisnar Chase"
        pageDescription="If you've been hurt in a car accident call our Chula Vista car accident lawyers for a free case review. Call 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Chula Vista Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <h2>Passion. Trust. Results. Since 1978</h2>
        <p>
          If you've been injured in a car accident contact our Chula Vista car
          accident attorneys for a free consultation. Call 800-561-4887.
        </p>
        <p>
          Bisnar Chase personal injury attorneys opened its doors in 1978.{" "}
          <Link to="/attorneys/john-bisnar">John Bisnar</Link> had been in a
          serious car accident and remembers how poorly he was represented. John
          decided he'd focus on the best client care in the industry to his
          personal injury clients -- and he did just that.
        </p>
        <p>
          {" "}
          For over 35 years now, Bisnar Chase has maintained a{" "}
          <Link to="/about-us/testimonials">great client rating</Link> and
          received thousands of referrals from former satisfied clients. Bisnar
          Chase believes that your care starts from the very first phone call to
          the day your settlement check leaves the office. Every client is a top
          priority and every staff member is dedicated to John's principle.
        </p>
        <h2> No Fee Guarantee</h2>
        <p>
          Being injured and having your life tossed upside down is bad enough,
          but having to worry about legal fees is a nightmare. Bisnar Chase has
          a superior no win, no fee promise that we've stuck with for over three
          decades. While we are representing you there will be no cost to you
          and if we don't win your case, you'll pay nothing, period! John Bisnar
          and Brian Chase believe in their no fee philosophy and they also
          believe it helps you to focus on healing by giving you one less thing
          to worry about.
        </p>
        <h2>Experienced Trial Lawyers</h2>
        <p>
          Brian Chase wants only the best for his clients. Our team of trial
          lawyers are at the top of their field. Bisnar Chase attorneys are all
          Super Lawyers, have superb AVVO ratings and spend a lot of time in the
          courtrooms perfecting their skill. We've taken on some of the biggest
          cases that other law firms wouldn't touch and we have the experience
          and resources to do it.
        </p>
        <h2>Car Accident Case Results</h2>
        <ul>
          <li>
            <strong>$8,500,000</strong> - Motor vehicle accident - wrongful
            death
          </li>
          <li>
            <strong>$3,000,000</strong> - Motor vehicle accident
          </li>
          <li>
            <strong>$2,360,000</strong> - Wrongful death, commercial vehicle -
            motor vehicle accident
          </li>
          <li>
            <strong>$1,050,000</strong> - Auto Defect Car Accident
          </li>
          <li>
            <strong>$900,000</strong> - Product defect - motor vehicle accident
          </li>
          <li>
            <strong>$600,000</strong> - Auto defect - motor vehicle accident
          </li>
        </ul>
        <h2>A High Rated Law Firm</h2>
        <ul>
          <li>2015 NADC Top One Percent Lawyers</li>
          <li>2014, 2004 OCTLA Trial Lawyer of the Year </li>
          <li>2013 Top Attorney in O.C. Award</li>
          <li>2012 Trial Attorney of the Year, CAOC</li>
          <li>California Supreme Court and Appellate Court case winner</li>
          <li>Top 100 Trial Lawyers in America, ATLA - Since 2007</li>
          <li>Past President of Orange County Trial Lawyers Assoc.</li>
          <li>Top 50 Orange County Lawyers,SuperLawyers</li>
        </ul>
        <h2>Get Immediate Legal Help Today</h2>
        <p>
          If you've been injured in a car accident, contact our experienced
          Chula Vista car accident lawyers for a confidential case evaluation.
          The call is free and the advice may be priceless. Call 800-561-4887.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
