// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Hawthorne",
      population: 86199,
      totalAccidents: 3068,
      intersection1: "Crenshaw Blvd & El Segundo Blvd",
      intersection1Accidents: 116,
      intersection1Injuries: 133,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 357.0,
      city1Name: "Lawndale",
      city1Index: 213.6,
      city2Name: "Inglewood",
      city2Index: 356.1,
      city3Name: "Gardena",
      city3Index: 252.2,
      city4Name: "Commerce",
      city4Index: 264.1
    },
    locationBox3: {
      intersection2: "Crenshaw Blvd & 120th St",
      intersection2Accidents: 121,
      intersection2Injuries: 78,
      intersection2Deaths: 1,
      intersection3: "El Segundo Blvd & Prairie Ave",
      intersection3Accidents: 108,
      intersection3Injuries: 82,
      intersection3Deaths: 0,
      intersection4: "Imperial Highway & Inglewood Ave",
      intersection4Accidents: 78,
      intersection4Injuries: 64,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hawthorne Car Accident and Injury Lawyer - Bisnar Chase"
        pageDescription="Our Hawthorne personal injury attorneys will get you back on your feet in no time. Call Bisnar Chase for your free case evaluation today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Hawthorne Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>If you have been injured in a car crash,</strong> seeking
          compensation for your injuries and damages can often be overwhelming
          and confusing.{" "}
          <Link to="/resources/hiring-an-injury-lawyer">
            Hiring a knowledgeable car accident lawyer
          </Link>{" "}
          can be invaluable in these cases, offering critically important advice
          about procedures and options. For those who think car accidents only
          happen to the other guy, a quick glance at CHP accident statistics for
          Hawthorne, California can be quite enlightening. If you've been
          injured in an accident, contact Bisnar Chase and let us give you a
          free evaluation to see if you have a case!
        </p>
        <h2>
          Stretching the Truth in Medical Losses can Hurt Your Credibility
        </h2>
        <p>
          Should you fall victim to a car accident, you are fully entitled to
          seek fair compensation for your medical bills and other expenses. In
          doing so, experienced Hawthorne accident injury lawyers urge their
          clients never to exaggerate medical losses, for this can seriously
          damage one's credibility. You should also never add a previous medical
          condition with injuries you sustained in the accident in question.
          This, likewise, can severely damage your credibility and impair your
          chances for claims recovery. Mixing previous and current injuries or
          exaggerating medical conditions and injuries are just what claims
          adjusters and/or defense attorneys look for. And once discovered, they
          will dismiss anything you say about your injuries and treatment, and
          negotiating a settlement will prove very difficult. Most often, your
          case will move directly to trial before you have the chance to
          negotiate a fair recovery.
        </p>
        <p>
          Many car collision lawyers also suggest you keep a written log of
          medical expenses that relate directly to your current car accident.
          This will be useful when it comes time to negotiate a settlement.
        </p>
        <h2>
          Find a Car Accident Lawyer who is both Trustworthy and Experienced
        </h2>
        <p>
          A knowledgeable Hawthorne car crash lawyer can help you deal with the
          seemingly endless details, rules and procedures--as well as mistakes
          to avoid--when making a car crash injury claim. The best lawyers
          provide no charge, no pressure consultations. And fees are contingency
          based--you pay only after you prevail, in court or at the settlement
          table.
        </p>

        <h3>Los Angeles Office</h3>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
