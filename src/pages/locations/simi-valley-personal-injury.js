// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Simi Valley",
      population: 126181,
      totalAccidents: 2985,
      intersection1: "Erringer Rd & Cochran St",
      intersection1Accidents: 78,
      intersection1Injuries: 62,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 104.9,
      city1Name: "Moorpark",
      city1Index: 88.4,
      city2Name: "Agoura Hills",
      city2Index: 109.3,
      city3Name: "Hidden Hills",
      city3Index: 56.6,
      city4Name: "Thousand Oaks",
      city4Index: 90.8
    },
    locationBox3: {
      intersection2: "Royal Ave & Erringer Rd",
      intersection2Accidents: 62,
      intersection2Injuries: 53,
      intersection2Deaths: 0,
      intersection3: "Tapo St & Cochran St",
      intersection3Accidents: 95,
      intersection3Injuries: 71,
      intersection3Deaths: 0,
      intersection4: "Erringer Rd & Los Angeles Ave",
      intersection4Accidents: 69,
      intersection4Injuries: 37,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Simi Valley Personal Injury Attorneys - California (323) 238-4683"
        pageDescription="Call (323) 238-4683 for highest-rated personal injury attorneys serving Simi Valley, California. Specialize in catastrophic injuries, car accidents.  Free no-obligation consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Simi Valley Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Helping Simi Valley injured clients since 1978 with over $500 Million
          in verdicts and settlements. Your success is our success and our staff
          will stop at nothing to make sure you have a great client experience.
          There are a lot of law firms in Ventura County but not all provide top
          notch client care. At Bisnar Chase we pride ourselves on making sure
          you are receiving passionate trust-worthy services by dedicated
          lawyers.
        </p>
        <b>The Bisnar Chase Difference </b>
        <ul>
          <li> Top 100 Trial Lawyers in America</li>
          <li> 2015 NADC Top 1% Lawyers</li>
          <li> Member American Board of Trial Advocates</li>
        </ul>
        <p>
          If you've been injured and need a Simi Valley{" "}
          <Link to="/los-angeles"> personal Injury Attorney</Link> contact us
          for a free no obligation consultation. Our team of lawyers cover car
          accidents, catastrophic injuries, auto defects and premise liability
          cases in Simi Valley, California. We've been representing clients in
          and around Simi Valley since 1978 and have collected over 500 Million
          in settlements and verdicts. Call 1-800-561-4887 to speak with a Simi
          Valley injury attorney today.
        </p>
        <h2> No win, No fee promise for Simi Valley Personal Injury Case</h2>
        <p>
          Our promise to you is that we will never charge you if we don't win
          your case. If Bisnar Chase cannot collect for you then you simply
          don't pay. We've been offering this promise to our Simi Valley clients
          for over 3 decades.
        </p>
        <h2> Simi Valley Accidents Happen </h2>
        <p>
          Simi Valley has 20 city parks and five county parks to preserve large
          swaths of open space in the nearby Santa Susana Mountains, locally
          known as the "foothills". The city boasts six golf courses and the
          Kanan Ranch home development has nature trails for hikers, bicyclists
          and equestrians to enjoy. Two collegiate baseball teams: The Simi
          Valley Senators and the California Oaks of the California Collegiate
          League in Thousand Oaks, provide sports action to local fans.
        </p>{" "}
        <p>
          {" "}
          With this much activity in the city accidents are bound to happen.
          Whether you've been injured in a car accident, dog bite attack or slip
          and fall, we can represent you.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
