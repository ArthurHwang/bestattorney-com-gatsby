// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Monterey Park",
      population: 61085,
      totalAccidents: 1765,
      intersection1: "Atlantic Blvd & Garvey Ave",
      intersection1Accidents: 87,
      intersection1Injuries: 47,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 159.3,
      city1Name: "Alhambra",
      city1Index: 172.4,
      city2Name: "Montebello",
      city2Index: 270.8,
      city3Name: "Rosemead",
      city3Index: 186.2,
      city4Name: "Commerce",
      city4Index: 264.1
    },
    locationBox3: {
      intersection2: "Atlantic Blvd & Emerson Ave",
      intersection2Accidents: 86,
      intersection2Injuries: 35,
      intersection2Deaths: 0,
      intersection3: "Garvey Ave & Garfield Ave",
      intersection3Accidents: 73,
      intersection3Injuries: 34,
      intersection3Deaths: 0,
      intersection4: "Garfield Ave & Riggin St",
      intersection4Accidents: 49,
      intersection4Injuries: 23,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Monterey Park Personal Injury Attorneys - Bisnar Chase"
        pageDescription="If you've been injured in Monterey Park whether in a car accident, dog bite, or premises case, make sure you're fully equipped to fight for your fair compensation! "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Monterey Park Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            A traumatic event such as car accident is often a surreal experience
            for most of us.
          </strong>{" "}
          Even though we see it on the evening news, read about it in your local
          newspaper or may even drive by a Monterey Park accident scene, we
          never think it could happen to us. Suddenly we are those people who
          are wondering how to pay those medical bills and cope with lost income
          and other out-of-pocket costs. If you have been seriously injured,
          this can be a scary time for you and family. Your life is turned
          upside down and you are left facing severe physical, emotional and
          financial repercussions – all due to someone else's negligence or
          wrongdoing. Bisnar Chase can help with that. Contact us to see if you
          have a case!
        </p>

        <h2>What is Your Personal Injury Case Worth?</h2>
        <p>
          This is often a burning question for clients. This is because it is
          one of the most challenging aspects of the case for injured victims
          and their families to comprehend. The value or worth of a case will
          usually depend largely on the nature and extent of your injuries, the
          level of treatment that you receive and the effect the injuries have
          on your ability to return to work or earn a livelihood.
        </p>
        <p>
          For example, if you have suffered relatively minor injuries in a car
          crash that did not require hospitalization, your case may not be worth
          as much. But if you have suffered a traumatic brain injury in a car
          crash that requires lengthy hospitalization and rehabilitation, your
          case will be worth a lot more. Injured victims can seek compensation
          for damages including all medical expenses, lost wages,
          hospitalization, rehabilitation, permanent injuries, disabilities,
          pain and suffering and emotional distress.
        </p>

        <h2>Protecting Your Rights</h2>

        <p>
          If you have been injured due to someone else's negligence or
          wrongdoing, it is important to take certain steps to protect your
          legal rights. Always file an incident report with the appropriate
          agency or authority and get a copy of the report for your records.
          Seek and obtain medical attention and treatment for your injuries. Be
          sure to preserve all evidence and information from the scene including
          physical evidence, photographs, and contact information for the
          parties involved as well as eyewitnesses. Keep track of all expenses
          relating to the incident and your injuries.
        </p>

        <p>
          Contact an experienced Monterey Park personal injury lawyer who will
          work diligently to help you secure maximum compensation for all your
          damages and losses. At Bisnar Chase, we work on a contingent fee
          basis, which means you don't pay anything until we win for you. We are
          committed to the passionate pursuit of justice and fair compensation
          for our injured clients and their families. Call us at (800) 561-4887
          for a free consultation and comprehensive case evaluation.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
