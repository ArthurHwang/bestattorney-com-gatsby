// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Orange",
      population: 139969,
      totalAccidents: 6709,
      intersection1: "N Prospect St & E Chapman Ave",
      intersection1Accidents: 92,
      intersection1Injuries: 85,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 121.9,
      city1Name: "Villa Park",
      city1Index: 100.9,
      city2Name: "Anaheim",
      city2Index: 243.9,
      city3Name: "Tustin",
      city3Index: 129.3,
      city4Name: "Santa Ana",
      city4Index: 200.6
    },
    locationBox3: {
      intersection2: "Katella Ave & N Tustin St",
      intersection2Accidents: 89,
      intersection2Injuries: 79,
      intersection2Deaths: 0,
      intersection3: "N Tustin St & E Meats Ave",
      intersection3Accidents: 71,
      intersection3Injuries: 73,
      intersection3Deaths: 0,
      intersection4: "N Tustin St & E Chapman Ave",
      intersection4Accidents: 93,
      intersection4Injuries: 64,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange Personal Injury Attorneys - Bisnar Chase"
        pageDescription="If you're looking to hire an attorney, make sure they meet your needs and put you first! Bisnar Chase fights fiercely for our clients!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            If you're looking to retain an experienced personal injury attorney
            in Orange,
          </strong>{" "}
          he or she should be an advocate for your various claims and rights.
          You will have a skilled negotiator on your side of the table. Someone
          who will ensure you are fully compensated for your pain and suffering,
          as well as your expenses. In terms of expenses, most personal injury
          attorneys provide a free initial consultation to inform you of your
          options. Their fees won't be due unless they win your case or settle
          your compensation with the other side.
        </p>
        <p>
          Finding a qualified attorney in Orange often means going beyond the
          lawyer's basic qualifications. Do they possess the financial and legal
          capabilities to stage an effective case against high-powered insurance
          company lawyers and their experts? It's important to find a car
          accident lawyer that has a record of victories and successes -- in
          court and at the bargaining table. They should also be respected by
          their peers and respect you by being responsive to your needs. If
          you're in need of a personal injury attorney in Orange,{" "}
          <Link to="/about-us/">learn a little bit more about us</Link> and let
          us show you how we fight for our clients. Contact us to see if you
          have a case!
        </p>
        <p>
          The City of Orange was incorporated on April 6, 1888 under the general
          laws of the State of California. However, Orange dates back to 1869
          when Alfred Chapman and Andrew Glassell, both lawyers, accepted 1,385
          acres of land from the Rancho Santiago de Santa Ana as legal fees.
          Soon thereafter, the men laid out a one square mile town with ten-acre
          farm lots surrounding a forty-acre central town site. The center of
          the town site became known as the Plaza, which has become the symbol
          of the community. Today, the Plaza and the majority of the original
          one square mile town site, contain many homes and buildings dating to
          the early days of the City; the site is registered on the National
          Register of Historic Places.
        </p>
        <p>
          The Orange personal injury attorneys at Bisnar Chase Personal Injury
          Attorneys offer these resources for your convenience. If you are in
          need of an Orange personal injury lawyer, please call us at
          949-203-3814.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="https://www.cityoforange.org/" target="_blank">
              City of Orange Information Center
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.cityoforange.org/depts/police/default.asp"
              target="_blank"
            >
              Orange Police Department
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.cityoforange.org/depts/fire/default.asp"
              target="_blank"
            >
              Orange Fire Authority
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.orangeusd.k12.ca.us/" target="_blank">
              Orange Department of Education
            </Link>
          </li>
        </ul>
        <p>
          For{" "}
          <Link to="/locations/orange-car-accidents">
            Orange Car Accident Attorneys
          </Link>
          , call 949-203-3814 for a free consultation.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
