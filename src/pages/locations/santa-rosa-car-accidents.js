// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Rosa Car Accident Lawyer - Auto Injury Attorney"
        pageDescription="Call for an award-winning Santa Rosa, California, car accident lawyer. Free consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Troubles in Santa Rosa</h1>
        <BreadCrumbs location={location} />
        <p>
          The city of Santa Rosa is the fifth largest in the California bay area
          and is bound to see a variety of{" "}
          <Link to="/car-accidents">car accidents</Link> take place all the
          time. Lately, high numbers of drunk-driving accidents, as well as the
          potential for car accidents due to auto defects, have many residents
          concerned.
        </p>
        <h2>Santa Rosa Dealers Worry Over Safety of Lexus SUV Model</h2>
        <p>
          Aside from the decisions that each driver can make to be responsible
          on the road, other factors affect drivers' safety. Recent recalls of
          Toyota and Lexus models due to sudden unintended acceleration has
          concerned many vehicle owners, but another Lexus model, the GX460 has
          also caught the attention of a Santa Rosa Lexus Dealership.
        </p>
        <p>
          The luxury SUV went on sale earlier this year and the manager of the
          dealership said he had sold only seven to local drivers. After hearing
          the news that recently Toyota has halted sales of the model after
          Consumer Reports showed the vehicle to have high roll-over risk
          potential, the manager was concerned about consumer safety. The report
          followed a high-speed cornering test that showed the SUV's rear end
          "fishtailing." According to the magazine, this instability may be due
          to the vehicle's electronic stability control system being too slow to
          react.
        </p>
        <p>
          To date, no accidents or injuries related to this issue are known yet,
          but the Santa Rosa Lexus dealership manager has stayed current with
          updates on this concern in order to keep consumers in the area aware.
          Santa Rosa auto defect attorneys warn that any further issues with
          this model could end up in product liability lawsuits.
        </p>
        <h2>Drinking and Driving Takes a Toll</h2>
        <p>
          In Santa Rosa, as in all of California, driving under the influence
          continues to be a devastating problem.
        </p>
        <p>
          In April of this year, a 21-year-old man suffered major injuries after
          driving his Hyundai sedan into redwood trees near Stony Point Road.
          The weather was rainy and the roads were slick when the accident
          occurred at around 12:30 a.m. His vehicle ran into a tree, spun, and
          collided with another tree. When California Highway Patrol Officers
          found him, his seat was crushed-in. The young man was treated for his
          severe injuries at the Santa Rosa Memorial Hospital and arrested on
          suspicion of drunk driving.
        </p>
        <p>
          Only a few days prior to this accident, another Santa Rosa man, aged
          25, crashed his car into a traffic pole on Highway 12. Both the driver
          and his 21-year-old passenger were hospitalized with critical
          injuries. Police said the man had apparently been drinking at a bar in
          the downtown area before driving himself and his passenger in a 1996
          Honda Civic. When the young man tried to drive through a curve in the
          road, he struck the traffic pole and bridge abutment causing his car
          to be smashed so severely that the engine was separated from the
          vehicle.
        </p>
        <h2>Santa Rosa Car Accident Lawyers Know the Rules of the Road</h2>
        <p>
          If you have been a victim of a drunk-driving accident, auto defect, or
          any other kind of car accident injury, a good Santa Rosa car accident
          attorney can help you with the facts.
        </p>
        <div
          className="google-maps"
          data-map="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&amp;sll=33.660639,-117.873344&amp;sspn=0.068011,0.110035&amp;ie=UTF8&amp;ll=37.790388,-122.470522&amp;spn=0.02374,0.036478&amp;z=14&amp;output=embed"
        ></div>
        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&amp;sll=33.660639,-117.873344&amp;sspn=0.068011,0.110035&amp;ie=UTF8&amp;ll=37.790388,-122.470522&amp;spn=0.02374,0.036478&amp;z=14">
            View Larger Map
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
