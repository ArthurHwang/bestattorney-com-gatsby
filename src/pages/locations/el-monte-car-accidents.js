// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "El Monte",
      population: 115708,
      totalAccidents: 3867,
      intersection1: "Valley Blvd & Durfee Ave",
      intersection1Accidents: 58,
      intersection1Injuries: 52,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 203.4,
      city1Name: "South El Monte",
      city1Index: 241.0,
      city2Name: "Temple City",
      city2Index: 122.8,
      city3Name: "Rosemead",
      city3Index: 186.2,
      city4Name: "Baldwin Park",
      city4Index: 195.9
    },
    locationBox3: {
      intersection2: "Santa Anita Ave & Garvey Ave",
      intersection2Accidents: 52,
      intersection2Injuries: 47,
      intersection2Deaths: 0,
      intersection3: "Valley Blvd & Peck Rd",
      intersection3Accidents: 70,
      intersection3Injuries: 41,
      intersection3Deaths: 0,
      intersection4: "Ramona Blvd & Peck Rd",
      intersection4Accidents: 51,
      intersection4Injuries: 43,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="El Monte Car Accident Lawyer - Bisnar Chase"
        pageDescription="If you have been injured in an accident, learn more about what you can do to get compensated!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>El Monte Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>The best injury attorneys know that,</strong> for the
          uninitiated, dealing with most insurance companies after you've had a
          car accident is like walking through a minefield. And those who live
          and drive in the city of El Monte know that car accidents can happen
          to anyone at anytime. Bisnar Chase is here to guide injured victims
          through the difficult times after their accident to make sure that
          they get what they deserve from insurance companies and the guilty
          parties.
        </p>
        <h2>Achieving Fair Compensation is Often an Uphill Battle</h2>
        <p>
          Regrettably, when one suffers a car accident, obtaining justice is
          usually an uphill battle. Confronting big business or a government
          agency to reach an equitable settlement for one's pain, suffering and
          expenses takes skill and endless persistence. A savvy El Monte car
          accident lawyer will tell you that insurance companies, businesses and
          government agencies aren't designed to fairly compensate victims for
          damages that their employees or clients have caused. Unless you "hold
          their feet to the fire" for their legal and moral obligations, these
          agencies will simply offer the bare minimum (or nothing at all) in
          compensation.
        </p>
        <h2>For Insurance Companies, Saving Money on Claims is 100% Profit</h2>
        <p>
          Many experienced car accident lawyers have discovered that it's far
          more profitable for an insurance company to withhold money when
          settling a car accident claim than it is to recruit new clients and
          draw premiums. What really skilled El Monte car crash lawyers know is
          that if a casualty insurance firm makes a nickel profit for every
          premium dollar it collects, it's doing fine; but, every dollar that
          the firm saves on a claim is pure, 100 percent profit. Which means
          that it's 20 times more valuable to an insurance company to save money
          on claims than to pull in premiums.
        </p>
        <h2>Consult the Best Car Accident Lawyer You Can Find</h2>
        <p>
          No wonder insurance companies retain the best lawyers to keep
          compensation payouts to an absolute minimum. All the more reason for
          you to consult the best El Monte car collision lawyer you can find. He
          or she will stand firm for your rights and leverage their skill and
          knowledge to negotiate the most equitable compensation package. You'll
          be glad to know that most car accident attorneys grant free initial
          consultations, and that the fees they charge won't be passed on to you
          unless they are victorious on your behalf--in court or at the
          bargaining table.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
