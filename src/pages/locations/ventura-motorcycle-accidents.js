// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {
  pageSubject: "motorcycle accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Ventura Motorcycle Accident Verdict - Bisnar Chase Lawyers"
        pageDescription="Call 800-561-4887 for highest-rated motorcycle accident lawyers serving Ventura California.  No win, no-fee.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ventura Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Like most California cities with populations over 100,000, Ventura was
          faced with a rising tide of car accidents that resulted in many
          deaths. Deciding it had had enough, Ventura installed red-light
          cameras at 17 different intersections.
        </p>
        <p>
          The move paid off. Red-light running accidents at those crossroads
          fell by 80 percent, and red-light running crashes citywide dropped 29
          percent. Despite the reduction in accidents, fatalities and injuries
          still occur. A Ventura car accident lawyer has knowledge of the area
          and local courts. Finding the right skilled lawyer will be the
          difference between a good case and a great case.
        </p>
        <h3>After the car accident</h3>
        <ul>
          <li>
            Do seek immediate medical attention, even if you think you feel
            fine.
          </li>
          <li>Do file a police report.</li>
          <li>Do get the insurance information from all parties involved.</li>
          <li>
            Do NOT talk to anyone about your car accident or your pain/injuries.
            This is especially true of insurance agents. Contact a qualified car
            accident attorney before you speak with anyone.
          </li>
          <li>Do get names and phone numbers of all witnesses.</li>
          <li>Do take photos or video of the accident scene.</li>
        </ul>
        <h2>Compensation for your injuries</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys has been assisting Californians
          for over thirty five years.{" "}
          <Link to="/attorneys/john-bisnar">John Bisnar</Link> first formed the
          law firm in 1978 after having a horrible experience with his car
          accident lawyer. He decided then that he would deliver a client care
          second to none. Since then, Bisnar Chase has won over $500 Million for
          their clients and represented more than 12,000 people. Our firm has
          highly skilled trial lawyers that will fight for you. If you've been
          in a car accident you may be entitled to compensation including
          medical bills, pain and suffering and time lost from work.
        </p>
        <h2>Free representation if we don't win!</h2>
        <p>
          At Bisnar Chase we believe you shouldn't have to worry about paying
          your lawyers while you are trying to focus on recovery. We offer a no
          win,{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">no fee guarantee</Link>{" "}
          and have since the day we opened our doors. If we don't win your case,
          regardless of cost involved, you won't pay a penny! We even advance
          all costs while your case is being worked.
        </p>
        <h2>Passion. Trust. Results.</h2>
        <p>
          We believe a great client experience should be available to everyone
          seeking legal representation. A good lawyer is one thing, but if the
          legal team doesn't support those efforts, you can feel lost and
          uninformed. Our team of dedicated legal professionals will work hard
          to make sure you are informed every step of the way during and after
          your case.
        </p>
        <p>
          If you've been injured in a car accident contact our Ventura car
          accident attorneys today and find out if you have a case. We will make
          your interaction with us a great experience. Lawyers and legal staff
          you can depend on and be proud of representing you.
        </p>
        <ul>
          <li>Over $500 Million recovered</li>
          <li>Representing Californians since 1978</li>
          <li>Served over 12,000 clients</li>
          <li>Superb AVVO rating</li>
          <li>Trial Lawyer of the Year 2004, 2014</li>
        </ul>

        <p>(800) 561-4887</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
