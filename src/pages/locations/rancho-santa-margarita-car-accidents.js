// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Rancho Santa Margarita",
      population: 49228,
      totalAccidents: 525,
      intersection1: "Santa Margarita Pkwy & Alicia Pkwy",
      intersection1Accidents: 68,
      intersection1Injuries: 23,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 44.7,
      city1Name: "Mission Viejo",
      city1Index: 70.0,
      city2Name: "Lake Forest",
      city2Index: 97.2,
      city3Name: "Costa Mesa",
      city3Index: 214.3,
      city4Name: "Santa Ana",
      city4Index: 200.6
    },
    locationBox3: {
      intersection2: "Santa Margarita Pkwy & Avenida De Las Flores",
      intersection2Accidents: 32,
      intersection2Injuries: 20,
      intersection2Deaths: 0,
      intersection3: "Santa Margarita Pkwy & Antonio Pkwy",
      intersection3Accidents: 37,
      intersection3Injuries: 18,
      intersection3Deaths: 0,
      intersection4: "Antonio Pkwy & Avenida De las Banderas",
      intersection4Accidents: 22,
      intersection4Injuries: 10,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rancho Santa Margarita Car Accident Lawyer - Bisnar Chase "
        pageDescription="Injured in a RSM accident? You need someone to help you fight the insurance companies and get your fair compensation for your medical bills and damages. Call Bisnar Chase Today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rancho Santa Margarita Car Accident Lawyers </h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Being advised by a knowledgeable Rancho Santa Margarita{" "}
            <Link to="/car-accidents">car accident attorney</Link>
          </strong>{" "}
          can save you a lot of aggravation should you suffer a car accident. If
          you're like most car accident victims, you'll need to recover
          non-economic or "pain and suffering" damages, in addition to
          compensation for your medical bills and for lost wages. Car accidents
          can happen in any city, even in the relatively "safe" master-planned
          community of Rancho Santa Margarita. If you've been injured in Rancho
          Santa Margarita, contact Bisnar Chase! We'll be there to take care of
          the difficulties of dealing with the insurance companies and medical
          billing companies, so you don't have to! Call and see if you have a
          case today!
        </p>
        <h2>Details Can Convince Skeptical Jurors</h2>
        <p>
          Better Rancho Santa Margarita auto accident lawyers will often advise
          their clients that if they hope to get a fair and adequate pain and
          suffering damage award, they'll need to convince often skeptical
          jurors of the severity of their non-economic damages.
        </p>
        <p>
          Details are a key factor. But keep in mind that if your case goes to
          trial, it may be six or seven months before your claim is presented to
          a jury. Which is why it's so important to keep a detailed record of
          how your injuries have affected your everyday activities. The fact is,
          you may not recall just how painful some basic tasks were for you.
          Things like preparing meals, putting away the dishes or bathing. Some
          other things you may want to jot down are the visits to relatives you
          passed on, difficulty attending Sunday worship, driving your children
          to school, missing their games and other events.
        </p>
        <h2>Keep a Log</h2>
        <p>
          {" "}
          Keeping a log of your pain and suffering will help remind you of how
          many sacrifices you had to make (and often continue to make) as a
          result of your injuries. Your log should include any discomfort,
          anxiety, difficulty in sleeping or in staying awake, and other
          problems caused by your car accident. You should record the emotional
          effects of missed activities, vacations, weddings, holiday activities
          and family events. The point is, pain and suffering are sometimes
          difficult to prove, which is why detailed records are so vital in
          negotiating a fair and reasonable settlement amount.
        </p>
        <h2>Seek Out the Best Lawyer</h2>
        <p>
          It's worth your while to find a skilled RSM car crash lawyer. One who
          offers no charge, no pressure consultations. Someone who can help you
          understand the things that every car accident victims should know.{" "}
          <Link to="/contact">Ask for a free consultation today.</Link>
        </p>
        <p>
          <strong>Orange County Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">1301 Dove St. Suite 120</div>
            <span className="locality">Newport Beach</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">92660</span>
            <br />
            <span className="tel">949-203-3814</span>
          </div>
        </div>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
