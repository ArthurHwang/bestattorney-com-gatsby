// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Inglewood",
      population: 111542,
      totalAccidents: 2261,
      intersection1: "Crenshaw Blvd & Manchester Blvd ",
      intersection1Accidents: 43,
      intersection1Injuries: 45,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 356.1,
      city1Name: "Hawthorne",
      city1Index: 357.0,
      city2Name: "El Segundo",
      city2Index: 130.1,
      city3Name: "Rosemead",
      city3Index: 186.2,
      city4Name: "Commerce",
      city4Index: 264.1
    },
    locationBox3: {
      intersection2: "Crenshaw Blvd & 104th St",
      intersection2Accidents: 29,
      intersection2Injuries: 43,
      intersection2Deaths: 0,
      intersection3: "Century Blvd & Crenshaw Blvd",
      intersection3Accidents: 40,
      intersection3Injuries: 39,
      intersection3Deaths: 0,
      intersection4: "Crenshaw Blvd & Imperial Hwy",
      intersection4Accidents: 42,
      intersection4Injuries: 37,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Inglewood Car Accident Lawyers - Bisnar Chase"
        pageDescription="Bisnar Chase represents injured Inglewood victims who need help getting compensation for medical bills, lost wages, and general damages. Contact us today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>An Inglewood Lawyer Can Help After An Accident</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            After they've suffered an Inglewood car accident, most people are
            emotionally vulnerable and confused.
          </strong>{" "}
          All the more reason to at least consult an experienced{" "}
          <Link to="/car-accidents">auto accident attorney</Link> before you
          rush headlong into filling out detailed insurance forms, signing
          release papers and offering recorded statements to insurance claims
          adjusters. Getting fair compensation from an insurance company is
          difficult - let Bisnar Chase take on that burden for you. We have the
          experience and knowledge to make sure that our clients don't have to
          worry about how they are going to pay their medical bills or make up
          for wages lost from work. We'll walk you through every step of the
          process. Contact us to see if you have a car accident injury case!
        </p>
        <h2>An Intriguing Spike in Inglewood Car Accidents</h2>
        <p>
          Inglewood lies just to the southwest of Santa Monica in the greater
          Los Angeles area. The city has a relatively low occurrence of car
          accidents and injuries compared to the area around it, but the city
          did have two surprising years in 2011 and 2012 when car accidents
          somehow tripled their normal going from a yearly average of 300 to an
          average of 950 for those two years. In these years, pedestrian
          accidents seemed to spike the most, resulting in 48 and 60 accidents
          per year, respectively, as opposed to 7 and 3 in 2013 and 2014.
          Fortunately, the yearly average of car accident deaths did not
          increase with any statistical significance in these 2 years.
        </p>

        <h2>9% of all accidents are from DUIs</h2>

        <p>
          In Inglewood, instances of drunk driving are actually lower than
          surrounding cities, but drunk drivers are still 3.5 times more likely
          to be in a car accident where someone is killed. For all car accidents
          since 2009, Inglewood has had a accident fatality rate of about .8%,
          or about 1 death in every 115 accidents. For drunk drivers, this
          number is 1 death in every 29 accidents – meaning that DUIs are still
          much more likely to lead to a traffic fatality. Statistically, the
          most frequent DUI incidents occur at 8-10pm, and then again at 2am on
          Inglewood streets.
        </p>

        <h2>Pitfalls to Avoid as an Inglewood Injury Victim</h2>

        <h3>Information Release Forms</h3>

        <p>
          Car accidents can be traumatizing enough without being further
          victimized by insurance companies, which is why Bisnar Chase warns
          victims to consult them before signing any forms given them by
          insurance companies. Among the most onerous are information release
          forms, which grant insurance companies permission to dig up virtually
          every file from your past. Even documents that have nothing to do with
          your accident -- like all medical records, school and employment
          records, your personnel files, and virtually every piece of
          information about you that has ever been documented. Insurance
          companies have dug up STD records, abortions, and other sensitive,
          private information. Occasionally, insurance companies will use this
          information as leverage to make you accept a reduced settlement.
        </p>

        <h3>Recorded Statements</h3>

        <p>
          Another thing insurance companies like to have is your recorded
          statement. Car accident lawyers know that all too often what you say
          can be twisted and taken out of context later to undermine your case.
          Having a car accident lawyer with you during these recording sessions
          and interrogations protects you from being harassed, asked premature,
          prejudicial or irrelevant questions, or coaxed into saying something
          that can be used against you later in court.
        </p>

        <h2>Fighting for Inglewood Clients</h2>

        <p>
          The more you know about how insurance companies operate, the more
          you'll see why consulting - and retaining if necessary - a qualified
          Inglewood accident lawyer can be so important. Bisnar Chase knows how
          difficult it can be to endure a car accident and be injured. If you
          are unable to work and have medical bills to pay it can be extremely
          stressful if you don’t know if you’re going to get a good settlement
          from the insurance company. We can help shoulder that burden! We know
          how to deal with insurance companies and with negligent parties so
          that you can get the best compensation possible. Contact us today to
          see if you have a case!
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
