// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Clarita Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 to speak with a Santa Clarita Car Accident Lawyer free of charge. Hundreds of millions in recovery, serving California since 1978.."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1> Santa Clarita Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Right after a car accident is a confusing time. Trying to figure out
          how to handle the insurance company or deal with the at-fault driver
          can be overwhelming. Before you do anything, contact a{" "}
          <Link to="/los-angeles/santa-clarita-personal-injury-lawyers">
            Santa Clarita personal injury attorney
          </Link>{" "}
          to review your rights. You may be entitled to compensation, and unless
          you are aware of your rights, you will find the insurance company
          hard-balling you. Don't accept a low payout and do not trust the
          defendant's insurance company. Speak with a qualified car accident
          attorney.
        </p>
        <p>
          An experienced auto accident lawyer will know the steps to take to
          protect you and get you the fair compensation you should be getting.
          In almost all cases, the insurance company will rush to settle with
          you saving them thousands of dollars. Your personal injury attorney is
          trained in dealing with insurance adjusters and will mediate on your
          behalf.
        </p>
        <h2>Why Hire Us?</h2>
        <ul>
          <li>
            Over $500 Million recovered for our clients in judgments, verdicts
            and settlements
          </li>
          <li>96% success rate</li>
          <li>Superb 10+ AVVO rating</li>
          <li>Serving California for over 35 years</li>
        </ul>
        <h2>Distracted Driving is Dangerous</h2>
        <p>
          "Eating and talking on hand-held cell phones can be a real distraction
          to driving," said <Link to="/attorneys/john-bisnar">John Bisnar</Link>
          . "People looking down to dial a number or balance a hot cup of coffee
          take a huge risk, especially when approaching an intersection." Not
          surprisingly, teen drivers have also contributed to Santa Clarita's
          auto accident rate. While 16- to 18-year-olds make up only12 percent
          of the driving population, they are involved in 25 percent of all
          driving collisions.
        </p>
        <p>
          "The major cause of death for teens aged 16 to 19 is traffic
          accidents," noted John Bisnar, an experienced car accident lawyer. "Up
          to 60 percent of teens killed while riding as passengers occur while
          another teen is driving. Sadly, nearly 100 local teens lost their
          lives in city traffic collisions--just in the past few years."
        </p>
        <p>
          Santa Clarita is taking aggressive steps to reduce teen car accidents
          through a number of comprehensive community programs. Among these is
          The Sheriff's Teen Traffic Offender Program (STTOP). Launched in 2004,
          residents call a hot line to report the vehicle license number of any
          teen they see driving dangerously. Incoming reports to STTOP have
          numbered between 250 and 300 annually. All calls are followed up with
          intervention efforts by a city sheriff's deputy targeting the drivers
          and their parents. Another effort to bring down the teen accident rate
          is the Every 15 Minutes Program. National statistics reveal that
          someone dies in a traffic-related incident every 15 minutes. Using
          high school students to role-play victims and a drunk driver, the
          program graphically dramatizes that tragedy and aftermath of an
          alcohol-related car accident.
        </p>
        <p>
          To find out if you are entitled to more than an unfair insurance offer
          call our Santa Clarita personal injury attorney today. 800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
