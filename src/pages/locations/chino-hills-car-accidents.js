// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Chino Hills",
      population: 80988,
      totalAccidents: 756,
      intersection1: "Pipeline Ave & Chino Hills Pkwy",
      intersection1Accidents: 55,
      intersection1Injuries: 23,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 100.0,
      city1Name: "Chino",
      city1Index: 224.1,
      city2Name: "Diamond Bar",
      city2Index: 123.8,
      city3Name: "Montclair",
      city3Index: 395.6,
      city4Name: "Pomona",
      city4Index: 345.4
    },
    locationBox3: {
      intersection2: "Peyton Dr & Eucalyptus Ave",
      intersection2Accidents: 55,
      intersection2Injuries: 20,
      intersection2Deaths: 0,
      intersection3: "Chino Hills Pkwy & Peyton Dr",
      intersection3Accidents: 35,
      intersection3Injuries: 13,
      intersection3Deaths: 0,
      intersection4: "Peyton Dr &  Chino Ave",
      intersection4Accidents: 25,
      intersection4Injuries: 12,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Chino Hills Car Accident Lawyers - San Bernardino County"
        pageDescription="call our Chino Hills car accident lawyers at 800-561-4887 for a free case evaluation. We have a 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Chino Hills Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          With a growing population of more than 80,000 people, Chino Hills in
          San Bernardino County, California was once ranked as America's 13th
          safest city by the FBI. While noteworthy, this distinction is
          overshadowed by the city's heavy traffic during rush-hour commutes.
          Naturally, heavy traffic and dangerous driving can easily lead to car
          crashes.{" "}
          <Link to="/san-bernardino">
            Bisnar Chase Personal Injury Attorneys{" "}
          </Link>{" "}
          are skilled and experienced litigators who will go to trial to defend
          you from insurance companies who don't want to pay for your medical
          bills or lost earnings. Contact Bisnar Chase to see if you have a case
          for your car accident or injury!
        </p>
        <h2>The Bisnar Chase Difference</h2>
        <p>
          Bisnar Chase has been serving Californians since 1978. During that
          time we have taken thousands of cases to trial and collected over $500
          Million in verdicts, judgments and settlements for our clients. The BC
          Principal is simple: Do right by the client and profit will flow. We
          know that we are only a solid company if our clients are satisfied. We
          pride ourselves on being one of the Best Places to Work year after
          year. Our staff truly care about each client.
        </p>
        <ul>
          <li>96% Success Rate</li>
          <li>Serving San Bernardino for Over 35 Years</li>
          <li>Superb 10+ AVVO Rated Lawyers</li>
          <li>Top 100 Trial Lawyers in America, ATLA - Since 2007</li>
          <li>2015 NADC Top One Percent Lawyers</li>
          <li>Highest Rated SuperLawyers</li>
          <li>Best Client Satisfaction Guaranteed</li>
          <li>$300M+ In Wins for Our Clients</li>
          <li>Years of Trial Experience</li>
          <li>Familiar with California Courts & Defense Teams</li>
        </ul>

        <p>
          Immediately call an experienced and reputable Chino Hills Car Accident
          Lawyer for a free consultation at 800-561-4887. We'll go over your
          case and let you know right up front if we think we can help you. If
          we do take your case and do not win, you'll pay nothing!
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
