// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Salinas",
      population: 155662,
      totalAccidents: 2975,
      intersection1: "E. Laurel Dr Natividad Rd",
      intersection1Accidents: 107,
      intersection1Injuries: 65,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 414.0,
      city1Name: "Marina",
      city1Index: 207.2,
      city2Name: "Mountain View",
      city2Index: 161.4,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "E. Boronda Rd & Natividad Rd",
      intersection2Accidents: 98,
      intersection2Injuries: 54,
      intersection2Deaths: 0,
      intersection3: "W. Laurel Dr & N. Davis Rd",
      intersection3Accidents: 79,
      intersection3Injuries: 34,
      intersection3Deaths: 1,
      intersection4: "S. Sanborn Rd & John St",
      intersection4Accidents: 55,
      intersection4Injuries: 27,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Salinas Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated personal injury attorneys, serving Salinas California.  Specialize in catastrophic injuries, car accidents & auto defects.  No win, no-fee lawyers.  Free legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Salinas Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          Contact the Salinas personal injury attorneys for a private free
          consultation. We've been assisting residents for over three decades
          for personal injuries such as car accidents, slip and falls and
          serious dog bites.
        </p>
        <p>
          Salinas, county seat of Monterey County and famously the hometown of
          American writer, John Steinbeck, is home to nearly 200,000 residents.
          The people of Salinas have seen their fair share of different types of{" "}
          <Link to="/">personal injury accidents</Link>, even since the
          beginning of the new year. Some of these accidents happen on the road
          but others occur during unexpected situations.
        </p>
        <h2>Unexpected Injuries Occur on the Diamond</h2>
        <p>
          Salinas Valley Pony League organizes many baseball games for Salinas'
          boys and young men. Since April of this year, however, the league has
          seen many personal injuries happen from the use of metal bats. Three
          injuries have occurred, mostly affecting pitchers in games who have
          been struck by a line drive hit mad by the batter using a metal bat.
        </p>
        <p>
          A 14-year-old pitcher was hit in the eye, and may lose his vision from
          the impact, and another player from the Salinas Jr. Giants suffered a
          broken nose.
        </p>
        <p>
          "I want some sort of safety precaution to be brought up for the
          pitcher because they're the ones closest to the batters," said the
          father of the 14-year-old pitcher. "Like the ophthalmologist told us,
          my son didn't stand a chance."
        </p>
        <p>
          The league is considering a switch to wooden bats which might help
          prevent these serious injuries. One issue in this kind of personal
          injury accident is that it is hard to place fault, yet players can end
          up losing eyesight or suffering from devastating brain injury.
        </p>
        <h2>Salinas Personal Injury Lawyers Know the Ropes</h2>
        <p>
          If you have been involved in a personal injury accident on the road or
          in any other kind of situation, you may benefit from consulting a
          personal injury attorney who can help you work out the details in your
          case.
        </p>
        <p>
          If you need help immediately, call one of our expert personal injury
          lawyers now and we will schedule a consultation the same day or by the
          next business day at the latest.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
