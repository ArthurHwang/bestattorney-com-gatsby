// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Seal Beach Car Accident Lawyers - Orange County Car Accident Attorneys"
        pageDescription="Call 949-203-3814 for highest-rated personal injury attorneys, serving Seal Beach California.  Specialize in car accidents, wrongful death & auto defects."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Seal Beach Car Accident Fatalities Linked to Holiday DUI Revelers
        </h1>
        <BreadCrumbs location={location} />
        <p>
          One after another, Seal Beach{" "}
          <Link to="/car-accidents">car accident lawyers</Link> will admit that
          car collisions are a regular occurrence in this community of nearly
          26,000 people. A gateway, coastal city that has retained its quaint,
          small-town atmosphere, Seal Beach in Orange County, California, has
          been challenged with higher than average traffic fatalities often
          caused by party-going revelers who insist on drinking and driving.{" "}
        </p>
        <h2>Seal Beach Car Accident Statistics Rise Above National Norm</h2>
        <p>
          In our report on{" "}
          <Link to="/resources/car-accident-statistics">
            2009-2014 Orange County Car Accident Statistics,
          </Link>{" "}
          we found that Seal Beach has an extremely high rate of DUI accidents,
          second only to Laguna Beach. In fact, an estimated 1.37% of the
          population of Seal Beach was involved in a DUI crash during that time
          period.
        </p>
        <h2>Seal Beach Car Crash Kills Bicyclist</h2>
        <p>
          In 2008, A Garden Grove man was charged with killing a female
          bicyclist while under the influence of drugs and alcohol. According to
          Orange County prosecutors, the man took prescription drugs with a
          blood-alcohol level of 0.13%, and then swerved his pickup truck off a
          Seal Beach road, striking the bicyclist. Seal Beach car accident
          lawyers intimate that the city has decided to aggressively go after
          drunk drivers.
        </p>
        <h2>Seal Beach Car Collisions Blamed on Impaired Drivers</h2>
        <p>
          To save lives and prevent injuries, Seal Beach often sets up Sobriety
          and Driver's License Checkpoints at troublesome streets and
          intersections--like Pacific Coast Highway and First Street. Seal
          Beach's efforts often dovetail with Orange County's "Avoid the 28" DUI
          Campaign, which consists of Roving DUI Saturation Patrols over holiday
          weekends. California's AVOID programs were intended to raise the
          awareness of the multiple problems caused by drunk driving, especially
          during major holidays. Seal Beach Police remind motorists that Buzzed
          Driving Is Drunk Driving. Virtually every Seal Beach car collision
          attorney will advise you to designate a sober driver before you begin
          to party with alcohol.
        </p>
        <h2>Seal Beach Scooter Patrol Prevents Car Crashes</h2>
        <p>
          For those who forget to assign a designated driver, there's the
          Scooter Patrol. This organization offers free transportation to bar
          patrons on major "drinking" holidays, including Super Bowl Sunday.
          Scooter Patrol riders will make their rounds on the weekends to let
          patrons know they can get a free ride home if they need it. Scooter
          patrol drivers take revelers home in their own car, return the keys,
          and come back on their scooter.
        </p>
        <p align="center">
          Immediately call an experienced and reputable Seal Beach Car Accident
          Lawyer for a free consultation at 800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
