// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Valencia Car Accident Lawyer - Car Crash Attorney"
        pageDescription="Call 949-203-3814 for a free consultation with the best Valencia car accident lawyers"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Valencia Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Over three decades of experience and victories. Proven results with
          over $500 Million collected for our clients in settlements and
          verdicts. Commitment to our community through various outreach
          programs and donations, Bisnar Chase not only handles complex cases,
          but cares for the people we share California with. Call one of our
          Valencia <Link to="/car-accidents">car accident attorneys</Link> to
          obtain a free consultation and see if you are entitled to
          compensation. Call 800-561-4887.
        </p>
        <h2>
          Car Accidents Can Be Curtailed Through Photo Enforcement and DUI
          Checkpoints
        </h2>
        <p>
          Valencia car accident lawyers know that car collisions can occur even
          in an affluent master-planned enclave like Valencia. Situated in the
          northwest section of Santa Clarita Valley, the community is home to
          Six Flags Magic Mountain theme park and adjacent to Interstate 5, a
          major freeway that creates major traffic during the holidays.
        </p>

        <h2>Car Crashes Can Be Reduced with Red-Light Cameras</h2>
        <p>
          With traffic safety as one of its major goals, Valencia created a
          photo enforcement program to reduce car collisions. The community set
          up red-light cameras at key, accident-prone intersections. These
          included Magic Mountain Parkway and Mc Bean Parkway, Mc Bean Parkway
          and Newhall Ranch Road, Valencia and McBean Parkway, and Lyons and
          McBean Parkway. Fines for red-light scofflaws are fairly hefty--$446.
        </p>

        <h2>Car Collisions Go Down When DUI Checkpoints Go Up</h2>

        <p>
          In addition, the community often works with Santa Clarita to set up
          DUI and Driver's License Checkpoints at various intersections and
          streets where car accidents have occurred.
        </p>
        <p>
          DUI checkpoints offer several safety benefits. Impaired drivers are
          promptly "removed from circulation" and usually arrested and booked.
          Sobriety checkpoints also provide an educational benefit in that they
          make the general public aware of the risks and dangers of driving
          while intoxicated.
        </p>
        <p>
          Finally, they save lives by making party-goers plan ahead and
          designate a driver who has abstained from drinking and can drive them
          home. Experienced Valencia car accident lawyers know the value of
          these and other pro-active efforts in saving lives.
        </p>

        <p>
          Bisnar Chase personal injury attorneys have been helping car accident
          victims get compensation for damages and injuries since 1978. Finding
          new ways to make the roadways and intersections safer for motorists
          and pedestrians is vital for creating a safe haven for all in the
          Valencia community. Unfortunately accidents still happen and usually
          because of negligence.
        </p>

        <p>
          If you were injured in a car accident in Valencia, please contact us
          for a free consultation with an experienced Valencia car accident
          lawyer. (800) 561-4887
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
