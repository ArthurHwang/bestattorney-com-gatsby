// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Palmdale Car Accident Lawyer - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Call 949-203-3814 for highest-rated car accident lawyers serving Palmdale California. 98% success rate and no win no fee promise. Serving Palmdale residents since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Palmdale Car Accident Lawyer Offers Alternate Settlement Option</h1>
        <BreadCrumbs location={location} />
        <p>
          It's not easy getting fair compensation for injuries and damages
          suffered in a car accident, especially if you opt to go through the
          process yourself. Knowledgeable{" "}
          <Link to="/practice-areas">Palmdale accident lawyers</Link> know that
          claimants and insurance company adjusters often can't agree on what
          constitutes a fair claims settlement. Before offering a way to get by
          this impasse, a review of accident statistics for Palmdale, California
          may be in order.
        </p>
        <h2>Palmdale Car Accidents Injure as Many as Two People Each Day</h2>
        <p>
          Palmdale's car accident statistics can be troubling. In 2006, the
          California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) reported that 10 people were killed and 756 were
          injured in city car crashes. Car accidents also injured 29
          pedestrians, 27 bicyclists and 38 motorcyclists. Drunk drivers caused
          four fatalities and 66 injuries. In 2008, six car collisions ended in
          as many fatalities.
        </p>
        <h2>You Could Win by Waiting</h2>
        <p>
          An astute{" "}
          <Link
            to="/locations/palmdale-personal-injury"
            title="City of Palmdale California"
          >
            Palmdale
          </Link>{" "}
          car accident lawyer knows that if you and the insurance adjuster reach
          an impasse on a fair compensation figure, you can try to settle the
          matter in court. This can go a number ways--the judge can award you
          more than your claim, less than your claim, or zip. If the person who
          caused the accident challenges the verdict, he or she may appeal and
          you're back to where you started.
        </p>
        <p>
          Another alternative is to simply wait things out. California gives
          accident victims two years to settle an injury claim or file a
          lawsuit. Insurance adjusters like to clear up accident claims as
          quickly as possible. They get paid by how fast they settle a case, as
          well as how much money they save their company. They're also fully
          aware that many car accident injuries take time to manifest
          themselves--so the longer they wait, the more injuries could pile up
          on a claim. Which is why Palmdale car collision lawyers advise that if
          you can drag out the negotiations, the insurance company may just
          agree to your desired compensation sum.
        </p>
        <h2>Consult an Experienced Car Accident Lawyer</h2>
        <p>
          Numerous car crash victims have discovered that going it alone takes
          skill, patience and knowledge. One can easily fall into traps and
          mistakes, and there are many specific procedures that claimants must
          adhere to. A skilled car accident lawyer can help you. The best
          Palmdale car crash lawyers offer no charge, no pressure consultations.
          And fees are contingency based.
        </p>
        <strong>Contact us for a free case review</strong>:<br />
        <p>
          6701 Center Drive West, 14th Fl.
          <br />
          Los Angeles, CA 90045
          <br /> (323) 238-4683
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
