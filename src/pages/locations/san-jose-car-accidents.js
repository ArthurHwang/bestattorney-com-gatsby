// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Jose Car Accident Lawyers - Bisnar Chase 800-561-4887"
        pageDescription="San Jose car accident lawyers specializing in traumatic and serious injuries. Free consultation 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Jose Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Nationally recognized award winning legal team of Bisnar Chase has
          over three decades of serving California with serious and complicated
          legal issues. Our San Jose{" "}
          <Link to="/car-accidents">car accident lawyers</Link> have the skills
          and resources to tackle some of the most complex cases. Car accidents
          cause serious injury and death to thousands every year in California
          and we represent those who were not at fault.{" "}
        </p>
        <p>
          It's very important that you understand your rights as a plaintiff and
          protect those rights. The legal team at Bisnar Chase will review your
          case free of charge, and if we take it, advance all costs until your
          case settles.{" "}
        </p>
        <p>
          <strong>
            Call the San Jose car accident lawyers today at 800-561-4887.
          </strong>
        </p>
        <h2>Injuries in San Jose</h2>
        <p>
          The 12 most crash-prone stretches of road in Stockton were identified
          in a recent state report. Known as the 5 Percent Report and released
          every year to raise awareness of California's safety needs and
          challenges, Stockton was shown to have its share of deadly
          intersections. Car accident statistics gleaned from California Highway
          Patrol's Statewide Integrated Traffic Records System (SWITRS) were
          used to identify problem streets.
        </p>
        <p>
          In many respects, San Jose is no different than many large California
          cities. It has its share of traffic accidents and the grim injury and
          death statistics that accompany them. The California Highway Patrol's
          Statewide Integrated Traffic Records System (SWITRS) noted that 46
          people died and 4,130 were injured in traffic collisions.. Alcohol
          accounted for 15 fatalities and 422 injuries in those collisions. The
          disturbing number of accidents underscored the need take
          action--beyond traditional enforcement and engineering efforts.
        </p>
        <h2>Changing Driver Behavior</h2>
        <p>
          To help reduce injuries and deaths on its streets and highways, San
          Jose created a unique program known as Street Smarts. The novel
          traffic-calming program was designed by the City's Department of
          Transportation to enhance the City's safety. The program focuses on
          raising public awareness and changing driver behavior.
        </p>
        <p>
          A key objective of the Street Smarts program was to slow down speeding
          drivers. A citywide public education campaign targeted the behaviors
          of drivers, pedestrians and bicyclists. Advertising and community
          relations messages spread the word that behavioral changes in
          neighborhoods, schools and businesses are necessary to reduce the
          city's accident rate.
        </p>
        <p>
          Working together with AAA, the program addresses and alters some
          critical behaviors that result in accidents. They include the
          following: speeding, running a red light, stop sign violations, school
          zone compliance and crosswalk safety & compliance.
        </p>
        <p>
          The Street Smarts program is so successful that it is seen as a model
          for other cities and counties. San Jose worked with regional partners,
          such as the Santa Clara County Traffic Safe Communities Network, to
          improve driving, walking and bicycling behaviors throughout the Bay
          Area. Countywide Street Smarts efforts reduced red light running
          violations up to 60 percent at critical intersections.
        </p>
        <p>
          To encourage greater regional participation in the Street Smarts
          program, San Jose opted to share creative materials developed for this
          campaign with any public agency at no cost, enabling other agencies to
          take advantage of the initial investment by San Jose to launch Street
          Smarts in their community. Besides16 Bay Area cities and public
          agencies that considered a regional partnership with San Jose, the
          City of Los Angeles also showed an interest in working to expand the
          Street Smarts campaign statewide.
        </p>
        <p>
          "The city of San Jose acted pro-actively to slow down speeding cars
          and limit cut-through traffic," noted nationally-recognized car
          accident lawyer John Bisnar. "This helped reduce accidents and kept
          children, pedestrians and bicyclists safer. Street Smart is one
          program that every major city in California should emulate."
        </p>
        <p>
          <strong>
            Call today to speak with a San Jose{" "}
            <Link to="/">personal injury lawyer</Link>. 800-561-4887.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
