// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Maria Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call to speak with a Santa Maria car accident attorney. No win, no fee guarantee. Best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Maria Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          The largest city in Santa Barbara County, California with nearly
          112,000 people, Santa Maria has a low car collision rate. Santa Maria{" "}
          <Link to="/car-accidents">car accident attorneys</Link> admit that the
          city's red-light camera and DUI enforcement efforts have helped hold
          down the number car collisions in the city.
        </p>
        <p>
          Santa Maria car sccident attorneys will tell you that red-light
          cameras can help cut down on car accidents because they nab red-light
          scofflaws "red handed" on tape. They also convince would-be red-light
          violators to heed the law.
        </p>
        <p>
          To reduce car accidents and injuries in the city, Santa Maria
          implemented a Red-Light Photo Enforcement program. It set up red-light
          cameras at major crash-likely intersections. Locations under their
          watchful eye include 4th Street and Mendocino Avenue, Betteravia and
          Miller, Mendocino Avenue and Administration Drive, Mendocino Avenue
          and Bicentennial Way, Miller and Stowell, Sonoma Highway and Farmers
          Lane, and South College Drive and East Battles Road.
        </p>
        <p>
          Drunk drivers pose a major problem for many cities. Santa Maria was
          pro-active and joined Santa Barbara County's Avoid the 12 DUI
          enforcement program. One DUI checkpoint was set up on Broadway to
          identify drunk drivers and those without a valid driver's license
          and/or registration. Over 1,053 vehicles were screened and two DUI
          arrests were made. Seven unlicensed drivers had their vehicles
          impounded and two vehicles were towed because their owners were too
          drunk to drive. Santa Maria's Police Chief acknowledged that
          hit-and-run accidents have fallen as a result of the these
          checkpoints.
        </p>
        <p>
          Police in many small but popular cities in California face difficulty
          in controlling through traffic. Santa Maria is a popular small city
          that many people flock to for stay-vacations and to enjoy the wineries
          and great outdoors. Many of the accidents that happen in Santa Maria
          are from non residents who are not familair with the city's streets.
        </p>
        <p>
          If you've been involved in a car accident contact our Santa Maria car
          accident lawyer for a free case evaluation. You may be entitled to
          compensation including medical bills. Call 800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
