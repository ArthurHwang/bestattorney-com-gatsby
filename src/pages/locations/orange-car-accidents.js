// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange Car Accident Lawyers - Car Accident Attorneys"
        pageDescription="Call 949-203-3814 for highest-rated Orange car accident lawyers offering free consultation"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <h2>
          Orange Car Accidents Blamed on "Orange Crush" and Hazardous Roads
        </h2>
        <p>
          Orange, California, in Orange County is a dynamic city with a growing
          population of over 140,000 people that has been burdened with the
          on-going tragedy of car accidents. Some areas in the city of Orange
          are very rural with hillside homes on winding streets. On the other
          hand, Orange is also the home to that nefarious nexus of three major
          freeways -- Interstate 5 (Santa Ana Freeway), State Route 22 and State
          Route 57 -- derisively known as the Orange Crush interchange. Sadly,
          it's been the site of too many major car accidents.
        </p>
        <p>
          As John Bisnar explained, "Named by the Guinness Book of World Records
          as the most complex interchange in the world, the Orange Crush is a
          bumper cars roulette of vehicles weaving and braking to create one of
          Orange County's worst bottlenecks. It provides 34 routes -- including
          on-ramps and off-ramps -- for 629,000 cars a day, traveling in 66
          lanes over 13 bridges."
        </p>
        <p>
          Some of the local Orange neighborhoods have their problems as well.
          Santiago Canyon Road at Kennymead was the site of a terrible car
          accident in 2008. Drivers speed down the hill on Santiago without
          adequate warning signs to alert them of the curve ahead. It also lacks
          a shoulder, so an out-of-control car can easily slip into the run-off
          ditch, resulting a potentially lethal rollover accident. In addition,
          the two yellow warning signs from the cemetery don't warn drivers of
          an oncoming curve. Drivers mistakenly believe that it's just a side
          street coming off Santiago Canyon Road. City Traffic Engineers have
          acknowledged residents' concerns and agreed to upgrade the signage on
          Santiago Canyon with a warning flasher to alert drivers of an
          on-coming curve.
        </p>
        <p>
          The city of Orange has been taking steps to reduce the number of car
          collisions. This includes the creation of a roundabout (often called a
          traffic circle) to help improve traffic flow. Roundabouts are a
          substitute for standard intersections and move traffic without stop
          signs or traffic signals -- just yield signs. While they take some
          getting used to, the U.S. Department of Transportation has endorsed
          the construction of roundabouts at new and existing intersections.
        </p>
        <p>
          In fact, Department statistics reveal that roundabouts are safer than
          ordinary intersections with a 90 percent reduction in{" "}
          <Link to="/wrongful-death">car accident fatalities</Link>, a 76
          percent reduction in car injury crashes and a 40 percent reduction in
          overall car crashes. It's no wonder 150 to 250 roundabouts are built
          annually. The Insurance Institute for Highway Safety likes them as
          well, saying that modern roundabouts are safer and accommodate more
          traffic than ordinary intersections.
        </p>
        <p>
          Call Bisnar Chase - an experienced and reputable Orange Car Accident
          Lawyer for a free consultation at 949-203-3814 or submit a contact
          form above!.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
