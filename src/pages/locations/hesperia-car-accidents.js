// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Hesperia",
      population: 92147,
      totalAccidents: 2453,
      intersection1: "Main St & Topaz Ave",
      intersection1Accidents: 82,
      intersection1Injuries: 45,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 262.7,
      city1Name: "Victorville",
      city1Index: 445.7,
      city2Name: "Apple Valley",
      city2Index: 217.6,
      city3Name: "Hesperia",
      city3Index: 262.7
    },
    locationBox3: {
      intersection2: "C Ave & Main St",
      intersection2Accidents: 117,
      intersection2Injuries: 31,
      intersection2Deaths: 0,
      intersection3: "Mariposa Rd & Main St",
      intersection3Accidents: 89,
      intersection3Injuries: 36,
      intersection3Deaths: 0,
      intersection4: "Esecondido Ave & Main St ",
      intersection4Accidents: 83,
      intersection4Injuries: 24,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hesperia Car Accident Lawyers - Bisnar Chase"
        pageDescription="If you've been injured in a car accident or personal injury case in Hesperia, Contact Bisnar Chase to walk you through getting compensated for your injuries!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Hesperia Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>With a population of over 92,000 people,</strong> Hesperia in
          San Bernardino County, California is the fourth largest city in the
          Palmdale Metropolitan Area. This high desert locale presents motorists
          with many wide-open stretches of highway, and Hesperia car accident
          lawyers will confirm that some people will drive excessively fast,
          resulting in tragic collisions. Adding to the danger are drunk drivers
          who have been responsible for several car crashes in the city.
        </p>
        <p>
          Being injured in a car accident can be a life-changing event - one
          that is difficult to go through when you don't have anyone to guide
          you through to make the correct decisions on your path to recovery.
          Bisnar Chase is an injury victim advocate who will walk you through
          what you need to do to be fairly compensated. We can help you get back
          to normal as soon as possible and we'll be there to answer any
          questions you may have about the personal injury process. Contact us
          to see if you have a case today!
        </p>

        <h2>Hesperia Car Accident Trends</h2>

        <p>
          In the past 5 years, Hesperia has had an average of 950 car accidents
          a year, with about 40% of those resulting in injuries. This concerning
          statistic has been slowly rising and has culminated in 469 injuries
          and 17 deaths in 2014, compared with 375 injuries and 10 deaths in
          2009. Truck accidents show the most change since 2009, dipping down to
          40 in 2011 but rising again to 67 – its highest point in the time
          period.{" "}
        </p>

        <p>
          Many of these accidents are also caused by drunk drivers, with the
          majority unsurprisingly taking place from 7-10pm. These drunk driving
          accidents make up more than 10% of all car accidents in Hesperia,
          reaching a high of 125 incidents in 2013, but this 10% of all car
          accidents accounted for nearly half of all car accident fatalities in
          the past 5 years. This is a tragic reminder of the danger of driving
          drunk. It not only leads to accidents, but to much more dangerous
          accidents. The statistics show that when you’re in Hesperia, you’re 7
          times more likely to be killed in a car accident when alcohol is
          involved.{" "}
        </p>

        <h2>What To Do If You’ve Been Injured In Hesperia?</h2>

        <p>
          Bisnar Chase Provides multiple resources for victims of car accidents
          and personal injury in general. If you’ve been injured, you probably
          want to know if you need a lawyer. If you have experienced a severe
          injury and have a multitude of medical bills to pay, you may have a
          higher valued case. You can read our resources on{" "}
          <Link to="/resources/value-of-your-personal-injury-claim">
            determining the value of your car accident claim
          </Link>{" "}
          and learning{" "}
          <Link to="/resources/hiring-an-injury-lawyer">
            how to hire a personal injury attorney
          </Link>{" "}
          once you’ve decided that you need one. You may also have questions
          about the personal injury case process, in which case you can view our{" "}
          <Link to="/faqs">Personal Injury FAQs.</Link>
        </p>

        <h2>Fighting for Our Clients</h2>

        <p>
          There are a lot of things to think about when you’re filing a claim
          with your auto insurance to be reimbursed for your injuries, property
          damage, lost wages, and general damages. It can be difficult for
          someone who doesn’t know the ins and the outs of how insurance
          companies work. Policyholders will have to know what documents to
          prepare and know how to negotiate their case over the phone. This is
          where Bisnar Chase comes in. Since we’ve had experience dealing with
          insurance companies from Hesperia car accidents and all over Southern
          California, we’re able to handle car accident cases smoothly while
          eliminating the usual stress that comes with dealing with insurance
          companies while injured. We want to make this process as easy as
          possible for you, while making sure that you get what you deserve. If
          you’re in Hesperia and you need a personal injury lawyer, contact us
          to see if you have a case!
        </p>

        <p>
          <strong>
            Related: We serve locations throughout San Bernardino County.
          </strong>{" "}
          Learn more about our{" "}
          <Link to="/san-bernardino">San Bernardino Practice Areas.</Link>
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
