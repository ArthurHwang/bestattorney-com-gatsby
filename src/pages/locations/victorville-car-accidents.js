// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Victorville",
      population: 121096,
      totalAccidents: 2747,
      intersection1: "Bear Valley Rd & Armarosa Rd",
      intersection1Accidents: 150,
      intersection1Injuries: 26,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 445.7,
      city1Name: "Adelanto",
      city1Index: 354.3,
      city2Name: "Apple Valley",
      city2Index: 217.6,
      city3Name: "Hesperia",
      city3Index: 262.7,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Bear Valley Rd & Industrial Blvd",
      intersection2Accidents: 90,
      intersection2Injuries: 33,
      intersection2Deaths: 1,
      intersection3: "Hesperia Rd & Bear Valley Rd",
      intersection3Accidents: 89,
      intersection3Injuries: 25,
      intersection3Deaths: 0,
      intersection4: "Bear Valley Rd & Mariposa Rd",
      intersection4Accidents: 88,
      intersection4Injuries: 22,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Victorville Car Accident Lawyers - Experienced Trial Attorneys"
        pageDescription="Let the experienced Victorville car accident lawyers help you to fair compensation. Call 800-561-4887 for a free consultation with a trial attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Victorville Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          Trust the experienced Victorville{" "}
          <Link to="/car-accidents">car accident lawyers</Link> of Bisnar Chase.
          Let us deal with the headache of the insurance company and defendant.
          Our team of top rated trial lawyers has been serving Victorville for
          over 35 years. We will advance all costs of your case and if we don't
          win, you don't pay.
        </p>
        <h2>Before You Talk To The Insurance Company</h2>
        <p>
          Bisnar Chase has dedicated car accident lawyers with 30 years of
          experience. We understand what you are going through. We have helped
          hundreds of car accident victims recover money. The best advice we can
          give you is to seek legal help before talking to an insurance company.
        </p>
        <h2> Victorville Dangerous Driver Enforcement Program</h2>
        <p>
          Like most cities in San Bernardino County, Victorville has seen a rise
          in car collisions.
        </p>
        <p>
          Determined to bring down the number of car collisions, the Victorville
          Police Department's Traffic Enforcement Division implemented the
          Dangerous Driver Enforcement program. Designed to boost enforcement in
          specific areas of the city, it also raises public awareness and
          education regarding driver safety.
        </p>

        <p>
          Approximately two years ago, the Dangerous Driver Enforcement program
          focused on four intersections along Palmdale Road. The day-long
          program addressed the problems at Park, Amargosa, Mariposa and
          Kentwood roads. These were intersections where motorists frequently
          executed dangerous turns, ran red lights or blocked traffic, causing
          traffic jams along Palmdale.
        </p>
        <p>
          Police cited 143 motorists for excessive speed, turning in the path of
          oncoming cars, following too closely, and changing lanes in an unsafe
          manner. Several drivers ignored signs that forbid right turns from the
          Target parking lot at Kentwood onto Palmdale road.{" "}
        </p>
        <p>
          One month later, another Dangerous Driver Enforcement program cited
          123 motorists for failing to stop for red lights, speeding, and not
          "buckling up" for safety with seat belts. Four drivers were ticketed
          for driving with a suspended license, five others were arrested for
          different offenses, and six cars were impounded. In one month,
          deputies issued over 1,600 citations for various infractions.{" "}
        </p>
        <p>
          The Dangerous Driver Enforcement program was borne out of citizen
          concerns over the reckless and often dangerous driving incidents they
          witnessed throughout Victorville. The program did have an impact,
          reducing car collisions by nearly 10% in a single year.{" "}
        </p>
        <p>
          "Programs like this are created when the public gets involved and
          demands that city officials take action to reduce car accidents," said
          John Bisnar. "Where appropriate, these programs can serve as a model
          for other cities throughout California."
        </p>
        <h2>Experienced Car Accident Lawyers</h2>
        <p>
          An experienced Victorville car accident lawyer from our law firm will
          provide a complete analysis of your case free of charge. We will help
          you decide how to move forward. Insurance companies want to settle for
          as little as they possibly can and they do hire the best lawyers to
          help them achieve this.
        </p>
        <p>
          If you want the best to go up against the best, you will need a car
          accident attorney that is seasoned with experience. At Bisnar Chase we
          have that experience. In addition we are a contingent fee based law
          firm. You pay when we win. This makes it easy for you.
        </p>
        <p>
          If you have been injured in a car accident in Victorville, please
          contact an experienced Victorville car accident lawyer for a free
          consultation with our law firm.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
