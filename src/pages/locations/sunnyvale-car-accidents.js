// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Sunnyvale",
      population: 147559,
      totalAccidents: 2342,
      intersection1: "Lawrence Expwy & Kifer Rd",
      intersection1Accidents: 128,
      intersection1Injuries: 61,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 125.0,
      city1Name: "Santa Clara",
      city1Index: 157.0,
      city2Name: "Mountain View",
      city2Index: 161.4,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Arques Av & Lawrence Expwy",
      intersection2Accidents: 74,
      intersection2Injuries: 29,
      intersection2Deaths: 0,
      intersection3: "Wolfe Rd & Central Expwy",
      intersection3Accidents: 43,
      intersection3Injuries: 31,
      intersection3Deaths: 0,
      intersection4: "Sunnyvale Saratoga Rd & Fremont Ave",
      intersection4Accidents: 43,
      intersection4Injuries: 21,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sunnyvale Car Accident Attorney - Bisnar Chase"
        pageDescription="Call for an award-winning Sunnyvale car accident attorney. Get a free legal case review with an experienced auto accident lawyer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sunnyvale Car Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          Contact our Sunnyvale car accident lawyers for a free consultation. No
          fee if we don't win. Let our experienced personal injury attorneys
          make sense of your accident. We will deal with the insurance companies
          and defendant so you don't have to.
        </p>
        <h2>Sunnyvale Accidents Blamed on Problem Roads and Intersections</h2>
        <p>
          Reluctant to lose its old farmer/orchard roots, yet eager to
          modernize, Sunnyvale is a family-oriented city burdened with traffic
          problems and sadly, <Link to="/car-accidents">auto accidents</Link>.
        </p>
        <p>
          According to residents, many car accidents and "near misses" have been
          caused by problem intersections.
        </p>
        <p>
          "Intersections that are confusing and/or dangerous to motorists
          require prompt attention." "Neglecting these areas simply creates the
          opportunity for more motor vehicle collisions."
        </p>

        <p>
          Red-light runners share the blame for a growing number of accidents in
          Sunnyvale as well that made up 7 percent of the 2,081 automobile
          accidents and two fatalities in the city.
        </p>
        <p>
          Fighting back, Sunnyvale chose the lower-tech approach to nabbing
          red-light violators. Instead of expensive red-light camera systems,
          the city opted for low-cost "Rat Boxes." These were affixed to signal
          boxes at 36 heavily traveled intersections throughout the city. Police
          officers can now catch red-light runners from the safety of the other
          side of the intersection. No more dangerous maneuvering through
          red-light cross traffic to engage the pursuit.
        </p>
        <p>
          Continuing its pro-active efforts to cut down the number of motor
          vehicle accidents, Sunnyvale targeted drunk drivers. In addition,
          police have conducted DUI checkpoints as a means to decrease drunk
          driving.
        </p>
        <p>
          Properly conducted, DUI checkpoints are a powerful tool that can
          reduce motor vehicle accidents.
        </p>
        <h2>Who is Responsible for a Dangerous Intersection or Road?</h2>
        <p>
          That depends. If the design of the intersection or roadway is flawed
          making it a hazard to motorists the city or state could be held liable
          for a car accident.
        </p>
        <p>
          This does not mean there is not a negligent driver involved. A
          motorist that causes an accident by driving wreckless, driving drunk,
          speeding or disobeying the traffic laws is creating a traffic hazard
          as well.
        </p>
        <h2>What Should You Do After a Car Accident?</h2>
        <ul>
          <li>Get medical help right away.</li>
          <li>File a police report.</li>
          <li>
            Get names, addresses, phone numbers, license plate numbers of all
            drivers involved in the accident.
          </li>
          <li>
            Take photos of damage to vehicles and take photos of injuries.
          </li>
          <li>Contact a Sunnyvale car accident attorney.</li>
        </ul>
        <p>
          Do not talk to any insurance company before you talk to an attorney.
          They may try to convince you to settle for whatever they can get away
          with giving you which is usually a very small amount of money that
          barely pays for the damage done to your car.
        </p>
        <p>
          For more information, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar, available at
          www.Amazon.com. Please contact our Sunnyvale car accident lawyers if
          you need help with your claim. We provide free legal consultations. No
          win, no fee lawyers since 1978.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
