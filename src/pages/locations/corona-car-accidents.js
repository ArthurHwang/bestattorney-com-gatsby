// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Corona",
      population: 159503,
      totalAccidents: 4742,
      intersection1: "Rimpau Ave & Magnolia Ave",
      intersection1Accidents: 48,
      intersection1Injuries: 29,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 157.1,
      city1Name: "Norco",
      city1Index: 164.9,
      city2Name: "Eastvale",
      city2Index: 27.8,
      city3Name: "Ontario",
      city3Index: 236.9,
      city4Name: "Pomona",
      city4Index: 345.4
    },
    locationBox3: {
      intersection2: "Ontario Ave & California Ave",
      intersection2Accidents: 38,
      intersection2Injuries: 22,
      intersection2Deaths: 0,
      intersection3: "6th St & Paseo Grande",
      intersection3Accidents: 36,
      intersection3Injuries: 18,
      intersection3Deaths: 0,
      intersection4: "Lincoln Ave & 6th St",
      intersection4Accidents: 34,
      intersection4Injuries: 8,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Corona Car Accident Lawyer - Riverside, California Auto Accident Attorney"
        pageDescription="Call 949-203-3814 for highest-rated product liability lawyers, serving Corona, Los Angeles, Orange County & San Francisco. No win, no-fee lawyers.  Free no-obligation legal consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Corona Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Any experienced Corona accident injury lawyer</strong> knows
          that people who suffer a car accident are typically overwhelmed by the
          endless things they have to do - long after their car crash. Corona
          drivers encounter the risks of getting into a car collision whenever
          they leave the driveway. Corona has recently become a hot real estate
          location, which means that the population density is increasing
          quickly, leading to more drivers on the road. This is especially true
          on the dreaded 91 freeway, where accidents are commonplace.
        </p>
        <p>
          If you've been injured in an accident or any other incident in Corona,
          let Bisnar Chase see you through the process of getting compensation
          for your injuries and damages. We're here to support you and make sure
          that you feel at peace when you're going through this difficult time
          in your life. If you want to see if you have a case, fill out our form
          or call us today!
        </p>
        <h2>Pain, Grief and Confusion of Car Accident May be Overwhelming</h2>
        <p>
          With these risks in mind, experienced{" "}
          <Link to="/car-accidents">Corona car accident attorneys</Link> advise
          anyone who has been injured in a car accident to try to restore order
          in their lives. There's no doubt that a car crash can cause a lot of{" "}
          <Link to="/head-injury/traumatic-brain-injury">
            pain and emotional trauma
          </Link>
          . Along with the grief and confusion, victims face doctors'
          appointments, phone calls and meetings with insurance companies. They
          must also deal with police, handle car repairs, and make special
          arrangements with employers and schools.
        </p>
        <h2>Keep Written Records of Car Accident Injuries</h2>
        <p>
          Faced with these seemingly endless disruptions, car accident lawyers
          urge you to try and maintain accurate records and write down your
          observations of your injuries and losses. Skilled Corona car collision
          lawyers realize that while written records alone can't restore your
          health, yet they provide medical practitioners with critically
          important information that can support and enhance the quality of your
          treatment. And, just as important, accurate written records can
          substantiate the degree of your losses and damages. This information
          can be crucial as you negotiate a just settlement with insurance
          companies. Their agenda, after all, is to limit your settlement amount
          to the lowest possible sum.
        </p>
        <h2>Beware of Dos and Don'ts in Dealing with Insurance Companies</h2>
        <p>
          Should you get into a car accident, there are certain "dos and don'ts"
          you must be aware of--before you speak to an insurance claims adjuster
          or sign any papers. Some experienced Corona car crash lawyers will
          provide free, no pressure, consultations to inform you of your rights
          and warn you of mistakes you should avoid.
        </p>
        <p>
          For more information, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
