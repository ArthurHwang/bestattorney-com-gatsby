// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Juan Capistrano",
      population: 35852,
      totalAccidents: 1166,
      intersection1: "Del Obispo & Ortega Hwy",
      intersection1Accidents: 79,
      intersection1Injuries: 18,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 107.5,
      city1Name: "Dana Point",
      city1Index: 145.4,
      city2Name: "Laguna Niguel",
      city2Index: 73.2,
      city3Name: "San Clemente",
      city3Index: 92.6,
      city4Name: "Aliso Viejo",
      city4Index: 47.5
    },
    locationBox3: {
      intersection2: "Camino Capistrano & Del Obispo St",
      intersection2Accidents: 56,
      intersection2Injuries: 22,
      intersection2Deaths: 0,
      intersection3: "Stonehill Dr & Camino Capistrano",
      intersection3Accidents: 33,
      intersection3Injuries: 14,
      intersection3Deaths: 0,
      intersection4: "Camino Las Ramblas & Camino De Vista ",
      intersection4Accidents: 16,
      intersection4Injuries: 14,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Juan Capistrano Car Accident Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 for San Juan Capistrano car accident lawyers.  No win, no-fee.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Juan Capistrano Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Contact the San Juan Capistrano car accident lawyers for a free no
          obligation consultation. We advance all case costs. If your case does
          not win, you don't pay. We are committed to excellence in client care
          and have been serving Orange County residents for over three decades.
        </p>

        <p>
          A number of San Juan Capistrano{" "}
          <Link to="/car-accidents"> car accident attorneys </Link> have
          indicated that car crashes often occur in ocean-close communities.
          With a population of nearly 37,000, San Juan Capistrano in Southern
          Orange County, California has implemented a number of proactive
          approaches to reduce car collisions.
        </p>
        <h2>Car Collisions in San Juan Capistrano Linked to Traffic Issues</h2>
        <p>
          With a renewed effort to reduce the number of car collisions in San
          Juan Capistrano, the city noted a number of issues. Traffic backing up
          at the Camino Capistrano exit of the southbound I-5 freeway was
          creating a dangerous road condition. The City also looked into adding
          an extension of San Juan Creek Road to La Pata to reduce traffic on
          Ortega Highway. City commissioners had reservations about the
          extension adding excessive traffic to the existing quiet street and
          its impact on the safety of Ambuehl Elementary School children.
          Lastly, an extension of Las Ramblas to La Pata was also considered to
          help reduce traffic on Ortega Highway.
        </p>
        <p>
          Another effort to reduce car collisions in San Juan Capistrano was the
          implementation of photo enforced signal lights at accident prone
          locations. Cameras were set up at Camino Capistrano and I-5 South Exit
          off ramp, Camino Del Avion and Del Obispo, and Del Obispo and Ortega
          Highway.
        </p>
        <h2>Roving DUI Patrols to Reduce San Juan Capistrano Car Accidents</h2>
        <p>
          Dovetailing these efforts to further cut down on San Juan Capistrano's
          car collision rate are DUI and Driver's License Checkpoints. Roving
          patrols were conducted March 21, 2009. The checkpoints helped identify
          and/or remove impaired drivers from San Juan Capistrano streets.
          Roving DUI patrols and checkpoints also help educate the public on the
          dangers of drunk driving, and to encourage the use of sober designated
          drivers for those who have had "one too many."
        </p>
        <p>
          San Juan Capistrano car crash lawyers have repeatedly acknowledged
          that a city's proactive steps to remove impaired drivers from San Juan
          Capistrano streets can save many lives and prevent needles injuries.
          They will tell you that a number of their clients have suffered great
          pain due to negligent or irresponsible motorists who got the behind
          wheel while they were drunk.
        </p>
        <p align="center">
          Immediately call an experienced and reputable San Juan Capistrano Car
          Accident Lawyers for a free consultation at 949-203-3814.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
