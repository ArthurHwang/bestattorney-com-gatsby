// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sacramento Nursing Home Fails to Provide Adequate Care"
        pageDescription="Were you or your loved one injured in a Sacramento nursing home? Call 949-203-3814 for a free consultation with an attorney. Years of results & happy clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sacramento Nursing Home Abuse Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have a loved one who has been abused in a California nursing
          home, contact the Sacramento{" "}
          <Link to="/nursing-home-abuse">Nursing Home Abuse Lawyers</Link>.
          Three decades of helping family members hold nursing home abusers
          responsible. An experienced nursing home abuse attorney can help you
          seek rightful compensation.
        </p>
        <h2>Signs of Deficient Elder Care</h2>
        <p>
          The team of inspectors from Operation Guardians found many signs of
          deficient care. The following observations were listed in the reports
          made by inspectors:
        </p>
        <ul>
          <li>
            Inspectors reviewed the care that was given to 13 current or former
            residents of the facility. They found examples of deficient care in
            almost every case.
          </li>
          <li>
            Pressure sores were a common issue for many of the patients.
            Pressure sores are often the direct result of a failure by workers
            to properly move bedridden patients. In one case, a patient had six
            pressure sores over a three-month period. The pressure sores were
            "infected, and cellulites surrounded the surface areas of the
            sores." This is a sign that the sore were not properly treated.
            Sadly, this patient died the day after inspectors requested that she
            receive treatment at an acute care facility.
          </li>
          <li>
            The facility failed to receive consent from the proper individuals.
            One patient was receiving treatment as requested by a boyfriend who
            did not have the legal authority to provide such consent.
          </li>
          <li>
            Sexual assault was also reported at the facility. One of the
            residents was assaulted by another resident. The victim suffered a
            stroke recently and was unable to communicate with the staff. The
            victim was not given a physical examination and did not receive
            treatment for the assault.
          </li>
          <li>
            One patient regularly left the facility without consent or
            supervision. An inspector actually observed the patient leaving
            while the staff remained unaware of the potentially dangerous
            situation. Additionally, her Wanderguard alert system did not
            activate when she left. She has a conservator, which means that she
            lacks the capacity for self-preservation and is at a high risk for
            injury or death while outside the facility.
          </li>
          <li>
            The physical therapy staff was also not providing adequate care
            during the inspection. Officials observed the physical therapists
            sitting at a table and talking "while billing for their services."
          </li>
          <li>
            Many of the patients were younger individuals. In many cases, there
            was a failure to properly plan their discharge.
          </li>
          <li>
            Some of the elderly patients were not receiving adequate end-of-life
            care either. Four of the patients' records lacked evidence that
            hospice care was considered or even offered. In some cases, the
            residents were specifically admitted for terminal care and were
            still not offered hospice care.
          </li>
          <li>
            There was a serious risk of falling injuries at the facility. Two of
            the residents had already suffered numerous falls because of
            deficient care. It was common for their falls to occur while they
            were unsupervised resulting in a prolonged period of suffering
            before being discovered. Inspectors found "gross failures" to
            prevent falling incidents at their facility.
          </li>
          <li>
            The facility failed to properly update their infection control
            report binder for two months.
          </li>
          <li>
            Several rooms did not have the names of the residents on the doors.
            This is a potential safety issue.
          </li>
          <li>
            The number of staff members at the facility failed to meet the
            minimum state requirements on three of the six days reviewed. All
            care facilities in California must have at least 3.2 nursing hours
            per resident per day.
          </li>
        </ul>
        <h2>Rights of Victims</h2>
        <p>
          Victims of nursing home negligence and abuse may pursue financial
          compensation for their losses and at the same time hold the at-fault
          party accountable. If you suspect that your loved one has received
          inadequate care or is being abused or neglected at a nursing facility,
          please do not hesitate to notify the authorities and to contact an
          experienced nursing home abuse lawyer. An experienced Sacramento
          nursing home abuse attorneys at our law firm can help the family
          protect the injured victim while holding the facility accountable for
          its wrongdoing. Please contact us for a free, comprehensive and
          confidential consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
