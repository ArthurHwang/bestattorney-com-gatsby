// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Rialto",
      population: 101910,
      totalAccidents: 2919,
      intersection1: "Riverside Ave & Baseline Rd",
      intersection1Accidents: 58,
      intersection1Injuries: 80,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 298.6,
      city1Name: "Fontana",
      city1Index: 223.5,
      city2Name: "Colton",
      city2Index: 302.7,
      city3Name: "San Bernardino",
      city3Index: 555.0,
      city4Name: "Grand Terrace",
      city4Index: 224.8
    },
    locationBox3: {
      intersection2: "Foothill Blvd & Riverside Ave",
      intersection2Accidents: 29,
      intersection2Injuries: 47,
      intersection2Deaths: 2,
      intersection3: "Riverside Ave & Merrill Ave",
      intersection3Accidents: 36,
      intersection3Injuries: 41,
      intersection3Deaths: 0,
      intersection4: "Riverside Ave & Valley Blvd",
      intersection4Accidents: 35,
      intersection4Injuries: 40,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rialto Car Accident Lawyers - Bisnar Chase"
        pageDescription="If you're injured in a car accident, you need an experienced Rialto Car Accident Lawyer to help you fight insurance companies so you can be compensated fairly. Call Bisnar Chase today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rialto Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            If you get into a car crash in Rialto and are severely injured,
          </strong>{" "}
          your primary goal should be to get the necessary medical attention you
          need. A <Link to="/car-accidents">skilled car accident lawyer </Link>{" "}
          will advise you, however, that signing a medical lien to take care of
          your medical expenses is typically the last thing you want to do.
          Bisnar Chase can help you get the compensation you need to deal with
          medical bills in addition to being compensated fairly for your
          property damage and general damages. Contact us to see if you have a
          case today!
        </p>
        <h2>
          Consider Other Payment Options Before Agreeing to a Medical Lien
        </h2>
        <p>
          Some of the most experienced Rialto car collision lawyers realize that
          there are far more preferred methods to settle your medical bills than
          signing a medical lien. "Sign a lien agreement with your doctor,
          chiropractor or therapist and your lawyer will have to pay your
          medical expenses directly from your claim settlement amount," notes
          John Bisnar. "Better to pay your medical expenses using your own
          medical insurance; that way, the insurance company will have some
          oversight on the services and the amounts billed to you by the medical
          provider. With a medical lien, there's no oversight; you're literally
          at the mercy of the medical provider and you'll be billed whatever
          they deem "typical." Usually, the services offered through medical
          liens are far more expensive.
        </p>
        <p>
          Then there's the other big drawback with medical liens: Should your
          settlement sum fall short of your medical bills--or worse yet, if you
          get no settlement at all--you would still be liable for the full
          amount of the medical services attached to the lien. Your medical
          provider can promptly demand full payment on all amounts they billed
          you. Conversely, if you don't sign a lien, you could keep a larger
          portion of your settlement amount.
        </p>
        <h2>Seek the Advice of a Car Collision Lawyer</h2>
        <p>
          A trustworthy <strong>Rialto car crash lawyer</strong> can provide you
          with important advice on medical liens and other car injury settlement
          matters. The finest lawyers offer no charge, no pressure
          consultations. They can guide you, and they have the skills and
          experience to make sure you are fairly compensated for your losses and
          medical expenses. Call 800-561-4887 today.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
