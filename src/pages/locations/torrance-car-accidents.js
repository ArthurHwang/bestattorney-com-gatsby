// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Torrance",
      population: 147478,
      totalAccidents: 4751,
      intersection1: " Crenshaw & 182nd St",
      intersection1Accidents: 96,
      intersection1Injuries: 59,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 162.4,
      city1Name: "Hawthorne",
      city1Index: 374.3,
      city2Name: "Long Beach",
      city2Index: 350.9,
      city3Name: "Los Angeles",
      city3Index: 274.6,
      city4Name: "Carson",
      city4Index: 313.1
    },
    locationBox3: {
      intersection2: "Hawthorne & Torrance",
      intersection2Accidents: 94,
      intersection2Injuries: 47,
      intersection2Deaths: 1,
      intersection3: "Artesia & Prairie",
      intersection3Accidents: 70,
      intersection3Injuries: 26,
      intersection3Deaths: 0,
      intersection4: "Crenshaw & Artesia",
      intersection4Accidents: 72,
      intersection4Injuries: 33,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Torrance Car Accident Attorneys - Bisnar Chase 323-238-4683"
        pageDescription="Contact the Torrance car accident lawyers of Bisnar Chase with a 96% success rate and over $500 Million recovered. Serving Los Angeles since 1978. Call 323-238-4683"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Torrance Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Bisnar Chase Personal Injury Attorneys have been serving car accident
          victims for over three decades. Our experienced Torrance car accident
          lawyers understand the ins and outs of the insurance game and will
          fight for your right to fair compensation. No fee if we don't recover
          for you. We handle catastrophic and serious car accidents throughout
          Los Angeles County. We have represented over 12,000 clients with over
          $500 Million in wins. Our 96% success rate speaks for itself. Contact
          our top rated attorneys today for a free consultation.{" "}
        </p>
        <p>
          We represent plaintiff injuries for car accidents including
          catastrophic and fatal cases involving{" "}
          <Link
            to="/locations/torrance-hit-and-run-accidents"
            title="Torrance hit and run lawyers"
          >
            hit and run injuries
          </Link>
          . Our team of Torrance car accident attorneys are top rated trial
          lawyers with an eye for details and a professional and aggressive
          nature. We are committed to representing our clients with passion,
          trust and results. Our firm will advance all costs during your case so
          that you don't have to. At the end of your case if we don't win, you
          will not pay.
        </p>
        <h2>Some of our car accident case results</h2>
        <ul>
          <li>$9,800,000 - Motor vehicle accident</li>
          <li>$8,500,000 - Motor vehicle accident - wrongful death</li>
          <li>$7,998,073 - Motor vehicle accident</li>
          <li>$3,000,000 - Motor vehicle accident</li>
        </ul>
        <p>
          While it's not necessary to retain a car accident lawyer for every car
          accident, you owe it to yourself and your family to at least talk to
          one before you fill out insurance forms, talk to claims adjusters and
          blindly follow insurance company procedures. Especially when there are
          moderate injuries involved.
        </p>

        <h2>Insurance Company Games Can Cost You</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/torrance-car-accident-lawyer.jpg"
              alt="Torrance Car Accident Lawyer"
              className="imgright-fixed"
            />
          </LazyLoad>
          If you get into a car accident, you'll probably call your insurance
          company first, which, as the best Torrance car accident lawyers say,
          is where the "games" begin. Your insurance claims adjuster will ask
          you to fill out all kinds of forms and sign several releases.
        </p>
        <p>
          He may also ask you to submit a recorded statement, which may sound
          simple and straightforward but can be fraught with legal landmines.
          After complying with all these requests, claims adjusters often do a
          disappearing act and are hard to reach.
        </p>
        <p>
          The reasons for this are twofold. First, your adjuster has all the
          information he needs, so his "amiable" and "concerned" manner is no
          longer required. Second, the longer he takes to pay your compensation,
          the more money he saves the insurance company. Consider this:
          according to the National Traffic Safety Administration, 2.5 million
          Americans are injured by traffic accidents every year.
        </p>
        <p>
          If your average claim is $2,000, the total value of those claims would
          be $5 billion. If insurance companies can delay payment on these
          claims for just thirty days, the interest is a whopping $500 Million!
          Any skilled car crash lawyer will admit that delay means dollars to an
          insurance company.
        </p>
        <h2>Getting Immediate Legal Help</h2>
        <p>
          No matter how nice insurance companies present themselves, their
          endgame is driven by profits. If they say all is well and there's no
          need to see a lawyer, you should do the opposite and at least{" "}
          <Link to="/los-angeles/contact">
            call a local car accident attorney
          </Link>{" "}
          -- most offer free initial consultations and the call will be well
          worth your while. Of course, not every injury claim needs a lawyer,
          but if you've been hurt in a motor vehicle accident, you should do
          what you can to protect yourself and your family. And that means
          consulting an experienced Torrance car accident attorney with
          experience in local courts and rules.
        </p>
        <p>
          {" "}
          Please contact one of our top rated Torrance car accident lawyers at
          Bisnar Chase today for a free no-obligation case evaluation. Call
          323-238-4683 today.{" "}
        </p>

        <h3>Los Angeles Office</h3>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
