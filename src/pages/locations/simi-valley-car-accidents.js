// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Simi Valley Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 to speak with a Simi Valley car accident lawyer. Bisnar Chase offers a free consultation to California clients and a no win no fee promise."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Simi Valley Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Ask a trusted Simi Valley{" "}
          <Link to="/los-angeles">car accident lawyer</Link> how to negotiate a
          just and equitable settlement after a car accident and the answers may
          surprise you. What's more, you'll be no less surprised by the sheer
          number of car accidents that happen every year in Simi Valley,
          California.
        </p>
        <h2>Negotiate or Settle?</h2>
        <p>
          If you get into a car accident, car accident lawyers will tell you
          that you'll be promptly contacted by an insurance adjuster. This
          person will have two goals: to settle fast, and to offer you the
          lowest possible compensation. Depending on your situation and level of
          patience, you can either accept the proposed settlement or negotiate
          for more. If you opt for the latter, you'll have to negotiate--a
          custom and practice the insurance industry has honed to perfection.
          Simi Valley car collision lawyers realize that insurance adjusters
          expect you to say no to their first offer, which means the burden to
          argue your case for more rests squarely on your shoulders.
        </p>
        <h2>Before Negotiating for More, Ask Yourself Five Questions</h2>
        <p>
          If you want to increase your settlement amount, you'll need to answer
          five important questions. What is your settlement goal? What is the
          strength (the evidence) of your claim? What are similar claims
          generally resolved for? What is your settlement bottom line? And what
          are your alternatives if you don't settle? Answering these questions
          may prove difficult, but there are people who can help you.
        </p>
        <h2>Consult a Accident Lawyer for Answers</h2>
        <p>
          If you have suffered a car crash injury, some Simi Valley car accident
          lawyers offer free, no pressure, consultations to answer many
          questions on how to negotiate a higher settlement.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
