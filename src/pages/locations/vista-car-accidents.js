// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Vista",
      population: 96929,
      totalAccidents: 2417,
      intersection1: "Hacienda Dr & S. Melrose Dr",
      intersection1Accidents: 73,
      intersection1Injuries: 26,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 247.8,
      city1Name: "Oceanside",
      city1Index: 248.4,
      city2Name: "San Marcos",
      city2Index: 175.5,
      city3Name: "Carlsbad",
      city3Index: 155.2,
      city4Name: "Encinitas",
      city4Index: 150.3
    },
    locationBox3: {
      intersection2: "N. Melrose Dr & Olive Ave",
      intersection2Accidents: 53,
      intersection2Injuries: 44,
      intersection2Deaths: 0,
      intersection3: "W. Vista Way & N. Melrose Dr",
      intersection3Accidents: 65,
      intersection3Injuries: 48,
      intersection3Deaths: 0,
      intersection4: "Vista Village Dr & N. Santa Fe Ave",
      intersection4Accidents: 58,
      intersection4Injuries: 34,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Vista Car Accident Lawyer - Auto Accident Attorney"
        pageDescription="Call 949-203-3814 for an experienced Vista car accident lawyers. Free consultations. 35 years in business. No win, no fee and we will come to you."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Vista Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Representing Vista residents since 1978. The car accident lawyers of
          Bisnar Chase have been helping Vista car accident victims for over
          three decades. With over $500 Million in wins, our clients know that
          we will fight for them. If you have questions about your legal rights
          after a no fault car accident contact us today.
        </p>
        <b>Trust. Passion. Results.</b>
        <ul>
          <li>96% success rate</li>
          <li> Over 12,000 clients</li>
          <li> Top rated trial lawyers</li>
        </ul>
        <p>
          The Vista car accident lawyers at Bisnar Chase have handled numerous
          accident claims that were the result of rear-end accidents at
          intersections.
        </p>
        <p>
          As reported by the National Highway Safety Traffic Administration,
          nearly half of all car collisions happen at intersections. Tragically,
          these crashes tend to be T-bone collisions, which usually cause more
          severe injuries than front or rear-end car collisions. The reason for
          this is that side car collisions offer fewer protections for
          occupants. What's more, these car accidents are more severe because
          red-light runners often speed up as they try to outrun the red light.{" "}
        </p>

        <h2>Choosing A Top Notch Vista Car Accident Lawyer</h2>
        <p>
          When choosing a car accident lawyer to represent your case, Bisnar
          Chase stands out above the competition. Here is why:
        </p>
        <ul>
          <li>
            We want our clients to be comfortable in knowing that we will do
            everything in our power to get them what they deserve. We also want
            them to know that we will keep them informed of everything that is
            happening with their case every step of the way.
          </li>
          <li>
            Trust - Passion - Results. We have been in business since 1978 and
            we have recovered millions of dollars in settlements. We are
            experienced, passionate and dilligent.
          </li>
          <li>
            At Bisnar Chase we understand how stressful and emotionally draining
            a car accident can be, therefore we do not want you to stress out
            about money. We have a{" "}
            <Link to="/about-us/no-fee-guarantee-lawyer">
              no win, no fee guarantee
            </Link>
            . If we do not win your case, you do not pay. We can do this because
            we are very confident that we will win your case. We have a 97.8%
            success rate.
          </li>
          <li>
            We realize that location is important to a car accident victim. We
            will not make you drive to us. Choosing the best law firm is more
            important than choosing the closest one. We will send our leading
            car accident lawyer to meet with you and complete a free legal case
            evaluation. At that point we will help you decide what steps to take
            next, even if you choose to go elsewhere.
          </li>
        </ul>

        <div style={{ marginBottom: "2rem" }} className="featurebox">
          "I was originally referred to Bisnar Chase by my uncle and have used
          them for two cases I was involved in. They really worked hard for a
          higher payout in my case and kept me informed along the way. I even
          received an extra check by first className mail that I didnt expect.
          Exceptional service!" E.Shaffer.
        </div>

        <h3>Red-Light Cameras Go Up to Bring Down Vista Car Accidents</h3>

        <p>
          With a population of nearly 96,000, Vista has suffered a number of
          tragic car crashes, often caused by red-light scofflaws. To hold down
          the number of car accidents, Vista installed red-light cameras at many
          major intersections throughout the city.
        </p>

        <p>
          Red-light cameras have reduced car accidents at troublesome
          intersections in Vista and the cameras do act as a deterrent to
          would-be red-light violators.{" "}
        </p>
        <p>
          If you have been injured in an auto accident in Vista, a qualified
          auto accident attorney is an essential member of your recovery team.
          Please call us for a free consultation.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
