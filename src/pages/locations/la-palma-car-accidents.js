// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "La Palma",
      population: 15892,
      totalAccidents: 407,
      intersection1: "La Palma Ave & Walker St",
      intersection1Accidents: 66,
      intersection1Injuries: 48,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 131.2,
      city1Name: "Cerritos",
      city1Index: 232.5,
      city2Name: "Cypress",
      city2Index: 126.8,
      city3Name: "Hawaiian Gardens",
      city3Index: 198.4,
      city4Name: "Buena Park",
      city4Index: 220.5
    },
    locationBox3: {
      intersection2: "Orangethorpe & Walker St",
      intersection2Accidents: 52,
      intersection2Injuries: 22,
      intersection2Deaths: 0,
      intersection3: "Crescent Ave & Moody St",
      intersection3Accidents: 40,
      intersection3Injuries: 21,
      intersection3Deaths: 1,
      intersection4: "Moody St & La Palma Ave",
      intersection4Accidents: 40,
      intersection4Injuries: 21,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Palma Car Accident Lawyer - Bisnar Chase"
        pageDescription="Bisnar Chase represents La Palma car accident and injury victims "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Palma Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Although La Palma is a small city</strong> with a total area
          of less than 2 square miles, it still has an annual ratio of about 1
          car accident for every 100 people, just a little bit less than nearby
          cities like Buena Park. In the past 5 years, the average car accident
          injuries was about 80 every year, so if you've been injured in a car
          crash, you're not alone. Bisnar Chase has experience representing
          injured victims against negligent parties in car accidents and other
          personal injury cases. We'll fight for you to get the compensation you
          deserve! Contact us and see if you have a case!
        </p>
        <p>
          An experienced{" "}
          <Link to="/car-accidents">La Palma injury attorney</Link> will tell
          you that if you are injured in a car crash in La Palma, you should
          consult a lawyer who is familiar with La Palma traffic and its city
          accident statistics.
        </p>
        <p>
          As an example, smart La Palma lawyers know that in 2006, 63 people
          were injured in La Palma car crashes. Specifically, city car accidents
          resulted in injuries to two pedestrians, five bicyclists and two
          motorcyclists. <Link to="/dui">Drunk drivers</Link> were also
          responsible for a number of serious car collisions--including eight
          injuries.
        </p>
        <h2>The How and Why of La Palma Car Accidents</h2>
        <p>
          "The most skilled La Palma car accident lawyers use local traffic
          hazards and accident statistics to help evaluate why and how an
          accident occurred," noted John Bisnar. "If a client were injured in a
          car collision along heavily traveled La Palma Avenue--which extends
          east of Weir Canyon Road to Lakewood Drive--experienced La Palma
          lawyers would try to determine if any traffic problems exist that
          might have caused the accident. They would search for evidence of poor
          lighting or road design, faulty traffic-control signals or improper
          signage."
        </p>
        <p>
          For car accidents that happened in the neighborhood of St. Anthony's
          Claret Parish, knowledgeable lawyers would be aware of residents'
          problems in backing out of their driveways onto La Palma Avenue. They
          would also be aware of resident's complaints about speeding cars and
          the safety of children walking to school.{" "}
          <Link to="/wrongful-death">La Palma car collision lawyers</Link> would
          study earlier accidents that occurred here. Skilled La Palma car
          collision lawyers would utilize these accident statistics and traffic
          hazards when preparing a case to help ensure that their clients are
          fairly compensated for their injuries and losses.
        </p>
        <p>
          Similarly, car accidents that have injured clients at the intersection
          of La Palma Avenue and Walker Street would be carefully studied,
          particularly since red-light scofflaws had driven the city to install
          red-light cameras at this location.
        </p>
        <p>
          Likewise, clients injured in a car crash on La Palma Avenue from Moody
          Street to Denni Street would prompt car accident lawyers to consider
          why motorists were allowed to make unsafe U-turns there, and why the
          city finally installed landscaped medians there to end this activity.
        </p>
        <h2>Seek the Advice of a La Palma Car Accident Lawyer</h2>
        <p>
          If you are involved in a traffic accident, you should try to find a
          trustworthy La Palma car crash lawyer. Initial no-obligation
          consultations are typically free to accident victims. And should you
          retain his or her services, you typically won't have to pay the
          lawyers' fees until after they succeed in court or at the bargaining
          table.
        </p>
        <p>
          For additional legal advice, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar. The book ($14.99
          value) is free to accident victims - just{" "}
          <Link to="/contact">contact us</Link> to get a copy sent to you, or to
          request a free consultation
        </p>
        <p>
          <strong>Orange County Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">1301 Dove St. Suite 120</div>
            <span className="locality">Newport Beach</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">92660</span>
            <br />
            <span className="tel">949-203-3814</span>
          </div>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
