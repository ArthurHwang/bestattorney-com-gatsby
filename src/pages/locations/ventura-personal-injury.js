// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Ventura",
      population: 110873,
      totalAccidents: 4621,
      intersection1: "Rose Ave & Central Ave",
      intersection1Accidents: 68,
      intersection1Injuries: 46,
      intersection1Deaths: 3
    },
    locationBox2: {
      mainCrimeIndex: 268.2,
      city1Name: "Oxnard",
      city1Index: 237.4,
      city2Name: "Port Hueneme",
      city2Index: 180.1,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Harbor Blvd & Gonzales Rd",
      intersection2Accidents: 74,
      intersection2Injuries: 41,
      intersection2Deaths: 3,
      intersection3: "Central Ave & Santa Clara Ave",
      intersection3Accidents: 74,
      intersection3Injuries: 34,
      intersection3Deaths: 1,
      intersection4: "Hueneme Rd & Las Posas Rd",
      intersection4Accidents: 50,
      intersection4Injuries: 30,
      intersection4Deaths: 3
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Ventura Personal Injury Lawyer - Bisnar Chase"
        pageDescription="please call 800-561-4887 for highest-rated Ventura personal injury lawyers. We advance all costs and if we dont win, you dont pay."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ventura Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          Work with <Link to="/about-us">skilled trial lawyers </Link> who
          advance all costs. Bisnar Chase Ventura personal injury lawyers have
          been serving California for over three decades. We are trial lawyers
          which means your case will never be handed off in the final stages. We
          represent you all the way to victory. Our personal injury attorneys
          are top rated with Avvo, SuperLawyers and we carry an A+ rating with
          the BBB.
        </p>
        <p>
          Suffering any type of serious personal injury can turn your life
          upside down. Whether you are in a car accident, or have suffered
          injuries in a workplace accident or a slip-and-fall accident, the
          consequences can be devastating. Injured victims are often faced with
          financial burdens including medical expenses and loss of earnings.
        </p>
        <p>
          If you would like to discuss the specifics or your case or to find out
          if you have a valid personal injury claim, please contact an
          experienced Ventura personal injury lawyer who will help you better
          understand your legal rights and options.
        </p>

        <p>
          To understand if you have a personal injury case, it is important to
          understand the three areas of liability that allow a victim of a
          Ventura accident to pursue a personal injury claim: negligence, strict
          liability and intentional wrongdoing. The most common area of
          liability is negligence.
        </p>
        <h2>Negligence</h2>
        <p>
          An individual can be held liable for your injuries if he or she was
          careless is causing them. Someone who acts negligently does not intend
          to cause an injury but does cause the accident through thoughtlessness
          or carelessness can be held accountable. Here are a few questions to
          ask in order to determine if there was negligence. Would a reasonable
          person have caused the accident? How could it have been prevented? A
          judge or an attorney will weigh the actions of the negligent parties
          against how a reasonable person is expected to act.
        </p>
        <h2>
          The injured party in a personal injury case must prove that there was
          a:
        </h2>
        <ul>
          <li>
            Duty of Care: The defendant owed a duty of care to the claimant.
            Generally, people have a duty to act reasonably and to not cause an
            injury to another. Ventura car drivers have the duty to drive safely
            and not put others in danger. Storeowners have a duty of care to
            provide safe premises. Employers have a duty of care to provide a
            safe workplace to their employees. What duty of care did the
            defendant have with regard to the claimant?
          </li>
          <li>
            Breach of Duty of Care: Did the defendant breach the duty of care?
            Did the car's driver exceed the speed limit putting other drivers in
            danger? Did the storeowner fail to post warning signs around a
            slippery walkway? Did the employer fail to provide adequate safety
            devices and training?
          </li>
          <li>
            What Caused the Injury: Did the breach of duty of care directly
            result in the accident? There must be a link between the defendant's
            negligence and the victim's suffering.
          </li>
          <li>
            Damages: What types of losses were suffered in the accident? Ventura
            personal injury victims may pursue compensation for physical
            injuries, financial losses and even emotional turmoil.
          </li>
        </ul>
        <h2>Strict Liability</h2>
        <p>
          This is when someone is responsible for an accident whether or not he
          or she was negligent. Victims in strict liability cases will not have
          to prove that the defendant was careless, but instead that the
          defendant was responsible for your injuries. For example, dog owners
          in California are liable for the damages their dogs cause. The
          manufacturers of defective products are also responsible for the
          damages their products cause under strict liability law.
        </p>
        <h2>Intentional Wrongdoing</h2>
        <p>
          This is when the defendant's criminal and intentional act directly
          resulted in the injuries. Victims of abuse may file a criminal
          complaint against the at-fault party, but a criminal proceeding will
          not result in financial compensation for the victim's suffering. A
          civil lawsuit, however, can result in support for losses such as
          medical bills and lost wages.
        </p>
        <p>
          These types of cases are not, however, always feasible or worthwhile.
          Intentional wrongdoing civil cases are practical for the victim when
          the at-fault party has resources that can actually cover the victim's
          losses. A skilled Ventura <Link to="/">personal injury lawyer</Link>{" "}
          can assess a case of wrongdoing and help the victim determine if a
          civil lawsuit is in their best interest.
        </p>
        <h2>Protecting Your Rights</h2>
        <p>
          If you have suffered an injury as the result of someone else's
          negligence or wrongdoing, it may be in your best interest to discuss
          your legal rights and options with an experienced Ventura personal
          injury lawyer.
        </p>
        <p>
          The skilled personal injury lawyers of Bisnar Chase Personal Injury
          Attorneys have more than 30 years of experience representing injured
          victims and their families. Please contact us for a free consultation
          and comprehensive case evaluation.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
