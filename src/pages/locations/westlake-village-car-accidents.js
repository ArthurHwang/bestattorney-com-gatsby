// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westlake Village Car Accident Lawyers - Bisnar Chase"
        pageDescription="Westlake Village car accidents have declined thanks to traffic calming and strict traffic enforcement."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Accident Lawyer Westlake Village</h1>
        <BreadCrumbs location={location} />
        <h2>Passion. Trust. Results.</h2>
        <p>
          For over 35 years Bisnar Chase Personal Injury Attorneys has been
          representing car accident victims in Los Angeles County. Our Westlake
          Village <Link to="/car-accidents">car accident lawyers</Link> have
          recovered hundreds of millions for our clients and provided a client
          care second to none. Let our expert injury specialists take the
          pressure off you and help get the compensation you deserve.
        </p>
        <ul>
          <li>
            <strong>96% success rate</strong>
          </li>
          <li>
            <strong>Over 35 years serving Los Angeles</strong>
          </li>
          <li>
            <strong>Over $500 Million in settlements and verdicts</strong>
          </li>
          <li>
            <strong>Superb AVVO Rating</strong>
          </li>
        </ul>
        <p>
          We have successfully settled or went to trial on thousands of personal
          injury cases. Bisnar Chase personal injury attorneys has the
          experience and resources to take on some of the biggest cases.
        </p>
        <h2>Car Accident Recovery Made Easier</h2>
        <p>
          After a car accident is a very traumatic time. The insurance companies
          don't make it any easier either. Our law firm takes all of that
          pressure away so that you can focus on recovery. In a typical case we
          can deal with the insurance companies, the medical providers and even
          point you in the direction of personalized medical care from providers
          that accept liens. We advance all costs during your case, and at the
          end if we don't win, you won't pay.
        </p>
        <ul>
          <li>
            <strong>Seek the highest recovery possible</strong>
          </li>
          <li>
            <strong>No fee unless we win</strong>
          </li>
          <li>
            <strong>Advance all costs</strong>
          </li>
          <li>
            <strong>Familiar with Los Angeles courts and rules</strong>
          </li>
          <li>
            <strong>Attorney of the year 2004, 2014</strong>
          </li>
        </ul>
        <h2>Traffic Calming Can Prevent Car Crashes</h2>
        <p>
          As an added traffic safety measure, Westlake Village is constantly
          evaluating the effectiveness of its traffic calming program. Among
          these are speed bumps like those on Three Springs Drive, which have
          reduced speeds by 7-10 mph. Other traffic calming measures include
          speed trailers, traffic circles and narrow streets. A growing number
          of Westlake Village car accident attorneys are in favor of such
          traffic safety measures. All combine to enhance the city's safety and
          lower its accident rate.
        </p>
        <h2>Immediate Legal Help</h2>
        <p>
          If you have been injured due to a car crash or other type of auto
          injury, please call for a free consultation with an experienced
          Westlake Village car accident attorney today.
        </p>
        <p>(800) 561-4887</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
