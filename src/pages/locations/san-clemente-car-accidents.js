// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Clemente",
      population: 65267,
      totalAccidents: 1837,
      intersection1: "El Camino Real & Avenida Palizada",
      intersection1Accidents: 43,
      intersection1Injuries: 27,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 96.0,
      city1Name: "Marina",
      city1Index: 207.2,
      city2Name: "Mountain View",
      city2Index: 161.4,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Calle Frontera & Avenida Vista Hermosa",
      intersection2Accidents: 31,
      intersection2Injuries: 16,
      intersection2Deaths: 0,
      intersection3: "Avenida Pico & Calle Del Cerro",
      intersection3Accidents: 38,
      intersection3Injuries: 13,
      intersection3Deaths: 1,
      intersection4: "Avenida Pico & Avenida Presidio",
      intersection4Accidents: 27,
      intersection4Injuries: 12,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Clemente Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 949-203-3814 for San Clemente car accident attorneys who care. Free consultations. Trust, passion, results!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Clemente Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          San Clemente road conditions cause many traffic delays and accidents.
          This area is known to be highly congested and in 2013 the fatal
          accidents increased 6.2%. There were numerous car accidents in 2013
          and 7 deaths. If you have been injured please contact the San Clemente{" "}
          <Link to="/car-accidents">car accident attorneys</Link> for a free and
          confidential consultation.
        </p>

        <p>
          A skilled auto accident attorney realizes that to serve a client more
          effectively, he or she should be familiar with the city, its streets
          and accident statistics where the accident occurred.{" "}
        </p>
        <h2>San Clemente's Alarming Accident Statistics</h2>
        <p>
          From 2009-2014, 19 people died and 1837 were injured in San Clemente
          car crashes. 539 DUI incidents caused 12 fatalities and 215 injuries.
          Bicycle accidents injured 88, and motorcycle accidents killed 3 and
          injured 134. Pedestrian accidents killed 5 and injured 99."
        </p>
        <h2>San Clemente's Special Traffic Problems</h2>
        <p>
          The city's traffic problems contribute to car accidents. Traffic
          movement through intersections is graded using an A through F standard
          established by the state. An "A" intersection being one that performs
          ideally, while a level "F" fails entirely. Although San Clemente's
          intersections are usually graded at "C" and above, Palizada/El Camino
          Real and Pico /I-5 often run at level "D."
        </p>
        <p>
          Other San Clemente traffic problems include congestion at San Clemente
          High School, pile-ups at the new outlet mall, and traffic jams at
          Confordia School. In addition, bicycle lanes on Avenida Pico have been
          reduced or eliminated, which creates a risk for bicyclists. There's
          also a need for traffic-calming measures on Calle Grande Vista road.
        </p>
        <h2>Consult an Experienced San Clemente Car Accident Attorney </h2>
        <p>
          If you are injured in a car accident, seek out the expert advice of a
          auto accident lawyer who knows the city. An experienced legal team can
          tell you if you have a case worth pursuing. He or she can stand up for
          your rights as a car crash victim.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
