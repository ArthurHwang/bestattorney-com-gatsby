// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Lakewood",
      population: 81121,
      totalAccidents: 1971,
      intersection1: "Del Amo Blvd & Paramount Blvd",
      intersection1Accidents: 95,
      intersection1Injuries: 46,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 228.6,
      city1Name: "Artesia",
      city1Index: 235.5,
      city2Name: "Bellflower",
      city2Index: 249.8,
      city3Name: "Hawaiian Gardens",
      city3Index: 198.4,
      city4Name: "Cerritos",
      city4Index: 235.2
    },
    locationBox3: {
      intersection2: "Bellflower Blvd & Del Amo Blvd",
      intersection2Accidents: 83,
      intersection2Injuries: 41,
      intersection2Deaths: 0,
      intersection3: "Del Amo Blvd & Woodruff Ave ",
      intersection3Accidents: 97,
      intersection3Injuries: 36,
      intersection3Deaths: 0,
      intersection4: "Bellflower Blvd & South St",
      intersection4Accidents: 73,
      intersection4Injuries: 32,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lakewood Personal Injury Attorneys - LA County, CA"
        pageDescription="Were you injured in a Lakewood accident? Call 949-203-3814 for a free consultation. Award winning attorneys are here to help. No win, no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lakewood Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Lakewood, California, has an interesting history and has been called
            the typical post-war suburb.{" "}
          </strong>
            Located close to Los Angeles, Lakewood sprang up almost overnight as
          the result of the explosion of suburban building in the 1950s. 
          Between 1950 and 1960, Lakewood jumped from a population of just a few
          hundred to thousands; today, the city is home to 81,000 people.  With
          all this rapid population growth, it is not surprising that this
          city's residents are victims of many personal injury cases, from car
          accidents to dog bites and premises liability cases.
        </p>
        <p>
          if you've been injured due to someone else's negligence, contact
          Bisnar Chase to see if you have a case. We'll help you through every
          step of the way so you don't have to worry about a thing!
        </p>
        <h2>A Fair Result for All Our Clients</h2>
        <p>
          At Bisnar Chase, we believe that personal injury victims should
          receive fair compensation when they’re injured because of an
          individual’s or a company’s negligence. This should be true whether
          you’ve been injured in Lakewood or anywhere else in California. After
          being injured and before speaking to an insurance company, it is best
          to make sure you consult a personal injury attorney in an attempt to
          best understand your options.{" "}
        </p>
        <h2>Getting Answers</h2>
        <p>
          Although Lakewood’s crime index falls under the national average of
          294.7 and{" "}
          <Link to="/locations/lakewood-car-accidents">
            car accident injury rate
          </Link>{" "}
          falls under the state average, injuries definitely still occur on an
          everyday basis here, and it can be beneficial for a victim to
          understand how the value of their case might be calculated by
          insurance companies and by lawyers. Bisnar Chase has written a guide
          to{" "}
          <Link to="/resources/value-of-your-personal-injury-claim">
            understanding the factors that determine your personal injury case
            value
          </Link>
          , which is a good place to start if you want to know if you have a
          case. Ultimately though, your questions will have to be answered in a
          free consultation with a personal injury lawyer.
        </p>

        <h2>Experience With Lakewood Injuries</h2>
        <p>
          Bisnar Chase has represented cases in Lakewood and all throughout
          California. We don’t just specialize in car accidents, but also
          represent clients who have been bitten by dogs, those who have been
          injured because of an auto or product defect, employees who haven’t
          been paid or given break time mandated by state law, and patients who
          have been injured by medical products. We’re on your side, and we want
          to help you fight your battle against the insurance companies! If you
          want to know if you have a case with us, just call us at (323)
          238-4683.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
