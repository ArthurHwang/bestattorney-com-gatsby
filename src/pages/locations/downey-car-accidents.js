// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Downey",
      population: 113242,
      totalAccidents: 2943,
      intersection1: "Florence Ave & Lakewood Blvd",
      intersection1Accidents: 137,
      intersection1Injuries: 134,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 264.2,
      city1Name: "Bell Gardens",
      city1Index: 194.5,
      city2Name: "Paramount",
      city2Index: 304.2,
      city3Name: "Bellflower",
      city3Index: 249.8,
      city4Name: "Cudahy",
      city4Index: 205.8
    },
    locationBox3: {
      intersection2: "Florence Ave & Paramount Blvd",
      intersection2Accidents: 133,
      intersection2Injuries: 82,
      intersection2Deaths: 1,
      intersection3: "Firestone Blvd & Lakewood Blvd ",
      intersection3Accidents: 134,
      intersection3Injuries: 77,
      intersection3Deaths: 1,
      intersection4: "Firestone Blvd & Paramount Blvd",
      intersection4Accidents: 112,
      intersection4Injuries: 94,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Downey Car Accident Lawyer - Los Angeles County Auto Accident Attorney"
        pageDescription="Were you injured in a car accident? Call 949-203-3814 for Downey car accident attorneys who care. Free consultations. Trust, passion, results! Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Experienced Downey Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>There's no substitute for experience,</strong> which is why
          the advice of a Downey personal injury attorney can be immeasurably
          valuable should you ever fall victim to a car accident in Downey,
          California. Getting fully compensated for all your injuries and
          damages can be quite daunting. And if you're in Downey, car accidents
          can happen to even the safest driver. Contact Bisnar Chase to see if
          you have a case for your car accident injury!
        </p>

        <h2>Avoid Mistakes -- Consult a Knowledgeable Car Accident Lawyer</h2>
        <p>
          Downey car accident lawyers who have decades of experience can be very
          helpful to anyone who has been injured in a car accident. They can
          make sure that you receive fair compensation for your damages and
          injuries. Let's say you rent a car through your insurance plan while
          your accident vehicle is being repaired. Instead of accepting the $20-
          or $25-per day rental allowance your plan provides, consider the fair
          rental value of your car damaged in the accident. If it's an expensive
          SUV or sports car, it no doubt rents for quite a bit more than the
          insurance firm will voluntarily offer. It's in your best interest to
          document the difference and make it part of your settlement claim for
          the accident.
        </p>
        <h2>More Useful Advice</h2>
        <p>
          Experienced Downey car collision lawyers offer another nugget of
          wisdom that can save you money. If the vehicle that was damaged in the
          accident is being stored in a towing yard, you may still get stuck
          with the storage fees -- if the other motorist doesn't have full
          coverage insurance. In this instance, you should remove your vehicle
          from the yard post haste, because chances are, you may not be
          reimbursed for this expense.
        </p>
        <p>
          Remember, when it comes to economic damages, you should attempt to
          reduce or eliminate any unnecessary costs, in particular, any expenses
          that don't qualify for reimbursement. Do, on the other hand, include
          every expense that is eligible for reimbursement.
        </p>
        <h2>
          The Best Car Accident Lawyers Offer No-Charge, No-Pressure
          Consultations
        </h2>
        <p>
          The most trusted Downey car crash lawyers will usually offer a free
          and confidential consultation to explain your rights and options. For
          more information, visit the Bisnar Chase Personal Injury Attorneys LLP
          web site at / or read "The Seven Fatal Mistakes That Can Wreck Your
          Personal Injury Claim" by California personal injury attorney John
          Bisnar. The book is free to accident victims--just go to
          www.BestAttorneyBooks.com to get a copy sent to you, or you can buy
          one at www.Amazon.com.
        </p>
        <p>
          Get more valuable advice by reading{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
