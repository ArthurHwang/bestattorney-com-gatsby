// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {
  pageSubject: "car-accident",
  location: "orange-county"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Aliso Viejo",
      population: 50175,
      totalAccidents: 545,
      intersection1: "Pacific Park & Aliso Viejo Pkwy",
      intersection1Accidents: 26,
      intersection1Injuries: 24,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 47.5,
      city1Name: "Laguna Hills",
      city1Index: 119.1,
      city2Name: "Laguna Woods",
      city2Index: 54.2,
      city3Name: "Laguna Niguel",
      city3Index: 73.2,
      city4Name: "Laguna Beach",
      city4Index: 144.6
    },
    locationBox3: {
      intersection2: "Pacific Park & Alicia Creek",
      intersection2Accidents: 50,
      intersection2Injuries: 21,
      intersection2Deaths: 0,
      intersection3: "Pacific Park & Alicia Pkwy",
      intersection3Accidents: 33,
      intersection3Injuries: 15,
      intersection3Deaths: 0,
      intersection4: "Aliso Creek & Aliso Viejo Pkwy",
      intersection4Accidents: 28,
      intersection4Injuries: 15,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Aliso Viejo Car Accident Attorneys - 949-203-3814"
        pageDescription="Call the Aliso Viejo car accident lawyers of Bisnar Chase. You may be entitled to compensation. Free case evaluation. 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Aliso Viejo Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Nestled in the hills of South Orange County, Aliso Viejo boasts
          well-designed streets and intersections without the crushing traffic
          of many larger cities, yet Aliso Viejo{" "}
          <Link to="/car-accidents"> car accident attorneys </Link> will express
          with concern that car collisions still take place often in this newer
          city of 50,000 people. To help reduce the number of car collisions in
          Aliso Viejo, DUI teams from the{" "}
          <Link to="https://ocsd.org/">
            Orange County Sheriff's Department (OCSD){" "}
          </Link>{" "}
          set their sights on a number of accident-prone areas within the city.
          Part of Orange County's "Avoid the 38" Program, OCSD's Aliso Viejo
          effort set up sobriety checkpoints at specific streets where a high
          percentage of DUI related arrests and collisions took place. The goal
          was to cite and/or arrest impaired motorists who were driving a motor
          vehicle without a driver's license or who were intoxicated.
        </p>

        <p>
          As in any city, impaired drivers are an ongoing threat to public
          safety that endangers motorists, pedestrians and cyclists. Statewide,
          nearly 200,000 drivers are arrested for driving under the influence of
          drugs or alcohol annually. Most DUIs are male, while an increasing
          number of drunk drivers are female.
        </p>
        <p>
          If you've been injured in a car accident, you deserve the best
          representation; to have someone looking out for you,{" "}
          <em>especially</em> if you've been hit by a drunk driver. Bisnar Chase
          are injury advocates. We'll answer your questions and fight for you!
          Call us or fill out our form below to see if you have a case!
        </p>
        <h2>National Car Accident Statistics Cause for Alarm</h2>
        <p>
          Statistics nationwide reveal even more tragic deaths and injuries. In
          2015, 19,000 people died in car crashes. That same year, injuries from
          car accidents were a staggering 2.3 million, most of them serious.
        </p>
        <p>
          Most Aliso Viejo motor vehicle accident lawyers acknowledge that
          impaired or reckless drivers must be removed from city streets and
          highways. These lawyers deal with the devastation drivers cause every
          year in Aliso Viejo. Many support local law enforcement programs to
          prevent drunk or reckless drivers from causing car crashes in the
          city.
        </p>
        <h2> Aliso Viejo Injury Statistics</h2>
        <p>
          {" "}
          In 2015 there were 50 car accidents at Pacific Park and Alicia Creek.
          Of those, 21 resulted in injuries. Alicia Pkwy saw another 33 crashes
          with 15 injuries. Aliso Creek is also an area with a high rate of car
          accidents having 28 with 15 injuries.{" "}
        </p>
        <h2> Get Immediate Legal Help</h2>
        <p>
          {" "}
          The serious injury attorneys of Bisnar Chase are seasoned trial
          lawyers with years of experience dealing with devastating car
          accidents. Our legal team fights every day for residents of Orange
          County and we may be able to help you too.
        </p>
        <p align="center">
          <strong>For a</strong> <Link to="/contact">free consultation</Link>{" "}
          <strong>at 949-203-3814</strong>
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
