// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React, { useState } from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
  TrafficLayer
} from "react-google-maps"
import mapStyles from "../../components/utilities/mapStyles"
import { DefaultBlueButton } from "../../components/utilities/"
import { serviceAreas } from "../../data/serviceAreas"

// Google maps component takes style array from import.  Change the array to change styling.

function Map(props) {
  // props are coming from button click from component below
  const [selectedArea, setSelectedArea] = useState(null)
  const aggregateCityData = [
    ...serviceAreas.OC.areas,
    ...serviceAreas.LA.areas,
    ...serviceAreas.IE.areas,
    ...serviceAreas.SD.areas,
    ...serviceAreas.NC.areas
  ]

  return (
    <GoogleMap
      defaultZoom={11}
      defaultCenter={{ lat: serviceAreas.OC.lat, lng: serviceAreas.OC.lng }}
      defaultOptions={{ styles: mapStyles }}
      center={{ lat: props.mainArea.lat, lng: props.mainArea.lng }}
    >
      {aggregateCityData.map((area, index) => (
        <React.Fragment key={index}>
          <TrafficLayer autoUpdate />
          <Marker
            position={{
              lat: area.lat,
              lng: area.lng
            }}
            onClick={() => {
              setSelectedArea(area)
            }}
          />
        </React.Fragment>
      ))}

      {selectedArea && (
        <InfoWindow
          position={{
            lat: selectedArea.lat,
            lng: selectedArea.lng
          }}
          onCloseClick={() => {
            setSelectedArea(null)
          }}
        >
          <div className="infowindow-text">
            <p className="infowindow-title">{selectedArea.area}</p>
            <p className="infowindow-content">
              {" "}
              <Link to={selectedArea.link}>
                Click here to see more <br />
                information about our <br /> {selectedArea.area} service
                location{" "}
                <FaArrowAltCircleRight
                  style={{ position: "relative", top: "3px" }}
                />
              </Link>
            </p>
          </div>
        </InfoWindow>
      )}
    </GoogleMap>
  )
}
// HOC to add google maps script and functionality
const WrappedMap = withScriptjs(withGoogleMap(Map))

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function LocationsPage({ location }) {
  // When a user clicks a button, state is updated and passed to map component above
  const [mainArea, setMainArea] = useState({
    lat: serviceAreas.OC.lat,
    lng: serviceAreas.OC.lng
  })

  const handleAreaClick = event => {
    if (event.target.innerHTML === "Orange County") {
      setMainArea({ lat: serviceAreas.OC.lat, lng: serviceAreas.OC.lng })
    }
    if (event.target.innerHTML === "Los Angeles") {
      setMainArea({ lat: serviceAreas.LA.lat, lng: serviceAreas.LA.lng })
    }
    if (event.target.innerHTML === "Inland Empire") {
      setMainArea({ lat: serviceAreas.IE.lat, lng: serviceAreas.IE.lng })
    }
    if (event.target.innerHTML === "San Diego") {
      setMainArea({ lat: serviceAreas.SD.lat, lng: serviceAreas.SD.lng })
    }
    if (event.target.innerHTML === "Northern California") {
      setMainArea({ lat: serviceAreas.NC.lat, lng: serviceAreas.NC.lng })
    }
  }

  return (
    <LayoutPAGEO sidebar={false} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Attorneys | Service Locations"
        pageDescription="Bisnar Chase serves injury clients all throughout California! Visit our city pages to see the areas we serve."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Service Area Locations</h1>
        <BreadCrumbs location={location} />

        <h3>We serve clients all over California.</h3>
        <p>
          Click on a marker to display more information regarding that service
          area location. Use this tool to easily access our service area pages
          that you are interested in. You may also choose your service area of
          interest from the list below. We have included real-time traffic for
          your convenience.
        </p>

        <div id="map-wrapper">
          <div id="buttons-wrapper">
            <DefaultBlueButton onClick={handleAreaClick}>
              Orange County
            </DefaultBlueButton>
            <DefaultBlueButton onClick={handleAreaClick}>
              Los Angeles
            </DefaultBlueButton>
            <DefaultBlueButton onClick={handleAreaClick}>
              Inland Empire
            </DefaultBlueButton>
            <DefaultBlueButton onClick={handleAreaClick}>
              San Diego
            </DefaultBlueButton>
            <DefaultBlueButton onClick={handleAreaClick}>
              Northern California
            </DefaultBlueButton>
          </div>
          <WrappedMap
            mainArea={mainArea}
            googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.GATSBY_GOOGLE_MAPS_APIKEY}`}
            loadingElement={<div style={{ height: "100%" }} />}
            containerElement={<div style={{ height: "100%" }} />}
            mapElement={<div style={{ height: "100%" }} />}
          />
        </div>

        <h3>Is your location missing from this map?</h3>
        <p>
          These areas above are where most of our clients live or have had
          injuries, but that doesn't mean we don't serve clients anywhere else.
          We're available to represent clients throughout California, and we
          also represent injured clients nationwide in auto defect cases. If you
          have any questions or want to know more, please call us at{" "}
          <strong>(800)-561-4887</strong>, or{" "}
          <Link to="/contact">fill out our contact form.</Link>
        </p>
        <div id="area-wrapper">
          <h2>List of Service Areas:</h2>
          <div id="area-grid">
            <ul className="area-list">
              {" "}
              <Link to="/orange-county">
                <p className="area-title">Orange County</p>
              </Link>
              {serviceAreas.OC.areas
                .sort((a, b) => a.area.localeCompare(b.area))
                .map(area => (
                  <li key={area.area}>
                    {" "}
                    <Link to={area.link}>{area.area}</Link>
                  </li>
                ))}
            </ul>
            <ul className="area-list">
              {" "}
              <Link to="/los-angeles">
                <p className="area-title">Los Angeles County</p>
              </Link>
              {serviceAreas.LA.areas
                .sort((a, b) => a.area.localeCompare(b.area))
                .map(area => (
                  <li key={area.area}>
                    {" "}
                    <Link to={area.link}>{area.area}</Link>
                  </li>
                ))}
            </ul>
            <ul className="area-list">
              {" "}
              <Link to="/riverside">
                <p className="area-title">Inland Empire</p>
              </Link>
              {serviceAreas.IE.areas
                .sort((a, b) => a.area.localeCompare(b.area))
                .map(area => (
                  <li key={area.area}>
                    {" "}
                    <Link to={area.link}>{area.area}</Link>
                  </li>
                ))}
            </ul>
            <ul className="area-list">
              {" "}
              <Link to="/san-diego">
                <p className="area-title">San Diego County</p>
              </Link>
              {serviceAreas.SD.areas
                .sort((a, b) => a.area.localeCompare(b.area))
                .map(area => (
                  <li key={area.area}>
                    {" "}
                    <Link to={area.link}>{area.area}</Link>
                  </li>
                ))}
            </ul>
            <ul className="area-list">
              {" "}
              <Link to="/san-francisco">
                <p className="area-title">Northern California </p>
              </Link>
              {serviceAreas.NC.areas
                .sort((a, b) => a.area.localeCompare(b.area))
                .map(area => (
                  <li key={area.area}>
                    {" "}
                    <Link to={area.link}>{area.area}</Link>
                  </li>
                ))}
            </ul>
          </div>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .area-title {
    border-bottom: 1px solid black;

    @media (max-width: 550px) {
      font-size: 1.4rem !important;
    }
  }

  li {
    margin-bottom: 0;
  }

  div.infowindow-text {
    p.infowindow-title {
      font-size: 1.4rem;
      text-align: center;
      margin-bottom: 4px;
    }

    p.infowindow-content {
      font-size: 1rem;
      text-align: center;
      margin: 0;
    }
  }

  #area-wrapper {
    margin-bottom: 2rem;
  }

  #area-grid {
    display: grid;
    grid-template-columns: repeat(5, 1fr);

    @media (max-width: 1024px) {
      grid-template-columns: repeat(2, 1fr);
      grid-gap: 10px;
    }

    .area-list p {
      text-align: left;
      font-size: 2rem;
      margin-bottom: 0.5rem;
      transition: all 0.3s;

      @media (max-width: 550px) {
        font-size: 1rem;
      }

      &:hover {
        color: ${({ theme }) => theme.colors.accent};
      }
    }
  }

  h3 {
    font-size: 4rem;
    padding: 0 2rem 1rem;
    text-align: center;
    line-height: 1;

    @media (max-width: 550px) {
      font-size: 2.5rem;
      padding: 0;
      margin-bottom: 1rem;
    }
  }

  h4 {
    font-size: 2rem;
    text-align: center;
    padding: 0 3rem 3rem;
    line-height: 1;
  }

  #map-wrapper {
    position: relative;
    width: 100%;
    height: 65rem;
    margin-bottom: 3rem;

    @media (max-width: 550px) {
      height: 35rem;
    }
  }

  #buttons-wrapper {
    display: flex;
    justify-content: space-between;
    position: absolute;
    z-index: 1;
    flex-direction: column;
    bottom: 0;
    padding-left: 2rem;
    padding-bottom: 2rem;

    @media (max-width: 550px) {
      height: 35rem;
      left: 0;
      bottom: 0;
      padding: 0;
      height: auto;
      margin-left: 0.5rem;
    }

    button {
      height: 3rem;
      margin-bottom: 1rem;
      width: 18rem;

      @media (max-width: 550px) {
        font-size: 0.8rem;
        width: 5rem;
        height: 2.5rem;
        margin-bottom: 0.3rem;
        padding: 0 4px;
      }
    }
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
