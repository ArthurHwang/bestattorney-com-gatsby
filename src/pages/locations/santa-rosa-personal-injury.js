// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Santa Rosa",
      population: 171990,
      totalAccidents: 6393,
      intersection1: "Guerneville Rd & Marlow Rd",
      intersection1Accidents: 55,
      intersection1Injuries: 65,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 193.4,
      city1Name: "Rohnert Park",
      city1Index: 215.7,
      city2Name: "Sebastopol",
      city2Index: 185.0,
      city3Name: "Cotati",
      city3Index: 205.2,
      city4Name: "Windsor",
      city4Index: 157.1
    },
    locationBox3: {
      intersection2: "Sebastopol Rd & Dutton Ave",
      intersection2Accidents: 81,
      intersection2Injuries: 67,
      intersection2Deaths: 0,
      intersection3: "Sebastopol Rd & Stony Point Rd",
      intersection3Accidents: 88,
      intersection3Injuries: 52,
      intersection3Deaths: 0,
      intersection4: "Cleveland Ave & Guerneville Rd",
      intersection4Accidents: 59,
      intersection4Injuries: 50,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Rosa Personal Injury Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated personal injury attorneys in Santa Rosa, California. Specialize in catastrophic injuries, car accidents, wrongful death & auto defects. No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Rosa Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Santa Rosa personal injury attorneys are available to evaluate your
          case free. Bisnar Chase offers a free consultation and also advances
          all costs. If we do not win your case, you will not pay. We have kept
          that promise for over 35 years as we serve Californians.
        </p>
        <p>
          As the fifth largest city in the bay area, with over 160,000
          residents, Santa Rosa has a variety of personal injury risks. As in
          many large cities, a great proportion of these{" "}
          <Link to="/">personal injury accidents</Link> happen on the city's
          streets.
        </p>
        <p>
          Sometimes a little bit of careless driving can cause a severe
          accident, as in the case of a collision that occurred in March. A man
          was driving a mother and her two children in a 2001 Dodge pickup when
          he suddenly continued through a red light that had been red for
          several seconds. They were traveling along Santa Rosa Avenue and
          collided with a 1993 Infinity. The pickup continued into a cement
          drainage culvert and the two children suffered major injuries.
        </p>
        <p>
          The driver and passengers of the Infinity were not injured, and when
          police came to investigate the scene, they found that the driver who
          ran the red light was not licensed to operate a vehicle. Santa Rosa
          personal injury attorneys will have you know that many accidents in
          California are caused by drivers without licenses, sometimes resulting
          in a hit-and-run as the driver attempts to flee the scene of the
          accident.
        </p>
        <h2>
          Motorcyclists and Pedestrians Must Practice Extra Safety Precautions
        </h2>
        <p>
          Santa Rosa personal injury accidents can involve other types of
          transportation as well. A man careened his motorcycle into oncoming
          traffic on Highway 116. The motorcyclists had traveled along a curve,
          change lanes, and continued into the opposing lane. He was in critical
          condition after the crash and was taken to Santa Rosa Memorial
          Hospital.
        </p>
        <p>
          A woman who witnessed the crash and called 911 described it as "life
          altering" because of how graphic the accident was and how quickly it
          happened. It is unclear as to how the motorcyclist lost control, but
          an example of how easily accidents can take place and result in{" "}
          <Link to="/catastrophic-injury">catastrophic injuries</Link>.
        </p>
        <p>
          Pedestrians must also be careful on the roads. In May of 2010, runners
          participating in a 199-mile relay race called "The Relay" from
          Calistoga to Santa Cruz passed through Santa Rosa as part of their
          trek. One runner, a 33-year-old woman of Lafayette, was struck by a
          vehicle at around 10:15pm on the westbound lane of D street. The
          driver was a 55-year-old man traveling at the speed limit, who said he
          did not see the runner as he drove around a bend in the road. It was
          dark out, and he claimed that she was not wearing reflective clothing
          to protect her at night.
        </p>
        <p>
          The woman suffered head and internal injuries from the collision, and
          was taken to the Santa Rosa Memorial Hospital. The relay race was
          organized to benefit people seeking organ transplants, and it is
          unfortunate that a participant in this event was injured on the road.
        </p>
        <h2>Santa Rosa Personal Injury Law Firms Can Help You</h2>
        <p>
          If you have been injured in an accident on or off the road, for which
          someone else might be at fault, you should contact a good Santa Rosa
          personal injury attorney or motorcycle accident lawyer to help you
          with your case.
        </p>
        <div
          className="google-maps"
          data-map="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&amp;sll=33.660639,-117.873344&amp;sspn=0.068011,0.110035&amp;ie=UTF8&amp;ll=37.790388,-122.470522&amp;spn=0.02374,0.036478&amp;z=14&amp;output=embed"
        ></div>
        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=5139+Geary+Boulevard,+San+Francisco,+CA,+94118&amp;sll=33.660639,-117.873344&amp;sspn=0.068011,0.110035&amp;ie=UTF8&amp;ll=37.790388,-122.470522&amp;spn=0.02374,0.036478&amp;z=14">
            View Larger Map
          </Link>
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
