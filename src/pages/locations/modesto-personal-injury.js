// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Modesto",
      population: 204933,
      totalAccidents: 8921,
      intersection1: "Scenic Dr & Bodem St",
      intersection1Accidents: 91,
      intersection1Injuries: 76,
      intersection1Deaths: 3
    },
    locationBox2: {
      mainCrimeIndex: 498.0,
      city1Name: "Turlock",
      city1Index: 330.6,
      city2Name: "Ceres",
      city2Index: 303.1,
      city3Name: "Manteca",
      city3Index: 276.2,
      city4Name: "Oakdale",
      city4Index: 293.5
    },
    locationBox3: {
      intersection2: "East Briggsmore Ave & Coffee Rd",
      intersection2Accidents: 89,
      intersection2Injuries: 79,
      intersection2Deaths: 0,
      intersection3: "Oakdale Rd & Floyd Ave",
      intersection3Accidents: 61,
      intersection3Injuries: 60,
      intersection3Deaths: 0,
      intersection4: "Coffee Rd & Floyd Ave",
      intersection4Accidents: 69,
      intersection4Injuries: 39,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Modesto Personal Injury Lawyer - Bisnar Chase"
        pageDescription="Injured in a car accident in Modesto? Bisnar Chase specializes in catastrophic injuries, car accidents, wrongful death & auto defects."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Modesto Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Modesto, California is the government seat for Stanislaus County,
          </strong>{" "}
          a sunny, inland region. The city, however, has a very high rate of
          assaults, more than doubling the US average in 2013, and steadily
          climbing since 2010. It seems this statistic is a warning of the
          personal injury dangers in Modesto. Personal injury occurring from
          assaults on private property can be as a result of property owner
          negligence, due to owners not providing safe and secure premises.
          Other examples of negligence cases that result in injuries are car
          accidents, dog bites, defective products, and slip and falls
          accidents. Bisnar Chase represents injured victims in their fight to
          be compensated for their bills and pain and suffering that they've
          experienced because of someone else's negligence. Contact us to see if
          you have a case today!
        </p>
        <h2>Police Chases Cause Accidents</h2>
        <p>
          Around 300 people die in police chase car accidents each year,
          according to a study by Harborview Injury Prevention and Research
          Center of Seattle. One instance of these dangerous car chases took
          place in Modesto in 2007 and has led to controversies over who was
          really at fault.
        </p>
        <p>
          A 27-year-old man in a stolen vehicle was pursued by two patrol cars
          in heavy traffic after suspicious activity was reported. The driver
          reached up to 70 mph and was trying to pass a car when he hit another
          head-on. Two were killed in the impacted car and the passenger of the
          stolen vehicle died as well.{" "}
        </p>
        <p>
          A Modesto fatal car accident attorney could help dissolve some of the
          confusion around this event. Families of the victims who died believe
          that the Sheriff's Department is at fault for "improperly chasing [the
          driver] through North Modesto". They believe the pursuit is what
          caused the deaths of their loved ones, and have filed two wrongful
          death lawsuits in Stanislaus County seeking damages against the county
          in 2009.
        </p>
        <p>
          County Sheriff responded by saying, "The deputy sheriff didn't make
          the choice to engage in the chase; the suspects made the choice by
          fleeing."
        </p>
        <h2>DUI Takes its Toll in Modesto</h2>
        <p>
          A 27-year-old man has been involved in two bicycle accidents, the more
          recent one resulting in a death. The driver is accused of striking and
          killing a bicyclist while under the influence of a painkiller and
          marijuana. At the scene of the accident, the man was exhibiting
          behavior as though under the influence of something, though there have
          been no studies to show that the painkiller he had taken, oxycodone,
          has any effect on driving.
        </p>

        <p>
          This fatal car accident is made all the worse because it was this
          driver's second offense and he had been called, by the Department of
          Motor Vehicles, "among California's worst drivers". The first accident
          he was implicated in resulted in the the injury of a boy riding his
          bicycle across the street in Modesto. He was found under the influence
          of intoxicants during this accident, as well.
        </p>
        <h2>
          When it Comes to Car Accidents, a Modesto Car Accident Lawyer Can Help
          You
        </h2>
        <p>
          If you have been injured by a drunk driver, been involved in a
          hit-and-run, or have sustained personal injury in any other way where
          another person might be at fault, Modesto personal injury law firms
          can help surface the details of your case.{" "}
        </p>
        <p>
          If you are the victim of a personal injury, you owe it to yourself to
          get a FREE copy of the book,{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim"
          </Link>{" "}
          ($14.99 value). If you need help immediately, call one of our expert
          personal injury lawyers now at 949-203-3814 and we will schedule a
          consultation the same day or by the next business day at the latest.
        </p>
        <h3 align="center">
          Free Modesto office, hospital or home consultations by appointment
          only.
        </h3>
        <h3 align="center">Call 949-203-3814</h3>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
