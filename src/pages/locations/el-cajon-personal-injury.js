// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "El Cajon",
      population: 102211,
      totalAccidents: 4506,
      intersection1: "Broadway & Ballantyne St",
      intersection1Accidents: 50,
      intersection1Injuries: 64,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 259.3,
      city1Name: "Santee",
      city1Index: 179.2,
      city2Name: "La Mesa",
      city2Index: 261.7,
      city3Name: "Carlsbad",
      city3Index: 155.2,
      city4Name: "Encinitas",
      city4Index: 150.3
    },
    locationBox3: {
      intersection2: "Chase Ave & Avocado Ave",
      intersection2Accidents: 50,
      intersection2Injuries: 58,
      intersection2Deaths: 0,
      intersection3: "Mollison Ave & Broadway",
      intersection3Accidents: 43,
      intersection3Injuries: 49,
      intersection3Deaths: 0,
      intersection4: "Washington Ave & Mollison Ave",
      intersection4Accidents: 39,
      intersection4Injuries: 41,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Lawyers in El Cajon"
        pageDescription="Were you seriously injured in an accident in El Cajon? Call 949-203-3814 for El Cajon personal injury attorneys who care. Free consultations. Decades of experience."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>El Cajon Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>El Cajon had an average of 450 injuries</strong> from
          accidents every year for the past 5 years, and has also had a higher
          than average rate of auto thefts and arson than other cities in the
          area, even San Diego. This leads to a higher chance of injury within
          the city. Personal Injury Attorneys like Bisnar Chase can help you
          through any difficult life situations like auto accidents and injuries
          - let us fight for you against insurance companies who don't want to
          pay what you deserve in compensation. Call us today and see if you
          have a case!
        </p>
        <p>
          El Caj&oacute;n is Spanish for "The Box", relating to it being a
          parcel of land granted out of the vast Mission San Diego de Alcala
          tract and used for farming by Spanish missionaries.
        </p>
        <p>
          The <strong>El Caj&oacute;n personal injury attorneys</strong> at
          Bisnar Chase Personal Injury Attorneys offer these El Caj&oacute;n
          resources for your convenience. If you are in need of an El
          Caj&oacute;n personal injury lawyer, please call us at 949-203-3814.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="https://www.ci.el-cajon.ca.us/" target="_blank">
              City of El Cajon, CA
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.ci.el-cajon.ca.us/dept/fire.html"
              target="_blank"
            >
              El Cajon Fire Department
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.elcajonpolice.org/" target="_blank">
              El Cajon Police Department
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.sdcl.org/locations_EC.html" target="_blank">
              El Cajon Public Library
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.cajon.k12.ca.us/" target="_blank">
              El Cajon Unified School District
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.dmv.ca.gov/fo/offices/appl/fo_data_read.jsp?foNumb=669"
              target="_blank"
            >
              El Cajon DMV Office
            </Link>
          </li>
        </ul>
        <h3 align="center">
          Free El Cajon office, hospital or home consultations by appointment
          only.
        </h3>
        <h3 align="center">Call 949-203-3814</h3>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
