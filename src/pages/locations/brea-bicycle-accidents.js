// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {
  pageSubject: "bicycle-accident",
  location: "orange-county"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brea Bicycle Accident Attorneys Bisnar Chase"
        pageDescription="Brea Bicycle Accident Attorneys are here to help you during your time of recovery. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Brea Bicycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          The skilled Brea{" "}
          <Link to="/orange-county/bicycle-accidents">
            bicycle accident lawyers
          </Link>{" "}
          of Bisnar Chase, for the last three decades, helped injured victims
          and their families obtain just compensation for their losses. In cases
          where negligence or wrongdoing is involved, injured victims can seek
          compensation to cover damages including medical expenses, lost wages,
          cost of hospitalization, rehabilitation and pain and suffering.
        </p>
        <p>
          Families of deceased bicycle accident victims can file a wrongful
          death claim seeking compensation for medical and funeral costs, lost
          future income and loss of love and companionship. Those injured by a
          hit-and-run driver or an uninsured/underinsured driver may be able to
          seek compensation through the uninsured motorist clause of their own
          auto insurance policies. If you or a loved one has been injured in a
          Brea motorcycle accident, please contact us for a free and
          comprehensive consultation.
        </p>
        <h2>Bike Crash Statistics</h2>
        <p>
          Sunset Magazine named Brea as one of the five best suburbs to live in
          the western United States. It is known for its support of the arts,
          vibrant downtown and the popular Brea Mall. Brea only had
          approximately 35,000 residents as of 2015, but there are always a
          number of visitors, workers and shoppers from out of the area as well.
          These factors along with congestion on the nearby 57 Freeway can
          create a dangerous environment for bicyclists in Brea.
        </p>
        <p>
          California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) reported recently that 238 people were injured in Brea
          car accidents. During the same year, 13 people were injured in bicycle
          accidents. A half a dozen states including California make up over 50%
          of cycling deaths.
        </p>
        <p>
          Brea has a very popular bike trail called{" "}
          <Link to="https://www.mapmyride.com/us/brea-ca/" target="_blank">
            Carbon Canyon
          </Link>
          . This park has hundreds of visitors every day and it's a typical
          location for some serious bike injuries. Since the region is over 124
          acres, cyclists have to be careful of all the cross traffic from other
          vehicles, motorcycles and other cyclists. Many of the accidents that
          cyclists have, happened while they were wearing a bike helmet. the
          helmet certainly helps to save lives but typically many of the
          injuries are to the limbs as well. Proper bicycle attire should be
          worn to protect knees and elbows.
        </p>
        <h2>History of Brea Bicycle Accidents</h2>
        <p>
          Brea has had its share of tragic bike accidents. In early 2005, a man
          in his 30s sustained serious head and internal injuries after being
          struck by a hit-and-run driver. According to The Orange County
          Register, he was riding a mountain bike on Palm Street when a maroon
          van struck him and drove off. Brea officers were able to track down
          the 51-year-old driver and arrest him on suspicion of felony driving
          under the influence and hit-and-run.
        </p>
        <p>
          A witness to a crash reported on BikeForums.net that on February 13,
          2011, a cyclist was rear-ended on Central Avenue in Brea. There were a
          number of witnesses who not only saw the car strike the bike rider,
          but also were able to stop the driver from leaving the scene.
          Witnesses say the bicyclist flew backwards and land on the hood of a
          car before rolling off. The driver appeared to be intoxicated at the
          time of the collision.
        </p>
        <h2>Seeking Bike Injury Compensation</h2>
        <p>
          Very often, bicycle accidents result in serious injuries for the
          cyclist as opposed to occupants of a car. Bicyclists are exposed to
          the roadway and the elements. Some of the common injuries sustained in
          bike accidents include head injuries, broken bones, internal injuries
          and spinal cord damage.
        </p>
        <p>
          Anybody who has been injured in a Brea bike accident would be well
          advised to take a number of steps to help protect his or her rights.
        </p>
        <ul>
          <li>First, contact the authorities right away.</li>
          <li>It will be important to get the facts of the case on record.</li>
          <li>
            You will also want to get the contact information for the other
            parties involved and from anyone who may have witnessed the crash.
          </li>
          <li>
            During this time, do not admit fault for the crash. Simply collect
            information and wait for an officer to arrive at the crash site.
          </li>
          <li>
            If you have a phone that can take photos, photograph the crash site,
            your bicycle, any vehicles involved and any injuries you may have
            suffered.
          </li>
          <li>
            Once you have given your information for the police report, it is
            important that you seek out immediate medical attention, which will
            help maximize your chances for a complete recovery.
          </li>
        </ul>
        <p>
          To learn more or to see if you have a case,{" "}
          <Link to="/contact">contact a Brea bike accident lawyer</Link> at
          949-203-3814.
        </p>
        <p>&nbsp;</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
