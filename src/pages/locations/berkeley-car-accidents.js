// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Berkeley",
      population: 116768,
      totalAccidents: 4540,
      intersection1: "University Ave & 6th St",
      intersection1Accidents: 47,
      intersection1Injuries: 39,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 388.2,
      city1Name: "Albany",
      city1Index: 206.0,
      city2Name: "Kensington",
      city2Index: 124.1,
      city3Name: "Emeryville",
      city3Index: 1068.2,
      city4Name: "El Cerrito",
      city4Index: 377.2
    },
    locationBox3: {
      intersection2: "Shattuck Ave & University Ave",
      intersection2Accidents: 60,
      intersection2Injuries: 30,
      intersection2Deaths: 0,
      intersection3: "Martin Luther King Jr Way & University Ave",
      intersection3Accidents: 43,
      intersection3Injuries: 27,
      intersection3Deaths: 1,
      intersection4: "University Ave & San Pablo Ave",
      intersection4Accidents: 48,
      intersection4Injuries: 29,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Berkeley Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Berkeley Car Accident Lawyer. Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Berkeley Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Berkeley, CA has a high population density and a lot of small streets
          and tight intersections, which can lead to dangerous car accidents,
          especially late at night. Berkeley also has a high crime rate and is
          bordered by emeryville, which has an even higher crime index according
          to city-data.com.{" "}
        </p>
        <p>
          If you've been injured in a Barkeley car crash,{" "}
          <Link to="/contact"> contact</Link> Bisnar Chase Personal Injury
          Attorneys. We'll tell you if you have a case and can get compensation
          for your injuries and suffering, and we'll walk with you every step of
          the way so you will not have to worry about
        </p>
        <p>
          Berkeley in California's Alameda County has been plagued with a
          notoriously dangerous intersection that has hundreds of pedestrians,
          bicyclists, motorcycles and cars struggling through a traffic gauntlet
          to avoid car crashes.
        </p>
        <p>
          The intersection of Gilman Street and Interstate 80 in West Berkeley
          has traffic coming from 14 directions and is controlled entirely by
          stop signs. Called "suicide alley" and "no man's land" by motorists
          and bicyclists alike, many avoid this intersection altogether.
          Understandable, since it averages 11 major car accidents annually --
          54 in the last five years. Undeterred, at least 20,000 cars make their
          way through this intersection every day, as do hundreds of bicyclists
          and pedestrians who take their chances in these crossroads.
        </p>
        <p>
          "With heavy traffic volumes funneling cars, bicyclists and pedestrians
          into poorly regulated cross roads, alternatives must be explored to
          reduce car crashes," stated car accident lawyer John Bisnar.
        </p>
        <p>
          Things could go from bad to worse if the problem remains unsolved.
          Traffic studies indicate that at least 27,000 daily motorists are
          expected to pass through the intersection in the next 20 years. While
          some have proposed stoplights be installed to help regulate traffic,
          others insist a red light would back up traffic onto the freeway and
          across the train tracks.
        </p>
        <p>
          The bold solution recently proposed by city planners to cut down on
          car collisions at this intersection is a double roundabout (often
          called a traffic circle), one on each side of the highway. Stop signs
          would be removed and traffic would flow continuously in a circle.
          Widely used in Europe, the project would be a novel approach for the
          Bay Area. While there have been complaints that traffic circles are
          confusing, Berkeley traffic engineers prefer roundabouts over traffic
          lights, which they claim could create traffic jams during peak hours.
          This is one solution, they say, could reduce many car crashes at the
          accident-prone intersection.
        </p>
        <p>
          For even greater safety, the roundabouts project would include a
          mixed-use path for cyclists and pedestrians on the north side of the
          traffic circles. Since a nearby sports field draws many young
          children, pedestrians and cyclists, the separate path could provide an
          added level of safety.
        </p>
        <p>
          "Roundabouts can be useful in reducing car accidents at high traffic
          intersections, since they eliminate the dangerous right-angle
          collisions and red-light runner car crashes that can prove so lethal
          in regular crosswalks," noted John Bisnar, the well-known{" "}
          <Link to="/car-accidents"> car accident lawyer</Link> of BISNAR CHASE.{" "}
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
