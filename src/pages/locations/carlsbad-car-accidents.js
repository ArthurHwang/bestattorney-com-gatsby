// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Carlsbad",
      population: 110972,
      totalAccidents: 3461,
      intersection1: "Paseo Del Norte & Palomar Airport Rd",
      intersection1Accidents: 72,
      intersection1Injuries: 55,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 155.2,
      city1Name: "Encinitas",
      city1Index: 150.3,
      city2Name: "Vista",
      city2Index: 247.8,
      city3Name: "Oceanside",
      city3Index: 248.4,
      city4Name: "San Marcos",
      city4Index: 175.5
    },
    locationBox3: {
      intersection2: "Palomar Airport Rd & Melrose Rd",
      intersection2Accidents: 56,
      intersection2Injuries: 51,
      intersection2Deaths: 0,
      intersection3: "Palomar Airport Rd & El Camino Real",
      intersection3Accidents: 65,
      intersection3Injuries: 45,
      intersection3Deaths: 0,
      intersection4: "Avenida Encinas & Palomar Airport Rd",
      intersection4Accidents: 40,
      intersection4Injuries: 26,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Carlsbad Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Carlsbad Car Accident Lawyers. We specialize in catastrophic injury car accidents. Over $500 Million won."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Carlsbad Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Palomar Airport Rd is one of the most dangerous roads in Carlsbad,
          having seen 784 accidents from 2010-2015, and 602 injuries from those
          accidents. As a result, intersections with Palomar Airport Rd,
          especially around highway 5, should be driven cautiously. Carlsbad has
          an average population density, but that doesn't mean that there aren't
          going to be some reckless drivers that cause crashes. Bisnar Chase{" "}
          <Link to="/car-accidents">Car Accident Attorneys </Link>represnt
          victims who have been injured by reckless drivers or DUIs. Contact us
          today to see if you have a case!
        </p>

        <p>
          Carlsbad in San Diego County, California is taking a hard look at
          improving life for pedestrians. The seaside community has many
          joggers, bicyclists and walkers who are at increased risk of ending up
          in a car accident.
        </p>
        <p>
          "Thousands of people are killed and injured each year in car
          collisions involving pedestrians," said nationally recognized
          pedestrian and car accident attorney John Bisnar. Many of these
          accidents are preventable if both motorist and pedestrian simply use
          more caution and obey traffic laws."
        </p>
        <p>
          The City of Carlsbad's Pedestrian Master Plan uncovered a number of
          traffic conditions, followed by recommendations to cut down on the
          number of car accidents:
        </p>
        <ul>
          <li>
            Pacific Coast Highway south of Avenida Encinas is a problem area.
            Angled parking, high-speed traffic, and no sidewalks, cause
            pedestrians to walk in the bike lane.
          </li>
          <li>
            A pathway is needed between Laguna Drive and the City of Oceanside
            (Eaton Street). Children cross there regularly.
          </li>
          <li>
            A four-way stop controlled intersection is needed at Buena Vista
            Circle and Laguna Drive.
          </li>
          <li>
            A Chestnut Avenue pedestrian crossing is needed to get past the
            railroad tracks.
          </li>
          <li>
            Madison Street and Roosevelt Street are too wide. Traffic calming is
            needed to reduce speeds.
          </li>
          <li>
            Carlsbad Boulevard is very unsafe and not traversable on foot
            because of high vehicle speeds.
          </li>
          <li>
            The intersections of Laguna Drive & Madison Street and Laguna Drive
            & Roosevelt Street are not safe for crossing.
          </li>
          <li>
            There is a lack of traffic control on Carlsbad Boulevard between
            Pine Avenue and Tamarack Avenue.
          </li>
          <li>
            The Carlsbad Village Road/I-5 off ramps are dangerous, since
            motorists don't look before turning off the freeway.
          </li>
          <li>
            Cars are too close to the dirt walking areas along Carlsbad
            Boulevard, south of Cannon Road and north of Tamarack Avenue, which
            presents a dangerous pedestrian environment.
          </li>
          <li>
            A signalized pedestrian crosswalk is needed on Carlsbad Boulevard
            between Tamarack Avenue and Pine Avenue.
          </li>
          <li>
            A pedestrian crossing is needed between Tamarack Avenue and Pine
            Street along Carlsbad Boulevard. The pedestrian crossing should be
            lit so motorists can see pedestrians at night.
          </li>
        </ul>
        <p>
          Overall statistics suggest that pedestrians fare poorly in car
          collisions:
        </p>
        <ul>
          <li>A pedestrian is killed in the U.S. every 99 minutes.</li>
          <li>
            600 pedestrian fatalities and 13,000 injuries occur every year in
            California.
          </li>
          <li>
            28 percent of California's pedestrian deaths occur in San Diego
            County.
          </li>
          <li>
            Only half of the pedestrians struck by a motor vehicle going 30 mph
            survive, and just 15 percent survive if the vehicle is traveling at
            40 mph.
          </li>
        </ul>
        <h2> Contacting a California Car Accident Attorney</h2>
        <p>
          Be sure to consult with a California car accident lawyer that is
          familiar with Carlsbad. You'll want your attorney to be very familiar
          with the courts, insurance companies and defense lawyers in the area.
          Even the smallest mistake can cost you thousands of dollars in
          recovery. Bisnar Chase Personal Injury Attorneys have represented
          California residents for over 35 years. We've taken on some of the
          toughest cases with <Link to="/case-results">winning results.</Link>{" "}
          Call for a free consultation with an experienced Carlsbad car accident
          attorney at 800-561-4887.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
