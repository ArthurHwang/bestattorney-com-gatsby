// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Norwalk",
      population: 106589,
      totalAccidents: 4240,
      intersection1: "Rosecrans Ave & Studebaker Rd",
      intersection1Accidents: 108,
      intersection1Injuries: 39,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 242.0,
      city1Name: "Santa Fe Springs",
      city1Index: 295.0,
      city2Name: "Artesia",
      city2Index: 235.5,
      city3Name: "Cerritos",
      city3Index: 232.5,
      city4Name: "Bellflower",
      city4Index: 249.8
    },
    locationBox3: {
      intersection2: "Alondra Blvd & Pioneer Blvd",
      intersection2Accidents: 85,
      intersection2Injuries: 41,
      intersection2Deaths: 0,
      intersection3: "Alondra Blvd & Studebaker Rd",
      intersection3Accidents: 103,
      intersection3Injuries: 34,
      intersection3Deaths: 0,
      intersection4: "Rosecrans Ave & Bloomfield Ave",
      intersection4Accidents: 98,
      intersection4Injuries: 31,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Norwalk Car Accident Lawyer - Bisnar Chase"
        pageDescription="Injured in a Norwalk Car Accident? Let Bisnar Chase fight to get you fairly compensated for your medical bills and general damages!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Norwalk Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Norwalk has a surprisingly high population density</strong>{" "}
          for a Southern California city, which means there will constantly be
          more cars on the road and an increased danger of car accidents. One of
          the most dangerous intersections in Norwalk is at Rosencrans Ave and
          Studebaker Rd, where the speed limit is 40mph on both streets, but
          cars often go much faster. This has resulted in many accidents over
          the past few years. <Link to="/car-accidents">Car accidents</Link> can
          result in catastrophic injury or even death, and usually bring about
          huge amounts of stress for victims who have been injured by negligent
          drivers. Bisnar Chase can alleviate that stress and make your claims
          process easy! Contact us to see if you have a case!
        </p>
        <p>
          In case after case, the most experienced Norwalk auto accident
          attorneys have surmised that when it comes to declaring medical losses
          after a car crash -- honesty and accuracy are crucial. Getting fair
          compensation for your medical expenses can sometimes be a challenge.
          But if you drive in Norwalk, California, you face traffic challenges -
          and the possibility of a car accident - on a daily basis.
        </p>
        <h2>Do not Confuse Prior Injuries with Current Car Accident Claims</h2>
        <p>
          The most trusted accident injury lawyers say never exaggerate your
          medical losses. And if you suffer from a prior medical condition,
          don't link that condition with current accident claims. If you do, the
          claims adjuster or defense attorney will discredit your claim and
          refuse to believe anything you say about your injuries and treatment.
          This will result in your case falling out of negotiations with the
          insurance company, and move it to trial before you can secure anything
          resembling a "fair" recovery.
        </p>
        <p>
          The most successful Norwalk car collision lawyers advise their clients
          to keep a written record of their medical expenses and to list only
          those that pertain to their car crash. Observing these simple rules
          can substantially boost your credibility and make it far more likely
          that you'll receive a settlement that is both fair and equitable.
        </p>
        <h2>Look for a Trustworthy, Experienced Car Accident Lawyer</h2>

        <p>
          For more advice, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar. The book is free
          to accident victims - just contact us to get a copy sent to you, or
          you can buy one at Amazon.com.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
