// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Los Altos",
      population: 30010,
      totalAccidents: 504,
      intersection1: "El Camino Real & San Antonio Rd",
      intersection1Accidents: 43,
      intersection1Injuries: 26,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 91.4,
      city1Name: "Mountain View",
      city1Index: 161.4,
      city2Name: "Los Altos Hills",
      city2Index: 74.5,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Sunnyvale",
      city4Index: 125.0
    },
    locationBox3: {
      intersection2: "Foothill Expwy & El Monte Ave",
      intersection2Accidents: 42,
      intersection2Injuries: 25,
      intersection2Deaths: 0,
      intersection3: "San Antonio Rd & Foothill Expwy",
      intersection3Accidents: 29,
      intersection3Injuries: 24,
      intersection3Deaths: 0,
      intersection4: "El Camino Real & Jordan Ave",
      intersection4Accidents: 18,
      intersection4Injuries: 14,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Altos Car Accident Lawyer - California Auto Accident Attorney"
        pageDescription="Call 949-203-3814 for highest-rated accident injury lawyers, serving Los Altos, Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Altos Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Although Los Altos has an extremely low rate of injuries from auto
            accidents,
          </strong>{" "}
          there are still about 100 injuries every year in the city's 6.35
          square mile area. Car accident injuries are often severe and can
          require thousands of dollars in medical payments. If you want someone
          to walk along side you to make sure you get compensated for your
          medical bills and lost wages, contact Bisnar Chase! We represent
          injured victims in Norcal and all over California. Fill out our form
          or call to see if you have a case with us!
        </p>
        <p>
          Known for its friendly atmosphere and its jogging/biking routes, Los
          Altos has had its share of car accidents. A recent four-year study
          conducted by the Los Altos Traffic Commission analyzed 1,468 traffic
          collisions in the city. The results showed that a large percentage of
          traffic, injury and bicycle/pedestrian collisions occurred at the
          intersections of El Camino Real (State Highway 92) and San Antonio Rd
          (a County arterial roadway). In many instances these accidents were
          caused by heavy high traffic and speeding motorists.
        </p>
        <p>
          Bicyclists suffered 66 accidents and pedestrians were involved in 26.
          Most bicycle accidents happened on Foothill Expressway and El Camino
          Real. The greatest number of bicycle/pedestrian accidents occurred on
          Fremont Avenue, San Antonio Road, Los Altos Avenue and El Monte
          Avenue. Motor vehicles failing to yield the right-of-way caused most
          of the bicycle accidents, which resulted in injuries. The pedestrian
          accidents occurred while people were in a crosswalk. Six bicycle
          accidents involved school-aged children during the school commute,
          although none of these accidents occurred where school crossing guards
          were on watch.
        </p>
        <p>
          For more information, read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar. The book is free
          to accident victims -just contact us to get a copy sent to you, or you
          can buy one at Amazon.com. Please call our{" "}
          <Link to="/car-accidents">California car accident lawyers</Link> for a
          free consultation at 949-203-3814.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
