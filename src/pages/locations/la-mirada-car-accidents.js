// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Mirada Car Accident Lawyer - Bisnar Chase"
        pageDescription="Bisnar Chase is a Car Accident Attorney serving La Mirada and LA County. We fight along side you to get you the best settlement from the insurance companies. Call us today!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Mirada Car Accidents and Statistics</h1>
        <BreadCrumbs location={location} />

        <h2>La Mirada Car Accident Attorneys</h2>

        <p>
          Bisnar Chase is a car accident victim's advocate. We know that
          accidents can be difficult to go through, and can be even more
          frustrating when you're injured and have to deal with your recovery
          from injuries at the same time as your communication with insurance
          companies, medical billing companies, and your doctors or therapists.
          That's where Bisnar Chase steps in. Hiring a personal injury attorney
          can help you by allowing you to heal and recover while your attorney
          does the work for you, and negotiates the best settlement from the
          insurance companies. To find out more, call us to learn more about
          your options as an injured victim!{" "}
          <div className="centered">
            {" "}
            <Link to="/los-angeles/contact"></Link>
          </div>
        </p>

        <h2>Encouraging Car Accident Statistics</h2>

        <p>
          La Mirada is a surprisingly safe place when it comes to car accidents
          and alcohol-related driving incidents. Throughout California, there is
          an average of 1 fatality in every 193 car accidents. In La Mirada,
          however, there is one death in every 584 car accidents, meaning that
          La Mirada residents are 3 times less likely to be killed in a car
          accident. This statistic continues for DUIs. 11% of all accidents in
          California are due to driving under the influence. This statistic is
          7% in La Mirada, resulting in far fewer DUI deaths.{" "}
        </p>

        <p>
          One interesting piece of information to note is that truck accidents
          make up 8% of all car accidents in La Mirada. This is most likely due
          to the fact that La Mirada lies directly off of Highway 5 – a road
          used frequently by truckers travelling up and down California.
          According to the National Bridge Inventory, an average of 34,000
          trucks pass through La Mirada every day, increasing the danger of
          truck accidents. Fortunately, less than half a percent of truck
          accidents actually result in a fatality.
        </p>

        <h2>Car Accident Injuries Still Happen in La Mirada</h2>

        <p>
          Despite the safer environment in La Mirada compared to the rest of
          California, there were still over 1,300 injuries from accidents in the
          last 5 years. This leads to a lot of people trying to recover
          compensation for medical bills and property damage, which can be
          difficult when dealing with an insurance company for the first time.
          Often, insurance adjusters will have a friendly, helpful demeanor,
          while also trying to compensate you the least amount possible for your
          losses. Adjusters are naturally trained to increase their company
          profits, leading to some adjusters trying to extract information from
          you that may be harmful to your claim. For this reason, it’s important
          to hire an attorney to help you negotiate with these adjusters.{" "}
          <Link to="/locations/la-mirada-personal-injury-attorneys">
            La Mirada Personal Injury Attorneys
          </Link>{" "}
          like Bisnar Chase are experienced in dealing with adjusters for car
          accidents and know the tricks they use to get away with paying out a
          lower claim. Our job is to comb through all of the facts of the
          accident to make sure that we’re getting you the highest settlement
          possible. For example, take a look at a case of{" "}
          <Link to="/case-results/rickey-prock">
            one of our clients, Rickey Prock.
          </Link>{" "}
          Prock was catastrophically injured in a motorcycle accident and was
          offered $15,000 by a lawyer who didn’t fully examine the situation.
          When Prock went to Bisnar Chase, we did some research and determined
          that the driver who hit Prock was on the clock working for a
          corporation at the time, so corporation was responsible to pay for the
          damages of the driver. We ended up getting Rickey Prock 3 million
          dollars to take care of the surgeries and physical therapy required to
          heal him. Learn more about{" "}
          <Link to="/case-results">other case results</Link> from Bisnar Chase!
        </p>

        <h2>See If You Have A Case!</h2>

        <p>
          This is the way we treat all of our clients – we make sure that we
          don’t miss anything so that our clients’ needs can be fully taken care
          of, while keeping the stress level of the injured victims at a
          minimum. We take care of paperwork and negotiations and allow our
          clients to just focus on healing physically and psychologically. If
          you feel like you need a lawyer to help you deal with this difficult
          time in your life,{" "}
          <Link to="/los-angeles/contact">contact Bisnar Chase</Link> today!
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
