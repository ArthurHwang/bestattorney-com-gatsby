// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Juan Capistrano Car Accident Statistics - Bisnar Chase"
        pageDescription="San Juan Capistrano car acident lawyer discusses traffic and statistics in the city."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1> San Juan Capistrano Car Accident Lawyer Discusses Statistics</h1>
        <BreadCrumbs location={location} />
        <p>
          A skilled{" "}
          <Link to="/locations/san-juan-capistrano-car-accidents">
            San Juan Capistrano accident lawyer
          </Link>{" "}
          appreciates the value of traffic conditions and accident statistics
          when evaluating a car collision case for an injured client. Smart
          lawyers are fully aware that San Juan Capistrano, California has a
          special set of traffic problems that contribute to its car crashes.
        </p>
        <h2>San Juan Capistrano's Car Accident Statistics Are Alarming</h2>
        <p>
          "Knowledgeable San Juan Capistrano car crash lawyers know that, as
          recently as 2006, two people died and 125 were injured in San Juan
          Capistrano car collisions," notes John Bisnar. "They know that
          pedestrian vs. car crashes killed two and injured four; that bicycle
          vs. car accidents injured five, motorcycle collisions injured six, and
          drunk drivers were responsible for one death and 21 injuries."
        </p>
        <h2>San Juan Capistrano's Traffic Problems Cover the City</h2>
        <p>
          The best San Juan Capistrano car accident lawyers are also keenly
          aware of San Juan Capistrano's many road dangers and traffic problems.
          They know, for example, that the extremely heavy traffic on Ortega
          Highway -- particularly where Ortega Highway intersects with I-5 --
          has become a serious choke point, creating substantial delays,
          congestion and the potential for car collisions. They also know that
          backed up traffic at the Camino Capistrano exit of the southbound I-5
          freeway often creates a dangerous condition for both drivers and
          pedestrians. And most knowledgeable San Juan Capistrano car crash
          lawyers realize that congested traffic in and around Mission San Juan
          competes with bicyclists and endless pedestrian tourists, creating
          more potential for accidents. Lastly, most every San Juan car accident
          lawyer knows about the young bicyclist who lost his leg as he tried to
          cross San Juan Creek Road and Paseo Christina -- the heavily overgrown
          vegetation in the median of the intersection was the subject of a
          lawsuit that cost the city millions of dollars.
        </p>
        <h2>A Skilled Car Accident Lawyer Can Assist You</h2>
        <p>
          If you become injured as the result of a no fault car crash, you
          should consult an experienced car collision lawyer who is familiar
          with the city, its traffic conditions and car accident profile. A
          trustworthy personal injury lawyer can advise you of your many rights
          as an accident victim. The best car accident lawyers offer free, no
          obligation consultations to accident victims.{" "}
          <strong>
            Please call our San Juan Capistrano car accident lawyers at
            949-203-3814.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
