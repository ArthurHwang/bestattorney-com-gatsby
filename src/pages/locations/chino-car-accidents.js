// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Chino",
      population: 80988,
      totalAccidents: 3414,
      intersection1: "Central Ave & Philadelphia St",
      intersection1Accidents: 134,
      intersection1Injuries: 61,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 224.1,
      city1Name: "Chino Hills",
      city1Index: 100.0,
      city2Name: "Montclair",
      city2Index: 395.6,
      city3Name: "Ontario",
      city3Index: 236.9,
      city4Name: "Pomona",
      city4Index: 345.4
    },
    locationBox3: {
      intersection2: "Riverside Dr & Ramona Ave",
      intersection2Accidents: 97,
      intersection2Injuries: 46,
      intersection2Deaths: 0,
      intersection3: "Euclid Ave & Kimball Ave",
      intersection3Accidents: 73,
      intersection3Injuries: 50,
      intersection3Deaths: 0,
      intersection4: "Euclid Ave & Pine Ave",
      intersection4Accidents: 72,
      intersection4Injuries: 53,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Chino Car Accident Lawyer - Bisnar Chase"
        pageDescription="Call 800-561-4887 for the highest rated Chino car accident attorneys, serving California. Best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Chino Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          A skilled{" "}
          <Link to="/san-bernardino/car-accidents">
            Chino car accident attorney{" "}
          </Link>{" "}
          will tell you that car accident victims often make costly mistakes
          when dealing with doctors, chiropractors and other medical providers.
          One major mistake often made is signing a medical lien for medical
          care. Bisnar Chase Personal Injury Attorneys are experienced and can
          help you make the best decisions when dealing with your medical
          providers and insurance companies. Contact us today to see if you have
          a case!
        </p>
        <h2>Car Accidents in Chino</h2>
        <p>
          Most auto accidents in Chino result in minor to major injuries. In the
          last five years there have been 134 accidents and 61 injuries at the
          busy intersections of Central and Philadelphia. Riverside and Ramona
          intersections are another high accident area having 97 car accidents
          with almost half resulting in injury. San Bernardino has approx. 2
          million residents and local access to casinos and amphitheaters make
          for hectic roadways.
        </p>
        <h2> Don't Make Mistakes</h2>
        <p>
          {" "}
          A skilled car accident attorney will tell you not to do anything with
          your case before getting the advice of a local experienced personal
          injury lawyer. Mistakes made during the initial process can cost you
          dearly. Areas of negotiation that you might not see is where a skilled
          attorney dealing with car accidents will know what to do to protect
          you.{" "}
        </p>
        <h2>Medical Liens Tend to Over Treat and Over Charge Patients</h2>
        <p>
          {" "}
          Car accident injury victims need to choose their medical providers
          prudently. "Car crash victims can be over charged for medical
          services, especially if they sign a medial lien," says John Bisnar.
          "Doing so means doctors are paid directly from the compensation amount
          when a case finally settles. This can be a very enticing deal for the
          medical provider." The reason for this is that doctors and
          chiropractors will often work together to "over treat" a patient and
          enlarge the final medical bill with unnecessary and high priced
          procedures. They will also stall and evade requests for copies of
          billing statements and other medical records. When the case settles,
          they're first in line to collect their bills, leaving little left over
          for the accident victim's other expenses.
        </p>
        <p>
          The other problem with medical liens is that they can be very tempting
          for cash-strapped accident victims, especially if they lack proper
          medical insurance. With a medical lien, bills can be deferred for as
          long as the trial or settlement negotiations continue. "In some
          instances, arbitrators have been asked to reduce the inflated amounts
          charged by the medical providers," notes John Bisnar. "But rather than
          agree to the reduced amounts, the lien providers sued their patients
          for the original charges, pointing back to the medical liens, which
          their patients had signed months or even years earlier."
        </p>
        <h2>
          Seek the Advice of a Car Accident Lawyer Regarding Medical Liens
        </h2>
        <p>
          An experienced Chino car crash lawyer can help guide you on the best
          way to deal with medical care costs after a car collision injury. Some
          of the best lawyers offer{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            no charge, no pressure consultations.
          </Link>{" "}
          They will help you avoid costly mistakes and ensure your are fairly
          compensated for your various losses and injuries. Call for a free
          consultation at 800-561-4887.
        </p>
        <ul>
          <li>96% Success Rate</li>
          <li>Serving San Bernardino for Over 35 Years</li>
          <li>Superb 10+ AVVO Rated Lawyers</li>
          <li>Top 100 Trial Lawyers in America, ATLA - Since 2007</li>
          <li>2015 NADC Top One Percent Lawyers</li>
          <li>Highest Rated SuperLawyers</li>
          <li>Best Client Satisfaction Guaranteed</li>
          <li>$300M+ In Wins for Our Clients</li>
          <li>Years of Trial Experience</li>
          <li>Familiar with California Courts & Defense Teams</li>
        </ul>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
