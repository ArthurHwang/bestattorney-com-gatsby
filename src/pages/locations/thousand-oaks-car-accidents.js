// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Thousand Oaks",
      population: 128731,
      totalAccidents: 4979,
      intersection1: "Moorpark Rd & Thousand Oaks Blvd",
      intersection1Accidents: 131,
      intersection1Injuries: 56,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 90.8,
      city1Name: "Westlake Village",
      city1Index: 115.6,
      city2Name: "Moorpark",
      city2Index: 88.4,
      city3Name: "Agoura Hills",
      city3Index: 109.3,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Moorpark Rd & Hillcrest Dr",
      intersection2Accidents: 113,
      intersection2Injuries: 52,
      intersection2Deaths: 0,
      intersection3: "Janss Rd & Moorpark Dr",
      intersection3Accidents: 99,
      intersection3Injuries: 56,
      intersection3Deaths: 0,
      intersection4: "Westlake Blvd & Thousand Oaks Blvd",
      intersection4Accidents: 95,
      intersection4Injuries: 27,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Thousand Oaks Car Accident Lawyer - 323-238-4683"
        pageDescription="call for top-rated Thousand Oaks car accident lawyers. Get a Free Consultation today. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Thousand Oaks Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Protect yourself after a no fault car accident by consulting the
          Thousand Oaks car accident attorneys of Bisnar Chase. We are
          recognized as the Top 1% of Attorneys in California and have won over
          $500 Million for our clients in judgments, verdicts and settlements.
        </p>
        <h2>Avoid Rushing Into a Hasty and Unfair Settlement</h2>
        <p>
          Any seasoned Thousand Oaks car accident attorney will admit that you
          don't have to retain a car collision lawyer for every car accident.
          Yet virtually all lawyers will advise those who have been injured in a
          car crash to a least consult a car accident lawyer before agreeing to
          a quick insurance settlement.
        </p>

        <h2>Don't be in Rush to Settle with the Insurance Company</h2>
        <p>
          Knowledgeable Thousand Oaks car accident lawyers know that when people
          are injured in a car accident, they want to settle with their
          insurance company as fast as possible. This is understandable.
          Confronted with mounting bills, weeks and often months out of work,
          they need cash. But as difficult as it may be to wait, it's typically
          better to fully settle a claim than to accept a fast settlement. The
          simple reason is that losses and{" "}
          <Link to="/catastrophic-injury">
            injuries suffered in a car accident
          </Link>{" "}
          may not surface for months, sometimes even years later.
        </p>
        <h2>Get a Thorough Medical Diagnosis Before You Sign a Release</h2>
        <p>
          Knowing this, insurance adjustors will happily agree to a fast
          settlement. They know that the sum you receive will be less than the
          insurance company has set aside for your claim. All the more reason to
          undergo a complete and thorough medical diagnosis of your injuries
          before you sign any insurance release forms. A savvy Thousand Oaks car
          crash lawyer knows that many car accident injuries take time to reveal
          the full extent of their damage. Once the injury fully manifests
          itself--sometimes as a permanent disability--the car accident lawyer
          can build a solid case to ensure you are justly compensated for the
          full extent of your loss.
        </p>
        <h2>Insurance Companies Want to Avoid Long-Term Damage Claims</h2>
        <p>
          For example, if you should suffer a back injury in a car accident, the
          long-term damage and problems you may encounter are typically far more
          difficult to determine. You could require multiple medical procedures,
          long-term care and/or expensive prosthetics. So it's wise to wait as
          long as possible before negotiating a final settlement. Insurance
          adjustors realize this, so the sooner they settle with you, the lower
          the compensation for your losses. One begins to see why the advice of
          an experienced Thousand Oaks car collision lawyer can be so valuable.
        </p>
        <p>
          The insurance industry's own research reveals that car accident injury
          victims who retain the services of a lawyer get far greater settlement
          sums than those who do not. This is true even after legal fees have
          been paid to the lawyer.
        </p>
        <p>
          If you need legal assistance, please call us for a free consultation.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
