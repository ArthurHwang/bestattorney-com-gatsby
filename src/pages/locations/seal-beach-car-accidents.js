// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Seal Beach",
      population: 24605,
      totalAccidents: 1540,
      intersection1: "Westminster Ave & Seal Beach Blvd",
      intersection1Accidents: 107,
      intersection1Injuries: 54,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 113.0,
      city1Name: "Los Alamitos",
      city1Index: 204.5,
      city2Name: "Cypress",
      city2Index: 126.8,
      city3Name: "Hawaiian Gardens",
      city3Index: 198.4,
      city4Name: "Westminster",
      city4Index: 229.8
    },
    locationBox3: {
      intersection2: "Seal Beach Blvd & Golden Rain Rd",
      intersection2Accidents: 58,
      intersection2Injuries: 35,
      intersection2Deaths: 0,
      intersection3: "Seal Beach Blvd & Old Ranch Pkwy ",
      intersection3Accidents: 61,
      intersection3Injuries: 28,
      intersection3Deaths: 0,
      intersection4: "Seal Beach Blvd & St Cloud ",
      intersection4Accidents: 30,
      intersection4Injuries: 15,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Seal Beach Car Accident Lawyers - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Seal Beach car accident attorneys.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Seal Beach Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Seal Beach is a sleepy beach town with tiny streets and limited
          parking. Its boutiques and fine dining are a draw for many people, but
          that popularity causes a lot of traffic issues. When divided by the
          population of a city, Seal Beach has the{" "}
          <Link to="/resources/car-accident-statistics">
            highest rate of accidents out of any other city in Orange County
          </Link>{" "}
          at 168.03 accidents for every 1,000 people living there, from
          2009-2014. Seal Beach pedestrian accidents are also quite common as
          are hundreds of bumper taps in crowded parking areas. Our law firm has
          been representing clients in Seal Beach for over 35 years. We've
          collected hundreds of millions for our clients and have a 96% success
          rate. Call our office for a free consultation with a Seal Beach car
          accident lawyer.
        </p>
        <p>
          A respected Seal Beach{" "}
          <Link to="/car-accidents"> car accident attorney </Link> will realize
          that accident statistics and traffic problems are a factor when one
          seeks to understand the "how and why" of a car crash. Seasoned car
          accident lawyers know that Seal Beach, California has unique traffic
          concerns that can play a role in a car accident settlement for
          injuries and losses.
        </p>
        <h2>Seal Beach Has Special Traffic Concerns</h2>
        <p>
          Successful Seal Beach car accident lawyers are also aware of this
          city's many traffic concerns. They know, for example, that Pacific
          Coast Highway cuts through Seal Beach and that it is frequently the
          location of accidents caused by drunks who drive too fast. They know,
          too, that PCH has no barriers separating cars approaching each other
          from opposite directions, so motorists who lose control of their
          vehicles sometimes cross over into oncoming traffic with devastating
          results. PCH is a busy six-lane state highway that can be very
          difficult to cross for bicyclists and pedestrians.
        </p>
        <p>
          Another problem Seal Beach car collision lawyers are familiar with is
          the over preponderance of alcohol establishments on Main Street. These
          can create a potential for car accidents, particularly after midnight,
          when drunks hit the streets.
        </p>
        <p>
          Also potentially dangerous are the wide, curving stretches of Seal
          Beach Boulevard. The street snakes through the Leisure World
          retirement community and a Seal Beach Naval Weapons Station housing
          area. The street serves as the main artery between central Seal Beach
          and the San Diego Freeway, and many motorists speed through it as they
          rush to and from town.
        </p>
        <h2>
          Look to an Experienced Seal Beach Car Accident Lawyer for Assistance
        </h2>
        <p>
          If you are involved in car accident injury in Seal Beach, consult a
          car crash lawyer who is familiar with the traffic conditions and
          accident statistics of Seal Beach. The most trusted Seal Beach car
          collision lawyers provide free, no obligation consultations to
          accident victims. They can tell you if you have a case and advise you
          how to proceed. The best lawyers work on a contingency basis (you pay
          their fees only when the case settles or you win in court).
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
