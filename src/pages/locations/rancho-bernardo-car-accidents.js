// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rancho Bernardo Car Accident Lawyers "
        pageDescription="Contact the Rancho Bernardo Car Accident Lawyers for a free consultation. Over $500 Million won for our clients. Serving California since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rancho Bernardo Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Virtually any Rancho Bernardo{" "}
          <Link to="/car-accidents">car accident law firm</Link> will admit that
          Rancho Bernardo has been the site of some horrific car accidents. As a
          planned community and also part of the city of San Diego, California,
          Rancho Bernardo is situated about 20 miles north-north-east of
          downtown San Diego.{" "}
        </p>
        <p>
          Yet the two areas couldn't be further apart as far as layout, street
          design and cross section of motorists is concerned. Unlike downtown
          San Diego with its nightclubs and rooftop bars, Rancho Bernardo is
          essentially a bedroom community. It's also an area filled with teen
          drivers and winding roads. Combined, they often spell disaster.
        </p>
        <h2>
          Rancho Bernardo Car Accident Claims the Life of Popular Cheerleader
        </h2>
        <p>
          Who in Rancho Bernardo can forget the beautiful 15-year-old
          cheerleader who{" "}
          <Link
            to="https://brittanycurcio.com/pdf_files/RBHS.pdf"
            target="_blank"
          >
            lost her life
          </Link>{" "}
          ? Sadly, the teen snuck out one night for a joy ride, lost control of
          her mother's car and struck the light pole on Bernardo Center Drive
          near Cayenne Ridge Road. The young teen had no driver's license and
          had failed to buckle up. She was hurled from the car and died at the
          scene. Rancho Bernardo car collision lawyers have learned that many
          teens, too eager to get their driver's license, often sneak out
          without their parents' knowledge to go joy riding in the family car.
        </p>
        <h2>Rancho Bernardo Car Collision Kills One and Injures Three</h2>
        <p>
          Another tragic accident killed one person and injured three in a
          violent car crash that blocked several lanes of southbound Interstate
          15 in Rancho Bernardo. On February 16, the multi-vehicle car collision
          occurred just north of Bernardo Center Drive, according to the
          California Highway Patrol.
        </p>
        <h2>
          Red-Light Cameras May Reduce Likelihood of Intersection Car Crash
        </h2>
        <p>
          Working hard to drive down the number of injuries and fatalities
          resulting from car crashes at intersections, community officials
          decided to implement photo enforcement technology. They agreed to set
          up red-light cameras on Bernardo Center Drive and Rancho Bernardo Road
          and at Carmel Mountain Road and Rancho Carmel Drive. A consensus of
          Rancho Bernardo car crash lawyers insist that photo enforcement
          systems, when correctly administered, can deter the deadly collisions
          that often occur at troublesome intersections.
        </p>
        <p>
          Across San Diego County, more than 16,000 adult DUI arrests are made
          annually. Most of those so arrested are males--about 13,000 vs. 3,000
          for females. Over 4,000 DUI arrests occur within the city of San
          Diego, which includes Rancho Bernardo. In total, the San Diego County
          Sheriff's Department arrests over 1,500 DUI violators annually, while
          the California Highway Patrol makes over 7,400 impaired-driver
          arrests.
        </p>
        <h2>Get Immediate Legal Help</h2>
        <p>
          Reach us today for a free case evaluation. Our Rancho Bernardo car
          crash attorneys will review the facts of your case and provide a free
          assessment. Call 800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
