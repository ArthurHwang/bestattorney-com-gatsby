// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brea Brain Injury Attorneys & Traumatic Head Injury Lawyers"
        pageDescription="Searching for a Brea brain injury attorney? Call 949-203-3814 for a free consultation. No win, no fee lawyers. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Brea Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          A big part of what Bisnar Chase handles are catastrophic injury cases.
          Over the past 35 years we've helped some amazing people get the
          compensation they deserved after a devastating injury. A brain injury
          is often a silent and complicated medical condition to treat. Care may
          be ongoing for life including rehabilitation. Our{" "}
          <Link to="/head-injury">brain injury attorneys</Link> are not only
          compassionate and caring, but highly skilled in serious injury cases.
        </p>
        <h2>The Silent Injury</h2>
        <p>
          Brain damage consequences can range from minor concussions to a full
          coma from which a victim never emerges.  In between are the many
          levels of brain damage that can cause so much heartache to the victim
          and his or her family.  Each case of traumatic head injury must be
          evaluated individually to appreciate its unique results and the impact
          they have on the families of those who suffer through them.
        </p>
        <p>
          For example, a car accident could result in a victim receiving a sharp
          blow to the head that develops into a serious concussion.  This victim
          may gradually recover from this injury if he is careful not to hit his
          head again and follows the doctor's instructions for after care.  It
          is entirely possible that this victim will not experience life-long
          effects of this accident, although he may have a weak area in his
          brain for the rest of his life that could have serious consequences if
          he is ever involved in another accident.
        </p>
        <p>
          On the other hand, a different victim might receive a relatively minor
          blow to the head in a slip and fall and assure herself and her family
          that she is not &ldquo;really hurt.&rdquo;  She might collapse a few
          days later and die, unaware that the &ldquo;little accident&rdquo; she
          had actually resulted in a serious blood clot to the brain that
          eventually caused a stroke that led to her death.
        </p>
        <p>
          On the face of it, the results of those two types of traumatic injury
          do not seem to match the seriousness of the initial accident.  This is
          one reason why it is so important to seek immediate medical attention
          if you are involved in any type of collision or accident that includes
          a blow to the head, even if you feel no effects of the trauma.
        </p>
        <h2>Passionate Legal Representation</h2>
        <p>
          We've taken on some pretty extraordinary cases. A former client
          suffered a traumatic brain injury on a day like any other day.{" "}
          <Link to="/case-results/christopher-chan">Christopher Chan</Link> was
          just riding his bike home from school when he was struck by a van at
          an intersection and received a traumatic brain injury. Christopher's
          injuries were serious and he was airlifted to a local hospital here in
          California. Bisnar Chase went after the city for a dangerous
          intersection and received a $16M recovery for Chris and his family. We
          understand the heartache of having a loved one injured or killed in a
          catastrophic event. Let our compassionate team help you to get fair
          compensation like we did for Christopher.
        </p>
        <ul>
          <li>96% Success Rate</li>
          <li>Over $500 Million Won For Our Clients</li>
          <li>Serving Orange County since 1978</li>
          <li>2015 NADC Top One Percent Lawyers</li>
        </ul>
        <p>
          Before you speak to any insurance companies or defense lawyers, talk
          with our brain injury attorneys in Brea.  Depending on your prognosis,
          you may need expensive care that must be paid for somehow.  Collecting
          damages may be the only way you can afford proper medical treatment or
          possible lifelong care. Call 949-203-3814 for your{" "}
          <Link to="/contact">free case evaluation.</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
