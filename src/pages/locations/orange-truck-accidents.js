// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange Truck Accident Lawyers - 949-203-3814"
        pageDescription="Contact the Orange Truck Accident Lawyers if you've been injured. Free consultation, 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange Truck Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          If you've been involved in a truck accident please contact our Orange
          truck accident a attorneys to discuss your injuries. Our legal team
          has over three decades of experience dealing with traffic accidents
          and may be able to help you get fair compensation. We can discuss your
          treatment plan, medical bills and lost wages and develop a legal
          strategy. We are trial lawyers with decades of courtroom experience.
          Call 949-203-3814 today.
        </p>
        <h2>Truck Accidents in Orange</h2>
        <p>
          The busy city of Orange, California, with a population of over 140,000
          has several tourist attractions such as the Orange Circle, and is home
          to Chapman University and Santiago Community College. Next door to
          cities like Anaheim and the Angel's stadium, the city of Orange,
          located at the very center of Orange County, has high truck traffic
          flowing through its borders on a regular basis, and often times in
          unsafe conditions. Contacting an Orange{" "}
          <Link to="/truck-accidents">Truck Accident Lawyer</Link> is ideal if
          you were in a truck accident as many times, important rules and
          regulations are neglected without the help of a specialist.
        </p>
        <p>
          On an average day, fifty people are killed or injured in a truck
          accident. In crashes involving a truck or tractor trailer and a
          passenger vehicle, the occupants of the passenger vehicle are 50 times
          more likely to be killed than the truck driver.
        </p>
        <h2>There are various causes of truck accidents, which can include:</h2>
        <ul>
          <li>Long hours in service/ sleepiness</li>
          <li>Intoxicated or distracted Drivers</li>
          <li>Driver error</li>
          <li>Substandard Inspections</li>
          <li>Longer Combination Vehicles</li>
          <li>Hazardous Materials</li>
          <li>Defective parts in the truck</li>
          <li>Missing road signs or dangerous road conditions</li>
        </ul>
        <h2>Truck Injury Representation</h2>
        <p>
          Bisnar Chase represents a wide range of truck accident cases including
          car vs. truck, equipment failure accidents, negligence, and hit and
          run. California truck accident lawyer John Bisnar has the knowledge
          and experience to effectively represent you from beginning to end. We
          will never shift your case to someone else because all of our
          attorneys are trial lawyers with years of experience. If you or
          someone close to you has been injured in an accident involving a truck
          or commercial vehicle, contact our Orange truck accident lawyers
          today. We can help.
        </p>
        <p>Call us for a free consultation at: 949-203-3814.</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
