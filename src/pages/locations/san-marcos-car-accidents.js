// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Marcos",
      population: 89387,
      totalAccidents: 2157,
      intersection1: "W. San Marcos Blvd & S. Rancho Santa Fe Rd",
      intersection1Accidents: 97,
      intersection1Injuries: 50,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 175.5,
      city1Name: "Escondido",
      city1Index: 272.0,
      city2Name: "Vista",
      city2Index: 247.8,
      city3Name: "Carlsbad",
      city3Index: 155.2,
      city4Name: "Encinitas",
      city4Index: 150.3
    },
    locationBox3: {
      intersection2: "S. Rancho Santa Fe Rd & Descanso Ave",
      intersection2Accidents: 43,
      intersection2Injuries: 34,
      intersection2Deaths: 0,
      intersection3: "W. San Marcos Blvd & Bent Ave",
      intersection3Accidents: 39,
      intersection3Injuries: 26,
      intersection3Deaths: 0,
      intersection4: "Via Vera Cruz & W. San Marcos Blvd",
      intersection4Accidents: 38,
      intersection4Injuries: 21,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Marcos Car Accident Lawyers - Bisnar Chase 800-561-4887"
        pageDescription="Call 800-561-4887 for highest-rated San Marcos car accident attorneys. Free no-obligation legal consultations!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Marcos Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          The San Marcos car accident lawyers of Bisnar Chase take on some of
          the toughest cases in California. With over three decades in
          catastrophic injuries, our team has the experience and resources to
          get you fair compensation. Contact us for a free case review today.
        </p>
        <p>
          With a population of nearly 83,000, San Marcos in northern San Diego
          County, California has experienced a number of car crash tragedies. In
          fact, San Marcos{" "}
          <Link to="/car-accidents"> auto accident attorneys </Link> have
          intimated that reckless drivers and dangerous roads are key
          contributors in many car collisions.{" "}
        </p>
        <p>
          As with most cities, dangerous intersections have contributed to some
          tragic San Marcos car crashes. One hazardous intersection can be found
          at San Marcos Blvd. and Rancho Santa Fe Road. Another dangerous
          crossroad is located at San Marcos Blvd. and Acacia Rd. Motorists
          often zip down these roads at high speeds. Still another dangerous
          intersection can be found at San Antonio and Harvey Street.
        </p>
        <p>
          In August 2008, a tragedy occurred at one such intersection. Sadly, a
          19-year-old Encinitas man lost his life at West San Marcos Boulevard
          and Acacia when the Jeep he was a passenger in collided with a 2007
          Honda Pilot turning left onto West San Marcos Boulevard from Acacia
          Drive. The 36-year-old driver of the Jeep was traveling on West San
          Marcos Boulevard when he tried to run a red light. The driver was
          seriously injured and the passenger was killed at the scene. The
          driver of the Honda was treated at the scene for minor injuries.{" "}
        </p>
        <p>
          Part of its proactive efforts to reduce car crashes, San Marcos set up
          a DUI checkpoint at 1600 San Elijo Road on October 31st, 2008. A total
          of 963 vehicles were screened. Three drivers were arrested on
          suspicion of drunk driving, 19 citations were issued and 16 vehicles
          were towed away.{" "}
        </p>
        <p>
          An increasing number of San Marcos car collision lawyers approve of
          sobriety checkpoints as a means to remove{" "}
          <Link to="/dui">intoxicated drivers</Link> from the road and make
          streets safer.{" "}
        </p>

        <p> Contact our San Marcos car accident lawyers at 800-561-4887.</p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
