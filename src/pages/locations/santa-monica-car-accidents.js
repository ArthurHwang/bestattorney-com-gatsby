// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Monica Car Accident Lawyer - Bisnar Chase "
        pageDescription="Call for highest-rated Santa Monica car accident lawyers with over 35 years in personal injury. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Track Accident Expenses, Says Santa Monica Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Keeping an accurate record of car crash expenses is important, say
          Santa Monica <Link to="/car-accidents">car accident lawyers.</Link>{" "}
          Many car cash victims lose sight of this fact. Just as many fail to
          realize the alarming rate of car accidents that occur in Santa Monica,
          California, a city where cars, bicycles and pedestrians often share
          tight spaces.
        </p>
        <h2>Santa Monica is no Stranger to Tragedies</h2>
        <p>
          Statistics compiled as recently as 2006 by the California Highway
          Patrol's Statewide Integrated Traffic Records System (SWITRS) revealed
          that seven people died and 619 were injured in Santa Monica car
          crashes. Five pedestrians were killed and 105 were injured in car
          collisions. A total of 81 bicyclists and were injured. And car
          accidents killed one motorcyclist and injured 27. Drunk drivers caused
          three fatalities and 65 injuries. In 2007, six car collisions killed
          six people.
        </p>
        <h2>Record Car Crash Damages to Ensure Fair Compensation</h2>
        <p>
          An experienced Santa Monica car accident lawyer knows the importance
          of documenting damages that result from a car crash. He or she will
          often tell their clients to save expense receipts and itemize all
          accident-related costs. This includes any expenses incurred months
          after the accident. Your losses may include medical bills, pay stubs
          to show lost wages, and all accident-related expenses. You can even
          include expenses paid by an insurer or health plan. The objective is
          to be fairly compensated for your losses after an accident.
        </p>
        <h2>Compensation for Domestic Services You Can no Longer Perform</h2>
        <p>
          If your car accident leaves you stuck in a wheelchair for months on
          end, you may be reimbursed for domestic services you are unable to
          perform. This would include gardening or landscaping maintenance you
          normally perform at home. By the same token, if your car accident
          injuries prevent you from cooking, cleaning and/or doing the laundry,
          you would be entitled to compensation for housecleaning and prepared
          meals. And if you were the sole source of transportation for taking
          your kids to soccer practice, music or dance lessons, hiring
          transportation services would also be a reimbursable expense.
        </p>
        <p>
          As you can see, the importance of sound, experienced legal advice in
          matters relating to a car collision can be highly beneficial in
          securing fair compensation for your losses. If you are injured in a
          car crash, you should consult a skilled car accident lawyer. The best
          Santa Monica car crash lawyers offer free, no pressure, consultations
          to apprise you of your rights and lay out your options.
        </p>
        <h3>Los Angeles Office</h3>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
