// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Temecula",
      population: 114327,
      totalAccidents: 3792,
      intersection1: "Winchester Rd & Ynez Rd",
      intersection1Accidents: 105,
      intersection1Injuries: 87,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 256.9,
      city1Name: "Santa Clara",
      city1Index: 157.0,
      city2Name: "Mountain View",
      city2Index: 161.4,
      city3Name: "Cupertino",
      city3Index: 87.1,
      city4Name: "Los Altos",
      city4Index: 91.4
    },
    locationBox3: {
      intersection2: "Margarita Rd & Winchester Rd",
      intersection2Accidents: 99,
      intersection2Injuries: 63,
      intersection2Deaths: 0,
      intersection3: "Rancho California Rd & Margarita Rd",
      intersection3Accidents: 63,
      intersection3Injuries: 48,
      intersection3Deaths: 1,
      intersection4: "Jefferson Ave & Winchester Rd",
      intersection4Accidents: 89,
      intersection4Injuries: 46,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Temecula Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Call 800-561-4887 for highest-rated Temecula personal injury attorneys. No win, no-fee lawyers. Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Temecula Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Having a personal injury thrust upon you by a negligent party is
          traumatizing. Where do you turn for help and how will you know that
          you are being fairly compensated? Bisnar Chase Temecula{" "}
          <Link to="/">personal injury attorneys </Link> have client care second
          to none with an A+ BBB rating. We know how devastating a personal
          injury is and will guide you every step of the way.
        </p>
        <ul>
          <li> 96% success rate </li>
          <li> Named Best Place to Work 4 years in a row</li>
          <li> Over $500 Million in wins for our clients</li>
          <li> Named Top 1% of Attorneys </li>
        </ul>
        <p>
          Bisnar Chase Temecula personal injury lawyers are experienced at
          helping victims obtain compensation for serious injuries caused by
          another's negligence or wrong doing.
        </p>
        <p>
          The types of cases we handle include, but are not limited to car
          accidents, premises liability such as a slip and fall, defective
          products, employment discrimination, wrongful termination, job injury,
          and dog bites.
        </p>
        <h2>How Our Temecula Personal Injury Team Works</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/BisnarChasePhotosmall.jpg"
            className="imgleft-fixed"
            alt="temecular car accident attorneys of Bisnar Chase"
            style={{ marginBottom: "2rem" }}
          />
        </LazyLoad>
        <p>
          We have a team of professional and knowledgeable paralegals and
          attorneys that will investigate your case thoroughly and diligently.
          This means we will retrieve all neccessary information needed to
          pursue a settlement for you.
        </p>

        <p>
          We know how difficult it is to do this on your own when you are
          suffering physically, mentally and emotionally from a personal injury
          accident. We might need some small details from you, but our staff
          will be your liason between insurance companies, medical billing, etc.
          We will work on your behalf to seek justice and recover the money that
          you are entitled to.
        </p>

        <h2>No Win, No Fee Lawyers</h2>
        <p>
          When you suffer a serious personal injury the last thing you want to
          worry about is money, especially when you might be out of work for a
          period of time. Peace of mind for our clients is important to us. That
          is why we provide a no-win, no-fee guarantee.{" "}
          <em>
            <strong>
              "We will front all necessary costs of your case and we will
              protect you from having to repay those costs if your case is not
              successful."
            </strong>
          </em>
        </p>
        <h2>Car Accidents are #1 Cause of Personal Injury in Temecula</h2>
        <p>
          The number one personal injury claim in Temecula is an auto accident.{" "}
          <Link to="/locations/temecula-car-accidents">
            Temecula car accident
          </Link>{" "}
          cases make up the majority of personal injury lawsuits. Why? In
          Riverside County alone there are more than 2 million people. The
          freeways are clogged with bumper to bumper traffic during rush hours.
          Distracted driving and drunk driving are responsible for the majority
          of car accident injuries and fatalities and believe it or not, texting
          and driving accidents have surpassed drunk driving accidents.
        </p>
        <div className="featurebox">
          <p align="center">Bisnar Chase has a 96% success rate.</p>
          <p align="center">
            We care about our clients and it shows in the way we handle our
            cases.
          </p>
        </div>
        <p>
          Bisnar Chase Personal Injury Attorneys are more than just experienced
          at winning car accident and auto defect cases. We are passionate about
          making sure the victims get justice and compensation. It is not only
          the victim that suffers, but the family members as well. It is even
          worse when there is a wrongful death due to a drunk driver's
          negligence or a defective auto part that should never have been on the
          market.
        </p>
        <h2>Our Compassion for our Clients Drives our Passion to Win</h2>
        <p>
          We understand your pain and suffering which drives our passion to help
          you get what you deserve. This is what sets us apart from other
          Temecula personal injury lawyers. Just recently our attorneys settled
          a motorcycle accident case for $187,000. The defense attorneys offered
          $25,000. We did not give up.
        </p>
        <p>
          We want our clients to have what they need to be able to live
          according to their circumstance. A person that has to be in a
          wheelchair for life with ongoing medical treatment and nursing care is
          going to need a sufficient amount of money to live. Loss of income
          also comes into play as well as emotional pain and anguish.
        </p>
        <p>
          Every personal injury case is unique and we at Bisnar Chase have a
          deep understanding of the laws that pertain to personal injury
          accident lawsuits. We employ the best staff to work with our clients.
          This is imperative for winning cases and is the reason we have a 97%
          success rate.
        </p>
        <p>
          If you or a family member have been the victim of a personal injury
          and you need legal representation, we can help. Our Temecula personal
          injury lawyers will sit down with you one-on-one and provide a free,
          no-obligation, confidential case evaluation. If you can't get to us,
          we will be more than happy to come to you. Contact us today for help.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
