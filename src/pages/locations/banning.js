// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Banning Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Call 800-561-4887 to reach experienced Banning personal injury lawyers with a history of success. We've recovered hundreds of millions of dollars."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Banning Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Banning <Link to="/">personal injury lawyers</Link> at Bisnar Chase
          have assisted more than 12,000 personal injury victims and have
          obtained hundreds of millions of dollars in settlements and verdicts.
          Their highly trained legal professionals have experience representing
          victims of a wide variety of personal injuries and have done so with
          aggressive litigation and superior client representation.
        </p>

        <p>
          They limit their number of clients so that every person who requires
          their services receives a superior client experience and is kept
          up-to-date on their case progress and developments. Their long history
          of satisfied clients and outstanding case results have made them the
          superior choice for Banning personal injury legal assistance for over
          30 years.
        </p>
        <h2>Why Choose Our Personal Injury Attorneys?</h2>
        <p>
          Few firms boast the outstanding accomplishments that Banning attorneys
          at Bisnar Chase have obtained. Whether you have been injured in an
          auto accident or any other type of personal injury, the personal
          injury lawyers at Bisnar Chase will be able to assist you. The
          following is a list of accomplishments that sets Bisnar Chase above
          the rest.
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              <Link
                to="https://profiles.superlawyers.com/california-southern/newpt-beach/lawyer/john-bisnar/bad97eb4-5759-4f9d-b0b3-ae4f943ab0e0.html"
                title="SuperLawyer John Bisnar"
                target="_blank"
              >
                SuperLawyers 2015
              </Link>
            </strong>
            . SuperLawyers is an organization which identifies lawyers who have
            obtained outstanding professional achievements and acquired a high
            degree of peer recognition. Both partners have held this coveted
            award for more than seven years consecutively.
          </li>
          <li>
            <strong>
              {" "}
              <Link
                to="https://www.lawyers.com/California/Newport-Beach/Bisnar-Chase-LLP-168466-f.html"
                target="_blank"
              >
                Lexis Nexis Martindale-Hubbell Review of 4.7 out of 5
              </Link>
            </strong>
            . The Lexis Nexis Martindale-Hubbell Law Directory is widely relied
            upon for authoritative reviews on the legal profession. Legal
            ability and professional ethics are criteria which are included in
            the law firm review.
          </li>
          <li>
            <strong>
              {" "}
              <Link
                to="https://www.avvo.com/attorneys/92660-ca-brian-chase-339392.html"
                target="_blank"
              >
                10 out of 10 Avvo Rating
              </Link>
            </strong>
            . Avvo is an organization which provides reliable reviews on doctors
            and attorneys. John Bisnar and Brian Chase have received a perfect
            10 out of 10 review. Avvo incorporates legal background,
            disciplinary history, and professional achievements.
          </li>
          <li>
            <strong>Top 100 Trial Lawyers</strong>. Brian Chase has had
            unparalleled results for his clients, and he has been recognized by
            the National Trial Lawyers Association. Membership is only granted
            by special invitation and is awarded only to attorneys who have
            superior leadership, influence, and reputation.
          </li>
        </ul>
        <LazyLoad>
          <img
            className="imgleft-fixed mb"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase"
          />
        </LazyLoad>
        <div className="snippet">
          <p>
            <span>
              {" "}
              <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
              Partner:
            </span>
            <br />
            “I went to law school knowing I wanted to be a personal injury
            attorney. I wanted my life’s work to have a positive impact on other
            people’s lives.”
          </p>
        </div>
        <h2>Personal Injury Experience in Several Areas</h2>
        <p>
          Bisnar Chase have sustained a 96% success rate for their clients over
          a wide variety of different types of injuries. We have attorneys that
          specialize in various types of injuries including;
        </p>
        <ul>
          <li>Slip and fall</li>
          <li>Serious dog bites</li>
          <li>Auto defects</li>
          <li>Car accidents (even catastrophic)</li>
          <li>Defective products</li>
          <li>Motorcycle accidents</li>
          <li>Employee harassment</li>
        </ul>
        <p>
          Victims of personal injuries within California have a limited time to
          file a claim. Once your statute of limitations has expired, it is
          almost impossible to obtain compensation for your injuries. Call now
          to get immediate assistance with your Banning personal injury claim.
          The outcome of your personal injury lawsuit may come down to timing.
          Before too much time lapses, find out your damages you might be
          entitled to. Certain facts have to be proven in most personal injury
          lawsuits so time is of the essence. Especially for remembering all the
          details of your case.
        </p>
        <h2>Free Consultations from Top-Notch Attorneys</h2>
        <p>
          Bisnar Chase offers a free,{" "}
          <Link to="/contact">no obligation consultation</Link>. If you or a
          loved one have suffered any type of injury, it is in your best
          interest to contact an experienced injury lawyer immediately and find
          out what your options are.
        </p>
        <p>
          <strong>Call 800-561-4887 NOW.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
