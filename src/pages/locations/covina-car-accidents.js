// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Covina",
      population: 48508,
      totalAccidents: 2175,
      intersection1: "Grand Ave & Cypress St ",
      intersection1Accidents: 21,
      intersection1Injuries: 28,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 235.2,
      city1Name: "Glendora",
      city1Index: 178.2,
      city2Name: "Azusa",
      city2Index: 255.6,
      city3Name: "West Covina",
      city3Index: 231.5,
      city4Name: "San Dimas",
      city4Index: 164.5
    },
    locationBox3: {
      intersection2: "Vincent Ave & San Bernardino Rd",
      intersection2Accidents: 20,
      intersection2Injuries: 21,
      intersection2Deaths: 1,
      intersection3: "Citrus Ave & Covina Blvd",
      intersection3Accidents: 18,
      intersection3Injuries: 19,
      intersection3Deaths: 0,
      intersection4: "Grand Ave & San Bernardino Rd",
      intersection4Accidents: 14,
      intersection4Injuries: 16,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Covina Car Accident Lawyer - Los Angeles County Accident Attorney"
        pageDescription="Have you or a loved one been injured in a car accident in Covina? Call (323) 238-4683 for a free case evaluation with a top rated Covina car accident lawyer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Covina Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            A good Covina attorney will tell you that Covina has its own car
            accident and traffic profile,
          </strong>{" "}
          and that this information can be useful when seeking compensation for
          a car collision injury. When you're injured in an accident or in any
          other case where you can submit a claim to be reimbursed by insurance
          companies, you want to make sure you take the right steps on the road
          to recovery and compensation. Bisnar Chase can help you with that. We
          have the experience to fight for you to make sure that your insurance
          will compensate you for your medical bills, lost wages, property
          damage, and general damages. If you want to see if you have a case for
          your injury, contact us today!
        </p>
        <h2>Covina's Unique City Accident Stats Can Be Useful</h2>
        <p>
          "The most skilled Covina car accident lawyers have come to realize
          that a city's traffic accident facts and statistics can be useful,"
          notes John Bisnar. For example, in 2008, speeding caused 238 crashes
          in Covina, turning left without yielding caused 83 accidents, and
          red-light runners caused 55 collisions. Over the years, savvy lawyers
          have also become familiar with Covina's accident-prone intersections:
          19 crashes each at Azusa/Cypress and Vincent/San Bernardino, 17 at
          Citrus/Covina, 16 at Badillo/Grand and 13 at Lark Ellen/San
          Bernardino. "If you suffer an injury in a car accident and seek
          compensation for damages, this information and experience can help you
          when your lawyers prepare your case for trial or go through settlement
          negotiations," adds Bisnar.
        </p>
        <h2>Seasoned Car Accident Lawyers Use Accident Data Wisely</h2>
        <p>
          Successful Covina car crash lawyers will analyze previous car
          accidents, their cause and severity, and where they occurred. If, for
          example, a Covina intersection is improperly designed and often
          confuses motorists, or is poorly controlled or enforced so as to
          encourage red-light scofflaws and unsafe turns, this information can
          become useful. The ability to properly evaluate a car accident takes
          skill and practice. All the more reason for those involved in a car
          crash to seek the advice of an experienced car collision lawyer. The
          finest Covina lawyers offer free no obligation consultations to
          accident victims. Many can be retained on a contingency basis (you
          don't pay their fees unless they win or settle your case).
        </p>
        <p>
          For more advice, please read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
