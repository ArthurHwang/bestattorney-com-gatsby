// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Sonoma",
      population: 10892,
      totalAccidents: 250,
      intersection1: "5th St West & W, Napa St",
      intersection1Accidents: 26,
      intersection1Injuries: 9,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 193.0,
      city1Name: "Napa",
      city1Index: 187.9,
      city2Name: "Yountville",
      city2Index: 130.5,
      city3Name: "Cotati",
      city3Index: 205.2,
      city4Name: "Windsor",
      city4Index: 157.1
    },
    locationBox3: {
      intersection2: "5th St West & Andrieux St",
      intersection2Accidents: 15,
      intersection2Injuries: 7,
      intersection2Deaths: 0,
      intersection3: "Studley St & 5th St West",
      intersection3Accidents: 16,
      intersection3Injuries: 5,
      intersection3Deaths: 0,
      intersection4: "W. Spain St & 5th St West",
      intersection4Accidents: 15,
      intersection4Injuries: 6,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sonoma Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Contact the Sonoma County Personal Injury Attorneys at 800-561-4887 for a free consultation. Passionate representation. Years of results & satisfied clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sonoma County Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          Sonoma personal injury attorneys representing Californians for 35
          years. A team of six trial lawyers who've received top honors such as
          SuperLawyers and AVVO make up the team at Bisnar Chase. We have
          collected over two hundred and fifty million in verdicts and judgments
          for our clients.
        </p>
        <p>
          Let an attorney from our firm help you make sense of your personal
          injury claim. Personal injury lawsuits vary depending on how bad your
          injury is and how long treatment will be required. A skilled Sonoma
          County personal injury lawyer will understand the rules of the local
          courts and be able to make sense of your case and its potential. Our{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            consultations are free
          </Link>{" "}
          and we advance all costs in your case.
        </p>
        <ul>
          <li> Aggressive Representation</li>
          <li> Dedicated Legal Team</li>
          <li> 96% Success Rate</li>
        </ul>
        <p>
          Sonoma is a historic city situated in the Sonoma Valley with the
          Mayacamas Mountains to the east, Sears Point to the southwest and the
          Sonoma Mountains to the west. As of 2010, Sonoma had over 10,500
          residents. Sonoma is also well-known for its beautiful wine country
          and is also home to many historic and scenic locations including the
          Mission San Francisco Solano, the home of General Mariano Guadalupe
          Vallejo and the Presidio of Sonoma.
        </p>
        <p>
          California State Route 12 passes through many of the most populated
          areas of the Sonoma Valley. It connects Santa Rosa to the north and
          Napa to the east. There are also State routes 121 and 116 that run to
          the south of town connecting cities such as Petaluma, Napa,
          Schellville and locations in Marin County. Highway 37 and the 101 are
          vital to travel to and from Sonoma as well. These roadways are where
          many of the most devastating Sonoma car accidents occur.
        </p>
        <h2>State Highway 121</h2>
        <p>
          Speeding is often a cause of serious injury accidents. Several people
          were injured in a Sonoma car accident on State Highway 121 when two
          vehicles collided with a big rig. According to a KTVU news report, the
          truck accident occurred just east of Eight Street just south of the
          city of Sonoma. All three vehicles were heavily damaged and a number
          of people suffered moderate injuries.
        </p>
        <h2>Highway 37</h2>
        <p>
          Another crash that involved a speeding motorist on a Sonoma highway
          resulted in the death of a family of four. On November 28, 2009,
          Steven Culbertson, 19, was traveling at 90 mph when he ran a red light
          at Lakeville Road and Highway 37. According to an ABC news report, his
          Mini Cooper clipped a Honda CR-V and broadsided a minivan carrying the
          Maloney family. John Maloney, his wife Susan and their two children,
          Aiden, 8 and Grace, 5, were all killed in the crash. Culbertson died
          later at the hospital as well.
        </p>
        <h2>Highway 12</h2>
        <p>
          Highway 12 is the site of many tragic accidents each year. On April 7,
          2012, serious injuries resulted from a crash involving a red Mercury
          Cougar and a white pickup truck. According to The Napa Patch, the
          Sonoma County crashes occurred on westbound Highway 12 at old Sonoma
          Road. Officials say Stanley Hilton, 48, let his Mercury drift over the
          centerline where it struck a truck driven by Andrea Virdell, 39. A
          40-year-old in the Mercury and a 23-year-old woman in the truck
          suffered significant injuries.
        </p>
        <h2>Dealing with Insurance Companies</h2>
        <p>
          A big part of handling a personal injury claim involves dealing with
          insurance companies. Whether it is an auto accident, a dog bite claim
          or an on-the-job injury, it is critical that victims exercise caution
          in terms of what they say or the actions they take. Personal injury
          victims with pending claims should remember not to sign any papers or
          rush into agreements without first talking to their own attorney. Once
          an agreement is signed, the case will be closed and it will not be
          possible to claim additional compensation that may be needed to pay
          for future medical costs and other expenses related to the injury.
        </p>
        <h2>Contacting a Sonoma Personal Injury Lawyer</h2>
        <p>
          The experienced Sonoma <Link to="/"> personal injury lawyers</Link> at
          our law practice have more than three decades of experience helping
          injured victims and their families seek compensation for their
          injuries and losses. Be it against a reckless driver, a negligent
          corporation or a nursing home that abuses or neglects its elderly
          patients, we have fought tirelessly for the rights of our clients and
          helped hold the at-fault party accountable. Our attorneys work on a
          contingency fee basis, which means that we do not get paid until you
          get compensated for your losses. Please contact us to obtain more
          information about pursuing your legal rights.
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
