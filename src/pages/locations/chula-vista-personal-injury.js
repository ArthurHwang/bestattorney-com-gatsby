// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Chula Vista",
      population: 256780,
      totalAccidents: 6613,
      intersection1: "Broadway & H St",
      intersection1Accidents: 69,
      intersection1Injuries: 56,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 180.9,
      city1Name: "Coronado",
      city1Index: 114.5,
      city2Name: "National City",
      city2Index: 315.4,
      city3Name: "Imperial Beach",
      city3Index: 227.7,
      city4Name: "Lemon Grove",
      city4Index: 280.0
    },
    locationBox3: {
      intersection2: "3rd Ave & L St",
      intersection2Accidents: 69,
      intersection2Injuries: 55,
      intersection2Deaths: 1,
      intersection3: "Palomar St & Industrial Bl",
      intersection3Accidents: 51,
      intersection3Injuries: 53,
      intersection3Deaths: 1,
      intersection4: "Broadway & Palomar St",
      intersection4Accidents: 62,
      intersection4Injuries: 53,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Chula Vista Personal Injury Lawyer - Accident Attorney"
        pageDescription="Bisnar Chase serves Chula Vista residents who need assistance with their personal injury case. No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Chula Vista Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            If you've been injured and need to undertand your rights,
          </strong>{" "}
          contact our Chula Vista personal injury attorneys for a free and
          confidential consultation. Bisnar Chase Personal Injury Attorneys will
          walk you through the process every step of the way, whether you've
          been injured in a car accident, dog bite, premises case, or any other
          injury incident.
        </p>
        <p>
          Chula Vista, one of California's most southernmost cities, sits on the
          border of Mexico. The approximately 220,000 people who reside in this
          sunny city have seen a fair share of accidents over the last few
          years, including the rise in drunk-driving related accidents that have
          taken San Diego County--the county in which Chula Vista is located--by
          surprise at the end of 2009.
        </p>
        <p>
          According to a law enforcement coalition, nearly 500 people were
          arrested for driving under the influence during a 10-day period ending
          the day after Christmas in San Diego County this last year. The
          California Highway Patrol made 60 DUI arrests in San Diego County
          during the 60 hours following 6pm on Christmas Ever alone. One of the
          three fatalities that occurred was that of a Chula Vista man who drove
          into a tree. Any{" "}
          <Link to="/dui">Chula Vista drunk driving attorney</Link> will tell
          you how disappointing these numbers are.
        </p>
        <h2>Pedestrians and Bicyclers Must Heed Warnings As Well</h2>
        <p>
          From June 2003 through June 2008, 294 car collisions involved Chula
          Vista pedestrians. One, in April of 2007, left a 15-year-old boy dead
          after being hit by a sport utility vehicle. The young man was walking
          along the I-5 at 11pm, a very unsafe time to be a pedestrian on a
          highway.
        </p>
        <p>
          A pedestrian advocacy group, entitled "Walk San Diego," has detailed
          the 10 most dangerous intersections for those on-foot, and one of
          them, Broadway and H Street, is in Chula Vista. The unfortunately
          reality is that pedestrians have a much higher likelihood of dying in
          a car collision, because they are lacking the protection that a car
          would give them. This is not to suggest that people should always
          drive, but rather to draw attention to the fact that cars and
          pedestrians alike need to be more concerned with safety on Chula
          Vista's roads.
        </p>
        <p>
          Not only pedestrians get tangled up in fatal car accidents. A story
          that has caught the eye of many Chula Vistans, is that of the young
          girl and her father who were struck by a trolley while on bike. The
          daughter was in a chair attached to the bike when her father rode them
          over the trolley tracks. The trolley driver operated the brakes, but
          it was too late to stop in time. The bicycle flew 40 feet from the
          point of impact, killing the young girl and injuring her father.
        </p>
        <h2>Chula Vista Personal Injury Law Firms Watch the Road for You</h2>
        <p>
          If you or a loved one has been involved in a car accident whether on
          foot, on a bicycle, on in a car, or have sustained personal injuries
          from any other kind of accident, it is in your best interest to
          contact a good Chula Vista personal injury attorney.
        </p>
        <p>
          If you are the victim of a personal injury, you owe it to yourself to
          get a copy of the book,{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim"
          </Link>
          . If you need help immediately, call one of our expert personal injury
          lawyers now at 949-203-3814 and we will schedule a consultation the
          same day or by the next business day at the latest.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
