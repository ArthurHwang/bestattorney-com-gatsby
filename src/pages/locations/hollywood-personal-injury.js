// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hollywood Personal Injury Attorney - 323-238-4683"
        pageDescription="Call the Hollywood Personal Injury Attorneys of Bisnar Chase. 323-238-4683 for a free consultation. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Hollywood Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          It sometimes seems that everyone in the world has heard of Hollywood,
          California, due to the television and movie industry.  However, few of
          the millions of tourists who visit this lovely Los Angeles town see
          beyond the glamour and flash to the day-to-day life of its residents. 
          Just like anyone else, Hollywood's citizens experience personal injury
          accidents and other problems that require the assistance of
          professionals.  For accident victims in this city, our{" "}
          <Link to="/los-angeles">Los Angeles personal injury lawyers</Link>{" "}
          stand ready to help recover damages and assist them in getting their
          lives back to normal as quickly as possible.
        </p>
        <h2>Personal Injuries in Hollywood</h2>
        <p>
          Personal injury accidents cover a wide range of types.  Hollywood
          residents may suffer in car collisions or in accidents involving
          trucks, motorcycles, bicycles, or pedestrians.  They may also suffer
          from injuries sustained in a slip and fall accident, in a medical
          malpractice case, or in a dog bite incident.  The type of injury is
          not nearly as important, however, as the fact that the victim seeks
          professional legal assistance to recover damages.
        </p>
        <p>
          Personal injury attorneys in Hollywood are the first line of defense
          for an injured victim.  No matter how slight or serious these
          injuries, collecting money from the person who hurt a victim is almost
          never easy.  There are laws in place to protect victims, but sometimes
          they are still taken advantage of by unscrupulous attorneys, insurance
          company representatives, or the person or company who caused the
          accident.
        </p>
        <h2>Experienced Representation Matters</h2>
        <p>
          It is sad to think that an accident victim would be victimized a
          second time by the workings of the legal system, but unfortunately
          that is the case for many people.  Because so many accident victims
          are not familiar with the way the law works in an injury case, they do
          not know the proper steps to take to protect themselves and collect
          money from the at-fault party.
        </p>
        <p>
          However, there is also good news.  Accident victims do not have to
          have legal knowledge to collect damages for their injuries.  All they
          have to do is contact a professional personal injury lawyer in
          Hollywood, then relax and focus on recovery instead of on trying to
          handle a lawsuit.
        </p>
        <p>
          The advantages of hiring a professional accident attorney are
          numerous.  First, victims can turn over all the details of a lawsuit
          to a person who understands them.  This relieves a great deal of
          stress and gives victims a feeling of self-confidence.  Next, victims
          know that there will be no need to worry about negotiating with the
          opposing side; sometimes these representatives can be aggressive, and
          it is important that victims turn this job over to a professional
          lawyer. {" "}
        </p>
        <p>
          Finally, victims can rest assured that the final settlement will
          represent the largest amount that can be obtained given the
          circumstances of the case, rather than wondering if they are settling
          for a fraction of what they could collect. A free consultation with a
          Hollywood personal injury lawyer should be a priority any time you are
          involved in an accident.  It is important to protect your rights, so
          schedule your <Link to="/los-angeles/contact">free consultation</Link>{" "}
          appointment today. C<strong>all 323-238-4683</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
