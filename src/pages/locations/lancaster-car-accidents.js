// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lancaster Car Accident Lawyer - Bisnar Chase"
        pageDescription="Were you injured in a car accident? Call 323-238-4683 for Lancaster car accident attorneys who care. Free consultations. Trust, passion, results! Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Negotiating Advice from a Lancaster Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          Negotiating a claim for damages with an insurance company can be quite
          a challenge, especially if you decide to do it without retaining legal
          counsel. An experienced{" "}
          <Link to="/los-angeles/lancaster-personal-injury-lawyers">
            Lancaster personal lawyer
          </Link>{" "}
          knows that there are a number of negotiating tactics that have proven
          effective in arriving at a fair compensation amount. However, before
          revealing one possible tactic, an overview of the accident statistics
          for Lancaster, California may serve to highlight the number of people
          who face this task daily.
        </p>
        <h2>Many Endure the Tragedy of Lancaster Car Accidents</h2>
        <p>
          Lancaster's car accidents occur with alarming frequency. In 2006, the
          California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) reported that 15 people were killed and 802 were
          injured in city car crashes. Car accidents also killed five
          pedestrians and injured 41. A total of 26 bicyclists were injured in
          car collisions. And one motorcyclist was killed and 16 were injured.
          Drunk drivers caused six fatalities and 61 injuries. In 2008, 10 car
          collisions ended in as many deaths.
        </p>
        <h2>Waiting Until Adjuster "Shows His Hand" Can Be Advantageous</h2>
        <p>
          It's a simple tactic seldom employed by novice negotiators; yet it's
          used by some of the best Lancaster car accident lawyers to negotiate
          the largest possible sums in accident insurance claims: Wait until the
          insurance adjuster submits his initial offer--before you submit yours.
          Naturally, the adjuster's offer will be under what you have in mind.
          When you come back with your counter offer, include all the damages;
          list them, and total it all up with a real number.
        </p>
        <p>
          Another tactic is to ask for the maximum amount of the insured
          driver's coverage. Even if you don't know what the maximum policy
          limit is, asking for it works two ways to your advantage: It fixes a
          demand that's within the limits set by the insurance company's
          liability, so it can surely be regarded as reasonable; it also opens
          up the insurance company to a judgment that could exceed the actual
          limits of the policy. The latter will give you some extra leverage in
          negotiations that are sure to follow. Let's say, for example, that a
          jury delivers a judgment that's above a policy's limits. In this case,
          the insurance company would be have to pay the entire award,
          regardless of the limits. This is one thing insurance companies will
          want to avoid.
        </p>
        <h2>Car Accident Lawyers Have Other Valuable Negotiating Techniques</h2>
        <p>
          The most experienced Lancaster car crash lawyers have acquired many
          critically important negotiating skills. These can be extremely
          valuable when car accident victims have to deal with insurance
          adjusters who negotiate car accident claims every day. All the more
          reason to seek the advice of an experienced car crash lawyer. The best
          lawyers offer no charge, no pressure consultations. In seeking their
          sage advice, you have nothing to lose and much to gain.
        </p>
        <p>
          <strong>Local Office</strong>
        </p>
        <div className="vcard">
          <div className="adr">
            <div className="street-address">
              6701 Center Drive West, 14th Fl.
            </div>
            <span className="locality">Los Angeles</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">90045</span>
            <br />
            <span className="tel">(323) 238-4683</span>
          </div>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
