// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Monterey County Personal Injury Attorneys - Bisnar Chase"
        pageDescription="Contact the Monterey County Personal Injury Attorneys for a free and confidential consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Monterey County Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          Monterey County has almost a half a million residents. With many
          points of interest and winding roads, Monterey has seen its share of
          personal injuries. Monterey, Carmel, Salinas and Big Sur are popular
          destinations and every year there are hundreds of slip and falls, car
          accidents and other injuries that require the expertise of local
          personal injury lawyers.{" "}
        </p>
        <p>
          Bisnar Chase has been representing Californians for over thirty five
          years with verdicts and settlements in the hundreds of millions. If
          you are looking for an experienced personal injury attorney please
          call 800-561-4887 for a free consultation.
        </p>
        <h3>Types of Personal Injuries We Represent</h3>
        <ul>
          <li>
            <strong>Slip and fall</strong>
          </li>
          <li>
            <strong>Car accident</strong>
          </li>
          <li>
            <strong>Motorcycle accidents</strong>
          </li>
          <li>
            <strong>Serious dog bites</strong>
          </li>
          <li>
            <strong>Defective products</strong>
          </li>
          <li>
            <strong>Premises liability</strong>
          </li>
        </ul>
        <p>
          A free consultation with a Monterey County{" "}
          <Link to="/">personal injury attorney</Link> will give you the
          information you need about your chances of collecting damages for your
          accident.  An experienced attorney will never give you false hope
          about your claim, nor will he or she give you an inflated value as a
          target settlement.  If you do not have a case, an accident lawyer will
          tell you so.  On the other hand, if your case has merit, a good lawyer
          can help you recover damages to pay for your injuries and to make sure
          you are fairly represented.
        </p>
        <h2>When To Hire a Personal Injury Attorney</h2>
        <p>
          With most personal injury cases you have to have damages. If a person
          claims to have bought a product that could have been harmful, but was
          not, then it's very hard to argue damages for that. However, if you
          were in a car accident, even if the other party had no insurance, you
          may be entitled to recovery if you were injured. The type of injuries
          and length of treatment will have a lot to do with the compensation
          you receive.{" "}
        </p>
        <p>
          A good attorney will have years of trial experience and a lot of
          success in the courtroom. The last thing you'll want is for your
          attorney to hand off your case to another law firm if it has to go to
          trial. You should consult a personal injury attorney if you have
          sustained even minor to moderate injuries; the attorney will have the
          expertise to tell you if you have a case and whether to begin seeking
          long term treatment.
        </p>
        <h2>Reputation and Results</h2>
        <p>
          Bisnar Chase has made a name for itself for being tough injury
          attorneys. The insurance companies and defense lawyers know that going
          up against Bisnar Chase will be a fight. We've spent decades dealing
          with the insurance companies and local courts and we know how to win.{" "}
        </p>
        <p>
          Our success rate is above 96% and we've recovered over $500 Million
          for our clients. Over 12,000 clients have put their trust in us and
          client satisfaction is very important to us. We want your experience
          to be a stress free one and to help take the load of the accident off
          your mind. We truly understand how accidents and injuries affect your
          life and will work very hard to make the process easier on you.
        </p>
        <p>
          Find out if you have a case. Call today to speak with a Monterey
          County personal injury attorney for a{" "}
          <Link to="/contact">free consultation.</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
