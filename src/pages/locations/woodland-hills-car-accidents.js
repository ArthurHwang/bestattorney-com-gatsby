// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Car Accident Attorney Woodland Hills - Bisnar Chase"
        pageDescription="Call 323-238-4683 for a free consultation with a Woodland Hills car accident lawyer. Best client care since 1978"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Woodland Hills Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          A knowledgeable <Link to="/car-accidents">car accident lawyer</Link>{" "}
          knows that to serve a client more effectively, he or she should have a
          working knowledge of Woodland Hills, its traffic concerns and accident
          statistics. That is why we pride ourselves on being the personal
          injury attorney for you.
        </p>

        <p>
          Bisnar Chase has spent over three decades helping residents of Los
          Angeles including Woodland Hills. We are familiar with the courts and
          the attorneys, and can represent you in a fair and aggressive way.
          Call today for your free consultation with an experienced attorney.
          Call 323-238-4683.
        </p>

        <h2>Heavy Traffic in Woodland Hills</h2>
        <p>
          Woodland Hills California borders the San Fernando Valley and Santa
          Monica Mountains. There are approx. 31 schools in the area between
          elementary, trade and private schools. This creates a lot of foot
          traffic and a concern for pedestrian injuries and car accidents.
        </p>
        <h2>Five Tragedies in One Day in Woodland Hills</h2>
        <p>
          "Astute car accident lawyers have become aware of the city's many
          painful car accident tragedies," noted John Bisnar. "On one sad day,
          for example, five people were killed in Woodland Hills car accidents.
          A motorcyclist was fatally injured at Mulholland Drive near Flamingo
          Street when a truck turned in front of him.
        </p>
        <p>
          Soon afterward, two elderly women died when their car ran a red light
          and crashed into an oncoming car at Shoup Avenue and Oxnard Street.
          Later, a motorist was killed when his convertible flipped on Winnetka
          Avenue. And finally, a 14-year-old boy was struck and killed by a car
          while he was riding his bike on Fallbrook Avenue, across Saticoy
          Street."
        </p>
        <h2>Woodland Hills' Special Traffic Problems</h2>

        <p>
          The city has also had a problem with red-light runners at Topanga
          Canyon and Burbank, and Victory Blvd. and Mason Avenue. Accidents like
          these can be particularly deadly. Additional traffic problems in
          Woodland Hills include motorists who tend to speed on San Feliciano
          Drive, and on Dumetz between San Feliciano Drive and Topanga Canyon.
        </p>

        <p>
          Many commuters use San Feliciano Drive as a bypass street instead of
          taking Topanga Canyon. Three hit-and-run accidents and five cars
          totaled are among the tragedies that have occurred just on the 4700
          block of San Feliciano--all due to excessive speeding. Most of these
          collisions occurred at the curve at the bottom of the hill at Cerillos
          due to speeding.
        </p>
        <h2>Get Experience - Get Results</h2>
        <p>
          If you get into a car crash in Woodland Hills, talk to a car accident
          attorney who knows the city. A skilled injury lawyer will let you know
          if you should pursue your case and what you should do next. The most
          trusted accident attorneys offer free, no obligation consultations to
          injured victims.
        </p>
        <p>
          Bisnar Chase attorneys are trial lawyers with a track record of
          winning. We've helped over 12,000 people and we may be able to help
          you too. Bisnar Chase has made a name for itself of trust and results.
          Call<strong> </strong>today for a{" "}
          <Link to="/los-angeles/contact">free consultation</Link>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
