// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "header-images/Class Action Official Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Class Action Lawyers - Bisnar Chase"
        pageDescription="Contact the California class action lawyers today for a free consultation. No win, no fee. Recover damages in all types of class actions! Call 800-561-4887"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Class Action Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Class Action Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            The <strong>California Class Action Attorneys</strong> of Bisnar
            Chase have been taking on the most complex cases for over{" "}
            <strong>39 years</strong>. Our attorneys have garnered an estimated
            amount in excess of <strong>$500 Million dollars </strong>in
            winnings for our clients.
          </p>
          <p>
            Our class action lawyers will fight for the compensation you
            deserve. The attorneys of Bisnar Chase hold a{" "}
            <strong>96% success</strong> rate and if there is no win there is no
            fee. If you or someone you love has suffered due to someone else's
            negligence call <strong>800-561-4887</strong> and receive a{" "}
            <strong>free consultation.</strong>
          </p>
          <h2>Why File a Class Action Lawsuit?</h2>
          <p>
            A{" "}
            <Link
              to="https://en.wikipedia.org/wiki/class_action"
              target="_blank"
            >
              class action
            </Link>{" "}
            lawsuit is a type of civil lawsuit in which the claims and rights of
            many people are combined and decided in a single case. Typically,
            specific plaintiffs are named in the class action lawsuit to
            represent the claims of other class members so that everyone with
            the same complaint or injury does not have to file individual
            lawsuits. In a class-action suit, all these complaints are
            consolidated into one case.
          </p>
          <p>
            There are usually several benefits to filing a class action lawsuit
            as opposed to an individual civil complaint or claim. Some benefits
            can range in the inexpensive cost of filing the suit to the great
            recovery amount the plaintiff will receive.
          </p>
          <h2>5 Steps for Filing a Class Action Lawsuit</h2>
          <p>
            Here is a list of some of the elements and actions that must be
            taken as part of the process of filing a class action lawsuit.
          </p>
          <ol>
            <li>
              <strong>The complaint:</strong> In order to file a class action
              lawsuit, the same type of harm must have been done to a group of
              people. The size of the complaint will not affect whether or not
              you have a class action lawsuit. What's important is that a group
              of people suffered some type of loss.
            </li>
            <li>
              <strong>Significant number of victims:</strong> There must be so
              many claimants that it is not reasonable for each to file an
              individual claim.
            </li>
            <li>
              <strong>Class action attorney:</strong> Make sure you speak with a
              knowledgeable attorney preferbly a California lawyer who has
              recent experience handling cases similar to yours.
            </li>
            <li>
              <strong>Representative:</strong> One or more persons must be named
              to represent all of the claimants in the lawsuit. This is the
              class Representative.
            </li>
            <li>
              <strong>Demands:</strong> You will have to decide what type of
              compensation you want. In most cases, a class action lawsuit will
              result in financial compensation.
            </li>
          </ol>
        </div>

        <LazyLoad>
          <img
            src="/images/consumer Class action law suit Image.jpg"
            width="100%"
            alt="class action attorneys in California"
          />
        </LazyLoad>
        <h2>Types of Class Action Lawsuits</h2>
        <p>
          A class action case is strengthened when there is more than one person
          or a class of people filing the suit. Cases that present an obvious
          wrongdoing can receive major attention and sends a direct message to
          the offenders. Below are the most common class action lawsuits.
        </p>
        <p>
          <strong>Consumer class Actions:</strong> These types of actions are
          generally brought when a group of consumers complain about being
          harmed by a corporation's systematic and illegal practices.
        </p>
        <p>
          <strong>Product Liability class Actions: </strong>Cases involving{" "}
          <Link to="/defective-products">defective products</Link> can be
          brought to the courts such as a person suffering severe side effects
          from a weight loss pill or blood pressure medication.
        </p>
        <p>
          <strong>Employment Class Actions:</strong>{" "}
          <Link to="/employment-law/class-action">
            Employment class action lawsuits
          </Link>{" "}
          are typically filed on behalf of many employees of a company for
          claims ranging from complaints against workplace discrimination or
          sexual harassment to unpaid wages, illegal break times or overtime
          pay.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Class action lawyers"
            style={{
              backgroundImage:
                "url('/images/images/california class action lawsuit text header.jpg')"
            }}
          >
            <h2>Winnings From Class Action Lawsuits</h2>
          </div>
        </LazyLoad>
        <p>
          The compensation sought by consumers through class action litigation
          varies with each lawsuit. The potential value of your class action
          suit will depend on the severity of the losses suffered by the
          victims.
        </p>
        <p>
          <strong>
            Types of losses that will be considered in a class action lawsuit
            include:
          </strong>
        </p>
        <ul>
          <li>Damages for personal injury</li>
          <li>Litigation costs</li>
          <li>Compensation for economic losses</li>
        </ul>
        <p>
          The compensatory damages from a class action lawsuit will often be
          distributed among the class members based on the damages each member
          suffered. In some cases,{" "}
          <Link
            to="http://www.slate.com/articles/news_and_politics/explainer/2000/07/what_are_punitive_damages.html"
            target="_blank"
          >
            {" "}
            punitive damages
          </Link>{" "}
          may also be awarded.
        </p>
        <h2>Class Action Suits Change Laws</h2>
        <p>
          Class action lawsuits, that are usually namefied as civil litigation
          disputes are necessary and beneficial to the public good. Often,
          states with their budget woes and depletion of resources do not have
          the ability to police illegal activities perpetrated by large
          corporations.
        </p>
        <p>
          However, Class action lawsuits empower ordinary citizens who have been
          wronged to enforce the law and seek justice. Another important goal of
          these lawsuits is to encourage corporations to comply with the law.
        </p>
        <p>
          In September of 2017 hackers were able to access the social security
          numbers, addresses and drivers license information of over 143 million{" "}
          <Link to="/blog/massive-equifax-data-breach-affects-more-than-143-million-consumers">
            {" "}
            Equifax
          </Link>{" "}
          users. Equifax is one of the three largest credit companies and holds
          over 820 million consumers in their database.
        </p>
        <p>
          The lawyers of Bisnar Chase have followed this case and believed that
          it was only right to help and protect the people whose information has
          been compromised.
        </p>
        <p>
          Without class action lawsuits and excellent legal representation,
          consumers do not have a realistic way to fight back against large
          corporations.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Immediate Help for Your Class Action Lawsuit</h2>
        <p>
          The experienced California class action lawyers at the law offices of
          Bisnar Chase will be able to help individuals determine if they are
          eligible for either joining an existing class action lawsuit or filing
          a new class action lawsuit.
        </p>
        <p>
          We can help you evaluate your options and help you seek compensation
          for your significant losses. If you have questions or concerns about
          class action lawsuits or <Link to="/">personal injury claims</Link>,
          please contact us at
          <strong> (800)-561-4887</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
