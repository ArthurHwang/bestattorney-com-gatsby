// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="First American Title class action lawsuit filed."
        pageDescription="Bisnar Chase Personal Injury Attorneys have filed a class action lawsuit against First American Title for a data breach involving their website and consumer's private financial information being breached."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          FIRST AMERICAN FINANCIAL CORPORATION ; FIRST AMERICAN TITLE COMPANY
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Defendant Name:</strong> BEN DINH, individually, and on behalf
          of all others similarly situated
        </p>
        <p>
          <strong>Case Number:</strong> 8:19-cv-01105-AG-DFM
        </p>
        <p>
          <strong>Court:</strong> UNITED STATES DISTRICT COURT CENTRAL DISTRICT
          OF CALIFORNIA
        </p>
        <p>
          <strong>Practice Area:</strong>{" "}
          <Link to="/class-action" title="class action" target="_blank">
            Class Action
          </Link>
        </p>
        <p>
          <strong>Status:</strong> Active; Jury Trial Demanded
        </p>
        <p>
          <strong>Date Filed:</strong> 06/04/19
        </p>
        <p>
          <strong>Documents:</strong>{" "}
          <Link
            to="/pdf/first-american-title-class-action.pdf"
            title="Class action filed against First American Title"
            target="_blank"
          >
            Original Complaint
          </Link>{" "}
          (PDF)
        </p>
        <p>
          <strong>Attorneys Involved:</strong>{" "}
          <Link to="/attorneys/brian-chase" title="Brian Chase">
            Brian Chase
          </Link>
          ,{" "}
          <Link to="/attorneys/jerusalem-beligan" title="Jerusalem Beligan">
            Jerusalem Beligan
          </Link>
          ,{" "}
          <Link to="/attorneys/ian-silvers" title="Ian M. Silvers">
            Ian M. Silvers
          </Link>
        </p>
        <h3 style={{ marginBottom: "2rem" }}>
          <strong>Think you have a case? </strong>Contact us using the form
          above to have a class action specialist contact you.
        </h3>
        <h2>Details of the First American Data Breach</h2>
        <p>
          On May 24, 2019, cybersecurity researcher Brian Krebs announced that
          First American published on its website more than 885 million
          sensitive mortgage documents (the “Data Breach”). These documents
          contained the confidential, private information of Plaintiff and
          putative Class members including, but not limited to, their names,
          email addresses, mailing addresses, dates of birth, social security
          numbers, bank account numbers, lender details, mortgage and tax
          records, driver’s license images, and other personal information
          (collectively, “PII”).
        </p>
        <p>
          Since the Data Breach was first announced by Brian Krebs, First
          American has admitted that a design defect in one of its applications
          exposed the PII of its customers. Based on information and belief,
          First American hired an independent security forensic company and upon
          determining there was unauthorized access to Plaintiff and Class
          member’s PII, First American shut down external access to the
          application.
        </p>
        <p>
          While it is unclear when the Data Breach first began, the exposed
          documents date back to at least 2003 and were made available to the
          public without any security protection on the First American website.
          For instance, no username or password was required to view Plaintiff
          and Class members’ PII, and the webpage lacked industry standard-two
          factor authentication
        </p>
        <h2>The Disappointing Web Design Error that Caused the Data Breach</h2>
        <p>
          Most disappointing is that First American allowed the Data Breach to
          occur, despite it being caused by a relatively common website design
          error called Insecure{" "}
          <Link
            to="https://codedx.com/insecure-direct-object-references/"
            title="Insecure Direct Object Reference"
            target="_blank"
          >
            Direct Object Reference
          </Link>
          , which occurs when a link to a webpage with sensitive information is
          created and intended to only be seen by a specific party, but there is
          no method to actually verify the identity of who is viewing the link.
        </p>
        <h3>CLASS ACTION ALLEGATIONS</h3>
        <p>
          First American is the largest title insurance company in the United
          States, earning $5.3 billion per year in revenue from selling title
          insurance and other closing services. As Forbes noted in 2006, First
          American prices its title insurance at 1,300% above its margin cost.
          The average policy with First American (in 2006) cost about $1,500 but
          running a title search—now that records are digitized—costs as little
          as $25. And First American pays only about $75 per policy to pay
          claims.
        </p>
        <p>
          Customers believe that—at a minimum—the large sum they pay towards
          title insurance buys them security and peace of mind that their
          sensitive documents will be securely stored. As Ben Shoval, the man
          who discovered the First American breach, explains: “The title
          insurance agency collects all kinds of documents from both the buyer
          and seller, including Social Security numbers, driver’s licenses,
          account statements ... You give them all kinds of private information
          and you expect that to stay private.”
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
