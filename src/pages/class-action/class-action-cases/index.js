// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Class Action Cases filed by Bisnar Chase Personal Injury Attorneys"
        pageDescription="Class Action Cases that have been filed by Bisnar Chase Personal Injury Attorneys"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Class Action Cases Filed by Bisnar Chase</h1>
        <BreadCrumbs location={location} />
        <h2>2019</h2>
        <p>
          1 -{" "}
          <Link
            to="/class-action/class-action-cases/great-call-best-buy"
            title="GreatCall Best Buy Class Action Lawsuit"
            target="_blank"
          >
            GREATCALLS, INC.; BEST BUY CO
          </Link>
          .{" "}
        </p>
        <p>
          2 -{" "}
          <Link
            to="/class-action/class-action-cases/chattem-inc-class-action"
            title="Chattem Inc, Sanofi Inc class action"
          >
            {" "}
            CHATTEM, INC.; SANOFI, INC.
          </Link>{" "}
        </p>
        <p>
          3 -{" "}
          <Link
            to="/class-action/class-action-cases/first-american-title-class-action"
            title="First American Title Class Action Lawsuit"
          >
            FIRST AMERICAN TITLE
          </Link>
        </p>
        <p>
          4 -{" "}
          <Link
            to="/class-action/class-action-cases/quest-diagnostics-class-action"
            title="Quest Diagnostic Class Action"
          >
            {" "}
            QUEST DIAGNOSTICS
          </Link>
        </p>{" "}
        <Link to="/class-action/class-action-cases/quest-diagnostics-class-action"></Link>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
