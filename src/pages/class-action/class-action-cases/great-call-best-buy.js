// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="GreatCall Inc, Best Buy Inc: Class Action Filed May 2019"
        pageDescription="Bisnar Chase Personal Injury Attorneys have filed a class action lawsuit against GreatCall Inc, Best Buy Inc. for a dangerous and defective product on behalf of Scott Barns."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>GreatCall, Inc.; Best Buy Co., Inc.</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Defendant Name:</strong> SCOTT BARNES, individually and on
          behalf of all others similarly situated
        </p>
        <p>
          <strong>Case Number:</strong> 2:19-cv-04457-JFW-GJS
        </p>
        <p>
          <strong>Court:</strong> UNITED STATES DISTRICT COURT CENTRAL DISTRICT
          OF CALIFORNIA, WESTERN DIVISION
        </p>
        <p>
          <strong>Practice Area:</strong>{" "}
          <Link to="/class-action" title="class action" target="_blank">
            Class Action
          </Link>
        </p>
        <p>
          <strong>Status:</strong> Active; Jury Trial Demanded
        </p>
        <p>
          <strong>Date Filed:</strong> 05/22/2019
        </p>
        <p>
          <strong>Documents:</strong>{" "}
          <Link
            to="/pdf/great-call-class-action.pdf"
            title="Class action filed against GreatCall, Best Buy"
            target="_blank"
          >
            Original Complaint
          </Link>{" "}
          (PDF)
        </p>
        <p>
          <strong>Attorneys Involved:</strong>{" "}
          <Link to="/attorneys/brian-chase" title="Brian Chase">
            Brian Chase
          </Link>
          ,{" "}
          <Link to="/attorneys/jerusalem-beligan" title="Jerusalem Beligan">
            Jerusalem Beligan
          </Link>
          ,{" "}
          <Link to="/attorneys/ian-silvers" title="Ian M. Silvers">
            Ian M. Silvers
          </Link>
        </p>
        <h3 style={{ marginBottom: "2rem" }}>
          <strong>Think you have a case? </strong>Contact us using the form
          above to have a class action specialist contact you.
        </h3>
        <h2>Details of the Defective Medical Device Case</h2>
        <p>
          Plaintiff Scott Barnes (“Plaintiff”), individually and on behalf of
          all others similarly situated, bring this class action against
          Defendants GreatCall, Inc., Best Buy, Co., Inc. and DOES 1 to 10
          (collectively referred to herein as “Defendants”), and in support
          thereof the following, based upon personal information, investigation
          of his counsel, and upon information and belief as to all other
          allegations.
        </p>
        <p>
          Defendants are the leading provider of connected health and personal
          emergency response services to the aging population, with more than
          900,000 paying subscribers. Defendants introduced the new{" "}
          <strong>Lively Mobile Plus</strong> (the “Defective Medical Alert
          Device”) and advertised it as “the highest standard in medical
          alerts.” Defendants further represent on their website that the
          Defective Medical Alert Device will provide “help anytime, anywhere,
          24/7".
        </p>
        <h2>
          The Defective Medical Alert Device fails to provide what was promised
          to consumers
        </h2>
        <p>
          The Defective Medical Alert Device is defective in that the fall
          detection and GPS does not function and fails to provide emergency
          services when activated (the “Defect”); thus, placing
          consumers—predominantly elderly and disabled persons who are relying
          on the Defective Medical Alert Device—at great risk of serious
          injuries, including death if emergency services either arrive late or
          are never alerted.
        </p>
        <h3>CLASS ACTION ALLEGATIONS</h3>
        <p>
          Pursuant to Federal Rule of Civil Procedure 23, Plaintiff brings this
          lawsuit on behalf of himself and all similarly situated individuals.
          Plaintiff seeks to represent the following nationwide class: All
          persons in California and throughout the United States who purchased
          one or more of the Defective Medical Alert Devices (the “Class”).
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
