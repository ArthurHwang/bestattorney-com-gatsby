// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Chattem Inc., Sanofi Inc. class action lawsuit filed."
        pageDescription="Bisnar Chase Personal Injury Attorneys have filed a class action lawsuit against Chattem Inc. for burns resulting from using the Icy Hot product."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>CHATTEM INC. ; SANOFI INC.</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>Defendant Name:</strong> RUTHIE MARTIN, individually, and on
          behalf of all others similarly situated
        </p>
        <p>
          <strong>Case Number:</strong> 1·9sf CV17036
        </p>
        <p>
          <strong>Court:</strong> SUPERIOR COURT OF THE STATE OF CALIFORNIA IN
          AND FOR THE COUNTY OF LOS ANGELES{" "}
        </p>
        <p>
          <strong>Practice Area:</strong>{" "}
          <Link to="/class-action" title="class action" target="_blank">
            Class Action
          </Link>
        </p>
        <p>
          <strong>Status:</strong> Active; Jury Trial Demanded
        </p>
        <p>
          <strong>Date Filed:</strong> 05/16/2019
        </p>
        <p>
          <strong>Documents:</strong>{" "}
          <Link
            to="/pdf/chattem-inc-class-action.pdf"
            title="Class action filed against First American Title"
            target="_blank"
          >
            Original Complaint
          </Link>{" "}
          (PDF)
        </p>
        <p>
          <strong>Attorneys Involved:</strong>{" "}
          <Link to="/attorneys/brian-chase" title="Brian Chase">
            Brian Chase
          </Link>
          ,{" "}
          <Link to="/attorneys/jerusalem-beligan" title="Jerusalem Beligan">
            Jerusalem Beligan
          </Link>
          ,{" "}
          <Link to="/attorneys/ian-silvers" title="Ian M. Silvers">
            Ian M. Silvers
          </Link>
        </p>
        <h3 style={{ marginBottom: "2rem" }}>
          <strong>Think you have a case? </strong>Contact us using the form
          above to have a class action specialist contact you.
        </h3>
        <h2>Details of the Chattem Inc Class Action (Icy Hot)</h2>
        <p>
          This is a putative class action for damages and equitable relief
          against Defendants relating to their formulation, manufacture,
          testing, marketing, promotion, distribution, and sale of their
          defective topical pain relief product: ICY HOT MEDICATED NO MESS
          APPLICATOR (the “ICY HOT APPLICATOR” or the “Defective Product”).
        </p>
        <p>
          The ICY HOT APPLICATOR is a topical pain relief product manufactured
          and sold by Defendants in California and made popular by the way
          Defendants market the product, including the notable packaging—the
          trusted bright and colorful eye-catching and well known ICY HOT logo.
          In addition, Defendants advertise in various channels, including
          through social media and television commercials with movie celebrity
          and ex-NBA player Shaquille O’Neal, who endorses the product.{" "}
        </p>
        <p>
          Defendants market and advertise the ICY HOT Applicator as a “Long
          Lasting Pain Relief” applicator (emphasis added). They further market
          and advertise it as containing “Maximum Strength” “Pain Reliving
          Liquid” (emphasis added).{" "}
        </p>
        <h3>CLASS ACTION ALLEGATIONS</h3>
        <p>
          As alleged above, Defendants manufacture, market, and sell the ICY HOT
          APPLICATOR to citizens in California. Packaged in widely recognized,
          brightly colored ICY HOT logo, the Defective Product is sold for
          approximately $5.48 per 2.5 ounces. Consumers who are seeking
          immediate and temporary pain relief are attracted by the eye-catching
          packaging.
        </p>
        <p>
          Once lured in, the packaging claims (in all-caps and bold lettering)
          that it contains “MAXIMUM STRENGTH” “PAIN RELIEVING LIQUID” that is
          not only “Powerful” and “Fast Acting,” but (in bold, all caps, and red
          lettering) provides “LONG LASTING PAIN RELIEF.”{" "}
        </p>
        <p>
          {" "}
          Defendants’ website reiterates the claims on the Defective Product’s
          packaging: the ICY HOT APPLICATOR contains a “Maximum Strength” “Pain
          Relieving Liquid” that provides “Long Lasting Pain Relief.”
        </p>
        <p>
          “I used this product twice on my lower back and I would never use it
          again. The second time I used it I started feeling this burning
          sensation. My daughter looked at my lower back and told me it looked
          as if I had burned myself. I followed the directions and could not
          believe this was happening to me. In the past I have used similar
          products and have never had a problem. My lower back looks as if it
          was burned. It started out as a dark mark on the right side of my
          lower back and over the last few days as the dead skin comes off my
          lower back is pink in color. I would never use this or any of their
          products again.”
        </p>{" "}
        <Link to="/class-action/class-action-cases/quest-diagnostics-class-action"></Link>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
