// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Quest Laboratories Class Action Filed June 2019"
        pageDescription="Bisnar Chase Personal Injury Attorneys have filed a class action lawsuit against Quest Laboratories, Optum360 Services, American Medical Collection Agency for private financial data breach"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          QUEST DIAGNOSTICS, INC.; OPTUM360 SERVICES, INC.; AMERICAN MEDICAL
          COLLECTION AGENCY
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>Defendant Name:</strong> JOHANNA A. MAYER, individually, and
          on behalf of all others similarly situated
        </p>
        <p>
          <strong>Case Number:</strong> 5:19-cv-01029-JGB-SHK
        </p>
        <p>
          <strong>Court:</strong> UNITED STATES DISTRICT COURT CENTRAL DISTRICT
          OF CALIFORNIA
        </p>
        <p>
          <strong>Practice Area:</strong>{" "}
          <Link to="/class-action" title="class action" target="_blank">
            Class Action
          </Link>
        </p>
        <p>
          <strong>Status:</strong> Active; Jury Trial Demanded
        </p>
        <p>
          <strong>Date Filed:</strong> 06/05/19
        </p>
        <p>
          <strong>Documents:</strong>{" "}
          <Link
            to="/pdf/quest-diagnostics-class-action.pdf"
            title="Quest Diagnostics class action lawsuit complaint"
            target="_blank"
          >
            Original Complaint
          </Link>{" "}
          (PDF)
        </p>
        <p>
          <strong>Attorneys Involved:</strong>{" "}
          <Link to="/attorneys/brian-chase" title="Brian Chase">
            Brian Chase
          </Link>
          ,{" "}
          <Link to="/attorneys/jerusalem-beligan" title="Jerusalem Beligan">
            Jerusalem Beligan
          </Link>
          ,{" "}
          <Link to="/attorneys/ian-silvers" title="Ian M. Silvers">
            Ian M. Silvers
          </Link>
        </p>
        <h3 style={{ marginBottom: "2rem" }}>
          <strong>Think you have a case? </strong>Contact us using the form
          above to have a class action specialist contact you.
        </h3>
        <h2>Details of the Quest Diagnostics Data Breach</h2>
        <p>
          On June 3, 2019, Quest Diagnostics, one of the largest blood testing
          providers in the country, announced a data breach whereby nearly 12
          million of its customers, including Plaintiff and putative Class
          members, had their personally identifiably information (“PII”) and
          protected health information (“PHI”) accessed by unauthorized parties
          due to the negligent data security of AMCA, one if its billing
          collections vendors (the “Data Breach”). Specifically, the PII and PHI
          accessed included, but was not limited to, Plaintiff’s and Class
          members’ personal information (e.g. Social Security Numbers),
          financial information (credit card numbers and bank account
          information), and medical information.
        </p>
        <p>
          In its SEC filing relating to the Data Breach, Quest Diagnostics
          announced that unauthorized parties accessed between August 1, 2018
          and March 30, 2019 AMCA’s system, which contained Plaintiff’s and
          Class members’ PII and PHI. Despite unauthorized parties having access
          to the AMCA system for more than six months, AMCA only learned of the
          Data Breach as a result of receiving information from a security
          compliance firm that works with credit card companies.
        </p>
        <p>
          Given AMCA’s relationship to Question Diagnostics (AMCA provides
          services to Optum360, which in turn provides payment services to Quest
          Diagnostics), Plaintiff and Class members were blindsided by the Data
          Breach announcement given most have never heard of AMCA or Optum360
          and were unaware that their information would be shared with these
          entities, causing additional emotional harm.{" "}
        </p>
        <h3>CLASS ACTION ALLEGATIONS</h3>
        <p>
          Beginning around August 1, 2018, unauthorized parties accessed the
          AMCA system that contained Plaintiff’s and Class members’ PII and PHI.
          Plaintiff and Class members are customers who paid and provided their
          PII and PHI to Quest Diagnostics in exchange for diagnostic and
          medical services. Quest Diagnostics provided Plaintiff and Class
          members’ PII and PHI to Optum360 who in turn provided that information
          to AMCA for billing and collection purposes.{" "}
        </p>
        <p>
          {" "}
          For more than six months, unauthorized parties maintained
          uninterrupted access to the AMCA system, containing the PII and PHI of
          nearly 12 million customers of Quest Diagnostics.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
