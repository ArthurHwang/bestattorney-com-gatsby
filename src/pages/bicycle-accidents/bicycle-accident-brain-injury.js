// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bicycle Accident Head injuries - What is your claim worth?"
        pageDescription="Find out what your personal injury claim is worth after a head injury from a bicycle accident"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Head Injuries From a Bicycle Accident</h1>
        <BreadCrumbs location={location} />
        <p>
          If you or a loved one has suffered a head injury from a bicycle
          accident, the experienced{" "}
          <Link to="/bicycle-accidents">California bike accident lawyers</Link>{" "}
          at Bisnar Chase can help you seek and obtain maximum compensation for
          your damages and losses. Head injuries caused by bicycle accidents can
          result in long-term or even permanent disabilities. Victims of these
          types of injuries and their families face physical, emotional and
          financial challenges on a daily basis. Our law firm has helped
          severely injured clients and their families get much-needed
          compensation for not just medical treatment, but to maintain a
          reasonable quality of life. <strong>Call us at (800) 561-4887</strong>{" "}
          to find out how we can help you.
        </p>
        <h2>Bicycle Accidents and Head Injuries</h2>
        <p>
          Statistics show that head injuries occur in 22 to 47 percent of
          bicycle accidents often as a result of a collision with a motor
          vehicle. Head injuries are responsible for more than 60 percent of all
          bicycle-related deaths and the majority of bicycle accident injuries
          that result in long-term disabilities. While football tends to
          dominate the discussion of sports-related head injuries, research
          shows that bike accidents account for far more traumatic brain
          injuries each year.{" "}
          <Link
            to="https://www.traumaticbraininjury.net/move-over-football-cycling-is-the-biggest-cause-of-sports-related-tbi/"
            target="_blank"
          >
            Cycling was also the leading cause of head injuries
          </Link>{" "}
          in children under 14, causing 40,272 injuries, roughly double the
          number related to football (21,878).
        </p>
        <h2>What are the Effects of a Brain Injury?</h2>
        <p>
          A traumatic brain injury usually results from a blow to the head that
          affects the brain. In bicycle accident cases, this occurs when a
          bicyclist is thrown to the ground or is thrown off the bike and hits
          the pavement or the vehicle that struck the bicycle. A brain injury is
          very different from a broken limb or a punctured lung because it's an
          injury that can affect every aspect of a human being – physical,
          emotional, behavioral and cognitive.
        </p>
        <p>
          Traumatic injuries may be mild such as a concussion or severe such as
          a skull fracture or swelling/bleeding in the brain. A brain injury can
          be classified as mild or severe. A concussion is an example of a mild
          brain injury although recent research suggests that even these types
          of injuries could have long-term effects. Some of the common effects
          of brain injuries include headaches, memory issues, mood swings, loss
          of consciousness and seizures. A number of traumatic brain injuries
          result in death.
        </p>
        <p>
          Survivors often struggle with limited physical functions, abnormal
          speech or language and other emotional or behavioral problems.
          Long-term rehabilitation is often necessary to maximize function and
          independence. Even with so-called mild brain injuries, the
          consequences to a person's life can be dramatic. Even a slight change
          in brain function can have a dramatic impact on an individual's family
          life, job, social and community interaction.
        </p>
        <h2>Bicycle Accident Head Injuries and Children</h2>
        <p>
          Bicycle accidents are the primary cause of traumatic brain injuries
          among children including teens. An estimated 107 bicyclists under the
          age of 15 dies in bicycle-related accidents each year and 12,000
          suffer some type of injury. Children between the ages of 0 and 20 make
          up 23.4 percent of cycling deaths each year. The most common cause of
          these types of bicycle accidents is collision with another vehicle.
          More than 540,000 people visit the nation's emergency room over
          bicycle-related collisions and of those about 67,000 suffer from some
          type of head trauma. So, head injuries account for over 60 percent of
          bicycle-related fatalities.
        </p>
        <p>
          Our law firm handled the case of a teenager who suffered a
          catastrophic brain injury in a bicycle accident. We helped the family
          secure a <strong>$16 million settlement</strong> from the city where
          the accident occurred. Our lawyers argued that a dangerous bicycle
          roadway condition caused the teen's catastrophic bicycle accident.
        </p>
        <h2>What is Your Head injury Claim Worth?</h2>
        <p>
          The value of a head injury case depends on a number of factors.
          Typically, brain injury cases bring verdicts or settlements in the
          millions – for good reason. Victims and their families undergo
          significant medical and other expenses, not to mention the pain and
          suffering they have to endure in the aftermath of such a traumatic
          event. The more severe the head injury, the higher the value of your
          claim is likely to be. Typically head injury victims can seek
          compensation for damages including:
        </p>
        <ul type="disc">
          <li>
            Medical expenses including emergency room costs, emergency
            transportation, hospitalization, cost of surgery, medication,
            diagnostic tests, medical equipment, etc.
          </li>
          <li>
            Cost of rehabilitation including physical, occupational and
            behavioral therapy
          </li>
          <li>
            Expenses relating to round-the-clock care and nursing for patient,
            if the injuries are catastrophic
          </li>
          <li>Lost income during recovery</li>
          <li>
            Lost future income and loss of earning capacity, if the injuries
            resulted in permanent disabilities
          </li>
          <li>Past and future pain and suffering</li>
          <li>Mental anguish</li>
          <li>Loss of consortium</li>
        </ul>
        <p>
          There is no question that the cost of caring for someone who is living
          with a{" "}
          <Link to="/head-injury/traumatic-brain-injury">head injury</Link> can
          run into the millions. An experienced bike accident lawyer who has the
          knowledge and resources to explore all potential avenues of
          compensation will be able to help you secure the monetary support you
          or your loved one needs to maintain an acceptable quality of life.
        </p>
        <h2>Contacting an Experienced Bike Accident Lawyer</h2>
        <p>
          In any personal injury case where there are significant medical
          expenses and other losses, having an experienced head injury lawyer on
          your side can make a huge difference. The California bicycle accident
          lawyers at Bisnar Chase have successfully handled bicycle accidents
          that resulted in brain injuries and have obtained millions for victims
          and their families. We work on a contingency fee basis, which means
          you pay nothing unless we recover compensation for you.{" "}
          <strong>Call us at (800) 561-4887 to for a free consultation</strong>{" "}
          and comprehensive case evaluation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
