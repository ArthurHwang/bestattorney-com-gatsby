// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import Img from "gatsby-image"
import { graphql } from "gatsby"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bike-accidents/Tustin Bicycle Lawyers Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Bicycle Accident Lawyer - Bike Crash Attorneys"
        pageDescription="California Bicycle accident attorneys with over 40 years of bike crash injury law can help with your accident claim. Call 800-561-4887 for a free consultation with an experienced trial lawyer with a proven track record in winning cases."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Bicycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Bicycle Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          If you have been involved in a bicycle accident, it is important that
          you contact the authorities, collect information and seek immediate
          medical attention. It is also critical that you seek the advice and
          guidance of an{" "}
          <strong>experienced California bicycle accident lawyer</strong> who
          will fight for your rights and ensure that you are fairly compensated
          for your losses.
        </p>
        <p>
          The skilled{" "}
          <Link to="/">
            California personal injury attorneys of Bisnar Chase
          </Link>{" "}
          have more than four decades of experience recovering damages for
          injured bicyclists and their families. Our success rate of 96 percent
          is practically unprecedented in the industry.{" "}
          <strong>We have collected more than $500 Million in damages</strong>{" "}
          for our clients and received superior ratings from peer groups such as
          the American Trial Lawyers Association and Martindale-Hubbell.
        </p>
        <p>
          If you or a loved one has been injured in a bicycle accident due to
          someone else's negligence or wrongdoing, please
          <strong>
            contact us at 800-561-4887 for a free consultation and comprehensive
            case evaluation
          </strong>
          .
        </p>
        <h2>The Value of Your Bicycle Accident Claim</h2>
        <p>
          Every bicycle accident victim case is different. So, there is no way
          to tell you how much your claim may be worth without examining the
          specifics of your case. Negligent drivers can be held accountable for
          all of the injuries, damages and losses they cause. This means that
          injured cyclists can seek financial compensation for their physical
          injuries, financial losses and mental anguish. A successful claim will
          include compensation for;
        </p>
        <ul>
          <li>lost wages</li>
          <li>lost future wages</li>
          <li>medical expenses</li>
          <li>hospitalization fees</li>
          <li>the cost of rehabilitation</li>
          <li>physical pain</li>
          <li>mental anguish and other related damages.</li>
        </ul>
        <h2>Common Causes of Bicycle Accidents</h2>
        <p>
          There are numerous ways in which bicycle riders can end up dead or
          left with serious accident injuries. Some of the common causes of
          bicycle accidents are as follows:
        </p>
        <ul>
          <li>
            Failure on the part of a motor vehicles to yield the right-of-way
          </li>
          <li>Bike lane intrusion</li>
          <li>Failure to stop at an intersection</li>
          <li>
            Dooring; accidents where drivers open doors of parked vehicles
            directly in bikers' path
          </li>
          <li>Drivers who are under the influence of alcohol and/or drugs</li>
          <li>Distracted drivers</li>
          <li>Roadways with potholes or debris</li>
          <li>Poor lighting</li>
          <li>Dangerous roadway design</li>
        </ul>
        <h2>Determining Liability for a Bicycle Accident</h2>
        <p>
          If you or a loved one has been hurt in a bicycle accident, you may be
          able to seek financial compensation for your losses. In order to hold
          an at-fault party accountable for your suffering, you will have to
          prove that the{" "}
          <strong>
            at-fault driver's negligence was a contributing factor
          </strong>{" "}
          in the crash. If the driver was speeding, impaired, distracted,
          fatigued or otherwise careless, he or she can be held accountable for
          your losses.
        </p>
        <h3>
          <strong>
            There are a number of ways to determine liability and prove fault.{" "}
          </strong>
        </h3>
        <ul>
          <li>Did anyone witness the crash?</li>
          <li>
            Were there surveillance cameras that can show how the crash
            occurred?
          </li>
          <li>
            Was the driver cited by the authorities or arrested for reckless
            behavior?
          </li>
          <li>
            Did you take photos of the crash site and preserve your bike for a
            thorough inspection?
          </li>
        </ul>
        <p>
          In some cases, other parties can also be held liable. For example, if
          a dangerous or defective roadway caused a bicycle accident, the city
          or governmental agency responsible for maintaining the roadway can be
          held liable as well. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            California Government Code Section 911.2
          </Link>
          , any personal injury or wrongful death claim must be filed within 180
          days of the incident.
        </p>
        <h2>California Bicycle Laws</h2>
        <img
          src="/images/bike-accidents/bike-accident-sign-thumbnail.jpg"
          width="160"
          height="148"
          className="imgright-fluid"
          alt="california bicycle accident lawyer"
        />
        <p>
          One of the best ways to reduce your chances of being involved in a
          California bicycle accident is to understand rules of the road. Under
          California Vehicle Code:
        </p>
        <ul>
          <li>
            <strong>Section 21200</strong>: You have the right to ride on the
            street except on freeways and bridges where no-biking signs are
            posted. This vehicle code also requires all riders to obey the same
            rules and laws as drivers of other vehicles including stopping at
            signs and traffic lights.
          </li>
          <li>
            <strong>Section 21202</strong>: You must remain on the right side of
            the lane unless you are passing a vehicle or preparing for a left
            turn.
          </li>
          <li>
            <strong>Section 21650</strong>: Allows bicyclists to leave the right
            side of the lane to avoid slow moving traffic and construction
            activity.
          </li>
          <li>
            <strong>Section 21650</strong>: You must ride with the flow of
            traffic and not against it.
          </li>
          <li>
            <strong>Section 21804</strong>: Riders must yield to traffic before
            entering the roadway.
          </li>
        </ul>
        <img
          src="/images/bike-accidents/anaheim-bicycle-accident-lawyer-helmet-floor.jpg"
          width="100%"
          alt="California Bicycle Accident Attorney"
        />
        <h2>Staying Safe on Your Bike</h2>
        <p>
          Riding your bicycle is a healthy way of getting around, saving money
          and doing your part to preserve the environment. Unfortunately, bike
          riding can be extremely dangerous. If you are an avid rider, it is
          important you understand not only how to stay safe on busy roadways,
          but also how you can protect your rights if you have been injured in a
          bicycle accident caused by someone else's negligence or wrongdoing.
        </p>
        <p>
          Bicycle accidents can happen at any time. All bicyclists must do their
          part to prevent injury accidents. At the same time, motorists must
          also remember that they share the roadways with other vehicles such as
          motorcycles and bicycles. Perhaps the best way to prevent an accident
          is to ride defensively.
        </p>
        <p>
          Keep your distance from vehicles, avoid blind spots and never assume
          that drivers can see you. You can also prevent accidents by making
          drivers aware of your intentions. Use hand signals when turning and
          make eye contact with nearby motorists before entering an
          intersection. There are a number of ways to increase your visibility,
          including wearing bright clothing, installing red and white
          reflectors, utilizing a white lamp on the front of your bike and
          turning on a flashing light on your rear seat. Above all, wear your
          helmet at all times.{" "}
          <Link to="/bicycle-accidents/bicycle-accident-brain-injury">
            Head injuries in a bicycle accident
          </Link>{" "}
          is all too common.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bike crash attorneys in California"
            style={{
              backgroundImage:
                "url('/images/bike-accidents/Tustin Bike Accident Attorneys Lady Justice.jpg')"
            }}
          >
            <h2>Passionate Representation</h2>
          </div>
        </LazyLoad>
        <p>
          We are dedicated to representing bike riders. The Southern California{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          <strong>
            have represented 12,000 accident victims and served California for
            more than four decades
          </strong>
          . We have the resources and talent to win extremely difficult cases
          including bicycle accidents. Whether you've been told by other
          attorneys that you don't have a case, our office may be able to help
          you receive compensation. Don't wait for the statute of limitations to
          expire. Get help today from our passionate and professional team of
          trial lawyers.{" "}
          <strong>
            Call the law firm of Bisnar Chase at 800-561-4887 for a free
            consultation.
          </strong>
        </p>
        <p>
          Video: The NHTSA offers bicycle safety tips for adults including
          learning how to "Drive your bike" just like you would drive a car.
          Mountain bikes, cruisers, and ten-speeds all have different safety
          precautions to take into consideration.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/jdrrxIpQpt4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1565883713362!5m2!1sen!2sus"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
