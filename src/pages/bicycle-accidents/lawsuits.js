// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bicycle Accident Lawsuits - What to Expect"
        pageDescription="Covering the legal process of bicycle accident lawsuits and what to expect after the injury ."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bicycle Accident Lawsuits: What to Expect</h1>
        <BreadCrumbs location={location} />
        <p>
          Being involved in a bicycle accident is a terrifying experience. Once
          the accident happens there will be a lot of healing to do, both
          physically and mentally. A bicycle rider is no match for a vehicle and
          this can result in serious and catastrophic injuries. Typically the
          driver of the vehicle is at fault, ignoring the safety laws in place
          for bike riders.
        </p>
        <p>
          Dangerous lane changes and being distracted are the most common causes
          of car on bike accidents. The minute the accident happens will be when
          you need to start collecting evidence for your case, but be sure to
          seek immediate medical care before anything else.
        </p>
        <p>
          Your bike accident lawsuit will be something an experienced{" "}
          <Link to="/bicycle-accidents">
            California bicycle accident lawyer
          </Link>{" "}
          should handle. From evidence to witness statements, the process can
          become very overwhelming.
        </p>
        <h2>After the Accident</h2>
        <ul>
          <li>Seek medical attention</li>
          <li>Call the police, file a police report</li>
          <li>Obtain all driver contact information</li>
          <li>Take photos if possible of scene and injuries</li>
          <li>Get witness statements</li>
          <li>Take notes before you forget any details</li>
          <li>
            <strong>Do not speak to any insurance adjuster</strong>
          </li>
          <li>
            As soon as possible, hire a local bike accident lawyer familiar with
            the area
          </li>
        </ul>

        <h3>The Legal Process</h3>
        <p>
          Depending on the extent of your injuries your case may settle or go to
          trial. In both situations, the case usually begins in the same way -
          your attorney will communicate with the insurance company to give
          details about the circumstances surrounding your accident and
          injuries, and how badly you were affected by the situation. This
          includes not only medical bills but time off work, property damage,
          and any other disability you may suffer as a result of your accident.
          Once paperwork has been delivered and all the facts of the case are
          clear, your attorney will demand an amount that they believe is fair
          from the insurance company. There will often be negotiation between
          the insurance adjuster and the attorney, unless the evidence for that
          amount of compensation is overwhelmingly powerful.
        </p>
        <p>
          If negotiations fall through however, your attorney will work with his
          team to file a lawsuit and start the discovery process. Many times
          cases do settle out of court unless the insurance company feels very
          strongly that they have a strong defense and a reason to deny you
          adequate compensation. In complicated cases your lawyer may hire
          expert witnesses to prove that you were not responsible for your
          injuries and that the insurance company owes you for their clients'
          mistakes.
        </p>

        <h3>Compensation</h3>
        <p>
          Your attorney will ask for compensation to cover your treatment and
          pain and suffering in most cases. He will also factor in current and
          lost wages, all your medical costs now and in the future, physical
          therapy and ongoing rehabilitation as well as any long term care you
          might require.
        </p>
        <p>
          An experienced injury attorney will take all of these factors into
          consideration to completely protect your interest. If you don't have
          complete faith in your attorney consider switching to one who will
          have your best interest.
        </p>

        <h3>Bicycle Safety Tips to Remember</h3>
        <p>
          If you've been injured in a bicycle accident chances are you are now
          far more cautious than before. Always keep these tips in mind when
          riding your bike on public streets.
        </p>
        <ul>
          <li>Make sure your bicycle is fully operational.</li>
          <li>
            Always inspect your bike before use. If you are unable to make an
            evasive maneuver, your chances of being involved in a collision
            increase.
          </li>
          <li>Wear a helmet.</li>
          <li>
            The Insurance Institute for Highway Safety (IIHS) reports that
            wearing a helmet reduces your risk of head injury by 85 percent.
          </li>
          <li>Stay in control.</li>
          <li>
            Keep both hands on your handlebars. Your ability to make evasive
            maneuvers is drastically reduced when riding with one hand.
          </li>
          <li>Make yourself visible.</li>
          <li>
            Whether you are riding during the day or at night, wear bright
            colors. Always equip your bicycle with a light or reflective device
            when riding at night.
          </li>
          <li>Avoid Road Hazards</li>
          <li>
            Keep your eyes open for construction areas, animals, puddles, and
            other obstacles. When riding in groups, make sure that the person
            leading keeps those behind him informed of upcoming hazards.
          </li>
        </ul>
        <h3>Ready to Seek Advice?</h3>
        <p>
          Contact our experienced bike accident lawyers to find out what you may
          be entitled to. We never charge a fee unless we win your case so
          you'll know that we are only being paid for work we perform.
        </p>
        <p>
          We also shield you from liability by advancing all costs until your
          case settles. We've represented more than 10 people and collected over
          $500 Million in{" "}
          <Link to="/case-results">verdicts and settlements</Link>. Contact us
          today for a free consultation. Call 1-800-561-4887.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
