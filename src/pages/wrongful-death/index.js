// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- /
// -------------------------------------------------- /
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident",
  spanishPageEquivalent: "/abogados/accidentes-de-camiones"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-wrongful-death-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Wrongful Death Attorney - Trusted CA Lawyers"
        pageDescription="Our California Wrongful Death Attorneys have successfully represented thousands of clients & won hundreds of millions for their loved ones. Get compensated for your pain and suffering and do so before the wrongful death statute of limitation is expired."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california wrongful death attorneys"
            title="california wrongful death attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            The California Wrongful Death Attorneys of{" "}
            <strong> Bisnar Chase </strong>have been representing and winning
            cases for over <strong> 39 years</strong>. Having won over
            <strong> $500 Million </strong>for their clients, they have
            established a <strong> 96% success rate </strong>and an
            authoritative and impressive reputation in the courtroom and the
            real world.
          </p>
          <p>
            This law firm is passionate and dedicated to bringing the victims of
            wrongful deaths to justice, and helping rebuild the lives that were
            shattered in many different types of cases, but more specifically,
            wrongful deaths.
          </p>
          <p>
            When you are a client of Bisnar Chase, you are apart of the family.
            Your questions will always be answered and made to feel one at home
            with us.
          </p>
          <p>
            The paralegals, negotiators, mediators and other legal staff are
            known for their friendly and charismatic personalities, professional
            and efficient work ethics, and ability to always take each step
            while holding your hand through this incredibly difficult
            experience.
          </p>
          <p>
            We are here to help, and will take every step to make sure you are
            comfortable and know whats going on at all times.
          </p>
          <p>
            Get your <strong> Free Consultation </strong>and{" "}
            <strong> Case Evaluation </strong>today. For immediate help or to
            get answers about a wrongful death damages claim, contact our
            experienced and trusted{" "}
            <strong> California wrongful death lawyers</strong> at{" "}
            <strong> 800-561-4887</strong>.
          </p>
          <p>
            We will discuss your options with you and if we are able to help
            you, we'll provide a <strong> no payment guarantee</strong> that if
            we don't win your wrongful death case, you won't pay.
          </p>
          <p>
            In addition, we shield you from liability throughout your case by
            advancing all costs until your case settles. We have experienced
            wrongful death trial attorneys and litigation staff. This is
            important because not all wrongful death attorneys go to trial. We
            do.
          </p>
        </div>
        <center>
          <p>
            <em>
              Attorney{" "}
              <Link to="/attorneys/john-bisnar" target="new">
                John Bisnar
              </Link>{" "}
              Talks About California Wrongful Death Claims
            </em>
          </p>
        </center>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_okWKolf9qc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Recovering from the loss of a family member is one of life's most
          difficult challenges. Following your loss, the weight of the new
          responsibilities that rest upon your shoulders may seem too difficult
          to bear.
        </p>
        <p>
          Our{" "}
          <strong>
            {" "}
            California wrongful death attorneys have assisted more than 12,000
            clients since 1978
          </strong>{" "}
          and have developed a 96% success rate while recovering hundreds of
          millions of dollars for those they represent.
        </p>
        <p>
          We understand how to treat clients recovering from debilitating grief
          and will give you straight forward answers to help you put your life
          back on track.
        </p>
        <p>
          Whether you have a wrongful death damages case or not, you can count
          on Bisnar Chase{" "}
          <Link to="/" target="new">
            Personal Injury Attorneys
          </Link>{" "}
          to give you straight forward answers and advice to help you make the
          best decision for you and your family.
        </p>
        <p>
          <strong>
            {" "}
            If you have lost a loved one as a result of someone's negligence,
            contact us today to take advantage of our free professional
            consultation
          </strong>
          .
        </p>
        <p>
          Bisnar Chase have represented California wrongful death victims for
          over 39 years. John Bisnar, managing partner, has instilled a passion
          for justice in every staff member and has set the bar for personal
          injury law firm standards of excellence across the nation.
        </p>
        <p>
          Brian Chase, senior partner, has achieved unrivaled courtroom success;
          in the last year alone he has recovered millions of dollars for his
          clients. In August of 2011, Brian obtained a record-breaking verdict
          for <strong> $24.7 million dollars</strong>.
        </p>

        <h2>Damages That Can Be Recovered From a CA Wrongful Death Claim</h2>

        <p>
          A California wrongful death claim can be complicated and particular,
          which is why it is so important to have an experienced wrongful death
          attorney on your side.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/california-legal-damages.jpg"
            width="100%"
            alt="california wrongful death laywer"
          />
        </LazyLoad>
        <p>
          As the survivor of a loved one, you can file a wrongful death lawsuit
          for the recovery of the following three kinds of damages:
        </p>
        <ol>
          <li>
            The loss of the love, companionship, comfort, affection, society,
            solace, moral support, and (if a spouse is a claimant) consortium of
            the decedent;
          </li>
          <li>
            The value of the household services the decedent would have provided
            in the future, if any, and
          </li>
          <li>
            The value of the financial support which the claimant would have
            received from the decedent but for the death.
          </li>
        </ol>
        <p>
          After a loved one has passed away, there are only certain people who
          may file a wrongful-death claim. First in line are the surviving
          spouse, children, and surviving issue of deceased children of the
          decedent. These survivors can file separately or jointly. If these
          types of claimants do not exist, next in line are the person's:
        </p>
        <ul>
          <li>parents</li>
          <li>then brothers and sisters</li>
          <li>then the children of deceased brothers and sisters</li>
          <li>then grandparents</li>
          <li>and then their lineal descendants</li>
        </ul>
        <p>
          A second group of claimants, which are not covered in the first group
          are:
        </p>
        <ul>
          <li>
            putative spouse (a person who can prove that he/she had good faith
            belief that they were married to the decedent, but were not married
            by law)
          </li>
          <li>children of putative spouse</li>
          <li>stepchildren</li>
          <li>and parents of decedent</li>
        </ul>
        <p>
          The only way that those qualified under the second group can file a
          wrongful death claim is if they can prove that they were dependent
          upon the decedent at the time of death.
        </p>
        <p>
          The final class of claimant is any minor who can prove that they were
          a resident of the decedent's household for 180 days prior to the death
          and that they were dependent upon the decedent for at least 50% of
          their support at the time of death.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california wrongful death claim attorney"
            style={{
              backgroundImage:
                "url('/images/text-header-images/potential-claim-thinking.jpg')"
            }}
          >
            <h2>The Potential Value of a California Wrongful Death Claim</h2>
          </div>
        </LazyLoad>

        <p>
          Sometimes, the value of a wrongful death claim is determined by the
          defendant's liability insurance coverage or his or her assets.
          However, an experienced California wrongful death attorney can help
          victims' families obtain maximum compensation by carefully examining
          the specific aspects of the case at hand.
        </p>
        <p>
          First, it may have to be determined if an estate should be created.
          When there are several individuals claiming compensation, a probate
          estate may be created. The wrongful death claim will then be brought
          in the name of the estate rather than the individual claimants.
        </p>
        <p>
          Another important step in a wrongful death case is to establish the
          life expectancy of the decedent. Some of the factors that may be taken
          into consideration include the victim's age, lifestyle, medical
          history and occupation.
        </p>
        <p>
          It can also be a challenge to determine how much financial support
          survivors lost as a result of their loved one's death. This would
          require an assessment of the decedent's employment and salary history,
          anticipated future wages including promotions, raises, etc.
        </p>
        <h2>Wrongful Death Versus Murder</h2>
        <p>
          What is the difference between wrongful death and murder?{" "}
          <Link
            to="http://www.encyclopedia.com/social-sciences-and-law/law/law/wrongful-death"
            target="new"
          >
            Wrongful death lawsuits
          </Link>{" "}
          are a civil action. These are lawsuits brought forth by the deceased
          victim's estate or family members, most often, immediate family
          members.
        </p>
        <p>
          The claimants attempt to prove that the defendant was responsible or
          civilly liable for the deceased person's death as the result of a
          careless, deliberate or negligent act. The burden of proof is also
          different and lesser in civil lawsuits.
        </p>
        <p>
          Murder or homicide cases on the other hand are criminal cases. The
          burden of proof in criminal cases is on the prosecution. The
          prosecutor must prove &ldquo;beyond a reasonable doubt&rdquo; that the
          person committed the murder.
        </p>
        <p>
          It is important to remember that the wrongdoer will be punished and
          ordered to pay restitution in a murder case. But only a civil wrongful
          death claim can help compensate victims' families monetarily for the
          losses sustained.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california wrongful death legal representation"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-california-wrongful-death-lawyers.jpg')"
            }}
          >
            <h2>Contacting an Experienced Wrongful Death Attorney</h2>
          </div>
        </LazyLoad>

        <p>
          The experienced <strong> California Wrongful Death Attorneys</strong>{" "}
          at Bisnar Chase have over <strong> 39 year of experience</strong>{" "}
          successfully handling complex wrongful death cases. We handle our
          cases not only with skill, but also with sensitivity and compassion.
          Our <strong> 96% success rate </strong>speaks for itself.
        </p>
        <p>
          If you would like to hear it from our clients, check out our{" "}
          <Link to="/about-us/testimonials" target="new">
            Video Testimonials
          </Link>
          .
        </p>
        <p>
          We understand what families are going through after a loved one's
          death. We understand that no amount of money can bring back a beloved
          family member. But, we are here to help you through this difficult
          time and do everything in our power to help you get justice and fair
          compensation for your losses.
        </p>
        <p>
          To determine if you are eligible in any category to file a California
          wrongful death claim, it is important to have a knowledgeable attorney
          on your side. By retaining a CA wrongful death attorney from Bisnar
          Chase, you can be assured that we will take care of all the paperwork,
          research, and answer all of your questions.
        </p>
        <p>
          Please call our office for your free and confidential consultation
          with one of our wrongful death attorneys. We can help you answer any
          questions and tell you how to handle the case.
        </p>
        <p>
          <strong> Call us at 949-203-3814</strong> For a Free, No-Hassle,
          Confidential, No-Obligation Consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
