// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="PTSD Symptoms and Treatment Options - Bisnar Case"
        pageDescription="Post Traumatic Stress is a mental disorder brought on by serious injury or harm. If you suffer from PTSD, you may qualify for compensation. Continue reading to learn more from our attorneys"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>PTSD - Post Traumatic Stress Disorder Syndrome</h1>
        <BreadCrumbs location={location} />

        <p>
          According to the APA (American Psychological Association) motor
          vehicle accidents are the leading cause of PTSD. Any type of violent
          incident or accident (assault, car accident, etc.) can be extremely
          traumatic.
        </p>
        <p>
          For many victims of trauma, the consequences of the incident will fade
          just as their physical wounds heal. Unfortunately, may other trauma
          victims develop what is known as Post Traumatic Stress Disorder (PTSD
          or PTSS), which calls for more intensive treatment.
        </p>
        <p>
          If you suffer from PTSD from the careless actions of the at-fault
          party, chances are that the quality of your life has diminished due to
          daily suffering and expensive medical bills for your pain and
          suffering. If this is the case, you may be entitled to compensation
          and our California{" "}
          <Link to="/catastrophic-injury" target="_blank">
            {" "}
            catastrophic injury lawyers
          </Link>{" "}
          can help you take legal action to obtain this compensation.
        </p>
        <p>
          Call us now at <strong>800-561-4887</strong> for your free case
          review.
        </p>
        <p>
          Continue reading to learn more about Post Traumatic Stress Disorder
        </p>
        <h2>What is PTSD?</h2>
        <p>
          During a traumatic situation, your heart may begin pounding,
          adrenaline may be pumping and your mind will be racing. Your fear and
          your body's response to that fear allows you to either fight against
          the situation or run from it.
        </p>
        <p>
          The "fight-or-flight" response to danger is a healthy and normal
          reaction. Someone suffering from PTSD will have a damaged or altered
          flight-or-fight response. They often feel stressed or even terrified
          long after they have been pulled out of a potentially dangerous
          situation.
        </p>
        <p>
          PTSD can develop after a frightening ordeal that either resulted in
          physical harm or the threat of physical harm. It is possible to
          develop PTSD if you are the person who was injured or even if you only
          witnessed the harmful event.
        </p>
        <p>
          For example, soldiers who witness friends getting shot can experience
          PTSD just as much as those who are wounded. You do not, however, have
          to go to war to suffer from PTSD.
        </p>
        <p>
          Victims of dog attacks, mugging, rape, torture, child abuse, car
          accidents, plane crashes, bombings or even natural disasters can
          suffer from PTSD as well.
        </p>
        <h2>Symptoms of PTSD</h2>
        <p>
          Many PTSD symptoms can be grouped into <strong>three</strong>{" "}
          different categories:
        </p>
        <ol>
          <li>
            <b>Re-experiencing trauma</b>: For some, PTSD manifests in the form
            of flashbacks, bad dreams and terrifying thoughts. Many struggle
            with reliving their trauma over and over. In such cases, they may
            experience fight-or-flight like symptoms despite not currently being
            in any danger. There are many things that may trigger these
            debilitating symptoms such as a noise, object, situation or even the
            person's own thoughts.
          </li>
          <li>
            <b>Avoidance</b>: Some victims of PTSD either feel emotionally numb
            or for a strong need to stay away from places and events that remind
            them of their traumatic experience. Those who were involved in a
            traumatic car accident may never step into a car again. Victims of
            scary dog bite incidents may do whatever it takes to avoid being
            near a dog. Others experience guilt, depression, anxiety or worry.
            For many, PTSD can result in a loss of interest in activities they
            once loved.
          </li>
          <li>
            <b>Hyper-arousal</b>: It is common for victims of PTSD to get easily
            startled or to feel on-edge. Some experience difficulties sleeping.
            Instead of being triggered like re-experiencing symptoms,
            hyper-arousal symptoms can be constant. Many suffering from these
            types of symptoms are stressed or angry. Acute stress is common for
            victims of traumatic experiences, but it becomes PTSD if it persists
            for weeks or months.
          </li>
        </ol>
        <h2>PTSD and Children</h2>
        <p>
          Children can suffer from PTSD after being in a serious accident such
          as a car crash, swimming pool drowning incident or dog attack. While
          some children are able to move on from their experience, others have
          extreme reactions.
        </p>
        <p>
          Young children struggling with PTSD may start to wet their bed, lose
          their ability to communicate, act out in surprising ways or become
          clingy to their parents.
        </p>
        <p>
          Older children may begin to exhibit destructive behaviors or
          experience extreme guilt. It is important that children are given the
          opportunity to discuss what they have gone through with a
          professional.
        </p>
        <h2>Genetics and PTSD</h2>
        <p>
          As is true with any illness, disease or disorder involving the brain,
          PTSD is incredibly complex.
        </p>
        <p>
          Scientists are actively researching how fear memories are created. For
          example, scientists are looking into stathmin, a protein integral to
          the formation of fear memories and gastrin-releasing peptide (GRP), a
          chemical in the brain that is released during emotional events.
        </p>
        <p>
          According to the National Institute of Mental Health, mice that do not
          make stathmin are less likely to freeze when facing danger and those
          with GRP can control their fear response.
        </p>
        <p>
          Unfortunately, many different genes likely affect PTSD. So there will
          be no easy or simple cure. Doctors hope to create targeted treatments
          that suit each victim's individual needs. One day, they may even be
          able to prevent PTSD before it causes harm.
        </p>
        <h2>The Medical Cost of PTSD</h2>
        <p>Many victims of PTSD struggle to return to normalcy.</p>
        <p>
          Every aspect of their life is affected by the stress they experienced
          and the anxiety they continue to experience. These symptoms can often
          be lessened through various forms of therapy.
        </p>
        <p>
          It is necessary for victims of trauma to talk about their feelings and
          what they went through. For others, drugs such as Zoloft or Paxil may
          be needed to treat depression and reduce symptoms such as anger, worry
          and sadness.
        </p>
        <p>
          Treatment for PTSD is necessary, but it is not cheap. Anyone who is
          experiencing PTSD because of the negligence of someone else should
          seek legal help immediately to receive compensation. The post
          traumatic stress disorder accident attorneys at Bisnar Chase can help
          you take legal action against the at-fault party and we don't charge
          you until we win your case.
        </p>
        <p>
          Call us now at <strong>800-561-4887</strong> now to set up your free,
          no-obligation case review.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
