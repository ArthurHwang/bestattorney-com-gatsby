// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nerve Damage: What You Need to Know - Bisnar Chase"
        pageDescription="Nerve damage can change a victim's life forever. Therefore we have made a comprehensive guide for nerve damage injury. We also offer free consultation in the case you want to take legal action."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Nerve Damage Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          The nervous system is a part of everything the body does. It regulates
          breathing, controls muscles and senses temperature.
        </p>
        <p>
          Nerves are an essential part in many body functions. With that said,
          if you suffer from nerve pain or damage, it can have a significant
          impact on your quality of life.
        </p>
        <p>
          This is why the California{" "}
          <Link to="/catastrophic-injury" target="_blank">
            {" "}
            catastrophic damage injury lawyers
          </Link>{" "}
          at Bisnar Chase have composed a comprehensive guide for what to do in
          the case of nerve damage. In addition, if your nerve damage was caused
          by the negligence of another party, the attorneys at Bisnar Chase can
          help you take legal action to receive compensation to help pay for you
          inevitable medical bills.
        </p>
        <p>
          Continue reading to learn more about nerve damage and call{" "}
          <strong>800-561-4887</strong> now to set up your free case review.
        </p>
        <p>For immediate legal help, call us at 800-238-4683</p>
        <h2>Various Types of Nerves in the Body</h2>
        <p>There are three types of neurons, or nerves, in the body:</p>
        <ul>
          <li>
            <strong>Motor nerves:</strong> These nerves control movement. They
            pass information from the brain and spinal cord to the muscles.
          </li>
          <li>
            <strong>Autonomic nerves:</strong> These transmitters control heart
            rate, blood pressure, temperature regulation and digestion.
          </li>
          <li>
            <strong>Sensory nerves</strong>: These neurons are why we feel pain
            and other sensations because they relay information from the skin
            and muscles back to the spinal cord and brain.
          </li>
        </ul>
        <h2>Symptoms of Nerve Damage</h2>
        <p>
          Since there are three different types of nerves and they are located
          throughout the entire body, there is a wide array of nerve damage
          symptoms. Which nerves are damaged and where they are affected have a
          significant impact on the type of symptoms that the victim will
          suffer.
        </p>

        <p>
          If you sustain damage to your <strong>motor nerves</strong>, you may
          experience twitching, muscle atrophy and even paralysis. Trauma to{" "}
          <strong>autonomic nerves</strong> can result in excessive sweating,
          lightheadedness, constipation, sexual dysfunction, bladder dysfunction
          and an inability to sense chest pain. Damaged{" "}
          <strong>sensory nerves</strong> can result in numbness, sensitivity,
          burning, tingling and issues with positional awareness.
        </p>
        <p>
          It is even possible for victims of nerve damage to sustain trauma to
          multiple types of nerves in a single incident. In such cases, the
          consequences may include a combination of symptoms such as
          lightheadedness, weakness and burning all at the same time.
        </p>
        <h2>Causes of Nerve Damage</h2>
        <p>
          There are many different types of nerve damage and they all have
          different symptoms and treatments. Here are a few examples of the many
          causes of nerve damage:
        </p>
        <ul>
          <li>
            <b>Cancer</b>: Cancerous masses can push against or even crush
            nerves resulting in nerve pain or damage. Cancer treatments, such as
            chemotherapy and radiation, can result in nerve damage as well.
          </li>
          <li>
            <b>Trauma</b>: Whenever the nerves are impacted or compressed, it
            can result in nerve damage. Examples of nerve damage injuries from
            trauma include pinched nerves and crush injuries. You can suffer
            significant nerve damage in a slip-and-fall accident, sporting
            accident or car accident.
          </li>
          <li>
            <b>Drugs</b>: Many powerful medications, especially those used to
            treat cancer or HIV, are known to cause nerve pain.
          </li>
          <li>
            <b>Diseases</b>: Lou Gehrig's disease results in progressive nerve
            damage. Infectious diseases that can affect nerves include herpes
            viruses, hepatitis C and Lyme disease.
          </li>
          <li>
            <b>Vitamin deficiencies</b>: Having low levels of vitamin B6 or B12
            can result in nerve pain or even nerve damage.
          </li>
          <li>
            <b>Dog bites</b>: Severe dog mauling incidents could result in nerve
            injuries.
          </li>
          <li>
            <b>Defective products</b>: Products that are poorly made or designed
            can also cause nerve injuries. An example is trans-vaginal mesh
            products that have been known to cause serious nerve injuries.
          </li>
        </ul>
        <h2>Treatment for Nerve Damage</h2>
        <p>Many nerve injuries are permanent.</p>
        <p>
          There are a number of treatments, however, that can reduce the
          symptoms. It is important to remember that nerve damage is often
          progressive, which means it gets worse over time. This is why it is
          important to see a doctor when you first notice symptoms. Early
          diagnosis and treatment will greatly reduce the likelihood of
          permanent damage.
        </p>
        <p>
          Treatment may begin with treating the underlying condition that may be
          causing the nerve pain or damage.
        </p>
        <p>
          This may include surgery, physical therapy, nutritional deficiency
          correcting or the regulation of blood sugar levels. Then, the
          treatment may involve lessening the nerve pain through pain relievers,
          antidepressants or anti-seizure medications.
        </p>
        <p>
          Some people have experienced great success with alternative methods
          such as acupuncture, biofeedback, meditation or hypnosis as well.
        </p>
        <h2>Types of Nerve Damage</h2>
        <ul>
          <li>Peripheral nerves</li>
          <li>Compressed nerve</li>
          <li>Radial nerve</li>
          <li>Obturator nerve</li>
          <li>Brain nerve damage</li>
          <li>Oculomotor nerve</li>
          <li>Foot or finger nerve damage</li>
          <li>Axillary nerve damage</li>
          <li>Trochlear nerve damage</li>
          <li>Sciatic Nerve</li>
        </ul>

        <h2>The Cost of Treating Nerve Damage</h2>
        <p>
          Unfortunately,{" "}
          <Link
            to="https://www.webmd.com/pain-management/nerve-pain-self-care#1"
            target="_blank"
          >
            treatment for nerve damage
          </Link>{" "}
          can be expensive. Since nerve damage rarely heals fully, many victims
          require treatment for years.
        </p>
        <p>
          Depending on the severity of the damage, some victims are not able to
          return to work and others can only return in a limited manner.
          Fortunately, there are legal options available for some victims of
          nerve damage.
        </p>
        <p>
          If you have suffered nerve injuries on the job, you may be able to
          pursue financial compensation for your losses through workers'
          compensation insurance. If your work related injuries resulted from a
          defective product or the negligence of someone other than your
          employer, you may be able to file a third-party claim as well.
        </p>
        <p>
          When nerve damage results from a traffic accident, victims can file a
          claim against the driver responsible for the crash. In such cases,
          support may be available for medical bills, future medical treatments,
          lost wages, pain and suffering, physical therapy fees and other
          related damages.
        </p>
        <p>
          Regardless whom the at-fault party may be, the experienced California
          nerve damage accident attorneys at Bisnar Chase will help you take
          legal action to get you the compensation that you not only need, but
          also deserve.
        </p>
        <p>
          Call us today at <strong>800-561-4887</strong> to set up your free
          consultation with one of are award winning nerve damage injury
          lawyers.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
