// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/California Neck Injury Case Lawyers Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Neck Injury Case Lawyer - Facet Joint Injury Lawyers"
        pageDescription="The California Neck Injury Case Lawyers of Bisnar Chase have been helping people who have experienced facet joint neck injuries from causes such as whiplash win over $500 Million in compensation. Bisnar Chase' injury attorneys have been specializing in neck injury cases for 40 years. Call 800-561-4887 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Neck Injury Case Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Neck Injury Case Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          For the past <strong>40 years</strong>, the{" "}
          <strong>California Neck Injury Case Lawyers</strong> of{" "}
          <Link to="/orange-county" target="_blank">
            Bisnar Chase
          </Link>{" "}
          have been{" "}
          <strong>gaining hundreds of millions in compensation</strong> for
          victims who have suffered severe neck trauma. Neck injuries can leave
          a person with hefty medical bills, the inability to work and can even
          affect intimate relationships.
        </p>
        <p>
          Our neck facet joint injury attorneys believe that you deserve to be
          compensated for someone else's carelessness. With four locations and a
          litigation team on standby, the law firm of Bisnar Chase has been able
          to hold a <strong>96% success rate</strong> for our clients.
        </p>
        <p>
          If you or a loved one has experienced a serious neck injury due to a
          third party's negligence <strong>Call 800-561-4887</strong>. When you
          contact us, you will receive a<strong> free case analysis</strong> and
          an opportunity to speak to a highly-rated California neck injury case
          attorney. Victims of neck injuries shouldn't have to pay out-of-pocket
          to recover.
          <strong> Contact us today</strong>.
        </p>
        <h2>5 Causes of Neck Injuries</h2>
        <p>
          Neck injuries otherwise known as cervical spine injuries cause a
          person to feel symptoms such as stiffness in the neck, severe pain and
          also can cause discomfort in the jaw and radiating{" "}
          <Link to="/catastrophic-injury/back-injury-lawyer">back pain</Link>.
          The type of treatment that is necessary for pain in the neck depends
          on the cause of the neck strain. Below are five prevalent causes of
          neck injuries.
        </p>
        <p>
          <strong>5 common causes of neck injuries</strong>:
        </p>
        <ol>
          <li>
            <strong>Whiplash</strong>: Experts report that over 75% of whiplash
            victims have experienced symptoms for up to 6 months or longer.{" "}
            <Link
              to="https://www.webmd.com/pain-management/guide/pain-management-whiplash"
              target="_blank"
            >
              Whiplash
            </Link>{" "}
            occurs when the neck suddenly undergoes a sudden force that causes
            the neck to move back and forth abruptly. Treatment for whiplash
            injuries ranges from prescription medications to lidocaine
            injections which numbs the area.
          </li>

          <li>
            <strong>Vehicle accidents</strong>: At the time of a{" "}
            <Link to="/car-accidents">car crash</Link> the impact and
            acceleration that the body endures can cause serious impairments to
            the cervical spine. Although neck injuries from collisions can be
            treated, the spine will never fully recover if it has been damaged.
          </li>

          <li>
            <strong>Carrying bulky objects</strong>: Many people sprain their
            neck vigorously just by performing everyday tasks. Whether is it
            lifting boxes at work or hauling luggage around in the airport it is
            important not to put too much stress on your neck or back. Physical
            therapist, Barbara B. Boucher shares the proper technique when
            lifting heavy objects. "When lifting, you should always keep the
            object close to your body to avoid straining both neck and back
            muscles. When lifting from the ground, bend down from your hips and
            knees. Try to get close to the object," says Boucher.
          </li>

          <li>
            <strong>Athletic Activities</strong>: It is very common that at the
            beginning of a training season that athletes experience neck
            strains. Neck strains can also be referred to as pulling or tearing
            a muscle. Athlete or not though, performing any type of new activity
            that requires an exceptional amount of physical movement can pull
            your neck muscle. If you do not slowly transition into vigorous
            training you can suffer from a{" "}
            <Link to="/head-injury/traumatic-brain-injury-tips" target="_blank">
              traumatic brain injury
            </Link>
            . Rest or messages are usually required after an athlete experiences
            a neck strain.
          </li>

          <li>
            <strong>Bad posture while working</strong>: It may seem trivial but
            hunching all the time over a computer can actually negatively affect
            your neck over time. If pressure or forward head movement is a daily
            action, then there is a huge probability of the small facet, which
            is located in the joints, can become aggravated. The{" "}
            <Link
              to="https://www.spine-health.com/conditions/arthritis/facet-joint-disorders-and-back-pain"
              target="_blank"
            >
              facets in the spine
            </Link>{" "}
            are one of the elements that aid in flexibility. If your employment
            requires you to sit at a desk make sure that both feet are on the
            ground and your back is straight.
          </li>
        </ol>
        <LazyLoad>
          <img
            src="/images/images/Neck Injury center image.jpg"
            width="100%"
            alt="California neck injury case attorneys"
          />
        </LazyLoad>

        <h2>Common Types of Neck Injuries</h2>
        <p>
          Damage to the cervical spine can range from being moderate to serious.
          When there is even a slight amount of damage to the neck, this can
          affect other parts of the spine such as the soft tissue, nerves,
          muscles or ligaments. Soft tissue that is harmed is treatable but if
          there is severe injury to the spine it can be dangerous.
        </p>
        <p>
          <strong>Herniated Disc</strong>: A{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/herniated-disk/symptoms-causes/syc-20354095"
            target="_blank"
          >
            herniated disc
          </Link>{" "}
          occurs in age groups from 30-50 years old and takes place when the
          core of a disc slips out and then puts pressure on the cervical nerve.
          If the cervical nerve experiences pressure then most likely, the pain
          would radiate down the arm to the fingers.
        </p>
        <p>
          <strong>Cervical Dislocation</strong>: A cervical dislocation takes
          place when bones in the spine become separated. People who experience
          cervical dislocations are usually victims of car accidents. Car crash
          victims who suffer from a cervical dislocation endure extreme
          stiffness and pain in the neck.
        </p>
        <p>
          <strong>Spinal Cord Injury</strong>: Even though the neck is the most
          flexible part of the spine, it still is one of the fragile areas. The
          cervical spine can only hold up to fifteen pounds in weight. If any
          more pressure is put upon the neck it can leave a person to be
          paralyzed.
        </p>
        <p>
          <strong>Neck Fracture</strong>: The neck is made up of seven cervical
          bones. If one of these bones is compromised a victim of a neck
          fracture can lose temporary sensation in their arms and legs. A minor
          neck fracture can take up to 8 weeks to heal and in more severe
          fractures, surgery would be the next option.
        </p>
        <h2>Knowing the Value of Your Case</h2>
        <p>
          Although neck injury cases differ from person-to-person, it is
          strongly suggested that any person who has experienced harm due to a
          third party's negligence should file a suit. Not all neck injury
          accident cases are the same.
        </p>
        <p>
          For example one of the many personal injury verdicts and settlements
          in California included an employee being awarded $25,000,000 million
          dollars. The employee, who was harmed and paralyzed in a construction
          accident, received the amount due to the status of his health and lost
          wages.
        </p>
        <p>
          The average herniated disc settlement value in California would vary
          from the individual who has faced paralysis, due to the on-going
          treatment that would follow. Below are elements that would establish
          the value of your case.
        </p>
        <p>
          <strong>Factors that determine the value of a case</strong>:
        </p>
        <ul>
          <li>Severity of the injury</li>
          <li>Medical expenses</li>
          <li>After-care assistance</li>
          <li>Wages lost</li>
          <li>Loss of consortium</li>
          <li>Pain and suffering</li>
          <li>Liability</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Neck injury lawyers in California"
            style={{
              backgroundImage:
                "url('/images/personal-injury/Tustin Personal Injury text header image - Copy.jpg')"
            }}
          >
            <h2>Bisnar Chase's Attorneys Are on Your Side</h2>
          </div>
        </LazyLoad>
        <p>
          The <strong>California Neck Injury Case Lawyers</strong> of Bisnar
          Chase guarantee that if there is no win there is no fee. Suffering
          harm to the cervical spine can be crippling and can be physically and
          emotionally damaging. Victims and family members deserve compensation
          for the losses that they have experienced.
        </p>
        <p>
          If you or someone you know is living with a serious neck injury due to
          someone else's negligence call <strong>Call 800-561-4887</strong>.
          Contact us today and receive a<strong> free consultation</strong> with
          an experienced California personal injury attorney. You and your
          family deserve justice for a third party's wrongdoing.{" "}
          <strong>Call today</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
            Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
