// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rotator Cuff Injury Lawyers - Bisnar Chase"
        pageDescription="Rotator cuff pain and injury can be brought on by repetitive strain from work. Call our Rotator Cuff Injury Lawyers for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rotator Cuff Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          In most cases, car accident victims will complain of neck and shoulder
          pain. This pain can be a warning sign of a rotator cuff injury that
          could require surgery. These rotator cuff surgeries can cost upwards
          of $80,000.00 and there is no guarantee that additional surgeries wont
          be needed.
        </p>
        <p>
          The rotator cuff is composed of the muscles and tendons of the
          shoulder. These muscles and tendons connect the shoulder blade to the
          upper arm and hold the ball of the upper arm in the shoulder socket.
          Injuring your rotator cuff can be debilitating and painful.
        </p>

        <p>
          Even small, day-to-day tasks, such as reaching for something on a
          shelf, can become extremely challenging. Fortunately, many rotator
          cuff injuries can heal on their own with rest and self-care. Some
          injuries, however, are much more serious and require immediate medical
          attention.
        </p>
        <p>
          If you have suffered serious injury from a negligent party that
          resulted in a rotator cuff injury, you may be entitled to compensation
          for your pain and suffering. The only way to know for sure is to
          schedule your free consultation with the expert Bisnar Chase{" "}
          <Link to="/catastrophic-injury">catastrophic injury attorneys</Link>{" "}
          now by calling
          <strong>800-561-4887</strong>.
        </p>
        <h2>Symptoms of Rotator Cuff Injuries</h2>
        <p>
          Rotator cuff injuries can result from sudden falls or from long
          repetitive arm motions. Therefore, the symptoms of rotator cuff
          injuries can occur immediately or they may slowly develop over days,
          weeks or months of manual labor.
        </p>
        <p>
          Common symptoms of rotator cuff injuries include pain, tenderness of
          the shoulder, a loss of range of motion in the shoulder, or weakness.
          The pain of rotator cuff injuries is particularly prominent when
          attempting to reach, lift or pull.
        </p>
        <p>
          Minor rotator cuff injuries can get better with ice and rest. Those
          who are healing may find heat, stretches and massages helpful.
        </p>
        <p>
          Unfortunately, these types of home remedies are not sufficient for all
          shoulder injuries. You should seek out medical attention if you are
          experiencing severe shoulder pain, if you are unable to use your arm,
          if your arm feels weak or if your shoulder has hurt for over a week.
          In addition, you should save all medical invoices and receipts
          pertaining to your recovery as they may help you in the long run
          acquire compensation for your injuries.
        </p>
        <h2>What Causes Rotator Cuff Injuries?</h2>
        <p>
          There are many ways in which you can hurt the muscles and tendons of
          your upper arm:
        </p>
        <ul>
          <li>
            Tendinitis is when the rotator cuff becomes inflamed because of
            overuse or overload. This is common for individuals who do a lot of
            overhead activities.
          </li>
          <li>
            Tears and strains can also occur. It is possible to weaken tendons
            through tendinitis resulting in{" "}
            <Link
              to="https://ard.bmj.com/content/53/6/359.short"
              target="_blank"
            >
              chronic tendon degeneration
            </Link>
            . Weakened tendons are prone to tears.
          </li>
          <li>
            Bursitis is when the bursa, a fluid-filled sac in the shoulder
            joint, becomes inflamed and irritated.
          </li>
        </ul>
        <p>
          In many cases, rotator cuff injuries result from normal wear and tear.
          Middle-aged individuals commonly experience a breakdown of the protein
          in the tendons and muscles surrounding the rotator cuff. As we age, we
          become more prone to injury and degeneration.
        </p>
        <p>
          In some cases, you can develop calcium deposits within the shoulder
          area that pinches the rotator cuff area as well. You can even cause a
          rotator cuff injury by slouching too much.
        </p>
        <p>
          Physical activities and falls are common contributing factors in
          rotator cuff injuries as well. For example, falls commonly result in
          shoulder injuries. If you use your arm to brace yourself while
          falling, you can bruise or tear a muscle or tendon.
        </p>
        <p>
          You can also strain yourself by lifting a heavy object or by reaching
          for something that is over your head. The object does not even have to
          be heavy to cause an injury as repetitive stress through consistent
          movement can cause inflammation and tearing. This is common for
          athletes as well as for painters, carpenters and individuals who work
          in construction and manufacturing.
        </p>
        <h2>Treatment for Rotator Cuff Injuries</h2>
        <p>
          Since rotator cuff injuries typically develop over time, you may not
          need to rush to the emergency room.
        </p>
        <p>
          Once you make your doctor's appointment, there are things you can do
          to reduce your symptoms. Make sure you rest your shoulder and avoid
          painful movements. You should also apply cold packs to your shoulder
          to reduce inflammation. If needed, you can also take over-the-counter
          drugs such as aspirin or ibuprofen.
        </p>
        <p>
          When meeting with your doctor, make sure you accurately describe where
          the pain is located. You also need to discuss what types of activities
          aggravated your pain, what movements make it worse and if you have any
          weakness or numbness in your arm.
        </p>
        <p>
          The doctor would likely order an x-ray to examine the bones as well as
          an MRI scan or ultrasound scan to determine the cause of your pain and
          the severity of the damage. From there, you may need steroid
          injections, surgery or arthroplasty.
        </p>
        <p>
          Your doctor's visit as well as all the various treatments will add up
          quickly to a very hefty bill. This is why that it is urgent that if
          you have suffered injury due to the neglience of another party that
          you should seek legal concil immediately. If you choose the expert
          California rotator cuff injury lawyers at Bisnar Chase, we will
          discuss your legal rights and options during your free consultation
          and we will help you take legal action as well against the at-fault
          party.
        </p>
        <h2>Getting the Legal Help You Need</h2>
        <p>
          Seeking help should not end with seeing a doctor. If you were injured
          at work, you may want to discuss your legal options with the attorneys
          at Bisnar Chase. We can discuss how you can get worker's compensation
          to help pay bills as you recover as well as how we can et you the
          settlement you deserve to make things right. If you were injured
          because of someone else's negligence, call us now at{" "}
          <strong>800-561-4683</strong> to set up your free consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
