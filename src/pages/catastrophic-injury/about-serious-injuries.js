// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="About Serious Injuries - Types of injuries"
        pageDescription="Bisnar Chase Personal Injury Attorneys discuss serious injuries resulting from car accidents to medical neglience. Click to learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>About Serious Injuries</h1>
        <BreadCrumbs location={location} />
        <p>
          Bisnar Chase offers legal assistance to all Californians. If you've
          been seriously injured and think you may be entitled to compensation,
          contact our{" "}
          <Link to="/catastrophic-injury">catastrophic injury lawyers</Link> for
          a free consultation.
        </p>
        <p>
          We have represented over <strong>12,000 thousand clients </strong>and
          have collected over <strong>$500 Million</strong> in settlements for
          them. Many of the cases we handle are catastrophic injuries that lead
          to lifelong medical care. From skull fractures to neurological
          problems, we've taken on some of the toughest cases and won. Our legal
          team is committed to the best possible representation for our clients.
        </p>
        <p>
          <strong>Types of Catastrophic Injuries:</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/catastrophic-injury/amputations" target="_blank">
              Amputation
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/burn-injuries" target="_blank">
              Burn injuries
            </Link>
          </li>
          <li>Traumatic Brain Injuries</li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              Severe Spinal Cord Injuries
            </Link>
          </li>
          <li>Chemical or Toxic Poisoning</li>
        </ul>
        <h2>Serious Injury from a Car Accident</h2>
        <p>
          Motor vehicle accidents can happen in the least expected of moments.
          One minute you and your loved ones are safely driving on a public road
          and in a fraction of a second, your vehicle receives the most
          devastating blow and all of your lives are changed forever.
        </p>
        <p>
          If the ear piercing sound of the on-impact collision isn't scary
          enough, more often that not, an innocent passenger is injured in the
          process and these victims are never the same after the accident.
        </p>
        <h2>Fatal Injuries Resulting from Serious Injury</h2>
        <p>
          There are many serious injuries a individual can sustain after a major
          car accident. More often than not, these are cases of blunt trauma and
          whiplash. If treated immediately, victims can expect to make a full,
          or close to full, recovery.
        </p>
        <p>
          However, if these victims do not get the medical attention they need
          in these critical moments for their serious injuries, the lasting
          damage can results in permanent brain damage and/or internal bleeding.
          Both of these serious injuries can lead to death if left untreated
          after the accident.
        </p>
        <p>
          In addition, parts and debris can scatter all over the place as a
          direct result of impact from the motor vehicle accident and when this
          happens, sometimes these pieces of debris can penetrate the bodies of
          the victims involved and put these victims in life threatening
          situations.
        </p>
        <p>
          In some cases, nearby individuals can removed the debris that
          penetrated the victim and apply pressure to the bleeding until
          paramedics arrive to the scene. However, in most cases, when a victim
          is penetrate by debris, it is best to leave the debris in the victim
          until trained medical processionals can remove it in the E.R. and
          operate accordingly.
        </p>
        <p>
          If you or a loved one has suffered serious injury or if you have lost
          a loved one from the lack of care after sustaining serious injury, you
          may be entitled to compensation. Call the experienced serious injury
          lawyers at Bisnar Chase now to know your rights and see if you qualify
          for compensation.
        </p>
        <h2>Pain and Suffering from Serious Injury</h2>
        <p>
          Fractures and dislocated joints can also be a resulting serious injury
          that derives from a car accident. Not only are these injuries
          extremely painful, they can also affect the quality of life after
          healing, and in some cases, fractures can be life threatening as they
          can indirectly pierce vital organs.
        </p>
        <p>
          Regardless of the serious injury, it is safe to say that life after
          these types of incidents will never be the same. With this in
          consideration, it is only just that the innocent victims should get
          fairly compensated for their injuries, pain and suffering. This is
          where the experienced serious injury attorneys at Bisnar Chase come
          into play. We have helped hundreds of injured clients over the past
          three decades that have been involved in motor vehicle incidents as
          describe above. We know the law and how we can use it to help you and
          your loved ones see the justice that you all deserve.
        </p>
        <p>
          If you have sustained serious injury from a car accident, call our
          lawyers at Bisnar Chase now for a free case review.
        </p>
        <h2>Serious Injury at Work and Worker's Compensation</h2>
        <p>
          Serious injury can come from a wide variety of places outside of auto
          accidents and auto trauma. The second most common place that serious
          injury occurs is in the workplace.
        </p>
        <p>
          Although federal laws are written to protect the average laborer
          on-the-job and the various labor unions that fight for the well being
          of its members, accidents happen and sometimes they can be as
          devastating, if not more, than auto accidents.
        </p>
        <p>
          If this is a case of negligence on your employer's behalf for not
          ensuring the safety of the workplace, we can help you take legal
          action with our serious injury lawyers.
        </p>
        <p>
          Regardless, we can help you get your workers compensation, and in some
          cases, your disability pay if you can no longer work. No matter your
          situation, the attorneys at Bisnar Chase will fight for you so you do
          not have struggle as you rebuild your life after your accident.
        </p>
        <h2>Serious Injury from Medical Negligence</h2>
        <p>
          Medical professionals go through years of education to ensure that all
          of their patients stay at optimal health and recover quickly from any
          illnesses or injuries.
        </p>
        <p>
          Although medical professionals are held to a high regard, sometimes
          medical professionals can cut corners and in turn, the patients suffer
          from serious injury. The most recent case of this type of medical
          negligence is when{" "}
          <Link to="https://www.cnn.com/2016/10/11/health/california-dental-water-bacteria/">
            bacteria in a Anaheim dentist's water sent over 30 children to the
            hospital
          </Link>
          .
        </p>
        <p>
          If you have received serious injury from the negligence of a medical
          practitioner you may be entitled to compensation. Contact a personal
          injury attorney well versed in medical malpractice cases.
        </p>
        <h2>Serious Injury Lawyers For You</h2>
        <p>
          Contact our offices today and see what we may be able to do for you.
          One of our experienced serious injury attorneys can answer all of your
          questions with a free private consultation. Call
          <strong> 800-561-4887 </strong>
          to schedule your free consultation today..
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
