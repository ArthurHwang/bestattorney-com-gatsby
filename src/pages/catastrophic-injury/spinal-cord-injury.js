// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { Pie } from "react-chartjs-2"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "catastrophic-injuries/spinal-cord-injury2.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  const chart1Data = {
    options: {
      responsive: true
    },
    labels: [
      "Vehicular Incidents",
      "Falls",
      "Violence",
      "Sports",
      "Medical/Surgical",
      "Other"
    ],
    datasets: [
      {
        data: [38.3, 31.6, 13.8, 8.2, 4.6, 3.5],
        backgroundColor: [
          "#F7464A",
          "#46BFBD",
          "#FDB45C",
          "#474687",
          "#468748",
          "#72488e"
        ]
      }
    ],

    borderColor: "#fff"
  }

  const chart2Data = {
    options: {
      responsive: true
    },
    labels: [
      "Incomplete Tetraplegia",
      "Incomplete Paraplegia",
      "Complete Paraplegia",
      "Complete Tetraplegia",
      "Complete Recovery"
    ],
    datasets: [
      {
        data: [47.2, 20.4, 20.2, 11.5, 0.8],
        backgroundColor: ["#474687", "#F7464A", "#FDB45C", "#468748", "#46BFBD"]
      }
    ],

    borderColor: "#fff"
  }

  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Spinal Cord Injury Lawyers in California - Bisnar Chase"
        pageDescription="A spinal cord injury can result in permanent paralysis and expensive care. Find out how the spinal cord injury lawyers of Bisnar Chase can help SCI victims."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Spinal Cord Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Spinal cord injury"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          A spinal cord injury is a form of{" "}
          <Link to="/catastrophic-injury" target="new">
            catastrophic injury
          </Link>{" "}
          which can have a devastating and life-changing impact on an
          individual. This form of major trauma can leave a victim with
          permanent paralysis from the neck down. In the most severe cases, it
          can even destroy that person’s ability to speak or breathe without
          help, and some victims will require full-time care for the rest of
          their lives.
        </p>

        <p>
          If you or a loved one have suffered such an injury, you may be looking
          for help, information, and the services of a top-quality California
          spinal cord injury attorney. You can turn to Bisnar Chase for help.
          Our expert lawyers make it their mission to ensure clients get the
          compensation and justice that they deserve.
        </p>

        <p>
          Read on for all the information you need about spinal injuries, or
          jump ahead to learn how to take legal action with the best spinal cord
          injury lawyer for your case.
        </p>

        <div style={{ marginBottom: "2rem" }} align="center">
          {" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is a Spinal Cord Injury?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Common are Spinal Cord Injuries?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What are the Most Common Spinal Cord Injury Causes?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Types of Spinal Cord Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Complete vs Incomplete SCIs{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Common are the Different Forms of Spinal Damage?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Costs for Spinal Cord Injury Victims{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              SCI Diagnosis and Treatments{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Happens After a Spinal Cord Injury?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              SCI Lawsuits: Who is Liable?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Spinal Cord Injury Compensation{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Trust Bisnar Chase with Your Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">What is a Spinal Cord Injury?</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/spinal-injury.jpg"
            width="45%"
            alt="spinal injury"
            className="imgleft-fluid"
          />
        </LazyLoad>

        <p>
          A spinal cord injury (SCI) refers to a specific form of back damage
          which can impact a person’s entire body.
        </p>

        <p>
          The spinal cord is one of the most important elements of the human
          body. It is essentially a long tube which contains a bundle of nerves.
          It runs from the base of the skull to a person’s waist, connecting the
          brain to all other parts of the body through ‘nerve branches’.
        </p>

        <p>
          This tube (<i>shown in yellow in the image, left</i>) is a key
          component of the central nervous system, transmitting signals from the
          brain to all other parts of the body. This is what allows you to move.
        </p>

        <p>
          The spinal cord is so important that it has its own protection system.
          It is surrounded by the spinal column – the backbone itself. This
          solid column of vertebrae bones encases the spinal cord tube,
          protecting it from harm.
        </p>

        <p>
          A spinal cord injury refers to this vital tube being damaged. As the
          cord is responsible for sending signals from the brain to the body, a
          spinal cord injury can result in temporary or permanent paralysis,
          depending on its severity.
        </p>

        <p>
          Damage to the spinal cord cannot be fixed. Contusions and tears to the
          cord will leave a permanent impact on the body.
        </p>

        <h2 id="header2">How Common are Spinal Cord Injuries?</h2>

        <p>
          The number of new SCI cases occuring each year is rising all the time.
          The most recent{" "}
          <Link
            to="https://www.nscisc.uab.edu/Public/Facts%20and%20Figures%20-%202018.pdf"
            target="new"
          >
            research by the National Spinal Cord Injury Statistical Center
          </Link>{" "}
          shows that there are about 17,700 new cases in the United States every
          year.
        </p>

        <p>
          The figures also show that there are between 247,000-353,000
          (estimated to be about 288,000) people living with an SCI in the U.S.
          already.
        </p>

        <p>
          While they are treated as fairly rare, an SCI is extremely serious and
          can have a devastating and permanent effect on a person.
        </p>

        <h2 id="header3">
          What are the Most Common Spinal Cord Injury Causes?
        </h2>

        <p>
          The five most common SCI causes account for a huge 96.5% of all spinal
          cord injuries. They are:
        </p>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/spinal-cord-injury-causes.jpg"
            width="300"
            alt="spinal cord injury causes"
            className="imgright-fluid"
          />
        </LazyLoad>

        <ol>
          <li>
            <span>
              <b>
                <u>Auto accidents </u>
              </b>
              According to the latest research,{" "}
              <Link to="/car-accidents" target="new">
                car crashes
              </Link>{" "}
              and other assorted vehicular incidents are responsible for nearly
              39% of all spinal cord injuries. This category includes everything
              from{" "}
              <Link to="/motorcycle-accidents" target="new">
                motorcycle accidents
              </Link>{" "}
              to{" "}
              <Link to="/truck-accidents" target="new">
                truck crashes
              </Link>
              , and everything in between. This is the most common cause of
              spinal injuries.
            </span>
          </li>

          <li>
            <span>
              <b>
                <u>Falls</u>{" "}
              </b>
              The second most common cause of spinal injuries is a victim
              suffering a fall. Anyone can slip or trip, and a{" "}
              <Link
                to="/premises-liability/slip-and-fall-accidents"
                target="new"
              >
                fall-related accident
              </Link>{" "}
              can have devastating consequences. This accounts for just over 31%
              of all SCIs. Falls are also the leading cause of spinal cord
              injuries for people aged over 45.
            </span>
          </li>

          <li>
            <span>
              <b>
                <u>Violence</u>{" "}
              </b>
              Instances of violence provide the third leading cause of spinal
              cord injuries. This is a wide term which can encompass all kinds
              of altercations. However, the primary source of such injuries in
              the United States is gunshot wounds. Acts of violence are
              responsible for 8.2% of all cases.
            </span>
          </li>

          <li>
            <span>
              <b>
                <u>Sports injuries</u>{" "}
              </b>
              Sports injuries make up 4.6% of all SCIs. These usually occur in
              high-impact contact sports, such as football and hockey. However,
              they can also be caused by solo sports involving great falling
              potential, such as horse riding or snowboarding.
            </span>
          </li>

          <li>
            <span>
              <b>
                <u>Medical or surgical</u>{" "}
              </b>
              Rounding out the top five is the medical and surgical incident
              category. This accounts for just 3.5% of all spinal cord injuries,
              and can include medical conditions and treatment issues, such as
              surgical complications.
            </span>
          </li>
        </ol>

        <center>
          <h3>The Most Common Causes of Spinal Cord Injuries</h3>
        </center>

        <LazyLoad>
          <Pie data={chart1Data} />
        </LazyLoad>
        <br />

        <h3>SCI Demographics</h3>

        <p>
          Anyone can suffer a spinal cord injury. Sometimes an SCI might be the
          result of a one-off accident through no fault of the victim, such as a
          high-impact car accident. The chance of a person damaging their spinal
          cord is greatly increased if they take part in potentially-dangerous
          sports or activities.
        </p>

        <p>
          While a devastating injury can hit anyone, the statistics (compiled
          from 2015-2018) show that certain demographics are at greater risk
          than others.
        </p>

        <ul>
          <li>
            About 78% of all victims in new spinal cord injury cases are men.
          </li>
          <li>
            The average age of SCI victims (at the time of the injury) is 43.
            This is a huge rise over the past few decades. In the 1970s the
            average was 29-years-old.
          </li>
        </ul>

        <h2 id="header4">Types of Spinal Cord Injuries</h2>

        <p>There are four main types of spinal cord injury. They are:</p>

        <ul>
          <li>Cervical spinal cord injury</li>
          <li>Thoracic spinal cord injury</li>
          <li>Lumbar spinal cord injury</li>
          <li>Sacral spinal cord injury</li>
        </ul>

        <p>
          Vertebrae and spinal nerves are grouped into sections. Each of these
          four injury types relate to damage to a different area of the spine.
        </p>

        <p>
          The different nerve groupings have labels which allow doctors to
          pinpoint the exact area of spinal damage. The grouping labels are:
        </p>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/types-spinal-cord-injury.jpg"
            width="305"
            className="imgleft-fixed"
            alt="types of spinal cord injury"
          />
        </LazyLoad>

        <ul>
          <li>C1-C8 (Cervical)</li>
          <li>T1-T12 (Thoracic)</li>
          <li>L1-L5 (Lumbar)</li>
          <li>S1-S5 (Sacral)</li>
        </ul>

        <p>
          When it comes to spinal injuries, location is everything. It can
          dictate the injury victim’s prognosis.
        </p>

        <p>
          In general, paralysis may occur from the point of the spinal cord
          damage downward. This means that an injury to the upper regions of the
          spinal cord have the potential to provide bigger problems.
        </p>

        <p>
          For instance, a C1 spinal cord injury is the most severe diagnosis
          possible. In comparison, an injury to the S5 nerve, while still
          serious, carries the least severe long-term effects.
        </p>

        <p>
          Read on for the spinal cord injury symptoms associated with each area
          of the back.
        </p>

        <h2>Cervical Spinal Cord Injury</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/cervical-spinal-cord-injury.jpg"
            width="72"
            className="imgright-fixed"
            alt="cervical spinal cord injury"
          />
        </LazyLoad>

        <p>
          No one wants to hear that they have a cervical SCI. A cervical spinal
          cord injury occurs at the top of the back and is the most serious type
          of damage possible. It refers to any injury in the C1-C8 section of
          spinal nerves.
        </p>

        <ul>
          <li>
            Cervical injury could result in paralysis to a person’s hands, arms,
            legs, and torso.
          </li>
          <li>May compromise ability to support their own head/neck.</li>
          <li>Victims may be unable to breathe on their own.</li>
          <li>
            A cervical injury may prevent a person from being able to talk.
          </li>
          <li>
            Likely to compromise control over bodily functions, such as bowel
            and bladder function.
          </li>
          <li>
            A person suffering C5-C8 damage might be able to talk and breathe
            unaided, but their breathing may still be weakened.
          </li>
          <li>Victims usually have to wear stabilizing devices.</li>
          <li>
            Loss of sensation and problems with body temperature regulation.
          </li>
        </ul>

        <h2>Thoracic Spinal Cord Injury</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/thoracic-spinal-cord-injury.jpg"
            width="75"
            className="imgright-fixed"
            alt="thoracic spinal cord injury"
          />
        </LazyLoad>

        <p>
          The thoracic area is the largest portion of the spinal cord, making up
          the middle section of a person’s back. Injuries to this region are
          likely to have slightly less severe effects than a cervical injury.
        </p>

        <ul>
          <li>
            Thoracic spinal cord injuries are likely to result in paraplegia.
            Arm and hand mobility are usually unaffected, but a victim will
            often suffer a loss of function in their legs.
          </li>
          <li>
            Victims could also lose control of their chest, back, and abdominal
            muscles.
          </li>
          <li>Loss of control over bodily functions.</li>
          <li>
            Most injury victims will wear a brace to support and stabilize their
            bodies.
          </li>
          <li>
            Thoracic injuries are rarer than some other areas, as the rib cage
            offers additional protection.
          </li>
        </ul>

        <h2>Lumbar Spinal Cord Injury</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/lumbar-spinal-cord-injury.jpg"
            width="75"
            alt="lumbar spinal cord injury"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          The lumbar section of the spine covers the lower back area, and any
          damage to the spinal cord in this area is likely to result in lasting
          effects on the lower body.
        </p>

        <ul>
          <li>
            Likely to result in extreme weakness or complete paralysis in a
            person’s legs. Will probably have to use a wheelchair.
          </li>
          <li>Can also affect movement and function of the hips.</li>
          <li>Loss of feeling and lack of control over bodily functions.</li>
          <li>Arms, hands, and upper torso are usually unaffected.</li>
          <li>Loss of control over muscles and organs below the waist.</li>
          <li>Usually requires surgery and supportive braces or equipment.</li>
        </ul>

        <h2>Sacral Spinal Cord Injury</h2>

        <p>
          The sacral region is the very bottom part of the back, right where the
          spinal cord ends at the waist. Injuries to this area are least likely
          to result in permanent paralysis, but can still be life-altering.
        </p>

        <ul>
          <li>
            Sacral SCIs usually cause a loss of mobility or paralysis in legs
            and hips.
          </li>
          <li>
            Victims may still be able to walk after suffering an injury,
            depending on the severity.
          </li>
          <li>
            Bodily functions such as bladder and bowel control can be impacted.
            This may be managed with specialist equipment.
          </li>
          <li>Compromises signals to lower parts of the leg.</li>
        </ul>

        <h2 id="header5">Complete vs Incomplete Spinal Cord Injuries</h2>

        <p>
          Whether a spinal cord injury is complete or incomplete refers to how
          permanent the effects are.
        </p>

        <p>
          Spinal cord damage cannot be reversed. However, in the case of an
          incomplete spinal cord injury, the damage is not total. The brain will
          still be able to send signals through the spinal nerves, though they
          may be weaker. This means that some feeling and mobility will still be
          possible below the level of an incomplete injury.
        </p>

        <p>
          In contrast, a complete injury will mean that no signals can reach the
          nerves due to the extent of the spinal cord damage. This results in
          complete paralysis and loss of sensation below the point of injury.
        </p>

        <h2 id="header6">
          How Common are the Different Types of Spinal Damage?
        </h2>

        <p>
          The most common diagnosis in new cases of spinal cord injuries is
          incomplete{" "}
          <div className="tooltip">
            tetraplegia
            <span className="tooltiptext">
              Tetraplegia: More commonly known as quadriplegia. This is a form
              of paralysis that affects a person’s arms, legs, and torso.
            </span>
          </div>
          . Nearly half of all victims experience a variable level of paralysis
          in all limbs.
        </p>

        <p>
          The next most common diagnosis is an incomplete form of{" "}
          <div className="tooltip">
            paraplegia
            <span className="tooltiptext">
              Paraplegia: A form of paralysis which affects a person’s lower
              body, usually compromising the functionality of the legs.
            </span>
          </div>{" "}
          – effecting just the legs or lower bodies of the injured parties.
        </p>

        <p>
          Less common are complete forms of tetraplegia and paraplegia, where
          the victim is left with no function or feeling at all. This is because
          the level of trauma required to completely sever the connection
          between the brain and nerves is extreme.
        </p>

        <p>
          No matter what form of damage has been suffered, the{" "}
          <b>California spinal cord injury attorneys</b> of Bisnar Chase can
          help.
        </p>
        <center>
          <h3>The Most Common Spinal Cord Injury Diagnosis</h3>
        </center>
        <LazyLoad>
          <Pie style={{ marginBottom: "2rem" }} data={chart2Data} />
        </LazyLoad>
        <h2 style={{ marginTop: "2rem" }} id="header7">
          Costs for Spinal Cord Injury Victims
        </h2>

        <p>
          Everyone knows how expensive medical treatment and rehabilitation
          costs are. These costs mount even higher for spinal cord injuries,
          which result in major trauma that requires specialist care.
        </p>

        <p>
          The average length of hospital stay after a <b>spinal cord injury</b>{" "}
          is currently 11 days. This number was close to 30 days in the 1970s,
          but is dropping thanks to medical advances.
        </p>

        <p>
          In addition, about 30% of all SCI victims have to be admitted to
          hospital again in the year following their injury. The average length
          of this second hospital stay is 22 days.
        </p>

        <p>
          Aside from medical care, the costs of living with a spinal cord injury
          are astronomical. From specialist equipment to surgery and
          rehabilitation costs, the expenses are huge.
        </p>

        <h3>Average Annual Costs by Type of Spinal Cord Inury</h3>

        <table>
          <tr>
            <th>Injury Type</th>
            <th>Year 1</th>
            <th>Each Following Year</th>
          </tr>
          <tr>
            <td>Tetraplegia (C1-C4 damage)</td>
            <td>$1,102,403</td>
            <td>$191,436</td>
          </tr>
          <tr>
            <td>Tetraplegia (C5-C8 damage)</td>
            <td>$796,583</td>
            <td>$117,437</td>
          </tr>
          <tr>
            <td>Paraplegia</td>
            <td>$537,271</td>
            <td>$71,172</td>
          </tr>
        </table>

        <h3>
          Lifetime Costs Based on the Age at which a Spinal Cord Injury is
          Sustained
        </h3>

        <table>
          <tr>
            <th>Injury Type</th>
            <th>25-years-old</th>
            <th>50-years-old</th>
          </tr>
          <tr>
            <td>Tetraplegia (C1-C4 damage)</td>
            <td>$4,891,398</td>
            <td>$2,688,229</td>
          </tr>
          <tr>
            <td>Tetraplegia (C5-C8 damage)</td>
            <td>$3,573,960</td>
            <td>$2,198,305</td>
          </tr>
          <tr>
            <td>Paraplegia</td>
            <td>$2,391,872</td>
            <td>$1,569,714</td>
          </tr>
        </table>

        <p>
          These costs look solely at the dollar amount associated with spinal
          cord injuries. They do not take into account the impact on a person’s
          quality of life, and how they are affected on a day to day basis.
          These factors are just as important as money.
        </p>

        <h2 id="header8">Spinal Cord Injury Diagnosis and Treatments</h2>

        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/spinal-cord-injury-treatment.jpg"
            width="350"
            alt="spinal cord injury treatment"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          For someone who has suffered a spinal cord injury, the first step will
          be diagnosing the problem. Tests that could be carried out to identify
          the source and severity of the spinal damage include:
        </p>

        <ul>
          <li>X-rays</li>
          <li>CT scan</li>
          <li>MRI</li>
        </ul>

        <p>
          Most people with an SCI will be checked into intensive care at a
          hospital. The type of treatment suggested for an injury victim will
          depend on the kind of damage they have suffered. Unfortunately for
          anyone sustaining a spinal cord injury, there is no treatment which
          can completely cure this kind of damage.
        </p>

        <p>
          There are some spinal cord injury treatment options which can help
          though. These are:
        </p>

        <ul>
          <li>
            <b>
              <u>Stabilization</u> –
            </b>{" "}
            Doctors may use equipment such as neck collars and body braces to
            immobilize the body and prevent further damage.
          </li>
          <li>
            <b>
              <u>Surgery</u> –
            </b>{" "}
            While surgery cannot fix a damaged spinal cord, it can be used to
            help with other signs of the traumatic injury, such as fractured
            vertebrae or herniated discs. This can reduce pain and prevent more
            problems developing.
          </li>
          <li>
            <b>
              <u>Experimental treatments</u> –
            </b>{" "}
            Researchers are constantly working to develop new treatment methods.
            One new method being tested includes inducing hypothermia for a
            short period to prevent inflammation causing more damage to the
            body.
          </li>
        </ul>

        <h2 id="header9">What Happens After a Spinal Cord Injury?</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/spinal-cord-injury-diagnosis.jpg"
            width="250"
            alt="spinal cord injury diagnosis"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          After an injured person is stabilized and has undergone any initial
          treatments available to them, the next step is rehabilitation.
        </p>

        <p>
          The type of rehabilitation a person undergoes will depend on the type
          and severity of their injury and prognosis.
        </p>

        <p>
          Sometimes it might involve specific exercises to work on strengthening
          muscles, helping to compensate for any lost function elsewhere. In
          other cases, a patient might work on redeveloping their motor skills,
          such as hand and finger movements.
        </p>

        <p>
          Rehabilitation may also include learning how to use technological
          aids. New technology is constantly in development for people who have
          suffered major spinal injuries.
        </p>

        <p>Some of the devices available include:</p>

        <ul>
          <li>
            <b>
              <u>Hi-tech wheelchairs</u> –
            </b>{" "}
            The latest electronic wheelchairs can feature stair-climbing
            capabilities, along with other hi-tech advancements.
          </li>
          <li>
            <b>
              <u>Computer software</u> –
            </b>{" "}
            Computers can be fitted with an extraordinary range of software,
            include voice-activated controls for those without the use of their
            hands, and even software controlled by eye movements for those with
            extreme levels of paralysis.
          </li>
          <li>
            <b>
              <u>Electronic aids</u> –
            </b>{" "}
            Functional electrical stimulation systems are in development which
            allow people with spinal cord injuries to use their weakened muscles
            more effectively.
          </li>
        </ul>

        <h2 id="header10">Spinal Cord Injury Lawsuits: Who is Liable?</h2>
        <LazyLoad>
          <img
            src="/images/catastrophic-injuries/spinal-cord-injury-liability.jpg"
            width="300"
            className="imgleft-fixed"
            alt="spinal cord injury liability"
          />
        </LazyLoad>
        <p>
          Most spinal cord injuries are catastrophic. They can leave lives in
          ruins, with victims facing huge costs, potential paralysis and
          long-term care. These are tough circumstances to face for anyone.
        </p>

        <p>
          While a lawsuit cannot mend a damaged spinal cord, it can ease the
          financial burden of such an injury, as well as holding those
          responsible accountable.
        </p>

        <p>
          Liability in spinal cord cases will depend on the circumstances of a
          specific injury. If you have suffered a personal injury to your spinal
          cord as a result of the actions of another person, you may be able to
          take legal action.
        </p>

        <p>
          If the actions of the other party were negligent, careless, reckless,
          or malicious, they can be sued for compensation.
        </p>

        <p>
          For instance, if a person suffers a spinal cord injury in a{" "}
          <Link to="/car-accidents" target="new">
            car crash
          </Link>
          , another driver could be liable if they are found to be at fault.
        </p>

        <p>
          If someone sustains a spinal injury as a result of a fall, it would
          depend on the cause and conditions. If the fall is a result of unsafe
          conditions, there may be a{" "}
          <Link to="/premises-liability" target="new">
            premises liability case
          </Link>
          , with the owners or operators of the location at fault.
        </p>

        <p>
          In some cases, an injury may occur due to a faulty product, such as a
          failed seatbelt or airbag. This may result in{" "}
          <Link to="/defective-products" target="new">
            product liability
          </Link>
          , with a manufacturer being found at fault.
        </p>

        <p>
          It is always worth getting legal advice from the experienced attorneys
          of Bisnar Chase to find out if you have a case. Our California spinal
          cord injury lawyers are specialists, and can help take your case
          forward.
        </p>

        <div
          className="text-header content-well"
          title="Gavel with money"
          style={{
            backgroundImage:
              "url('/images/catastrophic-injuries/spinal-cord-compensation2.jpg')"
          }}
        >
          <h2 id="header11">Spinal Cord Injury Compensation</h2>
        </div>

        <p>
          If you pursue an SCI lawsuit and someone is found to be liable for
          your injury, what kind of compensation can you expect? As always in
          personal injury cases, there is no set answer to this question.
        </p>

        <p>
          Spinal cord injury compensation will depend on a range of factors.
          These include:
        </p>

        <ul>
          <li>Medical expenses for immediate care and additional surgeries</li>
          <li>Costs related to ongoing specialist treatments</li>
          <li>
            Potential costs for full-time care, depending on the severity of the
            injury
          </li>
          <li>Lost earning potential</li>
          <li>Pain and suffering</li>
          <li>Emotional cost</li>
          <li>The cost of equipment and devices, such as wheelchairs</li>
          <li>
            Home expenses, such as making it wheelchair accessible or adding
            voice-activated technology
          </li>
        </ul>

        <p>
          The serious nature and consequences of a spinal cord injury are
          far-reaching. They impact every aspect of everyday life, from simple
          tasks that most people take for granted, to jeopardizing relationships
          and career aspirations.
        </p>

        <p>
          Due to the colossal costs involved with SCI treatment and care, there
          is a huge scope in the compensation that can be claimed.
        </p>

        <p>
          However, it is also important to remember that a{" "}
          <b>spinal cord injury lawsuit</b> is not just about the money. It is
          also a chance to hold the culprit accountable for the actions that led
          to the injury, and to see justice done.
        </p>

        <div
          className="text-header content-well"
          title="Bisnar Chase Attorneys"
          style={{
            backgroundImage:
              "url('/images/catastrophic-injuries/spinal-cord-injury-attorneys.jpg')"
          }}
        >
          <h2 id="header12">Trust Bisnar Chase with Your Case</h2>
        </div>

        <p>
          Spinal injuries can change everything. The{" "}
          <b>spinal cord injury lawyers of Bisnar Chase</b> are dedicated to
          helping victims who have suffered catastrophic injuries. We know how
          big an impact this may have had on your day-to-day life, and our law
          firm wants to help.
        </p>

        <p>
          Bisnar Chase has established an outstanding <b>96% success rate</b>{" "}
          over 40 years in business, with more than <b>$500 million</b> won. Our
          team is made up of fearless trial lawyers who have a great track
          record of securing the best possible outcome for their clients. We
          value results, but we also believe in providing a personal touch
          through a superior attorney-client relationship.
        </p>

        <p>
          With Bisnar Chase, if we don’t win, you don’t pay. Take advantage of
          our ‘No Win, No Fee’ guarantee, and make sure you get the compensation
          and justice that you deserve for a spinal cord injury. Call{" "}
          <Link to="tel:+1-800-561-4887">(800) 561-4887</Link> for a free
          consultation if you or a loved one have suffered a{" "}
          <b>spinal cord injury in California</b>.
        </p>

        <h2>Spinal Cord Injury Resources</h2>

        <ul>
          <li>
            {" "}
            <Link to="https://www.amtrauma.org/default.aspx" target="new">
              American Trauma Society
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://unitedspinal.org/" target="new">
              United Spinal Association
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.uab.edu/medicine/sci/" target="new">
              UAB Spinal Cord Injury Information Network
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://helphopelive.org/" target="new">
              Help Hope Live
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.dol.gov/odep/" target="new">
              Office of Disability Employment Policy - United States Department
              of Labor
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  #protection {
    float: left;
    padding-right: 15px;
    padding-top: 10px;
  }

  #paramedics {
    float: right;
    padding-left: 15px;
    padding-top: 5px;
  }

  #types {
    float: left;
    padding-right: 15px;
  }

  #xray {
    float: left;
    padding-right: 30px;
  }

  #chair {
    float: right;
    padding-left: 15px;
  }

  #liability {
    float: left;
    padding-right: 15px;
    padding-top: 10px;
  }

  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px;
    border-radius: 6px;

    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -140px;

    opacity: 0;
    transition: opacity 0.3s;
  }

  .tooltip .tooltiptext::before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #dddddd;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
