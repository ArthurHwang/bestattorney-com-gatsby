// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Amputations - Losing Limbs to Serious Injuries"
        pageDescription="Serious and catastrophic injuries can result in amputations, or the loss of a limb. Learn recovery, costs, and get the help you need, now!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Amputations</h1>
        <BreadCrumbs location={location} />
        <p>
          Losing one of your body parts can be an extremely traumatic event both
          physically and mentally.
        </p>
        <p>
          Amputation injuries may be sustained as the result of traumatic
          incidents such as an explosion, a construction accident, a car crash
          or an act of violence.
        </p>
        <p>
          An amputation is a{" "}
          <Link to="/catastrophic-injury">catastrophic injury</Link> because
          once you lost a body part, you will likely lose its use and
          functionality as well as reorient your life around your injury. Even
          if you may be able to obtain prosthetics, your body will never be the
          same again. You may lose your job, career and earning potential.
        </p>
        <p>
          In addition, you will have to spend a significant amount of money for
          medical expenses, surgeries and prosthetics and your mental health may
          suffer in the process.
        </p>
        <p>
          If your injuries were caused by someone else's negligence or
          wrongdoing, it is important that you obtain more information about
          your rights from the amputation attorneys at Bisnar Chase.
        </p>
        <p>
          If your need immediate legal help, please call our amputation injury
          attorneys at <strong>800-561-4887</strong> now.
        </p>

        <h2>Causes of Amputation</h2>
        <p>
          There are a number of reasons why some injured victims may need to
          have an arm, hand, finger, leg, foot and/or toe removed.
        </p>
        <p>
          In many cases, injured victims require amputation surgery because
          there is not enough circulation in a specific part of their body.
          Without proper blood flow, the body's cells could die or become
          infected.
        </p>
        <p>
          There are many types of serious accidents that can result in an
          amputation including car accidents, heavy machinery accidents,
          construction accidents, health complications and house fires. Other
          causes include cancerous tumors, serious infections, frostbite and a
          thickening of nerve{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/mortons-neuroma/symptoms-causes/syc-20351935"
            target="_blank"
          >
            tissue called neuroma
          </Link>
          .
        </p>

        <h2>Limb Removal</h2>
        <p>Not all amputations are treated in the same manner.</p>
        <p>
          Your doctor will have to carefully check for your pulse near where the
          surgeon will cut. It will be necessary to compare skin temperature and
          nerve sensitivity. The dying parts of your limb will have limited
          sensation, less heat and a faint pulse.
        </p>
        <p>
          The goal is typically to leave as much healthy tissue as possible.
        </p>
        <h2>What Happens to Amputated Body Parts?</h2>
        <p>
          The surgeon will have to remove the dead tissue and crushed bones.
          Your surgeon will also smooth uneven bones, seal off blood vessels and
          shape the muscles around the stump so that you can have an artificial
          limb attached in the future. Depending on your injuries, the surgeon
          may close the wound right away or leave the site open in case you need
          additional surgery.
        </p>

        <h2>Recovering from an Amputation</h2>
        <p>
          Patients who require an amputation are typically in the hospital for
          an extended period of time.
        </p>
        <p>
          Their wound will need dressing and careful monitoring. For example, it
          is common for patients to suffer from phantom pain where they sense
          pain in the amputated limb. Others experience grief over their lost
          limb. In these types of cases, psychological counseling is often
          needed in addition to medication.
        </p>
        <p>
          Physical therapy should begin as soon as possible. Stretching
          exercises should begin shortly after the surgery. Practice with an
          artificial limb may begin within a couple of weeks. However, the road
          to recovery is often long and difficult. Many patients require:
        </p>
        <ul>
          <li>
            Exercises to improve their body control, strength and flexibility.
          </li>
          <li>
            Occupational therapy to help learn how to carry out daily activities
            and become independent.
          </li>
          <li>
            Therapy sessions that teach them how to use their artificial limbs.
          </li>
          <li>
            Emotional support to help them deal with the grief of losing a limb.
          </li>
        </ul>

        <h2>The Cost of Losing a Limb</h2>
        <p>The expenses for losing a limb can add up very quickly.</p>
        <p>
          A 2007 study at the{" "}
          <Link
            to="https://www.jhsph.edu/research/centers-and-institutes/johns-hopkins-center-for-injury-research-and-policy/"
            target="_blank"
          >
            Center for Injury Research and Policy at Johns Hopkins Bloomberg
            School of Public Health
          </Link>{" "}
          examined the health care costs for 545 patients over a two-year
          period.
        </p>
        <p>
          The cost for patients who received reconstruction of a body part was
          $81,316. The cost of amputation was $91,106. When calculating their
          long-term losses, the results were considerably different. Lifetime
          health care costs for reconstruction patients was $163,282 compared to
          the $509,275 for amputation patients
        </p>
        <p>
          The costs reported in that 2007 study only reflect health care costs.
          There are many other costs and expenses that result from an
          amputation, including:
        </p>
        <ul>
          <li>Lost wages</li>
          <li>Loss of earning potential</li>
          <li>The cost of rehabilitation services</li>
          <li>Prosthetics</li>
          <li>Counseling</li>
          <li>Prescription drugs</li>
        </ul>

        <h2>Get the Legal Help You Need</h2>
        <p>
          If you or a loved one has required an amputation, it is important to
          understand your legal options.
        </p>
        <p>
          You may not only be faced with significant expenses, but also the
          inability to make a living. You may never be able to work or support
          your family.
        </p>
        <p>
          The emotional toll in such situations can be significant, not only for
          the victim, but also his or her family. It is important during these
          challenging times to obtain the resources, support and counsel you
          need to get on that road to recovery.
        </p>
        <p>
          Call us, the amputation injury attorneys, now at{" "}
          <strong>800-561-4887</strong> for free legal advice and a case review.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
