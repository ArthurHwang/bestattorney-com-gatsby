// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Torn Meniscus Injury and Care - Post-injury treatment"
        pageDescription="A torn meniscus injury is not only painful, but also expensive to treat and may put you out of work. Call 800-5612-4887 now for a free case review and to lean more about your legal rights."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Torn Meniscus Injury and Care</h1>
        <BreadCrumbs location={location} />

        <p>
          Torn meniscus injuries are commonly derived from car accidents and
          work related injuries.
        </p>
        <p>
          The knee is the largest joint in the human body and sustaining a knee
          injury can greatly affect mobility. One of the most common knee
          injuries you can suffer is a torn meniscus.
        </p>
        <p>
          There are two c-shaped pieces of cartilage in each knee that act as
          cushions between the thighbone and the shinbone. When one of these
          four cushions gets torn, the knee may feel unstable, stiff and
          painful. Rest and ice can help in some torn meniscus cases, but many
          instances require extensive surgery.
        </p>
        <p>
          If you have sustained a torn meniscus injury, you are going to need a
          seasoned{" "}
          <Link to="/catastrophic-injury">catastrophic injury lawyer</Link> that
          will fight for you. To learn more about your legal rights after a torn
          meniscus injury and for a free case review, call us now toll free at{" "}
          <strong>800-561-4887 </strong>to leanr more.
        </p>
        <h2>Meniscus Tear Symptoms</h2>
        <p>
          If you have suffered a meniscal injury, you may hear a "popping" sound
          and experience swelling and stiffness. If you twisted or rotated your
          knee in an awkward manner, you may feel immediate pain as well.
        </p>

        <p>
          Soon after your injury, you most likely will experience difficulty
          straightening your knee and other mobile restrictions.
        </p>
        <p>
          If you are experiencing any of these symptoms, you should seek medical
          help right away. You can cause more damage to your meniscus by
          continuing to move and put weight on it. The longer you wait, the more
          serous your injury may become so it is imperative you get the medical
          attention you need as soon as humanly possible.
        </p>
        <h2>What Causes Meniscal Injuries</h2>
        <p>
          Any activity involving a forceful rotation of your knee has the
          potential to result in a torn meniscus injury.
        </p>
        <p>
          Turning, pivoting, squatting, kneeling or lifting can all result in
          knee injuries. You can even tear your meniscus by improperly lifting
          something that is too heavy.
        </p>
        <p>
          These types of injuries often occur during rigorous exercise or during
          work-related manual labor activities. Athletes, particularly those
          playing basketball and football, are at risk of suffering knee
          injuries as well and older individuals are more likely to suffer torn
          meniscus injuries because of years of wear and tear.
        </p>
        <h2>Diagnosing Torn Meniscus</h2>
        <p>
          In many cases, the immediate consequences of suffering a torn meniscus
          are clear during a routine physical exam.
        </p>
        <p>
          If it appears that the meniscus was damaged, your doctor will likely
          order additional tests. An x-ray will not reveal the damaged
          cartilage, but it could reveal additional problems with the knee. An
          ultrasound will allow the doctor to see inside the knee and look for
          loose cartilage. An MRI (magnetic resonance imaging) can provide
          detailed images of the hard and soft tissues of the knee.
        </p>
        <h2>How to Treat a Torn Meniscus Injury</h2>
        <p>
          Depending on the severity of the tear, your doctor may only recommend
          rest, ice and over-the-counter pain relief.
        </p>
        <p>
          You most definitely will have to avoid any activities that aggravates
          your injury and you may be required to use crutches to take pressure
          off your knee. Unfortunately, this type of conservative treatment is
          often not enough.
        </p>
        <p>
          If your knee is painful even after a prolonged period of rest and
          therapy, your doctor may recommend surgery. In some cases, a torn
          meniscus can actually be repaired. In other circumstances, a surgeon
          will trim the meniscus or perform{" "}
          <Link
            to="https://orthoinfo.aaos.org/topic.cfm?topic=a00109"
            target="_blank"
          >
            arthroscopic surgery
          </Link>
          .
        </p>
        <p>
          Arthroscopic surgery is when an instrument called an arthroscope is
          inserted into the knee through a small incision. The arthroscope
          contains a camera and a light and allows doctors to see inside the
          knee and what repairs need to be made. All surgeries are serious, but
          arthroscopic surgery has a much faster recovery time than open knee
          procedures since this procedure is minimally invasive.
        </p>
        <p>
          Recovery still typically takes a number of weeks or even months after
          which the patient will need physical therapy to regain strength and
          mobility.
        </p>
        <h2>How to Protect Your Legal Rights</h2>
        <p>
          The expenses related to knee injuries can add up quickly and surgeries
          are extremely expensive, even for those with adequate health insurance
          coverage.
        </p>
        <p>
          Recovery often lasts months at a time and victims typically have to
          spend long periods of time away from work as they heal. Time away from
          work means a loss of wages and some victims are not able to return to
          work because of their knee injuries and the physical demands of their
          occupation. If you have suffered a meniscal tear in an accident that
          was caused by someone else's negligence, you may be able to receive
          monetary support and compensation.
        </p>
        <p>
          A catastrophic injury claim is a civil lawsuit that you can file
          against the party responsible for your suffering. A successful claim
          can result in financial compensation for all your past, current and
          future medical bills related to the injury in addition to any pain and
          suffering you may have experienced during your healing process and
          beyond.
        </p>
        <p>
          Compensation may also be available for lost wages, loss of earning
          potential and lost future wages. In serious injury accidents, support
          may even be available for non-economic losses such as physical pain
          and mental anguish. Those who have suffered a debilitating knee injury
          sould call the expert lawyers at Bisnar Chase to get the legal help,
          guidance and resources they need in order to protect their rights and
          best interests.
        </p>
        <p>
          Call Bisnar Chase now at <strong>800-561-4887 </strong>to set up your
          free consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
