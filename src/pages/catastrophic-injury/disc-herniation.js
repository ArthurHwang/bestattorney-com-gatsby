// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Disc Herniation - Causes, Symptoms and Care - Bisnar Chase"
        pageDescription="Disk herniation injuries can be very serious and painful. Here are some facts from our disc herniation lawyers about the injury and what you can do if you have it."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Disc Herniation Treatment, Symptoms and Care</h1>
        <BreadCrumbs location={location} />

        <p>
          Severe trauma to the back can cause a number of different injuries
          that are painful and debilitating. A disc herniation injury can result
          from lifting something heavy or just putting to much physical stress
          on your back as possible.
        </p>
        <p>
          A herniated disk is when any of the rubbery cushions (known as discs
          or disks) slips, ruptures or is pushed out. Anyone experiencing severe
          back pain should immediately see a doctor, because this type of injury
          could require medical treatment and/or rehabilitative care.
        </p>
        <p>
          Continue reading to learn more about disc herniation and if your
          injury was cause by the negligence of another party, you may be
          entitled to compensation and the{" "}
          <Link to="/catastrophic-injury" target="_blank">
            <strong>catastrophic injury lawyers</strong>
          </Link>{" "}
          at Bisnar Chase can help.
        </p>
        <p>
          <strong>
            Call us toll free now at 800-561-4887 to set up your free
            consultation{" "}
          </strong>
        </p>
        <h2>Disk Herniation Symptoms</h2>
        <p>
          Some victims of a slipped, ruptured or herniated disc experience very
          few symptoms. Others, however, require surgery because of the sheer
          amount of pain they are experiencing from their disk herniation
          injury.
        </p>
        <p>
          A herniated disc can damage nearby nerves resulting in catastrophic
          pain, numbness, and weakness in an arm or leg.
        </p>
        <p>
          Our well educated disk herniation lawyers advise anyone who is injured
          and reading this to keep a physical record of all your medical
          expenses as well as keep a journal that details the pain and suffering
          you are going through as these will both help you receive maximum
          compensation for your injuries.
        </p>

        <h2>Difference Between Herniated Disc and Bulging Disk</h2>
        <p>
          The disks in your back act as cushions between the vertebrae in your
          spine. These discs have a tough cartilage outer layer that surrounds a
          softer cartilage center.
        </p>
        <p>
          A{" "}
          <Link
            to="https://spinecare.luminhealth.com/conditions/bulging-disc/"
            target="_blank"
          >
            bulging disk
          </Link>{" "}
          is one that extends outside the space it normally occupies. The part
          of the disk that bulges in a bulging disk injury is typically the
          outer layer. This is part of the aging process, but it can occur at
          any age.
        </p>
        <p>
          A{" "}
          <Link
            to="https://www.webmd.com/back-pain/tc/herniated-disc-topic-overview"
            target="_blank"
          >
            herniated disk
          </Link>{" "}
          is when a crack in the tough outer layer of cartilage allows some of
          the inner cartilage to escape.
        </p>
        <p>
          A protrusion of the softer inner cartilage often affects one distinct
          area of the disk. This type of injury is less common than a bulging
          disk, but when it does occur, it can be very painful.
        </p>
        <h2>What Causes a Disc Herniation Injury?</h2>
        <p>
          The development of most disk herniation injuries is gradual and rarely
          happens in the moment.
        </p>
        <p>
          The wear and tear on our backs as we age coupled with disc
          degeneration can result in disc ruptures and slips.
        </p>
        <p>
          Our spinal disks even become less flexible as we age as they lose some
          of their water content. Herniated disc injuries, however, are not
          always just a part of aging.
        </p>
        <p>
          For some, a disc herniation injury can be sudden and painful. Workers
          who are required to lift heavy objects, for example, may suffer disk
          herniation by using their back muscles instead of their leg and thigh
          muscles.
        </p>
        <p>
          Twisting and lifting is particularly hazardous. In other cases,
          victims of slip-and-fall accidents or violent car accidents can suffer
          a herniated disc injury as well.
        </p>
        <p>
          Those particularly at risk of suffering from disk herniation include
          middle-age adults who are beginning to experienced age-related
          degeneration, individuals with excessive body fat and workers with
          physically demanding jobs.
        </p>
        <p>
          Repetitive lifting, pushing, bending, pulling and twisting can result
          in a herniated disc. If your disc herniation injury is a direct result
          from your employer consistently asking you to lift 50 lbs. or more and
          not enforce safe lifting techniques, you may be entitled to
          compensation for your injuries.
        </p>

        <h2>When to Seek Medical Attention for Your Disk Herniation</h2>
        <p>
          In severe cases, a herniated disc can compress a part of you spinal
          cord, resulting in permanent weakness or paralysis.
        </p>
        <p>
          In such cases of herniation, emergency surgery will be needed. Anyone
          experiencing back pain should look into their medical options, but
          those who are suffering from the following symptoms should see a
          doctor
          <em>
            <strong> immediately</strong>
          </em>
          :
        </p>
        <ul>
          <li>
            <b>Bladder issues:</b> Individuals who have a bulging disc pressing
            against the group of long nerve roots at the bottom of their spinal
            cord may become incontinent or have difficulties urinating.
          </li>
          <li>
            <b>Numbness:</b> The progressive loss of feeling in the inner
            thighs, back of legs and rectum could be a sign of a serious injury.
          </li>
          <li>
            <b>Worsening physical issues:</b> When weakness, numbness and pain
            continue to increase, medical attention will be required.
          </li>
        </ul>
        <p>
          A brief review of your medical history and a physical exam may be all
          that is needed to determine if you are suffering from a herniated
          disk.
        </p>
        <p>
          If, however, your doctor wants to make sure that you are not suffering
          from additional conditions, your doctor may order an X-ray, a CT-scan,
          MRI or a Myelogram.
        </p>
        <p>
          These are all different imaging tests that will help your doctor
          determine the extent of your injuries and what treatments will be
          needed.
        </p>

        <h2>Getting the Legal Support You Need</h2>
        <p>
          If you believe that you suffered a serious back injury, regardless of
          the circumstances, it is crucial that you get medical and legal help
          right away.
        </p>
        <p>
          It may take weeks or even several months for individuals to completely
          recover from disc herniation injuries. During this time, you most
          likely will not be able to work or even perform normal household tasks
          such as mopping, cooking or picking up a child.
        </p>
        <p>
          Injured victims suffer not only emotionally and physically, but also
          financially.
        </p>
        <p>
          If you have suffered from a disk herniation injury and would like to
          know more about your legal rights, call us toll free now at{" "}
          <strong>800-561-4887</strong>. We will sit down with you, regardless
          of location, one-on-one to discuss your legal rights and help you take
          legal action against the at-fault party if deemed necessary.
        </p>
        <p>
          <strong>
            Call us today to set up your free, no-obligation consultation at
            1-800-561-4887.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
