// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Catastrophic Product Liability Attorneys - Bisnar Chase"
        pageDescription="If you have been injured due to the neglience of a manufacturer of a defective product, you may be entitled to compensation. Call us at 800-561-4887 to learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Catastrophic Product Liability Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          {" "}
          <Link to="/catastrophic-injury">
            Bisnar Chase catastrophic injury attorneys
          </Link>{" "}
          specialize in representing people who have been seriously injured or
          killed as the result of defective or incorrectly designed products.
        </p>
        <p>
          There is a lot of mental and emotional anguish that comes from
          experiencing a defective product. This is due in part to the trust we
          have for the product manufacturers and the companies that sell or
          distribute these products.
        </p>
        <p>
          From child toys to pet food to auto parts and other miscellaneous
          products that we purchase, we just expect these products to be safe as
          they should be.
        </p>
        <p>
          If you have been injured as a result of big business' disregard for
          consumer safety via a defective product, we urge you to call us now at
          <strong> 800-561-4887</strong> to set up your free consultation.
        </p>
        <p>
          You, as a consumer and as a human being, have rights that shall not be
          infringed upon. Continue reading to learn more about product defects
          and what you need to do to both protect yourself from them as well as
          what to do if you become a victim.
        </p>
        <h2>What is Product Liability?</h2>
        <p>
          Unfortunately, not all designers, manufacturers and sellers of
          products live up to the safety expectations and the requirements of
          our laws with regard to their products.
        </p>
        <p>
          {" "}
          <Link
            to="https://en.wikipedia.org/wiki/Product_liability"
            target="_blank"
          >
            Product liability
          </Link>{" "}
          refers to a manufacturer or seller being held liable for placing a
          defective product into the hands of a consumer. Responsibility for
          a product defect that causes injury lies with all sellers of
          the product who are in the distribution chain according to FindLaw.
        </p>
        <p>
          With this in mind, it would be in a company's best interest to only
          sell products after rigorous safety test. Despite this being common
          sense, thousands of recalls are issued out each year on account of
          safety issues.
        </p>
        <p>
          It's also important to note that not all companies that have product
          recalls had the malicious intent to hurt their customers. Regardless,
          it is the responsibility of the company and the manufacturer to ensure
          the quality and safety of their good before they reach the public.
        </p>
        <p>
          If you have sustained injury from a defect product, this means you are
          most likely entitled to compensation and an experienced lawyer can
          help you take legal action.
        </p>
        <h2>How to Identify a Defective Product</h2>
        <p>
          In most cases, it is hard to detect when a product doesn't live up to
          safety expectations, however, there are some warning signs that can
          help you be proactive and vigilant to product deficiency:
        </p>
        <ol>
          <li>
            The product itself was made with a fundamental flaw that makes the
            whole line obsolete. If this is the case, store the defective
            product away from the reach of children or any other person(s). Look
            into the product and find out if there has been a government issued
            recall. If there has, return your defective product to the point of
            sale and obtain a refund. If you have sustained injury from the same
            product, it would be wise to consult with a catastrophic product
            liability lawyer before returning the item.
          </li>
          <li>
            In some cases, the product could have been safely designed by the
            company, however, the manufacturer could have strayed from the
            initial design and thus inadvertently created a defect. The same
            guidelines apply in this case, however, you may be qualified for a
            new product depending on the company and the recall. Again, if you
            have been injured, keep detailed records of our injury and save and
            invoices or receipts pertaining to medical expenses and recovery.
            This will help your attorney fight for your compensation in court.
          </li>
          <li>
            Companies and manufacturers alike are also responsible for informing
            their consumers on how to safely utilize their products. If
            confusion arises on how to use a product and the given written
            instructions do not cover your questions, we advise you to contact
            the given company immediately. If a company is not detailed enough
            to give consumers adequate instructions to avoid confusion and harm,
            it is best to return the product before any harm is sustained.
          </li>
        </ol>
        <p>
          In some case, you may not necessarily have to prove that the
          manufacturer was "negligent".
        </p>
        <h2>How to Prove Product Liability Negligence</h2>
        <p>
          Product liability claims, often called "strict product liability" can
          be established if you are able to prove the following factors:
        </p>
        <ul>
          <li>The product was defective.</li>
          <li>
            The defect existed prior to the manufacturer releasing the product.
          </li>
          <li>The defect caused your damages.</li>
        </ul>
        <p>
          If you and your catastrophic injury lawyer can prove these things in a
          court of law, the odds are in your party's favor of receiving the
          compensation you deserve. This is why it is absolutely essential to
          have an experienced attorney representing you who knows product
          liability law from from to back and can also read between the lines.
        </p>
        <p>
          When you choose Bisnar Chase, we assure you that everyone on our legal
          team is proficient in law interpretation.
        </p>
        <h2>How Can I Take Legal Action Against the Manufacturer?</h2>
        <p>
          Bisnar Chase has reputable experience in representing individuals who
          have been seriously injured or killed as the result of defective or
          incorrectly designed products.
        </p>
        <p>
          If you have suffered serious injuries due to a defective product you
          may be able to file a claim against the manufacturer to recover
          damages. <strong>Call 949-203-3814</strong>.
        </p>
        <p>
          Contact us today to learn more about your rights as an injured victim.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
