// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Workplace Accident Injury Lawyers - Bisnar Chase"
        pageDescription="If you or a love one has been injured on the job, you may qualify for compensation. There's hope. Call our workplace accident injury lawers now at 800-561-4887 for a free case review."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Workplace Accident Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Bisnar Chase{" "}
          <Link to="/catastrophic-injury" target="_blank">
            catastrophic injury
          </Link>{" "}
          law firm specializes in serious and personal injury cases where people
          have been hurt "on the clock" in the workplace.
        </p>
        <p>
          The workplace should be a safe working environment for both employers
          and employees. Not only is it a case of ethics and morals, but it is
          also the law above all else.
        </p>
        <p>
          If you have sustain injury on the job and have reason for believing it
          was due to the negligence of your employer, you may be entitled to
          compensation. Call us toll free now at
          <strong>800-561-4887</strong> for immediate legal help and to set up
          your free, no-obligation consultation.
        </p>
        <h2>Workplace Accidents Statistics</h2>
        <p>
          There is not a specific list of workplace accidents as any injury
          sustained at work, on-the-clock can be considered a workplace injury.
        </p>
        <p>
          For example, construction is one of the largest industries in
          California and, as a high risk industry, it ranks third in the nation
          for death-related injuries. For every
          <strong> 100,000</strong> workers in the construction industry,{" "}
          <strong>15</strong> die from on-the-job injuries.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.hse.gov.uk/statistics/causinj/" target="_blank">
            Health and Safety Executive (HSE)
          </Link>{" "}
          in the calendar year of 2011/2012, <strong> 591,000 </strong>
          employees were estimated to have been injured at work. Out of
          <strong> 114,000 </strong>
          injuries reported to employers by employees, <strong> 24,000 </strong>
          were serious or major injuries and
          <strong> 173 </strong>
          employees were fatally injured.
        </p>
        <p>
          There has been a decline in the number of injuries reported by
          employers over the past decade. This does not necessarily mean a
          decline in injuries, but the percentage of job-related injuries
          reported by workers has decreased.
        </p>
        <h2>What Makes a Work Environment Dangerous</h2>
        <p>
          There are many work environment factors that can make any place of
          employment a "death trap." Some of these factors include
        </p>
        <ul>
          <li>Explosions and fire</li>
          <li>Gas leak or exposure to toxic chemicals</li>
          <li>Harsh cleaning agents/chemicals</li>
          <li>Wet, slippery floors and pavement</li>
          <li>Rusty metal structures</li>
          <li>Old, splintered furniture</li>
          <li>Walls with lead paint</li>
          <li>Obstructed emergency exits</li>
          <li>Frayed cables/live wire</li>
          <li>Dysfunctional equipment/Improperly maintained machines</li>
          <li>Poorly prepared food</li>
          <li>Lack of supervision and/or staff</li>
          <li>Heavy equipment</li>
          <li>Unorganized/poorly maintained injury records</li>
          <li>Failing to comply/post OSHA standards</li>
          <li>Lack of routine safety inspections</li>
        </ul>
        <h2>Imminent Danger in the Workplace</h2>
        <p>
          There are conditions when an employee can legally refuse to work until
          the workplace is restored to a safe environment again. If "imminent
          danger" is present in the employee's workplace, the law protects the
          employee until the employer rectifies the situation.
        </p>
        <p>A few examples of imminent danger in the workplace include:</p>
        <ul>
          <li>A roof that is on the verge of collapsing</li>
          <li>Ungrounded electric equipment</li>
          <li>Fire hazards</li>
        </ul>
        <p>
          Not all safety hazards in the workplace can be classified under
          "imminent danger." Often, situations that threaten the safety of the
          workplace can be resolved with these simple steps:
        </p>
        <ul>
          <li>
            You can file a written complaint with your team's human resources
            department. HR will attempt to resolve your conflict with the
            workplace.
          </li>
          <li>
            If HR fails or dismisses your complaint, you can take your grievance
            to{" "}
            <Link
              to="https://www.legalmatch.com/law-library/article/how-to-file-an-osha-complaint.html"
              target="_blank"
            >
              OSHA{" "}
            </Link>
            or a similar government agency.
          </li>
          <li>
            You can file a private lawsuit with your employer if the above
            methods fail you or if you just want to cut pass any bureaucracy.
            Many employees shy away from this option for fear that they will be
            mistreat in the future for "biting the hand that feeds them" We at
            Binar Chase would like to take this moment to remind you that it is
            absolutely illegal for employers to discriminate this way against
            their employees. If this does happen down the road, our friendly
            employment law attorneys can fight for you in court once more and
            help you recover any losses as well as compensate for your pain and
            suffering in the workplace.
          </li>
        </ul>
        <p>
          If you have questions about the safety of your current workplace or if
          your employer threatens to terminate your employment if you do not
          work in these conditions, contact our experienced team of workplace
          injury accident attorneys now. Your safety is our number one concern
          and you have rights as an employee that should not be disregarded.
        </p>
        <h2>When Should I Report a Work Related Injury</h2>
        <p>
          Any time you sustain an injury on the job that is directly related to
          your job duties and functions, you need to immediately be report the
          injury to your supervisor. Your supervisor is then required to submit
          the incident to the proper source.
        </p>
        <p>
          An employer that does not report the work injury can be fined up to{" "}
          <strong>$5,000</strong> for the first-time incident and up to $10,000
          and up to six months in jail if there is more than one incident that
          has not been reported.
        </p>
        <h2>Do I Need a Lawyer For A Workplace Accident Injury</h2>
        <p>
          Despite stringent safety standards and regulations, California workers
          often face high risks of on-the-job injury every day.
        </p>
        <p>
          Employers, contractors, and property owners may be held liable for
          serious injuries or wrongful deaths sustained at a place of work.
        </p>
        <p>
          If you or a loved one has been injured in a work accident, don't
          hesitate to call us toll free now for a fast, easy no-obligation
          review of your case. Call<strong> 949-203-3814</strong> now. We will
          fight for you.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
