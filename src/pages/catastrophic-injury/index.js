// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        regex: "/california-catostrophic-injury-attorney-banner.jpg/"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Catastrophic Injury Attorneys - Bisnar Chase"
        pageDescription="A catastrophic injury lawyer can help you recover compensation from your life altering injuries. If you’ve been the victim of a severe injury please call Bisnar Chase for top rated representation. Call 800-561-4887. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Catastrophic Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Catastrophic injury attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text"></div>
        <p>
          The <strong>California Catastrophic Injury Attorneys</strong> at
          Bisnar Chase are here to help accident victims and their families gain
          the fair compensation they deserve for the losses they have faced.
        </p>
        <p>
          Injuries that are considered catastrophic can leave a person suffering
          from permanent disabilities directly affecting their quality of life.
        </p>
        <p>
          Even basic daily activities can prove difficult when you have
          sustained a catastrophic injury. Many victims of brain injuries and
          spinal cord injuries become overwhelmed with medical expenses and are
          unable to recover financially because their disability prevents them
          from working or earning a livelihood.
        </p>
        <p>
          If you or a loved one has been catastrophically injured, call us now
          at <strong>800-561-4887</strong> now for legal advice and to
          <strong>set up your free consultation</strong>.
        </p>
        <h2>What Makes an Injury Catastrophic in California?</h2>
        <LazyLoad>
          <img
            src="/images/personal-injury/catostrophic-injury-lawyer-burn-image.jpg"
            width="100%"
            alt="california catastrophic injury lawyer"
          />
        </LazyLoad>
        <p>
          The American Medical Association (
          <Link to="https://www.ama-assn.org/" target="_blank">
            AMA
          </Link>
          ) definition of a catastrophic injury is a serious injury to the brain
          or spinal cord. All serious injuries can have great physical,
          emotional and financial costs.
        </p>
        <p>
          Catastrophic injuries, however, can result in permanent disabilities
          that affect every aspect of an individual's life.
        </p>
        <p>
          Furthermore, catastrophic injuries often affect far more than the
          victim. They also have a significant impact on the victim's family
          members who must make sacrifices to ensure that their loved one
          receives adequate care and attention. This can cause financial and
          emotional burdens for all involved.
        </p>
        <h2>6 Types of Catastrophic Injuries</h2>
        <p>
          There are many kinds of serious injuries that can be classified as
          catastrophic if the symptoms are long lasting.
        </p>
        <p>
          <strong>Some catastrophic injury examples include</strong>:
        </p>

        <ol>
          <li>
            <strong>Burn injuries:</strong> Severe burns can result in permanent
            disfigurement and nerve damage. Burns injuries are considered
            catastrophic when a significant portion of the body is permanent
            damaged.
          </li>
          <li>
            <strong>Multiple bone fractures:</strong> A single bone fracture can
            heal over a period of weeks or months with proper treatment. When
            someone suffers multiple bone fractures in the same accident, he or
            she may experience long-term effects. Some victims of multiple bone
            fractures never regain their strength and mobility and experience a
            lifetime of reduced physical abilities.
          </li>
          <li>
            <strong>Amputations:</strong> Loss of limbs is a significant
            catastrophic injury. Such injuries may occur as a result of car
            crashes or on-the-job injuries. Injured victims are often unable to
            work or return to their jobs after an accident that involves an
            amputation. A catastrophic injury lawyer can win you the
            compensation you need for medical expenses and cost of equipment
            such as prosthetics.
          </li>
          <li>
            <strong>Spinal cord injuries:</strong> Devastating injuries to the
            neck or back can result in trauma to the spinal cord. Since the
            spinal cord is responsible for transmitting signals from the brain
            to the rest of the body,{" "}
            <Link
              to="/catastrophic-injury/spinal-cord-injury"
              target="new"
              title="California Spinal Cord Injury Lawyer"
            >
              spinal cord injuries
            </Link>{" "}
            can result in paralysis.
          </li>
          <li>
            <strong>Traumatic brain injuries:</strong> Mild concussions do not
            typically result in catastrophic brain injuries, but severe damage
            to the brain can have permanent consequences. Victims of
            catastrophic brain injuries often suffer from physical and mental
            disabilities.
          </li>
          <li>
            <strong>Electrocution injuries:</strong> Electrocution occurs when
            an electric current passes through a person’s body, and can result
            in serious injury or death. Victims can suffer a wide range of
            lasting{" "}
            <Link
              to="/misc/electrocution-injury-lawyer"
              target="new"
              title="California Electrocution Injury Lawyer"
            >
              electrocution injuries
            </Link>
            , from major burns and internal injuries to sudden cardiac arrest.
          </li>
        </ol>
        <h2>Causes of Catastrophic Injuries</h2>
        <p>
          There are many ways in which an individual may suffer catastrophic
          injuries. Car crashes are among the most common causes of catastrophic
          injuries. Many of the most severe permanent injuries result from
          rollovers, head-on collisions and t-bone collisions.
        </p>
        <p>
          This is why it is vital for vehicle occupants to wear a seatbelt at
          all times.
        </p>
        <p>
          <strong>
            You can also suffer a life-changing head or back injury in a
          </strong>{" "}
          :
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/premises-liability/slip-and-fall-accidents">
              Slip-and-fall accident
            </Link>
          </li>
          <li>Ladder accident</li>
          <li>Scaffolding collapse</li>
          <li>Car fire</li>
          <li>Sporting activity</li>
          <li>Assault or work-related accident</li>
        </ul>
        <p>
          Regardless of what caused your injuries, there are legal options
          available that could help you get the support you need and rightfully
          deserve.
        </p>
        <h2>Catastrophic Injury Statistics in California</h2>
        <p>
          The majority of catastrophic injury claims involve traumatic brain
          injuries and facet joint injuries. According to the{" "}
          <Link
            to="https://www.nscisc.uab.edu/PublicDocuments/fact_figures_docs/Facts 2013.pdf"
            target="_blank"
          >
            {" "}
            National Spinal Cord Injury Statistical Center
          </Link>
          , there are about 273,000 people currently living with spinal cord
          injuries in the United States and there are approximately 12,000 new
          cases each year.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.cdc.gov/" target="_blank">
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>
          , approximately 1.7 million people sustain a traumatic brain injury
          annually.
        </p>
        <p>
          In the case of a catastrophic injury, a full recovery is not possible,
          however, there are rehabilitation techniques that can help injured
          victims regain their quality of life to a certain degree. Vocational
          programs can even help them find a job they can perform.
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/doctor-patient-image.jpg"
            width="100%"
            alt="Catastrophic injury lawyer in California"
          />
        </LazyLoad>
        <h2>Who Is Liable for a Catastrophic Injury?</h2>
        <p>
          There are accident cases in which one or more parties are responsible
          for the injury. To identify who is liable for catastrophic injuries
          your <strong>California Catastrophic Injury Attorney</strong> will
          most likely ask "who was at the scene of the incident?"
        </p>
        <p>
          <strong>
            Examples of liable parties can be if you were injured{" "}
          </strong>
        </p>
        <ul>
          <li>
            At work operating machinery your employer maybe held accountable for
            injuries
          </li>
          <li>
            In a vehicle accident that involved drunk driving, the driver in the
            other car can be held liable
          </li>
          <li>
            Developed a skin rash due to a product without a warning label you
            can pursue an injury case as well
          </li>
        </ul>
        <h2>Receiving Fair Compensation for Your Severe Injuries</h2>
        <p>
          Catastrophic injuries can have an enormous impact on victims and their
          families. It often affects the victim's quality of life as well.
        </p>
        <p>
          Many victims of catastrophic injuries suffer from psychological issues
          such as depression and may even undergo personality changes resulting
          in stress and frustration for their family. This is why it is
          important that victims are provided support for their suffering in
          addition to compensation for their physical injuries.
        </p>
        <p>
          Victims of catastrophic injuries have the right to pursue financial
          compensation for their losses.
        </p>
        <p>
          <strong>
            Damages that your catastrophic injury lawyer can recover for you
            are:{" "}
          </strong>
        </p>
        <ul>
          <li>Lost current and future wages</li>
          <li>Loss of life's enjoyment</li>
          <li>Physical pain and suffering</li>
          <li>Mental anguish</li>
          <li>Permanent disability coverage</li>
          <li>Current and future medical bills</li>
          <li>The cost of rehabilitation services</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Catastrophic injury lawyers in California"
            style={{
              backgroundImage:
                "url('/images/personal-injury/injury-text-header-photo.jpg')"
            }}
          >
            <h2>Have You Suffered From a Catastrophic Injury in California?</h2>
          </div>
        </LazyLoad>
        <p>
          When you have suffered a life-changing injury, the first step is to
          make recovery your first priority. The experienced{" "}
          <strong>California Catastrophic Injury Attorneys</strong> at Bisnar
          Chase have represented severely injured victims who have suffered from
          catastrophic injuries.
        </p>
        <p>
          We know the pain and suffering not only victims, but also their family
          members, endure in such situations.
        </p>
        <p>
          The law firm of{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          serves as a one-stop legal resource for these victims and their
          families. Our goal is to help these victims and their family members
          reclaim their lives and obtain a sense of justice and financial
          security.
        </p>
        <p>
          Please contact us at <strong>800-561-4887</strong> to receive a free
          case analysis and obtain more information about pursuing your legal
          rights.
        </p>
        <p>
          <strong>
            Find our more about the law firm of Bisnar Chase by clicking the
            video below
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
