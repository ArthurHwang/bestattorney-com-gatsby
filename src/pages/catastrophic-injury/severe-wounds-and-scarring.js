// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Severe Wounds, Scarring and Blisters - Bisnar Chase"
        pageDescription="Serious wounds and scars can result in blisters, skin tear, or skin displacement. Continue reading to learn more and call 800-561-4887 for immediate legal help."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Severe Wounds, Scarring and Blisters</h1>
        <BreadCrumbs location={location} />

        <p>
          Many types of injuries result in scarring. Severe burns can even
          result in disfigurement. These types of injuries can have a lasting
          impact on the victim's quality of life and perception of self-worth.
          In addition to the physical injuries, scars and disfigurement can also
          cause lifelong emotional and mental issues.
        </p>
        <p>
          If your scars or severe wounds were created by the negligence or
          malicious intent of another party, you may be entitled to
          compensation. The experienced{" "}
          <Link to="/catastrophic-injury" target="_blank">
            catastrophic injury lawyers
          </Link>{" "}
          at Bisnar Chase can discuss your legal rights and what actions you can
          take at your complimentary consultation. Call us now at{" "}
          <strong>800-561-4887</strong> to set up your free case review.
        </p>

        <h2>Why Do Scars Develop</h2>
        <p>
          There are many factors that can determine whether or not a wound or
          burn will result in permanent scarring. Some of these factors include:
        </p>
        <ul>
          <li>
            <strong>The age of the victim</strong>
          </li>
          <li>
            <strong>The severity the injury</strong>
          </li>
          <li>
            <strong>The depth of the wound. </strong>
          </li>
        </ul>
        <p>
          Scars form when the top layer or second layer of the skin is damaged.
          In response to a burn, the body forms a protein called{" "}
          <Link
            to="https://www.phoenix-society.org/resources/entry/burn-wound-care"
            target="_blank"
          >
            collagen
          </Link>{" "}
          to help with the healing process.
        </p>
        <p>
          Under normal conditions, collagen fibers are laid down in a relatively
          organized manner. When there is a deep burn, these collagen fibers may
          be laid down in a disorganized manner or even on top of a raised burn
          resulting in an odd texture and appearance.
        </p>
        <h2>The Healing Process for Scars and Burns</h2>
        <p>
          If you have suffered a burn injury, it may not be clear if or when
          your scars will completely heal. The healing time will be dependent on
          the burn degree you had sustained. Third degree burns take the longest
          to heal.
        </p>
        <p>
          Within the first few months of your injury, a scar will begin to
          develop. After about six months, your scar will look as pronounced as
          it ever will. Only after a year has passed will it become clear what
          your scar will look like. It will hopefully fade, flatten and become
          less sensitive.
        </p>
        <p>
          Cosmetic procedures to exist to reduce the appearance of these burns
          and scars. Although these procedures can be expensive, they are an
          option for aesthetically treatment after your injury. Our lawyers at
          Bisnar Chase can help you obtain the settlement you deserve to cover
          these types of surgeries as well as any other medical expenses that
          pertained to your recovery and any lost wages.
        </p>

        <h2>Hypertrophic Burn Scars</h2>
        <p>
          Hypertrophic scars can develop within a few months after the injury.
          They stay within the area of the original burn injury and they are
          often warm, hypersensitive, puffy and itchy.
        </p>
        <p>
          These types of scars are more pronounced than other types of scars
          because they typically have hues of deep red or purple and are
          elevated above the surface of the skin. When these scars form near
          joints, they can decrease the victim's ability to move.
        </p>
        <p>
          A contracture is when severe scars develop across joints. When
          hypertrophic scars result in a contracture on the victim's legs, it
          may affect the victim's ability to squat, sit, climb stairs or walk.
          Basically, this type of scarring can render a victim immobile after a
          catastrophic accident.
        </p>
        <p>
          When a contracture exists around the arms, this scarring can affect
          the victim's ability to eat, groom, dress or bathe. It is often
          possible, however, to reduce the consequences of contractures through
          stretching and physical therapy. Regardless, it is apparent that if a
          victim suffers from this type of injury, their quality of life will
          never be the same following the accident.
        </p>

        <h2>Blisters and Skin Tears</h2>
        <p>
          Blisters are a common complication of burn scars. Blisters can develop
          if the scar is rubbed or sheared during the healing process.
        </p>
        <p>
          Avoid tight clothing or any garments that put pressure on the scar.
          Skin tears can occur if you scratch your scar or bump into something
          as your scar is still healing. Skin tears are potentially dangerous
          because they can result in an infection if they are not properly
          cleaned and treated. If a skin tear does occur during this stage of
          the healing, seek medical help immediately. In addition, document this
          medical expense for future use after you have been treated.
        </p>

        <h2>How to Prove Fault in Burn Accident</h2>
        <p>
          If you have suffered a serious injury resulting in severe wounds and
          scarring, you need to gather as much evidence as possible that shows
          the changes, pain and suffering that you have experienced since the
          accident.
        </p>
        <p>
          Having comparable photos of how you looked before the incident and how
          you look now can prove useful if you choose to file a catastrophic
          injury claim. It is also well advised to document your injuries by
          taking photos soon after the incident and during the healing process.
        </p>
        <p>
          Have a professional photographer take your photos so that you will
          have large, high-resolution shots that clearly state the severity of
          your scars and injuries.
        </p>
        <p>
          Keep a journal of your day-to-day struggle of recovery. This testimony
          can be viable to winning the compensation you deserve. Also, keep a
          well organize record of all your recovery expenses.
        </p>

        <h2>The Best Legal Help for Burn Injuries</h2>
        <p>
          Scars and burns resulting from a serious injury can cause long lasting
          pain and embarrassment.
        </p>
        <p>
          Severe wounds and scarring injury can affect the victim's confidence,
          and sadly, it can affect the way they are perceived by others.
          Permanent scars can even affect your social life and your ability to
          secure a job. If your wounds or scars were caused by the negligence of
          someone else, you may be able to receive compensation for your
          considerable suffering.
        </p>
        <p>
          Financial support may be available for all of the medical treatments,
          surgical procedures and hospitalization related to the accident.
        </p>
        <p>
          Additionally, support should be available for your mental anguish and
          physical pain. It is important to understand the potential value of
          your claim before accepting any settlement offer. It is common for
          insurance providers and other third parties to offer inadequate
          settlements to quickly resolve cases. It is critical during this
          difficult time that you obtain the legal help, counsel and guidance
          you need to protect your rights and best interests.
        </p>
        <p>
          Call us now at <strong>800-561-4887</strong> for your free case
          review. We promise to deliver you the settlement that you deserve. In
          the small chance that we do not live up to this promise, you don't pay
          us anything; no win, no fee.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
