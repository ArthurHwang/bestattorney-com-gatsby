// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/Back Injury Official Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Back Injury Case Attorneys - Negligence Case Compensation"
        pageDescription="The California Back Injury Case Lawyers of Bisnar Chase have been winning compensation for victims of negligent back injuries for over four decades. Spinal cord injury cases can be complicated to maneuver through and you need an expert on your side. The Bisnar Chase Injury Attorneys will fight for you. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Back Injury Case Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California back injury case lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong>California Back Injury Case Lawyers</strong> of Bisnar
          Chase understand that suffering from a severe back injury can be
          devastating. Millions of spinal cord injury victims find themselves
          unable to work or worse; paralyzed. Lost wages, loss of consortium and
          medical expenses must be compensated for if there is any chance of
          recovery.
        </p>
        <p>
          Our exceptional legal representation guarantees that{" "}
          <strong>if there is no win there is no fee</strong>. For the past{" "}
          <strong>40 years</strong>, the law firm of{" "}
          <Link to="/">Bisnar Chase</Link> has held a{" "}
          <strong>96% success rate</strong> in wins for damages back injury
          victims have faced. With six highly-rated lawyers and a determined
          litigation team, Bisnar Chase's California Back Injury Case Attorneys
          have collected over <strong>$500 Million dollars</strong> in
          compensation for our clients.
        </p>
        <p>
          If you or someone you know has endured pain and suffering due to a
          back injury, spinal cord injury or{" "}
          <Link to="/catastrophic-injury/neck-injury-case-lawyer">
            serious neck injury
          </Link>
          , <strong>Call 800-561-4887</strong>. Upon your call, you will receive
          a<strong> free consultation</strong> with one of our experienced
          injury attorneys. You do not have to go up against insurance companies
          and negligent parties alone.
          <strong> Contact us today</strong>.
        </p>
        <h2>What Is the Function of the Spinal Cord?</h2>
        <p>
          The spinal cord is the pathway shared by the brain and the body. The
          brain transmits signals through the spinal cord and sends the messages
          to the body for movement. Bones and clear fluid, otherwise known as
          cerebral spinal fluid, surround the spinal cord as a means of
          protection.
        </p>
        <p>
          <strong>
            There are five main regions that make up the spinal cord:
          </strong>
        </p>
        <ol>
          <li>Neck (cervical)</li>
          <li>Chest (thoracic)</li>
          <li>Abdominal (lumbar)</li>
          <li>Pelvic (sacral)</li>
          <li>Tail-bone (coccygeal)</li>
        </ol>
        <p>
          If any of these spinal nerves are damaged, it can cost a victim their
          ability to be mobile.
        </p>
        <h2>Causes of Back Injuries</h2>
        <p>
          In the United States, over 17,000 people fall victim to serious or
          catastrophic back injuries. About 38% of these victims have endured{" "}
          <Link to="/catastrophic-injury/spinal-cord-injury" target="_blank">
            spinal cord injuries
          </Link>{" "}
          from a car accident. These spinal injuries occur in a vehicle crash
          because of the impact on the head and back.
        </p>
        <p>
          <strong>Other causes of spinal cord injuries are</strong>:
        </p>
        <ul>
          <li>
            <strong>Violence</strong>: According to Peace Alliance, over 1.6
            million deaths are a result of violence. Violence encompasses
            anything relating to physical, emotional, psychological or sexual
            abuse.
          </li>
          <li>
            <strong>Surgical complications</strong>: When a spinal cord surgery
            is performed, there is always a slight chance there might be an
            error. If one of the nerves in the spine are damaged this can lead
            the patient to be paralyzed.
          </li>
          <li>
            <strong>Falls</strong>: The National Spinal Cord Injury
            Database reports that spinal cord injuries from slip and falls have
            increased for the past four decades. Usually, the population that
            suffers the most harm to the spine as a result of falls are elderly
            people.
          </li>
          <li>
            <strong>Sports</strong>: Although helmets and protective gear are
            available to athletes, it does not always prevent them from having
            serious spinal cord injuries. Athletes that are involved in sports
            such as football, hockey or gymnastics have an increased chance of
            suffering from a serious back injury.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/personal-injury/Causes of Spinal Cord and Back Injury Chart.jpg"
            width="75%"
            className="imgcenter-fluid"
            alt="Causes of spinal cord injuries graph"
            title="back injury case lawyers"
          />
        </LazyLoad>
        <center>
          <blockquote>
            Data Courtesy of The National Spinal Cord Injury Statistical Center
          </blockquote>
        </center>
        <h2>The Settlement for Workers' Compensation</h2>
        <p>
          Almost 74% of people who have experienced a back injury have received
          an average amount of $23,000 dollars for workers compensation. When an
          employee is injured in the duration of a company's employment, this is
          known as workers' compensation.
        </p>
        <p>
          Usually, after the employee has been severely injured, the workers'
          compensation insurance of the employer will cover the medical expenses
          and/or lost wages the employee has endured. If an employer does not
          acquire insurance, the employee can pursue a lawsuit against the
          employer.
        </p>
        <p>
          In the state of California, an employee has up to five years to file a
          workers comp. claim.
        </p>
        <h2>Complete vs Incomplete Back Injuries</h2>
        <p>
          To determine if a spinal cord injury is complete or incomplete is
          based on how much damage has been inflicted to the spine. One of the
          factors that contribute to a complete or incomplete spinal cord injury
          is where the impairment has taken place.
        </p>
        <p>
          <strong>
            Complete and incomplete back injuries are classified as the
            following
          </strong>
          :
        </p>
        <p>
          <strong>Complete Back Injury</strong>: A complete spinal cord injury
          is when a nerve/nerves in the spine are completely damaged. If a nerve
          along the spine has been completely impaired this can disrupt the
          signals that go to and from the brain to the body, unfortunately,
          leaving a person paralyzed.
        </p>
        <p>
          <strong>Incomplete Back Injury</strong>: When the spine has suffered
          some harm to the nerves but is still able to feel sensation in their
          body and still has the ability to move, this is known as an incomplete
          back injury. The amount of movement that a person can display differs
          from person-to-person.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Spinal cord injury attorney in California"
            style={{
              backgroundImage:
                "url('/images/personal-injury/spinal cord injury text header image.jpg')"
            }}
          >
            <h2>Types of Spinal Cord Injuries</h2>
          </div>
        </LazyLoad>
        <p>
          The aftermath of a spinal cord injury can be irreversible. The type of
          back injury you have will determine your recovery. There are three
          main spinal cord injuries that are common.
        </p>
        <p>
          <strong>3 Common spinal cord injuries</strong>:
        </p>
        <ol>
          <li>
            <strong>Tetraplegia</strong>: Injuries of this magnitude involve the
            inability of having any sensation or control of the body from the
            neck down. Another term that is commonly used to describe this type
            of spinal injury is known as quadriplegia.
          </li>
          <li>
            <strong> Paraplegia</strong>: When a patient looses any feeling from
            the lower part of the body this is known as{" "}
            <Link to="https://en.wikipedia.org/wiki/Paraplegia" target="_blank">
              paraplegia
            </Link>
            . Usually when the thoracic area of the spinal cord is damaged this
            level of paralysis may be the end result.
          </li>
          <li>
            <strong>Triplegia</strong>: Triplegia is when one arm and both legs
            lose sensation or the ability to move. This can be an outcome from
            an incomplete injury.
          </li>
        </ol>
        <h2>Dealing with Long Term Back Injuries</h2>
        <p>
          Studies show that 11%-40% of Americans suffer from chronic back pain.
          Chronic back pain is discomfort in the lower back that continues for
          more than 12 weeks. The pain level of lower back pain can vary from
          person-to-person, but there are three predominately types of chronic
          back pain.
        </p>
        <p>
          <strong>3 Common types of lower back pain</strong>:
        </p>
        <ol>
          <li>
            <strong>Sciatica</strong>: Pain that creeps in from the lower back
            and travels down the leg is usually one of the symptoms of a sciatic
            nerve injury. The sciatic nerve, which is located in the lower part
            of the spine, becomes painful when it is irritated. It can be
            difficult to sit or walk as well. Treatment for this type of pain
            includes putting ice or warm compressions on the lower back, pain
            prescriptions or epidural steroid injections.
          </li>
          <li>
            <strong>Slipped or bulging disc</strong>: Discs in the spine hold
            the important role of absorbing shock and supporting the upper body
            for movement. Disc degeneration comes with age or when severe
            pressure is inflicted upon the lower part of the spine. Most people
            who have suffered from a herniated disc are from the ages 35-50.
            Recovery comes from physical therapy, spinal manipulations or if
            needed, surgery.
          </li>
          <li>
            <strong>Arthritis</strong>: Stiffness in the morning or a tingling
            and numbness sensation in the spine can be due to spinal arthritis.
            Arthritis in the lower back is common among people 45 years and
            older. If a young person is suffering from this type of lower back
            pain it is due to trauma or a genetic disorder. Acupuncture,
            medications and strengthening exercises are used as treatment.
          </li>
        </ol>
        <h2>The Road to Recovery After a Back Injury</h2>
        <p>
          Waking up from an accident and discovering that you have lost
          sensation in parts of your body can be confusing and terrifying. Most
          patients that realize that they are paralyzed find themselves in
          disbelief. The road to recovery and the treatment that follows is one
          that consists of hardship and requires commitment.
        </p>
        <p>
          <strong>
            Below are options that a patient can take to begin the emotional and
            physical healing process:
          </strong>
        </p>
        <p>
          <strong>Enrolling in recovery programs</strong>: Physical therapy,
          participating in exercises that enhance skill development and speaking
          to a counselor can serve as an outlet for psychological, emotional and
          social support. It is vital that close relatives partake in these
          programs to better understand and help their loved one.
        </p>
        <p>
          <strong>Meeting and working with specialists</strong>: The team that
          is involved in the treatment program is very important. Team members
          are usually organized by a case worker and includes physical
          therapists, occupational therapists, counselors, nurses, at-home
          caretakers and nutritionists.
        </p>
        <p>
          <strong>Engaging in recreational activities</strong>: In order to
          improve and sustain a satisfying quality of life, recreational
          activities that do not include watching television or staying
          sedentary are significant. Leisure time should be spent delving into
          new interest or{" "}
          <Link
            to="https://www.christopherreeve.org/living-with-paralysis/health/staying-active/recreation"
            target="_blank"
          >
            recreational sports
          </Link>{" "}
          such as bowling, hunting, flying or dancing.
        </p>

        <h2>Damages That Can Be Compensated in a Back Injury Case</h2>
        <p>
          Spinal cord injuries are permanent and can prevent a survivor of
          carrying out daily tasks. Family members that are affected may need
          aid to care for their loved one. Although a cure for a spinal cord
          injury does not exist, seeking damages for a third party's negligence
          will greatly help family members and spinal cord injury survivors in
          their recovery.
        </p>
        <p>
          <strong>Spinal cord injury survivors may be compensated for: </strong>
        </p>
        <ul>
          <li>Medical costs</li>
          <li>Loss of wages</li>
          <li>Loss of consortium</li>
          <li>At-home care and assistance</li>
          <li>Pain and suffering</li>
        </ul>
        <LazyLoad>
          <img
            src="/images/personal-injury/Official Back injury page.jpg"
            width="100%"
            alt="Back injury lawyers in California"
          />
        </LazyLoad>
        <h2>Hire the Back Injury Experts to Be on Your Side</h2>
        <p>
          The law firm of Bisnar Chase believes that accident victims should not
          have to pay out-of-pocket for someone else's carelessness. Our
          <strong> California Back Injury Case Lawyers </strong>guarantee to
          produce an outstanding result and if there is{" "}
          <strong>no win there is no fee</strong>.
        </p>
        <p>
          Your injury should not stop you from having a full and happy life.
          Call the California back injury case attorneys of Bisnar Chase and
          receive a <strong>free case analysis</strong>.
          <strong>Contact (949) 203-3814</strong> and learn more about exploring
          your legal rights.
        </p>
        <p>
          <strong>
            Call 949-203-3814 or <Link to="/contact">contact us</Link>.
          </strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
            Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
