// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Burn Injury Accident Lawyers - Bisnar Chase"
        pageDescription="Burns can be nasty aqnd painful injuries. Get the facts here now about burn injuries causes, symptoms and legal rights if caused by an accident"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Burn Injury Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Burn injuries can occur whenever your skin is exposed to extreme heat,
          chemicals, sunlight, electricity and/or radiation.
        </p>
        <p>
          Some of the most common causes of burn injuries are fires, scalds from
          hot liquids and exposure to flammable liquids and gases.
        </p>
        <p>
          If you have sustained a burn injury, you may need to undergo skin
          grafts and other cosmetic surgery procedures, which may be extremely
          costly and not often covered by health insurance.
        </p>
        <p>
          Burn injuries also cause significant emotional and financial strain on
          victims and their families. If you or a loved one has been affected by
          a burn injury due to the negligence of another party, you maybe
          entitled to compensation.
        </p>
        <p>
          If this is the case, we urge you to call
          <strong> 800-561-4887 </strong>to speak to one of our highly trained}{" "}
          <Link to="/catastrophic-injury">catastrophic injury lawyers</Link>{" "}
          about your legal rights and if you qualify for compensation.
          Regardless of your situation, we also urge you to keep reading so you
          may be prepared in the case that you or your loved ones get seriously
          injured from a burn accident.
        </p>

        <h2>Burn Injury Statistics</h2>
        <p>
          According to the{" "}
          <Link
            to="https://www.cdc.gov/safechild/burns/index.html"
            target="_blank"
          >
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>
          , there is a fire-related death every
          <strong>169 minutes</strong> and a burn injury every{" "}
          <strong>30 minutes</strong> in the United States.
        </p>
        <p>
          Approximately<strong> 85 percent</strong> of all fire deaths in 2009
          occurred in homes. Not counting firefighters,{" "}
          <strong>2,640 people</strong> were killed and <strong>13,350</strong>{" "}
          were injured in home fires in the United States in the year 2010. Most
          victims of home fires are not killed from burns but from smoke
          inhalation and from breathing in toxic gases.
        </p>
        <p>
          Over a third of all home fire deaths occur in homes without smoke
          alarms. Alcohol is also a contributing factor in an estimated{" "}
          <strong>40</strong> percent of residential fire deaths.
        </p>
        <p>
          Home fires are not the only cause of burn injuries. According to the{" "}
          <Link
            to="https://www.usfa.fema.gov/prevention/outreach/burn_prevention.html"
            target="_blank"
          >
            U.S. Fire Administration
          </Link>
          , approximately 1 in 7 fires responded to by fire departments involve
          highway vehicles. Between the years 2008 and 2010, there were about{" "}
          <strong>194,000 </strong>highway vehicle fires each year resulting in{" "}
          <strong>300</strong> deaths and <strong>1,250</strong> injuries
          annually.
        </p>
        <p>
          One of our savvy burn injury lawyers can fill you in on more
          statistics concering burn injuries as well as more information on your
          legal rights concering these types of accidents.
        </p>

        <h2>Causes of Burn Injuries</h2>
        <p>
          Cooking is normally the cause of residential fires, however, smoking
          is the number one cause of fire-related deaths.
        </p>
        <p>
          Fire-related deaths are on the rise during the winter months because
          there is an increase of the use of heating devices and chimneys.
        </p>
        <p>
          Burn injuries can also come from car fires after an accident. When a
          person sustains a burn injury in a car fire, it must be know if the
          fire was catalyzed by a <Link to="/auto-defects">vehicle defect</Link>
          .
        </p>
        <p>
          When burn injuries are sustained in the workplace, an investigation
          will be needed to review the safety policy of the employer and the
          negligence of the property owner.
        </p>
        <p>
          Whereever your burn injury originated, our professional burn injury
          lawyers can help you identify the at-fault party of the accident and
          can help you take legal action accordingly.
        </p>

        <h2>The Three Types of Burn Degrees</h2>
        <p>
          Some burn injuries can heal with limited medical treatment while more
          severe burns require more complex procedures such as skin grafts.
        </p>
        <p>
          Depending on the severity of the injury, the burn will be classified
          by the following:
        </p>
        <ul>
          <li>
            <b>First-degree burns:</b> This is when only the outer layer of the
            skin (Epidermis) is damaged.
          </li>
          <li>
            <b>Second-degree burns:</b> In a second-degree burn, the outer layer
            of the skin is damaged as well as the layer of skin directly
            underneath it (dermis).
          </li>
          <li>
            <b>Third-degree burns:</b> This is when the deepest layer of skin is
            damaged or destroyed. In third-degree burns, there is likely damage
            to the surrounding tissues as well.
          </li>
        </ul>

        <h2>Burn Injury Symptoms</h2>
        <p>
          Burn injuries can result in blistering, swelling, scarring, shock and
          even death.
        </p>
        <p>
          The skin is a protective organ on your body, so when the skin suffers
          damage, the body is vulnerable to infections and additional injuries.
        </p>
        <p>
          Those who have suffered a first-degree burn will likely be able to
          promote the healing process with antibiotic creams.
        </p>
        <p>
          Victims of third-degree burns, however, will need professional care to
          encourage new skin to grow.
        </p>
        <p>
          If you do plant to take legal action for your burn injury, take
          pohysical notes of your symptoms as well as keep a record of all the
          receipts and invoices pertaining to your recovery so your burn injury
          lawyer can use these pieces of evidence to get you the compensation
          you deserve.
        </p>

        <h2>Skin Grafts and Burn Injuries</h2>
        <p>
          Skin grafting involves the transplantation of skin from a healthy part
          of the body to an area of the body where the skin has been permanently
          damaged. This type of surgical procedure is typically reserved for
          victims who have suffered a serious injury.
        </p>
        <p>
          A successful skin graft will reduce the amount of treatment needed and
          improve the appearance of the area that receives the graft.
        </p>
        <p>
          Some victims of burn injuries require grafts where only a thin layer
          is removed from the donor section, while others require a full
          thickness skin graft.
        </p>
        <p>
          Full thickness grafts are typically more painful and there is a
          greater chance that the body will not accept the skin.
        </p>
        <p>
          If your burn injury is serious enough that you will need skin grafts,
          it is safe to say you will have to pay some hefty medical bills in the
          near future, however, if you were the innocent of some victim of
          someone else's neglience, isn't it just that you should be compensated
          for your injuries?
        </p>
        <p>
          The knowelegable burn injury attorneys at Bisnar Chase think so, and
          if you chose us, we will deliver you that settlement or you won't have
          to pay us a dime. This is our promise to our clients.
        </p>

        <h2>Burn Injury Prognosis</h2>
        <p>
          A few decades ago, victims of serious burns over large portions of
          their body had very slim chances of surviving.
        </p>
        <p>
          Medical technology has since improved to the point where victims who
          have suffered burn injuries over 90 percent of their body can now
          survive.
        </p>
        <p>
          They will, however, have permanent impairments and scars. Even
          immediate treatment at a burn center will not result in a complete
          recovery if the damage is extensive and deep. These are no doubt
          catastrophic injuries.
        </p>
        <h2>How to Take Legal Action After a Burn Accident</h2>
        <p>
          The expenses related to the treatment of burn injuries can add up
          quickly.
        </p>
        <p>
          Many victims require multiple procedures and lengthy hospital stays.
          Victims and their families would be well advised to seek legal counsel
          and obtain access to the resources they need during these challenging
          times. This is where Bisnar Chase comes in.
        </p>
        <p>
          At Bisnar Chase, it is our mission to help you, in your time of need,
          obtain information about your rights as well as to pursue legal action
          against the at-fault party.
        </p>
        <p>
          Our trained burn injury attorneys are skilled and knowledgeable about
          the law. In addition, we all operate on a contingency basis. This
          means that in the rare case that we lose or do not deliver you the
          settlement that you deserve, you will not have to pay us a dime.
        </p>
        <p>
          With so little to lose and so much to gain, why wait any longer to
          give us a call? If that was not enough to convenience you to call us,
          let us also remind you that there is a stature of limitation
          concerning many cases of catastrophic injury, so if you plan to take
          action, you need to acct now.
        </p>
        <p>
          Call us now at<strong> 800-561-4887 </strong>to schedule your free
          consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
