// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Serious Injury Lawyers: Testimonials -Bisnar Chase"
        pageDescription="Serious injury laws hhas settled thousands of personal injury cases worth over 500 million dollars. Here are some testimonials from satisfied clients"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Serious Injury Lawyers - Testimonials.</h1>
        <BreadCrumbs location={location} />

        <p>
          <b>
            Our serious injury lawyers have settled thousands of personal injury
            cases worth over 500 million dollars. If you've had a serious
            injury, call 949-203-3814 for a free consultation
          </b>
          .
        </p>
        <h2>Serious Injury Settlements - Testimonials.</h2>
        <p>J. L. "So happy that they were so nice."</p>
        <p>
          C.W. from Legal Network Staffing: "On behalf of Legal Network Staffing
          Service and the team of temporary employees who were dispatched to
          work in your offices, I would like to personally thank you for
          providing such a conducive atmosphere in which to work.
        </p>
        <p>
          "Legal Network Staffing Service has been in business since 1977, and I
          don't remember a time when I received such complimentary feedback from
          the temporary employees concerning any law office. It was said that,
          "The Bisnar firm was a lovely place to work," that "We could feel the
          team effort put forth toward client representation," and that "It was
          obvious that it started from the top to make the chemistry of the firm
          so wonderful."
        </p>
        <p>
          "Shannon was very clear with her requirements, and it was evident that
          she had your client's welfare in mind since she was still working with
          me into the evening after she went home to her family obligations.
        </p>
        <p>
          "Thank you very much, John, for making our staffing job so pleasant."
        </p>
        <p>
          A.S.: For a relatively smaller case, I was treated as if it was a
          million plus recovery!!! J.S.: Bisnar Chase will not stop until they
          are satisfied and their expectations are very high.
        </p>
        <p>
          R.W. The team at Bisnar Chase is always ready to serve and goes beyond
          to a personal side. Thank you. N.L.: Bisnar Chase treats their clients
          with respect and professionalism.
        </p>
        <p>
          R.R.: In my time of loss and confusion Bisnar Chase gave me the peace
          of mind to worry only about my injuries and get better - That helped
          me to get back on my feet and get back to a normal life!!! Thank You!!
        </p>
        <p>
          J.A.: Bisnar Chase &amp; Staff are the most courteous and professional
          firm I have had the pleasure of using!!!
        </p>
        <p>
          P.Y.: Like A Good Neighbor, Bisnar Chase were there for me at my time
          of need.
        </p>
        <p>
          M.B.: My experience with Bisnar Chase was a heartwarming experience. I
          really felt cared for and taken care of each and every time and step
          of the way. Thank you to everyone at Bisnar Chase.
        </p>
        <p>
          K.N.: "We received an email from A.G. Edwards stating they have
          received the settlement check from Nissan. Once again, I cannot begin
          to articulate our gratitude to the Bisnar Chase staff - we are so very
          thankful for all you have done for Josh. As parents, Charlotte and I
          can rest in peace, when that day comes, knowing that Josh's needs have
          been taken care of, and his financial future is in good hands with
          sound financial planners at A.G. Edwards.
        </p>
        <p>
          "On December 17, 2003 we received a call from the paramedics stating
          that Josh and Lisa had been in a severe accident, and we needed to
          meet them at the emergency room. I remember telling God that I can
          deal with missing limbs because they can be replaced, but please do
          not take him from us. A month later we met with members of your staff
          for the first time, Josh had a traumatic brain injury and his future
          was looking pretty bleak; we had no idea where he would be eighteen
          months later. All we could think of at the time was that our son's
          future, his goals, and dreams had been taken from him so abruptly.
        </p>
        <p>
          "As time passed, Josh continued to improve and he began his journey of
          recovery. Many have been involved in this process, and you and your
          staff have been an integral part of Josh's recovery, and we will be
          forever grateful. God has brought so many people into our lives, and
          we are so very blessed. Numerous emails were sent to Personal Injury
          Attorney's. John...you were on a ski trip at Big Bear with your
          family, and you were the only Personal Injury Attorney to respond - I
          believe this was divine intervention. Josh has a professor from Mt.
          San Jacinto College who has taken him "under his wing," and is showing
          him other career possibilities.
        </p>
        <p>"John, the future looks good for Josh - thank you!"</p>
        <p>
          K.L: I just wanted everyone to know how grateful Miguel and Luga were
          to your office and everyone that participated in their case I met with
          them this morning to give them their settlement checks &#150; Miguel
          stated from the first time he walked into our office and from walking
          out the door today was one of the most heartwarming experiences he has
          ever had. They both felt cared for and taken care of every step of the
          way! I just wanted to pass that on to each of you and to applaud you
          for such a fine job!
        </p>
        <p>
          M.A.B.: My experience with Bisnar Chase was a most heartwarming
          experience. I really felt cared for and taken care of each and every
          step of the way. Thank you very much John, Brian, Jerry and entire
          staff. God Bless.
        </p>
        <p>
          M.L.: "Bisnar Chase RESOLVED MY CASE WITHIN 90 DAYS FOR $500,000.00!"
        </p>
        <p>
          I was extremely pleased with the way John Bisnar handled my legal
          matter. I was disabled and collecting insurance from a disability
          policy, when the insurance company set up a "trial balloon" and
          decided I was no longer eligible to collect. John Bisnar of Bisnar
          Chase represented me in the successful lawsuit against the insurance
          company. My case was resolved within 90 days of my case being filed.
          Not only did I recover financially $525,000, but I received personal
          satisfaction in standing up to the insurance company that was ripping
          me off.
        </p>
        <p>
          I feel that the settlement I received was due mainly to John Bisnar's
          tenacity and thorough investigation procedures. I have referred
          several people to Bisnar Chase and will continue to do so.
        </p>
        <p>S.M.: "Bisnar Chase HAS WON THEM ALL!"</p>
        <p>
          I have been in the construction business for many years and have
          retained the services of Bisnar Chase on a number of matters.
        </p>
        <p>
          I had dealt with other law firms before turning to Bisnar Chase. I
          found that the law firm of Bisnar Chase and John Bisnar personally
          were always available to answer all of my questions and address my
          concerns thoroughly. The staff is always courteous and attentive and I
          would not hesitate to recommend Bisnar Chase. In fact - I have on many
          occasions.
        </p>
        <p>D.L.: "I WOULD NOT CHANGE A THING ABOUT HOW MY CASE WAS HANDLED!"</p>
        <p>
          On a 1-10 scale, I would give Bisnar Chase a 12!! I retained the
          services of Bisnar Chase after very tragic circumstances necessitated
          bring a lawsuit against a major automobile manufacturer. I was
          extremely satisfied with the manner in which my family and I were
          treated. "Every single person at the firm was attentive, courteous and
          very knowledgeable."
        </p>
        <p>C.B.: "Bisnar Chase IS A GREAT LAW FIRM!"</p>
        <p>
          I retained the services of Bisnar Chase and was very happy with the
          expert treatment I received. Bisnar Chase and John Bisnar in
          particular were aggressive, efficient and understanding. I work for a
          family law firm in Orange County, and was so satisfied with Bisnar
          Chase that I regularly recommend clients to John Bisnar.
        </p>
        <p>R.T. "I WAS VERY SATISFIED WITH Bisnar Chase!"</p>
        <p>
          I found myself in need of a personal injury attorney and turned to
          Bisnar Chase. I felt my legal matter was given the highest quality of
          legal service and professionalism. I was very satisfied with the
          outcome of my case and the handling of if by Bisnar Chase.
        </p>
        <p>
          M.G.: "Hiring Bisnar Chase was the best thing I could have done for my
          situation. They handled all legal issues in a competent and
          professional manner, allowing me to focus on my daily life without
          worrying about the case."
        </p>
        <p>R.B.: "I REFER PEOPLE TO Bisnar Chase WITH NO HESITATION!"</p>
        <p>
          I have known Brian Chase for many years and know how deeply he cares
          about his clients. I refer people to the firm without hesitation. The
          attorneys at Bisnar Chase are truly interested in the welfare of their
          clients.
        </p>
        <p>
          K.H.: "Mr. Bisnar &amp; his staff are very courteous, honest, and
          professional. They took care of everything from start to finish and I
          would highly recommend this Attorney Firm to anyone. Thank you all for
          all your help."
        </p>
        <p>
          A.V.: "Pleasantly surprised with the outcome. Bisnar Chase staff very
          sensitive to situation, very professional."
        </p>
        <p>
          E.N.: "This firm cares not only for themselves but for their clients
          as well. They took very good care of me and I know that they can do
          the same for you!"
        </p>
        <p>
          B.M.: "Bisnar Chase is a miracle worker, who can win your case with
          special &amp; persetency. P.S. Thank you so much. "
        </p>
        <p>
          K.T.: "Bisnar Chase can play ball with the big dogs and the little
          ones, and win."
        </p>
        <p>
          D.Y. &amp; A.Y.: "Bisnar Chase helped us get through the process, from
          beginning to end."
        </p>
        <p>
          R.N.: "I think everybody at Bisnar Chase is great. I didn't have to do
          anything."
        </p>
        <p>C.M.: "Bisnar Chase DID A FABULOUS JOB!"</p>
        <p>
          "Everyone in the firm was very professional. From the receptionists,
          to the paralegal personnel, they were the best."
        </p>
        <p>
          L.P.: "Bisnar Chase has been the best place and law firm. They made me
          feel so comfortable and assisted me with every detail regarding my
          case."
        </p>
        <p>
          N.M.: "Bisnar Chase made me feel very special and were there every
          step of the way."
        </p>
        <p>D.N.: "Very professional staff."</p>
        <p>
          J.R.: "Bisnar Chase is a very professional law firm and one I would
          recommend to anyone!"
        </p>
        <p>
          M.W.: "A professional, organized company that really cares about their
          client."
        </p>
        <p>
          E.A.: "I felt I was informed all the time about my case and
          settlement."
        </p>
        <p>
          R.S. &amp; G.S.: "If we had to do this all over again, we would still
          come back to Bisnar Chase."
        </p>
        <p>
          C.P.: "If this would ever happen to me again, Bisnar Chase would be
          the first people I would call."
        </p>
        <p>
          M.V.R. "Because of Bisnar Chase's wonderful service, I was able to
          have my surgery right away."
        </p>
        <p>D.G.: "Forget Larry H. Parker, go to Bisnar Chase."</p>
        <p>D.R.: "I felt protected with Bisnar Chase. "</p>
        <p>L.S.: "Professional cut and dry. Gets the job done. "</p>

        <p>
          If you believe you have a serious injury claim, do not hesitate to
          contact us for a free one-on-one consultation. Fill out the online
          form or call us at <strong>949-203-3814</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
