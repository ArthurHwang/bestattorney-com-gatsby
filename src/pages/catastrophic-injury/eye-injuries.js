// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Eye Injuries: Medical Treatment & Victim’s Rights – Bisnar Chase"
        pageDescription="Eye injuries can be very serious. If you've had a serious injury to the eye by another, here are some steps to protect your health and rights."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Eye Injury Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          Eyes are referred to many as the "windows to the soul" and are also a
          crucial component of our everyday lives.
        </p>
        <p>
          Eye injuries can result in permanent vision loss, partial vision loss
          or vision restrictions that may affect every aspect of your life.
        </p>
        <p>
          If your eye injury resulted from someone else's carelessness,
          negligence or wrongdoing, it may be in your best interest to learn
          about your legal rights and options by consulting with one of the{" "}
          <Link to="/catastrophic-injury"> catastrophic injury lawyer</Link> at
          Bisnar Chase.
        </p>

        <p>
          Whether the at-fault party responsible for your suffering was a
          product manufacturer, a doctor or a careless driver, financial support
          may be available for your medical treatment, lost wages and other
          related damages.
        </p>
        <h2>When to Seek Help for Your Eye Injury</h2>
        <p>
          If you are issues with your vision or you have recently suffered an
          eye injury, it is imperative to pay attention.
        </p>
        <p>
          Symptoms such as flashing lights, eye floaters and blurred edges may
          seem like minor annoyances, but they could be signs of something much
          more serious.
        </p>
        <p>
          If you are experiencing these or other symptoms, you must seek out
          medical attention{" "}
          <em>
            <strong>immediately</strong>
          </em>
          .
        </p>
        <h2>Warning Signs for Eye Injury</h2>
        <p>
          See a doctor if you are experiencing eye pain, vision loss, spots in
          your vision, "cobwebs" in your field of view, colored circles around
          lights or a bulging of the eye or eye tissue.
        </p>
        <p>
          Other serious symptoms include{" "}
          <Link
            to="https://care.american-rhinologic.org/epiphora"
            target="_blank"
          >
            excessive tearing
          </Link>{" "}
          and eyelids that are stuck together.
        </p>
        <p>
          Tears in the outer ocular walls, bleeding on or inside the eye and
          vision loss are all warning signs of a potentially serious eye injury.
        </p>
        <p>
          Seeking help right away will not only document your injuries for legal
          purposes and for your eye injury attorney, but also increase your
          chances of making a full recovery.
        </p>
        <h2>Common Eye Injuries</h2>
        <p>
          Eyes are extremely vulnerable organs. With this in mind, just about
          any eye trauma can have serious consequences. Some of the most common
          eye injuries include:
        </p>
        <ul>
          <li>
            <b>Scratched cornea</b>: A scratched eye is susceptible to infection
            from bacteria or fungus.
          </li>
          <li>
            <b>Penetrating injuries</b>: This is when metal or other sharp
            objects enter and damage the eye.
          </li>
          <li>
            <b>Chemical burns</b>: Acids cause redness and burning and alkali
            can cause substantial eye pain.
          </li>
          <li>
            <b>Eye bleeding</b>: This is when there is a leakage of blood,
            making the white of the eye appear red.
          </li>
          <li>
            <b>Traumatic iritis</b>: This is inflammation of the colored part of
            the eye.
          </li>
          <li>
            <b>Eye swelling</b>: Swollen eyelids can result from blunt trauma.
          </li>
          <li>
            <b>Hyphemas</b>: This is when there is bleeding between the cornea
            and the iris.
          </li>
        </ul>
        <h2>Treatments for Traumatic Eye Injuries</h2>
        <p>
          The type of treatment needed depends on the type of injury suffered.
        </p>
        <p>
          Surgery is required in cases involving blunt or penetrating injuries.
          Surgery, however, is not necessary in all circumstances.
        </p>
        <p>
          Some{" "}
          <Link
            to="https://health.clevelandclinic.org/2016/10/8-reasons-swollen-eye-eyelid/"
            target="_blank"
          >
            minor corneal injuries
          </Link>{" "}
          could be treated in a less evasive way.
        </p>
        <p>
          As long as there is no foreign material in the eye, an eye patch may
          be worn to allow the eye to heal on its own. Victims of these types of
          serious injuries should avoid driving or other types of strenuous
          activities because having use of only one eye affects depth
          perception.
        </p>
        <p>
          Some eye injuries do not result from foreign objects but from chemical
          burns.
        </p>
        <p>
          Powerful chemicals such as acids or alkalis are extremely dangerous.
          If you have sustained a chemical burn to your eye, you should
          immediately flush your eyes out with water for 30 minutes and then
          seek immediate medical attention.
        </p>
        <p>
          If your eyes are swelling, you might want to place ice on your eyes as
          you are being transported to the hospital. It is not, however,
          advisable to rub or apply pressure to the eye.
        </p>
        <h2>Rights of Serious Eye Injury Victims</h2>
        <p>
          If you have suffered an eye injury as a result of someone else's
          negligence, it is possible to seek compensation for your losses.
        </p>
        <p>
          In cases involving eye injuries, victims often require eye doctors to
          review their cases or even testify in court as experts. Depending on
          the age of the victim and the circumstances of the incident that
          caused the injury, it may be necessary to speak with a legal expert
          who can offer better insight.
        </p>
        <h2>The Medical Cost of an Eye Injury</h2>
        <p>
          Suffering a serious eye injury can result in expensive medical bills
          and time away from work.
        </p>
        <p>
          Catastrophic eye injuries can result in a partial or complete loss of
          vision. There are legal options available to help injured victims
          recover some of their losses if the accident resulted from someone
          else's negligence. Some questions you should ask yourself include:
        </p>
        <ul>
          <li>Did the eye trauma occur during a car accident?</li>
          <li>
            Was the accident the result of a defective product or a dangerous
            property?
          </li>
        </ul>
        <p>
          Support should be available for anyone who has suffered an injury
          because of the negligence or wrongdoing of another.
        </p>
        <h3>
          Whether it is through a product liability claim, personal injury claim
          or premises liability claim, financial support may be available for:
        </h3>
        <ul>
          <li>
            <strong>Medical bills:</strong> All current and future medical
            expenses related to the accident should be included in the claim.
          </li>
          <li>
            <strong>Lost wages:</strong> All wages lost while the victim heals
            should be included in the claim. Additional support may be available
            when the injuries reduce the victim's earning potential or ability
            to return to work.
          </li>
          <li>
            <strong>Pain and suffering:</strong> Compensation may be available
            for the victim's mental anguish and physical suffering.
          </li>
          <li>
            <strong>Rehabilitation costs:</strong> Victims can also be
            reimbursed for expenses relating to rehabilitation or therapy they
            require to heal completely.
          </li>
        </ul>
        <h2>Eye Injury Lawyers You Can Trust</h2>
        <p>
          Suffering an eye injury can be a painful and emotional experience for
          you and your family.
        </p>
        <p>
          If you have suffered a serious eye injury, call the experienced eye
          injury lawyers at Bisnar Chase toll free now at{" "}
          <strong>800-561-4887</strong> for immediate legal advice and to set up
          your free, no-obligation consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
