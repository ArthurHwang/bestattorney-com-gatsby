// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cal Trans Negligence Attorneys -Bisnar Chase"
        pageDescription="Have your or a loved one recently been injured? The Caltrans negligence lawyers at Bisnar Chase can help you take legal action against Cal Trans and get you the compensation you deserve. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>CalTrans Negligence Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If your loved one was severely injured in a vehicle accident in the
          Golden State, the California Department of Transportation could be at
          fault.
        </p>
        <p>
          If you've suffered a no fault CalTrans injury you may be entitled to a
          recovery. Our experienced trial lawyers have recovered hundreds of
          millions for our clients and may be able to help you too.{" "}
          <strong>Call 800-561-4887 </strong>for a<strong> FREE </strong>
          consultation.<strong> No recovery, no fee.</strong>
        </p>
        <h2>CalTrans Traffic Accidents</h2>
        <p>
          On a daily basis, Cal Trans hears complaints about medians, guardrails
          and CalTrans onramp closures. These Cal Trans made structures can be
          installed incorrectly in rare cases. This cases can result in
          catastrophic injuries and fatalities and CalTrans may be at fault for
          negligence.
        </p>
        <p>
          Bisnar Chase{" "}
          <Link to="/catastrophic-injury">catastrophic injury lawyers</Link>{" "}
          recently represented a Laguna Beach man who was paralyzed after his
          Jeep ran off Interstate 15 in Escondido, Ca., in a spot that,
          according to him, should have had a CalTrans guardrail.
        </p>
        <h2>CalTrans Traffic Accidents</h2>
        <p>
          Cal Trans operates with a multi-billion dollar budget and works with
          the California Highway Patrol to identify places where there are more
          crashes than usual, and CalTrans evaluates those sites to see if any
          changes can make the road safer.
        </p>
        <p>
          The amount of highway outstretches the available manpower of both the
          CHP and Cal Trans combined necessary keep every inch of California
          asphalt safe.
        </p>
        <p>
          By contacting the CalTrans negligence attorneys of Bisnar Chase, you
          are taking the next step to protecting yourself after a vehicle
          injury.
        </p>
        <h2>How to Hold a Government Agency Liable in a Court of Law</h2>
        <p>
          Top rated Avvo{" "}
          <Link
            to="https://www.avvo.com/attorneys/92660-ca-brian-chase-339392.html"
            target="_blank"
          >
            Attorney Brian Chase
          </Link>{" "}
          argued in the man's trial that if there had been a Cal Trans
          guardrail, the plaintiff's Jeep would have bounced around and stopped
          rather than rolling over and falling down a ravine.
        </p>
        <p>
          Not only would this have been safer, Chase said in the trial, but Cal
          Trans would have been in compliance rather than compulsion.
        </p>
        <i>
          "In client cases involving highway accidents, more often than not,
          they arise because Cal Trans didn't follow their own rules
        </i>
        ,<em>"</em>
        Chase explains.
        <i>
          "When CalTrans fails to follow its own rules, our drivers suffer
          injuries.
        </i>
        "<h2>Road Conditions in California</h2>
        <p>
          <em>
            "When we represent your case against Cal Trans, you are making roads
            safer for all drivers, our verdicts force Cal Trans to improve
            highway hazards and prevent accident's,"
          </em>{" "}
          says Attorney John Bisnar.
        </p>
        <p>
          If you are seriously injured from a vehicle crash caused by Cal Trans
          negligence, contacting Bisnar Chase is one of the best decisions you
          can make toward protecting your legal rights against both the road
          conditions in California as well as against CalTrans.
        </p>
        <h2>Critical Injury Attorneys that You Can Trust</h2>
        <p>
          Cal Trans receives billions of taxpayer dollars to keep our roads
          safe. The CalTrans negligence lawyers of Bisnar Chase ensures Cal
          Trans is doing its job by representing clients like you.
        </p>
        <p>
          When Cal Trans fails, you have a legal remedy. Bisnar Chase will make
          sure it's the right remedy for you.
        </p>
        <p>
          Call us today at <strong>800-561-4887</strong> for a free consultation
          and to be informed of your rights.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
