// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { ScholarshipContact } from "../../components/elements/forms/Form_Scholarship"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

function dateCheck() {
  const now = new Date().getTime()
  const decTimestamp = 1576454399
  const junTimestamp = 1560643199
  if (now >= junTimestamp || now <= decTimestamp) {
    return true
  } else {
    true
  }
}

export default function BranchOutPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="The Branch Out Scholarship | Bisnar Chase"
        pageDescription="The Branch Out Scholarship awards $1000 twice a year to students who are committed to expanding their education and being a positive influence in their community"
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The Branch Out Scholarship</h1>
        <BreadCrumbs location={location} />

        <h2 className="header-blue">
          The Branch Out Scholarship: Commitment to excellence in education and
          communities
        </h2>
        <p>
          Bisnar Chase Personal Injury Attorneys is hosting a bi-yearly
          scholarship giving $1,000 to a student who demonstrates through their
          personal statements that they are committed to expanding their
          education and using their education to improve their communities.
          Bisnar Chase will choose a winner Upon announcing the winner, Bisnar
          Chase will also give away $500 to the 501c3 charitable organization of
          the winner's choice.*
        </p>

        <div id="scholarship-row1" className="row">
          <div
            style={{ color: "#85bb65", fontSize: "3rem" }}
            id="scholarship-money"
            className="col-md-4 col-sm-4 border-box text-center"
          >
            $1,000
          </div>
          <div
            id="scholarship-bullets"
            className="col-md-8 col-sm-8 border-box"
          >
            <h3>awarded to a student whose submission details:</h3>
            <ul>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;Why they are committed to education
              </li>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;The struggles that their community goes through
              </li>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;How they plan to solve issues within their community
              </li>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;How their education will help bring that plan to
                life
              </li>
            </ul>
          </div>
        </div>
        <div id="scholarship-row2" className="row">
          <div
            style={{ color: "#85bb65", fontSize: "3rem" }}
            id="scholarship-money2"
            className="col-md-4 col-sm-4 border-box text-center"
          >
            $500
          </div>
          <div
            id="scholarship-bullets2"
            className="col-md-8 col-sm-8 border-box"
          >
            <h3>awarded to a charity:</h3>
            <ul>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;Chosen by the scholarship winner
              </li>
              <li>
                <i className="fa fa-chevron-circle-right" />
                &nbsp;&nbsp;Must be a registered 501c3 Charitable Organization
              </li>
            </ul>
          </div>
        </div>
        <div className="divider-80" />
        <div id="scholarship-row3" className="row">
          <div
            id="scholarship-requirements"
            className="col-md-6 col-sm-6 border-box"
          >
            <h3>
              The following requirements must be met in order for a student to
              be considered to win this scholarship.
            </h3>
            <ul>
              <li>
                Applicants must submit their answers to the questions in the
                form below. Applicants may submit their answers in essay form
                via email instead.
              </li>
              <li>
                Applicant must be a student enrolled in a United States
                university pursuing an undergraduate or graduate degree or must
                be a senior in high school.
              </li>
              <li>
                Student must have at least a 2.5 GPA and winner must provide
                proof of GPA
              </li>
            </ul>
          </div>
          <div
            id="scholarship-deadlines"
            className="col-md-6 col-sm-6 text-center border-box well"
          >
            <h2 className="text-center">
              A winner will be chosen twice a year
            </h2>
            <span style={{ fontSize: "2rem", fontWeight: "600" }}>
              The deadline for the upcoming scholarship is:{" "}
            </span>
            <p
              style={{
                fontSize: "2.5rem",
                color: "red",
                fontWeight: "600",
                textDecoration: "underline"
              }}
            >
              December 15, 2019 at 11:59pm
            </p>
            <p>
              on which date the scholarship winner will be chosen and announced
              via email to the winner.
            </p>
          </div>
        </div>
        <div className="divider-80" />

        {dateCheck() ? (
          <>
            <h2 className="header-blue text-center">
              Tips for Winning the Scholarship
            </h2>
            <p>
              Bisnar Chase will choose a winner who we believe has the best plan
              to improve their community, and the most motivation and drive to
              see their plans come to fruition. Our goal is to benedit the most
              people by empowering the difference-makers around us.
            </p>
            <ul>
              <li>
                <strong>Read the Tips for Each Question:</strong> Each question
                will have a
                <i
                  className="scholarship-question fa fa-question-circle tippy"
                  title="Hover on me to see more text!"
                  aria-hidden="true"
                />{" "}
                Symbol next to it. Hover over it with your mouse or tap on it
                (on your phone) to reveal more specific tips about this
                question.
              </li>
              <li>
                <strong>Stay on Topic:</strong> This scholarship is not awarded
                to the person who completes the most hours of volunteer work.
                Mention your accomplishments or personal anecdotes only when
                relevant.
              </li>
              <li>
                <strong>Keep it Readable and Brief:</strong> The easiest
                community plans to understand are the ones that are quick to
                read and understand. Don't use ostentatious verbiage unless
                necessary. Keep your answers as short as possible without
                sacrificing detail.
              </li>
              <li>
                <strong>Be Specific on 'How':</strong> Be sure to give as much
                detail as possible on how your proposed plans will address your
                issues. (eg. Don't just tell us you want to open a homeless
                shelter for example, tell us what your homeless shelter will do
                to prevent homelessness in the future. How are you planning on
                getting your funding?What will be your most beneficial programs
                and policies?)
              </li>
            </ul>

            <ScholarshipContact />
          </>
        ) : (
          <div id="scholarship-form">
            <h2 className="header-blue text-center">
              Applications for the Branch Out Scholarship have now concluded!
            </h2>
            <p>
              We will announce the winner shortly. After announcing the winner,
              we will re-open submissions for the next scholarship period (~6
              months).
            </p>
          </div>
        )}

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
