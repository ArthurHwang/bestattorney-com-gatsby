// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Second Harvest Food Bank July 2012"
        pageDescription="Bisnar Chase Personal Injury Attorneys sponsors the Second Harvest Food Bank."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Second Harvest Food Bank</h1>
        <BreadCrumbs location={location} />

        <p>
          In conjunction with Second Harvest Food Bank of Orange County, Bisnar
          Chase Personal Injury Attorneys handed out food for the 7th time to
          Orange County families at their Adopt-A-Pantry food bank on July 7,
          2012. The donation started at 1:00 p.m. at the Spanish Pentecostal
          Church located at 211 N. Waverly, Orange, California.
        </p>
        <p>
          Second Harvest Food Bank of Orange County has dispersed more than 270
          million pounds of food since 1983 and is the largest private hunger
          relief organization in the county, partnering with 470 charitable
          organizations to feed an average of 240,000 people each month. For
          every dollar that is donated 94.6 cents goes directly to hunger relief
          programs.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys has made it their bimonthly
          pledge to team up with Second Harvest Food Bank of Orange County to
          donate time and food to Orange County's less fortunate. John Bisnar,
          the law firm's founder comments that "When people have to choose
          between food and gas, food and healthcare or food and heat, they
          usually go to bed hungry. We don't want people to have to choose. I
          remember what that was like when I was a child. We want to put food on
          the tables so that no one, especially children, go hungry."
        </p>
        <p>
          Individuals who would like to help donate time or food at future
          events please contact Danielle Olsen of Bisnar Chase at
          dolson@bisnarchase.com. Businesses who would like to sponsor food
          giveaways are asked to visit the Second Harvest Food Bank website at
          http://www.FeedOC.org to learn how to donate funds, food, and/or time.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
