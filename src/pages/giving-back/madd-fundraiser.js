// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="MADD 2012 Fundraiser Jersey Mike's Subs"
        pageDescription="Bring this flyer with you and the MADD foundation will receive 20% of your pre-tax total."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>MADD Orange County 2012 Rubio's Fundraiser</h1>
        <BreadCrumbs location={location} />

        <p>
          Bring this flyer with you and the MADD foundation will receive 20% of
          your pre-tax total.
        </p>
        <p>
          COME JOIN THE FUN and HELP RAISE SOME MONEY FOR THE MADD Foundation!!!
          Make sure you <Link to="/pdf/mikes-subs.pdf">DOWNLOAD</Link> the flier
          and present it to the cashier.
        </p>
        <p>
          <img
            src="/images/madd-flier.jpg"
            alt="Jersey Mike's Sub Fundraiser"
            className="imgcenter-fluid"
          />
        </p>
        <p>
          {" "}
          <Link to="/pdf/mikes-subs.pdf">Download the flier here</Link>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
