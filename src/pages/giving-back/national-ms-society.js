// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="National Multiple Sclerosis MS Society
DOCUMENT_ROOT"
        pageDescription="In keeping with our belief in supporting worthy causes, Bisnar Chase recently donated to the National Multiple Sclerosis Society."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1> Bisnar Chase Donates to National MS Society</h1>

        <BreadCrumbs location={location} />

        <p align="right">February 15, 2012</p>
        <p>
          In keeping with our belief in supporting worthy causes, Bisnar Chase
          recently donated to the National Multiple Sclerosis Society. The
          National MS Society is a non-profit organization dedicated to helping
          victims of MS and their families and in finding a cure for this
          disease.
        </p>
        <p>
          Founded in 1947 as the Society for the Advancement of Multiple
          Sclerosis Research, the organization has worked for more than sixty
          years to fund research into finding a cure for MS. The National MS
          Society has awarded grants in over 17 countries to such notable
          researchers as Jonas Salk and Rita Levi-Montalcini, both Nobel prize
          winners.{" "}
        </p>
        <p>
          Today, the Society operates under a set of core values that define its
          mission and purpose:
        </p>
        <ul>
          <li>Commitment</li>
          <li>Leadership</li>
          <li>Integrity</li>
          <li>Excellence</li>
          <li>Teamwork</li>
        </ul>
        <p>
          The National Multiple Sclerosis Society is determined to find a cure
          for MS, a disease that often begins in the mid-20s with mild symptoms
          such as blurred vision or muscular weakness and progresses to
          blindness or paralysis. This disease of the central nervous system can
          progress quickly with no warning or stay dormant for long periods of
          time. Those who have MS live day-to-day, hoping that their symptoms
          will not worsen and that they will remain active and functional. New
          drugs promise some hope for victims of MS, but there remains a great
          deal of work to be done in research into the cause and cure for this
          disease.
        </p>
        <p>
          Bisnar Chase supports the efforts of the National Multiple Sclerosis
          Society to find a cure and offer hope to millions who suffer from MS.
          If you would like to make a donation to the National Multiple
          Sclerosis Society, you can give a one-time or recurring monthly gift
          online at the National MS Society's website.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
