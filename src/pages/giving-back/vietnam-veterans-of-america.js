// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Injury Attorneys Makes Contribution to Vietnam Veterans"
        pageDescription="The Vietman Veterans of America is an organization that Bisnar Chase fully supports and promotes issues that are important to Vietnam vets."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Makes Contribution to Vietnam
          Veterans of America
        </h1>

        <BreadCrumbs location={location} />

        <p align="right">May 14, 2012</p>
        <p>
          John Bisnar has always felt a need to help his fellow Vietnam
          Veterans. The war in Vietnam was a difficult battle physically,
          emotionally and mentally for the many veterans who fought so bravely
          for our country. Vietnam veterans continue to face a number of
          challenges in their life and deserve our help.
        </p>
        <p>
          Currently, Bisnar Chase Personal Injury Attorneys has given a generous
          donation to the Vietnam Veterans of America or VVA.{" "}
        </p>
        <h2>About the Vietnam Veterans of America</h2>
        <p>
          An organization exclusively for veterans and their families, the
          Vietnam Veterans of America was founded in 1978 and is currently the
          only national Vietnam veterans organization congressionally chartered.
          The organization is a tax-exempt not-for-profit corporation.
        </p>
        <h2>Goals of the Vietnam Veterans of America</h2>
        <p>
          It is the Vietnam Veterans of America's goal to promote and support
          issues that are important to Vietnam veterans such as creating a new
          identity for this generation and changing the general perception of
          the public and media of Vietnam veterans.
        </p>
        <h2>How You Can Help</h2>
        <p>
          Show your support and{" "}
          <Link to="http://www.vva.org/joinvva.html" target="_blank">
            become a member
          </Link>{" "}
          of Vietnam Veterans of America.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
