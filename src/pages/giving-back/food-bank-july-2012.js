// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Attorneys Team Up With Second Harvest Food Bank"
        pageDescription="Bisnar Chase attorneys help feed hungry California citizens."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Attorneys Team Up With Food Bank</h1>
        <BreadCrumbs location={location} />

        <p>
          Second Harvest Food Bank and the Bisnar Chase personal injury law firm
          based in Newport Beach, CA have teamed up to help promote a "full
          stomach" in areas where struggling citizens could really use the help.
        </p>
        <p>
          On Saturday July 7, 2012, Bisnar Chase attorneys, staff members and
          their children were seen gathered around a grocery-filled Second
          Harvest Food Bank truck in Orange, California which was handing out
          food to those in need. Over the following 3 hours, more than 121
          families and 556 individuals received much-needed bags and boxes of
          food.
        </p>
        <p>
          The contribution provided by Second Harvest Food Bank and Bisnar Chase
          staff members was just one of many such food distributions that they
          have been systematically providing for over a year.
        </p>
        <p>
          According to FeedingAmerica.org, 48.8 million Americans lived in "food
          insecure" homes in 2010. Of those, 32.6 million were adults and 16.2
          million were children. Statistics for the same year show households
          with children reporting a much higher rate of "food insecurity" than
          those without, 20.2 percent compared to 11.7 percent.
        </p>
        <p>
          John Bisnar, managing partner of the Bisnar Chase Law Firm, who has
          assisted more than 6,000 injury victims with their personal injury
          claims, feels that the firm's contributions to public service programs
          is equally important to the community.
        </p>
        <p>
          "California accident victims need someone to stick up for them, so
          they can receive compensation for their medical expenses, lost income,
          and suffering. The Orange County families that are struggling to keep
          adequate food on the table need someone to stick up for them. We have
          taken it upon ourselves to stick up for both groups. It is
          heartwarming to see the glow that is shared by the people receiving
          the food, our employees and their children who help hand it out. It is
          very inspiring." Says Mr. Bisnar.
        </p>
        <h2>Summer Service Ideas Provided By Bisnar Chase Law Firm</h2>
        <p>
          According to the U.S. Bureau of Labor Statistics, more than 60 million
          Americans volunteered a median of 52 hours in 2007. There are several
          organizations put in place to help you help others, and they would
          love to have you.
        </p>
        <p>
          The following are ways that you can help out-no matter what state you
          are in.
        </p>
        <ul>
          <li>
            Nature.Org
            <ul>
              <li>
                The nature conservancy has locations throughout the United
                States. We face several environment concerns. Doing your part to
                be one of nature's allies is something that we will all benefit
                from.
              </li>
            </ul>
          </li>
          <li>
            Habitat.Org
            <ul>
              <li>
                At Habitat for Humanity, volunteers are dedicated to providing
                every man, woman, and child with a safe and affordable place to
                live. If you are looking to make an immediate impact toward
                people who are truly in need, Habitat.Org will get you started.
              </li>
            </ul>
          </li>
          <li>
            AnimalRescue.Org
            <ul>
              <li>
                Have a soft spot for helpless animals? The Animal Rescue League
                can help you help them in no time.
              </li>
            </ul>
          </li>
          <li>
            BBBS.Org
            <ul>
              <li>
                Role models are in short supply. If you would like to make a big
                impact, contact Big Brothers Big Sisters to make a big
                difference toward today's youth.
              </li>
            </ul>
          </li>
        </ul>
        <h2>
          About: Bisnar Chase,{" "}
          <Link to="/">Personal Injury Attorney Orange County</Link>
        </h2>
        <p>
          Bisnar Chase is a California based personal injury law firm that has
          developed a reputation as trustworthy professionals who produce proven
          results while serving their community. The firm has been featured on
          several popular media outlets, including Fox, NBC, and ABC. They have
          recovered several multimillion dollar settlements and verdicts.
        </p>
        <p>
          Bisnar Chase lawyers assist victims in California and provide all of
          their clients with a "No Win No Fee Guarantee".
        </p>
        <p>
          For a free consultation call 949-203-3814 or visit their website at /.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
