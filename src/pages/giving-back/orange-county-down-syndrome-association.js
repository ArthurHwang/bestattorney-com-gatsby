// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="The Down Syndrome Association of Orange County - Bisnar Chase Donation"
        pageDescription="Bisnar Chase is proudly supporting the Down Syndrome Association of Orange County by donating to the Buddy Walk, which will be held at Angel Stadium of Anaheim.  The walk will help raise money for the more than 350,000 individuals who have Down syndrome nationwide."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          The Down Syndrome Association of Orange County Needs Your Support
        </h1>

        <BreadCrumbs location={location} />

        <p>
          Bisnar Chase is proudly supporting the Down Syndrome Association of
          Orange County by donating to the Buddy Walk, which will be held at
          Angel Stadium of Anaheim. The walk will help raise money for the more
          than 350,000 individuals who have Down syndrome nationwide.
        </p>
        <p>
          Through the efforts of local business owners, corporations and people
          like you, over $9.5 million dollars was raised nation-wide last year.
          We hope that our combined support and awareness for the cause will
          help the foundation exceed that number this year.
        </p>
        <p>
          As a firm that helps people, we want to do our part to ensure that
          each individual has the opportunity to reach their full potential. The
          Down Syndrome Association of Orange County needs your support too. You
          can help by making a contribution, which will enhance the quality of
          life for those who have Down syndrome.
        </p>
        <p>
          Money raised will aid local programs for people who have Down syndrome
          and their families, provide education to local communities, help
          support research and more.
        </p>
        <p>
          To make your tax-deductible donation please{" "}
          <Link to="http://buddywalk.kintera.org/faf/r.asp?t=4&i=1032653&u=1032653-300492219&e=5966242866">
            click here
          </Link>
          .
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
