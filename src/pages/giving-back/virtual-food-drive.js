// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange County Virtual Food Drive"
        pageDescription="Virtual food drives are the new, convenient way to feed the hungry in Orange County. Want to help? Start here."
      />
      <ContentWrapper>
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Virtual Food Drive</h1>
        <BreadCrumbs location={location} />
        <h3>
          <font color="D26D13">
            Do you want to do something good for your community?
          </font>
        </h3>
        <img
          src="/images/charity/Second_Harvest_FoodBank_logo.png"
          className="imgright-fixed"
          alt="second-harvest-logo"
        />
        <p>
          Virtual food drives are the new, convenient way to feed the hungry in
          Orange County. Want to help? Start here. Giving back to the community
          is and always has been very important to the Bisnar Chase family. We
          have been a partner with{" "}
          <Link to="http://feedoc.org/" target="_blank">
            Second Harvest Food Bank
          </Link>{" "}
          to help feed thousands of hungry people through Adopt-A-Pantry.{" "}
        </p>
        <p>
          Now we are teaming up with them for virtual food drives. We need your
          help too. Hunger does not discriminate. Did you know that 1 in 4
          children will go hungry tonight?
        </p>
        <h3>
          <font color="D26D13">
            Many families in Orange County are in need of good nutritious food.
          </font>
        </h3>
        <p>
          Virtual food drives are fun and giving is easy. Go to the{" "}
          <Link
            to="http://vad.aidmatrix.org/vadxml.cfm?driveid=709"
            target="_blank"
          >
            interactive grocery store
          </Link>{" "}
          and start filling your basket.{" "}
        </p>
        <p>
          Every dollar that you donate will provide 3 meals to hungry families
          in Orange County. That's phenomenal!{" "}
        </p>
        <ul>
          <li>
            Simply choose the food and the monetary amount you want to
            contribute.
          </li>
          <li>
            Once you reach that amount simply Check Out with a credit card.{" "}
          </li>
          <li>
            Your food purchase will be distributed and you will receive a
            receipt for your tax deductible donation.{" "}
          </li>
        </ul>
        <p>
          This is the easiest and most convenient way to hold a food drive. It
          is cost effective and convenient.
        </p>
        <h3>
          <font color="D26D13">How can you help?</font>
        </h3>
        <iframe
          width="100%"
          height="500"
          src="https://www.youtube.com/embed/CCPpdfJSWfA?rel=0"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
        />
        <p>
          Your team can spread the word via email or through their favorite
          social media networks,{" "}
          <strong>just click on the share heart below</strong>, and they can
          purchase food from the comfort of their homes or at work.
        </p>
        <div id="shareThisShareHeart" className="shareEgg"></div>
        <p>
          This is a{" "}
          <strong>
            <font color=" DB1723">fantastic way to donate</font>
          </strong>{" "}
          a whole lot of good nutritious food to a whole lot of hungry people!
        </p>{" "}
        <Link
          to="http://vad.aidmatrix.org/vadxml.cfm?driveid=709"
          target="_blank"
        >
          <img
            src="/images/charity/virtual-food-drive.jpg"
            className="imgcenter-fluid"
            alt="food-drive-logo"
          />
        </Link>
        <p>
          <center>
            <strong>Bisnar Chase Personal Injury Attorneys</strong>
          </center>
        </p>
        <p>
          <center>
            <strong>Passion - Trust - Results</strong>
          </center>
        </p>
        <p>
          <center>
            <strong>Since 1978</strong>
          </center>
        </p>
        <p>
          <center>
            <strong>1301 Dove Street, Newport Beach, CA 92660</strong>
          </center>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
