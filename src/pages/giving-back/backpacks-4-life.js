// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { BackpacksContact } from "../../components/elements/forms/Form_Backpacks4Life"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

import { FaInstagram, FaTwitter } from "react-icons/fa"
import img6 from "../../images/backpacks4life-supplies.jpg"
import img1 from "../../images/backpacks4life.jpg"
import BackPacks4LifeImg from "../../images/charity/backpacks4life.jpg"
import img7 from "../../images/text-header-images/javi-ryan-backpacks4life.jpg"
import img2 from "../../images/text-header-images/packing-bc.webp"
import img9 from "../../images/text-header-images/raiders-hat-kid-bc.jpg"
import img5 from "../../images/text-header-images/raining-bc.webp"
import img3 from "../../images/text-header-images/razors-chap-bc.jpg"
import img8 from "../../images/text-header-images/three-kids-bc.jpg"
import img4 from "../../images/text-header-images/van-loaded-bc.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function Backpacks4LifePage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase provides backpacks to the underprivileged"
        pageDescription="Backpacks4Life is a program funded by Bisnar Chase to provide backpacks with personal supplies for the homeless throughout Orange County, Calif."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Backpacks4Life: Providing filled backpacks to the underprivileged
        </h1>
        <BreadCrumbs location={location} />
        <span className="hands-logo">
          {" "}
          <Link
            to="https://www.yelp.com/biz_photos/bisnar-chase-personal-injury-attorneys-newport-beach-4?select=RBT4GRnL3-DP1zHFMBcCAg"
            target="_blank"
          >
            <img
              src={BackPacks4LifeImg}
              style={{ verticalAlign: "middle" }}
              alt="backpacks4life by Bisnar Chase"
              width="200"
              className="imgleft-fixed"
            />
          </Link>
        </span>

        <p>
          <strong>Backpacks4Life</strong> is an outreach program created by
          Edison High School student Mira Kirilova and Bisnar Chase Personal
          Injury Attorneys of Newport Beach. The program has been created to
          provide necessities to those in need throughout Orange County &amp;
          Los Angeles.
        </p>

        <p>
          We have helped thousands of homeless and underprivileged in the
          Souther California region and our program has become popular among
          local homeless shelters, charities and churches.
        </p>

        <p>
          {" "}
          <Link to="/">Bisnar Chase</Link> provides the backpacks and supplies.
          The firm also reaches out to various businesses in the Orange County
          area for donations.
        </p>
        <h2>Bisnar Chase's Contribution as of 2019</h2>
        <p>
          As of 2019, Bisnar Chase have put together and donated over 12,000
          backpacks. Each backpack is hand packed by the staff, attorneys,
          friends and family, filling each backpack with items like
          toothbrushes, toothpaste, floss, mouth wash, hand sanitizer, shampoo,
          conditioner, body wash, blankets, flashlights and other necessities.
        </p>
        <p>
          If you or someone you know is in need of backpacks for an upcoming
          charity event please contact us below.
        </p>
        <h2>Donations of Supplies/Volunteers Needed</h2>

        <p>
          If you know someone who would like to get involved please contact us
          for more information: <Link to="tel:9492033814">949-203-3814</Link>.
          We are actively seeking donations for volunteer time or backpack
          supplies. Contact Danielle or Kristi if you would like to help.
        </p>

        <div>
          <h2 style={{ textAlign: "center" }}>
            Follow us on Instagram and Twitter:
          </h2>
          <p
            style={{
              textAlign: "center",
              display: "flex",
              justifyContent: "space-evenly"
            }}
          >
            {" "}
            <Link to="https://www.instagram.com/bckpacks4life" target="_blank">
              <FaInstagram style={{ fontSize: "5rem" }} />
            </Link>{" "}
            <Link to="https://www.twitter.com/bckpacks4life" target="_blank">
              <FaTwitter style={{ fontSize: "5rem" }} />
            </Link>
          </p>
        </div>

        <BackpacksContact />
        <br />

        <div className="grid-image-wrapper">
          <LazyLoad>
            <img
              src={img1}
              alt="backpacks4life program sponsored by Bisnar Chase"
            />
          </LazyLoad>

          <LazyLoad>
            <img src={img2} alt="packing-bc.JPG" />
          </LazyLoad>
          <LazyLoad>
            <img src={img3} alt="razors-chap-bc.jpg" />
          </LazyLoad>
          <LazyLoad>
            <img src={img4} alt="van-loaded-bc.jpg" />
          </LazyLoad>
          <LazyLoad>
            <img src={img5} alt="packing-bc.webp" />
          </LazyLoad>
          <LazyLoad>
            <img src={img6} alt="Backpacks4life.com" />
          </LazyLoad>
          <LazyLoad>
            <img src={img7} alt="javi-ryan-backpacks4life" />
          </LazyLoad>
          <LazyLoad>
            <img src={img8} alt="three-kids-bc.jpg" />
          </LazyLoad>
          <LazyLoad>
            <img src={img9} alt="raiders-hat-kid-bc.jpg" />
          </LazyLoad>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .grid-image-wrapper {
    display: grid;
    grid-gap: 1rem;
    grid-template-columns: 1fr 1fr 1fr;
    margin-bottom: 2rem;

    img {
      margin-bottom: 0;
      object-fit: cover;
      width: 100%;
      height: 250px;
    }

    @media (max-width: 700px) {
      grid-template-columns: 1fr 1fr;
    }
    @media (max-width: 500px) {
      grid-template-columns: 1fr;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
