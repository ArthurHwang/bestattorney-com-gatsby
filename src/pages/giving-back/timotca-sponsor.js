// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="TIMOTCA Sponsor 2012"
        pageDescription="For those who are unfamiliar with TIMOTCA, the non-profit has used their educational and enlightenment programs to promote cultural understanding, peace and nonviolence though art."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>BISNAR CHASE Personal Injury Attorneys Support TIMOTCA</h1>

        <BreadCrumbs location={location} />

        <p align="right">June 12, 2012</p>
        <p>
          Bisnar Chase has proudly donated to the TIMOTCA. For those who are
          unfamiliar with TIMOTCA, the non-profit has used their educational and
          enlightenment programs to promote cultural understanding, peace and
          nonviolence though art. 100's of schools have benefited from the
          program's Art Beyond Borders traveling exhibit. The exhibit is a
          educational piece that gives students and adults a better
          understanding of how art can be used to promote cultural awareness,
          understanding and non-violence.
        </p>
        <p>
          This year, the group is determined to build a peace monument and this
          new program is the beginning to raise the funds. The new program
          "Citizen of the World" is designed to raise funds which will support
          the Art beyond Borders traveling exhibit as well as raise money for
          promotional items as well as their new Citizen of the World Peace
          Monument.
        </p>
        <p>
          TIMOTCA has a goal of raising $15,000. You can help them to exceed
          this goal by making a donation of any amount on their website
          www.timotca.org or by sending a check payable to TIMOTCA to:
        </p>
        <p>
          TIMOTCA, BOX 427
          <br />
          Laguna Beach, CA 92652
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
