// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="MADD 2012 Fundraiser at Rubio's in Costa Mesa, CA"
        pageDescription="Dine at Rubio's, take food to go, or pick up a Rubio's a-Go-Go catering order and Rubio's will donate 20% to MADD."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>MADD Orange County 2012 Rubio's Fundraiser</h1>
        <BreadCrumbs location={location} />

        <p>
          Dine at Rubio's, take food to go, or pick up a Rubio's a-Go-Go
          catering order and Rubio's will donate 20% to MADD. Make sure you{" "}
          <Link to="/pdf/RubiosFundraiser_16726_2012-05-02.pdf">DOWNLOAD</Link>{" "}
          the flier and present it to the cashier.
        </p>
        <img
          src="/images/RubiosFundraiser.jpg"
          alt="rubios fundraiser"
          className="imgcenter-fluid"
        />
        <p>
          {" "}
          <Link to="/pdf/RubiosFundraiser_16726_2012-05-02.pdf">
            Download the flier here
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
