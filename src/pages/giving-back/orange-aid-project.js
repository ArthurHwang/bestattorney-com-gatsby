// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Orange Aid Project 2012"
        pageDescription="In keeping with our belief in supporting worthy causes, Bisnar Chase recently donated to the Orange Aid Project."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Makes Donation to Share our Selves 19th Annual Wild &
          Crazy Taco Night Benefiting the Orange Aid Project
        </h1>

        <BreadCrumbs location={location} />

        <p align="right">February 23, 2012</p>
        <p>
          Share Our Selves 19th Annual Wild & Crazy Taco Night for Orange Aid
          was held on February 23, 2012, and raised over $60,000 to help feed
          Orange County residents. Bisnar Chase is proud to have been a part of
          this fantastic event.
        </p>
        <p>
          The Annual Wild & Crazy Taco Night Benefitting the Orange Aid Project
          has been held every year for the past 19 years, and offers OC
          residents the chance to enjoy food, music, and fun while donating
          money to a worthwhile cause. A star-studded array of 26 great chefs
          served food to over 700 guests, and also donated a door prize of over
          26 gift cards to "Dine Around OC" at participating restaurants. Guests
          had a great time mingling between the bar and the spicy tacos served
          by these master chefs.
        </p>
        <p>
          The Orange Aid project collects food from restaurants, bakeries,
          grocery stores, and catering services for distribution to low-income
          families. Orange Aid gives out about 275 bags of groceries every day
          to Orange County's hungry.
        </p>
        <p>
          Share Our Selves, the parent organization that sponsored this event,
          is the largest comprehensive organization in Orange County providing
          for the needs of those in poverty. Volunteers work many hours every
          week to provide medical and dental care, legal aid, holiday programs,
          and other important services. Share Our Selves supports Orange Aid in
          its endeavors to help feed hungry people in Orange County.
        </p>
        <p>
          Bisnar Chase is proud to be able to help this worthwhile cause and
          play a small part in the alleviation of the suffering of Orange
          County's families in poverty. If you would like to make a donation to
          Orange Aid or Share Our Selves, contact them at
          www.shareourselves.org.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
