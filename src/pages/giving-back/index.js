// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { DefaultBlueButton } from "../../components/utilities/"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

import CommunityHero from "../../images/community-hero-teacher.jpg"
import LakersWinner from "../../images/IMG_1833.jpg"
import SecondHarvestFood from "../../images/second-harvest-food-bank-oc.jpg"
import SecondHarvest from "../../images/second-harvest.png"
import Veterans from "../../images/VLI_Suporter_Logo_Version_2.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function GivingBackPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Giving Back - Bisnar Chase Community Activities"
        pageDescription="Bisnar Chase is deeply involved in community activities and supports local groups in Orange County."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Giving Back</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            <em>
              "Ask not what your country can do for you - ask what you can do
              for your country."
            </em>{" "}
            John F. Kennedy, Inaugural Address 1961
          </p>
          <p>
            In response to those words and that challenge,{" "}
            <Link to="/">Bisnar Chase</Link> has chosen to devote a portion of
            our attorneys fees and resources to support organizations that feed
            the hungry, search for cures, provide services for children and work
            to make our world a safer place. We encourage you to, "&hellip;ask
            what you can do&hellip;" and chip in what you can.
          </p>
          <p>
            As Mahatma Gandhi once said, "You must be the change you wish to see
            in the world."
          </p>
          <p>
            See All Photos:{" "}
            <Link
              to="https://www.pinterest.com/bisnarchase/bisnar-chase-community-outreach/"
              target="_blank"
            >
              Bisnar Chase Events and charities
            </Link>
            .
          </p>
        </div>
        {/* <h2>
          <strong>The Bisnar Chase Branch Out Scholarship: </strong>
          <br />
          <strong>Commitment to Excellence in Education and Communities</strong>
        </h2> */}
        <h2 style={{ lineHeight: "1" }}>
          The Bisnar Chase Branch Out Scholarship:
          <br />
          Commitment to Excellence in Education and Communities
        </h2>
        <p>
          Bisnar Chase is offering a $1,000 scholarship to students in the 11th
          grade through college with a GPA of 2.5 or higher. The scholarship is
          based on providing a 500-word personal statement, which addresses why
          they are committed to education; the struggles faced by their
          respective communities and how they believe they can help. In
          addition, Bisnar Chase will give $500 to a non-profit charity of the
          winner's choice. The deadline for this scholarship is every June and
          December, ongoing.
          <br />
          <DefaultBlueButton>
            {" "}
            <Link
              className="apply-btn"
              style={{
                fontSize: "1.6rem",
                textTransform: "uppercase",
                fontWeight: "600"
              }}
              to="/giving-back/branch-out-scholarship"
              target="new"
            >
              CLICK HERE FOR THE Branch Out Scholarship APPLICATION
            </Link>
          </DefaultBlueButton>
          .
        </p>
        <h2>Backpacks4Life: Supplies for Those in Need</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys is proud of the backpack
          program we've created to help those in need in our community. The
          program was created in 2014 to help supply people in our local cities
          with personal supplies. The backpacks are each filled with a blanket,
          socks, water bottle, deodorant and other personal care items. Each
          month we visit Mary's Kitchen located in Orange, California and hand
          out the backpacks to the homeless. In addition we have supplied 4
          Sisters in Movement and Toya on the Move in East Los Angeles for their
          donation drives. To date we have filled and handed out over 12,000
          backpacks.
          <br />
          <DefaultBlueButton>
            {" "}
            <Link
              className="apply-btn"
              style={{
                fontSize: "1.6rem",
                textTransform: "uppercase",
                fontWeight: "600"
              }}
              to="/giving-back/backpacks-4-life"
            >
              CLICK HERE FOR THE BackPacks4Life.com APPLICATION
            </Link>
          </DefaultBlueButton>
        </p>
        <h2>One Tough Cookie Scholarship</h2>
        <p>
          The One Tough Cookie Scholarship allows enrolled students to share
          their stories about how they have overcome their most difficult
          obstacles to get to where they are today, or to share their most
          inspirational story about their life. Students will be given a link
          where they can vote for their stories and encourage others to vote for
          their stories as well. The story with the most votes by September 1st,
          2016, will win the scholarship award. The scholarship is available
          nationwide and can do a lot to help a struggling student help cover
          expenses.
          {/* You can learn more about the{" "} */}
          {/* {" "}<Link to="about-us/giving-back/one-tough-cookie">
            One Tough Cookie Scholarship program here
          </Link> */}
          .
        </p>
        <h2>
          KnotsofLove.org
          <br />
        </h2>
        <p>
          Bisnar Chase Personal Injury Attorneys has partnered with the
          organization to create beanies and blankets for babies in the hospital
          and for disabled veterans. Knots of Love has collected over 400K items
          for donations. Most of the beanies go to those going through chemo.
          Our own staff members spend countless hours knitting blankets, beanies
          and scarfs to spread joy throughout hospitals and non-profits and
          hopefully make someone's day a little brighter.{" "}
        </p>
        <h2>
          ClothingtheHomeless.org
          <br />
        </h2>
        <p>
          Bisnar Chase Personal Injury Attorneys collects donations for this
          local charity and the items include backpacks, clothing, jewelry and
          personal care items. Mitch, the director of the program works
          tirelessly to make sure the underprivileged receive items.
          ClothingtheHomeless has events each month in Orange County to allow
          people to come and "shop" for typically a few dollars and often times
          free of charge. We regularly gather men's and women's clothing and
          provide it to Mitch to help his cause in our wonderful community.{" "}
        </p>
        <h2>Haven's Heart</h2>
        <p>
          Haven Penman was struck and killed by a drunk driver just eight days
          after her 13th birthday. In Haven's honor, Bisnar Chase Personal
          Injury Attorneys joined in to raise donations and supplies for the
          Path of Life Ministries Homeless Shelter.{" "}
          <Link
            to="https://www.pinterest.com/bisnarchase/bisnar-chase-community-outreach/"
            target="_blank"
          >
            {" "}
            See photos here!
          </Link>
        </p>
        <h2>Thanksgiving Donation</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys donated 1,000 turkeys at two
          locations for Thanksgiving. The distributions took place at St.
          Norbert's church in Orange and Anaheim Vineyard in Anaheim. 500
          turkeys were distributed at each location to those in need. The entire
          staff of Bisnar Chase came out to help.{" "}
          <Link
            to="https://www.pinterest.com/bisnarchase/bisnar-chase-community-outreach/"
            target="_blank"
          >
            See photos.
          </Link>
        </p>
        <h2>
          {" "}
          <Link id="feedoc" />
          Adopt a Pantry
        </h2>
        <p>
          We had a great time at the Rock Church Adopt a Pantry drive.{" "}
          <Link
            to="/blog/wp-content/uploads/2013/01/adopt-pantry-2013-2.jpg"
            target="_blank"
          >
            Turn out
          </Link>{" "}
          for the drive was very successful. Teaming with Second Harvest Food
          Bank, the drive which took place in Orange, delivered 9000 pounds of
          food to people of need. Since 1983 Second Harvest has distributed 257
          million pounds of food through their hunger relief programs. Some of
          these programs are adopt a pantry, feed the children, food shelters
          and seniors' programs. To learn more or see how you can help, please
          visit their{" "}
          <Link
            to="https://feedoc.org/"
            target="_blank"
            name="Feed Orange County"
          >
            website
          </Link>
          .
        </p>
        <h2 className="giving-back">
          {" "}
          <Link id="specialneeds" />
          Special Needs Contest Winner
        </h2>
        <img
          src={LakersWinner}
          alt="Lakers ticket winner for Bisnar Chase special needs program"
          className="imgright-fixed"
          width="200"
        />
        <em>Sara picks up her Lakers tickets at our Newport Beach office</em>.
        <p>
          And, we have our Lakers tickets winner! Sara is a HUGE Lakers fan, as
          is her mother and today she was beaming as she and a volunteer from{" "}
          <em>
            {" "}
            <Link to="https://www.abilityfirst.org/" target="_blank">
              AbilityFirst
            </Link>
          </em>{" "}
          picked up her tickets. Sonia, who works for AbilityFirst saw the
          drawing on Facebook and entered the contest on behalf of her
          organization, and Sara won in our random drawing. It brings us great
          joy to see Sara enjoy attending a LIVE Lakers game, and by Sonia
          entering, she certainly made Sara's day! Sara will be going to the
          game at the Staples Center with her mother.
        </p>
        <p>
          The competition to enter the drawing was open to anyone with
          connection to a special needs child who liked and commented on our
          Lakers post. Be sure to follow our{" "}
          <Link
            to="https://www.facebook.com/california.attorney"
            name="Bisnar Chase Facebook"
          >
            Facebook page
          </Link>{" "}
          so you can enter our giveaways and drawings!
        </p>
        <h2 className="giving-back">
          {" "}
          <Link id="academic" />
          Rewarding Academic Success
        </h2>
        <p>
          <img
            src={CommunityHero}
            alt="awarding academic excellence in Orange County"
            className="imgright-fixed"
          />
          Bisnar Chase has created an academic reward for local students. The
          prize will be two Lakers tickets awarded to a local student with
          straight A's on their most recent report card.
        </p>
        <p>
          The idea behind this donation is to bring to light and reward students
          who work hard. Bisnar Chase also provides periodic Lakers ticket
          awards to special needs children within our community. The drawings
          are announced on our{" "}
          <Link
            to="https://www.facebook.com/california.attorney"
            target="_blank"
          >
            Facebook
          </Link>
          ,{" "}
          <Link to="https://twitter.com/bisnarchase" target="_blank">
            Twitter
          </Link>{" "}
          and{" "}
          <Link
            to="https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
            target="_blank"
          >
            Google
          </Link>{" "}
          social channels. Be sure to follow us to be notified when the drawings
          take place.
        </p>
        <h2 className="giving-back">
          {" "}
          <Link id="adoptfamily" />
          Holiday Adopt A Family
        </h2>
        <LazyLoad>
          <iframe
            src="https://www.youtube.com/embed/Hh7gP_1lvtA?rel=0"
            width="100%"
            height="500"
            frameBorder="0"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          For the seventh straight year, Bisnar Chase participated in Share Our
          Selves' Adopt A Family program. This year, our employees adopted an
          Orange County family of 10. The firm has collected at least 40 gifts
          for this family including gift cards. Usually, Bisnar Chase adopts one
          large family or two medium-sized families and ensures that each family
          member has a minimum of three gifts to open. This year, the program,
          which is in its 42nd year, will serve more than 1,100 families with
          children recommended by Costa Mesa and Santa Ana schools, according to
          the organization's website.
        </p>
        <p>
          Our office held a gift wrapping party where we enjoyed pizza and
          wrapped over 40 presents for our adopted family. We've included the
          video here and if you'd like to see some of the pictures be sure to
          head over to our{" "}
          <Link to="https://pinterest.com/bisnarchase/bisnar-chase-community-outreach/">
            Community Outreach board on Pinterest
          </Link>
          . Don't forget to follow our pin boards while you're there! Happy
          Holidays!
        </p>
        <h2>Meals on Wheels</h2>
        <p>
          Bisnar Chase proudly supports the Riverside Meals on Wheels program.
          With annual donations we are able to help them keep their community
          program alive and well to assist thousands of seniors in need.{" "}
        </p>
        <p>
          To learn more, please visit:{" "}
          <Link to="https://www.riversidemow.org/">
            https://www.riversidemow.org/
          </Link>
        </p>
        <h2>We Support Veterans</h2>{" "}
        <Link to="https://www.vetslegal.com">
          <img
            src={Veterans}
            className="imgright-fixed"
            alt="FeedOc.Org - Second Harvest Food Bank"
          />
        </Link>
        <p>
          Bisnar Chase is proud to sponsor Veterans Legal Institute. Veterans
          Legal Institute® (VLI) provides pro bono legal assistance to homeless,
          at risk, disabled and low income current and former service members to
          eradicate barriers to housing, healthcare, education, and employment
          and foster self-sufficiency.
        </p>
        <p>
          Core Values Outreach: VLI brings together military members, veterans,
          and the concerned public at large through volunteerism, education,
          outreach, and policy advocacy.
        </p>
        <p>
          Compassion: VLI is committed to showing compassion and understanding
          regarding the unique set of military specific issues associated with
          traumatic combat deployments
        </p>
        <p>
          Quality: VLI is dedicated to providing high quality legal services and
          advocacy.
        </p>
        <p>
          Leadership: VLI raises the standard in the legal industry by providing
          legal services at the upmost levels of professionalism and excellence
          for the benefit of its clients and the overall veteran community.
        </p>
        <p>
          Empowerment: VLI empowers its clients through direct legal services.
        </p>
        <h2 className="giving-back">
          {" "}
          <Link id="fillplates" />
          Fill Plates This Holiday
        </h2>
        <p>
          <img
            src={SecondHarvest}
            alt="Second Harvest Food Bank"
            className="imgright-fixed"
          />
          Thousands of families in Orange County are struggling with hunger and
          rely on families like yours for help and hope.
        </p>
        <p>
          Your gift goes a long way to feed the hungry. Together, we can provide
          the gift of food and hope to hard working families that might
          otherwise go without.
        </p>
        <p>
          Let's make sure that no family sits down to an empty plate this
          holiday. Our staff at Bisnar Chase donated towards hundreds of meals
          for December. Find out how you can help too by visiting Second Harvest
          Food Bank of Orange County.
        </p>
        <h2 className="giving-back">
          Turkey Mania! Bisnar Chase Donates over 1,000 Turkeys to OC Families
        </h2>
        <p>
          {" "}
          <Link to="https://www.feedoc.org">
            <img
              src={SecondHarvestFood}
              className="imgright-fixed"
              alt="FeedOc.Org - Second Harvest Food Bank"
            />
          </Link>
          Bisnar Chase proudly supports the Adopt a Pantry program through
          Second Harvest Food Bank of Orange County. In November we passed out
          over 1,000 turkeys along with miscellaneous vegetables to families in
          Orange County as well as to deserving families from March Air Reserve
          Base.
        </p>
        <p>
          The food donation program was spread over three separate days at three
          locations and families from all over Orange County came out. The
          locations of the donations were Connected Blessings in Anaheim, St.
          Norbert Catholic Church in Orange and March Air Reserve Base in
          Riverside.
        </p>
        <p>
          Second Harvest Food Bank of Orange County has given more than 270
          million pounds of food since 1983 and is the largest private hunger
          relief organization in the county, partnering with 470 charitable
          organizations to feed an average of 240,000 people each month. For
          every dollar that is donated 94.6 cents goes directly to hunger relief
          programs.
        </p>
        <h2>More Headlines of Bisnar Chase Community Involvement</h2>
        <ul>
          <li>
            Bisnar Chase employees donate clothing for job program. The staff
            donated used business clothing to WHW -- an employment program for
            disadvantaged people. The clothing drive brought in a lot of
            clothes, shoes, purses and personal hygiene items. All of the items
            are distributed to those in need and who are looking for work and
            need clothes for job interviews, training etc.
          </li>
          <li>
            Bisnar Chase Donates money to WHW "Suits for a Cause" . This program
            provides business attire to disadvantaged people seeking employment.
            Bisnar Chase donated money as a sponsor.
          </li>
          <li>
            Bisnar Chase Employees Donate $1151.00 to Create "Sweet Cases" for
            Foster Kids.
          </li>
          <li>Bisnar Chase Awards $1,000 Scholarship to Law Student in Need</li>
          <li>
            Child Safety: Bisnar Chase Personal Injury Attorneys Sponsors 800
            Safety Kits for Huntington Beach School Children
          </li>
          <li>Second Harvest Food Bank</li>
          <li>
            Bisnar Chase Partners with Share Our Selves During the Holidays to
            Support Orange County Families in Need
          </li>
          <li>Walk to End Alzheimer's Event</li>
          <li>Bisnar Chase Legal Scholarship</li>
          <li>
            Feeding Orange County's Less Fortunate - Personal Injury Attorney's
            With Heart
          </li>
          <li>Attorneys Team Up With Food Bank</li>
          <li>
            Bisnar Chase Personal Injury Attorneys Organize Charity of Free
            Lakers Tickets to Special Needs Kids
          </li>
          <li>MADD Orange County 2012 Rubio's Fund-raiser</li>
          <li>Jersey Mike's Sub Fund raiser</li>
          <li>
            Bisnar Chase Personal Injury Attorneys Helps MADD Raise $1,500 at
            Roller Derby
          </li>
          <li>
            Bisnar Chase Makes A Donation To Walk Like MADD On Behalf Of
            Lawrence Buckfire
          </li>
          <li>Bisnar Chase Donates To National MS Society</li>
          <li>
            A Thousand Free Turkeys, Food Items To Be Donated In November To
            Area's Needy
          </li>
          <li>
            Bisnar Chase Makes Donation To Share Our Selves 19th Annual Wild
            &amp; Crazy Taco Night Benefiting The Orange Aid Project
          </li>
          <li>
            The Down Syndrome Association Of Orange County Needs Your Support
          </li>
          <li>People Over Profits</li>
          <li>
            Bisnar Chase, Second Harvest Food Bank And The Raise Foundation
            Donate 9,600 Pounds Of Food To Orange County's Needy
          </li>
          <li>Supporting Talk About Curing Autism (TACA)</li>
          <li>Bisnar Chase Personal Injury Attorneys Support TIMOTCA</li>
          <li>
            Bisnar Chase Personal Injury Attorneys Makes Contribution To Vietnam
            Veterans Of America
          </li>
          <li>Virtual Food Drive</li>
          <li>Bisnar Chase Personal Injury Attorneys Walked Like MADD</li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  button {
    /* height: 4rem; */
    margin: 2rem 0 1rem;
    padding: 1rem;
  }
  .apply-btn {
    color: ${({ theme }) => theme.links.hoverBlue};

    &:hover {
      color: ${({ theme }) => theme.colors.accent};
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
