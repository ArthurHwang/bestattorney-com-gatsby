// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="MADD Roller Derby Sponsored by Bisnar Chase"
        pageDescription="Bisnar Chase Personal Injury Attorneys is a long time sponsor of MADD. They continue to help spread the awareness of the dangers of drunk driving."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Helps MADD Raise $1,500 at
          Roller Derby
        </h1>

        <BreadCrumbs location={location} />

        <p>
          Danielle Olson from{" "}
          <Link to="/">Bisnar Chase Personal Injury Attorneys</Link> was able to
          help MADD with their sponsor's Bisnar Chase & Consor pack the house at
          The Rinks in Huntington Beach. Fans rushed through the doors for the
          event, which featured two lively teams of roller derby girls, live
          music by the band{" "}
          <Link to="http://www.almostdawn.com">Almost Dawn</Link>, delicious
          food and a raffle.
        </p>
        <p>
          The two hour event was able to raise $1,500 and 100% of the proceeds
          went to Mothers Against Drunk Driving to help end the pandemic of
          drunk driving.
        </p>
        <h2>About MADD</h2>
        <p>
          Mothers Against Drunk Driving is one of the nations most respected
          non-profit organizations. Since 1980, MADD's mission as stated on
          their website has been The mission of Mothers Against Drunk Driving is
          to stop drunk driving, support the victims of this violent crime and
          prevent underage drinking."
        </p>
        <p>
          The organization was founded by a mother who lost her daughter to a
          drunk driver. Since the founding of the organization, MADD has worked
          to prevent drunk driving in all their works.
        </p>
        <h2>How You Can Help</h2>
        <p>
          To date MADD's endless work has saved about 300,000 lives. You can
          help MADD make a difference in your community too. There are two ways
          you can give back. One is by making a donation of any site to MADD.
          Another way you can help is by getting involved with MADD. You can{" "}
          <Link to=" http://www.madd.org/get-involved/volunteer/ ">
            volunteer your time
          </Link>{" "}
          and become an activist in saving lives by signing up on their website.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
