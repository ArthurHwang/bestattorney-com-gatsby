// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite",
  sidebarLinks: {
    title: "Dog Bite Injury Information",
    links: [
      {
        linkName: "Dangerous Breeds",
        linkURL: "/dog-bites/dangerous-dog-breeds"
      },
      {
        linkName: "Factors Causing Canine Aggression",
        linkURL: "/dog-bites/factors-causing-canine-aggression"
      },
      {
        linkName: "First Aid For Dog Bites",
        linkURL: "/dog-bites/first-aid-dog-bites"
      },
      {
        linkName: "Injury & Infections from Dog Bites",
        linkURL: "/dog-bites/injury-infections-dog-bites"
      },
      {
        linkName: "Dog Bite Prevention",
        linkURL: "/dog-bites/prevention-tips"
      },
      {
        linkName: "Dog Bite Statistics",
        linkURL: "/dog-bites/statistics"
      },
      {
        linkName: "Victim's Rights",
        linkURL: "/dog-bites/victim-rights"
      },
      {
        linkName: "Why Dog's Bite",
        linkURL: "/dog-bites/why-dogs-bite"
      },
      {
        linkName: "California Dog Bite Laws",
        linkURL: "/dog-bites/california-dog-bite-laws"
      },
      {
        linkName: "Criminal Penalties",
        linkURL: "/dog-bites/criminal-penalties"
      },
      {
        linkName: "Insurance Breed Bans",
        linkURL: "/dog-bites/insurance-ban-breeds"
      },
      {
        linkName: "Newport Beach Home",
        linkURL: "/newport-beach"
      }
    ]
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/newport-beach-dog-bite-lawyer.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Dog Bite Lawyers – Dog Attack Attorney"
        pageDescription="The Newport Beach dog bite lawyers of Bisnar Chase are highly experienced in winning dog attack cases. Dog bite injuries can be severe, and we excel in helping victims. Contact us now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Dog Bite Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach dog bite lawyer"
            title="Dog Bite Attorney Newport Beach"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been attacked by an aggressive dog or suffered a serious
          bite injury, the Newport Beach dog bite lawyers of Bisnar Chase can
          help.
        </p>
        <p>
          We handle a lot of dog bite cases here in Newport Beach. As a
          beautiful beach town, Newport is home to a huge number of dog owners.
          Most of those owners are responsible and have well-behaved pets.
        </p>
        <p>
          However, we know only too well that a lot of people do not keep their
          dogs on leashes or train them properly. Unstable or dangerous dogs can
          cause serious injuries when they bite people or other animals.
        </p>
        <p>
          Our Newport Beach dog bite attorneys have helped countless clients
          across the area. Bisnar Chase has a 96% success rate and has collected
          more than $500 million in verdicts and settlements for injury victims.
          We can help make sure you get the compensation you deserve for a dog
          attack injury.
        </p>
        <p>
          If you have suffered a dog bite injury in Newport Beach,{" "}
          <Link to="tel:+1-949-203-3814">call us now at (949) 203-3814</Link>,
          or go to our <Link to="/contact">contact page</Link> to set up a free
          consultation with our team of experienced and compassionate{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Attorneys"
          >
            personal injury lawyers
          </Link>
          .
        </p>

        <h2>Dangerous Dog Breeds in Orange County</h2>

        <p>
          Many people just focus on breeds such as pit bulls, rottweilers and
          German Shepherds when they think of dangerous dogs. However, many
          breeds can be dangerous if the individual dog is not trained or cared
          for properly.
        </p>
        <p>
          That said, some breeds are more dangerous than others, purely because
          they are larger and more powerful. If a large and powerful dog decides
          to attack, it can be extremely difficult to stop. Check out our
          comprehensive guides to{" "}
          <Link
            to="/dog-bites/dangerous-dog-breeds"
            target="new"
            title="Top 13 Most Dangerous Dog Breeds"
          >
            the most dangerous dog breeds found in Newport Beach
          </Link>
          .
        </p>

        <h2>Dog Bite Statistics</h2>

        <p>
          Dog bite statistics paint a scary picture and show exactly why you
          need to be careful around a dangerous dog.
        </p>
        <p>
          A huge number of dog attack injuries are recorded every year, while
          many more go unreported.
        </p>
        <ul>
          <li>
            There are 4.7 million dog bites per year across the United States,{" "}
            <Link
              to="https://www.cdc.gov/mmwr/preview/mmwrhtml/mm5226a1.htm"
              target="new"
              title="CDC Dog Bite Statistics"
            >
              according to the CDC{" "}
            </Link>
          </li>
          <li>
            The number of dog bites needing medical attention has increased at a
            rate 15X faster than dog ownership
          </li>
          <li>
            More than 850,000 people sustain bites that need medical attention
            each year in the U.S.
          </li>
          <li>
            Nearly 10,000 people are hospitalized in the U.S. due to{" "}
            <Link
              to="https://www.dogsbite.org/dog-bite-statistics.php"
              target="new"
              title="Dog Bite Injury Statistics"
            >
              dog bite injuries{" "}
            </Link>{" "}
            every year
          </li>
          <li>
            More than{" "}
            <Link
              to="https://about.usps.com/newsroom/national-releases/2019/0411-usps-releases-dog-attack-national-rankings.htm"
              target="new"
              title="Postal Carrier Dog Attack Statistics"
            >
              5,700 postal carriers were attacked by dogs in 2018
            </Link>
            . That number eclipsed 6,000 in each of the two previous years.
          </li>
        </ul>

        <h2>Dog Bite Injuries in Newport Beach</h2>

        <p>
          An aggressive dog can cause serious harm, leaving its victim with
          major injuries. Dog bites can happen anywhere, from the beach or
          boardwalk to a park, or even your own back yard.
        </p>
        <p>
          These are some of the most common dog bite injuries suffered in
          Newport Beach, in and around the <strong> 92660</strong>,{" "}
          <strong> 92661</strong>,<strong> 92662</strong>, and{" "}
          <strong> 92663</strong> areas:
        </p>
        <ul>
          <li>
            <strong> Bruises, scratches, and gashes</strong>
          </li>
          <li>
            <strong> Puncture wounds and torn flesh</strong>
          </li>
          <li>
            <strong> Broken bones</strong>
          </li>
          <li>
            <strong> Torn ligaments</strong>
          </li>
          <li>
            <strong> Facial injuries</strong>
          </li>
          <li>
            <strong> Infections</strong>
          </li>
          <li>
            <strong> Scarring</strong>
          </li>
          <li>
            <strong> Emotional trauma and lasting anxiety</strong>
          </li>
        </ul>
        <p>
          Anyone who has suffered such an injury due to a dangerous dog attack
          should contact our Newport Beach dog bite lawyers for help.
        </p>

        <h2>Dog Bite Compensation in Newport Beach, CA</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/dog-attack-lawyer-newport-beach.jpg"
            width="50%"
            className="imgright-fluid"
            alt="A dangerous and aggressive dog bares its teeth"
            title="Newport Beach Dog Attack Injury Attorneys"
          />
        </LazyLoad>

        <p>
          While some people are reluctant to take legal action over a dog bite
          injury, it is often the right thing to do. Filing a personal injury
          lawsuit may help to hold an irresponsible owner accountable, as well
          as securing compensation for the victim.
        </p>
        <p>
          The level of compensation you might receive in a California dog bite
          case will always vary on the circumstances of the specific bite
          incident.
        </p>

        <p>Factors impacting dog bite compensation will include:</p>
        <ul>
          <li>
            <strong> Type and severity of the injury</strong>
          </li>
          <li>
            <strong> Medical costs</strong>
          </li>
          <li>
            <strong> The cost of future care and rehab</strong>
          </li>
          <li>
            <strong> Permanent physical scarring</strong>
          </li>
          <li>
            <strong> Psychological scarring</strong>
          </li>
          <li>
            <strong> Loss of future income</strong>
          </li>
        </ul>
        <p>
          Depending on the circumstances, a dog bite might be worth a relatively
          small amount of compensation, or it might lead to a lawsuit worth
          hundreds of thousands of dollars.
        </p>
        <p>
          A specialist dog bite attorney in Newport Beach will be able to give
          you expert guidance on your dog bite claim.
        </p>

        <h2>
          Dog Bite Liability – Who is Responsible for an Attack in Newport
          Beach?
        </h2>

        <p>
          When a dog behaves aggressively and attacks a person or animal, their
          owner is liable according to{" "}
          <Link
            to="/dog-bites/california-dog-bite-laws"
            target="new"
            title="California Dog Bite Laws"
          >
            California dog bite laws
          </Link>
          .
        </p>
        <p>
          Different states in the U.S. have varying dog bite rules. Some states
          use a law called the 'One Bite Rule'. This law essentially gives dog
          owners a second chance if their pet bites someone, as long as the
          owner had no reason to expect their dog would turn violent. But
          California uses a dog bite law called 'strict liability'.
        </p>
        <p>
          The strict liability law means that a dog owner is liable and can be
          held responsible for their dog's actions, even if it is the first time
          the dog has injured someone.
        </p>
        <p>
          It is important to remember that the owner is only liable as long as:
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              The victim was not trespassing at the time of the attack
            </strong>
          </li>
          <li>
            <strong> The dog was not provoked</strong>
          </li>
          <li>
            <strong>
              {" "}
              The dog was not on police, government, or military duty
            </strong>
          </li>
        </ul>
        <p>
          Our dog bite lawyers in Newport Beach can help victims navigate the
          complicated legal system and file a strong personal injury lawsuit.
        </p>

        <h2>What Evidence Does Your Dog Bite Attorney Need?</h2>

        <p>
          If you have been bitten by a dog, you should collect as much evidence
          as possible to support your legal claim.
        </p>
        <p>
          It is crucial to procure evidence quickly to make sure it is properly
          preserved. This can include:
        </p>
        <ul>
          <li>
            <strong> A written account of the events</strong>
          </li>
          <li>
            <strong>
              {" "}
              Photographs or video footage of the scene or the attack itself
            </strong>
          </li>
          <li>
            <strong> Pictures of your injuries</strong>
          </li>
          <li>
            <strong> Eye-witness statements and contact details</strong>
          </li>
          <li>
            <strong> Medical bills and records</strong>
          </li>
          <li>
            <strong>
              {" "}
              A copy of the police report if applicable, or any physical
              evidence such as torn clothing
            </strong>
          </li>
        </ul>
        <p>
          The best dog bite injury lawyers in Newport Beach will be able to use
          this information – plus anything else which is relevant to the attack
          – to build a strong case for their clients.
        </p>

        {/* {/* <h2>Finding the Best Dog Bite Lawyer Near You</h2> */}

        <p>
          If you have been bitten by a dog, Bisnar Chase can help. We have been
          a part of the Newport Beach community for more than 40 years,
          providing compassionate guidance and expert advice to countless
          clients over that span.
        </p>
        <p>
          With a <b>96% success rate</b>, the Newport Beach dog bite attorneys
          of Bisnar Chase excel in getting the best results possible for our
          clients. We also offer a 'No Win, No Fee' guarantee, opening up our
          services to everyone who needs them. If we don't win your case, you
          don't pay.
        </p>
        <div className="contact-us-border">
          <p>
            Call our <b>Newport Beach dog bite lawyers</b> now on{" "}
            <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>, visit our law
            firm's Newport offices, or click to{" "}
            <Link
              to="/contact"
              target="new"
              title="Contact Bisnar Chase Personal Injury Lawyers"
            >
              contact us{" "}
            </Link>{" "}
            for a free case evaluation.
          </p>
        </div>

        <h3>Videos About Dog Bite & Attack Cases</h3>
        <ul>
          <li>
            {" "}
            <Link to="/blog/dog-attacks-on-rise-video">
              Are California Dog Attacks on the Rise?{" "}
            </Link>
          </li>
        </ul>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
