// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "slip-and-fall/newport-beach-slip-and-fall-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Slip-and-Fall Lawyers – Fall Injury Experts – Bisnar Chase"
        pageDescription="The Newport Beach slip-and-fall lawyers of Bisnar Chase are here to help fall injury victims. Slip-and-fall accidents caused by property-owner negligence can result in serious injuries, from broken bones to traumatic head injuries. Contact our slip-and-fall attorneys in Newport Beach for a free consultation now."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Slip-and-Fall Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach Slip-and-Fall Lawyers"
            title="Newport Beach Slip-and-Fall Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have fallen and suffered an injury, it is time to contact the
          Newport Beach slip-and-fall lawyers of Bisnar Chase. Whether you have
          slipped in a grocery store aisle or tripped down a set of stairs, we
          may be able to help you pursue legal action.
        </p>
        <p>
          Many victims of slip-and-fall accidents suffer very serious injuries.
          These can include everything from broken bones to traumatic head
          injuries. Our{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Lawyer"
          >
            Newport Beach personal injury lawyers
          </Link>{" "}
          have seen it all, including severe falls causing paralyzing and fatal
          injuries. If you have been hurt in or around the{" "}
          <strong> 92660</strong> Newport area, we want to help.
        </p>
        <p>
          If your slip or fall has been caused by another party's negligence, we
          can help you secure the compensation you deserve – and hold the
          culprits accountable.
        </p>
        <p>
          Call our specialist Newport Beach slip-and-fall attorneys now on{" "}
          <Link
            to="tel:+1-949-203-3814"
            title="Click to call Bisnar Chase if you are using a cell phone"
          >
            (949) 203-3814
          </Link>{" "}
          for immediate help, or go to our{" "}
          <Link to="/contact">contact page</Link> to arrange a free case review.
        </p>

        <h2>What is a Slip-and-Fall Accident?</h2>

        <p>
          Slip-and-fall is a term used to describe a certain type of injury
          lawsuit. Essentially, a slip-and-fall – or trip-and-fall, as they are
          sometimes called – is a form of{" "}
          <Link
            to="/premises-liability"
            target="new"
            title="California Premises Liability Lawyer"
          >
            premises liability
          </Link>{" "}
          accident involving the victim falling and suffering an injury.
        </p>
        <p>
          There are a few factors which make an accident a slip-and-fall. The
          victim must slip, trip, or fall, and suffer some form of injury – no
          matter how minor. The incident also must happen in a location which is
          owned or managed by another person or company.
        </p>
        <p>Common slip and trip risks include:</p>
        <ul>
          <li>
            <strong>
              {" "}
              <u>Uneven surfaces</u> –
            </strong>{" "}
            This can include hazardous conditions such as broken paving stones,
            torn carpet, or potholes.
          </li>

          <li>
            <strong>
              {" "}
              <u>Slippery floors</u> –
            </strong>{" "}
            Often caused by spillages, or rain and mud being tracked onto an
            interior floor.
          </li>

          <li>
            <strong>
              {" "}
              <u>Outdoor elements</u> –
            </strong>{" "}
            When elements such as rain, snow, ice, and sleet create a dangerous
            underfoot surface outdoors.
          </li>

          <li>
            <strong>
              {" "}
              <u>Debris</u> –
            </strong>{" "}
            If objects are left on the floor where they should not be, this can
            cause a serious trip hazard.
          </li>

          <li>
            <strong>
              {" "}
              <u>Poor visibility</u> –
            </strong>{" "}
            When an area carries a high danger of falls due to poor lighting or
            a lack of visibility.
          </li>

          <li>
            <strong>
              {" "}
              <u>Unstable flooring</u> –
            </strong>{" "}
            This can include trip hazards such as loose floorboards, bricks,
            paving stones or tiles.
          </li>

          <li>
            <strong>
              {" "}
              <u>Stairs and escalators</u> –
            </strong>{" "}
            A{" "}
            <Link
              to="/premises-liability/escalator-elevator-injury-lawyers"
              target="new"
              title="California Escalator Accident Lawyer"
            >
              malfunctioning escalator{" "}
            </Link>
            or a compromised set of stairs can provide a serious danger.
          </li>
        </ul>

        <p>
          If you or a loved one have suffered an accident caused by one of these
          trip dangers, or any other type of underfoot hazard, you should call
          our slip-and-fall lawyers in Newport Beach for expert guidance.
        </p>

        <h2>Common Slip-and-Fall Accident Locations</h2>

        <p>
          A slip-and-fall accident must happen on premises owned by another
          party. Some of the most common places for a victim to trip or slip
          include:
        </p>
        <ul className="mb">
          <li>
            <strong>
              {" "}
              Grocery stores, general retail stores, malls, and shopping centers
            </strong>
          </li>
          <li>
            <strong> Restaurants</strong>
          </li>
          <li>
            <strong> Office buildings and businesses</strong>
          </li>
          <li>
            <strong> Sidewalks, parking lots and similar public areas</strong>
          </li>
          <li>
            <strong> Parks and other outdoor areas</strong>
          </li>
          <li>
            <strong> Private homes</strong>
          </li>
          <li>
            <strong> Amusement parks</strong>
          </li>
          <li>
            <strong>
              {" "}
              Sporting stadiums, concert halls, and similar event venues
            </strong>
          </li>
          <li>
            <strong> Theaters</strong>
          </li>
        </ul>

        <h2>Who Is Liable for a Slip-and-Fall Accident in Newport Beach?</h2>
        <LazyLoad>
          <img
            src="/images/slip-and-fall/newport-beach-trip-and-fall-injury-attorney-near-me.jpg"
            width="40%"
            alt="A cracked and raised sidewalk paving stone which is creating a dangerous trip-and-fall hazard for pedestrians."
            title="Newport Beach Slip-and-Fall Injury Lawyer"
            className="imgright-fluid"
          />
        </LazyLoad>

        <p>
          Who is at fault for a slip-and-fall injury if it is suffered at one of
          the above locations?
        </p>
        <p>
          In many cases, the owner of the property will be liable. A manager
          responsible for running or maintaining the location could also be at
          fault, while a slip or trip hazard on a sidewalk or in a public park
          might involve council or government liability.
        </p>
        <p>
          It is the responsibility of the owner or manager to keep their
          premises safe. If they have failed to do so then our Newport Beach
          slip-and-fall attorneys can help you launch a lawsuit to seek
          compensation for their negligence.
        </p>
        <p>A property owner is at fault if:</p>
        <ul>
          <li>
            <strong> They caused the dangerous condition</strong>
          </li>
          <li>
            <strong>
              {" "}
              They knew about the danger and failed to do anything about it
            </strong>
          </li>
          <li>
            <strong> They should have known about the hazard</strong>
          </li>
        </ul>
        <p>
          <b>
            <u>Example:</u>
          </b>{" "}
          A store manager is informed of a liquid spill but fails to have it
          cleaned up in a reasonable amount of time, or put a warning sign out
          by the slick surface, and a customer slips and is injured. In this
          case, the owner/manager would be liable, as they knew about the
          problem but failed to act.
        </p>
        <p>
          <b>
            <u>Example:</u>
          </b>{" "}
          A building owner is aware of loose and broken paving stones on their
          property, but puts off having them replaced. In the meantime, a person
          trips on the hazard and falls. The owner would be held liable for
          failing to provide a safe environment.
        </p>
        <p>
          However, it is important to note that an owner/manager is NOT always
          at fault.
        </p>
        <p>
          If they have taken all reasonable steps to provide a safe environment
          and reacted quickly to any hazards, there is unlikely to be a
          liability issue. If you have been injured in a fall, contact our
          slip-and-fall negligence lawyers in Newport Beach to find out if you
          have a case.
        </p>

        <h2>Slip-and-Fall Accident Statistics</h2>

        <p>
          A huge number of people suffer injuries in slip-and-fall accidents
          every year. These injuries account for a large portion of hospital
          visits, as well as billions of dollars in compensatory personal injury
          claims.
        </p>
        <ul>
          <li>
            More than one million people visit emergency rooms each year due to{" "}
            <Link
              to="https://nfsi.org/nfsi-research/quick-facts/"
              target="new"
              title="Slip-and-Fall Accident Statistics"
            >
              slip-and-fall accidents{" "}
            </Link>
          </li>

          <li>
            Slip-and-fall incidents make up the leading cause of workplace
            injuries and employee compensation claims
          </li>

          <li>
            The National Safety Council states that the cost of workplace
            slip-and-fall accidents in compensation and medical costs tops $70
            billion every year
          </li>

          <li>
            At least 20% of all fall accidents result in serious injury,
            including broken bones and head wounds
          </li>

          <li>
            The CDC indicates that falls are the most common{" "}
            <Link
              to="https://www.cdc.gov/homeandrecreationalsafety/falls/adultfalls.html"
              target="new"
              title="Slip-and-Fall Injury Statistics"
            >
              cause of traumatic brain injuries
            </Link>
          </li>
        </ul>

        <h2>
          What Should You Do After a Slip-and-Fall Accident in Newport Beach?
        </h2>

        <p>
          There are several key steps you should take after suffering a slip or
          trip accident in Newport Beach. These include gathering evidence,
          allowing you to pursue a lawsuit with the help of a slip-and-fall
          attorney.
        </p>
        <p>Take the following steps when possible:</p>
        <ol>
          <li>
            <span>
              <strong> Treatment:</strong>Get medical attention, depending on
              the severity of your injuries.
            </span>
          </li>
          <li>
            <span>
              <strong> Documentation:</strong>Document the scene. Identify the
              cause of your accident if possible, and take pictures. Write a
              first-hand account of the incident when you can.
            </span>
          </li>
          <li>
            <span>
              <strong> Witness:</strong>Talk to anyone who witnessed your fall.
              Take statements and contact details from any witnesses.
            </span>
          </li>
          <li>
            <span>
              <strong> Report:</strong>Make an official report of the incident
              and request a copy.
            </span>
          </li>
          <li>
            <span>
              <strong> Paperwork:</strong>Collect proof of any expenses relating
              to the fall.
            </span>
          </li>
          <li>
            <span>
              <strong> Hire an Attorney:</strong>Speak to an experienced Newport
              Beach slip-and-fall accident lawyer as soon as possible.
            </span>
          </li>
        </ol>

        <h2>How a Slip-and-Fall Attorney Can Help</h2>
        <LazyLoad>
          <img
            src="/images/slip-and-fall/slip-and-fall-accident-attorney-newport-beach.jpg"
            width="40%"
            alt="A man slips on a slick floor and falls next to a caution sign."
            title="Newport Beach Slip-and-Fall Accident Lawyer"
            className="imgleft-fluid mb"
          />
        </LazyLoad>

        <p>
          If you have been injured in a slip-and-fall accident, you should
          contact a professional who can provide expert help. Some victims
          believe that they can handle a personal injury case themselves, but
          this rarely goes well.
        </p>
        <p>
          Our Newport Beach slip-and-fall lawyers have extensive experience when
          it comes to handling these cases. We are adept at negotiating with
          insurance companies, but are also prepared to go to trial if an
          agreement cannot be reached. Bisnar Chase knows the Orange County
          court system well and will fight to make sure our clients receive the
          maximum compensation possible.
        </p>

        <h2>Finding the Best Newport Beach Slip-and-Fall Lawyer Near Me</h2>

        <p>
          Bisnar Chase is proud to support residents in the{" "}
          <strong> 92660</strong> Newport Beach area, as well as anyone else who
          needs help across Southern California.
        </p>
        <p>
          We have been based in Newport Beach for more than 40 years and our
          personal injury attorneys are passionate about taking on slip-and-fall
          claims for members of our community.
        </p>
        <p>
          Our knowledge of the local area – from the courts and judges to
          California law – is a significant benefit when it comes to tackling
          tough slip-and-fall cases for our clients. When you are looking for
          the best Newport Beach slip-and-fall accident attorneys near you, look
          no further than Bisnar Chase.
        </p>

        <h2>Slip-and-Fall Injury Compensation</h2>

        <p>
          The injuries you can suffer in a slip-and-fall accident can be
          significant. Common injuries include:
        </p>
        <ul>
          <li>Bruising and sprains</li>
          <li>Fractures and broken bones</li>
          <li>
            {" "}
            <Link
              to="/catastrophic-injury/spinal-cord-injury"
              target="new"
              title="California Spinal Cord Injury Lawyer"
            >
              Spinal cord injuries
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/head-injury"
              target="new"
              title="California Head Injury Lawyer"
            >
              Traumatic head injuries{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/wrongful-death"
              target="new"
              title="Newport Beach Wrongful Death Lawyer"
            >
              Wrongful death{" "}
            </Link>
          </li>
        </ul>
        <p>
          The level of compensation that our Newport Beach accident lawyers can
          win for you will depend on a range of factors. It will depend on the
          severity of any injuries suffered and the level of pain and suffering
          a person goes through, as well as medical bills and ongoing care
          costs.
        </p>
        <p>
          Any compensation award will depend entirely on the individual case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IuD-S72PMxk"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Bisnar Chase: Superior Representation</h2>

        <p>
          Bisnar Chase is dedicated to providing clients with superior
          representation. Our law firm is made up of staff members who are
          passionate about what they do and dedicated to helping fall injury
          victims.
        </p>
        <p>
          We have an outstanding track record, maintaining a{" "}
          <b>96% success rate</b>. We have also collected more than{" "}
          <b>$500 million</b> for our clients, ensuring they are made whole for
          their suffering.
        </p>
        <p>
          Better yet, Bisnar Chase offers a 'No Win, No Fee' guarantee. We
          advance all costs and make sure you do not have to pay a cent unless
          we win your case.
        </p>

        <center>
          <div className="contact-us-border">
            <p>
              Contact the Newport Beach slip-and-fall lawyers of Bisnar Chase
              now.{" "}
              <Link
                to="tel:+1-949-203-3814"
                title="Click to call Bisnar Chase if you are using a cell phone"
              >
                Call us on (949) 203-3814
              </Link>
              , visit our law offices in Newport Beach, or click to{" "}
              <Link
                to="/contact"
                target="new"
                title="Contact Bisnar Chase Personal Injury Attorneys"
              >
                contact us{" "}
              </Link>{" "}
              and arrange a free consultation.
            </p>
          </div>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
