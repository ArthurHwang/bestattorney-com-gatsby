// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/newport-beach-truck-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Truck Accident Lawyer – Bisnar Chase"
        pageDescription="Truck accidents can be devastating, causing severe injuries and even deaths. The Newport Beach truck accident lawyers of Bisnar Chase offer superior representation to truck crash victims. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach truck accident lawyer"
            title="Newport Beach truck accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The Newport Beach truck accident lawyers of Bisnar Chase are here to
          help if you have been injured in an auto accident involving a semi
          truck or 18-wheeler.
        </p>
        <p>
          These huge vehicles are a common sight on the roads in and around
          Newport Beach. But they can also pose extreme dangers to other
          drivers. While a{" "}
          <Link to="/newport-beach/car-accidents" target="new">
            car accident
          </Link>{" "}
          can be catastrophic, a truck crash is more likely to result in major
          damage and severe injuries because of the sheer size, weight, and
          force involved.
        </p>
        <p>
          {" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          has been representing truck accident victims throughout Orange County
          for more than 40 years, and our{" "}
          <Link to="/newport-beach" target="new">
            personal injury lawyers
          </Link>{" "}
          may be able to help you too. Before talking to the other side's
          insurance agents, you should contact Bisnar Chase for a free
          consultation with a skilled Newport Beach truck accident attorney. We
          will fight for you, and do everything we can to make sure you get the
          justice and compensation you deserve.
        </p>
        <p>
          Call <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> for
          immediate help from a top-class truck accident lawyer in Newport
          Beach.
        </p>

        <h2>Common Truck Accident Causes in Newport Beach</h2>

        <p>
          Many of the most common causes of truck accidents in Newport Beach
          involve some form of negligence on the part of the truck driver.
        </p>
        <p>The top crash causes include:</p>

        <h3>
          <b>
            <u>Speeding</u>
          </b>
        </h3>
        <p>
          A truck driving too fast – especially when it is carrying a heavy load
          in a tractor trailer truck – is a recipe for disaster and can lead to
          a serious crash.
        </p>

        <h3>
          <b>
            <u>Distracted Driving</u>
          </b>
        </h3>
        <p>
          If a driver is texting, eating, or doing something else which takes
          their attention from the road, it can be extremely dangerous for
          everyone around them.
        </p>

        <h3>
          <b>
            <u>Fatigue</u>
          </b>
        </h3>
        <p>
          While there are laws in place to stop truckers from spending too many
          consecutive hours on the road, tiredness can still be a huge problem
          and cause a loss of concentration.
        </p>

        <h3>
          <b>
            <u>Inexperienced Driver Negligence</u>
          </b>
        </h3>
        <p>
          Driving a commercial truck is not the same as driving a car. An
          inexperienced driver may be more likely to make a mistake as they are
          less used to the truck-driving challenges.
        </p>

        <h3>
          <b>
            <u>Load Issues</u>
          </b>
        </h3>
        <p>
          If a truck is hauling a heavy load, it is vital that its trailer is
          properly attached. A mistake in loading and securing the cargo, or a
          malfunction with the trailer, could cause a major accident.
        </p>

        <h3>
          <b>
            <u>Road Conditions</u>
          </b>
        </h3>
        <p>
          A truck accident could be caused by a poorly-maintained or marked
          road. This can lead to driver confusion or erratic driving, which can
          result in a dangerous driving situation.
        </p>

        <h2>Truck Accident Statistics</h2>

        <ul>
          <li>
            Trucks which are hauling huge trailers often weigh up to 30 times
            more than regular cars.
          </li>
          <li>
            The stopping distance for a cargo truck traveling within highway
            speed limits is more than 500 ft.
          </li>
          <li>
            About{" "}
            <Link
              to="https://www.iihs.org/iihs/topics/t/large-trucks/fatalityfacts/large-trucks"
              target="new"
            >
              one in 10{" "}
            </Link>{" "}
            of all auto accident deaths on highways happen in truck crashes.
          </li>
          <li>
            More than{" "}
            <Link
              to="https://www.trucks.com/2018/10/04/large-truck-fatalities-29-year-high/"
              target="new"
            >
              4,750 people died in truck crashes{" "}
            </Link>{" "}
            across the U.S. in 2017.
          </li>
          <li>
            About $20 billion is awarded to truck accident victims through court
            cases and insurance settlements every year.
          </li>
        </ul>

        <h2>Who Is Liable for a Newport Beach Truck Accident?</h2>

        <p>
          When it comes to truck crash personal injury cases, liability can
          depend on a few different factors.
        </p>
        <p>
          It will often rest on the cause of the crash itself. It is important
          to identify the careless or negligent party, and how their actions
          caused the accident. In shared-fault cases,{" "}
          <Link
            to="/resources/understanding-comparative-negligence"
            target="new"
          >
            comparative negligence
          </Link>{" "}
          may apply.
        </p>
        <p>
          But another factor which can impact liability is the situation of the
          truck itself. If it is a commercial truck, it may not be as simple as
          just blaming the driver.
        </p>
        <p>
          These are some of the possible liable parties in Newport Beach truck
          crash cases:
        </p>
        <ul>
          <li>The truck driver.</li>
          <li>The owner of the truck.</li>
          <li>The trucking company that the driver works for.</li>
          <li>Those responsible for loading the cargo onto the truck.</li>
          <li>
            The truck manufacturer or those responsible for its maintenance.
          </li>
        </ul>
        <p>
          An experienced truck accident lawyer in Newport Beach will be able to
          assess your case and help identify the parties which should be named
          in an injury claim.
        </p>

        <h2>What to Do After a Newport Beach Truck Crash</h2>

        <ol>
          <li>
            <span>Move away from your vehicle to safety if possible.</span>
          </li>
          <li>
            <span>Get medical attention.</span>
          </li>
          <li>
            <span>Call the police.</span>
          </li>
          <li>
            <span>
              Document the scene, secure witness statements, and organize your
              evidence.
            </span>
          </li>
          <li>
            <span>
              Contact an expert truck accident attorney in Newport Beach.
            </span>
          </li>
        </ol>

        <p>
          After you have been involved in a truck accident it is important that
          you arrange and organize all evidence which helps to form a picture of
          what happened.
        </p>

        <LazyLoad>
          <img
            src="/images/truck-accidents/truck-crash-lawyers-in-newport-beach.jpg"
            width="50%"
            alt="Emergency services dealing with the wreckage of a car next to a truck on the side of a highway after a major crash between the two vehicles."
            className="imgleft-fluid mb"
          />
        </LazyLoad>

        <p>
          Make sure you take pictures of the scene and the vehicles involved
          following the crash, as long as you are able to do so safely. Speak to
          anyone who saw the incident, and get statements or contact details for
          them.
        </p>
        <p>
          You can also request the police report for the incident – which may
          provide an indication of the cause of the accident – as well as your
          own medical bills and reports showing the extent of your injuries.
        </p>
        <p>
          Once you have assembled all potential evidence, contact the Newport
          Beach truck accident lawyers of Bisnar Chase. It is important to hire
          your chosen attorney as soon as possible after a crash.
        </p>

        <h2>Lawyers for Truck Drivers</h2>

        <p>
          In most personal injury claim cases, it is assumed that the truck
          driver was at fault. This is often because car crashes and motorcycle
          accidents involving trucks will almost always involve the other
          vehicle coming off worse. But this is not always the case.
        </p>
        <p>
          Independent truck drivers who become crash victims – and are hurt due
          to the negligence of another driver – can also seek help from a
          Newport Beach truck accident lawyer.
        </p>

        <h2>Common Truck Accident Injuries</h2>

        <p>
          If you are hit by a truck as a pedestrian or a driver of another
          vehicle, it is likely that you will suffer serious injuries. Trucks
          are large and extremely heavy. They are difficult to stop quickly, and
          any collision involving a truck is likely to carry a lot of force.
        </p>
        <p>Common truck crash injuries include:</p>
        <ul>
          <li>Cuts, scrapes and bruises</li>
          <li>Broken bones</li>
          <li>Internal organ damage</li>
          <li>Whiplash</li>
          <li>
            {" "}
            <Link to="/head-injury" target="new">
              Head injuries{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/brain-injury" target="new">
              Brain injuries{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              Spinal cord injuries{" "}
            </Link>{" "}
            – possibly resulting in paralysis
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/wrongful-death" target="new">
              Death{" "}
            </Link>
          </li>
        </ul>

        <h2>Compensation for a Newport Beach Truck Accident</h2>

        <p>
          If you have been injured in a collision involving a truck, there is a
          good chance that you will be entitled to compensation for your pain,
          suffering, and any lasting or life-altering effects suffered.
        </p>
        <p>
          There is no guaranteed level of compensation though. This will always
          depend on the circumstances of a crash, and how badly you have been
          impacted by it.
        </p>
        <p>
          A huge range of factors can help decide what level of compensation a
          victim might receive after being injured in a truck crash. They
          include:
        </p>
        <LazyLoad>
          <img
            src="/images/truck-accidents/newport-beach-truck-accident-attorneys.jpg"
            width="50%"
            className="imgright-fluid"
            alt="A large truck driving through heavy standing water on the road, splashing waves of water up on either side of it."
          />
        </LazyLoad>
        <ul>
          <li>Property damage</li>
          <li>The types of injuries suffered</li>
          <li>Pain and suffering</li>
          <li>Anxiety and mental anguish caused</li>
          <li>Medical bills and expenses</li>
          <li>Lost wages</li>
          <li>
            A loss of livelihood (if the injuries are bad enough to stop the
            victim working)
          </li>
          <li>Ongoing care and rehab costs</li>
          <li>Specialist equipment (in the case of disabilities)</li>
          <li>Compensation for loss of life</li>
        </ul>
        <p>
          High-quality legal representation from a Newport Beach personal injury
          attorney who specializes in truck accident cases can go a long way
          toward maximizing the amount of compensation the victim gets.
        </p>

        <h2>Beat the Insurance Companies with a Truck Accident Lawyer</h2>

        <p>
          Many of the trucks on the road are being driven for large commercial
          companies. These large trucking companies often have big legal teams
          and insurance firms behind them.
        </p>
        <p>
          When dealing with the aftermath of a truck crash, it is important to
          remember that the opposition insurance agents are not working toward
          your best interests. It is their job to settle the case as quickly as
          possible, for the least money possible.
        </p>
        <p>
          Don't let a large trucking company take advantage of you if it is at
          fault for a crash. Always talk to a truck accident lawyer before
          entering discussions with the insurance company. It is also a good
          idea to have your attorney present through negotiations.
        </p>

        <h2>
          What to Look for in the Best Newport Beach Truck Accident Lawyers
        </h2>

        <p>
          Not all attorneys are equal. At Bisnar Chase, we pride ourselves on
          proving superior representation, and firmly believe we have the best
          lawyers for the job when it comes to truck accident cases in and
          around Newport Beach.
        </p>
        <p>
          Here are some of the factors you should look for in a high-quality
          firm and lawyer:
        </p>
        <ul>
          <li>A law firm with a well-established history of success.</li>
          <li>A firm which has handled – and won – cases similar to yours.</li>
          <li>
            A firm with trained trial lawyers who are ready to fight for you in
            court.
          </li>
          <li>
            A firm which advances all costs and offers a 'No Win, No Fee'
            guarantee.
          </li>
          <li>A firm which is flexible and responsive to your needs.</li>
        </ul>

        <h2>Why Hire Bisnar Chase for Your Truck Accident Case?</h2>

        <p>
          At Bisnar Chase, we can offer elite truck crash legal representation
          for those who have been injured in and around Newport Beach.
        </p>

        <p>
          If you have sustained injuries in a collision with a truck, we can
          help. We have been in business for more than <b>40 years</b>, helping
          countless victims over that span. The lawyers at Bisnar Chase have a{" "}
          <b>96% success rate</b>, winning more than <b>$500 million</b> for our
          clients.
        </p>
        <p>
          We believe in securing justice for our clients and offer a 'No Win, No
          Fee' guarantee. This means that our services are open to everyone, and
          will not cost a dime unless we win the case.
        </p>
        <p>
          Call <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> for
          immediate help, or visit our offices in Newport Beach. You can also
          click to{" "}
          <Link to="/orange-county/contact" target="new">
            contact us
          </Link>{" "}
          now for a free consultation.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
