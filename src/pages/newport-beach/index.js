// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { FaStar } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/newport-beach-personal-injury-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Newport Beach",
      population: 87273,
      totalAccidents: 3563,
      intersection1: "Route 1 & Dover Ave",
      intersection1Accidents: 88,
      intersection1Injuries: 69,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 143.6,
      city1Name: "Costa Mesa",
      city1Index: 214.3,
      city2Name: "Fountain Valley",
      city2Index: 153.0,
      city3Name: "Irvine",
      city3Index: 89.6,
      city4Name: "Huntington Beach",
      city4Index: 184.5
    },
    locationBox3: {
      intersection2: "MacArthur Blvd & Bison Ave",
      intersection2Accidents: 72,
      intersection2Injuries: 54,
      intersection2Deaths: 0,
      intersection3: "Route 1 & Superior Ave",
      intersection3Accidents: 72,
      intersection3Injuries: 53,
      intersection3Deaths: 0,
      intersection4: "San Joaquin Hills & MacArthur Blvd",
      intersection4Accidents: 60,
      intersection4Injuries: 42,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Personal Injury Lawyers – Bisnar Chase"
        pageDescription="The Newport Beach Personal Injury Lawyers of Bisnar Chase excel in taking on injury cases of all kinds, from car accidents to dog bites – 96% success rate, $500M recovered, no win, no fee. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach Personal Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The Newport Beach personal injury lawyers of Bisnar Chase are here to
          help injury victims of all kinds when they need it most.
        </p>
        <p>
          There are a lot of attorneys out there, but only a few will give you
          the personal attention and legal expertise that your case needs. The
          personal injury lawyers at Bisnar Chase have been assisting injury
          victims in Newport Beach for more than 40 years with an outstanding
          96% success rate.
        </p>
        <p>
          Looking for a personal injury lawyer in or near the 92660 area? We can
          help.
        </p>
        <p>
          We are passionate about helping people and have the resources to win
          your case. We know the courts, defense lawyers, and culture in Orange
          County, and know that our firm can help win the compensation that you
          deserve.
        </p>
        <p>
          Our clients receive a very personal first-class experience from
          dedicated staff members who are passionate about pursuing justice. We
          have secured settlements and verdicts totaling more than $500 million
          for more than 12,000 clients. If you have been hurt due to someone
          else's negligence, contact us now.
        </p>
        <p>
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> or{" "}
          <Link to="/contact">contact us</Link> for a free consultation with the
          expert Newport Beach personal injury attorneys of Bisnar Chase.
        </p>

        <h2>What Kinds of Newport Beach Injury Cases do We Take?</h2>

        <p>
          In our 40 years of representing clients in Newport Beach, we have
          handled and won a huge variety of different cases.
        </p>
        <p>
          Newport Beach is a tourist hotspot, known for its beautiful beaches,
          as well as its stunning waterfront views and activities. But with
          these come plenty of potential hazards and injury causes, including
          busy roads and a lot of pedestrian foot traffic.
        </p>
        <p>
          These are some of the most common cases taken on by our Newport Beach
          personal injury lawyers:
        </p>

        <h3>
          <u>Road and Traffic Accidents</u>
        </h3>
        <p>
          {" "}
          <Link
            to="/newport-beach/car-accidents"
            target="new"
            title="Newport Beach Car Accident Lawyer"
          >
            Car accidents
          </Link>{" "}
          are a particular danger in Newport. PCH is always busy, and the
          peninsula is packed with cars when the sun is out.{" "}
          <Link
            to="/newport-beach/motorcycle-accidents"
            target="new"
            title="Newport Beach Motorcycle Accident Lawyer"
          >
            {" "}
            Motorcycle accidents
          </Link>{" "}
          also offer a major risk of injury, as do{" "}
          <Link
            to="/newport-beach/bus-accidents"
            target="new"
            title="Newport Beach Bus Accident Lawyer"
          >
            bus
          </Link>{" "}
          and{" "}
          <Link
            to="/newport-beach/truck-accidents"
            target="new"
            title="Newport Beach Truck Accident Lawyer"
          >
            truck accidents
          </Link>{" "}
          in Newport Beach. Bisnar Chase also handles road crashes which are
          classed as{" "}
          <Link
            to="/newport-beach/dui-accidents"
            target="new"
            title="Newport Beach DUI Accident Lawyer"
          >
            {" "}
            DUI
          </Link>{" "}
          or{" "}
          <Link
            to="/newport-beach/hit-and-run-accidents"
            target="new"
            title="Newport Beach Hit-and-Run Lawyer"
          >
            hit-and-run accidents
          </Link>
          .
        </p>
        <h3>
          <u>Bike and Pedestrian Accidents</u>
        </h3>
        <p>
          Those who live in Newport Beach often try to avoid the traffic and
          enjoy the sunny weather by cycling. However, Newport is one of the
          worst areas in Southern California – and even state-wide – when it
          comes to{" "}
          <Link
            to="/newport-beach/bicycle-accidents"
            target="new"
            title="Newport Beach Bicycle Accident Lawyer"
          >
            {" "}
            bike accident
          </Link>{" "}
          statistics. It can also be a dangerous area for{" "}
          <Link
            to="/newport-beach/pedestrian-accidents"
            target="new"
            title="Newport Beach Pedestrian Accident Lawyer"
          >
            {" "}
            pedestrian accidents
          </Link>
          , with many people crossing busy roads to get to the beach.
        </p>
        <h3>
          <u>Dog Bites</u>
        </h3>
        <p>
          You are likely to see a dog being walked by its owner every time you
          go out into Newport Beach. But{" "}
          <Link
            to="/newport-beach/dog-bites"
            target="new"
            title="Newport Beach Dog Bite Lawyer"
          >
            {" "}
            dog bite attacks
          </Link>{" "}
          are also common in the area. A dog might attack if it is angry or
          scared. In most cases, a dog's owner will be liable if it bites
          someone or causes injury.
        </p>
        <h3>
          <u>Premises Liability Cases and Workplace Injury</u>
        </h3>
        <p>
          Property owners and managers are responsible for making sure a
          location is safe. If you are injured on someone else's property due to
          their negligence, you may be able to take legal action. An example
          might be a{" "}
          <Link
            to="/newport-beach/slip-and-fall-accidents"
            target="new"
            title="Newport Beach Slip-and-Fall Accident Lawyer"
          >
            {" "}
            slip-and-fall accident
          </Link>{" "}
          inside a Newport Beach shop.
        </p>
        <h3>
          <u>Serious Injuries</u>
        </h3>
        <p>
          In cases where a person has suffered a{" "}
          <Link
            to="/catastrophic-injury"
            target="new"
            title="Catastrophic Injury Lawyer"
          >
            {" "}
            catastrophic injury
          </Link>
          , such as a{" "}
          <Link
            to="/catastrophic-injury/spinal-cord-injury"
            target="new"
            title="Spinal Cord Injury Lawyer"
          >
            {" "}
            spinal cord injury
          </Link>
          , a brain injury, paralysis, or even{" "}
          <Link
            to="/newport-beach/wrongful-death"
            target="new"
            title="Newport Beach Wrongful Death Lawyer"
          >
            {" "}
            wrongful death
          </Link>
          , a personal injury lawyer should be contacted. These are major
          injuries with life-altering consequences, but a personal injury
          attorney in Newport Beach can help victims seek compensation.
        </p>

        <h2>Lawsuit Success with Bisnar Chase</h2>

        <p>Some of our recent verdicts and settlements include:</p>

        <ul>
          <li>
            <strong> Motorcycle Accident – Award: $30,000,000</strong>
          </li>
          <li>
            <strong>
              {" "}
              Auto Defect – Seatback Failure – Award: $24,744,000
            </strong>
          </li>
          <li>
            <strong> Bicycle Accident – Award: $16,444,904</strong>
          </li>
          <li>
            <strong> Car Accident with Auto Defect – Award: $16,000,000</strong>
          </li>
          <li>
            <strong> Facility Negligence – Award: $11,000,000</strong>
          </li>
          <li>
            <strong> Premise Liability – Award: $10,030,000</strong>
          </li>
        </ul>

        <p>
          We have litigated thousands of cases and won hundreds of millions for
          our clients. Senior partner{" "}
          <Link to="/attorneys/brian-chase" target="new">
            {" "}
            Brian Chase
          </Link>{" "}
          has a reputation in Newport Beach as one of the very best personal
          injury attorneys in Southern California. He secures top-class results
          for his clients and is recognized with an array of awards every year –
          marking him out as one of the best in the business.
        </p>
        <p>
          Brian went to law school to pursue his dream of becoming a personal
          injury lawyer, because it is important to him to give injured
          plaintiffs a voice. He works tirelessly representing injured clients
          and fighting greedy insurance companies. Just as importantly, Brian
          has assembled a team of attorneys at Bisnar Chase who are just as
          passionate as he is.
        </p>
        <p>
          Our personal injury trial lawyers have obtained some hugely impressive
          results for our clients. We have taken on cases other law firms
          wouldn't touch. We have the experience, knowledge and resources to get
          great results!
        </p>

        <h2>Taking on Insurance Companies</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/personal-injury-attorney-in-newport-beach.jpg"
            width="100%"
            alt="Young couple going through personal injury bills and expenses"
            title="No Win, No Fee Personal Injury Law Firm in Newport Beach"
          />
        </LazyLoad>

        <p>
          We realize that you've got enough on your plate in dealing with your
          injuries and recovery. Our caring staff will take the pressure off of
          you so that you can focus on getting better. You can count on our
          legal team to take on the insurance companies and protect you all the
          way.
        </p>
        <p>
          It is important to remember that the insurance firm is not your
          friend. They will often lowball you because it is their goal to settle
          claims as quickly and for as little money as possible.
        </p>
        <p>
          Call on a personal injury lawyer who specializes in taking on
          insurance firms to make sure you get fair value on your claim.
        </p>

        <h2>What Compensation Can a Personal Injury Lawyer Win for You?</h2>

        <p>
          The level of compensation that a client might receive will always vary
          depending on their case. These are some of the key factors which can
          contribute to a compensation decision:
        </p>

        <ul>
          <li>Type and severity of the injury</li>
          <li>Medical bills and expenses of the victim</li>
          <li>Levels of pain and suffering</li>
          <li>
            Costs of ongoing care and equipment in catastrophic injury cases
          </li>
          <li>Property damage</li>
          <li>Loss of income and ability to earn a living</li>
        </ul>

        <h2>
          Dedication. Experience. Knowledge. Results – What to Look for in a
          Personal Injury Lawyer
        </h2>

        <p>
          It is important that you work with a Newport Beach personal injury
          attorney who you trust. These are some of the key features that you
          should look for when hiring a law firm to handle your case:
        </p>

        <ul>
          <li>A firm with a strong reputation.</li>
          <li>
            A firm which has years of experience and an outstanding success
            rate.
          </li>
          <li>The resources to advance all costs until a case is won.</li>
          <li>A track record of winning cases similar to your own.</li>
          <li>
            Flexibility – lawyers who truly listen and are responsive to your
            needs.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/newport-beach-personal-injury-compensation.jpg"
            width="100%"
            alt="Personal injury lawyer shakes hands with clients"
            title="Personal Injury Claim Representation in Newport Beach"
          />
        </LazyLoad>

        <h2>Bisnar Chase Client Reviews</h2>

        <p>
          Finding a trustworthy law firm is very important. Luckily, review
          websites like Google and Yelp can help steer you in the direction of
          trusted firms, allowing you to make an informed decision.
        </p>
        <p>
          At Bisnar Chase we are very proud of our client reviews and we work
          hard to make every client feel like they are our only client. Our
          entire staff is passionate about providing a legal service second to
          none.
        </p>
        <p>Here are just a few reviews of our services:</p>

        <div id="testimonials-box mb">
          {/* <center> */}

          {/* </center> */}
          <div className="testimonial mb">
            <div className="testimonial-review">
              <center>
                <blockquote style={{ fontSize: "1.4rem" }}>
                  Bisnar Chase provided outstanding service in negotiating with
                  the other person's insurance company. They worked hard to get
                  me the best settlement and the turn around time was fast. On a
                  scale of 1 to 10 I'd have to give the staff and attorneys of
                  Bisnar Chase in Newport Beach a 10.
                </blockquote>
              </center>
            </div>
            <div className="name col-md-6 border-box">
              <center>
                <h4>- J. McMillan</h4>
              </center>
            </div>
            <div className="status col-md-6 border-box">
              <center>
                <strong> (Former Client)</strong>
              </center>
            </div>
            <center>
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
            </center>
            <div className="clearfix"></div>
          </div>
          {/* <center> */}

          {/* </center> */}
          <div className="testimonial mb">
            <div className="testimonial-review mb">
              <center>
                <blockquote style={{ fontSize: "1.4rem" }}>
                  I knew I made the right choice with Bisnar Chase because they
                  went above and beyond to help. I especially want to thank
                  Colleen and Chris for meeting all my needs and walking me
                  through every piece of information. You have excellent
                  employees in this firm! I was treated like family and the
                  service and professionalism was above and beyond.
                </blockquote>
              </center>
            </div>
            <div className="name col-md-6 border-box">
              <center>
                <h4>- M. Cheng</h4>
              </center>
            </div>
            <div className="status col-md-6 border-box">
              <center>
                <strong> (Former Client)</strong>
              </center>
            </div>
            <center>
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
            </center>
            <div className="clearfix"></div>
          </div>
          {/* <center> */}

          {/* </center> */}
          <div className="testimonial mb">
            <div className="testimonial-review">
              <center>
                <blockquote style={{ fontSize: "1.4rem" }}>
                  They handled all the problems with the insurance companies.
                  All the contact back and forth – it was either a quick phone
                  call or a quick email. Other than forwarding them the doctor's
                  bills and talking to them, it was a piece of cake. My
                  settlement was amazing.
                </blockquote>
              </center>
            </div>
            <div className="name col-md-6 border-box">
              <center>
                <h4>- J. Gray</h4>
              </center>
            </div>
            <div className="status col-md-6 border-box">
              <center>
                <strong> (Former Client)</strong>
              </center>
            </div>
            <center>
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
              <FaStar className="star" />
            </center>

            <div className="clearfix"></div>
          </div>
        </div>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/oCeN5TviMcY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Newport Beach Personal Injury Lawyer Q&A</h2>

        <p>
          We know that the legal process can be a minefield, but we can help you
          navigate it. Here are some of the most important questions you might
          have:
        </p>

        <h3>
          <u>How Much Time Do I Have to File a Claim in California?</u>
        </h3>
        <p>
          There is a time limit in the State of California for filing a personal
          injury claim, therefore it is important to contact an attorney as soon
          as possible after you get medical treatment. If you choose to wait and
          the delay brings you close to the two-year statute of limitations, it
          could affect your case.
        </p>
        <h3>
          <u>What If I Do Not Have Money for Court Costs and Fees?</u>
        </h3>
        <p>
          We understand how traumatic and stressful an accident is. That is why
          we will go the extra mile for you. We advance all necessary costs of
          your case and we will shield you from having to repay those advances
          if your case is not successful. We will come to you if you cannot get
          to us and provide a free case consultation.
        </p>
        <h3>
          <u>What Should I Do After I Am Injured?</u>
        </h3>
        <p>Following an incident or injury, your first steps should be to:</p>
        <ol>
          <li>
            <span>Seek medical attention.</span>
          </li>
          <li>
            <span>Call the police.</span>
          </li>
          <li>
            <span>
              Collect evidence – such as pictures of the scene and your
              injuries, eye-witness statements, and physical evidence. You
              should also request a copy of the incident report from{" "}
              <Link to="http://www.nbpd.org/" target="new">
                Newport Beach Police Department
              </Link>
              , as well as copies of your medical reports.
            </span>
          </li>
        </ol>
        <h3>
          <u>How Do I File a Claim in Newport Beach and Get My Case Started?</u>
        </h3>
        <p>
          Call our Newport Beach personal injury lawyers and we will provide you
          with the information you need to get started. We are dedicated to
          making this process as pain-free as possible for you.
        </p>

        <h2>Bisnar Chase: Serving Newport Beach for More Than 40 Years</h2>

        <LazyLoad>
          <img
            src="/images/newport-beach-chamber-of-commerce.png"
            alt="Newport Beach Chamber of Commerce"
            title="Bisnar Chase - Newport Beach Community Personal Injury Lawyers"
            width="300"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Bisnar Chase is proud to have been a part of the Newport Beach
          community for more than 40 years. We have served the area since 1978,
          proving top-tier representation when residents are in need of a
          Newport Beach personal injury lawyer.
        </p>
        <p>
          While our primary law office is based in Newport Beach, we also serve
          the surrounding areas. This includes other cities within Orange
          County, such as Santa Ana, Costa Mesa and Huntington Beach. We also
          serve communities in Riverside County and Los Angeles County, among
          others.
        </p>

        <p className="clearfix">
          Bisnar Chase is a proud member of the{" "}
          <Link
            to="https://www.chamberofcommerce.com/newport-beach-ca/1334081517-bisnar-chase-personal-injury-attorneys"
            target="new"
          >
            {" "}
            Newport Beach Chamber of Commerce
          </Link>
          . We are heavily involved in our community and work diligently to
          satisfy each and every client. If you are in need of an Orange County
          accident lawyer, please consider letting us lead your efforts.
        </p>

        <h2>How Do I Find the Best Personal Injury Lawyers Near Me?</h2>

        <p>
          When you are in need of an effective personal injury attorney in or
          around the 92660 Newport Beach area, look no further than Bisnar
          Chase.
        </p>
        <p>
          We pride ourselves on providing superior representation for our
          clients, providing experience and expertise, as well as a
          compassionate and personal touch. Bisnar Chase has been in business
          for more than four decades, and maintains an outstanding{" "}
          <b>96% success rate</b>. We have collected more than{" "}
          <b>$500 million</b> for our clients over that time.
        </p>
        <p>
          Allowing us to handle your personal injury case means you will receive
          the best in client care. We have developed a reputation that we are
          very proud of, and have a history of getting great results for our
          clients. Bisnar Chase is the natural choice for people searching for
          the best personal injury lawyers in Newport Beach.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> for help
          now, or click to{" "}
          <Link to="/contact" target="new">
            contact us
          </Link>
          .
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .star {
    color: orange;
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
