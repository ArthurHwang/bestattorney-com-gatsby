// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "car-accidents/newport-beach-hit-and-run-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Hit and Run Lawyers - Bisnar Chase | Bisnar Chase"
        pageDescription="Leaving the scene of an accident is a serious crime. The Newport Beach hit-and-run lawyers of Bisnar Chase excel in getting victims the justice and compensation they deserve. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Hit-and-Run Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach hit and run lawyers"
            title="Newport Beach hit and run lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <b>Newport Beach hit-and-run lawyers</b> of Bisnar Chase are
          passionate about securing justice for accident victims who do not know
          where to turn.
        </p>
        <p>
          A hit-and-run accident is exactly what it sounds like. It is when a
          car, pedestrian, or cyclist is hit by a vehicle, with the offending
          party fleeing from the scene without stopping or waiting for the
          police to arrive.
        </p>
        <p>
          If you have been injured in a hit-and-run incident,{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          can help. We have been representing Newport Beach residents for more
          than 40 years, collecting hundreds of millions of dollars in
          settlements and verdicts for our clients.
        </p>
        <p>
          Our <Link to="/newport-beach">personal injury attorneys </Link> can
          help you navigate your hit-and-run ordeal. We can take the stress out
          of dealing with insurance companies and medical bills and help victims
          get the compensation they deserve. We do what we do because we believe
          in helping innocent people who have been wronged.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> now for
          specialist help from a Newport Beach hit-and-run attorney.
        </p>

        <div className="mb">
          {" "}
          <Link to="/newport-beach/hit-and-run-accidents#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Types of Hit-and-Run Accidents <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Newport Beach Car Accident Hit-and-Runs{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Hit-and-Run Pedestrian Accidents in Newport Beach{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Hit-and-Run Accident Statistics <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Do People Leave the Scene After an Accident?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Victims Should do After a Hit-and-Run in Newport Beach{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Common Hit-and-Run Injuries <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Who Pays in a Hit-and-Run Accident Case in Newport Beach?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/newport-beach/hit-and-run-accidents#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Hire the Best Hit-and-Run Lawyers <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>Types of Hit-and-Run Accidents in Newport Beach</h2>

        <p>
          The term ‘hit-and-run’ usually refers to an incident in which a driver
          hits something or someone with their vehicle, and then knowingly
          leaves the scene without leaving their information.
        </p>
        <p>
          These incidents can be divided into misdemeanor hit-and-runs – which
          usually solely involve property damage – and felony hit-and-runs,
          which result in injury or death.
        </p>
        <p>Types of hit-and-runs can include:</p>
        <ul>
          <li>
            {" "}
            <Link to="/newport-beach/car-accidents" target="new">
              Car accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/bicycle-accidents" target="new">
              Bicycle accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/truck-accidents" target="new">
              Truck accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/motorcycle-accidents" target="new">
              Motorcycle accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/newport-beach/pedestrian-accidents" target="new">
              Pedestrian accidents{" "}
            </Link>
          </li>
          <li>Stationary object accidents (such as hitting a parked car)</li>
        </ul>
        <div id="header2"></div>
        <h2>Newport Beach Car Accident Hit-and-Runs</h2>

        <p>
          A Newport Beach car accident hit-and-run lawyer might be required if
          there is a multi-car crash and one of the drivers refuses to stop. In
          some cases, the driver may stop briefly but leave before the police
          arrive. In others, a hit-and-run driver may flee without ever
          stopping.
        </p>
        <p>
          Newport Beach is often hit by heavy traffic, especially when the
          weather is good and people head to the beach. PCH and the peninsula
          can easily become clogged, making car, truck, and motorcycle accidents
          more likely.
        </p>
        <div id="header3"></div>
        <h2>Hit-and-Run Pedestrian Accidents in Newport Beach</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/newport-beach-hit-and-run-attorneys.jpg"
            width="50%"
            className="imgleft-fluid"
            alt="A man sitting in the road holding his injured leg after being involved in a hit-and-run. The car which hit him is driving away in the background."
          />
        </LazyLoad>

        <p>
          Newport Beach is a tourist attraction in its own right. With top-notch
          sandy beaches, shops, and restaurants, it is a popular destination.
          Pedestrians often have a false sense of security when crossing through
          the busy peninsula to get to the beach, but many drivers fail to see
          and stop for the crossings – especially if they are unfamiliar with
          the area.
        </p>
        <p>
          Cyclists are also a common sight in the area, both on PCH and further
          into Newport. Many people choose to cycle to and from the beach, as
          well as using bicycles as a mode of transport when they are drinking
          at a Newport bar.
        </p>
        <p>
          When a pedestrian or a cyclist is hit by a driver who then leaves the
          scene of the accident, it is a hit-and-run. A victim should contact a
          specialist hit-and-run lawyer in Newport Beach for help.
        </p>

        <div id="header4"></div>
        <h2>Hit-and-Run Accident Statistics</h2>

        <ul>
          <li>
            There were{" "}
            <Link
              to="https://newsroom.aaa.com/2018/04/hit-run-deaths-hit-record-high/"
              target="new"
            >
              20149 hit-and-run deaths{" "}
            </Link>{" "}
            in 2016 - the highest annual number ever recorded
          </li>
          <li>Hit-and-run fatalities have risen by more than 60% since 2009</li>
          <li>
            There were more than 737,000{" "}
            <Link
              to="https://aaafoundation.org/hit-and-run-crashes-prevalence-contributing-factors-and-countermeasures/"
              target="new"
            >
              hit-and-run crashes{" "}
            </Link>{" "}
            across the U.S. in 2015
          </li>
          <li>
            About 140,000 people are injured in hit-and-run accidents every year
          </li>
          <li>
            A hit-and-run crash happens every 43 seconds in the U.S. on average
          </li>
        </ul>
        <div id="header5"></div>
        <h2>Why Do Hit and Run Accidents Happen?</h2>

        <p>
          In many Orange County hit-and-run cases, the driver might be doing
          something wrong when the crash occurs, such as speeding, driving
          without insurance, or driving under the influence of alcohol or drugs.
        </p>
        <p>
          In other instances, they might just be trying to avoid repercussions
          from the accident by fleeing the scene.
        </p>
        <p>
          No matter the circumstances, leaving the scene of an accident is
          illegal in California, and will only make any punishment worse.
        </p>
        <div id="header6"></div>
        <h2>What Victims Should do After a Hit-and-Run in Newport Beach</h2>

        <LazyLoad>
          <img
            src="/images/car-accidents/hit-and-run-lawyer-in-newport-beach.jpg"
            width="80%"
            className="imgcenter-fluid"
            alt="A hit-and-run victim taking a picture of the damaged rear end of their car on their phone."
          />
        </LazyLoad>
        <p>At the scene of a hit-and-run accident you should:</p>
        <ol>
          <li>
            <span>
              Try to take in as many details about the hit-and-run driver as
              possible.
            </span>
          </li>
          <li>
            <span>Call 911.</span>
          </li>
          <li>
            <span>Move away from your vehicle to a safe position.</span>
          </li>
          <li>
            <span>
              Make sure anyone who needs medical attention gets treatment,
              including yourself.
            </span>
          </li>
          <li>
            <span>
              Talk to police officers about the hit-and-run driver. Try to give
              them as much detail as possible, including:
            </span>
          </li>

          <ul>
            <li>The make, model, and color of the car</li>
            <li>The license plate number if possible</li>
            <li>
              Any characteristics of the driver you could see, such as age,
              gender, hair length and color
            </li>
            <li>
              Anything unique or unusual about the vehicle, such as dents or
              bumper stickers
            </li>
            <li>How the crash happened</li>
            <li>The direction the car was headed in</li>
          </ul>

          <li>
            <span>
              Take pictures at the scene - including the scene itself, any marks
              on the road, your vehicle, and anything else relevant.
            </span>
          </li>
          <li>
            <span>
              Talk to witnesses who saw the accident. Get signed statements from
              them, or take their contact details.
            </span>
          </li>
          <li>
            <span>Contact a Newport Beach hit-and-run lawyer.</span>
          </li>
        </ol>

        <div id="header7"> </div>
        <h2>Hit-and-Run Accident Injuries</h2>

        <p>
          Hit-and-run accidents can result in severe injuries for victims.
          Whether the victim is a pedestrian or another driver, these crashes
          can lead to:
        </p>
        <ul>
          <li>Broken bones and severe cuts and wounds</li>
          <li>Internal injuries</li>
          <li>
            Whiplash and{" "}
            <Link to="/catastrophic-injury/back-injury-lawyer" target="new">
              back injuries{" "}
            </Link>
          </li>
          <li>
            Catastrophic injuries - including{" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              spinal cord injuries{" "}
            </Link>{" "}
            and{" "}
            <Link to="/head-injury" target="new">
              head trauma{" "}
            </Link>
          </li>
          <li>Emotional distress and anxiety</li>
          <li>Death</li>
        </ul>
        <div id="header8"></div>
        <h2>Who Pays in a Hit-and-Run Accident Case in Newport Beach?</h2>

        <p>
          If you have been injured by a fleeing driver, a Newport Beach
          hit-and-run lawyer can help you take action to secure the justice and
          compensation you deserve.
        </p>
        <p>Hit-and-run cases can be divided into two categories.</p>
        <h3>
          <u>If the Hit-and-Run Driver is Caught by the Police:</u>
        </h3>
        <p>
          If the driver is caught, they could be the subject of a hit-and-run
          charge. It might also be possible to make a legal claim against them
          for compensation.
        </p>
        <p>
          In some cases, the driver might have insurance that you can claim
          against. However, many hit-and-run cases involve drivers without
          insurance. This information will dictate the actions of your
          hit-and-run lawyer.
        </p>
        <h3>
          <u>If the Driver is Not Caught:</u>
        </h3>
        <p>
          If there is no driver to take legal action against, your other option
          is to seek compensation through your own insurance.
        </p>
        <p>
          There are several different forms of insurance that you could have in
          California. A hit-and-run lawyer in Newport Beach will be able to
          assess your policy and help you with the negotiations with your own
          insurance firm to make sure you get the maximum amount possible.
        </p>
        <p>Hit-and-run compensation can depend on:</p>
        <ul>
          <li>The damage to your own car, bike, or other vehicle</li>
          <li>The injuries you have sustained</li>
          <li>Medical expenses and ongoing care</li>
          <li>Lost wages</li>
          <li>Pain and suffering</li>
        </ul>
        <div id="header9"></div>
        <h2>Bisnar Chase - The Right Hit-and-Run Accident Lawyers for You</h2>

        <p>
          Bisnar Chase has been representing clients in hit-and-run cases for
          more than <b>40 years</b>. We have handled high-profile cases across
          Southern California, maintaining an outstanding{" "}
          <b>96% success rate</b> and collecting more than <b>$500 million</b>{" "}
          for our clients to date.
        </p>
        <p>
          We believe in building trusting attorney-client relationships, and
          ensuring that our clients feel comfortable with every step of the
          legal process. In addition, we aim to make our services as accessible
          as possible to all people. We advance all case-related costs, offering
          a 'No Win, No Fee' guarantee. This means that you will not pay unless
          we win your case. If you have been injured in an accident, the{" "}
          <b>Newport Beach hit-and-run lawyers</b> of Bisnar Chase can provide
          expert help and sound legal advice.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> for
          immediate help, visit our Newport Beach law offices, or click to{" "}
          <Link to="/orange-county/contact" target="new">
            contact us
          </Link>{" "}
          for a free case evaluation.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
