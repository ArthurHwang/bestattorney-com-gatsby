// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { Pie } from "react-chartjs-2"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "brain-injury/newport-beach-brain-injury-lawyer.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  const chartData = {
    labels: [
      "Fall Injuries",
      "Hit by an Object",
      "Vehicle or Roadway Accidents",
      "Assault/Act of Violence",
      "Unknown",
      "Other"
    ],
    datasets: [
      {
        data: [47, 15, 14, 9, 8, 7],
        backgroundColor: [
          "#F7464A",
          "#474687",
          "#FDB45C",
          "#46BFBD",
          "#72488e",
          "#468748"
        ]
      }
    ],

    borderColor: "#fff"
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Brain Injury Lawyers – Bisnar Chase"
        pageDescription="The Newport Beach brain injury lawyers of Bisnar Chase provide expert help to victims who have suffered traumatic brain injuries. Get the justice and compensation you deserve - call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach brain injury lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you or a loved one have suffered a brain injury, you should contact
          Bisnar Chase to speak to our expert{" "}
          <strong> Newport Beach brain injury lawyers</strong>.
        </p>

        <p>
          We know how serious a brain injury can be. Whether a victim has
          suffered a concussion in a car accident or hit their head in a
          slip-and-fall accident, the effects can be severe. A brain injury
          victim can be left facing serious pain, or even permanent and
          life-altering side effects.
        </p>

        <p>
          The Newport Beach brain injury attorneys of Bisnar Chase are dedicated
          to making sure brain injury victims get the compensation they deserve.
          We offer expert guidance through a free consultation, as well as a ‘No
          Win, No Fee’ guarantee, enabling anyone to seek justice.
        </p>

        <p>
          Contact our{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Lawyers"
          >
            personal injury lawyers
          </Link>{" "}
          now – <Link to="tel:+1-949-203-3814"> call (949) 203-3814 </Link> for
          immediate help or click the button below.
        </p>

        <h2>Why Should You Hire a Newport Beach Brain Injury Lawyer?</h2>
        <hr />

        <p>
          The main reason to hire a Newport Beach brain injury lawyer is that
          it’s the best way to get justice and make sure you are compensated for
          your injury.
        </p>

        <p>
          Suffering a brain injury can dramatically alter your life. In some
          cases, the injury will be temporary, and you might be left dealing
          with pain and other symptoms for a few days or weeks. Symptoms from
          other brain injuries can last for months or years, with some even
          leaving victims battling symptoms for the rest of their lives. In
          extreme cases, a personal injury involving the brain can even prove
          fatal.
        </p>
        <LazyLoad>
          <img
            src="/images/brain-injury/brain-injury-attorney-newport-beach.jpg"
            width="40%"
            className="imgleft-fluid"
            alt="A doctor examines a scan of a brain injury victim."
            title="Newport Beach Brain Injury Lawsuit Attorney"
          />
        </LazyLoad>

        <p>
          In cases involving personal injury and{" "}
          <Link
            to="/newport-beach/wrongful-death"
            target="new"
            title="Newport Beach Wrongful Death Lawyer"
          >
            {" "}
            wrongful death
          </Link>
          , you should consult with legal help. If you were harmed due to the
          negligence of another person or company, you should consider hiring a
          Newport Beach brain injury attorney.
        </p>
        <p>
          While you focus on your health, a high-quality brain injury lawyer
          will be able to fight for your rights. At Bisnar Chase, our attorneys
          are passionate about holding wrongdoers and negligent people
          responsible when their actions have serious consequences.
        </p>

        <p>
          If you have suffered a brain injury, hire a head injury lawyer in
          Newport Beach to help ease the weight on your shoulders. Your attorney
          will be able to build a strong case and give you the best chance of
          success.
        </p>

        <h2>Types of Brain Injury</h2>
        <hr />

        <p>
          The Newport Beach brain injury lawyers of Bisnar Chase are experienced
          in taking on all kinds of personal injury cases, from minor injuries
          to catastrophic harm and wrongful death.
        </p>

        <p>
          When it comes to damage to the brain, it is important to remember that
          the term ‘brain injury’ covers a huge range of harm. Knowing what type
          of injury you have sustained is important when deciding how to
          approach a legal claim.
        </p>

        <p>
          The following types are all traumatic brain injuries – caused by
          external sources and traumas:
        </p>

        <h3>
          <b>
            <u>Concussion</u>
          </b>
        </h3>

        <ul>
          <li>
            Concussions are caused by trauma to the brain, either from a direct
            impact to the head, or from a rapid movement which shakes the brain,
            such as a whiplash injury.
          </li>
          <li>A concussion is the most common form of brain injury.</li>
          <li>
            It can result in damage to the blood vessels and nerves in the
            brain, as well as brain bleeding or swelling.
          </li>
          <li>
            Some concussions heal within a week without any symptoms developing.
            Others can leave a victim struggling for months or years.
          </li>
        </ul>

        <h3>
          <b>
            <u>Brain Contusion</u>
          </b>
        </h3>

        <ul>
          <li>This is a term describing a bruise on the brain.</li>
          <li>
            Contusions are usually caused by a physical impact to the head.
          </li>
          <li>
            Contusions can cause bleeding in the brain and may require surgery
            to facilitate recovery.
          </li>
        </ul>

        <h3>
          <b>
            <u>Penetrating Wound</u>
          </b>
        </h3>

        <ul>
          <li>
            Penetration refers to brain injuries caused by objects being forced
            into the brain.
          </li>
          <li>
            This form of brain injury most commonly occurs from gunshot and
            stabbing wounds – a bullet or knife impacting the skull might push
            skin, bone, and other fragments into the brain.
          </li>
        </ul>

        <h3>
          <b>
            <u>Lesser Known Types of Traumatic Brain Injury</u>
          </b>
        </h3>

        <ul>
          <li>
            <i>Coup-Contrecoup</i> – Dual harm, with injuries occurring on both
            sides of the brain. This happens when one side of the brain is hit
            by an impact so hard that it causes the brain to move and hit the
            other side of the skull.
          </li>
          <li>
            <i>Diffuse Axonal</i> – Caused by violent shaking of the head and
            brain. Can cause nerve and structural tears, resulting in severe
            brain damage.
          </li>
        </ul>

        <h2>How Do Brain Injuries Happen in Newport Beach?</h2>
        <hr />

        <p>
          Brain injuries usually occur due to a powerful physical impact to the
          head, or rapid and violent movements which ‘shake’ the brain and cause
          it to hit the inside of the skull.
        </p>

        <p>
          These are some of the most common everyday causes of brain injuries in
          and around Newport Beach:
        </p>

        <ul>
          <li>
            Vehicle accidents (including{" "}
            <Link
              to="/newport-beach/car-accidents"
              target="new"
              title="Newport Beach Car Accident Lawyer"
            >
              {" "}
              car crashes
            </Link>
            ,{" "}
            <Link
              to="/newport-beach/motorcycle-accidents"
              target="new"
              title="Newport Beach Motorcycle Accident Lawyer"
            >
              motorbike accidents
            </Link>
            ,{" "}
            <Link
              to="/newport-beach/truck-accidents"
              target="new"
              title="Newport Beach Truck Accident Lawyer"
            >
              truck accidents
            </Link>
            ,{" "}
            <Link
              to="/newport-beach/pedestrian-accidents"
              target="new"
              title="Newport Beach Pedestrian Accident Lawyer"
            >
              {" "}
              accidents involving pedestrians
            </Link>
            , and more.)
          </li>
          <li>Sporting injuries</li>
          <li>
            Falls (such as a{" "}
            <Link
              to="/newport-beach/slip-and-fall-accidents"
              target="new"
              title="Newport Beach Slip and Fall Injuries"
            >
              {" "}
              slip-and-fall accident{" "}
            </Link>
            resulting in the victim hitting their head)
          </li>
          <li>Being struck by a person or object</li>
          <li>Violent assaults</li>
        </ul>

        <p>
          According to the{" "}
          <Link
            to="https://www.cdc.gov/traumaticbraininjury/get_the_facts.html"
            target="new"
            title="Brain Injury Statistics"
          >
            {" "}
            Centers for Disease Control (CDC)
          </Link>
          , the leading causes of traumatic brain injuries vary by age group.
        </p>

        <p>
          Across the U.S., stats show that about 2.8 million people suffer brain
          injuries each year, including nearly 20,000 victims in California. The
          top cause of brain injuries is people falling, followed by object
          impacts and car accidents:
        </p>

        {/* <canvas }, 600); id="myChart" height="250" } width="400"></canvas> */}
        <LazyLoad>
          <Pie className="mb" data={chartData}></Pie>
        </LazyLoad>

        <h2 style={{ marginTop: "2rem" }}>Symptoms of a Brain Injury</h2>
        <hr />

        <p>
          There is an extremely wide range of symptoms that you might experience
          if you have suffered a brain injury in Newport Beach. They can include
          everything from physical side effects to mental issues.
        </p>

        <p>
          {" "}
          <Link
            to="https://www.headway.org.uk/about-brain-injury/individuals/effects-of-brain-injury/"
            target="new"
            title="Brain Injury Symtoms"
          >
            Brain injury symptoms
          </Link>{" "}
          can include:
        </p>

        <ul>
          <li>
            <b>
              <u>Physical</u>
            </b>
            <ul>
              <li>Weakness and a loss of balance</li>
              <li>Paralysis</li>
              <li>Tremors</li>
              <li>Fatigue</li>
              <li>Problems with speech and language</li>
              <li>Impaired vision</li>
            </ul>
          </li>

          <li>
            <b>
              <u>Behavioral</u>
            </b>
            <ul>
              <li>Loss of impulse control</li>
              <li>Aggression</li>
              <li>Lethargy</li>
            </ul>
          </li>

          <li>
            <b>
              <u>Cognitive</u>
            </b>
            <ul>
              <li>Memory issues</li>
              <li>A difficulty concentrating</li>
              <li>Reduced ability to reason and process information</li>
            </ul>
          </li>

          <li>
            <b>
              <u>Emotional</u>
            </b>
            <ul>
              <li>Anxiety</li>
              <li>Depression</li>
              <li>Mood swings</li>
              <li>Personality changes</li>
            </ul>
          </li>
        </ul>

        <p>
          These are just some of the major and life-altering impacts that a
          brain injury can have on a person. Some of these symptoms might be
          temporary, but in other cases, they could be long-lasting or even
          permanent.
        </p>

        <p>
          These symptoms can turn a person’s life upside down. Many brain injury
          victims lose the ability to work and live independently. This is why
          injury clients should turn to an experienced Newport Beach brain
          injury attorney for help. Contact us now and let us seek justice for
          you.
        </p>

        <h2>Brain Injury Claims in Newport Beach</h2>
        <hr />

        <p>
          The statute of limitations for a brain injury lawsuit in California
          will usually be two years. This means that a victim should file their
          claim within two years of the accident causing their injury.
        </p>

        <p>
          It is important to remember that there are exceptions which will
          change the statute of limitations for some cases. It is best to
          contact a brain injury lawyer in Newport Beach as soon as possible to
          get the ball rolling on a legal claim.
        </p>

        <h2>Brain Injury Compensation in Newport Beach</h2>
        <hr />

        <p>
          What level of compensation can you expect if your brain injury lawsuit
          is successful? As with all personal injury claims, it will depend on
          your specific circumstances.
        </p>

        <p>
          Typically, compensation will depend on some of the following factors:
        </p>

        <ul>
          <li>
            <strong> The level of pain and suffering endured.</strong>
          </li>
          <li>
            <strong> Medical bills and expenses.</strong>
          </li>
          <li>
            <strong> Lost earnings – if you have been unable to work.</strong>
          </li>
          <li>
            <strong>
              {" "}
              Loss of livelihood – if the brain injury is severe enough to stop
              you doing your chosen job.
            </strong>
          </li>
          <li>
            <strong> Rehab costs.</strong>
          </li>
          <li>
            <strong> Impact of paralysis or loss of brain function.</strong>
          </li>
          <li>
            <strong> Costs of care and equipment.</strong>
          </li>
          <li>
            <strong> Loss of relationships.</strong>
          </li>
        </ul>

        <p>
          Provide your attorney with as much information as possible to help
          them build a strong legal case for you.
        </p>

        <h2>
          Factors to Consider When Choosing the Best Brain Injury Lawyer Near
          You
        </h2>
        <hr />
        <LazyLoad>
          <img
            src="/images/brain-injury/newport-beach-traumatic-brain-injury-lawyers.jpg"
            width="297"
            className="imgleft-fixed"
            alt="A 3D rendering of a person suffering from a brain injury."
            title="Newport Beach Brain Injury Compensation Lawyer"
          />
        </LazyLoad>

        <p>
          When choosing the best Newport Beach brain injury lawyer for your
          case, there are several factors that you should take into account,
          from past results to resources and location.
        </p>

        <p>
          At Bisnar Chase we pride ourselves on providing superior legal
          representation across Southern California. We have served Newport
          Beach – where our law offices are based – since 1978, and have more
          than 40 years of experience when it comes to successfully representing
          clients across the area. We serve the
          <strong> 92660</strong>, <strong> 92661</strong>,{" "}
          <strong> 92662</strong> and <strong> 92663</strong> areas, as well as
          those in surrounding cities looking for a nearby brain injury lawyer.
        </p>

        <p>
          Bisnar Chase also has the resources to take on major injury cases. Our
          personal injury attorneys are adept at negotiating favorable
          settlements. But they are also highly effective trial lawyers and are
          ready to fight for justice in court when a case calls for it.
        </p>

        <p>
          Our firm and lawyers have also received{" "}
          <Link
            to="/about-us/lawyer-reviews-ratings"
            target="new"
            title="Qualities of the Best Newport Beach Brain Injury Lawyers"
          >
            many awards
          </Link>
          through the years, including being recognized as Super Lawyers. In
          addition, Bisnar Chase offers a{" "}
          <Link
            to="/about-us/no-fee-guarantee-lawyer"
            target="new"
            title="No Win, No Fee Guarantee Lawyer"
          >
            ‘No Win, No Fee’ guarantee
          </Link>{" "}
          to clients. This means that our firm will advance all reasonable costs
          relating to your case. If we don’t win, you will not have to pay us.
        </p>

        <h2>Superior Representation with Bisnar Chase</h2>
        <hr />

        <p>
          Our track record makes{" "}
          <Link
            to="/"
            target="new"
            title="Bisnar Chase: Personal Injury Attorneys"
          >
            Bisnar Chase
          </Link>{" "}
          the perfect choice for people looking for the best brain injury
          attorneys in Newport Beach. We have a <b>96% success rate</b> and have
          collected more than <b>$500 million</b> for our clients.
        </p>

        <p>
          Beyond our courtroom skills, we pride ourselves in providing superior
          representation with a personal and compassionate touch. Our staff
          truly care about their clients and are dedicated to achieving the best
          result possible for them.
        </p>

        <div className="contact-us-border">
          <p>
            Call our <strong> Newport Beach brain injury lawyers</strong> on{" "}
            <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> now for
            immediate help, visit our Newport Beach law offices, or click to{" "}
            <Link
              to="/contact"
              target="new"
              title="Contact Bisnar Chase Personal Injury Lawyers"
            >
              contact us
            </Link>
            .
          </p>
        </div>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
