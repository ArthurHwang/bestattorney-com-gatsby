// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/newport-beach-pedestrian-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Pedestrian Accident Lawyers - Orange County, CA"
        pageDescription="The Newport Beach pedestrian accident lawyers of Bisnar Chase provide superior representation in car crash cases involving pedestrians, hit-and-runs, bicycle accidents and more. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach pedestrian accident lawyer"
            title="Newport Beach pedestrian accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been seriously injured in a Newport Beach pedestrian
          accident, the local{" "}
          <Link to="/newport-beach" target="new">
            personal injury lawyers of Bisnar Chase
          </Link>{" "}
          can help you secure maximum compensation for your injuries, damages,
          and losses. Our successful track record – combined with our experience
          in successfully handling pedestrian accident cases in Newport Beach
          and across Southern California – puts us in a unique position to
          produce the best possible outcome in your case.
        </p>
        <p>
          We also offer a 'No Win, No Fee' guarantee, ensuring that our clients
          do not have to pay any up-front or out-of-pocket costs unless we win
          their case and recover compensation for them. This means that you
          won't pay a dime unless we are successful.
        </p>
        <p>
          To schedule your free, no-obligation consultation and case evaluation,
          call the <strong> Newport Beach pedestrian accident lawyers</strong>{" "}
          of Bisnar Chase at{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>.
        </p>

        <h2>Have You Been Injured in a Pedestrian Accident?</h2>

        <p>
          Being out and about is integral to life in this coastal town. Both
          residents and visitors to Newport Beach come out here to enjoy
          sun-kissed beaches, boating along the bay, two spectacular piers and
          walking or jogging on the Strand.
        </p>
        <p>
          In a beach community that is busy year-round, being a pedestrian can
          also prove dangerous. Newport Beach sees its share of pedestrian
          accident injuries, and unfortunately, fatalities. From 2009 to 2014,
          Newport Beach had 186 pedestrian accidents, as shown in our{" "}
          <Link to="/resources/car-accident-statistics">
            Orange County car accident statistics report
          </Link>
          .
        </p>
        <p>
          If you have been injured in a pedestrian accident, these are the steps
          you should take to ensure your safety and to make sure your legal
          rights are protected:
        </p>
        <LazyLoad>
          <img
            src="/images/pedestrian-accidents/pedestrian-accident-lawyers-in-newport-beach.jpg"
            width="50%"
            className="imgright-fluid"
            alt="A person using their phone to take a picture of damage to the rear bumper of a car"
          />
        </LazyLoad>

        <ul>
          <li>
            <strong> Police report:</strong> File a police report and obtain a
            copy for your own records. A police report helps pin down the time,
            date and circumstances of the collision. In some cases, the
            responding or investigating officer might make a note of who
            appeared to be at fault. For example, the officer might mention in
            the report that the motorist had failed to yield to the pedestrian
            who was in a crosswalk. Such reports could be extremely valuable
            during the claims process.
          </li>

          <li>
            <strong> Medical attention:</strong> Seek and obtain medical
            attention. Pedestrians often face the danger of suffering internal
            injuries such as internal bleeding, which may not be apparent right
            away. Do not decline medical attention at the scene. If you have
            been injured, get the treatment and care you need, and make sure you
            follow the doctor's orders and recommendations.
          </li>

          <li>
            <strong> Preserving evidence:</strong> Try to get as much evidence
            from the scene as possible including photos, eyewitness accounts and
            information from other parties involved, such as the vehicle license
            plate number, driver's license, contacts, and insurance details. Be
            sure to preserve any type of evidence such as torn or bloody
            clothing.
          </li>

          <li>
            <strong> Protecting your rights:</strong> Contact an experienced
            pedestrian accident attorney in Newport Beach who will work every
            step of the way to ensure that your rights and best interests are
            protected.
          </li>
        </ul>

        <h2>The Most Common Types of Pedestrian Accidents</h2>

        <p>
          The most common types of pedestrian accidents involve collisions
          between people and vehicles. Pedestrians and drivers both have a duty
          to act responsibly on the roads, but accidents still happen – for a
          wide range of reasons. These include:
        </p>
        <ul>
          <li>
            <strong> Distracted Driving –</strong> When drivers are texting,
            eating, or otherwise distracted, they are less likely to see and
            stop for pedestrians.
          </li>

          <li>
            <strong> Reversing –</strong> Vehicles pose a danger to pedestrians
            when they are backing up, as visibility can often be a problem for
            the driver.
          </li>

          <li>
            <strong> Drunk Driving –</strong> When a person is driving under the
            influence of alcohol or drugs, their vision and reflexes are
            affected, making a crash more likely.
          </li>
          <LazyLoad>
            <img
              src="/images/pedestrian-accidents/pedestrian-accident-injury-lawyers.jpg"
              width="50%"
              alt="An injured pedestrian laid on the street on a crossing, next to a car."
              className="imgright-fluid"
            />
          </LazyLoad>

          <li>
            <strong> Intersection and Crossing Accidents –</strong> A pedestrian
            could be hit by a car while dashing across the street as the light
            changes. People can also be hit reckless drivers trying to make a
            green light.
          </li>

          <li>
            <strong> Dart-Outs –</strong> In some cases, a pedestrian might walk
            out in front of a motor vehicle, either misjudging its speed or
            expecting it to stop. In some cases, a person may step in front of a
            stopped car as it starts moving again.
          </li>

          <li>
            <strong> Right Turns –</strong> Cars turning right at a traffic
            light or stop sign may expect the turn to be clear, without
            realizing a pedestrian is about to cross.
          </li>

          <li>
            <strong> Left Turns –</strong> When cars make left turns, they are
            turning across traffic. Making sure the road is clear of oncoming
            cars can distract them from checking whether a pedestrian is
            crossing the street.
          </li>
        </ul>

        <h2>Pedestrian Accident Injuries</h2>

        <p>
          A car or trucking accident involving a pedestrian can cause major
          injuries. They can even result in wrongful death. The most common
          injury types include:
        </p>

        <ul>
          <li>Broken bones.</li>
          <li>Severe trauma and soft tissue injuries.</li>
          <li>
            {" "}
            <Link to="/head-injury" target="new">
              Head injuries
            </Link>
            .
          </li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              Spinal cord injuries
            </Link>
            .
          </li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/amputations" target="new">
              Amputation
            </Link>
            .
          </li>
          <li>Emotional trauma.</li>
          <li>Death.</li>
        </ul>

        <p>
          In most pedestrian accidents, the bumper or hood of the vehicle will
          typically hit the pedestrian's legs and hips. This means that broken
          bones in those areas are quite common in pedestrian accidents.
          Depending on the speed at which the striking vehicle is traveling, the
          pedestrian can be thrown from the point of impact. Head injuries and
          internal organ damage are common in such cases.
        </p>
        <p>
          Often in the case of fractures, there are also injuries to blood
          vessels and tissues. These types of injuries can lead to limb
          amputations. Some individuals suffer permanent injuries or
          disabilities because of severe brain injuries or spinal cord trauma.
          Others lose their livelihoods. Pedestrian accident injuries could also
          prove fatal.
        </p>
        <p>
          Some of these injuries can leave victims with facing severe and
          life-long effects.
        </p>
        <h2>How Long Will a Pedestrian Accident Lawsuit Take?</h2>

        <p>
          How long it takes to get a pedestrian accident case resolved in
          Newport Beach will depend on the facts of the case. People with less
          severe injuries will often get the medical treatment and care they
          need and go on to make a quick recovery.
        </p>
        <p>
          Such cases can be completed within a few months. This time frame
          allows for the treatment of injuries and negotiations with insurance
          firms.
        </p>
        <p>
          If you have suffered severe or catastrophic injuries, your case could
          take longer. If the insurance company does not offer you a fair
          settlement, it may become necessary to take the case to a trial. This
          could add more time to your case. When an{" "}
          <Link to="/car-accidents/insurance-hardball">
            insurance company is playing hardball
          </Link>
          , as they often do, it is important to pursue all options to get the
          justice and compensation you deserve.
        </p>
        <h2>The Worth of Your Pedestrian Accident Case</h2>

        <p>
          The{" "}
          <Link to="/resources/value-of-your-personal-injury-claim">
            worth or value of a pedestrian accident case
          </Link>{" "}
          often depends on the nature and extent of the victim's injuries, as
          well as the losses he or she has incurred as a result of the injuries
          – both economic and non-economic. Here are some of the elements that
          go into determining what your case will be worth:
        </p>

        <ul>
          <li>Medical expenses – current and future costs.</li>

          <li>Time away from work.</li>

          <li>Loss of future earning potential or livelihood.</li>

          <li>Cost of rehab treatments such as physical therapy.</li>

          <li>Pain and suffering.</li>

          <li>Mental anguish.</li>
        </ul>
        <p>
          Other factors that could affect the value of your claim include the
          facts of your case, fault, liability and prior medical history. An
          experienced Newport Beach pedestrian accident attorney can not only
          help you assess your losses such as incurred costs and lost wages, but
          also non-economic damages such as pain and suffering, which can be
          more challenging to determine.
        </p>
        <h2>Contacting an Experienced Accident Lawyer in Newport Beach</h2>

        <p>
          If you or a loved one has been injured in a pedestrian crash, it would
          be in your best interests to contact an experienced Newport Beach
          pedestrian accident attorney.
        </p>
        <p>
          Bisnar Chase has called Newport Beach home for 40 years. Over that
          time, our firm has developed a successful track record of dealing with
          insurance companies and helping injured victims and their families
          recover maximum compensation. Our firm has a <b>96% success rate</b>,
          collecting more than <b>$500 million</b> for our clients.
        </p>
        <p>
          As experienced{" "}
          <strong> Newport Beach pedestrian accident lawyers</strong>, we pride
          ourselves on offering superior representation to clients. Bisnar Chase
          also offers a 'No Win, No Fee' guarantee, as well as free consultation
          services, to make sure our skills are open to everyone who needs them.
        </p>
        <p>
          Call us on <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>, visit
          our Newport Beach law offices, or click{" "}
          <Link to="/contact">here</Link> to contact us now.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
