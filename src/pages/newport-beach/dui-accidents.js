// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dui-images/newport-beach-dui-accident-lawyer.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach DUI Injury Lawyers – Bisnar Chase"
        pageDescription="The Newport Beach DUI injury lawyers of Bisnar Chase have been representing victims of DUI accidents for more than 40 years. Drunk driving is dangerous, causing injuries and putting innocent lives at risk. We help DUI crash victims get the justice and compensation they deserve. Call now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach DUI Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach DUI Accident Lawyers"
            title="Newport Beach DUI Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been injured in a DUI accident caused by a drunk driver in
          Newport Beach, call Bisnar Chase now for a free consultation. Our
          Newport Beach DUI injury lawyers are experts when it comes to
          supporting crash victims.
        </p>
        <p>
          Among the wealthiest communities in the country, Newport Beach in
          Orange County, California, is a beautiful beach town packed with
          stylish bars and pubs. Unfortunately, far too many choose to get
          behind the wheel after drinking, and the number of DUIs in Newport
          Beach are worryingly high.
        </p>
        <p>
          A DUI crash can easily result in an innocent victim suffering serious
          injuries. If you or a loved one have been hurt, you can turn to the
          Newport Beach DUI attorneys of Bisnar Chase. We can help you secure
          the justice and compensation you deserve.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call us on (949) 203-3814 </Link> for
          immediate help, or go to our <Link to="/contact">contact page</Link>{" "}
          to contact a skilled DUI lawyer in Newport Beach.
        </p>

        <h2>Types of DUI Cases</h2>
        <hr />

        <p>
          The DUI laws in California are applied to anyone who is driving while
          under the influence of alcohol and/or drugs. This means that a DUI
          accident refers to a collision involving at least one drunk or drugged
          driver.
        </p>
        <p>
          Note: Authorities may also refer to a case as a DUID. This refers
          specifically to a person driving while under the influence of drugs.
        </p>
        <p>
          To be guilty of an alcohol-related DUI, a driver has to exceed the
          legal blood/alcohol limit. This is specific to the state they are in.
          In California, the blood/alcohol content limit is:
        </p>
        <ul>
          <li>
            <b>0.08% for drivers aged 21+</b>
          </li>
          <li>
            <b>0.01% for drivers aged under 21</b>
          </li>
        </ul>
        <p>
          The Newport Beach DUI accident lawyers of Bisnar Chase have handled
          all kinds of cases involving drunk drivers. These can include:
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="/newport-beach/car-accidents"
              target="new"
              title="Newport Beach Car Accident Lawyer"
            >
              Car accidents{" "}
            </Link>{" "}
            involving two or more vehicles
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/motorcycle-accidents"
              target="new"
              title="Newport Beach Motorcycle Accident Lawyer"
            >
              Motorcycle accidents
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/truck-accidents"
              target="new"
              title="Newport Beach Truck Accident Lawyer"
            >
              Truck accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/bicycle-accidents"
              target="new"
              title="Newport Beach Bicycle Accident Lawyer"
            >
              Bicycle accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/bus-accidents"
              target="new"
              title="Newport Beach Bus Accident Lawyer"
            >
              Bus accidents{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/pedestrian-accidents"
              target="new"
              title="Newport Beach Pedestrian Accident Lawyer"
            >
              Pedestrian accidents{" "}
            </Link>
            involving a vehicle and someone on foot
          </li>
          <li>
            {" "}
            <Link
              to="/newport-beach/hit-and-run-accidents"
              target="new"
              title="Newport Beach Hit-and-Run Lawyer"
            >
              Hit and runs{" "}
            </Link>
          </li>
        </ul>
        <p>
          If any of these accident types involve a drunk driver, they would
          count as a DUI. Anyone who has been hurt in one of these DUI crash
          types should contact a DUI lawyer in Newport Beach as soon as
          possible.
        </p>

        <h2>Bisnar Chase – DUI Victim Attorneys</h2>
        <hr />

        <p>
          Here at Bisnar Chase, we pride ourselves on being top-quality{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Lawyer"
          >
            personal injury lawyers
          </Link>
          . As such, we work solely with the victims of DUI cases.
        </p>
        <p>
          Other law firms take on criminal cases and provide specialist criminal
          defense lawyers to work on DUI defenses. That is not what we do. We
          are not criminal defense attorneys who work at getting DUI charges
          dismissed.
        </p>
        <p>
          We work with victims who have been hurt due to the actions of a drunk
          driver.
        </p>
        <p>
          Getting behind the wheel when you are under the influence of alcohol
          or drugs is a very bad idea. Anyone who drinks and drives does so
          knowing that they could cause a serious accident. This is because
          driving under the influence can cause:
        </p>
        <ul>
          <li>
            <strong> Blurred vision</strong>
          </li>
          <li>
            <strong> Slow reaction times</strong>
          </li>
          <li>
            <strong> Lack of coordination</strong>
          </li>
          <li>
            <strong> Loss of concentration</strong>
          </li>
          <li>
            <strong> An inability to make rational decisions</strong>
          </li>
        </ul>
        <p>
          We work closely with DUI victims in Newport Beach to help ensure that
          they are properly compensated for any injuries and damage inflicted
          upon them by a drunk driver.
        </p>

        <h2>DUI Accident Statistics</h2>
        <LazyLoad>
          <img
            src="/images/dui-images/newport-beach-dui-injury-attorney.jpg"
            width="300"
            alt="Police cars driving at night with flashing lights on."
            className="imgright-fixed"
            title="Newport Beach DUI Injury Lawyers"
          />
        </LazyLoad>

        <p>
          The stats can make for scary reading when it comes to DUI accidents in
          Newport Beach.
        </p>

        <p>
          In 2018 there were{" "}
          <Link
            to="http://www.nbpd.org/crime/statistics/crime_by_year.asp"
            target="new"
            title="Newport Beach DUI Statistics for 2018"
          >
            429 DUI arrests in Newport
          </Link>
          , and those are just the drunk drivers who were caught. This
          represents a rise of nearly 50% in DUI arrests over just the last
          three years – a huge and frightening increase. It is also reasonable
          to believe that many more would have driven while drunk and escaped
          punishment.
        </p>
        <p>
          In addition to drunk driving arrests, there were 818 reported drug
          abuse violations in 2018, as well as a further 450 arrests for
          drunkenness. How many of those revelers would have driven home after a
          day or night of drinking?
        </p>
        <p>
          <u>According to the CDC:</u>
        </p>
        <ul>
          <li>
            There is an average of 29 deaths due to{" "}
            <Link
              to="https://www.cdc.gov/motorvehiclesafety/impaired_driving/impaired-drv_factsheet.html"
              target="new"
              title="DUI Accident Statistics"
            >
              car crashes involving a drunk driver{" "}
            </Link>
            every single day in the U.S.
          </li>
          <li>
            DUI crashes cause nearly 30% of all traffic-related fatalities
            across the United States
          </li>
          <li>There were more than one million DUI arrests in 2016</li>
          <li>
            Local police statistics show that the number of DUI arrests in
            Newport Beach has risen by nearly 50% between 2015-2018
          </li>
          <li>
            A DMV study shows that there is an annual average of 17,117{" "}
            <Link
              to="https://www.dmv.ca.gov/portal/wcm/connect/848b335c-1360-4473-a35d-4da3345c666a/s5-257.pdf?MOD=AJPERES&CVID="
              target="new"
              title="California Statistics For DUI Convictions"
            >
              reckless driving convictions{" "}
            </Link>
            involving alcohol or drugs in California
          </li>
        </ul>

        <h2>Common Injuries Sustained by Drunk Driving Victims</h2>
        <hr />

        <p>
          Driving while under the influence of alcohol or drugs can lead to
          extremely dangerous scenarios on the road. It is not uncommon for DUI
          drivers to be swerving, speeding, or driving erratically, due to the
          effects of the substance they have consumed.
        </p>
        <p>
          The injuries a person can sustain in a DUI accident can vary hugely.
          It can depend on how fast the offending car was going, where it hit
          you, and whether you were in a vehicle with some protection, or
          perhaps on a bicycle or on foot.
        </p>
        <p>
          Some of the frighteningly common injuries our DUI accident lawyers
          have seen include:
        </p>
        <ul>
          <li>Cuts and bruises</li>
          <li>Broken bones</li>
          <li>Head injuries</li>
          <li>
            {" "}
            <Link
              to="/newport-beach/brain-injury"
              target="new"
              title="Newport Beach Brain Injury Lawyer"
            >
              Brain injuries{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/catastrophic-injury/spinal-cord-injury"
              target="new"
              title="California Spinal Cord Injury Lawyer"
            >
              Spinal cord injuries
            </Link>
          </li>
          <li>Internal injuries including organ damage</li>
          <li>Emotional suffering and trauma</li>
          <li>
            {" "}
            <Link
              to="/newport-beach/wrongful-death"
              target="new"
              title="Newport Beach Wrongful Death Lawyer"
            >
              Death{" "}
            </Link>
          </li>
        </ul>
        <p>
          A high-impact collision can have a major and life-altering impact on a
          DUI victim. Some crashes can leave victims paralyzed and struggling to
          do tasks which they would once have found simple. Even relatively
          minor injuries can result in serious ongoing pain and lengthy rehab.
        </p>
        <p>
          A knowledgeable DUI attorney in Newport Beach can help victims hold a
          drunk driver responsible for their actions.
        </p>

        <h2>How Much Compensation Can You Get From a DUI Lawsuit?</h2>
        <hr />

        <p>
          The amount of compensation a DUI accident victim could get through a
          legal claim can vary. It will depend on a wide range of factors,
          including how bad their injuries are, and the long-term effects the
          crash may have on them.
        </p>
        <p>Compensation factors include:</p>
        <ul>
          <li>
            <strong> Type of injury</strong>
          </li>
          <li>
            <strong> Medical costs</strong>
          </li>
          <li>
            <strong> Emotional trauma and mental anguish</strong>
          </li>
          <li>
            <strong> Costs relating to ongoing rehab and care</strong>
          </li>
          <li>
            <strong> Property damage</strong>
          </li>
          <li>
            <strong> Loss of life</strong>
          </li>
        </ul>
        <p>
          When you contact Bisnar Chase, one of our Newport Beach DUI injury
          attorneys will be able to assess your case and help you fight for the
          most compensation possible.
        </p>

        <h2>What Evidence do you Need in a Newport Beach DUI Lawsuit?</h2>
        <hr />

        <p>
          You can help your Newport Beach DUI attorney build the strongest case
          possible for you by assembling a few key pieces of evidence.
        </p>
        <ol>
          <li>
            <span>The police report from the DUI accident</span>
          </li>
          <li>
            <span>
              Medical reports and proof of costs relating to your injuries
            </span>
          </li>
          <li>
            <span>
              Pictures and/or video footage from the accident and the scene
            </span>
          </li>
          <li>
            <span>Pictures of your injuries</span>
          </li>
          <li>
            <span>A first-hand account of the crash</span>
          </li>
          <li>
            <span>Eye-witness statements and contact details</span>
          </li>
        </ol>
        <p>
          In some cases, legal claims can take longer than we would like.
          Assembling a strong range of evidence relating to your case will help
          your lawyer to get things moving quickly.
        </p>

        <h2>How Much Does it Cost to Hire a Newport Beach DUI Lawyer?</h2>
        <LazyLoad>
          <img
            src="/images/dui-images/dui-lawyer-newport-beach.jpg"
            width="40%"
            className="imgleft-fluid"
            alt="Two people shake hands over a legal agreement."
            title="DUI Attorneys in Newport Beach"
          />
        </LazyLoad>

        <p>
          A lot of people are put off from trying to take legal action, even if
          they are the victim of a serious DUI crash, due to the perception that
          it is expensive to hire a lawyer.
        </p>

        <p>
          With Bisnar Chase, that is not the case. If you are the victim of a
          DUI accident, we strongly believe that finances should not stand
          between you and justice.
        </p>
        <p>
          We advance all costs by offering a 'No Win, No Fee' guarantee to our
          clients. This means that our law firm takes care of the expenses
          involved in taking a lawsuit forward. We pledge to cover all
          reasonable case-related expenses, which could include everything from
          filing fees to expert testimony if a case goes to trial.
        </p>
        <p>
          The 'No Win, No Fee' system ensures that our clients do not pay for
          our services unless we win their case. We see this as an essential
          service. It makes our significant legal skills and resources available
          to everyone and allows our Newport Beach DUI accident attorneys to
          help those who need it most.
        </p>

        <h2>Bisnar Chase: Top-Rated DUI Attorneys Based in Newport Beach</h2>
        <hr />

        <p>
          Our law firm has been based in Newport Beach for more than 40 years,
          since its formation in 1978. Since then, Bisnar Chase has gone from
          strength to strength, handling thousands of cases and helping plenty
          of DUI injury victims.
        </p>
        <p>
          Our attorneys are known for their tenacity and dedication to our
          clients. We are proud to be a part of the Newport Beach community and
          have worked hard to represent local residents, earning a reputation
          for success.
        </p>
        <p>
          There are plenty of personal injury attorneys out there, but we
          believe in our ability to provide superior representation, along with
          a personal touch.
        </p>
        <p>
          <u>Some of our honors include:</u>
        </p>
        <ul className="mb">
          <li>
            <strong> Top Avvo Rated Lawyers</strong>
          </li>
          <li>
            <strong> Top-rated SuperLawyers</strong>
          </li>
          <li>
            <strong> Top 100 Trial Lawyers in America</strong>
          </li>
          <li>
            <strong> Members of the American Board of Trial Advocates</strong>
          </li>
          <li>
            <strong> NADC Top 1% Lawyers Nationwide</strong>
          </li>
        </ul>

        <h2>How to Find the Best DUI Attorneys Near You</h2>
        <LazyLoad>
          <img
            src="/images/dui-images/dui-victims-lawyer-newport-beach.jpg"
            width="40%"
            className="imgright-fluid"
            alt="A DUI car crash victim sitting on the ground in front of the wrecked front end of their car."
            title="Best DUI Attorneys in Newport Beach"
          />
        </LazyLoad>

        <p>
          It is important to work with an attorney who you can trust, and who
          understands your case. This is especially true for a DUI injury
          victim. If you have been hurt due to the actions of a drunk driver,
          you need a DUI lawyer in Newport Beach who is going to ease the burden
          on your shoulders while you focus on recovery.
        </p>
        <p>
          It is always a good idea to do your research on a potential attorney
          before hiring them, making sure they are a good fit for your case.
        </p>
        <p>The best Newport Beach DUI lawyer or firm for your case will:</p>
        <ul>
          <li>
            <strong>
              {" "}
              Have worked on similar cases with a track record of success
            </strong>
          </li>
          <li>
            <strong> Have an excellent reputation</strong>
          </li>
          <li>
            <strong>
              {" "}
              Be willing to go to trial if that is the best option for the case
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Offer a 'No Win, No Fee' guarantee to protect their clients
            </strong>
          </li>
          <li>
            <strong> Be easy to communicate with</strong>
          </li>
          <li>
            <strong>
              {" "}
              Be flexible and responsive to the needs and wishes of their client
            </strong>
          </li>
        </ul>
        <p>
          Bisnar Chase ticks these boxes. Our successful lawyers are expert
          negotiators and always strive to secure the best settlement offers
          possible for their clients.
        </p>
        <p>
          However, sometimes the best choice is to go to trial, and our
          attorneys are equally comfortable in the courtroom. Just as
          importantly, they are easy to keep in touch with – unlike many lawyers
          – and are always responsive to the client's needs.
        </p>

        <h2>Bisnar Chase: Superior Representation</h2>
        <hr />

        <p>
          If you are in need of a top-rated DUI accident lawyer in Newport
          Beach, look no further than Bisnar Chase. We are dedicated to helping
          our clients and have a track record of achieving great results.
        </p>
        <p>
          Over the last four decades, our firm has developed a{" "}
          <b>96% success rate</b>, while collecting more than{" "}
          <b>$500 million</b> for our clients.
        </p>
        <p>
          If you are looking for a nearby{" "}
          <strong> Newport Beach DUI lawyer</strong>, or need an attorney to
          handle a case in or around the 92660 area, we want to help.
        </p>

        <div className="contact-us-border">
          <p>
            {" "}
            <Link to="tel:+1-949-203-3814">
              Call Bisnar Chase at (949) 203-3814{" "}
            </Link>{" "}
            for immediate help, visit our Newport Beach law offices, or click to{" "}
            <Link
              to="/contact"
              target="new"
              title="Contact Bisnar Chase Personal Injury Attorneys"
            >
              contact us{" "}
            </Link>{" "}
            for a free consultation.
          </p>
        </div>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
