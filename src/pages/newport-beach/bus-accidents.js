// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bus-accidents/newport-beach-bus-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Bus Accident Lawyer - Bisnar Chase"
        pageDescription="The Newport Beach bus accident lawyers of Bisnar Chase excel in helping bus crash victims. A bus accident can cause major injuries, but our attorneys can make sure victims get the compensation they deserve."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bus Accident Attorneys Newport Beach</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach bus accident lawyer"
            title="Newport Beach Bus Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been the victim of any type of bus accident, please
          contact the Newport Beach{" "}
          <Link to="/bus-accidents" target="new" title="Bus Accident Lawyers">
            bus accident lawyers
          </Link>{" "}
          of Bisnar Chase. Bus crashes can leave passengers with catastrophic
          injuries and may also lead to extreme property damage.
        </p>
        <p>
          Bus accidents can cause life-altering injuries in a variety of ways. A
          bus crash victim may be a passenger, or someone in another vehicle
          involved in a collision with the bus. No matter how an injury happens,
          victims can turn to a Newport Beach bus accident attorney for help.
        </p>
        <p>
          The law firm of Bisnar Chase has won more than $500 million dollars
          for our injury clients. Bisnar Chase's legal experts have been
          providing superior representation to victims for more than 40 years.
          We excel at ensuring our clients get the compensation they deserve.
        </p>
        <p>
          To find out if you have a case, contact our Newport Beach bus accident
          attorneys now. We offer a free consultation and a 'No Win, No Fee'
          guarantee.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814</Link>, or go to
          our <Link to="/contact">contact page</Link>.
        </p>

        <h2>How Do I Know If I Need a Bus Crash Attorney?</h2>
        <hr />
        <p>
          If you have been injured in a bus accident in or around Newport Beach
          area, including the <strong> 92660</strong> area, you may be in need
          of a bus crash lawyer.
        </p>
        <p>
          When you are not fully compensated for the damages you have faced
          after a public transportation accident then you might be left to pay
          out-of-pocket for hefty medical expenses. But an experienced{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Lawyers"
          >
            {" "}
            personal injury lawyer
          </Link>{" "}
          will be able to investigate your accident. Your attorney will examine
          the evidence and help you decide whether to make an injury claim.
        </p>
        <p>
          Hiring a bus accident lawyer can also help you maneuver through the
          complexities of a bus crash case. The law offices of Bisnar Chase are
          passionate about winning your case. We look at every possible angle to
          get you every penny of the compensation you deserve.
        </p>

        <h2>Types of Bus Accident Cases We Handle</h2>
        <hr />
        <p>
          There are several different types of buses on the roads around
          Southern California. In Orange County, you are likely to see
          everything from bright yellow school buses to city-run local public
          transport vehicles.
        </p>
        <p>The bus accident cases we handle include:</p>
        <ul>
          <li>
            <strong> School buses</strong>
          </li>
          <li>
            <strong> Public transportation and commuter buses</strong>
          </li>
          <li>
            <strong> Private commuter buses</strong>
          </li>
          <li>
            <strong> Party bus accidents</strong>
          </li>
          <li>
            <strong> Long-distance transport buses</strong>
          </li>
          <li>
            <strong> Tourist tour buses</strong>
          </li>
        </ul>
        <p>
          A bus accident counts as any kind of collision involving a bus. If you
          were a driver in a{" "}
          <Link
            to="/newport-beach/car-accidents"
            target="new"
            title="Newport Beach Car Accident Lawyers"
          >
            {" "}
            car accident
          </Link>{" "}
          caused by a bus, or a{" "}
          <Link
            to="/newport-beach/bicycle-accidents"
            target="new"
            title="Newport Beach Bicycle Accident Lawyers"
          >
            {" "}
            cyclist who was hit
          </Link>
          , you should contact a bus accident lawyer in Newport Beach for expert
          help.
        </p>
        <p>
          If you are not sure whether your crash counts as a bus accident, call
          Bisnar Chase now for guidance.
        </p>

        <h2>Bus Accident Statistics</h2>
        <hr />
        <LazyLoad>
          <img
            src="/images/bus-accidents/bus-crash-attorney-newport-beach.jpg"
            width="300"
            className="imgright-fixed"
            alt="Passengers sitting inside a public transit bus"
            title="Newport Beach Bus Crash Lawyers"
          />
        </LazyLoad>

        <p>
          There are far fewer bus crashes than car accidents across the U.S.
          every year, thanks largely to there being far fewer buses than cars on
          the roads.
        </p>
        <p>
          However, bus accidents are likely to result in more severe damage and
          injuries. Buses are extremely large and heavy vehicles. As a result,
          they are tougher to steer and stop quickly. They also have large blind
          spots.
        </p>
        <p>
          These are just some statistics showing how dangerous a bus accident
          can be:
        </p>
        <ul>
          <li>A standard city bus measures up to 45 feet long.</li>
          <li>
            School buses can weigh up to about 36,000 lbs – 10x the average
            weight of a midsize car.
          </li>
          <li>
            According to the Federal Motor Carrier Safety Administration
            (FMCSA), there were nearly{" "}
            <Link
              to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2017"
              target="new"
            >
              5,000 fatal crashes involving large trucks and buses
            </Link>
            in 2017.
          </li>
          <li>
            In 2017 there were 116,000 crashes involving trucks and buses which
            resulted in injuries.
          </li>
          <li>
            Of all recorded bus crashes, 40% involve school buses and 35%
            involve transit buses.
          </li>
        </ul>

        <h2>Types of Bus Crashes in Newport Beach – Who Is Responsible?</h2>
        <hr />
        <p>
          All crashes are different, and there are several potentially liable
          parties who could be responsible for a bus accident.
        </p>
        <p>These include:</p>
        <ul>
          <li>
            <b>
              <u>The Bus Driver</u> –
            </b>{" "}
            A bus accident can be caused by negligence on the part of the bus
            driver. Negligent bus driver behavior which can result in an injury
            could include driving while fatigued or intoxicated. Bus drivers
            also need to have the required training to operate large specialist
            vehicles.
          </li>

          <li>
            <b>
              <u>Third Party Drivers</u> –
            </b>{" "}
            In many cases, bus accidents are caused by the dangerous or reckless
            driving of other people on the road.
          </li>

          <li>
            <b>
              <u>Bus Operating Company</u> –
            </b>{" "}
            In some cases, the company running the bus service may be liable.
            This might be the case if they have knowingly used a sub-par driver,
            have not completed proper maintenance on the vehicle or provided the
            correct equipment.
          </li>

          <li>
            <b>
              <u>Bus Manufacturer/Maintenance Workers</u> –
            </b>{" "}
            Accidents can result from technical or mechanical faults as a result
            of manufacturer error. Problems can also arise if proper vehicle
            maintenance is not carried out.
          </li>

          <li>
            <b>
              <u>Passenger Behavior</u> –
            </b>{" "}
            In rare cases, passenger behavior onboard a bus can cause driver
            distractions and result in a bus accident.
          </li>
        </ul>
        <p>
          Bus accidents can involve anything from a{" "}
          <Link
            to="/newport-beach/pedestrian-accidents"
            target="new"
            title="Newport Beach Pedestrian Accident Lawyer"
          >
            vehicle striking a pedestrian
          </Link>
          or cyclist in Newport Beach, to a bus colliding with another vehicle
          or crashing into an obstacle in or alongside the road.
        </p>
        <p>
          Riding on a bus can and sometimes does lead to serious injuries or
          even{" "}
          <Link
            to="/newport-beach/wrongful-death"
            target="new"
            title="Newport Beach Wrongful Death Lawyer"
          >
            wrongful death
          </Link>
          .
        </p>
        <p>
          Your Newport Beach bus accident lawyer will be able to guide your
          lawsuit based on the evidence of your case.
        </p>

        <h2>Factors Affecting Newport Beach Bus Crash Claims</h2>
        <LazyLoad>
          <img
            src="/images/bus-accidents/newport-beach-bus-accident-injury-attorney.jpg"
            width="40%"
            className="imgleft-fluid"
            alt="A parked California school bus"
            title="Bus Crash Compensation Newport Beach"
          />
        </LazyLoad>

        <p>
          Whether the accident involves a person on the bus injured during a
          crash, a car driver hit by a bus, or a pedestrian or bicycle rider who
          was injured in a bus crash, each type of case brings its own
          challenges to the victim and the attorney. But experienced personal
          injury attorneys understand the laws regarding bus crash injuries and
          can help you gain a fair settlement no matter the circumstances of
          your accident.
        </p>
        <p>
          Whether the bus is privately or publicly owned will affect your case.
          No matter who is operating the bus, a duty of care is owed to the
          passengers. However, with city-run public buses, there might be a
          shorter statute of limitations. It is always best to contact a Newport
          Beach bus accident lawyer as soon as possible.
        </p>
        <p>
          As with any vehicle crash injury, medical costs can soar. But this is
          not the only factor which can impact a victim's compensation in a bus
          accident claim. Others include:
        </p>
        <ul>
          <li>
            <strong> Type of injury and level of pain and suffering</strong>
          </li>
          <li>
            <strong> Medical expenses</strong>
          </li>
          <li>
            <strong> Lost earnings and the ability to work</strong>
          </li>
          <li>
            <strong> Property damage</strong>
          </li>
        </ul>
        <p>
          An experienced Newport Beach Bus Accident Lawyer will be able to
          maximize your compensation by assessing all of these aspects, and
          more. Our attorneys are experienced when it comes to negotiating with
          bus companies and insurance companies for our clients.
        </p>
        <p>
          If a case cannot be resolved through a settlement, our bus crash trial
          lawyers can also offer top-quality legal representation in the
          courtroom.
        </p>

        <h2>Common Bus Accident Injuries in Newport Beach</h2>
        <hr />
        <p>
          When a bus crash happens, it often has dire consequences for the
          victims.
        </p>
        <p>
          The following are common injuries that bus crash victims suffer from:
        </p>
        <p>
          <strong> Spinal Cord Injury:</strong> A{" "}
          <Link
            to="/catastrophic-injury/spinal-cord-injury"
            target="new"
            title="California Spinal Cord Injury Lawyer"
          >
            spinal cord injury
          </Link>{" "}
          can lead to devastating physical and emotional outcomes. Many people
          who suffer from spinal trauma will be left with a{" "}
          <Link
            to="https://www.christopherreeve.org/living-with-paralysis/newly-paralyzed/how-is-an-sci-defined-and-what-is-a-complete-vs-incomplete-injury"
            target="_blank"
          >
            complete or incomplete spinal cord injury
          </Link>
          . An incomplete spinal cord fracture is an injury that leaves the
          accident victim partially paralyzed. This means that the individual
          loses sensation or movement in a certain part of the body.
        </p>
        <p>
          A complete spinal cord injury involves a person being completely
          paralyzed from the point of their injury down. If a person has
          suffered an injury directly at the neck, then they may lose sensation
          and the ability to move any body part from the neck down. Spinal cord
          injury victims can accumulate huge medical costs – both immediate, and
          in ongoing care. These costs can include rehabilitation and equipment.
        </p>
        <p>
          <strong> Whiplash:</strong> A person suffering from minor or extreme
          neck pain after a bus incident can possibly be suffering from{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/whiplash/symptoms-causes/syc-20378921"
            target="_blank"
          >
            {" "}
            whiplash
          </Link>
          . Whiplash is when the neck experiences a strong impact that forces
          the neck to move back and forth. Symptoms can include extreme pain to
          the neck and jaw, ears ringing or feeling stiffness in the shoulder,
          and can last for months.
        </p>

        <LazyLoad>
          <img
            src="/images/bus-accidents/bus-accident-whiplash-injury.png"
            width="100%"
            alt="A 3D rendering of how a whiplash injury occurs"
            title="Whiplash Injury Lawyers"
          />
        </LazyLoad>
        <p>
          <strong> Lacerations, Bruising and Broken Bones:</strong> Many people
          suffer from broken bones when they are involved in a bus accident. The
          force and the weight of the bus can lead many victims to strike the
          inside of the bus violently. Bus accident victims can also suffer from
          lacerations – from flesh wounds to deep cuts. This may result in a
          victim requiring stitches or surgery.
        </p>
        <p>
          <strong> Internal Injuries and Head Injuries:</strong> The force of a
          bus accident can result in severe internal injuries. In addition, many
          buses are not fitted with seatbelts. This means that a serious
          collision can easily cause a passenger to fall and hit their head.
          Injuries involving the head and brain can be particularly serious and
          should be treated immediately.
        </p>

        <h2>What Evidence Does a Newport Beach Bus Crash Lawyer Need?</h2>
        <hr />
        <p>
          You should provide your attorney with as much evidence as possible if
          you have been involved in a bus accident in Newport Beach. The more
          evidence they have, the better the case they can make on your behalf.
          Evidence can include:
        </p>
        <ul>
          <li>
            An account of the incident supported by pictures and video footage
            from the scene.
          </li>
          <li>
            Pictures of your injuries, as well as supporting medical records.
          </li>
          <li>A police report from the incident.</li>
          <li>Eyewitness statements, or witness contact details.</li>
          <li>
            Proof of medical expenses and lost earnings related to the crash.
          </li>
        </ul>

        <h2>The Bisnar Chase Difference</h2>
        <hr />
        <p>
          Bisnar Chase is based in the Newport Beach community and has been
          helping residents for more than four decades. We have successfully
          helped more than 12,000 clients over that time, collecting more than{" "}
          <b>$500 million</b> for them in verdicts and settlements.
        </p>
        <p>
          Our mission statement is to provide superior representation with a
          compassionate approach. What's more, our 'No Win, No Fee' guarantee
          ensures that our services are open to everyone. We only charge a fee
          if we win your case.
        </p>
        <p>
          If you are looking for the best Newport Beach bus accident lawyers,
          look no further than Bisnar Chase. Our expert attorneys have a huge
          amount of experience helping victims across the area. If you have
          suffered an injury in <strong> 92660</strong>, <strong> 92661</strong>
          , <strong> 92662</strong>, <strong> 92663</strong>, or surrounding
          areas, get in touch now.
        </p>
        <p>
          Call our <strong> Newport Beach bus accident lawyers</strong> for
          immediate help at <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>
          , or click to{" "}
          <Link to="/contact" target="new">
            contact us
          </Link>{" "}
          now.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
