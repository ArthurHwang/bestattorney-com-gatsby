// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "employment/newport-beach-employment-lawyer.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Employment Lawyers - Labor Law Experts | Bisnar Chase"
        pageDescription="The Newport Beach employment lawyers of Bisnar Chase win compensation and justice for wronged employees. If you have been the victim of discrimination, a wage and hour dispute, wrongful termination, or any workplace violation, call now for a free consultation with an expert employment  law attorney in Newport Beach."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Employment Law Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach Employment Lawyers"
            title="Newport Beach Employment Law Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced Newport Beach employment lawyers at Bisnar Chase are
          passionate about protecting the rights of workers and holding
          employers accountable.
        </p>
        <p>
          Our skilled attorneys have won a huge range of employment lawsuits in{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Attorneys"
          >
            Newport Beach
          </Link>
          , ranging from wage and hour disputes to discrimination cases. We are
          here to make sure employers – from large firms to smaller companies –
          cannot take advantage of their staff.
        </p>
        <p>
          For immediate legal help, contact the Newport Beach employment
          attorneys of Bisnar Chase. We offer a free, no-obligation case review.
          Our experts can provide the guidance you need to decide how to take
          your case forward.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814</Link>, or go to
          our <Link to="/contact">contact page</Link> for expert help now.
        </p>

        <h2>Your Employee Rights in California</h2>
        <LazyLoad>
          <img
            src="/images/employment-lawyers.jpg"
            alt="Employment Attorneys Newport Beach"
            title="Newport Beach workplace lawsuit"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Employees across Southern California have basic rights in the
          workplace. Unfortunately, we have seen plenty of cases in which their
          employers abuse these rights.
        </p>

        <p>
          Workers should reasonably expect to be compensated for the hours they
          work – including overtime when applicable. Everyone is also legally
          entitled to break and lunch periods when they work a certain number of
          hours.
        </p>
        <p>
          In addition, employees should be properly equipped to perform their
          duties, and have the right to work in an environment which is free
          from discrimination. If you believe your workplace rights have been
          violated, contact an employment lawyer in Newport Beach for help.
        </p>

        <h2>What Do Newport Beach Employment Lawyers Do?</h2>

        <p>
          When you contact Bisnar Chase, you will get a free consultation to
          explore the circumstances of your case, combined with expert guidance.
        </p>
        <p>
          Once one of our Newport Beach employment lawyers takes a case on, they
          will work to establish evidence supporting your claim. They will then
          use this evidence to either:
        </p>
        <ul>
          <li>
            <b>
              Negotiate a settlement, with the offending employer paying out an
              acceptable amount of compensation, OR;
            </b>
          </li>

          <li>
            <b>Take the case to court and fight for a favorable decision</b>
          </li>
        </ul>

        <p>
          We are adept at handling wage and hour violations, discrimination
          cases, abusive work environments, and wrongful terminations. If the
          evidence indicates that yours is not an isolated case, we are also
          skilled at handling class action lawsuits.
        </p>

        <h2>Our California Employment Lawsuit Victories</h2>

        <p>
          We are proud of the results we have achieved for our clients in
          various employment lawsuit cases. You can count on us to fight for the
          maximum amount of compensation possible for you.
        </p>
        <p>Some of our recent employment lawsuit results include:</p>

        <center>
          <table>
            <tr></tr>
            <tr>
              <td>
                <b>$2,000,000</b>
              </td>
              <td>
                <b>Wage and Hour</b>
              </td>
            </tr>
            <tr>
              <td>
                <b>$1,478,819</b>
              </td>

              <td>
                <b>Wage and Hour</b>
              </td>
            </tr>
            <tr>
              <td>
                <b>$600,000</b>
              </td>

              <td>
                <b>Wage and Hour</b>
              </td>
            </tr>
            <tr>
              <td>
                <b>$575,000</b>
              </td>

              <td>
                <b>Wage and Hour</b>
              </td>
            </tr>
            <tr>
              <td>
                <b>$250,000</b>
              </td>

              <td>
                <b>Wage and Hour</b>
              </td>
            </tr>
          </table>
        </center>

        <p>
          The level of compensation a victim might receive in an employment
          dispute legal case will come down to the circumstances of the specific
          case. But our Newport Beach labor lawyers are experts when it comes to
          getting the best results possible.
        </p>

        <h2>Types of Employment Lawsuits</h2>

        <p>
          There is a wide variety of ways in which an employee's rights can be
          violated. How do you know whether you might have a case for an
          employment lawsuit? Read our handy guide below to learn more about the
          different types of workplace violations you might suffer in Newport
          Beach.
        </p>
        <h3>
          <u>Wage and Hour Disputes in Newport Beach</u>
        </h3>
        <p>
          Some of the most common employment disputes in Newport Beach involve{" "}
          <Link
            to="/employment-law/wage-and-hour"
            target="new"
            title="California Wage and Hour Lawyers"
          >
            wage and hour violations
          </Link>
          . This can include:
        </p>
        <ul>
          <li>
            <b>Paying an employee less than the minimum wage</b>
          </li>
          <li>
            <b>A failure to pay overtime when overtime work has been done</b>
          </li>
          <li>
            <b>Paying lower than the contracted rate for overtime work</b>
          </li>
          <li>
            <b>Making illegal deductions from a worker's paycheck</b>
          </li>
        </ul>
        <p>
          Investigations have shown that a huge percentage of employees are not
          paid the correct amount of overtime, while others are not compensated
          for overtime or off-the-clock work at all. Read more about the{" "}
          <Link
            to="https://www.dol.gov/whd/overtime_pay.htm"
            target="new"
            title="Overtime Pay Information"
          >
            overtime pay regulations
          </Link>
          .
        </p>
        <h3>
          <u>Legal Break Times</u>
        </h3>
        <p>
          Workers are entitled to breaks and meal periods. This means that
          employers must allow a paid 10-minute break for every four hours the
          employee works by law. The employee is also guaranteed a lunch break
          of at last 30 minutes, which must be taken before their fifth hour of
          working ends.
        </p>
        <h3>
          <u>Discrimination Lawsuit</u>
        </h3>
        <p>
          Some labor and employment law cases include workplace discrimination.
          This is when negative actions were taken against a worker or potential
          employee due to their race, age, ethnicity, disability, sex or
          religion.
        </p>

        <p>
          It is against the law for these factors to be used in decisions
          regarding hiring, pay, and promotions, among others.
        </p>

        <h3>
          <u>Hostile Working Environment </u>
          <LazyLoad>
            <img
              src="/images/employment/employment-lawyers-in-newport-beach.jpg"
              width="40%"
              className="imgright-fluid"
              alt="Sexual harassment in the office - a boss touches his female employee's shoulder inappropriately"
              title="Newport Beach Workplace Discrimination Attorneys"
            />
          </LazyLoad>
        </h3>
        <p>
          Any worker forced to suffer a{" "}
          <Link
            to="/employment-law/hostile-work-environment-attorney"
            target="new"
            title="California Hostile Work Environment Lawyer"
          >
            hostile working environment
          </Link>{" "}
          may have a legal claim. This can refer to unreasonable actions taken
          which cause employee discomfort. A couple of common examples include{" "}
          <Link
            to="/employment-law/sexual-harassment"
            target="new"
            title="California Sexual Harassment Lawyers"
          >
            sexual harassment
          </Link>{" "}
          and workplace bullying.
        </p>
        <p>
          An employer may be culpable if they are directly responsible for this
          behavior, or if they knew about it and did not do anything to stop it.
        </p>
        <h3>
          <u>Wrongful Termination</u>
        </h3>
        <p>
          This refers to any kind of dismissal which is carried out for
          illegitimate or illegal reasons. There may be a{" "}
          <Link
            to="/employment-law/wrongful-termination"
            target="new"
            title="California Wrongful Termination Lawyers"
          >
            wrongful termination
          </Link>{" "}
          case if the firing:
        </p>
        <ul>
          <li>
            <b>Goes against company guidelines or public policy</b>
          </li>
          <li>
            <b>Is a result of discrimination</b>
          </li>
          <li>
            <b>Is a result of employer retaliation</b>
          </li>
          <li>
            <b>
              Was caused by a worker refusing to comply with unreasonable or
              illegal demands
            </b>
          </li>
        </ul>
        <p>
          If any of these situations have been suffered by an employee in the
          workplace, they should seriously consider pursuing legal action.
          Moving forward with employment litigation can result in the victim
          receiving compensation, but it is also important to hold the offenders
          accountable.
        </p>

        <h2>Labor Laws in California</h2>
        <LazyLoad>
          <img
            src="/images/employment/newport-beach-labor-lawyers.jpg"
            width="35%"
            className="imgright-fluid"
            alt="An overworked employee falls asleep on his desk next to a large pile of paperwork"
            title="Labor Law Attorneys in Newport Beach"
          />
        </LazyLoad>

        <p>
          Employees in California receive a high level of protection from the
          state's labor laws. The{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Fair_Labor_Standards_Act_of_1938"
            target="new"
            title="The Fair Labor Standards Act"
          >
            Fair Labor Standards Act (FLSA)
          </Link>{" "}
          provides the basis for many employment laws, augmented by
          California-specific legislation.
        </p>
        <p>
          The FLSA was created in 1938 to prevent employers from forcing their
          employees to work excessive hours. It established a national minimum
          wage and guaranteed 1.5x pay for overtime in certain jobs.
        </p>
        <p>
          This employment act has been added to and expanded on by further laws
          over the years. For instance, the Portal-to-Portal Act of 1947
          established that all work-related activities, performed for the
          employer's benefit, must be paid – regardless of when they are
          performed.
        </p>
        <p>
          The following are just some of the important points of California
          employment law which apply to workers in Newport Beach.
        </p>
        <ul>
          <li>
            <b>
              As of 2019, the minimum wage is $11 per hour for companies with 25
              or fewer employees. Minimum wage is $12 for companies with 26 or
              more employees
            </b>
          </li>

          <li>
            <b>
              Minimum wage is scheduled to increase by $1 each year until 2023
            </b>
          </li>

          <li>
            <b>
              Overtime applies when an employee works more than 8 hours per day,
              or 40 hours per week
            </b>
          </li>

          <li>
            <b>
              8/40 hour overtime rates should be 1.5x the employee's normal pay
            </b>
          </li>

          <li>
            <b>
              Employees will be entitled to 2x normal pay if they work overtime
              amounting to: 12+ hours in a day, or 8+ hours on 7 days in a row
            </b>
          </li>

          <li>
            <b>
              Some positions are exempt from the overtime regulations. These can
              include executives, admin staff, and computer software pros
            </b>
          </li>

          <li>
            <b>
              All working hours must be paid, and employers cannot force their
              staff to work outside of those hours without overtime
            </b>
          </li>

          <li>
            <b>
              All expenses which are necessary to a worker performing their
              duties must be reimbursed by the employer
            </b>
          </li>

          <li>
            <b>Employers are NOT required to provide paid vacation time</b>
          </li>
        </ul>
        <p>
          For more information on any of these California laws, contact a
          specialist Newport Beach employment law attorney. They will be able to
          provide more detail on the laws applicable to you, and help you decide
          how to proceed.
        </p>

        <h2>How Much Does a Newport Beach Employment Lawyer Cost?</h2>

        <p>
          When it comes to employment law litigation, you need a specialist
          employment attorney on your side. But many people are put off because
          they think it will be expensive to hire a lawyer. That does not have
          to be the case.
        </p>
        <p>
          At Bisnar Chase we offer a 'No Win, No Fee' guarantee. Our law firm
          will advance all necessary costs relating to your case. If we do not
          win, you will not have to pay. This system ensures our services are
          open to everyone who needs them.
        </p>
        <p>
          We have many years of experience representing employees who have been
          wronged by their employers, and believe in our ability to help win
          your case too.
        </p>

        <h2>Finding the Best Newport Beach Employment Lawyers Near You</h2>

        <p>
          Bisnar Chase is a top-quality law group which prides itself on
          providing expert help to wronged employees in Newport Beach and across
          Orange County. If you need an employment attorney in or around the{" "}
          <strong> 92660</strong> area, we can help.
        </p>
        <p>
          We believe that our track record speaks for itself. We have a{" "}
          <b>96% success rate</b> and have collected more than{" "}
          <b>$500 million</b> for our clients.
        </p>
        <p>
          Our employment lawyers represent clients who have suffered due to the
          illegal actions of their employers. They will do everything possible
          to get those victims as much compensation as possible, and hold the
          offending employers accountable.
        </p>

        <center>
          <div className="contact-us-border">
            <p>
              Call the Newport Beach employment lawyers of Bisnar Chase now on{" "}
              <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> for
              immediate help, or click to{" "}
              <Link
                to="/contact"
                target="new"
                title="Contact Bisnar Chase Personal Injury Attorneys"
              >
                contact us{" "}
              </Link>{" "}
              and arrange a free initial consultation.
            </p>
          </div>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  td,
  th {
    border: 1px solid #050505;
    text-align: center;
    padding: 8px;
  }

  tr:nth-child(even) {
    background-color: #dddddd;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
