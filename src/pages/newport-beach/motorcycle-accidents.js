// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/newport-beach-motorcycle-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Motorcycle Accident Lawyer – Bisnar Chase"
        pageDescription="Motorcycle accidents are very common and can result in catastrophic injuries or death. The Newport Beach motorcycle accident lawyers of Bisnar Chase have been representing victims and winning motorcycle crash cases for more than 40 years. Call our Newport Beach Beach motorcycle crash attorneys for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach motorcycle accident lawyer"
            title="Newport Beach Motorcycle Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been the victim of a motorbike crash, the Newport Beach
          motorcycle accident lawyers of Bisnar Chase can help. Our team of
          skilled and experienced trial lawyers will fight to make sure you get
          the compensation you deserve.
        </p>
        <p>
          Motorbike accidents happen more often than you might think in Newport
          Beach. A motorcycle is an extremely powerful machine, capable of
          incredible top speeds and the ability to maneuver through traffic.
          Unfortunately, they also offer little protection to riders, and a
          crash can leave victims with severe injuries.
        </p>
        <p>
          When you or a loved one suffer an injury in a motorbike crash, it can
          be life-altering. But our Newport Beach motorcycle accident attorneys
          are here to help. Contact us now for expert guidance on your case. If
          you have been hurt, we can help you find justice.
        </p>
        <p>
          {" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814 </Link> for
          immediate help from our motorcycle crash lawyers in Newport Beach, or
          go to our <Link to="/contact">contact page</Link> to arrange a free
          case review.
        </p>

        <h2>Causes of Motorcycle Crashes in Newport Beach</h2>

        <p>
          A large proportion of motorbike crashes involve collisions with cars
          and other vehicles. Motorcycles are smaller and harder to see than
          most other vehicles, and many accidents happen because a careless car
          driver did not even see the biker. Some of the most common motorcycle
          crash causes include:
        </p>
        <ul>
          <li>
            <b>Dangerous Turns</b>
            <p>
              Many crashes happen due to a vehicle making a left turn across
              oncoming traffic with the driver failing to see an approaching
              motorbike.
            </p>
          </li>
          <li>
            <b>Lane Changes</b>
            <p>
              It is common for motorcycles to be hit by cars as the larger
              vehicle is changing lanes, particularly on fast-moving freeways.
              The biker will often be hidden in the car's blind spot.
            </p>
          </li>
          <li>
            <b>Road Conditions</b>
            <p>
              Dangerous road conditions, such as a slick surface, or a cracked
              and uneven roadway, can be especially dangerous for motorcycle
              riders.
            </p>
          </li>
          <li>
            <b>Hazards</b>
            <p>
              Inanimate hazards, such as debris in the road, or vehicles parked
              where they shouldn't be, can be especially dangerous due to the
              high speeds and lack of protection offered by motorcycles.
            </p>
          </li>
        </ul>
        <p>
          Some crashes involving motorcycles happen due to people driving under
          the influence. But many are simply a result of carelessness, with
          drivers failing to see a motorbike approaching.
        </p>
        <p>
          While they are much less powerful, many of these same hazards apply to
          scooter riders. Scooter engine sizes are usually capped at about
          250cc, but a crash can equally leave a rider with serious injuries.
        </p>

        <h2>Motorcycle Accident Injuries</h2>

        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/motorcycle-accident-lawyer-newport-beach.jpg"
            width="40%"
            alt="A helmet lying in the street next to a crashed motorcycle"
            className="imgright-fluid"
            title="Lawyers for Motorbike Crash Victims in Newport Beach"
          />
        </LazyLoad>

        <p>
          A motorcycle crash can cause severe injuries. In fact, motorcycle
          riders are far more likely to sustain serious injuries in a collision
          than the driver of a car. This is for multiple reasons. For one thing,
          motorbikes are inherently less stable than four-wheeled vehicles. They
          also lack the protection of an enclosed chassis. When a bike is
          involved in a collision, there is nothing to absorb the impact or stop
          the rider being thrown to the ground at high speeds.
        </p>

        <p>
          Unfortunately, if a car accident in Newport Beach involves a
          motorcycle, the rider is almost guaranteed to suffer an injury – or
          worse.
        </p>
        <p>
          The laws surrounding protective equipment for motorcycles varies by
          state. In the state of California, all motorcycle riders{" "}
          <Link
            to="https://www.dmv.ca.gov/portal/dmv/?1dmy&urile=wcm:path:/dmv_content_en/dmv/pubs/dl655/mcycle_htm/preparing"
            target="new"
            title="California Motorbike Helmet Laws"
          >
            must wear a helmet by law
          </Link>
          . However, it is common for motorcyclists in Newport Beach to ditch
          any other protective gear, such as thick leather jackets and pants.
        </p>
        <p>
          This clothing can make biking conditions unbearable when the high
          temperatures hit in Southern California. But riding without protective
          clothing also leaves bikers defenseless in the event of an accident.
        </p>
        <p>Some of the most common injuries include:</p>
        <ul>
          <li>
            <b>Cuts and bruises</b>
          </li>
          <li>
            <b>
              Torn flesh – when a rider hits the road at high speeds with
              exposed skin
            </b>
          </li>
          <li>
            <b>Broken bones</b>
          </li>
          <li>
            <b>Serious burns</b>
          </li>
          <li>
            <b>Torn ligaments and tendons</b>
          </li>
          <li>
            <b>Severe trauma including internal injuries</b>
          </li>
          <li>
            <b>
              {" "}
              <Link
                to="/catastrophic-injury/spinal-cord-injury"
                target="new"
                title="California Spinal Cord Injury Lawyer"
              >
                Spinal cord injuries
              </Link>
            </b>
          </li>
          <li>
            <b>
              {" "}
              <Link
                to="/newport-beach/brain-injury"
                target="new"
                title="Newport Beach Brain Injury Lawyers"
              >
                Brain injuries{" "}
              </Link>
            </b>
          </li>
          <li>
            <b>Paralysis</b>
          </li>
          <li>
            <b>
              {" "}
              <Link
                to="/newport-beach/wrongful-death"
                target="new"
                title="Newport Beach Wrongful Death Lawyers"
              >
                Death{" "}
              </Link>
            </b>
          </li>
        </ul>
        <p>
          There is a wide spectrum of possible motorcycle accident injuries. No
          matter how badly you were hurt you should contact an experienced
          motorbike accident lawyer in Newport Beach for the best help and
          guidance.
        </p>
        <p>
          Our{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Attorneys"
          >
            Newport Beach personal injury lawyers
          </Link>{" "}
          specialize in cases in which victims have suffered physical harm or
          trauma. Call now for immediate assistance.
        </p>

        <h2>Newport Beach Motorcycle Accident Statistics</h2>

        <p>
          Motorcycle accidents are responsible for a high number of road
          injuries in Orange County. According to the{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/CollisionReports.jsp"
            target="new"
            title="California Road Accident Statistics"
          >
            California Statewide Integrated Traffic Records System (SWITRS)
          </Link>
          :
        </p>
        <ul>
          <li>
            There were more than 1,000 collisions involving motorbikes in Orange
            County in 2018
          </li>
          <li>
            While some crashes only resulted in property damage, a huge 82%
            caused injury or death
          </li>
          <li>
            Motorcycle accidents led to 42 injuries and 1 fatality in Newport
            Beach in 2018 – as well as 1,015 injuries and 25 deaths across
            Orange County
          </li>
          <li>
            In 2017, more than 5,000 riders were{" "}
            <Link
              to="https://www.iihs.org/topics/motorcycles#by-the-numbers"
              target="new"
              title="Motorcycle Accident Fatality Statistics"
            >
              killed in motorbike crashes{" "}
            </Link>{" "}
            in the U.S.
          </li>
          <li>
            Statistics show that you are nearly 30 times more likely to be
            injured if you are riding a motorbike, compared to driving a car
          </li>
        </ul>

        <h2>Why Hire a Newport Beach Motorcycle Accident Lawyer?</h2>
        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/newport-beach-motorcycle-crash-attorney-near-me.jpg"
            width="45%"
            className="imgright-fluid"
            alt="The wreckage of a motorbike on fire at the side of a freeway after an accident"
            title="Newport Beach Motorcycle Accident Injury Attorneys"
          />
        </LazyLoad>

        <p>
          If you or a loved one have suffered injuries in a motorbike crash, you
          deserve justice. Our motorcycle accident attorneys based in Newport
          Beach can offer expert help, giving you the best chance possible of
          legal success.
        </p>
        <p>
          Many people ask how long they should wait to hire a lawyer for a
          motorcycle accident case. The answer is that you should hire an
          attorney as soon as possible. There is a statute of limitations on
          such cases, and it is best to get the ball rolling quickly.
        </p>
        <p>
          Hiring a high-quality attorney can ease the burden on your shoulders.
          When you suffer a severe injury, you need to focus on recovery.
          Working with a motorcycle accident lawyer near you will allow you to
          do just that.
        </p>
        <p className="clearfix">
          Our experts will be able to guide you through every step of the legal
          process. They will be able to build a strong case for your motorcycle
          crash lawsuit, using evidence including:
        </p>
        <ul>
          <li>
            <b>Police reports</b>
          </li>
          <li>
            <b>Medical reports and proof of expenses</b>
          </li>
          <li>
            <b>Pictures and video footage from the scene of the accident</b>
          </li>
          <li>
            <b>Proof of injuries</b>
          </li>
          <li>
            <b>Witness statements</b>
          </li>
        </ul>
        <p>
          Our attorneys are adept at negotiation compensation settlements with
          opposing insurance companies and lawyers. However, sometimes an
          acceptable settlement cannot be agreed. The best Newport Beach
          motorcycle accident lawyers are also ready to take your case to trial
          when needed.
        </p>

        <h2>Experienced Trial Lawyers</h2>

        <p>
          At Bisnar Chase, we have plenty of trial experience. In fact, managing
          partner Brian Chase has received numerous trial awards – including
          Trial Lawyer of the Year. Brian Chase has been nationally recognized
          as one of the nation's top motorcycle trial attorneys. If your
          defendant's insurance company is not offering a fair settlement, we
          can take them to court and make them reassess their decision to
          lowball your claim.
        </p>
        <p>
          Our reputation as some of the best trial lawyers in the U.S. is
          well-known among insurance companies. This may allow us to settle your
          case far more simply because the insurance company is wary of taking
          us on in court. Brian has obtained millions of dollars for his clients
          over the years, even obtaining a record-breaking $24.7 million verdict
          for a client who suffered serious injuries.
        </p>

        <h2>Motorbike Crash Compensation</h2>
        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/motorbike-accident-compensation-newport-beach.jpg"
            width="50%"
            alt="An injured rider lies next to his scooter in the road after a crash"
            className="imgleft-fluid"
            title="Finding the Best Motorbike Crash Compensation Lawyers in Newport Beach"
          />
        </LazyLoad>

        <p>
          No two crashes are exactly the same and the amount of compensation a
          motorcycle accident victim might be awarded will always vary from case
          to case.
        </p>

        <p>
          Compensation will depend on a variety of factors. These can include
          the severity of a victim's injuries and the level of pain and
          suffering they have experienced, as well as their medical expenses and
          ongoing care costs. Beyond physical pain, the level of emotional
          trauma experienced during a crash can also play a part in the amount
          of compensation a victim can expect.
        </p>

        <p>
          Our Newport Beach motorbike accident attorneys are experienced in
          handling these kinds of cases, and will be able to work with you to
          target an acceptable compensation amount. We will always fight for the
          maximum amount possible for our clients.
        </p>

        <h2>Superior Representation with Bisnar Chase</h2>

        <p>
          Our law firm's goal is to offer superior representation to every
          client. We take great pride in serving Newport Beach – including
          residents in and around the
          <strong> 92660</strong> area – where we have been based for more than
          40 years.
        </p>
        <p>
          Bisnar Chase maintains an excellent <b>96% success rate</b> and has
          collected more than <b>$500 million</b> for the firm's many satisfied
          clients. We also offer a 'No Win, No Fee' guarantee, advancing all
          costs to make sure motorcycle accident victims do not pay a penny
          unless we win their case.
        </p>
        <p>
          Contact us today for a free consultation, and find out more about how
          Bisnar Chase can help you.
        </p>

        <center>
          <div className="contact-us-border">
            <p>
              Call our Newport Beach motorcycle accident lawyers at{" "}
              <Link to="tel:+1-949-203-3814"> (949) 203-3814 </Link> for
              immediate help, or click to{" "}
              <Link
                to="/contact"
                target="new"
                title="Contact Bisnar Chase - Personal Injury Attorneys"
              >
                contact us{" "}
              </Link>{" "}
              now.
            </p>
          </div>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
