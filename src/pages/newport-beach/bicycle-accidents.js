// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bike-accidents/newport-beach-bicycle-accident-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Bicycle Accident Lawyer – Bisnar Chase"
        pageDescription="Newport Beach is a cycling accident hotspot, but the Newport Beach bicycle accident lawyers of Bisnar Chase can help victims who have been injured. Call our skilled team of attorneys now for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Bicycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="newport beach bicycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Newport Beach is a beautiful city to cycle in, with stunning bays,
          beaches, and scenery. But cycling in the area can also be dangerous.
          If you have been injured in a bike crash, contacting a{" "}
          <strong> Newport Beach bicycle accident lawyer</strong> should be one
          of your top priorities.
        </p>
        <p>
          Bike accidents do not just result in scuffs and bruises. They can
          leave victims with serious and life-changing injuries. If that has
          happened to you, we can help. The law firm of Bisnar Chase has been
          representing clients in Newport Beach since 1978 for no-fault and
          serious bicycle injury cases.
        </p>
        <p>
          We know the local courts, defense teams and insurance companies, and
          have the resources and skills to get top compensation for your
          injuries. Contact us now for a free consultation – call{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>.
        </p>

        <h2>Newport Beach Bike Accident Statistics</h2>
        <p>
          It may be a great city for cycling, but Newport Beach is also a
          cycling accident hotspot.
        </p>
        <ul>
          <li>
            According to our{" "}
            <Link to="/resources/car-accident-statistics">
              Orange County car accident statistics research
            </Link>
            , Newport Beach has the highest rate of bicycle accidents in the
            area.
          </li>
          <li>
            Newport Beach bike accidents involving car or{" "}
            <Link to="/newport-beach/truck-accidents" target="new">
              truck crashes{" "}
            </Link>{" "}
            are highly dangerous, carrying a 10% greater chance of injury than
            collisions involving motorcycles.
          </li>
          <li>Of all Newport Beach cycling accidents, 97% result in injury.</li>
          <li>
            Nine cyclists have died in Newport Beach bicycle accidents in the
            last six years.
          </li>
          <li>
            Every six hours a cyclist is fatally injured in a bike accident.
          </li>
          <li>
            Approximately one million children are injured every year in
            bicycle-related accidents.
          </li>
          <li>
            Nearly half of all bicycle accidents involve children under age 16.
          </li>
        </ul>

        <h2>Bicycle Accidents on the Rise in Newport Beach</h2>
        <LazyLoad>
          <img
            src="/images/bike-accidents/bike-accident-lawyers-newport-beach.jpg"
            width="52%"
            className="imgleft-fluid"
            alt="A cyclist sitting in the road next to his bike and holding his injured knee after being hit by a car."
          />
        </LazyLoad>
        <p>
          Newport Beach has been named one of the{" "}
          <Link to="/newport-beach/worst-city-for-bicyclists">
            {" "}
            worst cities in California for bicycle accidents
          </Link>{" "}
          – and with good reason.
        </p>

        <p>
          Bicycle accidents in Newport Beach have become very common, as more
          people take to the streets on two wheels to avoid high gas prices, get
          exercise, enjoy the scenic ocean views, or to get around the heavy
          peninsula traffic.
        </p>
        <p>
          But with a higher volume of cyclists comes greater danger of bike
          accidents and injuries. While bicycling is a fun, active sport that
          many people enjoy, the crash statistics are grim when you compare them
          to those for pedestrians or other forms of transportation.
        </p>
        <p>
          There has been an upsurge in bicycle-related accidents in the past few
          years, probably due to the fact that more people are out cycling than
          ever before.
        </p>

        <h2>Common Causes of Bike Accidents in Newport Beach</h2>
        <p>
          Bicycle accident injuries most commonly occur when a cyclist comes
          into contact with a motor vehicle. These are some of the key crash
          causes which create the need for a bike accident lawyer:
        </p>
        <ul>
          <li>
            A vehicle turning in front of a bike:
            <ul>
              <li>
                A car or truck pulling out of a side street or driveway in front
                of a bike.
              </li>
              <li>
                A vehicle overtaking a cyclist and then turning right in front
                of them.
              </li>
              <li>
                A driver making a left turn as a bike approaches from the
                opposite direction.
              </li>
            </ul>
          </li>

          <li>
            A car or truck rear-ending a bike – common when bikes have to cross
            lanes to pull around a parked car.
          </li>

          <li>
            A car or truck moving too far to the right of the lane and colliding
            with the cyclist.
          </li>

          <li>
            Someone in a parked vehicle opening their door in front of a bike.
          </li>

          <li>
            Some bike accidents occur due to a driver steering erratically,
            speeding, or being under the influence of drugs or alcohol.
          </li>
        </ul>
        <p>
          The most common causes of bicycle accidents usually involve fault on
          the part of the driver. Cyclists must observe the rules of the road
          and behave in a safe manner, but drivers in cars and trucks also have
          a responsibility to respect a bike's place on the road.
        </p>
        <p>
          <i>Non-vehicle-related accidents include:</i>
        </p>
        <ul>
          <li>Bike crashes due to the poor conditions of the road.</li>
          <li>Awkward falls.</li>
          <li>Cyclist error or crashing into a fixed object.</li>
          <li>Hitting a dog or swerving to avoid a collision.</li>
        </ul>

        <h2>Bicycle Accident Injuries</h2>
        <p>
          When most people think of riding a bicycle, the injuries they would
          associate with it are fairly minor. But bike accidents can result in
          serious injuries, especially when a vehicle is involved.
        </p>
        <p>
          <i>These include:</i>
        </p>
        <ul>
          <li>Cuts, scrapes and bruising</li>
          <li>Broken bones</li>
          <li>
            {" "}
            <Link to="/head-injury" target="new">
              Head injuries{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/catastrophic-injury/back-injury-lawyer" target="new">
              Back{" "}
            </Link>{" "}
            and{" "}
            <Link to="/catastrophic-injury/spinal-cord-injury" target="new">
              {" "}
              spinal cord injuries{" "}
            </Link>
          </li>
          <li>Facial damage</li>
          <li>Wrongful death</li>
        </ul>
        <p>
          If such an injury is caused by a driver's negligence, or another
          party, Bisnar Chase's experienced bicycle accident attorneys can fight
          to secure compensation and justice for you.
        </p>

        <h2>What to do After a Bike Crash in Newport Beach</h2>
        <p>
          If you are involved in a Newport Beach bike accident, your top
          priority must be to get to safety and seek medical attention. After
          that, you can focus on steps which will help you take legal action.
        </p>
        <ol>
          <li>
            <span>
              <b>Get to safety.</b> If you are able to move after a bicycle
              accident, you should get out of the road to make sure you do not
              suffer further injury.
            </span>
          </li>

          <li>
            <span>
              <b>Get treatment.</b> Do not refuse treatment at the scene and
              follow up with a doctor if necessary. This will ensure your
              safety, and you can request any medical reports if you decide to
              take legal action.
            </span>
          </li>

          <li>
            <span>
              <b>Call the police.</b> A police officer will be able to secure
              the scene and record relevant details. You can later request the
              police report to support your personal injury case.
            </span>
          </li>

          <li>
            <span>
              <b>Document the scene.</b> Take pictures or video footage of the
              scene, the bike, and any other vehicles or objects involved.
            </span>
          </li>

          <li>
            <span>
              <b>Preserve any related evidence.</b> Get witness statements.
              Speak to witnesses and secure statements or contact details.
            </span>
          </li>

          <li>
            <span>
              Contact a{" "}
              <strong> Newport Beach bicycle accident attorney</strong> to help
              take your case forward.
            </span>
          </li>
        </ol>

        <h2>How an Experienced Bike Injury Lawyer Can Help</h2>
        <LazyLoad>
          <img
            src="/images/bike-accidents/newport-beach-bicycle-accident-attorneys.jpg"
            width="42%"
            className="imgleft-fluid"
            alt="A bicycle trapped under the front bumper of a car after a crash."
          />
        </LazyLoad>
        <p>
          If you have been the victim of a bicycle accident, discuss your case
          immediately with an experienced bike accident lawyer in Newport Beach.
          In most cases, you will find that you are entitled to far more in
          compensation than you have been offered by the driver or their
          representatives.
        </p>
        <p>
          Insurance companies and defense lawyers work hard to convince victims
          to take immediate settlements, with the result that many of these
          victims end up taking far less than the amount to which they are
          actually entitled.
        </p>
        <p>
          The <Link to="/newport-beach">personal injury lawyers </Link> of
          Bisnar Chase can help maximize your compensation. Every accident is
          different, but you may be entitled to damages for your injuries, pain
          and suffering, medical bills and ongoing care costs, as well as lost
          wages and more.
        </p>
        <p>
          Do not discuss your case unless your Newport Beach car accident lawyer
          is present.
        </p>

        <h2>Seeking Legal Help with Bisnar Chase</h2>
        <p>
          <strong> Newport Beach bicycle accident lawyers</strong> work with
          victims to navigate the complex law surrounding accident cases. Even
          if your case seems clear-cut, it is important to get good legal advice
          from a high-quality attorney.
        </p>
        <p>
          Bisnar Chase has been serving the Newport Beach community for 40
          years. Our attorneys have an outstanding <b>96% success rate</b> and
          have collected more than
          <b> $500 million</b> for their clients. We believe in providing
          superior representation and offer a 'No Win, No Fee' guarantee.
        </p>
        <p>
          If you are in need of a bicycle accident attorney in Orange County,
          call <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> for
          immediate help, or click to contact us for a free consultation.
        </p>
        <p>
          Let our bike accident injury attorneys help you secure the justice you
          deserve.
        </p>

        <div itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
