// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "wrongful-death/newport-beach-wrongful-death-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Wrongful Death Lawyers – Bisnar Chase"
        pageDescription="The Newport Beach wrongful death lawyers of Bisnar Chase have been fighting for victims and their families for 40 years. We offer compassion and support if you have lost a loved one through negligence or malicious action. Contact our skilled wrongful death attorneys for a free consultation - 96% success, $500m won."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Wrongful Death Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Newport Beach Wrongful Death Attorneys"
            title="Newport Beach Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Losing a loved one can feel like an impossible burden to bear. When a
          person dies due to the negligence or actions of another person, this
          is a case of wrongful death and could be subject to legal action. If
          this describes your case, you should contact Bisnar Chase to speak
          with an expert Newport Beach wrongful death lawyer as soon as
          possible.
        </p>
        <p>
          Our{" "}
          <Link
            to="/newport-beach"
            target="new"
            title="Newport Beach Personal Injury Attorneys"
          >
            Newport Beach personal injury attorneys
          </Link>{" "}
          can help, and are dedicated to supporting people in and around the{" "}
          <strong> 92660</strong> area. A wrongful death lawsuit cannot bring
          your loved one back, but it can hold those responsible accountable for
          their actions. We have a <b>96% success rate</b>, and will fight for
          compensation and justice on your behalf.
        </p>
        <p>
          It is important to remember that you are not alone. We are here to
          offer compassion, support, and guidance every step of the way.
        </p>
        <p>
          Contact Bisnar Chase now to speak to a dedicated wrongful death
          attorney in Newport Beach.{" "}
          <Link to="tel:+1-949-203-3814">Call (949) 203-3814</Link>, or go to
          our <Link to="/contact">contact page</Link> to arrange a free case
          evaluation.
        </p>

        <h2>What is a Wrongful Death Lawsuit?</h2>

        <p>
          Wrongful death occurs when a person is killed or dies due to the
          negligent, reckless, careless, or malicious actions of another person
          or entity. When this happens in Newport Beach, a wrongful death claim
          or lawsuit can be filed.
        </p>
        <p>
          The objective of a lawsuit is to help make the family or loved ones of
          the deceased victim whole, in terms of both emotional and financial
          losses suffered.
        </p>
        <p>
          A Newport Beach wrongful death attorney can help guide families
          through the legal process by compiling a strong case, filing a
          lawsuit, and negotiating a settlement or going to trial.
        </p>

        <h2>Types of Wrongful Death in Newport Beach</h2>

        <p>
          A wrongful death case can involve a wide range of different scenarios.
          These can include:
        </p>
        <ul>
          <li>
            <strong> Intentional acts of violence: </strong>A victim could be
            killed in an armed robbery, shooting, assault, or another similar
            malicious act.
          </li>
          <li>
            <strong> Vehicle accident: </strong>Including{" "}
            <Link
              to="/newport-beach/car-accidents"
              target="new"
              title="Newport Beach Car Accident Lawyer"
            >
              car accidents
            </Link>
            ,{" "}
            <Link
              to="/newport-beach/truck-accidents"
              target="new"
              title="Newport Beach Truck Accident Lawyer"
            >
              truck crashes
            </Link>
            , and{" "}
            <Link
              to="/newport-beach/motorcycle-accidents"
              target="new"
              title="Newport Beach Motorcycle Accident Lawyer"
            >
              motorcycle fatalities{" "}
            </Link>
            which involve negligence or reckless actions on the road.{" "}
            <Link
              to="/newport-beach/dui-accidents"
              target="new"
              title="Newport Beach DUI Accident Lawyer"
            >
              DUI accidents
            </Link>{" "}
            are also a rising cause of{" "}
            <Link
              to="http://www.nbpd.org/crime/statistics/crime_by_year.asp"
              target="new"
              title="Newport Beach Police Incident Statistics"
            >
              injuries and wrongful death
            </Link>
            .
          </li>
          <li>
            <strong> Defective products: </strong>Unsafe products, the use of
            which can result in serious injury or death. These might include
            defective vehicles, dangerous pharmaceuticals, and malfunctioning
            products.
          </li>
          <li>
            <strong> Workplace incidents: </strong>From locational hazards, such
            as toxic exposure, to job-related fatal accidents when a worker has
            been placed in a precarious or dangerous position.
          </li>
          <li>
            <strong> Pedestrian and bicycle accidents: </strong>May include{" "}
            <Link
              to="/newport-beach/pedestrian-accidents"
              target="new"
              title="Newport Beach Pedestrian Accident Lawyer"
            >
              fatal injuries to a pedestrian{" "}
            </Link>
            or{" "}
            <Link
              to="/newport-beach/bicycle-accidents"
              target="new"
              title="Newport Beach Bicycle Accident Lawyer"
            >
              cyclist{" "}
            </Link>{" "}
            caused by a road hazard or a collision with a vehicle.
          </li>
          <li>
            <strong> Premises liability: </strong>Such as a death caused by a{" "}
            <Link
              to="/newport-beach/slip-and-fall-accidents"
              target="new"
              title="Newport Beach Slip-and-Fall Lawyer"
            >
              slip-and-fall accident
            </Link>
            , resulting from negligence on the part of the property owner or
            manager.
          </li>
          <li>
            <strong> Dog bites: </strong>A serious attack from a{" "}
            <Link
              to="/newport-beach/dog-bites"
              target="new"
              title="Newport Beach Dog Bite Lawyer"
            >
              dangerous dog{" "}
            </Link>{" "}
            can cause severe injuries, even leading to fatalities in extreme
            cases.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IUvwlj6ew0I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Generally speaking, any harm which would be covered by a personal
          injury claim in the state of California could result in a wrongful
          death lawsuit if it becomes a fatality.
        </p>
        <p>
          Talk to a wrongful death expert for more information, and to find out
          whether a lawsuit can be filed for your case.
        </p>

        <h2>Who Can File a Wrongful Death Lawsuit?</h2>

        <p>
          The process of filing a wrongful death lawsuit in Newport Beach can be
          confusing for many people. If a family member has died due to the
          negligence of another person, how do you know who should file a claim?
        </p>
        <p>
          Different states have varying rules, but in California, the following
          people are allowed to file a wrongful death lawsuit:
        </p>
        <ul>
          <li>
            <b>The spouse or domestic partner of the deceased victim</b>
          </li>
          <li>
            <b>The deceased victim's children</b>
          </li>
        </ul>
        <p>
          In some cases, a victim will not have a direct descendant. California
          law also allows other people to make claims – if they relied on the
          victim financially, or are potential beneficiaries. These include:
        </p>
        <ul>
          <li>
            <b>Parents of the victim</b>
          </li>
          <li>
            <b>Siblings and other close relatives of the deceased</b>
          </li>
          <li>
            <b>The victim's stepchildren</b>
          </li>
          <li>
            <b>
              A{" "}
              <div className="tooltip">
                putative spouse
                <span className="tooltiptext">
                  In a putative marriage, the spouses enter into an arrangement
                  in good faith, but are not legally married. This usually
                  happens when a technical issue (such as a prior marriage),
                  prevents a legal binding. Putative spouses still have legal
                  rights.
                </span>
              </div>
              , or children from this relationship
            </b>
          </li>
        </ul>

        <p>
          If you are unsure if you are able to file a wrongful death claim, or
          want to start the lawsuit process, contact one of our Newport Beach
          wrongful death lawyers for expert help and guidance.
        </p>

        <h2>Newport Beach Wrongful Death Compensation</h2>

        <p>
          It is impossible to put a dollar amount on a person's life, especially
          when it is the life of a loved one.
        </p>

        <p>
          For many people, a wrongful death lawsuit is more about justice and
          accountability than it is about money. In some cases, it may lead to
          improved safety regulations or laws to prevent similar tragedies from
          happening. However, wrongful death cases also often involve the
          awarding of compensation.
        </p>
        <p>
          The amount of compensation will vary based on the details of an
          individual case. There is a wide range of factors which can impact the
          amount of compensation awarded in a successful lawsuit. Some of these
          are financially based, while others are emotional. They include:
        </p>
        <LazyLoad>
          <img
            src="/images/wrongful-death/newport-beach-wrongful-death-claim-lawyer.jpg"
            width="50%"
            alt="A grieving woman sits with her face in her hands at the funeral of a loved one."
            title="Wrongful Death Attorneys in Newport Beach"
            className="imgright-fluid"
          />
        </LazyLoad>
        <ul>
          <li>
            <strong> Medical costs relating to the injury/death</strong>
          </li>
          <li>
            <strong> Loss of expected income</strong>
          </li>
          <li>
            <strong> Loss of inheritance or financial support</strong>
          </li>
          <li>
            <strong>
              {" "}
              The value of services the victim would have been able to provide
              if they were not deceased
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Any pain and suffering they were subjected to before their death
            </strong>
          </li>
          <li>
            <strong> Funeral and burial costs</strong>
          </li>
          <li>
            <strong> Loss of companionship for surviving loved ones</strong>
          </li>
          <li>
            <strong> Loss of care, nurturing, and emotional support</strong>
          </li>
        </ul>

        <p>
          Any money awarded in a wrongful death case is meant to compensate
          people for the loss of their loved one, as well as accounting for any
          benefits they may reasonably have expected to receive, had the victim
          lived.
        </p>
        <p>
          It is the job of our skilled Newport Beach wrongful death attorneys to
          build the strongest case possible, showing what has been lost, as well
          as the pain and suffering this has caused.
        </p>

        <h2>Finding the Best Newport Beach Wrongful Death Lawyer Near Me</h2>
        <LazyLoad>
          <img
            src="/images/wrongful-death/best-newport-beach-wrongful-death-lawyer-near-me.jpg"
            width="50%"
            alt="A supportive wrongful death attorney shows compassion for his client by holding her hand."
            title="Learn How to File a Newport Beach Wrongful Death Lawsuit"
            className="imgright-fluid"
          />
        </LazyLoad>
        <p>
          Losing a loved one is extremely painful and can leave family members
          struggling to cope, both emotionally and financially. Our firm
          represents clients who need compassion and support - and our dedicated
          staff members offer just that.
        </p>
        <p>
          It is our job to ease the burden on grieving families. At Bisnar
          Chase, our wrongful death lawyers will provide experience, guidance
          and support, fighting for the right outcome in your claim.
        </p>

        <p>
          We care deeply about the members of our community. Our law firm has
          been established in Newport Beach, CA, since 1978, and we take pride
          in helping the residents here when they need it most. We excel in
          supporting those people in and around the <strong> 92660</strong>{" "}
          Newport Beach area, but also extend our services through Orange County
          and Southern California.
        </p>
        <p>
          Our goal is to make sure every client gets superior representation.
          While many cases are resolved through a settlement, our attorneys are
          also skilled trial lawyers who will not hesitate to take your case to
          court.
        </p>

        <h2>
          How Much Does It Cost to Hire a Wrongful Death Attorney in Newport
          Beach?
        </h2>

        <p>
          Bisnar Chase ensures that its services are affordable for everyone by
          advancing all costs. We offer a 'No Win, No Fee' guarantee. This means
          that our firm will take care of any reasonable costs required to win
          your wrongful death case.
        </p>
        <p>
          Our costs are only covered if we win your case. If not, you will not
          have to pay. Offering this system is important to our firm, as it
          allows us to help people of all means.
        </p>
        <p>
          Find out more about our 'No Win, No Fee' guarantee by contacting a
          Newport Beach wrongful death lawyer at Bisnar Chase now.
        </p>

        <h2>Trust Bisnar Chase with Your Case</h2>

        <p>
          If you have lost a loved one due to an act of negligence,
          recklessness, or malicious intent, Bisnar Chase can help you seek
          justice. We practice law with compassion, and will make sure you get
          the guidance you need through the difficult legal process.
        </p>
        <p>
          Bisnar Chase has a top track record, collecting more than{" "}
          <b>$500 million</b> for our many clients over the last 40 years. We
          also maintain an outstanding
          <b> 96% success rate</b>. If you have a wrongful death case, trust us
          to fight for you and your loved ones.
        </p>

        <center>
          <div className="contact-us-border">
            <p>
              Call our <strong> Newport Beach wrongful death lawyers</strong>{" "}
              now on <Link to="tel:+1-949-203-3814">(949) 203-3814</Link>, visit
              our law offices, or click to{" "}
              <Link
                to="/contact"
                target="new"
                title="Contact Bisnar Chase Personal Injury Attorneys"
              >
                contact us{" "}
              </Link>{" "}
              and arrange a free consultation.
            </p>
          </div>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px;
    border-radius: 6px;

    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -140px;

    opacity: 0;
    transition: opacity 0.3s;
  }

  .tooltip .tooltiptext::before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
