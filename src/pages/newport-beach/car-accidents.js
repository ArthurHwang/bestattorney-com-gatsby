// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/newport-beach-car-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Newport Beach Car Accident Lawyers - Orange County - Bisnar Chase"
        pageDescription="The expert Newport Beach car accident lawyers of Bisnar Chase have a 96% success rate, winning more than $500M for their clients. Crash victims should call 800-561-4887 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Newport Beach Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="newport beach car accident lawyer"
            title="Newport Beach Car Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Car accidents are one of the leading causes of death and injury. If
          you've been seriously injured in a motor vehicle accident, please
          contact our Newport Beach car accident lawyers for a detailed case
          review.
        </p>

        <p>
          Bisnar Chase has been helping injured plaintiffs since 1978. We offer
          outstanding legal representation with a{" "}
          <strong> "No Win, No Fee"</strong> promise. This means that if you
          don't win, you won't pay.
        </p>

        <p>
          We are so confident in our ability to represent you that we will even
          advance all costs until your car accident case is won! Call{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> today for a{" "}
          <strong> free consultation</strong>.
        </p>

        <h2>Hiring the Right Attorney Matters</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/newport-beach-car-accident-attorney.jpg"
            width="50%"
            alt="Newport Beach car accident attorney"
            className="imgleft-fluid"
          />
        </LazyLoad>

        <p>
          The experienced Newport Beach car accident attorneys at Bisnar Chase
          have successfully helped a huge number of clients win fair
          compensation for major car accident injuries and losses.
        </p>

        <p>
          Hiring an attorney who has spent decades fighting car accident cases
          is the best way for accident victims to boost their chances of getting
          the best results.
        </p>

        <p>
          A personal injury lawyer taking on plaintiff car accident cases should
          have trials and mediations of this type on his or her resume. The{" "}
          <strong> Newport Beach car accident lawyers</strong> of Bisnar Chase
          specialize in this kind of case. They have years of experience in
          tackling car accident claims and are passionate about finding justice
          for their clients.
        </p>

        <p>
          Our law firm has been located in Newport Beach, California, for more
          than four decades, and we love our community. It's important to us
          that we are here to help our neighbors in their time of need. We can
          provide the right{" "}
          <Link to="/newport-beach">personal injury attorney </Link> for you.
          Let us hear your case details and see if we can help you get the
          compensation and support you deserve.
        </p>

        <p>
          We only represent plaintiffs, so with Bisnar Chase, you are sure to
          have an attorney who excels in plaintiff recoveries. Because you are
          not the at-fault driver, your car accident lawyer will work to build
          an effective case, protecting your rights and proving the negligence
          of other parties.
        </p>
        <LazyLoad>
          <img
            className="imgleft-fixed mb"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase"
          />
        </LazyLoad>

        <p>
          Our attorneys have spent decades representing clients in Orange County
          and across Southern California. We have the skill and experience to
          get results for{" "}
          <strong> car accident victims in Newport Beach</strong>.
        </p>

        <p>
          <span>
            {" "}
            <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
            Partner:{" "}
          </span>
          “We've taken on some of the most difficult car accident cases with
          great results. Our legal team has earned a reputation for winning
          tough cases in Orange County.”
        </p>
        <h2>Recent Motor Vehicle Victories</h2>
        <ul>
          <li>$9,800,000.00 - Motor vehicle accident</li>
          <li>$8,500,000.00 - Motor vehicle accident - wrongful death</li>
          <li>$2,815,958.00 - Dangerous road condition</li>
          <li>$900,000.00 - Motor vehicle accident</li>
        </ul>

        <p>
          You can also explore more of our{" "}
          <Link to="/case-results" target="new">
            case results and success stories
          </Link>
          .
        </p>

        <h2>Top 5 Most Common Reasons for Car Accidents</h2>

        <p>
          You can never prepare for getting into a car accident, because they
          can happen anywhere, anytime, and for many reasons - when you least
          expect it.
        </p>

        <p>
          Here are the <strong> top-5 most common car accident causes</strong>{" "}
          in Orange County:
        </p>

        <ol>
          <li>
            <strong> Speeding</strong>: According to this{" "}
            <Link
              to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812021"
              target="new"
            >
              {" "}
              NHTSA Report
            </Link>
            , estimates that the annual economic cost to society
            of speeding-related crashes is $40.4 billion. Speeding is a
            contributing factor in approximately 30% of all fatal crashes, and
            more than 10,000 lives are lost in speeding-related crashes every
            year.
          </li>

          <li>
            <strong> Driving Under the Influence</strong>: Drugs other than
            alcohol (legal and illegal) are involved in about 16% of motor
            vehicle crashes. According to a
            <strong>
              {" "}
              <Link
                to="https://www.cdc.gov/motorvehiclesafety/impaired_driving/impaired-drv_factsheet.html"
                target="new"
              >
                recent study
              </Link>
            </strong>
            , 10,497 people died in alcohol-impaired driving crashes in 2016,
            accounting for nearly one-third (28%) of all traffic-related deaths
            in the United States.
          </li>

          <li>
            <strong> Driver Error</strong>: Encountering hazardous road debris
            and misjudging your avoiding-maneuver, daydreaming and not paying
            complete attention, or not having the appropriate body language
            behind the wheel can all result in a catastrophic car accident.
          </li>

          <li>
            <strong> Auto Malfunction or Defect</strong>: There are countless{" "}
            <Link to="/auto-defects" target="new">
              vehicle fault{" "}
            </Link>{" "}
            types which have caused crashes and put drivers in danger. You can
            experience premature airbag deployment, brake failure, sudden
            unexpected acceleration, windshield wiper failure during a
            rainstorm, and much more. These can can all prove deadly.
          </li>

          <li>
            <strong> Distracted Driving</strong>:{" "}
            <Link
              to="/blog/category/car-accidents/distracted-driving"
              target="new"
            >
              Distracted driving{" "}
            </Link>{" "}
            is the world's most increasingly dangerous hazard as technological
            advances continue to find their ways into the hands of drivers. The
            rise of smartphones makes texting, phone calls, Facebook and other
            social media platforms, emails, YouTube, news, apps, games and more
            accessible behind the wheel - making the roads more dangerous.
          </li>
        </ol>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/L6uepXw9gZA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>How Common are Newport Beach Car Accidents?</h2>

        <p>
          According to{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/userLogin.jsp"
            target="new"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System (SWITRS)
          </Link>
          :
        </p>
        <ul>
          <li>
            <strong> 100 fatalities</strong> and{" "}
            <strong> 13,368 injuries</strong> resulted from traffic accidents in
            Orange County in one year.
          </li>
          <li>
            In the city of Newport Beach, there were{" "}
            <strong> 450 injuries</strong> reported due to traffic accidents
            during that same year, including car crashes,{" "}
            <Link to="/newport-beach/truck-accidents" target="new">
              truck accidents
            </Link>
            , and all other vehicular incidents.
          </li>
          <li>
            Of the Newport Beach car accidents that year,{" "}
            <strong> 63 were alcohol-related</strong>.
          </li>
          <li>
            <strong> Driving under the influence</strong> continues to be a
            problem in Newport Beach.
          </li>
        </ul>

        <h2>Injuries Suffered in Orange County Traffic Accidents</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/car-accident-lawyers-newport-beach.jpg"
            width="35%"
            className="imgleft-fluid"
            alt="car accident lawyers Newport Beach"
          />
        </LazyLoad>

        <p>
          Repairing your vehicle can be expensive, but it is nothing compared to
          the physical, emotional and financial cost of suffering an injury in a
          car crash.
        </p>

        <p>
          Even if you are in a relatively low-speed collision while wearing a
          seatbelt, you can still sustain a serious injury. For example, if you
          are involved in a rear-end collision, you can experience soft tissue
          neck injuries that can make moving your head extremely painful.
        </p>

        <p>
          If you strike your head on your steering wheel, dashboard or window,
          you can sustain a traumatic brain injury with short or long-term
          consequences. It is also common for{" "}
          <strong> car accident victims</strong> to suffer bone fractures,
          lacerations, sprains, strains, contusions, and burns.
        </p>

        <p>
          If there is any chance that you have been hurt in a vehicle crash,
          even if you don't believe you have been injured, it is important that
          you see your doctor. Early diagnosis and treatment will give you the
          best chance of having a full recovery. This will also give you a clear
          record of your injuries and the treatment you received if you decide
          to hire a <strong> Newport Beach car crash lawyer</strong>.
        </p>

        <h2>Shared Fault in California Car Accidents</h2>

        <p>
          It is possible that a court may find that you shared in the fault of
          causing the accident. In{" "}
          <Link
            to="/resources/understanding-comparative-negligence"
            target="new"
          >
            comparative negligence cases
          </Link>{" "}
          like this, the cause of the crash can have a big impact on your
          compensation.
        </p>

        <p>
          Hiring a Newport Beach car accident attorney who has shared-fault
          claim experience could be critical to the outcome of your case. If an
          insurance company rules that you were also negligent and shared blame
          in causing the accident, it can become the most critical piece of your
          injury claim.
        </p>

        <p>
          A highly-skilled <strong> personal injury attorney</strong> will have
          experience in fighting against a shared-fault claim.
        </p>

        <h2>Protecting Your Rights</h2>

        <p>
          There are many ways in which you can protect your rights after a car
          crash. Immediately after the accident, you should:
        </p>
        <ul>
          <li>
            <strong> Call the authorities.</strong> All injury accidents must be
            reported to the police.
          </li>
          <li>
            <strong> Stay at the crash site.</strong> You are considered a
            hit-and-run driver if you leave the site of an injury accident.
          </li>
          <li>
            <strong> Offer assistance.</strong> In addition to making sure your
            occupants are safe, you must also provide assistance to anyone else
            who may have been hurt in the crash.
          </li>
          <li>
            <strong> Take photos.</strong> Every piece of evidence can prove
            useful if you later choose to file a claim. Take photos of the site
            of the crash, the vehicles and your injuries.
          </li>
          <li>
            <strong> Write down the details.</strong> It is easy to forget minor
            details such as how fast you were going, exactly when the accident
            occurred and what you were doing at the time of the crash. Write
            down everything you remember as soon as you can, either on a piece
            of paper or your phone.
          </li>
          <li>
            <strong> Be careful what you say.</strong> It is best not to discuss
            the details of the accident with anyone. What you say can be used
            against you, so remember to never admit fault for the crash.
          </li>
          <li>
            <strong> Collect critical information.</strong> Before leaving the
            crash site to go the hospital, make sure you have all the
            information you need. You should have the phone number, name and
            insurance provider of all of the drivers involved in the crash. You
            should also have contact information from anyone who may have
            witnessed the crash.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Newport beach car accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/potential-claim-worth-good-news-newport-beach.jpg')"
            }}
          >
            <h2>The Potential Value of Your Claim</h2>
          </div>
        </LazyLoad>

        <p>
          Every car crash is different. There can be a wide range of factors
          that contribute to an accident, from driver fault to road layout,
          weather conditions, vehicle faults and much more. The circumstances of
          a crash will always have a huge bearing on the value of a claim.
        </p>

        <p>
          In many cases, the insurance provider for the driver at fault for the
          crash might cover the car damage and basic medical expenses. But
          simply covering repairs and initial medical bills is often not enough.
        </p>

        <p>
          It is vital that you understand the{" "}
          <strong> true potential value of your claim</strong> before accepting
          an offer from an insurance firm. Once you agree to a settlement offer,
          you forfeit your right to seek extra compensation, and your case will
          be closed.
        </p>

        <p>
          The best Newport Beach car accident lawyers will fight for the true
          value of your claim. You can seek support for current and future
          medical bills, lost wages, a loss of earning potential, hospital
          costs, fees for rehabilitation and extra treatments, as well as
          compensation for mental and physical pain and suffering, and other
          related damages.
        </p>

        <p>
          Keep records of your losses and document any struggles the car crash
          has caused. Once you hire a{" "}
          <strong> car accident attorney in Newport Beach</strong>, this will
          help them build the best case possible for your claim. The personal
          injury law firm of Bisnar Chase has a wealth of experience and can
          help you find justice.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/nTkYFvwIJ8w"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Get Immediate Local Help</h2>

        <p>
          Call the Bisnar Chase law office now to find out how we can help you.
          The call is free, and a specialist will be able to assess your case.
        </p>

        <p>
          With more than <strong> 40 years of experience</strong> in providing
          superior representation to Newport Beach residents who have been
          injured in a car crash,{" "}
          <Link to="/newport-beach/motorcycle-accidents" target="new">
            motorcycle accident
          </Link>
          , or any other road incident. Bisnar Chase can offer the help you
          need.
        </p>

        <p>
          We have collected <strong> more than $500 million</strong> for our
          clients, and we may be able to recover money for you too.
        </p>

        <p>
          Contact our experienced Newport Beach car accident lawyers today at{" "}
          <Link to="tel:+1-949-203-3814">(949) 203-3814 </Link> for a free case
          evaluation. We can help you.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
