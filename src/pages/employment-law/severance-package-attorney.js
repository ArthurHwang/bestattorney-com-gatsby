// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Fair Severance Pay Lawyer - Bisnar Chase"
        pageDescription="Laid off or fired? Contact the California fair severance pay attorneys of Bisnar Chase. Free consultations. 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Fair Severance Pay Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="algolia-search-text">
          <p>
            <strong>Notice</strong>:{" "}
            <strong>
              For fair severance pay consultations, there may be a retainer fee
              to investigate your case based on the labor intensive research of
              all records.{" "}
            </strong>
          </p>
          <p>
            If you have just been laid off or you have been let go from your
            job, discussing a fair severance pay with your employer can be
            intimidating.
          </p>
          <p>
            It is critical to remember not to sign a severance agreement if the
            terms are unclear or if you do not agree with what the document
            states. You may be giving up your rights without even knowing it.
            Employers often count on employees being unable to properly and
            effectively negotiate a severance pay at the time of termination.
          </p>
          <p>
            This can be an important and challenging time in your life. Your
            severance pay could affect not only your financial future, but also
            your family's current financial stability. Your employers may not
            even take you seriously until you have an employment law firm on
            your side, such as Bisnar Chase. Having an experienced attorney will
            let your employer know that you are serious about being treated in a
            fair and equitable manner.
          </p>
          <p>
            If you have been terminated or laid of at your place of work and you
            feel your employer did not provide you with adequate severance pay
            to reflect your wages and benefits, our{" "}
            <Link to="/employment-law">
              {" "}
              California employment law attorneys
            </Link>{" "}
            may be able to help you. Call us now at{" "}
            <strong>800-561-4887</strong> for your free case evaluation.
          </p>
          <h2>Protecting Your Rights</h2>
          <p>
            It is common for employers to low-ball employees with a poor
            severance pay counting on shocked employees to quickly accept what
            they get and move on.
          </p>
          <p>
            In addition, companies also require employees to sign agreements and
            release from claims to receive severance pay. These documents
            basically ask workers to sign off their legal rights to sue, to work
            for a competitor or even prevent them from collecting unemployment.
          </p>
          <p>
            At Bisnar Chase, our skilled employment attorneys utilize their
            superior negotiation skills to help clients secure a fair severance
            pay that will hopefully give them a much-needed fresh start.
          </p>
          <p>
            If you have been wrongfully terminated or have been the victim of
            harassment, discrimination, retaliation or a hostile work
            environment, our legal team will fight for your rights and help
            ensure that you are compensated in a way that reflects your time and
            work that you put into your former place of employment.
          </p>
        </div>
        <h2>Do I Need an Employment Attorney?</h2>
        <p>
          A knowledgeable severance pay lawyer will be able to investigate your
          case and establish whether you have a legal claim against your
          employer for discrimination, retaliation or wrongful termination. An
          attorney will also be able to assess the severance pay you have
          received including the financial terms of your pay to determine if it
          is in fact fair and accurate.
        </p>
        <p>
          Your lawyer will be able to directly negotiate with your employer or
          help you with background information and pointers so you can secure
          the best severance pay.
        </p>
        <p>
          Your attorney can properly analyze the terms of your severance
          agreement and explain to you the benefits and pitfalls of accepting
          the offer that your employer has offered. What is important to
          understand is that a severance agreement is a legal document. Do not
          sign it until you understand the terms and their implications.
        </p>
        <h2>Are Employers Required to Offer Severance Packages?</h2>
        <p>
          California is an{" "}
          <Link
            to="http://www.ncsl.org/research/labor-and-employment/at-will-employment-overview.aspx"
            target="_blank"
          >
            at-will employment
          </Link>{" "}
          state. What this means is that an employer can terminate employment
          for no reason at all, unless that reason does not have its basis in
          discrimination or retaliation. Severance pay, which is also known as
          termination or separation pay, refers to money and/or benefits
          provided by the employer to an employee who has been laid off, fired
          or has resigned.
        </p>
        <p>
          While there is no specific contractual obligation or an employment
          policy, California employers are not required by law to provide
          severance pay. However, many employers do offer a severance package in
          exchange for the employee's agreement not to sue.
        </p>
        <p>The following employees may be eligible to receive severance pay:</p>
        <ul type="disc">
          <li>
            Workers with employment contracts that clearly state employers must
            pay severance. Some contracts may even state how much severance the
            employee is entitled to, should the contract be terminated.
          </li>
          <li>
            Workers whose employment has been terminated as part of mass
            layoffs. Under the{" "}
            <Link
              to="https://en.wikipedia.org/wiki/Worker_Adjustment_and_Retraining_Notification_Act"
              target="_blank"
            >
              {" "}
              WARN Act
            </Link>
            , if the company becomes defunct or if the company lays off many of
            its employees without giving at least a 60-day notice, the company
            may have to provide a severance pay including salary and benefits.
          </li>
          <li>
            Employees who work for companies that have severance policies in
            place.
          </li>
        </ul>
        <h2>Factors That Affect Your Severance Pay</h2>
        <p>
          There are many factors that could affect the amount of money or level
          of benefits that you could get as part of your severance pay. Here are
          just some of the most common factors:
        </p>
        <ul type="disc">
          <li>The amount of time you have served with the company.</li>
          <li>Your seniority at the company.</li>
          <li>The circumstances surrounding your employment termination.</li>
          <li>The size and profitability of the company.</li>
          <li>
            The time needed for you to find employment without suffering
            economic hardship.
          </li>
        </ul>
        <p>
          The amount of severance pay your employer offers is non-negotiable,
          however, an employee may be able to negotiate for other non-monetary
          benefits such as continued medical and dental benefits for a specified
          time, a favorable letter of reference, retention of certain company
          property such as a cell phone or laptop or continued funding for
          pursuing education.
        </p>
        <h2>Implications and Deadlines for Severance Packages</h2>
        <p>
          It is also important that employees understand how their severance pay
          will affect their ability to receive unemployment benefits.
        </p>
        <p>
          For example, severance paid as a lump sum will not affect the
          employee's right to unemployment benefits, however, if it is paid out
          bi-weekly as if the employee is still receiving a paycheck, it may
          delay the employee's ability to collect unemployment benefits. It is
          crucial to be mindful of this information when you negotiate your
          severance pay with your employer. Usually, as part of the severance
          agreement, employers will ask the employee to agree not to file any
          claims or lawsuits.
        </p>
        <p>
          Deadlines to sign and accept your severance agreement could range from
          no time to 21 days or longer. Your California employment lawyer may be
          able to extend severance pay time limits or deadlines.
        </p>
        <h2>Employment Attorneys in California</h2>
        <p>
          Finding a good paying job is the American dream and at Bisnar Chase,
          we want to see you thrive in the work place. Sometimes this means
          taking your skill set to a new company. Leaving one job and starting
          work at another is a long, difficult and often stressful process. In
          the case of lay offs, you may not even have a say in when it is your
          time to leave.
        </p>
        <p>
          If finding a new job wasn't hard enough, there are many other factors
          that come into play such as filing for unemployment benefits and
          keeping up with your expenses while between jobs. At Bisnar Chase, we
          will make this transition as smooth as our lawyers can possibly make
          it. During your free consultation, our attorneys will go over the
          facts of your employment and the events that lead up to your
          termination. We will then look at the severance package that is on the
          table and help you negotiate the terms with your former employer so
          you are compensated in a fair manner for the time you put in with your
          company as well as to make sure you are fully taken care of while you
          search for new employment.
        </p>
        <p>
          Being laid off or fired from your place of work can put your life in a
          tail spin and can affect the way you view yourself. If you have been
          laid off or had your employment terminated, you do not have to go
          through this alone. Call us now at <strong>800-561-4887</strong> to
          schedule your free case review with Bisnar Chase.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .arrow {
    position: relative;
    top: 2px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
