// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities/"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "employment/california-employment-class-action-attorney-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Class Action Employment Lawyers - Labor Law Attorneys"
        pageDescription="If you have experienced problems in the workplace such as discrimination or are looking to file a wage and hour claim call toll free at 800-561-4887 to obtain a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Class Action Employment Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California class action employment lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong>California Class Action Employment Lawyers</strong> of
          Bisnar Chase are dedicated to helping employees who have been treated
          poorly in the workplace.
        </p>
        <p>
          At Bisnar Chase we have a wealth of experience when it comes to
          protecting the rights of workers in California. Our law firm has
          developed a{" "}
          <strong>
            96% success rate and has collected more than $500 million
          </strong>{" "}
          for our clients over 40 years in business.
        </p>
        <p>
          If you and other employees at your workplace have been wronged, our
          experts can help you build an{" "}
          <Link to="/employment-law">employment lawsuit</Link>.
        </p>
        <p>
          <strong>Call 1-800-561-4887</strong> for immediate help from a
          California class action employment attorney. When you contact us today
          you will receive free legal advice from a top-notch expert.
        </p>
        <h2>What is a class Action Employment Lawsuit?</h2>
        <p>
          Labor law attorneys define class action claims in employment law as a
          single lawsuit, brought forward by a group of employees, against a
          single party – often their employer.
        </p>
        <p>
          The group of plaintiffs bringing the lawsuit is known as a "class".
          They will all be people with similar complaints against the defendant.
        </p>
        <p>
          If the negative actions of an employer have similarly affected a large
          group of employees, a class action employment lawyer may be able to
          help. By bringing a class action suit rather than a series of
          individual cases, it can strengthen the claim of the plaintiffs.
        </p>
        <h2>The Process of a class Action Employment Lawsuit</h2>
        <p>
          When a class action lawsuit is filed amongst employees then as one of
          the employees you will receive a notice. Experts say to follow the
          instructions of the notice and then after provide the documentation to
          your California class Action Employment lawyer.
        </p>
        <p>
          Your labor law attorney and their team of legal experts will begin the
          process of collecting discovery. Discovery is not only the act of
          collecting facts of the case but it is also the exchange of those
          facts between both legal teams for the plaintiff and defendant. 
        </p>
        <p>
          After discovery, the case must have a class certification. This means
          that a great number of consistencies have been proven amongst all the
          plaintiffs in relation to the defendant's wrongdoings. A motion is
          then made to bring the issue to the courts.
        </p>
        <LazyLoad>
          <img
            alt="a group of people in a meeting"
            width="100%"
            title="California labor and employment lawyers"
            src="../images/employment/california-labor-law-attorneys-class-action-people-image.jpg"
          />
        </LazyLoad>
        <h2>How Many People are Needed for a class Action Lawsuit?</h2>
        <p>
          There is no set number of people required for a class action suit.
          Cases involving large classes of wronged employees are easier to bring
          through the courts.{" "}
        </p>
        <p>
          A judge will often ask for dozens of plaintiffs to be involved in a
          class for it to proceed, but a class could also consist of hundreds or
          even thousands of employees.
        </p>
        <h2>Do I Have a Wrongful Termination Case?</h2>
        <p>
          A{" "}
          <Link to="/employment-law/wrongful-termination">
            wrongful termination
          </Link>{" "}
          is when a worker is fired by an employer under unlawful circumstances.
          For instance, an employer who terminates a worker's employment to
          retaliate against an employee who disclosed illegal activity taking
          place in the company is in violation of the whistleblower act. Other
          wrongful termination cases may include an employer violating
          anti-discrimination laws, labor laws, whistleblower or quid pro quo.{" "}
        </p>
        <p>
          Quid Pro Quo is an employer promising a higher salary or higher
          position in the company in exchange for sexual services. If the
          employee refuses and upon their refusal is fired, then the workers can
          file either a sexual harassment claim or a wrongful termination claim.
        </p>
        <h2>U.S. Equal Employment Opportunity Commission Laws</h2>
        <p>
          The EEOC serves as an authority and advocate for those in the
          workforce. The Equal Employment Opportunity Commission acts as a
          checks and balances system to protect employees.
        </p>
        <LazyLoad>
          <img
            alt="A paper that states the acronym eeoc"
            width="100%"
            title="Wage and Hour lawyers"
            src="../images/employment/employment-claas-action-lawyers-in-california-eeoc-image.jpg"
          />
        </LazyLoad>
        <p>
          There are several federal laws that have been instilled to safeguard
          workers from negligent or discriminatory employers. Employers may not
          discriminate a worker or applicant based on their gender, age,
          disabilities, religion, color or pregnancy.
        </p>
        <h3>
          {" "}
          List of EEOC{" "}
          <Link to="https://www.eeoc.gov/laws/statutes/index.cfm">
            Federal Laws
          </Link>
          :
        </h3>

        <ul>
          <li>
            <strong>
              Title VII of the Civil Rights Act of 1964 (Title VII)
            </strong>
          </li>
          <li>
            <strong>The Pregnancy Discrimination Act</strong>
          </li>
          <li>
            <strong>The Equal Pay Act of 1963 (EPA)</strong>
          </li>
          <li>
            <strong>
              The Age Discrimination in Employment Act of 1967 (ADEA)
            </strong>
          </li>
          <li>
            <strong>
              Title I of the Americans with Disabilities Act of 1990 (ADA)
            </strong>
          </li>
          <li>
            <strong>
              Sections 102 and 103 of the Civil Rights Act of 1991
            </strong>
          </li>
          <li>
            <strong>
              Sections 501 and 505 of the Rehabilitation Act of 1973
            </strong>
          </li>
          <li>
            <strong>
              The Genetic Information Nondiscrimination Act of 2008 (GINA)
            </strong>
            <strong></strong>
          </li>
        </ul>
        <h2>Wage and Hour Rules and Regulations</h2>
        <p>
          California employers are to follow the guidelines under the{" "}
          <Link to="https://www.flsa.com/coverage.html">
            Fair Labor Standards Act
          </Link>
          . The FLSA is an employment law which states that workers are entitled
          to a minimum wage and should be compensated for overtime if they are
          to work over 40 hours in a week.
        </p>
        <p>Employees are also entitled to meal and rest breaks.</p>
        <p>
          {" "}
          How do you know that you have a strong wage and hour class action
          case?
          <strong>
            Some big indicators that signal that you and your fellow employees
            may be untreated unfairly are if the employer is:
          </strong>
        </p>
        <ul>
          <li>Requiring Off-the-clock work without being compensated</li>
          <li>Underpaying due to too many deductions being taken out of pay</li>
          <li>Taking too many deductions out of their employees pay</li>
        </ul>
        <p>
          <strong>
            One of the most common questions that is asked amongst employees is
            "Do I get overtime if I am a salaried worker?
          </strong>
          "
        </p>
        <p>
          One big misconception that workers who work under the salary system
          are that they are exempt from overtime. This is not the case in most
          instances. The{" "}
          <Link to="https://www.latimes.com/la-fi-qa-overtime-20160601-snap-story.html">
            Los Angeles Times
          </Link>{" "}
          reported that as of December 2016 &ldquo;…salaried workers who earn
          less than $47,476 will now be eligible for overtime pay.&rdquo;{" "}
        </p>
        <p>
          There are cases where employers do not have to pay overtime to their
          workers.
        </p>
        <p>
          This law does not apply to people who are self-employed or have a
          professional occupation such as a lawyer.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Whistleblowing Attorneys"
            style={{
              backgroundImage:
                "url('../images/employment/whsitle-blower-attorneys-in-california-whistle-text-header.jpg')"
            }}
          >
            <h2>Can I Be Fired for Whistleblowing?</h2>
          </div>
        </LazyLoad>
        <p>
          A whistleblower is an employee who informs a third party or law
          enforcement of any wrongdoings or illegal activity that is taking
          place in the workplace.
        </p>
        <p>
          A group of employees often fear that they will lose their jobs if they
          were to report any violations that are taking place in a work
          environment. Another fear that employees face after reporting a crime
          in the workplace is hostility they will face, but whistleblowers do
          have rights and are protected under federal law.
        </p>
        <p>
          The{" "}
          <strong>
            United State Department of Labor Section 11(c) of the OSH Act states
          </strong>{" "}
          &ldquo;<strong>If workers have been retaliated</strong> or
          discriminated against for exercising their rights, they must file a
          complaint with OSHA within 30 days of the alleged adverse
          action.&rdquo;
        </p>
        <p>
          To strengthen your case  it is important to gather any evidence such
          as emails, witness accounts or even photos of the crimes being
          committed to your California whistleblower attorney.
        </p>
        <h2> class Action vs Individual Employment Cases </h2>
        <p>
          An individual case is different from a class action lawsuit because
          there is only one plaintiff that is suing an employer. Although there
          is not a set amount needed for a California class action case there
          needs to be more than one person.{" "}
        </p>
        <p>
          class action employment lawsuits may also take longer than a case with
          a single plaintiff due to the size and the number of people involved.
          In class action lawsuits the process can be stalled for factors such
          as both parties collecting discovery (facts and evidence of the case),
          the defendant does not want to settle, or the courts are not
          proceeding quickly enough.
        </p>
        <h2>The Cost of Hiring a California class Action Employment Lawyer</h2>
        <p>
          Bisnar Chase offers a &lsquo;No Win, No Fee' guarantee. We will
          advance all the costs necessary to winning your case and will only
          collect a fee if we win for you. Our fee will be a standard percentage
          of the settlement or court-awarded compensation. me class action
          lawsuits can take 9 months – 2 years to settle.
        </p>
        <p>
          Bisnar Chase, class action employment lawyer,{" "}
          <Link to="/attorneys/jerusalem-beligan">
            <strong>Jerusalem Beligan</strong>
          </Link>{" "}
          states the importance of hiring a labor law attorney.
        </p>
        <p>
          "
          <em>
            Employment law covers a wide spectrum of claims, ranging from
            discrimination, to harassment, to unpaid wages, just to name a
            few.&nbsp; Each claim brings with it differing pitfalls and
            minefields.&nbsp; You need an employment lawyer to help you navigate
            through these legal minefields because any wrong step could lead to
            your case being time barred or you might not recover all of the
            money you are entitled.&nbsp; Here, at Bisnar | Chase, we have
            experienced employment lawyers who can guide you through the complex
            and intricate web of employment law
          </em>
          ", says Beligan. &nbsp;&nbsp;&nbsp;&nbsp;
        </p>
        <LazyLoad>
          <img
            alt="a group of legal experts"
            width="100%"
            title="class action employment litigation"
            src="../images/employment/bisnar-chase-office-photo-image.jpg"
          />
        </LazyLoad>
        <h2>Trust Bisnar Chase</h2>
        <p>
          Contact Bisnar Chase now for immediate help with your class action
          employment claim. We are experienced trial lawyers with the resources
          to take on large class actions.
        </p>
        <p>
          Our labor and employment attorneys are passionate about protecting the
          workplace rights of our clients and are dedicated to offering superior
          representation.{" "}
        </p>
        <p>
          If you or a group of employees is looking to file a class action
          lawsuit against your employer call the Law Firm of Bisnar Chase.
        </p>
        <p>
          Contact the California class action employment lawyers of Bisnar Chase
          now. <strong>Call 1-800-561-4887 </strong>and arrange a{" "}
          <strong>free consultation</strong>.{" "}
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .arrow {
    position: relative;
    top: 2px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
