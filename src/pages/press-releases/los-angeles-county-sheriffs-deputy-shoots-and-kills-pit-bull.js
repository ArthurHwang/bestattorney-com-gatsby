﻿// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="L.A. County Deputy Kills Pit Bull that Attacked Animal Control Officer"
        pageDescription="If you were injured in a dog bite, you may be able to get compensation. Call 323-238-4683 for award-winning California dog bite attorneys. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles County Sheriff's Deputy Shoots and Kills Pit Bull</h1>
        <BreadCrumbs location={location} />
        <h3>
          California dog bite lawyer discusses the importance of remaining
          vigilant about dogs roaming the neighborhood in light of a dog attack
          in La Mirada that left an animal control officer injured and another
          small dog dead. A Nov. 20 CBS news report states that the deputy shot
          the dog in order to save the animal control officer from getting
          seriously injured or killed.
        </h3>
        <p>
          <img
            src="/images/dog-bites/pit bull attacks.jpg"
            className="imgright-fixed"
            alt="pit bull attack"
          />
          A Los Angeles County Sheriff's Deputy shot and killed a pit bull after
          it killed a Chihuahua and attacked an Animal Control Officer Mireya
          Martinez. According to a Nov. 20 CBS news report, the dog attack
          occurred on Nov. 17 in the 13500 block of Ramsey Avenue in La Mirada.
          Martinez was trying to contain the attacking dog when it bit her, the
          report states. The article reports that the on-duty deputy who was at
          the scene responded immediately by shooting the dog and saving the
          Martinez's life. Animal Control officers are warning the public to
          remain vigilant about dogs without leashes that are roaming in
          neighborhoods, the article states. Anyone who wishes to report a stray
          or loose dog is asked to call animal control officers at 562-940-6898.
        </p>
        <p>
          According to the U.S. Centers for Disease Control and Prevention
          (CDC), dogs bite more than 4.7 million people each year in the United
          States and of these victims, 800,000 seek medical attention and
          386,000 require treatment in an emergency department. About 16 people
          die each year in dog attacks and the rate of dog bite-related injuries
          is highest for children ages 5 to 9, the CDC reports. Almost
          two-thirds of injuries among children ages 4 years and younger are to
          the head or neck region, the agency states.
        </p>
        <p>
          This incident casts the spotlight on the importance of being vigilant
          when it comes to stray and loose dogs, said John Bisnar, founder of
          the Bisnar Chase personal injury law firm. "If you come across an
          unfamiliar dog, stay away and do not approach it," he says. "Anyone
          who comes across a stray dog, a loose dog or dogs behaving unusually
          would be well advised to report it to the animal control authorities."
        </p>
        <p>
          Bisnar says dog owners have a responsibility to ensure that their pets
          are not roaming around the neighborhood. "This puts the public in
          grave danger. Dog attacks can result in major injuries and permanent
          disfigurement. In some cases, it could also result in death. Dog
          owners whose negligence leads to injury or death can be held civilly
          liable for losses sustained by the victims and their families."
        </p>
        <p>About Bisnar Chase</p>
        <p>
          The <Link to="/dog-bites">California dog bite lawyers</Link> of Bisnar
          Chase represent victims of dog bites and many other personal injuries.
          The firm has been featured on a number of popular media outlets
          including Newsweek, Fox, NBC, and ABC and is known for its passionate
          pursuit of results for their clients. Since 1978, Bisnar Chase has
          recovered millions of dollars for personal injury victims including
          dog bite victims.
        </p>
        <p>
          For more information, or for a free consultation, please call
          323-238-4683.
        </p>
        <p>
          Source:
          http://losangeles.cbslocal.com/2012/11/20/deputy-kills-pit-bull-after-animal-control-officer-attacked-in-la-mirada/
        </p>
        <p>
          http://www.cdc.gov/homeandrecreationalsafety/dog-bites/dogbite-factsheet.html
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
