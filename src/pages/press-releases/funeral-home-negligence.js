// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Funeral Home Left Murdered Girl's Body to Decay"
        pageDescription="A funeral home who was to cremate a mother's murdered daughter was accused of abandoning the body. Her badly decomposed body was found on a table in the funeral home's basement."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Funeral Home Left Mother's Murdered Daughter's Body to Rot</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10425696.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 13, 2013
          </i>
        </p>
        <p>
          Jessica Bachman was grief-stricken when she learned that her
          2-year-old daughter, Ranasia Knight was beaten and killed by her
          former boyfriend (case no.13-0231, Lancaster County
          <img
            src="/images/funeral-home-negligence.jpg"
            alt="Funeral Home Negligence"
            width="200"
            className="imgright-fixed"
          />
          Prison). According to a Feb. 10 news report in The Intelligencer
          Journal, Bachman's pain was worsened when police told her that
          Ranasia's body was found rotting on a table at the basement of a local
          funeral home.
        </p>
        <p>
          The mother of a 2-year-old girl murdered by her former boyfriend (case
          no.13-0231, Lancaster County Prison) says that the funeral home, which
          was supposed to cremate the child's body, allowed it to rot in a
          basement for a month. According to a Feb. 10 news report in The
          Intelligencer Journal, Jessica Bachman's 2-year-old daughter, Ranasia
          Knight, died on Jan. 12 from what Bachman thought was a fall down the
          stairs. However, on Jan. 14, Bachman's former boyfriend Lester
          Johnson, was arrested for the death of the child after he allegedly
          admitted to police that he kicked and punched the child, blinding her
          in one eye, fatally injuring her, the report said.
        </p>
        <p>
          On Feb. 1, police told Bachman that they had found Ranasia's badly
          decomposed body on a table in the basement of a local funeral home,
          The Intelligencer Journal reports. Bachman's friend had paid the
          funeral director $400 to have Ranasia cremated and deliver the ashes
          to Bachman, but the director allegedly "acted strange" throughout the
          purpose avoiding her calls and making excuses for delays, the report
          states.
        </p>
        <p>
          Bachman tells The Intelligencer Journal that she was promised that her
          daughter would be taken care of, but instead found out from the
          authorities that the little girl's body and three others had been
          abandoned in a case of what appears to be{" "}
          <Link
            to="/funeral-home"
            title="Funeral Home Negligence"
            onClick="linkClick(this.to)"
          >
            funeral home negligence
          </Link>
          . Police found the bodies during a raid of the funeral home after
          receiving complaints that the director had taken money, but not
          followed through with the cremations. Another funeral home took over
          Ranasia's care at no charge, the article states.
        </p>
        <p>
          Reports of funeral home abuse are shocking, appalling and disgusting,
          said John Bisnar, founder of the Bisnar Chase personal injury law
          firm. "Losing a loved one is heartbreaking for families as it is. All
          that these families want is for their loved ones to be laid to rest
          with peace and dignity. Snatching away that sense of closure from
          grieving families is unacceptable."
        </p>
        <p>
          Incidents of funeral home abuse are particularly egregious, Bisnar
          says. "It is important for victims of funeral home abuse to understand
          that they do have rights. Often funeral home abuse claims serve
          several purposes. They help right a wrong, they hold wrongdoers
          accountable and they create public awareness about negligent funeral
          homes."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California personal injury lawyers of Bisnar Chase represent
          victims of funeral home abuse. The firm has been featured on a number
          of popular media outlets including Newsweek, Fox, NBC, and ABC and is
          known for its passionate pursuit of results for their clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St., Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          http://lancasteronline.com/article/local/813347_Mother--who-lost-daughter--tells-of-dealings-with-funeral-director.html
        </p>
        <p>
          Court record:
          http://www.lancasterpolice.com/news/press-releases/1-15-13-arrest-made-in-child-homicide/
        </p>
        <p></p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
