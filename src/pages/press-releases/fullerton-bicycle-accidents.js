// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Police Seek the Public's Help to Apprehend Hit-and-Run Driver Involved in Fatal Fullerton Bicycle Accident"
        pageDescription="Have you been injured in a Fullerton bicycle accident? Call 949-203-3814 to speak with an experienced legal professional about possible legal action. The attorneys at Bisnar Chase offer free no obligation case evaluations to all bicycle accident victims."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Police Seek the Public's Help to Apprehend Hit-and-Run Driver Involved
          in Fatal Fullerton Bicycle Accident
        </h1>
        <BreadCrumbs location={location} />
        <p>
          Officers arrived at the scene to find Paine, who had been hit while
          riding his bike northbound across Bastanchury. The impact of the
          collision threw Paine into the air and a second vehicle struck him. He
          was transported to a local hospital where he was pronounced dead.
        </p>
        <p>
          The first vehicle, which police have described as a black sedan, made
          no attempt to stop and fled the scene. The second vehicle, a silver
          Volkswagen Jetta, stopped and remained at the scene. Police are
          looking into who had the green light at the time. Anyone with
          information about this hit-and-run collision is urged to contact
          traffic investigator Brandon Clyde at 714-738-6812 or traffic
          investigator Mike Moon at 714-738-6813.
        </p>
        <p>
          The California personal injury attorneys at BISNAR CHASE are offering
          a $1,000 reward through the Hit-and-Run Reward Program for information
          leading to the arrest and felony conviction of the hit-and-run driver
          responsible for the death of Richard Paine. Those with information can
          call in tips anonymously via the Hit-and-Run Reward Program's website
          or through the{" "}
          <Link to="http://www.hitandrunreward.com/">Hit-and-Run Reward</Link>{" "}
          hotline at 800-6-Hit-N-Run (800-644-8678).
        </p>
        <p>
          Tips offered through this program are facilitated nationwide by WeTip,
          a non-profit victim's advocacy group, which is funded by participating
          U.S. personal injury attorneys. All tips made through the Hit-and-Run
          Reward Program are treated with the strictest anonymity.
        </p>
        <p>
          This program is a way for our personal injury attorneys to partner
          with members of the community who care enough to report these crimes
          to get hit-and-run drivers off our highways.
        </p>
        <p>
          "It is very rare for hit-and-run drivers to turn themselves in," says
          John Bisnar, founder of Bisnar Chase{" "}
          <Link to="/">California personal injury attorneys</Link> and the
          Hit-and-Run Reward Program. "This is why it is crucial for
          eyewitnesses to come forward with information. Make no mistake,
          hit-and-run is a very serous crime and it is important that these
          hit-and-run drivers are held accountable."
        </p>
        <h2>About WeTip</h2>
        <p>
          WeTip is a leading anonymous crime reporting service and law
          enforcement advocacy non-profit organization that facilitates
          anonymous tips from all 50 states. Since 1972, WeTip has facilitated
          over 521,000 anonymous tips leading to almost 16,000 criminal arrests
          and more than 8,000 convictions, including hit-and-run offenders. In
          March 2010, WeTip partnered with the Bisnar Chase personal injury law
          firm to launch the Hit-and-Run Reward program aimed at wiping out hit
          and run driving offenses in the state of California.
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          Orange County personal injury attorneys represent people who have been
          very seriously injured or lost a family member due to an accident,
          defective product or negligence. The law firm has obtained settlements
          and judgments totaling nearly two hundred of million dollars for their
          clients by winning a wide variety of challenging personal injury cases
          involving car accidents, work place injuries and defective products,
          including defective automobiles. Additionally, the law firm represents
          people who have been seriously injured or lost a family member due to
          drunk driving and hit-and-run collisions.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
