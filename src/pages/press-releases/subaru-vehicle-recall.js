// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Subaru Recalls Vehicles that Can Start on their Own"
        pageDescription="Subaru has announced an auto defect recall for about 50,000 vehicles in the United States and Canada because they could take off on their own without warning."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Subaru Recalling Vehicles that Can Start on their Own: Brian Chase
          Weighs In
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/3/prweb10509761.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          Subaru has announced an auto defect recall for about 50,000 vehicles
          in the United States and Canada because they could take off on their
          own without warning. According to a Detroit News report, the remote
          engine starter in these vehicles could malfunction and start them
          suddenly and inadvertently.
        </p>
        <img
          src="/images/subaru-recall.jpg"
          width="250"
          height="153"
          alt="subaru recall"
          className="imgright-fixed"
        />
        <p>
          The <Link to="/auto-defects">Subaru recall</Link> covers several
          vehicle models including the 2012-13 Impreza, 2010-13 Legacy, 2010-13
          Outback and 2013 XV Crosstrek, the report states.
        </p>
        <p>
          According to the article the vehicles could pose a danger if parked in
          an enclosed garage because of the potential buildup of carbon
          monoxide, which could result in asphyxiation. The engine in these
          vehicles may start and run for 15 minutes and the engine may continue
          to start and stop until the battery in the key fob dies or the vehicle
          runs out of gas, the report states.
        </p>
        <p>
          By the end of April Subaru dealers will replace the key fobs at no
          cost to consumers, according to the article. Subaru started an
          investigation into the matter after noticing an increase in warranty
          claims in July. This occurred soon after a supplier had made a change
          to improve the key fob in 2011, the article states.
        </p>
        <p>
          "This is a dangerous auto defect that could result in serious injury
          or death, said Brian Chase, auto recall expert and partner at Bisnar
          Chase personal injury law firm.
        </p>
        <p>
          "If you own one of these recalled vehicles, it is important that you
          take your vehicle to a dealer right away and get it inspected. In
          these cases, it is never worth it to take a chance."
        </p>
        <p>
          "Anyone who has been injured in an incident involving a defective
          vehicle or faulty auto part has legal rights", Chase said.
        </p>
        <p>
          "Vehicle manufacturers are responsible for the product they put on the
          market. Injured victims may be able to seek compensation for the
          injuries and damages they may have suffered as a result of the
          defective vehicle."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>
        <p>
          Source: <br />
          http://www.detroitnews.com/article/20130307/AUTO0104/303070410/1361/Subaru-recalls-47-000-cars--SUVs-that-can-start-on-their-own
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
