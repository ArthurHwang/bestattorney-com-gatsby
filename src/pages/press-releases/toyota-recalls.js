// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Press release - Toyota Recall - CA Personal Injury Attorneys"
        pageDescription="Press release: Toyota recall & Auto Defect Complaints. Were you injured? Call 949-203-3814 for CA personal injury attorneys who can help. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Weighs in on Recent Toyota
          Rulings, Auto Defects Complaints and Auto Recalls
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            The California Personal Injury Attorneys of Bisnar Chase Personal
            Injury Attorneys (/) today weighed in on several key rulings against
            Toyota in a Santa Ana, California courtroom, as well as significant
            auto defects issues making state and national headlines in recent
            weeks.
          </strong>
        </p>
        <img
          src="/images/Brian-chase-new.jpg"
          alt="Associates Day"
          className="imgright-fixed"
        />
        <p>
          According to Reuters, consumers who claim Toyota negatively impacted
          the value of their vehicles by failing to disclose or fix problems
          associated with sudden acceleration, won a bid by a Santa Ana,
          California judge to have their claims heard.
        </p>
        <p>
          Toyota wanted to dismiss consumer claims of economic loss, but U.S.
          District Judge James V. Selna ruled to allow the cases against the
          automaker to proceed.
        </p>
        <p>
          Toyota believes it will prevail in court, claiming the burden of proof
          is on the plaintiffs to demonstrate its electronic throttle control
          system was defective.
        </p>
        <p>
          <strong>
            Brian Chase, auto defects expert and partner at Bisnar Chase
            Personal Injury Attorneys, weighed in on Selna's ruling.
          </strong>
        </p>
        <p>
          "While significant, this ruling doesn't automatically mean victory for
          Toyota consumers," said Chase. "However, it sends a clear message that
          if a manufacturer makes certain claims about its products - and
          there's enough doubt as to the validity of those claims - consumers
          have the right to due process to collect damages."
        </p>
        <p>
          Last week, Selna ruled California law can be applied to economic loss
          claims against Toyota - yet another strike against the automaker's
          attorneys.
        </p>
        <p>
          "The reason they argued so vehemently against this issue is because
          under California law, consumers typically have a better chance of
          recovering damages here than in other states," Chase said.
        </p>
        <h2>Ford Freestyle SUV Recall</h2>
        <p>
          Bloomberg reports approximately 170,000 Ford Freestyle SUVs are
          currently under investigation by the National Highway Traffic Safety
          Administration (NHTSA) due to "unintended lunging."
        </p>
        <p>
          The problem has been described in complaints as a sudden and
          unexpected movement when the vehicle is moving either forward or
          backward, even though the driver's foot isn't on the accelerator.
        </p>
        <p>
          18 crashes may be attributed to the problem and close to 250 lunging
          complaints have been filed, according to NHTSA.
        </p>
        <p>
          <strong>
            Chase commented on the problem and the subsequent investigation.
          </strong>
        </p>
        <p>
          "Even though the Freestyle lunging problem has been reported at low
          speeds, sudden acceleration of any vehicle, at any time, is extremely
          dangerous and highly unacceptable."
        </p>
        <p>
          Chase cites the severity of sudden acceleration as evidenced in
          Toyota's recall of more than 20 million vehicles worldwide since late
          2009 due to this issue while expressing concern at the lack of impact
          recalls had on Toyota's sudden acceleration issues prior to 20091.
        </p>
        <p>
          "In late 2009, NHTSA issued a recall on Toyota vehicles for gas pedal
          entrapment from floor mats and carpet problems on the heels of a 2007
          recall for related problems2. In my opinion, NHTSA's 2007 recall had
          little bearing, if any, on safety and design changes at Toyota."
        </p>
        <h2>Rental Car Recalls: Update</h2>
        <p>
          A recent New York Times article highlighted how several major rental
          car companies continue to rent vehicles that have been recalled if
          they don't think the problem is serious, according to information
          provided to NHTSA.
        </p>
        <p>
          Current law allows rental car companies to rent cars that have been
          recalled and not repaired, even though it's illegal for dealers to
          sell them.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys announced its support of laws
          designed to protect motorists from rental car safety problems in
          March, citing recent rental car safety legislation introduced by
          lawmakers here in California and by federal regulators in Washington.
        </p>
        <p>
          The measures that were introduced stem from a 2004 incident in which
          two sisters, Raechel and Jacqueline Houck of Santa Cruz, California,
          died while driving a PT Cruiser they rented from Enterprise Rent-A-Car
          (Houck v. Enterprise Rent-A-Car, No. HG052220018, Alameda County,
          California Superior Court, Judgment 6/9/10).
        </p>
        <p>
          <strong>
            Today, Chase responded to several points in the article.
          </strong>
        </p>
        <p>
          <strong>Article excerpt #1:</strong> [The rental car companies] have
          also argued that it is unfair to focus on rental companies when other
          fleet operators or even individual consumers are not required to carry
          out recall repairs.
        </p>
        <p>
          "Most consumers do in fact get the recall work performed - to argue
          otherwise is irresponsible," said Chase. "But, if a consumer chooses
          not to do so, that's their prerogative. That person is not renting
          their vehicle to millions of unsuspecting consumers. To draw that
          analogy is further evidence of why we do not want to rely on rental
          car companies making these decisions.
        </p>
        <p>
          <strong>Article excerpt #2:</strong> The rental companies said they
          were doing a good job of protecting consumers, but also urged
          N.H.T.S.A. to adopt a labeling system for recalls that would indicate
          whether a recall was so serious that the affected vehicles should
          immediately be parked.
        </p>
        <p>
          "NHTSA issued advisories regarding the safety of 15 passenger vans3,
          with a suggested warning placard to be placed in those vehicles for
          consumers to see4," said Chase. "Based on our research, this idea was
          initially ignored by rental car companies and is largely ignored even
          today."
        </p>
        <p>
          "NHTSA has also proven not be the best protector of our safety when it
          comes to vehicle safety," said Chase. "In my opinion, the Federal
          Motor Vehicle Safety Standards are, for the most part, watered down,
          lobbied for by the auto industry, 'cream puff safety' standards.
        </p>
        <ol>
          <li>
            Robert E. Cole, Who Was Really at Fault for the Toyota Recalls?, May
            1, 2011,{" "}
            <Link
              to="http://www.theatlantic.com/business/archive/2011/05/who-was-really-at-fault-for-the-toyota-recalls/238076/2/"
              target="_blank"
            >
              http://www.theatlantic.com/business/archive/2011/05/who-was-really-at-fault-for-the-toyota-recalls/238076/2/
            </Link>
          </li>
          <li>
            Robert E. Cole, Who Was Really at Fault for the Toyota Recalls?, May
            1, 2011,{" "}
            <Link
              to="http://www.theatlantic.com/business/archive/2011/05/who-was-really-at-fault-for-the-toyota-recalls/238076/2/"
              target="_blank"
            >
              http://www.theatlantic.com/business/archive/2011/05/who-was-really-at-fault-for-the-toyota-recalls/238076/2/
            </Link>
          </li>
          <li>
            Consumer Advisory: Federal Government Restates Rollover Warning for
            15-Passenger Van Users, May 20, 2009,{" "}
            <Link to="http://www.nhtsa.gov/CA/10-14-2010" target="_blank">
              http://www.nhtsa.gov/CA/10-14-2010
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://www.safercar.gov/staticfiles/safercar/Passenger%20Van%20Safety/vanhangtag.pdf"
              target="_blank"
            >
              http://www.safercar.gov Passenger Van Safety PDF
            </Link>
          </li>
        </ol>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people who have
          been very seriously injured or lost a family member due to an
          accident, defective product, negligence or unsafe rental car
          throughout the country from their Newport Beach, California
          headquarters. The auto defects law firm has won a wide variety of auto
          defect cases against most of the major auto manufacturers, including
          Ford, General Motors, Toyota, Nissan and Chrysler, manufacturer of the
          PT Cruiser. Brian Chase is the author of the most up-to-date and
          comprehensive auto defect book available today, Still Unsafe at Any
          Speed: Auto Defects that Cause Wrongful Deaths and Catastrophic
          Injuries. For more information, visit Mr. Chase's blog at
          http://www.ProductDefectNews.com.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
