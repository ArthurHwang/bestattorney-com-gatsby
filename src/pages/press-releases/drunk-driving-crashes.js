// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dallas Cowboys' Jerry Brown Killed in Alleged Drunk Driving Crash"
        pageDescription="California wrongful death attorney John Bisnar warns holiday drivers about the dangers of drinking and driving after an alleged DUI crash killed Dallas Cowboys' Jerry Brown early morning Dec. 8 2012."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Wrongful Death Attorney John Bisnar Weighs In On Drunk Driving Crashes
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            California wrongful death attorney John Bisnar warns holiday drivers
            about the dangers of drinking and driving after an alleged DUI crash
            killed Dallas Cowboys' Jerry Brown early morning Dec. 8 2012.
          </strong>
        </p>
        <p>
          <img
            src="/images/nfl-player-jerry-brown.jpg"
            alt="NFL Player Jerry Brown"
            className="imgright-fixed"
          />
          Dallas Cowboys defensive lineman Josh Brent is facing vehicular
          manslaughter charges in connection with an alleged DUI crash that
          killed his teammate Jerry Brown on Dec. 8, according to a{" "}
          <Link
            to="http://www.scribd.com/doc/116141800/Joshua-Price-Brent-PC-Affidavit"
            title="drunk driving crash"
            onClick="linkClick(this.to)"
          >
            court affidavit
          </Link>{" "}
          (Case number: 120028613) filed by the Irving Police Department and a{" "}
          <Link
            to="http://www.cbsnews.com/8301-504083_162-57558186-504083/josh-brent-dallas-cowboys-player-accused-in-fatal-car-crash-says-jerry-brown-was-very-best-friend/"
            title="drunk driving crash"
            onClick="linkClick(this.to)"
          >
            CBS news report
          </Link>
          .
        </p>
        <p>
          According to the Dec. 8 probable cause affidavit, officers who arrived
          on scene immediately after the fiery crash determined that Brent was
          intoxicated. The officer observed "sufficient clues to indicate that
          he had lost his mental and physical faculties to operate a motor
          vehicle on a public roadway due to his intoxication level," the
          affidavit states. It also states that Brent admitted to the arresting
          officer that he consumed alcohol, but would not state the name of the
          club.
        </p>
        <p>
          Brown was transported to an area hospital where he was pronounced
          dead, the affidavit states. Brent also refused to provide a blood
          sample and officers were able to require him to do so because of the
          death of Brown, who was his passenger at the time, according to Irving
          Police Officer Travis Huckaby, who prepared the{" "}
          <Link
            to="http://www.scribd.com/doc/116141800/Joshua-Price-Brent-PC-Affidavit"
            title="Jerry Brent affidavit"
            onClick="linkClick(this.to)"
          >
            affidavit
          </Link>
          .
        </p>
        <p>
          According to the National Highway Traffic Safety Administration
          (NHTSA), in 2010, 10,228 people were killed in the United States as a
          result of DUI collisions, including driving driving crashes, which
          accounted for 31 percent of the total traffic accident fatalities
          nationwide. Although traffic accident deaths went down by 4.9 percent
          compared to 2009, an average of one DUI death occurred every 51
          minutes in 2010, the NHTSA report states.
        </p>
        <p>
          DUI deaths are extremely tragic because they are entirely preventable,
          said John Bisnar, founder of the Bisnar Chase personal injury law
          firm. "We're already into the holidays now, one of the most deadliest
          times of the year when it comes to drunk driving crashes. This
          incident is a tragic warning for every driver to stop and think before
          picking up those keys."
        </p>
        <p>
          Drinking and driving is simply not worth it, says Bisnar. "If you are
          attending a holiday party or event, please designate a sober driver or
          call a cab. Please do not take a chance with your life or the life of
          a loved one or a friend. One lapse in judgment can prove very costly."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California hit-and-run lawyers at Bisnar Chase represent families
          or victims of hit-and-run accidents. The firm has been featured on a
          number of popular media outlets including Newsweek, Fox, NBC, and ABC
          and is known for its passionate pursuit of results for their clients.
          Since 1978, Bisnar Chase has recovered millions of dollars for victims
          of serious personal injuries and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
