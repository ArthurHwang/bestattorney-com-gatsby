// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="FDA Recall of Drug Used to Prevent Chemotherapy Induced Nausea, Zofran"
        pageDescription="Product Liability Attorneys Bisnar Chase Weigh in on Recall of Drug Used to Prevent Chemotherapy Induced Nausea -- anti-nausea drug Zofran and its issues on cardiac side effects"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Weighs in on Recall of Drug Used to Prevent Chemotherapy
          Induced Nausea, Zofran
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            California product liability lawyer discusses anti-nausea drug
            Zofran, which was removed from the market because of the potential
            for serious cardiac risks. According to a Dec. 4 safety alert issued
            by the U.S. Food and Drug Administration (FDA), states that the
            drug, which is used to treat chemotherapy patients, has the
            potential to cause abnormal or fatal heart rhythm.
          </strong>
        </p>
        <p>
          <img
            src="/images/chemotherapy-drug-recall.jpg"
            alt="chemotherapy drug recall"
            className="imgright-fixed"
          />
          The U.S. Food and Drug Administration (FDA) is{" "}
          <Link
            to="/pharmaceutical-litigation"
            title="chemotherapy drug recall"
            onClick="linkClick(this.to)"
          >
            recalling all Zofran 32 mg.
          </Link>{" "}
          single intravenous doses because they have been linked to potentially
          serious heart problems. According to an FDA alert posted on Dec. 4,
          the drugs (generic name: ondansetron hydrochloride), which are used to
          treat nausea in cancer patients who are undergoing chemotherapy
          treatment, can lead to Torsades de Pointes, an abnormal, potentially
          fatal heart rhythm. The drugs are sold in pre-mixed solutions of
          either dextrose or sodium chloride in plastic containers, the report
          states.
        </p>
        <p>
          However, the FDA is recommending the intravenous regimen of 0.15 mg of
          Zofran administered every four hours for three doses to prevent
          chemotherapy-induced nausea and vomiting since it remains "effective
          for the prevention of nausea" for these patients, according to the
          alert.
        </p>
        <p>
          According to the Mayo Clinic's website, Torsades de Pointes has been
          defined as an arrhythmia or irregular heartbeat that is characterized
          by the two lower chambers or ventricles beating fast, making the waves
          of an ECG monitors look twisted. When such arrhythmia occurs, less
          blood is pumped out from your heart, the Mayo Clinic website states.
          Also, when less blood reaches the brain, there is an increased
          possibility that the individual will faint. The Mayo Clinic's website
          points out that when an episode persists, it can lead to
          life-threatening arrhythmia called ventricular fibrillation.
        </p>
        <p>
          Drug manufacturers have a legal obligation to consumers to properly
          test all drugs that are put in the market, said John Bisnar, founder
          of the Bisnar Chase personal injury law firm. "It is understandable
          and even acceptable that almost all medications will have some type of
          minor side effect. However, when the side effects result in lifelong
          health complications or death, it begs the question as to how that
          drug was put in the market in the first place."
        </p>
        <p>
          Drug makers also have the responsibility to recall dangerous or
          defective drugs in a timely manner, Bisnar said. "It is also important
          to get the word out to medical professionals and to consumers
          promptly. This could make a huge difference in the number of lives
          that are saved."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California hit-and-run lawyers at Bisnar Chase represent families
          or victims of hit-and-run accidents. The firm has been featured on a
          number of popular media outlets including Newsweek, Fox, NBC, and ABC
          and is known for its passionate pursuit of results for their clients.
          Since 1978, Bisnar Chase has recovered millions of dollars for victims
          of serious personal injuries and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
