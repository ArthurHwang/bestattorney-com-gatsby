// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase And Second Harvest Food Bank Give Meals To Community Residents"
        pageDescription="California Personal Injury Lawyers team up with Second Harvest Food Band of Orange County to help community residents receive essential meals."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          {" "}
          <Link to="/press-releases/second-harvest-food-bank-2011">
            Bisnar Chase and Second Harvest Food Bank of Orange County Lend a
            Hand and Provide Essential Meals to Community Residents
          </Link>
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The entire Bisnar Chase Personal Injury Attorneys staff is teaming up
          with Second Harvest Food Bank of Orange County to provide food to
          local residents in Santa Ana, California. The firm has connected with
          the Second Harvest Food Bank to sponsor their Mobile Pantry program
          which will distribute fresh produce and dry goods directly to
          residents of Orange County who are in need of food at the Southwest
          Community Center of Santa Ana.
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/3/prweb8237135.htm">
            PRWEB
          </Link>
          ) March 24, 2011
        </p>
        <p>
          On April 4th from 10:00 a.m. to 1:00 p.m. Second Harvest Food Bank's
          Mobile Pantry along with enthusiastic Bisnar Chase Personal Injury
          Attorneys staff members will be at the Southwest Community Center
          passing out fresh food to local families and individuals. In less than
          half a day, the staff will provide almost 4,000 meals that will reach
          around 200 families of Santa Ana, California. Bisnar Chase Personal
          Injury Attorneys is proud to be directly involved in bringing a
          lifeline of much-needed assistance to people that otherwise would not
          have been given it.
        </p>
        <h2>
          Mobile Pantry Brings Food Straight to the People Who Need it Most
        </h2>
        <p>
          The Second Harvest Food Bank Mobile Pantry program gives individuals
          and businesses an opportunity to contribute to their community by
          delivering fruit, vegetables, and dry goods to the people who need it
          most. Each week the pantries are setup and with each truck delivery
          they provide an estimated 5,000 pounds of fresh produce and food items
          to Orange County residents.
        </p>
        <h2>Hunger is an Epidemic</h2>
        <p>
          It affects people from all demographics. There are an estimated
          615,000 individuals who struggle with hunger in Orange County and of
          these people 77% of them do not qualify for government food assistance
          programs and solely rely on food banks for all of their meals.
        </p>
        <h2>About Second Harvest Food Bank of Orange County</h2>
        <p>
          Second Harvest Food Bank of Orange County, a member of Feeding
          America, is the largest private hunger relief organization in the
          county, partnering with more than 470 charitable organizations to feed
          an average of 240,000 people each month. Since 1983, Second Harvest
          Food Bank of Orange County has distributed more than 272 million
          pounds of safe and nutritious surplus food. For every dollar that is
          donated, 94.6 cents goes directly to hunger-relief programs. To learn
          more, please visit{" "}
          <Link to="http://www.feedoc.org">http://www.feedoc.org.</Link>
        </p>
        <h2>About Southwest Community Center</h2>
        <p>
          Founded in 1970 by Annie Mae Tripp, a domestic worker with seven
          children, the Southwest Community Center has been serving the
          low-income residents of Orange County for more than 35 years. Their
          mission is to provide the basic needs of food, clothing, shelter,
          household items, referral services and other support services to the
          homeless, low-income and economically disadvantaged in an effort to
          enable them to become or retain self-sufficiency.
        </p>
        <p>
          Annie Mae embarked on her mission by serving meals of soup and bread
          to 12 homeless men from her garage and by 1971 she was able to rent a
          building for the purpose of ministry to the poor. Along with three
          extraordinary women, Betty Thompson, Jean Forbath, and Bernice
          Randford, Annie Mae sought out churches, people and organizations for
          support. Annie Mae's legacy lives on through the center and now
          provides 10,000 meals a month, distributes food baskets, clothing and
          provides many other services to those in need.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys's Los Angeles attorneys
          represent people who have been very seriously injured or lost a family
          member due to an accident, defective product or negligence throughout
          California. The law firm has won a wide variety of challenging
          personal injury and defective product cases, involving car accidents,
          work related injuries, dog attacks and defective products. Visit their
          blog at bestatto-gatsby.netlify.app/blog
        </p>
        <p>
          {" "}
          <Link to="/press-releases/community-outreach">
            Click Here For More Community Outreach Press Releases
          </Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
