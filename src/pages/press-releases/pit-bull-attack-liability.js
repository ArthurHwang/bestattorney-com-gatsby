// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Sheriff's Deputy Shoots and Kills Pit Bull After Animal Attack"
        pageDescription="A male and female pit bull attacked a young boy and his mother. Injury lawyer talks about the liability in dog bite cases in California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Pit Bull Attacks Child, Then Mother. Injury Lawyer Talks CA Liability
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10408733.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on Feb, 08, 2013.
          </i>
        </p>
        <p>
          <b>
            Video: Injury Lawyer, John Bisnar Discusses Dog Bite Injury
            Liability in California.
          </b>
        </p>
        <center>
          <iframe
            width="420"
            height="315"
            src="https://www.youtube.com/embed/_LQ8M7WPjik?rel=0"
            frameborder="0"
            allowfullscreen
            title="pit bull death"
          ></iframe>
        </center>
        <p>
          Kayla Shoestring, 29, and her son Hayden, 4, were injured in a dog
          attack after they were{" "}
          <Link to="/dog-bites">bitten by a pit bull</Link> owned by Glenda
          Aguilar whom they were visiting at the time. According to a Feb. 5
          news report in The Independent, the incident occurred the evening of
          Feb. 6 in the backyard of Aguilar's home in Grand Hill, Neb.
        </p>
        <p>
          The Shoestrings were visiting Aguilar at her home when Hayden went to
          the backyard with a male and female pit bull, the report stated. His
          mother ran out when she heard her son screaming and saw that he was
          being attacked by a male dog. When she tried to intervene and pulled
          her son away, the male pit bull turned on Shoestring, chased her and
          pinned her against the house according to the report.
        </p>
        <p>
          While Hayden suffered a bite to the face, his mother suffered several
          puncture wounds and significant damage to the muscle and tendons in
          her forearms. Both dogs were confiscated by the authorities, according
          to the report. The article also states that the female pit bull was in
          heat at the time and that the male pit bull, which attacked, was not
          neutered.
        </p>
        <p>
          In California, much like Nebraska, where this dog attack took place,
          the law works in favor of dog bite victims, said John Bisnar, founder
          of the Bisnar Chase personal injury law firm. "Both states have a
          strict liability statute when it comes to dog bites. This means that a
          dog owner is responsible for the actions of his or her pet &#150; no
          ifs or buts.
        </p>
        <p>
          There are a few exceptions such as when a victim has trespassed or
          provoked the animal. But in most cases, the dog owner can be held
          financially responsible for the injuries, damages and losses suffered
          by the victim."
        </p>
        <p>
          California&#146;s Civil Code Section 3342 states: "The owner of any
          dog is liable for the damages suffered by any person who is bitten by
          the dog while in a public place or lawfully in a private place,
          including the property of the owner of the dog, regardless of the
          former viciousness of the dog or the owner&#146;s knowledge of such
          viciousness."
        </p>
        <p>
          Bisnar says dog bite victims often suffer significant physical pain
          and mental anguish. "As in this case, a dog attack can result in
          debilitating injuries as well as severe emotional distress for the
          injured victims and those who view such a vicious attack. It is
          critical that dog bite victims seek prompt medical attention so they
          can get the treatment and procedures they need to repair the injuries
          and scars."
        </p>
        <p>
          "Children are especially vulnerable to these types of injuries, both
          physically and emotionally. Parents should exercise extreme caution
          when allowing their children around large breed dogs that have a
          tendency to cause more damage when do they bite."
        </p>
        <p>
          If you've been injured from a dog bite or animal attack, call the
          Personal Injury Attorney of Bisnar Chase at{" "}
          <b>949-203-3814 for a free consultation.</b>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
