// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Files Lawsuit Against the State of California, County of Los Angeles, and City of Alhambra"
        pageDescription="Since 1978, our California dangerous property lawyers have been helping victims just like you.  Call 1-866-868-4452 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Files Lawsuit Against the State
          of California, County of Los Angeles, and City of Alhambra
        </h1>
        <BreadCrumbs location={location} />

        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/6/prweb8518622.htm">
            PRWEB
          </Link>
          ) April 27, 2011
        </p>
        <p>
          The personal injury lawyers of Bisnar Chase Personal Injury Attorneys
          filed a lawsuit against the State of California, County of Los
          Angeles, and City of Alhambra for Dangerous Condition of Public
          Property that allegedly caused Tony C. Simpson to be killed by an
          oncoming vehicle. According to court documents, in the early morning
          of March 23, 2010, Simpson was driving his motorcycle on the freeway
          when he pulled to the left in order to enter the carpool lane. At this
          moment, Simpson's motorcycle came upon an unexpected and unmarked,
          uneven pavement, which is said to have caused him to lose control of
          his motorcycle and tip over in the middle of the lane. A bus traveling
          in the carpool lane had no time to avoid hitting Simpson who was
          directly in its driving path. The records state, Simpson was struck by
          the bus and killed on impact.
        </p>
        {/* <img
          src="/images/BC.jpg"
          alt="Bisnar Chase Personal Injury Attorneys"
          className="imgright-fluid"
        /> */}
        <h2>Alleged Dangerous Public Property Leaves Man Dead</h2>
        <p>
          The wrongful death attorneys of Bisnar Chase Personal Injury Attorneys
          claim the highway's uneven pavement made for dangerous driving
          conditions that were not observable by the motoring public and the
          uneven pavement was ultimately the source of Simpson's death. The suit
          goes on to state; the freeway's pavement had a four-inch drop-off and
          was too steep for the motoring public to drive across safely and
          without incident. The firm asserts the roadway was a hazard to drivers
          who had no way of knowing the drop-off was present.
        </p>
        <h2>
          Linda Jones, Mother of Tony C. Simpson, Seeks Retribution for Sons
          Death and Is Denied
        </h2>
        <p>
          On September 14, 2010 Jones filed a claim with the State of
          California, County of Los Angeles and City of Alhambra pursuant to
          Government Code sections 905 and 910 . Each claim was rejected, so
          Jones has been forced to file a lawsuit against these government
          entities to obtain compensation for the damages she has suffered
          because of her serious and permanent injuries.
        </p>
        <h2>
          Bisnar Chase Personal Injury Attorneys seeks Justice for Tony C.
          Simpson and Linda Jones
        </h2>
        <p>
          According to court records, the dangerous condition of the public
          property in which Simpson was driving his motorcycle on was the legal
          cause of his death and all damages incurred by Linda Jones. The suit
          claims the four-inch drop off exposed the motoring public and the
          deceased Simpson to the risks and dangers of not being able to safely
          navigate into the carpool lane. These alleged dangers created an
          unsafe condition of the public property, which should have been
          foreseeable and preventable by the defendants. Bisnar Chase Personal
          Injury Attorneys asserts all public entities had the resources
          reasonably available to properly correct the defective roadway
          condition and they failed to warn the public of the hazardous
          drop-off. Consequently, Jones is now seeking recovery for the wrongful
          death of her son caused by the defendant's alleged disregard to remedy
          the dangers of the roadway.
        </p>
        <p>
          Jones claims to have suffered severe emotional damage as well as
          economic damages from the loss of her son and Bisnar Chase Personal
          Injury Attorneys will seek all recoverables she is due. Linda Jones
          along with the wrongful death attorneys of Orange County filed a suit
          against State of California, County of Los Angeles and City of
          Alhambra on February 28, 2011 with Superior Court of Los Angeles. The
          case is currently pending, case # GC 046915
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys Orange County Personal
          Injury Lawyers represent people who have been very seriously injured
          or lost a family member due to an accident, defective product or
          negligence throughout California. The law firm has won a wide variety
          of challenging cases against governmental agencies, including school
          districts, Caltrans, cities, the State of California and the U.S.
          Federal Government. Get a complimentary copy of The Seven Fatal
          Mistakes That Can Wreck Your California Personal Injury Claim. For
          more information, please visit http://www.CaliforniaInjuryBlog.com.
        </p>
        <p>
          {" "}
          <Link to="/motorcycle-accidents/press-releases">
            Click Here For More Motorcycle Accident Press Releases
          </Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
