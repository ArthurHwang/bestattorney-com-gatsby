// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Files Lawsuit against Huntington Beach School for Discriminatory and Wrongful Termination Infractions"
        pageDescription="The Bisnar Chase attorneys have filed a lawsuit against The Pegasus School in Huntington Beach, California for discrimination against a teacher afflicted with the HIV virus."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Files Lawsuit against Huntington Beach School for
          Discriminatory and Wrongful Termination Infractions
        </h1>
        <BreadCrumbs location={location} />
        <p>
          June 14, 2012 -- NEWPORT BEACH, California -- Bisnar Chase Personal
          Injury Attorneys (/) of Orange County, California today announced the
          firm's employment attorneys have filed a lawsuit (case number: 30-2012
          00565920) against The Pegasus School in Huntington Beach, California
          for discrimination against a teacher afflicted with the HIV virus.
        </p>
        <p>
          The lawsuit charges The Pegasus School with multiple infractions,
          including workplace discrimination due to sexual orientation, medical
          condition and physical disability; creating and/or knowingly
          permitting harassment and a hostile work environment; breach of
          contract; wrongful termination; and violations of business and
          professions codes.
        </p>
        <p>
          <strong>
            Middle School Science Teacher Harassed, Verbally Abused Due to HIV
            Disease, according to attorney Brian Chase.
          </strong>
        </p>
        <p>
          <strong>Matthew Edmondson</strong>, a beloved middle school science
          teacher who had worked at The Pegasus School in Huntington Beach,
          California for four years before leaving to teach in Texas, was asked
          to return to the school to teach 8th grade science in the spring of
          2007.
        </p>
        <p>
          Past administrators and staff were aware Matthew was a homosexual male
          who was infected with the HIV virus, with no reported incidents of
          workplace harassment or abuse.
        </p>
        <p>
          In October 2011, Matthew's doctor changed his medication which caused
          adverse side effects. Despite these adverse side effects,{" "}
          <strong>
            Matthew never used more than his allotted personal time off
          </strong>{" "}
          (or more than 10 days) during the 2011 - 2012 school year. In fact,
          throughout his tenure from 2002 on, he rarely used personal time off.
        </p>
        <p>
          On numerous occasions, Matthew communicated the side effects of his
          new medication to recently appointed Head of School,{" "}
          <strong>John Zurn</strong> and <strong>Joseph Williamson</strong>, the
          middle school director.
        </p>
        <p>
          According to court documents both{" "}
          <strong>
            Zurn and Williamson waged a discrimination campaign against Matthew
          </strong>{" "}
          starting in February 2012, including a series of randomly scheduled
          meetings under the guise of discussing Matthew's curriculum plans for
          the coming year even though no other Pegasus teachers were asked to
          meet to discuss their curriculum plans. It was during these meetings
          that Zurn and Williamson demanded to know Matthew's reasons for
          personal time off while criticizing his teaching methods for the first
          time in his career, despite Matthew's glowing 9-year teaching
          evaluations.
        </p>
        <p>
          Matthew unsuccessfully attempted to record one of the randomly
          scheduled meetings in an effort to document the harassment to which he
          was subjected. Upon learning of this, Zurn and Williamson demanded
          that Matthew sign a Confidential Agreement and Release on March 21 or
          they would contact the District Attorney's office and have him
          arrested and prosecuted.
        </p>
        <p>
          Under duress, Matthew signed the agreement two days later and left the
          school.
        </p>
        <h2>
          Bisnar Chase Sues The Pegasus School for Discrimination and Wrongful
          Termination
        </h2>
        <p>
          "This is a textbook case of workplace discrimination," said{" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link>, partner at
          Bisnar Chase Personal Injury Attorneys. "What's tragic is that this is
          a respected teacher, friend and colleague who has spent his entire
          professional career educating children and helping others. Simply put,
          Matthew was forced out of his job due to egregious verbal abuse,
          derogatory comments, insults and threats of criminal prosecution for
          no other reasons than his sexual orientation and medical condition."
        </p>
        <p>
          The lawsuit seeks compensatory damages, general damages for emotional
          distress and pain and suffering, restitution for lost employment
          benefits, a preliminary and permanent injunction requiring The Pegasus
          School to cease and desist unlawful business practices, and for
          related court costs.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          {" "}
          <Link to="/">Bisnar Chase Personal Injury Attorneys</Link> represent
          people who have been very seriously injured or lost a family member
          due to a <Link to="/">California personal injury accident</Link>, car
          accident, defective product or negligence throughout the state.{" "}
          <Link to="/employment-law">
            The Bisnar Chase California Employment Attorneys
          </Link>{" "}
          represent people who have been the victims of employer abuse which
          includes wage and overtime claims, wrongful termination, sexual
          harassment claims, disability discrimination, overtime pay claims,
          breach of contract complaints and more. The firm specializes in
          representing people and never businesses, corporations, insurance
          companies or governmental agencies. Bisnar Chase has helped thousands
          of people with traffic accidents, defective products, dog bites,
          nursing home abuse, traumatic brain injuries and paralysis cases. For
          more information, please visit /.
        </p>
        <p>
          {" "}
          <Link to="/press-releases/miscellaneous">
            Click Here For More Miscellaneous Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
