// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Death of Woman Charged in Fatal Hit and Run Crash"
        pageDescription="California hit-and-run lawyer comments about the death of 50-year-old Danielle Halverson, who, according to a Dec. 3 news report in The Journal-Review, may have died of an overdose."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Weighs in on Death of Woman Charged in Fatal Hit and Run
          Crash
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            California hit-and-run lawyer comments about the death of
            50-year-old Danielle Halverson, who, according to a Dec. 3 news
            report in The Journal-Review, may have died of an overdose. The
            report states Halverson was arrested and charged in connection with
            a March 17 hit-and-run crash that left 14-year-old Julianna Hobbs
            dead and 14-year-old Rachel Russell with injuries.
          </strong>
        </p>
        <p>
          {/* <img
            src="/images/charged-with-hit-and-run-accident.jpg"
            alt="hit and run accident"
            className="imgright-fluid"
          /> */}
          Danielle Halverson, 50, was found dead in her Florida home according
          to a Dec. 3 news report in The Daytona Beach Journal-Review. The
          article states that Halverson was charged in connection with a{" "}
          <Link
            to="/pedestrian-accidents/hit-and-run"
            title="Hit and run accident"
          >
            hit-and-run
          </Link>{" "}
          collision on March 17, which killed Julianna Hobbs, 14 and left Rachel
          Russell, also 14, with injuries.
        </p>
        <p>
          Halverson was charged after she admitted to Florida Highway Patrol
          investigators that she hit the teens after losing control of her car
          while reaching for her cell phone, the report states.
        </p>
        <p>
          Officials believe that Halverson may have died from some type of
          overdose, said sheriff's spokesman Gary Davidson. Davidson said based
          on the circumstances, lack of injury and a note left by Halverson.
        </p>
        <p>
          "This is simply a tragic situation and my thoughts and prayers are
          with everyone concerned," said John Bisnar, founder of the Bisnar
          Chase personal injury law firm and the Hit-and-Run Reward Program,
          which offers a $1,000 reward for any tip that results in the arrest
          and felony conviction of the hit-and-run driver.
        </p>
        <p>
          In all states including California, leaving the scene of an accident
          where someone has been injured or killed is a serious crime.
          California Vehicle Code 20001 (a) states: "The driver of a vehicle
          involved in an accident resulting in injury to a person, other than
          himself or herself, or in the death of a person shall immediately stop
          the vehicle at the scene of the accident."
        </p>
        <p>
          Bisnar says hit-and-run victims and their families often bear the
          brunt of the incident."It takes a physical, emotional and financial
          toll not only on victims, but also their families. Injured victims
          often face significant losses in the form of medical expenses, lost
          earnings, cost of hospitalization, surgeries, physical therapy and
          medical devices."
        </p>
        <p>
          Whether it is driving while distracted by a cell phone or leaving the
          scene of a crash, these are actions that can have tragic consequences,
          Bisnar says. "Drivers must be vigilant at all times. That means
          staying off cell phones or other types of in-vehicle distractions. The
          law requires any driver involved in a car accident to stop
          immediately, regardless of who was at fault."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California hit-and-run lawyers at Bisnar Chase represent families
          or victims of hit-and-run accidents. The firm has been featured on a
          number of popular media outlets including Newsweek, Fox, NBC, and ABC
          and is known for its passionate pursuit of results for their clients.
          Since 1978, Bisnar Chase has recovered millions of dollars for victims
          of serious personal injuries and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
