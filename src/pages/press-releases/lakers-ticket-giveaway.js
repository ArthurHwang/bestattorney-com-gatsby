// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Win Two Free Los Angeles Lakers Tickets in the Bisnar Chase Personal Injury Attorneys 'Jack-O-Lakers' Contest"
        pageDescription="Call 949-203-3814 for Highest-rated Personal Injury Attorneys.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Win Two Free Los Angeles Lakers Tickets in the Bisnar Chase Personal
          Injury Attorneys "Jack-O-Lakers" Contest
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Avid Lakers fans at Bisnar Chase Personal Injury Attorneys look for
            most creative Los Angeles Lakers jack-o-lantern.
          </strong>
        </p>
        <p>
          Newport Beach, CA{" "}
          <Link to="http://www.prweb.com/releases/bisnar-chase/pumpkin-contest/prweb4669914.htm">
            (Vocus)
          </Link>{" "}
          October 19, 2010
        </p>
        <p>
          <img
            src="/images/lakers-winner.jpg"
            alt="lakers halloween"
            className="imgright-fixed"
          />
          As avid Los Angeles Lakers fans, the Newport Beach personal injury
          attorneys of Bisnar Chase Personal Injury Attorneys today announced
          the launch of the first ever Bisnar Chase Personal Injury Attorneys
          "Jack-O-Lakers" contest.
        </p>
        <p>
          The contest, open to the public and judged by the Bisnar Chase
          Personal Injury Attorneys staff, will award two free Los Angeles
          Lakers tickets to the person who makes the most creative Los Angeles
          Lakers jack-o-lantern.
        </p>
        <p>
          The rules are simple. Entrants must be 18 years of age or older, must
          be residents of California and must use a real pumpkin to be eligible
          to win. Submissions may be carved, painted or decorated.
        </p>
        <p>
          A digital photo of your Jack-O-Lakers creation should be submitted to
          Danielle Olson at Bisnar Chase Personal Injury Attorneys via email at
          dolson@bisnarchase.com by 5:00 pm PDT on October 28, 2010. Please
          include your name, street address and phone number in the email.
        </p>
        <p>
          The Bisnar Chase Personal Injury Attorneys staff will review the
          submissions and pick the winner. The winner will be announced at 12:00
          pm PDT on October 31, 2010 at the Bisnar Chase Personal Injury
          Attorneys website, bestatto-gatsby.netlify.app. The top submissions
          and the winner will also be featured in the November 2010 Bisnar Chase
          Personal Injury Attorneys newsletter.
        </p>
        <p>
          The top winner will receive two free Los Angeles Lakers tickets to an
          upcoming 2010-11 regular season game. These are "lower level" seats --
          much better than nose bleed seats.
        </p>
        <p>
          "One look at our conference room and you know we're diehard Lakers
          fans here at Bisnar Chase Personal Injury Attorneys," said John
          Bisnar, founder of the personal injury law firm. "We thought this
          contest would be a great way to the public involved in supporting the
          team this season while celebrating Halloween in the process. We're
          looking forward to seeing just how creative Los Angeles Lakers fans
          can be."
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Top-rated Bisnar Chase Personal Injury Attorneys have represented
          thousands of Californians who have been very seriously injured or lost
          a family member due to an accident, defective product or negligence
          throughout California. The law firm promotes auto safety by pursuing
          makers of defective vehicles, governmental agencies responsible for
          unsafe roadways and offering rewards for turning in hit and run
          drivers. For more information, visit http://www.HitAndRunReward.com
          and http://www.ProductDefectNews.com.
        </p>
        <p>###</p>
        {/* <p>
          {" "}<Link to="/offices.html">Click Here</Link> for a list of local
          offices.
        </p> */}
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
