// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Product Recall - Glass Shards Found in Lean Cuisine Mushroom Ravioli"
        pageDescription="A product recall has been issued for Nestle's Lean Cuisine mushroom ravioli after multiple reports came in about glass in the entrees."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Glass Shards Found in Lean Cuisine Mushroom Ravioli</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10429350.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 13, 2013.
          </i>
        </p>
        <img
          src="/images/food-recall-attorneys.jpg"
          width="200"
          alt="Food Recall Attorneys"
          className="imgright-fixed"
        />
        <p>
          Nestle has recalled 500,000 dishes of its Lean Cuisine mushroom
          ravioli after three consumers reported finding glass fragments in the
          entrees. According to a Feb. 12 news report in USA Today, no injuries
          or illnesses have been reported as a result of the recalled products.
        </p>
        <p>
          Nestle has issued a voluntary{" "}
          <Link to="/defective-products" onClick="linkClick(this.to)">
            product defect recall
          </Link>{" "}
          for more than 500,000 dishes of its frozen Lean Cuisine mushroom
          ravioli after three customers reported finding glass shards in the
          entrees.
        </p>
        <p>
          According to a Feb. 12 USA Today news, no other products are involved
          in the recall and so far, no injuries or illnesses have been reported.
          The recall covers packages of Culinary Collection Mushroom Mezzaluna
          Ravioli produced in early November and distributed nationwide, the
          report states.
        </p>
        <p>
          The recalled packages were marked with the UPC number 13800-58358,
          product codes of 2311587812 and 2312587812 and a "best before" date of
          December 2013, according to the company's{" "}
          <Link
            to="http://www.nestleusa.com/Media/press-releases/LeanCuisineRecallFeb2013.aspx"
            onClick="linkClick(this.to)"
          >
            news release
          </Link>{" "}
          issued Feb. 8. The news release states that the recalled entrees were
          manufactured in early November 2012.
        </p>
        <p>
          Since these are popular products, Nestle officials state in the
          release that few of these products remain in distribution, the release
          states. The company is asking consumers to examine their freezers to
          ensure that they do not have any of the entrees left. If they do have
          them, Nestle asks that they refrain from consuming the product and
          instead call Nestle Consumer Services at 1-866-586-9424 or email
          leancuisine(at)casupport.com for further information and instructions.
          The company states in the release that no other products were involved
          in the recall.
        </p>
        <p>
          "Manufacturers have a legal obligation to ensure that their products
          are safe for consumers," said John Bisnar, founder of the Bisnar Chase
          personal injury law firm. "It is up to companies to have strong
          quality control systems in place to ensure that the products they make
          are not defective and that they are safe for people to consume. It is
          indeed fortunate that no one was injured by these glass fragments in
          the food"
        </p>
        <p>
          "A voluntary recall by the manufacturer is a step in the right
          direction. This type of "early action" on their part can help avoid
          injuries later on. Food products in particular can cause serious
          injury or illness if contaminated," he said. "Anyone who has been
          injured as the result of a defective product has legal rights. Injured
          victims in such cases can file a product liability claim against the
          manufacturer of the defective product seeking damages for the
          injuries, damages and losses they may have sustained."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          Bisnar Chase represents victims of defective products such as
          contaminated food. The firm has been featured on a number of popular
          media outlets including Newsweek, Fox, NBC, and ABC and is known for
          its passionate pursuit of results for their clients. Since 1978,
          Bisnar Chase has recovered millions of dollars for victims of
          defective products.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>Sources:</p>
        <p>
          http://www.usatoday.com/story/news/nation/2013/02/12/lean-cuisine-recall-glass/1912307/
        </p>
        <p>
          http://www.nestleusa.com/Media/press-releases/LeanCuisineRecallFeb2013.aspx
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
