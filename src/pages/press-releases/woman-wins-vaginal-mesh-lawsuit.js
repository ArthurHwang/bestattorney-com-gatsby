// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Jury Awards $3.35 Million to Woman Involved in a Vaginal Mesh Lawsuit"
        pageDescription="A jury in New Jersey said Johnson & Johnson must pay a South Dakota woman $3.35 million for failing to adequately warn her doctor about the potential dangers of a transvaginal mesh implant."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Jury Awards $3.35 Million to Woman Involved in a Vaginal Mesh Lawsuit
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10474128.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          A New Jersey jury has awarded $3.35 million to 47-year-old Linda
          Gross, who filed a vaginal mesh lawsuit alleging that Johnson &
          Johnson and its subsidiary, Ethicon, did not warn her doctor of the
          potential dangers of the mesh implant and misrepresented the product
          in marketing brochures.
        </p>

        <p>
          A Feb. 25 Reuters news article states that this verdict, the first
          among 1,800 pending{" "}
          <Link to="/medical-devices/vaginal-mesh-lawsuits">
            vaginal mesh lawsuits
          </Link>
          , could have a significant impact on thousands of lawsuits filed
          against other makers of similar products.
        </p>
        <p>
          Gross's lawsuit, which was filed in state Superior Court in Atlantic
          City, alleged that the Gynecare Prolift vaginal mesh implant was not
          safe. The article states that Gross filed her lawsuit after having
          surgery in 2006 to install the implant and treat her pelvic organ
          prolapse.
        </p>
        <p>
          She alleged in her suit that instead of alleviating her problems, the
          mesh implant caused severe complications including mesh erosion, scar
          tissue, inflammation, infection and nerve damage.
        </p>
        <p>
          Gross not only had to seek medical treatment, but also had to undergo
          18 surgeries to repair the damage caused by the mesh, the article
          states. The nine-member jury arrived at the verdict after a six-week
          trial and the judge has ruled that she will allow arguments on
          punitive damages, Reuters reports.
        </p>
        <p>
          "With this verdict, the panel of six women and three men has delivered
          a strong and timely statement to Johnson & Johnson, Ethicon and
          several other manufacturers of vaginal mesh implants. A large verdict
          such as this tells us that the jury and the court deeply understood
          the pain and suffering of Ms. Gross. To endure 18 surgeries to correct
          one surgery is unthinkable.
        </p>
        <p>
          These corporations cannot put profits before consumer safety.
          Thousands of plaintiffs who continue to suffer excruciating pain and
          severe complications as a result of defective products are looking to
          our civil justice system for relief."
        </p>
        <p>
          Bisnar said the verdict in this case gives hope to thousands of other
          such victims who are said to be suffering in silence and waiting in
          line to have their case heard.
        </p>
        <p>
          "These lawsuits as they unfold will hopefully create public awareness
          about vaginal mesh implants. Had these women and their doctors
          received accurate information regarding the potential dangers of these
          mesh implants, they could have made a better and informed decision
          about the course of their treatment."
        </p>
        <p>
          For more information, please call 949-203-3814 for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>
          Source: <br />
          http://www.reuters.com/article/2013/02/25/us-jj-vaginalmesh-idUSBRE91O0WI20130225
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
