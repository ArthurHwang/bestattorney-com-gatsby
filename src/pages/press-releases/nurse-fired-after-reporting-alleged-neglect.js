// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nurse Fired after Allegedly Neglecting Patient"
        pageDescription="A nurse filed a lawsuit against her former employer after they fired her, claiming she was fired after reporting elder neglect and abuse at the nursing home."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Nurse Fired after Reporting Alleged Patient Neglect &amp; Files
          Lawsuit
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10413453.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 11, 2013
          </i>
        </p>
        <p>
          A licensed vocational nurse filed a lawsuit (Case Number
          4:13-cv-00051) against her former employer, claiming that she was
          fired after making good faith reports of patient neglect and abuse to
          supervisors. According to a Feb. 5 article in The Southeast Texas
          Record, Susan J. Rapp was a nurse assigned to a patient's home health
          care and lodged a complaint against another nurse who also cared for
          the same patient.
        </p>
        <img
          src="/images/employment-lawsuit.jpg"
          width="195"
          height="129"
          border="0"
          alt="Employment lawsuit"
          className="imgright-fixed"
        />
        <p>
          Susan J. Rapp, a licensed vocational nurse, has filed a lawsuit
          against Maxim Healthcare Services Inc., alleging that she was let go
          after reporting patient neglect and abuse, according to a Feb. 5{" "}
          <Link
            to="http://setexasrecord.com/news/281181-nurse-sues-former-employer-after-losing-job-for-reporting-patient-neglect"
            onClick="linkClick(this.to)"
          >
            news report
          </Link>{" "}
          in The Southeast Texas Record. The lawsuit (Case Number
          4:13-cv-00051), which was filed in the Eastern District of Texas,
          Sherman Division, alleges that Rapp was fired for reporting a day
          nurse's neglect and abuse of a patient she cared for at night.
        </p>
        <p>
          Rapp alleges in the{" "}
          <Link to="/employment-law" onClick="linkClick(this.to)">
            employment lawsuit
          </Link>{" "}
          that the problems she reported were not resolved and that she was
          further instructed by her supervisors not to document what she
          witnessed or the effects it had on the patient in the patient's
          medical records. Two weeks after she made the report, Rapp was
          terminated for allegedly "exhibiting unprofessional behavior, crossing
          professional boundaries, and inappropriately documenting her patient's
          medical records," The Record states.
        </p>
        <p>
          The lawsuit (Case Number 4:13-cv-00051) accuses Maxim Healthcare of
          retaliating against Rapp for reporting a violation of the state's
          Health and Safety Code. The suit also asks the court to issue an
          injunction to stop the defendant from further unlawful employment
          practices and rehire Rapp, according to court documents.
        </p>
        <p>
          It is illegal to fire an employee for reporting a violation of state
          or federal law in good faith, said John Bisnar, founder of the Bisnar
          Chase personal injury law firm. "There are state and federal laws that
          allow employees to report safety violations or wrongdoing in good
          faith. They have rights and cannot be retaliated against for doing the
          right thing."
        </p>
        <p>
          Suing one's employer or former employer can be a stressful process,
          Bisnar said. "In most cases, these plaintiffs are fighting to clear
          their name and reputation. They are looking to have their day in court
          to prove that they did nothing wrong. It is not easy to stand up and
          fight against your employer or former employer. That is why plaintiffs
          in these types of cases require the help and guidance of an
          experienced employment lawyer who can help them navigate these
          troubled waters."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California employment lawyers of Bisnar Chase represent victims of
          discrimination, harassment and retaliation at the workplace. The firm
          has been featured on a number of popular media outlets including
          Newsweek, Fox, NBC, and ABC and is known for its passionate pursuit of
          results for their clients. Our offices are located at 1301 Dove St.
          Newport Beach, CA 92660.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove Street #120 <br />
          Newport Beach, CA 92660
        </p>
        <p>
          Sources:
          http://setexasrecord.com/news/281181-nurse-sues-former-employer-after-losing-job-for-reporting-patient-neglect
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
