// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brian Chase of Bisnar Chase Nominated for Prestigious CAALA Trial Lawyer of the Year Award"
        pageDescription="Nation's leading personal injury attorney and auto defects expert adds CAALA Trial Lawyer of the Year nominee to growing list of accolades."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Brian Chase of Bisnar Chase Nominated for Prestigious CAALA Trial
          Lawyer of the Year Award
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <em>
            Nation's leading personal injury attorney and auto defects expert
            adds CAALA Trial Lawyer of the Year nominee to growing list of
            accolades.
          </em>
        </p>
        <p>
          <img
            className="imgleft-fixed"
            src="/images/brianchase.jpg"
            alt="Brian Chase CAALA Award"
          />
          Newport Beach, CA (PRWEB) October 1, 2012
        </p>
        <p>
          Newport Beach, CA, October 1, 2012 Newport Beach, CA, October 1, 2012
          - Bisnar Chase California Personal Injury Attorneys, LLP
        </p>
        <p>
          (www.bestatto-gatsby.netlify.app) today announced partner Brian Chase,
          one of the nation's leading personal injury attorneys and auto defects
          experts, has been nominated by the Consumer Attorneys Association of
          Los Angeles (CAALA) for its prestigious "Trial Lawyer of the Year"
          award. The winner will be announced at CAALA's Annual Installation and
          Awards Dinner this coming January.
        </p>
        <p>
          With nearly 2,800 members, CAALA is the nation's largest local
          association of plaintiffs' attorneys representing a powerful advocacy
          for victims' rights and equal access to justice. Like Brian Chase, the
          association's consumer attorneys protect people from unsafe products,
          unsafe medicine, unfair business practices and unscrupulous, negligent
          corporate conduct, subscribing to the highest standards of quality
          legal representation and ethical conduct.
        </p>
        <p>
          Since 1972, CAALA has paid tribute to distinguished members of the
          trial bar, prominent jurists, and other individuals who have made
          significant contributions to protect the civil justice system.
        </p>
        <p>
          Chase's selection as a CAALA Trial Lawyer of the Year nominee is just
          one in a rapidly growing list of accolades he's received so far this
          year, in addition to the numerous awards he's earned throughout his
          19-year career as a leading personal injury attorney.
        </p>
        <p>
          "Brian's newest Trial Lawyer of the Year nomination is a reflection of
          his passionate representation of our clients and the outstanding
          results he obtains for them," said John Bisnar, founder of Bisnar
          Chase. "He's a force to be reckoned in the courtroom and defense law
          firms know it. No one works harder or more enthusiastically to make
          things right for our clients and to hold wrongdoers accountable. He is
          very deserving of the awards he's earned through the years."
        </p>
        <p>
          Chase is a native of Southern California and a graduate of
          Pepperdine's law school. He's litigated dozens of jury trials and has
          litigated both local and national auto product liability and
          catastrophic injury cases, obtaining judgments and settlements
          exceeding $100 million.
        </p>
        <p>
          His expertise lies in making automakers accountable for defective
          vehicles that have caused fatal and catastrophic injuries.
        </p>
        <p>
          Chase's professional accolades are numerous. Last month, he was named
          a finalist for the Consumer Attorney of the Year Award by Consumer
          Attorneys of California (CAOC) and a 2012 Orange County Top Lawyer by
          Avvo for a third time.
        </p>
        <p>
          Other professional honors include:
          /press-releases/brian-chase-caala-trial-lawyer.html
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The{" "}
          <Link
            to="/orange-county/car-accidents"
            title="Orange County auto accident lawyer"
          >
            Orange County auto accident lawyers
          </Link>{" "}
          at Bisnar Chase represent people who have been severely injured or
          have lost a family member due to a traffic accident, defective
          product, or negligence. The law firm has obtained nearly two hundred
          million dollars in settlements and verdicts for their clients by
          winning a wide variety of challenging personal injury cases. The law
          firm also represents people who have been seriously injured or lost a
          family member due to drunk driving and hit-and-run collisions.
        </p>
        <p>
          If you know of someone seriously injured in an auto accident, call
          949-203-3814 or fill out the online form at:{" "}
          <Link to="/" onClick="linkClick(this.to)">
            /
          </Link>{" "}
          for a free evaluation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
