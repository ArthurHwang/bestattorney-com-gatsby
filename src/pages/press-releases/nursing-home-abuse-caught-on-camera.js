// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Granddaughter Places Hidden Camera in Nuraing Home Abuse Case"
        pageDescription="A New York woman placed a hidden camera in her 89-year-old grandmother's room after noticing unusual markings and bruising on her body and accuses Gold Crest Care Center of nursing home abuse"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>New York Nursing Home Abuse Caught on Camera</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10451936.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          Diana Valentin says she suspected that her elderly grandmother was
          possibly being abused in a Bronx nursing home and alleges it was true
          after placing a hidden camera in the 89-year-old resident's room at
          Gold Crest Care Center.
        </p>
        <p>
          According to a Feb. 15 news report, Valentin placed a hidden camera
          after noticing bruising and unusual markings on her grandmother, Ana
          Louisa Medina. She alleges that the injuries were caused by a nursing
          aide at the center, the report states.
        </p>
        <center>
          <iframe
            width="420"
            height="315"
            src="https://www.youtube.com/embed/520_pSDAx_s?rel=0"
            frameborder="0"
            allowfullscreen
            title="youtube"
          ></iframe>
        </center>
        <p>
          Valentin told ABC News that she became suspicious after the nursing
          home told her that her grandmother's injuries occurred when she banged
          her hands on the bed railing. Valentin says the hidden camera recorded
          600 hours of footage, some of which shocked her.
        </p>
        <p>
          She said she saw the nursing aid grab her grandmother's arm, twist it
          back, lift her off the bed and slam her into the bed, the report
          states. After watching the videos, Valentin said she transferred her
          grandmother to an emergency room and later to a different nursing
          home, the article said.
        </p>
        <p>
          Valentin did the right thing by paying attention to her instincts that
          something was terribly wrong, said John Bisnar, founder of the Bisnar
          Chase personal injury law firm, which represents victims of{" "}
          <Link to="/nursing-home-abuse">nursing home abuse and neglect.</Link>
        </p>
        <p>
          "When family members see slap marks, unexplained bruises, pressure
          marks, burns or blisters on their loved one's body, that should
          certainly raise red flags. In this case, Valentin did the right thing
          by first complaining to administrators. When she did not get a
          satisfactory answer, she took the matter into her own hands."
        </p>
        <p>
          "Symptoms of abuse can show up in many forms including depression and
          weight loss. If a nursing home resident is not being properly cared
          for they may withdraw and become severely depressed. It may not always
          be physical injuries in a case of abuse and families need to be very
          aware of any signs a loved one may be displaying."
        </p>
        <p>
          Nursing home abuse is an unfortunate reality in our society today,
          Bisnar said. "That does not mean it is acceptable. Nursing homes have
          a duty to protect their residents and keep them safe. Injured victims
          and their families have legal rights. Victims can file a civil
          personal injury claim in such cases seeking compensation for injuries
          and damages. The civil justice system offers a way for victims of
          nursing home abuse to get justice and fair compensation for their
          losses."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>
        <p>
          Source: <br />
          http://abclocal.go.com/wabc/story?section=news/local/new_york&id=8995262
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
