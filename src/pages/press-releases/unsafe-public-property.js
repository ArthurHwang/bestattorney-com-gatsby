// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Attorneys Files Lawsuit Against City of San Clemente, California for Dangerous Condition of Public Property"
        pageDescription="Are you a seriously injured victim? Call 949-203-3814 for San Clemente personal injury attorneys who can help. Free consultations. Trust, passion, results!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Files Lawsuit Against City of
          San Clemente, California
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The California personal injury attorneys of Bisnar Chase Personal
          Injury Attorneys have filed a lawsuit against the City of San
          Clemente, California for dangerous condition of public property. The
          suit stems from a March 2010 incident in which Miguel Gama Macias, a
          resident of Santa Ana, California, was severely injured after he
          struck a raised curb with his motorcycle on the Southbound I-5 Freeway
          near the El Camino Real exit in San Clemente. The suit alleges Macias
          struck the dangerous curb, which was unmarked to motorists and too
          steep to navigate across safely, after he attempted to make a legal
          stop on the right shoulder of the freeway. The lawsuit, which also
          names the State of California and the County of Orange as defendants
          in this case, seeks damages for pain and suffering, disfigurement and
          disability, emotional and mental distress, medical expenses, loss of
          past and future earnings and court costs.
        </p>
        <p>
          Newport Beach, California{" "}
          <Link to="http://www.prweb.com/releases/personal-injury/orange-county/prweb8027253.htm">
            (Vocus/PRWEB)
          </Link>{" "}
          December 15, 2010
        </p>
        <p>
          The Orange County personal injury attorneys of Bisnar Chase Personal
          Injury Attorneys (/) have filed a lawsuit against the City of San
          Clemente, California for dangerous condition of public property. The
          suit stems from a March 2010 incident in which Miguel Gama Macias, a
          resident of Santa Ana, California, was severely injured after he
          struck a raised curb with his motorcycle on the Southbound I-5 Freeway
          near the El Camino Real exit in San Clemente. The suit alleges Macias
          struck the dangerous curb, which was unmarked to motorists and too
          steep to navigate across safely, after he attempted to make a legal
          stop on the right shoulder of the freeway. The lawsuit, which also
          names the State of California and the County of Orange as defendants
          in this case, seeks damages for pain and suffering, disfigurement and
          disability, emotional and mental distress, medical expenses, loss of
          past and future earnings and court costs. The case is pending in the
          California Superior Court of Orange County, case # 00424537.
        </p>
        <h2>
          Alleged Dangerous California Roadway Severely Injures Motorcyclist
        </h2>
        <p>
          According to court documents, Miguel Gama Macias was riding his
          motorcycle March 25, 2010 at approximately 11:00 PM on the I-5 Freeway
          South, near the El Camino Real exit in San Clemente, California, when
          he attempted to make a legal stop on the right hand shoulder of the
          road.
        </p>
        <p>
          As he attempted to safely pull over, Miguel's motorcycle collided with
          an unexpected, unobservable and unmarked raised curb adjacent to an
          asphalt island located between the lanes reserved for through traffic
          and the paved shoulder of the road. Miguel sustained severe, permanent
          and debilitating injuries as a result of the collision.
        </p>
        <h2>
          San Clemente, County of Orange, State of California Named as
          Defendants
        </h2>
        <p>
          The lawsuit alleges the City of San Clemente, the County of Orange and
          the State of California bear responsibility in this case due to
          hazardous roadway conditions that caused Miguel to sustain severe
          personal injuries.
        </p>
        <p>
          Those hazards include a curb that was unmarked and unobservable by the
          motoring public and a curb that was too steep for drivers to safely
          navigate across it. What's more, the suit alleges that in spite of
          these dangers, there were no signs that alerted motorists to the
          hazards, nor did the city, county or state remedy the problem in any
          way to prevent crashes from happening in that vicinity.
        </p>
        <p>
          "Motorists have a legal right to safe roadway conditions when they
          travel," said Brian Chase, senior partner of Bisnar Chase Personal
          Injury Attorneys. "And local municipalities and state governments bear
          a legal responsibility when hazardous conditions exist that compromise
          safe travel. In this case, Miguel suffered severe personal injuries
          that will forever debilitate him due to the negligence demonstrated by
          the city, county and state."
        </p>
        <p>
          The lawsuit seeks compensatory damages for past and future pain and
          suffering, disfigurement, disability, emotional and mental stress and
          worry, medical expenses, loss of earning ability and other
          rehabilitative, palliative and corrective care for the personal
          injuries Miguel sustained. The case is pending in the Orange County
          Superior Court, Judge Andrew P. Banks presiding, case #00424537.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people who have
          been very seriously injured or lost a family member due to an
          accident, defective product or negligence throughout California. The
          law firm has won a wide variety of challenging cases against
          governmental agencies, including school districts, Caltrans, cities,
          the State of California and the U.S. Federal Government. Get a
          complimentary copy of The Seven Fatal Mistakes That Can Wreck Your
          California Personal Injury Claim. For more information, please visit
          http://www.CaliforniaInjuryBlog.com.
        </p>
        <p>
          {" "}
          <Link to="/motorcycle-accidents/press-releases">
            Click Here For More Motorcycle Accident Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
