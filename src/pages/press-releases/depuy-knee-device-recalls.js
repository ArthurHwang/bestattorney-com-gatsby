// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="DePuy Recalls Defective Knee Replacement Device"
        pageDescription="Johnson & Johnson subsidiary, DePuy Orthopaedics, has recalled a defective knee replacement device because of its potential to cause fractures and other complications among the patients who receive them."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>DePuy Recalls Defective Knee Replacement</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10467318.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          Johnson & Johnson subsidiary, DePuy Orthopaedics, has recalled a knee
          replacement device because of its potential to cause fractures and
          other complications among the patients who receive them.
        </p>
        <p>
          According to a Feb. 22 news report, the{" "}
          <Link to="/medical-devices/knee-replacement-trial">
            defective knee replacement
          </Link>{" "}
          can lead to loss of function, loss of limb, infection, compromised
          soft tissue or even death. The company has warned doctors and
          hospitals that the devices should not be used and any unused stock
          should be returned immediately, the report states.{" "}
          <img
            src="/images/defective-knee-replacement.jpg"
            width="204"
            height="158"
            alt="hip replacements"
            className="imgright-fixed"
          />
        </p>
        <p>
          The device, which is called the LPS Diaphyseal Sleeve, is used to
          create a better fit and feel for patients receiving knee replacement
          with the company's LPS System, the article states. The U.S. Food and
          Drug Administration (FDA) has classified the Depuy recall as Class
          One, which is the most serious type of designation, usually given to
          products that could cause serious complications or death, the report
          states.
        </p>
        <p>
          So far, the FDA has received 10 reports of fractures or loosening of
          the product, but no deaths, according to the article.
        </p>
        <p>
          The report states that the announcement of this recall comes at a
          sensitive time for Johnson & Johnson when the U.S. Department of
          Justice announced that it is investigating the company's marketing of
          its hip replacements that were recalled in 2010. The company is facing
          more than 10,000 product liability lawsuits in connection with those
          recalled products, the ASR XL Hop, the article states.
        </p>
        <p>
          "Medical device manufacturers have a responsibility not only to make
          products that are safe for consumers, but also issue timely recalls on
          products that have given rise to problems or health complications
          among those who have used them", said John Bisnar, founder of the
          Bisnar Chase personal injury law firm. "These are products that are
          placed inside people's bodies. There needs to be extensive research
          and testing before these devices are distributed or even approved."
        </p>
        <p>
          "The potential complications listed here are extremely severe and
          worrisome", Bisnar said. "The list of side effects in this case
          includes loss of mobility, loss of limb, infection and even death.
          This product has the potential to have serious, adverse, life changing
          effects on the people who use them. While I'm relieved that J & J has
          taken the steps to recall these dangerous products, it remains to be
          seen how many patients have actually been affected by them and to what
          degree."
        </p>
        <p>
          Bisnar Chase encourages users of the Depuy product to learn as much as
          they can about the device and to discuss at length with their doctor;
          the safest method in dealing with treatment options.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>
          Source: <br />
          http://harrismartin.com/article/15776/depuy-recalls-orthopedic-knee-sleeve-for-potential-device-fractures/{" "}
          <br />
          http://www.startribune.com/business/192523851.html?refer=y
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
