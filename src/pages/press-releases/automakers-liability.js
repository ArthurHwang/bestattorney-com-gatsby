// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Car Accident Attorneys - What Automakers Don't Want You To Know"
        pageDescription="What Automakers Don't Want You To Know - Information about auto defects. Were you injured? Call 949-203-3814 for CA car accident attorneys who care. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>What Automakers Don't Want You to Know</h1>
        <BreadCrumbs location={location} />

        <h2>
          Bisnar Chase Personal Injury Attorneys Reveals Driving Dangers of
          Deadly Auto Defects
        </h2>
        <p>
          <strong>Newport Beach, CA, May 1, 2012</strong> -- Bisnar Chase
          Personal Injury Attorneys (/) of Orange County, California who
          specialize in national catastrophic injury and wrongful death cases
          involving defective cars, today revealed information about deadly auto
          defects they say automakers don't want the driving public to know.
        </p>
        <ol>
          <li>
            <strong>Defective seat backs.</strong>
            <br />
            Surprisingly, the firm says that when it comes to strength and
            structural integrity, a seat as flimsy as a lawn chair can pass
            today's minimum safety requirements. During a crash, defective car
            seats come unattached from the floor, or defective seat backs flip
            into the rear compartment, causing restraint systems to fail and
            occupants to be tossed around the interior of the vehicle. The firm
            says many of its clients have been injured as a result of defective
            car seats, including Jaklin Romine who was awarded $24.7 million
            last year after being rendered a quadriplegic when her defective
            seat back collapsed causing her head to strike the rear compartment.
          </li>
          <li>
            <strong>Insufficient roof strength.</strong>
            <br />
            Even today, many vehicles lack sufficient roof strength, causing
            occupants to sustain head and spinal cord injuries and blunt force
            trauma during rollover crashes. A{" "}
            <Link to="/auto-defects/roof-crush/new-car-roof-standards">
              crushed car roof
            </Link>{" "}
            can also compromise the functionality of seatbelts and windows which
            can lead to passenger ejection resulting in catastrophic injuries
            and death. Gloria Levesque was rendered an incomplete quadriplegic
            in a Ford Expedition rollover accident in 2003 after the SUV's
            substantially weak roof collapsed and crushed inward, causing her to
            sustain severe head and spinal cord injuries.
          </li>
          <li>
            <strong>Defective seat belts.</strong>
            <br />
            Surprisingly, a large number of auto defects cases represented by
            Bisnar Chase Personal Injury Attorneys involve seat belt
            malfunctions of one kind or another despite restraint system
            advancements through the years. Seat belt defects include those that
            are easily unlatched by flying objects or body parts during a crash,
            others that are made with poorly designed webbing that loosens or
            tears in crashes, and still others that come detached from their
            anchor points and mounts. In the case of Joshua Newman, his
            defective seat belt gave way after his car crashed into a light pole
            in 2003. Joshua suffered a traumatic brain injury, chest trauma and
            heart failure. His passenger, whose seat belt did not fail, received
            minor injuries.
          </li>
          <li>
            <strong>Defective windows.</strong>
            <br />
            In addition to keeping outside objects from entering a vehicle,
            windows serve an important purpose of keeping vehicle occupants
            inside. What the firm has learned is that in certain vehicles,
            automakers have cut costs by using weak metal frameworks around
            windows – usually located around side and rear windows – and cheaper
            tempered glass that shatters more easily than laminated glass. In
            2006, Michael Samardzich was permanently blinded after shards of
            tempered glass pierced and ruptured his left eye in a crash
            involving a GMC Yukon in which he was a passenger. And in 2007,
            Carmen Todd was killed in a GMC Yukon Denali rollover accident. The
            wrongful death case cited a malfunctioning and unsafe window design,
            among other defects, that allowed Carmen to be partially ejected
            from the vehicle.
          </li>
          <li>
            <strong>Insufficient lateral and roll stability.</strong>
            <br />
            The firm says historical design flaws in higher profile vehicles
            like Sport Utility Vehicles (SUVs) and trucks, coupled with the lack
            of mandatory Electronic Stability Control (ESC), have made certain
            vehicles more susceptible to loss of steering control, loss of
            traction control and rollovers. While all 2012 model year vehicles
            are required to have ESC in the U.S., not all cars manufactured
            before 2012 were equipped with ESC – technology that was first
            introduced in 1987. <br />
            “Rollover crashes due to insufficient lateral and roll stability are
            common,” said <Link to="/attorneys/brian-chase">Brian Chase</Link>,
            auto defects expert and partner at Bisnar Chase Personal Injury
            Attorneys. “What’s shocking is the 25-year lapse in mandating a
            technology like ESC which has been proven to prevent one-third of
            the nation’s fatal car accidents. When it comes right down to it,
            safety features are an added expense. What we’ve learned is that
            automakers simply care more about profitability than they do about
            protecting innocent motorists."
          </li>
        </ol>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys represent people who have been
          very seriously injured or lost a family member due to a{" "}
          <Link to="/">California personal injury accident</Link>, car accident,
          defective product or negligence throughout the state. The auto defects
          law firm has won a wide variety of auto defect cases against many
          major auto manufacturers, including Ford, General Motors, Toyota,
          Nissan and Chrysler. Brian Chase, auto defects expert and partner at
          Bisnar Chase is the author of the most up-to-date and comprehensive
          auto defect book available today, Still Unsafe at Any Speed: Auto
          Defects that Cause Wrongful Deaths and Catastrophic Injuries.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
