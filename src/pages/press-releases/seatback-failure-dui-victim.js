// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Obtains $2 Million Settlement for DUI Victim's Family"
        pageDescription="A dui victim's family has won a two million dollar settlement for the death of their loved one and for seat back failure of his Nissan Murano."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Obtains $2 Million Settlement for Family of DUI Victim
        </h1>
        <BreadCrumbs location={location} />
        <h2>
          Victory for the mother of a 51-year-old man killed in a collision
          caused by a drunk driver.
        </h2>
        <p>
          <img
            src="/images/brook-boynton.jpg"
            alt="Brook Boynton was killed by a drunk driver and seat back failure"
            className="imgright-fixed"
          />{" "}
          Newport Beach, Calif. Feb. 15, 2013 - On the evening of Oct. 16, 2009
          on the state Route 73 toll road about a mile north of Laguna Canyon
          Road.
        </p>
        <p>
          Brook Boynton was driving a 2004 Nissan Murano in the number two lane
          and defendant Richard Caselli was driving a 2001 Volvo S-60 in the
          number one lane.
        </p>
        <p>
          Multiple witnesses stated that he was traveling at speeds over 100
          mph. Caselli lost control on a downhill curve and the right front of
          his vehicle struck the left rear of Boynton's Nissan.
        </p>
        <p>
          During this initial impact, the driver's seat of the Nissan collapsed
          backwards (known as{" "}
          <Link to="/auto-defects/seatback-failure">seat back failure</Link>)
          and Boynton came out of his seatbelt as the vehicle tripped on the
          curb and rolled over several times. Boynton was then ejected from the
          vehicle as it rolled over and was pronounced dead at the scene.
        </p>
        <p>
          Caselli was arrested on suspicion of drunk driving and gross vehicular
          manslaughter while intoxicated (Orange County Superior Court Case
          Number: 10ZF0082) and subsequent toxicology testing by the Orange
          County Sheriff's Crime Lab put his blood alcohol concentration (BAC)
          at .186, more than twice the legal limit.
        </p>
        <p>
          He also tested positive for cocaine, according to court records.
          Caselli pleaded guilty to vehicular manslaughter while intoxicated on
          May 11, 2010 in the criminal case and served two and a half years in
          prison, court records (Case Number: 10ZF0082) indicate.
        </p>
        <p>
          The DUI victim's sole heir, his 84-year-old mother Johnella Boynton,
          approached Bisnar Chase after being told by another law firm to simply
          accept Caselli's insurance limits of $15,000 and collect the
          additional $85,000 in uninsured motorist insurance on her son's
          policy, said Scott Ritsema, trial attorney at the Bisnar Chase
          personal injury law firm.
        </p>
        <p>
          The legal team at Bisnar Chase filed a products liability case (Case
          Number 00509530) against Nissan, which eventually settled the case
          confidentially. Johnella Boynton's wrongful death lawsuit (Case
          Number: 00509530) against Caselli was set to go to trial on Feb. 25,
          when the case was settled Feb. 14 during a Mandatory Settlement
          Conference, Ritsema said.
        </p>
        <p>
          {" "}
          <Link
            to="http://www.ocregister.com/articles/boynton-248311-caselli-brook.html"
            target="_blank"
          >
            The Boynton family
          </Link>{" "}
          was determined to hold Caselli accountable for his actions, Ritsema
          said. "Although money cannot bring back a loved one, this family got
          all of the justice they could get from our civil justice system," he
          said. "Mr. Caselli has this additional 'ball and chain' strapped to
          his ankle to remind him for the rest of his life of what he did to the
          Boynton family."
        </p>
        <p>
          <img
            src="/images/brook-boyndon.jpg"
            alt="Brook Boyndon was an accomplished triathelete "
            className="imgleft-fluid"
            height="200"
            width="150"
          />
          Brook was a member of the Orange County Triathlon Club and an amazing
          runner.
        </p>
        <p>
          According to the website{" "}
          <Link
            to="http://www.triathica.com/brook-boynton-killed-by-a-drunk-driver/"
            target="_blank"
          >
            Triathica
          </Link>
          , he was such a good runner and so fit that some thought he must be
          part of a relay.
        </p>
        <p>
          <i>
            "Every time I saw him running in a triathlon I thought that he must
            be part of a relay because no one has legs that fresh after a long
            swim and bike ride".
          </i>{" "}
          -Ron Saetermoe
        </p>
        <p>
          Bisnar Chase represent victims of car accidents including{" "}
          <Link to="/dui/victims-rights">DUI victims</Link> and seat back
          failures. The firm has been featured on a number of popular media
          outlets including Newsweek, Fox, NBC, and ABC and is known for its
          passionate pursuit of results for their clients. Since 1978, Bisnar
          Chase has recovered millions of dollars for victims and their
          families. For more information, please call 949-203-3814 or visit /
          for a free consultation. We are located at 1301 Dove Street #120,
          Newport Beach, CA 92660.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
