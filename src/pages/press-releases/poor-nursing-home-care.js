// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Report Shows Medicare Paid $5.1 Billion for Poor Nursing Home Care"
        pageDescription="A Feb. 27 article by The Associated Press reported that Medicare paid millions in taxpayer dollars to nursing homes nationwide that were providing substandard care to residents. The report states that about $5.1 billion in Medicare funds went to skilled nursing facilities that failed to meet federal quality of care rules placing patients in dangerous conditions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Government Report Shows Medicare Paid $5.1 Billion for Poor Nursing
          Home Care
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/3/prweb10505024.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          Medicare paid about $5.1 billion for patients to stay in skilled
          nursing facilities that even failed to meet federal quality of care in
          2009, a Feb. 27 Associated Press news article reported. According to
          the report, in some cases, patients were placed in{" "}
          <Link to="/nursing-home-abuse">
            dangerous and neglectful conditions
          </Link>
          .
        </p>
        <img
          src="/images/nursing-home-abuse-pr.jpg"
          width="250"
          height="171"
          alt="nursing home abuse"
          className="imgright-fixed"
        />
        <p>
          One out of every three times that patients ended up in nursing homes
          in 2009, they landed in facilities that failed to follow basic care
          requirements spelled out by the Department of Health and Human
          Services, the federal agency that administers Medicare, the article
          states.
        </p>
        <p>
          Under federal law, nursing homes are required to write up care plans
          to fit the needs of each resident so that doctors, therapists and
          caregivers are on the same page when it comes to ensuring the
          patient's well-being, the report says. AP reports that many nursing
          home residents are going without the critical help they need and that
          the government is paying billions of taxpayer dollars to these skilled
          nursing facilities.
        </p>
        <p>
          Federal investigators estimate that in out of five stays, patients'
          health problems were not even addressed in care plans, which falls way
          short of government directives in that regard, the report states. In
          other cases residents got therapy they did not need, which the report
          said, was lucrative for nursing homes because they would be reimbursed
          at a higher rate by Medicare.
        </p>
        <p>
          "This report is shocking at many levels", said John Bisnar, founder of
          the Bisnar Chase personal injury law firm. "It is appalling that a
          number of nursing homes do not even provide their residents with basic
          care plans, which is a violation of federal law. These types of
          violations can put nursing home patients at risk of suffering serious
          injuries or even death."
        </p>
        <p>
          "What is even more outrageous is the fact these{" "}
          <Link to="/nursing-home-abuse/injury-victims">nursing homes</Link> are
          taking billions of taxpayer dollars", Bisnar says. "Some of these care
          facilities apparently gave residents physical and occupational therapy
          they did not need just to pocket the Medicare dollars. But the
          facilities also failed to provide basic care to residents such as
          formulating a care plan that would ensure their physical, mental and
          psychological well-being."
        </p>
        <p>
          Bisnar says nursing homes that receive taxpayer dollars should have
          accountability. "The funding for these facilities must be tied to the
          nursing homes' ability to meet basic requirements with regard to
          patient care. The agency should also strengthen enforcement of its
          regulations and oversight."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>
          Source: <br />
          http://bigstory.ap.org/article/medicare-paid-51b-poor-nursing-home-care-0
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
