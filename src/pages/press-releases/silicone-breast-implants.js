// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="FDA Safety Updates about Silicone Breast Implants Put Women at Risk of Health Complications"
        pageDescription="Call 949-203-3814 for Highest-rated Silicone Breast Implant Attorneys.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          FDA Safety Updates about Silicone Breast Implants Put Women at Risk of
          Health Complications
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>June 30, 2011</strong> - The California product defects
          attorneys of Bisnar Chase Personal Injury Attorneys
          (bestatto-gatsby.netlify.app) say they believe the recent FDA report
          offering updated safety information about silicone breast implants
          puts women at risk of health complications due to questionable data
          upon which the report is based, as well as the confusing nature of the
          information contained within.
        </p>
        <p>
          The FDA posted a summary regarding updated safety information for
          Silicone Gel-Filled Breast Implants1 at its website last week, stating
          that when the agency allowed these types of breast implants back on
          the market in November 2006, it required the manufacturers of the
          implants to "conduct follow-up studies to learn more about the
          long-term performance and safety of the devices."
        </p>
        <p>
          According to the report, the updated FDA information is based on
          analyses of post-approval studies conducted by Allergan and Mentor -
          manufacturers of silicone gel-filled breast implants - as well as its
          own data and information about silicone breast implants found in
          scientific publications.
        </p>
        <p>
          Brian Chase, product defects expert and partner at Bisnar Chase
          Personal Injury Attorneys, says he has serious concerns about the
          FDA's updated safety information.
        </p>
        <p>
          "I find it absurd that the FDA would include information provided by
          manufacturers of silicone breast implants in its study, and then rely
          on that information to develop its updated safety findings," said
          Chase. "This is a perfect example of the fox guarding the henhouse.
          What's more, in the report, both manufacturers have admitted to the
          FDA the difficulties in following women who have received silicone
          gel-filled breast implants."
        </p>
        <p>
          Chase, a 17-year product defects attorney who has represented clients
          due to medical device defects, goes on to say he has real problems
          with some of the conclusions the report draws one, in particular, that
          states "preliminary data do not indicate that silicone gel-filled
          breast implants cause breast cancer, reproductive problems or
          connective tissue disease, such as rheumatoid arthritis."
        </p>
        <p>
          Chase says that immediately following this verbiage, the report states
          that in order to rule out these and other rare complications, studies
          would need to enroll larger groups of women and for longer periods of
          time than the ones that have been conducted to this point, and that
          the FDA is holding an expert advisory panel in the next few months to
          discuss how post-approval studies on breast implants can be more
          effective.
        </p>
        <p>
          "In my opinion, it's negligent and reckless for the FDA to publish
          what appear to be premature findings before adequate numbers of women
          have been analyzed for appropriate lengths of time, especially when
          independent studies have shown there could be a connection between
          silicone gel-filled implants and ongoing immunogenicity of silicon2."
        </p>
        <p>
          Finally, Chase says that overall, the FDA report is contradictory,
          confirming that silicone gel-filled breast implants are safe and
          effective when used as intended, but then stating that they are also
          associated with significant local complications and outcomes,
          including capsular contracture, reoperation, removal, and implant
          rupture, and also with some women experiencing breast pain, wrinkling,
          asymmetry, scarring and infection.
        </p>
        <p>
          "With associated complications such as these, I find it unbelievable
          the FDA would claim silicone gel-filled breast implants are safe and
          effective," said Chase.
        </p>
        <p>
          1 FDA, Silicone Gel-Filled Breast Implants: Updated Safety
          Information, June 22, 2011,
          http://www.fda.gov/ForConsumers/ConsumerUpdates/ucm259825.htm
        </p>
        <p>
          2 Admin, Study Shows Proof Systemic Inflammatory Reaction to Silicone
          Breast Implants, June 10, 2011,
          http://nutritiondietnews.com/study-shows-proof-systemic-inflammatory-reaction-to-silicone-breast-implants/854249/
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys{" "}
          <Link to="/defective-products">
            California Product Defects Attorneys
          </Link>{" "}
          represent people who have been very seriously injured or lost a family
          member due to an accident, a defective product such as a medical
          device, or negligence throughout California and the U.S. The law firm
          has won a wide variety of challenging serious injury defective product
          cases against a number of Fortune 500 companies. For more information,
          please visit bestatto-gatsby.netlify.app.
        </p>
        <p>
          {" "}
          <Link to="/press-releases/miscellaneous">
            Click Here For More Miscellaneous Press Releases
          </Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
