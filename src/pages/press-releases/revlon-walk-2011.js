// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Staff Members Give Back With Community Outreach"
        pageDescription="Bisnar Chase Personal Injury Attorneys supports local entertainment community through 2011 Revlon Walk."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          {" "}
          <Link to="/press-releases/revlon-walk-2011">
            Bisnar Chase Personal Injury Attorneys Staff Members Support the
            Entertainment Industry Foundation and Revlon Run/Walk
          </Link>
        </h1>
        <BreadCrumbs location={location} />
        <p>
          On Saturday, May 7th, Marta De La Torre along with her family members
          and Bisnar Chase Personal Injury Attorneys staff members/ friends,
          Chris Schlindwein, Katie McConnell and Courtney Ridenour will walk
          over 3 miles in a combined effort to help eliminate women's cancers.
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/5/prweb8373722.htm">
            PRWEB
          </Link>
          ) May 03, 2011
        </p>
        <h2>Marta De La Torre Focuses on Community Outreach</h2>
        <p>
          Each year Marta and her family plan their annual calendar and
          community outreach is always first on the schedule. Together they have
          participated in many fundraising walks and this year, they decided the
          EIF Revlon Walk is the perfect event to support for Mother's Day
          weekend. Marta is passionate about this cause and says, "I want to be
          a part of something bigger. It's so little to give your time to
          contribute to a larger cause. I believe by walking we are all
          contributing a token of help and if we each contribute a token we can
          make a tremendous difference in the world."
        </p>
        <p>
          "Chicas Contra el Cancer" is the official name of the Bisnar Chase
          Personal Injury Attorneys staff's team of mothers, daughters and
          friends. Together they will help in the fight against women's cancer
          and spend their Mother's Day weekend supporting other women who have
          been diagnosed with cancer.
        </p>
        <h2>About the Entertainment Industry Foundation and Revlon Run/Walk</h2>
        <p>
          Created in 1994 through the committed and collective efforts of the
          Entertainment Industry Foundation, Lilly Tartikoff and Ronald O.
          Perelman, the EIF REVLON Run/Walk for Women has grown to become one of
          the nation's largest 5K fundraising events. To date, the Run/Walks (in
          Los Angeles and New York) have distributed over 60 million dollars for
          cancer research, treatment, counseling and outreach programs. Thanks
          in part to these funds, new treatments are being developed and lives
          are being saved.
        </p>
        <p>
          For more information on the Entertainment Industry Foundation/Revlon
          Walk/Run or to support Marta's and the Bisnar Chase Personal Injury
          Attorneys Staff Team, "Chicas Contra el Cancer" visit{" "}
          <Link to="http://do.eifoundation.org/site/PageServer">
            http://www.revlonrunwalk.org
          </Link>
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Since 1978, top-rated Bisnar Chase Personal Injury Attorneys have won
          hundreds of millions of dollars for over 12,000 clients who have been
          very seriously injured or lost a family member due to an accident,
          defective product or negligence throughout California. The law firm
          has won a wide variety of challenging personal injury and defective
          product cases against some of the largest insurance companies,
          corporations and governmental agencies. For more information, be sure
          to visit / to get your free copy of{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim."
          </Link>
        </p>
        <p>
          {" "}
          <Link to="/press-releases/community-outreach">
            Click Here For More Community Outreach Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
