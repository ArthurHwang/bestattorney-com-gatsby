// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brian Chase Personal Injury Expert Appears on the 'Larry on the Law' Show"
        pageDescription="Were you severely injured in a car accident? Call 949-203-3814 for California personal injury attorneys who can help. Free consultations. Great client care."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Brian Chase Official Personal Injury Expert of Larry Elder's New
          "Larry on the Law" Show
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Brian Chase, 17-year law veteran and partner at the California
            personal injury attorneys of Bisnar Chase Personal Injury Attorneys
            (bestatto-gatsby.netlify.app), has been invited by Larry Elder,
            best-selling author and KABC radio talk show host, to serve as the
            official and exclusive on-air personal injury expert of the new
            "Larry on the Law Show" (LarryOnTheLaw.com) airing Saturdays from
            3:00 p.m. to 5:00 p.m. (PT) on 790 KABC FM Los Angeles beginning
            January 29.
          </strong>
        </p>
        <p>
          Los Angeles, California{" "}
          <Link to="http://www.prweb.com/releases/larry-on-the-law/personal-injury-lawyer/prweb8084800.htm">
            (PRWEB)
          </Link>{" "}
          January 24, 2011
        </p>
        <img
          src="/images/larryelderbrianjohn.jpg"
          alt="Larry Elder"
          className="imgright-fixed"
        />
        <p>
          Brian Chase, 17-year law veteran and partner at the California
          personal injury attorneys of Bisnar Chase Personal Injury Attorneys
          (/), has been invited by Larry Elder, best-selling author and KABC
          radio talk show host, to serve as the official and exclusive on-air
          personal injury expert of the new "Larry on the Law Show"
          (http://www.LarryOnTheLaw.com) airing Saturdays from 3:00 p.m. to 5:00
          p.m. (PT) on 790 KABC FM Los Angeles beginning January 29.
        </p>
        <p>
          The show, hosted by "The Sage from South Central" Larry Elder -- an
          attorney, former host of the nationally syndicated "The Larry Elder
          Show" and current nationally-syndicated host of KABC radio's daily
          morning talk show in Los Angeles -- is designed to answer legal
          questions and to help alleviate the fear and confusion many people
          face when dealing with legal issues.
        </p>
        <p>
          Chase, who has litigated dozens of jury trials, has recovered hundreds
          of millions of dollars for his clients, and has the highest rating
          possible for ethical standards and professional abilities from
          Martindale-Hubbell, was asked to serve as the show's official and
          exclusive personal injury expert to field personal injury and product
          liability questions and to discuss some of his firm's most
          high-profile cases with "Larry on the Law" listeners.
        </p>
        <p>
          Brian Chase makes his radio talk show debut on January 29, 2011, from
          3:00 p.m. to 5:00 p.m. (PT) and will be featured monthly during the
          personal injury segment of "Larry on the Law."
        </p>
        <p>
          A graduate of Pepperdine University School of Law, Chase was named
          Trial Lawyer of the Year in the area of Defective Products in 2004 and
          was listed as one of the Top 100 Trial Lawyers by the American Trial
          Lawyers Association in 2007. In 2009, he was named a Top Lawyer in
          Orange County by OC Metro magazine. In 2010, he was named a Top 50
          Orange County Super Lawyer by Orange Coast Magazine for the fourth
          consecutive year.
        </p>
        <p>
          Chase was lead attorney on three important, precedent-setting
          appellate opinions: Schreiber v. Estate of Kiser (a California Supreme
          Court case dealing with expert witness designations); Hernandez v.
          State of California (an appellate case from the Second Appellate
          District dealing with governmental design immunity); and Mazon v. Ford
          Motor Company (an auto products case dealing with Forum Non
          Conveniens).
        </p>
        <p>
          Currently, Chase serves as a Vice President of the Consumer Attorneys
          of California and is past President of the Orange County Trial Lawyers
          Association. He is a frequent lecturer on litigation-related topics in
          continuing education programs for trial lawyers.
        </p>
        <p>
          He has published numerous articles and has made a number of radio and
          television appearances, including interviews on CBS Special Reports,
          Peter Jennings/World News Tonight, and America's Best Lawyers, in
          addition to FOX 11 News Los Angeles reports discussing vehicle black
          boxes and defective seat car seats.
        </p>
        <p>
          His book, "Still Unsafe at Any Speed: Auto Defects That Cause Wrongful
          Deaths and Catastrophic Injuries," is the most comprehensive literary
          work on auto defects since Ralph Nader's 1965 book of the same
          subject, "Unsafe at Any Speed."
        </p>
        <p>
          The son of a janitor, Elder was born and raised in South Central Los
          Angeles. He attended Brown University, receiving a BA in Political
          Science in 1974. He then attended University of Michigan, School of
          Law, graduating in 1977.
        </p>
        <p>
          He was host of the television show, "Moral Court," distributed by
          Warner Brothers Television. In the fall of 2004, Warner Brothers
          returned Elder to daily television with "The Larry Elder Show," an
          hour-long, multitopic show that brought common sense, sound advice and
          realistic, workable solutions.
        </p>
        <p>
          In his best-selling book, "The 10 Things You Can't Say in America,"
          Elder skewers the crippling myths that dominate the public agenda. His
          latest book, "What's Race Got to Do with It? Why it's Time to Stop the
          Stupidest Argument in America," is being praised as an important,
          groundbreaking must-read for the future of race relations in America.
          Elder also writes a nationally syndicated newspaper column distributed
          through Creators Syndicate.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people who have
          been very seriously injured or lost a family member due to an
          accident, defective product or negligence throughout California. The
          law firm has won a wide variety of high profile, challenging cases
          against some of the nation's largest companies and governmental
          agencies. There is a reason their website address is
          bestatto-gatsby.netlify.app.
        </p>
        <h2>About Larry on the Law</h2>
        <p>
          Hosted by nationally-syndicated radio talk show host, best-selling
          author and attorney, Larry Elder, "Larry on the Law" is a stimulating
          weekly radio show designed to answer legal questions and conundrums
          and to help take away some of the fear and confusion people face when
          dealing with legal issues. Experts from a variety of legal branches,
          including personal injury, family law, tax/IRS and general law, field
          questions from listeners, impart legal advice and discuss cases that
          are informative, timely and interesting. One way or another, many of
          the issues facing our country today find themselves before the courts.
          If you have a legal problem or if something about the legal system is
          bugging you, "Larry on the Law," -- broadcast Saturdays on 790 KABC FM
          Los Angeles -- is your judge and jury authority. Tune in!
        </p>

        <p>
          {" "}
          <Link to="/press-releases/firm-accomplishments">
            Click Here For More Accomplishment Press Releases
          </Link>
        </p>
        <p>
          {" "}
          <Link to="/contact">Click Here</Link> for a list of local offices.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
