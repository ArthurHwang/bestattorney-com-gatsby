// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Beverly Hills Hit And Run Road Rage"
        pageDescription="Police are looking for a suspect involved in a road rage incident that left a bicyclist injured.The driver allgedly rammed the bicyclist with his vehical intentionally, then fled the scene."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Beverly Hills Hit And Run</h1>
        <BreadCrumbs location={location} />

        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/4/prweb10668955.htm">
              press release
            </Link>
          </i>
        </p>

        <p>
          Police in Beverly Hills are looking for a suspect in a road rage
          incident that has left a bicyclist injured. According to an April 23
          media release posted by the Beverly Hills Police Department, the April
          3 incident was captured on surveillance video that shows a while BMW
          striking the biker head-on in an alley.
        </p>

        <p>
          The police department's statement says that the driver intentionally
          rammed the bicyclist with the vehicle, pinning him to a metal rolling
          trash bin between Wetherly and Almont drives.
        </p>

        <p>
          The bicyclist admitted that the incident was sparked by a fight that
          he had earlier with the driver of the BMW, according to the statement.
          The bicyclist also said he punched the driver in the face to which the
          driver responded by threatening to kill him, officials say.
        </p>

        <p>
          Police describe the{" "}
          <Link to="/pedestrian-accidents/hit-and-run">
            Beverly Hills hit-and-run
          </Link>{" "}
          suspect as a white or Middle Eastern male in his mid-30s with dark
          hair, dark eyes and a thin build while the vehicle is a BMW 328i,
          possibly 2008 model or later, according to the police department's
          news release.
        </p>

        <p>
          The Newport Beach personal injury law firm of Bisnar Chase is calling
          on the community to join hands with law enforcement and help apprehend
          this hit-and-run driver. The firm is announcing a $1,000 reward for
          information leading to the arrest and felony conviction of the
          hit-and-run driver. These anonymous tips may be provided through the
          WeTip crime hotline, 800-6-Hit-N-Run (800-644-8678).
        </p>

        <p>
          The goal of the Hit-and-Run Reward Program is to help law enforcement
          apprehend hit-and-run drivers, said John Bisnar, founder of the Bisnar
          Chase personal injury law firm and the Hit-and-Run Reward Program.
          "Our goal with this program is to help eradicate hit-and-run
          incidents, which have risen to epidemic proportions in California,
          particularly in Los Angeles and Orange counties."
        </p>

        <p>
          There is absolutely no excuse for leaving the scene of an injury
          crash, Bisnar said. "The law requires motorists to stop immediately,
          wait for authorities to arrive, exchange information with the other
          parties involved and most importantly, render aid to the injured
          victim. In this case, it is indeed fortunate that the bicyclist was
          not killed."
        </p>

        <p>About Bisnar Chase</p>

        <p>
          The Newport Beach personal injury attorneys at Bisnar Chase represent
          those who have been seriously injured in traffic accidents, including
          hit-and-run collisions. The law firm has obtained nearly $500 million
          in settlements and verdicts for their clients by winning a wide
          variety of challenging personal injury cases.
        </p>

        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        <p>
          Sources: https://local.nixle.com/alert/4991716/,
          https://www.facebook.com/beverlyhillspd
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
