// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Personal Injury Lawyers of the Law Firm of Bisnar Chase Release New Book"
        pageDescription="personal injury law books written by Bisnar Chase offer easy to underatand help after a car accident or serious injury including compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Personal Injury Lawyers John Bisnar and Brian Chase Release
          New Book
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link
              to="http://www.prweb.com/releases/2013/11/prweb11288031.htm"
              target="blank"
            >
              press release
            </Link>
          </i>
        </p>
        <img
          src="/images/personal_injury_law_book.jpg"
          alt="personal_injury_law_book"
          className="imgright-fixed"
        />

        <p>
          The <Link to="/">California personal injury lawyers</Link> of the
          Bisnar Chase law firm have released a new reference guide for accident
          victims with significant input from 13 top injury attorneys
          nationwide. The book has been divided into seven chapters, each
          dealing with important aspects of a personal injury case.
        </p>

        <p>
          The first chapter talks about the part insurance companies play in
          personal injury cases and how victims and their families can deal
          effectively with them. The second chapter aims to help victims
          understand the types of damages to which they are entitled. The
          crucial third chapter advises injury victims about how they can choose
          a lawyer who will best represent their interests. The other chapters
          walk the readers through the process of filing the claim and getting
          it resolved either through a settlement or jury trial.
        </p>

        <p>
          "We want this book to be a simple, accessible guidebook for people
          dealing with a serious injury, written in plain English," says John
          Bisnar, senior partner at the Bisnar Chase personal injury law firm.
          "After reading this reference guide, we hope victims and their
          families come away with a better idea of what to expect in the entire
          aftermath of the accident from follow-ups with your doctors to the day
          they cash their settlement checks.
        </p>

        <p>
          Another important reason this book was written was to help protect
          accident victims from the financial threat posed by insurance
          companies. "Readers will get the strong message from this book that
          insurance companies are not on their side and that insurance adjusters
          are not their friends. Like all businesses, insurance companies are in
          business to make money. When you pay your premiums, they make money.
          And when you make your claim, they lose money."
        </p>

        <p>
          This{" "}
          <Link to="/resources/book-order-form">
            California personal injury law book
          </Link>{" "}
          also aims to answer the questions individuals have about issues such
          as fault, insurance claims and the risks and benefits of filing a
          lawsuit, Bisnar says. "This book is an attempt to answer the most
          common questions we have been asked by clients in more than three
          decades of our experience as personal injury attorneys."
        </p>

        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
