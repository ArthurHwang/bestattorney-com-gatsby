// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Toddler Mauled To Death In Dog Attack"
        pageDescription="A Texas toddler was mauled to death in a dog attack after the child stepped into his neighbor's yard to retrieve a balloon that had flown away. The owner of the dog allegedly did nothing."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Toddler Mauled To Death In Dog Attack; Owner Does nothing</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10457263.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          Isaiah Ray Aguilar, a 2-year-old boy, was fatally attacked by a
          neighbor's pit bull when he chased a balloon into his neighbor's front
          yard. According to a Feb. 19 Fox News report, the boy was playing with
          his brother in the front yard of his home when the balloon blew into
          the neighbor's yard and into the path of a chained pit bull.
        </p>
        <p>
          The dog attacked the boy who screamed out for help as his relatives
          and the dog owner looked on, the report states. While Isaiah's family
          ran toward him and another neighbor administered CPR, a Kens5 report
          quotes family members as saying that the dog owner shook his head and
          went indoors without taking any action.
        </p>
        <center>
          <iframe
            width="420"
            height="315"
            src="https://www.youtube.com/embed/_LQ8M7WPjik?list=UURMtvG2jyspSYrDgoXo7EVA"
            frameborder="0"
            allowfullscreen
            title="youtube"
          ></iframe>
        </center>
        <p>
          According to Fox News, the boy died from his injuries and the dog
          owner is not facing any charges. The incident has also outraged the
          community and raised questions about whether anti-tethering laws go
          far enough to protect the town's children. City officials told Fox
          News that the dog was tied up properly and the dog owner seemed to
          comply with all the city rules.
        </p>
        <p>
          This case has divided the town with many demanding that the dog owner
          be brought to justice and others saying that the dog owner should not
          be blamed, according to the report. Officials have organized a town
          hall meeting to discuss the incident and the city's pet laws, the
          article states.
        </p>
        <p>
          "This was a brutal{" "}
          <Link to="/newport-beach/dog-bites">dog attack</Link> that should
          cause outrage in the community and draw the attention of city
          officials", said John Bisnar, founder of the Bisnar Chase personal
          injury law firm.
        </p>
        <p>
          "It is time for the city to look into whether their laws and
          ordinances protect all of their residents. Is it sufficient for dog
          owners to tether their dogs or should dogs, especially ones that have
          propensity to attack, be secured in an enclosure so they don't pose a
          danger to the public? There are important questions to be answered
          here."
        </p>
        <p>
          "There is no question that dog owners are responsible for their pets
          and the actions of their pets", Bisnar said. "Being a responsible dog
          owner means making sure that your pet is properly trained so it
          doesn't present a danger to the community. Dogs must be spayed and
          neutered. Dog owners should also carry adequate liability insurance so
          injured victims are not saddled with the expenses. Responsible dog
          ownership is the key to preventing these types of heartbreaking
          tragedies."
        </p>
        <p>
          "Children are especially prone to dog bites since any sudden moves
          towards an unstable dog could trigger a bite or attack. Most dog bites
          are from fearful and unstable dogs and people need to take precautions
          to protect their children, especially if they know there is an
          aggressive dog in their neighborhood.
        </p>
        <p>
          "Dog owners, especially those who own large breeds, need to take extra
          steps to ensure that their dogs are on-leash or fenced for both the
          dog's safety as well as other people. If a dog bite's in a state such
          as California, the owner is responsible. Since larger breeds tend to
          cause more damage from a bite than smaller breeds, the owners need to
          be more diligent in proper training."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>
        <p>
          Sources: <br />
          http://latino.foxnews.com/latino/news/2013/02/19/outrage-after-2-year-old-killed-by-pitbull-in-texas/{" "}
          <br />
          http://www.kens5.com/news/Dog-owner-allegedly-stands-by-as-pit-bull-mauls-kills-toddler-191728921.html
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
