// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Car Accident Victim receives Close to $100,000.00 when At-Fault Driver only had $30,000 In Insurance"
        pageDescription="California attorneys discuss importance of UIM coverage after car accident victim received more than defendant's insurance policy. Call 949-203-3814 to obtain a free consultation for your California auto accident lawsuit."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Car Accident Victim receives Close to $100,000.00 when
          At-Fault Driver only had $30,000 In Insurance
        </h1>
        <BreadCrumbs location={location} />

        <p>
          The Orange County auto accident attorneys at Bisnar Chase have
          recently obtained $99,990.00 for a client who suffered injuries in a
          California car accident where the at-fault driver only had $30,000 in
          insurance. Linda, a Honolulu, Hawaii resident, was traveling on the 15
          Freeway in Southern California when she was struck from behind. Linda
          was then pushed into the vehicle in front of her and suffered fairly
          serious injuries.
        </p>
        <p>
          Since the at-fault driver only had a $30,000 insurance policy form
          Continental American Insurance Company (CAIC), and had caused damage
          to two different not-at-fault drivers, the{" "}
          <Link to="/car-accidents">Bisnar Chase law firm</Link> negotiated a
          split of the $30,000 between the two innocent drivers. The majority,
          $23,618.39, went to their client, Linda and the balance to the other
          not-at-fault driver.
        </p>
        <p>
          In addition to the California mandated liability coverage, Linda
          purchased Uninsured Motorist Bodily Injury coverage (known as "UIM")
          in the amount of $100,000. UIM coverage protects traffic injury
          victims from uninsured and underinsured drivers. The UIM coverage is
          additional insurance that Linda purchased for her own protection from
          other at-fault drivers that do not have insurance or do not have
          enough insurance to cover all of Linda's damages.
        </p>
        <p>
          According to information released by the Insurance Research Council
          (IRC), one in seven motorists do not have insurance. The National
          Association of Insurance Commissioners (NAIC) has found that close to
          10.8 billion dollars in costs were attributed to drivers without
          insurance in 2007.
        </p>
        <p>
          According to 2010 data from the Centers for Disease Control and
          Prevention (CDC), medical care costs and productivity losses
          attributed to motor vehicle accidents exceeded $99 billion. The U.S.
          Department of Transportation reported that more than 2,000,000 people
          suffered traffic related injuries in 2009 collisions.
        </p>
        <p>
          Due to Linda having purchased UIM coverage, the Bisnar Chase attorneys
          were able to negotiate an additional recovery for her from USAA
          insurance in the amount of $76,371.61, bringing her total recovery to
          $99,990.00.
        </p>
        <p>
          John Bisnar, managing partner of the Bisnar Chase law firm, has helped
          obtain hundreds of millions of dollars for over 12,000 clients. "Had
          Linda not purchased UIM coverage, we would not have had the
          opportunity to obtain the additional $76,000.00. It is typical for
          $100,000 of UIM insurance to cost less than $40 a year. Too many auto
          accident victims come to our doors with serious injuries caused by an
          uninsured driver or a driver with the minimum amount of coverage,
          $15,000. Linda was very wise to have purchased UIM coverage. It is
          relatively inexpensive and is the most important auto insurance to
          have to protect you and your family."
        </p>
        <p>
          "Economic times have forced many Americans to cut corners, but when it
          comes to the safety and financial security of you and your family, I
          highly recommend purchasing as much UIM coverage that your auto
          insurance carrier will allow." Says Mr. Bisnar.
        </p>
        <h2>About: Bisnar Chase, Personal Injury Lawyer Orange County</h2>
        <p>
          Bisnar Chase is a California based auto accident law firm that has
          sustained a reputation as trustworthy professionals who produce
          consistent results. The firm has been featured on several popular
          media outlets, including Fox 11, NBC, and CBC. They have produced a
          significant number of multimillion dollar verdicts and settlements for
          their clients.
        </p>
        <p>
          Bisnar Chase attorneys assist auto collision victims in California and
          provide all of their clients with a "No Win No Fee Guarantee".
        </p>
        <p>
          For a free consultation call 949-203-3814 or visit their website at /.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
