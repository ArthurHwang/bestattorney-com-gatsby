// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Injury Lawyers Win $187,800 for Motorcycle Accident Victim"
        pageDescription="Bisnar Chase California personal injury attorneys secure verdict in Los Angeles motorcycle accident case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Injury Lawyers Obtain Favorable Jury Verdict for
          Motorcycle Accident Victim
        </h1>
        <BreadCrumbs location={location} />
        <i>
          Attribution: Aug 2013 -- This article is the syndication source of a{" "}
          <Link
            to="http://www.prweb.com/releases/2013/8/prweb11049548.htm"
            target="_blank"
          >
            recently released
          </Link>{" "}
          press release from Bisnar Chase
        </i>
        <h2>
          The California personal injury lawyers of Bisnar Chase obtained an
          $187,800 verdict (Case Number: VC059513) for a motorcycle accident
          victim who suffered neck and wrist injuries that required surgery. The
          jury in this case, which was filed in Los Angeles Superior Court,
          determined that there was no comparative negligence on the part of the
          victim.
        </h2>
        <p>
          The <Link to="/">California personal injury lawyers</Link> of Bisnar
          Chase secured an $187,000 jury verdict Aug. 8 in a challenging
          motorcycle accident case (Case Number: VC059513), which was heard in
          Los Angeles County Superior Court. According to court documents (Case
          Number: VC059513), the plaintiff, Joseph Hubbard, was riding his
          motorcycle in the carpool lane on the northbound 605 Freeway on Oct.
          18, 2009.
        </p>
        <p>
          The lawsuit states that as traffic began to slow in the fast lane, the
          defendant, Alan Parrish, entered the carpool lane, causing Hubbard to
          rear-end Parrish's vehicle. Hubbard suffered a neck injury that
          required injections and physical therapy. In addition, he suffered a
          wrist injury for which he had to undergo surgery, court records state.
        </p>
        <p>
          <img
            src="/images/gavin-final.jpg"
            alt="H. Gavin Long - California Personal Injury Lawyer"
            className="imgright-fixed"
            width="160"
          />
          According to court records Parrish's attorney disputed liability in
          this case claiming that Hubbard was driving too fast and tried to pass
          the defendant, said Bisnar Chase personal injury trial lawyer Gavin H.
          Long.
        </p>

        <p>
          Parrish also disputed the nature and extent of Hubbard's injuries
          claiming only neck sprain and strain and that the wrist surgery was
          due to manual labor Hubbard performed over the three-year period
          between the date of the accident and surgery, he said.
        </p>

        <p>
          The defense offered Hubbard $25,000 although Hubbard offered to settle
          for $100,000, Long said. Jurors returned a verdict of $187,800.
        </p>
        <p>
          Long said he called three treating doctors as experts while the
          defense called an accident reconstruction specialist, billing expert,
          radiologist, hand surgeon and spine surgeon as experts, according to
          court records. The jury also determined that there was no comparative
          negligence on the part of the plaintiff, Hubbard. Jurors voted 11-1 in
          favor of Hubbard, Long said.
        </p>
        <p>
          This particular case was not without its challenges, Long says. "The
          main challenge I faced was that I was representing a motorcycle rider
          who was being partially blamed for the collision. They hired every
          expert they could think of: accident reconstructionist to attempt to
          show plaintiff was at fault, a billing expert to say the medical
          expenses were unreasonable, a radiologist to challenge the readings on
          the MRIs, a spine and hand surgeon to dispute the nature and extent of
          plaintiff&#146;s injuries."
        </p>
        <p>
          Long said he only called three doctors who treated Hubbard. "The jury
          found no comparative negligence on the plaintiff's part, which means
          that they did not find that our client had been negligent in any way.
          We will recover most of our costs from the defendant on top of
          interest on the amount of the judgment." Long says, for him, it is
          always satisfying to be the underdog in a case and come out on top.
        </p>
        <p>
          About Bisnar Chase
          <br />
          The California personal injury lawyers of Bisnar Chase represent
          victims of motorcycle accidents and other serious accidents and
          injuries. The firm has been featured on a number of popular media
          outlets including Newsweek, Fox, NBC, and ABC and is known for its
          passionate pursuit of results for injured clients. Since 1978, Bisnar
          Chase has recovered hundreds of millions of dollars for personal
          injury victims including motorcycle accident victims.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
