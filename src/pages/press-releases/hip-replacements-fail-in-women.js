// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hip Replacement Failure Affects Women More - New Study"
        pageDescription="A large study funded by the U.S. Food and Drug Administration (FDA) has found that hip replacements are more likely to fail in women than in men."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>New Study: Hip Replacements More Likely to Fail in Women</h1>
        <BreadCrumbs location={location} />
        <p>
          Starting in 2014, the U.S. government will make it mandatory for
          pharmaceutical and medical device manufacturers to begin publicly
          reporting payments they make to doctors. According to a Feb. 1 news
          report by ProPublica, the release of this data in September 2014 would
          mark a milestone in the push to bring transparency into medicine.
        </p>
        <img
          src="/images/product-liability-attorneys.jpg"
          alt="Product Liability Attorneys"
          className="imgright-fixed"
        />
        <p>
          In September 2014, the government will release information about
          payments received by physicians from dug and medical device
          manufacturers. According to a Feb. 1{" "}
          <Link to="http://www.propublica.org/article/feds-to-publicize-drug-and-device-company-payments-to-doctors-next-year">
            report
          </Link>{" "}
          by ProPublica, once posted, patients will be able to see if their
          doctors receive money from any of the companies whose products they
          prescribe.
        </p>
        <p>
          ProPublica's own{" "}
          <Link to="http://projects.propublica.org/docdollars/">
            Dollars for Docs tool
          </Link>{" "}
          is a freely available source for members of the public to search and
          analyze the payments made since 2009 by a dozen drug companies, the
          report states. ProPublica compiled its database using information
          posted on the corporations' websites that was required as part of
          settlements with the federal government over allegations of improper
          marketing.
        </p>
        <p>
          ProPublica states that this information was supposed to become public
          the beginning of this year under provisions of the Physicians Payments
          Sunshine Act, which was part of the 2010 health reform law. But
          federal officials instead released proposed regulations in December
          2011 and have since been gathering and analyzing comments, the report
          states.
        </p>
        <p>
          The data to be released in September 2014 will include payments made
          from August to December of this year, giving companies sufficient time
          to gather and report the information. The types of payments to be
          reported include speaking fees, consulting payments, research, gifts,
          food, entertainment, honoraria, research grants, royalties and license
          fees, among others, the report states.
        </p>
        <p>
          It is important that patients and consumers know if their doctors have
          a financial relationship with the companies that make the medicines or
          medical devices they need, said John Bisnar, founder of the Bisnar
          Chase personal injury law firm.
        </p>
        <p>
          "I'm really pleased to hear that this information is finally going to
          become public. When consumers know these facts and data, it allows
          them to have a dialog with their doctors and make an informed decision
          about the course of their treatment. There is no question that
          disclosure brings more credibility and accountability to the process."
        </p>
        <p>
          A patient wants to feel confident about the choices his or her doctor
          is making, Bisnar said. "This is not because patients suspect that
          their doctors were somehow influenced by their relationships with drug
          makers or medical device companies. On the other hand, this
          transparency will allow patients to feel comfortable with the course
          of treatment their doctors have chosen for them. In addition, it will
          also promote accountability by holding those in the healthcare field
          responsible for the important decisions they make."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California{" "}
          <Link to="/defective-products">product liability lawyers</Link> of
          Bisnar Chase represent victims of defective products such as
          medications. The firm has been featured on a number of popular media
          outlets including Newsweek, Fox, NBC, and ABC and is known for its
          passionate pursuit of results for their clients. Since 1978, Bisnar
          Chase has recovered millions of dollars for victims of defective
          products.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit{" "}
          <Link to="/" onClick="linkClick(this.to)">
            /
          </Link>{" "}
          for a free consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
