// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Verdict in Difficult Orange County Car Accident Case - Bisnar Chase."
        pageDescription="H.Gavin Long, a personal injury attorney for Bisnar Chase in Newport Beach, CA obtained a verdict in a very challenging car accident case in May of 2013."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Newport Beach Personal Injury Law Firm Bisnar Chase Wins Challenging
          Auto Accident Case
        </h1>
        <BreadCrumbs location={location} />
        <h2>
          Law firm obtains compensation for Orange County woman who suffered
          injuries in a car accident involving two other vehicles despite a
          tough venue and strong liability and causation defenses. An Orange
          County jury returned a verdict in this case (Number: 30-2011-00491993)
          May 28.
        </h2>
        <i>
          <p>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/6/prweb10790307.htm">
              press release
            </Link>{" "}
            from Bisnar Chase
          </p>
        </i>
        <p>
          The <Link to="/">Newport Beach personal injury law firm</Link> of
          Bisnar Chase obtained compensation for medical expenses and other
          related damages in a challenging auto accident case (Number:
          30-2011-00491993), which resulted in injuries for the victim. The jury
          returned a verdict May 28. The case was filed in Orange County
          Superior Court, Central Division, on July 18, 2011.
        </p>
        <p>
          According to court documents, the case stemmed from an incident, which
          occurred Sept. 22, 2009 in the Tustin Marketplace. Court records state
          that the defendant in the case, Jillann Benson, was northbound on
          Jamboree in the number two lane when she was cut off by another
          vehicle traveling in the third lane that crossed over three lanes to
          make a left turn in front of her vehicle. Benson swerved in an attempt
          to avoid hitting the vehicle in turn striking the car of Estora Pugh,
          according to the lawsuit. The third vehicle, which cut off Benson's
          car, left the scene of the incident, the complaint states.
        </p>
        <p>
          Pugh declined treatment at the scene, but reported hip pain a month
          after the accident and an x-ray four months after the accident showed
          that she had suffered a hairline fracture in her hip, the lawsuit
          states. The jury returned a gross $89,000 verdict in Pugh's favor,
          amount that excludes attorney's fees and costs, according to the
          judgment issued. The defense had offered her $15,000.
        </p>
        <p>
          <img
            src="/images/gavin.jpg"
            className="imgright-fixed"
            height="210"
            width="210"
            alt="H. Gavin Long - Car accident attorney - Bisnar Chase"
          />{" "}
          <Link to="/attorneys/gavin-long">Gavin Long</Link>, a trial lawyer
          with the Bisnar Chase personal injury law firm, said this case was
          extremely challenging for many reasons. "In this case, our client had
          declined treatment and denied feeling any pain at the scene," Long
          explained. "She had a preexisting hip condition, prior hip surgery
          recommendation arising from hip arthritis.
        </p>

        <p>
          The police report (Report Number: 09-5896, Tustin Police Department)
          attributed a statement to Pugh essentially stating that she took
          evasive action as well because she anticipated the defendant would
          take evasive action in response to the defendant getting cut off by
          the unidentified third car."
        </p>
        <p>
          This statement essentially corroborated the defenses argument that the
          "sudden emergency" doctrine should be a complete defense to the case,
          Long said. "There was little property damage to both vehicles and
          medical records suggested that Pugh's inability to work was due to
          work issues and not the accident."
        </p>
        <p>
          But, Long said, he was happy to see that the jury not only awarded his
          client compensation for medical expenses, loss of earnings and other
          damages, but also determined 50-50 liability.
        </p>
        <p>
          This is Long's third straight jury trial victory in the last five
          months. He says a difficult victory like this one not only builds
          confidence and helps maintain the firm's already superior reputation,
          but also gives him the satisfaction of having fought the good fight
          for Estora Pugh and won justice for her against very tough odds.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
