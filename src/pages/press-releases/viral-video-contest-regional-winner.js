// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Awards Recipient for Distracted Driving Viral Video"
        pageDescription="Call 949-203-3814 for Highest-rated Depuy Hip Implant Attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California. No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Distracted Driving Viral Video Contest along with Car Accident
          Attorneys, Bisnar Chase Personal Injury Attorneys
        </h1>
        <BreadCrumbs location={location} />
        <p>
          Finalist of 2010-11 American Lawyer Academy (ALA) Viral Video
          Scholarship Contest Western Region have been chosen and are Waiting
          for Your Votes
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/5/prweb8373544.htm">
            PRWEB
          </Link>
          ) May 03, 2011
        </p>
        <p>
          <strong>
            Last week the contestants who worked diligently to produce short
            videos that educate and promote safe driving were narrowed down to
            three finalists for the Western Region American Lawyer Academy Viral
            Video Scholarship Contest.
          </strong>
        </p>
        <img
          src="/images/viral-video-winner.jpg"
          alt="Viral Video"
          className="imgright-fixed"
        />
        <p>
          These three finalists have been posted to the Best Attorney website
          and need the support of their fans to make it to the next round.
          Interested individuals are encouraged to go to the website and vote
          for their favorite video. Voters can help choose the Western Regional
          Finalist who will make it to the National Round. Voters can go to the
          Video Finalist Page and vote for their favorite video.
        </p>
        <p>
          The purpose of these videos is to help inform the public of the
          thousands of accident victims who have been injured or killed in motor
          vehicle collisions as a result of distracted driving. The ultimate
          goal of the scholarship contest is to save future lives through
          encouraging the videos to go viral and reach as many people as
          possible.
        </p>
        <img
          src="/images/viral-video-west-finalist-1.jpg"
          alt="Distracted Driving"
          className="imgright-fixed"
        />
        <p>
          According to Distraction.gov in 2009, 5,474 people were killed in U.S.
          roadways and an estimated additional 448,000 were injured in motor
          vehicle crashes that were reported to have involved distracted
          driving.
        </p>
        <p>
          Regional voting closes on May 7, 2011. Once the numbers are tallied,
          the winner of the Western region will be sent to the National Contest
          and will have a chance to win a scholarship that goes toward their
          college education. Not only will these videos provide public awareness
          of driving safety, but it will also give winners the chance to
          continue their education and expand their knowledge.
        </p>
        <img
          src="/images/viral-video-west-finalist-2.jpg"
          alt="Distracted Driving"
          className="imgright-fixed"
        />
        <h2>About the ALA Viral Video Scholarship Contest</h2>
        <p>
          The American Lawyer Academy Viral Video Scholarship Contest offers
          $25,000 in scholarships toward college tuition at a recognized
          academic institution. Students in the Western Region that want to use
          their talents and creativity to help spread a positive safe driving
          message that captures the imagination of the YouTube generation can
          visit Video Finalist Page to get video western regional submission
          guidelines, contest rules, and more.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys is a California personal injury
          law firm that represents people who have been very seriously injured
          or lost a family member due to an accident, defective product or
          negligence. The law firm has recovered hundreds of millions of dollars
          for its over 12,000 clients since 1978 by winning a wide variety of
          challenging personal injury cases involving traffic collisions, work
          place injuries and defective products, including defective
          automobiles, against some of the world's largest companies and
          governmental agencies. Get a complimentary copy of John Bisnar's book,
          "The Seven Fatal Mistakes That Can Wreck Your California Personal
          Injury Claim,"
        </p>
        <p>
          {" "}
          <Link to="/press-releases/community-outreach">
            Click Here For More Community Outreach Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
