// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nebraska Wrongful Death Lawsuit Invokes Fetal Death Law"
        pageDescription="a wrongful death lawsuit invoked the state's fetal death law in an accident where an overtired trucker killed a pregnant woman and her unborn child in a fatal car accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Nebraska Wrongful Death Lawsuit Invokes State's Fetal Death Law</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Orange County wrongful death lawyer comments on a truck crash
            lawsuit (Case Number 8:2012cv00383) in Nebraska, which invokes the
            state's fetal death law. According to a Nov. 4 news report in The
            AP, the lawsuit was filed by the parents of a family killed in a car
            accident in September.
          </strong>
        </p>
        <p>
          Newport Beach, CA (PRWEB) November 11, 2012 -- A wrongful death
          lawsuit (Case Number 8:2012cv00383) that was filed by a California
          couple whose family was killed in a crash for the first time invokes
          the state's fetal death law. According to a Nov. 4 Associated Press
          news report, the lawsuit was filed in Nebraska District Court on Oct.
          29 by Bradley and Nancy Baumann, parents of Christopher and Diana
          Schmidt who were killed along with their children in the crash.
        </p>
        <p>
          <img
            src="/images/nebraska-wrongful-death.jpg"
            alt="nebraska wrongful death lawsuit invokes fetal death law"
            className="imgright-fixed"
          />
          The report states that the Schmidts were traveling in separate cars on
          Sept. 9 when a large truck driven by Josef Slezak rammed into the back
          of Christopher Schmidt's car. The force of the collision pushed his
          car into his pregnant wife's vehicle, which was crushed under the
          semi, the report states. The couple, their two children and their
          unborn son, died instantly.
        </p>
        <p>
          The AP report states that this is the first civil lawsuit to cite the
          2003 Nebraska state law, which has been controversial over the years.
          So far, prosecutors have only used this state law to charge motorists
          who contributed to fatal accidents, the report states. In this case,
          the truck driver, who officials say struck Schmidt's car at 75 mph
          without hitting the brakes, has been charged with vehicular
          manslaughter, vehicular homicide and one count of vehicular homicide
          of an unborn child, the report states. The lawsuit alleges that Slezak
          violated federal hours-of-service rules by staying on the road far
          longer than the maximum time allowed for truckers.
        </p>
        <p>
          "I offer my deepest condolences to the family members and friends of
          the Schmidts who are grieving this terrible loss," said John Bisnar,
          founder of the Bisnar Chase{" "}
          <Link to="/">personal injury law firm in Orange County</Link>, CA.
        </p>
        <p>
          Truck drivers and trucking companies are required to follow several
          federal safety regulations including the hours-of-service regulations,
          which require truckers to be off-duty for at least 10 hours before
          they can drive, said Bisnar. "Fatigued truck drivers who are pushing
          unrealistic deadlines and trucking firms that ignore violations of
          these rules must be held accountable. These regulations are in place
          to ensure that safety of the traveling public."
        </p>
        <p>
          It will be very interesting indeed to watch how the federal court in
          Nebraska handles the issue of the unborn child, says Bisnar. "In my
          opinion, losing a child is emotionally devastating for a parent or
          grandparent regardless of whether that child is inside or outside the
          womb. But, how the federal courts interpret this state law in a civil
          wrongful death case could set an important precedent on the matter."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The{" "}
          <Link to="/wrongful-death">California wrongful death attorneys</Link>{" "}
          of Bisnar Chase represent victims of auto accidents, defective
          products, dangerous roadways, and many other personal injuries. The
          firm has been featured on a number of popular media outlets including
          Newsweek, Fox, NBC, and ABC and is known for its passionate pursuit of
          results for their clients. Since 1978, Bisnar Chase has recovered
          millions of dollars for victims of auto accidents, auto defects and
          dangerously designed and/or maintained roadways. For more information,
          please call 949-203-3814 or visit / for a free consultation.
        </p>
        <p>
          <em>
            Sources:
            http://www.sfgate.com/news/crime/article/Neb-crash-lawsuit-invokes-fetal-death-law-4007073.php,
            http://dockets.justia.com/docket/nebraska/nedce/8:2012cv00383/60774/{" "}
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
