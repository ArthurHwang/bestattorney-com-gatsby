// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Personal Injury Attorneys - Bisnar Chase Associates Day"
        pageDescription="Are you seriously injured? Call 949-203-3814 for California personal injury attorneys who care. Free consultations. No win, no fee attorneys. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Celebrates First Ever "Associates Day" Honoring Firm's
          Award-Winning Personal Injury Attorneys
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The California Personal Injury Attorneys of Bisnar Chase Personal
          Injury Attorneys (/) today celebrated their first-ever "Associates
          Day" in honor of the firm's hard-working, award-winning personal
          injury attorneys. The entire Bisnar Chase Personal Injury Attorneys
          staff celebrated the firm's associates Jerusalem Beligan, J. Michael
          McClure, Scott Ritsema and Sarah Serpa with a surprise catered
          breakfast and appreciation ceremony at the Bisnar Chase Personal
          Injury Attorneys offices in Newport Beach, California.
        </p>
        <img
          src="/images/Associates Day.jpg"
          alt="Associates Day"
          className="imgright-fixed"
        />
        <p>
          John Bisnar, founder of Bisnar Chase Personal Injury Attorneys, said
          his firm created Associates Day to honor what he calls our "special
          forces leaders" in the fight for personal injury victims' rights.
        </p>
        <p>
          "Going up against corporate giants and insurance companies is like
          going to war," said Bisnar. "Our associates are on the front line,
          going head-to-head in battle, representing people who have suffered
          catastrophic injuries or lost a family member. Our associates are the
          leaders of our "special forces units" of this firm " highly skilled,
          heavily armed and ready to fight " and as such, we felt it was
          important to honor them for their hard work and commitment to our
          clients' causes."
        </p>
        <p>
          <strong>
            Information about today's Bisnar Chase Personal Injury Attorneys
            Associates Day honorees follows.
          </strong>
        </p>
        <h2>
          Jerusalem "The Rocket" Beligan, Personal Injury and Employment Law
          Attorney
        </h2>
        <p>
          Jerusalem Beligan, a southern California native and standout high
          school football player, where he earned the nickname "The Rocket" for
          his speed and evasive maneuvers on the field - followed in his
          father's footsteps to become a lawyer and is now one of the most
          victorious associates on the Bisnar Chase Personal Injury Attorneys
          team.
        </p>
        <p>
          He has an extraordinary track record for his legal prowess in
          particular, his tough-as-nails approach at going up against
          corporations for unfair treatment of employees " including more than
          ten employer/employee relations cases of significant note.
        </p>
        <p>
          Jerusalem was raised in Diamond Bar, California, and graduated magna
          cum laude from the University of California, Riverside in 1997. He
          obtained his Juris Doctor from Southwestern University School of Law
          in 2000 and passed the California state bar that same year when he
          began practicing law at the young age of 25.
        </p>
        <h2>J. Michael McClure, Personal Injury Attorney</h2>
        <p>
          New to the Bisnar Chase Personal Injury Attorneys team, but not new to
          law, J. Michael McClure's compassionate and determined philosophy has
          led him to win numerous cases and try over 35 jury trials to verdict.
        </p>
        <p>
          One of Michael's the most prestigious accomplishments is his granted
          membership in to The American Board of Trial Advocates (ABOTA), an
          esteemed professional organization with exclusive membership limited
          only to the most impressive trial attorneys.
        </p>
        <h2>Scott Ritsema, Personal Injury and Defective Products Attorney</h2>
        <p>
          Scott Ritsema joined Bisnar Chase Personal Injury Attorneys in 2008.
          He has litigated 15 trials to verdict and has conducted over 50
          arbitrations to conclusion.
        </p>
        <p>
          Scott is known for being a bulldog plaintiff's attorney, with the
          tenacity and determination he brings to every case he is handles.
        </p>
        <p>
          He was admitted to the California Bar in 1988 - the same year in which
          he won the Order of the Coif Award - and is currently a member of the
          Orange Country Trial Lawyers Association, American Bar Association and
          Consumer Attorneys of California.
        </p>
        <h2>Sarah Serpa, Personal Injury Attorney</h2>
        <p>
          Sarah Serpa is an outstanding litigator with extensive experience in
          products liability, MediCal/legal issues, brain injury matters, public
          entity litigation and employer vs. independent contractor issues.
        </p>
        <p>
          Sarah single-handedly won one of the largest personal injury case
          settlements in U.S. history and throughout her career, she has
          received numerous honors including Southern California Rising Stars
          2007; Outstanding Efforts and Involvement in the Promotion of Trial
          Lawyer Objectives of the Right for a Fair Jury Trial, Member of the
          Year, OCTLA, 2009; and was named a Southern California Super Lawyer in
          2011.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people throughout
          the country who have been very seriously injured or lost a family
          member due to an accident, defective product or negligence. The law
          firm has won a wide variety of challenging cases against careless
          drivers, corporate giants, insurance companies and negligent
          governmental agencies. For more information, please visit /.
        </p>
        <p>####</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
