// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Files Motion for Class Certification against Coca-Cola"
        pageDescription="The employment attorneys at Bisnar Chase Personal Injury Attorneys and Arias Ozzello & Gignac LLP announced today they filed a motion for class certification against Coca-Cola..."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Files Motion for Class Certification against Coca-Cola
        </h1>
        <BreadCrumbs location={location} />

        <p>
          June 12, 2012 -- NEWPORT BEACH, California -- The employment attorneys
          at Bisnar Chase, Personal Injury Attorneys (/) and Arias Ozzello &
          Gignac LLP announced today they filed a motion for class certification
          against Coca-Cola on May 29, 2012 to certify a class of at least 3,373
          current and former Merchandisers for failing to provide compliant meal
          periods and failing to reimburse them for business expenses incurred
          during the course-and-scope of their employment.{" "}
          <strong>The hearing on the motion is set for July 2, 2012</strong>.
        </p>
        <p>
          The case against Coca-Cola, which operates roughly 27 branches
          throughout the state of California, was brought in September 2011, in
          California state court in Los Angeles, but was removed to federal
          court based on a motion filed by Coca-Cola under the Class Action
          Fairness Act (CAFA).
        </p>
        <p>
          The proposed Class plaintiffs seek to certify consists of:
          <em>
            All former and current Merchandisers who worked for Coca-Cola in one
            of its branches in the State of California (the "Class Members")
            during the period between September 7, 2007 to the conclusion of
            this action (the "Class Period").
          </em>
          The Subclasses plaintiffs seek to certify are, among others, the
          following:
        </p>
        <p>
          (a) Subclass 1:
          <br />
          All Class Members who worked shifts of at least six hours during the
          Class Period whose time records show no meal period was taken [the
          "MISSED MEAL-PERIOD SUBCLASS"].
        </p>
        <p>
          (b) Subclass 2:
          <br />
          All Class Members who worked shifts of at least six hours during the
          Class Period whose time records show less than a 30-minute meal period
          [the "SHORT MEAL-PERIOD SUBCLASS"].
        </p>
        <p>
          (c) Subclass 3:
          <br />
          All Class Members who worked shifts of at least six hours during the
          Class Period whose time records show a meal period after the fifth
          hour of work [the "LATE MEAL-PERIOD SUBCLASS"].
        </p>
        <p>
          (d) Subclass 4:
          <br />
          All Class Members who were not reimbursed for all work-related
          non-commute mileage during the Class Period [the "NON-COMMUTE MILEAGE
          SUBCLASS"].
        </p>
        <p>
          (e) Subclass 5:
          <br />
          All Class Members, who qualified for commute mileage under Defendant's
          Mileage Reimbursement Policy and Procedure, but were not paid commute
          mileage during the Class Period [the "COMMUTE MILEAGE SUBCLASS"].
        </p>
        <p>
          "What differentiates this case from other class action wage-and-hour
          cases is that Coca-Colas own records establishes its liability," said
          Brian Chase, partner at Bisnar Chase. "Coca-Cola recorded every
          incident of a non-compliant meal period, including missed-meal,
          short-meal or late meal periods. Under California law, employers are
          required to pay premium wages when an employee is not provided a meal
          period. Coca-Cola lacked a policy of paying Merchandisers premium pay
          for meal period violations."
        </p>
        <p>
          The California employment lawyers at Bisnar Chase and Arias Ozzello &
          Gignac LLP are seeking damages for premium wages for meal-period
          violations, unreimbursed expenses, and waiting time penalties for
          failing to compensate former employees within the Class Period for
          these unpaid wages.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Employment Attorneys represent people who have been
          the victims of employer abuse which includes wage and overtime claims,
          wrongful termination, sexual harassment, disability discrimination,
          work breaks, meal breaks, forced deduction from pay checks, expense
          reimbursement, travel expenses, uniform cost and upkeep reimbursement,
          breach of contract complaints and more. Bisnar Chase has won thousands
          of cases for regular people against big business, insurance companies
          and governmental agencies. For more information, please visit{" "}
          <Link to="/">/</Link>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
