// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Attorneys Named as Co-Counsel in Product Liability Lawsuit Against Isuzu"
        pageDescription="Were you seriously hurt in a car accident? Call 949-203-3814 for a California product liability attorney who can help. Free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Attorneys Named as Co-Counsel in Product
          Liability Lawsuit Against Isuzu
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            The California auto defects lawyers of Bisnar Chase Personal Injury
            Attorneys have been named as co-counsel in an auto defect product
            liability case against Isuzu.
          </strong>
        </p>
        <p>
          The lawsuit was filed by Gilbert, Ollanik & Komyatte, P.C., a leading
          Colorado automotive product liability law firm, specializing in auto
          defects and product safety. The suit alleges the 2001 Isuzu Rodeo
          manufactured and sold by the defendant and its subsidiaries and
          affiliates was defective and failed to properly protect Idaho
          residents Stacey Schrock and Christina Monroe from severe personal
          injuries during an October 2008 rollover accident. The case is pending
          in the Orange County Superior Court of California, case #
          30-2010-00419235.
        </p>
        <h2>The defendants listed in this case are the following:</h2>
        <ul>
          <li>Isuzu Motors Limited</li>
          <li>
            Isuzu North American Corporation Isuzu Motors America, LLC (through
            its predecessor Isuzu Motors America, Inc.)
          </li>
          <li>
            Subaru of Indiana Automotive, Inc. (through its predecessor
            Subaru-Isuzu Automotive, Inc.)
          </li>
          <li>Isuzu Manufacturing Services of America, Inc.</li>
          <li>Isuzu Motors America, Inc.</li>
          <li>Isuzu Technical Center of America, Inc.</li>
          <li>American Isuzu Motors Inc.</li>
          <li>Fuji Heavy Industries, Ltd.</li>
          <li>Takata Corporation</li>
          <li>TK Holdings, Inc.</li>
          <li>Takata Seat Belts, Inc.</li>
          <li>TI Holdings, Inc.</li>
        </ul>
        <h2>Isuzu Rodeo Rollover Accident Severely Injures Two Women</h2>
        <p>
          According to court documents, Stacey Schrock and Christina Monroe,
          both residents of Idaho, were the properly-restrained occupants of a
          2001 Isuzu Rodeo as it traveled westbound on Interstate 84 near
          Highway 27, close to the town of Burley, Idaho on October 24, 2008.
        </p>
        <p>
          Even at legal highway speeds, the driver was unable to maintain
          control of the Isuzu Rodeo as it careened toward the center median. In
          an effort to regain control, the driver over-corrected the vehicle's
          steering upon which the SUV went into a non-recoverable broadside
          skid. It rotated clockwise and rolled over, causing the ejection of
          the plaintiffs who sustained severe and traumatic personal injuries,
          including massive brain injuries and paraplegia, in the crash.
        </p>
        <h2>Numerous Isuzu Rodeo Defects Alleged</h2>
        <p>
          The lawsuit alleges the 2001 Isuzu Rodeo was wrought with a variety of
          defects, including the following:
        </p>
        <ul>
          <li>
            Insufficient lateral and roll stability to keep the vehicle upright
            during cornering and handling maneuvers by ordinary drivers within
            reasonably foreseeable roadway and traffic conditions.
          </li>
          <li>
            Defective and unsafe seats and seat backs that failed during the
            crash sequence, not only allowing the plaintiffs to careen
            backwards, but also facilitating their ejection through the rear
            passenger window.
          </li>
          <li>
            Defective and unsafe seat belts, which failed during the crash
            sequence, further facilitating the ejection of the plaintiffs
            through the rear passenger window.
          </li>
          <li>
            The use of defective and unsafe glass and glass glazing in the side
            and rear windows which completely failed during the crash sequence,
            allowing the plaintiffs to be ejected from the vehicle through
            openings in the windows.
          </li>
        </ul>
        <p>
          "Not only were the defendants aware the Isuzu Rodeo was defective and
          dangerous, they failed to fix the problems or warn the public about
          those problems," said Brian Chase, partner at Bisnar Chase Personal
          Injury Attorneys and co-counsel in the case. "Despite the availability
          of simple solutions that would fix these problems, the defendants
          chose to cut costs at the expense of human safety."
        </p>
        <h2>According to Chase, these solutions include the following:</h2>
        <ul>
          <li>
            An electronic stability control system that uses electronic sensors
            to monitor steering commands, brake pressure and engine torque, to
            prevent rollover and side slip crashes.
          </li>
          <li>
            The use of wider and stronger metal in the seats, seat backs and
            seat connectors, to prevent seat and seat back failure during crash
            sequences.
          </li>
          <li>
            The use of laminated glass and protective glazing, to prevent the
            vehicle's windows from breaking and causing openings through which
            occupants could be ejected.
          </li>
        </ul>
        <p>
          "For just a few hundred dollars, Isuzu could have corrected these
          problems yet it chose to advance its own pecuniary interests," said
          Brian Chase. "As a result, two innocent women will suffer for the rest
          of their lives."
        </p>
        <p>
          The lawsuit seeks damages for severe personal injuries and pain and
          suffering; emotional and mental distress; disfigurement, loss of
          bodily function and impairment; past and future medical expenses; loss
          of past and future wages and earning ability; and court costs.
        </p>
        <h2>About Gilbert, Ollanik & Komyatte, P.C.</h2>
        <p>
          Gilbert, Ollanik & Komyatte, P.C. Colorado automotive product
          liability lawyers represent injured people and their families in
          lawsuits against some of the world's largest corporations. Since its
          inception in 1969, the firm's product safety lawyers have focused on
          litigation involving auto accidents and rollovers where people are
          seriously injured or killed because of automobile defects. The firm's
          attorneys have recovered more than $150 million dollars in verdicts &
          settlements on behalf of injured consumers in states across the
          country, including Colorado, Iowa, Missouri, California, Nebraska,
          Pennsylvania, South Dakota, Florida, Texas, Utah, Washington, Georgia,
          West Virginia, New Mexico, Wyoming, and Illinois
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people who have
          been very seriously injured or lost a family member due to an
          accident, defective product or negligence throughout the country from
          their California headquarters. The auto defects law firm has won a
          wide variety of auto defect cases against most of the major auto
          manufacturers, including Ford, General Motors, Toyota, Nissan and
          Chrysler. Brian Chase is the author of the most up-to-date and
          comprehensive auto defect book available today, Still Unsafe at Any
          Speed: Auto Defects that Cause Wrongful Deaths and Catastrophic
          Injuries. For more information, visit Mr. Chase's blog at
          http://www.ProductDefectNews.com.
        </p>
        {/* <p>
          {" "}<Link to="/offices">Click Here</Link> for a list of local
          offices.
        </p> */}

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
