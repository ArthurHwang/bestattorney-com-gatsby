// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Defective drugs: Many Compounding Pharmacies in MA Fail Health Inspections"
        pageDescription="Very few compounding pharmacies in Massachusetts have passed surprise health inspections, & are similar to the one responsible for last year's outbreak."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Many Compounding Pharmacies in MA Fail Surprise Health Inspections
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10413314.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 11, 2013
          </i>
        </p>
        <p>
          Only four out of 37 compounding pharmacies in Massachusetts made the
          grade after surprise health inspections over the last several months.
        </p>
        <p>
          According to a Feb. 6 news report in The Boston Globe, the 37
          pharmacies that were inspected are similar to the Framingham facility
          blamed for the fatal outbreak of fungal meningitis last year.
        </p>
        <img
          src="/images/defective-drug-lawsuits.jpg"
          width="171"
          alt="Defective Drug Lawsuits"
          className="imgright-fixed"
        />
        <p>
          Surprise state inspections at 37 specialty pharmacies in Massachusetts
          show that only four have been complying with industry safety
          standards.
        </p>
        <p>
          According to a Feb. 6{" "}
          <Link to="http://www.boston.com/whitecoatnotes/2013/02/05/just-massachusetts-compounding-pharmacies-passed-surprise-health-inspections/zFT7FKklNPfj6EdW4tYu1L/story.html">
            article
          </Link>{" "}
          in The Boston Globe, the inspected facilities are similar to the
          Framingham pharmacy that was shut down after a fatal fungal meningitis
          outbreak that has been blamed for 45 deaths and 700 illnesses
          nationwide.
        </p>
        <p>
          The article reports that state inspectors found serious violations in
          11 compounders prompting the state to temporarily shut down all or
          part of their operations while 21 others were cited for relatively
          minor violations.
        </p>
        <p>
          The Department of Public Health began its inspections in October after
          New England Compounding Center's
          {/* <!-- Touched by Arthur 10/23/18 -->
      <!-- {" "}<Link to="/blog/wrongful-death-lawsuit">contaminated steroids</Link>  --> */}{" "}
          <Link to="/blog/wrongful-death-lawsuit-filed-by-husband-of-latina-music-star-jenni-rivera">
            contaminated steroids
          </Link>{" "}
          were linked to a countrywide meningitis outbreak. Patient safety
          advocates say stricter oversight of the industry is a necessity not
          only in Massachusetts but nationwide.
        </p>
        <p>
          The report states that many other states do not even require their
          pharmacies to follow nationally accepted safety guidelines when it
          comes to injectable and intravenous medications.
        </p>
        <p>
          Compounding pharmacies usually prepare doses and formulations of drugs
          for individual patients that are not commonly available from drug
          makers, the article states.
        </p>
        <p>
          The lack of oversight in this industry is absolutely appalling, said
          John Bisnar, founder of the Bisnar Chase personal injury law firm,
          which represents victims of defective drugs. "It is necessary that
          other states learn from the tragedy that has happened in Massachusetts
          and tighten up standards and procedures. To not even have basic safety
          requirements for medications that are injected into patients' bodies
          is absolutely unacceptable."
        </p>
        <p>
          It is also deeply disturbing that many of these compounding pharmacies
          that were inspected had unsanitary conditions, Bisnar said. "Product
          manufacturers are required to provide items that are safe for their
          consumers. Whether it is furniture, medical devices, drugs or
          household appliances, manufacturers have the legal obligation to put
          on the market, products that are safe for consumers."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California product liability lawyers of Bisnar Chase represent
          victims of defective products such as prescription drugs. The firm has
          been featured on a number of popular media outlets including Newsweek,
          Fox, NBC, and ABC and is known for its passionate pursuit of results
          for their clients. Since 1978, Bisnar Chase has recovered millions of
          dollars for victims of defective products.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          http://www.boston.com/whitecoatnotes/2013/02/05/just-massachusetts-compounding-pharmacies-passed-surprise-health-inspections/zFT7FKklNPfj6EdW4tYu1L/story.html
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
