// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Teenager Awarded $63 Million After Losing Skin Taking Motrin"
        pageDescription="A teenager who has lost her skin in a life threatening drug reaction to Motrin has been awarded a settlement of $63 Million."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Jury Awards $63 Million to Teenager Who Lost Skin After Taking Motrin
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <em>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10437700.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 11, 2013.
          </em>
        </p>
        <img
          src="/images/defective-drug-attorneys.jpg"
          width="225"
          alt="Defective Drug Attorneys"
          className="imgright-fixed"
        />
        <p>
          A jury (Case number: PLCV2007-00064) has awarded $63 million to a
          Massachusetts teen and her parents nearly a decade after she suffered
          a life threatening drug reaction that caused her to lose most of her
          skin after taking Johnson &amp; Johnson's children's pain reliever,
          Motrin. According to a Feb. 14 CBS News report, the pharmaceutical
          giant and its subsidiary, McNeil-PPC Inc. have been ordered to pay
          Reckis and her family a total of $109 million including interest.
        </p>
        <p>
          The family of a teenager, who lost most of her skin and suffered
          irreversible health consequences including organ damage, has been
          awarded $63 million in damages in a{" "}
          <Link to="/defective-products">product liability lawsuit</Link> (Case
          number: PLCV2007-00064) nearly 10 years after she took Children's
          Motrin.
        </p>
        <p>
          A Feb. 14 CBS News report states that a Plymouth Superior jury ordered
          pharmaceutical giant Johnson &amp; Johnson and its subsidiary
          McNeil-PPC to pay Samantha Reckis and her parents a total of $109
          million, which includes interest.
        </p>
        <p>
          The article states that Samantha was 7 years old when she was given
          Motrin brand ibuprofen and suffered a side effect known as toxic
          epidermal necrolysis. She lost 90 percent of her skin, suffered brain
          damage and serious respiratory issues, leaving her with only 20
          percent of lung capacity.
        </p>
        <p>
          Surgeons had to drill through her skull to relieve the pressure on her
          brain, the report states. The medication inflamed her throat, mouth,
          eyes, esophagus, intestinal tract, respiratory and reproductive
          systems, forcing doctors to put her in a coma according to the
          article. The results of this{" "}
          <Link to="/pharmaceutical-litigation">defective drug</Link> reaction
          have been devastating.
        </p>
        <p>
          The Reckis family filed the lawsuit (Case number: PLCV2007-00064) in
          January 2007 alleging that Samantha was blinded by Motrin and that the
          manufacturer failed to warn consumers that the drug could cause life
          threatening reactions.
        </p>
        <p>
          The trial, which lasted five weeks, ended on Feb. 13 when the jury
          awarded $50 million in compensatory damages to Samantha and $6.5
          million to each of her parents. McNeil has stated they are not pleased
          with the verdict and are exploring their legal options to appeal the
          decision, CBS News reports.
        </p>
        <p>
          The landmark verdict is a victory for consumer safety, said John
          Bisnar, founder of the Bisnar Chase personal injury law firm. "I'm
          glad that this jury saw the facts of this case and the suffering
          endured by this child. Pharmaceutical companies have a duty and a
          responsibility to warn consumers about known, devastating drug
          effects. Consumers have a right to know what they are putting in their
          bodies."
        </p>
        <p>
          Most pharmaceutical companies tend to rest easy after get their drugs
          approved by the U.S. Food and Drug Administration, said Bisnar. "Their
          responsibility doesn't end there. It is also their duty to equip
          consumers with facts and information, so consumers can make an
          informed decision about whether or not they want to use the drug.
        </p>
        <p>
          Putting the dangers or risks associated with a particular drug is not
          the best marketing strategy for these companies. But, it is verdicts
          such as this one, that jolt product manufacturers to the realization
          that they need to start putting people ahead of profits."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          Bisnar Chase represent victims who have suffered serious side effects
          as a result of defective drugs. The firm has been featured on a number
          of popular media outlets including Newsweek, Fox, NBC, and ABC and is
          known for its passionate pursuit of results for their clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. We are located at 1301 Dove Street #120, Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          http://www.cbsnews.com/8301-204_162-57569388/jury-awards-$63m-to-samantha-reckis-girl-who-lost-skin-after-taking-motrin/
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
