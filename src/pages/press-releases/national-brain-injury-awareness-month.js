// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Shares Thoughts On National Brain Injury Awareness Month"
        pageDescription="The Brain Injury Association of America is kicking off their yearlong campaign to educate the public about the dangers of traumatic brain injuries by declaring March as National Brain Injury Awareness Month."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          March is National Brain Injury Awareness Month: Bisnar Chase Shares
          Thoughts
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/3/prweb10514100.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          The Brain Injury Association of America is kicking off their yearlong
          campaign to educate the public about the dangers of traumatic brain
          injuries by declaring March as National Brain Injury Awareness Month.
        </p>
        <p>
          According to the association's website, the goal of this campaign is
          to help prevent brain injuries, foster research and education to help
          those affected by brain injury and bring hope and healing to millions
          of people in the United States who are living with a brain injury.
        </p>
        <img
          src="/images/brain-injury-photo.jpg"
          width="205"
          height="171"
          alt="nursing home abuse"
          className="imgright-fixed"
        />
        <p>
          The personal injury attorneys of Bisnar Chase are 100 percent
          supportive of the Brain Injury Association and the work they do in our
          communities. We represent victims who have suffered brain injuries as
          a result of auto accidents and other acts of negligence or wrongdoing.
        </p>
        <p>
          The Brain Injury Association describes a{" "}
          <Link to="/head-injury/brain-trauma">traumatic brain injury</Link> or
          TBI as a blow, jolt or bump to the head or a penetrating head injury
          that disrupts the normal function of the brain.
        </p>
        <p>
          The statistics compiled by this group are informative and powerful:
        </p>
        <p>
          -- In the United States, 1.7 million people, including 475,000
          children, sustain a TBI each year.
        </p>
        <p>
          -- 3.1 million individuals live with a lifelong disability as the
          result of a TBI.
        </p>
        <p>
          -- 52,000 people die and 275,000 people are hospitalized each year
          with brain injuries.
        </p>
        <p>
          The main causes of TBIs are; <br />
          -- Falls (35 percent) <br />
          -- Car crashes (17 percent) <br />
          -- Workplace accidents (16 percent) <br />
          -- Assaults (10 percent)
        </p>
        <p>
          -- The average cost of hospital-based acute rehab costs $8,000 per
          day.
        </p>
        <p>
          -- The range for post-acute residential is about $850 to $2,500 per
          day.
        </p>
        <p>
          -- Day treatment programs such as physical and occupational therapy
          cost about $1,000 per day.
        </p>
        <p>
          -- The medical and other costs of TBI total an estimated $76.3 billion
          each year.
        </p>
        <p>
          Traumatic brain injuries affect not only the victims, but also their
          families, says John Bisnar, founder of the Bisnar Chase personal
          injury law firm. "We represented a young brain injury victim whose
          parents had to take time off work or even give up their job to care
          for their son. It is emotionally and financially rough for these
          families."
        </p>
        <p>
          It is eye-opening to see the statistics presented by the Brain Injury
          Association, Bisnar says. "It really drives home the impact these
          types of catastrophic injuries have on individuals and their families
        </p>
        <p>
          During the month of March, let us all resolve not only to spread
          awareness, but also to support survivors and their families."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>
        <p>Source: http://www.biausa.org/brain-injury-awareness-month.htm</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
