// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Personal Injury Attorneys - California Accident Lawyers"
        pageDescription="Injured in an accident? Call 949-203-3814 for high rated personal injury lawyers. Serving California since 1978. No win no fee attorneys. Call today for more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Client Stories</h1>
        <BreadCrumbs location={location} />

        <p>FOR IMMEDIATE RELEASE - October 4, 2007</p>
        <p>
          Contact: Todd Richissin
          <br />
          Cook & Schmid
          <br />
          443-388-9910
        </p>
        <p align="center">
          <strong>
            <u>
              Bisnar Chase Personal Injury Attorneys FILES FEDERAL LAWSUIT
              AGAINST CALIFORNIA SUPREME COURT AND APPELATE DISTRICT ALLEGING
              VIOLATIONS OF DUE PROCESS AND EQUAL PROTECTION CLAUSES OF U.S.
              CONSTITUTION
            </u>
          </strong>
        </p>
        <p align="center">
          <strong>
            <u>
              Accusations Judges Use 'Unpublished' Opinions To Avoid Judicial
              Review
            </u>
          </strong>
        </p>
        <p align="center">
          <strong>
            <u>
              Suit On Behalf Of Boy Partially Blinded by Edison Employee Could
              Dramatically Alter How California Civil Cases Are Won and Lost
            </u>
          </strong>
        </p>
        <p>
          <em>
            Hild v. California Supreme Court, California Court of Appeal, Second
            Appellate District
          </em>
        </p>
        <p>
          LOS ANGELES - In a move that could fundamentally alter how lawsuits
          are won and lost in California courts, the Orange County law firm
          Bisnar Chase Personal Injury Attorneys today filed suit against the
          state's Supreme Court and one of its appellate districts, alleging
          widespread violations of due-process and equal protection rights
          guaranteed in the U.S. Constitution.
        </p>
        <p>
          The suit argues that rules imposed by the state Supreme Court have led
          some or all of the districts that make up the California Court of
          Appeal to increasingly issue rulings accompanied by "unpublished"
          opinions. Under the high-court's rules, "unpublished" opinions are
          essentially ''non-precedential'' or "non-citation" decisions that
          cannot be cited by attorneys in future cases to bolster their
          arguments nor can jurists consult them for precedent or even guidance.
        </p>
        <p>
          And because unpublished opinions have no practical reach outside the
          case at hand, only in exceptionally rare cases will the higher court
          review an unpublished opinion. Joshua Hild, a teen-ager from Big
          Creek, a small town about 90 miles northeast of Fresno, is named as
          the plaintiff in the suit. Acting through his guardian ad litem, Hild
          sued the Southern California Edison Co. (SCE) after a paint gun being
          handled by an on-duty employee of the company discharged, permanently
          blinding him in his right eye.
        </p>
        <p>
          Legal scholars say the lawsuit is a legitimate challenge to the
          California Supreme Court's rule. A similar rule was struck down at the
          federal level last year for opinions after Jan. 1. (For revised
          federal rule see:{" "}
          <Link to="http://nonpublication.com/rule32.1.pdf" target="_blank">
            http://nonpublication.com/rule32.1.pdf
          </Link>
          )
        </p>
        <p>
          The suit asks for an order forbidding enforcement of the "non-citation
          rule," as it has become known (C.R.C. Rule 8.115), and for the court
          to void the appellate ruling on the grounds it was a product of an
          unconstitutional system of "selective prospectivity," which deprived
          him of his 14th Amendment rights to due process and equal protection.
        </p>
        <p>
          In 2005, a jury that heard his case in Los Angeles County Superior
          Court awarded him $704,633. SCE appealed, and on June 25, 2007, the
          California Court of Appeal, Second Appellate District, issued its
          unpublished opinion, rejecting the jury's verdict.
        </p>
        <p>
          "The appellate court reached conclusions that are absolutely contrary
          to legislative and established law," said Brian D. Chase, the attorney
          who represented the teen at trial and during appeal and who filed the
          case in U.S. District Court, Northern District of California.
        </p>
        <p>
          "This child loses an eye to an on-duty Edison employee, for which a
          jury found Edison responsible, and now gets an obviously
          pre-determined unpublished opinion. And because of that - it being
          unpublished - there's almost no chance that the Supreme Court will
          review it. So, the ruling itself - flawed as it is - is left to stand
          even though it doesn't have even a single leg to support it."
        </p>
        <p>
          The appeals court, the federal suit says, "offered no explanation
          whatsoever as to how or why the scores of other cases cited by
          Plaintiff - which plainly conflicted with its decision - were or are
          'distinguishable." Further, the appeals court "purposely" issued its
          opinion as "unpublished" to make certain that the teen was denied the
          right to judicial review, the federal suit says.
        </p>
        <p>
          "When you consider the practical affect of this rule, it's
          dumbfounding that it could have been on the books this long," Chase
          said. "It brings the legal process less accountability, less
          transparency, less uniformity and more opportunities for people to get
          their day in court only to be ruled against by a secretive-like
          opinion that is flawed from top to bottom."
        </p>
        <h2>ABOUT Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys, is a California law firm,
          headquartered in Newport Beach, that litigates consumer, serious
          personal injury and wrongful death cases throughout the country.
          Bisnar Chase Personal Injury Attorneys accepts all types of consumer
          and personal injury cases, but the firm built its formidable
          reputation on winning defective automobile cases. Bisnar Chase
          Personal Injury Attorneys recovers millions of dollars annually for
          its clients, including cases against the largest automobile
          manufacturers in the world. For more information, visit:{" "}
          <Link to="http://www.auto-defect-attorneys.com" target="_blank">
            www.auto-defect-attorneys.com
          </Link>
          , www.bisnar-chase.com and{" "}
          <Link to="/">www.bestatto-gatsby.netlify.app</Link>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
