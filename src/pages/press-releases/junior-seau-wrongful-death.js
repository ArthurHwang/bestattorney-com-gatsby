// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Wrongful Death Lawsuit Filed By Family of NFL Star Junior Seau"
        pageDescription="Junior Seau's family has filed a wrongful death lawsuit in California alleging that multiple concussions lead to the NFL star's suicide."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Wrongful Death Lawsuit Filed by NFL Star Junior Seau's
          Family Members
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <em>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/1/prweb10364072.htm">
              press release
            </Link>{" "}
            from Bisnar Chase.
          </em>
        </p>
        <p>
          <strong>
            Former linebacker Junior Seau's family has filed a wrongful death
            lawsuit against the NFL alleging that his suicide last May was the
            result of brain damage caused by violent hits he endured while
            playing football.
          </strong>
        </p>
        <p>
          <img
            src="/images/Junior_Seau.jpg"
            alt="Junior Seau pictured here before NFL retirement"
            className="imgright-fixed"
          />
          Retired football star Junior Seau's family has sued the National
          Football League alleging that his suicide was the result of a
          degenerative brain disease caused by repeated hits to the helmet.
        </p>
        <p>
          The case was filed in California, San Diego County Superior Court -
          Case number 37-2013-00031265-CU-PO-CTL.
        </p>
        <p>
          According to a Jan. 24 CNN report, Seau committed suicide in the
          bedroom of his Oceanside, Calif. home.
        </p>
        <p>
          Earlier this month, Seau was determined to have suffered from chronic
          traumatic encephalopathy, a brain disease that can be caused by
          constantly getting hit on the head, the report states. The{" "}
          <Link to="/wrongful-death/">wrongful death lawsuit</Link>, which was
          filed Jan. 23, accuses the NFL and the helmet manufacturer, Riddell
          Inc. of fraud, negligence and concealment.
        </p>
        <p>
          According to court records, the lawsuit was filed by Seau's three sons
          and daughter as well as his wife Gina Seau. The family issued a
          statement to CNN saying that they hope the lawsuit sends a message to
          the NFL that it needs to "take care of its former players, acknowledge
          its decades of deception on the issue of head injuries and player
          safety, and make the game safer for future generations."
        </p>
        <p>
          More than 1,500 former NFL players are suing the league alleging that
          it hid the dangers of concussions from them, the CNN report states.
          According to the article, Seau was one of several NFL players who were
          diagnosed with the same brain disease and committed suicide.
        </p>
        <p>
          <img
            src="/blog/wp-content/uploads/2013/01/jeau-seau-brain-injury.jpg"
            alt="Junior Seau retired from the NFL in 2010 following multiple concussions"
            className="imgright-fluid"
          />
          During his 20-year NFL career, Seau played for the San Diego Chargers,
          the Miami Dolphins and the New England Patriots, the report stated. He
          suffered sub-concussive and concussive blows to the head during his
          time as a player and began to show emotional instability and suffer
          insomnia until he died, the lawsuit stated.
        </p>
        <p>
          He also suffered from other symptoms such as lack of focus, aggressive
          behavior, depression and withdrawing from his family, and drank to
          cope with his problems.
        </p>
        <p>
          The purpose of wrongful death lawsuits is not only to seek monetary
          compensation for alleged negligence or wrongdoing, but also to shine
          the light on important safety issues, said{" "}
          <Link to="/attorneys/john-bisnar">John Bisnar</Link>, founder of
          Bisnar Chase personal injury law firm.
        </p>
        <p>
          <em>
            "I hope that this particular case brings out more facts about what
            really happened to Junior Seau and how concussions affect our
            professional athletes.
          </em>
        </p>
        <p>
          <em>
            What we find out from this case may help prevent tragic brain
            injuries, which apparently have the potential to destroy lives.
            There should be more of an effort, not only on the part of the NFL,
            but also local football leagues and high schools to create awareness
            about the danger of brain injuries in sports.
          </em>
        </p>
        <p>
          <em>
            New studies are increasingly showing that concussions must not be
            taken lightly and can lead to serious health issues. If you suffered
            a concussion during football practice or during a game, you should
            get medical treatment right away and stay away from the field until
            you completely recover. It is heartbreaking to know now that these
            tragedies could have been prevented."
          </em>
        </p>
        <p>
          <em>
            Sources:
            <br />
            http://edition.cnn.com/2013/01/23/sport/nfl-seau-lawsuit/index.html
            <br />
            http://courtindex.sdcourt.ca.gov/CISPublic/casedetail?casenum=201300031265&casesite=SD&applcode=C
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
