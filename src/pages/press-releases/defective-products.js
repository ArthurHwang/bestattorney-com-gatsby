// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Attorneys - Defective Product Press Releases"
        pageDescription="Product defect press releases. If you have been injured as a result of a defective product, call 949-203-3814 to speak with a Bisnar Chase attorney and receive a free no obligation case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Defective Product Press Releases</h1>
        <BreadCrumbs location={location} />

        <ul>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-no-win-no-fee">
              Bisnar Chase Vaginal Mesh Lawyers Offer No Win No Fee Guarantee
              For Victims Throughout the United States
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/johnson-johnson-vaginal-mesh-discontinued">
              Johnson & Johnson Stops Selling Vaginal Mesh Implants Following
              Multiple Defective Product Lawsuits
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices">
              New Studies Link Yaz to Irritable Bowel Syndrome
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/infuse-graft-lawsuits">
              Medtronic Infuse Bone Graft Questioned As Allegations Unfold
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-lawsuits">
              Attorneys Move Forward With Vaginal Mesh Lawsuits
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/johnson-johnson-depuy-hip-implant">
              Bisnar Chase Sues Johnson & Johnson for Prosthetic Hip Implant
              Defects
            </Link>
          </li>
        </ul>
        <h2>More Releases</h2>
        <div classname="col3flt">
          <p>
            {" "}
            <Link to="/press-releases">All</Link>
            <br />{" "}
            <Link to="/press-releases/community-outreach">
              Community Outreach
            </Link>
            <br />{" "}
            <Link to="/medical-devices/hip-implants">Prosthetic Hip</Link>
            <br />{" "}
            <Link to="/press-releases/defective-products">
              Defective Products
            </Link>
          </p>
        </div>
        <div classname="col3flt">
          <p>
            {" "}
            <Link to="/press-releases/automakers-liability">Auto Accident</Link>
            <br /> <Link to="/motorcycle-accidents">Motorcycle Accident</Link>
            <br />{" "}
            <Link to="/press-releases/firm-accomplishments">
              Accomplishments
            </Link>
          </p>
        </div>
        <div classname="col3flt">
          <p>
            {" "}
            <Link to="/dui">Drunk Driving</Link>
            <br />{" "}
            <Link to="/medical-devices/vaginal-mesh-press-releases">
              Vaginal Mesh
            </Link>
            <br />{" "}
            <Link to="/press-releases/defective-products">Auto Defect</Link>
            <br /> <Link to="/press-releases">Miscellaneous</Link>
          </p>
        </div>
        <div classname="clear"></div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
