// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Christopher Weigl, Boston UV student illed in a bicycle accident"
        pageDescription="Christopher Weigl, a 23-year-old Boston University photojournalism student, was killed in a bicycle accident after a tractor-trailer making a wide turn struck him as he rode along the designated lane, a Dec. 6 news article in The Boston Globe reports."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Bicycle Accident Lawyers Bisnar Chase React to Death of
          Student
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Christopher Weigl, a 23-year-old Boston University photojournalism
            student, was killed in a bicycle accident after a tractor-trailer
            making a wide turn struck him as he rode along the designated lane,
            a Dec. 6 news article in The Boston Globe reports. The article
            states that Weigl wrote his own obituary for a class three months
            before his fatal accident and that his death is drawing attention to
            the dangers faced by bicyclists on Boston roadways.
          </strong>
        </p>
        <p>
          <img
            src="/images/christopher-weigl.jpg"
            alt="Christopher Weigl Bicycle Accident"
            className="imgright-fixed"
          />
          Christopher Weigl, 23, was the fifth bicyclist to be killed in Boston
          this year after a tractor-trailer making a turn struck him at a street
          intersection, according to a Dec. 6 news report in The Boston Globe.
          The article states that that the{" "}
          <Link
            to="/bicycle-accidents"
            title="bicycle accident"
            onClick="linkClick(this.to)"
          >
            bicycle accident
          </Link>{" "}
          occurred when Weigl was riding his bike in a designated lane on
          Commonwealth Avenue the morning of Dec. 6 when a large truck making a
          turn on to Saint Paul Street hit him. Weigl wrote a self-obituary just
          three months before his death as part of a class exercise, the report
          states.
        </p>
        <p>
          In November, another Boston University student, Chung-Wei "Victor"
          Yang was killed when he came into contact with an MBTA bus while
          riding through the intersection of Brighton and Harvard avenues, the
          Globe reports. Weigl was riding on a main thoroughfare where a painted
          lane has been established for bicyclists as part of a safety campaign
          to encourage bike riding in the city, the article states.
        </p>
        <p>
          According to the National Highway Traffic Safety Administration's
          (NHTSA) recent report, the number of bicyclist fatalities nationwide
          in 2010 was 2 percent lower than the 628 bicyclist fatalities reported
          in 2009. However, in 2010, 618 bicyclists were killed and 52,000 were
          injured in traffic accidents across the country. Bicycle accidents
          accounted for 2 percent of all motor vehicle fatalities in the United
          States.
        </p>
        <p>
          Although a number of cities across the United States are trying to
          make their roadways safer for bicyclists by designating bike lanes,
          much more remains to be done, said John Bisnar, founder of the Bisnar
          Chase personal injury law firm. "There are still a number of roadways
          that are extremely dangerous for bicyclists because of traffic flow
          and visibility issues."
        </p>
        <p>
          Bisnar says, with more and more people adopting the bicycling
          lifestyle to save money and conserve the environment, cities must
          launch safety campaigns to educate both cyclists and drivers.
          "Motorists must realize and must be constantly reminded that they
          share the roadway with other vehicles including bicycles. It is also
          important to remember that bicyclists have the same rights and
          responsibilities and drivers of other vehicles."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California bicycle accident lawyers at Bisnar Chase represent
          families or victims of bicycle accidents. The firm has been featured
          on a number of popular media outlets including Newsweek, Fox, NBC, and
          ABC and is known for its passionate pursuit of results for their
          clients. Since 1978, Bisnar Chase has recovered millions of dollars
          for victims of serious personal injuries and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
