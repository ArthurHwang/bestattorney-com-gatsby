// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Can Social Media Sabotage Your Auto Defect Lawsuit? Recent Court Order to Produce Private Facebook Content May Have Cost Alleged Victim $61,000,000.00"
        pageDescription="Social media sites may sabotage your personal injury and auto defect lawsuits. This woman may have lost million as a result of posting misleading pictures. Have you been injured in an accident? Call 949-203-3814 for a free case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Can Social Media Sabotage Your Auto Defect Lawsuit?</h1>
        <BreadCrumbs location={location} />
        <p>
          The effects of{" "}
          <Link to="/press-releases/facebook-social-media-warning">
            social media
          </Link>{" "}
          have been felt world-wide by almost every person on the planet. Many
          of those who utilize the numerous social sites available are rewarded
          with a level of connectivity that has never before been achievable,
          but for one woman in Nevada, her use of social media sites may have
          cost her $61,000,000.00.
        </p>
        <p>
          According to court documents recently filed in Nevada, a woman
          claiming to have suffered serious and life-threatening injuries as a
          result of defective auto parts has been ordered to "produce complete
          and un-redacted copies of Facebook and other social networking site
          accounts."
        </p>
        <p>
          The Plaintiffs claim within court documents that on April 27, 2007,
          "massive, life-threatening, permanent, and irreversible injuries" were
          sustained as result of an auto collision. The Plaintiffs allege that
          the Defendant was engaged in the business of manufacturing and
          distributing seatbelt systems, including the one that had been
          installed in the Plaintiff's vehicle at the time of her accident, and
          that the seatbelt system installed was defective when it left the
          Defendant's manufacturing facility.
        </p>
        <p>
          In addition, the Plaintiffs are pursuing action against the airbag
          manufacturers for "negligence, gross negligence, negligence, per se,
          and product defect. Id." The plaintiffs seek to recover over
          $61,000,000.00.
        </p>
        <p>
          Following these accusations, one of the defendants claims within court
          documents to have obtained wall posts and photographs from one of the
          Plaintiff's public Facebook accounts. The Defendant alleges that the
          Facebook posts showed the Plaintiff's "ability to swing on a swing
          set, dance, and engage in water sports." The Defendant also claims to
          have had access to pictures displaying the Plaintiff participating in
          social activities including "consumption of alcohol, bowling with
          friends, and late night partying."
        </p>
        <p>
          The Defendant claims that the Plaintiff changed her Facebook profile
          privacy setting in February of 2011, which blocked public profile
          viewing from thereon out.
        </p>
        <p>
          The Defendant has issued a motion to Compel the Plaintiff to produce
          her social media accounts for review and the courts have granted their
          approval.
        </p>
        <p>
          Brian Chase, auto defect lawyer, has been assisting victims of
          defective auto parts for decades and stresses the dangers of social
          media content to all of his clients. "The fact that auto manufacturers
          have produced defective products that have killed and maimed drivers
          cannot be disputed. A picture says a thousand words, but pictures can
          also be very misleading. Whether or not the Defendants in this
          particular case were responsible for the Plaintiff's injuries has yet
          to be determined, but it would be a shame if the outcome of her case
          was based on circumstantial evidence."
        </p>
        <p>
          "Our clients are informed at the outset about the dangers of utilizing
          social media and we strongly suggest that they shut down their sites
          during the pendency of their lawsuit. Even a paralyzed victim will
          have a night out, but there are potential pitfalls in documenting good
          times for victims pursuing damages from defective product and auto
          manufacturers. A skilled defense attorney can characterize an innocent
          photo of friends out to dinner on a social media site into a drunken
          dance party at an all night club to the detriment of a seriously
          injured accident victim," Says Mr. Chase.
        </p>
        <h2>About Bisnar Chase Defective Product Attorneys</h2>
        <p>
          The attorneys at Bisnar Chase represent people who have suffered
          catastrophic injuries or have lost a family member as a result of
          defective products, auto collisions, nursing home abuse and dog
          attacks. For over 30 years, the Bisnar Chase law firm has consistently
          provided top-notch legal representation on a "no win - no fee" basis
          and have won dozens of million dollar claims against Fortune 500
          companies, governmental agencies and insurance companies.
        </p>
        <p>
          For more information about the Bisnar Chase law firm, call
          949-203-3814 or visit their website at /.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
