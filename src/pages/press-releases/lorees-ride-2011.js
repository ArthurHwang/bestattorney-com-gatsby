// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Mother Set to Conquer 2011 AIDS/Lifecycle Cycling Journey"
        pageDescription="Call 949-203-3814 for Highest-rated Auto Defects Attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California. No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Mission Viejo Mother Set to Conquer California's Coast in the 2011
          AIDS/Lifecycle Cycling Journey
        </h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/Loree-on-the-ride-2010.jpg"
          alt="Lorees Ride"
          className="imgright-fixed"
        />
        <p>Newport Beach, CA, May 19, 2011</p>
        <p>
          For the fourth year in a row, California personal injury attorneys of
          Bisnar Chase Personal Injury Attorneys are sponsoring Loree Glenn in
          her effort to raise funds for AIDS/HIV awareness and research by
          bicycling 545 miles from San Francisco to Los Angeles on June
          5-11,2011 in the inspiring AIDS/Lifecycle event.
        </p>
        <h2>Loree's Ride</h2>
        <p>
          Loree Glenn will peddle vigorously through some of California's most
          scenic countryside in her endeavor to help raise awareness and funds
          for AIDS research, public knowledge campaigns and medical treatments.
        </p>
        <p>
          The strenuous seven-day ride will challenge Loree physically and
          mentally. Determination, Loree says, is the key to helping her tackle
          40 to 106 mile bike rides each day. With the love and support of her
          family, she has completed this monumental journey for the last three
          years and triumphed over physical injuries and exhaustion.
        </p>
        <p>
          Throughout the years, Loree Glenn has become an inspiration to the
          Bisnar Chase Personal Injury Attorneys staff. Her motivation and
          endurance to power through miles upon miles of treacherous asphalt
          have made the law firm enthusiastic to sponsor her ride for the fourth
          consecutive year.
        </p>
        <p>
          As part of the sponsorship, Bisnar Chase Personal Injury Attorneys is
          providing a monetary donation for the AIDS/Lifecycle event. All
          proceeds go directly to supporting HIV/AIDS services of the L.A. Gay &
          Lesbian Center and San Francisco AIDS Foundation.
        </p>
        <h2>Why Loree Rides: AIDS is Not Over</h2>
        <p>
          According to{" "}
          <Link to="http://www.aidslifecycle.org/">Aidslifecycle.org</Link>, in
          California alone 151,000 people are living with HIV and it costs them
          an estimated $20,000 per a year for medical care and treatments.
          Because of this, AIDS/LifeCycle has made it their mission to provide
          critical services to people living with disease.
        </p>
        <p>
          Aidslifecycle.org states, "as the number of people living with HIV
          increases, so does the need for the services provided by the San
          Francisco AIDS Foundation and the L.A. Gay & Lesbian Center, the two
          agencies producing this event."
        </p>
        <p>
          Loree Glenn along with Bisnar Chase Personal Injury Attorneys
          encourage people to support the cause through donations on{" "}
          <Link to="http://www.tofighthiv.org/site/TR?px=1175708&fr_id=1330&pg=personal">
            Loree's personal Aids Life Cycle webpage
          </Link>{" "}
          and join the fight against this disease.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys is a Newport Beach, California
          personal injury law firm that represents people who have been very
          seriously injured or lost a family member due to an accident,
          defective product or negligence. The law firm has a history of
          supporting events for the benefit of the people of California such as
          Mothers Against Drunk Driving's Walk Like MADD event, Second Harvest
          Food bank's Mobile Food Pantry, Avon's Walk for Breast Cancer as well
          as many other philanthropic events. For a more inclusive list of
          philanthropic activities visit Bisnar Chase Personal Injury
          Attorneys's Giving Spotlight webpage.
        </p>
        <p>
          {" "}
          <Link to="/press-releases/community-outreach">
            Click Here For More Community Outreach Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
