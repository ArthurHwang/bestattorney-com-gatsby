// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Community Outreach Press Releases - Bisnar Chase Law Firm - Helping Hands"
        pageDescription="Community Outreach press releases. Have an idea for how we can help out? Use our contact form to send us your ideas and our staff may take you up on it."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Community Outreach Press Releases</h1>
        <BreadCrumbs location={location} />

        <ul>
          <li>
            {" "}
            <Link to="/giving-back/food-bank-july-2012">
              Bisnar Chase Food Giveaway July 2012
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/giving-back/food-bank-july-2012">
              Bisnar Chase Food Giveaway 2012
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/giving-back/alzheimers-walk">
              Bisnar Chase Assists Walk to End Alzheimer's
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/car-accidents/rental-car-safety">Rental Car Safety</Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/">
              Viral Video Contest Winner Announced
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/lorees-ride-2011">
              AIDS Lifecycle 2011
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/viral-video-contest-regional-winner">
              Bisnar Chase Viral Video Content Spreads Word About Distracted
              Driving
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/revlon-walk-2011">
              Bisnar Chase Supports Revlon Walk
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/rental-car-safety-2011">
              Brian Chase Speaks about Rental Car Safety
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/second-harvest-food-bank-2011">
              Bisnar Chase Teams Up With Second Harvest Food Bank
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/hit-and-run-reward">
              Bisnar Chase Offers Reward for Information Regarding Hit-and-Run
              Driver
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases/adopt-a-family-program">
              Bisnar Chase Adopt-A-Family 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnar-chase/pumpkin-contest/prweb4669914.htm">
              Jack-O-Lakers Contest Launched
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/john-bisnar/viral-video-scholarship/prweb4656144.htm">
              Viral Video Scholarship Accepting Admissions
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/safe-driving/viral-video-contest/prweb4518884.htm">
              Viral Video Scholarship Launch
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnar-chase/breast-cancer-denim-day/prweb4309474.htm">
              Bisnar Chase Fights Breast Cancer
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnar-chase-sponsors/american-legion-surf-fest/prweb4213314.htm">
              Bisnar Chase Sponsors Surf Fest 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2010/06/prweb4096334.htm">
              Loree Glenn AIDS Lifecycle 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnar-chase-sponsors/AIDS-life-cycle-ride/prweb4205444.htm">
              Lifecycle AIDS 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnarchase/kristiesfoundation/prweb4159514.htm">
              Kristie's Foundation Support
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2010/06/prweb4096334.htm">
              Lifecycle AIDS 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2010/05/prweb4037704.htm">
              $13,000.00 Raised for MADD
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2010/03/prweb3821324.htm">
              Walk Like MADD 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/bisnar-chase/AIDS-Lifecycle-Ride/prweb3810554.htm">
              AIDS Lifecycle 2010
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/hit_and_run/car_accidents/prweb3806904.htm">
              Hit and Run Reward Program Launch
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/09/prweb2905534.htm">
              Boys to Men Training Event
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/07/prweb2662544.htm">
              Boys to Men Raises $125,000.00
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/07/prweb2640064.htm">
              OC Super Fair 2009
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/07/prweb2627504.htm">
              Rendezvous Surf Fest 2009
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/06/prweb2611194.htm">
              OC Fair 5K Run 2009
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prweb.com/releases/2009/06/prweb2566564.htm">
              AIDS Lifecycle Trek 2009
            </Link>
          </li>
        </ul>
        <h2>More Releases</h2>
        <div className="col3flt">
          <p>
            {" "}
            <Link to="/press-releases">All</Link>
            <br />{" "}
            <Link to="/press-releases/community-outreach">
              Community Outreach
            </Link>
            <br />{" "}
            <Link to="/press-releases/defective-products">
              Defective Products
            </Link>
          </p>
        </div>
        <div className="col3flt">
          <p>
            {" "}
            <Link to="/press-releases/firm-accomplishments">
              Accomplishments
            </Link>
          </p>
        </div>
        <div className="col3flt">
          <p>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-press-releases">
              Vaginal Mesh
            </Link>
            <br />
          </p>
        </div>
        <div className="clear"></div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
