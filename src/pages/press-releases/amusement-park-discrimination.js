// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Family Sues Disneyland Employee For Discrimination"
        pageDescription="Family Files Civil Rights Lawsuit For Amusement Park Discrimination After Disneyland's White Rabbit Refused to Hug Black Children"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Family Sues Disneyland For Discrimination</h1>
        <BreadCrumbs location={location} />

        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10461342.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          An African American family has filed a civil lawsuit (Case Number:
          30-2013-00630743-CU-CR-CJC, Orange County Superior Court.) against a
          Disneyland employee and the amusement park after the man dressed as
          the White Rabbit character from "Alice in Wonderland" allegedly
          refused to hug black children during their visit last summer.
        </p>
        <img
          src="/images/disneyland-civil-rights-lawsuit.jpg"
          width="186"
          height="249"
          alt="hip replacements"
          className="imgright-fixed"
        />
        <p>
          According to a Feb. 20 news report in The Orange County Register, six
          extended family members, three children and three adults, are named as
          plaintiffs in the amusement park lawsuit. The Register reports that
          the adult family members said they sued because Disney refused to take
          meaningful action in response to their complaints.
        </p>
        <p>
          Disney officials responded by saying that the lawsuit is without
          merit, that they do not tolerate discrimination of any kind, and that
          they plan to vigorously contest the lawsuit.
        </p>
        <p>
          According to the civil complaint (Case Number:
          30-2013-00630743-CU-CR-CJC, Orange County Superior Court.), two boys,
          ages 6 and 9, spotted the rabbit during their Aug. 11 trip to
          Disneyland. One of the boys and his 6-year-old cousin went up to the
          rabbit and tried to hold its hand, but instead of hugging the children
          as characters at the park often do, the White Rabbit allegedly shook
          his paw free of the younger child's grip and stepped away, the lawsuit
          states. The rabbit, according to the complaint, similarly rejected two
          other children of the same family.
        </p>
        <p>
          The character then turned to children of other races who had formed a
          line behind this African American family and gave them hugs and
          kisses, the plaintiffs alleged. One of the children's father told the
          Register that their feelings were hurt by the Disney employee's
          actions. They demanded that Disneyland publicly apologize for the
          incident, but instead each set of parents and children were offered
          $500 in tickets.
        </p>
        <p>
          'The family should be commended for not taking the settlement, which
          would have prevented this incident from being made public," said John
          Bisnar, founder of the Bisnar Chase personal injury law firm.
          "Although we live in a diverse community here in Orange County, there
          are still incidents where racism and discrimination can surface and it
          is very sad and unfortunate."
        </p>
        <p>
          "It is important that those who feel they are victims of{" "}
          <Link to="/orange-county/amusement-park-accident-lawyer">
            discrimination
          </Link>
          , whether in the workplace or in a public place or a popular
          destination, fight for their rights", Bisnar said. "It is crucial to
          shed the light on these types of incidents so they are never repeated.
          Being discriminated against based on one's race, skin color, gender,
          nationality, religion or sexual orientation can be devastating and
          humiliating. When it happens in a public place to innocent, young
          children, it can be extremely hurtful for everyone involved."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          http://www.ocregister.com/news/family-496598-disney-rabbit.html
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
