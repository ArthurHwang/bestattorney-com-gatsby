// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="The O.C. Names Bisnar Chase Personal Injury Lawyers Best Places to Work"
        pageDescription="The personal injury law firm of Bisnar Chase has been named among best places to work in Orange County for a second year."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase Personal Injury Lawyers Named <br />
          Best Places to Work in Orange County
        </h1>
        <BreadCrumbs location={location} />

        <i>
          <p>
            Attribution: Aug 2013 -- This article is the syndication source of a
            recently released{" "}
            <Link
              to="http://www.prweb.com/releases/2013/8/prweb11014831.htm"
              target="_blank"
            >
              press release
            </Link>{" "}
            from Bisnar Chase
          </p>
        </i>
        <h3>
          The Orange County personal injury law firm has been listed as one of
          the best employers by the Orange County Business Journal for the
          second year in a row.
        </h3>
        <p>
          <b>Bisnar Chase</b> is one of 32 small companies countywide to be
          recognized in the Orange County Business Journal's Best Places to Work
          for 2013.
        </p>
        <p>
          For the second year in a row, the{" "}
          <Link to="/">California personal injury law firm</Link> of Bisnar
          Chase has been named as one of the best places to work by the Orange
          County Business Journal.
        </p>
        <p>
          According to the magazine's website, each year, Orange County's most
          exceptional employers are featured in the its Best Places to Work
          Special Report.
        </p>
        <p>
          The Orange County Business Journal's website states that the
          businesses who have made it to this prestigious list excel in the
          finest treatment, recruitment and retention of their employees.
        </p>

        <p>
          The Aug. 4 issue where the list was published also explains the
          methodology that was used to arrive at this list. Companies that
          aspire to be on this list are evaluated, scored and ranked based on
          their workplace policies, practices, philosophies and systems.
        </p>

        <p>
          According to OCBJ, an important part of the selection process is a
          detailed employee survey that asks a company's associates about the
          quality of their workplace including leadership and planning,
          corporate culture, communications, job satisfaction, work environment,
          salary and benefits and training opportunities.
        </p>
        <p>
          An 80 percent response from employees on questionnaires is required in
          order to be considered for the list.
        </p>
        <p>
          {" "}
          <Link to="/attorneys/john-bisnar">John Bisnar</Link>, founder and
          senior partner of Bisnar Chase, says being on this list is an honor
          and a matter of pride for all of the law firm's associates.
        </p>
        <p>
          <i>
            "This award is a tribute to the talents and efforts of our
            administrator Shannon Barker and our team leaders Ricardo Hernandez,
            Marta De La Torre and Ned Spilsbury. We are very proud of all our
            associates and the superior legal representation and customer
            service we provide for our clients."
          </i>
        </p>

        <p>
          Bisnar said that the recognition is especially meaningful because it
          comes from two reputed organizations that are also industry leaders in
          the Orange County Business Journal and the Best Companies Group.
        </p>

        <p>
          <i>
            "We highly value our workplace and the positive environment we have
            created for ourselves. The credit for this recognition goes to every
            member of our team who works hard to create and maintain a fun
            atmosphere while maintaining a high degree of professionalism. It
            can be a tough balance to maintain at times, but our associates make
            it seem so easy."
          </i>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
