// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laws Must Protect Motorists from Rental Car Safety Problems"
        pageDescription="Southern California rental car safety provided by Orange County car accident lawyers.  Call 949-203-3814 for expert advice."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          {" "}
          <Link to="/press-releases/rental-car-safety-2011">
            Bisnar Chase Personal Injury Attorneys: Laws Must Protect Motorists
            from Rental Car Safety Problems
          </Link>
        </h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/brian_new.jpg"
          alt="Bisnar Chase Personal Injury Attorneys"
          className="imgright-fixed"
        />
        <p>
          The California Personal Injury Attorneys of Bisnar Chase Personal
          Injury Attorneys (/) today announced their support of laws designed to
          protect motorists from rental car safety problems, citing recent
          rental car safety legislation introduced by state lawmakers here in
          California and by federal regulators in Washington.
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/3/prweb8236541.htm">
            PRWEB
          </Link>
          ) March 24, 2011
        </p>
        <p>
          Due to a frightening rental car safety loophole, current law allows
          rental car companies to rent cars that have been recalled and not
          repaired, even though it's illegal for dealers to sell those same
          cars.
        </p>
        <ul>
          <li>
            At a news conference in Sacramento yesterday, Assemblyman Bill
            Monning introduced a measure that would prevent cars that are
            subject to recalls from being rented until repairs are made to fix
            the problems.
          </li>
          <li>
            On March 2, 2011, U.S. Senator Charles Schumer introduced new
            legislation that would prevent rental car agencies from renting
            vehicles that are under a safety recall before repairs are made.
          </li>
        </ul>
        <p>
          "It's absolutely mind blowing that current law allows rental car
          agencies to rent recalled vehicles to unwitting motorists without
          repairs having been made," said Brian Chase, partner at Bisnar Chase
          Personal Injury Attorneys. "Any vehicle that is subject to a recall,
          be it in a dealer showroom or, in this case, on a rental car lot,
          absolutely must be repaired before it's placed in the hands of
          consumers. It's a no-brainer. We support any legislation that ties up
          this ridiculous loophole to protect the motoring public."
        </p>
        <p>
          The measures introduced by Monning and Schumer stem from a 2004
          incident in which two sisters, Raechel and Jacqueline Houck of Santa
          Cruz, California, died in a fiery crash while driving a PT Cruiser
          they rented from Enterprise Rent-A-Car (Houck v. Enterprise
          Rent-A-Car, No. HG052220018, Alameda County, California Superior
          Court, Judgment 6/9/10). The PT Cruiser had been recalled due to a
          defective power steering hose that was prone to leaking and catching
          on fire, but it was not repaired before being rented to the Houck
          sisters.
        </p>
        <p>
          According to recent news reports*, the National Highway Traffic Safety
          Administration (NHTSA) surveyed three major car rental companies -
          Hertz, Enterprise (owner of National and Alamo), and Avis/Budget. The
          survey found that from June 2006 through July 2010, all three
          companies allowed customers to rent cars that had been recalled and
          not repaired.
        </p>
        <p>
          The rental car companies blasted back, saying they determine which
          recalls are the most serious and have a very good track record of
          getting those serious problems fixed.
        </p>
        <p>
          "My experience has taught me to know better than to let a rental car
          company decide which repairs should be made and which ones shouldn't
          and which cars are safe to rent and which ones aren't," said Chase.
          "When given the choice, they may put profit over safety."
        </p>
        <p>
          Chase goes on to say that many of the vehicles first arriving at
          rental car companies often lack safety advancements from the get-go.
        </p>
        <p>
          "I've seen many instances where rental car companies purchase cars
          from dealers without critical safety options, such as electronic
          stability control or side curtain airbags," Chase continued. "They
          omit safety advances to save a buck. Along those lines, it makes no
          sense whatsoever, then, to allow rental car companies the option to
          choose which vehicles should be repaired before being rented, when
          they're subject to a recall. Makes no sense at all."
        </p>
        <p>
          Chase said his firm is familiar with rental car safety problems,
          having been involved in more than half a dozen lawsuits against
          Enterprise, with many of those lawsuits claiming Enterprise knowingly
          rented dangerous 15-passenger vans to consumers who were unaware of
          safety problems. Chase says that when it comes to renting a car from
          any agency, it's important for consumers to ask questions.
        </p>
        <p>
          "Inquire about the car you're renting and whether or not it's subject
          to a safety recall. If it is, rent something else. If the rental agent
          refuses to answer your questions, go somewhere else."
        </p>
        <p>
          {" "}
          <Link to="http://abcnews.go.com/Blotter/schumer-demands-fix-dangerous-rental-car-loophole/story?id=13030589&page=1">
            *Lee Ferran, Schumer Demands Fix to 'Dangerous' Rental Car Loophole,
            March 2, 2011.
          </Link>
        </p>
        <p>
          {" "}
          <Link to="http://www.santacruzsentinel.com/ci_17676460">
            *Jondi Gumz, Assemblyman Monning to Introduce Rental Car Safety
            Bill, March 23, 2011.
          </Link>
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people who have
          been very seriously injured or lost a family member due to an
          accident, defective product, negligence or unsafe rental car
          throughout the country from their Newport Beach, California
          headquarters. The auto defects law firm has won a wide variety of auto
          defect cases against most of the major auto manufacturers, including
          Ford, General Motors, Toyota, Nissan and Chrysler, manufacturer of the
          PT Cruiser. Brian Chase is the author of the most up-to-date and
          comprehensive auto defect book available today, Still Unsafe at Any
          Speed: Auto Defects that Cause Wrongful Deaths and Catastrophic
          Injuries. For more information, visit Mr. Chase's blog at
          http://www.ProductDefectNews.com.
        </p>
        <p>
          {" "}
          <Link to="/press-releases/community-outreach">
            Click Here For More Community Outreach Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
