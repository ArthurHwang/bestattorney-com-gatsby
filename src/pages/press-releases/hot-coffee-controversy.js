// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Personal Injury Lawyers - Hot Coffee Stirs Controversy in Law Community"
        pageDescription="Hot Coffee spills can be dangerous! Call 949-203-3814 for top-rated personal injury lawyers who faithfully serve all of Southern Califoria."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>"Hot Coffee" Stirs Controversy in the Law Community</h1>
        <BreadCrumbs location={location} />
        <p>
          The 88-minute documentary "Hot Coffee" is broken up into four
          different and distinct chapters which show the manner in which
          government officials are trying to limit the power of the public when
          it comes to class action lawsuits. It also shows how some people over
          the years have taken advantage of the system with "jackpot justice,"
          but overall does a terrific job of standing up for the people.
        </p>
        <h2>Hot Coffee at McDonalds</h2>
        <p>
          The beginning of the film goes over a case where Stella Liebeck, an
          81-year-old woman, bought a cup of coffee from McDonalds and while
          sitting in the passenger seat with the cup of coffee between her legs,
          spilled the drink and caused devastating burns to her inner thighs.
          The coffee was ridiculously hot and Liebeck never truly recovered.
        </p>
        <p>
          Even though she never recovered the full amount that the jury awarded
          her, $2.9 million, due to caps on punitive damages, people like to
          point to this case as the jumping off point of frivolous lawsuits in
          America. Liebeck was mocked endlessly and the facts of her case were
          skewed into oblivion.
        </p>
        <p>
          The fact of the matter was that Liebeck was hurt and needed more money
          for her medical bills, but due to regulations and tort reform backed
          by "big business" lobbyists, she only received a portion of what she
          was owed. McDonalds never learned their lesson.
        </p>
        <h2>Jamie Leigh Jones vs. KBR and Halliburton</h2>
        <p>
          Jamie Leigh Jones was an impressionable 19-year-old girl. Not knowing
          where her life would take her Jones decided to take a job at KBR, a
          subsidiary of Halliburton at the time, and was sent to Iraq. She was
          told she would be safe, she was told she would have her own living
          quarters, and that she had nothing to worry about.
        </p>
        <p>
          What actually happened was that she was placed into a living situation
          where she was entrenched with 400 men. She was then drugged and
          brutally raped by several of those very men. Afterwards, she was
          treated with a rape kit which was turned over to KBR, parts of which
          subsequently disappeared, and was locked in a storage trailer behind
          armed guards while she begged and pleaded to be released. She was
          imprisoned.
        </p>
        <p>
          Due to a forced arbitration clause in her employment contract with KBR
          she couldn't have her day in court to recover economic and
          non-economic damages. She was forced to settle for a confidential
          amount behind closed doors where nobody would hear of her story. This
          is not a frivolous lawsuit and Jones' story led Senator Al Franken,
          MN, to pass an amendment against forced arbitration.
        </p>
        <h2>Tort Reform and Judge Diaz</h2>
        <p>
          Do you know what the US Chamber of Commerce is? Did you know that it's
          not actually part of the government? Do you have any idea what they're
          doing and who they're manipulating? Well, Mississippi resident and
          former Mississippi Supreme Court Justice Oliver Diaz does.
        </p>
        <p>
          The <Link to="http://www.uschamber.com/">US Chamber of Commerce</Link>{" "}
          tried tirelessly to get Diaz off of his seat in the Supreme Court by
          funding a "big business" candidate who was running against him. People
          like Karl Rove began this group to try to put business-favorable
          judges in high seats all across the country so that they'd rule on the
          side of business rather than the consumer.
        </p>
        <p>
          {" "}
          <Link to="https://en.wikipedia.org/wiki/Tort_reform">
            Tort reform
          </Link>{" "}
          is the main ideal behind these changes. Business lobbyists and
          corporate politicians side with businesses for a big paycheck and let
          the general public suffer. They want to limit your ability to protect
          yourself against large companies and limit medical malpractice suits
          to protect doctors and insurance companies.
        </p>
        <h2>Is Any of This Right?</h2>
        <p>
          Of course frivolous lawsuits are wrong, but class action lawsuits are
          what keep our children's toys safe, it's what keeps lead out of our
          paint, it's the only means we have to fight back against big business
          who favor "profits over people." There are hundreds of examples of
          U.S. and foreign corporations selling American consumers products that
          the corporations know will injure or kill a number of consumers.
        </p>
        <p>
          There are hundreds of examples of U.S. and foreign corporations taking
          significant advantage of American consumers but in small ways. Ways
          that we might not notice. Ways that without the ability for all of us
          to ban together to fight back as a class, we would have no chance.
        </p>
        <p>
          Take the little rip-offs that cost us each a few bucks a month and add
          up to millions if not billions of dollars of profits for the
          corporation. No one is going to fight Bank of America in court on
          their own over a two buck rip-off a month for a year. Bank of American
          made millions off of small over charges to their account holders.
          Without class action lawsuits, how could we stop them? We certainly
          can't count on our big business, bought and paid for government to
          protect us. The foxes are watching the hen house.
        </p>
        <p>
          Big business is using their superior economic strength and PR firms to
          push "tort reform" on the American public in order to protect
          themselves from being held accountable. They are selling "tort reform"
          as something good for the public. Do you buy that?
        </p>
        <p>
          Who do you trust more to protect you from corporate wrongdoing,
          politicians making "tort reform" laws "for your benefit" or a jury of
          your peers? We let juries decide if someone should be convicted of a
          crime and sentenced to death, but we can't trust a jury to decide an
          economic dispute in a personal injury case? That makes no sense.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
