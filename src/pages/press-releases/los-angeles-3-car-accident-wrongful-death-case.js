// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Complaint for wrongful death arising from dangerous conditions"
        pageDescription="Summary of case: wrongful death case of a three car collision involving decedent kimberly Anne Downes"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Personal Injury Legal Cases</h1>
        <BreadCrumbs location={location} />
        <h2>Three Car Collision Results in Wrongful Death of a Young Woman</h2>
        <p>
          James R. Downes, and Myong Sun Kim, Individually and as Successors in
          Interest of Kimberly Anne Downes, decedent,
        </p>
        <p>Plaintiffs</p>
        <p>V.</p>
        <p>State of California: California Department of Transportation</p>
        <p>
          (CalTrans); Holly Ann Lee; Gaylene Lee; Victoria Vasquez; Miguel
          Alvarez, and Does 1-100,
        </p>
        <p>Defendants</p>
        <p>Sup. Ct. L.A. Co. East District (Pomona, CA)</p>
        <p>
          <strong>Case No.: KC056861</strong>
        </p>
        <p align="center">
          <strong>
            COMPLAINT FOR WRONGFUL DEATH DAMAGES ARISING FROM DANGEROUS
            CONDITION OF PUBLIC PROPERTY (Government Code sec. 835);{" "}
            <Link to="/wrongful-death">WRONGFUL DEATH</Link> DAMAGES FOR
            NEGLIGENT OWNERSHIP AND OPERATION OF A MOTOR VEHICLE
          </strong>
        </p>
        <p>DEMAND FOR JURY TRIAL</p>
        <p>
          COME NOW, Plaintiffs James R. Downes, and Myong Sun Kim, Individually
          and as Successors in Interest of Kimberly Anne Downes, decedent, as
          and for their causes of action against State of California: California
          Department of Transportation (CalTrans); Holly Ann Lee; Gaylene Lee;
          Victoria Vasquez; Miguel Alvarez, and Does 1-100 inclusive, allege as
          follows:
        </p>
        <p>
          <b>
            <u>ALLEGATIONS COMMON TO ALL CAUSES OF ACTION</u>
          </b>
        </p>
        <p>
          1.) At all relevant times herein mentioned, Plaintiff JAMES R. DOWNES
          was and is the father of KIMBERLY ANNE DOWNES, deceased, and is a
          lawful heir of the estate of said KIMBERLY ANNE DOWNES.
        </p>
        <p>
          2.) At all relevant times MYONG SUN KIM was and is the mother of
          KIMBERLY ANNE DOWNES, deceased, and is a lawful heir of the estate of
          said KIMBERLY ANNE DOWNES.
        </p>
        <p>
          3.) Decedent KIMBERLY ANNE DOWNES was 22 years old at the time of her
          death, and died intestate, with JAMES R. DOWNES and MYONG SUN KIM
          surviving her as the only heirs of said decedent, and they therefore
          are entitled to bring this action pursuant to California Code of Civil
          Procedure sec. 377.60 (b).
        </p>
        <p>
          4.) At all relevant times, upon information and belief, SR-60, also
          known as the Pomona Freeway, was and is a roadway owned, operated,
          maintained and controlled by defendants State of California and
          California Department of Transportation, hereinafter "CalTrans".
        </p>
        <p>
          5.) On or about February 13, 2009, plaintiffs' decedent KIMBERLY ANNE
          DOWNES, hereinafter "DOWNES", was lawfully operating her 2002 Hyundai
          Santa Fe within the applicable speed limit, eastbound on SR-60 in the
          city of Diamond Bar at approximately 3:28 a.m. when for unknown
          reasons, her vehicle was caused to swerve to the left, striking the
          center concrete median wall and overturning onto the driver's side in
          the diamond lane, also known as the carpool lane approximately 1564
          feet east of the Grand Ave. overpass.
        </p>
        <p>
          6.) Upon information and belief, within a short time of the aforesaid
          event, defendant HOLLY ANN LEE , hereinafter "LEE", operating a 2007
          Toyota Yaris owned by defendant GAYLENE LEE and traveling eastbound in
          the diamond lane of SR-60 struck the overturned vehicle of plaintiffs'
          decedent, who then and there was outside of her SUV in close proximity
          to the collision of the two vehicles.
        </p>
        <p>
          7.) Upon information and belief, immediately following this collision,
          defendant VICTORIANA VAZQUEZ hereafter, "VAZQUEZ", driving a 2003 Jeep
          Liberty owned by defendant MIGUEL ALVAREZ, struck the person of
          plaintiffs' decedent DOWNES while VAZQUEZ was traveling in the
          eastbound lanes of SR-60, causing massive and fatal injuries to
          DOWNES, resulting in her death.
        </p>
        <p>
          8.) On or about June 25, 2009 a written Notice of Claim was duly
          served upon the defendant State of California Claims Board and a
          formal Rejection of said claim occurred on August 13, 2009.
        </p>
        <p>
          9.) As this incident occurred within the city of Diamond Bar in the
          county of Los Angeles, Pomona judicial district, venue is properly
          brought in this Court pursuant to Government Code sec. 955.2.
        </p>
        <p>
          10.) As a result of the combined negligent acts and omissions of all
          defendants and the dangerous condition of public property of
          defendants State of California and CalTrans as described herein below,
          plaintiffs' decedent died from critical and fatal injuries on February
          13, 2009 and as a legal result thereof, JAMES R. DOWNES and MYONG SUN
          KIM have each suffered economic and non-economic damages for the
          wrongful death of their daughter, in an amount according to proof.
        </p>
        <p>
          <b>
            <u>FIRST CAUSE OF ACTION</u>
          </b>
        </p>
        <p>(Dangerous Condition of Public Property- Against Defendants</p>
        <p>State of California, CalTrans and Does 1-50, Inclusive)</p>
        <p>
          11.) Plaintiffs refer to each of the allegations in paragraphs 1-10,
          above, which are incorporated by reference herein.
        </p>
        <p>
          12.) At all relevant times, upon information and belief, SR-60, also
          known as the Pomona Freeway, was and is a public roadway built,
          constructed, owned, repaired, serviced and maintained by the
          defendants State of California (hereafter "STATE") and CalTrans for
          the use and convenience of the motoring public.
        </p>
        <p>
          13.) Upon information and belief, at a point in time prior to February
          13, 2009, defendants STATE and CalTrans undertook certain traffic
          surveys and scene analysis studies in the immediate vicinity of the
          accident site, and a decision was made to erect approximately four (4)
          double armed high intensity light poles in the center median of SR-60
          immediately east and west of the crash site, just east of the Grand
          Ave. Overpass.
        </p>
        <p>
          14.) Upon information and belief, said decision was based, in whole,
          or in part, was based upon certain line of sight and vision issues
          pertaining to the diamond (HOV) lane of SR-57 northbound as it merged
          into the HOV diamond lane of SR-60 eastbound and crossed under the
          Grand Ave. Overpass.
        </p>
        <p>
          15.) During hours of darkness, as HOV lane traffic transitioned from
          the SR-57 to SR-60 lanes, the prior lack of lighting resulted in a
          dangerous condition of darkness, compounded by the overpass at Grand
          Ave.
        </p>
        <p>
          16.) At all relevant times, it was the duty and legal obligation of
          defendants STATE, CalTrans and Does 1-50, as agents servants and
          employees of each, individually and jointly, to ensure and make
          certain that the SR-60 double armed light poles were in good operating
          condition and that the lights were in fact all activated and turned on
          during the hours of darkness each night and early morning.
        </p>
        <p>
          17.) The defendants STATE and CalTrans, and Does 1-50, inclusive,
          created, and maintained at the time of this accident a dangerous
          condition of public property, in the following respects:
        </p>
        <p>
          a.) Failing to have any of the double armed center median lights east
          of the Grand Ave overpass operating and turned on at the time of this
          incident when it was approximately 3:28 a.m. and dark outside;
        </p>
        <p>
          b.) In causing, allowing and permitting all of the aforesaid lights to
          be in an inoperable condition such that there was no artificial
          illumination of the HOV lane in this location either that night or for
          a long period prior thereto;
        </p>
        <p>
          c.) In failing and neglecting to perform regular and routine
          inspections of the lighting on SR-60 just east of the Grand Ave
          overpass during hours of darkness, so as to ensure and be certain that
          said lights over the center median area were on, and working, and
          properly illuminating the roadway;
        </p>
        <p>
          d.) In failing and neglecting to give adequate warnings of any kind to
          the motoring public as to the outage, or non-working status of the
          lights at the accident locale.
        </p>
        <p>
          18.) As a direct and proximate result of the dangerous condition of
          excessive darkness created by the absence of the overhead lights
          working at the time of this incident, the plaintiffs' decedent, and
          defendants LEE and VASQUEZ were unable to timely or clearly observe
          and notice the series of crash events on February 13, 2009 in
          sufficient time to take appropriate and proper evasive actions to
          avoid causing the fatal injuries to plaintiffs' decedent DOWNES.
        </p>
        <p>
          19.) Upon information and belief, at all relevant times, the
          defendants STATE, Caltrans and Does 1-50, inclusive had actual and/or
          constructive notice for a long period of time prior to February 13,
          2009 that said lights were not properly on and working in t he crash
          site vicinity, and despite said notice, these defendants failed to
          repair or replace said lights and fixtures, and further failed to take
          reasonable steps to warn the motoring public of the resulting
          dangerous condition of public property.
        </p>
        <p>
          20.) As a direct and legal cause of said dangerous condition of public
          property, for which these defendants are liable, Plaintiffs have been
          permanently deprived of the love, companionship, affection, solace,
          future guidance and future services and support of the decedent
          DOWNES, all to their damages in a sum that exceeds the jurisdiction of
          all lower courts.
        </p>
        <p>
          21) Accordingly, Plaintiffs are entitled to wrongful death damages
          from these defendants.
        </p>
        <p>
          <b>
            <u>
              SECOND CAUSE OF ACTION AGAINST LEE AND GAYLENE LEE, and DOES 51-75
            </u>
          </b>
        </p>
        <p>
          22.) Plaintiffs refer to each of the allegations in paragraphs 1-21,
          above, which are incorporated by reference herein.
        </p>
        <p>
          23.) At all relevant times defendant GAYLENE LEE and DOES 51-75, was
          and is the registered owner of a 2007 Toyota Yaris bearing plate
          number 6AYM975.
        </p>
        <p>
          24.) Upon information and belief, defendant GAYLENE LEE gave actual
          and/or permissive use of the aforesaid vehicle to defendant LEE (HOLLY
          ANN LEE) and said vehicle was being operated on February 13, 2009 with
          the full permission and consent of GAYLENE LEE.
        </p>
        <p>
          25.) On or about February 13, 2009 defendant LEE was negligent and
          careless in operating the Toyota Yaris, and as a result thereof she
          caused and contributed to the fatal injuries and death of plaintiffs'
          decedent DOWNES.
        </p>
        <p>
          26.) The negligence and carelessness of defendant LEE, for which
          defendant GAYLENE LEE is statutorily and vicariously liable, consisted
          of, but is not limited to the following:
        </p>
        <p>
          a.) Consuming Margarita alcoholic beverages after her work shift at
          Disneyland at The Lost Bar in the hours immediately preceding this
          accident, so as to operate the Toyota Yaris in an impaired condition
          with slower reflexes;
        </p>
        <p>
          b.) Driving at an excessive and unsafe speed in the HOV lanes of SR-57
          and SR-60 so as to not have proper and adequate control of the Toyota
          Yaris;
        </p>
        <p>
          c.) In failing and neglecting to maintain a proper state of vigilance
          and attention to roadway and traffic conditions so as to timely
          observe, and avoid forcibly striking the overturned vehicle and person
          of KIMBERLY DOWNES;
        </p>
        <p>
          d.) In negligently crashing into the DOWNES vehicle in the HOV lane of
          SR-60 so as to cause violent contact with the DOWNES vehicle and the
          person of KIMBERLY DOWNES;
        </p>
        <p>
          e.) And in otherwise failing and neglecting to drive her vehicle in a
          safe and prudent manner as required by law;
        </p>
        <p>
          27.) As a proximate and direct result of the negligence and
          carelessness of defendant LEE, for which defendant GAYLENE LEE and
          DOES 51-75 are vicariously and statutorily liable, Plaintiffs have
          been permanently deprived of the love, companionship, affection,
          solace, future guidance and future services and support of the
          decedent DOWNES, all to their damages in a sum that exceeds the
          jurisdiction of all lower courts.
        </p>
        <p>
          28.) Accordingly, Plaintiffs are entitled to wrongful death damages
          from these defendants.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
