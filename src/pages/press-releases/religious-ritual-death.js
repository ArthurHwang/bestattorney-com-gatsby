// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Wrongful Death Lawsuit: Teenager Dead Because Of Bizarre Religious Ritual"
        pageDescription="Call 949-203-3814 for Highest-rated Wrongful Death Attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California. No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Teenager Dies in Bizarre Religious Ritual, Bisnar Chase Personal
          Injury Attorneys Sues for Wrongful Death
        </h1>
        <BreadCrumbs location={location} />

        <img
          src="/images/brian-chase-briana-benbo-pr.jpg"
          alt="Wrongful Death"
          width="150"
          className="imgright-fixed"
        />
        <p>
          The California personal injury attorneys of Bisnar Chase Personal
          Injury Attorneys have filed a lawsuit against a Las Vegas, Nevada
          church after a bizarre religious ritual claimed the life of a
          16-year-old girl.
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/6/prweb8570298.htm">
            PRWEB
          </Link>
          )June 13, 2011
        </p>
        <p>
          The California personal injury attorneys of Bisnar Chase Personal
          Injury Attorneys have filed a lawsuit against a Las Vegas, Nevada
          church after a bizarre religious ritual claimed the life of a
          16-year-old girl.
        </p>
        <p>
          According to court documents, officials at Church of Divine
          Appointment, also known as The Moment of Truth Ministries, allegedly
          instructed Brianna Benbo, who was a parishioner attending services at
          the church alone, to ingest a capsule containing bee pollen and other
          substances prior to fasting as part of a strange religious ritual.
        </p>
        <p>
          After church officials administered the substance, Brianna had a
          severe allergic reaction to it, went into anaphylactic shock and died
          from cardiac arrest and respiratory failure within hours.
        </p>
        <p>
          The lawsuit was brought against the church and its owners, Glories
          Powell and Lula Davis, by the victim's mother, Bridgette
          Jennings-Benbo. The case, which seeks damages for negligence and
          wrongful death, is pending in the Eighth Judicial District Court of
          Clark County, Nevada, case # A-11-642421-C.
        </p>
        <h2>
          Teenager Dies from Substances Administered for Religious Fasting
        </h2>
        <p>
          According to court documents, Brianna died on July 7, 2010 after the
          teenager ingested a combination of bee pollen and other substances in
          capsulated form administered by staff members at Church of Divine
          Appointment.
        </p>
        <p>
          "This was a zealous religious fasting ritual in which church staff
          members administered drugs to an unsupervised minor," said Brian
          Chase, partner at Bisnar Chase Personal Injury Attorneys. "While
          people do, in fact, participate in fasting for religious ceremonial
          purposes, it was absolutely unnecessary and reckless for church
          officials in this case to provide drugs to an unaccompanied minor
          prior to fasting."
        </p>
        <p>
          The lawsuit claims the capsules contained a substance that was
          formulated, modified, blended and distributed by the church's owners
          and employees to parishioners who were unaware of its health risks(1),
          even though ingestion of processed bee-collected pollen has been shown
          to produce possible fatal consequences.
        </p>
        <p>
          "We allege the defendants acted negligently and tortuously by
          administering a potentially deadly cocktail of bee pollen and other
          chemicals to a girl with severe allergies to the substance," said
          Chase. "Not only did they package and distribute the supplement while
          ignoring its health risks, they provided no warning to parishioners
          about its possible side effects. In short, we believe their actions
          are directly responsible for Brianna's unnecessary and untimely
          death."
        </p>
        <p>
          In addition to damages for wrongful death and negligence, the lawsuit
          seeks additional compensation on behalf of Brianna's mother,
          Bridgette, for grief, mental anguish, sorrow and mental and physical
          pain, as well as funeral and burial costs.
        </p>
        <h2>
          For more information, read the full Bisnar Chase Personal Injury
          Attorneys Church of Divine Appointment complaint here.
        </h2>
        <p>
          (1) Bee Pollen Benefits and Side Effects, 6/13/11, WebMD.com{" "}
          <Link to="http://www.webmd.com/balance/bee-pollen-benefits-and-side-effects">
            http://www.webmd.com/balance/bee-pollen-benefits-and-side-effects
          </Link>
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys represent people in
          California and throughout the U.S., including Nevada, who have been
          very seriously injured or lost a family member due to an accident, a
          defective product, negligence or abuse. The law firm has won a wide
          variety of challenging serious personal injury, wrongful death and
          defective products cases against individuals as well as Fortune 500
          companies. For more information, please visit /
        </p>
        <p>
          {" "}
          <Link to="/press-releases/miscellaneous">
            Click Here For More Miscellaneous Press Releases
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
