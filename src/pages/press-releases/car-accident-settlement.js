// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase: San Diego Jury Awards $160,000 to Car Accident Victim"
        pageDescription="California car accident attorneys secure jury verdict for car accident victim who suffered head and neck injuries in a rear-end collision. The $160,000 verdict in the case (#37-2011-0054633) came after Allstate Insurance Company had made a final offer of $70,000 to the plaintiff."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Diego Jury Awards $160,000 to Car Accident Victim</h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            California car accident attorneys secure jury verdict for car
            accident victim who suffered head and neck injuries in a rear-end
            collision. The $160,000 verdict in the case (#37-2011-0054633) came
            after Allstate Insurance Company had made a final offer of $70,000
            to the plaintiff.
          </strong>
        </p>
        <p>
          <img
            src="/images/california-car-accident-attorney.jpg"
            alt="california car accident attorney"
            className="imgright-fixed"
          />
          The California car accident attorneys of{" "}
          <Link
            to="/car-accidents"
            title="california car accident lawyer"
            onClick="linkClick(this.to)"
          >
            Bisnar Chase
          </Link>{" "}
          obtained a $160,000 jury verdict (Case number: 37-2011-0054633, San
          Diego County Superior Court) for plaintiff Matthew Jensen, who
          suffered head and neck injuries in a rear-end accident on the 5
          Freeway in the San Diego area on June 20, 2009. According to court
          documents (Case number: 37-2011-0054633), Jensen's vehicle was in the
          emergency lane on the southbound side of the freeway when it was
          rear-ended by another vehicle driven by Jonathan Coburn.
        </p>
        <p>
          The San Diego County jury heard the case over four days and returned a
          verdict in Jensen's favor the afternoon of Dec. 6. The jury awarded
          Jensen $160,000 for damages including medical expenses, treatment
          costs and pain and suffering. Gavin Long, a trial attorney with the
          Bisnar Chase personal injury law firm, said Jensen had to go to the
          emergency room for treatment soon after the incident and also had to
          get chiropractic treatment for his injuries. In addition, he suffered
          recurring headaches as a result of his head injuries, Long said.
        </p>
        <p>
          He said the case was a challenging one because Allstate Insurance
          Company refused to settle for the insurance policy limit, which was
          $100,000. "Their final offer was $70,000, which is when we decided to
          take the case to trial," Long said. "Allstate is notorious for not
          paying fair value on these types of cases. It is their business model
          because they are a big insurance company with seasoned defense
          attorneys. They know very well that of the thousands of cases that are
          filed, very few will go to trial. They have no problem taking that
          risk."
        </p>
        <p>
          It takes a personal injury law firm that has the willingness,
          resources and conviction to go after insurance companies to remain
          persistent and win these types of cases, Long said. "In this case, the
          jury was very attentive and fair. They found our client to be reliable
          and likeable. In the end, justice was served."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California hit-and-run lawyers at Bisnar Chase represent families
          or victims of hit-and-run accidents. The firm has been featured on a
          number of popular media outlets including Newsweek, Fox, NBC, and ABC
          and is known for its passionate pursuit of results for their clients.
          Since 1978, Bisnar Chase has recovered millions of dollars for victims
          of serious personal injuries and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
