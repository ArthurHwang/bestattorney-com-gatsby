// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase: Recent J.D. Power Survey Reveals Lack of Public Awareness of Defective Car Seat Dangers; Low Safety Standards Linked to Catastrophic Injuries"
        pageDescription="Bisnar Chase: Recent J.D. Power Survey Reveals Lack of Public Awareness of Defective Car Seat Dangers; Low Safety Standards Linked to Catastrophic Injuries"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Bisnar Chase: Recent J.D. Power Survey Reveals Lack of Public
          Awareness of Defective Car Seat Dangers; Low Safety Standards Linked
          to Catastrophic Injuries
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <strong>
            Attorneys say survey results "troubling" considering last year's
            $24.7 million judgment for quadriplegic woman injured by defective
            car seat in trial against Johnson Controls.
          </strong>
        </p>
        <p>
          NEWPORT BEACH, Calif. -- (BUSINESS WIRE )-- The auto defects experts
          at Bisnar Chase Personal Injury Attorneys, LLP
          (www.bestatto-gatsby.netlify.app) say a recent survey by J.D. Power
          and Associates reveals a fundamental lack of public awareness about
          the dangers of defective car seats. What's more, they say today's
          persistently low industry safety standards enable the design and
          manufacture of defective car seats that cause occupants to be
          catastrophically injured or killed at alarming rates.
        </p>
        <p>
          "What's critically important is safety. Is your seatback going to stay
          upright if someone rear-ends you? Is your seat going to stay mounted
          to the floor if someone hits you head-on? The answers to these
          questions should be the benchmark for quality."
        </p>
        <p>
          Citing last year's $24.7 million verdict for a quadriplegic woman
          catastrophically injured by a defective car seat in a trial against
          Johnson Controls, Bisnar Chase partner and lead auto defects attorney,
          Brian Chase, calls the survey results - which ranked a Johnson
          Control's joint venture company at the top of the list - "troubling."
        </p>
        <p>
          J.D. Power says its 2012 U.S. Seat Quality and Satisfaction Study is
          based on responses from more than 74,700 new vehicle owners who were
          asked to rate the quality of their vehicle seats and seat belts based
          on whether or not they experienced defects/malfunctions or design
          problems during the first 90 days of ownership. The two top-ranked
          companies in the survey were Avanzar Interior Technologies, Ltd. - a
          joint venture between Johnson Controls Inc. and SAT Auto Technologies
          Ltd. - and TS Tech Co., Ltd, with 3.3 problems per every 100 vehicles.
        </p>
        <p>
          The industry average, according to J.D. Power, is 5.5 problems per
          every 100 vehicles. Chase says that while it's easy for vehicle owners
          to detect obvious seat problems like material flaws, malfunctioning
          headrests, faulty adjustment controls or heating element problems,
          they're completely unaware of the most dangerous problems. These
          hazards include weak seat backs that easily collapse during minor
          collisions injuring front and/or rear occupants and faulty brackets
          that fail to keep seats mounted to the floor during a crash, enabling
          occupants to be flung around the interior compartment or forcibly
          ejected from the car.
        </p>
        <p>
          "What people don't understand is that today's absurdly low safety
          standards allow car seats with the structural integrity of a lawn
          chair to be installed in many new cars," said Chase. "What's even more
          disturbing is that certain manufacturers do just enough to meet those
          minimum standards. Drivers are literally sitting on ticking time bombs
          and they don't even know it." Chase says people need to look no
          further than the large number of auto product liability lawsuits his
          firm represents that are directly linked to defective car seats,
          pointing to three noteworthy cases from last year alone. " A jury
          awarded Jaklin Romine $24.7 million in a trial against Johnson
          Controls after she was rendered a complete quadriplegic when her
          defective car seat - a seat Johnson Controls was legally responsible
          for designing, manufacturing and selling - broke backwards during a
          rear-end collision, allowing her body to submarine underneath the
          seatbelt and into the rear compartment of the car where she violently
          struck her head. She sustained devastating injuries that confine her
          to a wheelchair for the rest of her life. "
        </p>
        <p>
          Bisnar Chase filed a lawsuit against Nissan after Phillip Bloom was
          severely injured when his defective car seat broke apart from its
          floor bracket in a head-on collision, enabling the car seat cushion
          latching system and restraint system to catastrophically fail. He was
          tossed about the interior compartment of the 2005 Nissan Xterra in
          which he was riding, sustaining severe injuries including permanent
          paraplegia.
        </p>
        <p>
          In another Bisnar Chase lawsuit filed against Nissan, Brook Boynton
          was killed when the defective car seat of his 2004 Nissan Murano broke
          and collapsed backwards, rendering the seat belt useless and allowing
          his body to submarine out from underneath it. He was ejected from the
          vehicle and died at the scene of the rear-end collision a short time
          later. "In Jaklin's case, we learned Johnson Controls was well aware
          of the dangers and risks of weak seats since the early to mid-90s,"
          said Chase. "Because of this knowledge, the company had actually
          developed a design that was more structurally sound to prevent seats
          from collapsing in accidents such as hers." Chase says the fact that a
          Johnson Controls joint venture company ranked at the top of the J.D.
          Power survey demonstrates a lack of public education and ultimately, a
          false sense of security. "To say a manufacturer ranks high because it
          uses quality leathers or because its seats successfully slide
          frontwards or backwards a majority of the time is troubling," said
          Chase. "What's critically important is safety. Is your seatback going
          to stay upright if someone rear-ends you? Is your seat going to stay
          mounted to the floor if someone hits you head-on? The answers to these
          questions should be the benchmark for quality."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          Bisnar Chase (/) represents people who have been very injured or lost
          a family member due to an accident or a defective product throughout
          the nation from its Newport Beach, California offices. Bisnar Chase
          has won a wide variety of auto defect cases against most of the major
          auto manufacturers, including Ford, General Motors, Toyota, Chrysler
          and Nissan. Brian Chase is the author of the most up-to-date and
          comprehensive auto defect book available today, "Still Unsafe at Any
          Speed." The Los Angeles trial lawyers' associations recently nominated
          Mr. Chase for a "Trial Lawyer of the Year" award for his success in
          auto defects cases.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
