// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Millions of Recalled Cars Have Been Sold Without Necessary Repairs"
        pageDescription="According to a recent report, millions of recalled cars are being sold without the repairs they need. This presents big danger to consumers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Millions of Recalled Cars Sold Without Needed Repairs</h1>
        <BreadCrumbs location={location} />
        <p>
          <em>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10420971.htm">
              press release
            </Link>{" "}
            by Bisnar Chase on February 11, 2013.
          </em>
        </p>
        <img
          src="/images/auto-product-liability-attorneys.jpg"
          width="250"
          height="188"
          alt="Auto Products Liability Attorneys"
          className="imgright-fixed"
        />
        <p>
          Every year, millions of recalled vehicles are sold without much-needed
          repairs to unsuspecting buyers. According to a Feb. 11 ABC news
          report, Carfax just completed a study, which shows that 2 million
          unrepaired recalled vehicles were offered for sale online just in the
          year 2012.
        </p>
        <p>
          A recently completed Carfax study shows that more than 2 million
          recalled vehicles were sold to car buyers without the needed repairs
          last year. According to a Feb. 11{" "}
          <Link
            to="http://abcnews.go.com/Business/car-buyers-beware-millions-recalled-cars-sold-needed/story?id=18443148"
            onClick="linkClick(this.to)"
          >
            ABC news report
          </Link>
          , those sales were just online and only the sites that Carfax
          cataloged, giving reason to believe that the actual number is much
          higher.
        </p>
        <p>
          Carfax can tell that the{" "}
          <Link to="/auto-defects" title="defective used cars">
            defective vehicles
          </Link>{" "}
          have been recalled but not fixed because federal officials make recall
          notices for certain vehicle makes and models available, the report
          states. Manufacturers and dealers track the vehicle identification
          numbers of the individual cars that are brought in for the needed
          repairs, according to the article.
        </p>
        <p>
          The article states that consumers can be putting themselves and their
          families in grave danger by buying vehicles that may have been
          recalled for a host of safety defects. Some of the recently recalled
          vehicles have problems such as faulty airbags, fuel leaks, parking
          brakes that don't work and defective seatbelts.
        </p>
        <p>
          These are all dangerous defects that could result in a crash or
          serious injuries to vehicle occupants in the event of a crash, the
          report states. On the other hand, consumers who are aware that they
          are about to buy a recalled, unrepaired vehicle can use that
          information as a bargaining chip for negotiating the price, the
          article states.
        </p>
        <p>
          The lesson from this report is that consumers should be aware of
          recall notices and take them seriously whether they are buying or
          selling vehicles, says John Bisnar, founder of the Bisnar Chase
          personal injury law firm. "This information is very easy to check. You
          can try the National Highway Traffic Safety Administration's (NHTSA)
          website,{" "}
          <Link to="http://www.SaferCar.gov">http://www.SaferCar.gov</Link> or
          the Insurance Institute of Highway Safety's (IIHS) website, which also
          offers the same information at no cost."
        </p>
        <p>
          Bisnar also calls for new legislation to require dealers to inform
          buyers that they are buying a defective vehicle that has not been
          fixed. "State law doesn't require that right now and it is very
          dangerous.
        </p>
        <p>
          We now have sponsored federal legislation to require rental car
          companies to repair recalled vehicles before putting them on the road
          again. It is absolutely worth your time to check if the vehicle you
          are about to buy has been recalled. It takes just a few minutes and
          can help save you a lot of trouble and heartache."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California auto defect lawyers of Bisnar Chase represent those who
          have suffered serious personal injuries as a result of auto product
          defects. The firm has been featured on a number of popular media
          outlets including Newsweek, Fox, NBC, and ABC and is known for its
          passionate pursuit of results for their clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          http://abcnews.go.com/Business/car-buyers-beware-millions-recalled-cars-sold-needed/story?id=18443148
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
