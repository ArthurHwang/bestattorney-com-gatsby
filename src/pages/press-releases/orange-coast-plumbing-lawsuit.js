// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Vs. Orange Coast Plumbing Incorporated Lawsuit"
        pageDescription="Orange Coast Plumbing Lawsuit by Bisnar Chase claims employees rights were violated by deducting uniform costs from paychecks of its staff."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bisnar Chase Vs. Orange Coast Plumbing Incorporated Lawsuit</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Orange Coast Plumbing, Inc. Violated California Law by Deducting The
            Cost to Maintain Its Uniforms From The Weekly Wages of Its Hourly
            Employees And Forced Them to Sign Releases in an Attempt to Prevent
            Them From Being Able to Recover These Wages
          </strong>
        </p>
        <p>
          In this time and age, you would think that most employers have become
          more sophisticated and aware of their legal obligations, especially in
          light of all the <Link to="/employment-law">employment lawsuits</Link>{" "}
          that have arisen in the last decade to enforce compliance with the
          California Labor Code and Wage Orders. But that is not the case. Some
          employers continue to ignore and skirt around the law to gain an
          economic advantage over law abiding competitors. One such employer is
          Orange Coast Plumbing, Inc.
        </p>
        <p>
          Orange Coast is a corporation and is in the business of providing
          residential and commercial plumbing, and Heating Ventilation and
          Air-Conditioning ("HVAC") services. Touting itself as "the premier
          residential and commercial plumbing and heating company in Southern
          California since 1977," Orange Coast operates its business from its
          facility located on 1506 North Clinton Street, Santa Ana, CA 92703.
        </p>
        <p>
          Orange Coast employs both plumbers and HVAC technicians to respond to
          service calls 24 hours, seven days a week. Orange Coast's owner rules
          with an iron fist and employs archaic rules that are in direct
          contravention with California law. Among the many violations, one
          specifically stands out. In its Employee Handbook, Orange Coast says
          that it will deduct the cost to maintain its uniforms from the weekly
          wages of its employees - regardless of whether the employees clean
          their uniforms themselves. Not only did it say it would, it actually
          did and continued this practice from at least September 2007 to April
          2012. Orange Coast deducted between $6.25 to $6.60 from the wages of
          its service technicians on a weekly basis. During that time period,
          Orange Coast employed about 50 service technicians.
        </p>
        <p>
          Why did Orange Coast stop its illegal practice in April 2012? Because
          former employees thought what Orange Coast was doing was wrong and
          wanted to correct it.
        </p>
        <p>
          In September 2011, Bisnar Chase, on behalf of these former employees,
          filed a class action lawsuit against Orange Coast for unpaid overtime,
          meal and rest break violations, unlawful deduction of wages, and
          violations of Business & Professional Code &#167; 17200 [the Unfair
          Competition Law ("UCL")] and the Private Attorneys General Act (the
          "PAGA").
        </p>
        <p>
          Just four months after the complaint was filed, on January 11, 2012,
          at about 6:30 a.m., Orange Coast paged all of its service technicians
          and required them to attend a mandatory meeting. The bookkeeper, Lore
          Kramer, and the brothers of the owner of Orange Coast, Morgan Flynn
          and Joe Flynn were present and conducted the meeting. Lore and the
          Flynn brothers stood in front of the room. Approximately 30 service
          technicians were present.
        </p>
        <p>
          Lore began by explaining that the topic of the meeting was regarding
          the class action complaint brought by former employees. Within 30
          seconds after Lore started speaking, Morgan interrupted with
          profanities and negative comments about the complaint and the
          plaintiffs. Lore told the service technicians that if they signed a
          release, Orange Coast will give them $250. The release allegedly
          prohibits the service technicians from recovering back wages for the
          unlawful deductions, overtime, and premium pay for having to work
          through their lunches and breaks.
        </p>
        <p>
          Two of the plumbing technicians who were present asked what the $250
          was for. Morgan replied by saying that "John's lawyer said that we
          have to give you this money for this to be legal."
        </p>
        <p>
          Throughout the entire meeting, Morgan and Joe kept repeating that
          <strong>
            "If you don't sign these papers, then you are with them. If you sign
            these papers, then we know that you are with us!"
          </strong>{" "}
          After their speeches, Morgan and Joe passed out a single page
          settlement agreement for the service technicians to sign. Morgan and
          Joe hovered behind the service technicians to ensure that each
          settlement agreement was signed. Neither Lore, Morgan nor Joe told any
          of the service technicians that they could consult with an attorney
          before signing the settlement agreement or take time to review and
          deliberate about signing the release. After the settlement agreements
          were signed, the Flynn brothers picked them up from the service
          technicians and none of the service technicians were given a copy.
        </p>
        <p>
          Would you consider the circumstances in which the service technicians
          sign the release voluntary or uncoerced?
        </p>
        <p>
          "There are a number of violations displayed within Orange Coast's own
          polices and records. California employees and employers are fortunate
          to have a state which provides them the mechanism to equalize the
          playing field against those employers who try to take advantage of
          their work force and skirt around the law. We hope to provide
          businesses with additional reasons to promote fair, lawful work
          environments for the people that work for them," says Mr. Beligan.
        </p>
        <p>About Bisnar Chase Wage and Hour Attorneys</p>
        <p>
          Bisnar Chase attorneys assist those seeking counsel for wage and hour,
          wrongful termination, breach of contract, overtime pay, and disability
          discrimination claims. The Bisnar Chase law firm has been assisting
          victims since 1978 and have sustained a 97.5% success rate.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
