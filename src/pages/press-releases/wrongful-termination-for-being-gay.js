// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Wrongful Termination For Being Gay and Pregnant"
        pageDescription="A woman alleges Mars fired her after her co-workers discovered she was gay, and that she wqas discrimiated against for being pregnant."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Woman Claims Wrongful Termination For Being Gay and Pregnant</h1>
        <BreadCrumbs location={location} />
        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link to="http://www.prweb.com/releases/2013/2/prweb10461034.htm">
              press release
            </Link>
          </i>
        </p>
        <p>
          A lawsuit (Case Number L000055-13) filed by Theresa Kwiecinski in New
          Jersey Superior Court alleges that her former employer, Mars Chocolate
          North America, fired her because she was a lesbian and became
          pregnant.
        </p>
        <p>
          According to a Feb. 21 news report in The Express-Times, the lawsuit
          (Case Number L000055-13, New Jersey Superior Court) alleges that
          Kwiecinski was fired in September 2011 after her co-workers learned
          about her sexual orientation and she was "treated differently" because
          she was pregnant.
        </p>
        <img
          src="/images/mars-woman-fired-for-being-gay.jpg"
          width="250"
          height="160"
          alt="hip replacements"
          className="imgright-fixed"
        />
        <p>
          The article states that Kwiecinski's treatment at work began to
          deteriorate in March 2011 when her colleagues met her partner and
          their son during a week-long work conference in Florida. Since that
          time, Kwiecinski alleges that she and her family were treated
          differently.
        </p>
        <p>
          Kwiecinski stated in the employment lawsuit (Case Number L000055-13,
          New Jersey Superior Court) that after returning from the conference
          she was given an "unreasonable work load" and was ignored when she
          asked for help.
        </p>
        <p>
          In addition to discrimination based on sexual orientation, Kwiecinski
          alleges she was also discriminated a year before that when she was
          dealing with a complicated pregnancy. While she had received good work
          reviews prior to her pregnancy, she was given reviews of "below
          expectations" because she could not travel due to her complicated
          pregnancy, the lawsuit states.
        </p>
        <p>
          After her pregnancy and revelation of her sexual orientation,
          Kwiecinski alleges that she was placed on a performance involvement
          plan that proved impossible and culminated in her{" "}
          <Link to="/employment-law/wrongful-termination">
            wrongful termination
          </Link>{" "}
          in September 2011.
        </p>
        <p>
          "While federal laws relating to discrimination based on pregnancy and
          gender are clear, the laws surrounding discrimination based on sexual
          orientation are murkier," said John Bisnar, founder of the Bisnar
          Chase personal injury law firm. "Sexual orientation discrimination
          includes being treated differently, harassed or ridiculed because of
          your perceived sexual orientation gay, bisexual or homosexual."
        </p>
        <p>
          Bisnar explains that although there is no federal law that
          specifically outlaws workplace discrimination based on sexual
          orientation, nearly half the states, including California and New
          Jersey, where this lawsuit was filed, have laws that prohibit
          discrimination based on sexual orientation in both public and private
          jobs. "Discrimination in the workplace based on one's personal
          attributes be it race, skin color, nationality, gender, religion or
          sexual orientation -- is unfair. Anyone who has suffered such
          discrimination or harassment can find recourse in the civil justice
          system."
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation. Our offices are located at 1301 Dove St. Newport Beach,
          CA 92660.
        </p>
        <p>
          Source:
          <br />
          http://www.lehighvalleylive.com/warren-county/express-times/index.ssf/2013/02/hunterdon_county_woman_alleges.html
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
