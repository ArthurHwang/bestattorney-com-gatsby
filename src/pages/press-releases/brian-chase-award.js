// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brian Chase One This Years OCTLA's Top Product Liability Trial Lawyer"
        pageDescription="Brian Chase is a California auto products liability and catastrophic injury lawyer with a top gun reputation. Brian has won millions for his clients serious injury and accident cases"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Brian Chase Named One of OCTLA's Top Guns Product Liability Trial
          Lawyer of the Year
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <img
            src="/images/brianchase4.jpg"
            alt="Brian D Chase"
            className="imgright-fixed"
          />
          OCTLA's (Orange County Trial Lawyers Association) "The Gavel" writes:
        </p>
        <blockquote>
          <p>
            {" "}
            <Link
              to="https://plus.google.com/115675781126354169894/about"
              target="_blank"
              name="Brian D Chase - Google+"
            >
              Brian D. Chase
            </Link>{" "}
            is a partner in Bisnar Chase Personal Injury Attorneys in Newport
            Beach. The firm has a national practice and specializes in
            automobile product liability and catastrophic injury cases.
          </p>
        </blockquote>
        <p>
          He has tried over twenty jury trials and, in the last couple of years,
          has entered into numerous seven - and eight-figure settlements with
          various automobile manufacturers.
        </p>
        <p>
          Mr. Chase was involved in two published appellate opinions:{" "}
          <em>Schreiber v. Estate of Kiser</em> (a California Supreme Court case
          dealing with expert witness designations) and
          <em>Hernandez v. State of California</em> (an appellate case from the
          Second Appellate District dealing with governmental design immunity).
        </p>
        <p>
          Within the last year, he settled during trial two major cases against
          Ford Motor Company involving wrongful deaths and catastrophic brain
          injuries for significant confidential sums.
        </p>
        <p>
          Mr. Chase is the First Vice President of Orange County Trial Lawyers,
          serves on the Board of Governors for CAOC, and is a nationwide
          lecturer on litigation-related topics. Mr. Chase lives with his two
          daughters in California.
        </p>
        <h2>Articles By Attorney Brian Chase</h2>
        <ul>
          <li>
            {" "}
            <Link to="/blog/identifying-auto-product-defect-cases">
              Identifying and Litigating Quality Auto Products Defect Cases
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/motions-in-limine">
              Expert Witnesses and Motions in Limine
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/increasing-your-tort-recovery">
              Multiplying Your Tort Recovery by Elimating Settlement Set-offs
              Under Proposition 51
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/road-edge-recovery-manuever">
              Cutting Edge Testing For Rollover Cases: NHTSA&#8217;s Road Edge
              Recovery Maneuver
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/trial-setting-preference">
              Making Motions for Trial-Setting Preference
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/utilizing-expert-witnesses">
              Utilizing Expert Witnesses
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/hmo-lien-rights">
              Health Maintenance Organizations and Their Lien Rights
            </Link>
          </li>
        </ul>
        <h2>Representation by Brian Chase</h2>
        <p>
          Brian Chase handles serious car accident, auto defects and product
          liability cases in California. If you'd like to have a free
          consultation about your case,{" "}
          <strong>please call 949-203-3814</strong>.
        </p>
        <p>
          {" "}
          <Link to="/attorneys/brian-chase">
            Click here to read Brian Chase's bio
          </Link>
          .
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
