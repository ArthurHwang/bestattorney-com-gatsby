// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

// interface Props {
//   location: any
// }

export default function PressReleasesPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Personal Injury Press Releases by Bisnar Chase"
        pageDescription="The latest press releases by Bisnar Chase Personal Injury Attorneys including verdicts, settlements, awards and nominations."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Press Releases</h1>
        <BreadCrumbs location={location} />
        Our press releases ordered by date
        <ul id="press-release-list">
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12768353-bisnar-chase-secures-11-million-jury-verdict-for-wife-and-children-of-man-who-died-by-suicide-while-in-rehab.html"
              target="_blank"
            >
              Trial Lawyer, H. Gavin Long Secures $11 Million Dollar Victory in
              Wrongful Death Case
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.einpresswire.com/article/483394955/auto-defect-attorney-brian-chase-details-specifics-on-liability-of-faulty-seat-belts-and-airbags"
              target="_blank"
            >
              Auto Defect Attorney Brian Chase Details Specifics on Faulty
              Seatbelts and airbags.
            </Link>
            <em>
              Chase recently spoke with AskTheLawyers.com™ in an interview to
              address a specific type of products liability case: auto defects.
            </em>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12750796-top-personal-injury-law-firm-bisnar-chase-adds-two-rising-stars-to-its-attorney-team.html"
              target="_blank"
            >
              Bisnar|Chase Continues to Expand Their Trial Lawyer Team.
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12735314-bisnar-chase-welcomes-two-new-attorneys-to-its-growing-team.html"
              target="_blank"
            >
              Bisnar|Chase Welcomes Two New Attorneys to Its Growing Team.
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12736969-bisnar-chase-stands-in-solidarity-with-father-of-four-who-lost-wife-and-unborn-daughter-in-hemet-dui-crash.html"
              target="_blank"
            >
              Bisnar|Chase To Represent Zachary Kincaid Over Devasting Loss of
              Wife &amp; Baby.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12724473" target="_blank">
              Brian Chase Named Lawyer of the Year 2019.
            </Link>{" "}
            The firm also had two other attorneys honored with Best Lawyers in
            America.
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12720475" target="_blank">
              Bisnar|Chase Named Best Places to Work 2018.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12686025" target="_blank">
              Bisnar|Chase Celebrates Four Decades of Service.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12663428" target="_blank">
              Bisnar|Chase Will Continue Investigations into Equifax Data
              Breach.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12649077" target="_blank">
              Bisnar|Chase Named Best Places To Work 2017.
            </Link>
            For a sixth year in row the law firm of Bisnar|Chase has been named
            Best Places to Work.
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12654105" target="_blank">
              Brian Chase Featured on CNN on Ford Carbon Monoxide Poisoning
              Cases Brian Chase, senior partner with the Newport Beach auto
              defect law firm of Bisnar Chase speaks to CNN about the dangers
              posed by Ford Explorers that are leaking carbon monoxide into
              vehicle compartments &amp; sickening occupants.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.prlog.org/12610273" target="_blank">
              Bisnar Chase Secures $10M in Auto Rollover Crash
            </Link>{" "}
            The personal injury law firm of Bisnar Chase has obtained a $10
            million judgment against an impaired driver for the families of
            victims who died in a rollover crash. The firm also settled an
            additional auto product liability claim against Chrysler for an
            undisclosed amount.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12583372-bisnar-chases-gavin-long-wins-octlas-top-gun-personal-injury-trial-lawyer-of-the-year.html"
              target="_blank"
            >
              H. Gavin Long Wins Trial Lawyer of the Year from the OCTLA
            </Link>
            H. Gavin Long, a personal injury attorney with the Newport Beach
            personal injury law firm of Bisnar Chase has been named Trial Lawyer
            of the Year under the personal injury category by the Orange County
            Trial Lawyers Association (OCTLA).
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12569414-bisnar-chase-named-best-place-to-work-for-2016.html"
              target="_blank"
            >
              Bisnar Chase Named Best Places to Work
            </Link>{" "}
            For the 5th year in a row the firm has been voted Best Places to
            Work in Orange County.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12554007-newport-beach-personal-injury-law-firm-obtains-settlement-for-injured-rollover-crash-victim.html"
              target="_blank"
            >
              Newport Beach Auto Defect Attorney Secures Settlement for Family
              of Rollover Crash Victims
            </Link>
            The Newport Beach auto defect law firm of Bisnar Chase has helped
            the family of two killed in an SUV rollover crash obtain a
            confidential monetary settlement in a lawsuit filed against the
            automaker, tire manufacturer and auto dealership. Bisnar Chase has
            obtained a settlement for the family of two individuals who were
            killed in an SUV rollover crash in Fresno.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12554007-newport-beach-personal-injury-law-firm-obtains-settlement-for-injured-rollover-crash-victim.html"
              target="_blank"
            >
              Newport Beach Personal Injury Law Firm Obtains Settlement for
              Injured Rollover Crash Victim
            </Link>
            The Newport Beach personal injury law firm of Bisnar Chase has
            helped a catastrophically injured victim secure a significant
            settlement in an auto product liability lawsuit.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12551275-bisnar-chase-obtains-one-million-dollars-in-truck-accident.html"
              target="_blank"
            >
              Bisnar Chase Obtains $1M in Truck Accident Case
            </Link>{" "}
            Personal injury law firm of Bisnar Chase helped the family of a
            39-year-old man obtain $1 million in a wrongful death case (Case No.
            VCU-262573) from a truck driver who struck and killed the victim
            while making a turn in the parking lot of a truck stop.
          </li>
          <li>
            {" "}
            <Link to="https://prlog.org/12549737" target="_blank">
              Brian Chase Discusses the J&amp;J Talcum Powder Health Issues and
              Victim's Rights.
            </Link>{" "}
            Newport Beach product defect attorney Brian Chase talks about the
            rights of women who have been diagnosed with ovarian cancer after
            using Johnson &amp; Johnson's talcum powder and baby powder for
            feminine hygiene purposes. A St. Louis jury in February ordered the
            company to pay $72 million to the family of a woman who succumbed to
            ovarian cancer after using the powder for over 40 years.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.prlog.org/12528097-bisnar-chase-welcomes-new-member-to-its-trial-team.html"
              target="_blank"
            >
              Tom Antunovich Joins Legal Team.
            </Link>{" "}
            As the newest member of Bisnar Chase's trial team, Antunovich will
            work with the firm's team of trial attorneys handling personal
            injury and product defect cases, representing injured clients and
            helping them seek compensation for their losses.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12513766-personal-injury-law-firm-bisnar-chase-files-product-liability-lawsuit-against-cigs.html"
              target="_blank"
            >
              Personal Injury Law Firm Bisnar Chase Files Product Liability
              Lawsuit Against E-Cigs
            </Link>
            Bisnar Chase product defect attorneys have filed a product liability
            lawsuit (Case No. BC 595511) on behalf of an injured consumer
            alleging he suffered severe injuries from exploding e-cig.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/automakers-liability">
              Bisnar Chase Personal Injury Attorneys Reveals Driving Dangers of
              Deadly Auto Defects.
            </Link>{" "}
            The things that automakers don't want you to know. Read about
            defective seat backs, insufficient roof strength, defective
            seatbelts, windows, airbags, and insufficient roll stability.
          </li>

          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12494360-brian-chase-of-bisnar-chase-named-in-2016-best-lawyers-in-america.html"
              target="_blank"
            >
              Brian Chase of Bisnar Chase Named in 2016 Best Lawyers in America
            </Link>
            Brian Chase, managing partner and senior trial attorney at the
            personal injury law firm of Bisnar Chase has been named in the 2016
            Best Lawyers in America. Chase has the distinction of being featured
            in the 22nd edition of the publication.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12509893-outgoing-caoc-president-brian-chase-lauds-organizations-accomplishments.html"
              target="_blank"
            >
              Outgoing CAOC President Brian Chase Lauds Organization's
              Accomplishments
            </Link>
            Brian D. Chase, outgoing Consumer Attorneys of California (CAOC)
            President and senior partner at the Newport Beach personal injury
            law firm of Bisnar Chase praised the organization's accomplishments
            for the year during its annual dinner.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12484570-bisnar-chase-secures-3-million-jury-verdict-for-client-with-traumatic-brain-injury.html"
              target="_blank"
            >
              Bisnar Chase Secures $3 Million Jury Verdict for Client with
              Traumatic Brain Injury
            </Link>
            Bisnar Chase obtains $3 million jury award for their client, Joseph
            Butenhoff, who suffered a permanent brain injury in a Long Beach
            pedestrian accident.
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12466400-bisnar-chase-named-one-of-the-best-places-to-work-in-orange-county-fourth-year-in-row.html">
              Bisnar Chase Named #1 Best Places to Work by OCBJ
            </Link>{" "}
            For a fourth year in a row, Bisnar Chase has been named as Best
            Place to Work in Orange County. This year, Bisnar Chase made the
            list as the #1 small business.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.pitchengine.com/pitches/08cb7773-deec-41c7-b983-8a6cd4cfa709"
              target="_blank"
            >
              Bisnar Chase Employees Fundraise to Help Foster Kids.
            </Link>{" "}
            Employees at Newport Beach personal injury law firm Bisnar Chase
            raised about $1,150 to create "sweet cases," which are small
            suitcases with blankets, activity books and other goodies for foster
            children who move from home to home.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12452973-bisnar-chase-seeks-injunction-to-stop-lorillard-from-promoting-blu-products-as-safe.html"
              target="_blank"
            >
              Bisnar Chase Seeks Injunction to Stop Lorillard from Promoting Blu
              Products as Safe.
            </Link>
            The Newport Beach personal injury law firm of Bisnar Chase is
            seeking an injunction to stop Lorillard Tobacco Company from
            promoting its Blu e-cigarettes as safe and healthy under the
            Consumer Legal Remedies Act.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12452513-bisnar-chase-personal-injury-attorneys-sponsors-800-safety-kits-for-school-children.html"
              target="_blank"
            >
              Bisnar Chase Personal Injury Attorneys Sponsors 800 Safety Kits
              for School Children.
            </Link>
            Newport Beach personal injury law firm Bisnar Chase has sponsored
            800 safety kits for children at John H. Eader Elementary School in
            Huntington Beach.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12448760-bisnar-chase-awards-1000-scholarship-to-law-student-in-need.html"
              target="_blank"
            >
              Bisnar Chase Awards $1,000 Scholarship to Law Student in Need.
            </Link>
            Bisnar Chase will award Benita Hudson, a second year student at the
            Nashville School of Law, a $1,000 scholarship to pursue her dream of
            building a successful family law practice geared to helping
            low-income families.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12426280-bisnar-chase-consumer-attorneys-caution-those-affected-by-anthem-security-breach.html"
              target="_blank"
            >
              Bisnar Chase Consumer Attorneys Caution Those Affected by Anthem
              Security Breach
            </Link>
            Anthem Inc., one of the nation's largest healthcare networks,
            announced a massive security breach this month, which they said,
            would affect 80 million current and former customers. Bisnar Chase
            consumer attorneys say those affected could be facing serious
            identity theft risks in the long term.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12414024-brian-chase-of-bisnar-chase-named-as-one-of-californias-top-10-personal-injury-lawyers.html"
              target="_blank"
            >
              Brian Chase Named California Top Attorney by the NAOPIA
            </Link>
            Brian Chase, Senior Partner at the Newport Beach personal injury law
            firm of Bisnar Chase has been named as one of the Top 10 Personal
            Injury Lawyers in the state of California, by the National Academy
            of Personal Injury Lawyers (NAOPIA).
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12413946-california-personal-injury-attorney-brian-chase-attends-swearing-in-ceremonies-in-sacramento.html"
              target="_blank"
            >
              Brian Chase Attends Swearing-in Ceremonies in Sacramento
              California
            </Link>
            Brian Chase, senior partner at the Newport Beach personal injury law
            firm of Bisnar Chase and current President of the Consumer Attorneys
            of California (CAOC), received an invitation to attend swearing-in
            ceremonies for Gov. Jerry Brown, Lt. Gov. Gavin Newsom, the Supreme
            Court Justices - Leondra Kruger and Mariano-Florentino Cuellar,
            Attorney General Kamala Harris and Insurance Commissioner Dave Jones
            in Sacramento on Jan. 5
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12399253-brian-chase-of-bisnar-chase-takes-office-as-president-of-the-consumer-attorneys-of-california.html"
              target="_blank"
            >
              Brian Chase of Bisnar Chase Personal Injury Attorneys Takes Office
              as CAOC President
            </Link>
            Brian Chase, senior partner at the Newport Beach personal injury law
            firm of Bisnar Chase was installed as the Consumer Attorneys of
            California's (CAOC) 2015 President during the group's 53rd Annual
            Convention in San Francisco Nov. 15 . Chase is nationally renowned
            for taking on corporate giants and representing victims of auto
            defects and other dangerous products.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12392180-newport-beach-personal-injury-law-firm-of-bisnar-chase-nominated-for-2014-litigator-award.html"
              target="_blank"
            >
              Bisnar Chase Nominated for Prestigous Litigator Award 2014
            </Link>
            The 2014 Litigator Awards will be presented nationally for the first
            time ever. Bisnar Chase is one of the nominees. Awards will be
            presented in 75 different categories separated by practice area
            specialties.
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12385267" target="_blank">
              Bisnar Chase Personal Injury Lawyers Involved in $400 Million Endo
              Vaginal Mesh Settlement
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12385201" target="_blank">
              H. Gavin Long of Bisnar Chase Named to the American Board of Trial
              Advocates.
            </Link>{" "}
            H. Gavin Long, a personal injury attorney with Bisnar Chase has
            received the notable distinction of being named as a member of the
            American Board of Trial Advocates. One of the oldest and most
            respected organizations for trial attorneys.
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12370254" target="_blank">
              Brian Chase of Bisnar Chase Named OCTLA's Products Liability Trial
              Lawyer of the Year
            </Link>{" "}
            Brian D. Chase, senior partner with the Newport Beach personal
            injury law firm of Bisnar Chase, has been named Trial Lawyer of the
            Year under the Products Liability category by the Orange County
            Trial Lawyers Association (OCTLA).
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12366402" target="_blank">
              Bisnar Chase Trial Lawyer Doug Carasso Featured in September Issue
              of OC Lawyer.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12346018" target="_blank">
              Personal Injury Lawyer Brian Chase Featured on the Cover of
              Attorney Journal Orange County.
            </Link>{" "}
            Brian Chase, senior partner at the Orange County personal injury law
            firm of Bisnar Chase, has been featured on the cover of the Attorney
            Journal's Orange County edition.
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12345992" target="_blank">
              Bisnar Chase Makes Orange County's Best Places to Work List for
              Third Year Straight.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12341791" target="_blank">
              Attorney Doug Carasso Joins Bisnar Chase's Personal Injury
              Litigation Team.
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://www.prlog.org/12334297" target="_blank">
              California Auto Defect Lawyer Brian Chase Skeptical about GM's
              Internal Investigation Report.
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12331483-california-product-liability-lawyer-warns-about-defective-nest-labs-co-smoke-alarms.html"
              target="blank"
            >
              California Product Liability Lawyer Warns about Defective Nest
              Labs CO + Smoke Alarms
            </Link>
            Palo Alto-based Nest Labs issued a recall for more than 440,000 of
            its CO + Smoke Alarms because they can fail to alert those in the
            home when a fire breaks out or carbon monoxide gas leak occurs.
          </li>
          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12316663-newport-beach-personal-injury-law-firm-fights-insurance-company-wins-favorable-verdict.html"
              target="_blank"
            >
              Newport Beach Personal Injury Law Firm Fights Insurance Company
              Wins Favorable Verdict.
            </Link>
            The Newport Beach personal injury attorneys at BISNAR CHASE secured
            a jury verdict (Case Number M119273, Monterrey County Superior
            Court) for a car accident victim that was more than eight times
            larger than the insurance limit.
          </li>
          <li>
            Appellate Court Decision in Seatback Defect Case a Victory for
            Bisnar Chase Product Liability Lawyers. An appellate court upheld
            (Case Number: B239761) a $24.7 million jury verdict awarded to
            Jaklin Mikhal Romine who was rendered a permanent quadriplegic when
            the defective driver's seat of her car collapsed during a rear-end
            collision in October 2006.
          </li>

          <li>
            BISNAR CHASE Personal Injury Lawyers Obtain $340,000 Settlement in
            Orange County Pedestrian Accident Case
          </li>

          <li>
            {" "}
            <Link
              to="http://www.prlog.org/12288501-bisnar-chase-files-class-action-lawsuit-against-manufacturer-of-electronic-cigarettes.html"
              target="_blank"
            >
              Bisnar Chase Files Class Action Against e-Cig Maker.
            </Link>
            The Newport Beach consumer law firm of BISNAR CHASE has filed a
            class action lawsuit (Case number: 30-2014-00705711-CU-FR-CXC/Orange
            County Superior Court) against Njonc. and Sottera Inc.,
            manufacturers of certain brands of electronic cigarettes or
            e-cigarettes. According to court documents, the plaintiff and class
            representativ Ine, Eric McGovern, is seeking compensatory and
            punitive damages from the manufacturers of the device alleging that
            he experienced various symptoms including nausea, dizziness, and
            persistent pain in his chest after using the electronic cigarettes.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/california-personal-injury-law-book">
              Bisnar Chase Personal Injury Lawyer Appointed to Prestigious Board
              of Directors Representing Trial Attorneys.
            </Link>
            H. Gavin Long, trial attorney for the Newport Beach personal injury
            law firm of Bisnar Chase has been appointed to the board of
            directors of the Consumer Attorneys of California (CAOC).
          </li>

          <li>
            {" "}
            <Link to="/press-releases/california-personal-injury-law-book">
              California Personal Injury Lawyers John Bisnar and Brian Chase
              Release New Book to Demystify Personal Injury Law for Consumers
            </Link>
            The new reference guide for accident victims has significant input
            from 13 top injury attorneys nationwide.
          </li>

          <li>
            Bisnar Chase Personal Injury Lawyers Secure $30-Million Judgment in
            Motorcycle Accident Brain Injury Case 25-year-old Lucas Vogt gets
            compensation for his motorcycle accident.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/los-angeles-motorcycle-verdict">
              California Injury Attorney H. Gavin Long Secures Verdict in his
              Client's Los Angeles Motorcycle Accident
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/best-places-to-work-oc">
              Bisnar Chase Personal Injury Lawyers Named Among Best Places to
              Work in Orange County
            </Link>{" "}
            by OC Business Journal.
          </li>

          <li>
            H. Gavin Long Obtains Verdict in Difficult{" "}
            <Link to="/press-releases/pugh-car-accident">
              Orange County Car Accident Case
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/national-brain-injury-awareness-month">
              March is National Brain Injury Awareness Month
            </Link>{" "}
            The Brain Injury Association of America is kicking off their
            yearlong campaign to educate the public about the dangers of
            traumatic brain injuries by declaring March as National Brain Injury
            Awareness Month.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/subaru-vehicle-recall">
              Subaru Recalling Vehicles that Can Start on their Own.
            </Link>{" "}
            Subaru has announced an auto defect recall for about 50,000 vehicles
            in the United States and Canada because they could take off on their
            own without warning.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/poor-nursing-home-care">
              Government Report Shows Medicare Paid $5.1 Billion for Poor
              Nursing Home Care.
            </Link>{" "}
            Medicare paid about $5.1 billion for patients to stay in skilled
            nursing facilities that even failed to meet federal quality of care
            in 2009, a Feb. 27 Associated Press news article reported.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/woman-wins-vaginal-mesh-lawsuit">
              Jury Awards $3.35 Million to Woman in Vaginal Mesh Lawsuit.
            </Link>{" "}
            A New Jersey jury has awarded $3.35 million to 47-year-old Linda
            Gross, who filed a vaginal mesh lawsuit alleging that Johnson &
            Johnson and its subsidiary, Ethicon, did not warn her doctor of the
            potential dangers of the mesh implant.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/depuy-knee-device-recalls">
              DePuy Recalls Defective Knee Replacement Device
            </Link>{" "}
            DePuy Orthopedics, a Johnson & Johnson subsidiary, has recalled a
            defective knee replacement device because of its potential to cause
            fractures and other complications among the patients who receive
            them.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/amusement-park-discrimination">
              Family Sues Disneyland Employee For Discrimination
            </Link>{" "}
            An African American family has filed a civil lawsuit against a
            Disneyland employee and the amusement park for discrimination after
            the man dressed as the White Rabbit character from "Alice in
            Wonderland" allegedly refused to hug black children.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/wrongful-termination-for-being-gay">
              Woman Claims Wrongful Termination For Being Gay
            </Link>{" "}
            Theresa Kwiecinski in New Jersey Superior Court alleges that her
            former employer, Mars Chocolate North America, wrongfully terminated
            her because she was a lesbian and became pregnant.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/toddler-dog-attack">
              Toddler Mauled To Death In Dog Attack
            </Link>{" "}
            Isaiah Ray Aguilar, a 2-year-old boy, was a victim of a fatal dog
            attack when he chased a balloon into his neighbors front yard. The
            owner of the dog allegedly did nothing.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/nursing-home-abuse-caught-on-camera">
              New York Nursing Home Abuse Caught on Camera
            </Link>{" "}
            A Bronx woman placed a hidden camera in her 89-year-old
            grandmother's room after noticing unusual markings and bruising on
            her body and accused Gold Crest Care Center of nursing home abuse
            and neglect.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/hip-replacements-fail-in-women">
              New Study: Hip Replacements More Likely to Fail in Women
            </Link>{" "}
            Hip replacements are more likely to fail in women than in men,
            according to one of the largest studies of its kind that involved
            U.S. patients.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/motrin-product-liability-lawsuit">
              A Teenager Who Took Motrin Lost Her Skin in a Deadly Drug Reaction
            </Link>{" "}
            In another appalling drug reaction case, a teenager took Motrin and
            suffered a life-threatening reaction that resulted in her losing her
            skin. The teen was awarded $63 million.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/seatback-failure-dui-victim">
              Bisnar Chase Obtains $2 Million Settlement for Family of DUI
              Victim
            </Link>{" "}
            Brook Boynton was killed by a drunk driver in Orange County. His
            family has civil justice.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/glass-shards-in-mushroom-ravioli">
              Glass Shards Were Found In Containers of Lean Cuisine Mushroom
              Ravioli
            </Link>{" "}
            In a shocking food recall, Nestle has recalled 500,000 containers of
            its Lean Cuisine Mushroom Ravioli. Reports started coming into the
            company about glass fragments being found in the containers.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/funeral-home-negligence">
              Mother's Murdered Daughter's Body Neglected by Funeral Home
            </Link>{" "}
            In a shocking case of funeral home neglect, the body of a mother's
            murdered daughter was left to rot on a table in the downstairs
            basement of a local funeral home.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/recalled-cars-sold-without-repairs">
              Millions of Recalled Cars Were Sold Without the Necessary Repairs
              Being Performed
            </Link>{" "}
            According to a recently completed Carfax study, more than 2 million
            recalled vehicles have been sold to buyers without the necessary
            repairs in the last year.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/companies-failing-health-inspections">
              Many Compounding Pharmacies in MA Do Not Pass Surprise Health
              Inspections
            </Link>{" "}
            Many compounding pharmacies in Massachusetts have failed many recent
            surprise health inspections. This raises concern because many are
            now similar to the pharmacy who was responsible for tainted vaccines
            that caused outbreaks last year.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/nurse-fired-after-reporting-alleged-neglect">
              Nurse Fired After Reporting Elder Abuse & Neglect
            </Link>{" "}
            A nurse has been fired after reporting alleged abuse and neglect of
            an elderly patient. She has filed an employment lawsuit against her
            employer.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/pit-bull-attack-liability">
              Pit Bull Attacks Mother and Son
            </Link>{" "}
            Attorney John Bisnar Discusses Dog Bite Liability in California.
          </li>

          <li>
            {" "}
            <Link to="/blog/bicycle-accident-wrongful-death-lawsuit">
              After Driver Was Not Charged in Fatal Bike Crash, Family Files a
              Wrongful Death Lawsuit
            </Link>{" "}
            The family of a 41 year old wrongful death bicycle crash victim
            filed a lawsuit after the driver was not criminally charged after
            causing the collision.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/government-requires-publications-of-payments-to-doctors">
              Government Requires Disclosure of Payments to Doctors
            </Link>{" "}
            In a landmark decision, the Government is now taking initiative to
            write new legislation that will require drug and medical device
            companies to completely disclose payments made to Doctors. In
            September 2014, the Government will release information about these
            payments.
          </li>

          <li>
            {" "}
            <Link to="/bus-accidents/">
              California Tour Bus Crash - Fatal Injuries
            </Link>{" "}
            Seven dead, dozens injured in horrific tour bus accident in
            Riverside County.
          </li>

          <li>
            {" "}
            <Link to="/blog/new-generic-form-of-propecia-launch-disappointment">
              New Generic Form of Propecia Is a Big Disappointment
            </Link>{" "}
            Despite the attempts to block the approval of a drug that is known
            to cause various side effects, a new generic form of Propecia has
            been approved by the FDA anyway.
          </li>

          <li>
            {" "}
            <Link to="/blog/cold-cough-syrup-recall">
              Triaminic and Theraflu Recalled After Discovery of Faulty Caps 8
              children accessed the cough syrups, one being hospitalized. 2.3
              million bottles recalled.
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/blog/massive-toyota-potentially-dangerous-airbag-recall">
              Massive Toyota Recall of 1.3 Million Vehicles Worldwide - Caused
              by Defective Airbags.
            </Link>{" "}
            A new Toyota recall covers about 900,000 Corolla and Corolla Matrix
            models worldwide for potentially faulty airbags. The full recall
            expands to 1.3 million vehicles.
          </li>

          <li>
            {" "}
            <Link to="/food-poisoning/new-cdc-reports">
              CDC Releases First Estimates Of Food Borne Illnesses Caused by
              Certain Food Sources in the U.S.
            </Link>{" "}
            A new report that the U.S. Centers for Disease Control & Prevention
            has released discusses how leafy greens and contaminated dairy
            resulted in the most hospitalizations.
          </li>

          <li>
            {" "}
            <Link to="/blog/new-survey-finds-that-many-people-admit-to-talking-on-their-cell-phones-while-driving">
              New Survey Shows Many People Use Their Cell Phones While Driving
            </Link>
            The results of a new survey show that many people use their cell
            phones while driving, even though they know that it's very dangerous
            to do so.
          </li>

          <li>
            {" "}
            <Link to="/blog/brian-chase-honored-at-octlas-50th-anniversary-event">
              Brian Chase Accepts Award at Orange County Trial Lawyer
              Association's 50th Anniversary Event
            </Link>
            - Brian Chase, one of the nation's leading personal injury attorneys
            and auto defects experts was awarded during OCTLA's 50th Anniversary
            Awards Gala.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/junior-seau-wrongful-death">
              California Wrongful Death Lawsuit Filed by NFL Star Junior Seau's
              Family Members
            </Link>{" "}
            Former linebacker Junior Seau's family has filed a wrongful death
            lawsuit against the NFL alleging that his suicide last May was the
            result of brain damage caused by violent hits he endured while
            playing football.
          </li>

          <li>
            {" "}
            <Link to="/blog/report-claims-johnson-and-johnson-had-knowledge-of-their-defective-hip-implants">
              New Report Shows J&J Had Knowledge of Their Implants' Failure Rate
            </Link>
            Johnson & Johnson knew that their defective hip implants would fail
            within five years among 40% of patients who received it, according
            to an internal analysis.
          </li>

          <li>
            {" "}
            <Link to="/blog/honda-recalls-defective-airbags">
              Auto Defects Responsible for 750,000 Honda Vehicle Recall
            </Link>{" "}
            Missing rivets are responsible for causing driver's side airbags to
            fail in the latest 750,000 vehicle recall from Honda.
          </li>

          <li>
            {" "}
            <Link to="/blog/womans-legs-amputated-after-serious-dog-bite">
              Dog Bite Injury Attorney Reacts to News of Woman Whose Legs Were
              Amputated After Dog Bite
            </Link>{" "}
            A woman in Texas tried to break up a scuffle between two family dogs
            when she suffered a dog bite. This resulted in an infection after
            which her legs and fingers had to be amputated.
          </li>

          <li>
            {" "}
            <Link to="/blog/iowa-care-facility-patient-deaths">
              Nursing Home Abuse Lawyer Reacts to Patient Deaths in Iowa Care
              Facility
            </Link>{" "}
            Several fines have resulted from the neglect and abuse of patients
            at The Golden Age Nursing and Rehabilitation Center in Centerville,
            Iowa
          </li>

          <li>
            {" "}
            <Link to="/blog/hospital-employees-flu-shot">
              Employment Lawyer Comments on Hospitals' Move to Fire Workers who
              Refuse to Take Flu Shots
            </Link>{" "}
            Hospitals are increasingly cracking down on their own employees who
            refuse to take flu shots.
          </li>

          <li>
            {" "}
            <Link to="/blog/boeing-dreamliners-grounded">
              Product Liability Lawyer Comments on the Grounding of Boeing
              Dreamliners.
            </Link>{" "}
            Authorities in the United States and Japan have ordered airlines to
            stop flying their new Boeing 787 aircraft until a fire risk linked
            to battery failures can be fixed.
          </li>

          <li>
            {" "}
            <Link to="/blog/dog-attack-lawsuit-verdict">
              Personal Injury Lawyers Secure $120,000 Jury Verdict in San Diego
              Dog Bite Case
            </Link>{" "}
            Contentious Dog Bite Case Results in Win for Victim.
          </li>

          <li>
            {" "}
            <Link to="/blog/propublica-foia-report">
              Nursing Home Abuse Lawyer Commends Group for Getting the
              Government to Release Uncensored Nursing Home Inspection Reports
            </Link>
            ProPublica, a nonprofit group put in a Freedom of Information Act
            (FOIA) following which the government released uncensored write-ups
            of problems found during nursing home inspections around the
            country.
          </li>

          <li>
            {" "}
            <Link to="/defective-products/metal-water-bottles">
              Product Liability Lawyer Comments on Metal Water Bottles that Can
              Seriously Injure Children
            </Link>{" "}
            NBC report warns consumers about popular metal water bottles that
            could cause children to get their tongues stuck inside. The report
            states that several children had to undergo complicated surgery in
            order to free their tongues.
          </li>

          <li>
            {" "}
            <Link to="/blog/gm-recall-vehicle-rollaways">
              Auto Defects | General Motors Recalls 2013 Vehicles That Could
              Roll Away
            </Link>{" "}
            GM has announced a recall of more than 68,000 new vehicles because
            they could roll away after being parked.
          </li>

          <li>
            {" "}
            <Link to="/nursing-home-abuse/convicted-felons">
              Endangering the Elderly | California Officials Housed Convicted
              Felons at Nursing Home
            </Link>{" "}
            Private prison company housed convicted murderer, rapist and two
            other felons at an Oklahoma nursing home.
          </li>

          <li>
            {" "}
            <Link to="/medical-devices/johnson-johnson-vaginal-mesh-trial">
              Johnson & Johnson Are Faced with Their First Vaginal Mesh Trial
            </Link>{" "}
            A South Dakota woman alleges the device caused her constant pain and
            has had 18 subsequent surgeries to fix the problem.
          </li>

          <li>
            {" "}
            <Link to="/head-injury/second-impact-syndrome">
              Head Injury Attorney Talks About Struggles of Second Impact
              Syndrome
            </Link>{" "}
            Second Impact Syndrome resulted from football injuries that caused
            Cody Lehe to fall into a coma with massive brain swelling and an
            irregular heartbeat.
          </li>

          <li>
            {" "}
            <Link to="/truck-accidents">
              Truck Accident Attorney Weighs in on Fatal Truck Accident
            </Link>{" "}
            Where the Trucking Company Had a History of Violations. A deadly
            chain reaction truck accident from a company that had a history of
            violations was cited at least 23 times.
          </li>

          <li>
            {" "}
            <Link to="/blog/toyota-billion-dollar-settlement">
              Bisnar Chase Auto Defect Attorney Reacts to Toyota's Record $1
              Billion Settlement
            </Link>{" "}
            California auto defect liability lawyer comments on settlement
            agreement in a class-action lawsuit [Case number: 8:10ML2151 JVS
            (FMOx) United States District Court for Central District of
            California] in which Toyota Motor Corp. has agreed to pay more than
            $1 billion.
          </li>

          <li>
            {" "}
            <Link to="/orange-county/hit-and-run-accidents">
              An increase in the number of hit and run accidents in Orange
              Countyis discussed with John Bisnar.
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/blog/money-for-lunch">
              Auto defects lawyer Brian Chase sits down with Money for Lunch
              talk radio to discuss the hidden dangers of auto defects and
              products liability issues
            </Link>
            Money for Lunch talk radio is a national radio syndication.
          </li>

          <li>
            {" "}
            <Link to="/giving-back/adopt-a-family">
              Bisnar Chase Partners with Share Our Selves During the Holidays to
              Support Orange County Families in Need
            </Link>{" "}
            Employees of the Newport Beach personal injury law firm participates
            in the Orange County nonprofit's 42nd annual Adopt A Family program
            giving a number of families the extra help they need during the
            holiday season.
          </li>

          <li>
            {" "}
            <Link to="/employment-law/wrongful-termination">
              California Employment Lawyer, John Bisnar, Reacts to Firing of
              Meteorologist
            </Link>{" "}
            Attorney John Bisnar talks about Rhonda Lee, a meteorologist for
            KTBS Louisiana news station, who was said to be fired after she
            responded to a comment about her hair left by a viewer on the
            station's Facebook page.
          </li>

          <li>
            {" "}
            <Link to="/blog/honda-rollaway-recall">
              Bisnar Chase Comments on Honda's Recall of 800,000 Vehicles for
              Rollaway Problems
            </Link>{" "}
            American Honda announced its plans to recall minivans and
            crossover-utility vehicles to address an issue that could cause
            these vehicles to slip out of park and roll away. According to a
            Dec. 12 NBC news report, the problem has resulted in injuries and
            incidents.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/student-bicycle-accident">
              California Bicycle Accident Lawyers Bisnar Chase React to Death of
              Student
            </Link>{" "}
            Christopher Weigl, a 23-year-old Boston University photojournalism
            student, was killed in a bicycle accident after a tractor-trailer
            making a wide turn struck him as he rode along the designated lane,
            a Dec. 6 news article in The Boston Globe reports.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/drunk-driving-crashes">
              Wrongful Death Attorney John Bisnar Weighs In On Drunk Driving
              Crashes
            </Link>{" "}
            California wrongful death attorney John Bisnar warns holiday drivers
            about the dangers of drinking and driving after an alleged DUI crash
            killed Dallas Cowboys' Jerry Brown early morning Dec. 8 2012.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/car-accident-settlement">
              Bisnar Chase: San Diego Jury Awards $160,000 to Car Accident
              Victim
            </Link>{" "}
            California car accident attorneys secure jury verdict for car
            accident victim who suffered head and neck injuries in a rear-end
            collision.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/chemotherapy-drug-recall">
              Bisnar Chase Weighs in on Recall of Drug Used to Prevent
              Chemotherapy- Induced Nausea
            </Link>{" "}
            California product liability lawyer discusses anti-nausea drug
            Zofran, which was removed from the market because of the potential
            for serious cardiac risks.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/fatal-hit-and-run-crash">
              Bisnar Chase Weighs in on Death of Woman Charged in Fatal Hit and
              Run Crash
            </Link>{" "}
            California hit-and-run lawyer comments about the death of
            50-year-old Danielle Halverson, who, according to a Dec. 3 news
            report in The Journal-Review, may have died of an overdose.
          </li>

          <li>
            {" "}
            <Link to="/giving-back/free-lakers-tickets">
              Bisnar Chase Donates Lakers Tickets to Special Needs Children
              Through Facebook Campaign
            </Link>{" "}
            The law firm is passionate about doing good in their community,
            especially with children.
          </li>

          <li>
            {" "}
            <Link to="/locations/torrance-hit-and-run-accidents">
              Arrest Made in Torrance Hit-and-Run
            </Link>{" "}
            A drug and alcohol counselor has been charged with manslaughter in
            the hit and run death of a 31 year old man.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/los-angeles-county-sheriffs-deputy-shoots-and-kills-pit-bull">
              Los Angeles County Sheriff's Deputy Shoots and Kills Pit Bull
            </Link>{" "}
            that Attacked Animal Control Officer - California dog bite lawyer
            discusses the importance of remaining vigilant about dogs roaming
            the neighborhood in light of a dog attack in La Mirada that left an
            animal control officer injured and another small dog dead.
          </li>

          <li>
            {" "}
            <Link to="/nursing-home-abuse/majesty-health-rehab-center">
              Government Report Blames Nursing Home for Elderly Patient's Death
            </Link>{" "}
            - California nursing home abuse lawyer weighs in on a report
            released by the South Carolina Department of Health, which blames an
            Easley nursing home for the death of an elderly resident.
          </li>

          <li>
            {" "}
            <Link to="/nursing-home-abuse/wild-mushroom-soup-fatalities">
              Two Nursing Home Residents Died after Consuming Wild Mushroom Soup
            </Link>{" "}
            - California nursing home lawyer voices his opinion on the tragic
            death of two seniors who died at a Loomis care facility after
            consuming soup that was prepared by their caregiver.
          </li>

          <li>
            {" "}
            <Link to="/nursing-home-abuse/lack-of-care">
              Disabled Teen Placed in Nursing Home Died from Alleged Lack of
              Care
            </Link>{" "}
            vehicles that left four people injured.
          </li>

          <li>
            {" "}
            <Link to="/press-releases/nebraska-wrongful-death-lawsuit">
              Nebraska Wrongful Death Lawsuit Invokes State's Fetal Death Law
            </Link>{" "}
            - Wrongful death attorney John Bisnar comments on the case.
          </li>

          <li>
            {" "}
            <Link to="/resources/march-air-reserve">
              Newport Beach Personal Injury Law Firm Donates Turkeys to Military
              at March Air Reserve Base
            </Link>{" "}
            - Bisnar Chase to donate turkeys for Thanksgiving to help March ARB
            members and their families facing financial difficulties.
          </li>

          <li>
            {" "}
            <Link to="/premises-liability/marriott-hotel-lawsuit">
              Lawsuit Alleges Marriott was Responsible for Fatally Infecting an
              Irish Tourist with Legionnaires' Disease.
            </Link>
            California wrongful death attorney weighs in on lawsuit (Case
            number: 012-M1-109345) filed by special administrator of a man who
            died from Legionnaire's disease.
          </li>

          <li>
            {" "}
            <Link to="/blog/nhtsa-investigates-honda-pilot-suvs-defective-brakes">
              NHTSA Investigates Honda Pilot SUVs for Potentially Defective
              Brakes
            </Link>{" "}
            -- California auto defect lawyer weighs in on a government probe
            into 88,000 Honda Pilot SUVs for faulty brakes. An Oct. 12 New York
            Times news report states that the vehicles may have a defect where
            the brakes may be applied without warning.
          </li>

          <li>
            {" "}
            <Link to="/newport-beach/worst-city-for-bicyclists">
              Newport Beach- One of the Worst Cities in State for Bike Accidents
            </Link>{" "}
            -- Newport Beach bicycle accident lawyer comments on the importance
            of making infrastructural changes and moving away from a "cars-only"
            mentality in order to prevent bike accidents in our communities.
            Newport Beach ranked among the most unsafe cities for bicyclists in
            California, according to study done by the California Office of
            Traffic Safety.
          </li>

          <li>
            {" "}
            <Link to="/blog/toyota-fire-hazard-recalls">
              Toyota Recalls 7.4 Million Vehicles for Fire Hazard
            </Link>{" "}
            -- California auto defect lawyer weighs in on Toyota Motor Corp.'s
            recall of 7.43 million vehicles worldwide for potential fire hazards
            involving faulty power window switches. An Oct. 10 article in The
            Wall Street Journal bills this recall as the largest ever for a
            single auto part.
          </li>

          <li>
            {" "}
            <Link to="/giving-back/one-thousand-turkeys-donated">
              A Thousand Free Turkeys, Food Items to be Donated in November to
              Area's Needy{" "}
            </Link>{" "}
            -- Second Harvest Food Bank and Bisnar Chase California Personal
            Injury Attorneys, LLP today announced they're teaming up to give
            away close to a thousand turkeys and other Thanksgiving food items
            this holiday season, to help wipe out hunger in Southern California
          </li>

          <li>
            {" "}
            <Link to="/newport-beach/hit-and-run-accidents">
              Man Arrested in Fatal Newport Beach Hit-and-Run Crash - Bisnar
              Chase Urges Community Involvement
            </Link>{" "}
            -- Newport Beach personal injury law firm Bisnar Chase encourages
            communities to join hands with local law enforcement and help
            apprehend hit-and-run drivers. Recent news reports show that a
            number of hit-and-run accidents in Southern California remain
            unsolved.
          </li>

          <li>
            {" "}
            <Link to="/auto-defects">
              Bisnar Chase: Recent J.D. Power Survey Reveals Lack of Public
              Awareness of Defective Car Seat Dangers;Low Safety Standards
              Linked to Catastrophic Injuries
            </Link>
            -- Attorneys say survey results troubling considering last year's
            $24.7 million judgment for quadriplegic woman injured by defective
            car seat in trial against Johnson Controls
          </li>

          <li>
            {" "}
            <Link to="/blog/brian-chase-lawyer-of-the-year">
              Brian Chase of Bisnar Chase Nominated for Prestigious CAALA Trial
              Lawyer of the Year Award
            </Link>{" "}
            -- Nation's leading personal injury attorney and auto defects expert
            adds CAALA Trial Lawyer of the Year nominee to growing list of
            accolades.
          </li>

          <li>
            {" "}
            <Link to="/giving-back/second-harvest-food-bank">
              Bisnar Chase, Second Harvest Food Bank and The Raise Foundation
              Donate 9,600 Pounds of Food to Orange County's Needy
            </Link>
            -- California personal injury law firm hands out fruits, vegetables
            and snacks, which served hundreds, during a food giveaway over the
            weekend in Laguna Hills. The firm partners with Second Harvest Food
            Bank and other local agencies to sponsor these food giveaways every
            other month.
          </li>
        </ul>
        <p>&nbsp;</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  ul#press-release-list {
    margin-left: 0;
    list-style-type: none;

    a {
      display: block;
      font-weight: 600;
      font-size: 1.6rem;
    }

    li {
      border: medium double ${({ theme }) => theme.colors.grey};
      padding: 1rem;
      margin-top: 1rem;
      border-radius: 2px;
      font-size: 1.2rem;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
