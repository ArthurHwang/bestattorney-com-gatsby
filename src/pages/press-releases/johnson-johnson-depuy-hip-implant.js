// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Attorneys Sue Johnson & Johnson for Prosthetic Hip Defects"
        pageDescription="Defective Johnson & Johnson Depuy products lawyers serving California injury victims since 1978. Receive a free no-obligation legal consultation by calling 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          {" "}
          <Link to="/press-releases/johnson-johnson-depuy-hip-implant">
            Bisnar Chase Personal Injury Attorneys Sues Johnson & Johnson for
            Prosthetic Hip Implant Defects
          </Link>
        </h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/DePuy-ASR-hip-recall.jpg"
          alt="Associates Day"
          className="imgright-fixed"
        />
        <p>
          The California personal injury attorneys of Bisnar Chase Personal
          Injury Attorneys (bestatto-gatsby.netlify.app) have filed a lawsuit
          against Johnson & Johnson Services, Inc.; Johnson & Johnson, Inc.;
          Depuy Orthopedics, Inc.; Vail Consulting, LLC; Thomas Vail M.D.; and
          Thomas Schmalzried M.D. The lawsuit alleges the defendants designed,
          distributed, manufactured, sold and marketed a prosthetic hip implant
          device known as the "ASR System," that was ultimately defective.
          Because of its alleged defects, plaintiff Kenneth Eichler developed
          painful and dangerous complications, will have to undergo unnecessary
          and painful revision surgeries, and will have lifelong residual
          problems as a result. The lawsuit seeks damages for sustained severe
          personal injuries, severe emotional distress, mental anguish, economic
          losses, court costs and other related damages. The case is pending in
          the Superior Court of California, Los Angeles County, case # BC459289.
        </p>
        <p>
          (
          <Link to="http://www.prweb.com/releases/2011/6/prweb8518622.htm">
            PRWEB
          </Link>
          ) June 02, 2011
        </p>
        <h2>
          Man Suffers Painful Effects of Alleged{" "}
          <Link to="/medical-devices/hip-implants">Defective Hip Implant</Link>
        </h2>
        <p>
          In June 2008, plaintiff Kenneth Eichler underwent total hip
          replacement surgery and was implanted with the ASR System, a
          prosthetic device designed to replace a patient's natural hip joint
          due to disease, deterioration or fracture.
        </p>
        <p>
          Shortly after surgery and as a result of the alleged defective nature
          of the prosthesis a component of the device eventually loosened and
          detached internally, creating metal debris inside the plaintiff's hip
          pocket. The lawsuit alleges that as a result of the device's failure
          and the subsequent detachment and metal fragments, the plaintiff
          suffers excruciating pain and the inability to walk and is now advised
          to undergo painful revision surgeries. Those surgeries consist of
          either some or all of the ASR components being surgically removed or
          "explanted" from the patient's body and replaced with new components.
        </p>
        <h2>Johnson & Johnson Hip Device Recalled Due to Defects</h2>
        <p>
          The lawsuit alleges data and information that only recently became
          known and publicly available shows that the ASR System had high rates
          of loosening, failure and dangerous metal debris release causing
          patients to develop complications to the point where they had to
          undergo revision surgeries.
        </p>
        <p>
          The lawsuit also alleges that the implanting surgeon felt the ASR
          System was "too challenging" due to its abnormally high risks of early
          failure, that it generated unusual and dangerous levels of metal
          debris accumulation in many patients' bodies, and that it caused other
          complications following surgery.
        </p>
        <p>
          The lawsuit goes on to allege that despite both actual and
          constructive notices of such problems and defects, the defendants
          continued to market, sell, promote and defend the defective device for
          years, never warning doctors or patients of its unacceptable risks.
        </p>
        <p>
          Finally, the suit claims that after selling the ASR System for more
          than five years during which time the defendants received and
          concealed repeated warnings of aberrantly high failure rates - they
          decided to recall the device in August 2010.
        </p>
        <p>
          "Hip replacement surgery, by its very nature, is invasive and
          painful," said Brian Chase, partner at Bisnar Chase Personal Injury
          Attorneys. "Surgeries that are successful require long recovery
          periods, let alone ones involving components with alleged defects.
          It's reprehensible to consider the defendants were aware of these
          defects, concealed these defects, and in turn, put innocent patients
          at risk of significant complications."
        </p>
        <p>
          The defendants are being sued for civil damages for alleged liability,
          negligence and fraud due to manufacturing and design defects, failure
          to warn and failure to recall and/or retrofit the ASR System.
        </p>
        <h2>About Bisnar Chase Personal Injury Attorneys</h2>
        <p>
          The Bisnar Chase Personal Injury Attorneys{" "}
          <Link to="/medical-devices/hip-implants">
            Orange County Depuy lawyers
          </Link>{" "}
          represent people who have been very seriously injured or lost a family
          member due to an accident, a defective product such as a medical
          device, or negligence throughout California. The law firm has won a
          wide variety of challenging serious injury defective product cases
          against a number of Fortune 500 companies. Get a complimentary copy of
          The Seven Fatal Mistakes That Can Wreck Your California Personal
          Injury Claim. For more information, please visit{" "}
          <Link to="/">bestatto-gatsby.netlify.app</Link>.
        </p>
        <p>
          {" "}
          <Link to="/medical-devices/hip-implants">
            Click Here For More Hip Implant Press Releases
          </Link>
        </p>
        <p>
          {" "}
          <Link to="/press-releases/defective-products">
            Click Here For More Defective Product Press Releases
          </Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
