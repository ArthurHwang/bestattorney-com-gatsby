// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  spanishPageEquivalent: "/abogados/accidentes-de-camiones"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/truck-accidents-california-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Truck Accident Attorney - Trucking Accident Lawyers"
        pageDescription="Contact the California truck accident attorneys of Bisnar Chase if you've been injured in a big rig, semi truck or commercial trucking or trailor accident. You may be entitled to compensation for the crash including lost wages and punitive damages for your injury claim."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california truck accident attorneys"
            title="california truck accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            The <strong> California Truck Accident Attorneys</strong> of{" "}
            <strong> Bisnar Chase</strong> have been representing injured
            victims of big rig, semi truck, 18-wheeler and tractor trailer
            accidents for <strong> over 40 years</strong>. Our law firm has the
            skill, resources and experience in taking on complex cases and
            winning maximum compensation for our clients involved in commercial
            truck crashes.
          </p>
          <p>
            Many top-rated law firms in the country do not want difficult cases
            that have the possibility of failure, but the legal staff at Bisnar
            Chase quite often take cases that were previously denied, and win
            them. The <strong> California Truck Accident Lawyers</strong> of{" "}
            <strong> Bisnar Chase</strong> have a{" "}
            <strong> 96% success rate</strong> and have won over
            <strong> $500 Million for their clients</strong>.
          </p>
          <p>
            You are probably here because you've been involved in an accident
            with a commercial or large truck and are wondering about your
            rights. Our skilled legal staff and{" "}
            <Link to="/" target="new">
              California Personal Injury Lawyers
            </Link>{" "}
            are here to help. After receiving medical care, the next important
            thing to do is secure your rights with a plaintiffs' truck accident
            attorney. Time will be an important factor in determining the value
            of your case.
          </p>
        </div>
        <LazyLoad>
          <img
            src="/images/text-header-images/semi-truck-accident-with-car.jpg"
            width="100%"
            alt="big rig truck accident lawyer"
          />
        </LazyLoad>

        <p>
          <strong>
            {" "}
            We've recovered over $500 Million for our personal injury clients
            and may be able to help you too
          </strong>
          . Truck accident cases can be complicated. Bisnar Chase have a long
          and successful track record of representing injured victims and their
          families obtain fair compensation for their losses.
        </p>

        <p>
          We know how trucking companies' insurers work and have been able to
          help victims secure compensation out of many challenging semi and
          truck accident cases. More than anything else, we are all about the
          passionate pursuit of justice for our clients. If you or a loved one
          has been injured in a California truck wreck involving a semi, tractor
          trailer or big rig, please contact us at
          <strong> 800-561-4887</strong> to obtain more information about
          pursuing your legal rights.
        </p>
        <p>
          <em>
            Truck accident lawyers John Bisnar and Brian Chase discuss the
            dangers of congested California highways filled with negligent or
            tired truck drivers and how to defend your rights in a truck
            accident case.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/mX8wMo8QTzo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Truck & Semi Accidents</h2>
        <p>
          Even careful and vigilant driving may not be sufficient to prevent a
          catastrophic truck accident. Very often, it is the occupants of the
          smaller passenger vehicle who end up suffering major injuries in a
          semi truck accident or other large commercial truck. These types of
          collisions could also result in fatalities. Those who are injured in
          large truck wrecks take weeks, months or even several years to recover
          from their injuries. Some of them may never recover due to the nature
          and extent of their injuries. It is important that truck accident
          victims and families of deceased crash victims understand their legal
          rights and options.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california dog bite injury treatment"
            style={{
              backgroundImage:
                "url('/images/text-header-images/causes-of-truck-accidents.jpg')"
            }}
          >
            <h2>Causes of California Truck Crashes</h2>
          </div>
        </LazyLoad>

        <p>
          There are many potential causes of California truck accidents. Some of
          the most common contributing factors include:
        </p>
        <ul>
          <li>
            <strong> Driver fatigue</strong>: Truck drivers are often required
            to work long hours. Truckers who fail to obey federal regulations
            regarding the number of hours they can work without rest are more
            likely to drive while drowsy. Under federal law, truck drivers are
            required to maintain logs detailing the number of hours they drove
            and the number of hours they rested.
          </li>
          <li>
            <strong> Excessive speed</strong>: Large tractor-trailers are hard
            to stop on flat roadways at reasonable speeds. Truck drivers who go
            too fast downhill or who exceed the speed limit run the risk of
            rear-ending vehicles in front of them or losing control on a turn.
          </li>
          <li>
            <strong> Poor vehicle maintenance</strong>: Truck owners must
            properly maintain their vehicles and drivers must inspect the truck
            before each long trip. Tire and brake failure are some of the most
            common causes of devastating truck accidents that can have an impact
            on other vehicles as well.
          </li>
          <li>
            <strong> Improper loading</strong>: The way a trailer is loaded can
            have a significant impact on how the truck handles. If a trailer is
            loaded too much to the front, back or side, it may cause the vehicle
            to veer out of control. Jackknifing accidents are usually caused by
            an overloaded or improperly loaded big rig.
          </li>
        </ul>
        <p>
          To learn more about semi-truck accident statistics you can visit{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2014"
            target="new"
          >
            FMCSA
          </Link>
          .
        </p>
        <h2>Determining Liability for a Truck Accident</h2>
        <p>
          One of the reasons why truck accident claims are more complicated than
          other claims is because of the many potential liable parties. The
          truck driver can be held accountable for his or her negligence. The
          trucking company is a potentially liable party in cases where
          negligent hiring or poor vehicle maintenance was involved. The truck's
          owner can also be held responsible for failing to properly maintain
          the vehicle. Even the truck's manufacturer can be held accountable if
          a{" "}
          <Link to="/auto-defects" target="new">
            defective truck part
          </Link>{" "}
          contributed to the crash.
        </p>
        <h2>Preserving the Evidence After the Crash</h2>
        <p>
          It will be necessary to thoroughly inspect the crash site, review the
          police report, the damaged vehicles and the driver's log.
          Unfortunately, unless the trucking company is contacted soon after the
          crash to preserve logs and records, much of it may be lost, destroyed
          or misplaced. For example, driver's logs, vehicle maintenance records
          and cell phone records can be valuable evidence when it comes to
          determining fault and liability for semi truck and other truck
          accident cases.
        </p>
        <p>
          That is why it is crucial for injured victims and their families to
          move quickly when it comes to protecting their rights. Trucking
          companies usually have insurance companies and defense attorneys at
          the scene working tirelessly to collect evidence and to look out for
          their best interests. Injured victims and their families also need a
          committed advocate on their side who will fight for their rights and
          protect their best interests.
        </p>
        <h2>The Value of Your Trucking Claim</h2>
        <p>
          It is important to understand the potential value of your claim
          because it is common for insurance companies to offer inadequate
          settlements. By accepting your first offer you are forfeiting your
          right to pursue additional compensation. In other words, if your
          settlement covers your immediate medical bills but doesn't compensate
          you for future treatment procedures, those financial burdens will fall
          onto your shoulders. You may have to pay for your own future expenses
          such as the cost of rehabilitation services or lost future wages.
        </p>
        <p>
          There are a number of ways to calculate the amount of damages you have
          suffered. A medical professional can give you an estimate of your
          medical bills, hospitalization fees and future treatment costs. You
          may be able to calculate on your own how much you have lost in terms
          of wages.
        </p>
        <p>
          Remember to consider lost future income, loss of earning potential and
          loss of livelihood, if your injuries prevent you from returning to
          work. You may need help, however, in determining the financial value
          of non-economic damages such as emotional losses such as pain and
          suffering. One way to do that is to compare your case to recent claims
          similar to yours.
        </p>
        <h2>Trusted Accident & Injury Attorneys</h2>
        <p>
          Our team of <strong> California Truck Accident Lawyers</strong> are
          here to represent you in this time of your life, when you need the
          support that you need. Our highly skilled and experienced legal staff
          and personal injury attorneys have been representing our clients for
          over 40 years and have left an impressive footprint in the legal world
          and justice system.
        </p>
        <p>
          Every client is a client for life, check out our{" "}
          <Link to="/about-us/testimonials" target="new">
            client interviews and video testimonials
          </Link>{" "}
          to see the positive feedback and how we changed these peoples lives.
        </p>
        <p>
          Contact our trusted team of <strong> injury lawyers at </strong>{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          if you've been injured in a truck accident. Big rigs and semi truck
          accidents need a qualified attorney who can get you maximum
          compensation. Call <strong> 800-561-4887</strong> for a{" "}
          <strong> Free consultation</strong>.
        </p>
        <p>
          {" "}
          <Link to="/car-accidents/rental-car-safety" target="new">
            Related: A Guide to Dealing With Rental Car Accidents
          </Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
