// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Taking Your Personal Injury Claim to Court"
        pageDescription="Taking your personal injury claim to court. What you should know. The decision of the judge will most likely rest on how well-prepared you are for your case and how strong your evidence is."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Taking Your Claim to Court</h1>
        <BreadCrumbs location={location} />

        <p>
          Once your "big day" in court arrives, the decision of the judge will
          most likely rest on{" "}
          <img
            src="/images/courtroom.jpg"
            className="imgright-fixed"
            alt="bisnar chase courtroom"
          />{" "}
          how well-prepared you are for your case and how strong your evidence
          is. Despite the prevalence of emotional court scenes on television,
          rarely if ever are judges swayed by strong emotional appeals. Instead,
          judges respect and give verdicts in favor of plaintiffs who have
          organized their materials and their arguments in a way which convinces
          the judge that the plaintiff has a true claim for damages.
        </p>
        <p>
          With this in mind, it is vital to do your homework and prepare your
          case. It is normal to be a bit nervous at appearing in court in front
          of a judge, but bear in mind that the better-prepared you are, the
          less nervous you will be. This means taking notes and making an
          outline of your case, having your evidence organized and ready to
          present, and keeping your appeal short and to the point. Lengthy
          arguments do not win trials; good evidence does. Spend some time
          writing down what you want to say; never "wing it" when you arrive. A
          good outline or notes will be your best friend when you arrive at
          court.
        </p>
        <h2>Interviewing Witnesses</h2>
        <p>
          Evidence can also include witnesses. You will have the opportunity to
          interview witnesses, and you may be interviewed by the opposing party
          or attorneys. In fact, in many cases, witness evidence is the only
          "real" evidence as to who caused the injury and the circumstances of
          the accident. Witnesses are a two-edge sword; good witnesses can
          bolster your case tremendously, but bad witnesses can make you look
          unprepared, or worse, untruthful.
        </p>
        <p>
          Choose your witnesses carefully. You may only be able to call
          relatives and friends; if you do, be sure to talk with them about the
          importance of staying unemotional and factual about the case. Good
          witnesses simply tell their stories in their own words briefly and
          straightforwardly.
        </p>
        <h2>Be Prepared for Court</h2>
        <p>
          When you arrive in court, you should have, at a minimum, two extra
          copies of your complaint, your service proofs, and any evidence you
          plan to present. It is helpful to have this information in a
          loose-leaf binder or other organizational framework so that you can
          easily find anything that is requested of you.
        </p>
        <p>
          Practice for your trial by having a friend or family member ask you
          questions about your case, and see how quickly you can find your
          supporting information in your file. If you cannot put your hands on a
          document or piece of information within one minute, it loses a lot of
          its value in arguing a case. Be very familiar with your paperwork
          before you ever walk into the courtroom.
        </p>
        <p>
          Spend some time talking to your witnesses. Like you, most people are
          nervous about appearing in court, even if they are being supportive of
          you. It helps both of you to talk a bit before the trial, go over the
          evidence to be presented, and try to anticipate questions the other
          side may ask of you or the witness. Do not "manufacture" evidence or
          testimony; this will come out during the hearing and make you look
          very bad. Truth is always the best option.
        </p>
        <p>
          In short, come to court prepared and do your best, and your case will
          go as well as possible.
        </p>
        <p>
          If you are planning to do it yourself,{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim"
          </Link>{" "}
          by John Bisnar is a highly recommended read.
        </p>
        <p>
          If you want assistance with your personal injury claim from an
          experienced attorney, call Bisnar Chase for a free case review today.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
