// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function CaseTimeLinePage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Car Accident Case Timeline"
        pageDescription="Interactive case timeline of the car accident personal injury case process"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Accident Case Timeline</h1>
        <BreadCrumbs location={location} />

        <h2 className="header-blue">Our Case Timeline</h2>
        <p>
          Below you will find an image of our car accident case timeline. Please{" "}
          <Link to="/contact">contact</Link> us if you have any questions.
        </p>

        <p className="nopc notablet" />
        <img
          id="static-timeline"
          height="auto"
          width="100%"
          className="timeline-small"
          src="/images/case-timeline-static.jpg"
          alt="Our Timeline Describing How A Typical Car Accident Case Proceeds"
          title="Contact Bisnar Chase for a Free Case Evaluation!"
        />

        <p>
          Please <Link to="/contact">contact</Link> us if you have any
          questions.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
