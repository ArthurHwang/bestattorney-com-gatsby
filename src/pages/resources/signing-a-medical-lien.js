// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function SigningMedicalLienPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Signing A Medical Lien - California Personal Injury Lawyers & Attorneys"
        pageDescription="Should I sign a medical lien? Call 949-203-3814 for highest-rated personal injury attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Should I Sign a Medical Lien?</h1>
        <BreadCrumbs location={location} />
        <p>
          {" "}
          In my experiences as a{" "}
          <Link to="/">California personal injury lawyer</Link>, signing off on
          a medical lien agreement could be one of the worst mistakes you can
          make when you have been injured in a car accident. A medical lien
          agreement with a health provider means that your personal injury
          lawyer is obligated to pay your medical expenses billed by the
          provider directly from the proceeds of your{" "}
          <strong>personal injury claim</strong>. This applies even if your
          settlement is lower than your medical bills or if you don't even
          receive a settlement for the injuries suffered during your car
          accident. Therefore one of the biggest advantages of not signing into
          a medical lien agreement is that you get to keep a larger portion of
          your <strong>accident injury claim</strong>.{" "}
        </p>
        <h2>
          Maximize Your Personal Injury Settlement and Get Better Healthcare
        </h2>
        <p>
          If you aren't awarded a personal injury claim and you are signed into
          a lien, things can get really grim. It has also been my observation
          that compared to a medical bill that is covered by an insurance
          company, the health care expenses tend to be much higher under a lien
          agreement. The lien agreement provides no oversight of the
          appropriateness of services. As such, doctors under lien agreements
          often charge more and sometimes even over treat their patients. They
          do so because they are more interested in securing a constant flow of
          income from their patients. So by not signing a lien, you can position
          yourself for lower healthcare costs in the event that you are not
          awarded a <strong>personal injury settlement</strong>. Of course, once
          a patient signs a lien agreement, it is very difficult to get out of
          it. My last book{" "}
          <Link to="/resources/book-order-form">
            The Seven Fatal Mistakes That Can Wreck Your California Personal
            Injury Claim
          </Link>{" "}
          provides several examples of such personal injury cases and{" "}
          <strong>car accident injury settlements</strong> and tackles the
          subject of medical lien agreements in much greater detail.{" "}
        </p>
        <h2>Consult a California Car Accident Injury Lawyer</h2>
        <p>
          In unfortunate event that you or your family members get injured in a
          car accident, you should be able to secure the best healthcare for you
          and your loved ones. This is essential not only because your family's
          health more important than anything else, but also because you are
          also required by law to get the proper healthcare so that the injuries
          suffered during the <strong>car crash</strong> do not get worse. If
          you fail to do so, and your injuries worsen, you will be held
          responsible for the worsening of your injuries and not the person who
          initially caused the injury. So if and when you are faced with such
          important decisions about various healthcare services and agreements,
          please consult a{" "}
          <Link to="/car-accidents">California car accident lawyer</Link> so
          that they can assist you in providing the best solution for the
          wellness of your family.
        </p>
        <p>
          If you would like to know more about how to go about when you are
          presented with a medical lien, please visit our{" "}
          <Link to="/car-accidents">"Do It Yourself Car Accident Claim"</Link>{" "}
          page. Or if you would like to know what is the best way to choose a
          lawyer that best fits your needs, feel free to visit our{" "}
          <Link to="/resources/hiring-an-injury-lawyer">
            "How To Choose A Lawyer"
          </Link>{" "}
          page.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
