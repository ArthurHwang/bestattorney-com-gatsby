// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function SpecialNeedsTrustsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Negotiation Tips When Dealing With Car Insurance Adjusters"
        pageDescription="Negotiation tips when dealing with your car insurance company. Dealing with the car insurance adjuster, settlement tips and advice from local car accident attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Negotiation Tips When Dealing With Car Insurance Companies</h1>
        <BreadCrumbs location={location} />

        <p>
          We would all do well to remember that car insurance companies are{" "}
          <strong>for-profit</strong> institutions. They are interested in
          making money, not protecting{" "}
          <Link to="/car-accidents">car accident victims</Link>. Because of
          this, it's important to keep your guard up at all times. Here are some
          general tips for dealing with car insurance adjusters.
        </p>
        <ol>
          <li>
            <strong>Be Detailed and Thorough</strong> -- Present the car
            insurance adjuster with documents that list the full extent of your
            damages and losses. The areas you will most likely want to cover are
            the cost of your car, lost wages, medical bills and extended care
            costs. You should also describe the extent of your pain and
            suffering.
          </li>
          <li>
            <strong>
              Let the Car Insurance Adjuster Make the Initial Offer
            </strong>{" "}
            -- The car insurance adjuster will want you to make the initial
            settlement demand first. If you make the first offer, then you are
            limiting the amount of your recovery from the beginning.
            <br />
            The car insurance company knows that you may very well ask for less
            than what the adjuster is willing to offer. Instead, insist that the
            car insurance adjuster makes the first offer, an offer that
            addresses all of your damages.
            <br />
            Once the insurance adjuster commits to an offer, tell him you'll
            need some time to think it over and to consult with family members
            or your lawyer. When you're ready, you can counter with your offer.
            Make the insurance adjuster sweat, and always act politely
            disappointed with whatever you are offered.
          </li>
          <li>
            <strong>Make Small Concessions</strong> -- Make small concessions
            when negotiating a personal injury settlement. Big concessions are
            generally taken as a sign that you are poor negotiator, and can lead
            to even more aggressive haggling by the insurance adjuster.
            <br />
            Making small concessions will also maximize your settlement,
            allowing for no unnecessary losses. Keep in mind that whenever you
            make even a small concession, you're demonstrating your flexibility
            and trustworthiness. This allows you to ask for concessions in
            return -- as an act of good faith.
          </li>
          <li>
            <strong>Take Your Time</strong> -- When it comes to negotiation,
            patience is definitely a virtue. Don't hurry the insurance adjuster
            to accept a quick settlement. It can make you seem desperate, which
            will put you on worse footing in negotiations. There are two
            effective ways to stall for time: Tell the adjuster you want to
            consult a lawyer before you accept or tell him that you want your
            injuries to heal before getting back to him with an answer.
          </li>
        </ol>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
