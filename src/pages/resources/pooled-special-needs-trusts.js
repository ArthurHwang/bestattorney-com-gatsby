// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function SpecialNeedsTrustsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Practical Legal Advice - Pooled Special Needs Trusts - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Advice on choosing a special needs trustee. Call 949-203-3814 for highest-rated personal injury attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Pooled Special Needs Trusts for Unmarried Clients Over 65</h1>
        <BreadCrumbs location={location} />

        <p>
          If a client is over the age of 65, settlements can greatly affect the
          client's benefits if the client is in poor health and receiving SSI,
          food stamps, Medicaid, Section 8 housing, Veterans' benefits,
          long-term care Medicaid assistance, HUD-subsidized housing for the
          elderly, or any other means-tested benefits. The settlement has the
          potential to affect the client's eligibility negatively unless careful
          planning is implemented.
        </p>
        <p>
          Pooled <strong>special needs trusts</strong> are a good option for
          preserving governmental benefits for single disabled adults over 65
          (not for married couples, as pooled trusts are not advisable). These
          kinds of trusts preserve assets for such individuals and seek to
          supplement their care and increase the quality of their lives.
          Becoming a member of a pooled special needs trust is an excellent
          planning option that can help maintain the beneficiary's security.
        </p>
        <p>
          Some of the disbursements available from the pool include guardianship
          fees, legal fees, hair care, podiatry, medically necessary
          transportation, attendant care and translators. A client receiving
          24-hour care in a nursing facility has pooled trust assets available
          for the client's care while Medicaid benefits are uninfluenced. A
          frequent example of how pooled trusts are disbursed is for nursing
          facility bed holds. This is necessary if a client experiences a
          prolonged hospitalization that would normally force the nursing home
          to make the bed available to another client, relinquishing the
          hospitalized client's spot and causing them to be discharged to an
          unfamiliar facility. Being displaced to a new facility could be
          traumatic for the disabled client, so disbursements of this kind are
          important. Other disbursment uses include preservation or maintenance
          of a beneficiary's home, including taxes, repairs, snow removal and
          lawn care.
        </p>
        <p>
          There is current debate over whether transfers to pooled special needs
          trusts for clients over 65 are legal under federal Medicaid policies,
          as specified in the Social Security Administration's Program
          Operations Manual (POMs). Each state implements Medicaid according to
          its own plan, so policies regarding clients over 65 vary. Some states
          impose penalties for transfers and being knowledgeable about what is
          allowable in the client's specific jurisdiction is important.
        </p>
        <p>
          See more{" "}
          <Link to="/resources/special-needs-trusts-distribution">
            advice on Special Needs Trusts
          </Link>
          .
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
