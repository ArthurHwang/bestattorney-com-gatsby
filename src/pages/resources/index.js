// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import {
  FaArrowAltCircleRight,
  FaBook,
  FaCar,
  FaCarCrash,
  FaGavel
} from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

// import BestBusinessBadge from "../../images/logo/best-personal-injury-lawyers-bestrateddotcom.png"
// import NAOPIABadge from "../../images/logo/NAOPIA-top-ten.png"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ResourcesPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Helpful Resources for Accident Victims - Bisnar Chase"
        pageDescription="Helpful resources to find out the legal process after a car accident or car accident statistics."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Helpful Legal Resources</h1>
        <BreadCrumbs location={location} />

        <div className="resources-page">
          <div>
            <h2 className="resource-category" id="resource-category-1">
              <FaCarCrash id="crash-icon" className="header-icon" /> Car
              Accident Resources
            </h2>
            <Link to="/resources/injured-in-car-accident" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  What To Do After A Car Accident{" "}
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Read this list of actions to take and NOT to take after being
                  in a car accident, to keep you safe and maximize your
                  potential compensation.
                </div>
              </div>{" "}
            </Link>
            <Link to="/car-accidents/who-is-at-fault" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Who is at Fault in an Uncommon Car Accident?
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Watch animations of unusual car accidents in which the car at
                  fault and the car with the right-of-way may not immediately be
                  obvious. Read our opinion on who might be considered in the
                  wrong and our references to the California State Law that
                  backs them up.
                </div>
              </div>
            </Link>
            <Link to="/resources/negotiation-tips" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Negotiation Tips When Dealing With Car Insurance Companies
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Insurance Companies are for-profit institutions, and often
                  value making money over protecting victims and ensuring fair
                  compensation. Read this list to see how you can most
                  effectively deal with insurance companies.
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/top-causes-of-car-accidents" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Top Causes of Car Accidents
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  This article lists the top causes of car accidents and how to
                  avoid them.
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/car-accident-statistics" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  OC Car Accident Statistics 2009-2014
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  A list of Orange County Car Accident Statistics as reported by
                  the California Highway Patrol's Statewide Integrated Traffic
                  Records System (
                  <Link to="http://iswitrs.chp.ca.gov/Reports/jsp/CollisionReports.jsp">
                    SWITRS
                  </Link>
                  )
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/case-timeline" target="_blank">
              <div className="resource-link nomobile notablet">
                {" "}
                <div className="link-row">
                  Car Accident Case Timeline
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  A info-graphic of the events and process of a car accident
                  personal injury case - whether filing a lawsuit or demanding a
                  settlement from an insurance company.
                </div>
              </div>
            </Link>
            <Link to="/resources/balance-billing" target="_blank">
              <div style={{ marginBottom: "2rem" }} className="resource-link">
                {" "}
                <div className="link-row">
                  The Legality of Balance Billing
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  California Emergency Room physicians are not allowed to
                  balance bill their patients, and there is legislation in place
                  to prevent patients from receiving surprise bills from
                  in-network hospital doctors that are not covered by insurance.
                </div>
              </div>
            </Link>
          </div>

          <div>
            <h2 className="resource-category" id="resource-category-2">
              <FaGavel id="gavel-icon" className="header-icon" /> The Legal
              Process
            </h2>
            <Link
              to="/resources/value-of-your-personal-injury-claim"
              target="_blank"
            >
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  What is the Value of my Personal Injury Claim?
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  A list of the 7 primary factors that affect the value of your
                  personal injury claim, and a description of circumstances that
                  can lead to a large settlement.
                </div>
              </div>{" "}
            </Link>
            <Link
              to="/resources/understanding-comparative-negligence"
              target="_blank"
            >
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Understanding Comparative Negligence
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Comparative negligence is a rule of law which is used in
                  shared-blame accident cases. Find out how courts assign
                  accident fault, and how this will impact your compensation
                  claim.
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/hiring-an-injury-lawyer" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  How to Hire a Personal Injury Lawyer
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  If you don't know anything about hiring a personal injury
                  lawyer, make sure you find the right people and ask the right
                  questions.
                </div>
              </div>{" "}
            </Link>
            <Link to="/faqs" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Personal Injury FAQs
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  For questions on the personal injury claim process, or to
                  learn more about how to file a claim, visit the FAQs.
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/signing-a-medical-lien" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Should You Sign A Medical Lien?
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Doctors and Hospitals may push you to sign a medical lien. We
                  recommend against signing a lien. Read this article for more
                  information.
                </div>
              </div>{" "}
            </Link>
            <Link
              to="/resources/preparing-your-personal-injury-claim"
              target="_blank"
            >
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Preparing Your Personal Injury Claim
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Read our article on how to best prepare for a personal injury
                  claim that you are filing yourself.
                </div>
              </div>
            </Link>
            <Link
              to="/resources/taking-your-case-to-small-claims-court"
              target="_blank"
            >
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Taking your Case to Small Claims Court
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  If you don't think your case will settle for a large amount of
                  money, the best thing you might be able to do is to take your
                  case to small claims court.
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/legal-dictionary" target="_blank">
              <div style={{ marginBottom: "2rem" }} className="resource-link">
                {" "}
                <div className="link-row">
                  Dictionary of Legal Terms
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  A list of many legal terms that are used in the personal
                  injury field. If you come across a word you don't understand,
                  find it here.
                </div>
              </div>{" "}
            </Link>
          </div>

          <div>
            <h2 className="resource-category" id="resource-category-3">
              <FaCar className="header-icon" /> Car Accident Prevention
            </h2>
            <Link to="/california-motor-vehicle-code" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  California Motor Vehicle Codes (CVCs)
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  View this list of California Motor Vehicle Codes to learn more
                  about safe driving.
                </div>
              </div>{" "}
            </Link>
            <div className="resource-link">
              <div style={{ fontSize: "1.6rem" }} className="link-row">
                List of Vehicles with Dangerous Defects
                <FaArrowAltCircleRight className="arrow-right" />
              </div>
              <div className="text-row">
                A list of cars with dangerous defects from the last 25 years.
              </div>
            </div>
            <Link to="/resources/top-five-car-safety-features" target="_blank">
              <div style={{ marginBottom: "2rem" }} className="resource-link">
                {" "}
                <div className="link-row">
                  Top 5 Car Safety Features
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  A list of 5 of the most important safety features in cars
                  currently.
                </div>
              </div>{" "}
            </Link>
          </div>

          <div>
            <h2 className="resource-category" id="resource-category-4">
              <FaBook className="header-icon" /> Misc. Resources
            </h2>
            <Link to="/blog" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Bisnar Chase Blog
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  We update our blog with stories about car accidents, defective
                  products, and other injury related news to keep you up to date
                  and safe from potential dangers.
                </div>
              </div>
            </Link>
            <Link to="/giving-back" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Giving Back
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Details of how Bisnar Chase has been giving back to our
                  community for years and investing resources in making the
                  world a better place. Go here to find our scholarship
                  opportunities!
                </div>
              </div>{" "}
            </Link>
            <Link to="/resources/book-order-form" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Personal Injury Handbooks and Guides
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  John Bisnar and Brian Chase have written guides and reference
                  books to help both injury victims and consumers of defective
                  products. Read more about them and find out how to order them
                  here!
                </div>
              </div>{" "}
            </Link>
            <Link to="/referrals" target="_blank">
              <div className="resource-link">
                {" "}
                <div className="link-row">
                  Attorney Referrals
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  If you're looking to generate additional income for your firm
                  and you don't have a personal injury department, you can refer
                  us your cases! We've paid out referral fees in excess of
                  $500,000 to several firms that have referred us qualified
                  cases!
                </div>
              </div>{" "}
            </Link>
            <Link to="/press-releases" target="_blank">
              <div style={{ marginBottom: "2rem" }} className="resource-link">
                {" "}
                <div className="link-row">
                  Bisnar Chase Press Releases
                  <FaArrowAltCircleRight className="arrow-right" />
                </div>
                <div className="text-row">
                  Read more about the cases that Bisnar Chase has won and the
                  ways that Bisnar Chase has been featured in the news.
                </div>
              </div>
            </Link>
          </div>
        </div>

        <p style={{ fontSize: "1.6rem" }}>
          {" "}
          <Link to="/resources/bisnar-chase-videos">
            <strong>Bisnar Chase Videos</strong>
          </Link>
          <br />
          See our latest videos on cases we've won, news related videos we've
          been featured in and personal injury Q&amp;A.
        </p>
        <div style={{ clear: "both" }} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .resources-page .resource-category {
    color: #21323f;
  }

  .resources-page .resource-link a {
    font-size: 1.6rem;
  }

  .resources-page .text-row {
    margin-bottom: 10px;
    font-size: 1.4rem;
  }

  .resources-page .resource-link .link-row {
    background-color: ${({ theme }) => theme.colors.secondary};
    /* padding-left: 10px; */
    color: ${({ theme }) => theme.colors.primary};
  }

  .resources-page .resource-link .link-row:hover {
    /* background-color: #e7deb5; */
    transition: color 0.5s;
    -moz-transition: color 0.5s;
    color: ${({ theme }) => theme.colors.accent};
  }

  .resources-page .resource-link .text-row {
    box-shadow: 0px 0px 50px -8px rgb(235, 226, 190) inset;
    padding: 10px 10px 10px 50px;

    @media (max-width: 500px) {
      padding: 10px 1rem;
    }
  }

  .resources-page .resource-link {
    outline: 1px solid #e8e6db;
  }

  #crash-icon {
    font-size: 3rem;
  }

  h2 {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .header-icon {
    margin: 0 1rem;
  }

  .resource-link {
    margin-bottom: 1rem;
  }

  .link-row {
    display: grid;
    grid-template-columns: 4fr 1fr;
    padding: 0.5rem 2rem;
    font-size: 1.4rem;
    .arrow-right {
      justify-self: flex-end;
      align-self: center;
    }

    @media (max-width: 500px) {
      font-size: 1.4rem;
    }
  }

  .arrow-right {
    color: ${({ theme }) => theme.colors.accent};
    margin-left: 2rem;
    font-size: 1.8rem;

    @media (max-width: 500px) {
      font-size: 1.4rem;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
