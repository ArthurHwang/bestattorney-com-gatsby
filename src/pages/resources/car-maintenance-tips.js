// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function CarMaintenancePage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="5 Car Maintenance Tips - Car Accident Lawyers & Attorneys"
        pageDescription="Read now for essential car maintenance tips. Call 949-203-3814 for car injury attorneys in Orange County,California. Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>5 Car Maintenance Tips</h1>
        <BreadCrumbs location={location} />

        <p>
          In{" "}
          <Link to="/resources/car-maintenance-tips">
            5 Tips for Car Engine Maintenance
          </Link>
          , we went over simple ways to maintain your engine in the summer
          months. Here we address 5 more ways you can keep your car running in
          the heat.
        </p>
        <ol>
          <li>
            <strong>Check Your Tires</strong> -- Hot weather is the most
            difficult condition for your vehicle's tires. If your tires have any
            significant wear, cracks, or other damage you should replace
            immediately. Beyond that, you should check the air pressure in all
            the tires including the spare. Feel free to read our{" "}
            <Link to="/resources/car-tire-maintenance-tips">
              tire maintenance
            </Link>{" "}
            article for more tips on this subject.
          </li>
          <li>
            <strong>Replace Your Windshield Wipers</strong> -- Rubber wiper
            blades typically last around one year due to the change in
            temperatures, hot to cold. If your wipers aren't providing you with
            a clear view during wet conditions, you should have them replaced.
          </li>
          <li>
            <strong>Wax Your Exterior</strong> -- Both the sun and summer smog
            can cause damage to your vehicle's paint. During your next car wash,
            you should make sure to have your vehicle waxed or sprayed with a
            paint protector to shield it from the harm of the sun's ultraviolet
            rays.
          </li>
          <li>
            <strong>Get a Sun Shade</strong> -- Nobody likes getting into a car
            that is baking inside -- so why not get a sun shade for your vehicle
            so the interior doesn't get so hot when it is parked in direct
            sunlight? Besides keeping your car at a much more manageable
            temperature, a sunshade will also protect your car's interior
            surfaces from fading or cracking.
          </li>
          <li>
            <strong>Protect Yourself</strong> -- Lastly, your car will only be
            as cool as the driver that's in it -- so make sure to protect
            yourself during the hot weather. A few summer time necessities
            include sunscreen for the arms (especially if you put your arm out
            the window), sunglasses for the eyes so you can always see well, a
            baseball cap if you have a convertible, and a bottle of water to
            keep you hydrated along the way.
          </li>
        </ol>
        <p>
          Keeping your car maintained will help protect you against car
          accidents and roadside breakdowns.
        </p>
        <p>
          Please see{" "}
          <Link to="/resources/automotive-consumer-help-advice">
            Automotive Consumer Help &amp; Advice
          </Link>{" "}
          for more articles.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
