// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Top Five Car Safety Features"
        pageDescription="Five most important safety features in cars to help motorists avoid catastrophic injuries and death."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Top Five Most Important Safety Features for Cars</h1>
        <BreadCrumbs location={location} />

        <p>
          An outline of the{" "}
          <strong>
            five most important safety features in cars to help motorists avoid
            catastrophic injuries and death
          </strong>{" "}
          on the heels of a report by the National Highway Traffic Safety
          Administration that shows U.S. traffic fatalities soared 13.5 percent
          in the first quarter of 2012, a significant increase over the same
          timeframe a year ago.
        </p>
        <p>
          According to the report, an estimated 7,630 people died in motor
          vehicle traffic crashes for the first quarter this year. That's a
          13.5% increase over 6,720 fatalities in the first quarter of 2011,
          according to NHTSA.
        </p>
        <p>
          "While certain safety features like electronic stability control and
          antilock brakes come standard on new cars, pay particular attention to
          used car safety equipment, especially on older models," said{" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link>, partner at
          Bisnar Chase and auto defects expert. "If you can, it's worth the
          added investment to pay more for a late model used car with advanced
          safety features, or a vehicle with added equipment versus a stripped
          down model."
        </p>
        <ol>
          <li>
            <strong>Electronic Stability Control</strong>
            <br />
            When a car goes in a direction different than the position of the
            steering wheel, Electronic Stability Control (ESC) uses steering
            angle sensors and wheel sensors to assist with braking, to help a
            driver maintain control. ESC was introduced in a few cars in 1995
            and is standard in all 2012 models.
          </li>
          <li>
            <strong>Antilock Brakes</strong>
            <br />
            Antilock braking systems (ABS) prevent wheels from locking up during
            emergency braking by automatically regulating brake pressure,
            enabling the driver to maintain control while helping the car stop
            in the shortest possible distance. In 1987, ABS came standard on
            some vehicles and optional on others. It's now standard on all 2012
            models.
          </li>
          <li>
            <strong>Side Curtain Airbags</strong>
            <br />
            During a side impact crash, side curtain airbags inflate to protect
            your head and/or your chest. Unlike front airbags, side curtain
            airbags may stay inflated for a longer period of time to protect
            occupants during a rollover. Side curtain airbags were introduced in
            1998 and are standard or optional on many new cars today.
          </li>
          <li>
            <strong>Rear Center Seat Lap/Shoulder Belts</strong>
            <br />
            Some vehicles are equipped with a lap belt in the rear center seat,
            not a shoulder and lap belt combination which is a much safer option
            in preventing severe injuries and death. Lap/shoulder belt
            combinations became law-mandated standard equipment in all rear
            center seats beginning in 2007.
          </li>
          <li>
            <strong>Collision Warning Systems</strong>
            <br />
            Today, there are a number of new cars that come equipped with
            advanced collision warning systems, including forward collision and
            lane departure warnings. Collision warning systems use radar to
            detect when vehicles or objects come within dangerous proximity to
            the vehicle and will issue audio or visual warnings to alert the
            driver of the hazard.
          </li>
        </ol>
        <h2>About Bisnar Chase Personal Injury Attonreys</h2>
        <p>
          Bisnar Chase Personal Injury Attorneys represent people who have been
          very seriously injured or lost a family member due to a California
          personal injury accident, car accident, defective product or
          negligence throughout the state. The auto defects law firm has won a
          wide variety of auto defect cases against many major auto
          manufacturers, including Ford, General Motors, Toyota, Nissan and
          Chrysler.
        </p>
        <p>
          Brian Chase, auto defects expert and partner at Bisnar Chase Personal
          Injury Attorneys is the author of the most up-to-date and
          comprehensive{" "}
          <Link to="/resources/book-order-form">auto defect book</Link>{" "}
          available today, Still Unsafe at Any Speed: Auto Defects that Cause
          Wrongful Deaths and Catastrophic Injuries.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
