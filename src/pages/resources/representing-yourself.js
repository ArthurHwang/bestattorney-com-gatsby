// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function RepresntingYourselfPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Representing Yourself in Personal Injury Matters"
        pageDescription="Many times smaller personal injury matters can be handled by the injured person quicker, more economically satisfying by negotiating the matter themselves."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Representing Yourself in Personal Injury Matters & Small Claims Court
        </h1>
        <BreadCrumbs location={location} />
        <p>
          Some personal injury matters are better handled by the injured party
          themselves rather than hiring an attorney or a law firm. Many times
          smaller personal injury or absolutely clear matters can be handled by
          the injured person quicker, more economically and, many times, more
          personally satisfying by negotiating the matter themselves or taking
          the matter to small claims court. Many times, even on absolutely clear
          matters, the insurance company will not pay the injured party what
          they will pay the same person represented by a skilled, well-known law
          firm.
        </p>
        <p>
          Say your personal injury claim has a true value of $6,000 and the
          insurance company is only willing to pay $4,000. Your choices are to
          accept the $4,000, hire an attorney or take the matter to small claims
          court yourself. Let us assume that the attorney of your choice was
          able to obtain the $6,000. After paying attorney's fees you are back
          to $4,000 (1/3 attorney fee); there is no financial gain from hiring
          the attorney. If you were to take the matter to small claims court and
          represent your case decently, you should recover $5,000 (the small
          claims court maximum recovery). You may also get more satisfaction
          having presented the case yourself and by being able to tell your side
          of the story in the face of the party who injured you, who also had to
          come to court. The chances are the case will be concluded quicker by
          going to small claims court.{" "}
          <Link to="#assistance">
            See below for small claims guides and assistance.
          </Link>
        </p>
        <p>
          Say your personal injury claim has a true value of $60,000 and the
          insurance company is only willing to pay you $25,000. Your choices
          then are to accept the $25,000 or hire an attorney. Assuming the
          attorney recovers the $60,000, after attorney's fees you'll have about
          $40,000, much better than accepting the $25,000. This is the type of
          situation where a skilled attorney can really make a difference for
          you. However, if the insurance company is willing to pay you $50,000
          and you do not have any health insurance reimbursement issues, you
          would be better off handling the matter yourself, at least
          financially, even though the insurance company is cutting you short.
        </p>
        <p>
          Say your personal injury claim has a true value of $60,000 and the
          insurance company is only willing to pay $25,000, their policy
          maximum, and you have underinsured motorist coverage on your
          automobile policy, be sure to get a consultation with a reputable law
          firm before accepting anything from the opposing party's insurance
          company.
        </p>
        <p>
          Say your personal injury claim has a true value of $250,000, you will
          need a reputable attorney in nearly every case. Get a consultation
          with a very experienced, reputable law firm before doing anything,
          even before giving a statement to anyone other than the police at the
          scene.. Then you can make an informed, intelligent decision.
        </p>
        <p>
          I strongly suggest seeking the advice of a reputable law firm before
          deciding to handle any claim yourself. Reputable law firms will give
          you an honest assessment of your claim, its value and discuss your
          options with you, including your option to handle the matter yourself
          and how to accomplish that. Most law firms, including mine, will
          provide the consultation and evaluation of your claim without any
          charge or obligation. On claims clearly worth $25,000 or less, or
          claims that are relatively clear, we will coach injured people in
          handling the negotiations themselves (without a fee), in order for
          them to obtain a fair settlement through negotiations with the
          insurance companies. If negotiations are not successful, we refer the
          injured party to an attorney we believe is appropriate for them and
          their case. If the case is large enough for us to make a significant
          difference for the client, we will offer our services.
        </p>
        <h3>
          {" "}
          <Link name="assistance" />
          Small Claims Court Assistance on the Web
        </h3>
        <p>
          {" "}
          <Link
            to="http://www.courtinfo.ca.gov/selfhelp/smallclaims/scbasics.htm"
            target="_blank"
          >
            http://www.courtinfo.ca.gov/selfhelp/smallclaims/scbasics.htm
          </Link>
        </p>
        <h3>Great book on small claims court:</h3>
        <p>
          <strong>Everybody's Guide to Small Claims Court in California</strong>
          <br />
          by Attorney{" "}
          <Link to="http://www.nolo.com/author.cfm/ObjectID/AA374A07-A680-4885-AD1836CCB9AC0890">
            Ralph Warner
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
