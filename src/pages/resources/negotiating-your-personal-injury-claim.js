// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function NegotiatingPersonalInjuryPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Negotiating Your Personal Injury Case - Bisnar Chase Injury Attorney's"
        pageDescription="Negotiation tips when dealing with your car insurance company. Dealing with the car insurance adjuster, settlement tips and advice from local car accident attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Negotiation Tips When Dealing With Car Insurance Companies</h1>
        <BreadCrumbs location={location} />

        <p>
          Negotiating a{" "}
          <Link to="/" target="_blank">
            California car accident claim
          </Link>{" "}
          will take some skill on your part. You will need to be aggressive,
          patient, smart, and composed. When and if you decide to settle your
          own personal injury claim it is important to keep these eight steps in
          mind.
        </p>
        <ul>
          <li>
            Submit your demand after the adjuster's first offer. Wait for the
            adjuster to give an initial figure. After that you may demand the
            policy limit or total your damages and send to the adjuster.
          </li>
          <li>
            Ask for more than you expect to get. Be sure to leave room for the
            adjustor to negotiate you down.
          </li>
          <li>
            Ask plenty of questions. Ask the adjuster what his authority is. Get
            specific figures as to how much he is allotted for each item. Do the
            math with him to confirm accuracy. For non-economic damages, ask how
            he arrived at the amount for pain and suffering. Keep notes too.
          </li>
          <li>
            Make small concessions. The best negotiators make all their
            concessions in small increments. This could work in your favor so
            that you receive a larger overall settlement.
          </li>
          <li>
            Be patient. Don't accept or counter an offer during the same
            meeting. If you try to push the adjuster toward a quick settlement
            he will sense you're in a hurry and desperate for money.
          </li>
          <li>
            Always maintain your composure. You can affect an outcome of a deal
            simply by your demeanor.
          </li>
          <li>
            Refrain from showing interest in the offer. Appear reluctant to
            settle. After all, California allows two years to settle a personal
            injury claim.
          </li>
          <li>
            Resort to a higher authority. Let the adjuster know that you've
            chosen not to make the final decision without assistance. Tell him
            that you will be consulting with a spouse or attorney friend.
          </li>
        </ul>
        <p>
          By following these eight guidelines you will be on your way to a
          better negotiation and higher settlement for your personal injury
          claim. Remember, it is always a good idea to consult with an
          experienced{" "}
          <Link to="/" target="_blank">
            California personal injury lawyer
          </Link>{" "}
          before starting your California injury claim. The best car accident
          lawyers will offer a free no obligation consultation.
        </p>
        <p>
          If you would like to know more about how to go negotiate your personal
          injury case, please visit our{" "}
          <Link to="/car-accidents">"Do It Yourself Car Accident Claim"</Link>{" "}
          page.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
