// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function CarAccidentStatisticsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Safety Features To Look For When Buying Used Car - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Important safety features you should be aware of when buying a used car. California personal injury lawyer Brian Chase discusses car safety issues in this video"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cautions to Take When Buying a Used Car</h1>
        <BreadCrumbs location={location} />

        <h2 align="center">
          ABC 7 News Special Safety Report with Brian Chase
        </h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/itLGXBQ5qBY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          {" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link> of Bisnar Chase
          Personal Injury Attorneys explains to ABC 7 News safety features you
          should look for when buying a used car.
        </p>
        <p>
          Recently, KABC here in southern California decided to do a feature on
          safety features in used cars and Binsar Chase's own Brian Chase was
          consulted as an expert. Their thesis was that if you want to buy an
          older, used car with certain safety features, you'd have to know when
          those safety features were widely installed in vehicles.
        </p>
        <p>
          For instance, the side curtain airbag wasn't installed into cars until
          1998, so any car manufactured beforehand wouldn't have them. Side
          curtain airbags can prevent your head from banging into the side
          windows of the car and can prevent you from being ejected out of the
          car, Chase said.
        </p>
        <p>
          Electronic Safety Control (ESC) is another extremely important safety
          feature in vehicles, but was only introduced to a few vehicles in
          1995. Consumer reports auto expert said they have, "Long been a big
          fan of stability control. We think it's the most important safety
          feature since the seatbelt." Anti-lock brakes, which pump the brake
          pads against the wheel much faster than a human could, were standard
          or optional in 1987.
        </p>
        <p>
          One of the biggest safety features in a vehicle is its seatbelts.
          Before laws mandated it, some back seats only had lap bands, which
          were responsible for several injuries and deaths. Until 1989 consumers
          didn't have the option of lap and shoulder bands. Chase said to make
          sure all of the seatbelts in your used car have a 3-point belt,
          meaning they have lap and shoulder belts for the utmost safety.
        </p>
        <p>
          Brian Chase is an expert in auto defects and has mountains of
          knowledge on the subject. He and his firm have crash tested many
          vehicles and found defects in seatbelts, seatbacks, SUV roofs, and
          many others.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
