// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ChangeBrakesPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="4 Signs You Need to Change Your Brakes"
        pageDescription="When should you change your brakes? Our Auto Help and Advice can answer many of your questions.Call 949-203-3814 for  auto accident injury attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>4 Signs You Need to Change Your Brakes</h1>
        <BreadCrumbs location={location} />
        <p>
          Your braking system is one the most crucial safety mechanisms in your
          car and can be the cause of{" "}
          <Link to="/car-accidents">serious car accidents</Link>. Your brakes
          and tires control where your car goes, and when it stops. The two are
          also the most prone to wear and tear. Here are four warning signs that
          you need to change your brakes:
        </p>
        <ol>
          <li>
            <strong>Soft Pedal</strong> -- Do you need to press down harder on
            the brake pedal than you used to in order to come to a complete
            stop? If so, you may have a problem. A couple possibilities are that
            air may have got into the brake lines and caused an improper
            bleeding in the system or that water got in and caused the brake
            fluid to boil before forming gases in the lines and reducing your
            stopping power. Regardless, if you have a soft pedal then you should
            get it checked by an auto mechanic as soon as possible.
          </li>
          <li>
            <strong>Squealing and Groaning</strong> -- If your brakes squeal or
            groan when you are driving faster than 6 miles per hour then it may
            indicate that your brake pads, calipers and rotors aren't a good
            match; your brake pads are worn out; or your brake pads are not
            properly fitted for your car. In any case, you should take your car
            to a trusted auto mechanic.
          </li>
          <li>
            <strong>Pulling and Vibrations</strong> -- If your brakes are
            pulling to one side, it could be unbalanced tire pressure or a
            problem with your brake caliper. If your tire pressures are fine,
            then the likely problem is that your brake caliper is sticking,
            leaking, or not sliding correctly because of corrosion. By fixing
            your brake caliper, you can increase the life of your brake pads and
            get better control over your steering.
          </li>
          <li>
            <strong>Grinding Noise</strong> -- Whenever you hear a grinding
            noise from your car it means trouble. Breaks are no exception. If
            you hear a grinding noise you should get off the road as quickly as
            possible and get your car towed for maintenance right away. The
            grinding noise from your brakes could be caused by harshly worn
            brake linings that are pushing against the rotors. Replacing rotors
            is very expensive, and the less wear on them the better.
          </li>
        </ol>
        <p>
          Put simply, if you ever notice an issue with your brakes you should{" "}
          <strong>get them checked right away</strong>. A problem with your
          brakes not only puts you in serious risk of getting in a car accident,
          but it also endangers anybody riding in your vehicle and those around
          you.
        </p>
        <p>
          Don't play around when it comes to changing your brakes -- always
          minimize your risk of car accidents and ensure your safety.
        </p>
        <p>
          Please see{" "}
          <Link to="/resources/automotive-consumer-help-advice">
            Automotive Consumer Help &amp; Advice
          </Link>{" "}
          for more articles.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
