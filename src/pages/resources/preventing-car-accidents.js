// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function PreventingCarAccidentsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Secret To Preventing Car Accidents - How To Prevent a Car Crash"
        pageDescription="Car accidents can be fatal! Learn how to prevent car accidents and avoid crashes. Call 949-203-3814 if you're in need of top rated car accident attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The Secret To Preventing Car Accidents</h1>
        <BreadCrumbs location={location} />
        <p>
          Having an understanding of{" "}
          <Link to="/resources/top-causes-of-car-accidents">
            what causes car accidents
          </Link>{" "}
          is the secret to avoid being involved in one. For example, if you know
          that 31 percent of all traffic deaths in the United States in the year
          2010 involved a drunk driver, you can greatly reduce your chances of
          being in an accident by not driving while impaired or getting in a car
          with an impaired driver. When you identify certain risky behaviors
          that cause or contribute to most car accidents, you can take steps to
          avoid those behavioral patterns and considerably reduce your risk of
          getting into a car accident. Learning about the most common causes of
          car accidents and acting on that knowledge can keep you and your
          family safe.
        </p>
        <h2>Stay Sober and Avoid Drunk Drivers</h2>
        <img
          src="/images/drink-drive.jpg"
          alt="don't drink and drive!"
          className="imgright-fixed"
        />
        <p>
          Even small amounts of alcohol can impair your physical and cognitive
          abilities. Furthermore, never get into a car with a driver who has
          been drinking. When on the roadway, if you see a vehicle that is
          swerving in traffic, keep your distance, pull over and call the
          authorities.
        </p>
        <p>
          The number of alcohol-related fatalities has decreased significantly
          over the years, but it is still tragically high. According to the
          National Highway Traffic Safety Administration (NHTSA), 10,228 people
          were killed in <Link to="/dui">alcohol-related accidents</Link> in the
          year 2010. That is 31 percent of all traffic deaths that year. In
          1982, however, drunk driving fatalities made up 52 percent of all
          traffic deaths. There have been many awareness campaigns and DUI
          crackdowns since 1982, so it is reasonable to assume that our
          awareness of the dangers of drunk driving has resulted in a decrease
          in deaths.
        </p>
        <h2>Stay Focused and Put Away Your Phone</h2>
        <p>
          Another way to avoid being in an accident is to keep your eyes and
          mind on the roadway. The number of distracted drivers on the roadway
          spiked when smart phones first became popular a few years ago. As
          awareness grows regarding how dangerous it is to text and drive, the
          number of distraction-related accidents has decreased slightly. It is
          still, however, one of the leading causes of injury accidents. NHTSA
          reports that 3,328 people were killed and about 421,000 were injured
          in distraction-related crashes in the year 2012.
        </p>
        <h2>Slow Down</h2>
        <img
          src="/images/slow-down.gif"
          className="imgright-fixed"
          alt="please slow down"
          width="200px"
        />
        <p>
          Under California law, drivers are required to operate their vehicles
          at or below the speed limit and drive at a speed that is appropriate
          given traffic, weather and roadway conditions. California Vehicle Code
          Section 22350 (Basic Speed Law) states: "No person shall drive a
          vehicle upon a highway at a speed greater than is reasonable or
          prudent having due regard for weather, visibility, the traffic and on
          surface and width of, the highway, and in no event at a speed which
          endangers the safety of persons or property."
        </p>
        <h2>Use Extra Caution Near Your Home</h2>
        <p>
          Another secret to preventing car accidents is knowing where they
          occur. A large number of car accidents happen near the home. When
          traveling on a street that you are familiar with, you are more likely
          to lose focus and relax. You know the streets. You know every turn and
          every dip. Using your familiarity as an excuse to lose focus can prove
          dangerous.
        </p>
        <p>
          Driving inattentively is always extremely dangerous no matter where
          you are. You must remain focused in case something unexpected occurs.
          It only takes a moment for a child to dart onto the street or for a
          car to turn from a driveway without yielding the right of way. If you
          want to reduce your chances of being in an accident, you must remain
          vigilant until you reach home safely.
        </p>
        <h2>Secrets to Surviving a Crash</h2>
        <p>
          Not all accidents are preventable. You can be traveling a safe speed
          in your lane of traffic when an out-of-control car veers into your
          lane. This type of accident is common and there is rarely enough time
          to react. Your chances of survival are relative to the speed and size
          of the vehicles and the safety devices in your car. Of course, what
          safety devices are in your vehicle will not matter if you don't use
          them.
        </p>
        <p>
          Make sure to always wear a seatbelt. According to The Centers for
          Disease Control and Prevention, seat belts reduce serious
          crash-related injuries and deaths by approximately 50 percent. Airbags
          help as well, but they are not effective if you are not wearing a seat
          belt.
        </p>
        <h2>Understanding Your Options</h2>
        <p>
          If you are{" "}
          <Link to="/resources/injured-in-car-accident">
            injured in a car accident
          </Link>
          , it is important that you take the steps necessary to protect your
          rights. Make sure you get the contact information from anyone who
          witnessed the accident and the insurance details from the other
          drivers involved. Notify the police, take photos of the crash site and
          seek out medical attention that same day. Last, but not least,
          research your options before making any legal decisions. Financial
          support may be available for all of the losses you have suffered.
        </p>
        <p>
          An experienced California car accident lawyer at Bisnar Chase can help
          secure fair and full compensation for all your losses. We have more
          than three decades of experience protecting the rights of injured
          victims and their families. Please contact us at 949-203-3814 for a
          free consultation and comprehensive case evaluation.
        </p>
        <p>
          Infographic: car accidents can be prevented including the fact that
          someone dies in a car accident every 13 minutes.
        </p>
        <img
          src="/images/car_accident_infographic.jpg"
          alt="The Secret To Preventing Car Accidents"
          // className="imgcenter-fluid"
          width="100%"
        />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
