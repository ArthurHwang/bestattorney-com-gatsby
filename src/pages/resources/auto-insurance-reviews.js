// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

// import BestBusinessBadge from "../../images/logo/best-personal-injury-lawyers-bestrateddotcom.png"
// import NAOPIABadge from "../../images/logo/NAOPIA-top-ten.png"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function AutoInsuranceReviewsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Auto Insurance Reviews"
        pageDescription="A listing of auto insurance resources. Call 949-203-3814 for auto accident injury attorneys, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, products liability, wrongful death & auto defects.  No win, no-fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Auto Insurance Reviews</h1>
        <BreadCrumbs location={location} />

        <p>
          NEW:{" "}
          <Link to="/premises-liability/property-damage">
            How to Handle Your Own Property Damage Insurance Claim
          </Link>
        </p>
        {/* <p>
          NEW:{" "}
          {" "}<Link to="/top-car-insurance-providers">
            Top Car Insurance Providers
          </Link>
        </p> */}
        <p>
          NEW:{" "}
          <Link to="/resources/auto-insurance-reviews">
            Auto Insurance Company Reviews
          </Link>
        </p>
        <p>
          NEW:{" "}
          <Link to="/resources/california-car-insurance-myths">
            Car Insurance Myths & Misconceptions
          </Link>
        </p>
        <p>
          NEW:{" "}
          <Link to="/truth-about-car-insurance">Truth About Car Insurance</Link>
        </p>
        <p>
          NEW:{" "}
          <Link to="/resources/california-car-insurance-myths">
            Car Insurance Myths & Misconceptions
          </Link>
        </p>
        <p>
          {" "}
          <Link to="/car-accidents">Farmers Insurance</Link> was established in
          1928 by John C. Tyler and Thomas E. Leavey. It is a personal lines
          property and casualty insurance group, providing homeowners insurance,
          auto insurance, life insurance and financial services in the United
          States. It is the third largest insurance group in the US servicing
          over 10 million households in 41 states. Farmers Group Inc. is
          headquartered in Los Angeles, California.{" "}
          <Link to="/car-accidents">More&hellip;</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
