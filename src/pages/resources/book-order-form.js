// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"
import folderOptions from "./folder-options"

// import BestBusinessBadge from "../../images/logo/best-personal-injury-lawyers-bestrateddotcom.png"
// import NAOPIABadge from "../../images/logo/NAOPIA-top-ten.png"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function BookOrderFormPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Order the Books - California Personal Injury Law Books"
        pageDescription="John Bisnar and Brian Chase are book authors. Order their personal injury books on Amazon."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Personal Injury Books Written by Bisnar Chase</h1>
        <BreadCrumbs location={location} />
        <p>
          Sometimes a little help may be all you need after an injury or
          accident. We've created accident help guides to answer questions you
          have about dealing with injury law in California.{" "}
          <Link to="/attorneys/john-bisnar"> John Bisnar</Link> and{" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link> have co-authored
          several books as well as writing stand alone books on the subject of
          personal injury law. These books can be purchased on Amazon in
          paperback.
        </p>{" "}
        <Link
          to="http://www.amazon.com/Seven-Mistakes-California-Personal-Injury/dp/1607251019"
          target="_parent"
        >
          <LazyLoad>
            <img
              src="/images/personalinjurybooksm.gif"
              alt="The Seven Fatal Mistakes That can Wreck Your California Personal Injury Claim - by John Bisnar"
              className="imgleft-fluid"
            />
          </LazyLoad>
        </Link>
        <p>
          {" "}
          <Link
            to="http://www.amazon.com/Seven-Mistakes-California-Personal-Injury/dp/1607251019"
            target="_blank"
          >
            <em>
              <strong>
                The Seven Fatal Mistakes That Can Wreck Your California Personal
                Injury Claim.
              </strong>
            </em>
          </Link>{" "}
          What do you need to know to effectively deal with an insurance company
          to settle your personal injury claim? The answers are in this book. In
          this book, John Bisnar explains in detail the pitfalls awaiting
          honest, fair-minded, self-represented personal injury victims. He
          shows how to avoid those pitfalls so as not to be victimized again.
          The Seven Fatal Mistakes is a must-read for personal injury victims
          before they work on settling their claim. It will save you time and
          keep you from leaving money on the table. The Seven Fatal Mistakes is
          the book that insurance companies hope you never read!
        </p>
        <div className="clearfix" />{" "}
        <Link
          to="http://www.amazon.com/Still-Unsafe-At-Any-Speed/dp/1615845755"
          target="_blank"
        >
          <LazyLoad>
            <img
              src="/images/autodefectbooksm1.gif"
              alt="Still Unsafe At Any Speed - by Brian Chase"
              className="imgleft-fluid"
            />
          </LazyLoad>
        </Link>
        <p>
          {" "}
          <Link
            to="http://www.amazon.com/Still-Unsafe-At-Any-Speed/dp/1615845755"
            target="_blank"
          >
            <strong>
              <em>Still Unsafe at Any Speed</em>
            </strong>
          </Link>
          . In 1965, then-unknown lawyer Ralph Nader wrote Unsafe at Any Speed,
          an expose of the automobile industry's disregard for consumer safety.
          Four decades later, car makers continue to cause needless human
          suffering by producing defective, unsafe vehicles. Auto defects cases
          cover a wide range of possibilities, and an attorney is well advised
          to pay close attention when someone has suffered a catastrophic injury
          or loss of a loved one in a vehicle accident. Did an SUV roll over? An
          airbag fail to deploy? A tire come apart? A fuel tank explode?
          Sometimes, attorneys mistakenly believe they do not have a case
          against a car manufacturer - or worse yet, don't even think about it
          to begin with.
        </p>
        <p>
          <span>
            <LazyLoad>
              <img
                src="/images/personal-injury/new-california-personal-injury-book-image.jpg"
                alt="Personal injury law book"
                className="imgleft-fluid lastbook"
                style={{ marginBottom: "2rem" }}
              />
            </LazyLoad>
          </span>
          <strong>NEW!</strong>{" "}
          <Link
            to="http://www.amazon.com/gp/product/1935411209"
            target="_blank"
          >
            <strong>
              <em>
                California Personal Injury Law - a reference guide for accident
                victims
              </em>
            </strong>
          </Link>
          . This reference guide is an overview of personal injury law, the
          personal injury claim process and the litigation of a personal injury
          case. It provides a step by step guide through the entire process from
          injury to settlement.
        </p>
        <p>
          This is an easy to read and understand guide meant to help you cut
          through the redtape and be compensated for your injuries. If you've
          had an accident or injury at the fault of someone else, we wrote this
          book for you!
        </p>
        <div className="clear" />
        <p align="center">
          If you still need help after reading our books, please give us a call
          at <strong>949-203-3814</strong> for a Free Case Consultation.
        </p>
        <div id="imgcenter-fluid-container" className="clearfix">
          <LazyLoad>
            <img
              src="/images/building-withsign.jpg"
              alt="Contact us"
              // className="imgcenter-fluid"
            />
          </LazyLoad>
        </div>
        <p style={{ textAlign: "center" }}>
          <strong>Call our 24-hour toll-free number 800-561-4887</strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* span img {
    float: left;
    margin: 5px 20px 20px 5px;
  } */

  a img,
  p img {
    width: 100px;
  }

  .lastbook {
    width: 119px;
  }

  #imgcenter-fluid-container {
    width: 100%;
    text-align: center;

    /* img {
      margin: 0 auto;
    } */
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
