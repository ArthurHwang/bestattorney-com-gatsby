// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

// import BestBusinessBadge from "../../images/logo/best-personal-injury-lawyers-bestrateddotcom.png"
// import NAOPIABadge from "../../images/logo/NAOPIA-top-ten.png"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function BCVideoPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Attorneys Videos"
        pageDescription="Bisnar Chase Personal Injury Attorneys video library. Find videos of our latest verdicts, news and more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bisnar Chase Videos</h1>
        <BreadCrumbs location={location} />

        <div className="grid-container-video-page">
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/wgdmqv922z8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />

          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/B7WQsT4pTqs"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />

          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/InG4KSRAI5Y"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
          <iframe
            src="https://player.vimeo.com/video/343278280/"
            width="560"
            height="315"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen={true}
          />

          <LazyLoad>
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/EDTq-8PZEpA"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/jKy3wa5M2yM"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/aPFADR23InA"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/414yNyALsPo"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/wmv1HKnhW7U"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/BhVPzkYsLuE"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/PS0XXH4Gz9g"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/LXnZuFkBXzE"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/EDTq-8PZEpA"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/nbnjoHTHLNQ"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/LV8aupBPuh8"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />

            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/ZkiMe2N2Nx8"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  h3 {
    font-size: 24px;
    color: #930;
  }

  .grid-item {
    width: 10px;
  }

  .grid-container-video-page {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 20px;
  }

  @media (max-width: 1240px) {
    .grid-container-video-page {
      grid-template-columns: 1fr;
      justify-items: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
