// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function CarTireMaintenancePage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="5 Car Tire Maintenance Tips To Keep You Safe"
        pageDescription="Tire Maintenance is important for preventing accidents.Call 949-203-3814 for highest-rated car accident attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>5 Car Tire Maintenance Tips To Keep You Safe</h1>
        <BreadCrumbs location={location} />

        <p>
          We all rely on our tires to keep us safe on the road. They keep us
          firmly planted with their tread, provide cushion from the bumps in the
          road, and are with us for thousands of miles at a time. Being a car's
          only connection to the ground, tires are an extremely important part
          of driving safety. It is relatively easy to maintain one's tires for
          optimal use, but it is a chore that is often ignored. You never know
          when a hazard may be headed in your direction, but by maintaining your
          tires you will be better prepared to avoid harm altogether.
        </p>
        <p>Here are some important tire maintenance tips:</p>
        <ol>
          <li>
            <strong>Proper Inflation</strong> -- Only 1 out of 7 drivers checks
            their tire pressure on a regular basis. Yet, keeping your tires
            properly inflated is easily the most important aspect of tire
            maintenance. If your tires aren't inflated correctly, it will be
            harder to turn, they will wear out faster, provide a bumpier ride,
            and even reduce your car's gas mileage!
          </li>
          <li>
            <strong>Regular Rotation</strong> -- It's common to rotate tires
            every 500-800 miles, but you should refer to the owner's manual to
            see the car manufacturer's specific recommendations. With that said,
            if you ever notice a problem with your tires, then you should get
            them checked right away by a qualified technician.
          </li>
          <li>
            <strong>Balancing/Alignment</strong> -- Proper balancing and
            alignment are necessary for you to have complete control over your
            car. If you ever notice an abnormal vibration, pounding, shaking, or
            pulling then you should see a tire technician immediately. These
            problems can be major safety issues.
          </li>
          <li>
            <strong>Repair/Replacement</strong> -- Legally, you need a certain
            amount of tread on your tires for them to be accepted on the road.
            This may vary from state to state, and you can check online to find
            regulations in your area. The "penny test" is a common way to gauge
            your tread depth and check if your tires need replacement. Put a
            penny into several tread grooves across the tire with Lincoln's head
            aimed downward. If Lincoln's head is always covered by the tread
            depth, you have more than 2/32" of tread depth remaining. You should
            also look into repairing or replacing your tires if there is damage
            to the sidewall, a bulge sticking out, or a deep cut in the rubber
            (even if the tire is still holding air). Any one of these issues
            could lead to a car accident.
          </li>
          <li>
            <strong>Don't Forget the Spare</strong> -- Lastly, you should always
            make sure your spare tire is accessible and properly inflated in
            case of emergency.
          </li>
        </ol>
        <p>
          Having bad tires is a common reason that car accidents occur, so
          please keep these simple tire maintenance tips in mind.
        </p>
        <p>
          Please see{" "}
          <Link to="/resources/automotive-consumer-help-advice">
            Automotive Consumer Help &amp; Advice
          </Link>{" "}
          for more articles.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
