// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function PreparePersonalInjuryClaimPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Preparing Your Personal Injury Claim - California Personal Injury Lawyers"
        pageDescription="How should I prepare for my personal injury claim? Call 949-203-3814 for highest-rated personal injury attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>How Should I Prepare For My Personal Injury Claim?</h1>
        <BreadCrumbs location={location} />

        <p>
          Personal injury victims often make the mistake of believing that an
          insurance adjuster is on their side. This is not the case. Insurance
          adjusters are well trained to withhold as much money as possible. This
          leaves the accident victim and the adjuster with conflicting
          interests. As an accident victim, you want to receive the highest
          settlement possible. If you decide to settle your own case without an
          attorney, you will need skill in two critical areas: preparation and
          negotiation.
        </p>
        <p>
          Before you can negotiate your claim you need to prepare yourself. You
          wouldn't take a trip to Europe without planning out all the details.
          This is true with a{" "}
          <Link to="/">California personal injury claim</Link>. To prepare for
          your negotiations ask and answer these five basic questions:
        </p>
        <ol>
          <li>What is the strength (evidence) of your claim?</li>
          <li>What are similar claims generally resolved for?</li>
          <li>What is your settlement goal?</li>
          <li>What is your settlement bottom line?</li>
          <li>What alternatives do you have if you don't settle?</li>
        </ol>
        <p>
          The strength of your claim lies in the evidence. This includes
          statements of witnesses, police reports, photos, and documentation of
          your losses. Losses include medical bills and any other expenses that
          stem from your accident.
        </p>
        <p>
          After you have evidence your next step is to prepare a cover letter.
          This letter is also referred to as a demand letter. Your cover letter
          will list damages and summarize the evidence. When preparing this
          letter be sure to ask for more than you would like to receive. This
          will give you room to negotiate the price down later.
        </p>
        <p>
          Research what similar claims have been resolved for. Find your local
          law library and make copies of the cases you find. Reference the cases
          when negotiating your claim. It may also be a good idea to consult
          with a{" "}
          <Link to="/car-accidents">California car accident attorney</Link> to
          get their opinion as well.
        </p>
        <p>
          Define a settlement goal. What is the amount you would like to walk
          away with (within reason)? After you have determined this you can
          focus your strategy around your settlement goal. Be prepared to
          justify your claim with evidence you have prepared.
        </p>
        <p>
          What is your settlement bottom line? In other words, what is the
          lowest dollar amount you will settle for? If this amount is denied you
          will need to seek alternative methods.
        </p>
        <p>
          <strong>If you do not settle what alternatives do you have?</strong>
        </p>
        <ul>
          <li>
            You could go to small claims court. This will usually take at least
            two trips to the courthouse plus a filing and service-of-process
            fee. The trial will take place six to eight weeks after you have
            filed. The judge could reward you more money, less money, or nothing
            at all from the trial. If the wrongdoer appeals the verdict you may
            have to do this process over again.
          </li>
          <li>
            You could wait and do nothing at all. In California you have two
            years to <strong>settle a claim or file a lawsuit</strong>. If you
            slow down the process an adjuster may be tempted to accept your
            bottom line number. Adjusters usually do not like their personal
            injury claims to pile up. They are judged on how much money they
            save the company as well as the speed in which they do so.
          </li>
          <li>
            Retain the services of a skilled California personal injury
            attorney. If you are not getting the results you want you can search
            for an experienced personal injury lawyer. Make sure the attorney
            you choose knows his way around the court room and has the respect
            of the judge. Ask him or her what their success rate is.{" "}
          </li>
        </ul>
        <p>&nbsp;</p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
