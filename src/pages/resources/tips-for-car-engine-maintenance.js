// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="5 Tips for Car Engine Maintenance - Legal Help & Advice"
        pageDescription="Simple Ideas for car engine maintenance. If you have been in an accident, call 949-203-3814 for high rated personal injury attorneys.Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>5 Tips for Car Engine Maintenance</h1>
        <BreadCrumbs location={location} />

        <p>
          If you are planning to use your car as a daily driver in the heat, or
          even if you have a big trip coming up, you should make sure everything
          is ready to go so you can keep your car looking, feeling and running
          cool this year.
        </p>
        <p>
          Here are 5 steps to help your car keep its cool so you can get not
          only avoid the wear and tear of a hot summer, but also stay
          comfortable while driving in the sunshine and avoid car accident
          emergencies.
        </p>
        <ol>
          <li>
            <strong>Change Your Oil</strong> -- Replacing your old oil offers
            much better lubrication protection for your car's engine and is an
            essential task for older vehicles. You should check your car's user
            manual to see how often you need an oil change -- and if there's any
            doubt as to when you last changed it, get one right away.
          </li>
          <li>
            <strong>Upgrade Your Coolant</strong> -- If your coolant level is
            too low, your engine may overheat and or even corrode. You should
            check your coolant and make sure the reservoir is always filled to
            the proper level. (Never open your radiator cap while the radiator
            is still hot.)
          </li>
          <li>
            <strong>Check Your Cooling Fan</strong> -- High mileage cooling fan
            clutches (100,000+ miles) are often weak, do not provide sufficient
            cooling and could cause the engine to overheat. You should either
            check your cooling fan yourself (by turning the A/C on max and
            seeing if the fan comes on) or take your car to a maintenance center
            to make sure it is working effectively.
          </li>
          <li>
            <strong>Check Your Air Conditioning</strong> -- Few people will miss
            their A/C during the winter months, but during the summer there are
            bound to be some days you need it to work. If your air conditioning
            isn't blowing cold air, your refrigerant charge may be low.
          </li>
          <li>
            <strong>Monitor Your Battery</strong> -- Hot weather is bad for car
            batteries, as the heat causes the battery to degenerate faster. No
            one likes to be stranded with a dead battery, so it's a good idea to
            keep and eye on yours. Batteries over 5 years old are more likely to
            fail, and many shops are willing to test your battery for free.
          </li>
        </ol>
        <p>
          Car engine failure can be a prelude to car accidents -- so don't
          neglect these important maintenance tips. Find more tips on our{" "}
          <Link to="/resources/car-maintenance-tips">
            5 Car Maintenance Tips
          </Link>{" "}
          page.
        </p>
        <p>
          Please see{" "}
          <Link to="/resources/car-maintenance-tips">
            Automotive Consumer Help &amp; Advice
          </Link>{" "}
          for more articles.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
