// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function DrivingSafetyTipsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="5 Ways to Increase Your Safety on the Road"
        pageDescription="5 ways to increase your safety on the road offers tips in dealing with other motor vehicle drivers and increase your safety."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>5 Ways to Increase Your Safety on the Road</h1>
        <BreadCrumbs location={location} />

        <ol>
          <li>
            <strong>Take Your Time</strong> -- A lot of auto accidents are
            caused by people who are impatient, distracted or late to an
            appointment. Leaving early, taking your time and knowing the route
            to your destination are good ways to avoid car accidents altogether.
          </li>
          <li>
            <strong>Make Sure You Are Insured</strong> -- The fact of the matter
            is, if you can't afford to insure your car, you shouldn't be
            driving. Car accidents are so common that the risk of driving
            without insurance is too great. Driving without coverage makes you a
            danger to yourself and your passengers, as well as all other drivers
            on the road. There are many alternative means of transportation and
            there is simply no excuse for driving without coverage.
          </li>
          <li>
            <strong>Raise Your Limits</strong> -- Most people just search for
            the cheapest car insurance coverage they can find. This is
            understandable, but those basic plans may not have enough coverage
            to protect you fully. I'd recommend a minimum of $100,000 per person
            and $300,000 per accident for bodily injury -- with the same limits
            for UM/UIM (Uninsured and Underinsured Motorist coverage) -- and up
            to $100,000 per accident for property damage. It's not expensive to
            increase your coverage, and it will provide you with phenomenal
            relief if you were ever involved in a serious car accident.
          </li>
          <li>
            <strong>
              Get Uninsured Motorist/Under Inured Motorist Coverage
            </strong>{" "}
            -- Uninsured and Underinsured Motorist (UM/UIM) coverage is an
            important addition to insurance coverage plans. If you're injured by
            a driver who is uninsured or underinsured, then he or she will be
            liable for the damages, but it could take years to collect. With
            UM/UIM, you'll have no such problems, as your own insurance company
            will protect you.
          </li>
          <li>
            <strong>Report Your Car Accident Right Away</strong> -- Lastly, if a
            car accident ever occurs, you need to make sure you handle it in the
            proper manner. By calling law enforcement right away and by filing a
            police report -- including the details of each car and individual
            involved -- you will have a much easier time dealing with your
            insurance company. For more information about what to do after an
            accident, read our{" "}
            <Link to="/resources/steps-after-a-car-accident">
              7 Steps After a Car Accident article
            </Link>
            .
          </li>
        </ol>
        <p>
          Please see{" "}
          <Link to="/resources/automotive-consumer-help-advice">
            Automotive Consumer Help &amp; Advice
          </Link>{" "}
          for more articles.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
