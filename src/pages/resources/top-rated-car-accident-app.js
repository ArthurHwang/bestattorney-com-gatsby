// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Top Rated Car Accident App Receives Upgrade"
        pageDescription="The Car Accident Sidekick - a mobile phone app,gets updated."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Top Rated Car Accident App Receives Upgrade</h1>
        <BreadCrumbs location={location} />

        <p>
          <em>
            Attribution: August 2012 -- This article is the syndication source
            of a{" "}
            <Link
              to="http://www.prweb.com/releases/2012/8/prweb9818209.htm"
              target="_blank"
            >
              recently released
            </Link>{" "}
            press release from Bisnar Chase.
          </em>
        </p>
        <p>
          <em>
            The Car Accident SideKick is the only auto collision documentation
            application with more than 10 ratings on the iTunes marketplace with
            a five star rating. The application was recently updated to
            accommodate a wider range of victims and provide additional
            resources in the event of a serious injury collision.{" "}
          </em>
        </p>
        <p>
          <img
            className="imgleft-fixed"
            src="/images/car-accident-mobile-app.png"
            alt="Car accident mobile app"
          />
          The{" "}
          <Link
            to="http://www.trafficcollisionattorneys.com/"
            title="Car Accident SideKick"
            target="_blank"
          >
            Car Accident SideKick
          </Link>{" "}
          is the only auto collision documentation application, with more than
          10 user ratings, on the iTunes marketplace with a five star rating.
          Having already developed a reputation for providing comprehensive
          documentation technology and resources to accident victims throughout
          the United States, the application recently updated to accommodate a
          wider range of victims and provide additional resources in the event
          of a serious injury collision.
        </p>
        <p>
          According to the National Highway Traffic Safety Administration
          (NHTSA), nearly 31,000 people lost their lives in United States auto
          collisions in 2009. When serious-injury collisions occur, people need
          to act quickly and decisively to care for victims, prevent additional
          injuries, and gather evidence. Those decisions will impact all of
          those involved and can be crucial to determine fault and whether or
          not the injured will be able to afford proper medical care by holding
          negligent parties accountable.
        </p>
        <p>
          The Car Accident SideKick mobile application was designed by{" "}
          <Link to="/">Bisnar Chase</Link> to help people involved in traffic
          collisions make the most educated choices at the moments which may
          matter most. Horrific car accidents can cause panic, which can cloud
          the minds of even the most seasoned emergency medical personnel.
        </p>
        <p>
          When properly utilized, the Car Accident SideKick has the capability
          to provide involved parties with emergency resources and step-by-step
          accident tools to help avoid common mistakes people make after a
          traffic accident.
        </p>
        <p>
          The Car Accident SideKick is free and is available for download
          through the Droid and iTunes marketplace. Those who utilize the
          software receive a number of assistance capabilities including:
        </p>
        <ul>
          <li>A step-by-step accident tip checklist</li>
          <li>One-touch emergency medical personnel contact</li>
          <li>
            Real-time accident report forms with photo and audio recording
            capabilities
          </li>
          <li>Exclusive one-touch insurance claims filing</li>
          <li>Built-in flashlight function</li>
          <li>"Best Attorney" locator enabled with GPS</li>
          <li>Update: New attorneys available in additional states</li>
        </ul>
        <p>
          John Bisnar, Orange County car accident lawyer, has seen the
          devastation associated with serious-injury auto collisions. "Since the
          inception of the Car Accident SideKick we have seen several positive
          reviews and hundreds of downloads. In addition to providing emergency
          tools, the app helps victims properly document the scene of their
          accident. We hope to help protect victims from future injuries and
          assist those who deserve to receive compensation for other drivers'
          negligence the types of tools needed to avoid an improper
          investigation," says Mr. Bisnar.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
