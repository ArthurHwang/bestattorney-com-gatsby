// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "bike-accidents/bicycle-on-street-pink-helmet.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Niguel Bicycle Accident Attorneys -  Bike Accident Lawyers"
        pageDescription="Were you injured in a bike accident? Please call 949-203-3814 for Laguna Niguel Bicycle accident attorneys who can help. Free consultations and we advance all costs throughout your case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Niguel Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Laguna Niguel bicycle accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Bisnar Chase's{" "}
          <strong> Laguna Niguel Bicycle Accident Lawyers</strong> have been
          working with victims who have suffered from serious injuries due to a
          bike crash for over <strong> 40 years</strong>. Distracted drivers
          need to be held accountable for their wrongdoing's.
        </p>
        <p>
          With the help of a bicycle accident attorney, you can collect damages
          for your injuries and recover payments for all your expenses connected
          to the accident. With our legal representation our clients have won
          over <strong> $500 million dollars in compensation</strong>.{" "}
        </p>
        <p>
          When you call <strong> 949-203-3814</strong> today you will receive a{" "}
          <strong> free consultation</strong> with a top-notch legal expert.
        </p>
        <h2>Bike Crash Statistics</h2>
        <p>
          Compiling available data on bike riders helps us to understand how and
          why bicycle accidents happen and hopefully prevent future suffering of
          bicycle riders.{" "}
        </p>
        <p>
          According to a survey conducted by the{" "}
          <Link to="https://www.transportation.gov/" target="_blank">
            {" "}
            United States Department of Transportation
          </Link>
          , in one year, there were at least 57 million people, or 23 percent of
          the population 16 or older, who biked at least once during the year,
          often in the summer.  One quarter of these riders rode for
          recreational or leisure purposes, while another quarter rode for
          health and exercise.  That means that 50 percent of the people riding
          bicycles at any given time are doing so as a form of recreation rather
          than transportation.  Only five percent of the people surveyed rode
          for transportation to or from work or school.
        </p>
        <p>
          Most bicycle rides consist of a mile or less close to home. {" "}
          <strong>
            {" "}
            This means that the 52,000 people injured and the 618 people killed
            in 2010 were primarily riding very close to home when the accidents
            occurred
          </strong>
          .
        </p>

        <LazyLoad>
          <img
            src="../images/bike-accidents/man-on-floor.jpg"
            width="100%"
            alt="Bicycle collision lawyers in Laguna Niguel"
          />
        </LazyLoad>

        <h2>5 Most Common Causes of Bike Accidents</h2>
        <p>
          {" "}
          Bicycles are different from motor vehicles in several respects.
          Bicycles do not offer much protection for riders making them
          vulnerable to dangerous weather conditions and reckless drivers. It is
          important for cyclists to pay attention to their surroundings and wear
          the proper bicycle gear to remain safe. Below are the top five causes
          of bicycle accidents and{" "}
          <Link
            to="https://www.safekids.org/tip/bike-safety-tips"
            target="_blank"
          >
            {" "}
            bike saftey tips
          </Link>{" "}
          to make sure you stay safe while riding.{" "}
        </p>
        <ol>
          <li>
            <strong> Distracted driving</strong>: Inattentive driving does not
            just include texting and driving. Distracted driving can also be
            putting on makeup, adjusting the radio or eating while driving.
            Drivers have the responsibility to keep themselves and other riders
            on the road safe.{" "}
          </li>
          <li>
            <strong> Speeding</strong>: Drivers are to remain cautious around
            pedestrians and bike riders, but cyclists are to do their part to
            remain safe as well. It is recommended that bikers not pedal as hard
            as they can around cars especially when turning. Excess speed can
            make it hard to stop when necessary. If you cannot come to a
            complete halt and you lose control, you can possibly collide into
            incoming traffic.{" "}
          </li>
          <li>
            <strong> Cycling too close to drivers or riders</strong>: Motorists
            are required to give cyclist at least three feet of space on the
            street. Riders should not assume that they will be given enough
            space by drivers. Many people suggest that using the side-walk may
            be a safer alternative for traveling by a cyclist. There are some
            areas where riders are prohibited and should look out for to avoid a
            ticket.{" "}
          </li>
          <li>
            <strong> Dangerous weather conditions</strong>: Driving in the rain
            is hazardous but riding a bike in the rain can be deadly. Not only
            are cyclists facing accidentally slipping into traffic, but they
            also run the risk of being hit by motorists. Statistics have shown
            that over 16% of crashes have been caused by weather conditions.
            Cyclists need to be weary of weather conditions that can be
            dangerous such as heavy rain, hail and snow.{" "}
          </li>
          <li>
            <strong> Lane merging</strong>: Drivers crashing into cyclists is a
            common occurrence for one primary reason: not signaling while
            turning. Motorists also need to pay attention to their blind spot.
          </li>
        </ol>
        <h2>Injuries you can Claim for your Laguna Niguel Bicycle Crash</h2>
        <p>
          Injuries a cyclist can obtain in a bike accident can be severe and may
          even leave some paralyzed. Many riders hit by cars lose their lives on
          the road as well. If you have suffered catastrophic injuries in a
          bicycle collision, you may be able to claim compensation. Below are
          the most common injuries that are claimed in a bicycle incident.
        </p>
        <p>
          <strong>
            {" "}
            Injuries that you can compensated for after a bike accident include
          </strong>
          :{" "}
        </p>
        <p>
          <strong>
            {" "}
            <Link to="/laguna-niguel/brain-injury" target="_blank">
              {" "}
              Traumatic brain injury{" "}
            </Link>
          </strong>
          : Even if a cyclist wears a helmet there is still a chance that they
          can suffer from major head injuries in a crash. The{" "}
          <Link
            to="https://www.cdc.gov/traumaticbraininjury/index.html"
            target="_blank"
          >
            {" "}
            Center for Disease Control and Prevention
          </Link>{" "}
          revealed that in one year, over 2.5 million people were admitted to
          the emergency room due to a traumatic brain injury. People who have
          suffered from an injury sustained from a bike accident should be
          rushed to the hospital if they are experiencing seizures, feeling
          drowsy or confused and if their pupils are dilating.{" "}
        </p>
        <p>
          <strong> Broken bones</strong>: What is not often popularized are
          cyclists suffering from broken bones on their face. If hit face first
          to the ground, riders may have to undergo facial reconstruction
          surgery due to breaking their nose, cheekbones or even chipping their
          teeth.{" "}
        </p>
        <p>
          <strong> Heavy scarring</strong>: Asphalt or concrete can cause
          serious scarring to a rider. They may suffer open wounds or deep cuts
          and may need stitches or surgery to heal the injury.{" "}
          <Link to="https://www.healthline.com/health/skin-graft" target="new">
            Skin grafting
          </Link>{" "}
          can be an option if the rider has lost a good amount of their
          epidermis from the accident. Road rash is another common injury that
          riders suffer from collisions as well.{" "}
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="bike crash lawyers in Laguna Niguel"
            style={{
              backgroundImage:
                "url('../images/text-header-images/text-header-gavel-money.jpg')"
            }}
          >
            <h2>Compensation for a Laguna Niguel Injury </h2>
          </div>
        </LazyLoad>
        <p>
          With the help of a{" "}
          <strong> Laguna Niguel Bicycle Accident Lawyer</strong>, you can
          collect damages for your injuries and recover payments for all your
          expenses connected to the accident. The law firm of Bisnar Chase has
          earned millions in recovery for accident victims and continues to hold
          a <strong> 96% percent success rate</strong> in winning injury cases.
        </p>
        <p>
          Our{" "}
          <Link to="/laguna-niguel" target="_blank">
            {" "}
            personal injury attorneys
          </Link>{" "}
          have over <strong> 40 years of experience</strong> helping injury
          victims fight against negligent parties and huge insurance companies.{" "}
        </p>
        <p>Our mission is :</p>
        <p>
          <strong>
            {" "}
            <em>
              "To provide superior client representation in a compassionate and
              professional manner while making our world a safer place."
            </em>
          </strong>
        </p>
        <p>
          Talk to a bicycle accident attorney in Laguna Niguel about your
          injuries today.
        </p>
        <p>
          When you call our law offices today at{" "}
          <strong>
            {" "}
            949-203-3814 you will receive a free legal advice on your injury
            case.{" "}
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
