// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/laguna-niguel-pedestrian-accident-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Niguel Pedestrian Accident Lawyers - Orange County Injury Attorneys"
        pageDescription="Were you injured in a Laguna Niguel pedestrian accident? Call 949-203-3814 for attorneys who can help. Free consultations. Over 40 years of results & expertise."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Niguel Pedestrian Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Laguna Niguel Pedestrian Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you are the victim of a pedestrian accident or you have a child who
          has been a victim, talk to the{" "}
          <strong> Laguna Niguel Pedestrian Accident Lawyers</strong> of Bisnar
          Chase.{" "}
        </p>
        <p>
          Bisnar Chase has been representing clients who have been severely
          injured by someone else's carelessness for over 40 years. We believe
          that you should not have to pay out-of-pocket for a{" "}
          <Link to="/laguna-niguel">personal injury </Link> that was not your
          fault.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Our legal representation has sustained a 96% success rate and has
            also won pedestrian accident victims millions of dollars in
            compensation
          </strong>
          .
        </p>
        <p>
          {" "}
          Contact the Law Firm of Bisnar Chase today and earn a free
          consultation.
        </p>
        <p>
          {" "}
          <strong> Call 949-203-3814</strong> to explore your rights as a
          pedestrian accident victim.
        </p>
        <h2>How Can a Laguna Niguel Pedestrian Accident Occur?</h2>
        <p>
          Pedestrian accidents can occur at any time or any location. Many
          accidents can be prevented, but for drivers who did not take
          precautions to prevent the accident, should be held accountable for
          their negligence. Pedestrians who were injured in an accident can be
          in the hospital for months and accumulate a hefty amount of medical
          bills.
        </p>
        <p>How do pedestrian collisions occur though?</p>
        <p>
          <strong> Pedestrian accidents happen due to</strong>:
        </p>
        <ul>
          <li>
            <strong> Crosswalks not being visible</strong>: When pedestrians
            cross the street, they are required to cross the street on a visibly
            marked pathway. If a motorist is speeding and does not see and yield
            to the crosswalk they may crash into the pedestrian. Liability for a
            pedestrian accident can fall on either a driver, government, city or
            state entity.
          </li>
          <li>
            <strong>
              {" "}
              <Link
                to="https://www.nhtsa.gov/road-safety/pedestrian-safety"
                target="_blank"
              >
                {" "}
                Pedestrian Safety{" "}
              </Link>{" "}
              is compromised more at night
            </strong>
            : Walkers who cross the street at night have an increased
            probability of being hit by a car. According to NHSTA, most
            pedestrians who had been killed by a driver were between the times
            of 6 P.M.- 9 P.M. Car lights shine only 160 feet ahead of the
            vehicle. This may seem like there would be enough light to spot a
            walker, but if a driver cannot spot a pedestrian on time it can lead
            to serious injuries. Night runners or walkers are advised to wear
            bright colors or reflective gear to signal their presence to
            drivers.
          </li>
          <li>
            <strong> Alcohol-use</strong>: Being under the influence of any drug
            while operating a vehicle is illegal. In one year, over 1.4 million
            motorists were arrested for driving drunk. Making the choice to
            drive while you are drunk is considered negligent and most
            importantly dangerous. If a drunk driver kills a pedestrian due to
            driving under the influence, then the surviving family members of
            the victim can file a wrongful death claim. Drunk driving can not
            only cost you a multitude of money, but it can also cost you jail
            time.{" "}
          </li>
          <li>
            <strong> Dangerous weather conditions</strong>: It can be difficult
            for drivers to see during heavy rain or heavy snowfall. Hazardous{" "}
            <Link
              to="https://ops.fhwa.dot.gov/weather/q1_roadimpact.htm"
              target="_blank"
            >
              {" "}
              weather events can impact roads{" "}
            </Link>{" "}
            tremendously. In a study conducted by the Federal Highway
            Administration, it has been reported that over 556,151 accidents had
            occurred due to heavy rainfall. Drivers must remain extra cautious
            during the rainy season because of the mixture of the water, oil and
            other car substances causing the road to be extra slippery.{" "}
          </li>
          <li>
            <strong> Drivers not paying attention to the road</strong>: There
            are times where pedestrians do not adhere to the rules and
            regulations of the road. Drivers are to be extra attentive due to
            people spontaneously crossing the street or jaywalking. The best way
            to assure that you will not hit a pedestrian is by not being
            distracted while driving. Distracted driving has contributed to over
            3,450 people being killed (NHSTA). Remember that being on your phone
            is not the only way to drive distracted. Eating or merely changing
            the radio station can be considered inattentive driving as well.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="../images/pedestrian-accidents/child-crossing-street-laguna-niguel.jpg"
            width="100%"
            alt="Laguna Niguel crosswalk injury lawyers"
          />
        </LazyLoad>

        <h2>Pedestrian Victims are More Likely to Be Children & Elders</h2>
        <p>
          The statistics on all pedestrian accident deaths and injuries are
          frightening, but the numbers regarding child victims of pedestrian
          accidents are especially disturbing.  Laguna Niguel pedestrian
          accident lawyers represent both adults and children who are the
          victims of pedestrian accidents. Children represent about ten percent
          of all pedestrian accident deaths and injuries; however, the children
          who are hurt or killed in pedestrian accidents tend to fall into very
          specific groups.
        </p>
        <p>
          Child pedestrian deaths are far more common in the late afternoon,
          according to Safe Kids.  Almost half of all child pedestrian deaths
          take place between 4:00 and 8:00 p.m., the hours after school lets out
          but before children are called in for the evening.  During these
          times, children are far more likely to be playing on the streets, so
          the sheer number of potential victims goes up.  This also coincides
          with the time that many people are returning home from work, so there
          is a higher number of people in a hurry to get home and perhaps not
          paying close attention to children playing in or around the streets.{" "}
        </p>
        <LazyLoad>
          <img
            src="../images/pedestrian-accidents/elder-child-crossing-street.jpg"
            width="100%"
            alt="Pedestrian collision lawyers in Laguna Niguel"
          />
        </LazyLoad>
        <p>
          Without a responsible adult to represent their interests, juvenile
          victims of pedestrian accidents could be powerless to recover these
          damages.  When a child does not have parents who are able or willing
          to help recover the damages, courts can appoint a guardian who will
          represent the best interests of the child and will talk to attorneys{" "}
          on his or her behalf.
        </p>
        <p>
          A Southern California personal injury lawyer can help parents get the
          right legal representation for their children if they are the victims
          of pedestrian accidents.  While a child has no legal
          &ldquo;rights&rdquo; under our system, a parent or guardian, or a
          person appointed by the court, can file a lawsuit on behalf of a child
          if he or she has been the victim of a personal injury accident.  When
          a child is injured in a pedestrian accident, he or she is depending on
          parents or other adults to do the right thing and help recover the
          damages due for medical bills, living expenses, and other costs.{" "}
        </p>
        <h2>Injuries that you Can Claim as a Pedestrian Collision Victim</h2>
        <p>
          When pedestrians cross the road, they are vulnerable to cars or large
          semi-trucks. Pedestrians that are involved in car collisions are left
          with catastrophic injuries and at times the accident can also be
          fatal. There are common injuries that pedestrians suffer from. Seek
          medical attention immediately to receive the proper treatment for your
          injuries.{" "}
        </p>
        <p>
          <strong> Soft tissue injuries</strong>: Strains, sprains and
          contusions are classified as soft tissue injuries. Soft tissue
          injuries are a result of a sudden blow to the body or a fall. Experts
          say that if you suffer from a soft tissue injury to use the RICE
          procedure. RICE is an acronym that Rest, Ice, Compression and
          Elevation.{" "}
        </p>

        <p>
          <strong>
            {" "}
            <Link
              to="https://www.cdc.gov/traumaticbraininjury/basics.html"
              target="_blank"
            >
              {" "}
              Traumatic brain injury and Concussions
            </Link>
          </strong>
          : The Center for Disease Control and Prevention reported that over 2.5
          million people suffered from a traumatic brain injury in one year. A
          traumatic head injury is classified as an abrupt blow to the cranium
          that affects the brain functioning normally. Medical professionals
          state that even if a brain injury is diagnosed as minor initially does
          not mean that there will not be complications later. It is important
          that if you are feeling symptoms such as confusion, anxiety or
          constant migraines that you seek medical attention right away.
        </p>
        <p>
          <strong> Emotional trauma</strong>: Being involved in an accident can
          be traumatizing. Victims of pedestrian accidents can come out of an
          incident with anxiety, depression or PTSD. If you have suffered from
          severe trauma after a pedestrian collision you deserve to have the
          resources to help you cope with your pain. Counseling and obtaining
          medication can be costly. The Laguna Pedestrian Accident Lawyers of
          Bisnar Chase are here to fight for the compensation you have a right
          to after your traumatizing experience. Call our law offices today for
          a free case analysis.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pedestrian accident lawyers in Laguna Niguel"
            style={{
              backgroundImage:
                "url('../images/pedestrian-accidents/laguna-niguel-pedestrian-accident-text-header.jpg')"
            }}
          >
            <h2> How to Prevent a Pedestrian Collision </h2>
          </div>
        </LazyLoad>
        <p>
          Experts say that for the past decades' pedestrian accidents have
          continued to increase. What is more alarming is the number of
          fatalities that have risen in the past years as well. There are ways
          to avoid being involved in a pedestrian accident though. When you make
          it a point to be safe you can prevent serious injuries and a multitude
          of problems thus after.{" "}
        </p>
        <ul>
          <li>
            <strong> Do not speed</strong>: Slowing down around pedestrians can
            increase your chances of avoiding a pedestrian accident. You never
            know if a walker will cross the street unpermitted. Being aware of
            your surroundings and going at a steady speed will give you enough
            time to brake just in case any unpredictable incidents take place
          </li>
          <li>
            <strong> Make eye contact with pedestrians</strong>: Before a
            pedestrian crosses the street make eye contact with them so you both
            acknowledge each other's presence and actions. Eye contact acts as a
            communicator on whether you or the pedestrian should proceed
            forward.
          </li>
          <li>
            <strong> Be especially careful around school zones</strong>: Around
            school zones be extra cautious when driving and always adhere to the
            crossguards directions. Also, be courteous and patient while driving
            alongside school buses. School bus drivers have difficulty seeing
            and may not be able to brake on time if caught off guard.
          </li>
          <li>
            <strong> Have patience with elderly cross walkers</strong>: Senior
            citizens may proceed slowly into a crosswalk because their body
            functions may not be up to par. Experts say that when a person
            reaches 65 years of age, they may also have extreme difficulty with
            their sight and may not see where they are going. Be patient and
            allow the elder to take his/her time.
          </li>
          <li>
            <strong>
              {" "}
              Do not drive around cars that are slow or completely stopped
            </strong>
            : There may be a reason why the car in front of you has completely
            come to a halt. If you proceed around them because you are rushing,
            you may strike a pedestrian that you did not see before or
            potentially be involved in a{" "}
            <Link to="/laguna-niguel/car-accidents" target="_blank">
              {" "}
              car accident
            </Link>
            .{" "}
          </li>
        </ul>
        <h2>Hire a Pedestrian Accident Lawyer in Laguna Niguel</h2>
        <LazyLoad>
          <img
            src="../images/pedestrian-accidents/cypress-pedestrian-accident-image.jpg"
            width="100%"
            alt="Laguna Niguel pedestrian accident legal representation"
          />
        </LazyLoad>
        <p>
          Determining fault for a crosswalk injury can be complicated. With one
          of our<strong> Laguna Niguel Pedestrian Accident Lawyers</strong>, you
          can win the compensation you deserve as a driver or a pedestrian.
          Under California Law pedestrians always have the right of way. Drivers
          who do not abide by this law should be held accountable for their{" "}
          <span>negligence.</span>
        </p>
        <p>
          The law firm of Bisnar Chase has won over{" "}
          <strong> $500 million dollars in compensation for victims</strong> who
          have been left with catastrophic injuries after an accident. A Laguna
          Niguel pedestrian accident attorney can clearly indicate liability for
          your accident and get you the compensation you need to recover.
        </p>
        <p>
          <strong>
            {" "}
            Call The Personal Injury Law Firm of{" "}
            <Link to="/">Bisnar Chase </Link> today at 949-203-3814
          </strong>
          .{" "}
        </p>
        <p>
          <strong>
            {" "}
            When you contact us today you will receive a free consultation for
            your injury case.
          </strong>{" "}
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
