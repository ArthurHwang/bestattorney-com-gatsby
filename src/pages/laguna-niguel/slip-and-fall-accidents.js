// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "slip-and-fall/slip-fall-laguna-niguel.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Niguel Slip & Fall Attorneys - Premise Liability Lawyers"
        pageDescription="Victims of slip and falls may be entitled to compensation for their injuries. Contact the Laguna Hills slip and fall attorneys of Bisnar Chase for a free case review. We have recovered hundreds of millions for our clients throughout Orange County."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Niguel Slip & Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Laguna Niguel Slip & Fall Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Slip-and-fall accidents have the potential to result in serious
          injuries that may require extensive medical treatment and
          rehabilitation. The experienced Laguna Niguel slip-and-fall lawyers of
          Bisnar Chase can help prove fault and liability in slip-and-fall
          cases.{" "}
        </p>
        <p>
          Our <Link to="/laguna-niguel">personal injury </Link> attorneys have
          more than four decades of experience representing the rights of
          accident victims and their families. To find out how we can help you
          seek and obtain fair compensation for your losses, please contact our
          offices to schedule your free consultation.
        </p>
        <p>
          Call <strong> 949-203-3814</strong> to speak to a top-notch legal team
          expert.
        </p>
        <h2>What Does the Term Premise Liability Mean?</h2>
        <p>
          Premise liability is a term used amongst personal injury victims that
          states that a person experienced great pain and suffering due to a
          property owners' negligence. Make note that just because you were
          injured on someone else's property does not mean that the owner of the
          premises is to blame for injuries of slip and falls such as broken
          bones or a brain injury.{" "}
        </p>
        <p>
          At times liability may fall on the caretaker of that property such as
          a store manager.
        </p>
        <p>
          In order to prove that there was carelessness on the part of the
          property owner, it{" "}
          <strong>
            {" "}
            needs to be proven that the owner knew about the problem and did not
            take measures to rectify the problem
          </strong>
          .
        </p>
        <p>
          <strong>
            {" "}
            Different types of{" "}
            <Link
              to="https://en.wikipedia.org/wiki/Premises_liability"
              target="_blank"
            >
              {" "}
              premise liability{" "}
            </Link>{" "}
            claims can include
          </strong>
          :{" "}
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/orange-county/amusement-park-accident-lawyer">
              Amusement park incidents{" "}
            </Link>
          </li>
          <li>Fires</li>
          <li>Swimming pool accidents</li>
          <li>Flooding </li>
          <li>Elevator or escalator accidents </li>
          <li>Snow or rain accidents</li>
          <li> Unstable staircase accidents</li>
        </ul>
        <LazyLoad>
          <img
            src="../images/slip-and-fall/store-slip-fall-image.jpg"
            width="100%"
            alt="Slip and fall accident lawyers in Laguna Niguel"
          />
        </LazyLoad>
        <h2>Where do Slip and Falls Usually Occur?</h2>
        <p>
          Many establishments aim to prevent any premise liability incidents due
          to the number of fees that can accumulate from a lawsuit and the
          business's credibility could be jeopardized. No matter the fact,
          property owners are accountable for injuries that take place on their
          property. There are various kinds of businesses and locations where
          slip and fall accidents commonly take place.
        </p>
        <p>
          <strong> Slip and falls usually take place at</strong>:{" "}
        </p>
        <p>
          <strong> Department or Grocery Stores</strong>: Research has indicated
          that over eight million people in one year have been admitted to the
          emergency room due to slip and falls. It is the duty of the store
          manager or supervisor to make sure customers are safe while shopping.
          Department stores can be held liable for slip and falls, for example,
          a puddle of water was water leaking from a fridge and the supervisor
          was informed. If the supervisor was informed and did not take measures
          to clean up the puddle or merely put a "caution&rdquo; sign, then the
          supervisor actions could be negligent. Common slip and fall locations
          that occur in a store are at the entrance of the store or a place in
          the store that has poor lighting.
        </p>
        <p>
          <strong> Slippery Parking Lots</strong>: Property owners have the
          responsibility to make sure through harsh weather conditions, clients
          can proceed through a parking lot safely. Parking lots are to be
          cleared of excess snow or puddles of water to prevent customer
          injuries. The{" "}
          <Link
            to="http://clmmag.theclm.org/home/article/AEI%20snow%20slip%20and%20falls"
            target="_blank"
          >
            {" "}
            Natural Accumulation
          </Link>{" "}
          <Link
            to="http://clmmag.theclm.org/home/article/AEI%20snow%20slip%20and%20falls"
            target="_blank"
          ></Link>{" "}
          law reinforces that property managers should properly obtain the area
          in which they own. If there were precautions a customer could have
          taken{" "}
          <Link
            to="http://www.ltcc.edu/_resources/pdfs/campus_healthandsafety/walkingsafelyonice.pdf"
            target="_blank"
          >
            {" "}
            to be safe in ice
          </Link>{" "}
          or slippery surfaces, and they fell because of their own failure to be
          safe then this can reduce the amount that is obtained in a settlement.
        </p>
        <p>
          <strong> Construction Sites</strong>: Liability for construction site
          slip and fall injuries can either belong to the construction site
          owner or the supervisor of the construction site. Poor judgment on the
          supervisor's end can lead to a construction worker filing a claim for
          an injury. Workers who have experienced this type of slip and fall can
          also file for workers compensation. If the employer refuses to
          compensate for the injury and blames the incident on the worker, the
          employee has a right to explore their legal options.{" "}
        </p>
        <p>
          Property owners can prevent slip-and-fall accidents by staying on top
          of the hazardous conditions on their premises. Workers should
          immediately clean up wet areas, signs should be put up around
          hazardous conditions and damaged areas should be repaired as soon as
          possible. Pedestrians can help prevent such accidents by remaining
          attentive. However, when a slip-trip-and-fall accident occurs as a
          result of someone else's negligence, it is important that you protect
          your rights from a personal injury lawyer.{" "}
        </p>
        <h2>Laguna Niguel Indoor Slip-and-Fall Accidents</h2>
        <p>
          There are many hazardous conditions that can result in a slip-and-fall
          accident. A few of the most common causes of indoor slip-and-fall
          accidents include:
        </p>
        <ul>
          <li>
            Spilled liquids: Wet floors can result in sudden and violent falls.
            It is the responsibility of property owners and managers to act
            quickly to clean up slick areas. Were the wet conditions reported to
            an employee? For how long did the property manager know about the
            wet floor before he or she had it cleaned up? Were visitors warned
            about the spill?
          </li>
          <li>
            Recently cleaned floor: Workers who have recently mopped a floor
            must post wet floor signs. Otherwise, pedestrians may walk over the
            wet conditions without knowing that they are in danger of slipping.
            Soap, wax and water are the cause of thousands of injuries each
            year. It is crucial that visitors and workers are aware of slippery
            walkways and hallways so that they can walk carefully or avoid the
            hazardous conditions altogether.
          </li>
          <LazyLoad>
            <img
              src="../images/slip-and-fall/uneven-floors-laguna-niguel.jpg"
              width="100%"
              alt="Laguna Niguel slip and fall lawyers"
            />
          </LazyLoad>
          <li>
            Uneven floors: Some walkways are simply not safe. If not properly
            maintained, walkways, paths and hallways may become cracked or
            uneven. Individuals can trip or stumble over these types of
            conditions.
          </li>
          <li>
            Debris: It is easy to trip over debris. Boxes, papers, folders and
            items left lying on the floor or along hallways can cause
            individuals to trip and fall.
          </li>
          <li>
            Torn carpets: Bumps, tears and dips in a carpet can cause tripping
            accidents. These types of conditions should be repaired or covered
            right away.
          </li>
          <li>
            Poor lighting: Some falling accidents are the result of inadequate
            lighting. All areas that may have pedestrians should have
            substantial lighting. Pedestrians who cannot see the floor or what's
            on the floor are more likely to slip and fall.
          </li>
          <li>
            Dangerous stairs: Broken, cracked and wet staircases can be
            extremely dangerous. Staircases should have properly secured rails,
            and cracked steps need to be repaired right away.
          </li>
        </ul>
        <h2>Outdoor Slip-and-Fall Accidents</h2>
        <p>
          Falling accidents can occur outside as well. These types of incidents
          commonly involve damaged walkways. For example, a cracked sidewalk or
          pathway should be repaired right away. Poorly assembled and maintained
          paving stones can result in trip-and-fall accidents. Wet smooth
          sidewalks can result in slip-and-fall incidents. Sudden dips or rises
          can cause a pedestrian to lose his or her balance. Potholes, curbs,
          steps and areas where water collects should be clearly marked. All
          outdoor stairs should have sturdy railings capable of supporting
          pedestrians who need help getting up and down the steps. A failure to
          provide safe conditions outside a property is a form of negligence
          that can result in liability issues for the property owner. In cases
          involving public property or the public right-of-way, the city or
          governmental agency responsible for maintaining the property can be
          held liable. Under California law, personal injury claims against
          governmental agencies must be filed within six months of the incident.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Slip and fall incident lawyers in Laguna Niguel"
            style={{
              backgroundImage:
                "url('../images/slip-and-fall/laguna-niguel-text-header.jpg')"
            }}
          >
            <h2>Legal Representation in Laguna Niguel </h2>
          </div>
        </LazyLoad>
        <p>
          Proving liability in a slip-and-fall case can be a challenging
          process. It is important to understand what causes these types of
          accidents, which can happen anywhere at any time. Depending upon the
          circumstances of the incident, victims of premise liability accidents
          may be able to seek compensation for their losses when they hire a
          Laguna Niguel Slip and Fall Lawyer.
        </p>
        <p>
          <strong>
            {" "}
            When you contact the law firm of Bisnar Chase you will receive a
            free case analysis
          </strong>
          . The Laguna Niguel slip and fall attorneys of Bisnar Chase believe
          that if there is no win there is no fee for personal injury cases.{" "}
        </p>
        <p>
          Our lawyers have experience and have served the Los Angeles, Orange
          County and Inland Empire for<strong> over 40 years</strong>. We have
          won millions of dollars for injury victims who have been injured
          through dog bites, car accidents and have represented families who
          have also experienced a wrongful death.
        </p>
        <p>
          <strong>
            {" "}
            Call us today at 949-203-3814 to get free legal advice.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
