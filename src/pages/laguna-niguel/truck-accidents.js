// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/truck-highway-laguna-niguel-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Laguna Niguel Truck Accident Lawyers - O.C. Semi-Truck Injury Attorneys"
        pageDescription="Call 949-203-3814 for top rated Laguna Niguel truck accident attorneys. Free consultation and quality client care since 1978. Dealing with trucking firms, insurance companies and aggressive defense attorneys may be unavoidable if you or a loved one has been seriously injured in a trucking accident. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Laguna Niguel Truck Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Laguna Niguel truck accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Dealing with trucking firms, insurance companies and aggressive
          defense attorneys may be unavoidable after a serious truck accident.
          If you have suffered from severe injuries after a catastrophic truck
          collision contact the{" "}
          <strong> Laguna Niguel Truck Accident Lawyers</strong> of Bisnar
          Chase.
          <p>
            Immediately after a truck accident, injured victims should focus on
            getting much-needed medical attention and the treatment required for
            a full recovery. Our Laguna Niguel truck accident attorneys have
            been helping people who suffered from a devastating{" "}
            <Link to="/laguna-niguel">personal injury </Link> for over 40 years.
            <p>
              The Law Firm of Bisnar Chase{" "}
              <strong>
                {" "}
                has held a 96% success rate in winning millions of dollars
              </strong>{" "}
              in compensation for your injuries.
            </p>
            <strong> Call 949-203-3814 for a free consultation</strong>.{" "}
          </p>
          <h2>Important Steps to Take after a Truck Accident </h2>
          <p>
            The first few minutes after an accident can be chaotic and
            frightening, to say the least. Although it may be traumatizing you
            must remain calm and be alert in order to handle for the situation
            to the best of your ability. There are steps that you must take
            after an accident to not only protect yourself physically but also
            legally.{" "}
          </p>
          <p>
            <strong> Make sure all involved in the accident are safe</strong>:
            If you are in a lot of pain, it may be in your best interest to stay
            where you are and wait for the authorities and emergency personnel
            to arrive on the scene. Do not move the injured victim. If the
            accident victim is moved it may be more harm than help. Ideally, you
            can have a friend or witness collect the data you need after the
            accident if you are physically incapable of doing so.{" "}
          </p>
          <p>
            <strong>
              {" "}
              Collect the information you need to file a personal injury claim
            </strong>
            : Information that you need to obtain after a big-rig accident
            includes the name of the truck driver, his or her driver's license
            number, the license plate information and the company for whom
            he/she works for and the trucking company's insurance provider.
            Additional information that can tremendously help your truck
            accident case may be the exact time, location of the crash how the
            accident occurred, how fast you were going and how fast you think
            the truck was going. You should also contact any witnesses to
            collect an outside perspective of how the accident occurred.{" "}
          </p>
          <p>
            <strong> Gather physical evidence from the truck collision</strong>:
            Photographs showing the accident scene, position of the vehicles,
            damage to the vehicles, skid marks and injuries are valuable for
            your injury case. Photographs provide clues for investigators on
            what exactly took place. This can also provide proof that the driver
            was negligent when operating the truck. Experts say that if you do
            not have your phone on you to take pictures, keep a disposable
            camera in your glove compartment.{" "}
          </p>
          <p>
            <strong> Hire a Laguna Niguel truck accident attorney</strong>: A
            truck accident lawyer will be able to provide more details regarding
            your situation, and where to go next to figure out the best legal
            path for recovery. Bisnar Chase has been representing clients who
            have been involved in tragic accidents for over 40 years. Our injury
            attorneys can win you the compensation you need such as medical
            bills, after-care or lost wages. Call the Orange County of Bisnar
            Chase today for a{" "}
            <strong> free case-analysis at 949-203-3814</strong>.
          </p>
          <LazyLoad>
            <img
              src="../images/truck-accidents/doctors-brain-injury.jpg"
              width="100%"
              alt="Truck collision lawyers in Laguna Niguel"
            />
          </LazyLoad>
        </p>
        <h2>5 Common Truck Accident Injuries in Laguna Niguel</h2>
        <p>
          Injured victims must obtain prompt medical attention in order to
          maximize their chances of a full recovery. Seeking medical attention
          also creates a medical record. If you have suffered from serious
          injuries, it would also be a good idea to journal what you are going
          through as well. Some of your experiences that should be documented
          include the activities you are unable to perform after the accident
          such as working or simply just walking. Think about how the accident
          has affected your quality of life.
        </p>
        <p>
          <strong>
            {" "}
            Below are the top five most common injuries that victims after a
            truck accident
          </strong>
          .
        </p>
        <ol>
          <li>
            <strong> Broken bones</strong>: Some bones that are commonly damaged
            during a truck accident include the femur and the ribs. The femur is
            one of the most durable bones in the body and is in the thigh. If
            there is a strong impact to the thigh this can shatter the femur
            potentially leaving a person unable to walk for months. Ribs that
            are fractured in a collision usually are from the airbags that
            combusted during the accident. Ribs that are obstructed can pose an
            extreme threat to the cardiovascular system.
          </li>
          <li>
            <strong> Lacerations</strong>: A laceration is when a sharp object
            tears through the tissue. When an auto accident takes place there is
            a high chance that broken glass or metal that is exposed can tear
            through the epidermis. The procedures for superficial lacerations
            may include stitches or staples to close the wound. Deep cuts that
            are a result of a truck collision may be permanent. Many either stay
            with scars for life or may need cosmetic surgery to fix their facial
            structure. If a cut is deep enough though, people can lose an
            extensive amount of blood that can cause a person to go into shock.
          </li>
          <li>
            {" "}
            <Link
              to="https://www.brainline.org/article/facts-about-traumatic-brain-injury"
              target="_blank"
            >
              {" "}
              <strong> Traumatic head injuries</strong>{" "}
            </Link>
            : An estimated 15% of people who experience a traumatic head injury
            will have to live the rest of their life with intense side-effects.
            Effects can include anger outbursts, suicidal thoughts or some may
            lose the ability to perform everyday tasks such as changing out of
            clothes. If a person has hit their head hard during the accident,
            they can form a hematoma, the brain can swell leading to permanent
            brain damage or suffer from a wrongful death.
          </li>
          <li>
            <strong> Neck injuries</strong>: In catastrophic truck accidents,
            the neck being tainted can damage the spinal cord greatly. One of
            the most common neck injuries sustained in a collision is a{" "}
            <Link
              to="https://www.webmd.com/back-pain/neck-strain-whiplash"
              target="_blank"
            >
              {" "}
              whiplash injury
            </Link>
            . Whiplash is defined as the neck suddenly jolting back and forth
            quickly. Whiplash can cause the discs or joints of the spinal cord
            to be severely damaged. Truck accident victims have a 24-hour window
            to obtain treatment before the neck injury worsens. If you
            experience symptoms such as stuffiness, tingling, dizziness or
            numbness seek medical attention right away.
          </li>
          <li>
            <strong> Paralysis</strong>: The central part of the nervous system
            is in the spinal cord. If the nerves in the spine are compromised
            this can lead to a person being partially or completely paralyzed.
            Even if the spinal cord injury does seem minor at first, later the
            injury can lead to paralysis. Any type of fracture or dislocation
            can affect a person's inability to be mobile. Victims who have
            become paralyzed after a serious truck accident may need months of
            intense physical therapy or round-the-clock care for the rest of
            their lives.{" "}
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Truck accident attorneys in Laguna Niguel"
            style={{
              backgroundImage:
                "url('../images/truck-accidents/laguna-niguel-truck-accident.jpg')"
            }}
          >
            <h2>Hiring a Laguna Niguel Truck Accident Lawyer</h2>
          </div>
        </LazyLoad>
        <p>
          If you have suffered a serious injury in a truck crash, it is
          important that you retain the services of an experienced{" "}
          <strong> Laguna Niguel Truck Accident Lawyer</strong>. Timing is very
          important because, in a large truck accident case, insurers and
          attorneys for the trucking firm get to work right away. Some of these
          corporations can have their people get to the crash scene within
          minutes to gather the evidence they need to protect their best
          interests.{" "}
        </p>
        <p>
          Victims need a skilled lawyer on their side who is fighting for their
          rights and looking out for their best interests.
        </p>
        <p>
          The experienced Laguna Niguel truck accident attorneys at{" "}
          <Link to="/">Bisnar Chase </Link> have a long and successful track
          record of representing personal injury cases for over four decades.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Please contact the law offices of Bisnar Chase at 949-203-3814 for a
            free, comprehensive and confidential consultation
          </strong>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
