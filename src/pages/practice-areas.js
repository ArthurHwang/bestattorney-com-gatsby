// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import { graphql, StaticQuery } from "gatsby"
import Img from "gatsby-image"
import React from "react"
import { FaArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "../components/elements/Link"
import { PAData } from "../data/home/practiceAreas"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

const PRACTICE_AREA_QUERY_INDEX = graphql`
  query {
    allFile(
      sort: { fields: [name], order: ASC }
      filter: { relativeDirectory: { eq: "homepage" } }
    ) {
      edges {
        node {
          childImageSharp {
            fluid(maxWidth: 800, maxHeight: 800) {
              ...GatsbyImageSharpFluid_withWebp_tracedSVG
            }
          }
        }
      }
    }
  }
`

export default function PracticeAreas({ location }) {
  return (
    <StaticQuery
      query={PRACTICE_AREA_QUERY_INDEX}
      render={data => {
        const images = data.allFile.edges

        return (
          <LayoutDefault sidebar={false} {...FolderOptions}>
            <SEO
              pageSubject={FolderOptions.pageSubject}
              pageTitle="Practice Areas - Bisnar Chase Personal Injury Attorneys"
              pageDescription="Bisnar Chase represent clients in California for car accidents, wrongful death, auto defects and other injuries and accidents."
            />
            <ContentWrapper>
              {/* ------------------------------------------------------ */}
              {/* ----------------------CODE BELOW---------------------- */}
              {/* ------------------------------------------------------ */}
              <h1>Bisnar Chase Practice Areas</h1>
              <BreadCrumbs location={location} fixPACrumb={true} />

              <StyledPracticeArea>
                <TextWrapper>
                  {/* <p className="PA-title">How we can help you</p> */}
                  <p className="PA-subtext">
                    We have the resources, experience, and talent to take on
                    some of the most complex accident & injury cases in
                    California. Browse through our practice areas below for more
                    information:
                  </p>
                </TextWrapper>
                <PAGrid>
                  {images.map(({ node: { childImageSharp } }, index) => (
                    <Link
                      className="card-wrapper"
                      to={PAData[index].href}
                      key={index}
                    >
                      <PACard>
                        <Img
                          // durationFadeIn={1000}
                          alt={PAData[index].alt}
                          className="card-image"
                          fluid={childImageSharp.fluid}
                        />
                        <p className="card-text">
                          <span className="card-title">
                            {PAData[index].name}{" "}
                            <span className="arrow-right">
                              <FaArrowAltCircleRight />
                            </span>
                          </span>
                          <span className="card-desc">
                            {PAData[index].desc}
                          </span>
                        </p>
                      </PACard>
                    </Link>
                  ))}
                </PAGrid>
              </StyledPracticeArea>

              <StyledPALists>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/car-accidents">
                      Car Accidents
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/motorcycle-accidents">
                        Motorcycle Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/truck-accidents">Truck Accidents</Link>
                    </li>
                    <li>
                      <Link to="/truck-accidents/garbage-truck-accident-lawyers">
                        Garbage Truck Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/pedestrian-accidents">
                        Pedestrian Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/bicycle-accidents">Bicycle Accidents</Link>
                    </li>
                    <li>
                      <Link to="/bicycle-accidents/electric-bicycle-accident-lawyer">
                        Electric Bike Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/bus-accidents">Bus Accidents</Link>
                    </li>
                    <li>
                      <Link to="/dui">DUI Accidents</Link>
                    </li>
                    <li>
                      <Link to="/boat-accidents">Watercraft Accidents</Link>
                    </li>
                    <li>
                      <Link to="/misc/train-accidents">Train Accidents</Link>
                    </li>
                    <li>
                      <Link to="/car-accidents/hazardous-roads">
                        Hazardous and Defective Roads
                      </Link>
                    </li>
                    <li>
                      <Link to="/car-accidents/rideshare-accident-lawyer">
                        Rideshare Accidents
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/dog-bites">
                      Dog Bites
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/dog-bites/dangerous-dog-breeds">
                        Dangerous Breeds
                      </Link>
                    </li>
                    <li>
                      <Link to="/dog-bites/california-dog-bite-laws">
                        California Dog Bite Laws
                      </Link>
                    </li>
                    <li>
                      <Link to="/dog-bites/victim-rights">Victim's Rights</Link>
                    </li>
                    <li>
                      <Link to="/dog-bites/factors-causing-canine-aggression">
                        Factors of Canine Aggression
                      </Link>
                    </li>
                    <li>
                      <Link to="/dog-bites/insurance-ban-breeds">
                        Insurance Breed Bans
                      </Link>
                    </li>
                    <li>
                      <Link to="/dog-bites/injury-infections-dog-bites">
                        Injuries and Infections from Bites
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/auto-defects">
                      Auto Defect Cases
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/auto-defects/rollovers">
                        Rollover Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/roof-crush">Roof Crush</Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/seatback-failure">
                        Seat Back Failure
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/ford-carbon-monoxide">
                        Ford Carbon Monoxide Poisoning
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/autonomous-car-accident-lawyer">
                        Autonomous Car Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/airbag-failures">
                        Airbag Failure
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
                        Sudden Unintended Acceleration
                      </Link>
                    </li>
                    <li>
                      <Link to="/auto-defects/defective-seatbelts/types-of-seat-belt-injuries">
                        Seatbelt Failure
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/premises-liability">
                      Premise Injuries
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/premises-liability/assault">Assault</Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/burn-injuries">
                        Fires & Burn Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/premises-liability/slip-and-fall-accidents">
                        Slip and Fall Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/premises-liability/swimming-pool-accident-lawyer">
                        Swimming Pool Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/premises-liability/hotel-injuries">
                        Hotel Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/premises-liability/amusement-park-accidents">
                        Amusement Park Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/cheerleader-injury-lawyer">
                        Cheerleader Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/premises-liability/rehab-facility-negligence">
                        Rehab Facility Negligence
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/defective-products">
                      Defective Products
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/medical-devices">
                        Defective Medical Devices
                      </Link>
                    </li>
                    <li>
                      <Link to="/pharmaceutical-litigation">
                        Pharmaceutical Litigation
                      </Link>
                    </li>
                    <li>
                      <Link to="/pharmaceutical-litigation/talcum-powder">
                        Talcum Powder
                      </Link>
                    </li>
                    <li>
                      <Link to="/defective-products/failure-to-warn">
                        Product Failure to Warn
                      </Link>
                    </li>
                    <li>
                      <Link to="/defective-products/mass-tort-lawyer">
                        Mass Torts
                      </Link>
                    </li>
                    <li>
                      <Link to="/defective-products/e-cig-injury-lawyer">
                        E-cig Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/defective-products/lithium-ion-battery-explosion-lawyer">
                        Lithium-Ion Battery Explosions
                      </Link>
                      <br />
                    </li>
                    <li>
                      <Link to="/orange-county/hoverboard-injury-lawyers">
                        Exploding Hoverboards
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/employment-law">
                      Employment Law
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/employment-law/wage-and-hour">
                        Wage & Hour
                      </Link>
                    </li>
                    <li>
                      <Link to="/employment-law/wrongful-termination">
                        Wrongful Termination
                      </Link>
                    </li>
                    <li>
                      <Link to="/employment-law/sexual-harassment">
                        Sexual Harassment
                      </Link>
                    </li>
                    <li>
                      <Link to="/employment-law/hostile-work-environment-attorney">
                        Hostile Work Environment
                      </Link>
                    </li>
                    <li>
                      <Link to="/employment-law/class-action">
                        Class Action Lawsuits
                      </Link>
                    </li>
                    <li>
                      <Link to="/job-injuries">Job Injuries</Link>
                    </li>
                    <li>
                      <Link to="/job-injuries/construction-accidents">
                        Construction Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/job-injuries/explosion-accidents">
                        Explosion Accidents
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/nursing-home-abuse">
                      Abuse & Neglect
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>

                  <ul>
                    <li>
                      <Link to="/nursing-home-abuse/faqs">Abuse FAQs</Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse">Nursing Home Abuse</Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse/insurance">
                        Denial of Insurance
                      </Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse/fraud">Fraud</Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse/bedsores">
                        Bedsore Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse/bed-rail-death">
                        Bedrail Deaths
                      </Link>
                    </li>
                    <li>
                      <Link to="/nursing-home-abuse/elder-neglect">
                        Elder Neglect
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/catastrophic-injury">
                      Catastrophic
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/catastrophic-injury/amputations">
                        Amputations
                      </Link>
                    </li>
                    <li>
                      <Link to="/head-injury">Head Injuries</Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/neck-injury-case-lawyer">
                        Neck Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/nerve-damage">
                        Nerve Damage
                      </Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/back-injury-lawyer">
                        Back Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/eye-injuries">
                        Eye Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/post-traumatic-stress-disorder">
                        PTSD
                      </Link>
                    </li>
                    <li>
                      <Link to="/catastrophic-injury/spinal-cord-injury">
                        Spinal Cord Injury
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
                <PAListCard>
                  <h3>
                    <Link className="PA-title" to="/misc">
                      Miscellaneous
                      <FaArrowAltCircleRight className="right-arrow" />
                    </Link>
                  </h3>
                  <ul>
                    <li>
                      <Link to="/class-action">Class Action Cases</Link>
                    </li>
                    <li>
                      <Link to="/legal-malpractice">Legal Malpractice</Link>
                    </li>
                    <li>
                      <Link to="/food-poisoning/foodborne-illness">
                        Food Poisoning
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/telephone-consumer-protection-act">
                        Consumer Telephone Protection
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/atv-accident-lawyers">ATV Accidents</Link>
                    </li>

                    <li>
                      <Link to="/rv-accidents">RV & Motorhome Accidents</Link>
                    </li>

                    <li>
                      <Link to="/misc/aviation-accident-lawyers">
                        Aviation Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/job-injuries/explosion-injuries">
                        Explosion Accidents
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/child-injury-lawyer">Child Injuries</Link>
                    </li>
                    <li>
                      <Link to="/misc/cruise-ship-injury-lawyer">
                        Cruise Ship Injuries
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/last-minute-trial-attorney">
                        Last-minute Trial Attorneys
                      </Link>
                    </li>
                    <li>
                      <Link to="/misc/electrocution-injury-lawyer">
                        Electrocution Injuries
                      </Link>
                    </li>
                  </ul>
                </PAListCard>
              </StyledPALists>

              <StyledBottomText>
                <p>
                  Personal injury law is a specialized practice that victims of
                  injuries will want to consider over a standard consumer
                  attorney. Many aspects of injury law can be overlooked when
                  attorneys not familiar with injury and accident law represent
                  a case. Our attorneys are personal injury attorneys and trial
                  lawyers. If we represent you we will do so from beginning to
                  end and not transfer your file to a lawyer for trial. All of
                  our attorneys are trial lawyers to ensure you receive the best
                  representation possible.
                </p>

                <p>
                  Bisnar Chase also represents victims of emotional trauma. Not
                  all personal injury cases are a result of physical injuries,
                  and we fight for people who have been wronged but cannot fight
                  for themselves.{" "}
                  <strong>
                    If you have been injured in a situation not covered by the
                    above practice areas, please{" "}
                    <Link to="/contact">contact us</Link> and get a free
                    consultation to see if you have a case!
                  </strong>
                </p>
              </StyledBottomText>

              {/* ------------------------------------------------------ */}
              {/* ----------------------CODE ABOVE---------------------- */}
              {/* ------------------------------------------------------ */}
            </ContentWrapper>
          </LayoutDefault>
        )
      }}
    />
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  .PA-title {
    /* font-size: 2rem; */
    font-weight: 600;
  }

  .right-arrow {
    position: relative;
    top: 2px;
    left: 5px;
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`

const StyledBottomText = styled("section")`
  margin: 2rem auto;
  max-width: 1100px;
`

const StyledPALists = styled("section")`
  max-width: 1100px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin: 0 auto;
  grid-gap: 2rem;

  @media (max-width: 1300px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (max-width: 900px) {
    grid-template-columns: 1fr;
  }
`

const PAListCard = styled("div")`
  background-color: #eee;
  padding: 2rem;
  padding-bottom: 0;

  h3 {
    text-align: center;
    font-size: 2rem;
    line-height: 2.4rem;
    margin-bottom: 1rem;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
  }

  ul li {
    text-align: left;
  }
`

const TextWrapper = styled("div")`
  max-width: 1100px;
  margin: 0 auto;

  @media (max-width: 1024px) {
    max-width: 90%;
  }

  @media (max-width: 700px) {
    max-width: 90%;
  }

  .PA-title,
  .PA-subtext {
    text-align: center;
  }

  .PA-title {
    color: ${({ theme }) => theme.colors.secondary};
    font-size: 3rem;
    text-align: center;
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    border-radius: 0px 0px 20px 20px;
    padding: 0 0rem 1.5rem;
    line-height: 1;
    font-variant: all-petite-caps;
    font-style: italic;
    font-weight: 600;
    width: 100%;

    @media (max-width: 450px) {
      font-size: 1.8rem;
    }
  }

  .PA-subtext {
    text-align: center;
    letter-spacing: 1px;
    line-height: 1.8;
    font-size: 1.4rem;
    margin: 3rem 0;

    @media (max-width: 700px) {
      text-align: initial;
      max-width: 100%;
    }
  }
`

const StyledPracticeArea = styled("section")`
  padding: 0 5rem 2rem;

  .card-wrapper {
    overflow: hidden;
    border-radius: 0.5rem;
  }

  @media (max-width: 1535px) {
    padding: 0 0 4rem;
  }

  @media (max-width: 1024px) {
    padding: 0 0 4rem;
  }

  @media (max-width: 700px) {
    padding: 0 0 3rem;
  }
`

const PACard = styled("div")`
  transition: all 0.3s;
  position: relative;
  overflow: hidden;

  @media (max-width: 700px) {
    height: 200px;
  }

  .card-image {
    transition: transform 0.5s;

    img {
      @media (max-width: 700px) {
        object-position: center -102px !important;
      }
    }
  }

  .card-text {
    position: absolute;
    background: RGBA(0, 0, 0, 0.8);
    height: 100%;
    width: 100%;
    bottom: 0;
    margin: 0;
    text-align: center;
    font-size: 2rem;
    font-variant: common-ligatures small-caps;
    color: ${({ theme }) => theme.colors.primary};

    transform: translateY(calc(100% / 1.18));
    transition: transform 0.5s;

    @media (max-width: 700px) {
      transform: translateY(calc(100% / 1.41));
    }

    @media (max-width: 465px) {
      transform: translateY(calc(100% / 1.325));
    }

    .card-title {
      display: block;
      width: 100%;
      height: 70px;
      padding-top: 2.5%;

      @media (max-width: 1220px) {
        font-size: 1.6rem;
      }

      @media (max-width: 877px) {
        font-size: 1.2rem;
      }

      @media (max-width: 700px) {
        font-size: 1.6rem;
      }
    }

    .arrow-right {
      font-size: 1.6rem;
      position: relative;
      top: 3px;
      left: 5px;
      color: ${({ theme }) => theme.colors.accent};
    }

    .card-desc {
      display: block;
      font-size: 55%;
      font-variant: normal;
      padding: 0 4rem;
      line-height: 2;
      position: relative;
      bottom: 15px;

      @media (max-width: 1660px) {
        font-size: 50%;
      }

      @media (max-width: 1050px) {
        padding: 1rem;
      }

      @media (max-width: 700px) {
        bottom: 30px;
        padding: 1rem;
      }

      @media (max-width: 465px) {
        font-size: 0.92rem;
        bottom: 38px;
      }
    }
  }

  &:hover {
    .card-image {
      transform: scale(1.2);
    }

    .card-text {
      transform: translateY(calc(100% / 4));
    }
  }
`

const PAGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin: 0 auto;
  max-width: 1100px;
  grid-gap: 2rem;

  @media (max-width: 1220px) {
    max-width: 100%;
    padding: 0;
  }

  @media (max-width: 1024px) {
    max-width: 100%;
    grid-gap: 1rem;
    padding: 0rem;
  }

  @media (max-width: 700px) {
    max-width: 100%;
    padding: 0rem;
    grid-template-columns: initial;
    grid-template-rows: repeat(9, 1fr);
  }
`
