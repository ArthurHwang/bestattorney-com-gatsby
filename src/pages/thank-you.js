// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ThankYouPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Thank You - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Our personal injury lawyers will be in touch with you to discuss your case. For immediate help call 949-203-3814."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Message Sent to Intake Expert - Thank You</h1>
        <BreadCrumbs location={location} />
        <h2 align="center">Thank You - Your Inquiry Has Been Submitted.</h2>
        <p>
          We know your time is important and we will contact you to discuss your
          case as soon as possible - Typically within one business day.{" "}
        </p>
        <p>
          If you need immediate help and want to talk with an intake expert now,
          call <strong> 949-203-3814</strong>. Office hours are{" "}
          <strong> 8:30 a.m. to 5:00 p.m. M-F</strong>. We do evaluate all
          claims submitted on weekends.
        </p>

        <iframe
          width="100%"
          height="500"
          src="https://www.youtube.com/embed/P4cWzcUwGxQ?rel=0"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
        />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
