// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lake Forest Wrongful Death Attorneys - Orange County, CA"
        pageDescription="The experienced Lake Forest wrongful death lawyers at Bisnar Chase have a long and successful track record of protecting the rights of victims’ families and holding wrongdoers accountable. We will fight for your rights to ensure that you are fairly compensated. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lake Forest Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          A &ldquo;wrongful death&rdquo; occurs when an individual loses his or
          her life as a result of someone else's negligence or wrongdoing. The
          loss of a loved one is hard enough on a family. It is true that
          families not only suffer emotionally, but also financially after the
          loss of their loved one. For example, when the family's breadwinner is
          killed in a car accident caused by a drunk driver, the family can file
          a wrongful death lawsuit against the at-fault driver claiming damages.
        </p>
        <p>
          Wrongful death claims could involve any type of fatal accident from a
          car accident to medical malpractice, product liability, workplace
          accidents and nursing home abuse or neglect. In addition to
          individuals, corporations and governmental agencies can also be held
          liable for acting negligently or intentionally. To pursue these types
          of cases, contacting an experienced
          <strong>
            {" "}
            Lake Forest{" "}
            <Link to="/orange-county/wrongful-death">
              wrongful death attorney{" "}
            </Link>
          </strong>{" "}
          should be your first step.
        </p>
        <h2>Civil Action</h2>
        <p>
          Wrongful death is a civil action as opposed to a criminal proceeding.
          While the Orange County District Attorney may prosecute a drunk or
          distracted driver criminally for causing a fatal accident, such a
          proceeding will not offer much in terms of monetary compensation for
          the victim's family other than restitution. If the driver is found
          guilty, he or she may be sentenced to jail time and/or probation. To
          seek compensation, victims' families must file a civil wrongful death
          claim.
        </p>
        <h2>Who May File a Wrongful Death Claim?</h2>
        <p>
          Not everyone may be eligible to file a wrongful death claim. The
          following parties may be eligible to file such as claim:
        </p>
        <ul>
          <li>
            Immediate family members: These include spouses, children and
            parents of unmarried children.
          </li>
          <li>Life partners and putative spouses.</li>
          <li>
            Minors and others who were financially dependent on the victim
          </li>
          <li>
            Distant family members such as grandparents, cousins, siblings,
            nieces or nephews.
          </li>
        </ul>
        <h2>Potential Defendants in a Wrongful Death Case</h2>
        <p>
          Wrongful death lawsuits may be brought against individuals or
          entities. A wrongful death action may include defendants such as:
        </p>
        <ul>
          <li>Negligent drivers</li>
          <li>
            Government agency that failed to fix a dangerous condition on a
            roadway
          </li>
          <li>Manufacturer of a dangerous and defective product</li>
          <li>
            Property owner who allowed a dangerous condition to exist on the
            premises
          </li>
          <li>Negligent or abusive nursing home</li>
          <li>Hospital or doctor that committed an error</li>
        </ul>
        <h2>Damages in a Wrongful Death Case</h2>
        <p>
          Monetary damages may be sought for medical and funeral costs, lost
          earnings, loss of benefits such as insurance or pension and loss of an
          inheritance caused by an untimely death. The non-economic damages that
          may be claimed in a wrongful death lawsuit include mental anguish,
          pain and suffering, loss of care, protection and nurturing, loss of
          love and companionship and loss of consortium from a deceased spouse.
          In some wrongful death cases such as product liability or nursing home
          abuse cases, punitive damages may be awarded, especially in the more
          egregious cases.
        </p>
        <h2>Contacting a Wrongful Death Attorney</h2>
        <p>
          Do you really need a lawyer to represent you in a wrongful death case?
          The answer is &ldquo;yes.&rdquo; Wrongful death cases are seldom
          straightforward. They involve complex calculations. How would you put
          a price tag on what your loved one's nurturing and support is worth? A
          knowledgeable and skilled lawyer will be able to make a comprehensive
          assessment and list of damages that can be claimed in a wrongful death
          case.
        </p>
        <p>
          The experienced Lake Forest wrongful death lawyers at Bisnar Chase
          have a long and successful track record of protecting the rights of
          victims' families and holding wrongdoers accountable. We will remain
          on your side, fight for your rights and ensure that you are fairly
          compensated for your tremendous losses. If you have lost a loved one
          as a result of someone else's negligence or wrongdoing in Lake Forest,
          please call us at 949-203-3814 for a free, comprehensive and
          confidential consultation.
        </p>
        <p align="center">
          <strong>
            {" "}
            Call an experienced and reputable Lake Forrest wrongful death lawyer
            for a free consultation at 949-203-3814{" "}
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
