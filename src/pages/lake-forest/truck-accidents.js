// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lake Forest Truck Accident Attorneys - Orange County, CA"
        pageDescription="If a negligent truck driver caused your serious injuries, please contact an experienced Lake Forest truck accident lawyer to better understand your legal rights and options. We will fight hard for your rights and ensure that the at-fault parties are held liable. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lake Forest Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Some of the most tragic truck accidents in Lake Forest are caused by
          truck driver error. Truck drivers who are impaired by alcohol or drugs
          or fatigued as a result of sleep deprivation put everyone on the
          roadway at great risk. Truck drivers also contribute to accidents by
          speeding, overloading the trailer, failing to properly check or
          maintain the vehicle and driving aggressively or recklessly. If a
          negligent truck driver caused your serious injuries, please contact an
          experienced
          <strong>
            {" "}
            Lake Forest{" "}
            <Link to="/truck-accidents">truck accident lawyer </Link>
          </strong>{" "}
          to better understand your legal rights and options.
        </p>
        <h2>Common Truck Driver Errors</h2>
        <p>
          According to the Federal Motor Carrier Safety Administration's (FMCSA)
          Large Truck Crash Causation Study, the following major driver errors
          were addressed.
        </p>
        <ul>
          <li>
            Driving too fast for the conditions. Often truck drivers operate at
            an excessive rate of speed without taking into consideration that it
            takes a significantly longer time to stop a large truck. A number of
            catastrophic rear-end truck accidents occur because truckers are
            unable to stop in time to avoid a collision. These types of
            incidents also result in chain-reaction crashes involving multiple
            vehicles and victims.
          </li>
          <li>
            Following too closely. It is against California law to follow
            vehicles ahead too closely. California Vehicle Code section 21703
            states: "The driver of a motor vehicle shall not follow another
            vehicle more closely than is reasonable and prudent, having due
            regard for the speed of such vehicle and the traffic upon, and the
            condition of, the roadway." California Vehicle Code section 21704
            also states that the driver "shall keep the vehicle he is driving at
            a distance of not less than 300 feet to the rear of any other motor
            vehicle subject to such speed restriction which is preceding it."
          </li>
          <li>
            Distracted driving. Talking on a cell phone, texting, communicating
            wirelessly, eating or drinking are all forms of distracted driving
            that can result in major injury truck accidents.
          </li>
          <li>
            Driver fatigue. Federal regulations specifically state the number of
            hours per shift a truck driver can work. Truck drivers are also
            required to take regular breaks and sufficient rest. A sleepy truck
            driver can pose significant hazards to others on the roadway.
          </li>
          <li>
            Drugs and alcohol: Trucking companies are required to hire drivers
            who do not have a history of DUI arrests. Under California law,
            commercial drivers cannot drive a vehicle with a blood alcohol
            concentration of 0.04 percent or higher. The legal limit is 0.08
            percent for drivers of other passenger vehicles. Employers are also
            required under the law to randomly test their drivers.
          </li>
          <li>
            Jackknifing: Rollovers or truck jackknifing accidents can be caused
            by driving errors such as taking a curve too fast, traveling at
            excessive rates of speed, fatigue, inexperience or improper load
            distribution.
          </li>
        </ul>
        <h2>Proving Driver Errors</h2>
        <p>
          Some trucking companies utilize electronic event data recorders in
          their vehicles. These are basically devices that record a variety of
          information about the truck and its operation such as speed patterns,
          brake use and driving time. An experienced truck accident attorney
          will work to secure the truck's event data recorder and other evidence
          such as cell phone records, driver logs and vehicle maintenance
          records. If these pieces of evidence are not requested promptly, they
          may be lost or destroyed.
        </p>
        <h2>Contacting an Experienced Injury Lawyer</h2>
        <p>
          The skilled <strong> Lake Forest truck accident lawyers </strong>at
          Bisnar Chase have helped numerous injured victims and their families
          obtain fair compensation for their injuries, damages and losses. The
          cost of treating serious injuries can add up quickly. You and your
          family should not be left paying for someone else's mistake or
          negligence. We will fight hard for your rights and ensure that the
          at-fault parties are held liable. Please call us at 949-203-3814 for a
          free consultation and comprehensive case evaluation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
