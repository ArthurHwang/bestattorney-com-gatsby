// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lake Forest Nursing Home Abuse Attorneys - Orange County, CA"
        pageDescription="Please call 949-203-3814 for Lake Forest Nursing Home Abuse Attorneys. We've been serving Orange County for over 40 years with a 96% success rate. Free consultation with top rated elder abuse lawyers who have represented tens of thousands.."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lake Forest Nursing Home Abuse Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          Do you have a loved one in a nursing home? More and more Americans are
          faced with the prospect of placing a loved one in a managed-care
          facility. Most American families now consist of two working adults who
          cannot stop working to care for a family member who needs full-time
          assistance. Nursing homes charge very high fees to guarantee that your
          loved one will be taken care of and receive proper treatment.
        </p>
        <p>
          Unfortunately, many people are discovering that{" "}
          <Link to="/nursing-home-abuse">
            nursing homes do not always live up to these promises
          </Link>
          . More and more cases of nursing home abuse are being brought to light
          every year, and many are questioning the entire system of elder care.
          Nursing homes have been the victims of strict budget cuts, and this
          has translated into hiring the very cheapest employees and stretching
          staff to their limits. Many nursing homes simply{" "}
          <strong>
            {" "}
            do not have the budgets to provide the quality of care
          </strong>{" "}
          that families and patients are expecting.
        </p>
        <p>
          However, this is not an excuse for abusive care. In many cases,
          nursing home neglect and abuse results directly from someone who
          simply does not want to do his or her job correctly, and translates
          this apathy into poor patient care. Many nursing home residents have
          been the victims of targeted abuse such as withholding of medication
          or the refusal of staff to properly clean the patients or their rooms.
          Many of these neglectful actions result in sickness for patients. Some
          nursing home residents have died when proper care was withheld.
        </p>
        <h2>Physical and Mental Abuse</h2>
        <p>
          Abuse can take even more sinister forms if someone on the staff or
          another resident enjoys hurting those who cannot fight back.
          Unfortunately, many
          <strong>
            {" "}
            elderly people are an easy target for physical and mental abuse
          </strong>
          , and they receive poor treatment at the hands of those who take
          advantage of their helplessness. Nursing home investigations have
          revealed cases where patients who could not report their abusers, such
          as Alzheimer's patients, have been the targets of physical abuse such
          as slapping, burning, or even rape.
        </p>
        <p>
          Our <strong> Lake Forest nursing home lawyers</strong> are dedicated
          to stamping out all nursing home abuse. To this end, these attorneys
          guide patients and families in filing suits not only to stop the abuse
          or neglect but to{" "}
          <strong>
            {" "}
            collect damages for the poor care received in the past
          </strong>
          . A nursing home suit might include damages for pain and suffering,
          medical costs for treatment, and mental anguish.
        </p>
        <h2>Holding Nursing Homes Accountable</h2>
        <p>
          Even more important, however, is the service performed by nursing home
          attorneys in Lake Forest in stopping further abuses to other
          residents. If nursing homes are allowed to ignore abuse or neglect,
          the behavior will never stop. By holding these companies or
          organizations responsible for the abuse and neglect that happens on
          their watch, nursing home lawyers help victims and their families
          ensure that what happened to them will not continue to happen to
          others.
        </p>
        <p>
          Talk to a professional Lake Forest nursing home abuse attorney
          immediately if you or a loved one has received poor treatment at the
          hands of the staff or residents of a nursing home. Please call us at
          949-203-3814 to find out more about what we can do for your legal
          situation. Free consultations available.
        </p>
        <p align="center">
          Immediately call an experienced and reputable Elder Abuse Lawyer for a
          free consultation at <strong> 949-203-3814</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
