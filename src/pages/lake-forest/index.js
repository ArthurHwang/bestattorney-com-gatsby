// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/lake-forest-personal-injury-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Lake Forest",
      population: 79312,
      totalAccidents: 1403,
      intersection1: "El Toro Rd & Trabuco Rd",
      intersection1Accidents: 57,
      intersection1Injuries: 41,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 97.2,
      city1Name: "Mission Viejo",
      city1Index: 70.0,
      city2Name: "Laguna Hills",
      city2Index: 119.1,
      city3Name: "Laguna Woods",
      city3Index: 54.2,
      city4Name: "Aliso Viejo",
      city4Index: 47.5
    },
    locationBox3: {
      intersection2: "Jeronimo Rd & Trabuco Rd",
      intersection2Accidents: 52,
      intersection2Injuries: 29,
      intersection2Deaths: 1,
      intersection3: "El Toro Rd & Muirlands Blvd",
      intersection3Accidents: 54,
      intersection3Injuries: 30,
      intersection3Deaths: 0,
      intersection4: "El Toro Rd & Rockfield Blvd",
      intersection4Accidents: 77,
      intersection4Injuries: 22,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lake Forest Personal Injury Attorneys - Orange County, CA"
        pageDescription="Contact the Lake Forest Personal Injury Attorneys of Bisnar Chase for a free consultation with experienced accident lawyers serving Orange County for over 40 years. We have collected more than $500 Million for our clients and may be able to help you too. Free consultation, 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lake Forest Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="lake forest personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            Lake Forest is a wonderful place to live; low crime rates, beautiful
            weather, and plenty of nearby attractions to keep you and the family
            satisfied. Unfortunately, what Lake Forest provides in comfort and
            entertainment it lacks in safety. Several Lake Forest locals, as
            well as tourists, are left to recover from serious injuries that
            could have been easily prevented. If you or a loved one has suffered
            injuries in Orange County, contact one of our
            <strong> Lake Forest personal injury lawyers</strong> today to
            receive free information that will help you make the right decision
            to move forward. We have a long history of success and
            record-breaking verdicts that will give you the confidence to leave
            the negotiating up to us and concentrate on getting your life back
            on track.
          </p>
          <p>
            The steps you take immediately following a Lake Forest car accident
            may affect your ability to receive fair compensation for your
            losses. If you are incapacitated or unable to gather information
            from the crash site, you will still be able to pursue compensation.
            But individuals who are able to obtain at least some information or
            evidence from the scene of the crash may simply have an easier time
            getting the support they need. Whether you are hurt in a car
            accident or some other type of incident, an experienced Lake Forest
            personal injury lawyer can help you take the necessary steps to
            obtain the compensation you need and rightfully deserve.
          </p>
          <h2>Contacting the Authorities</h2>
          <p>
            If you or anyone else has suffered an injury, you are legally
            required to contact the authorities. It will be in your best
            interest to get your side of the story in the police report. If an
            officer does not arrive at the crash site, you can file a report at
            the station the next day. Having a police report that supports your
            recollection of the crash can help bolster your claim. This is true
            even in other types of injury incidents. For example, if your loved
            one has been injured as a result of nursing home abuse or
            negligence, it is important that you file a complaint with the
            administrators first. Doing so creates a record and leaves a paper
            trail.
          </p>
          <h2>Collecting Information After a Personal Injury</h2>
          <p>
            Here is just some of the information you would be well advised to
            obtain in any personal injury incident especially in car accident
            cases:
          </p>
        </div>
        <ul>
          <li>
            Contact information: You will want the phone number, driver's
            license number and license plate number from any other drivers
            involved.
          </li>
          <li>
            Witnesses: If anyone witnessed the crash or the incident in question
            (besides a friend of yours), it would be a great idea to get it so
            you have an objective party to corroborate your account.
          </li>
          <li>
            Details: Write down the exact time and location of the incident.
          </li>
          <li>
            Take photos: Does your cell phone have the ability to take quality
            photos? If not, you should keep a disposable camera in your vehicle.
            It helps to have photos of the crash site or images showing your
            injuries. Take photos of the damage to the vehicles, skid marks, the
            positioning of the vehicles or debris left on the roadway. You may
            want to include the license plate numbers of the other vehicles
            involved in case you lose your notes.
          </li>
          <li>
            Take notes: As time passes, your recollection of how the incident
            occurred may become clouded. It can help to have the specifics of
            the crash in written form. Be specific. Refer to these notes any
            time you discuss your case with an insurance company or attorney.
          </li>
        </ul>
        <h2>Seek Immediate Medical Attention</h2>
        <p>
          There are a number of reasons why it is in your best interest to see a
          doctor right away following an accident. Many injuries such as
          whiplash and other back and neck injuries can get progressively worse
          over time if not properly diagnosed and treated. Head injuries and
          internal injuries may take time to fully manifest as well. Getting
          prompt diagnosis and treatment for your injuries will greatly increase
          your chances of a speedy and full recovery. Medical records can be
          used in court to prove the severity of your injuries as well. Remember
          never to allow{" "}
          <Link to="/resources/dealing-with-an-insurance-adjuster">
            {" "}
            insurance companies
          </Link>{" "}
          or other parties (other than your attorney) access to your medical
          records. Always be aware of what you are signing or agreeing to.
        </p>
        <h2>More than Just Injury Attorneys</h2>
        <p>
          At Bisnar Chase, we provide our clients with more than just legal
          assistance. Since our inception in 1978, we have been providing a
          <strong> first-class experience for every client we represent</strong>
          . When you call our firm or meet our intake specialists, you can feel
          the difference that comes from a staff that is truly passionate about
          obtaining justice for those who have been wronged. We take pride in
          holding those responsible for your injuries and making sure that you
          and your family will be taken care of while you adjust to your new
          lifestyle and move on with your life.
        </p>
        <p>
          Our commitment to helping those who are unable to help themselves
          extends beyond the doors of our law firm. Our entire staff
          participates in community outreach programs designed to give Orange
          County locals a boost during their time of need. We spend our own
          time, energy and money to participate as a group and the money that we
          raise is matched by John Bisnar and Brian Chase. The following is a
          list of programs that we are proud to be a part of and contribute to
          their success.
        </p>
        <ul>
          <li>Boys and Girls Club Chili Cook off</li>
          <li>
            Second Harvest Food Bank Orange County Resident Turkey Giveaway
          </li>
          <li>Share Our Selves (SOS) Adopt A Family Program</li>
          <li>HIV/AIDS Bicycle Fund raisers</li>
          <li>Walk Like M.A.D.D. Events</li>
          <li>Hit-And-Run Reward Program in conjunction with We Tip</li>
        </ul>
        <h2>Lake Forest Attorney Practice Areas</h2>
        <p>
          Having a personal injury attorney in Lake Forest who knows how to
          handle a variety of cases could work in your favor; our attorneys have
          experience settling and litigating a large number of different types
          of accident injury cases and have done so with a 97.5% success rate.
          The following are types of personal injury cases for which our firm
          has obtained maximum compensation for our client's injuries.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/lake-forest/car-accidents">Car Accidents </Link>
            <ul>
              <li>
                {" "}
                <Link to="/lake-forest/bus-accidents">Bus Accidents </Link>
              </li>
              <li>
                {" "}
                <Link to="/lake-forest/truck-accidents">Truck Accidents </Link>
              </li>
              <li>
                {" "}
                <Link to="/lake-forest/pedestrian-accidents">
                  Pedestrian Accidents{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/lake-forest/truck-accidents">Truck Accidents </Link>
              </li>
            </ul>
          </li>
          <li>
            {" "}
            <Link to="/lake-forest/dog-bites">Dog Attacks </Link>
          </li>
          <li>
            {" "}
            <Link to="/lake-forest/employment-lawyers">Employment Law </Link>
          </li>
          <li>
            {" "}
            <Link to="/lake-forest/brain-injury">Brain Injuries </Link>
          </li>
          <li>
            {" "}
            <Link to="/lake-forest/bicycle-accidents">Bike Accidents </Link>
          </li>
        </ul>
        <h2>Contacting a skilled Lake Forest Personal Injury Attorney</h2>
        <p>
          The objective of insurance companies is to protect their shareholders.
          They will take every opportunity possible to deny a claim or offer an{" "}
          <Link to="/resources/dont-rush-to-settle">
            {" "}
            inadequate settlement
          </Link>
          . It may not be in your best interest to face an insurance company
          alone. The experienced and knowledgeable attorneys at Bisnar Chase
          Personal Injury Attorneys have more than three decades of experience
          fighting insurance companies and holding wrongdoers accountable. We
          will remain on your side, fight for your rights and help ensure that
          you receive just compensation for your losses.
        </p>
        <p>
          The steps you take immediately following a car accident may affect
          your ability to receive fair compensation for your losses. If you are
          incapacitated or unable to gather information from the crash site, you
          will still be able to pursue compensation. But individuals who are
          able to obtain at least some information or evidence from the scene of
          the crash may simply have an easier time getting the support they
          need. Whether you are hurt in a car accident or some other type of
          incident, an experienced Lake Forest personal injury lawyer can help
          you take the necessary steps to obtain the compensation you need and
          rightfully deserve.
        </p>
        <h3 className="centered">
          Free Lake Forest office, hospital or home consultations by appointment
          only.
        </h3>
        <h3 className="centered mb">Call 949-203-3814</h3>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
