// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lake Forest Bicycle Accident Attorneys & Bike Injury Lawyers"
        pageDescription="Call 949-203-3814 for a free consultation with a Lake Forest Bicycle Accident Attorney specializing in serious injuries against bike riders in Orange County. We have collected over $500 Million for clients throughout California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lake Forest Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Lake Forest is a wonderful community to ride your bike, but bicycle
          riding can get dangerous. When a bicycle collides with a larger
          vehicle, it is often the bicyclist who is seriously injured. Bicycle
          accidents result in major or even catastrophic injuries including
          brain injuries, spinal cord damage, broken bones and internal
          injuries. Bicycle accident victims who have been injured due to the
          negligence of others can seek compensation for damages. An experienced
          Lake Forest{" "}
          <Link to="/bicycle-accidents">bicycle accident lawyer </Link> can help
          injured victims and their families better understand their legal
          rights and options.
        </p>
        <h2>Bicycle Accident Statistics</h2>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Records System (SWITRS), there were no fatalities, but 19 injuries
          reported as a result of Lake Forest bicycle accidents in 2009. During
          that same year, 12 fatalities and 1,199 injuries occurred as a result
          of bicycle accidents in Orange County.
        </p>
        <h2>Common Bicycle Accident Causes</h2>
        <p>
          There are many potential causes of Lake Forest bicycle accidents. Bike
          accidents may occur as a result of:
        </p>
        <ul>
          <li>
            <b>Lack of visibility</b>: Many bicycle accidents occur because car
            drivers simply do not notice the bicyclist until it is too late.
            Motorists must remain attentive and focused so that they will notice
            any bicyclists who may be ahead or alongside them. Bicyclists can
            increase their visibility by wearing bright and reflective gear and
            by adhering to California laws pertaining to lamps and reflectors.
          </li>
          <li>
            <b>Speeding motorists</b>: When drivers exceed the posted speed
            limit, there is an increased chance for a bicycle accident to occur.
          </li>
          <li>
            <b>Distracted driving</b>: Drivers who take their eyes or attention
            off the roadway have an increased chance of striking a bicyclist.
            Car drivers should turn off their phones and remain focused while
            driving. Bicyclists about to enter an intersection would be well
            advised to make eye contact with the nearby motorists.
          </li>
          <li>
            <b>Drunk driving</b>: Alcohol is a contributing factor in many Lake
            Forest car accidents. Drivers who are under the influence are more
            likely to speed, drive recklessly or make dangerous choices.
          </li>
          <li>
            <b>Failure to yield</b>: Many bicycle accidents occur at street
            intersections. Under California law, bicyclists have the same right
            of way privileges as motorists. Car drivers who fail to yield to
            bicyclists at four-way stops and traffic lights may be held
            accountable for the injuries and damages caused.
          </li>
          <li>
            <b>Dangerous roadway conditions</b>: Often, bicycle accidents occur
            due to dangerous roadways or intersections. They may also occur due
            to defective street conditions such as potholes or uneven pavement.
            In such cases, the city or governmental agencies responsible for
            maintaining the roadway can be held liable. Under California
            Government Code Section 911.2, any personal injury or wrongful death
            claim against a governmental entity must be filed within 180 days of
            the incident.
          </li>
        </ul>
        <h2>Determining Liability</h2>
        <p>
          Proving who was responsible for an accident is an important of the
          claim process. Injured bicyclists have to prove not only who caused
          the accident, but also that the at-fault party was negligent in some
          way. This process is simplified when the at-fault motorist is cited or
          charged by the authorities for the crash. A driver does not, however,
          have to face charges or be cited in order to be held civilly liable
          for an incident. Was the motorist speeding? Did the motorist fail to
          notice the bicyclist while looking at a phone or talking to a
          passenger? A skilled bicycle accident attorney will review the
          circumstances of the case to help hold the negligent driver
          accountable for the injuries suffered by the victim.
        </p>
        <p>
          An experienced Lake Forest bicycle accident lawyer will help victims
          pursue compensation for all of their losses. Negligent car drivers can
          be held accountable for:
        </p>
        <ul>
          <li>Medical expenses</li>
          <li>Hospitalization fees</li>
          <li>Cost of rehabilitation services</li>
          <li>Lost wages</li>
          <li>Disability</li>
          <li>Pain and suffering</li>
          <li>Loss of earning potential</li>
        </ul>
        <p>
          Unfortunately, it is common for insurance companies to offer
          inadequate settlements that do not fully cover the losses suffered by
          the victim. A skilled Lake Forest bike accident attorney will fight to
          protect the best interests of injured victims. The experienced Lake
          Forest bicycle accident lawyers at our firm have a long and successful
          track record of helping injured victims and their families obtain fair
          and full compensation for their losses. Please contact us for a free,
          comprehensive and confidential consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
