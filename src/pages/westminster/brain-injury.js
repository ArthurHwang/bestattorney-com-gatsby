// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "brain-injury/doctor-examine-patient.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Brain Injury Attorney - Head Trauma Lawyer"
        pageDescription="Contact the Westminster Brain Injury Lawyers of Bisnar Chase at 949-203-3814 and receive a free consultation. The law offices of Bisnar Chase have been gaining wins for clients for 40 years. If you have experienced a crippling head injury we will fight for the compensation you deserve!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Brain Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster Brain Injury Lawyer"
            title="Westminster Brain Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <strong>
            {" "}
            Westminster{" "}
            <Link to="/head-injury" target="_blank">
              {" "}
              Brain Injury Lawyers
            </Link>
          </strong>{" "}
          at the law firm of Bisnar Chase have a long and successful track
          record of providing quality legal representation to catastrophically
          injured victims and their families.
        </p>
        <p>
          Brain injuries can have a devastating impact on the lives of victims
          and their families. Many head injuries leave victims with
          long-lasting, negative health effects that can have a significant
          impact on their quality of life.
        </p>
        <p>
          There are many instances in which a brain injury is the direct result
          of someone else's negligence or wrongdoing. For{" "}
          <strong> 40 years</strong> our Westminster brain injury attorneys have
          been winning millions in compensation for injuries caused by a third
          party's carelessness.
        </p>
        <p>
          If you or a loved one has suffered a traumatic brain injury in
          Westminster call us at <strong> 949-203-3814</strong> for a{" "}
          <strong> free consultation</strong>.
        </p>
        <h2>What is a Traumatic Brain Injury?</h2>
        <p>
          A{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="_blank">
            {" "}
            traumatic brain injury
          </Link>{" "}
          (TBI) is when the brain suffers trauma from an external mechanical
          force. It can lead to permanent or temporary impairment of physical,
          cognitive and psychosocial functions. It can also result in a
          diminished or altered state of consciousness. Victims who have
          suffered a TBI probably lost consciousness following the incident that
          caused it. They often struggle to remember what happened immediately
          before and after the accident. TBI patients take baby steps in terms
          of recovery. However, very often, they never make a complete recovery.{" "}
        </p>
        <h2>Brain Injury Symptoms</h2>
        <p>
          The human brain is an incredibly complex organ. A TBI can have
          wide-ranging physical and psychological effects. Some symptoms may
          appear right away while others may manifest over days or even weeks.
        </p>
        <p>
          <strong> Victims of a mild brain injury may</strong>:
        </p>
        <ul type="disc">
          <li>Lose consciousness for a few seconds or minutes</li>
          <li>Feel dazed, confused and disorientated</li>
          <li>Suffer from memory or concentration problems</li>
          <li>
            Struggle with headaches, dizziness, loss of balance, nausea or
            vomiting
          </li>
          <li>Have high sensitivity to light or sound</li>
          <li>
            Feel depressed, anxious, on edge, fatigued or prone to mood changes
          </li>
        </ul>
        <p>
          <strong> Moderate to severe brain injury symptoms include</strong>:
        </p>
        <ul type="disc">
          <li>Loss of consciousness for minutes or hours</li>
          <li>Profound confusion</li>
          <li>Slurred speech</li>
          <li>Combativeness, agitation and odd behavior</li>
          <li>Inability to wake from sleep</li>
          <li>Numbness and weakness in the extremities</li>
          <li>A loss of coordination</li>
          <li>Repeated vomiting, nausea, convulsions, seizures</li>
          <li>Dilation of one or both of the pupils</li>
        </ul>
        <p>
          <em>
            If you are dealing with sleep issues after a concussion, this{" "}
            <Link
              to="https://www.tuck.com/concussions-and-sleep/"
              target="_blank"
            >
              {" "}
              article
            </Link>{" "}
            can be helpful.{" "}
          </em>
        </p>
        <LazyLoad>
          <img
            src="/images/brain-injury/brain-injury-stats.jpg"
            width="100%"
            alt="Brain injury lawyers in Westminster"
          />
        </LazyLoad>

        <h2>Brain Injury Statistics</h2>
        <p>
          Everyday in the United States, we see individuals suffer traumatic
          brain injuries as a result of incidents such as auto accidents, falls
          and gun violence. In such cases, a skilled Westminster brain injury
          lawyer may be able to help the victim pursue financial compensation
          for their tremendous losses and hold the at-fault parties accountable.
        </p>
        <p>
          <strong>
            {" "}
            According to statistics provided by The U.S.{" "}
            <Link
              to="https://www.cdc.gov/traumaticbraininjury/index.html"
              target="_blank"
            >
              {" "}
              Centers for Disease Control and Prevention
            </Link>{" "}
            (CDC)
          </strong>
          :
        </p>
        <ul type="disc">
          <li>1.7 million people sustain a TBI each year.</li>
          <li>
            TBI is a contributing factor in 30.5 percent of all injury-related
            fatalities.
          </li>
          <li>
            Nearly half a million visits to emergency rooms for brain injuries
            each year are made by children between the ages of 1 and 14.
          </li>
          <li>
            Elderly adults 75 years and older have the highest rates of
            TBI-related hospitalization and death.
          </li>
        </ul>
        <h2>Brain Injury Costs</h2>
        <p>
          There are many costs related to brain injuries. Immediate costs
          include emergency room costs, diagnostic tests, surgery and
          hospitalization. Victims also may need to spend several months in the
          hospital followed by time at a rehabilitation facility where they may
          have undergo physical, speech and even occupational therapy. Victims
          of severe brain injuries may incur even more losses.
        </p>
        <p>
          Many TBI victims may never return to work and may require
          round-the-clock assistance. The future costs associated with a brain
          injury can be challenging to calculate. In such cases, non-economic
          losses such as pain and suffering, loss of consortium and emotional
          distress must also be considered while filing a claim.
        </p>
        <h2>Liability for Brain Injuries in Westminster</h2>
        <p>
          Depending upon how the incident that caused the injury occurred,
          financial compensation may be available for the victim's losses. If
          the injury occurred as the result of a car accident, the at-fault
          driver can be held accountable.{" "}
          <LazyLoad>
            <img
              src="/images/brain-injury/brain-injury-liabilty.jpg"
              width="100%"
              alt="Westminster head injury lawyers"
            />
          </LazyLoad>
          The victim will have to prove that the car driver's negligence or
          wrongdoing contributed to the incident. If the head trauma occurred in
          a slip-and-fall accident, the property owner or manager may be held
          accountable for the incident. The victim will have to prove that the
          incident resulted from a hazardous condition and that the property
          owner was aware of the conditions and failed to fix it.
        </p>
        <p>
          There are many potential causes of brain injury accidents and a number
          of potentially liable.
        </p>
        <h2>Typical Causes for Head Injuries</h2>
        <p>
          A traumatic brain injury occurs when a sudden blow to the skull
          occurs. Depending on how harsh of the blow, brain cells can be
          temporary or in more extreme case scenarios permanatley damaged.
        </p>
        <p>
          <strong>
            {" "}
            Reoccurring causes of traumatic head injuries can include
          </strong>
          :
        </p>
        <p>
          <strong>
            {" "}
            <Link to="/westminster/car-accidents" target="_blank">
              {" "}
              Auto accidents
            </Link>
          </strong>
          : People who experience a head injury in a car accident could have
          possibly hit their head on the steering wheel, windshield or may have
          experienced whiplash from the sudden movement. Car wrecks is one of
          the most dominant causes for head injuries in the United States.
        </p>
        <p>
          <strong> Slip and falls</strong>: The Center for Disease Control and
          Prevention reported that about 20%-30% of head injuries are sustained
          from a slip, trips and falls. The medical treatment needed for a slip
          and fall injury can amount to almost $40,000 dollars.
        </p>
        <p>
          <strong> Athletic accidents</strong>: One of the most popularized
          causes of brain injuries are sports. Many of those who suffer from
          sport-related brain injuries are young adults as well. Although
          shocking,{" "}
          <Link
            to="https://synapse.ucsf.edu/articles/2016/04/14/reality-brain-trauma-sports"
            target="_blank"
          >
            {" "}
            the reality of brain trauma
          </Link>{" "}
          indicates that the average age for victims of sport-related injuries
          is 14 years old.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Brain injury attorneys in Westminster"
            style={{
              backgroundImage:
                "url('/images/bisnar-chase/text-header-image-westminster.jpg')"
            }}
          >
            <h2>Consulting a Westminster Attorney</h2>
          </div>
        </LazyLoad>
        <p>
          <strong> Since 1978</strong>, the{" "}
          <strong> Westminster Brain Injury Lawyers</strong> of Bisnar Chase has
          been passionately fighting for the rights of injury victims. We
          understand that medical bills and treatment can get costly and we
          believe that you deserve to be compensated for your injuries.
        </p>
        <p>
          Many head trauma patients become immobile and lose out for a future
          income for their household. With our legal representation we can win
          you not only the money need to recover from your accident but also to
          support your family.
        </p>
        <p>
          Contact the Westminster brain injury attorneys of Bisnar Chase and
          receive a free case analysis on your traumatic head injury case.
        </p>
        <p>
          Call <strong> 949-203-3814</strong> today.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
