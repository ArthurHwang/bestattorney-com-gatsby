// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/pedestrian-accident-show-street.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Pedestrian Accident Attorneys - California"
        pageDescription="If you are in need of a Westminster pedestrian accident lawyer contact the California injury attorneys of Bisnar Chase. Our attorneys hold a 96% success rate and have won over $500 Million dollars in compensation for our clients. Call 949-203-3814 for a free consultation. No win, no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Pedestrian accident lawyer in Westminster"
            title="Pedestrian accident lawyer in Westminster"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          An experienced{" "}
          <strong>
            {" "}
            <Link to="/westminster">Westminster</Link> Pedestrian Accident
            Lawyer
          </strong>{" "}
          can help victims better understand their legal rights and options.
          There are some roadways that are just not safe for pedestrians. Even
          when motorists exercise caution or when pedestrians have the
          right-of-way, there are several dangerous and busy intersections in
          Westminster where accidents may still occur.
        </p>
        <p>
          The law firm of Bisnar Chase has been representing accident victims
          for over <strong> 40 years</strong> and within those years have
          sustained a <strong> 96% success rate</strong> in winning personal
          injury cases. With the help of our legal team, the attorneys of Bisnar
          Chase have won over <strong> $500 million dollars</strong> in
          compensation for people who have suffered at the hands of negligence.
          Our personal injury lawyers also work on a contingency fee basis.
        </p>
        <p>
          Contact the law offices of Bisnar Chase today at{" "}
          <strong> 949-203-3814</strong> for a{" "}
          <strong> free consultation</strong>.
        </p>
        <h2>Pedestrian Accident Facts and Statistics</h2>
        <p>
          Accidents involving pedestrians happen more often than many realize.
          In one year, the{" "}
          <Link
            to="https://www.ghsa.org/resources/spotlight-pedestrians18"
            target="_blank"
          >
            {" "}
            Governors Highway Safety Association
          </Link>{" "}
          about <strong> 6,000 people</strong> <strong> have died</strong> due
          to drivers being careless on the road.
        </p>
        <p>
          The primary cause of people getting injured while jogging, running or
          simply just by walking is due to distracted driving. There are ways in
          which you can avoid being involved in a pedestrian accident and
          causing harm to a pedestrian as a driver.
        </p>
        <p>
          <strong> Pedestrians should</strong>:
        </p>

        <ul>
          <li>only use marked crosswalks to cross the street</li>
          <li>never jay walk</li>
          <li>
            make eye contact with a driver so they know that they are walking
            across the street
          </li>
          <li>wear bright colors if they are running or walking at night</li>
          <li>avoid being on the phone while crossing the street</li>
        </ul>
        <p>
          <strong> Drivers should</strong>:
        </p>
        <ul>
          <li>not text or answer any phone calls while driving</li>
          <li>follow speed limits posted on the road</li>
          <li>approach a crosswalk slowly</li>
          <li>be extra cautious around crosswalks in bad weather conditions</li>
          <li>not drive under the influence of drugs or alcohol</li>
        </ul>

        <LazyLoad>
          <img
            src="../images/pedestrian-accidents/blurry-street-lights.jpg"
            width="100%"
            alt="Westminster pedestrian accident attorneys"
          />
        </LazyLoad>

        <h2>5 Roadway Examples that Can Lead to Pedestrian Accidents</h2>
        <p>
          There are a number of hazardous conditions that can result in a
          pedestrian accident. The governing body in charge of the roadway must
          make sure that the design and upkeep of the roadway is adequate.
        </p>
        <p>
          <strong>
            {" "}
            Here are a five examples of dangerous or defective roadway
            conditions that may lead to a potential claim
          </strong>
          :
        </p>
        <ol>
          <li>
            <strong> Inadequate shoulders, sidewalks, walkways</strong>: Some
            roadways are simply not designed for pedestrians. When a roadway is
            unfit for pedestrians, there should be signs posted to warn
            pedestrians.
          </li>
          <li>
            <strong> Negligent foliage maintenance</strong>: Something as simple
            as trimming trees and bushes can prevent pedestrian accidents. When
            a pedestrian crossing sign or stop sign is partially covered by
            branches, the governing body in charge of the roadway must act
            quickly to trim the surrounding trees. Failure to do so may affect
            visibility on the street.
          </li>
          <li>
            <strong>
              {" "}
              Poor{" "}
              <Link
                to="https://www.rospa.com/rospaweb/docs/advice-services/road-safety/roads/street-lighting.pdf"
                target="_blank"
              >
                {" "}
                street lighting
              </Link>
            </strong>
            : One way to avoid being involved in a late night pedestrian
            accident is to increase your visibility. Roadways that have poor
            lighting can be extremely dangerous. Pedestrians should try to stay
            in well-lit areas. When there is a history of accidents at a certain
            area because of a lack of lighting, the government must fix the
            situation.
          </li>
          <li>
            <strong> Poor roadway maintenance</strong>: It is especially
            important for cities to maintain the roadway near intersections and
            crosswalks. Cars that strike uneven surfaces and potholes can veer
            out of control putting pedestrians in the area at great risk of
            suffering an injury.
          </li>
          <li>
            <strong> Dangerous design</strong>: There are several design errors
            that can prove dangerous for pedestrians. Is a crosswalk near a hill
            that has limited visibility? Is there an intersection at the end of
            a curve where drivers cannot stop in time? Does the crosswalk need
            flashing lights or better signage? Are the crosswalk lines faded?
          </li>
        </ol>
        <h2>Potentially Liable Parties for a Pedestrian Accident</h2>
        <p>
          When negligence on the part of a governmental agency results in an
          injury accident, that governmental agency may be held liable for the
          injuries suffered by the victim.
        </p>
        <p>
          <strong> In such cases, the victim will have to show that</strong>:
        </p>
        <ul>
          <li>The accident resulted from a hazardous condition</li>
          <li>The governmental agency knew of the dangerous condition</li>
          <li>
            The agency's failure to fix the condition directly contributed to
            the accident.
          </li>
          <li>The victim suffered injuries and losses as a result</li>
        </ul>
        <p>
          In some cases, it may be possible to show that the location had a
          history of similar incidents and that the government failed to rectify
          the problem. Accident claims involving governmental agencies must be
          filed promptly. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            {" "}
            California Government Code Section 911.2
          </Link>
          , any personal injury claim against a public entity must be filed
          within 180 days of the incident.
        </p>
        <p>
          Proving fault in such cases can be a challenging process and fault
          needs to be proven so that you may compensated for medical bills and
          lost wages. A skilled Westminster pedestrian accident attorney can
          review all available evidence such as surveillance tapes, interview
          witnesses and work with investigators to prove how the accident
          occurred.
        </p>
        <p>
          If a driver or government entity is not responsible for a crosswalk
          accident, there are other parties that can be held liable for the
          injury.
        </p>
        <p>
          There are instances where the pedestrian themselves can be held
          accountable for the incident as well. If the pedestrian was not
          abiding the laws of the road, they were not only putting themselves in
          danger but drivers as well.
        </p>
        <p>
          Examples where a pedestrian can be potentially liable for an accident
          can be if the pedestrian was jaywalking, walking when the traffic
          signal did not permit them to cross the street yet, being intoxicated
          when they entered the street or walking into areas that are prohibited
          to pedestrians such as bridges and highways.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Westminster corsswalk collision attorneys"
            style={{
              backgroundImage:
                "url('/images/pedestrian-accidents/pedestrian-westminster-text-header.jpg')"
            }}
          >
            <h2>Westminster Injury Legal Representation</h2>
          </div>
        </LazyLoad>
        <p>
          The experienced{" "}
          <strong> Westminster Pedestrian Accident Lawyers</strong> at our law
          firm have an excellent track record of pursuing compensation from
          cities and other governmental agencies on behalf of seriously injured
          victims and their families.
        </p>
        <p>
          The attorneys of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          aims to have an exceptional attorney client relationship.{" "}
        </p>
        <p>
          We have the conviction, the resources and the tenacity to fight
          against these powerful agencies and help injured victims obtain fair
          compensation for their injuries, damages, pain and suffering.
        </p>
        <p>
          Call the law offices of Bisnar Chase today at{" "}
          <strong> 949-203-3814</strong> and earn a{" "}
          <strong> free legal advice </strong>with a team expert.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
