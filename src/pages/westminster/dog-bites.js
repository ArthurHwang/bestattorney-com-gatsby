// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "dog-bites/angry-dog-teeth.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Dog Bite Lawyers - Bisnar Chase"
        pageDescription="The Westminster dog bite lawyers of Bisnar Chase have been fighting for fair compensation for clients for 40 years. Our experienced injury attorneys hold a 96% success rate and have received over $500 Million in wins for our clients including serious dog bites. Call 949-203-3814 and get a free consultation today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Dog Bite Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster dog bite lawyer"
            title="Westminster dog bite lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been seriously injured by a dog bite, contact our{" "}
          <strong> Westminster Dog Bite Lawyers at 949-203-3814</strong>.
          Recovery can be expensive, and there is no better way to ensure a
          better recovery than finding the best legal representation for your
          dog bite case.
        </p>
        <p>
          With a <strong> 96% success rate</strong> our injury lawyers have not
          only won countless awards but have also won over{" "}
          <strong> $500 million dollars</strong> in compensation for victims of{" "}
          <Link to="/westminster" target="_blank">
            {" "}
            personal injuries
          </Link>
          .
        </p>
        <p>Do not settle for an attorney whose sole interest is to cash out.</p>
        <p>
          At the law firm of Bisnar Chase, we don't only want to offer top-notch
          legal advice, but we also want to provide you with an excellent
          attorney client relationship.
          <em>Our mission statement is</em>: "To provide superior client
          representation in a compassionate and professional manner while making
          our world a safer place."
        </p>
        <p>
          When you call our law offices today you will receive a{" "}
          <strong> free consultation</strong> with a legal team member.
        </p>
        <h2>Dog Bite Laws in Westminster, California</h2>
        <p>
          When it comes to being bitten by a dog, California enforces a strict
          liability regulation. Meaning that the dog owner or person who was
          supposed to be supervising the dog is directly responsible for the
          injury caused by the animal attack.
        </p>
        <p>
          Dog owners are not only responsible for the injuries that a dog caused
          but also for the property that was damaged. It is not uncommon for a
          dog to chase after someone on a bicycle and rip the wheel off of the
          bike. Owners are held liable for injuries and must compensate the
          victim for all the damages that had taken place during the dog attack.
        </p>
        <h2>Compensation for a Westminster Dog Attack</h2>
        <p>
          Animal attacks can leave a victim physically and emotionally wounded.
          It can take months for a person to heal from a dog bite.
        </p>
        <p>
          The types of damages that you can receive compensation is for{" "}
          <strong> lost wages, medical bills and pain and suffering</strong>.
        </p>
        <p>
          These kinds of damages that can be compensated for are known as
          punitive damages. Punitive damages are losses that can be assessed by
          the courts so the dog bite victim can be reimbursed financially.
        </p>
        <p>
          Hiring a Westminster dog bite attorney can increase your chances of
          gaining recovery for the damages you have accumulated from your
          injuries.
        </p>
        <p>
          <strong> For 40 years, the law firm of Bisnar Chase</strong> has taken
          on the toughest of injury cases. Our lawyers have the expertise you
          need to fight against dog bite owners and insurance companies and
          believe that you are entitled to compensation for your losses.
        </p>
        <p>
          <strong> Contact us at 949-203-3814</strong>.
        </p>
        <h2>Common Dog Bite Injuries</h2>
        <p>
          Dog bite injuries can be severe and detrimental to your health. It's
          important to seek treatment right away if you have been bitten by a
          dog. Otherwise, infection and further serious complications can occur.
        </p>
        <p>
          One common injury that many dog bite victims suffer from is punctured
          skin. After the skin is torn the objective is to prevent any
          infection. Many dogs especially stray dogs do not have all the
          necessary vaccinations for illnesses such as rabies and{" "}
          <Link
            to="http://www.magnoliavets.com/articles/general/418579-lyme-disease-your-pets"
            target="_blank"
          >
            {" "}
            Lyme disease
          </Link>
          .
        </p>
        <LazyLoad>
          <img
            src="../images/dog-bites/plastic-surgery-dog-bite.jpg"
            width="100%"
            alt="Dog bite attorney in Westminster"
          />
        </LazyLoad>
        <p>
          Dog bite victims may also have to deal with repairing damaged skin
          with plastic surgery. Being mauled by a dog on the arm or even on the
          face can leave victims with serious deformities. Newport Beach,
          California Plastic surgeon Dr.Daines says that it's not an easy fix to
          repair{" "}
          <Link
            to="https://dainesplasticsurgery.com/facial-rejuvenation/facial-reconstruction/facial-trauma/"
            target="_blank"
          >
            facial trauma
          </Link>{" "}
          from a dog bite.
        </p>
        <p>
          "Some parts of the face heal quite well and do not require the
          immediate attention of a specialist. However, areas such as the nose,
          eyelids or lips can be difficult to repair and require a more
          specialized skill set. Lost tissue, such as in the event of a dog
          bite, also requires advanced reconstructive techniques to achieve
          positive long-term results," shares Daines.
        </p>
        <h2>5 Ways You Can Protect Your Child From a Dog Bite</h2>
        <p>
          In Westminster, there are approximately 90,000 people and around
          18,000 dogs. With a high percentage of pedestrian traffic from mail
          carriers, to children walking home from school, these statistics
          foreshadow a high possibility of a large number of dog attacks.
        </p>
        <p>
          Children are the most dominant population effected by animal attacks.
          Research has indicated that there over 333,687 children bitten every
          year by dogs.
        </p>
        <p>
          There are preventive measures you can take to protect your child from
          being bitten by a dog.
        </p>
        <ol>
          <li>
            Do not leave your child alone with a dog, specifically with a dog
            she/he doesn't know
          </li>
          <li>Inform your child on how to safely engage with a dog</li>
          <li>Teach your child to never go up to a dog they do not know</li>
          <li>
            If they are interacting with a dog, leave your child in a space
            where they can escape just in case the dog has a sudden outburst
          </li>
          <li>
            Pay attention to the dog's behavior throughout time with your child
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog attack lawyers in Westminster"
            style={{
              backgroundImage:
                "url('/images/dog-bites/doberman-dog-bite-header-image.jpg')"
            }}
          >
            <h2>What Causes a Dog to Bite?</h2>
          </div>
        </LazyLoad>

        <p>
          Dogs at any size can pose a threat to people. There are numerous
          amounts of{" "}
          <Link to="/dog-bites/dangerous-dog-breeds">dangerous dog breeds</Link>{" "}
          that can strike at any moment, but why do dogs attack? Below are the
          top causes that lead to dog bites.
        </p>
        <p>
          <strong>
            {" "}
            Reasons why a dog would attack may be because the dog is
          </strong>
          :
        </p>
        <p>
          <strong> Sick</strong>: Dogs that are experiencing bruising or
          soreness in a certain part of their body may be attacking you or
          someone that even knew the dog because they do not feel good. The{" "}
          <Link
            to="http://www.pethealthnetwork.com/dog-health/dog-checkups-preventive-care/top-10-signs-your-dog-may-be-sick-and-what-you-can-do-about"
            target="_blank"
          >
            {" "}
            signs that your dog may be sick
          </Link>{" "}
          is if they suddenly become vicious, they are panting very hard or they
          are constantly urinating.
        </p>
        <p>
          <strong> Afraid</strong>: If a dog is not used to socializing with
          strangers this can lead to a dog attacking out of fear. It is
          important to socialize your dog when they are a puppy so they will not
          be aggressive in later years around strangers.
        </p>
        <p>
          <strong> Territorial</strong>: When a dog is trying to guard what they
          think is their territory and is approached by a stranger, this can
          lead to dangerous consequences. Certain dog breeds that are known to
          be very territorial are{" "}
          <Link to="/dog-bites/rottweiler-dog-breed" target="_blank">
            {" "}
            rottweilers
          </Link>
          ,{" "}
          <Link to="/dog-bites/pit-bull-dog-breed" target="_blank">
            {" "}
            pit bulls
          </Link>{" "}
          and Siberian Huskies. The way to avoid your dog attacking someone who
          is in "their territory" is to train them at an early age that space is
          to be shared.
        </p>
        <h2>Top-Notch Legal Representation in Westminster</h2>
        <p>
          The experienced <strong> Westminster Dog Bite Lawyers</strong> at
          Bisnar Chase have dedicated their practice to making sure that dog
          bite victims can recover from their injuries.
        </p>
        <p>
          Our legal representation has sustained a{" "}
          <strong> 96% success rate</strong> and we have won compensation for
          our clients that have paid off hefty medical expenses and wages lost
          from taking time off of work.
        </p>
        <p>
          If you have been severely injured by a dog bite, please feel free to
          contact our Westminster dog bite attorneys at our law office for more
          information regarding your situation.
        </p>
        <p>
          Get a <strong> free consultation</strong> today at
          <strong> 949-203-3814</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove St. #120 <br />
          Newport Beach, CA 92660
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
