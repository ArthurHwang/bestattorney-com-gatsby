// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "nursing-home/westminster-nursing-home-abuse-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Nursing Home Abuse Lawyers - Orange County, CA."
        pageDescription="The Westminster Nursing Home Abuse Lawyers of Bisnar Chase have been handling elderly abuse cases for 40 years. If you or a loved one has been a victim of nursing home neglect, contact the law firm of Bisnar Chase and speak to an experienced elder abuse attorney. Contact 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Nursing Home Abuse Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Nursing home abuse lawyers"
            title="Nursing home abuse lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          With more and more baby boomers heading to our nation's nursing homes
          and assisted living centers, nursing home abuse and neglect are
          quickly becoming an unfortunate yet common reality.
        </p>
        <p>
          If you find that your loved one has been a victim of these situations,
          contact the <strong> Westminster Nursing Home Abuse Lawyers</strong>{" "}
          of Bisnar Chase. Our attorneys can help you establish where to go and
          help hold the nursing home accountable for their actions.
        </p>
        <p>
          There are hundreds of reported incidents of abuse, exploitation and
          neglect every year in Orange County. Our legal representation has won
          our clients over
          <strong> $500 million dollars in compensation</strong> for victims of{" "}
          <Link to="/westminster" target="_blank">
            {" "}
            personal injuries
          </Link>
          .
        </p>
        <p>Don't let your loved one suffer anymore.</p>

        <p>
          <strong>
            {" "}
            For immediate assistance, please call 949-203-3814 for a free
            consultation.{" "}
          </strong>
        </p>

        <h2>Elder Abuse Occurs Very Often</h2>
        <p>
          Elder abuse affects more seniors than people are led to believe. The
          Nursing Home Abuse Center reported that around 1.4 million elderly
          residents have been abused in a nursing home. Elders who are 65 and
          older are the target group who are affected by senior living abuse.
        </p>
        <p>
          Orange County is no exception either. In one year over 1,526 abuse
          cases were reported in Orange County. The reality is that there may
          have even been more cases of abuse that go unreported. Due to the
          constant threatening by a staff or family member, elders that find
          themselves in an abusive situation usually do not report the abuse out
          of fear.
        </p>
        <h2>What to Do If Your Loved One is Being Abused</h2>
        <p>
          If you have exhibited multiple signs of physical and emotional abuse
          from your loved one you immediately want to take action to end their
          pain and suffering. Many family members do not know how to tackle this
          situation.
        </p>
        <p>
          <strong>
            {" "}
            The following are steps you can take to help your loved one
          </strong>
          .
        </p>
        <p>
          <strong>
            {" "}
            Document any prominent changes in their behavior or physical state
          </strong>
          : If the staff has not been attentive and denies any physical changes
          to your loved one's current state you need to take initiative with
          documentation.  Keep a record of abuse with photos or videos of your
          loved one.
        </p>
        <p>
          <strong> Report the abuse</strong>: If you suspect that abuse has
          occurred at a Westminster nursing home, it is critical that you file a
          report with the appropriate authorities. Gather information from
          witnesses and from workers who may have information about the
          incident. You will have to report your suspicions to the nursing home
          administrator, the department of health, the{" "}
          <Link to="http://ssa.ocgov.com/abuse" target="_blank">
            {" "}
            social services agency of your county
          </Link>{" "}
          and the local police, depending on the nature of the abuse. All forms
          of elder abuse are illegal and care facilities that allow it to occur
          on their premises should be held liable for their negligence.
        </p>
        <p>
          <strong> Seek legal representation</strong>: The Westminster Nursing
          Home Abuse Lawyers of Bisnar Chase believe that your loved one is
          deserving of the best senior care. Families everyday entrust their
          loved ones to staff and when that trust is broken, we will be there to
          help. Our attorneys are dedicated to helping your family member get
          the compensation they deserve for their pain and suffering. Call the
          law offices of Bisnar Chase at 949-203-3814 for a free case analysis.
        </p>
        <h2>
          <strong> Signs of Emotional Abuse</strong> in a Westminster Nursing
          Home
        </h2>
        <p>
          Many elderly residents are in emotional pain or distress because of
          the way they are treated by their caretakers. Emotional abuse may
          involve shouting or use of threatening language, ridicule and verbal
          humiliation or blaming the victim for things that are not his or her
          fault. Passive emotional abuse can include ignoring the resident or
          isolating him or her for lengthy periods of time.
        </p>
        <p>
          There are many potential warning signs of emotional abuse. You should
          be concerned if your loved one:
        </p>
        <ul type="disc">
          <li>Has become non-communicative or withdrawn</li>
          <li>
            Is exhibiting strange regressive behavior such as biting, sucking or
            rocking
          </li>
          <li>
            Has recently begun exhibiting signs of low self-esteem or depression
          </li>
          <li>
            Has become anxious, is quickly agitated or suffers from mood swings
          </li>
        </ul>

        <LazyLoad>
          <img
            src="../images/nursing-home/westminster-image-physical-abuse.jpg"
            width="100%"
            alt="Nursing home abuse attorneys in Westminster"
          />
        </LazyLoad>

        <h2>
          <strong> Signs of Physical Abuse</strong>
        </h2>
        <p>
          Physical abuse is the non-accidental use of physical force against an
          elderly person. Unlike emotional abuse, physical abuse can result in
          visible physical injuries that should raise red flags for the victim's
          loved ones. Remember, when a patient suffers an injury in a falling
          accident or some other type of accident, it is the nursing home's
          legal obligation to inform the family. Therefore, when there are
          unexplained injuries, it may be a sign of a cover up. Ask to see the
          medical records and don't be afraid to ask questions. Observable signs
          of physical abuse include:
        </p>
        <ul type="disc">
          <li>Unexplained physical injuries</li>
          <li>Broken bones</li>
          <li>Bruises that do not appear to be fall-related</li>
          <li>Sprains</li>
          <li>Welts</li>
          <li>Unexplained, rapid weight loss</li>
          <li>
            Trauma to the wrist or elbow that may reflect use of physical
            restraints
          </li>
          <li>Frozen joints</li>
          <li>Lacerations</li>
          <li>Skull fractures</li>
          <li>Broken eyeglasses</li>
          <li>Fearfulness</li>
        </ul>
        <p>
          Neglect is also a type of abuse that can sometimes be overlooked.{" "}
          <Link to="/nursing-home-abuse/bedsores.html">
            Bedsores and or pressure ulcers
          </Link>{" "}
          are a sign of neglect and can be serious or even fatal if left
          untreated.
        </p>
        <h2>
          <strong> Signs of Sexual Abuse</strong>
        </h2>
        <p>
          The despicable act of performing non-consensual sexual contact or
          interaction with an elderly person can, frighteningly enough, happen
          anywhere and at any time. It is the obligation of all Westminster
          nursing homes to perform the proper background checks to ensure that
          they do not hire an employee with a criminal history. Caregivers are
          in a position of authority and power. Using that position of dominance
          to perform sexual acts or to force the elderly person to perform such
          acts is not only morally reprehensible, but it is also criminal.
        </p>
        <LazyLoad>
          <img
            src="../images/nursing-home/westminster-nursing-abuse-second-image.jpg"
            width="100%"
            alt="Senior living abuse lawyers in Westminster"
          />
        </LazyLoad>
        <p>Signs of sexual abuse may include:</p>
        <ul type="disc">
          <li>Genital infections</li>
          <li>Unexplained venereal disease</li>
          <li>Torn or strained underclothing</li>
          <li>Vaginal or anal bleeding</li>
          <li>Bruising around the inner thighs, upper abdomen or breasts</li>
        </ul>
        <h2>Westminster Staff Worker Negligence</h2>
        <p>
          When nursing homes are understaffed this can lead to serious problems.
          Not only is the staff over worked and stressed but for new staff
          coming in they will never receive proper training.
        </p>
        <p>
          Senior care centers purposely remain understaffed to increase the
          profits of the employees. More workers means more money to spend on
          labor.
        </p>
        <p>
          Experts say that there is a{" "}
          <Link
            to="https://www.seniorcaring.com/resources/elder-abuse-the-difference-between-abuse-and-neglect"
            target="_blank"
          >
            {" "}
            difference between elderly abuse and elderly neglect
          </Link>
          . Neglect involves not being attentive to a senior resident's needs.
          The lack of care leads to a senior being vulnerable to injuries and
          sanitation services. Residents rely on the staff to become mobile most
          of the time and if they are left in a bed all day without physical
          therapy, the elder can suffer from serious skin conditions such as
          painful rashes and bed sores.
        </p>
        <p>
          The staff member is not the only liable for the neglect. The senior
          living care center can face serious legal repercussions as well.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Westminster nursing home neglect lawyers"
            style={{
              backgroundImage:
                "url('/images/nursing-home/westminster-nursing-home-text-header.jpg')"
            }}
          >
            <h2>Actions to Take if Abuse is Suspected</h2>
          </div>
        </LazyLoad>
        <p>
          An experienced <strong> Westminster Nursing Home Abuse Lawyer</strong>{" "}
          at Bisnar Chase will fight for your rights and help ensure that the
          negligent parties are held liable for their wrongdoing. Injured
          victims and their families can also seek financial compensation for
          the injuries, losses and emotional trauma caused.
        </p>
        <p>
          <strong> Since 1978</strong>, our injury attorneys have been helping
          injury victims gain millions in recovery for a third party's wrong
          doings. The Law Firm of Bisnar Chase has also held a
          <strong> 96% success rate</strong> at winning cases.
        </p>
        <p>
          If your loved one has been abused or neglected in a senior care
          center, please contact our Westminster nursing home abuse attorneys at{" "}
          <strong> 949-203-3814</strong>. Upon your call you will receive a free
          case analysis from a top-legal team expert.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
