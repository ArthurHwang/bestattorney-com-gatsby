// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "bus-accidents/bus-on-road.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Bus Accident Lawyer - Bisnar Chase"
        pageDescription="The Westminster bus accident lawyers have been helping injury victims for 40 years. Our injury attorneys have received $500 Million in wins for our clients including serious bus and commercial transportation accidents. Call 949-203-3814 for a free consultation!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Bus Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster bus accident lawyers"
            title="Westminster bus accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced <strong> Westminster Bus Accident Lawyers</strong> at
          Bisnar Chase have a <strong> 96% percent success rate</strong> of
          holding wrongdoers liable for their negligence.
        </p>
        <p>
          Injured victims can seek compensation for damages including medical
          expenses, loss of wages, cost of hospitalization, rehabilitation and
          other related damages.
        </p>
        <p>
          There are many potential causes of Westminster bus accidents. When a
          crash occurs, it must be determined who was responsible for the
          accident and how it could have been prevented. Victims of these
          devastating accidents can seek financial compensation for their
          considerable pain and suffering.
        </p>
        <p>
          When you contact our law offices at <strong> 949-203-3814</strong> you
          will receive a <strong> free consultation</strong> with a top-legal
          team expert. Bisnar Chase's Westminster{" "}
          <Link to="/westminster" target="_blank">
            {" "}
            personal injury lawyers
          </Link>{" "}
          can help injured victims better understand their legal rights and
          options.
        </p>
        <p>Call us today.</p>

        <h2>7 Types of Bus Accidents in Westminster</h2>
        <p>
          There are usually multiple contributing factors when it comes to bus
          accidents. Understanding the many types of bus accidents can prove
          useful when attempting to determine who was responsible for a crash.
        </p>
        <p>
          Here are some of the most common types of bus accidents that can have
          devastating consequences:
        </p>
        <ol>
          <li>
            <strong> Head-on collisions:</strong> Some of the most serious
            injury accidents involve a head-on collision between a bus and
            another vehicle. These types of accidents can occur at an
            intersection or on a length of roadway where there is no center
            median. Drivers who are fatigued or distracted can cause these types
            of bus accidents by letting their vehicle drift into oncoming
            traffic. Other bus drivers may cause a head-on collision by making
            unsafe turns without properly yielding the right of way.
          </li>
          <li>
            <strong> Rollover accidents:</strong> Buses are typically top heavy.
            Rollover crashes may occur when a bus driver attempts to take
            evasive action. Bus rollovers often occur as the result of tire and
            brake failure. When there is a tire blowout, a large bus can go out
            of control and roll over. Passengers also tend to suffer major
            injuries in such rollover accidents, particularly if there are no
            seatbelts and they get ejected from the vehicle.
          </li>
          <li>
            <strong> Lane change collisions:</strong> Bus drivers must make sure
            that it is safe to change lanes or make a turn. Buses particularly
            have large blind spots, which is why bus drivers must be extra
            careful when they make a lane change.
          </li>
          <li>
            <strong> High-speed accidents:</strong> Bus drivers must not only
            remain under the speed limit, but they must also travel at a speed
            that is safe relative to the weather, roadway and traffic
            conditions. Bus drivers who travel too fast will struggle to come to
            a stop in time to avoid a collision.
          </li>
          {/* <LazyLoad>
            <img
              src="../images/bus-accidents/bus-busy-road.jpg"
              width="100%"
              alt="Bus crash lawyers in Westminster"
            />
          </LazyLoad> */}
          <li>
            <strong> Rear-end collisions:</strong> Was the bus driver
            tailgating? Was he or she traveling too fast to stop in time? Did
            the bus's brakes fail to work properly? Did the driver or bus owner
            properly inspect the brakes? Did a mechanic fail to properly install
            the brakes? Regardless of the cause, a rear-end bus collision can
            have devastating consequences.
          </li>
          <li>
            <strong> Drunk driving collisions:</strong> While passenger vehicle
            drivers are not over the legal limit until their blood alcohol
            content level reaches .08 percent, commercial drivers may not drive
            with a BAC of .04 percent or higher in California. Driving under the
            influence of alcohol and/or drugs is illegal under{" "}
            <Link
              to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=VEH&sectionNum=23152"
              target="_blank"
            >
              {" "}
              California law
            </Link>
            .
          </li>
          <li>
            <strong> Poor maintenance:</strong> When an accident occurs due to
            poor bus maintenance, the bus company can be held liable. Bus
            companies are required to conduct thorough safety checks to make
            sure that the vehicle is suitable for transporting passengers
            safely.
          </li>
        </ol>
        <p>
          The law offices of Bisnar Chase have dealt with a variety of bus
          accident cases for over <strong> 40 years</strong>. Our bus accident
          lawyers know that bus drivers and bus companies can be careless at
          times and we want to be the ones to fight for the compensation you
          deserve. The law firm of Bisnar Chase has won over{" "}
          <strong> $500 million dollars</strong> in recoveries for our clients.
          Contact us today at <strong> 949-203-3814</strong> for a
          <strong> free case evaluation</strong>.
        </p>
        <h2>Common Westminster Bus Crash Injuries</h2>
        <p>
          Injuries sustained in a bus accident will depend on the severity of
          the collision. Some injury victims can become permanently paralyzed or
          lose their lives. People who have suffered serious injuries should
          seek medical attention immediately.
        </p>
        <p>Below are the most common types of bus accident injuries:</p>
        <p>
          <strong>
            {" "}
            <Link to="/westminster/brain-injury" target="_blank">
              {" "}
              Traumatic brain injury
            </Link>{" "}
            or spinal cord injury
          </strong>
          : An estimated amount of 14,000 bus accidents that have occurred
          resulted in serious injuries, most of them being head injuries. Buses
          weigh more than a regular four-door vehicle and for that reason can
          cause an extensive amount of neurological damage to the brain. Victims
          of bus accident injuries may experience open head wounds, second
          impact syndrome or whiplash.
        </p>
        <p>
          <strong> Broken bones</strong>: The impact of a bus crash can result
          in victims experiencing shattered bones. Buses may roll over and
          because many buses do not have seatbelts installed, people can be sent
          flying into different parts of the vehicle. Bone fractures and{" "}
          <Link
            to="https://www.hoagorthopedicinstitute.com/orthopedic-services/trauma-fracture/"
            target="_blank"
          >
            {" "}
            bone bruising
          </Link>{" "}
          can end up being severe which can leave many bus accident victims in
          the emergency room. Emergency room visits, treatment and
          rehabilitation can lead to costly medical bills.
        </p>
        <p>
          <strong> Cuts and lacerations</strong>: Glass breaking during the bus
          crash can cause deep wounds and breakage in the skin. The types of
          lacerations that accident victims can experience during a bus crash
          are superficial, deep, clean or a complicated laceration. A person
          suffering from a complicated laceration can mean that different types
          of muscle tissue which includes nerves, organs and muscles can be
          severely affected.
        </p>

        {/* <LazyLoad>
          <div
            className="text-header content-well"
            title="Bus crash attorneys in Westminster"
            style={{
              backgroundImage:
                "url('../images/bus-accidents/bus-accident-lawyer.jpg')"
            }}
          >
            <h2>Rights of Injured Victims in Westminster</h2>
          </div>
        </LazyLoad> */}

        <h2>Rights of Injured Victims in Westminster</h2>
        <p>
          The Westminster Bus Accident lawyers know that insurance companies do
          not always fight for your rights. Families that have lost a loved one
          as a result of bus accidents can file a{" "}
          <Link to="/wrongful-death">wrongful death claim</Link> seeking
          compensation for medical and funeral expenses, lost future income and
          benefits and loss of love and companionship.
        </p>
        <p>
          Bus companies, insurance providers and other potentially liable
          parties will likely fight any claims made against them. It is critical
          that bus accident victims and their families get a knowledgeable
          Westminster bus accident attorney on their side to fight for their
          rights and look out for their best interests.
        </p>
        <p>
          If you have experienced pain and suffering due to a third party's
          negligence, please call us at <strong> 949-203-3814</strong> for a
          free evaluation on your injury case.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
