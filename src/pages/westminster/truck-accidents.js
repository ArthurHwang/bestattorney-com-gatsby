// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "truck-accidents/big-rig-westminster-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Truck Accident Lawyers - Bisnar Chase"
        pageDescription="The experienced Westminster truck accident lawyers of Bisnar Chase have been fighting for fair compensation since 1978. If a negligent truck driver caused a serious or catastrophic injury, injured victims can seek compensation from the truck driver and his employer for the damages caused. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Truck Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster truck accident lawyer"
            title="Westminster truck accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Truck accident victims who suffer from catastrophic injuries are often
          left facing significant medical expenses, taking significant time off
          work or are left with no way to earn a livelihood because of their
          disabilities.
        </p>
        <p>
          A skilled{" "}
          <strong>
            {" "}
            <Link to="/westminster" target="_blank">
              {" "}
              Westminster
            </Link>{" "}
            Truck Accident Lawyer
          </strong>{" "}
          can help you better understand your legal rights and options. Our
          attorneys have held a <strong> 96% percent success rate</strong>{" "}
          winning personal injury cases and have earned injury victims
          <strong> millions of dollars in compensation</strong>.
        </p>
        <p>
          If you have been severely injured in a truck crash due to a negligent
          truck driver you have the right to seek legal representation for your
          Westminster case.
        </p>
        <p>
          Call the law offices of Bisnar Chase at <strong> 949-203-3814</strong>{" "}
          for a <strong> no charge case analysis</strong>.
        </p>

        <h2>Liability for a Westminster Truck Accident</h2>
        <LazyLoad>
          <img
            src="../images/truck-accidents/cargo-semi-truck-loader.jpg"
            width="200"
            className="imgright-fixed"
            alt="Truck crash attorneys in Westminster"
          />
        </LazyLoad>
        <p>
          When it comes to truck collisions, proving accountability can be a
          complex process. Semi-trucks usually do not belong to the driver
          operating the truck. The company that the big rig belongs to can be
          liable for the truck accident.
        </p>
        <p>
          In truck accident cases, the negligent driver who caused the accident
          and his or her employer, the trucking or distribution company, can be
          held liable for the victim's injuries, damages and losses.
        </p>
        <p>
          Companies who own the semi-trucks or the companies who are occupying a
          truck under a lease have a responsibility to maintain the upkeep of
          the vehicles. The reason why some companies avoid consistently
          checking on the health of the truck is because they want to avoid
          paying for expensive auto parts. Since commercial trucks are larger in
          weight and size, a multitude of problems can arise.
        </p>
        <p>
          <strong>
            The{" "}
            <Link
              to="https://httparts.net/2016/04/five-common-semi-truck-issues-that-occur-out-on-the-open-road/"
              target="_blank"
            >
              {" "}
              types of issues
            </Link>{" "}
            that can arise as a truck driver are issues with
          </strong>
          :
        </p>

        <ul>
          <li>Radiator</li>
          <li>Leakage of oil</li>
          <li>Water pump</li>
          <li>Fan clutch</li>
          <li>Battery</li>
        </ul>
        <p>
          It is the obligation of the truck company to make sure that all parts
          of the truck are working properly. The manufacturer, loader of the
          truck and driver can also be held accountable for severe injuries
          obtained in a truck accident.
        </p>

        <p>
          It would be in the victim's best interest not to rush to a settlement
          or take the first offer from the insurance company. Trucking companies
          have powerful insurance companies and defense lawyers on their side
          who will do everything it takes to protect their best interests. A{" "}
          <strong> Westminster Truck Accident Lawyer</strong> can help you
          investigate the cause of the accident and can win you the compensation
          you would need to recover.
        </p>
        <h2>
          <strong> Types of Truck Accident Injuries</strong>
        </h2>
        <p>
          Truck accidents have the potential to cause catastrophic injuries. A
          catastrophic injury is one that leaves victims with lifelong health
          issues or disabilities.
        </p>
        <p>
          <strong>
            {" "}
            There are several types of major or catastrophic injuries that may
            be sustained in a Westminster truck crash.
          </strong>
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              <Link to="/westminster/brain-injury" target="_blank">
                {" "}
                Traumatic brain injury
              </Link>
              :
            </strong>{" "}
            Auto accidents are among the most common causes of traumatic brain
            injury. A brain injury occurs when there is impact to the head. In
            truck accidents, there is likely to be intrusion or blunt force
            trauma depending on the nature of the accident. For example, severe
            head injuries are common in truck underride accidents where the
            smaller vehicle slides under the back or side of the trailer.
            Traumatic brain injuries can be challenging to treat. Victims often
            undergo multiple surgeries, extensive rehabilitation and therapy.
            Some of them may even require full-time care 24/7. The cost of a
            traumatic brain injury can add up to millions of dollars over the
            lifetime of the victim.
          </li>
          <li>
            <strong> Spinal cord damage:</strong> A severe spinal cord injury
            will usually result in paralysis. Quadriplegia or paraplegia are
            common consequences of such truck accident injuries. Victims are
            left with lifelong disabilities in addition to undergoing
            significant medical treatment and rehabilitation. Injured victims
            may have to be hospitalized for weeks or even months. Those who are
            paralyzed may need specialized equipment. All of these costs could
            add up very quickly.
          </li>
          <li>
            <strong> Fractures:</strong> Broken bones can result in serious
            injuries. Depending on the type of fracture, it could take a long
            time for the victim to recover. Even a so-called minor fracture or
            hairline crack may require several physical therapy sessions. When
            there is a serious fracture, the victim will most likely need
            several months of therapy or rehabilitation in order to regain
            strength and mobility.
          </li>
          <li>
            <strong> Limb amputations:</strong> This refers to a limb that may
            have to be amputated as a result of serious injuries that may be
            suffered in a Westminster truck accident. Amputations of course have
            lifelong consequences for the victim. For example, he or she may
            never be able to return to his or her old job or remain involved in
            activities enjoyed prior to the accident.
          </li>
        </ul>
        <h2>
          <strong> Damages That Can Be Claimed for Westminster</strong> Truck
          Crash
        </h2>
        <p>
          Injured Westminster semi-truck accident victims can seek compensation
          for damages including but not limited to:
        </p>
        <ul type="disc">
          <li>Medical expenses</li>
          <li>Loss of wages</li>
          <li>Cost of hospitalization</li>
          <li>Rehabilitation</li>
          <li>Physical therapy</li>
          <li>Vocational training</li>
          <li>Disabilities</li>
          <li>Loss of ability to earn a livelihood</li>
          <li>Pain and suffering</li>
          <li>Emotional distress</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Westminster truck collision attorneys"
            style={{
              backgroundImage:
                "url('../images/truck-accidents/Tustin Truck Lawyers Text Header.jpg')"
            }}
          >
            <h2>The Dangers Truck Accidents Pose</h2>
          </div>
        </LazyLoad>
        <p>
          Semi-trucks can pose an even bigger threat than four-door vehicles
          because if an accident occurs the big rig can flip over and cause an
          extensive amount of damage.
        </p>
        <p>
          The trucks weight is dependent on a variety of factors such as the
          types of loads the truck is carrying and the weight of the cargo. Both
          should be thoroughly checked and secured by loaders before driving.
        </p>
        <p>
          The federal government has enforced weight limits on semi-trucks to
          prevent accidents. Any entity that violates these regulations will not
          only put themselves in danger but will also be facing expensive fees.
        </p>
        <p>
          Due to the trucks weight, drivers need to be extra cautious not to
          speed to refrain from{" "}
          <Link to="https://en.wikipedia.org/wiki/Jackknifing" target="_blank">
            {" "}
            jackknifing
          </Link>
          . Jackknifing is a common term used among truck drivers in which the
          truck skids forcing the front (towing) part of the truck to swerve
          into an angle that is facing the rest of the truck. Jackknifing is
          very common amongst truck drivers who do not take extra precautions
          when driving in bad weather conditions.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/83PbsxUd_cM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>
          <strong> Bisnar Chase's Injury Lawyers Will Fight For You</strong>
        </h2>
        <p>
          Our experienced <strong> Westminster Truck Accident Lawyer </strong>at{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          have helped numerous people with a variety of personal injury cases.
          Our truck collision attorneys have held negligent parties accountable
          and have obtained fair compensation for the losses injury victims have
          experienced.
        </p>
        <p>
          Please contact the law group of Bisnar Chase at{" "}
          <strong> 949-203-3814</strong> for a{" "}
          <strong> free consultation</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
