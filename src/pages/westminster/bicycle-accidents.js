// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "bike-accidents/bike-injury-image-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Bicycle Accident Lawyer - Orange County, CA"
        pageDescription="Bicycle accident victims have little to no knowledge of what to do after a bike accident. The Westminster Bicycle Accident Lawyers of Bisnar Chase will be there for you from start to finish. If you have been involved in an accident contact 949-203-3814 and receive a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Bicycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster bicycle accident lawyers"
            title="Westminster bicycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          For more than <strong> 40 years</strong>, the knowledgeable{" "}
          <strong> Westminster Bicycle Accident Lawyers</strong> at the law firm
          of Bisnar Chase have helped injured victims and their families receive
          fair and full compensation for their losses.
        </p>
        <p>
          Bicycle riders are more susceptible to injuries since they are not
          shielded from the elements and surrounding cars. Many people end up
          with serious wounds and at times lose their lives for just simply
          riding in the bike lane.
        </p>
        <p>
          The Westminster bicycle accident attorneys at Bisnar Chase fight hard
          for the rights of clients to ensure that they are justly compensated
          for lost wages and medical bills. Our results have amounted to over{" "}
          <strong> $500 million dollars</strong> in recoveries for bike crash
          victims. We have also sustained a{" "}
          <strong> 96% percent success rate</strong> in winning injury cases.
        </p>
        <p>
          If you or a loved one has been injured in a bike accident, please
          contact the{" "}
          <Link to="/westminster" target="_blank">
            {" "}
            personal injury
          </Link>{" "}
          law offices of Bisnar Chase at{" "}
          <strong> 949-203-3814 for a free consultation</strong>.
        </p>
        <h2>Bicycle Accident Statistics</h2>
        <p>
          Bicycle accidents are common on Westminster's busy roadways and they
          often result in serious injuries for bicyclists. According to recent
          statistics by the{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="_blank"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System
          </Link>
          (SWITRS), one person was killed and 46 were injured in Westminster
          bicycle accidents.
        </p>
        <p>
          Throughout Orange County, 12 fatalities and 1,199 injuries were
          reported as a result of bicycle accidents during that same year. When
          bicyclists are injured as the result of someone else's negligence,
          they may be able to seek compensation for their injuries and losses.
        </p>

        <LazyLoad>
          <img
            src="../images/bike-accidents/man-on-floor.jpg"
            width="100%"
            alt="Bike injury lawyers in Westminster"
          />
        </LazyLoad>

        <h2>3 Common Causes of Bike Crashes</h2>
        <p>
          There are many different causes of bicycle accidents. Some occur
          directly due to the carelessness of other motorists. What is true
          about all these factors though is that these accidents could have been
          prevented. Below are the most frequent elements that play into a
          bicycle crash.
        </p>
        <ol>
          <li>
            <strong> Failing to yield</strong>: Car drivers commonly fail to
            yield the right of way to bicyclists when entering traffic or while
            making a turn. Over 7.1% percent of intersection collisions are due
            to drivers not yielding to bicycle riders. In California, motorist
            must give at least three feet worth of space to cyclists.
          </li>
          <li>
            <strong> Speeding</strong>: Other drivers put bicyclists in danger
            by driving over the speed limit. Drivers must comply to the speed
            limits that are posted on highways or increase their speed if
            conditions are safe. If a driver is stopped for speeding and
            receives a ticket they can face up to $200 dollars in fines.
          </li>
          <li>
            <strong> Hazardous roadway condition</strong>: Accidents that
            involve dangerous road conditions such as large potholes, uneven
            surfaces and slick roadway conditions can have deadly results.
            County, city and state officials are responsible for the maintenance
            of roads. Liability for the poor road structures belong to a handful
            of government entities.
          </li>
        </ol>
        <p>
          Motorist are not the only accountable parties in a bicycle accident.
          Under the California bicycle law, riders have the same rights and
          responsibilities as drivers when{" "}
          <Link
            to="https://www.dmv.ca.gov/portal/dmv/detail/pubs/brochures/fast_facts/ffdl37"
            target="_blank"
          >
            {" "}
            sharing the road
          </Link>
          .
        </p>
        <h2>Injuries Suffered in Westminster Bicycle Accidents</h2>
        <p>
          Bicyclists are extremely vulnerable because they have very little in
          terms of protection. Even bicycle riders who are wearing helmets can
          suffer life-changing head injuries when they strike a vehicle or the
          roadway.
        </p>
        <p>
          Victims of bicycle accidents often suffer multiple bone fractures and
          serious lacerations. Others may suffer debilitating neck and back
          injuries. In the most severe accidents, bicyclists may even sustain
          trauma to their spinal cord resulting in permanent disabilities.
        </p>
        <p>
          The most frequent types of injuries is acute trauma. Bicycle accident
          related injuries usually are experienced by the upper part of the body
          such as the face, head or arms. The term road rash refers to a bike
          crash victim falling and then suffering from superficial injuries such
          as scrapes or cuts that are not that deep.
        </p>
        <p>
          Bicycle accident victims are strongly suggested to seek medical
          attention immediately after their collision even if injuries look
          minor.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bicycle accident attorneys in Westminster"
            style={{
              backgroundImage:
                "url('/images/bike-accidents/liability-image.jpg')"
            }}
          >
            <h2>Liability for Bicycle Collisions</h2>
          </div>
        </LazyLoad>

        <p>
          In order to pursue financial compensation for losses suffered in a
          Westminster bicycle accident, the injured cyclist will have to prove
          who was responsible for the incident. In most cases, this involves
          proving that a car driver was negligent at the time of the incident.
          Forms of driver negligence include driving while distracted, fatigued
          and while under the influence.
        </p>
        <p>
          Motor vehicles are not always the liable party for a bicycle accident.
          In some cases the bicycle manufacturer may be liable for the injuries
          suffered. Was a recent recall issued on the bicycle? Did a defective
          bicycle part cause the injury accident? If so, the bicycle
          manufacturer or the maker of the defective part can be held liable.
        </p>
        <p>
          When a bicycle accident occurs as the result of a dangerous or
          defective roadway, the governmental agency in charge of maintaining
          that roadway can be held liable. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            {" "}
            California Government Code Section 911.2
          </Link>
          , any personal injury or wrongful death claim against a governmental
          agency must be filed within 180 days of the incident.
        </p>
        <h2>Compensation for Westminster Bike Victims</h2>
        <p>
          Injured bicycle accident victims can seek compensation from the
          at-fault parties for damages including but not limited to medical
          expenses, loss of wages, cost of hospitalization, rehabilitation and
          pain and suffering. Many bicycle accident victims suffer major or
          catastrophic injuries that require lengthy hospital stays, surgeries
          and costly rehabilitative treatments. Often, victims and their
          families end up paying out-of-pocket for these continuing treatments.
          Victims may also not be able to return to work for an extended period
          of time. Some may never be able to return to work.
        </p>
        <h2>Contacting a Bicycle Accident Lawyer</h2>
        <p>
          The Westminster Bicycle Accident Lawyers of Bisnar Chase have been
          providing excellent legal representation that has given clients great
          results since 1978. Our lawyers do not only work hard to get you the
          compensation you deserve but they also work on a contingency fee
          basis. That means that if there is no win there is no fee. An
          insurance company will not always have your best interest at hand.
        </p>
        {/* <LazyLoad>
          <img
            src="../images/bisnar-chase/westminster-end-image.jpg"
            width="100%"
            alt="Westminster bicycle accident attorneys"
          />
        </LazyLoad> */}
        <p>
          Call us today at <strong> 949-203-3814</strong>. When you call you
          will receive a free case analysis for your bicycle accident case.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
