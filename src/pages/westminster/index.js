// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/santa-ana-personal-injury.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Personal Injury Lawyer - Orange County, CA"
        pageDescription="If you've been injured in a personal injury accident, contact the Westminster personal injury lawyers of Bisnar Chase. We have been helping California plaintiffs for over 40 years and may be able to help you too. 96% success rate, free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster personal injury lawyers"
            title="Westminster personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> Westminster Personal Injury Lawyers</strong> of Bisnar
          Chase have been litigating cases involving accident victims for 40
          years.
        </p>
        <p>
          Injuries sustained from{" "}
          <Link to="/westminster/car-accidents" target="_blank">
            {" "}
            car accidents
          </Link>{" "}
          or slip and falls happen more often than people know. The people that
          are involved in these incidents are left in the emergency room with
          hefty medical bills. The law firm of Bisnar Chase has won over{" "}
          <strong> $500 million dollars in compensation</strong> for clients who
          have faced many financial losses from a catastrophic occurrence.
        </p>
        <p>
          If you would like to receive a <strong> free consultation</strong> for
          your personal injury case, contact the Westminster personal injury
          attorneys at Bisnar Chase. When you call our law offices at{" "}
          <strong> 949-203-3814</strong> you will have the opportunity to speak
          to a top-legal expert.
        </p>

        <h2>What Qualifies as a Personal Injury?</h2>
        <p>
          A personal injury is any physical, mental or emotional damage acquired
          by an accident. The accident that caused the harm to the victim is
          usually due to someone else's negligence.
        </p>
        <p>
          A person who has experienced pain and suffering due to the
          carelessness of a third party can file a claim for a personal injury.
        </p>
        <p>
          There are many types of incidents that can qualify as a personal
          injury, but there are two core aspects that need to be proven so an
          accident victim can gain the highest compensation for their injury.
          The two primary elements that must be proven in a personal injury case
          are "duty of care" and "breach of duty".
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Duty_of_care"
              target="_blank"
            >
              {" "}
              Duty of Care
            </Link>
          </strong>
          : Although there is not a set definition for "duty of care" there is a
          legal responsibility that the Supreme court has enforced among parties
          such as doctors and manufacturers to properly care for their clients.
          The duty does not only fall on professionals and business owners
          though, this duty of care is also extended to employers and drivers.
          Duty of care must be proven in order to hold a third party accountable
          for a victim's injuries.
        </p>
        <p>
          <strong> Breach of Duty</strong>: When a third party has been
          determined for being a primary factor for an accident, it can then be
          determined that the third party violated their duty of care. One of
          the main questions that the courts will want to be answered is if the
          negligent party foresaw the risk of injury or was notified but did not
          take action to prevent injuries. For instance, if a manager was
          notified by an employee that the floor was slippery and the manager
          did not put a caution sign to warn customers to walk in that area, the
          courts will see this as an act of carelessness.
        </p>

        <LazyLoad>
          <img
            src="/images/car-accidents/car-accident-westminster.jpg"
            width="100%"
            alt="Westminster personal injury attorneys"
          />
        </LazyLoad>

        <h2>5 Common Types of Personal Injury Cases</h2>
        <p>
          Catastrophic accidents that could have been prevented occur everyday.
          What's worse is that many lose their lives at the hands of someone's
          negligence. If you've been wrongly injured, somebody needs to be held
          accountable - both morally and financially.
        </p>
        <p>
          Catastrophic accidents that could have been prevented occur every day.
          What's worse is that many lose their lives at the hands of someone's
          negligence. If you’ve been wrongly injured, somebody needs to be held
          accountable - both morally and financially.
        </p>
        <p>
          <strong>
            {" "}
            Personal injury cases encompass more than you might think, they can
            include and result from:
          </strong>
        </p>
        <ol>
          <li>
            <strong> Car accidents</strong>: According to{" "}
            <Link
              to="https://www.driverknowledge.com/car-accident-statistics/"
              target="_blank"
            >
              {" "}
              DriverKnowledge.com
            </Link>
            , over two million people suffer from permanent injuries every year
            due to car accidents. Auto accidents have a multitude of causes that
            range from drunk driving, speeding or texting and driving. Experts
            say that the way in which you can prevent an accident is by abiding
            by traffic laws and not driving distracted.
          </li>
          <li>
            <strong> Dog bites</strong>: Dogs can do more damage to a person
            than many people think. Reports have stated that over 27,000 cases
            of reconstruction surgery were performed in one year to restore the
            damages caused by dog bites.{" "}
            <Link to="/dog-bites/statistics" target="_blank">
              {" "}
              Dog Bite Statistics
            </Link>{" "}
            state that the primary victims of dog attacks are children and
            senior citizens. If you are approached by a dog that you are not
            familiar with do not look the dog directly in the eye and even if
            you feel compelled to, do not run, slowly proceed away from the dog.
          </li>
          <li>
            <strong> Defective products</strong>: In the United States millions
            of people have suffered serious injuries at the hands of product
            defects. Some of those product defects include house maintenance
            supplies, chemicals or children's products. Liable parties for
            catastrophic injuries can be the product designer or product
            manufacturer. If you or someone you know has experienced pain and
            suffering and are seeking representation for a products liability
            case, the Westminster Personal injury lawyers of Bisnar Chase will
            take on the big corporations for you and get you the highest
            compensation you deserve for your defective product case.
          </li>
          <li>
            <strong> Premises liability</strong>: A premise liability claim
            involves a victim being severely injured on the defendant's
            property. The most common premise liability lawsuit is a slip and
            fall injury. Slip and falls take place if a plaintiff slipped on wet
            floors or tripped over a poorly constructive set of stairs.
            Liability does not always fall on a property owner though. The
            employee or the supervisor at the time being of a store can also be
            held accountable for injuries especially if they were informed and
            did not do anything about it.
          </li>
          <li>
            <strong> Work accidents</strong>: Many employees are under the
            impression that when they are hurt at work, the company's insurance
            or workers compensation will cover the costs for the injuries.
            Although that may be true most of the time there are instances when
            an employee is not given the full amount of compensation. Lost wages
            and temporary disability may not be covered and workers are left to
            fend for themselves.
          </li>
        </ol>
        <h2>Where Do I Begin with my Personal Injury Claim?</h2>
        <p>
          Every personal injury case differs from the other in terms of injuries
          and damages that were obtained during the incident. The process of
          filing a claim can be daunting and complicated. Most accident victims
          are unaware of where to start when filing their personal injury claim.
          There are steps that you can take that would make the process easier.
        </p>
        <LazyLoad>
          <img
            src="../images/personal-injury/personal-injury-claim.jpg"
            width="100%"
            alt="Personal injury attorneys in Westminster"
          />
        </LazyLoad>
        <p>
          <strong>
            {" "}
            Actions you can take when you pursuing your personal injury case are
          </strong>
          :
        </p>
        <p>
          <strong> Ask if the insurance company will cover the damages</strong>:
          It is important before filing a personal injury claim for a car
          accident or slip and fall that you first inquire if the insurance
          company will offer you the compensation you would need to recover.
          There are instances though when you may be involved in an accident and
          the negligent party does not have insurance. If the liable entity does
          not have any assets to recover than the plaintiff may have a difficult
          time gaining any recovery for the damages.
        </p>
        <p>
          <strong> Speak to a personal injury attorney</strong>: If you have
          experienced catastrophic injuries, it is in your best interest to
          speak to a personal injury lawyer. Many law firms provide a free
          consultation in which they will discuss whether you have a strong case
          to take to the courts. At times you may be offered a contingency fee
          as well. A contingency fee means that if your lawyer does not win your
          case you do not have to pay. Hiring an injury lawyer will increase
          your chances of gaining the maximum compensation for your case.
        </p>
        <p>
          <strong>
            {" "}
            <Link to="http://www.courts.ca.gov/9616.htm" target="_blank">
              {" "}
              Filing a lawsuit
            </Link>
          </strong>
          : There are methods you can take to win the compensation you need
          before filing a claim. If the insurance company of the "third party"
          does agree to cover the cost of your injuries then you might not have
          to bring your case to the courts. Even if the insurance company has
          agreed to grant you the compensation you need if they are stalling or
          taking too long to reach a settlement you can still file a lawsuit.
        </p>
        <p>
          The Westminster Personal Injury Lawyers of Bisnar Chase are here to
          make the process of filing a personal injury claim easy and
          stress-free. Our attorneys will be with you every step of the way to
          make sure you are in the loop of knowing what's going on with your
          case.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Personal injury lawyers in Westminster"
            style={{
              backgroundImage:
                "url('/images/personal-injury/westminster-text-header-image - personal-injury.jpg')"
            }}
          >
            <h2>Legal Representation in Westminster</h2>
          </div>
        </LazyLoad>

        <p>
          Bisnar Chase has been helping victims of personal injuries in Orange
          County for over 40 years with magnificent results. Westminster lies
          right in the heart of Orange County, and over the years we've seen
          hundreds of people who suffered from severe injuries. We've been
          representing clients in Westminster since 1978, and we have not only
          the passion but the knowledge to help you get what you deserve.
        </p>
        <p>
          <strong>
            Contact the Westminster Personal Injury Lawyers of{" "}
            <Link to="/" target="_blank">
              {" "}
              Bisnar Chase
            </Link>{" "}
            at 949-203-3814. Upon your call you will receive a free consultation
            with an expert legal team member
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove St. #120
          <br /> Newport Beach, CA 92660
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
