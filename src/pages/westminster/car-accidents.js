// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/westminster-auto-accident-woman.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Westminster Car Accident Lawyer - Orange County CA"
        pageDescription="Call 949-203-3814 for a top rated Westminster car accident lawyers who can help you get fair compensation. The experienced  injury attorneys of Bisnar Chase have been legally representing victims of auto accidents for the past 40 years. Contact the law offices of Bisnar Chase and receive a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Westminster Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Westminster car accident lawyer"
            title="Westminster car accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Car accidents are totally unpredictable and can occur whether you
          drive a car, bike or motorcycle.
        </p>
        <p>
          The law firm of Bisnar Chase urges car crash victims to contact our
          experienced <strong> Westminster Car Accident Lawyers</strong> to
          learn more about collecting compensation for damages from their
          vehicle accident.
        </p>
        <p>
          <strong> Since 1978</strong>, we have have been serving clients who
          have endured catastrophic injuries due to careless drivers. We have
          won over <strong> $500 million dollars</strong> in compensation for
          our injury clients and are passionate about gaining justice for those
          in need.
        </p>
        <p>
          For a <strong> free consultation</strong> contact the personal injury
          lawyers of Bisnar Chase at <strong> 949-203-3814</strong>.
        </p>
        <p>You do not have to fight against big insurance companies alone.</p>
        <p>
          Call us now to consult with a top legal expert about your injury case.
        </p>
        <h2>Recovering From Your Westminster Auto Collision</h2>
        <p>
          Recoveries that you can earn from a car wreck will be based on the
          number of damages that took place during the auto collision. Not only
          can your car accident attorney fight for your physical injuries but
          they can also fight for any mental anguish or emotional pain and
          suffering.
        </p>
        <p>
          <strong>
            {" "}
            Other injuries or expenses you may be compensated for after a car
            accident are
          </strong>
          :
        </p>
        <ul>
          <li>
            <strong> Medical bills</strong>: Emergency room and doctor visits
            can be costly and before you know it bills can pile up. As a car
            accident victim, you are entitled to medical care, treatment and
            above all else compensation for those services. Documentation of
            visits with medical professionals will aid your car accident lawyer
            when fighting your case.
          </li>
          <li>
            <strong> Lost wages</strong>: Suffering from an injury can take a
            toll not only on you and your loved ones but also on your finances.
            For example, the average time to recover from a broken arm is about
            six weeks. This does not include rehab for an injury. Rehabilitation
            for injuries can take months. Pay stubs presented to the court can
            prove and give an estimated amount of time and money lost from your
            vehicle collision.
          </li>
          <li>
            <strong>
              {" "}
              <Link
                to="https://en.wikipedia.org/wiki/Pain_and_suffering"
                target="_blank"
              >
                {" "}
                Pain and suffering
              </Link>
            </strong>
            : To properly evaluate the pain and suffering damages, experts
            usually examine the personal and psychological aftermath a person is
            facing after a crash. An experienced Westminster auto accident
            attorney knows that a motor vehicle collision can devastate a
            person's quality of life. Those who are scarred or disfigured in an
            auto accident can often suffer severe emotional pain. This can cause
            embarrassment, humiliation, loss of companionship or friendship,
            loss of consortium (romantic affection), loss of enjoyment of life,
            and loss of guidance, tutelage and moral upbringing. Although it's
            not easy to put a price on these damages, keeping a record of them
            will aid a jury in understanding the need for compensation.
          </li>
        </ul>
        <h2>Pain and Suffering Isn't Always Easy to Define</h2>
        <p>
          A wise Westminster car accident lawyer knows that car accident victims
          can usually document the economic damages they sustained in a car
          collision. "The medical bills, car repairs, lost income and other
          'hard' expenses come in fast and furious after a car accident," notes
          attorney, John Bisnar. "But it's the pain and suffering damages that
          many car crash victims find difficult to document."
        </p>
        <p>
          This is understandable, since real guidelines for pain and suffering
          damages aren't exactly something you can look up in a book--like a
          Blue Book for car values. It's no wonder pain and suffering awards
          vary so widely. As a car accident victim, you must make a convincing
          argument, backed up by documents that reveal the level and extend of
          your pain and suffering.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Auto accident attorneys in Westminster"
            style={{
              backgroundImage:
                "url('/images/car-accidents/westminster-car-accident-header.jpg')"
            }}
          >
            <h2>Car Crashes in Westminster</h2>
          </div>
        </LazyLoad>
        <p>
          {" "}
          <Link
            to="http://www.city-data.com/city/Westminster-California.html"
            target="_blank"
          >
            {" "}
            Westminster, California
          </Link>{" "}
          is home to around 90,000 people as of 2015. With such a heavy
          population there is bound to be countless numbers of car accidents.
          Twelve fatal car accidents have occurred in the city of Westminster.
          In recent years it has exceeded California's total average of car
          accidents.
        </p>
        <p>
          Data has shown that the number one cause of car accidents is drunk
          driving. Drunk driving has killed over 10,000 people. Driving under
          the influence, in later years does not only involve alcohol though.
          According to the{" "}
          <Link
            to="https://www.cdc.gov/motorvehiclesafety/impaired_driving/impaired-drv_factsheet.html"
            target="_blank"
          >
            {" "}
            Center of Disease Control
          </Link>
          , 25% percent of DUI charges were due to marijuana usage.
        </p>
        <h2>DUI's are All Too Common in Westminster</h2>
        <p>
          Westminster is one of the most populated cities in Southern California
          and just like many metropolitans is filled with sports bars and
          nightclubs. Unfortunately, when the bars let out and the big game is
          over, many drivers hit the streets with their driving skills impaired
          by alcohol. And the result is often a tragic car accident.
        </p>
        <p>
          The{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="_blank"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System
          </Link>
          (SWITRS) reported that in one year, three fatalities and 50 injuries
          occurred due to alcohol-related auto collisions in Westminster. In
          Orange County, an estimated 13,000 DUI arrests take place every year.
        </p>
        <p>
          "The city of Westminster needs to be proactive in its efforts to
          control DUIs," noted car accident attorney John Bisnar. "Youthful
          drivers who get behind the wheel after spending hours in a sports bar,
          need to hand the keys to a designated driver to avoid a causing a
          serious car accident."
        </p>
        <p>
          Taking a tough stand against DUIs, Westminster frequently sets up DUI
          checkpoints along accident-prone roads throughout the city.
        </p>
        <p>
          <strong> Checkpoints can often be found along</strong>:
        </p>
        <ul>
          <li>Beach Blvd. at Westminster Blvd.</li>
          <li>Bolsa Ave. near the Westminster Mall</li>
          <li>Goldenwest St. between McFadden Ave.</li>
          <li>Hazard Ave.</li>
        </ul>
        <p>
          Westminster Police are required to post details of future DUI
          checkpoints. Typically, they provide several warnings--listed in The
          LA Times (Orange County Edition) or the Orange County Register.
        </p>
        <h2>Our Mission is to Help you Get Back on Your Feet</h2>
        <p>
          Trial attorney Brian Chase has dedicated his career to helping injury
          victims for over 25 years. With a specialization in auto defect case,
          Chase has been able to win millions in compensation for clients who
          had suffered tremendously from a personal injury incident.
        </p>
        <p>
          Chase shared "I specifically went to law school to become a personal
          injury trial attorney. It is what I love, what I do, and what I am."
        </p>
        <p>
          Over the years, his passion for aiding accident victims has also won
          him multiple trial lawyer awards. Chase's devotion to winning
          compensation for victims of auto accidents has been contagious at the
          law firm at Bisnar Chase.
        </p>
        <p>
          Brian Chase's team of top-notch trials attorneys and legal team
          members have many years of experience in cases such as car accidents,
          workers compensation and wrongful deaths. We have delivered successful{" "}
          <Link to="/case-results" target="_blank">
            {" "}
            case results
          </Link>{" "}
          and we strive to provide excellent customer satisfaction.
        </p>
        <p>
          At Bisnar Chase our mission statement is:{" "}
          <strong>
            "To provide superior client representation in a compassionate and
            professional manner while making our world a safer place."
          </strong>
        </p>
        <p>
          Check out how our{" "}
          <Link to="/about-us/testimonials" target="_blank">
            {" "}
            clients reviewed
          </Link>{" "}
          us:
        </p>
        <div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/lVUUCtM3Ebg"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>

          <center>
            <blockquote>
              "John Bisnar is like family, he really took care of us. I'm so
              happy that we found Bisnar Chase. They've been outstanding. They
              kept us informed with what's going on with the case."" <br />
              <strong> Maria Chan, former client</strong>
            </blockquote>
          </center>
        </div>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/oCeN5TviMcY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            "I was very happy that everyone I worked with from coming in and
            being greeted by the receptionist to my own actual personal lawyers.
            They were so amazing that I couldn't have wished or wanted for a
            better experience here at Bisnar Chase." <br />
            <strong> Alexis Bennet, former client</strong>
          </blockquote>
        </center>

        <h2>
          Legal Representation for Your Westminster Motor Vehicle Accident
        </h2>
        <p>
          If you are injured in a car accident, seek out an experienced{" "}
          <strong> Westminster Car Accident Lawyer</strong>. The law firm of
          Bisnar Chase has a <strong> 96% percent success rate</strong> and
          shows no signs of stopping. Our{" "}
          <Link to="/westminster" target="_blank">
            {" "}
            personal injury attorneys
          </Link>{" "}
          are genuinely interested in helping you.
        </p>
        <p>
          When you call today we will provide you with a no charge, no pressure
          consultation. Keep in mind, there are many things a car accident
          victim should know.
        </p>
        <p>
          Bisnar Chase's experienced auto accident attorney will keep you well
          informed about the rights you posses and the status of your case. Most
          importantly, we can help ensure that you are fairly compensated for
          your pain and suffering.
        </p>
        <p>
          Contact our Westminster car accident attorneys to discuss your case at{" "}
          <strong> 949-203-3814</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
