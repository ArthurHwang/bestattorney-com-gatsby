// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-motorcycle-accident.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Motorcycle Accident Attorneys | Bike Crash Lawyers"
        pageDescription="California motorcycle accident attorneys with 39 years experience handling motorcycle injuries. Call 949-203-3814 to talk to a motorcycle crash lawyer free."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Motorcycle Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california car accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            Our California Motorcycle Aaccident Lawyers are ready to answer your
            questions and speak with you immediately{" "}
            <strong> Free of Charge</strong>. Being hit on a{" "}
            <strong> motorcycle</strong> is a terrifying experience and we can
            help you get yourself and your bike on the road to recovery.
          </p>
          <p>
            At <strong> Bisnar Chase</strong>, we have over{" "}
            <strong> 39 years of experience</strong> helping seriously injured
            motorcyclists and other California personal injury clients get the{" "}
            <Link to="/case-results" target="new">
              maximum recovery for their damages
            </Link>
            . We know you have a lot of questions, we are here to answer them.
          </p>
          <p>
            <strong> For your Free Case Evaluation </strong>and{" "}
            <strong> Consultation Call 949-203-3814</strong>.
          </p>
          <p>
            The thrill of a ride on a motorcycle is often unparalleled. Those
            who ride with a passion know that there are few things as
            exhilarating as being out on California's scun-kissed roadways.
          </p>
          <p>
            However, motorcycling can become dangerous particularly because when
            a motorcycle collides with another vehicle, it is often the
            motorcyclist who suffers{" "}
            <Link to="/catastrophic-injury" target="new">
              catastrophic or fatal injuries
            </Link>
            . This is because motorcyclists have little protection and are
            absolutely vulnerable as they are exposed to the elements.
          </p>

          <p>
            When a motorcycle accident is caused by the negligence or wrongdoing
            of another, the injured victim can seek compensation for his or her
            injuries damages and losses.
          </p>
        </div>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9nqLzrBI8FE?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Motorcycle Accident Statistics</h2>
        <p>
          According to{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="new"
          >
            California Highway Patrol's Statewide Integrated Traffic Records
            System (SWITRS)
          </Link>
          :
        </p>

        <ul>
          <li>348 people were killed.</li>
          <li>
            9,625 were injured in California motorcycle accidents in the year
            2010.
          </li>
          <li>
            The CHP also reports that 40 fatally injured victims and 1,345
            injured motorcyclists were not wearing proper safety gear,
            specifically helmets, at the time of the crash.
          </li>
          <li>
            The number of motorcyclists killed in 2010 statewide decreased by
            9.1 percent from 2009 and the number of injured victims decreased by
            4.9 percent.
          </li>
        </ul>
        <p>
          The{" "}
          <Link to="https://www.chp.ca.gov/" target="new">
            California Highway Patrol
          </Link>{" "}
          estimates that:
        </p>
        <ul>
          <li>
            65.4 percent of all fatally injured motorcyclists were at fault for
            the crash.
          </li>
          <li>
            About 31.9 percent of all fatal motorcycle crashes were
            single-vehicle crashes where no other party was involved.
          </li>
        </ul>
        <h2>Common Causes of Motorcycle Accidents</h2>
        <ul>
          <li>
            <strong> Head-on collisions</strong>: Crashes involving motorcycles
            and other vehicles account for more than half of all fatal
            motorcycle accidents. Nearly 75 percent of the time, the vehicle
            strikes the motorcycle head-on. These types of crashes often prove
            fatal for the rider and/or passenger.
          </li>
          <li>
            <strong> Left-turn collisions</strong>: About 42 percent of all
            accidents involving a motorcycle and another vehicle involves a
            vehicle (other than the motorcycle) making a left turn. This type of
            collision occurs when the other vehicle is turning left at an
            intersection or into a driveway or parking lot.
          </li>
          <li>
            <strong> Speeding</strong>: Speed can be deadly for motorcyclists,
            regardless of who is speeding.
          </li>
          <li>
            Driving under the influence: Driving a motorcycle under the
            influence of alcohol or drugs can be extremely dangerous. The same
            applies to other drivers who are under the influence.
          </li>
          <li>
            <strong> Distracted driving</strong>: A distracted driver could, for
            example, drift into a bike lane or the shoulder where a bicyclist is
            riding.
          </li>
          <li>
            <strong> Roadway debris and hazards</strong>: Debris on the roadway
            such as mattresses or items dropped from other vehicles can prove
            fatal for motorcyclists. In addition, defective roadways with
            potholes or uneven pavement can also cause serious injury motorcycle
            crashes.
          </li>
        </ul>
        <h2>How Can Motorcyclists Prevent Accidents?</h2>
        <p>
          Here are a few safety measures that can help motorcyclists avoid
          serious injury accidents:
        </p>
        <ul>
          <li>
            <strong> Maintain your vehicle properly</strong>. Make sure you test
            your lights, turn signals and brakes before each ride. You should
            also make sure your bike's oil and fuel levels are good and that
            your mirrors are positioned correctly.
          </li>
          <li>
            <strong> Buy good quality gear</strong>. You need a helmet that fits
            well and does not affect your visibility. You should also wear
            bright clothing that increases your visibility.
          </li>
          <li>
            <strong> Practice safe riding habits</strong>. Stick to the speed
            limit and do not tailgate. Use your signals and only split lanes
            when it is safe to do so. Never enter an intersection until you are
            sure that nearby drivers see you.
          </li>
        </ul>
        <h2>Steps to Take after a Motorcycle Crash</h2>
        <p>
          If you are ever involved in a motorcycle crash, there are a number of
          steps that you can take to protect your rights. First and foremost,
          the law requires anyone involved in an accident to stop and remain at
          the scene.
        </p>
        <p>
          If you have been injured, do not move, and wait for emergency
          personnel to arrive. If, however, you can move around or have a friend
          at the scene to help you, here are a few useful steps that could be
          taken:
        </p>
        <ul>
          <li>
            <strong> Take photos</strong>: Photographic evidence of the crash
            site, the damage to your motorcycle and of your injuries can prove
            useful.
          </li>
          <li>
            <strong> Gather information</strong>: You will need the contact
            phone numbers and addresses of all parties involved as well as for
            anyone who witnessed the crash.
          </li>
          <li>
            <strong> Get specific</strong>: It is easy to forget important
            details if they are not written down. You are probably not thinking
            clearly in the aftermath of the crash. So, it will be useful to
            write down the specifics such as the date, time, location and
            position of the vehicles at the time of the incident.
          </li>
          <li>
            <strong> Filing a police report</strong>: It is important to get
            your account of the incident into the police report. Make sure you
            get a copy of the police report.
          </li>
          <li>
            <strong> Preserve your vehicle</strong>: It may be tempting to
            repair your motorcycle as soon as possible, but it is crucial to
            preserve your vehicle for a thorough examination by an expert.
          </li>
        </ul>
        <h2>Seeking Plaintiff Compensation</h2>
        <p>
          If you have been injured in a motorcycle accident caused by someone
          else's negligence or wrongdoing, you may be able to pursue financial
          compensation from the at-fault driver.
        </p>

        <p>
          Injured victims can seek compensation for damages including medical
          expenses, lost wages, cost of hospitalization, rehabilitation,
          therapy, permanent injuries, disabilities, pain and suffering and
          emotional distress.
        </p>

        <p>
          It is important to remember that the insurance company is not your
          ally. They have teams of well-trained adjusters who make sure that you
          get no money or as little compensation as possible.
        </p>
        <p>
          Do not rush into a settlement with the insurance company or any other
          party until you have spoken to an injury lawyer who will work to
          protect your best interests.
        </p>
        <h2>Passionate California Motorcycle Accident Lawyers</h2>
        <p>
          If you or a loved one has suffered from a motorcycle accident, contact
          the accomplished <Link to="/">personal injury lawyers </Link>{" "}
          specializing in motorcycle accidents for a free consultation. We will
          use our experience, knowledge and resources to achieve the best
          possible results for you and your family.
        </p>

        <p>
          <strong>
            {" "}
            Please contact us today at 949-203-3814 for a free consultation and
            comprehensive case evaluation.
          </strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
