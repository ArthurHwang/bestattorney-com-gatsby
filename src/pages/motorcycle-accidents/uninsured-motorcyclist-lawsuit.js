// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Uninsured Motorcyclist Awarded $2 Million In Truck Accident"
        pageDescription="Call 949-203-3814 for highest-rated motorcycle truck accident lawyers, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Uninsured Motorcyclist Awarded $2 Million In Lodi Truck Caused
          Accident
        </h1>
        <BreadCrumbs location={location} />

        <p>
          Most <strong> motorcycle accident attorneys</strong> will admit that a
          motorcycle collision can cause serious injuries to the rider. In 1996,
          M&R Company was harvesting a vineyard located near Lodi, California.
          An M&R employee instructed a hauler to park two semi-trailers close to
          the intersection of Turner Road and an access road leading into the
          vineyard. Christian P. Krough was eastbound on his motorcycle when a
          truck attempted a left-hand turn from the access road into the
          westbound lane of Turner. Krough didn't have enough time to stop and
          collided with the truck. The motorcycle crash caused Krough to suffer
          severe injuries, which rendered him a paraplegic.
        </p>
        <p>
          Krough brought suit against various individuals, including the truck
          driver and M&R. Krough testified that the truck simply pulled out from
          the parked trailers, leaving him no time to stop, and that the
          placement of the trailers visually obstructed the access road for
          those traveling east on Turner Road and blocked his view of the truck.
        </p>
        <p>
          "Mr. Krough suffered needlessly for a car crash that could easily have
          been prevented," observed John Bisnar, California{" "}
          <strong> motorcycle accident lawyer</strong>. "A negligent driver
          combined with a dangerous roadway obstruction created an "accident
          waiting to happen.""
        </p>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Reporting System (SWITRS), as recently as 2006, there were 19
          fatalities and 184 injuries involving motorcycles in San Joaquin
          County alone.
        </p>
        <p>
          By special verdict, the jury found that M&R was negligent in its
          placement of the trailers and that this negligence caused the truck
          accident. The jury also found no comparative fault on the part of
          Krough. The jury accordingly awarded Krough over $2 million in special
          damages. The net judgment, reduced by the amount of Krough's
          settlements with other defendants, was $1,453,000, plus costs. M&R did
          not appeal.
        </p>
        <p>
          "Mr. Krough suffered an irreparable loss in his ability to enjoy life,
          yet he chose to hold those responsible fully accountable for this
          needless motorcycle accident," noted
          <strong> California motorcycle accident attorney</strong> and
          motorcycle enthusiast, Brian Chase. "Our hope is that this lawsuit and
          similar lawsuits we have filed will motivate companies and individuals
          to consider the safety of others while performing their jobs."
        </p>
        <p>
          If you or a loved one has suffered serious injuries as the result of a
          defective auto part or vehicle, contact the experienced{" "}
          <Link to="/auto-defects">
            California auto products liability attorneys
          </Link>{" "}
          at Bisnar Chase Personal Injury Attorneys for a free consultation. We
          will use our extensive knowledge and resources to achieve the best
          possible results for you and your family.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
