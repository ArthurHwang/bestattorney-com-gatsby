// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Road Accident from Obstructed Roadway"
        pageDescription="Day filed a lawsuit against Fontana and San Bernardino County, his motorcycle accident lawyer alleging negligence in roadway maintenance, claiming hedges and weeds obstructed the roadway and partly caused the motorcycle accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Motorcyclist Accident Severely Injures Rider Due To Obstructed Roadway
        </h1>
        <BreadCrumbs location={location} />

        <p>
          As most <strong> motorcycle accident attorneys</strong> can tell you,
          obstructed roadways are a danger to motorist. In 1991, 19-year-old
          Russell Day, an uninsured motorcyclist, collided with a car in
          Fontana, CA. He suffered severe injuries.
        </p>
        <p>
          Day filed a lawsuit against Fontana and San Bernardino County, his{" "}
          <strong> motorcycle accident lawyer</strong> alleging negligence in
          roadway maintenance, claiming hedges and weeds obstructed the roadway
          and partly caused the motorcycle accident.
        </p>
        <p>
          "States, cities and the federal government have a responsibility to
          properly maintain all roads," noted John Bisnar,{" "}
          <strong> California motorcycle accident attorney</strong>. "The
          failure to maintain safe road conditions or properly warn riders of
          dangerous areas can lead to a motorcycle collision that could have
          been easily avoided."
        </p>
        <p>
          After Day waited years for a trial, a jury found Fontana and San
          Bernardino County responsible for half the motorcycle crash because of
          their negligence in roadway maintenance. The jury awarded the injured
          motorcyclist $455,000--enough to cover his lost wages and medical
          expenses, but not enough to cover future surgeries Day will need.
        </p>
        <p>
          The California Supreme Court had ruled that uninsured motorists
          injured in a car accident caused by a government agency's negligence
          cannot collect damages for pain and suffering. The high court said
          1996 voter-approved Proposition 213 precludes such awards against the
          government, although the measure does allow uninsured motorists to
          collect damages to recover medical expenses, lost wages and other
          out-of-pocket costs.
        </p>
        <p>
          "It's truly regrettable that Russell Day had to wait years to confront
          Fontana and San Bernardino County for their highway negligence," noted
          John Bisnar. "In this case, justice delayed was justice denied.
          Fortunately, Russell got his day in court and was compensated for his
          expenses."
        </p>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Reporting System (SWITRS), as recently as 2006, there were 39
          fatalities and 507 injuries involving motorcycles in San Bernardino
          County alone. And according to the U. S. Department of Transportation,
          in 2006, 96 motorcycle deaths were caused by roadway defects.
        </p>
        <p>
          "Russell Day's patience and fortitude are to be commended," noted{" "}
          <strong> California personal injury attorney</strong> and motorcycle
          enthusiast, Brian Chase. "After suffering with severe injuries and
          dealing with ever-mounting medical expenses, he chose to fight "city
          hall" and hold Fontana and San Bernardino County accountable for their
          dangerous roadway. Our hope is that this lawsuit, and the similar
          lawsuits we have filed against cities and counties, will motivate them
          to ensure our roads and highways remain safe for all motorcyclists."
        </p>
        <p>
          If you or a loved one has suffered serious injuries as the result of a
          defective auto part or vehicle, contact the experienced{" "}
          <Link to="/auto-defects">
            California auto products liability attorneys
          </Link>{" "}
          at BISNAR CHASE for a free consultation. We will use our extensive
          knowledge and resources to achieve the best possible results for you
          and your family.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
