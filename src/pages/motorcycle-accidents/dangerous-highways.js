// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Highway Accident Victim Sues Caltrans"
        pageDescription="Officer Jungsten, a 33-year-old sheriff's deputy, was driving his motorcycle when he rear-ended a tractor-trailer that had stopped in response to the car crash"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Motorcyclist Sues Caltrans for Dangerous Highway</h1>
        <BreadCrumbs location={location} />

        <p>
          Most <strong> product liability lawyers</strong> will agree that
          dangerous roadways pose a hazard to a variety of motorist everyday. In
          1998, a Sacramento motorist, confused as to where she should go as she
          approached a construction area, lost control of her vehicle. She
          struck a barrier, careened to the right and struck another vehicle.
          Motorists behind the car collision started braking. Officer Jungsten,
          a 33-year-old sheriff's deputy, was driving his motorcycle when he
          rear-ended a tractor-trailer that had stopped in response to the car
          crash. Tragically, he suffered fatal injuries and is survived by his
          wife and two minor children.
        </p>
        <p>
          Caltrans had hired a contractor to work on a highway construction
          project in the area. Their agreement specified that the left shoulder
          be blocked off and traffic shifted to the right using striping, tall
          orange tubular delineators, and eight temporary concrete barriers
          covering at least 150 feet.
        </p>
        <p>
          Jungsten's wife sued Caltrans and the Construction Company. Her{" "}
          <strong> product liability attorneys</strong> alleged that the highway
          in question constituted a dangerous condition. Specifically, that the
          construction company had failed to properly install barriers, which in
          fact consisted of only three barriers covering 60 feet; that the
          construction company had failed to place striping and delineators to
          guide motorists to the right; and that Caltrans had failed to properly
          inspect the area.
        </p>
        <p>
          Caltrans countered, contending that the barriers were indeed properly
          installed and that Jungsten was comparatively negligent for failing to
          stop and avoid the car accident. The jury sided with Jungsten, finding
          Caltrans and the construction company each 50 percent responsible. The
          jury awarded Jungsten's wife and children approximately $6.5 million.
        </p>
        <p>
          "Caltrans and the construction company they employed failed to work
          together to create a safe construction zone," observed John Bisnar,{" "}
          <strong> motor vehicle defect lawyer</strong>. "Timely
          construction-area inspections and follow-up by Caltrans could have
          averted this tragic motorcycle accident."
        </p>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Reporting System (SWITRS), as recently as 2006, there were 204
          motorcycle fatalities and 16,282 injuries caused by improper traffic
          signals and signs. Moreover, there were 12 fatalities and 559 injuries
          involving motorcycles in Sacramento County alone.
        </p>
        <p>
          "Mrs. Jungsten displayed tremendous courage amidst unimaginable grief
          in going after Caltrans and the construction company to hold them
          accountable for her husband's tragic death," noted
          <strong> California motorcycle accident attorney</strong> and
          motorcycle enthusiast, Brian Chase. "Our hope is that this lawsuit,
          and the similar lawsuits we have filed against CalTrans, will motivate
          them and their contractors to properly safeguard the motoring public
          while conducting their work of maintaining and improving our highways.
          Taking these prudent steps will reduce motorcycle accidents and
          fatalities."
        </p>
        <p>
          If you or a loved one has suffered serious injuries as the result of a
          defective auto part or vehicle, contact the experienced{" "}
          <Link to="/auto-defects">
            California auto products liability attorneys
          </Link>{" "}
          at Bisnar Chase Personal Injury Attorneys for a free consultation. We
          will use our extensive knowledge and resources to achieve the best
          possible results for you and your family.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
