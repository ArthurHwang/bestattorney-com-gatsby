// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Walnut Creek Motorcyclist Injured Due to Intersection Collision"
        pageDescription="Call 949-203-3814 for highest-rated motorcycle injury lawyers, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Van Runs Red Light, Injures Motorcyclist</h1>
        <BreadCrumbs location={location} />

        <p>
          Most <strong> motorcycle accident lawyers</strong> will admit that a
          collision with a van can result in serious personal injuries. In 1984,
          24-year-old Walnut Creek motorcyclist, Michael Barr collided with a
          flower shop delivery van at the intersection of Ygnacio Valley Road
          and La Casa Via Road. The van had run a red light and the subsequent
          collision caused his foot to be severed. Several of Barr's bones were
          broken and his foot was "literally ripped from his leg." His foot was
          later reattached by surgeons at John Muir Memorial Hospital. Although
          Barr's surgeons were able to save his foot through seven operations,
          the former insulation worker was unable to return to work.
        </p>
        <p>
          Barr filed a lawsuit against the van's driver who was cited by police
          for not stopping at a red light. An out-of-court settlement was
          reached that provided Barr with almost $1.2 million. Under the terms
          of the settlement, the insurance company for Hogan & Evers Flower --
          the owners of the van -- agreed to pay Barr $225,000 immediately, plus
          $2,000 a month over Barr's lifetime. Barr will also receive larger
          lump sums periodically. Part of the money will go to pay for Barr's
          medical bills, which amounted to $56,000.
        </p>
        <p>
          John Bisnar, California <strong> motorcycle accident attorney</strong>
          , made this observation, "The National Highway Traffic Safety
          Administration funded a USC study by Harry Hurt who investigated
          almost every aspect of 900 motorcycle accidents. Hurt found that
          intersections are the most likely place for a motorcycle accident to
          occur, with the other vehicle violating the motorcycle's right-of-way,
          and often violating traffic controls."
        </p>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Reporting System (SWITRS), as recently as 2006, there were 82
          motorcycle fatalities and 2,381 injuries among motorcyclists in the
          15-to-24 age group. Moreover, there were 10 fatalities and 220
          injuries involving motorcycles in Contra Costa County alone.
        </p>
        <p>
          "We commend Mr. Barr for pursuing those responsible for his accident,"
          said <strong> California motorcycle accident attorney</strong> and
          motorcycle enthusiast, Brian Chase. "His injuries changed the course
          of his life and we believe he did the right thing. Our hope is that
          this lawsuit, and the similar lawsuits we have filed against companies
          and government entities, will motivate them to properly train their
          employees to drive safely and observe all traffic laws. Taking these
          prudent steps will reduce motorcycle accidents and fatalities."
        </p>
        <p>
          If you or a loved one has suffered serious injuries as the result of a
          defective auto part or vehicle, contact the experienced{" "}
          <Link to="/auto-defects">
            California auto products liability attorneys
          </Link>{" "}
          at Bisnar Chase Personal Injury Attorneys for a free consultation. We
          will use our extensive knowledge and resources to achieve the best
          possible results for you and your family.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
