// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Accident Help California Injury Victims"
        pageDescription="Motorcycle accident help for california injury victims. Lists to cities offering motorcycle accident consultations free."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Motorcycle Accident Help List for California Injury Victims</h1>
        <BreadCrumbs location={location} />

        <p>
          To get help with your motorcycle accident please call us at
          949-203-3814 or choose from our list of cities below. If you do not
          find your city please call or email for specific information regarding
          the laws and regulations for your accident.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/motorcycle-accidents">
              California Motorcycle Accident Lawyers{" "}
            </Link>
          </li>
          <ul>
            <li>
              {" "}
              <Link to="/orange-county/motorcycle-accidents">
                Orange County Motorcycle Accident Lawyers{" "}
              </Link>
            </li>
            <ul>
              <li>
                {" "}
                <Link to="/anaheim/motorcycle-accidents">
                  Anaheim Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/buena-park/motorcycle-accidents">
                  Buena Park Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/costa-mesa/motorcycle-accidents">
                  Costa Mesa Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/cypress/motorcycle-accidents">
                  Cypress Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/fullerton/motorcycle-accidents">
                  Fullerton Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/fountain-valley/motorcycle-accidents">
                  Fountain Valley Motorcycle Collision Attorneys{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/garden-grove/motorcycle-accidents">
                  Garden Grove Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/huntington-beach/motorcycle-accidents">
                  Huntington Beach Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/irvine/motorcycle-accidents">
                  Irvine Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/laguna-niguel/motorcycle-accidents">
                  Laguna Niguel Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/la-habra/motorcycle-accidents">
                  La Habra Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/lake-forest/motorcycle-accidents">
                  Lake Forest Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/mission-viejo/motorcycle-accidents">
                  Mission Viejo Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/santa-ana/motorcycle-accidents">
                  Santa Ana Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/newport-beach/motorcycle-accidents">
                  Newport Beach Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              {/* <!-- <li>{" "}<Link to="/placentia/motorcycle-accidents">Placentia Motorcycle Accident Lawyers </Link></li> --> */}
              <li>
                {" "}
                <Link to="/westminster/motorcycle-accidents">
                  Westminster Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/yorba-linda/motorcycle-accidents">
                  Yorba Linda Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
            </ul>
          </ul>
          <ul>
            <li>
              {" "}
              <Link to="/los-angeles/motorcycle-accidents">
                Los Angeles Motorcycle Accident Lawyers{" "}
              </Link>
            </li>
            <ul>
              {/* <!-- <li>{" "}<Link to="/baldwin-park/motorcycle-accidents">Baldwin Park Motorcycle Accident Attorneys </Link></li> -->
          <!-- <li>{" "}<Link to="/burbank/motorcycle-accidents">Burbank Motorcycle Accident Lawyers </Link></li> --> */}
              <li>
                {" "}
                <Link to="/long-beach/motorcycle-accidents">
                  Long Beach Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
              {/* <!-- <li>{" "}<Link to="/carson/motorcycle-accidents">Carson Motorcycle Accident Lawyers </Link></li> --> */}
              {/* <li>Cerritos Motorcycle Accident Lawyers</li> */}
              {/* <!-- <li>{" "}<Link to="/culver-city/motorcycle-accidents">Culver City Motorcycle Accident Lawyers </Link></li> --> */}
              <li>
                {" "}
                <Link to="/locations/hollywood-motorcycle-accidents">
                  Hollywood Motorcycle Accident Attorneys{" "}
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/locations/lakewood-motorcycle-accidents">
                  Lakewood Motorcycle Accident Lawyers{" "}
                </Link>
              </li>
            </ul>
          </ul>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
