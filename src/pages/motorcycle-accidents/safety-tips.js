// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Safety Tips - Infographic"
        pageDescription="Stay safe and avoid injuries when riding your motorcycle.  Learn more by visiting our motorcycle safety tips page."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Motorcycle Safety Tips</h1>
        <BreadCrumbs location={location} />

        <p>
          Motorcycle safety is no joking matter. Please take your safety
          seriously. If you do become injured in a motorcycle accident we would
          like to help. Contact us immediately to schedule your free
          consultation with our reputable California{" "}
          <Link to="/motorcycle-accidents">motorcycle accident lawyers</Link>.
          Call <strong> 949-203-3814</strong> or contact our motorcycle accident
          attorneys at Bisnar Chase Personal Injury Attorneys.
        </p>

        <img
          src="/images/motorcycle-safety-tips-infographic.jpg"
          alt="Motorcycle Safety Tips (Infographic)"
          width="100%"
          className="imgcenter-fluid"
        />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
