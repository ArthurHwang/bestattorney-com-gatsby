// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Accident Victim Wins $31.5 Million Against State of California"
        pageDescription="Victim awarded $31.5 million verdict following serious injury collision. Call 949-203-3814 to obtain a free consultation from experienced motorcycle accident lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Motorcycle Accident Victim Received $31.5 Million in Lawsuit against
          State of California
        </h1>
        <BreadCrumbs location={location} />

        <p>
          Following one of several serious injury motorcycle accidents occurring
          on California State Route 138, a jury recently awarded one victim
          $31.5 million in a verdict against California's state agency
          responsible for rail, bridge, and highway transportation construction,
          planning, and maintenance (Caltrans).
        </p>
        <p>
          Caltrans was held responsible for what was determined to be a
          dangerous roadway condition.
        </p>
        <p>
          On April 29, 2009, David Evans was riding his motorcycle when he
          collided with an oncoming vehicle. The collision left Mr. Evans with
          severe spinal cord and brain injuries that will require
          around-the-clock nursing assistance for the remainder of his life.
        </p>
        <p>
          California Highway Patrol (CHP) documents showed that ten previous
          accidents had occurred at this location. The handling attorneys were
          able to convince the jury that Caltrans failed to provide sufficient
          protection for their client in an area that had been flagged for being
          especially dangerous.
        </p>
        <p>
          According to the 2009 California Statewide Integrated Traffic Records
          System (SWITRS), 232,777 people were injured and 3,076 suffered fatal
          injuries in California auto collisions. Of those injured, 13,083 were
          pedestrians, 12,043 were bicyclists, and 10,479 were traveling on
          motorcycles.
        </p>
        <p>
          Brian Chase,{" "}
          <Link to="/motorcycle-accidents">California motorcycle attorney</Link>{" "}
          and lifelong motorcycle enthusiast, has assisted dozens of motorcycle
          accident and dangerous roadway condition victims. "Too often,
          California's transportation agencies are made aware of dangerous
          roadway conditions and fail to quickly address the issue. In Mr.
          Evans' case, Caltrans' Chief of Operations had received a complaint
          eighteen months prior to his Evans' collision. Had this complaint, or
          the several motor vehicle accidents which took place in the area, been
          treated with the respect that they deserved, the roadway could have
          been modified to help prevent Mr. Evans' collision.
        </p>
        <p>
          "When Caltrans, or any other governmental agency, contributes to a
          dangerous condition, they put themselves in a position to be held
          liable. Luckily, Mr. Evans received a generous verdict which will
          allow him to receive the care he so desperately needs." Said Mr.
          Chase.
        </p>
        <p>About Bisnar Chase Personal Injury Lawyers Orange County</p>
        <p>
          Bisnar Chase attorneys represent victims of motorcycle accidents, car
          collisions, defective products, dangerous roadways, and many other
          personal injuries. Their firm has been featured on a number of popular
          media outlets including Fox, NBC, and ABC and has developed a
          reputation for providing proven results from trusted professionals.
        </p>
        <p>
          Since 1978, Bisnar Chase have recovered millions of dollars for
          victims of motorcycle accidents and victims of dangerously designed
          and/or maintained roadways.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
