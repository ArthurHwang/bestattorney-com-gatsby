// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { Helmet } from "react-helmet"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "brain-injury/anaheim-brain-injury-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Brain Injury Lawyer - Bisnar Chase"
        pageDescription="If you or a family member have experienced a traumatic brain injury contact the Anaheim Brain Injury Lawyers of Bisnar Chase. Our attorneys have been helping clients gain compensation for over 39 years. If there is no win there is no fee. Contact 949-203-3814 to discuss your brain injury case free of charge. "
      />
      <Helmet>
        <meta name="DC.title" content="bicycle accident attorney" />
        <meta name="geo.region" content="US-CA" />
        <meta name="geo.placename" content="Anaheim" />
        <meta name="geo.position" content="33.835293;-117.914504" />
        <meta name="ICBM" content="33.835293, -117.914504" />
      </Helmet>
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="anaheim brain injury lawyers"
            title="anaheim brain injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong>Anaheim brain injury lawyers</strong> at Bisnar Chase can
          help you fight for your rights. The attorneys of Bisnar Chase want to
          make sure that you receive fair compensation for medical expenses,
          lost earnings, hospitalization, surgery, rehabilitation, loss of
          livelihood and pain.
        </p>
        <p>
          If your brain injury was caused by someone else's negligence or
          wrongdoing, you should not have to suffer financially. Please{" "}
          <strong>
            call us at 949-203-3814 to obtain more information about pursuing
            your legal rights
          </strong>
          .
        </p>
        <h2>What Constitutes as a Brain Injury?</h2>
        <p>
          A{" "}
          <Link
            to="https://www.traumaticbraininjury.com/symptoms-of-tbi/severe-tbi-symptoms/"
            target="_blank"
          >
            traumatic brain injury
          </Link>{" "}
          is described to be a change in brain behavior due to an outside force.
          There are different categories of brain injuries that can range from
          mild to serious.
        </p>
        <p>
          Some of the symptoms of a brain injury can include complications with
          :
        </p>
        <ul>
          <li>Focusing</li>
          <li>Recollection</li>
          <li>Disorientation</li>
          <li>Sensory such as loss of touch, taste or smell</li>
          <li>Sudden anger spells</li>
        </ul>
        <p>
          The Anaheim personal injury attorneys of Bisnar Chase provide the best
          legal representation for a variety of personal injury cases such as
          car accidents, bicycle accidents and dog bites. Our{" "}
          <Link to="/anaheim" target="_blank">
            personal injury lawyers
          </Link>{" "}
          will serve as advocates for any personal injury claim.
        </p>

        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/brain-injury/anaheim-brain-injury-lawers-brain-scan.jpg"
                width="100%"
                // height="400"
                alt="Brain injury lawyers in Orange county"
              />
            </LazyLoad>
          </center>
        </p>

        <h2>The Different Types of Brain Injuries</h2>
        <p>
          Each class of brain injuries serves to be unique based on the amount
          or type of force that the brain has received after the accident.
        </p>
        <p>
          The following conveys the{" "}
          <Link
            to="https://www.biausa.org/about-brain-injury.htm"
            target="_blank"
          >
            type of brain injury
          </Link>{" "}
          you may have :
        </p>
        <h3>Concussion</h3>
        <p>
          A concussion is the typical injury doctors have observed in a majority
          of brain injury patients. This form of a brain injury is created when
          the brain undergoes trauma from a force or motion. What happens during
          the concussion is the blood vessels in the cranium expand and may even
          be damaged.
        </p>
        <p>
          Concussions are not obvious brain injuries that can be detected by a
          CAT Scan and could take years to recognize or heal.
        </p>
        <h3>Contusion</h3>
        <p>
          Contusions are applied solely to the head on impact and leads to the
          brain bleeding. This is also known to be a bruise on the brain. If the
          contusion grows, surgery would be the next course of action.
        </p>
        <h3>Open Head Injury</h3>
        <p>
          An open head injury is classified as an exposed head fracture. Just
          like brain injuries, there can also be different kinds of open head
          injuries. A few type of open head injuries can include depressed,
          compound or basilar skull fractures.
        </p>
        <h2>The Financial and Emotional Aftermath of a Brain Injury</h2>
        <p>
          Over 1.7 million people suffer from a traumatic brain injury each year
          in the United States. Brain injuries do not only effect the victim but
          the entire family. Caring for a loved one with a brain injury can be
          emotionally, physically and financially challenging.
        </p>
        <p>
          The cost of medical treatment and rehabilitation in traumatic brain
          injury cases can be extraordinary. Victims whose injuries were caused
          by carelessness should take action by hiring a personal injury
          attorney who will earn the compensation they are entitled to.
        </p>
        <h2>Rehabilitation and the Recovery Process</h2>
        <p>
          Experts strongly suggest that the victim of a brain injury should
          begin rehabilitation immediately. The recovery process also depends on
          how severe the brain injury is. Experts say that the shorter the coma,
          the better the outcome.
        </p>
        <p>
          Rehabilitation and therapy can help victims cope with the changes that
          come with their injury.
        </p>
        <p>
          The main objective of rehabilitation is to better the physical and
          psychological performance of each patient. Treatment various from
          patient-to-patient and in order to progress, specialized care is a
          must. Accumulated cost from the first emergency evaluation to the
          out-patient treatment can become expensive.
        </p>
        <p>
          The Anaheim brain injury attorneys of Bisnar Chase understand that
          recovery expenses are pricey and want you or your family member to
          receive the best care for a brain injury.
        </p>
        <h3>Rehabilitation Treatment Process</h3>
        <h5>
          Info-graphic courtesy of the Brain Injury Association of America
        </h5>
        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/brain-injury/anaheim-brain-injury-attorneys-info-graphic.png"
                width="100%"
                height=""
                alt="Orange county brain injury lawyers"
              />
            </LazyLoad>
          </center>
        </p>

        <h2>A Surprising Leading Cause of Brain Injuries</h2>
        <p>
          The Centers for Disease Control reported that the number one leading
          cause for a traumatic brain injury is falling. Over 47 percent of
          victims belong to this category and have left individuals to be
          hospitalized or worse, dead.
        </p>
        <p>
          Being struck by a object follows as the second leading cause of brain
          injuries.
        </p>
        <p>
          Experts strongly suggest that if you have suffered an impact to the
          head, whether it was after a car accident, fall or assault, it is
          important that you submit yourself to a medical examination right
          away.
        </p>
        <p>
          Receiving prompt medical attention also helps clients create a record
          that can serve as concrete evidence for your personal injury lawyer.
          Immediately contact a<strong>Anaheim personal injury attorney</strong>{" "}
          at Bisnar Chase to ensure you that you will compensated for the
          suffering you and your family have endured.
        </p>
        <h2>Why Football Can Be a Lead Cause for Brain Injuries</h2>
        <p>
          The beloved American sport is not only the number one televised event
          to watch, but it is also a popular sport that athletes need to watch
          for in regards to brain function.
        </p>
        <p>
          In an expert analysis, scientist evaluated 202 brains of former
          football players and found that 88 percent of the brains had CTE.
          Chronic Traumatic Encephalopathy is a brain disease that contains a
          protein called Tau that eventually kills brain cells.
        </p>
        <p>
          Lead examiner of the study Dr. Ann McKee says that this research
          proves that football players are most likely to suffer from CTE.
        </p>
        <p>
          "this is by far the largest [study] of individuals who developed CTE
          that has ever been described. And it <em>only </em>includes
          individuals who are exposed to head trauma by participation in
          football " McKee tells{" "}
          <Link
            to="https://www.npr.org/2017/07/25/539198429/study-cte-found-in-nearly-all-donated-nfl-player-brains"
            target="_blank"
          >
            NPR
          </Link>
          .
        </p>
        <p>
          CTE effects appear decades after the constant brain injury and consist
          of:
        </p>
        <ul>
          <li>Impulse control problems</li>
          <li>Suicidal thoughts</li>
          <li>Depression</li>
          <li>Aggression</li>
          <li>Dementia</li>
        </ul>
        <p>
          Unfortunately if you wanted to hire an attorney to hold the NFL liable
          for the injuries, there may lie a challenging road ahead. The only
          time that a player can file a suit is when the equipment is defected.
        </p>
        <p>
          The reason is that football is widely known to be an aggressive sport
          and by athletes participating in the sport this is the player
          consenting to the injuries that may occur. Liability can be fought in
          special cases such high school football injuries.
        </p>
        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/brain-injury/anaheim-bicycle-accident -lawyer-liable.jpg"
                width="100%"
                height=""
                alt="Personal injury attorney in Anaheim"
              />
            </LazyLoad>
          </center>
        </p>
        <h2>Who Should Be Held Accountable for a Brain Injury?</h2>
        <p>
          Liability usually depends on the circumstances of how the brain injury
          occurred and who was involved.
        </p>
        <p>
          For example, if the brain injury occurred as the result of a car
          accident caused by a drunk driver, that driver can be held liable. If
          a defective product caused the injury, then the manufacturer of that
          faulty product can be held liable. If negligence of nursing home staff
          caused a patient's fall, then, the nursing home can be held liable.
        </p>
        <h2>Bisnar Chase Aids Victims of Brain Injuries</h2>
        <p>
          Seventeen-year-old Christopher Chan was one week away from graduating
          high school when he suffered an irreparable brain injury accident. For
          weeks after the incident, Chan was in a comatose state and being fed
          through a tube.
        </p>
        <p>Chan's mother and father knew something had to be done.</p>
        <p>
          Bisnar Chase provided relief and support for Christopher and his
          family. Like Christopher, Bisnar Chase guarantees that you will
          receive the backing you need to survive and thrive after a life
          changing experience.
        </p>
        <center>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/lVUUCtM3Ebg"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
        </center>

        <h2>Brain Injuries and Health Insurance</h2>
        <p>Often, rehabilitation costs are not covered by health insurance.</p>
        <p>
          Victims face tremendous financial pressures, as do family members who
          have the responsibility of caring for them. Many victims and their
          families are expected to then pay out of pocket for medications,
          medical devices and rehabilitation services.
        </p>
        <h2>Reach an Experienced Brain Injury Attorney</h2>
        <p>
          Speak with our Anaheim brain injury lawyers today to see if you have a
          case. There is <strong>no cost to have us review your case</strong>{" "}
          and the information may be priceless.
        </p>
        <p>
          The Anaheim brain injury lawyers of Bisnar and Chase have earned
          clients over <strong>$500 million dollars</strong> and also obtain a{" "}
          <strong>96 percent</strong> success rate. We have over three decades
          of experience dealing with traumatic and catastrophic injuries.
        </p>
        <p>
          <strong>
            Call 949-203-3814 or <Link to="/contact">contact us</Link>.
          </strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys
            <br />
            1301 Dove St. #120
            <br />
            Newport Beach, CA 92660
          </span>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
