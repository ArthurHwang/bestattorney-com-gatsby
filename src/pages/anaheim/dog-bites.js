// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Dog Bite Attorneys - Bisnar Chase"
        pageDescription="Call 949-203-3814 for Anaheim dog bite lawyers with over 39 years experience. No win, no fee & hundreds of millions won. Our law firm will fight for you and get you the compensation you deserve. Time is limited so contact an attorney today to discuss your rights."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <img
            src="/images/dog-bites/anaheim-dog-bite-banner-image.jpg"
            alt="Anaheim Dog Bite Lawyers"
          />
        </div>

        <p>
          According{" "}
          <Link
            to="https://www.avma.org/public/Pages/Dog-Bite-Prevention.aspx"
            target="_blank"
          >
            AVMA,
          </Link>{" "}
          over 4.5 million people suffer serious injuries from dog bites every
          year in the United States. If you've been hurt from a dog bite please
          give us a call for a<strong> free consultation </strong>with our top
          rated <strong>Anaheim dog bite lawyers</strong>.
        </p>
        <p>
          Bisnar Chase has represented thousands of clients throughout
          California and we are here to provide you relief. Our legal team has
          the expertise and resources to take on very complex cases.
        </p>
        <p>
          When many attorneys turn down the difficult cases, we step in and take
          them on.
          <strong>
            {" "}
            Call 949-203-3814 for a no obligation free consultation.
          </strong>
        </p>
        <h2>What to Do If You Have Been Bitten by a Dog</h2>
        <p>
          It's best to contact a qualified{" "}
          <Link to="/anaheim">Anaheim personal injury lawyer</Link> as soon as
          possible after your dog bite injury. Taking photos and recording
          details after the attack will further help your attorney. This
          information will be used to file an injury lawsuit against the dog
          owner. Be sure to also gather all the evidence you have including
          witness statements.
        </p>
        <p>
          It is possible to hurt the case so be sure not to indulge in
          conversations with the insurance companies or other side without first
          consulting your attorney. Do not provide any personal or financial
          information to anyone other than your legal team and do not sign
          anything.
        </p>
        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/dog-bites/anaheim-dog-bite-lawyers-wound.jpg"
                width="100%"
                alt="Law firm in Anaheim for dog bite victims"
              />
            </LazyLoad>
          </center>
        </p>
        <h2>Treatment after Being Bitten by a Dog</h2>
        <p>
          The first step to take after surviving a dog bite is to make sure that
          you have moved to a safe location away from the dog.
        </p>
        <p>
          It is best to seek medical attention immediately especially if it
          appears that the dog has punctured or has torn the skin. It is also
          strongly advised that you speak with the dog owner to learn more about
          the dog's rabies immunization status.
        </p>
        <p>
          Even if the injury after the dog bite does not look severe it is still
          recommended that you go see a doctor. The reason being is that
          although there may not seem to be any injury on the surface of the
          skin there could be a possibility that the tissue under the skin could
          be damaged.
        </p>
        <p>
          The medical expenses after the dog bite can be expensive. At Bisnar
          Chase, we believe that you shouldn't have to pay out of pocket because
          of someone else's negligence. Contact us and one of our personal
          injury attorneys will fight for the compensation you rightfully
          deserve.
        </p>
        <h2>Tragic Dog Bite Incident in Anaheim</h2>
        <p>
          Two-year-old Grayson Bishop and his mother Samantha were brutally
          attacked by dogs when leaving their Orange County home in 2016.
        </p>
        <p>
          {" "}
          <Link
            to="https://abc7.com/news/mom-2-year-old-son-attacked-by-dogs-in-anaheim/1602555/"
            target="_blank"
          >
            ABC 7
          </Link>{" "}
          reported that two dogs escaped a neighbors residence and surrounded
          the mother and son. The dogs attacked and quickly started to chew on
          the mothers arm.
        </p>
        <p>
          After six minutes Samantha and Grayson Bishop were finally able to
          enter their home when a man passing through the neighborhood came to
          their aid.
        </p>
        <p>
          The toddler suffered a major laceration on his face and leg. Grayson's
          father said that the incident left him shocked and he hopes for it to
          never happen again.
        </p>
        <p>
          "Very scary to know that, you know, we can't walk out the door to get
          coffee wondering if there's going to be an animal to come attack,"
          said Grayson's father, Spenser Grayson.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/yO5MBfgl0OI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Answers to Common Dog Bite Questions</h2>
        <p>
          You might not think it happens too often, but dog bites and attacks by
          dogs are a fairly common occurrence. Man's best friend can turn on you
          in the blink of an eye, and when they do, it's somebody's moral and
          legal responsibility to pay for your pain and suffering.
        </p>
        <p>
          Just because dogs are lovable creatures, it doesn't mean they can't
          become aggressive. Oddly enough, dog bites and attacks have gone up
          nationally while dog ownership has stayed about the same. There's a
          few possible reasons for this:
        </p>
        <ul>
          <li>Irresponsible dog owners</li>
          <li>
            Drastic changes in temperature (during summer months) creating
            irritability
          </li>
          <li>More abandoned aggressive dogs in shelters</li>
          <li>More mixing of large and dangerous breeds</li>

          <li>
            {" "}
            <Link to="/blog/dog-attacks-on-rise-video">
              Are dog attacks on the rise?
            </Link>
          </li>
        </ul>
        <p>
          We can't really know for sure, but one thing we do know is that
          unprovoked dog bites will not be tolerated whatsoever. Especially in
          California where the law is "one bite is enough". A dog doesn't have
          to show a propensity towards being aggressive or even have a past for
          biting. California has a{" "}
          <strong>strict liability rule when it comes to dog bites</strong> and
          anyone that owns the dog will be liable.
        </p>
        <h2>Dog Bite Case Proof</h2>
        <p>
          There are a few things you need to prove before you have a solid dog
          bite case:
        </p>
        <ol>
          <li>The defendant owned the dog</li>
          <li>The dog bit the plaintiff</li>
          <li>
            The plaintiff was lawfully on the premises where the dog bite took
            place
          </li>
          <li>The dog bite caused the plaintiff injuries</li>
        </ol>
        <center>
          <LazyLoad>
            <img
              src="/images/dog-bites/dog-bite-attorneys.jpg"
              width="100%"
              alt="Dog bite lawyers in Anaheim"
            />
          </LazyLoad>
        </center>
        <h2>Do You need An Attorney?</h2>
        <p>
          That depends on how complex your case is and the extent of your
          injuries. If you have a fairly simple case you may be able to
          negotiate a settlement with the dog owner to pay your medical costs
          and time off work.
        </p>
        <p>
          If it's more complex than that, involving surgery, follow up care,
          mental or emotional trauma and pain and suffering then a dog bite
          attorney with a lot of experience would be ideal.
        </p>
        <h2>California Dog Bite Statute</h2>
        <p>
          Even if you have a sign posted on your premises that declares{" "}
          <strong>"Beware of dog"</strong> and a person is bitten while on your
          property, you will still be at fault. Short of someone attacking you
          or being on your property without consent, a dog bite is the fault of
          the owner and can result in heavy fees if a case is brought.
        </p>
        <p>
          If you own a home, your homeowners insurance will usually cover the
          general liability of a dog bite unless the breed has been excluded. If
          that's the case, you could be personally sued by a dog bite victim.
        </p>
        <h2>Act Quickly to Protect Your Rights</h2>
        <p>
          If you need more information give us a call and one of our skilled
          Anaheim dog bite attorneys will be happy to assist you. We can go over
          the details of your case including medical treatment, loss of earnings
          and continued therapy. Time is of the essence to preserve your rights.
          Be sure to provide your attorney with all of the evidence you have
          collected from the attack.
          <strong>Call 949-203-3814 for a free consultation.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
