// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle=" Best Rated Anaheim Car Accident Lawyers | Bisnar Chase "
        pageDescription="The Anaheim Car Accident Lawyers of Bisnar Chase believe that you should not have to endure pain and suffering due to someone else's negligence. Call 949-203-3814 for one of our car crash attorneys in Anaheim California. Recieve a free consultation and if there is no win there is no fee, guaranteed!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <img
            src="/images/car-accidents/anaheim-car-accident-attorney-banner-three.jpg"
            alt="Anaheim car accident lawyers"
          />
        </div>

        <p>
          Our <strong>Anaheim car accident attorneys </strong>have collected
          over <strong>$500 million </strong>in verdicts, judgments and
          settlements and we've been representing vehicle accident victims since
          1978. We have the experience and the means to take on some of the most
          complex cases.
        </p>
        <p>
          We are determined to get fair compensation for our clients and our
          objectives involve fighting for and representing the "little guys."
        </p>
        <p>
          If you or any family members have suffered from a catastrophic car
          accident <Link to="/contact">contact</Link> an experienced Anaheim
          lawyer immediately.
        </p>
        <p>
          Contact our personal injury attorneys in Anaheim, California at{" "}
          <strong>949-203-3814</strong> for a free consultation.
        </p>
        <h2>What Causes an Anaheim Car Accident?</h2>
        <p>
          According to CHP's Statewide Integrated Traffic Records System
          (SWITRS), over 220,000 people were injured in California and over
          3,000 died in a car accident. What contributed to many of these cases
          and factors that persist in Anaheim car accidents include:
        </p>
        <ul>
          <li>Drunk Driving</li>
          <li>Driving while Under the Influence of Drugs</li>
          <li>Texting While Driving</li>
          <li>Speeding Above the Posted Limit</li>
          <li>Severe Weather Conditions (Rain)</li>
          <li>Malfunctioning Car Equipment</li>
          <li>Road Hazards</li>
          <li>Driving Severely Under the Posted Limit</li>
          <li>Failure to Check "Blind Spots"</li>
          <li>Road Rage</li>
        </ul>
        <p>
          One or a combination of the above factors can cause a serious car
          accident.
        </p>
        <p>
          It is your duty as a driver on the road to be observant of all of
          these factors at all times. If you see any of these factors out on the
          road, you are encouraged to call 911 and report them to the
          authorities. Calling 911 while driving is permitted under California
          law.
        </p>
        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/car-accidents/anaheim-car-accident-attorneys-drunk-driver.jpg"
                width="100%"
                alt="Car accident lawyers in Anaheim"
              />
            </LazyLoad>
          </center>
        </p>
        <h2>Drunk Driving Statistics</h2>
        <p>
          Drunk driving is not only irresponsible, but it's also can cause
          injuries or worse; fatalities. According to{" "}
          <Link to="https://www.madd.org/statistics/" target="_blank">
            MADD
          </Link>
          (mothers against drunk driving) over 10.1 million people have been
          documented to drive under the influence.
        </p>
        <p>
          In one year, there was a total of 10,265 wrongful deaths due to drunk
          driving reported the{" "}
          <Link
            to="https://www.cdc.gov/motorvehiclesafety/impaired_driving/impaired-drv_factsheet.html"
            target="_blank"
          >
            {" "}
            CDC
          </Link>
          (Centers for Disease Control and Prevention).
        </p>
        <p>
          If you or a loved one was involved in a drunk driving accident in
          Orange County, the auto accident lawyers at Bisnar Chase are here to
          help. Our experienced car injury lawyers want to make sure that you
          and your loved one receive the care and compensation that you deserve.
        </p>
        <h2>Injuries Caused By Car Accidents in Anaheim </h2>
        <p>
          There are many injuries you can sustain from the aftermath of a car
          accident. Some of the minor injuries you can sustain include minor
          cuts and bruises and soreness. In more extreme cases, the various
          injuries you can sustain include:
        </p>
        <ul>
          <li>Lacerations</li>
          <li>Broken Bones</li>
          <li>Head Trauma/Brain Injury</li>
          <li>Penetrative Wounds</li>
          <li>Whiplash</li>
          <li>Collapsed Lungs/Organs</li>
          <li>Loss of Limbs</li>
        </ul>
        <h2>Most Predominant Injuries Due to Car Accidents</h2>
        <p>
          Among the injuries, the most common types of injuries are head an
          spinal cord traumas. Head injuries can include open head wounds,
          contusions and concussions.
        </p>
        <p>
          The spinal cord is a central part of our nervous system and controls
          the activity and the sensory feeling of the body. When an individual
          suffers from a spinal cord injury the aftermath can be grave.
        </p>
        <p>
          With a spinal cord injury, one can possibly become paralyzed.
          Currently, over 450,000 people are suffering from a spinal cord injury
          and of that amount 36 percent is due to car accidents.
        </p>
        <h2>Tips on How to Prevent a Car Accident in Anaheim </h2>
        <h3>Avoid Distractions</h3>
        <p>
          This means that you can not look at your phone while you are driving.
          In one year, there were over 341,000 car crashes that involved driving
          and texting. One text message can lead to one accident.
        </p>
        <h3>Refrain from speeding</h3>
        <p>
          In California, the maximum speed that can be driven on a highway is 65
          mph. It is prohibited to go over ten miles or drive less that 10 miles
          of than speed limit. Anything else, if caught, will cause law
          enforcement to step in.
        </p>
        <h3>Always use your seatbelt</h3>
        <p>
          By using your seatbelt you are not only keeping yourself safe while
          driving but if you are involved in an accident it can save your life.
        </p>
        <p>
          <center>
            <LazyLoad>
              <img
                src="/images/car-accidents/anaheim-car-accident-attorney-claim.jpg"
                width="100%"
                alt="Anaheim car accident lawyers "
              />
            </LazyLoad>
          </center>
        </p>
        <h2>Frequently Asked Questions on Anaheim Car Accident Claim</h2>
        <p>
          Being involved in a car accident can be horrific and stressful. Most
          motor vehicle accident victims are left confused and do not know who
          to turn to. There are measures you can to make sure you are on the
          right path though.{" "}
        </p>
        <p>
          <strong>
            The following are questions that are asked often by injury victims
            after an auto collision
          </strong>
          :
        </p>
        <p>
          {" "}
          <strong>How much can I recover after my accident?</strong> Every car
          wreck is unique. This means that there is not a set amount of
          compensation that each car accident victim receives. The amount of
          compensation that you receive for your injury case is dependent on
          your injuries and the property damage. After your accident, you will
          be contacted by the other driver's insurance company. Do not settle
          for the first offer the insurance company proposes to you. Insurance
          companies aim to give you the smallest amount of money. Speak to a
          personal injury attorney in Anaheim to explore your financial and
          legal options.{" "}
        </p>
        <p>
          <strong>What is comparative negligence?</strong> Comparative
          negligence in California states that a person who caused a car
          accident can qualify to receive compensation. Comparative negligence
          aims to make all people who were involved in an auto collision liable
          for the damages. Many people who are labeled as the defendant in the
          court of law can use comparative negligence to their advantage. A
          defendant who claims comparative negligence for their case may not
          have to pay the full amount for losses. The state of California uses a
          pure comparative negligence law.
        </p>
        <p>
          <strong>When should I hire an attorney?</strong> If you've been
          involved in a car accident, be sure to seek immediate medical
          attention before anything else and keep notes of what happened during
          and after the accident. Our car accident lawyers can walk you through
          all the various steps behind filing an injury claim and we will also
          fight off the vicious auto insurance companies that only care about
          their best interest. If you're going to file an auto accident claim,
          it is imperative that you know exactly what you are going against and
          you should also have legal representatives fighting on your side.{" "}
        </p>
        <p>
          <strong>Can I file a wrongful death claim?</strong> Families who have
          lost their loved one in an auto accident have the right to file a
          wrongful death claim. Surviving family members such as children and
          spouses are usually the people who file for this type of claim. Family
          members that have suffered the loss of a loved one can file for
          compensation for losses such as funeral expenses, medical expenses the
          deceased obtained before their accident and income that the deceased
          contributed to the household. If the deceased left his/her estate to a
          person that is not blood related they have the potential to file a
          wrongful death claim as well.
        </p>
        <p>
          <strong>What can damages can I be compensated for? </strong>You can be
          compensated for damages such as lost wages, medical bills, property
          damage and loss of consortium. You can also be compensated for the
          emotional damages that you have suffered because of an accident as
          well. Pain and suffering must be proven so you may earn recovery.
          Remember though, there is not a fixed amount of compensation that will
          be guaranteed for each car accident case. Upon your consultation with
          an Anaheim personal injury lawyer, you will learn what damages you can
          be compensated for and also be informed of what steps to take next.
        </p>

        <h2>Legal Protection for Pedestrians Against Car Accidents</h2>
        <p>
          Streets and intersections frequented by pedestrians deserve special
          attention. With dozens of pedestrian accidents growing at an
          exponential rate, there is a need for more focus on pedestrian safety
          in Anaheim. The{" "}
          <Link
            to="https://factfinder.census.gov/faces/nav/jsf/pages/community_facts.xhtml?src=bkmk"
            target="_blank"
          >
            United States Census Bureau
          </Link>{" "}
          stated that Anaheim, CA is home to an estimated amount of 349,007
          people.
        </p>
        <p>
          There are hundreds of thousands of people coming through Anaheim every
          year visiting local vacation destinations, and with that many
          pedestrians surrounding Disneyland, pedestrian accidents increase.
        </p>
        <p>
          While local residents have fought for more stop signs and traffic
          signals to slow down traffic, Anaheim's policies are based on specific
          guidelines. These take into account traffic and pedestrian volumes,
          accident history, and any unusual conditions.
        </p>
        <p>
          In fact, the city points to studies that show how simply improving an
          intersection's visibility by prohibiting parking near it is often more
          effective in reducing potential conflict points. They insist that too
          many stop signs reduce their effectiveness, and that they're typically
          ignored by drivers speeding up between stop signs.
        </p>
        <p>&nbsp;</p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/WRC9j2rCPeQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>
          Let Our Anaheim Car Accident Lawyers Deal With Insurance Companies
        </h2>
        <p>
          Find yourself saying "where can I find a car accident lawyer near me?"
        </p>
        <p>
          Protecting your rights and fighting for your case is a serious matter
          to our Anaheim car accident lawyers. The{" "}
          <strong>Law Firm of Bisnar Chase</strong> has the experience, skills,
          determination to take on all sorts of auto accident cases.
        </p>
        <p>
          Call us now at <strong>949-203-3814</strong> to speak to one of our
          attorneys and learn more on the legal services we can provide you.{" "}
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
