// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/Anaheim truck accident lawyers banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Truck Accidents Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 to speak with highly rated Anaheim truck accident lawyers and gain compensation for your injuries. Bisnar Chase Truck Accidents Attorneys have obtained a 96% success rate and have won over $500 million dollars in wins for our clients. Contact us to receive a free consultation!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Anaheim Truck Accident Attorneys"
            title="Anaheim Truck Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          In Anaheim, California, intense truck traffic is quite common on the
          streets of this popular Orange County city, but with it, truck
          accidents have become rampant. The{" "}
          <strong>Anaheim Truck Accident Attorneys</strong> of Bisnar Chase
          provide relief to lessen the pain and suffering of truck accident
          victims.
        </p>
        <p>
          Over the course of <strong>39 years</strong> the law offices of Bisnar
          Chase have earned over <strong>$500 million</strong> in winnings for
          our clients. We will listen to the details of your incident during
          your free consultation, and if you are entitled to compensation, we
          will do all the hard work for you while you recover.
        </p>
        <p>
          If you have been involved in a truck accident in Anaheim, call Bisnar
          Chase Anaheim <Link to="/anaheim">Accident Injury Lawyers</Link> now
          at <strong>949-203-3814</strong>.
        </p>
        <h2>What Causes Truck Accidents?</h2>
        <p>
          The{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2015"
            target="_blank"
          >
            Federal Motor Carrier Safety Administration
          </Link>
          (FMCSA) reported that there were 4,311 truck accidents that resulted
          in fatalities in 2015. That is an eight percent increase from 2014's
          stats. What are the possible factors that have made truck accidents
          more prevalent over the past 15 years?
        </p>
        <p>
          <strong>
            The various{" "}
            <Link
              to="https://www.fmcsa.dot.gov/safety/research-and-analysis/large-truck-crash-causation-study-analysis-brief"
              target="_blank"
            >
              causes of truck accidents
            </Link>{" "}
            include:
          </strong>
        </p>
        <ul>
          <li>Driver Fatigue</li>
          <li>Intoxicated Drivers</li>
          <li>Texting/Distracted Drivers</li>
          <li>Defective Equipment</li>
          <li>Substandard Inspections</li>
          <li>Weather Conditions</li>
          <li>Hazardous Materials</li>
        </ul>
        <LazyLoad>
          <img
            src="/images/truck-accidents/anaheim truck accident lawyers.jpg"
            width="100%"
            alt="Anaheim truck accident lawyers"
          />
        </LazyLoad>
        <h2>The Aftermath of a Truck Accident</h2>
        <p>
          Being involved in accident can be devastating not only to your
          physical health but mentally as well. Dealing with the aftermath also
          stems itself to be complicated. Many accident victims do not know
          where to begin and are not too sure about the legal options they
          posses.
        </p>
        <p>
          There are certain measures you can follow when involved in a truck
          accident.
        </p>
        <h2>
          {" "}
          <strong>5 Steps to Take After a Truck Accident</strong>
        </h2>
        <ol>
          <li>
            <strong>Contact law enforcement immediately.</strong> It is crucial
            that you fill out an accident report. Whether you perceive the
            accident to be big or small, this document will serve to be useful
            in the long run. Most importantly it is typically required by the
            law that you do this as well.
          </li>
          <li>
            <strong>Seek medical attention quickly.</strong> Even though you
            might not see a scrape or experience any physical pain after, it is
            strongly suggested that you be examined by a professional just in
            case you are suffering with pain down the line or if you have
            internal bleeding. If you are not admitted or seen by a medical
            professional after the incident it can be used against you as well
            in your claim.
          </li>
          <li>
            <strong>
              Reach out to the insurance company to file a report.
            </strong>{" "}
            This step is critical, it is not a step that can be overlooked and
            if skipped it could affect your case greatly.
          </li>
          <li>
            <strong>
              Say as little as possible when speaking to the other insurance
              company.
            </strong>{" "}
            In other words just state the facts when speaking to the counter's
            insurance company or your own for that matter. Insurance companies
            want to save money so your best interest isn't always theirs.
          </li>
          <li>
            <strong>Call a local truck accident attorney. </strong>The law
            offices of Bisnar Chase have been specializing in truck accidents
            for many years and have a 96% success rate. Contact{" "}
            <strong>949-203-3814</strong> and receive a free consultation with
            one of our expert truck accident attorneys.
          </li>
        </ol>
        <h2>Whose At Fault in a Truck Accident Case?</h2>
        <p>
          In truck accident cases, it can be challenging to resolve who is truly
          at fault. Perhaps a negligent tractor trailer driver caused a
          collision in Anaheim because he was drowsy after spending too many
          hours driving the road. Perhaps the driver of a car caused the
          accident because she was speeding or cutting the truck driver off.
        </p>
        <p>
          In many cases, neither driver is at fault. Often, truck accidents are
          caused by poorly maintained vehicles, improperly loaded vehicles or
          defective truck parts. We know how to determine who was at fault and
          take action against them.
        </p>
        <p>
          With that said, if both parties are not at fault, how do you take
          legal action? The simplified answer is to{" "}
          <strong>prove negligence</strong>.
        </p>
        <br />

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Truck accident lawfirm in Anaheim"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/anaheim truck accident lawyers text header.jpg')"
            }}
          >
            <h2>Proving Negligence in a Truck Accident Case</h2>
          </div>
        </LazyLoad>
        <p>
          To receive adequate compensation, you must be able to prove that
          somewhere in the course of the truck accident, someone's negligence
          caused you pain and suffering. The following can constitute negligence
          of a third party:
        </p>
        <ul>
          <li>Speeding Over the Posted Limit</li>
          <li>Failure to Check Blind Spots</li>
          <li>
            {" "}
            <Link to="https://www.abc.ca.gov/FORMS/ABC637.pdf" target="_blank">
              Intoxicated Driving
            </Link>{" "}
            (Drugs/Alcohol)
          </li>
          <li>Driving While Sleep Deprived</li>
          <li>Road Rage</li>
          <li>Neglect of Vehicle Maintenance</li>
          <li>Overall Reckless Driving</li>
          <li>Distracted Driving (phone)</li>
        </ul>
        <h2>How to File a Truck Accident Claim</h2>
        <p>
          Before filing your claim, you must be able to prove without a doubt
          that there was an individual or a group of individuals whose
          negligence caused you pain and suffering. If you can do that, you may
          proceed with legal action.{" "}
        </p>
        <p>
          It is also highly recommended that you hire an experienced attorney
          who knows truck accident law and the dangerous areas of Anaheim to
          assist you. The laws concerning truck accident are very complex and
          only a skilled attorney can help you receive maximum compensation for
          your injuries.
        </p>
        <p>
          If you lost a loved one in the aftermath of a truck accident, you may
          be able to additionally list this wrongful death to your pain and
          suffering when you and your lawyer are fighting for compensation.
        </p>
        <p>
          You can fight fight for both your injuries as well as your deceased
          since the court views{" "}
          <Link to="/anaheim/wrongful-death">wrongful death</Link> as a civil
          action and not a matter that would be dealt with in criminal court. If
          the court rules in your favor, you may be able to receive compensation
          for:
        </p>
        <ul>
          <li>Medical Expenses</li>
          <li>Therapy</li>
          <li>Lost Wages</li>
          <li>Funeral Costs</li>
          <li>Lost of Companionship/Loss of a Loved One</li>
        </ul>
        <h2>Why Choose Bisnar Chase?</h2>
        <p>
          Since 1978, Bisnar Chase Personal Injury Attorneys has been serving
          people in and around Anaheim California, specializing in vehicle and
          truck accident law.{" "}
        </p>
        <p>
          Our truck accident attorneys know what to look for when analyzing
          truck related accidents. Call us for a free consultation at:{" "}
          <strong>949-203-3814 </strong>and get the best options and legal team
          for your case.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys
          <br />
          1301 Dove St. #120
          <br />
          Newport Beach, CA 92660
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
