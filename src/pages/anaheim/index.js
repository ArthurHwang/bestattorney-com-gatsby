// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/Santa Ana Banner Image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Personal Injury Lawyers - Bisnar Chase Accident Attorneys"
        pageDescription="Call 949-203-3814 to speak with a top rated Anaheim personal injury lawyer for a free case evaluation. No win, no-fee guarantee and hundreds of millions recovered. Discuss your injury claim today with top lawyers of Bisnar Chase."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Anaheim personal injury attorneys"
            title="Anaheim personal injury attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Bisnar Chase's <strong>Anaheim Personal Injury Lawyers</strong> want
          to be the legal representation you choose for your personal injury
          accident. Filing an injury claim can seem intimidating and stressful.{" "}
        </p>
        <p>
          The Anaheim personal injury lawyers of Bisnar Chase want to be there
          to relieve some of the stress you may be dealing with.
        </p>
        <p>
          The legal representatives of our law Firm have over{" "}
          <strong>40 years of experience</strong> handling personal injury
          cases. Our attorneys have won over
          <strong>
            {" "}
            $500 million dollars for accident victims and their families
          </strong>
          .{" "}
        </p>
        <p>
          We will fight for the compensation you deserve from beginning to end.{" "}
        </p>
        <p>
          We have helped change the lives of clients throughout Orange County
          and have the resources, experience and dedication to help you too.
        </p>
        <p>
          If you or some you love has been seriously injured in a motorcycle
          accident or car crash call the dedicated injury lawyers of Bisnar
          Chase. Our attorneys also specialize in wrongful death cases and have
          won families millions in compensation for the loss of a loved one.{" "}
        </p>
        <p>
          <strong>
            When you call 949-203-3814 you will receive a free consultation.{" "}
          </strong>
        </p>
        <p>
          Let the law offices of Bisnar Chase help you get your life back.{" "}
          <strong>
            Call now to learn more about how you can be compensated for your
            personal injuries in California.{" "}
          </strong>
        </p>
        <h2>What Is an Anaheim Personal Injury Attorney?</h2>
        <p>
          A personal injury attorney in Anaheim legally represents an accident
          victim in order to win compensation for damages. California injury
          laws require that accident victims be compensated for their losses.
        </p>
        <p>
          Personal injury cases can be handled as a "formal" suit or can be
          settled outside of court.
        </p>
        <p>
          <strong>Official legal dispute</strong>: A formal legal dispute
          involves a plaintiff (private person) who is filing a personal injury
          suit against an individual or organization. The plaintiff will have to
          prove that the third party's carelessness led to severe injuries and
          costly damages.
        </p>
        <p>
          <strong>Settlement outside of court</strong>: A settlement that is
          informal usually involves the plaintiff, the third party, insurers and
          the attorneys. Parties try to reach a middle ground and often
          negotiate a settlement that satisfies the plaintiff and the attorneys.
        </p>
        <center>
          <LazyLoad>
            <img
              src="/images/brain-injury/anahiem-brain-injury-image.jpg"
              width="100%"
              alt="Anaheim Personal Injury Lawyers"
            />
          </LazyLoad>
        </center>
        <h2>7 Types of Personal Injury Cases</h2>
        <p>
          There are common types of personal injuries in California that involve
          dog bites, hoverboards and e-cigarettes.{" "}
        </p>
        <p>
          <strong>The top seven cases that clients sue for include:</strong>
        </p>
        <p>
          1. <strong>Auto defects</strong>: Car defects are not only
          inconvenient but they can also lead to severe injuries and even cause
          a fatal accident. It has been reported that Takata airbags have had
          the largest vehicle recall in history. NHSTA reported that over 37
          million cars were recalled due to defective airbags.
        </p>
        <p>
          2. <strong>Product liability</strong>: Products that are not
          manufactured correctly and cause serious physical or psychological
          injuries are elements for a product liability claim. Product liability
          is dependent on three factors which include strict liability,
          carelessness and breach of warranty.
        </p>
        <p>
          3. <strong>Brain injuries:</strong> A traumatic head injury can be
          described as a blow to the head that causes minor or major brain
          damage. Severe brain injuries can cause a person to lose their motor
          skills and also severely affect their emotional state. Different types
          of traumatic brain injuries include concussions, open head wound and
          second impact syndrome.
        </p>
        <p>
          4. <strong>Serious dog bite cases</strong>: About 4.5 million people
          are bitten by dogs each year. Unfortunately, a large portion of that
          amount is children. A personal damages lawyer strongly suggests that
          you seek medical attention immediately if your child has been bitten
          or mauled by a dog. If a dog is infected with rabies, puncture wounds
          that are inflicted on the child can turn deadly.
        </p>
        <p>
          5. <strong>Premise liability</strong>: If an injury was caused by a
          property owners' negligence this qualify' s as a premise liability
          case. It is the responsibility of a property owner to make sure that
          floors, stairs and overall building structure are safe to walk on.{" "}
        </p>
        <p>
          6.{" "}
          <strong>
            {" "}
            <Link to="/anaheim/car-accidents">Car accidents</Link>
          </strong>
          : In one year, over 54 million people suffered from serious injuries
          due to an auto accident. If you are a car accident victim and are
          looking to gain compensation for your injuries contact the best
          lawyers in Anaheim. Our accident attorneys will help you with your
          personal injury suit and guarantee you the best client service in
          Orange County.{" "}
        </p>
        <p>
          7. <strong>Commercial truck accidents</strong>: One fatality takes
          place in over 98% of big-rig truck collisions. Some causes of crashes
          that involve semi-trucks are changing lanes suddenly, driving close to
          big-rigs and speeding.
        </p>
        <p>
          The <strong>Anaheim Personal Injury Attorneys</strong> of Bisnar Chase
          are here to provide top-notch legal advice for kind of injury case.{" "}
        </p>
        <h2>
          Have You Suffered from a Traumatic Brain Injury in Anaheim? <br />
        </h2>
        <p>
          Suffering from a serious head injury can have short and long-term
          repercussions. The brain has the consistency of jelly and for this
          reason, it is one of the most vulnerable organs in the body.
        </p>
        <p>
          When the cranium experiences a sudden blow to the head a victim can be
          left with severe physical and emotional effects. Examples of sudden
          blows to the head include striking the steering wheel in a car
          accident or hitting the floor due to a slip and fall.
        </p>
        <p>
          {" "}
          The type of{" "}
          <Link
            to="https://www.stjudemedicalcenter.org/our-services/rehabilitation/neurological-rehabilitation/"
            target="_blank"
          >
            neurological rehabilitation
          </Link>{" "}
          needed will depend on the kind of head injury experienced by the
          accident victim.{" "}
        </p>
        <p>
          There are two types of head injuries that can be classified as mild or
          severe.
        </p>
        <p>
          <strong>Mild brain injury symptoms</strong>:
        </p>
        <ul>
          <li>Sleeping more than normally</li>
          <li>Alterations in mood such as becoming angry suddenly</li>
          <li>Dizziness</li>
          <li>Problems with completing sentences</li>
          <li>Seizures</li>
        </ul>
        <p>
          <strong>Severe brain injury symptoms</strong>:
        </p>
        <ul>
          <li>Inpatient</li>
          <li>Fluid draining from the ears or nose</li>
          <li>Constant vomiting</li>
          <li>Dilated pupils</li>
          <li>Complications with walking or talking</li>
        </ul>
        <p>
          The law group of Bisnar Chase has taken on complicated personal injury
          for the past four decades.
        </p>
        If we do not win your case you do not have to pay a cent out of your
        pocket.
        <p>
          Contact us today at <strong>949-203-3814</strong>.
        </p>
        <h2>
          Spinal Cord Injuries Caused by Motorcycle Crashes
          <br />
        </h2>
        <p>
          Victims of motorcycle accidents, if a helmet was worn, can decrease
          the risk of suffering from a brain injury. Although a motorcyclist may
          be protected by a helmet, their chances of having a spinal cord injury
          in the accident are not lessened. <br />
        </p>
        <p>
          If a person has been involved in a catastrophic accident, the chances
          that they will become partially paralyzed are high. Research shows
          that damage to the spinal cord can lead to a person being completely
          or partially paralyzed. It can take motorcycle accident victims years
          to heal. Even with on-going physical therapy, the damage to the spinal
          cord is irreversible.
          <br />
        </p>
        <p>
          A personal injury attorney in Anaheim can inform you of your legal
          options after an accident. You may pursue compensation for medical
          bills, at-home care, counseling and loss of consortium.
        </p>
        <h2>
          What to Do If You Have Experienced a Work Injury in Anaheim
          <br />
        </h2>
        <p>
          In one year approximately 466,00 work-related injuries occurred. The
          process of filing a worker's claim can be complicated. There are steps
          that you can take to make the claims process less stressful.{" "}
        </p>
        <p></p>
        <p>
          <strong>Immediately seek medical attention: </strong>At all cost, if
          you have experienced an injury due to a fall or dangerous equipment,
          acquire medical treatment promptly. The sooner you obtain professional
          help the sooner you can understand the severity of your condition and
          begin treatment.{" "}
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/anahiem-personal-injury-worker-hurt-image.jpg"
            width="100%"
            alt=" personal injury lawyers in Anaheim"
          />
        </LazyLoad>
        <p>
          <strong>Alert your supervisor: </strong>You need to notify your
          manager or supervisor right away. If you do not notify your manager
          within 30 days or you may not be able to collect workers compensation.
          Employees who have suffered from a severe injury are to be rewarded
          for the damages they faced according to California injury laws. If
          your employer was not equipped with workers compensation insurance,
          you can then pursue a personal injury case.
          <br />
        </p>
        <p>
          <strong>Complete a work injury claim: </strong>Your employer
          <strong> </strong>must provide you with a claim form within one day
          after being notified of your injury. Then after you can send the form
          or turn in the form into your employer and within 14 days expect the
          company's insurance to contact you.  If your employer does not provide
          you with the forms that you would need to file a claim you can print
          and download it at{" "}
          <Link to="https://www.dir.ca.gov/dwc/forms.html" target="_blank">
            CA.gov
          </Link>
          .
        </p>
        <h2>When Should I Hire an Anaheim Accident Lawyer?</h2>
        <p>
          It is strongly advised to seek out a lawyer near you for your accident
          case. An injury lawyer can help protect your rights against powerful
          insurance companies. The damages acquired in a personal injury
          accident are expensive. If you are in the Anaheim district, hiring an
          experienced personal injury damages lawyer will increase your chances
          of winning the maximum compensation you deserve.{" "}
        </p>
        <li>
          <strong>Medical bills</strong>: Medical expenses that were accumulated
          directly after the accident should not be the only losses accounted
          for. After an accident people may lose their ability to perform daily
          tasks. Continuous physical therapy and counseling aids injury victims
          in their recovery process.
        </li>
        <li>
          <strong>Loss of wages</strong>:
          <strong> The manager must provide the insurance company</strong>:
          <br />
          a)The number of hours you will miss due to your injuries
          <br />
          b)The hourly rate you were paid before your accident
          <br />
          c)The sick and vacation days that will be used while you are
          recovering
        </li>
        <li>
          <strong>Psychological pain and suffering</strong>: Flashbacks,
          anxiety, depression and insomnia can be some of the repercussions of
          being in an accident. Receiving help from a medical profession can aid
          in the healing process. In order to prove that you have suffered from
          a great amount of emotional distress, a letter from a therapist or
          counselor can validate this claim.
        </li>
        <li>
          <strong>Funeral and burial costs</strong>:{" "}
          <Link
            to="https://www.parting.com/blog/funeral-costs-how-much-does-an-average-funeral-cost/"
            target="_blank"
          >
            Funeral costs
          </Link>{" "}
          can range from $7,000-$10,000 dollars. If a loved one has passed
          suddenly it can be difficult to collect all the funds to carry out a
          proper burial. Negligent parties should be responsible for the
          wrongful death of a loved one.
        </li>
        <li>
          <strong>Property Damage</strong>: One of the first steps in trying to
          claim property damage is to contact your insurer right away. Providing
          evidence such as pictures will help prove how much damage was
          inflicted on your property.{" "}
          <strong>
            Different types of property damages that you can be compensated for
            are:
          </strong>{" "}
        </li>
        <div>
          a) damages to your car <br />
          b) personal belongings that have been damaged in the accident <br />
          c) towing expenses
        </div>
        <br />
        <h2>Proof of Suffering from Personal Injuries</h2>
        <p>
          <strong>Medical Documents </strong>
        </p>
        <p>
          Whether it be or email, every single piece of information that is
          between you and a specialist is vital for your accident attorney.
        </p>
        <p>
          Documentation that should be kept includes notes and emails about your
          catastrophic injury from your doctor. Receipts for specialized
          equipment (crutches, wheelchair), food for a particular diet or
          prescriptions will aid in reimbursement. It is also suggested that
          travel expenses for doctor visits be recorded as well.
        </p>
        <p>
          <strong>Photographs </strong>
        </p>
        <p>
          Snapshot images of the incident can prove to be essential to cases
          especially if you want to provide substantial evidence to your{" "}
          <strong>injury lawyer</strong>. The types of photos that should be
          taken should be the damage that has been done to the vehicle, highway
          wreckage and personal injuries.{" "}
        </p>
        <p>
          <strong>Bystander Observations </strong>
        </p>
        <p>
          Whether it be a slip &amp; fall, dog bite or a car accident,
          eyewitness reports can prove to be advantageous for your case. It is
          important to provide your personal injury lawyer with the names of the
          witnesses and their contact information.
        </p>
        <p>
          <strong>Notes </strong>
        </p>
        <LazyLoad>
          <img
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Riverside auto defect lawyer Brian Chase"
            className="imgleft-fixed"
          />
        </LazyLoad>
        <p>
          Keeping a daily record of the progression of your injuries is key.
          Describe every type of pain the moment that you feel it. Also document
          whether the pain keeps you from performing day-to-day tasks.{" "}
        </p>
        <p>
          <span>Brian Chase, Bisnar Chase Mission Statement:</span>
          <br />
          "To provide superior client representation in a compassionate and
          professional manner while making our world a safer place."
        </p>
        <h2>Personal Injury Cases in Anaheim</h2>
        <p>
          Anaheim, CA attracts people from all over the world for one reason:
          Disneyland. The happiest place on earth is not so happy at times
          though. The Orange Register has reported that the famous Anaheim
          attraction has had 140 legal claims brought before the court. The
          culprit? It's none other than the ground itself. One-third of the
          legal disputes belonged to cases that involved slips and falls.
        </p>
        <p>
          A "slip and fall" injury or a "trip and fall" claim is a personal
          injury that involves a person slipping or falling on someone else's
          property.
        </p>
        <p>
          The{" "}
          <Link to="https://www.cdc.gov/" target="_blank">
            Centers for Disease Control and Prevention
          </Link>{" "}
          claims that over 800,000 individuals are hospitalized annually for
          slip and fall accidents. These slip and fall accidents had also led to
          injury victims suffering from concussions or hip fractures.
        </p>
        <p>
          What's even more shocking is that $31 billion dollars annually are
          accumulated from direct medical cost because of slip and fall
          injuries.{" "}
        </p>
        <p>
          Bisnar Chase&nbsp;<strong>personal injury attorneys</strong> have
          served Orange County and Los Angeles for close to four decades. We
          have an astounding record of performance in aiding individuals in
          serious personal injuries. We have provided financial relief by
          obtaining compensation for accidents and impairments for clients
          throughout Anaheim with excellent results.{" "}
        </p>
        <h2>
          Client Reviews of Our Anaheim Legal Representation
          <br />
        </h2>
        <p>
          Our Anaheim personal injury lawyers are passionate about winning you
          the compensation you deserve. We believe that negligent parties should
          be held responsible for the damages you have suffered. We stand by our
          clients each step of the way and if we do not win your case toy do not
          have to pay. We believe that if you have suffered from a personal
          injury you deserve a contingency attorney who cares about protecting
          you and your family.{" "}
        </p>
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          <div>
            <blockquote>
              "John Bisnar is like family, he really took care of us. I'm so
              happy that we found Bisnar Chase. They've been outstanding. They
              kept us informed with what's going on with the case."
            </blockquote>
            <p>
              - <strong>Maria Chan, former client</strong>
            </p>
          </div>
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/lVUUCtM3Ebg"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
        </div>
        <div>
          <div>
            <blockquote>
              "They were always diligent in following up with me and keeping me
              abreast of where they were in the process. They did everything,
              everything. They actually doubled the original offer from the
              insurance company."
            </blockquote>
            <p>
              - <strong>Ed Solomon, former client</strong>
            </p>
          </div>
          <div>
            <LazyLoad>
              <iframe
                width="100%"
                height="500"
                src="https://www.youtube.com/embed/9-UJdHzYPw4"
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen={true}
              />
            </LazyLoad>
          </div>
        </div>
        <h2>Choose Bisnar Chase's Accident Attorneys in Orange County</h2>
        <p>
          The personal injury lawyers at Bisnar Chase have been named one of the
          Top 1% of Lawyers nationwide. Our accident attorneys also have a
          reputation respected by judges, juries and even defense counsels and
          our attorney-client relationship is our top priority.
        </p>
        <p>Let one of our skilled lawyers represent you.&nbsp;</p>
        <p>Shouldn't your pain and suffering end at your injuries?</p>
        <p>
          <strong>
            Contact our Personal Injury Law offices today for a free case
            review.
          </strong>
        </p>
        <p>
          Call us now at <strong>949-203-3814</strong> for a{" "}
          <strong>FREE CONSULTATION</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys
          <br />
          1301 Dove St. #120
          <br />
          Newport Beach, CA 92660
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
