// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "bus-accidents/Anaheim-Bus-Accident-Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Bus Accident Lawyers - Bisnar Chase"
        pageDescription="If you have been involved in transit crash, the Anaheim Bus Accident Lawyers of Bisnar Chase are here to help. Whether you were injured on a school bus or a public transportation system, our injury attorneys have the experience to fight for you. Contact 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Top Rated Anaheim Bus Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Anaheim bus accident attorneys"
            title="Anaheim bus accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The experienced <strong>Anaheim bus accident lawyers</strong> at
          Bisnar Chase have more than three decades of experience helping
          injured victims and their families obtain fair compensation for their
          injuries, damages and losses. We treat each and every case with care,
          compassion and commitment.
        </p>
        <p>
          Our <Link to="/anaheim">experienced personal injury lawyers</Link>{" "}
          have collected over <strong>$500 million dollars</strong> for our
          clients and you may be entitled to compensation too.
        </p>
        <p>
          If you or a loved one has been injured in an Anaheim bus accident,
          please call our experienced accident attorneys at{" "}
          <strong>949-203-3814 for a free consultation </strong>and
          comprehensive case evaluation.
        </p>
        <h2>Frequent Bus Accidents in Anaheim</h2>
        <p>
          Anaheim, California is the home of Disneyland's theme park and
          welcomes millions of tourist from all around the world. As a result of
          Anaheim's new-comers, massive amounts of buses are packed every day at
          bus stops in this Orange County metropolitan.
        </p>
        <p>
          The traffic congestion, aggressiveness and competition of bus companys
          can pressure the bus drivers to get their passengers to and from
          locations on-time, a large majority of the accidents in downtown
          Anaheim are from buses and commercial transportation owned by private
          companies.
        </p>
        <p>
          According to{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2015"
            target="_blank"
          >
            FMCSA
          </Link>{" "}
          (federal motor carrier safety administration), over 4,311 large trucks
          and buses were associated with fatal accidents in 2015. In the Orange
          County region alone, pedestrians were struck every six days within the
          same year reported The Orange County Register.
        </p>
        <h2>The Cause of Bus accidents?</h2>
        <p>
          The lack of secure passages has been the culprit to many collisions.
          Bus and{" "}
          <Link to="/anaheim/pedestrian-accidents">pedestrian accidents</Link>{" "}
          are prominent among Main street. Barber, Anthony Encinas said one
          prominent yet simple fix will solve the problem. Encinas tells the
          Orange County Register that &ldquo;We need a crosswalk."
        </p>
        <h2>School Bus Accidents in Anaheim</h2>
        <p>
          It's easy to assume that school buses would have an outstanding amount
          of accidents. In fact, school buses actually have less fatalities than
          any other vehicle. Even though school buses do not posses any seat
          belts, the amount of deaths among students is minor.
        </p>
        <p>
          The Governing gathered evidence from 2005-2014 and the results showed
          that five deaths take place each year. Approximately .01 school bus
          deaths happen every 100 million miles as opposed to .7 deaths for
          persons driving in passenger automobiles.
        </p>
        <p>
          Pennsylvania and Florida had the most bus crash-related fatalities
          among the states.
        </p>

        <h3>School Bus Deaths &amp; Crashes 2005-2014</h3>
        <h5>Data courtesy of Governing</h5>
        <center>
          <p>
            <LazyLoad>
              <img
                src="../images/bus-accidents/anaheim-bus-accident-info-sheet.png"
                width="65%"
                alt="school bus crashes and deaths 2005-2014"
                className="imgcenter-fluid"
              />
            </LazyLoad>
          </p>
        </center>
        <h2>Who Is Liable for Bus Accidents?</h2>
        <p>
          Tourist and school buses are known as "common carriers." Common
          carriers are defined as a element of an organization that transfers
          people and property from place-to-place.
        </p>
        <p>Common carries can prove to be at fault if:</p>
        <ul>
          <li>
            The common carrier did not show the best care and safety to their
            passengers.
          </li>
          <li>
            The common carrier failed to prevent an accident (for instance, not
            driving at the recommended speed).
          </li>
          <li>
            Individuals located in the vehicle of the common carrier have been
            physically injured or have suffered emotionally.
          </li>
        </ul>
        <p>
          One of the most notorious examples of a common carrier driver being at
          fault is the bus crash of Chattanooga, Tennessee.
        </p>
        <p>
          {" "}
          <Link
            to="https://www.cnn.com/2016/12/15/us/chattanooga-bus-crash/index.html"
            target="_blank"
          >
            CNN
          </Link>{" "}
          reported of the incident involving school bus driver Johnthony Walker
          killing six children because of his speeding. Walker was driving
          around 50 mph instead of the recommended speed of 30 mph. The bus then
          veered off a street, fell on it's side and then collided into a tree.
        </p>
        <p>
          As for the common carrier, CEO of Durham School Services David Duke,
          said that the unfortunate accident left him speechless.
        </p>
        <p>
          "I can't fathom the anguish of the parents whose children were
          involved in this horrific accident and it involved one of my company's
          buses," he said in a public documented announcement.
        </p>
        <p>
          Duke then went on to say that the bus association is taking extra
          steps to ensure the safety of future passengers.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="anaheim bus accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/Anaheim-bus-accident-lawyers-gradient-new.jpg')"
            }}
          >
            <h2>Anaheim Bus Safety Tips</h2>
          </div>
        </LazyLoad>

        <p>
          It is important for bus drivers to know that when passengers step onto
          common carrier territory, they are trusting the bus driver to safely
          transport them to their destination.
        </p>
        <p>
          When a parent allows their child to ride on a school bus, that parent
          is trusting that their son or daughter will reach their stop securely.
        </p>
        <p>
          The following tips are intended for bus drivers, passengers and
          pedestrians.
        </p>
        <h3>Keep an extra eye out when reversing</h3>
        <p>
          When a bus driver reverses, he or she should proceed slowly. Children
          and pedestrians may not be in the bus driver's lane of sight. It is
          strongly suggested that the individual operating the bus should look
          back before backing up.
        </p>
        <h3>Be cautious when driving into neighborhoods</h3>
        <p>
          Bus drivers should watch carefully when passing through neighborhoods
          especially if there aren't any sidewalks. Be mindful that younger
          children may be playing in the street.
        </p>
        <h3>Never walk behind a bus</h3>
        <p>
          It is important that pedestrians should be in the bus driver's sight.
          Make sure you make eye-contact with the driver so he or she knows
          where you are.
        </p>
        <center>
          <p>
            <LazyLoad>
              <img
                src="/images/bus-accidents/anaheim-bus-accident-attorneys-bus-driver.jpg"
                width="100%"
                alt="Anaheim bus accident lawyers"
              />
            </LazyLoad>
          </p>
        </center>
        <h2>What You Can Do If You Are Involved in a Bus Accident</h2>
        <p>
          Dealing with an accident or injury is stressful. Medical bills,
          insurance claims, time off work and other factors can make recovery
          hard. It is important to ask an attorney how long he or she has been
          practicing law specialized in commercial transportation matters.
        </p>
        <p>
          This will become more important when you have a case that involves
          complex legal issues and investigations, and it is also important that
          you find out if the lawyer has experience managing bus accident suits
          and to what degree. A standard consumer lawyer may not be enough. One
          well versed in injury and accident cases should be consulted.
        </p>
        <p>
          <strong>
            A trial lawyer specializing in bus and transit accidents is
            recommended to protect your rights
          </strong>{" "}
          in case the claim cannot be settled outside of court.
        </p>
        <p>
          There are several issues that are unique to bus accident cases. For
          example, bus accident cases involve laws that are specific to bus
          companies and bus drivers. The attorney should understand the concept
          of "duty of care" that a mass carrier owes to their passengers.
        </p>
        <h2>Lawyer Track Record</h2>
        <p>
          The quality of the attorneys experience is also critical.
          <em>
            What is the law firm's success rate? How much have they recovered
            for injured victims or their families? What is the law firm's
            reputation? How are they rated by their clients and peers? Did you
            ask for a list of their case results?
          </em>{" "}
          Results really matter in any profession and personal injury law is no
          exception.
        </p>
        <p>
          Also, ask the lawyer about how many cases they have settled and how
          many have gone to trial.
        </p>
        <p>
          Although a majority of personal injury cases settle out of court, it
          is good to know that your lawyer, in addition to being a skilled
          negotiator also has trial experience and can take the case to trial if
          necessary.
        </p>
        <p>
          When you do your research, also look into whether the lawyer has been
          disciplined by the{" "}
          <Link to="https://www.calbar.ca.gov/" target="_blank">
            California State Bar
          </Link>
          . Remember, you are entitled to know the disciplinary record of any
          lawyer you are interviewing.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Immediate Legal Help</h2>
        <p>
          Call and talk with one of our Anaheim bus accident lawyers. You may be
          entitled to compensation for your injuries. For over 40 years, Bisnar
          Chase has a proven track record of a <strong>96% success rate</strong>{" "}
          for our clients. <strong>Call 949-203-3814 today.</strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys
            <br />
            1301 Dove St. #120
            <br />
            Newport Beach, CA 92660
          </span>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
