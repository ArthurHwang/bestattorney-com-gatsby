// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/anaheim pedestian accident lawyers banner image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Anaheim Pedestrian Accident Attorneys - Bisnar Chase"
        pageDescription="Contact the Anaheim Pedestrian Accident attorneys of Bisnar Chase for a free case evaluation. If you've been injured, speak to a highly rated Anaheim injury lawyer. Call 949-203-3814. 96% success rate and over $500 million recovered!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Anaheim Pedestrian Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Anaheim Pedestrian Accident Lawyers"
            title="Anaheim Pedestrian Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Anaheim is the home to Disneyland and has served itself to be a
          tourism district, which houses restaurants, clubs, shops and other
          entertainment venues. Walking is a routine part of everyday life in
          this bustling Orange County city. The{" "}
          <strong>Anaheim Pedestrian Accident Lawyers </strong>at Bisnar Chase
          know that in a city where walking is prevalent there is bound to be
          pedestrian accidents. With over 340,000 residents and millions of
          tourists each year, pedestrian accidents are common.
        </p>
        <p>
          The experienced Anaheim pedestrian accident lawyers at{" "}
          <Link to="/">Bisnar Chase</Link> have a long and successful track
          record of protecting the rights of injured victims and their families.
          For more than <strong>40 years</strong>, we have fought for the rights
          of injured victims in Orange County. We offer a free consultation and
          have recovered
          <strong>$500 million</strong> in wins for our clients.
        </p>
        <p>
          If you have been involved in a pedestrian accident in Anaheim, call
          our <Link to="/anaheim">Anaheim Injury Lawyers</Link> now at{" "}
          <strong>949-203-3814</strong>.
        </p>
        <h2>Crosswalk Accidents</h2>
        <p>
          According to the{" "}
          <Link
            to="https://www.ocregister.com/2015/10/20/a-deadly-year-in-oc-a-pedestrian-is-struck-and-killed-every-6-days/"
            target="_blank"
          >
            Orange County Register
          </Link>{" "}
          in 2015, there were pedestrian deaths every 6 days in Anaheim.
        </p>
        <p>
          Crosswalks, both marked and unmarked, can be found at different
          Anaheim intersections. Sometimes, crosswalks may not be located at a
          street intersection. Regardless of where the crosswalks are located
          and regardless of whether they are marked or unmarked, California law
          requires that motorists yield the right-of-way to pedestrians in
          crosswalks.
        </p>
        <p>
          {" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=21950.&lawCode=VEH"
            target="_blank"
          >
            California Vehicle Code section 21950
          </Link>{" "}
          states:
          <i>
            "The driver of a vehicle shall yield the right-of-way to a
            pedestrian crossing the roadway within any marked crosswalk or
            within any unmarked crosswalk at an intersection." The same section
            also states that the driver of a vehicle approaching a pedestrian
            within any marked or unmarked crosswalk "shall exercise all due care
            and shall reduce the speed of a vehicle or take any other action
            relating to the operating of the vehicle as necessary to safeguard
            the safety of the pedestrian."
          </i>
        </p>
        <p>
          While motorists have the responsibility to stop at a crosswalk and
          yield the right-of-way to pedestrians, it is also the responsibility
          of pedestrians to cross the street safely and not jaywalk or cross
          against a light.
        </p>
        <p>
          No life is worth the shortcut you may save by jaywalking. Pedestrian
          accidents are a number one source of business for injury attorneys. It
          speaks volume to the number of pedestrian accidents that happen across
          California annually.
        </p>
        <p>
          Perhaps most of us think it will never happen to us, but being hit by
          a car while you're walking can happen in a flash.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pedestrian accident lawyers in Anaheim"
            style={{
              backgroundImage:
                "url('/images/pedestrian-accidents/anaheim pedestrian lawyers.jpg')"
            }}
          >
            <h2>Pedestrian Safety Tips</h2>
          </div>
        </LazyLoad>
        <p>
          Most injuries to pedestrians occur when they are hit because the
          driver was inattentive or didn't see the pedestrian in time to avoid
          the collision.
        </p>
        <p>
          There are several safety measures pedestrians can take to ensure that
          they are not struck.
        </p>
        <ul>
          <li>
            <strong>Be seen</strong>: It is important that pedestrians dress to
            be seen by drivers. If you walk at night wear reflective or
            retro-reflective materials that reflect light when headlights shine
            on them.
          </li>
          <li>
            <strong>Wear sturdy shoes</strong> that will give you good grip.
          </li>
          <li>
            <strong>Use pathways and sidewalks</strong> whenever they are
            available.
          </li>
          <li>
            <strong>Walk facing oncoming traffic</strong> if you ever walk on or
            near the road so that the driver and you can see each other.
          </li>
          <li>
            <strong>Plan your routes</strong> so you avoid hazardous crossings.
          </li>
          <li>
            <strong>Stop. Look. Listen</strong>: Stop and look for traffic in
            all directions before you cross the street.
          </li>
          <li>
            <strong>Do not rely on traffic signs and signals</strong>. You would
            be well advised to look out for traffic even when you are in a
            crosswalk, walking with the light or with the walk signal.
          </li>
          <li>
            <strong>Never jaywalk</strong> or dart into traffic lanes.
          </li>
        </ul>
        <p>
          While it is important for pedestrians to take all possible steps to be
          careful and defensive, it is also important that drivers do their part
          to consciously look out for pedestrians on the roadway and always
          yield the right-of-way to pedestrians.
        </p>
        <h2>Injuries &amp; Damages in Pedestrian Accidents</h2>
        <p>
          Pedestrians can be seriously injured in traffic accidents. Since
          pedestrians have very little protection, there is the potential for
          catastrophic injuries or even fatalities. Some of the common injuries
          sustained in pedestrian accidents include traumatic brain injuries,
          spinal cord damage, broken bones and internal injuries.
        </p>
        <p>
          If your pedestrian accident in Anaheim was caused by a negligent
          driver or a dangerous roadway, you may be able to seek compensation
          for medical expenses, lost wages, cost of hospitalization,
          rehabilitation and other related damages. If a dangerous or defective
          roadway caused the accident, the city or governmental agency
          responsible for maintaining the roadway can also be held liable.
          Please remember that any personal injury or wrongful death claim
          against a governmental agency must be filed within 180 days of the
          incident.
        </p>
        <h2>Why Choose Bisnar Chase Pedestrian Accident Injury Attorneys?</h2>
        <p>
          The specialized pedestrian accident attorneys of Bisnar Chase have
          been legally representing clients in the Orange County region for over
          39 years. We have a achieved a 96% and are with our clients from the
          beginning when the accident occurs until you are finally compensated
          for damages.
        </p>
        <p>
          <strong>If there is no win, there is no fee. </strong>
        </p>
        <p>
          <strong>
            Please call us at 949-203-3814 for more information or free case
            review.
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
