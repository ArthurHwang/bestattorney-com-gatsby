// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function OlivierMacielPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="2 Year Old Suffers Chemical Burns in Throat"
        pageDescription="Olivier Maciel received an 8 figure settlement for chemical burns to his throat."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>2 Year Old Olivier Maciel gets Chemical Burns in his Throat</h1>
        <BreadCrumbs location={location} />

        <p>
          On November 10th, 2008, Claudia Maciel and her 2 year old son,
          Olivier, were visiting Claudia's grandmother at a nursing home care
          and rehabilitation facility in Tustin. They were eating lunch on an
          enclosed patio and Olivier went towards the patio bushes to try to
          play with a bunny he had seen. Moments later, Claudia heard Olivier
          scream. He was clutching his mouth sitting next to a drinking cup
          containing blue liquid. The cup contained liquid laundry detergent,
          which Olivier had drank after it had been left there by the
          housekeeping staff of the facility.
        </p>
        <p>
          Olivier suffered third-degree chemical burns to his mouth, esophagus,
          and stomach. The esophagus chemical burns developed scarring which
          caused the esophageal passage to repeatedly constrict over time.
          Olivier had to be treated with surgical dialations under general
          anesthesia to widen the passage, which took place over a 4 year
          period. He was unable to eat solid foods after his accident, and
          required surgery to insert a gastronomy tube into the stomach that
          delivered food directly into his stomach.
        </p>
        <p>
          Olivier had permanent damage to his digestive system from this
          accident. He will need annual endoscopies for the rest of his life, in
          which doctors will examine his throat and esophagus with a camera on a
          flexible tube, to make sure that he does not develop esophageal
          cancer. Olivier will also constantly experience acid reflux due to his
          esophageal sphincter being damaged, which will allow his stomach acids
          to move into the esophagus.
        </p>
        <p>
          Bisnar Chase represented Claudia and Olivier in their case against the
          nursing facility and the company who was under contract to provide
          housekeeping and laundry services for the building. We put in over
          2000 hours making sure that Olivier got the representation he needed
          with the best expert witnesses that could testify to the damages that
          Olivier Maciel had to endure. These witnesses included a
          gastroenterologist, a toxicologist, a child psychologist, a pediatric
          surgeon, an anesthesiologist, a medical billing expert, a nursting
          expert, a testing chemist, a housekeeping standard of care expert, and
          an economist.
        </p>
        <p>
          As a result, Bisnar Chase won a 7-figure settlement from the
          housekeeping company that left the laundry detergent on the patio, and
          also collected a settlement from the nursing home facility, for a
          total of $10,030,000. This money was used to provide for the Maciel
          family, pay off their medical bills, and to set up a trust and annuity
          for Olivier Maciel to collect when he comes of age to pay for his
          higher education and other needs. "Olivier went through years of
          invasive surgeries to repair the damage to his body," says Olivier's
          lawyer, <Link to="/attorneys/scott-ritsema">Scott Ritsema</Link>. "As
          he got older, Olivier became more and more aware of the surgical
          procedures, to the point he would become agitated and cry before every
          procedure. Thankfully his doctors were eventually able to stabilize
          his condition. Through our work, Olivier was fairly compensated for
          his horrific injuries and his future is now secure."
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
