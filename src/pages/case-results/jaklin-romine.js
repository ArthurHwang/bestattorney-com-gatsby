// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function JalkinRominePage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Appellate Court Decision: A $24.7 Million Victory"
        pageDescription="An Appellate Court upholds the 24.7 million dollar verdict for Jaklin Romine, a victim of a defective seat that left her permanently paralyzed in a rear-end accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          $24.7 Million Award for Jaklin Romine - Defective Seatback Victim
        </h1>
        <BreadCrumbs location={location} />

        <p>
          <em>
            An appellate court upheld (Case Number: B239761) a $24.7 million
            jury verdict awarded to Jaklin Mikhal Romine who was rendered a
            permanent quadriplegic when the defective driver's seat of her car
            collapsed during a rear-end collision in October 2006.
          </em>
        </p>

        <p />

        <p>
          NEWPORT BEACH, Calif. -- Newport Beach, Calif. March 25, 2014 – In a
          decision issued March 17, the Court of Appeal for the State of
          California, Second Appellate District, ruled that a $24.7 million jury
          award to Jaklin Mikhal Romine in an auto product liability case must
          be upheld. According to court documents, Johnson Controls designed,
          manufactured and sold the seat, which collapsed during a rear-end
          collision in October 2006 at a street intersection in Pasadena, Calif.
        </p>

        <p>
          Court documents state that Romine suffered catastrophic head and
          spinal cord injuries in spite of wearing a seatbelt because her{" "}
          <Link to="/auto-defects/seatback-failure"> seatback collapsed</Link>.
          On impact, Romine's seat broke and collapsed backwards allowing her
          body to submarine rearward underneath her seatbelt and shoulder
          restraint. Her head struck the back of the rear passenger seat causing
          her to suffer the severe injuries that left her permanently paralyzed,
          according to court filings.
        </p>

        <p>
          The appellate court issued a decision stating that the seat that
          caused Romine's permanent injuries was in fact defective. The
          appellate court also upheld the jury's $24.7 million verdict for
          Romine. "We are extremely pleased that the court upheld the jury's
          verdict for our client," said Brian Chase, lead trial attorney and
          partner at Bisnar Chase. "Jaklin has undergone permanent injuries as a
          result of a defective seatback. Her life will never be the same
          again."
        </p>

        <p>
          Chase said that since the early to mid-90s, Johnson Controls was well
          aware of the dangers and risks of weak seats. Because of this
          knowledge, the company had actually developed a design that was more
          structurally sound to prevent seats from collapsing in accidents such
          as this one. “As a matter of fact, had Johnson Controls used their
          other seat design, a seat design that is much more structurally sound
          that it had known about for at least 10 years before this accident,
          Jaklin would be walking today and suffered little or no injuries in
          this accident” says Chase.
        </p>

        <p>
          "They had the know-how to build safer seats, but chose to put profits
          before the safety of consumers," Chase said. "This tragedy could have
          been entirely avoided."
        </p>

        <p>
          The appellate court's decision, which upholds the jury award and deems
          the seatback in question to have been defective, is a tremendous
          victory not only for the product liability lawyers at Bisnar Chase who
          poured their time, efforts and passion into this case, but also for
          consumers, Chase said.
        </p>

        <p>
          "The appellate court also upheld the 'consumer expectation test,'
          which means that consumers have a reasonable expectation of safety
          from a product," Chase said. "Auto makers and manufacturers of other
          products do not want to be held up to that test. They'd rather have a
          battle of experts and confuse the jury. But, thankfully, the court
          held up the consumer expectation test, which is a win for us and the
          consumers."
        </p>

        <p>
          Unfortuantely, seats in cars continue to be made with questionable
          quality for the sake of saving a few dollars here and there for the
          manufacturer. You can find more information about defective seat backs
          and other dangerous auto defects in our{" "}
          <Link to="/auto-defects">auto defects section.</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
