// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function LucasVogtPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Accident Victim Lucas Vogt Awarded $30 Million Settlement"
        pageDescription="Bisnar Chase outstanding case results from Personal injury lawyers in California which total over 500 million in verdicts and settlements."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>$30-Million Judgment for Motorcycle Accident Victim Lucas Vogt</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/scott_ritsema.jpg"
          alt="scott ritsema"
          className="imgright-fixed"
        />

        <p>
          <i>
            Attribution: This article is the syndication source of a recently
            released{" "}
            <Link
              to="http://www.prweb.com/releases/2013/10/prweb11214295.htm"
              target="blank"
            >
              press release
            </Link>
          </i>
        </p>

        <p>
          The personal injury lawyers of Bisnar Chase have obtained a
          $30-million judgment (Case number: 30-2012 00542712) for 25-year-old
          Lucas Vogt, who suffered a catastrophic brain injury in a 2011
          motorcycle accident. The personal injury lawsuit was filed in Orange
          County Superior Court on Feb. 6, 2012.
        </p>
        <p>
          Bisnar Chase personal injury lawyer Scott Ritsema said the $30-million
          judgment came against the driver who initially struck Vogt's
          motorcycle.
        </p>
        <p>
          "It is always our greatest pleasure to provide the means for a family
          to be able to{" "}
          <Link to="/catastrophic-injury">deal with a catastrophic injury</Link>
          ," Ritsema said. "We hope Lucas Vogt will be able to have the best
          recovery he can under the circumstances and maximize his time with his
          2-year-old daughter."
        </p>
        <p>
          According to court documents, the motorcycle accident occurred the
          evening of July 25, 2011, during rush hour traffic. Vogt was riding
          between the number one and two lanes on the 91 Freeway near Gypsum
          Canyon Road in Anaheim when the defendant, Andrew Saldate, attempted
          to make a lane change and collided with Vogt's motorcycle, the
          complaint states. The impact of the collision knocked Vogt from his
          motorcycle and into traffic lanes where he was struck by two other
          vehicles a 2005 Jeep Liberty driven by Shana Johnson and a 2000 BMW
          328i driven by Lori Dunbar, court documents state. Johnson and Dunbar
          both settled rather than proceed to trial.
        </p>
        <p>
          Vogt was treated by paramedics at the scene and was later transported
          to a trauma center, court documents state. He suffered a traumatic
          brain injury in addition to multiple fractures, road rash and other
          serious injuries. He had to undergo surgeries and extensive
          rehabilitation for his brain injury, according to the complaint.
        </p>
        <p>
          Although he has made "significant progress" since the accident, he is
          still unable to walk or communicate verbally, Ritsema said. Victims of
          catastrophic injuries such as Vogt go through tremendous physical,
          emotional and financial challenges and in addition to the victims,
          their families also suffer life-changing consequences, he said. Vogt's
          medical expenses to date add up to about $2 million, the complaint
          states.
        </p>
        <p>
          "Lucas has a long road to recovery," Ritsema said. "But we hope that
          the compensation he receives will help him take a few more steps in
          the right direction. We hope it will give him the best chance at
          making a recovery and enhancing his quality of life."
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
