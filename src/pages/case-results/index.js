// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React, { useState } from "react"
import { FaDollarSign, FaRegArrowAltCircleRight } from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { CallButton } from "../../components/utilities"
import { CaseResults } from "../../data/caseResults"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function CaseResultsPage({ location }) {
  const [results, setResults] = useState({
    caseResultsOutput: CaseResults.slice(0, 12)
  })

  const handleAwardClick = ({ target }) => {
    if (target.value === "all") {
      setResults({
        ...results,
        caseResultsOutput: [...CaseResults]
      })
    } else {
      const filteredOutput = CaseResults.filter(
        item => item.awardcat === target.value
      )
      setResults({
        ...results,
        caseResultsOutput: filteredOutput
      })
    }
  }

  const handleLocationClick = ({ target }) => {
    const filteredOutput = CaseResults.filter(item => {
      const values = Object.values(item.location)
      return values.includes(target.value)
    })
    setResults({
      ...results,
      caseResultsOutput: filteredOutput
    })
  }

  const handleCaseClick = ({ target }) => {
    const filteredOutput = CaseResults.filter(item => {
      const values = Object.values(item.category)
      return values.includes(target.value)
    })

    setResults({
      ...results,
      caseResultsOutput: filteredOutput
    })
  }

  const handleCatastrophicClick = ({ target }) => {
    if (target.innerHTML.includes("Non")) {
      const filteredOutput = CaseResults.filter(item => !item.catastrophic)
      setResults({
        ...results,
        caseResultsOutput: filteredOutput
      })
    } else {
      const filteredOutput = CaseResults.filter(item => item.catastrophic)
      setResults({
        ...results,
        caseResultsOutput: filteredOutput
      })
    }
  }

  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Case Results : Personal injury Verdicts & Settlements"
        pageDescription="Bisnar Chase outstanding case results from Personal injury lawyers in California which total over 500 million in verdicts and settlements."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Thousands of Personal Injury Success Stories</h1>
        <BreadCrumbs location={location} />

        <p>
          Bisnar Chase has been representing injured clients throughout
          California since 1978. We've taken on some of the biggest cases
          because we have the experience, expertise and resources to get
          results.
        </p>
        <p>
          <strong>
            To date, Bisnar Chase has collected more than $500 Million in
            settlements and verdicts
          </strong>
          .
        </p>
        <h2>Some of Our Cases</h2>

        <CaseResultsWrapper>
          <StyledFilterBox>
            <h3 className="title">View our cases by:</h3>
            <div className="buttons">
              <StyledCatastrophicBtn onClick={handleCatastrophicClick}>
                Catastrophic <br />
                Injury Cases
              </StyledCatastrophicBtn>
              <StyledNonCatastrophicBtn onClick={handleCatastrophicClick}>
                Non-Catastrophic <br /> Injury Cases
              </StyledNonCatastrophicBtn>
            </div>
          </StyledFilterBox>

          <StyledCaseResults>
            <h3 className="title">Filter our cases by:</h3>
            <div>
              <div className="checkboxes">
                <ul>
                  <h3 className="filter-title">Location</h3>
                  <li>
                    <input
                      onClick={handleLocationClick}
                      type="radio"
                      value="la"
                      name="location"
                    />{" "}
                    Los Angeles
                  </li>
                  <li>
                    <input
                      type="radio"
                      value="oc"
                      name="location"
                      onClick={handleLocationClick}
                    />{" "}
                    Orange County
                  </li>
                  <li>
                    <input
                      type="radio"
                      value="norcal"
                      name="location"
                      onClick={handleLocationClick}
                    />{" "}
                    Northern California
                  </li>
                  <li>
                    <input
                      type="radio"
                      value="other"
                      name="location"
                      onClick={handleLocationClick}
                    />{" "}
                    Other
                  </li>
                </ul>
                <ul>
                  <h3 className="filter-title">Case Type</h3>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="auto"
                      onClick={handleCaseClick}
                    />{" "}
                    Car Accident
                  </li>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="motorcycle"
                      onClick={handleCaseClick}
                    />{" "}
                    Motorcycle Accident
                  </li>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="pedestrian"
                      onClick={handleCaseClick}
                    />{" "}
                    Pedestrian Accident
                  </li>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="auto-defect"
                      onClick={handleCaseClick}
                    />{" "}
                    Auto Defect
                  </li>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="premises"
                      onClick={handleCaseClick}
                    />{" "}
                    Premises Liability
                  </li>
                  <li>
                    <input
                      type="radio"
                      name="case-type"
                      value="other"
                      onClick={handleCaseClick}
                    />{" "}
                    Other
                  </li>
                </ul>
                <ul className="case-award-box">
                  <h3 className="filter-title">Case Award</h3>
                  <li>
                    <input
                      name="case-award"
                      type="radio"
                      value="5+"
                      onClick={handleAwardClick}
                    />{" "}
                    More than $5 Million
                  </li>
                  <li>
                    <input
                      name="case-award"
                      type="radio"
                      value="1+"
                      onClick={handleAwardClick}
                    />{" "}
                    More Than $1 Million
                  </li>
                  <li>
                    <input
                      name="case-award"
                      type="radio"
                      value="conf"
                      onClick={handleAwardClick}
                    />{" "}
                    Confidential
                  </li>
                  <li>
                    <input
                      name="case-award"
                      type="radio"
                      value="all"
                      onClick={handleAwardClick}
                    />{" "}
                    All Cases
                  </li>
                </ul>
              </div>
            </div>
          </StyledCaseResults>
          <p
            style={{
              fontSize: "10px",
              lineHeight: "14px",
              textAlign: "center",
              paddingBottom: "1rem",
              paddingTop: "1rem"
            }}
          >
            Note: In some cases the names of our clients have been changed to
            "Doe" or "Smith" in the case of confidential settlements.
          </p>
        </CaseResultsWrapper>

        <StyledOutputWrapper>
          {results.caseResultsOutput.map((item, index) => (
            <CaseCard key={index}>
              {/* <LazyLoad height={107}> */}
              <img src={"/images/case-results/" + item.imageurl} />
              {/* </LazyLoad> */}
              <div className="text-area">
                <p className="card-header">{item.header}</p>
                <div className="card-body">
                  <p className="type">
                    <strong>Type of accident / injury:</strong>{" "}
                    <span>{item.type}</span>
                  </p>
                  <p className="summary">
                    <strong>Summary:</strong> <span>{item.summary}</span>
                  </p>
                </div>
              </div>

              <div className="award-area">
                <p>
                  Award: <FaDollarSign className="dollar-sign" />
                  <span>
                    {item.awardint
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  </span>
                </p>

                {item.storylink.includes("bestattorney") ? (
                  <Link
                    className={item.awardcat === "conf" ? "isDisabled" : ""}
                    to={item.storylink
                      .replace("https://www.bestatto-gatsby.netlify.app", "")
                      .replace(".html", "")}
                  >
                    Full Story{" "}
                    <FaRegArrowAltCircleRight className="right-arrow" />
                  </Link>
                ) : (
                  <Link
                    className={item.awardcat === "conf" ? "isDisabled" : ""}
                    to="/case-results"
                  >
                    Full Story{" "}
                    <FaRegArrowAltCircleRight className="right-arrow" />
                  </Link>
                )}
              </div>
            </CaseCard>
          ))}
        </StyledOutputWrapper>

        <div>
          <h2>Our Case Philosophy</h2>
          <p>
            <LazyLoad>
              <img
                src="/images/our-results1.jpg"
                alt="Case results for Bisnar Chase Personal Injury Attorneys"
                width="370"
                className="imgleft-fixed"
              />
            </LazyLoad>
            "Results" have been the primary focus of every one of the thousands
            of people we have represented since 1978. Like you, most came to us
            to recover financial compensation for their losses and injuries.
            They came for results. Our clients make up thousands of success
            stories. Stories of people like you who were injured or lost a
            family member and wanted professional assistance to recover the
            compensation they were legally due. They came for results.
          </p>
          <div>
            <p>
              Although we haven't won every case we have handled, we have won
              more than nine out of ten. Law firms that only take easy cases
              shouldn't ever lose. If a law firm consistently takes on
              challenging cases against very strong opponents, like we do, they
              are going to lose some cases. And we have. Which would you rather
              have representing you? A law firm that only takes easy, clear cut
              cases or a law firm that takes on challenging cases and wins
              nearly every one? If you are going to war, do you want warriors
              who have won most of their battles or warriors who have never been
              in a real battle?
            </p>
            <p>
              We have represented hundreds of people who have lost a family
              member. We have represented hundreds of people who have been
              attacked by dogs, been mistreated in nursing homes, suffered
              falls, were injured at work or who were cheated out of their full
              employment rights. We have represented hundreds of people who have
              suffered injuries due to defective products such as automobiles,
              prescription medication, medical devices, tools, household
              products and consumer goods. We have represented thousands of
              people who have been injured in traffic collisions.
            </p>
            <p>
              If we take on your case, we will professionally and passionately
              do our best to get you the results you are looking for. Some of
              our biggest results are confidential. What that means is one of
              the terms of the settlement agreement was that we not disclose to
              anyone how much the wrongdoer agreed to pay. There are other big
              cases, some of the very biggest, that according to the settlement
              agreement, we can not say anything at all about the case. Big
              companies many times don't want anyone to know that they did
              something wrong, so they require their settlement terms to be a
              secret. If a big company settles a lawsuit for a large amount of
              money, they don't want the public to know because it might make
              them look like they did something wrong or there is something
              wrong with their product.
            </p>
            <h2>Most of our cases are settled out of court. Why is that?</h2>
            <p>Three reasons:</p>
            <ol>
              <li>
                We have <Link to="/about-us">very skilled trial teams</Link>,
                that have winning reputations and the defense law firms know it.
              </li>
              <li>
                We are very thorough in case/trial preparation and most defense
                law firms see that while we are preparing our cases and this can
                be intimidating.
              </li>
              <li>
                We have highly skilled negotiators and an industry wide
                reputation for getting higher settlements for our clients and
                the mediators know it.
              </li>
            </ol>
            <p>
              For these three reasons, most defendants would prefer to settle
              our clients' claims rather than risk a jury trial with our trial
              teams. Remember: In every case, one side wins - and one side
              loses. Having the clout of <Link to="/">Bisnar Chase</Link> behind
              you might be the difference between winning and losing or might be
              a big difference in how much money actually goes into your pocket.
            </p>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  ul {
    list-style-type: none;
  }

  h3.title {
    text-align: center;
    margin-bottom: 1rem;
  }

  .isDisabled {
    pointer-events: none;
    text-decoration: line-through;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`

const StyledOutputWrapper = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto;
  grid-gap: 2rem;
  margin-bottom: 2rem;

  @media (max-width: 1420px) {
    grid-template-columns: 1fr;
  }

  @media (max-width: 1024px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (max-width: 825px) {
    grid-template-columns: 1fr;
  }
`

const CaseResultsWrapper = styled("div")`
  background-color: #eee;
  margin-bottom: 2rem;
`

const CaseCard = styled("div")`
  border: medium double ${({ theme }) => theme.colors.grey};
  display: grid;
  grid-template-areas:
    "img text"
    "award award";
  grid-template-rows: minmax(100px, 1fr) 35px;
  grid-template-columns: 1fr 2fr;

  img {
    object-fit: cover;
    margin-bottom: 0;
    width: 100%;
    height: 100%;
    grid-area: img;
  }

  .text-area {
    grid-area: text;

    .card-body {
      display: flex;
      flex-direction: column;
      padding: 1rem 1rem;
    }

    .type {
      font-size: 1rem;
      margin-bottom: 0.5rem;
      border-bottom: 1px solid black;
      padding-bottom: 0.5rem;
    }

    .summary {
      font-size: 1rem;
      margin: 0;
    }
  }

  .award-area {
    grid-area: award;
    display: flex;
    justify-content: space-between;
    padding: 0.5rem 0.5rem;
    background-color: #eee;
    align-items: center;

    p {
      margin-bottom: 0;
      font-size: 1.5rem;
      font-weight: 600;
      color: ${({ theme }) => theme.links.normal};

      span {
        font-weight: 600;
        color: ${({ theme }) => theme.links.normal};
      }
    }

    a {
      font-weight: 600;
      font-size: 1.5rem;
    }

    .dollar-sign {
      position: relative;
      top: 3px;
      color: #85bb65;
    }

    .right-arrow {
      position: relative;
      top: 2px;
    }
  }

  .card-header {
    width: 100%;
    background-color: #eee;
    text-align: left;
    padding: 0.4rem 1rem;
    text-transform: uppercase;
    font-weight: 600;
    margin-bottom: 0;
    font-size: 1.4rem;
  }
`

const StyledCaseResults = styled("div")`
  .checkboxes {
    display: flex;
    justify-content: space-around;

    ul {
      margin: 0;
    }

    @media (max-width: 1430px) {
      flex-direction: column;
      align-items: center;

      ul {
        margin-bottom: 2rem;
      }
    }
  }

  .filter-title {
    color: ${({ theme }) => theme.colors.grey};
  }
`

const StyledFilterBox = styled("div")`
  padding-top: 2rem;
  padding-bottom: 2rem;
  .buttons {
    text-align: center;
  }
`

const StyledCatastrophicBtn = styled(CallButton)`
  width: 300px;
  margin-right: 1rem;

  @media (max-width: 1430px) {
    width: 220px;
  }
  @media (max-width: 1225px) {
    display: block;
    margin: 0 auto 1rem;
  }
`

const StyledNonCatastrophicBtn = styled(CallButton)`
  background-color: ${({ theme }) => theme.colors.accent};
  width: 300px;
  margin-left: 1rem;

  @media (max-width: 1430px) {
    width: 220px;
  }

  @media (max-width: 1225px) {
    display: block;
    margin: 1rem auto 0;
  }

  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
  }
`
