// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function TopResultsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Top Case Results: A list of our best verdicts and settlements"
        pageDescription="CWe've compiled a list of our top-award cases to show the best that our firm has to offer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Our Top Case Results</h1>
        <BreadCrumbs location={location} />

        <p>
          These lists of cases contain some confidential settlements, so we are
          unable to list more information about the case. If we have more
          information to share, we have provided a link to that specific case
          and story.
        </p>

        <ul>
          <li>
            <span className="cresult">$32,698,073</span> - Auto defect - Seat
            manufacturers, Johnson Controls
          </li>

          <li>
            <span className="cresult">$30,000,000.00</span> - Motorcycle
            Accident
          </li>

          <li>
            <span className="cresult">$23,091,098.00</span> - Product liability
          </li>

          <li>
            <span className="cresult">$16,444,904.00</span> - Dangerous road
            condition, driver negligence
          </li>

          <li>
            <span className="cresult">$10,030,000.00</span> - Premises
            negligence
          </li>

          <li>
            <span className="cresult">$9,800,000.00</span> - Motor vehicle
            accident
          </li>

          <li>
            <span className="cresult">$8,500,000.00</span> - Motor vehicle
            accident - wrongful death
          </li>

          <li>
            <span className="cresult">$8,250,000.00</span> - Premises liability
          </li>

          <li>
            <span className="cresult">$7,998,073.00</span> - Product liability -
            motor vehicle accident
          </li>

          <li>
            <span className="cresult">$5,000,000.00</span> - Auto Defect
          </li>

          <li>
            <span className="cresult">$4,250,000.00</span> - Product Liability
          </li>

          <li>
            <span className="cresult">$3,075,000.00</span> - Product defect -
            motor vehicle accident
          </li>

          <li>
            <span className="cresult">$3,000,000.00</span> - Dangerous road
            design - Trolly vs. pedestrian
          </li>

          <li>
            <span className="cresult">$3,000,000.00</span> - Motorcycle Accident
          </li>

          <li>
            <span className="cresult">$2,815,958.00</span> - Premises Liability
          </li>

          <li>
            <span className="cresult">$2,800,000.00</span> - Negligent Road
            Design
          </li>

          <li>
            <span className="cresult">$2,500,000.00</span> - Govt. claim,
            product defect - motor vehicle accident
          </li>

          <li>
            <span className="cresult">$2,600,000.00</span> - Auto Defect
          </li>

          <li>
            <span className="cresult">$2,432,250.00</span> - Product liability -
            motor vehicle accident
          </li>

          <li>
            <span className="cresult">$2,375,000.00</span> - On the job injury,
            premises liability
          </li>

          <li>
            <span className="cresult">$2,360,000.00</span> - Wrongful death,
            commercial vehicle - motor vehicle accident
          </li>

          <li>
            <span className="cresult">$2,250,000.00</span> - Auto defect - motor
            vehicle accident
          </li>

          <li>
            <span className="cresult">$2,000,000.00</span> - Defective seatback
            - motor vehicle accident
          </li>

          <li>
            <span className="cresult">$2,000,000.00</span> - Wage and Hour Class
            Action
          </li>

          <li>
            <span className="cresult">$1,900,000.00</span> - Auto Defect
          </li>

          <li>
            <span className="cresult">$1,500,000.00</span> - Auto Defect
          </li>

          <li>
            <span className="cresult">$1,478,819.00</span> - Wage and Hour Class
            Action
          </li>

          <li>
            <span className="cresult">$1,050,000.00</span> - Auto Defect
          </li>

          <li>
            <span className="cresult">$900,000.00</span> - Product defect -
            motor vehicle accident
          </li>

          <li>
            <span className="cresult">$600,000.00</span> - Auto defect - motor
            vehicle accident
          </li>
        </ul>

        <p>
          If you want to read more about our case results, visit our{" "}
          <Link to="/case-results">main results page!</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
