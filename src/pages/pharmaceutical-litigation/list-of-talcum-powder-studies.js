// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Studies Done on the Dangers of Talcum Powder"
        pageDescription="listing the studies of the dangers with using talcum powder on the vagina area and the link to ovarian cancer"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>List of Studies of the Dangers of Talcum Powder</h1>
        <BreadCrumbs location={location} />
        <div
          className="text-header content-well"
          title="List of Scientific Studie"
          style={{
            backgroundImage: "url('/images/text-header-images/science.jpg')"
          }}
        >
          <h2 id="header14">List of Scientific Studies</h2>
        </div>
        <p>
          On this page we have compiled a list of talcum powder studies
          performed by various institutions around the world, and have listed
          their results. News sources report a varying amount of studies on the
          effects of genital talcum powder use, but we have found 20 total
          studies online from 1971 to 2016, 14 of which found a strong positive
          association between the genital use of talcum powder and occurences of
          ovarian cancer. The first study in 1971 should be discounted however,
          as asbestos was an ingredient in talcum powder up until 1973, and
          asbestos is a known carcinogen that would have affected the results of
          the study. The 7 remaining studies either did not find any link
          between talc and ovarian cancer, or the findings were positive but not
          statistically significant (Any link could have been coincidental).{" "}
          <Link to="/pharmaceutical-litigation/talcum-powder">
            More information on ovarian cancer and subsequent lawsuits can be
            found here.
          </Link>
        </p>
        <p>
          We have compiled the results of all of these studies and links here,
          as a comprehensive reference for those who want to know the scientific
          consensus on this topic.
        </p>
        <p>
          Note: Red Numbers indicate that a positive association has been found
          between genital or abdominal talcum powder use and increased risk of
          ovarian cancer. Green numbers indicate no significant association
          between talc and ovarian cancer could be found.
        </p>
        {/* <div id="expand-row" className="row border-box">
          <!--data-toggle="collapse" data-target=".panel-collapse"-->
          <button
            id="collapse-toggle"
            className="btn btn-lg btn-primary centered"
          >
            Collapse All Studies
          </button>
        </div> */}
        <div
          className="panel-group"
          id="accordion-1"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-1"
                to="#collapse-1"
                aria-expanded="true"
                aria-controls="collapse-1"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">1.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Talc and Carcinoma of the Ovary
                        and Cervix
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Suggested Association
                      Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1971
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-1"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> W. Henderson, C. Joslin, A.
                      Turnbull, K. Griffiths
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.readcube.com/articles/10.1111/j.1471-0528.1971.tb00267.x?r3_referer=wol&tracking_action=preview_click&show_checkout=1">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study was performed to examine the similarities between
                    the minerals talc and asbestos and whether talc use, which
                    at the time contained asbestos, had any effect on cancer in
                    the ovaries and cervix. The authors took 13 subjects with
                    ovarian or cervical cancer and obtained tissue samples from
                    them, to determine if any talc particles were present in the
                    samples.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    10 out of 13 of the samples were found to have contained
                    talc particles, and no asbestos particles were found in any
                    of the samples. This was the first study that suggested
                    there may be a link between talc use and ovarian cancer.
                    However, these results imply nothing more than correlation,
                    and the results of the study should be discounted due to the
                    presence of asbestos, a known carcinogen, in the talc.
                    Asbestos was removed from talcum powders in 1973.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-2"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-2"
                to="#collapse-2"
                aria-expanded="true"
                aria-controls="collapse-2"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">2.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Ovarian Cancer and Talc: A
                        Case-Control Study.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positive Association Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1982
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-2"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> D. Cramer, W. Welch, R. Scully,
                      C. Wojciechowski
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://onlinelibrary.wiley.com/doi/10.1002/1097-0142(19820715)50:2%3C372::AID-CNCR2820500235%3E3.0.CO;2-S/pdf">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This case-control study was undertaken to explore in more
                    detail the association between ovarian cancer and the
                    regular genital use of talc products such as baby powder.
                    Researchers interviewed 215 women with ovarian cancer and
                    found 215 controls who matched the age, race and residence
                    of the cases. The study also took into account when talc was
                    dusted on condoms or on diaphragms.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    This study found that there was a significantly higher
                    amount of ovarian cancer cases in subjects who used talc,
                    either as dusting powder or on sanitary napkins. This
                    conclusion still held true even when accounting for
                    interviewer and selection bias.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-3"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-3"
                to="#collapse-3"
                aria-expanded="true"
                aria-controls="collapse-3"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">3.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> A Case-Control Study of
                        Borderline Ovarian Tumors: The Influence of Perineal
                        Exposure to Talc
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Partial Association Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1988
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-3"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> B. Harlow, N. Weiss
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://aje.oxfordjournals.org/content/130/2/390.abstract?sid=f66484de-573b-4f0a-9239-462e1e140744">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study interviewed 116 female residents with serious
                    ovarian tumors and interviewed 158 control subjects in the
                    same area, with regards to their use of hygenic powders.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    There was no difference in risk of ovarian tumors between
                    women who used baby powder or corn starch and those who did
                    not. However, the results showed that women who specifically
                    used talc-containing deodorizing powders had 2.8 times the
                    risk of an ovarian tumor. This result showed that there was
                    a difference between powders containing talc and those that
                    did not contain talc.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-4"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-4"
                to="#collapse-4"
                aria-expanded="true"
                aria-controls="collapse-4"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-negative">4.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Personal and Environmental
                        Characteristics Related to Epithelial Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> No Significant Association
                      Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1988
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-4"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> A. Whittemore, M. Wu, R.
                      Paffenbarger Jr., D. Sarles, J. Kampert, S. Grosser, D.
                      Jung, S. Ballon, M. Hendrickson
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://aje.oxfordjournals.org/content/128/6/1228.abstract?ijkey=3d6deefb1ebc4e71dd842541b3d19c552f283c94&keytype2=tf_ipsecsha">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study was conducted to determine if vaginal exposure to
                    talc increased the risk of epithelial ovarian cancer.
                    Researchers assessed talcum powder use in 188 women with
                    epithelial ovarian tumors and in 539 control subjects. Other
                    factors such as consumption of coffee, tobacco, and alcohol
                    were also reviewed.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    Though there was some data showing that perineal use of
                    talcum powder more than 20 times a month were at 1.45 times
                    the risk for ovarian cancer, this data was not statistically
                    significant, and therefore must be treated as though it was
                    a sampling error. Thus, the study reported that there was no
                    significant link between baby powder use and ovarian cancer.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-5"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-5"
                to="#collapse-5"
                aria-expanded="true"
                aria-controls="collapse-5"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">5.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Risk Factors for Ovarian
                        Cancer: A Case-Control Study.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1989
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-5"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> M. Booth, V. Beral, P. Smith
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2247100">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study was conducted to determine whether several
                    factors including Mentrual Characteristics, reporductive and
                    contraceptive history, and history of exposure to
                    environmental factors like baby powder increase the risk of
                    epithelial ovarian cancer. The study was completed with 235
                    women diagnosed with cancer and 451 controls.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    According to the findings, women who used talc once a week
                    or more had increased for getting ovarian cancer. However,
                    the study notes that this evidence can be controversial
                    since the women were not asked how long they had been using
                    talc, or their frequency of past use.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-6"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-6"
                to="#collapse-6"
                aria-expanded="true"
                aria-controls="collapse-6"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">6.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Risk Factors for Epithelial
                        Ovarian Cancer in Beijing, China
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1991
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-6"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> Y. Chen, P. Wu, J. Lang, W. Ge,
                      P. Hartge, L. Brinton
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://ije.oxfordjournals.org/content/21/1/23.abstract?ijkey=9d30477ab97e5eb5b94d750b20b2f06a4d77cfbc&keytype2=tf_ipsecsha">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study of 112 ovarian cancer cases in Chinese women and
                    224 controls evaluated the risk of ovarian cancer with
                    regard to reproductive, medical, familial, and lifestyle
                    factors.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study found that perineal use of talcum powder increased
                    the risk of ovarian cancer after 3 months of usage.
                    Researchers concluded that this was similar to findings from
                    other institutions around the world.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-7"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-7"
                to="#collapse-7"
                aria-expanded="true"
                aria-controls="collapse-7"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">7.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Reproductive and Other Factors
                        and Risk of Epithelial Ovarian Cancer: An Australian
                        Case-Control Study. Survey of Women's Health Study
                        Group.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1995
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-7"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> D. Purdie, A. Green, C. Bain,
                      V. Siskind, B. Ward, N.Hacker, M. Quinn, G. Wright, P.
                      Russell, B. Susil
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/7558414">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study involving 824 women with ovarian cancer and 860
                    controls explored the risks factors associated with ovarian
                    cancer. In addition to talc use, other factors reviewed were
                    use of oral contraceptives, hysterectomy, tubal ligation,
                    number of incomplete pregnancies, BMI, menstrual history,
                    hormone replacement therapy, history of cancer, and smoking.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    Researchers found that talc use in the abdominal or perineal
                    region was positively associated with the occurrence of
                    ovarian cancer. This study also found that smoking, high
                    BMI, and family history of ovarian cancer was positively
                    associated.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-8"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-8"
                to="#collapse-8"
                aria-expanded="true"
                aria-controls="collapse-8"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">8.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Perineal Talc Exposure and Risk
                        of Ovarian Carcinoma.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1997
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-8"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> S. Chang, H. Risch
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/9191529">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study notes the former research into the effects of
                    talcum powder within the ovaries and expands on it by
                    examining 450 ovarian cancer patients and 564 controls in
                    southern Ontario. Subjects were interviewed about
                    reproductive and menstrual histories.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study concluded that exposure to talc was significantly
                    associated with risk of ovarian cancer. However, duration of
                    talc exposeure only showed a borderline significant
                    association when examining subjects with 10 years of
                    exposure, and there was no significant association found
                    between the risk of cancer and the frequency of exposure.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="accordion-9"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#accordion-9"
                to="#collapse-9"
                aria-expanded="true"
                aria-controls="collapse-9"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">9.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> Genital Talc Exposure and Risk
                        of Ovarian Cancer.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 1999
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-9"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong>
                        {" "}
                        Authors: D. Cramer, R. Liberman, L. Titus-Ernstoff, W.
                        Welch, E. Greenverg, J. Baron, B. Harlow
                      </strong>
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/10209948">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    The abstract for this study noted that previous studies
                    concerning baby powder use had their biological associations
                    called into question, and repeated the talc-ovarian cancer
                    association study with 563 newly diagnosed epithelial
                    ovarian cancer patients, and 523 controls from eastern
                    Massachusetts and New Hampshire.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The results of the study showed that ovarian cancer cases
                    were more likely than the controls to have used talcum
                    powder, and after adjusting for age, location, parity, oral
                    contraceptive use, BMI, and family history of cancer, users
                    of talc were 60% more likely to develop ovarian cancer than
                    those who did not use talc. The association was stronger for
                    women who were exposed to talcum powder before having a
                    child, and for women with serous epithelial cancer, as
                    opposed to mucinous epithelial cancer. The study concluded
                    that more public health warnings about the risks of powders
                    containing talc were warrented.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-10"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-10"
                to="#collapse-10"
                aria-expanded="true"
                aria-controls="collapse-10"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-negative">10.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong> Perineal Talc Exposure and
                        Subsequent Epithelial Ovarian Cancer: A Case-Control
                        Study.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong> No Significant Association
                      Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong> 1999
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-10"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> C. Wong, R. Hempling, M. Piver,
                      N. Natarajan, C. Mettlin
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/10074982">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study performed by the Roswell Park Cancer Institute in
                    New York, used 327 patients with epithelial ovarian cancer
                    as subjects and 693 controls. The study adjusted for age,
                    medical history, smoking history, and common demographic
                    metrics.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    This study found no significant association between talc use
                    and epithelial ovarian cancer even for women who had used
                    baby powder or other talc products for more than 20 years.
                    Additionally, patients with a history of tubal ligation or
                    hysterectomy were also not found to have a significant risk
                    for development of cancer.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-11"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-11"
                to="#collapse-11"
                aria-expanded="true"
                aria-controls="collapse-11"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-negative">11.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong> Prospective Study of Talc Use
                        and Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong> No Significant Association
                      Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong> 1999
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-11"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> D. Gertig, D. Hunter, D.
                      Cramer, G. Colditz, F. Speizer, W. Willet, S. Hankinson
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://jnci.oxfordjournals.org/content/92/3/249.full">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study acknowldeged the potential for recall or
                    selection bias in previous studies and attempted to shed
                    more light on the talc controversy. Researchers ascertained
                    talc exposure prior to the development of ovarian cancer for
                    the subjects, and ran a prospective analysis to determine if
                    there was a talc/cancer link without resporting to a
                    case-control study.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The conclusion of this study did not observe an overall
                    association between ovarian cancer and the use of talc,
                    though it did observe a small increase in serous ovarian
                    cancer in women who had used the product at least once.
                    There were some limitations of the study, the primary one
                    being that there was no available information on the
                    duration of use of the talc.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-12"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-12"
                to="#collapse-12"
                aria-expanded="true"
                aria-controls="collapse-12"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">12.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Factors Related to Inflammation
                        of the Ovarian Epithelium and Risk of Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2000
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-12"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> R. Ness, J. Grisso, C. C.
                      Cottreau, J. Klapper, R.Vergona, J. Wheeler, M. Morgan, J.
                      Schlesselman
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/11021606">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study compared 767 cases of recently diagnosed ovarian
                    cancer with 1367 controls to determine what the role of
                    inflammation was in developing ovarian cancer.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    Several factors were found to have reduced risk of ovarian
                    cancer, including breast feeding, oral contraceptives, and
                    the number of times a woman has been pregnant.
                    Hysterectomies and tubal ligation was also found to have
                    reduced the risk of ovarian cancer. Talc use, endometriosis,
                    ovarian cysts, and hyperthyroidism were found to have
                    increased the risk of cancer.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-13"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-13"
                to="#collapse-13"
                aria-expanded="true"
                aria-controls="collapse-13"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">13.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Perineal Talc Exposure and
                        Epithelial Ovarian Cancer Risk in the Central Valley of
                        California
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2004
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-13"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> P. Mills, D. Riordan, R. Cress,
                      H. Young
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/15382072">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study was conducted with 256 ovarian cancer cases in
                    the California Central Valley, along with 1,122 controls.
                    Subjects were questioned about perineal talc use, how often
                    it was used, and for how many years it was used, as well as
                    other demographic factors.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study found that there was an increased association of
                    ovarian cancer with perineal talc use, but there was no dose
                    response assocation, meaning that this association did not
                    increase as women dusted more frequently. Talc use resulted
                    in the highest risk for serous invasive tumors, and this
                    risk was cut dramatically in women who had undergone tubal
                    ligation.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-14"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-14"
                to="#collapse-14"
                aria-expanded="true"
                aria-controls="collapse-14"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">14.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Talcum Powder, Chronic Pelvic
                        Inflammation and NSAIDs in Relation to Risk of
                        Epithelial Ovarian Cancer.
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2008
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-14"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> M. Merritt, A. Green, C. Nagle,
                      P. Webb, Austrailia Cancer Study, Austrailian Ovarian
                      Cancer Study Group
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/17721999">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>A study of 1,576 Austrailian women
                    with low malignant potential (LMP) ovarian tumors and 1,509
                    controls was performed to determine whether chronic
                    inflammation was the causal mechanism by which talc use
                    would increase the risk of ovarian cancer.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study found that there was a statistically significant
                    association between talc use in the pelvic region and serous
                    epithelial ovarian cancer, and that there was also an
                    assocation for endometrioid ovarian cancer, but this was not
                    significant. It also found that there was an increased risk
                    of only endometrioid and clear cell ovarian cancer among
                    women with a history of endometriosis. Regular usage of
                    anti-inflammatory drugs reduced the risk of LMP mucinous
                    ovarian tumors but no other subtype of cancer. The final
                    conclusion was that chronic inflammation is not a major
                    factor in ovarian cancer risk.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-15"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-15"
                to="#collapse-15"
                aria-expanded="true"
                aria-controls="collapse-15"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">15.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Talc Use, Variants of the
                        GSTM1, GSTT1, and NAT2 Genes, and Risk of Epithelial
                        Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Association Stronger in GSTM1
                      Gene
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2008
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-15"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> M. Gates, S. Tworoger, K.
                      Terry, L. Titus-Ernstoff, B. Rosner, I. De Vivo, D.
                      Cramer, S. Hankinson
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2630413">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study aimed to understand the biological basis for the
                    assocation between talc use and ovarian cancer. It examined
                    1,175 cases and 1,202 controls from a new england
                    case-control study, and 201 cases and 600 controls from a
                    prospective Nurse's Health study.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    Talc was found to have an association with increased risk of
                    cancer, but especially in women who had the GSTT1-null
                    genotype and the GSTM1-present genotype. Women with only the
                    GSTM1 or NAT2 genes did not show any signs of assocation.
                    The results suggest that only certain women with specific
                    genotypes will experience an increased risk for ovarian
                    cancer when using talcum powder.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-16"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-16"
                to="#collapse-16"
                aria-expanded="true"
                aria-controls="collapse-16"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">16.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong> Markers of Inflammation and
                        Risk of Ovarian Cancer in Los Angeles County
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Talc Positively Associated
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2009
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-16"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> A. Wu, C. Pearce, C. Tseng, C.
                      Templeman, M. Pike
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4203374">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study examined the effect that inflammation has on the
                    development of ovarian cancer, specifically in concjuntion
                    with talc use, history of endometriosis, and
                    anti-inflammatory drugs. The study examined 609 women with
                    newly diagnosed epithelial ovarian cancer and 688 controls
                    in Los Angeles County.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study concluded that both talc use and history of
                    endometriosis increased the risk of ovarian cancer - up to 3
                    times the normal risk if both factors were present. On the
                    other hand, the frequent use of anti-inflammatory drugs was
                    found to have increased the risk of ovarian cancer in this
                    study. This was unexpected due to previous studies that
                    found that these drugs reduced the risk of cancer.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-17"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-17"
                to="#collapse-17"
                aria-expanded="true"
                aria-controls="collapse-17"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-negative">17.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Genital Powder Exposure and the
                        Risk of Epithelial Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong> Possible Association Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2011
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-17"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> K. Rosenblatt, N. Weiss, K.
                      Cushing-Haugen, K. Wicklund, M. Rossing
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3384556">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study took 812 cases and 1313 controls to shed more
                    light on the association between genital talcum powder use
                    and ovarian cancer risk. Subjects were asked about their
                    talc use after bathing or on sanitary napkins or diaphragms.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study found a slight increase in risk of ovarian cancer
                    with talc use after bathing, but no risk increase
                    association with a longer duration of use or extent of use.
                    The study also found no increase in risk when using talc on
                    sanitary napkins or diaphragms. Thus, researchers determined
                    that an association is merely "possible".
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-18"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-18"
                to="#collapse-18"
                aria-expanded="true"
                aria-controls="collapse-18"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">18.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Genital Powder Use and Risk of
                        Ovarian Cancer: A Pooled Analysis of 8,525 cases and
                        9,859 Controls
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>Positive Assocation Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2013
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-18"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> K. Terry, S. Karageorgi, Y.
                      Shvetsov, M. Merritt, G. Lurie, P. Thompson, M. Carney, R.
                      Weber, L. Akushevich, W. Lo-Ciganic, K. Cushing-Haugen, W.
                      Sieh, K. Moysich, J. Doherty, C. Nagle, A. Berchuck, C.
                      Pearce, M. Pike, R. Ness, P. Webb, M. Rossing, J.
                      Schildkraut, H. Risch, M. Goodman
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3766843">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study pooled information and resources from 8 different
                    studies each regarding ovarian cancer risk increasing with
                    talc usage. The total cases numbered 8,525, and made use of
                    9,859 controls.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    After pooling all of the participants' information, the
                    study concluded that genital poder use was associated with a
                    small risk in epithelial ovarin cancer, for serous,
                    endometrioid, and clear cell tumors, as well as borderline
                    serous tumors. However, the study found no association
                    between the number of lifetime applications or any
                    association between women who reported non-genital talcum
                    powder usage.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-19"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-19"
                to="#collapse-19"
                aria-expanded="true"
                aria-controls="collapse-19"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-negative">19.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study: </strong>Perineal Powder Use and Risk of
                        Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome: </strong>No Significant Association
                      Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date: </strong>2014
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-19"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> S. Houghton, K. Reeves, S.
                      Hankinson, L. Crawford, D. Lane. J. Wactawaski-Wende, C.
                      Thompson, J. Ockene, S. Sturgeon
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pubmed/25214560">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This cohort study enlisted 61,576 participants with 429
                    ovarian cancer cases, in an attempt to study the assocation
                    of talc with ovarian cancer without running a case-control
                    study, which has it's drawbacks. The average age of these
                    participants was 63.3 years.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>
                    The study could find no association between ever using
                    perineal powder and an increased risk of ovarian cancer. The
                    study noted that it had a large sample size and didn't have
                    to rely on the study cases' memory or biased recollection of
                    their talcum powder use as much as the case-control studies
                    that came before. However, it had its limitations including
                    the fact that all women were post-menopausal and therefore
                    could not gauge the effects of talc on younger women, and
                    also that the study only mentioned perineal powder, and not
                    specifically talc - meaning that the subjects may have been
                    using an alternative like corn starch.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel-group"
          id="study-20"
          role="tablist"
          aria-multiselectable="true"
        >
          <div className="panel panel-default">
            <div className="panel-heading" role="tab" id="headingOne">
              {" "}
              <Link
                role="button"
                className="study-name"
                data-toggle="collapse"
                data-parent="#study-20"
                to="#collapse-20"
                aria-expanded="true"
                aria-controls="collapse-20"
              >
                <div className="row">
                  <div className="col-md-1 col-sm-2 col-xs-4 border-box">
                    <p className="study-number-positive">20.</p>
                  </div>
                  <div className="col-md-11 col-sm-10 col-xs-8 border-box">
                    <div className="col-md-10 col-sm-12 col-xs-12 border-box">
                      <h4 className="panel-title">
                        <strong> Study:</strong> The Association Between Talc
                        Use and Ovarian Cancer
                      </h4>
                    </div>
                    <div className="col-md-2 col-sm-12 col-xs-12 border-box">
                      <i
                        className="fa fa-chevron-down study-expand nomobile"
                        aria-hidden="true"
                      ></i>
                    </div>

                    <div className="clearfix"></div>
                    <div className="border-box col-md-10"></div>
                    <div className="clearfix"></div>

                    <div className="col-md-8 border-box study-outcome">
                      <strong> Outcome:</strong> Talc Positive Association Found
                    </div>
                    <div className="col-md-4 border-box study-date">
                      <strong> Date:</strong> 2016
                    </div>
                  </div>
                </div>
              </Link>
            </div>
            <div
              id="collapse-20"
              className="panel-collapse collapse in"
              role="tabpanel"
              aria-labelledby="headingOne"
            >
              <div className="panel-body">
                <div className="row author-row">
                  <div className="col-md-9 border-box">
                    <p>
                      <strong> Authors:</strong> D. Cramer, A. Vitonis, K.
                      Terry, W. Welch, L. Titus
                    </p>
                  </div>
                  <div className="col-md-3 border-box">
                    <p>
                      {" "}
                      <Link to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4820665">
                        Link To Study
                      </Link>
                    </p>
                  </div>
                </div>

                <div></div>

                <div className="study-summary">
                  <p>
                    <strong> Summary:</strong>
                    This study examined 2,041 women with epithelial ovarian
                    cancer and used 2,100 age- and residence-matched controls.
                    The study calculated "talc-years" (or the product of
                    applications per and years used) and used this number to
                    determine whether there was an increase in likelihood of
                    ovarian cancer when a subject had a higher count of
                    talc-years.
                  </p>
                </div>

                <div className="study-result">
                  <p>
                    <strong> Results:</strong>A trend was found of increasing
                    risk of ovarian cancer with an increase in talc-years. This
                    trend was even more pronounced in premenopausal women, or
                    postmenopausal women using hormone therapy. Risks were also
                    found to vary by weight, smoking, and estrogen/prolactin
                    levels.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h3 id="header15">Study Limitations</h3>
        <p>
          It is important to note the limitations of the studies. Most of the
          studies were case-control studies, and the subjects of the study were
          asked to recall their baby powder usage and the frequency and duration
          of the usage. However, relying on a subjects memory can be dangerous
          because people's memory is often unreliable and can produce incorrect
          results.
        </p>
        <p>
          Another serious limitation is that many of the studies asked patients
          about their perineal powder use, but failed to specify whether their
          powder contained talc. Some of the participants could have reported on
          their corn starch use and skewed the data in that way.
        </p>
        <p>
          Other studies (19) also studied only a specific demographic, and was
          unable to get data for the population as a whole.
        </p>
        <h3 id="header16">Study Takeaways</h3>
        <p>
          These studies predominantly demonstrated that the usage of talcum
          powder in the genital area could increase the risk of developing
          ovarian cancer for certain women. However, they also contained
          interesting insights regarding talc use and ovarian cancer that is not
          readily available from existing online medical sources. This data is
          not necessarily considered medical fact and can be contested, but is
          still interesting to observe.
        </p>
        <ul>
          <li>
            Having a historectomy or tubal ligation lowers the risk of ovarian
            cancer (Study 4, 12, 13)
          </li>
          <li>
            Having a higher BMI (Body Mass Index) raises the risk of ovarian
            cancer (7)
          </li>
          <li>Smoking raises the risk of ovarian cancer (7)</li>
          <li>Hyperthyroidism raises the risk of ovarian Cancer (12)</li>
          <li>Ovarian cysts rise the risk of ovarian cancer (7)</li>
          <li>
            Having the GSTT1 and GSTM1 genes increase the risk of ovarian cancer
            if subject uses talc (15)
          </li>
          <li>
            There is no known association between the length or duration that a
            subject uses baby powder, and the risk of ovarian cancer (8, 10, 13,
            18)
          </li>
        </ul>
        {/* Hysterectomy lowers risk of ovarian cance but probably only if subject
        has not undergone tubal sterilization. (And only for those who had
        procedure 10 years prior.) (Study 4) Hysterectomy lowers risk and tubal
        ligation reduces risk. (study 12) Tubal Ligation highly reduced risk of
        OR (study 13) Talc risk does not increase on diaphragms or on sanitray
        napkins (study 17) Coffee consumption for more than 30 years could
        possibly increase risk of cancer (Study 4) Smoking increases risk (7)
        High BMI increases risk (7) Hyperthyroidusm increases risk (12) Ovarian
        Cysts increases risk (12) Inflammation does not play a role in
        development of cancer (study 14) Aspirin or other anti-inflammatory
        reduces the risk of mucinous ovarin tumors. (study 14) Aspirin INCREASES
        the risk of ovarian cancers (study 16) Endometriosis increases
        endometrioid or clear cell cancer risk (study 14) Endometriosis
        increases cancer risk (study 16) GSTT1 with GSTM1 increases the risk of
        ovarian cancer if the woman uses talc. Not if she doesn't. (study15) No
        association between length of use (10+ years, 20+ years) only that the
        subject had used talc once. (18, 8, 10, 13) */}

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Questions or Comments?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/talc-powder.jpg')"
            }}
          >
            <h2 id="header17">Questions or Comments?</h2>
          </div>
        </LazyLoad>
        <p>
          If you have any questions or comments about these studies, or would
          like more information for your publications, please contact us using
          the contact form at the top of the page. We have compiled a list of
          these studies and the location of where they can be found and would be
          happy to share with any individual or publication.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .panel-group {
    background: #eee;
    padding: 0 1rem;
    border: 2px solid grey;
    margin-bottom: 2rem;
  }
  .row {
    display: grid;
    grid-template-columns: 100px 1fr;
    border-bottom: 1px solid grey;
  }

  .row.author-row {
    display: block;
    border-bottom: none;
    padding-top: 1rem;
  }

  .study-number-positive,
  .study-number-negative {
    font-size: 4rem;
  }
  h4.panel-title {
    font-size: 1.8rem;
    padding-bottom: 5px;
  }

  .study-outcome {
    font-size: 1.8rem;
  }

  .study-date {
    font-size: 1.8rem;
  }

  h4.panel-title,
  .study-outcome,
  .study-date {
    @media (max-width: 700px) {
      text-align: left;
      font-size: 1.4rem;
    }
  }

  .col-md-1.col-sm-2.col-xs-4.border-box {
    border-right: 1px solid grey;
    background: #eee;
    display: flex;
    justify-content: center;
    align-items: center;

    p {
      margin-bottom: 0;
    }
  }

  .col-md-11.col-sm-10.col-xs-8.border-box {
    /* padding-left: 1rem; */
    padding: 2rem;
    background: #eee;
  }
  #results-wrap ul li a,
.related-content ul li a,
.related-content,
/* desktop navbar */
.full-panel,
#menu ul,
/* mobile dropdown menu */
ul#menu li a {
    box-sizing: content-box;
  }

  /* override bootstrap breadcrumb background */
  .breadcrumb {
    background-color: unset !important;
    padding: 0 !important;
  }

  .study-number-positive {
    font-weight: 700;
    color: #d9534f !important;
  }

  .study-number-negative {
    font-weight: 700;
    color: #5cb85c !important;
  }

  .study-name,
  .study-name:hover {
    color: #437092 !important;
    transition: color 0.15s;
  }

  .study-name:hover i {
    transition: color 0.15s;
    color: #66a9e4;
  }

  .study-expand {
    font-size: 34px;
    position: absolute;
    right: 20px;
    top: 10px;
  }

  .author-row div p {
    padding: 0 !important;
  }

  #content > h3 {
    border-bottom: 1px solid black;
    border-top: 1px solid black;
    padding: 10px 0;
    margin-top: 30px;
    margin-bottom: 15px;
    font-size: 22px;
  }

  #content .content-image {
    width: 30%;
    padding: 0 0 15px 15px;
  }

  .page-navigation p {
    padding: 0 !important;
    font-weight: 600;
    text-decoration: underline;
    cursor: pointer;
    color: #2a699d;
  }

  .page-navigation p:hover {
    color: #0e395d;
  }

  .page-navigation .indented p {
    padding: 0 0 0 20px !important;
    font-weight: 400;
  }

  .scroll-to-top {
    cursor: pointer;
    margin: 10px 0 20px;
    border: 1px solid #888;
    width: 115px;
    padding: 10px;
    border-radius: 5px;
    background-color: #eaf5ff;
  }

  .scroll-to-top:hover {
    background-color: #becbd6;
  }

  #expand-row {
    margin: 20px auto;
  }

  #expand-row button {
    display: block;
    margin: auto;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
