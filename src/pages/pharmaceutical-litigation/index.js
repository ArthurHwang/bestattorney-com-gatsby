// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Pharmaceutical Litigation - Defective Drug Lawyers"
        pageDescription="California defective drug lawyer that offer a no win, no fee promise and free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Defective Drug & Pharmaceutical Litigation Lawyer</h1>
        <BreadCrumbs location={location} />
        <h2>Now Taking Talcum Powder Cases!</h2>
        <p>
          Recent evidence has shown that continued use of Talcum Powder can
          significantly increase your risk of ovarian cancer! Bisnar Chase is
          now signing these cases and taking the fight to Johnson & Johnson.
        </p>
        <p>
          {" "}
          <Link
            className="btn btn-primary btn-lg"
            to="/pharmaceutical-litigation/talcum-powder"
            role="button"
          >
            Do I have a Talcum Powder Case?
          </Link>
        </p>
        <p>
          {" "}
          <Link
            className="btn btn-default btn-lg"
            to="/pharmaceutical-litigation/list-of-talcum-powder-studies"
            role="button"
          >
            Review Evidence from Talcum Powder Studies
          </Link>
        </p>

        <div
          className="text-header content-well"
          title="Red Pills"
          style={{
            backgroundImage: "url('/images/text-header-images/pills.jpg')"
          }}
        >
          <h2>Dangerous Drugs and Drug Recalls</h2>
        </div>

        <div className="algolia-search-text">
          <div className="content-well content-well-margin-bottom">
            <p>
              Defective drugs and over-the-counter medications can cause
              life-changing side effects. Large pharmaceutical companies often
              rush new drugs into the marketplace. Some medications are sold
              despite lack of proper testing or a comprehensive analysis of
              potential side effects. In some cases, it is discovered that drug
              companies knew of possible adverse reactions that they did not
              disclose to the public. As drug manufacturers rake in profits, the
              consumers are left with serious medical conditions with which they
              may have to deal for the rest of their lives. When drug companies
              manufacture and distribute dangerous products, it is important
              that they are held responsible for their wrongdoing.
            </p>

            <p>
              Many drugs are allowed in the marketplace despite having known,
              inherent dangers. Some may even be defective drugs. Drugs that
              have negative consequences that outweigh their effectiveness are
              often recalled. In the year 2009, the FDA reported over 1,742 drug
              recalls. A significant number of those recalls were related to
              manufacturing lapses, a low quality of raw materials, defective
              labeling and contamination.
            </p>
          </div>

          <LazyLoad>
            <div
              className="text-header content-well"
              title="Bisnar Chase Staff"
              style={{
                backgroundImage:
                  "url('/images/text-header-images/bisnar-chase.jpg')"
              }}
            >
              <h2>Drug Cases that Bisnar Chase Represents</h2>
            </div>
          </LazyLoad>

          <div className="content-well content-well-margin-bottom">
            <p>
              All drugs have the potential to cause harm, but certain popular
              medications are known for adverse side effects and negative
              consequences. Some of the most dangerous drugs that have been
              recently involved in pharmaceutical litigation include:
            </p>
            <ul>
              <li>
                <strong>
                  {" "}
                  <Link to="/pharmaceutical-litigation/accutane">
                    Accutane:
                  </Link>
                </strong>{" "}
                It has long been known that this powerful acne medication can
                cause liver damage and birth defects, but it has only become
                apparent in recent years that teenagers on Accutane can suffer
                psychiatric distress, depression and suicidal thoughts.
              </li>
              <li>
                <strong>
                  {" "}
                  <Link to="/pharmaceutical-litigation/talcum-powder">
                    Talcum Powder:
                  </Link>
                </strong>{" "}
                Talcum Powder is often used as baby powder to prevent rashes and
                to get rid of moisture and friction in the genital area.
                However, recent evidence shows that continued use of talcum
                powder over 4 years can significantly increase one's risk of
                ovarian cancer. Johnson & Johnson has hundreds of cases brought
                against them for allowing their dangerous products to continue
                to be sold.
              </li>
              <li>
                <strong>
                  {" "}
                  <Link to="/pharmaceutical-litigation/xarelto">Xarelto: </Link>
                </strong>
                A blood thinner used to prevent blood clots in the lungs and to
                prevent strokes. It has several other uses but the side effects
                have been linked to uncontrollable bleeding causing death in
                some patients. Once major internal hemorrhaging begins, there is
                no known cure to fix it, and the bleeding can be fatal.
              </li>
              <li>
                <strong>
                  {" "}
                  <Link to="/pharmaceutical-litigation/zofran-lawyers">
                    Zofran:
                  </Link>
                </strong>{" "}
                An anti nausea medication that was illegally marketed off label
                to pregnant women, who gave birth to deformed babies.
              </li>
              <li>
                <strong>
                  {" "}
                  <Link to="/pharmaceutical-litigation/low-t-lawyers">
                    Low Testosterone Therapy:
                  </Link>{" "}
                </strong>{" "}
                Low Testosterone Therapy is administered either by pill or
                injection, and is used to increase testosterone levels in men to
                treat symptoms such as depressions, decreased muscle mass, low
                sex drive, and erectile dysfunction. These treatments however
                come with a high risk of heart attack, stroke, and blood clots,
                despite being marketed as safe.
              </li>
              <li>
                <strong> Cipro:</strong> Cipro (Ciprofloxacin), along with
                others in the same class of antibiotics - fluoroquinolones, is
                often used to treat respiratory and urinary tract infections.
                However, use of fluoroquinolones has been accompanied by an
                increased risk of tendinitis and tendon rupture, especially in
                those who are over 60 or who already have tendon inflammation.
                The likelihood increases if the patient is on concomitant
                steroid therapy or has received an organ transplant. The huge
                influx of tendon rupture reports after taking Cipro pushed the
                FDA to demand that all fluoroquinolones carry "black box
                warning" on the packaging about the dangers of tendon ruptures
                while taking the medicine.
              </li>
              <li>
                <strong> Levaquin:</strong> Levaquin, like Ciprofloxacin, is
                another drug in the fluoroquinolones family. Levaquin also
                increases the risk of tendinitis and tendon rupture, especially
                in the achilles tendon. Levaquin has also led to a disorder
                called "Stevens-Johnson Syndrome," a skin disorder that
                originally starts with flu-like symptoms, but develops into
                rashes and blisters when cell death leads to the separation of
                the epidermis and the dermis skin layers.
              </li>
              <li>
                <strong> Plavix:</strong> Plavix prevents the platelets in the
                blood from clotting and is usually prescribed to patients who
                have had a recent heart attack or stroke, to prevent their blood
                from coagulating and causing more health issues. Unfortunately,
                taking Plavix comes with an increased risk of gastrointestinal
                and cerebral hemorrhaging, both of which can be life
                threatening. Plavix also increases the risk of Thrombotic
                Thrombocytopenic Purpura, a disorder where microscopic blood
                clots form and damage internal organs, often leading to
                neurological symptoms and kidney failure at the worst.
              </li>
              <li>
                <strong> Reglan:</strong> Reglan (Metoclopramide) is prescribed
                to treat heartburn when other heartburn medications haven't
                worked. It does this by increasing muscle contractions in the
                digestive tract, which then helps the stomach empty into the
                intestines, thereby preventing stomach acid from regurgitating
                into the esophagus. Reglan also has a potential side effect of
                causing Tadrive Dyskinesia disorder - a neurological disorder
                that causes repetitive involuntary movements like eye twitching
                or sticking the tongue out, as well as rapid arm or leg
                movement. In some cases, these symptoms persist indefinitely
                after Reglan use is discontinued.
              </li>
              <li>
                <strong> Zometa:</strong> Zometa is a biophosphonate - a class
                of drug used to treat both high levels of calcium in the blood
                usually caused by cancer, and osteoperosis for postmenopausal
                women. Zometa and other Biophosphonates also has a high number
                of potential dangerous side-effects, including Osteonecrosis of
                the Jaw (Dead Jaw Syndrome), femur fractures, esophageal cancer,
                and atrial fibrillation.
              </li>
            </ul>
          </div>

          <p>
            If you've been a victim of any of the side effects from these drugs,
            read on to see if you have a case!
          </p>
        </div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Brian Chase Pointing"
            style={{
              backgroundImage:
                "url('/images/text-header-images/brian-pointing.jpg')"
            }}
          >
            <h2>Do You Have a Case?</h2>
          </div>
        </LazyLoad>
        <div className="content-well content-well-margin-bottom">
          <LazyLoad>
            <img
              src="/images/pharmaceutical-litigation-cup.jpg"
              alt="a defective drug sits by a glass of water"
              className="imgright-fixed"
              height="150"
              width="150"
            />
          </LazyLoad>

          <p>
            If you've experienced more medical problems from any of the above
            drugs, you may be able to seek support through pharmaceutical
            litigation. There are a number of factors that will determine
            whether or not you have a case:
          </p>
          <ul>
            <li>
              Were you using the drug as prescribed? Failing to follow the
              directions listed can hurt your chances of receiving compensation.
            </li>
            <li>
              Were you taking the medication for the right reasons? Taking a
              drug for purposes not approved by the Food and Drug Administration
              (FDA) limits your legal options.
            </li>
            <li>
              Did you experience an adverse reaction that was not noted on the
              packaging? This is why it is important to ask your doctor about
              the possible side effects of drugs and to read{" "}
              <Link to="/defective-products/failure-to-warn">
                warning labels
              </Link>{" "}
              before taking a new drug.
            </li>
            <li>
              Have you suffered financial or physical losses because you took
              the drug? Have you required medical attention or missed work
              because of your reaction to the drug? These are the types of
              losses that can be covered through litigation.
            </li>
            <li>
              Did the drug company knowingly fail to notify consumers and
              doctors of the possible side effects? That is a form of negligence
              that can make them liable for their product.
            </li>
          </ul>
        </div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="The Importance of Pharmaceutical Litigation"
            style={{
              backgroundImage: "url('/images/text-header-images/gavel.jpg')"
            }}
          >
            <h2>The Importance of Pharmaceutical Litigation</h2>
          </div>
        </LazyLoad>

        <div className="content-well content-well-margin-bottom">
          <p>
            It is necessary and important to expose drug companies when they
            knowingly put consumers in danger. Furthermore, pharmaceutical
            litigation is often the only way for injured victims to receive the
            justice and financial support they need. Unsafe medications can
            cause severe medical conditions such as heart attacks, strokes,
            liver damage, bone density issues and even death. A successful
            lawsuit against a drug manufacturer can result in financial
            compensation for all of the resulting medical bills and other
            related damages suffered by victims and their families.
          </p>
        </div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Receiving Financial Support"
            style={{
              backgroundImage:
                "url('/images/text-header-images/giving-money.jpg')"
            }}
          >
            <h2>Receiving Financial Support</h2>
          </div>
        </LazyLoad>
        <div className="content-well content-well-margin-bottom">
          <p>
            There are legal options available for victims of dangerous drugs. If
            you are experiencing unforeseen side effects, please contact your
            doctor right away. It may be advisable to preserve the remaining
            prescription drugs and to keep records of the losses you have
            suffered since becoming ill. Check the U.S.{" "}
            <Link to="http://www.fda.gov">Food and Drug Administration</Link>{" "}
            (FDA) web site to see if the drug you were given has been the
            subject of a recall or warning.
          </p>
          <p>
            The experienced California pharmaceutical litigation lawyers at
            Bisnar Chase have represented those who have been seriously injured
            as the result of defective drugs. We have what it takes to fight
            against large corporations and help injured victims and their
            families secure fair and full compensation for their significant
            losses. Please contact us at 949-203-3814 for a free consultation
            and comprehensive case evaluation.
          </p>
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .content-well-margin-bottom {
    p:last-child {
      margin-bottom: 0;
    }
    ul {
      margin-bottom: 0;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
