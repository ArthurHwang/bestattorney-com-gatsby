// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Talcum Powder Lawyers - Ovarian Cancer Attorney"
        pageDescription="Contact our talcum powder ovarian cancer lawyers for a free consultation. If you've used baby powder and have ovarian cancer you may be entitled to compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Talcum Powder Ovarian Cancer Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            If you've received a diagnosis of ovarian cancer and have used baby
            powder you may be entitled to compensation. The talcum powder
            ovarian cancer lawyers of Bisnar Chase are actively taking these
            cases nationwide. We've assembled a team of trial lawyers to protect
            those who've been injured from this dangerous product from Johnson &
            Johnson. Call 1-800-561-4887 for a free consultation.
          </p>
          <p>
            Talcum Powder, more frequently referred to as baby powder, has
            recently come under fire for increasing the risk of ovarian cancer
            for women who use it regularly for feminine hygiene. Talcum powder
            and its inherent risks have been studied since 1982, at which point
            a link was suggested between{" "}
            <Link
              to="http://www.cancer.org/cancer/cancercauses/othercarcinogens/athome/talcum-powder-and-cancer"
              target="_blank"
            >
              {" "}
              ovarian cancer and genital talcum powder use
            </Link>
            . This research however was widely ignored by Johnson & Johnson
            (J&J), the primary manufacturer of baby powder product made with
            talc. As of 2016, Johnson & Johnson as been subject to 2 verdicts of
            $72 million and $55 million for not warning its consumers about the
            dangers of ovarian cancer when using their products. Bisnar Chase
            are talcum powder attorneys who have studied these cases and the
            effects of talcum powder extensively in order to represent victims
            who have developed ovarian cancer as a result of their baby powder
            use.
          </p>
        </div>
        <div className="page-navigation">
          <div className="well well-lg">
            <h2>Talcum Powder: Contents</h2>{" "}
            <Link
              className="topic"
              data-elementid="header1"
              to="/pharmaceutical-litigation/talcum-powder#header1"
            >
              1. Talcum Powder Overview
            </Link>
            <div className="indented">
              <p>
                {" "}
                <Link
                  data-elementid="header2"
                  to="/pharmaceutical-litigation/talcum-powder#header2"
                >
                  1-1. What is Talcum Powder?
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header3"
                  to="/pharmaceutical-litigation/talcum-powder#header3"
                >
                  1-2. How is Talcum Powder Used?
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header4"
                  to="/pharmaceutical-litigation/talcum-powder#header4"
                >
                  1-3. How does Talcum Powder Increase the Risk of Cancer?
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header5"
                  to="/pharmaceutical-litigation/talcum-powder#header5"
                >
                  1-4. What if I Use Talcum Powder?
                </Link>
              </p>
            </div>{" "}
            <Link
              className="topic"
              data-elementid="header6"
              to="/pharmaceutical-litigation/talcum-powder#header6"
            >
              2. Ovarian Cancer
            </Link>
            <div className="indented">
              <p>
                {" "}
                <Link
                  data-elementid="header7"
                  to="/pharmaceutical-litigation/talcum-powder#header7"
                >
                  2-1. What is Ovarian Cancer?
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header8"
                  to="/pharmaceutical-litigation/talcum-powder#header8"
                >
                  2-2. What are the Symptoms?
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header9"
                  to="/pharmaceutical-litigation/talcum-powder#header9"
                >
                  2-3. Who is At Risk?
                </Link>
              </p>
            </div>{" "}
            <Link
              data-elementid="header10"
              className="topic"
              to="/pharmaceutical-litigation/talcum-powder#header10"
            >
              3. Johnson & Johnson Lawsuits
            </Link>
            <div className="indented">
              <p>
                {" "}
                <Link
                  data-elementid="header11"
                  to="/pharmaceutical-litigation/talcum-powder#header11"
                >
                  3-1. Jacqueline Fox
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header12"
                  to="/pharmaceutical-litigation/talcum-powder#header12"
                >
                  3-2. Gloria Ristesund
                </Link>
              </p>
              <p>
                {" "}
                <Link
                  data-elementid="header13"
                  to="/pharmaceutical-litigation/talcum-powder#header13"
                >
                  3-3. Other Lawsuits
                </Link>
              </p>
            </div>
            <p>
              {" "}
              <Link
                data-elementid="header14"
                to="/pharmaceutical-litigation/talcum-powder#header14"
              >
                4. List of Scientific Studies
              </Link>
            </p>
            <p>
              {" "}
              <Link
                data-elementid="header17"
                to="/pharmaceutical-litigation/talcum-powder#header17"
              >
                5. Do You Have A Case?
              </Link>
            </p>
          </div>
        </div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Talcum Powder Overview"
            style={{
              backgroundImage:
                "url('/images/text-header-images/talc-powder.jpg')"
            }}
          >
            <h2 id="header1">Talcum Powder Overview</h2>
          </div>
        </LazyLoad>
        <h3 id="header2">What is Talcum Powder?</h3>
        <p>
          Talcum Powder is a crushed form of Talc - a magnesium silicate that
          happens to be the softest known mineral. As such it has an innate
          ability to absorb moisture and oils. These properties make it useful
          as baby powder to prevent diaper rash or for deodorizing. In 2011, the
          United States produced about 615,000 metric tons of talc. The majority
          of this is used in plastics and ceramic applications, but 7% was used
          to make baby powder and other cosmetics.
        </p>
        <p>
          In its natural state talc contains asbestos, which is a known
          carcinogen. However, in 1973, federal law has prohibited the use of
          asbestos in consumer product containing talc. Talcum powder is now
          processed to ensure that there is no asbestos in the product.
        </p>
        <p>
          Talc can be found in Johnson & Johnson Baby Powder and Johnson &
          Johnson Shower to Shower Body Powder, as well as in other cosmetics
          and personal care products.
        </p>
        <h3 id="header3">How is Talcum Powder Used?</h3>
        <h4>For Babies:</h4>
        <p>
          Baby Powder is primarily used to prevent diaper rash by sprinkling the
          powder into the diaper. This keeps the baby's bottom dry and reduces
          friction between the diaper and the skin, which in turn reduces the
          likelyhood of a rash.
        </p>
        <h4>For Adults:</h4>
        <p>
          Talcum Powder has commonly been used by adult women to dust their
          genitals or their sanitary napkins with talcum powder to keep the area
          moisture and odor-free. This continued usage of talcum powder on the
          genitals is what is believed to have caused ovarian cancer in
          thousands of women today.
        </p>
        <h3 id="header4">
          How does Talcum Powder Increase the Risk of Cancer?
        </h3>
        <p>
          Existing research is conflicted on the correlation between talcum
          powder usage and ovarian cancer risk, but studies have confirmed that
          talcum powder particles were found in a large number of ovarian
          tumors. The general consensus is that talcum powder, when regularly
          applied, can travel through the vagina, uterus and fallopian tubes
          into the ovaries.
        </p>
        <p>
          Talc is often used in surgical efforts to prevent{" "}
          <Link to="https://en.wikipedia.org/wiki/Pneumothorax" target="_blank">
            {" "}
            pneumonia,
          </Link>{" "}
          a condition where the lungs uncouple themselves from the chest wall.
          The talc has a inflammatory effect which causes the lungs to adhere to
          the chest wall. It is thought that this inflammatory effect also takes
          place within the ovaries after regular usage, inflaming the ovaries
          and opening them up to increased ovarian cancer risk. For a long time
          the scientific community has observed that long-term inflammation can
          contribute to cancer, as discussed in this{" "}
          <Link
            to="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1994795/"
            target="_blank"
          >
            {" "}
            Yale Department of Immunobiology Study.
          </Link>{" "}
          Experts believe that long-term inflammation in the ovaries can damage
          DNA, and that if any low-risk cancerous cells were to appear, the
          inflamed immune system would not be as effective at preventing the
          growth of these cells, thereby increasing the risk of cancerous
          growth.
        </p>
        <h3 id="header5">What if I Use Talcum Powder?</h3>
        <p>
          Ovarian cancer will affect approximately 1 in 70 women in their
          lifetimes, but preliminary studies show that talcum powder is only
          likely to increase your chances of getting cancer if you consistently
          use the powder for multiple years. If you use talcum powder
          consistently, we recommend switching to a{" "}
          <Link
            to="https://www.johnsonsbaby.com/powder/johnsons-baby-pure-conrnstarch-powder-with-magnolia-petals"
            target="_blank"
          >
            {" "}
            corn-starch based powder
          </Link>
          , which has no disadvantages over the talc. It may also be wise to
          keep an eye out for signs of ovarian cancer.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Ovarian Cancer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/ovarian-cancer.jpg')"
            }}
          >
            <h2 id="header6">Ovarian Cancer</h2>
          </div>
        </LazyLoad>
        <h3 id="header7">What is Ovarian Cancer?</h3>
        <p>
          Women diagnosed with ovarian cancer are found to have abnormal cells
          growing out-of-control in the ovaries. There are several types of
          ovarian cancer.
        </p>
        <ul>
          <li>
            <h4>Epithelial Ovarian Cancer</h4>

            <p>
              Most tumors in the ovaries are considered to be epithelial. This
              type of cancer starts to grow in the surface layer tissue, or the
              epithelium, of the ovaries. This type of cancer makes up 90% of
              the ovarian cancer cases, but many of these cases end up being
              benign and pose no risk to the patient. Epithelial tumors can be
              further split into the following sub-categories:
            </p>
            <ol>
              <li>
                <h5>Serous</h5>
                <p>
                  This is the most common and the most dangerous form of
                  epithelial ovarian cancer, due to spreading quickly and
                  usually becoming symptomatic after becoming a high stage
                  tumor. This results in the tumor not being discovered until it
                  is often too late.
                </p>
              </li>
              <li>
                <h5>Clear Cell</h5>
                <p>
                  This is the second most common type of epithelial ovarian
                  cancer and is believed to come about due to endometriosis. It
                  is usually detected at low stages, but doesn't necessarily
                  respond as well to chemotherapy with current methods.
                </p>
              </li>
              <li>
                <h5>Endometrioid</h5>
                <p>
                  The third most common, also believed to result from
                  endometriosis. When patients are diagnosed at a low stage,
                  they only require surgery to remove tumors and no additional
                  treatment after that.
                </p>
              </li>
              <li>
                <h5>Mucinous</h5>
                <p>
                  This is an uncommon type of carcinoma relatively unresponsive
                  to chemotherapy.
                </p>
              </li>
              <li>
                <h5>Unclassifiable</h5>
                <p>
                  A small percentage of epithelial tumors are unable to be
                  classified due to immature development of the cells.
                </p>
              </li>
            </ol>
          </li>

          <li>
            <h4>Germ Cell Ovarian Cancer</h4>
            <p>
              These tumors grow in the cells that produce the eggs within the
              ovaries. Unlike epithelial cancer, which occurs most frequently in
              women over 40, germ cell tumors often occur in teenagers and women
              in their 20s. Most of these patients can be cured and still
              maintain fertility if the tumor is found early enough.
            </p>
          </li>
          <li>
            <h4>Stromal Tumors</h4>
            <p>
              Stromal tumors in the ovaries affect the soft connective tissue
              that hold the ovaries together. These can either be classified as
              granulosa tumors or Sertoli Leydig tumors. These tumors are rare
              and are usually considered to be low grade cancers.
            </p>
          </li>
        </ul>
        <blockquote>
          Tumor information was found from several different resources,
          including{" "}
          <Link
            to="http://www.cancerresearchuk.org/about-cancer/type/ovarian-cancer/about/types-of-ovarian-cancer"
            target="_blank"
          >
            {" "}
            Cancer Research UK
          </Link>
          ,{" "}
          <Link
            to="http://www.ovarian.org/types_and_stages.php"
            target="_blank"
          >
            {" "}
            Ovarian.org
          </Link>
          , and{" "}
          <Link to="http://www.cancer.gov/types/ovarian" target="_blank">
            {" "}
            Cancer.gov.
          </Link>
        </blockquote>
        <h3 id="header8">What are the symptoms of Ovarian Cancer?</h3>
        <p>
          The symptoms are vague and not specific to ovarian cancer, but will
          worsen over time as the cancer grows. Be sure to see a doctor if you
          continue to experience these symptoms:
        </p>
        <ul>
          <li>Pelvic or Abdominal Pain</li>
          <li>Back Pain</li>
          <li>Chronic Fatigue</li>
          <li>Bloating/Indigestion</li>
          <li>Difficulty Eating</li>
          <li>Frequent Urination</li>
          <li>Abnormal Vaginal Discharge</li>
        </ul>
        <p>
          Even early stage cancer can reveal itself with these symptoms
          constantly showing themselves. If you seek a gynecologic evaluation
          quickly for a pelvic and rectovaginal exam, you may be able to detect
          cancer at an early stage and improve the likelihood of a good outcome.
        </p>
        <h3 id="header9">Who is At Risk?</h3>
        <p>
          90% of women who get ovarian cancer are older than 40, but there are
          certain factors that correlate more highly with ovarian cancer
          incidences. If any of these situations apply to you, it does not mean
          that you will get ovarian cancer, but you may want to speak to your
          doctor about the risk.
        </p>
        <ul>
          <li>Older than 40</li>
          <li>Other Ovarian or Breast Cancer in the Family</li>
          <li>Never Given Birth</li>
          <li>Difficulty Getting Pregnant</li>
          <li>Taken Hormone Replacement Therapy</li>
          <li>
            History of{" "}
            <Link
              to="http://www.medicinenet.com/endometriosis/article.htm"
              target="_blank"
            >
              {" "}
              Endometriosis
            </Link>
          </li>
          <li>
            Posses BRCA1 or BRCA2 gene mutations (Mostly found in people of
            Eastern European Jewish descent)
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Johnson & Johnson Cancer Lawsuits"
            style={{
              backgroundImage: "url('/images/text-header-images/j&j.jpg')"
            }}
          >
            <h2 id="header10">Johnson & Johnson Cancer Lawsuits</h2>
          </div>
        </LazyLoad>
        <h3 id="header11">$72 Million for Jacqueline Fox</h3>
        <LazyLoad>
          <img
            src="/images/jacqueline-fox.jpg"
            className="imgleft-fixed"
            alt="Jacqueline Fox Died of Ovarian Cancer in October 2015"
          />
        </LazyLoad>
        <p>
          Jackie Fox passed away from ovarian cancer in October 2015, but not
          before filing a case against Johnson & Johnson for failing to warn her
          about the documented dangers of using talcum powder. After her death
          her son, Marvin Salter, took over as plaintiff for the three-week
          trial in February 2016. The jury awarded Fox $10 million in actual
          damages and $62 million in punitive damages, although it is expected
          that an appeal will reduce this punitive amount considerably.
        </p>
        <p>
          Juries were shown an internal document from Johnson & Johnson where a
          medical consultant alleged that use of talcum powder came with a
          higher risk of ovarian cancer, and likened it to the link between
          cigarettes and cancer. The fact that Johnson & Johnson was warned
          about the potential dangers of talc and did not put a warning on the
          bottle or do anything else to take care of their consumers' health was
          what swayed at least one of the members of the jury. A spokesperson
          for Johnson & Johnson said that there are decades of science proving
          that talc usage is safe. Unfortunately, given the existence of
          multiple conflicting studies, it seems as if the company picked and
          chose what science it wanted to believe and used that as justification
          for not warning consumers.
        </p>
        <h3 id="header12">$55 Million for Gloria Ristesund</h3>
        <p>
          <LazyLoad>
            <img
              className="imgleft-fixed"
              src="/images/gloria-ristesund.jpg"
              alt="Gloria Ristesund was awarded $55 million as a result of Johnson & Johnson's Negligence."
            />
          </LazyLoad>
          Gloria Ristesund was also a frequent user of Johnson & Johnson brand
          talcum powder from 1973 to 2011, and was diagnosed with ovarian cancer
          in august 2011. As a result of her cancer she had to have a
          hysterectomy and other surgeries. Fortunately her cancer is now in
          remission, but the damage to her body has already been done. Gloria
          filed a lawsuit which was closed early May 2016 when the judge ordered
          Johnson & Johnson to pay $55 million, $5 million in actual damages and
          $50 million in punitive damages.
        </p>
        <h3 id="header13">Other Lawsuits</h3>
        <p>
          There are now more than 1200 other lawsuits against Johnson & Johnson
          for the same reason - that despite the fact that Johnson & Johnson was
          given fair warning from multiple studies that concluded a link between
          talcum powder use in the genitals and ovarian cancer, they did not
          replace the talcum powder with corn starch in their baby powder
          product, or even place a warning on the bottle that alerted its users
          to the danger.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="List of Scientific Studies"
            style={{
              backgroundImage: "url('/images/text-header-images/science.jpg')"
            }}
          >
            <h2 id="header14">List of Scientific Studies</h2>
          </div>
        </LazyLoad>
        <p>
          Starting in 1971, numerous scientific studies have been performed to
          determine if talcum powder use increased the likelihood of ovarian
          cancer. We curated a list of these studies and descriptions of their
          processes, and 15 out of the 20 studies that were performed found an
          association between genital talcum powder use and an increase in
          ovarian cancer. You can read the{" "}
          <Link to="/pharmaceutical-litigation/list-of-talcum-powder-studies">
            summarized descriptions and results of the talcum / baby powder
            studies here
          </Link>
          , as well as study takeaways such as other risk factors of ovarian
          cancer that were discovered from these studies.
        </p>
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <td>#</td>
              <td>Study Name</td>
              <td>Association Found?</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1.</td>
              <td>Talc and Carcinoma of the Ovary and Cervix</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Ovarian Cancer and Talc: A Case-Control Study</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>3.</td>
              <td>
                A Case-Control Study of Borderline Ovarian Tumors: The Influence
                of Perineal Exposure to Talc
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>4.</td>
              <td>
                Personal and Environmental Characteristics Related to Epithelial
                Ovarian Cancer
              </td>
              <td>No</td>
            </tr>
            <tr>
              <td>5.</td>
              <td>Risk Factors for Ovarian Cancer: A Case-Control Study</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>6.</td>
              <td>
                Risk Factors for Epithelial Ovarian Cancer in Beijing, China
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>7.</td>
              <td>
                Reproductive and Other Factors and Risk of Epithelial Ovarian
                Cancer: An Australian Case-Control Study. Survey of Women's
                Health Study Group.
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>8.</td>
              <td>Perineal Talc Exposure and Risk of Ovarian Carcinoma</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>9.</td>
              <td>Genital Talc Exposure and Risk of Ovarian Cancer</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>10.</td>
              <td>
                Perineal Talc Exposure and Subsequent Epithelial Ovarian Cancer:
                A Case-Control Study
              </td>
              <td>No</td>
            </tr>
            <tr>
              <td>11.</td>
              <td>Prospective Study of Talc Use and Ovarian Cancer</td>
              <td>No</td>
            </tr>
            <tr>
              <td>12.</td>
              <td>
                Factors Related to Inflammation of the Ovarian Epithelium and
                Risk of Ovarian Cancer
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>13.</td>
              <td>
                Perineal Talc Exposure and Epithelial Ovarian Cancer Risk in the
                Central Valley of California
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>14.</td>
              <td>
                Talcum Powder, Chronic Pelvic Inflammation and NSAIDs in
                Relation to Risk of Epithelial Ovarian Cancer
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>15.</td>
              <td>
                Talc Use, Variants of the GSTM1, GSTT1, and NAT2 Genes, and Risk
                of Epithelial Ovarian Cancer
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>16.</td>
              <td>
                Markers of Inflammation and Risk of Ovarian Cancer in Los
                Angeles County
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>17.</td>
              <td>
                Genital Powder Exposure and the Risk of Epithelial Ovarian
                Cancer
              </td>
              <td>No</td>
            </tr>
            <tr>
              <td>18.</td>
              <td>
                Genital Powder Use and Risk of Ovarian Cancer: A Pooled Analysis
                of 8,525 cases and 9,859 Controls
              </td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
            <tr>
              <td>19.</td>
              <td>Perineal Powder Use and Risk of Ovarian Cancer</td>
              <td>No</td>
            </tr>
            <tr>
              <td>20.</td>
              <td>The Association Between Talc Use and Ovarian Cancer</td>
              <td>
                <strong> Yes </strong>
              </td>
            </tr>
          </tbody>
        </table>{" "}
        <p>
          <Link
            className="btn btn-lg btn-primary"
            to="/pharmaceutical-litigation/list-of-talcum-powder-studies.html"
          >
            See Full List of Studies and Descriptions
          </Link>
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Do You Have A Case"
            style={{
              backgroundImage:
                "url('/images/text-header-images/brian-pointing.jpg')"
            }}
          >
            <h2 id="header17">Do You Have A Case?</h2>
          </div>
        </LazyLoad>
        <p>
          If you have been diagnosed with ovarian cancer and have frequently
          used talcum powder products for feminine hygiene, it is important that
          you contact an experienced talcum powder attorney to explore your
          options. The value or worth of your case will depend on your diagnosis
          and treatment as well as the economic and non-economic damages you
          have sustained as a result.
        </p>
        <p>
          Affected victims may be able to seek compensation for damages such as
          medical expenses, lost wages, hospitalization, cost of surgery,
          rehabilitative treatment, pain and suffering and emotional distress.
          Those who have lost a loved one because of ovarian cancer linked to
          talcum powder may also seek compensation by filing a wrongful death
          lawsuit.
        </p>
        <p>
          The talcum powder attorneys at Bisnar Chase are committed and
          dedicated to assisting individuals who have been injured by defective
          products or families that have lost loved ones to dangerous products.
          We have the resources it takes to fight for our clients, help them
          receive maximum compensation for their losses and hold manufacturers
          accountable for their negligence. We don't charge any fees or costs
          until we get you compensated. <Link to="/contact">Contact us</Link> at
          1-800-561-4887 for a free, comprehensive and confidential
          consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  td:first-child {
    padding: 8px;
  }
  .topic {
    font-size: 2rem;
  }

  .study-number-positive {
    font-weight: 700;
    color: #d9534f !important;
  }

  .study-number-negative {
    font-weight: 700;
    color: #5cb85c !important;
  }

  .study-name,
  .study-name:hover {
    color: #437092 !important;
    transition: color 0.15s;
  }

  .study-name:hover i {
    transition: color 0.15s;
    color: #66a9e4;
  }

  .study-expand {
    font-size: 34px;
    position: absolute;
    right: 20px;
    top: 10px;
  }

  .author-row div p {
    padding: 0 !important;
  }

  #content > h3 {
    border-bottom: 1px solid black;
    border-top: 1px solid black;
    padding: 10px 0;
    margin-top: 30px;
    margin-bottom: 15px;
    font-size: 22px;
  }

  #content .content-image {
    width: 30%;
    padding: 0 0 15px 15px;
  }

  .page-navigation p {
    padding: 0 !important;
    font-weight: 600;
    text-decoration: underline;
    cursor: pointer;
    color: #2a699d;
  }

  .page-navigation p:hover {
    color: #0e395d;
  }

  .page-navigation .indented p {
    padding: 0 0 0 20px !important;
    font-weight: 400;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
