// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Low T Lawyers - Testosterone Therapy Attorneys"
        pageDescription="California Low T Attorneys. Call 800-561-4887 for highest-rated pharmaceutical litigation lawyers specializing in Low-T cases."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Low-T Lawyer</h1>
        <BreadCrumbs location={location} />
        <h3 className="mb">
          Please note: Bisnar Chase is no longer taking Low-T cases and would
          refer you to Avvo.com to find a qualified Low-T Attorney. This page is
          for informational purposes only.
        </h3>
        <h2>
          <strong> What is Your Case Worth?</strong>
        </h2>
        <p>
          The value of your case will depend on a number of factors including
          the nature of the harm done by the testosterone product, the severity
          of the damage and the financial impact you have suffered as a result
          of such harm. Here are some of the factors that are usually taken into
          consideration while evaluating your testosterone therapy case:
        </p>
        <ul type="disc">
          <li>The medical expenses incurred as a result of the injury.</li>
          <li>
            The effect the injury on the physical and emotional health of the
            plaintiff.
          </li>
          <li>The physical pain and suffering endured by the plaintiff.</li>
          <li>Any future treatment costs caused by the testosterone use.</li>
          <li>Lost wages or loss of earning capacity.</li>
          <li>
            If the victim has passed away, the losses sustained by the surviving
            family members such as loss of love, care and companionship.
          </li>
        </ul>
        <h2>Low-T Therapy Treatment Dangers</h2>
        <p>
          Testosterone replacement therapy is used to treat men who have low
          levels of the male hormone, testosterone. This is a hormone that
          naturally occurs in the body. Low testosterone can cause symptoms such
          as depression, low sex drive, erectile dysfunction, fatigue, decreased
          muscle mass and difficulty concentrating. Low testosterone therapy can
          be administered through injections, oral pills/tablets, patches, or
          topical gels. Some of the commonly used low-testosterone therapy drugs
          include AndroGel, Androderm, Testim, Fortesta, Axiron, Striant and
          Testopel to mention a few.
        </p>
        <h2>What Are The Risks of Low-T Therapy?</h2>
        <p>
          Testosterone therapy does help men with low levels of the hormone, but
          it also comes with the risk of serious and potentially
          life-threatening health effects such as heart attack, stroke and blood
          clots. Drug companies that make these types of drugs{" "}
          <strong>
            {" "}
            aggressively market them promising men a better quality of life
          </strong>
          . It is also common for manufacturers of testosterone replacement
          drugs to target male consumers with television commercials to air
          during football games and other shows that are likely to be watched by
          men.
        </p>
        <p>
          In reality,{" "}
          <strong>
            {" "}
            testosterone therapy can cause a number of serious health
            complications
          </strong>{" "}
          for men that might actually outweigh the benefits. A number of men are
          filing lawsuits against the manufacturers of testosterone products
          claiming that the manufacturers failed to warn them about the health
          risks of testosterone therapy.
        </p>
        <h2>Who is Affected?</h2>
        <p>
          Several studies over the years have pointed out the possible risks for
          men taking testosterone-enhancing drugs. A{" "}
          <Link
            to="http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0085805"
            target="_blank"
          >
            {" "}
            recent study
          </Link>{" "}
          published in January 2014 in the PLoS One journal reported that men
          over the age of 65 and younger men with undiagnosed heart conditions
          faced double the risk of suffering a heart attack after the first 90
          days of treatment. This study, conducted by UCLA and the National
          Cancer Institute, looked at a large sample of 56,000 patients.
          However, this study was by no means the first to link Low-T therapy
          and heart issues. In 2013, a study published in the Journal of the
          American Medical Association (JAMA) showed a similar link. Another
          study published in the New England Journal of Medicine back in 2010
          also showed that older men were more likely to suffer from heart
          attacks after using testosterone therapy products. Regardless of age
          or health status, men have reported heart issues after taking this
          type of therapy.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .content-well-margin-bottom {
    p:last-child {
      margin-bottom: 0;
    }
    ul {
      margin-bottom: 0;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
