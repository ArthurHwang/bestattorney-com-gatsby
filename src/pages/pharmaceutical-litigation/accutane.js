// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Accutane Injury Attorneys - Law Offices of Bisnar Chase"
        pageDescription="California Accutane Attorneys. Call 800-561-4887 for highest-rated pharmaceutical litigation attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Accutane Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <h3 className="header-red mb">
          We are no longer taking these cases as of 2018. This page is for
          informational purposes only. Please seek out a qualified accutane
          attorney by visiting <Link to="https://Avvo.com">Avvo.com</Link>.
        </h3>

        <h2>
          Defective Drug Accutane Linked to Life-Threatening Abdominal Injuries
        </h2>

        <p>
          For many, Accutane was a miracle drug; no other acne medications were
          nearly as effective as advertised and it was offered as a last-chance
          solution to an overbearingly embarrassing and destructive acne problem
          that the patient may have already began to accept as permanent.
        </p>
        <p>
          When Accutane worked, and sometimes it did, patients were elated
          knowing that there was a solution to their sometimes life-long
          struggle and all it took was a pill. Unfortunately, this defective
          drug has spurred a significant number of patients to come forward with
          diagnoses that are substantially more dangerous than their acne and
          are just now learning that this may have been due to the negligence of
          these Accutane manufacturers and the carelessness in testing for this
          defective medication.
        </p>
        <p>
          Accutane active ingredient is known as Isotretinoin, which is used to
          treat a specific type of severe acne known as recalcitrant nodular
          acne. Isotretinoin is in the retinoid class of medications and works
          by using Vitamin A to help decrease the rate of production of
          substances that cause acne to form. It is usually taken twice a day
          and can take several weeks to show any signs of use. The generic names
          for Accutane are Sotret, Claravis and Amnesteem.
        </p>
        <h2>Isotretinoin and Accutane Side Effects</h2>
        <p>
          What makes defective products like these so dangerous? More reasons
          than many would like to believe. Some defective Accutane patients are
          coming forward with such severe complications that parts or all of
          their colon or rectum have had to be surgically removed to help bear
          the drug side effects. Our experience as product defect lawyers tells
          us that there may be serious implications of manufacturer negligence,
          which may have been easily prevented. The following is a list of
          Isotretinoin side effects that are both short and long-term.
        </p>
        <ul>
          <li>Crohn's Disease</li>
          <li>Inflammatory Bowel Disease (IBD)</li>
          <li>Joint and muscle pain</li>
          <li>Allergic reactions</li>
          <li>Anxiety</li>
          <li>Anger</li>
          <li>Depression</li>
          <li>Birth defects</li>
          <li>Irritable Bowel Syndrome (IBS)</li>
        </ul>
        <p>
          The serious side effects associated with Isotretinoin and Accutane can
          continue even after your treatment has stopped. Many do not associate
          their injuries with the use of these defective acne medications, which
          may lead to further complications. If you are experiencing any of
          these symptoms after using Accutane it is in your best interest to
          contact an attorney as soon as possible.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .content-well-margin-bottom {
    p:last-child {
      margin-bottom: 0;
    }
    ul {
      margin-bottom: 0;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
