// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"
import { Link } from "../components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { ReferralsContact } from "../components/elements/forms/Form_Referrals"
import BrianImage from "../images/brian-small-size.jpg"
import OfficeImage from "../images/office-meeting.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ReferralsPage({ location }) {
  return (
    <LayoutDefault {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Don't Miss Out on Millions in Referrals!"
        pageDescription="Refer your clients to Bisnar Chase and collect referral fees for PI cases."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>What Would Your Referral Fee Be?</h1>
        <BreadCrumbs location={location} />

        <div className="section-1-wrap">
          <h3>
            Bisnar Chase has paid out 8-figures in referral fees for all types
            of cases.
          </h3>
          <p>
            <strong>
              We welcome you to partner with Bisnar Chase Personal Injury
              Attorneys in major auto product liability/crashworthiness cases
              and all other types of catastrophic injury cases.
            </strong>
          </p>
        </div>
        <div className="section-2-wrap">
          <h3>Recent Verdicts & Settlements from Catastrophic Injuries</h3>
          <ul className="grid-wrap">
            <li>
              <div className="content-wrap">
                <p>Multiple 8 - Figures</p>
                <p>Seatback Failure - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 8 - Figures</p>
                <p>Burn Injury - Product Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 8 - Figures</p>
                <p>Dangerous Condition - Government Entity</p>
              </div>
            </li>

            <li>
              <div className="content-wrap">
                <p>8 - Figures</p>
                <p>Caustic Ingestion - Premises Liability</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>8 - Figures</p>
                <p>15-Passenger Van - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Rollover / Roofcrush - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Airbag/Restraints - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Door Latch Failure - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Seat Belt Failure - Auto Defect</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Car vs Pedestrian Accident</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Motorcycle Accident</p>
              </div>
            </li>
            <li>
              <div className="content-wrap">
                <p>Multiple 7 - Figures</p>
                <p>Auto Accident</p>
              </div>
            </li>
          </ul>
        </div>
        <div className="section-3-wrap">
          <div className="section-3-left">
            <p className="title">Partner with Bisnar Chase</p>

            <div className="content-wrap">
              <LazyLoad>
                <img src={OfficeImage} className="imgleft-fixed" />
              </LazyLoad>
              <p>
                Bisnar Chase Personal Injury Attorneys has obtained substantial
                verdicts and settlements in thousands of personal injury cases.
                We get results - because we do our homework - and we have the
                resources to win. Partner with Bisnar Chase or refer us the
                cases you cannot take and we will compensate your firm with
                generous referral fees.
              </p>
              <p className="clearfix">
                {" "}
                <Link to="/about-us">Learn more about us</Link> or{" "}
                <Link to="/contact">contact us</Link> if you have a case you
                would like to discuss.
              </p>
            </div>
          </div>
          <div className="section-3-right">
            <p className="title">Hear Brian Chase Speak at your firm</p>

            <div className="content-wrap">
              <LazyLoad>
                <img src={BrianImage} className="imgleft-fluid" />
              </LazyLoad>
              <p>
                Brian Chase has seen dozens of missed cases from attorneys who
                did not recognize a potential case. Some these cases came as a
                result of an auto defect, and the law offices that dismissed the
                cases were not trained to recognize or take auto defect cases.{" "}
                <strong>
                  Many of these "Missed Cases" have resulted in 7 and 8 figure
                  settlements.
                </strong>
              </p>
              <p>
                Brian Chase will come to your firm and educate your intake
                personnel and paralegals on how to recognize the auto defect
                case.{" "}
                <Link to="/contact">Schedule a free presentation today!</Link>
              </p>
            </div>
          </div>
        </div>

        <div className="section-4-wrap">
          <ReferralsContact />
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .section-1-wrap {
    margin-bottom: 2rem;
    padding-bottom: 2rem;
    border-bottom: medium double grey;
    h3 {
      font-size: 3rem;
      text-align: center;
      line-height: 1.2;
      color: #a30000;
      margin: 0 auto 2rem;
      font-weight: 800;
      width: 80%;

      @media (max-width: 975px) {
        font-size: 2.4rem;
      }

      @media (max-width: 500px) {
        font-size: 2rem;
        width: 100%;
      }
    }
    p {
      font-size: 1.4rem;
      margin: 0 auto;
      width: 70%;
      text-align: center;

      @media (max-width: 975px) {
        font-size: 1.2rem;
      }

      @media (max-width: 500px) {
        width: 100%;
      }
    }
  }

  .section-2-wrap {
    margin-bottom: 2rem;
    padding-bottom: 2rem;
    border-bottom: medium double grey;

    h3 {
      font-size: 2rem;
      text-align: center;
      line-height: 1;
      width: 80%;
      color: ${({ theme }) => theme.colors.secondary};
      margin: 0 auto 2rem;
    }

    li {
      background-color: ${({ theme }) => theme.colors.secondary};
    }

    .content-wrap {
      margin: 0 auto;

      p {
        text-align: center;
        margin-bottom: 0.5rem;

        &:first-child {
          font-weight: 600;
          text-transform: uppercase;
          font-style: italic;
          color: ${({ theme }) => theme.colors.accent};
        }

        &:last-child {
          margin-bottom: 0;
          color: ${({ theme }) => theme.colors.primary};
        }
      }
    }

    .grid-wrap {
      display: grid;
      grid-gap: 2rem;
      list-style-type: none;
      grid-template-columns: 1fr 1fr 1fr;
      margin: 0;
      width: 80%;
      margin: 0 auto;

      @media (max-width: 975px) {
        grid-template-columns: 1fr 1fr;
        width: 100%;
      }

      @media (max-width: 500px) {
        grid-template-columns: 1fr;
        width: 100%;
      }

      li {
        border: 1px solid grey;
        padding: 1rem;
      }
    }
  }

  .section-3-wrap {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
    margin-bottom: 2rem;
    padding-bottom: 2rem;
    border-bottom: medium double grey;

    @media (max-width: 975px) {
      grid-template-columns: 1fr;
    }

    .section-3-left,
    .section-3-right {
      background-color: #eee;
      padding: 2rem;
      p.title {
        font-weight: 600;
        text-transform: uppercase;
        font-style: italic;
        color: ${({ theme }) => theme.colors.secondary};
        text-align: center;
        border-bottom: 1px solid grey;
      }
    }

    .section-3-right {
      img {
        width: 150px;
      }
    }
  }

  .section-4-wrap {
    margin-bottom: 2rem;
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
