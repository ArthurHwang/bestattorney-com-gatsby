// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/food-poisoning-index-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Symptoms & Effects of Food Poisoning - Bisnar Chase"
        pageDescription="Our food poisoning attorneys are aggressive and guarantee maximum compensation. If our legal team does not win your case, you don't pay. Food borne-illnesses are very serious and should never be overlooked. Call for your free consultation and speak with a food poisoning lawyer today at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Food Poisoning</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="food poisoning"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            It is never a popular dinner topic, but a very common, very
            important, and potentially deadly area that people need to be aware
            of.
          </p>
          <h2>What is Food Poisoning?</h2>
          <p>
            There are many different beliefs of when certain foods go bad, but
            regardless of all the specifics, food poisoning is literally when an
            illness is caused by food contaminated with bacteria, viruses,
            parasites, or toxins.
          </p>
          <p>
            Depending on what type of food, how long it has been setting out and
            any contaminants the food has been exposed to will alter the type of
            food poisoning the person or persons experience. The majority of
            food poisoning symptoms are relatively similar, but some food
            poisoning symptoms can be extremely uncomfortable, painful,
            debilitating and even deadly.
          </p>
          <h2>Top 5 Most Common Food Poisoning Symptoms</h2>
          <p>
            Many people have experienced upset stomach, gurgling, rumbling or a
            warm stomach from food that did not agree with us at some point in
            our lives, but true food poisoning symptoms can be right out of a
            horror movie.
          </p>
        </div>
        <p>
          Here is a list of the{" "}
          <strong>Top 5 Most Common Food Poisoning Symptoms</strong>:
        </p>
        <ol>
          <li>Nausea</li>
          <li>Vomiting</li>
          <li>Watery or bloody diarrhea</li>
          <li>Abdominal pain and cramps</li>
          <li>Fever</li>
        </ol>
        <p>
          To learn more about food poisoning symptoms visit{" "}
          <Link to="/food-poisoning/symptoms" target="new">
            our symptoms page
          </Link>
          .
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="food poisoning medical treatment"
            style={{
              backgroundImage:
                "url('/images/text-header-images/food-poisoning-seeking-medical-attention.jpg')"
            }}
          >
            <h2>When to Seek Medical Attention</h2>
          </div>
        </LazyLoad>
        <p>
          It's easy to panic at the first signs of food poisoning, wanting to
          rush ourselves to the emergency room. In many cases, it is not
          completely necessary to go to the hospital, but here are some signs
          according to{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/food-poisoning/symptoms-causes/syc-20356230"
            target="new"
          >
            MayoClinic.org
          </Link>{" "}
          that it is that time to call 911 or head for the ER:
        </p>
        <ul>
          <li>
            Frequent episodes of vomiting and inability to keep liquids down
          </li>
          <li>Bloody vomit or stools</li>
          <li>Diarrhea for more than three days</li>
          <li>Extreme pain or severe abdominal cramping</li>
          <li>An oral temperature higher than 100.4 F (38 C)</li>
          <li>
            Signs or symptoms of dehydration — excessive thirst, dry mouth,
            little or no urination, severe weakness, dizziness, or
            lightheadedness
          </li>
          <li>
            Neurological symptoms such as blurry vision, muscle weakness and
            tingling in the arms
          </li>
        </ul>
        <h2>Top 12 Most Common Types of Food Poisoning</h2>
        <p>
          With all of the different varieties of food poisoning, ranging from
          all types of foods, locations, time of being left out and other
          contamination circumstances, here is a list of the top 12 most common
          types of food poisoning:
        </p>
        <ol>
          <li>
            {" "}
            <Link to="/food-poisoning/botulism" target="new">
              Botulism
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/ecoli" target="new">
              E.coli
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/foodborne-illness" target="new">
              Foodborne Illnesses
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/hepatitis-a" target="new">
              Hepatitis A
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/listeria" target="new">
              Listeria
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/raw-milk-sickness" target="new">
              Raw Milk Sickness
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/salmonella" target="new">
              Salmonella
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/shigella" target="new">
              Shigella
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/tainted-eggs" target="new">
              Tainted Eggs
            </Link>
          </li>
        </ol>
        <p>
          To learn more about the specifics of the list above, visit{" "}
          <Link to="/food-poisoning/types" target="new">
            {" "}
            Types of Food Poisoning
          </Link>
        </p>
        .<h2>Negligence and Food Poisoning</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/food-poisoning-restaurant-kitchen.jpg"
            width="100%"
            id="kitchen"
            alt="food poisoning from restaurant"
          />
        </LazyLoad>
        <p>
          Laws, regulations and{" "}
          <Link
            to="http://publichealth.lacounty.gov/eh/misc/ehpost.htm"
            target="_blank"
          >
            {" "}
            the Restaurant/Market Grading System
          </Link>{" "}
          are put to use to ensure the health and safety of everyone. Negligent
          food service is the number one cause of commercial food poisoning.
        </p>
        <p>
          Restaurants that do not wash, prepare, handle or store food properly
          have a massive risk and liability towards getting somebody sick, a
          group of people sick, or worse; killing someone.
        </p>
        <p>
          There are many ways restaurants, markets and other food and beverage
          establishments can prove negligent in their products and services:
        </p>
        <ul>
          <li>Employees, cooks and servers not washing hands</li>
          <li>
            Dropping food or drinks on the floor or other unsanitary spaces
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
