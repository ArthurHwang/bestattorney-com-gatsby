// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/e.coli-infection-attorney-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="E. Coli Infection Lawyers in Orange County"
        pageDescription="E. Coli can be very serious and life threatening. Take action and have the E. coli Infection Attorneys of Bisnar Chase represent you in your food poisoning case. Our lawyers have won over $500 Million for our clients and have over 39 years of experience. Call for your free consultation at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>E. Coli Infection Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="e.coli food poisoning attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Escherichia Coli O157:H7, commonly known as <strong>E. Coli</strong>,
          is the leading cause of food-borne illness. Recent studies show about
          75,000 cases of illnesses are reported each year, while the death toll
          is close to 100 people a year.
        </p>
        <h2>How is E. Coli Contracted?</h2>
        <LazyLoad>
          <img
            src="/images/food poisoning/undercooked-beef.jpg"
            className="imgright-fluid"
            alt="E. Coli Infection Attorneys"
          />
        </LazyLoad>
        <p>
          Although most strains of Escherichia Coli bacteria are harmless, the
          specific strain known as Shiga toxin-producing E. Coli produces
          powerful toxins and can cause serious illness and even death. E. Coli
          is usually contracted through contaminated foods such as:
        </p>

        <ul>
          <li>
            <strong>undercooked beef</strong>
          </li>
          <li>
            <strong>leafy vegetables</strong>
          </li>
          <li>
            <strong>bean sprouts</strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/food-poisoning/raw-milk-sickness" target="new">
                raw milk
              </Link>
            </strong>
          </li>
        </ul>
        <p>
          E. Coli can also be contracted if a victim swims in or drinks sewage
          contaminated water. Person to person contact in families and child
          care centers is also a known type of transmission.
        </p>
        <h2>How Long Does It Take Before E. Coli Infection Sets In?</h2>
        <p>
          People usually become ill between two and eight days after being
          exposed to the bacteria. The average is three to four days.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/ZmtjuCvzjOQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What Are The Symptoms of E. Coli?</h2>
        <ul>
          <li>
            <strong>non-bloody diarrhea</strong>
          </li>
          <li>
            <strong>severe abdominal cramping</strong>
          </li>
          <li>
            <strong>bloody diarrhea</strong>
          </li>
          <li>
            <strong>kidney failure</strong>
          </li>
          <li>
            <strong>death</strong>
          </li>
        </ul>
        <p>
          In some instances the infection causes no symptoms at all. The
          infection causes little or no fever and lasts between five and ten
          days.
        </p>
        <p>
          {" "}
          <Link to="http://www.cdc.gov/features/ecoliinfection/" target="new">
            Center for Disease Control and Prevention
          </Link>{" "}
          offers tips on how to lower your risk of E. Coli infection.
        </p>
        <p>
          <LazyLoad>
            <img
              src="/images/food poisoning/spinach-recall.jpg"
              className="imgleft-fixed"
              alt="e coli bacteria"
            />
          </LazyLoad>
          In some victims, especially children under the age of five and the
          elderly, the infection can cause a serious complication know as
          Hemolytic Uremic Syndrome (HUS). HUS is where red blood cells are
          destroyed and the kidneys fail. This occurs in about 8% of the
          patients who seek medical attention for E. Coli symptoms. In the
          United States, HUS is the leading cause of acute kidney failure in
          children, and E. Coli is the leading cause of HUS.
        </p>
        <p>
          <strong>
            Did you or a loved one get sick as the result of E. Coli? If you
            were exposed to E. Coli bacteria through contaminated food and you
            believe you are the victim of someone elses negligence, you might
            have a personal injury case.
          </strong>
        </p>
        <h2>Post E. Coli Infection Symptoms</h2>
        <p>
          After someone experiences an E.coli infection, there are a number of
          symptoms that may present themselves once the infection is moving or
          already has moved out of the host.
        </p>
        <ul>
          <li>Anemia</li>
          <li>Pale Skin Color</li>
          <li>Severe dehydration</li>
          <li>Easy bruising</li>
          <li>Nosebleeds</li>
          <li>Fatigue</li>
          <li>Shortness of breath</li>
          <li>Generalized swelling</li>
          <li>Kidney failure</li>
          <li>Jaundice</li>
          <li>Excessive bleeding</li>
          <li>Seizures</li>
          <li>Mental Changes</li>
          <li>
            In severe cases, death (approximately 100 cases per year in the US)
          </li>
        </ul>
        <h2>Dehydration Dangers</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/iv-drip-hydration.jpg"
            width="100%"
            alt="e.coli food poisoning dehydration"
          />
        </LazyLoad>

        <p>
          One of the most debilitating and dangerous symptoms of food poisoning
          and flu-like illnesses, is dehydration.
        </p>
        <p>
          Dehydration is when the body excretes too much water or liquid, so
          that the body is unable to absorb the necessary amount it needs to
          properly function. While vomiting and diarrhea prevent proper
          hydration from occurring, medical attention could be necessary if you
          are unable to consume and absorb enough water, fluid or liquid to
          properly function, stay healthy and safe.
        </p>
        <h2>Experienced Legal Representation You Can Trust</h2>
        <p>
          The <strong>California Food Poisoning Attorneys </strong>at{" "}
          <strong>Bisnar Chase </strong>have been representing clients and
          winning cases for over <strong>39 years</strong>. Our law firm has
          established a <strong>96% success rate </strong>and have obtained over{" "}
          <strong>$500 Million </strong>in legal victories.
        </p>
        <p>
          If you or a loved one has experienced injuries from food poisoning and
          need help getting your life back plus maximum compensation, give our
          attorneys a call for your
          <strong> Free consultation </strong>and{" "}
          <strong>case evaluation</strong> at
        </p>
        <p>
          <strong>
            Contact Bisnar Chase today at 949-203-3814 for a Free, No-Hassle,
            No-Obligation,{" "}
            <Link to="/contact.html" target="new">
              Legal Case Consultation
            </Link>
            .
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
