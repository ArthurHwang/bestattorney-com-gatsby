// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Food Sources Linked to Food Borne Illnesses in U.S., Says CDC"
        pageDescription="The CDC has reached a decision to release their first estimates of those food sources that have been linked to foodborne illnesses in the United States."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Food Sources Linked to Food Borne Illnesses in New CDC Report</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/food-borne-illnesses1.jpg"
          alt="food borne illnesses"
          className="imgright-fixed"
        />
        <p>
          A new report released by the U.S. Centers for Disease Control and
          Prevention (CDC) states that leafy greens such as spinach, lettuce and
          kale caused the most number of{" "}
          <Link
            to="/food-poisoning/foodborne-illness"
            title="food borne illnesses"
          >
            {" "}
            food-borne illnesses
          </Link>{" "}
          in the United States between the years 1998 and 2008. According to the
          Jan. 29 CDC report, contaminated dairy resulted in the most
          hospitalizations and poultry that was tainted with bacteria such as E.
          coli or Salmonella accounted for the most food poisoning deaths.
        </p>
        <p>
          The{" "}
          <Link
            to="http://wwwnc.cdc.gov/EID/article/19/3/11-1866.htm"
            title="food borne illness"
          >
            CDC study
          </Link>{" "}
          examined 4,887 outbreaks that resulted in 128,269 illnesses,
          hospitalizations and deaths when the food that caused them was known
          or suspected. The study appeared in the journal, Emerging Infectious
          Diseases. Researchers at the CDC found that leafy greens accounted for
          23 percent of the illnesses and dairy products, 14 percent.
        </p>
        <p>
          But, when they looked at hospitalizations due to food-borne illnesses,
          dairy products accounted for 16 percent. When it came to food
          poisoning deaths, poultry accounted for 19 percent and dairy 10
          percent, the report stated. Over the decade that was studied, 277
          people died from food-borne illnesses linked to poultry and 140 from
          illnesses connected to dairy products.
        </p>
        <p>
          "I'm encouraged to see the CDC compile a more comprehensive report to
          track illnesses related to food-borne contamination. These numbers can
          be critical to studying food poisoning trends and categories of
          high-risk food products," said John Bisnar, founder of the Bisnar
          Chase personal injury law firm. "Hopefully, these numbers will equip
          government agencies and the food industry with the information they
          need to make what we put in our mouths safer. This new study has made
          it easier to identify the categories of food products that are causing
          the most illnesses."
        </p>
        <p>
          Food producers have a legal and moral obligation to ensure that they
          are following all safety standards, Bisnar said. "As consumers, we
          rely heavily on these corporations to provide us with safe food. When
          the system fails, the consequences can be devastating for millions of
          Americans, which this newly released CDC study clearly shows."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California personal injury lawyers of Bisnar Chase represent
          victims who have been sickened by contaminated food. The firm has been
          featured on a number of popular media outlets including Newsweek, Fox,
          NBC, and ABC and is known for its passionate pursuit of results for
          their clients. Since 1978, Bisnar Chase has recovered millions of
          dollars for seriously injured victims and their families.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>
        <p>Sources:</p>
        <p>
          http://www.cdc.gov/foodborneburden/attribution-1998-2008.html
          http://www.cdc.gov/media/releases/2013/a0129_foodborne_illnesses.html
          http://wwwnc.cdc.gov/eid/
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
