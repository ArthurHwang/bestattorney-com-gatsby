﻿// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Salmonella Attorneys"
        pageDescription="Call 949-203-3814 for award-winning top-rated California salmonella poisoning attorneys. No-Win, No-Fee lawyers and Free consultations since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Salmonella Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <img
            src="/images/food poisoning/salmonella.jpg"
            className="imgright-fluid"
            width="35%"
            alt="salmonella poisoning"
          />
          <strong>Salmonella</strong> is a very dangerous bacterial disease,
          which untreated, can cause death. Most who are infected with
          Salmonella will develop symptoms within 12-72 hours of infection.
          Symptoms can include diarrhea, fever and abdominal cramping. The
          illness can last from 4 to 7 days and while most people recover
          without treatment, this is not always the case. In some cases, the
          patient can experience diarrhea that is so severe, hospitalization is
          necessary to keep the patient from suffering dehydration. Also, in
          these patients, the Salmonella can spread from the intestines to the
          blood stream, and then to any other parts of the body. If and when
          this occurs, it is critical that the patient receive antibiotics
          immediately to prevent death.
        </p>
        <h2>What is the difference between Salmonella and E. coli?</h2>
        <p>
          According to the{" "}
          <Link to="http://answers.hhs.gov/questions/7680" target="_blank">
            U.S. Department of Health and Human Services
          </Link>{" "}
          (HHS) Salmonella and E. Coli are two differnt types of bacteria.
        </p>
        <h2>E. Coli</h2>
        <p>
          E. Coli is an intestinal bacteria caused by foods such as undercooked
          meat. It is typically harmless if you are healthy although it can
          cause diarrhea. Young children, the elderly, and people with a weak
          immune system may experience much more serious effects such as bloody
          diarrhea, kidney failure, and death.
        </p>
        <h2>Salmonella</h2>
        <p>
          Salmonella is the most common type of food poisoning and is mostly
          present in foods of animal origin such as beef, poultry, milk and
          eggs, although it can develop in any type of food, including fruit and
          vegetables. Many raw foods of animal origin are contaminated, but the
          bacteria is killed through thorough cooking. Foods can also become
          contaminated by an infected food handler or one who has not washed
          their hands after using the restroom. Salmonella can also be found in
          the feces of some pets, especially if they are sick, and some reptiles
          are also known to be carriers of the Salmonella.
        </p>
        <p align="center">
          If you or a loved one has become seriously infected with{" "}
          <strong>Salmonella poisoning</strong> from eating contaminated food,
          please contact one of our personal injury attorneys.
        </p>
        <p align="center">
          <strong>
            Contact us today at 949-203-3814 for a Free No-Hassle,
            No-Obligation, Case Consultation.
          </strong>
        </p>
        <h3>Related Resources</h3>
        <ul>
          <li>
            {" "}
            <Link to="/food-poisoning/tainted-eggs">
              Salmonella Tainted Eggs
            </Link>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
