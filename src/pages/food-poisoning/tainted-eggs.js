// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Salmonella Tainted Eggs - Food Poisoning"
        pageDescription="Have you been sickened by salmonella tainted eggs? Call 949-203-3814 for highest-rated food poisoning attorneys"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Salmonella Tainted Eggs</h1>
        <BreadCrumbs location={location} />
        <h2>Have you experienced food poisoning from recalled eggs?</h2>
        <p>
          After a recent Salmonella outbreak a number of citizens and government
          agencies have shown their concern regarding possible{" "}
          <Link to="/food-poisoning/foodborne-illness">
            {" "}
            food borne illness
          </Link>{" "}
          cases. In collaboration with the FDA and CDC to find a common source
          of salmonella tainted eggs, investigations have been underway to find
          the providers. Some of the potential sources include Wright County Egg
          in Iowa and Hillandale Farms of Iowa, Inc.
        </p>
        <p>
          These eggs could still be in grocery stores, restaurants, and even
          your home. If you feel that you have experienced food poisoning or
          become ill from consuming recalled eggs, it is best to consult with
          your doctor.
        </p>
        <p>
          You have a right to compensation if you have eaten eggs and
          experienced food poisoning symptoms severe enough for you to visit a
          medical doctor. Persons infected with Salmonella Enteritidis usually
          experience the following food poisoning symptoms 12 to 72 hours after
          consumption:
        </p>
        <ul>
          <li>
            <strong>Fever</strong>
          </li>
          <li>
            <strong>Abdominal cramps</strong>
          </li>
          <li>
            <strong>Diarrhea</strong>
          </li>
        </ul>
        <p>
          The symptoms will usually last anywhere from four to seven days. The
          symptoms are more serious to elderly, infants, or those with impaired
          immune systems. In these cases, hospitalization may be necessary.
        </p>
        <h2>How Can I Tell If My Eggs Have Been Recalled?</h2>
        <LazyLoad>
          <img
            src="/images/egg-recall-plant-number-julian-date.jpg"
            alt="Salmonella Tainted Eggs "
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          The list of affected brands include: Albertsons, Alta Dena Dairy,
          Bayview, Becky, Boomsma's, Cal Egg, Challenge Dairy, Country Eggs,
          Driftwood Dairy, Dutch Farms, Farm Fresh, Glenview, Hidden Villa
          Ranch, Hillandale Farms, James Farms, Kemps, Lucerne, Lund, Mi Pueblo,
          Mountain Dairy, Nulaid, Pacific Coast, Ralph's, Shoreland, Sunny
          Farms, Sunny Meadow, Sunshine, Sun Valley, Trafficanda, West Creek,
          and Wholesome Farms, according to the FDA.
        </p>

        <p>
          According to information from the U.S. Food & Drug Administration's
          website, you can determine if your eggs are affected by checking your
          Plant Number and Julian Date provided on the side of your egg carton.
          If the Plant number and Julian Date both match with the numbers from
          the following table, your eggs are included in the recall. If both the
          affected Plant Number and Julian Date do not match your eggs are not
          involved in the recall.
        </p>
        <table cellspacing="0" cellpadding="0" width="550" border="3">
          <tbody>
            <tr>
              <th width="100">
                <font size="2">
                  <strong>Brand</strong>
                </font>
              </th>
              <th width="100">
                <font size="2">
                  <strong>Plant Number</strong>
                </font>
              </th>
              <th width="100">
                <strong>
                  <font size="2">Julian Dates</font>
                </strong>
              </th>
            </tr>
            <tr>
              <td>
                <font size="2">Albertson</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Alta Dena Dairy</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">209-224 (Loose 15-dozen units)</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Bayview</font>
              </td>
              <td>
                <font size="2">1686</font>
              </td>
              <td>
                <font size="2">142-149</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Boomsma's</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Challenge Dairy</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">209-224 (Loose 15-dozen units)</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Country Eggs, Inc</font>
              </td>
              <td>
                <font size="2">1946 or 1026</font>
              </td>
              <td>
                <font size="2">216-221</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Driftwood Dairy</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">209-224 (Loose 15-dozen units)</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Dutch Farms</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Farm Fresh</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Glenview</font>
              </td>
              <td>
                <font size="2">1720 or 1942</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Hidden Villa Ranch</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">209-224 (Loose 15-dozen units)</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Hillandale</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Hillandale Farms</font>
              </td>
              <td>
                <font size="2">1663</font>
              </td>
              <td>
                <font size="2">137 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Hillandale Farms</font>
              </td>
              <td>
                <font size="2">1860</font>
              </td>
              <td>
                <font size="2">099 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">James Farms</font>
              </td>
              <td>
                <font size="2">1720 or 1942</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Kemps</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Lucerne</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Lund</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Mountain Dairy</font>
              </td>
              <td>
                <font size="2">1091</font>
              </td>
              <td>
                <font size="2">167-174</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Mountain Dairy</font>
              </td>
              <td>
                <font size="2">1951</font>
              </td>
              <td>
                <font size="2">193-208</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Mountain Dairy</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Nulaid Medium</font>
              </td>
              <td>
                <font size="2">1951</font>
              </td>
              <td>
                <font size="2">195-210</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Pacific Coast</font>
              </td>
              <td>
                <font size="2">1720 or 1942</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Ralph's</font>
              </td>
              <td>
                <font size="2">1026, 1413, 1720, 1942 or 1946</font>
              </td>
              <td>
                <font size="2">136-229</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Shoreland</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sun Valley Medium</font>
              </td>
              <td>
                <font size="2">1951</font>
              </td>
              <td>
                <font size="2">195-209</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sunny Farms</font>
              </td>
              <td>
                <font size="2">1663</font>
              </td>
              <td>
                <font size="2">138 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sunny Farms</font>
              </td>
              <td>
                <font size="2">1860</font>
              </td>
              <td>
                <font size="2">099 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sunny Meadow</font>
              </td>
              <td>
                <font size="2">1663</font>
              </td>
              <td>
                <font size="2">139 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sunny Meadow</font>
              </td>
              <td>
                <font size="2">1860</font>
              </td>
              <td>
                <font size="2">099 - 230</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Sunshine</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
            <tr>
              <td>
                <font size="2">Trafficanda</font>
              </td>
              <td>
                <font size="2">1026, 1413, or 1946</font>
              </td>
              <td>
                <font size="2">136-225</font>
              </td>
            </tr>
          </tbody>
        </table>
        <p>
          These are not the only eggs that are currently under consideration. In
          Texas, officials are investigating 165 salmonella outbreaks that may
          be related to the Salmonella Enteritidis egg recall. All of the cases
          and the eggs tied to the recall share the same strain of Salmonella
          Enteritidis, according to state health officials.
        </p>
        <p align="center">
          <strong>
            If you or a loved one has suffered symptoms after eating recalled
            eggs, please contact our experienced food poisoning attorneys. We
            will use our experience, knowledge and resources to achieve the best
            possible results for you and your family.
          </strong>
        </p>
        <p align="center">
          <strong>
            Call us today at 949-203-3814 for a No-Hassle, No-Obligation, Free
            Case Consultation.
          </strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
