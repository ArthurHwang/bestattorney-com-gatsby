﻿// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Shigella Food Poison Lawyers"
        pageDescription="Call 949-203-3814 for top-rated California shigella food poisoning attorneys in Orange county."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>What is Shigella?</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>Shigella infection</strong> is a common cause and form of
          diarrhea experienced as a result of individuals becoming infected with
          the Shigella family of bacteria.
        </p>
        <p>
          While typically mild to moderate and brief, there are instances where
          Shigella can become more serious, and where post-infection symptoms
          and problems could continue for a period of months or even years.
          Continue reading to learn more about Shigella symptoms, treatment and
          prevention, and more.
        </p>
        <h2>How is Shigella contracted?</h2>
        <p>
          <img
            src="/images/food poisoning/sandwhich-shigella.jpg"
            alt="What is Shigella?"
            className="imgright-fixed"
            title="shigella bacteria"
          />
          Shigella is contracted through ingesting contaminated food or water
          which has even microscopic amounts of feces from those who were
          infected. Direct contact with the bacteria from an infected individual
          may also cause you to become infected.
        </p>
        <p>
          The most common forms of food which lead to an infection include
          salads and sandwiches -- items with a lot of hands-on preparation --
          and raw vegetables which have been unwashed.
        </p>
        <p>
          Shigella is far more commonplace in third world countries around the
          world than in the United States, and is therefore a common cause of
          traveler's diarrhea and associated problems.
        </p>
        <p>
          Other high risk settings for Shigella infection include child-care and
          school settings. This includes not only the adults who are caring for
          the children, but the children themselves, particularly toddlers
          between the ages of 2 and 4.
        </p>
        <h2>Common Shigella symptoms</h2>
        <p>
          A Shigella infection is known as Shigellosis. It is an infectious
          bacterial disease which causes diarrhea. Shigellosis may be caused by
          several forms of bacteria, including Shigella sonnei or "Group D"
          Shigella, which accounts for two thirds of cases in the United States.
        </p>
        <p>
          Shigella flexneri or "Group B" Shigella accounts for much of the rest
          in the United States. Other forms of Shigella are common in other
          countries, including Shigella dysenteriae type 1, which can be deadly.
        </p>
        <h2>In addition to diarrhea, other Shigella symptoms include:</h2>
        <ul>
          <li>Sudden abdominal cramps</li>
          <li>Fever</li>
          <li>Nausea and vomiting</li>
          <li>
            Diarrhea specifically including bloody or mucus-filled diarrhea
          </li>
        </ul>
        <h2>Shigella Treatment & Prevention</h2>
        <p>
          Luckily, Shigella treatment and prevention are both generally easy,
          although prevention is not always possible. This is especially the
          case when traveling in developing countries. In these instances, it is
          best to only drink treated or boiled water, eat only cooked hot foods,
          and food which only you have handled and washed.
        </p>
        <h2>When stateside, prevention includes:</h2>
        <ul>
          <li>
            Thoroughly washing hands with soap after using the bathroom,
            changing children's diapers, before and after preparing foods, etc.
          </li>
          <li>
            Disposing of soiled diapers and disinfecting diaper changing areas
            and/or equipment
          </li>
          <li>Supervise handwashing of toddlers and small children</li>
          <li>Avoiding drinking still water from ponds and lakes</li>
        </ul>
        <p>
          Shigella treatment typically consists of simply getting enough fluids
          and rest. The duration of the illness is typically less than a week in
          duration, and may only be a few days. Illness typically sets in within
          1 to 3 days after contact with contaminated food or water, but could
          take up to a week.
        </p>
        <p>
          If you have Shigellosis it is recommended that you stay home from
          school or work to avoid the further spread of the disease. If a child
          has Shigellosis, it is recommended that he or she is kept from contact
          with uninfected children for several days, and extra care and
          attention to detail is given towards thorough cleanliness,
          disinfecting, cleaning, etc.
        </p>
        <p>
          Shigella can be diagnosed through a laboratory test of stool when
          experiencing diarrhea and related symptoms.
        </p>
        <p>
          Antibiotics can also be provided which will fight off Shigellosis in a
          shorter period of time, including ampicillin and ciprofloxacin,
          however, some strains are resistant to antibiotics. Antidiarrheal
          agents including Imodium or Lomotil should be avoided, as they can
          aggravate the illness.
        </p>
        <p>
          In a small percentage of cases, as little as 2%, lingering symptoms
          may be present, including painful urination, or joint pain known as
          post-infectious arthritis. This can last for months or years and cause
          chronic arthritis, and results most typically when an individual
          predisposed to these issues develops a Shigella infection.
        </p>
        <p>
          If you or a loved one have been the victim of a Shigella infection in
          California and have suffered painful, long-lasting side effects and
          symptoms such as those mentioned above, then you should take action to
          seek proper remedies and protect your best interests.
        </p>
        <p>
          Get in touch with us today to learn more about how we can help, and
          we'll go over the details of your case and illness to determine the
          best course of action while providing you with an entirely free
          consultation.
        </p>
        <p align="center">
          <strong>
            If you or a loved one has become ill as the result of unsafe food,
            please contact an Orange County Shigella Poisoning Lawyer at BISNAR
            CHASE immediately. Call 949-203-3814 today for a free consultation.
          </strong>
        </p>
        <p align="center">&nbsp;</p>
        <h3>Related Resources</h3>
        <p>
          {" "}
          <Link to="/food-poisoning/types">Other Types of Food Poisoning</Link>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
