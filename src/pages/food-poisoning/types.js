﻿// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Types of Food Poisoning - Foodborne Illness"
        pageDescription="Experienced foodborne illness attorneys in Orange County. Get a Free Consultation. If you have a food poisoning case, call 949-203-3814 today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Types of Food Poisoning</h1>
        <BreadCrumbs location={location} />
        <h2>
          There are various types of food poisoning that cause severe injury and
          sometimes death:
        </h2>

        <h2>E.Coli</h2>
        <img
          src="/images/food poisoning/undercooked-beef100x100.jpg"
          alt="Types of Food Poisoning"
          className="imgleft-fixed mb"
          title="e coli in undercooked beef"
        />
        <p>
          E. Coli is a bacteria found in water and food, including sprouts and
          undercooked beef. This foodborne illness causes people to have severe
          diarrhea after consuming food that has been contaminated. E-coli can
          be transferred between people if an individual has traces of the
          bacteria on their hands.
        </p>

        <h2>Listeria</h2>
        <img
          src="/images/food poisoning/cantoloupe100x100.jpg"
          alt="Types of Food Poisoning, fruits"
          className="imgleft-fixed mb"
          title=" cantaloupe-listeria"
        />
        <p>
          Listeria is a bacteria that causes a foodborne illness called
          Listeriosis. Unlike other bacteria, Listeria can survive at very low
          temperatures ranging from O&deg;F to 50&deg;F. Many refrigerators fall
          within this range making it possible for the bacteria to grow in a
          refrigerator and contaminate food. Listeria is very dangerous because
          it can be passed from a mother to a child while pregnant or giving
          birth. It is recommended that certain refrigerated foods be heated
          prior to eating to help reduce the risk of Listeriosis.
        </p>

        <h2>Hepatitis A</h2>
        <img
          src="/images/food poisoning/hepatitus-taco.jpg"
          alt="hepatitis A"
          className="imgleft-fixed mb"
          title=" hepatitus-a from food contact"
        />
        <p>
          The virus, Hepatitis A, is one of five types of hepatitis that harm
          the liver. Every year in the United States, approximately one hundred
          people die due to Hepatitis A-related liver problems. Up to 50,000
          people are infected annually with the virus which is spread through
          contact with an infected person or when food or water comes in contact
          with an infected person.
        </p>

        <h2>Salmonella</h2>
        <img
          src="/images/food%20poisoning/salmonella-thumbnail.jpg"
          alt="Salmonella"
          className="imgleft-fixed mb"
          title="salmonella poisoning"
        />
        <p>
          Salmonella is a very serious and common bacteria that causes foodborne
          illness. There are over 2,000 different types of Salmonella bacteria.
          Salmonella can be found in many meat or meat products, such as pork,
          beef, poultry, dairy, eggs, and even vegetables and fruit. In the
          United States as many as 5,000 people die annually due to Salmonella.
        </p>

        <h2>Botulism</h2>
        <img
          src="/images/food poisoning/green-beans.jpg"
          alt="botulism"
          className="imgleft-fixed mb"
          title="botulism in canned vegetables"
        />
        <p>
          Botulism is a foodborne illness caused by the deadly bacteria called
          "Clostridium botulinum." It can be found in dirt, water, and the air.
          Botulism typically results from bacteria growing in canned goods, such
          as canned vegetables and meats. To help prevent Botulism, discard any
          old or bulging canned goods.
        </p>

        <h2>Shigella</h2>
        <img
          src="/images/food poisoning/shigella-sandwhich100x100.jpg"
          alt="shigella"
          className="imgleft-fixed"
          title="shigella bacteria"
        />
        <p>
          Shigella is a germ that causes a foodborne illness called
          "Shigellosis" and can be found in the intestines of humans.
          Shigellosis is spread when an infected person has germs are on their
          hands and touches another individual, infecting them with severe
          diarrhea. There are approximately 25,000 cases of Shigellosis reported
          annually in the United States.
        </p>
        <p>
          BISNAR CHASE Personal Injury Attorneys represent the victims of{" "}
          <strong>foodborne illness</strong>. Our skilled lawyers have a
          successful track record with impressive recoveries on behalf of our
          clients. We are passionate and dedicated legal professionals who care
          about our clients. We have been representing victims of foodborne
          illness and severe food poisoning since 1978.
        </p>
        <p align="center">
          If you or a loved one has become ill as the result of a{" "}
          <Link to="/food-poisoning/foodborne-illness">foodborne illness</Link>,
          please contact one of our California Food Poisoning Lawyers
          immediately.
        </p>
        <p align="center">
          <strong>
            Call us today for a No-Hassle, No-Obligation Free Case Consultation.
          </strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
