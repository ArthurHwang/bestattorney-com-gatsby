﻿// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/food-poison-symptoms-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Common Food Poisoning Symptoms - Causes of Food-borne Illnesses"
        pageDescription="Food poisoning can be very serious and cause life threatening symptoms. Find out what are the most common foods to cause food-born illness and treatments to regain health after a serious bout of food poisoning."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Common Food Poisoning Symptoms</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Common Food Poisoning Symptoms"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <h2>What Are The Symptoms of Food Poisoning?</h2>
        <LazyLoad>
          <img
            src="/images/food poisoning/food-poisoning-symptoms.jpg"
            className="imgright-fixed"
            alt="food borne illness symptoms"
          />
        </LazyLoad>
        <p>
          The symptoms of food poisoning may begin within an hour of eating
          contaminated food or may take up to a few weeks to appear. There are a
          variety of symptoms associated with food poisoning. Each one is
          different, and each may have different signs of sickness. The
          following is a list of some of the more common symptoms:
        </p>

        <ul>
          <li>Diarrhea</li>
          <li>Abdominal cramping</li>
          <li>Fever</li>
          <li>Headache</li>
          <li>Vomiting</li>
          <li>Exhaustion</li>
        </ul>
        <p>
          Although unpleasant, food poisoning will not have long-term or
          critical consequences if you are a healthy person. If you have certain
          medical conditions, including cancer, HIV, or diabetes, food poisoning
          may have serious effects on your health.
        </p>

        <h2>What Are The Causes of Food Poisoning?</h2>
        <p>
          Food poisoning is caused by bacteria, viruses, and parasites. Foods
          that have not been properly handled or stored may pose a threat to
          consumers.
        </p>
        <p>
          Often, food poisoning can be prevented by ensuring you wash your food,
          don't let perishables stay out of the fridge, and to not eat food that
          is left out or prepared inappropriately.
        </p>
        <h2>What Are Some Common Foods That May Be Hazardous?</h2>
        <LazyLoad>
          <img
            src="/images/food poisoning/meats_and_poultry.jpg"
            className="imgright-fixed"
            alt="meat and poultry food poisoning"
          />
        </LazyLoad>
        <p>
          According to the{" "}
          <Link
            to="https://www.cdc.gov/foodsafety/prevention.html"
            target="new"
          >
            Centers for Disease Control and Prevention
          </Link>
          , it is estimated that 1 in 6 get sick from eating contaminated food
          every year.
        </p>
        <p>
          Whether food has been improperly prepared or left at room temperature,
          the levels of illness can vary greatly. Usually you can treat yourself
          after a bout of food poisoning through rest and hydration, but more
          serious cases could require medical treatment.
        </p>

        <ul>
          <li>Eggs that are raw or undercooked</li>
          <li>
            {" "}
            <Link to="/food-poisoning/raw-milk-sickness" target="new">
              Raw milk
            </Link>
          </li>
          <li>Produce</li>
          <li>Chicken</li>
          <li>Tuna</li>
          <li>Macaroni salad</li>
          <li>Potato salad</li>
          <li>Raw seafood</li>
          <li>Clams</li>
          <li>Scallops oysters</li>
          <li>Mussels</li>
        </ul>
        <p>
          While these food have higher chances of containing bacteria and other
          food poisoning possibilities, they are not the only foods or types of
          food that can cause food poisoning, even severe cases.
        </p>
        <p>
          Anything you ingest can cause internal infections, chewing gum, candy,
          nuts, even water can cause internal infections and food poisoning.
        </p>
        <h2>Top 17 Food Illness Outbreaks in US History</h2>
        <p>
          Over time there have been many cases of food poisoning that leave
          victims in pain, and who have even succumbed to their infections.
        </p>
        <p>
          Here is a list of the{" "}
          <strong>Top 17 Food Illness Outbreaks in US History</strong>,
          information quoted and provided by HealthLine.com. Link to article
          below list.
        </p>
        <ol>
          <li>
            <p>
              <strong>2009: PCA peanut butter </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2011: Cargill ground turkey </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2013: Foster Farms chicken </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2015: Mexican cucumbers </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>1993: Jack in the Box hamburgers </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2006: Dole baby spinach </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2006: Taco Bell fast food </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2015: Chipotle Mexican Grill fast food </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>1977: Trini &amp; Carmen's hot sauce </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2015: Home-canned potatoes</strong>
            </p>
          </li>
          <li>
            <p>
              <strong>1985: Jalisco Products cheese </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>1998-1999: Hot dogs </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2002: Pilgrim's Pride turkey meat </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2011: Cantaloupes </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>1997: Frozen strawberries </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2003: Chi-Chi's salsa and chili con queso </strong>
            </p>
          </li>
          <li>
            <p>
              <strong>2016: Tropical Smoothie Cafe drinks </strong>
            </p>
            <p>
              For more on the above information and to learn specifics of each
              event, visit{" "}
              <Link
                to="https://www.healthline.com/health/worst-foodborne-illness-outbreaks#hepatitis-a"
                target="new"
              >
                {" "}
                healthline.com/health/worst-foodborne-illness-outbreaks
              </Link>
              .
            </p>
          </li>
        </ol>

        <h2>How Do I Prevent Food Poisoning at Home?</h2>
        <p>
          Remember to clean cutting boards, work areas, and utensils during and
          after food preparation to prevent contamination from food drippings.
          Cleanliness is an easy way to{" "}
          <Link to="/food-poisoning/foodborne-illness" target="new">
            {" "}
            prevent many kinds of foodborne illnesses
          </Link>
          . The following are some simple to follow tips to keep you safe:
        </p>
        <ul>
          <li>Use soap and warm water to wash hands</li>
          <li>
            Wash hands before preparing food and after touching raw meat or
            poultry.
          </li>
          <li>Use clean cooking utensils.</li>
          <li>
            Wash cutting boards frequently with warm water and soap, especially
            after cutting raw meat, fish, or poultry.
          </li>
          <li>Keep hair tied back or use a hair net.</li>
          <li>Wash fruit and vegetables with water before consuming.</li>
          <li>
            Use a smooth, non-porous cutting board to avoid absorbing the juices
            of raw meat.
          </li>
          <li>Cleanse the lids of canned food before opening.</li>
          <li>Wash dish towels in hot water once a week or whenever dirty.</li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/EC7UaLIAEP4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          During the holidays it is especially important to practice safe food
          handling. If you are eating out make sure all poultry and fish is
          thoroughly cooked. Food poisoning from road side restaurants while
          traveling is very common and can ruin your holiday vacation.
        </p>
        <p align="center">
          <strong>
            If you or a loved one has become seriously infected from eating
            contaminated food, please contact one of our food poisoning
            attorneys. Contact us today for a No-Hassle, No-Obligation, Free
            Case Consultation.
          </strong>
        </p>
        <h3>Related Resources</h3>

        <p>
          {" "}
          <Link to="/premises-liability">Mercury Poisoning From Fish</Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
