// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/boy-swimming-pool-attorneys-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Swimming Pool Accident Lawyers - Bisnar Chase"
        pageDescription="Call us now at 951-530-3711 to contact our Riverside swimming pool accident attorney. You may qualify for compensation and we'll fight for you. Free consultation, 96% success rate and over $500 Million recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Swimming Pool Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Swimming Pool Accident Attorneys"
            title="Riverside Swimming Pool Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The weather in <strong> Riverside County</strong> is perfect almost
          year-round for a dip in the pool.
        </p>
        <p>
          For the most part of the year, residents enjoy poolside parties and
          gatherings, but with the luxury of enjoying a swimming pool comes the
          danger of swimming pool accidents.
        </p>
        <p>
          Proactive safety measures can make you pool experience a fun and safe
          time, however, not all pool owners share the same sense of importance
          concerning pool safety.
        </p>
        <p>
          If you have been injured in a{" "}
          <strong> Riverside swimming pool accident</strong> cause by the
          negligence of a pool owner, call us our Riverside personal injury
          lawyers now at <strong> 951-530-3711 </strong>to set up your free
          consultation.
        </p>
        <p>
          During your <strong> Free Consultation</strong>, we will inform you of
          your rights and determine if your case qualifies you for{" "}
          <strong> compensation</strong> for your injuries.
        </p>
        <h2>How to Prevent Swimming Pool Accidents in Riverside</h2>
        <p>
          It can feel like a stone hit the bottom of your stomach when you read
          a headline along the lines of "8-Year-Old Child Drowns in Swimming
          Pool Accident."
        </p>
        <p>
          It emotionally hurts because not only was this child's life cut so
          drastically short, that child's death could have been prevented and
          that child could have gone on to do great things in their life.
        </p>
        <p>
          This is why our attorneys have have provided the tips below to help
          you and your loved ones prevent these types of tragedies from
          occurring:
        </p>
        <ul>
          <li>
            Install a{" "}
            <Link
              to="https://www.signs.com/blog/state-by-state-guide-to-pool-signage-and-fencing-requirements/"
              target="_blank"
            >
              {" "}
              safety fence
            </Link>{" "}
            around your pool. In most cases, it is required by law.
          </li>
          <li>
            Post safety signs. This is required for all public pools in
            California. This is optional for private pools but is strongly
            encouraged.
          </li>
          <li>
            Install adequate lighting if the pool will be used at night or dimly
            lit situations
          </li>
          <li>
            Keep a list of emergency contact information near your pool. This
            list should include 911, your local police station, and any other
            emergency services you deem are appropriate
          </li>
          <li>
            Always have a trained, experienced lifeguard on duty. It is
            important to have someone near the pool who knows CPR and is CPR
            certified in case someone drowns or passes out.
          </li>
          <li>
            Know the limit of how many people can be in your pool at once.
          </li>
          <li>
            Keep nearby pool pavement as dry as possible. Display wet floor
            signs if deemed necessary. It is better to be proactively safe than
            to have someone slip-and-fall and injury themselves on your
            premises.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Makes Swimming Pools So Dangerous?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dangers-swimming-pool-activities-fun-family-accidents-attorneys-lawyers.jpg')"
            }}
          >
            <h2>What Makes Swimming Pools So Dangerous?</h2>
          </div>
        </LazyLoad>

        <p>
          Owning property with a pool is a major responsibility that should not
          be taken lightly.
        </p>
        <p>
          Pools can pose a great threat to safety whether they are located at a
          hotel, school, water park or private home.
        </p>
        <p>
          Swimming pools particularly present serious dangers for young children
          who are inexperienced swimmers.
        </p>
        <p>
          Toddlers are especially at risk for wandering away, slipping into the
          pool and drowning without a sound.
        </p>
        <p>
          Children, even those who are intermediate or advanced swimmers, should
          be carefully supervised when they are in a pool.
        </p>
        <p>
          It takes just a few seconds for a child to slip underwater and drown.
          Swimming pools are silent killers and that is what makes them so
          dangerous.
        </p>
        <h2>Riverside Swimming Pool Accident Statistics</h2>
        <p>
          Drowning is the third most common cause of accidental death or in
          legal jargon a{" "}
          <Link to="/riverside/wrongful-death">wrongful death</Link>, in the
          United States causing almost 8,000 deaths each year.
        </p>
        <p>
          Experts estimate that there may be 600 near-drowning incidents for
          every reported drowning death.
        </p>
        <p>
          Near-drowning refers to survival after suffocation by submersion.
          Those who are 5 or younger are most at risk of near-drowning.
        </p>
        <p>
          Although victims do survive these types of accidents, near-drowning
          can result in catastrophic injury such as irreversible brain damage.
        </p>
        <p>
          This type of injury occurs when oxygen supply to the brain is cut off
          as a result of submersion. When a person is submerged, hypothermia or
          lower body temperature could also lead to heart arrhythmia.
        </p>
        <h2>How Can I Make My Swimming Pool More Safe?</h2>
        <p>
          Young children are the most vulnerable when it comes to swimming pool
          accidents and Riverside is no exception. There are several measures
          one can take to prevent these type of tragedies from happening.
        </p>
        <p>
          First and foremost, if you have yet to install a pool fence for your
          pool, do it now. Experts say is an effective tool to prevent
          accidents. According to the{" "}
          <Link to="https://www.cdc.gov/" target="new">
            U.S. Centers for Disease Control and Prevention
          </Link>
          , a four-sided fence that separates the pool on all sides from the
          house and the yard reduces a child's risk of drowning 83 percent over
          a three-sided fence that runs along the property line.
        </p>
        <p>
          You can't avoid having wet surfaces around your pool. They come with
          the territory. However, you can reduce the risk of a slip-and-fall
          accident by using any number of different anti-slip products that are
          available.
        </p>
        <p>
          There are adhesive pads and paint-on coatings that can give better
          grip.
        </p>
        <p>
          You should also make sure that your drains are updated. The drains in
          your swimming pool produce significant suction to keep your pool water
          continuously circulated and filtered.
        </p>
        <p>
          "Pool entrapment" occurs when a swimmer's body, hair or clothing
          becomes entangled in a drain or grate.
        </p>
        <p>
          Although, it doesn't happen often, it can become a serious or
          life-threatening incident.
        </p>

        <p>
          You can greatly reduce the risk of pool entrapment by making sure that
          your drain covers are in good repair and that they are in compliance
          with federal regulations.
        </p>
        <p>
          There are also vacuum release systems available as safety features
          that will automatically turn off the suction when a blockage is
          detected.
        </p>
        <p>
          <em>
            {" "}
            <Link
              to="http://www.mesaaz.gov/residents/fire-medical"
              target="new"
            >
              The Mesa Fire and Medical Department
            </Link>{" "}
            on 12 News Today, discuss the dangers of outdated and unsafe drain
            covers in pools, spas and hot tubs in the video below.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9Pzmw7HUTdE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Always have a well-stocked first-aid kit and a rescue-grade flotation
          device accessible and ready to use. Also have a phone nearby with the
          home's address posted by the phone.
        </p>
        <p>
          It can save a lot of time if someone has to call 911 in case of an
          emergency. It also helps to take CPR lessons. It's a skill that can
          save lives in crisis situations.
        </p>
        <p>
          Last but not the least, never underestimate a child's curiosity when
          it comes to pools. It helps to have not just one safety feature, but
          layers of protection when it comes to keeping young children away from
          pools.
        </p>
        <p>
          Pool fences, gates that lock, alarms, safety pool covers can all help.
          Clear pool areas of toys so that children are not attracted to the
          area. Still, nothing is a substitute for the watchful eye of a
          responsible adult.
        </p>
        <p>No child should be allowed in the pool unsupervised.</p>
        <h2>What Should I Do After A Swimming Pool Accident?</h2>
        <p>
          For every child who dies from drowning, another five receive emergency
          department care for nonfatal submersion injuries.
        </p>
        <p>
          More than half of drowning victims treated in emergency departments
          require hospitalization or transfer for further care. These nonfatal
          drowning injuries can cause severe brain damage that may result in
          long-term disabilities such as memory problems, learning disabilities,
          and permanent loss of basic functioning, putting victims in a
          permanent vegetative state.
        </p>
        <p>
          The worse part about of all of these cases is that there are causally
          related to the negligence of the owner and thus 100 percent
          preventable had the property owner took the time to ensure the safety
          of others.
        </p>
        <p>
          If your loved one has been seriously injured or even died due to the
          carelessness of a pool owner, call our{" "}
          <strong> Riverside Swimming Pool Accident lawyers</strong> at
          <strong>951-530-3711 </strong>for your{" "}
          <strong> Free Case Review </strong>and <strong> Consultation</strong>.
          If you qualify for compensation, we'll fight for you and make
          recovering from your accident as easy as possible.
        </p>
        <p>
          With over <strong> 39 years</strong> of representing{" "}
          <strong> Riverside</strong> and the <strong> Inland Empire</strong>,
          we understand the pain and frustration you are currently going through
          and we want to do everything we can to help you in this time of need.
        </p>
        <p>
          Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
