// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/pedestrian-hit-medical-attention-accident-attorney-lawyer-riverside-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Pedestrian Accident Lawyers - Bisnar Chase"
        pageDescription="Call 951-530-3711 to reach our Riverside pedestrian accident attorneys who will fight for you. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Pedestrian Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="riverside pedestrian accident lawyers"
            title="riverside pedestrian accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Pedestrian Accident Attorneys</strong> at{" "}
          <strong> Bisnar Chase</strong> are ready to fight and win your case no
          matter how difficult your case becomes, our team never gives up.
        </p>
        <p>
          <strong> Riverside</strong> has many busy streets and intersections
          that can be extremely dangerous for pedestrians. Unfortunately,
          pedestrians don't have the same safety devices and a steel frame that
          drivers and passengers of vehicles can count on to protect them.
        </p>
        <p>
          When a vehicle strikes a pedestrian, it is the pedestrian who suffers
          catastrophic or fatal injuries.
        </p>
        <p>
          In fact, pedestrian accidents often tend to result in life-changing or
          permanent injuries. For these victims, it can become a struggle to get
          through each day as they struggle physically, emotionally and
          financially.
        </p>
        <p>
          If you have become a victim in this type of accident, call{" "}
          <strong> Bisnar Chase</strong> Riverside Personal Injury Lawyers now
          at <strong> 951-530-3711</strong>. Our dedicated attorneys and staff
          go the extra mile for each and every client because we know that you
          count on us during this challenging and difficult time in your life.
        </p>
        <p>
          We are here to help you. Please reach out to us for a free,
          comprehensive and confidential consultation about your pedestrian
          accident. The call could be the difference between thousands of
          dollars owed to you as a victim.
        </p>
        <h2>What Leads to a Pedestrian Accident</h2>
        <p>
          The following factors can put you and other pedestrians in harms way:
        </p>
        <ul>
          <li>
            <strong> Intoxicated Drivers</strong>
          </li>
          <li>
            <strong> Drivers Who Ignore Traffic Laws and Lights</strong>
          </li>
          <li>
            <strong> Negligent Bus Drivers and Truck Drivers</strong>
          </li>
          <li>
            <strong> Drivers Who Ignore the Right-Away of Pedestrians</strong>
          </li>
        </ul>
        <p>
          Sometimes it may be hard to predict which drivers on the road will
          pose a threat to your safety, however, staying proactive and keeping
          an eye out for warning signs may prove to protect you when you are
          walking the streets.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/6b-shU5dPm8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Types of Drivers Pedestrians Should Look Out For</h2>
        <p>
          Drivers who are distracted or on edge may pose a threat to you safety.
          If a driver is looking down at their lap at a stop light, odds are
          they are utilizing their electronic device. This practice is now
          illegal under <strong> California</strong> <strong> law</strong> as
          well. Even if their phone is fasten to their dashboard as the law
          dictates, a driver that is taking time to check their mobile device
          will have less of a chance to notice you on the sidewalk.
        </p>
        <p>
          If you can make eye contact with the driver and establish your
          presence on the road, it may be safe to cross the street legally at
          the given time. However, in some cases, it might just be better to
          wait until this driver passes the intersection If you feel this driver
          is a legitimate threat to drivers and pedestrians (the driver appears
          to be intoxicated), call 911 immediately and let the dispatcher know
          the details of your situation and the driver's license plate number if
          possible.
        </p>
        <p>
          In other instances, a driver can be distracted by playing music in
          their car at a volume that impairs their ability to drive safe and
          make safe decisions concerning the road that they are on. Playing
          music at a loud volume is legal to a certain degree, however, if the
          music is too loud and prevents the driver from navigating the road
          safely, that driver is strongly encouraged to turn down their music.
        </p>
        <p>
          Emotional distress can also play a role in a pedestrian accident. If a
          driver is experiencing road rage, it is recommended that you distance
          yourself from that driver until that driver passes. In addition, if
          you feel this driver is driving recklessly or is an overall threat to
          the road, you are on again encouraged to call 911 and report this.
        </p>
        <p>
          Emotional distress can also come in the form of sadness. Sometimes,
          life can affect a driver in ways they can not foresee. For example,
          say a driver just got news that a loved one past away. This driver may
          be so distraught, they might be focused more on their sad thoughts
          than on the road that they are navigation. In addition, the same
          driver may begin to cry and this may alter the driver's vision,
          causing them to swerve into something or someone.
        </p>
        <p>
          Finally, a fatigued driver can pose just a great of a threat to the
          road as a drunk driver. Drivers may become tired due to the demands of
          their work or these drivers may have a long day in general.
          Regardless, it is the responsibility of the driver to know if they are
          awake and energized enough to safely operate a motorized vehicle. If
          this is not the case, the driver must make accommodations to regain
          strength and alertness before driving on the road again. Failure to do
          so will most likely lead to lead punishment for becoming a hazard to
          the road.
        </p>
        <h2>Pedestrian Accident Prevention</h2>
        <p>
          The best way to avoid serious pedestrian injuries is to use caution
          each and every day. If you are walking, obey the law and only cross
          when and where it is safe to do so.
        </p>
        <p>
          Never dart into traffic or enter the roadway from between two parked
          cars where oncoming drivers may not see you. Instead, cross at
          intersections and in crosswalks when you have the right of way. You
          would also be well advised to wear bright clothing, make eye contact
          with nearby drivers and carry a flashlight when walking at night.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/crosswalk-pedestrian-accident-attorneys-riverside.jpg"
            width="100%"
            alt="crosswalk-pedestrian-accident-attorneys-riverside.jpg"
          />
        </LazyLoad>

        <h2>Serious Injuries Suffered in Riverside Pedestrian Accidents</h2>
        <p>
          Pedestrians often suffer permanent disabilities even in relatively
          low-speed collisions. When struck by a car, pedestrians can be thrown
          on to the hard asphalt or slammed on to the windshield of the vehicle.
        </p>
        <p>
          They may suffer injuries such as head trauma, broken bones and road
          rash from being dragged by a vehicle. These types of injuries require
          immediate medical attention, but the expenses related to those
          injuries do not end any time soon.
        </p>
        <h2>Recovering After Your Pedestrian Accident</h2>
        <p>
          Sustaining a <strong> serious injury</strong> can turn a victim's
          entire life upside down. Some victims are forced to miss work while
          others are never again able to reach their full earning potential.
        </p>
        <p>
          After leaving the hospital, victims of traumatic injuries such as
          concussions and spinal cord injuries often require physical and
          occupational therapy.
        </p>
        <p>
          They may experience significant changes in their physical and
          cognitive abilities. Some victims even have to modify their homes by
          installing wheelchair ramps and additional railings. These types of
          life changes are not cheap and they can prove taxing for the victim
          and his or her family.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Should You Do after a Crash?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/cyclist-injured-after-car-accident-pedestrian-attorneys-lawyers-riverside.jpg')"
            }}
          >
            <h2>What Should You Do after a Crash?</h2>
          </div>
        </LazyLoad>

        <p>
          If you are ever involved in a <strong> Riverside</strong>{" "}
          <strong> pedestrian</strong> <strong> accident</strong>, it's
          necessary to act quickly.
        </p>
        <p>
          If you are injured, get to a safe place and call the authorities. Wait
          at the location for emergency responders and exchange information with
          the driver and other parties involved. Get the involved party's phone
          number, name, license plate, address and insurance information.
        </p>
        <p>
          You should also write down the contact information for anyone who
          witnessed the accident.
        </p>
        <p>
          If you have a cell phone, start taking photos and documenting right
          away. You should get a wide-angle shot of the whole crash site as well
          as close-up photos of the vehicle, skid marks and injuries.
        </p>
        <p>
          It is also advisable to write down everything you remember about the
          crash. Details can get lost when your adrenaline is pumping. Take a
          deep breath and write down everything you remember about the crash as
          soon as possible, when your memory is at its freshest.
        </p>
        <p>
          After speaking with the police, seek medical attention. Not only will
          seeing a doctor right away increase your chances of a full recovery,
          but it will also help document your injuries and the treatment
          received. This can be useful if you need to prove the severity of your
          injuries later.
        </p>
        <p>
          Last but not least, contact an experienced{" "}
          <strong>
            {" "}
            <Link to="/riverside">personal injury lawyer</Link>
          </strong>{" "}
          in Riverside to obtain more information about pursuing your legal
          rights.
        </p>

        <h2>What is My Claim Worth?</h2>
        <p>
          Victims of <strong> pedestrian</strong> <strong> accidents</strong>{" "}
          could file a lawsuit against the at-fault driver. If you have been
          injured, financial compensation may be available for all of the
          financial and emotional losses you have suffered.
        </p>
        <p>
          Negligent drivers can be held accountable for medical bills, lost
          wages, the cost of rehabilitation services, pain and suffering,
          permanent injuries and emotional distress. An experienced injury
          lawyer can help you understand what your claim is worth and how you
          could go about pursuing fair compensation for your losses.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/car-crosswalk-bike-bicycle-pedestrian-accident-riverside-attorneys-lawyers.jpg"
            width="100%"
            alt="car-crosswalk-bike-bicycle-pedestrian-accident-riverside-attorneys-lawyers.jpg"
          />
        </LazyLoad>

        <h2>Call Bisnar Chase Pedestrain Accident Lawyers Today</h2>
        <p>
          <strong> Bisnar Chase</strong> has been representing{" "}
          <strong> Riverside accident victims</strong> for over{" "}
          <strong> 39 years</strong>. We have collected over{" "}
          <strong> $500 Million</strong> in compensation and we may be able to
          help you too. Call us now at<strong> 951-530-3711 </strong>for a{" "}
          <strong> Free Consultation.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
