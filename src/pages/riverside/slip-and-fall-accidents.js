// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/old-woman-fall-down-stairs-slip-and-fall-attorneys-lawyers-riverside.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Slip and Fall Attorneys - Bisnar Chase"
        pageDescription="Our Riverside Slip-and-Fall lawyers can help if you've been injured in a premise liability case in Riverside County. Call us now at 951-530-3711 for your free consultation. We've served Californians since 1978 and have an impressive 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Slip and Fall Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Slip and Fall Lawyer"
            title="Riverside Slip and Fall Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Slip and Fall Accident Lawyers</strong> have been winning
          cases for our clients at <strong> Bisnar Chase</strong> for over{" "}
          <strong> 39 years</strong>. With an established
          <strong> 96% Success Rate</strong>, we have won over{" "}
          <strong> $500 Million</strong> in settlements and cases.
        </p>
        <p>
          <strong> Slip-trip-and-fall accidents</strong> may not seem or sound
          dangerous and some people are lucky to get away with a bruise or a
          scratch, however, too often <strong> Riverside</strong>
          <strong> slip and fall accidents</strong> have the potential to result
          in serious or even fatal injuries.
        </p>
        <p>
          If you have been injured in a{" "}
          <strong> Riverside slip-trip-and-fall accident</strong>, it is
          important that you understand your legal rights and options.
        </p>
        <p>
          The Riverside Personal Injury Attorneys of{" "}
          <strong> Bisnar Chase</strong> are able to provide a
          <strong> Free Consultation </strong>and<strong> Case Review </strong>
          to discuss your situation and case.
        </p>
        <p>
          If you need legal help, don't wait. Call us now at
          <strong> 951-530-3711</strong> to set up your free case review.
        </p>
        <h2>Getting the Legal Help You Need</h2>
        <p>
          <strong> Negligent property owners</strong> and managers can be held
          accountable for the injuries that occur on their premises. The
          property owner can be held liable for a
          <strong> Riverside slip-trip-and-fall accident</strong> when:
        </p>
        <ul>
          <li>The owner or an employee caused the dangerous condition.</li>
          <li>
            The owner of the premises or the employee knew or should have known
            about the dangerous condition, but did nothing to rectify it.
          </li>
          <li>
            The negligence resulted in injuries to a tenant, visitor or guest.
          </li>
          <li>
            The injuries caused the victim physical, emotional and financial
            losses.
          </li>
        </ul>
        <p>
          According to the{" "}
          <Link
            to="http://www.cdc.gov/traumaticbraininjury/get_the_facts.html"
            target="new"
          >
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>
          , one third of all traumatic brain injuries in the United States are
          caused by
          <strong> slip-and-falls</strong> alone. In fact, more brain injuries
          are caused by falls than car accidents. Falls can also result in other
          injuries such as broken bones that can leave a person disabled for an
          extended period of time.
        </p>
        <p>
          Our <strong> lawyers</strong> have a long and successful track record
          of holding negligent property owners and managers accountable.
          <strong> Injured victims of slip-trip-and-fall accidents</strong> can
          seek compensation to cover damages such as medical expenses, lost
          wages, hospitalization, rehabilitation costs, permanent injury, pain
          and suffering and emotional distress.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9-UJdHzYPw4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Slip-and-Fall Related Injuries in Riverside</h2>
        <p>
          The severity of your injuries often depends on a number of different
          factors. The elderly tend to suffer the most serious injuries in{" "}
          <strong> slip-trip-and-fall accidents</strong> because their bones are
          often weaker or more brittle compared to younger adults.
        </p>
        <p>
          The position of your body or how you fall can also determine the
          nature and severity of your injuries. For example, if you fall on your
          wrist, elbow, head or hip, you could suffer a devastating bone
          fracture. If you twist your ankle, knee or wrist, you could suffer a
          debilitating tendon sprain or muscle strain. If you strike your head
          as you fall, you may sustain a<strong> traumatic brain injury</strong>{" "}
          or lose consciousness.
        </p>
        <h2>The True Cost of Being Injured</h2>
        <p>
          {" "}
          <Link to="/riverside/premises-liability">
            Premises liability
          </Link>{" "}
          injuries are not only painful, but they can also be very expensive to
          treat. Even victims with adequate health care insurance can expect to
          face mounting medical bills. The cost of emergency services,
          surgeries, hospitalization, rehabilitation services and prescription
          drugs can add up very quickly.{" "}
        </p>
        <p>
          For many, the cost of a serious injury does not end with the medical
          expenses. Individuals who have sustained a serious injury may have to
          miss work and some may never be able to return at full capacity.
          Victims of slip-trip-and-fall accidents sometimes require
          modifications to their home to make daily activities easier, which can
          also prove extremely expensive.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Fall Accident Prevention"
            style={{
              backgroundImage:
                "url('/images/text-header-images/fall-accident-prevention-slip-attorney-lawyer.jpg')"
            }}
          >
            <h2>Fall Accident Prevention</h2>
          </div>
        </LazyLoad>

        <p>
          Property owners and managers have a duty and a legal responsibility to
          ensure that their premises are safe for tenants, visitors and guests.
          There are steps property owners can and should take to ensure that
          their premises are safe. Wet surfaces are one of the most common
          causes of slip-and-trip accidents. Property owners should keep their
          floors clean and dry.
        </p>
        <p>
          When the floor is wet, it is important that warning signs are placed
          to warn visitors. All aisles and passageways must be kept clear of
          debris and other tripping hazards. Broken stairs, bunched-up carpeting
          and elevators that are not flush with the ground should be repaired.
          If a dangerous condition has not been fixed, warning signs must be
          posted. Owners must provide proper lighting on their properties as
          well to prevent these types of accidents. In the case of shopping
          centers escalators must be repaired and maintained in order to prevent
          trip-and-fall accidents.
        </p>
        <h2>What Should You Do If You Have Been Injured?</h2>
        <p>
          If you are injured in a slip-trip-and-fall accident because of a
          hazardous condition, it is important that you take a moment to collect
          yourself. Once you have regained your composure:
        </p>
        <ul>
          <li>
            Try to determine if you have been injured and if you need medical
            attention.
          </li>
          <li>
            Collect as much information as possible from the scene of the
            incident.
          </li>
          <li>
            Report the incident to the property owner and/or person(s) on duty.
          </li>
          <li>Make sure you obtain a copy of the report.</li>
          <li>
            Get medical attention right away and follow the doctor's orders.
          </li>
        </ul>
        <p>
          Obtaining photographic evidence of the accident scene can prove
          invaluable. Should you choose to file a personal injury claim later,
          physical and visual evidence from the scene can really help bolster
          your case.
        </p>
        <p>
          If you have a cell phone, take photos of the dangerous condition that
          caused your injuries. Take photographs of your injuries as well. If
          anyone witnessed the incident, obtain his or her contact information
          as well. Eyewitnesses can help validate your account of what occurred.
        </p>
        <h2>How Do I Take Action Against the Property Owner?</h2>
        <p>
          If you have fallen and sustained injury, you may be entitled to
          compensation if negligence can be proven in terms of the property
          owner and/or the on-duty staff.
        </p>
        <p>
          With over <strong> 39 years</strong> serving both{" "}
          <strong> Riverside</strong> and the <strong> Inland Empire</strong>{" "}
          collectively and a <strong> 96% win rate</strong> with our clients,
          you have nothing to lose and so much to gain when you choose{" "}
          <strong> Bisnar Chase personal injury lawyers</strong>.
        </p>
        <p>
          If we don't <strong> Win Your Case</strong>,{" "}
          <strong> You Don't Pay.</strong>
        </p>
        <p>
          Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation </strong>and{" "}
          <strong> Case Evaluation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
