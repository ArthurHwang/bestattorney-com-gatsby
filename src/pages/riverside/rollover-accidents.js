// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/rollover-car-accident-brian-chase-attorney-lawyer-riverside.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Rollover Accident Attorneys of Bisnar Chase"
        pageDescription="Riverside Rollover Car Accident Lawyers winning over $500 Million for our clients in the Inland Empire. Free consultation. Call us now at 951-530-3711."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Rollover Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Rollover Car Accident Attorneys"
            title="Riverside Rollover Car Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Riverside Personal Injury Lawyers at <strong> Bisnar Chase</strong>{" "}
          have a long and successful track record of representing seriously and
          catastrophically injured clients.
        </p>
        <p>
          We hold at-fault parties be it negligent drivers or manufacturers of
          defective vehicles accountable for the injuries and losses sustained
          by the victims. If you or a loved one has been injured in a{" "}
          <strong> Riverside rollover accident</strong>, please contact us right
          away to discuss your legal options.
        </p>
        <p>
          Call us now at <strong> 951-530-3711</strong> now for a{" "}
          <strong> Free Case Review </strong>and <strong> Consultation</strong>.
          We will help you answer all your tough questions and help you take
          legal action if you are entitled to compensation.
        </p>
        <h2>What Is A Rollover Accident?</h2>
        <p>
          A rollover accident occurs when a vehicle tips over to its side or
          completely overturns. This type of a crash can be extremely dangerous
          because it often results in catastrophic or fatal injuries.
        </p>
        <p>
          Even victims who survive{" "}
          <strong> Riverside rollover accidents</strong> suffer major injuries
          such as bone fractures, neck injuries, traumatic brain injuries and
          spinal cord injuries that result in permanent physical disabilities.
        </p>
        <p>
          These types of injuries can affect not only the lives of the injured
          victims, but also their family members.
        </p>
        <h2>Rollover Crash Statistics</h2>
        <p>
          According to the{" "}
          <Link to="http://www.nhtsa.gov" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>
          , rollover crashes account for about a third of all passenger
          fatalities. In fact, there are over 280,000 rollovers resulting in
          approximately 10,000 traffic deaths each year in the United States.
        </p>
        <p>
          You can certainly reduce your chances of being fatally injured in a
          rollover by wearing a seatbelt. Passengers who are properly buckled
          are 75 percent less likely to sustain fatal injuries in a rollover.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/PHSDRRCs-5Q"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Rollovers Involving Vehicle Defects</h2>
        <p>
          In addition to negligent drivers, rollover accidents are commonly
          caused by{" "}
          <Link to="/riverside/auto-defect-lawyers">auto defects</Link> as well.
          Rollover crashes could be caused by:
        </p>
        <ul>
          <li>
            <strong> Tire defects </strong>
          </li>
          <li>
            <strong> Brake problems </strong>
          </li>
          <li>
            <strong> Poor vehicle design </strong>
          </li>
          <li>
            <strong> Steering failure </strong>
          </li>
          <li>
            <strong> Vehicle stalling issues </strong>
          </li>
        </ul>
        <p>
          When safety features fail during a rollover crash, serious injuries
          can occur. For example, if an airbag fails to deploy in a violent
          rollover crash, the vehicle's occupants will not be protected and are
          likely to suffer major traumatic injuries. If a seat belt fails, the
          vehicle's occupants could get partially or fully ejected. If a seat
          belt collapses during a rollover collision, serious or catastrophic
          injuries could occur.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Rollover Prevention"
            style={{
              backgroundImage:
                "url('/images/text-header-images/safe-rollover-vehicle-test-car-accident-attorney-lawyer-riverside.jpg')"
            }}
          >
            <h2>Rollover Prevention</h2>
          </div>
        </LazyLoad>

        <p>
          There are a number of ways in which you can reduce your chances of
          being injured or killed in a{" "}
          <strong> Riverside rollover accident</strong>.
        </p>
        <ul>
          <li>
            <b>Mind your speed.</b> In addition to obeying the speed limit, it
            is advisable to slow down while making turns and in wet conditions.
            Speed could cause drivers to lost control of their vehicles.
          </li>
          <li>
            <b>Stay focused.</b> A sudden jerk of the wheel can cause your
            vehicle to tip over. Distracted drivers sometimes fail to notice
            changes in traffic and roadway conditions that could have been
            avoided. If a distracted driver looks up at the last moment, he or
            she may try to jerk the wheel, resulting in a rollover.
          </li>
          <li>
            <b>Stay sober and alert.</b> Drowsy and intoxicated drivers are more
            likely to let their vehicles drift off the roadway. Many rollover
            accidents occur after a car leaves the roadway when the driver
            attempts to turn back into traffic.
          </li>
          <li>
            <b>Choose the right vehicle.</b> Top-heavy vehicles are more likely
            to tip over in an accident. This is why so many rollovers involve
            large SUVs, passenger vans, minivans and pickups. Many SUVs have
            been redesigned to make them wider and lower to the ground, but they
            are still more likely to roll during an accident than smaller
            passenger cars.
          </li>
          <li>
            <b>Avoid debris.</b> Some rollovers result from debris on the
            roadway. It is dangerous to strike large debris or to jerk your
            wheel to avoid debris. Try to make slow, measured turns to avoid
            hazardous conditions and report roadway debris to the authorities.
          </li>
          <li>
            <b>Wear a seatbelt.</b> During a rollover, the momentum of the
            occupants will continue. Those who are not secured inside the
            vehicle can get partially or completely ejected. Ejection injuries
            are often catastrophic or fatal because the victim undergoes trauma
            during the initial crash, when crashing into the window and when
            striking the ground.
          </li>
          <li>
            <b>Inspect your vehicle.</b> Having properly inflated tires and well
            maintained brakes will reduce your chances of getting involved in a
            rollover crash.
          </li>
          <li>
            <b>Obey traffic signs.</b> Rollovers involving multiple vehicles
            often result from broadside collisions. If you yield the
            right-of-way at intersections and obey traffic control devices, you
            are less likely to be involved in a T-bone accident.
          </li>
        </ul>

        <h2>If You Have Been Injured In A Riverside Rollover Accident</h2>
        <p>
          If you have been hurt in a rollover accident, it is important that you
          remain calm and notify the authorities. Seek medical attention right
          away and gather as much information as possible from the scene of the
          crash.
        </p>
        <p>
          Injured victims should consult a{" "}
          <strong> Riverside Rollover Car Accident Lawyer </strong>and can seek
          compensation from the at-fault parties for damages including:
        </p>
        <ul>
          <li>Medical expenses</li>
          <li>Lost wages</li>
          <li>Hospitalization</li>
          <li>Rehabilitation</li>
          <li>Pain and suffering</li>
          <li>Lost future income</li>
          <li>Emotional distress</li>
          <li>Loss of consortium</li>
        </ul>
        <p>
          Don't hesitate any longer. Contact our award-winning{" "}
          <strong> Riverside Rollover Car Accident Attorneys</strong> at
          <strong> 951-530-3711 </strong>for a <strong> Free</strong>
          <strong> Consultation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
