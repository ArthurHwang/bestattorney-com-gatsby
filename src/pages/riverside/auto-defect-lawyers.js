// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/auto-defects-banner-image-attorneys-lawyers (1).jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Auto Defect Attorney - Bisnar Chase"
        pageDescription="The Riverside Auto Defect Lawyers of Bisnar Chase can help you recover after an accident. Call 951-530-3711 now to learn more. Free consultation with top rated defective car attorneys with a 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Auto Defect Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="defective vehicle attorneys Riverside"
            title="defective vehicle attorneys Riverside"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          <strong> Riverside Auto Defect Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong>are here to represent you and{" "}
          <strong> win your case</strong>.
        </p>
        <p>
          If you or a loved one has been injured in a car accident that you feel
          may have been caused by an auto defect, please call one of our
          experienced <strong> Riverside Auto Defect Lawyer</strong> for a{" "}
          <strong> Free Consultation </strong>and{" "}
          <strong> Case Evaluation</strong>. For immediate assistance{" "}
          <strong> Call 951-530-3711</strong> to reach our legal team.
        </p>
        <p>
          <strong> Bisnar Chase</strong> has over <strong> 40 years</strong> of
          experience representing those injured by auto defects or dangerous
          design. Our legal team consists of experienced personal injury lawyers
          with decades of trial experience battling large automakers. We've
          collected over
          <strong> $500 Million</strong> for our clients and may be able to
          represent you too.
        </p>
        <h2>What Does an Auto Defect Consist of?</h2>
        <p>
          It's common for someone to be affected by an auto defect and not even
          know it. Whether it's a car accident, personal injury or property
          damage, you could be entitled to compensation.
        </p>
        <p>
          In a case involving a defect, but not a serious injury, is not a case
          that is economically feasible to pursue for either the client or the
          law firm.
        </p>
        <p>
          Here are just a few examples of the types of{" "}
          <strong> auto defect cases Bisnar Chase</strong> has pursued{" "}
          <strong> successfully</strong>:
        </p>
        <ul>
          <li>
            A <strong> rollover </strong>crash possibly involving an SUV, car,
            truck or high-profile vehicle. Examples include{" "}
            <strong> weak roofs</strong> that may have crushed, injured or
            killed occupants or an electronic stability control (ESC) system
            that failed.
          </li>
          <li>
            Defective restraint systems can partially or fully eject someone who
            appears to be properly buckled in with a seatbelt, harness or
            restraint device.
          </li>
          <li>
            Tread seperation or other manufacturing defects can cause a tire to
            fail.
          </li>
          <li>
            Defective doorlatches can cause someone to be crushed, ejected,
            injured or killed in the event of a crash or sudden maneuver.
          </li>
          <li>
            Defective gas tanks or faulty parts that overheat can cause vehicles
            to catch fire.
          </li>
          <li>
            The failure of airbags or sudden deployment without warning can
            result in devastating consequences.
          </li>
          <li>
            Seatback failure cases consist of the car seat collapsing backwards
            in the event of a crash.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/text-header-images/crash-test-safe-car-accident-auto-defect-attorneys-lawyers.jpg"
            width="100%"
            alt="Riverside auto defect lawyers"
          />
        </LazyLoad>
        <h2>Why Are Auto Defects Dangerous?</h2>
        <p>
          Moms and dads who are driving there children to sports practice or
          teenagers driving to the mall after school are a couple of reasons why
          we must ensure the guarentee of safely functioning vehicles.
        </p>
        <p>
          With such delicate cargo there is no room for error or faulty
          products. When an auto defect causes a dangerous situation for our
          family and loved ones, it can be catastrophic and life-changing.
        </p>
        <p>
          <strong> Auto consumers</strong> spend large amounts of money to buy a
          vehicle they can trust will be a reliable source of transportation and
          protect them in the event of an accident.
        </p>
        <p>
          If you have experienced an injury, contact an experienced{" "}
          <strong> Riverside Auto Defect Attorney</strong> as soon as possible,
          to ensure you do not miss out on compensation.
        </p>
        <p>
          A junior in high school just getting their license most likely doesn't
          have enough driving experience to calmly react to a sever tire blow
          out, windshield wiper failure during a rainstorm on the freeway or
          other potentially dangerous auto defects that can occur while driving,
          and could result in serious injury or catastrophic injury.
        </p>
        <p>
          Even the most experienced drivers are not safe from auto defects. In
          the event of a crash, a seat belt could tear and not protect the
          individual from ejection, the airbag inflator could explode and send
          shrapnel throughout the occupant cabin, potentially causing severe
          injuries or death, and many other auto defects that have been reported
          to cause loss of life, catastrophic and life-changing injuries
        </p>
        <p>
          According to{" "}
          <Link
            to="https://www-odi.nhtsa.dot.gov/recalls/recallprocess.cfm"
            target="new"
          >
            NHTSA
          </Link>
          , since 1966, when the{" "}
          <Link
            to="https://www.nhtsa.gov/laws-regulations/statutory-authorities"
            target="new"
          >
            National Traffic and Motor Vehicle Safety Act
          </Link>
          , originally enacted in 1966, approximately 400 million cars, trucks,
          buses, recreational vehicles, motorcycles, and mopeds, as well as 46
          million tires, 66 million pieces of motor vehicle equipment, and 42
          million child safety seats have been recalled to correct safety
          defects.
        </p>
        <p>
          Auto and product defects are recalled to insure the safety of
          consumers and the public from unfortunate events and possible
          catastrophic situations from happening.
        </p>
        <LazyLoad>
          <img
            className="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Riverside auto defect lawyer Brian Chase"
          />
        </LazyLoad>
        <p>
          At times, taking action to get a recalled item repaired or replaced
          may seem to be an inconvenient task, but can end up saving your life.
          To see if your vehicle has any new or outstanding recall status, you
          can input you vehicle's VIN number at{" "}
          <Link to="https://www.safercar.gov/" target="new">
            SaferCars.gov
          </Link>
          . The VIN number can be found on the exterior of the lower driverside
          portion of your front windshield.
        </p>
        <p>
          <span>Brian Chase, Senior Partner:</span> “Automakers need to be held
          accountable for the dangers and risks they pose to the public.”
        </p>
        <h2>Recent Auto Defect Case Victories</h2>
        <ul>
          <li>
            $32,698,073 - Auto Defect - Seat manufacturers, Johnson Controls
          </li>
          <li>$5,000,000.00 - Auto Defect</li>
          <li>
            $3,075,000.00 - Product Defect -{" "}
            <Link to="/riverside/auto-accidents">motor vehicle accident</Link>
          </li>
          <li>$2,600,000.00 - Auto Defect</li>
          <li>$2,250,000.00 - Auto Defect - motor vehicle accident</li>
        </ul>
        <p>
          Our skilled team of <strong> Riverside Auto Defect Lawyers</strong>{" "}
          have over <strong> 39 years of experience</strong> and have won over{" "}
          <strong> $500 Million</strong> for our clients.
        </p>
        <p>
          We go the extra step in making this world a safer place and demand
          auto makers and auto part makers to keep innocent lives safe by
          enforcing a zero-tolerance for negligence or wrong-doing throughout
          the manufacturing and distribution process, all the way to the
          consumers driveway.
        </p>
        <h2>When an Auto Defect Investigation is Needed</h2>
        <p>
          All auto manufacturers have a legal responsibility to design,
          manufacture and distribute vehicles that are safe for consumers. All
          too often, auto manufacturers put drivers, vehicle occupants and other
          commuters in danger by selling poorly designed and defectively
          manufactured vehicles and parts. When defective autos result in
          injuries or fatalities, auto manufacturers can be held accountable for
          the losses of victims and their families.
        </p>
        <p>
          Whenever injuries or fatalities are reported as the result of a{" "}
          <strong> Riverside</strong> <strong> car</strong>{" "}
          <strong> accident</strong>, it will be necessary to determine what
          caused the crash. It is important that the vehicle is preserved for a
          thorough investigation by an expert who can help determine if a
          defective vehicle or auto part caused or contributed to the incident.
          Here are some of the circumstances, which may warrant such an
          investigation:
        </p>
        <ul>
          <li>
            The car's airbags failed to deploy or deployed suddenly or
            inadvertently.
          </li>
          <li>
            The accident was caused by a tire blowout or tire tread separation.
          </li>
          <li>The vehicle accelerated suddenly and went out of control.</li>
          <li>The brakes failed to work properly.</li>
          <li>
            The seat restraint system failed to keep the occupants secured in
            the seats.
          </li>
          <li>The seat belt failed to remain latched during the crash.</li>
          <li>The vehicle rolled over.</li>
          <li>The vehicle's roof caved in.</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Defective Automobile and Auto Parts Are Common"
            style={{
              backgroundImage:
                "url('/images/text-header-images/defective-automobiles-products-common.jpg')"
            }}
          >
            <h2>Defective Automobile and Auto Parts Are Common</h2>
          </div>
        </LazyLoad>
        <h2>Litigating Auto Defect Cases in Rverside</h2>
        <p>
          When it comes to rightfully seeking justice for victims of auto
          defects and auto recalls, our highly successful team of{" "}
          <strong> Riverside Auto Defect Attorneys</strong> never back down from
          a legal battle, no matter how difficult it becomes.
        </p>
        <p>
          There is a large variety of auto defect cases our lawyers and staff
          deal with on a regular basis. When it comes to litigating auto defect
          cases, investing in the case is an important aspect.
        </p>
        <p>
          Auto defect cases can become very expensive, especially going up
          against some of the biggest corporations in the world, but{" "}
          <strong> Bisnar Chase</strong> knows how to take a difficult case that
          has been turned down from multiple other firms, and get a{" "}
          <strong> winning verdict</strong>.
        </p>
        <h2>Auto Defects and Safety Recalls</h2>
        <p>
          When a number of complaints are filed with the{" "}
          <Link to="https://www.nhtsa.gov/" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          about a specific part or vehicle, an investigation may take place to
          determine if a recall is recommended. Here are some of the most common
          auto defects and examples of{" "}
          <Link
            to="http://www.nhtsa.gov/Vehicle+Safety/Recalls+&+Defects"
            target="new"
          >
            recent recalls
          </Link>
          :
        </p>
        <ul>
          <li>
            <b>Defective tires:</b> Firestone had to recall tires on Ford
            Explorers and Mercury Mountaineers after 200 people were killed and
            3,000 major injuries resulted from catastrophic tire failures and
            tread separations that caused rollover crashes.
          </li>
          <li>
            <b>Defective airbags:</b> There have been many airbag recalls in
            recent years. Takata has had one of the largest auto recalls in
            history.
          </li>
          <li>
            <b>Defective brakes:</b> It is extremely dangerous to operate a
            vehicle with defective brakes. Toyota voluntarily recalled 233,000
            Prius hybrid vehicles and 9,000 Lexus hybrid models that were
            manufactured between March and October 2009 for brake pressure
            issues
          </li>
          <li>
            <b>Defective accelerator systems:</b> Toyota recalled about 5.2
            million vehicles for pedal entrapment and floor mat problems that
            made vehicles accelerate beyond the control of the driver.
          </li>
          <li>
            <b>Defective seat belts:</b> In one of the largest recalls in
            history, Japanese and American auto companies recalled approximately
            8.8 million vehicles. The defective seatbelt buckles failed to lock
          </li>
        </ul>
        <h2>Reasons Why Bisnar Chase Takes Uncertain Cases</h2>
        <p>
          Our team of dedicated lawyers take on arduous cases, because unlike
          the other firms that decline, we have the resources and experience to
          handle the most delicate and challenging situations. Even when the
          odds are against us, our skill, experience, dedication and passion
          come through victorious in the end.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/pb3gVUmYTCY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Brian Chase discusses why there are so few vehicle recalls.
          </blockquote>
        </center>
        <h2>Riverside Auto Defect Cases with Catastrophic Injuries</h2>
        <p>
          A large number of auto defect cases our{" "}
          <strong> Riverside Lawyers</strong> and staff handle tend to be geared
          around catastrophic injuries and wrongful death. There are many
          variations of personal injury, but all should be taken into
          consideration and taken seriously.
        </p>
        <p>
          Once medical treatment is received, injuries are documented and you
          have contacted our skilled team of personal injury and auto defect
          lawyers, it's time to dive into your case and produce
          <strong> maximum compensation</strong> to pay for medical costs, pain
          and suffering, lost wages, loss of future income and whatever other
          aspects are involved to your case specifically.
        </p>
        <p>
          Even though a large percentage of these cases can result in horrible
          injuries that can be life-changing and very difficult for the victims
          and their families to cope with, compensation can reduce a significant
          amount of stress and help make life easier and less troublesome at
          this point of life.
        </p>
        <p>
          We are here to provide the best legal representation for you so you
          receive the best chances of winning your case and being awarded with
          maximum compensation.
        </p>
        <p>
          In <strong> wrongful death cases</strong>, victims' families can seek
          a settlement that covers a variety of expenses including medical
          expenses, funeral costs, loss of future income, pain and suffering and
          the lost of a loved one.
        </p>
        <p>
          In cases where severe injuries occurred, the cost of rehabilitation,
          therapy, wheelchairs, medical devices and accessories can become very
          expensive and stressful. Being compensated will help dramatically and
          hopefully bring a positive aspect to such a unfortunate situation.
        </p>
        <h2>How Bisnar Chase Can Help Riverside Plaintiffs</h2>
        <p>
          The attorneys at <strong> Bisnar Chase</strong> have established a
          very strong and respected reputation within the courtroom and legal
          system. Having the ability to battle the largest automakers and
          corporations in the world is what makes our legal system so great.
        </p>
        <p>
          Our track record of going up against these giant corporations speak
          for themselves. Our firm has a <strong> 96% success rate</strong>,
          winning over <strong> $500 Million</strong> for our clients with over{" "}
          <strong> 40 years of experience</strong>.
        </p>
        <p>
          At <strong> Bisnar Chase</strong>, our{" "}
          <strong> Riverside Auto Defect Lawyers</strong> are ready to fight for
          your rights and hold wrongdoers accountable. If you have been injured
          or have lost a loved one as the result of an auto defect, please{" "}
          <strong> call us at 951-530-3711</strong> for a free, comprehensive
          and confidential consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
