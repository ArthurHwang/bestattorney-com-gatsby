// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/car-accident-riverside-county-attorneys-lawyers-bisnar-chase-wreck-crash-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Car Accident Lawyers - Auto Collision Attorneys"
        pageDescription="The top rated Riverside car accident attorneys of Bisnar Chase can help you recover after an accident. Call 951-530-3711 now to learn more about compensation for your injuries & property damage. Free consultation & 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside CA car accident lawyer"
            title="Bisnar Chase"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong>
            If you are looking for an attorney near you, our top rated Riverside
            Car Accidents lawyers
          </strong>{" "}
          at <strong> Bisnar Chase</strong> are here to represent you and your
          car accident case. Car accidents in Riverside can put you and your
          family's lives in a tailspin in a matter of moments. One minute you
          and your family are enjoying a drive down one of California's famous
          freeways and the next minute, you hear a thunderous crash from the
          side of your vehicle.
        </p>
        <p>
          If you are lucky, this scary encounter will end that same day, but for
          some, the aftermath of an auto accident can trail you around for the
          rest of your life and reduce the quality of your life.
        </p>
        <p>
          The experienced Riverside{" "}
          <Link to="/riverside">personal injury lawyers </Link>at Bisnar Chase
          have a long and successful track record of representing auto accident
          victims and their families. We understand the physical, emotional and
          financial challenges victims and their families face when involved in
          a Riverside Car Accident.{" "}
        </p>
        <p>
          Our attorneys work on a contingency fee basis, which means{" "}
          <strong> You Don't Pay Unless We Win</strong>.{" "}
          <strong> Call 951-530-3711 </strong>for a
          <strong> Free Consultation </strong>with an experienced car accident
          lawyer in Riverside, CA and the Inland Empire for over{" "}
          <strong> 40 years</strong>.
        </p>
        <h2>What Causes Motor Vehicle Collisions in Riverside, CA?</h2>
        <p>
          Many efforts are taken to make Inland Empire roads as safe as they can
          be. Unfortunately though, the truth is that intersections such as
          Magnolia Avenue and La Sierra Avenue can set off a car accident. There
          are a number of reasons why car accidents occur so often in this city.
          Driver negligence is one of the most common causes of car wrecks. For
          example, alcohol was a contributing factor in nearly one-third of all
          traffic accident fatalities in this part of the Inland Empire.
        </p>
        <p>
          California has experienced over{" "}
          <Link
            to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812749"
            target="_blank"
          >
            37,750 traffic collisions
          </Link>
          . In{" "}
          <Link
            to="https://countyofriverside.us/WorkingHere/TrafficReports.aspx"
            target="_blank"
          >
            Riverside County
          </Link>{" "}
          deaths by traffic collisions have increased over the years. According
          to the{" "}
          <Link
            to="http://www.riversidesheriff.org/coroner/statistics.asp"
            target="_blank"
          >
            Coroner's office
          </Link>
          , 274 deaths by traffic accidents in one year.
        </p>

        <h2>The Inland Empire's Most Dangerous Intersections</h2>
        <p>
          Riverside CA is no stranger to car accidents. In fact, in the city
          alone, there has been over 320 deaths due to car accidents. There are
          areas that are more affected than others though. If you proceed with
          caution around these busy intersections you can reduce your chances of
          being involved in a tragic accident.
        </p>

        <div class="grid-wrap">
          <p>
            <strong>La Sierra Avenue and Cypress Avenue:</strong> Many have
            fallen victim to this intersection, two of them being Corey Rees and
            Mirian Malaka Saad Ibrahem. The two were involved in an accident
            that led to Rees, 27, dying later at the City Community Hospital and
            Ibrahem, 21, dying at the scene of the accident. The impact was so
            strong that both motor vehicles were catapulted into a nearby
            resident's yard.
          </p>
          <LazyLoad>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m16!1m10!1m3!1d274!2d-117.4973415!3d33.9381429!2m1!1f261.45!3m2!1i1024!2i768!4f35!3m3!1m2!1s0x80dcb6f40e5ed42d%3A0x68754a3439b9ba2f!2sLa%20Sierra%20Ave%20%26%20Cypress%20Ave%2C%20Riverside%2%20CA%2092505!5e1!3m2!1sen!2sus!4v1567539884533!5m2!1sen!2sus"
              width=""
              height=""
              frameborder="0"
              allowfullscreen=""
            ></iframe>
          </LazyLoad>
        </div>

        <div class="grid-wrap">
          <p>
            <strong>Magnolia Avenue and Tyler Street:</strong> It is the law
            that if you were involved in a car accident, you must stay at the
            scene of the collision until the police arrive, especially if you
            were at fault. Some do not adhere to this law though. Erasto Gomez
            was turning at the intersection when he{" "}
            <Link
              to="https://abc7.com/news/riverside-hit-and-run-crash-kills-teenager/1647941/"
              target="_blank"
            >
              collided into a 19-year-old driver
            </Link>
            . She was found dead at the scene. Gomez was later arrested.
          </p>

          <LazyLoad>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13054.828883947353!2d-117.4612564420665!3d33.91199458382687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb735d85a546f%3A0x8b0c688758118f04!2sTyler%20St%20%26%20Magnolia%20Ave%2C%20Riverside%2C%20CA%2092503!5e1!3m2!1sen!2sus!4v1567541186204!5m2!1sen!2sus"
              width=""
              height=""
              frameborder="0"
              allowfullscreen=""
            ></iframe>{" "}
          </LazyLoad>
        </div>

        <div class="grid-wrap">
          <p>
            <strong>Jurupa Avenue and Van Buren Boulevard: </strong>Running a
            red light can not only cost you a ticket but it can also cost you
            your life. The Press-Enterprise reported that a speeding driver who
            ran a red light at the intersection of Jurupa Avenue and Van Buren
            Blvd. fled a three-car collision. Those who were involved in the
            crash were left with critical injuries. The driver who caused the
            accident is still unknown.
          </p>

          <LazyLoad>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3261.9209116668344!2d-117.4637187849361!3d33.95861303010607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb6b17de9c14f%3A0x9ac3e087910a5a55!2sVan%20Buren%20Boulevard%20%26%20Jurupa%20Ave%2C%20Riverside%2C%20CA%2092503!5e1!3m2!1sen!2sus!4v1567541246577!5m2!1sen!2sus"
              width=""
              height=""
              frameborder="0"
              allowfullscreen=""
            ></iframe>{" "}
          </LazyLoad>
        </div>

        <div class="grid-wrap">
          <p>
            <strong>Pierce Street and Golden Avenue:</strong>{" "}
            Forty-four-year-old, Corey Goetsch was pronounced dead at the scene
            of an accident which took place by La Sierra University. Authorities
            say that he ran a red light and then crashed into another vehicle.
            He was then ejected out of his seat. Speeding is against the law and
            limits are enforced to protect you and other drivers on the road.
          </p>

          <LazyLoad>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.525064015671!2d-117.4960821849371!3d33.91675103230575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb70461fd01b7%3A0x2ae00c9445bc4093!2sGolden%20Ave%20%26%20Pierce%20St%2C%20Riverside%2C%20CA%2092505!5e1!3m2!1sen!2sus!4v1567541402627!5m2!1sen!2sus"
              width=""
              height=""
              frameborder="0"
              allowfullscreen=""
            ></iframe>{" "}
          </LazyLoad>
        </div>

        <div class="grid-wrap">
          <p>
            <strong>Alessandro Boulevard and Via Vista:</strong> At times,
            drivers are not at fault in accidents. Sometimes collisions happen
            due to severe weather conditions. A{" "}
            <Link
              to="https://www.pe.com/2019/03/06/motorist-critically-injured-in-4-vehicle-crash-in-riverside/"
              target="_blank"
            >
              Yucaipa man died in an accident
            </Link>
            had to be cut out of the motor vehicle to be transported to the
            hospital where he died later on. When weather conditions are a
            dangerous be cautious.
          </p>
          <LazyLoad>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3262.956498531732!2d-117.3505052849368!3d33.93159353152614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcafef3644eaa1%3A0x98dfb5e4813037f!2sVia%20Vista%20Dr%20%26%20Alessandro%20Blvd%2C%20Riverside%2C%20CA%2092506!5e1!3m2!1sen!2sus!4v1567541440460!5m2!1sen!2sus"
              width=""
              height=""
              frameborder="0"
              allowfullscreen=""
            ></iframe>{" "}
          </LazyLoad>
        </div>

        <p>
          If you have been involved in a motor vehicle collision at any of these
          intersections call the Riverside Car Accident Lawyers of Bisnar Chase
          today for a free case analysis.
        </p>
        <h2>
          <strong>Top 7 Most Common Causes of Car Accidents</strong> in
          California
        </h2>
        <ol>
          <li>
            <strong>Illegal cell phone use while driving </strong>
          </li>
          <li>
            <strong>Reckless driving</strong>
          </li>
          <li>
            <strong>Driving under the influence</strong>
          </li>
          <li>
            <strong>Rain and other weather conditions</strong>
          </li>
          <li>
            <strong>Neglect or lack of routine maintenance</strong>
          </li>
          <li>
            <strong>Failure to comply with traffic laws</strong>
          </li>
          <li>
            <strong>Driving severely below the posted limit</strong>
          </li>
        </ol>
        <p>
          It is important for all motorists to not only obey the speed limit,
          but also to adjust their speed relative to the traffic, roadway and
          weather conditions. Many motorists fail to yield the{" "}
          <Link
            to="https://www.bestatto-gatsby.netlify.app/riverside/pedestrian-accidents.html"
            target="_blank"
          >
            right-of-way to pedestrians
          </Link>
          , bicyclists and other motorists when they enter an intersection or
          make a turn.
        </p>
        <p>
          A significant number of intersection accidents involve a failure to
          obey stop signs and traffic lights. In addition, drivers who are
          talking on their cell phone or texting are less likely to be alert.
          Distracted driving causes more than 500,000 accident injuries annually
          nationwide.
        </p>
        <p>
          <Link
            to="https://www.bestatto-gatsby.netlify.app/riverside/parking-lot-accidents.html"
            target="_blank"
          >
            Parking lot accidents
          </Link>{" "}
          is another major concern for motorist and pedestrian safety. More
          accidents happen in parking lots than any other location. Simply put,
          people disobey basic motor vehicle laws in parking lots and parking
          structures. This is also a heavily contended argument between defense
          and plaintiff counsel as to who was actually at fault.
        </p>
        <p>
          Finally, driving under the influence of alcohol and/or drugs can be
          extremely dangerous and can result in serious injury crashes.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/distracted-drivers-riverside-car-accidents-attorneys-lawyers.jpg"
            width="100%"
            alt="car accident lawyers in Riverside"
            title="what causes car accidents in Riverside County?"
          />
        </LazyLoad>

        <h2>
          <strong> Top 7 Most Common Causes of Car Accidents</strong> in
          California
        </h2>
        <p>
          The following is a list of the{" "}
          <strong> Top 7 Most Common Causes of Car Accidents</strong>, not just
          for Riverside County, but regardless of where you are in California:
        </p>
        <ol>
          <li>
            <strong>
              Illegal cell phone use while driving according to California cell
              phone law
            </strong>
          </li>
          <li>
            <strong> Reckless driving</strong>
          </li>
          <li>
            <strong> Driving under the influence</strong>
          </li>
          <li>
            <strong> Rain and other weather conditions</strong>
          </li>
          <li>
            <strong> Neglect or lack of routine maintenance</strong>
          </li>
          <li>
            <strong> Failure to comply with traffic law</strong>
          </li>
          <li>
            <strong> Driving severely below the posted limit</strong>
          </li>
        </ol>
        <p>
          It is important for all motorists to not only obey the speed limit,
          but also to adjust their speed relative to the traffic, roadway and
          weather conditions. Many motorists fail to yield the right-of-way to
          pedestrians, bicyclists and other motorists when they enter an
          intersection or make a turn.
        </p>
        <p>
          A significant number of intersection accidents involve a failure to
          obey stop signs and traffic lights. In addition, drivers who are
          talking on their cell phone or texting are less likely to be alert.
          Distracted driving causes more than 500,000 accident injuries
          nationwide.
        </p>
        <p>
          Finally, driving under the influence of alcohol and/or drugs can be
          extremely dangerous and can result in serious injury crashes.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/WRC9j2rCPeQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Types of Riverside Car Accident Injuries</h2>
        <p>
          Car accident injuries could range from minor to catastrophic or even
          fatal. Some of the common injuries suffered in Riverside car accidents
          include:
        </p>
        <ul>
          <li>
            <strong> Traumatic brain injuries</strong>
          </li>
          <li>
            <strong> Head injuries such as concussions</strong>
          </li>
          <li>
            <strong> Sprains and strains</strong>
          </li>
          <li>
            <strong> Soft tissue injuries </strong>
          </li>
          <li>
            <strong> Neck and back injuries</strong>
          </li>
          <li>
            <strong> Spinal cord damage</strong>
          </li>
          <li>
            <strong> Broken bones</strong>
          </li>
          <li>
            <strong> Abrasions</strong>
          </li>
          <li>
            <strong> Amputations</strong>
          </li>
          <li>
            <strong> Internal organ damage</strong>
          </li>
          <li>
            <strong> Burn injuries</strong>
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Happens After an Auto Accident in Riverside?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/what-happens-after-a-car-accident-attorneys-riverside-california.jpg')"
            }}
          >
            <h2>What Happens After an Auto Accident in Riverside?</h2>
          </div>
        </LazyLoad>

        <p>
          If you have been hurt in a <strong> Riverside Auto Accident</strong>,
          motorcycle accident or{" "}
          <Link to="/riverside/bus-accidents">bus accident</Link> it is critical
          that you remain calm and take the following steps to protect your
          rights:
        </p>
        <ul>
          <li>Remain at the scene of the crash</li>
          <li>Notify the Riverside Police Department or Sheriff</li>
          <li>
            Write down the contact information, insurance and driver's license
            details of the drivers involved
          </li>
          <li>
            Collect information from anyone who may have witnessed the crash
          </li>
          <li>Take photos of the crash site and of your injuries</li>
          <li>Seek medical attention right away</li>
        </ul>
        <h2>The Cost of an Auto Accident</h2>
        <p>
          When a victim is injured in a car accident, it can have tremendous
          financial repercussions as well. Victims may have to bear medical
          expenses and cost of hospitalization as well as negotiate with
          insurance companies for a settlement that will repair their damaged
          vehicle or replace it altogether. These victims may have to suffer
          wage loss as a result of being unable to work as they recover from
          their injuries.
        </p>

        <p>
          It is important in such cases that victims have a thorough
          understanding of their legal rights and options after a car accident.
          An experienced auto accident lawyer can help you with this as well as
          take action against the at-fault party or the insurance companies
          involved.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/two-people-car-accident-wreck-insurance-attorneys-lawyers (1).jpg"
            width="100%"
            alt="riverside car accident"
          />
        </LazyLoad>
        <h2>How to Seek Compensation After a Riverside Car Crash</h2>
        <p>
          If you have been injured in a <strong> Riverside Car Accident</strong>
          , it is possible to receive compensation for your injuries, damages
          and losses. Injured victims can seek compensation from the at-fault
          parties for damages such as medical expenses, lost wages,
          hospitalization, cost of rehabilitation, permanent injuries,
          disabilities, pain and suffering and emotional distress.
        </p>
        <p>
          When negligence or wrongdoing is involved in a fatal crash, families
          of deceased victims can file a
          <strong>
            {" "}
            <Link to="/riverside/wrongful-death">
              Riverside wrongful death
            </Link>{" "}
            claim
          </strong>{" "}
          seeking compensation for damages such as medical and funeral costs,
          lost future income and benefits and loss of love and companionship.
        </p>
        <h2>When Should I Hire a Riverside Car Accident Attorney</h2>
        <p>
          Car accidents in Riverside happen every day. People are left stressed
          and confused about who to turn to for help. Many choose to file a
          claim with their insurance company first and settle for the amount
          that is given to them. The amount that is provided though may not
          cover the costs for medical expenses, lost wages or property damage.
          Sometimes insurance companies of the negligent party take their time
          on the claim. This could lead to you acquiring more bills and this can
          delay the process of you getting your life back on track.
        </p>
        <p>
          If you believe that you have not received the compensation you deserve
          contact the Riverside car accident attorneys of Bisnar Chase. Our
          legal representation has helped countless families get their feet back
          on the ground.{" "}
          <strong> Call 951-530-3711 for free legal advice</strong>.{" "}
        </p>

        <h2>Riverside Car Accident Lawyers You Can Trust</h2>
        <p>
          Many victims are not aware of their legal rights after an accident and
          this can affect the amount of compensation they can receive. Let{" "}
          <strong> Bisnar Chase</strong> represent you and your car accident
          case. With over <strong> 40 years </strong>of experience and over{" "}
          <strong> $500 Million won for our clients, </strong>we have
          established a<strong> 96% Success Rate</strong>. We have been
          protecting injured claimants for decades and are very familiar with
          the courts and defense teams in Riverside County. We have built a
          reputation as one of the top leading California personal injury law
          firms.
        </p>
        <p>
          If you've been injured in a car crash, contact the Riverside CA Car
          Accident Attorneys of Bisnar Chase now at{" "}
          <strong> 951-530-3711</strong> for a
          <strong> Free Consultation</strong> to discuss your legal options.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .grid-wrap p {
    padding: 1rem;
    margin-bottom: 0;
  }
  .grid-wrap {
    border: 2px solid grey;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 15px;
    margin-bottom: 20px;
    background-color: #eee;
    padding-left: 10px;

    iframe {
      height: 100%;
      width: 100%;
    }
  }
  @media (max-width: 640px) {
    .grid-wrap {
      grid-template-columns: initial;
      grid-template-rows: 1fr 1.5fr;
      padding-left: 0;
      grid-gap: 0;
    }
    .grid-wrap p {
      padding: 10px !important;
      padding-top: 5px !important;
    }
  }
  .checked {
    color: gold;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
