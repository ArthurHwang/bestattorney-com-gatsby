// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite",
  sidebarLinks: {
    title: "Riverside Injury Information",
    links: [
      {
        linkName: "Dangerous Breeds",
        linkURL: "/dog-bites/dangerous-dog-breeds"
      },
      {
        linkName: "Factors Causing Canine Aggression",
        linkURL: "/dog-bites/factors-causing-canine-aggression"
      },
      {
        linkName: "First Aid For Dog Bites",
        linkURL: "/dog-bites/first-aid-dog-bites"
      },
      {
        linkName: "Injury & Infections from Dog Bites",
        linkURL: "/dog-bites/injury-infections-dog-bites"
      },
      {
        linkName: "Dog Bite Prevention",
        linkURL: "/dog-bites/prevention-tips"
      },
      {
        linkName: "Dog Bite Statistics",
        linkURL: "/dog-bites/statistics"
      },
      {
        linkName: "Victim's Rights",
        linkURL: "/dog-bites/victim-rights"
      },
      {
        linkName: "Why Dog's Bite",
        linkURL: "/dog-bites/why-dogs-bite"
      },
      {
        linkName: "California Dog Bite Laws",
        linkURL: "/dog-bites/california-dog-bite-laws"
      },
      {
        linkName: "Criminal Penalties",
        linkURL: "/dog-bites/criminal-penalties"
      },
      {
        linkName: "Insurance Breed Bans",
        linkURL: "/dog-bites/insurance-ban-breeds"
      },
      {
        linkName: "Riverside Home",
        linkURL: "/riverside"
      }
    ]
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/attack-from-a-dog-animal-accident-personal-injury-attorney-lawyer-riverside-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Dog Bite Lawyers - Animal Attack Attorney"
        pageDescription="Call 951-530-3711 now if you have been involved in an animal attack accident. Our experienced attorneys in Riverside will fight for your rights. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Dog Bite Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Dog Bite Injury Attorneys"
            title="Riverside Dog Bite Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Dog Bite Attorneys </strong>at{" "}
          <strong> Bisnar Chase </strong>have won over{" "}
          <strong> $500 Million </strong>for our clients, with over{" "}
          <strong> 40 years </strong>of experience and an established{" "}
          <strong> 96% Success Rate</strong>.
        </p>
        <p>
          Dogs may be man's best friend, but some dogs can also be dangerous
          because they are unpredictable and territorial. Regardless of the
          breed, dog attacks can cause painful injuries, permanent scars,
          disfigurement and mental toil. These preventable attacks can cause
          both physical and emotional damage to victims.
        </p>
        <p>
          If you've suffered from a dog bite injury, contact our Riverside{" "}
          <Link to="/riverside">personal injury lawyers</Link> now at
          <strong> 951-530-3711 </strong>to discuss your legal rights. Your
          consultation is free and you may be entitled to compensation.
        </p>
        <h2>What Types of Injuries Can Come from Dog Bites in Riverside?</h2>
        <p>
          In California, owners can be held financially responsible for injuries
          their pets cause.
        </p>
        <p>
          Under{" "}
          <strong>
            {" "}
            <Link
              to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=3342."
              target="new"
            >
              California's strict liability statute
            </Link>
          </strong>
          , <strong> dog owners</strong> can be held liable for injuries caused
          by their pets regardless of whether the animal has attacked before or
          regardless of whether the dog's owner knew of its prior{" "}
          <strong> viciousness</strong>.
        </p>
        <p>The follow injuries can be caused from dog bite accidents:</p>
        <ul>
          <li>
            <strong> Penetrative Wounds</strong>
          </li>
          <li>
            <strong> Broken Bones</strong>
          </li>
          <li>
            <strong> Infections/Rabies</strong>
          </li>
          <li>
            <strong> Deep Wounds/Lacerations</strong>
          </li>
          <li>
            <strong> Limp Amputation</strong>
          </li>
          <li>
            <strong> PTSD (Post Traumatic Stress Disorder)</strong>
          </li>
          <li>
            <strong> Brain Injury</strong>
          </li>
        </ul>
        <h2>California Compensation for Injuries</h2>
        <p>
          Under most circumstances, injured dog bite victims can seek
          compensation for damages thanks to California's strict liability
          statute concerning dog bite injuries. When you hire a Riverside dog
          bite lawyer, they will be able to share how much compensation you will
          be awarded.
        </p>
        <p>
          <strong>
            {" "}
            Damages that you can receive compensation for include:{" "}
          </strong>
        </p>
        <ul>
          <li>
            <strong> Medical Expenses </strong>
          </li>
          <li>
            <strong> Lost Wages </strong>
          </li>
          <li>
            <strong> Hospitalization </strong>
          </li>
          <li>
            <strong> Surgeries </strong>
          </li>
          <li>
            <strong> Permanent Injuries </strong>
          </li>
          <li>
            <strong> Pain and Suffering </strong>
          </li>
          <li>
            <strong> Emotional Distress </strong>
          </li>
        </ul>
        <p>
          <strong> Compensation</strong> in these cases can come from a dog
          owner's homeowners insurance policy, however, proceed with caution in
          these cases. Insurance companies like to low-ball on settlement offers
          to keep profit margins high, and if you take a low-ball offer, you can
          no longer pursue additional compensation for your injuries.
        </p>
        <p>
          An experienced dog bite attorney in Riverside can help you obtain fair
          and full compensation for their losses. The California law offices of
          Bisnar Chase have even successfully handled many challenging cases
          where the owners have tried to blame the victims.{" "}
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/aGlklcSADWA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Dog Bite Statistics</h2>
        <p>
          According to statistics gathered and reported on Dogsbite.org, an
          advocacy group for victims of attacks, there were{" "}
          <strong> 38 fatal dog attacks</strong> in the past year. Of those
          deaths, <strong> 19</strong> were adults 21 or older and the other
          half were children <strong> 8</strong> or younger. This included{" "}
          <strong> 15 children</strong> who were 2 or younger.
        </p>
        <p>
          Roughly <strong> a third of the victims</strong> were either visiting
          or living temporarily with the dog's owner. California and North
          Carolina led fatalities in the past year. Between 2005 and now,
          California was second in the nation with <strong> 28 deaths</strong>{" "}
          behind Texas, which had <strong> 32.</strong>
        </p>
        <p>
          The majority of dog bite attacks do not result in death according to
          the{" "}
          <Link
            to="https://www.cdc.gov/features/dog-bite-prevention/index.html"
            target="new"
          >
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>
          . About 4.5 million people are bitten annually of which roughly half
          are children. Approximately one in five of those bitten require
          medical attention.
        </p>
        <h2>Dog Breeds Most Prone to Dog Bite Accidents</h2>
        <p>
          It is true that the way dogs are treated contributes to their
          behavior. There are many great owners out there who are capable of
          raising well-behaved canines. That does not, however, change the fact
          that pit bulls and rottweilers are statistically the most dangerous
          breeds.
        </p>
        <p>
          In the past year, pit bulls contributed to{" "}
          <strong> 61 percent</strong> of all fatal dog attacks despite making
          up only <strong> 5 percent</strong> of the U.S. dog population. From
          2005 to now, pit bulls and rottweilers together accounted for{" "}
          <strong> 73 percent</strong> of all recorded dog-related deaths.
        </p>
        <p>
          Pit bulls are statistically responsible for the death of a person in
          the United States<strong> every 19 days</strong>, and a rottweiler
          kills someone <strong> every 91 days</strong>.
        </p>
        <h2>Preventing Riverside Dog Bites Accidents and Injuries</h2>
        <p>
          <strong> Riverside County</strong> requires that dogs are spayed or
          neutered. This is important because dogs that are spayed or neutered
          are less inclined to become aggressive. Owners can also prevent
          attacks by keeping their pets on a leash and by keeping them indoors.
        </p>
        <p>
          If you are an owner, never leave infants and young children alone with
          a dog, and do not do aggressive activities such as wrestling. If your
          dog seems unfriendly or aggressive, seek advice from a trainer or
          specialist.
        </p>
        <p>
          You can reduce your chances of being bitten by avoiding dogs that are
          eating, sleeping or nursing puppies. Avoid direct eye contact with a
          dog you do not know and never approach an unfamiliar animal. It is not
          advisable to run or scream as that may cause the dog to give chase.
          Instead, remain motionless like a tree. If knocked over, you should
          roll into a ball and be still.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/girl-dog-bite-injury-emergency-medical-care-attorneys-lawyers-riverside.jpg"
            width="100%"
            alt="dog bite injuries in Riverside, CA"
          />
        </LazyLoad>

        <h2>What To Do If You've Been Bitten By a Dog In Riverside</h2>
        <p>
          Determine the severity of your{" "}
          <Link to="/riverside/premises-liability">premises liability</Link>{" "}
          injuries after the dog bite accident. Call the authorities and seek
          out immediate medical attention. Once you have taken care of your own
          well being, do the following:{" "}
        </p>
        <ul>
          <li>
            Determine who owns the dog that attacked you. If you cannot
            establish this on your own, ask the neighbors surrounding the
            location of the dog bite accident.
          </li>
          <li>
            Write down the owner's information and the contact information from
            anyone who witnessed the attack.
          </li>
          <li>Take photos of the site of the accident and your injuries.</li>
          <li>Keep records of your medical expenses and lost wages.</li>
          <li>
            Report the incident to animal control and health services. This will
            establish a record of the dog's dangerous behavior.
          </li>
          <li>
            Write a journal documenting your daily struggles since the accident.
          </li>
        </ul>
        <p>
          You should also try to determine if the dog is up to date on all of
          its vaccinations.
        </p>
        <h2>Take Legal Action by Hiring A Riverside Dog Bite Attorney</h2>
        <LazyLoad>
          <img
            src="/images/newport beach platinum chamber of commerce member.webp"
            alt="Riverside Chamber of Commerce Member"
            width="291"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Contact the <strong> Riverside Dog Bite Lawyers </strong>now at
          <strong> the law offices of Bisnar Chase 951-530-3711 </strong>for a
          free case review of your <strong> dog bite case</strong>. You will
          most likely be
          <strong> entitled to compensation</strong> that will cover the cost of
          your pain, suffering and medical bills. In addition, our firm operates
          on a contingency basis. This means in the rare case that we can't
          deliver you your compensation, you don't have to pay us at all.
        </p>
        <p>
          {" "}
          <Link
            to="https://www.chamberofcommerce.com/riverside-ca/1335523916-bisnar-chase-personal-injury-attorneys"
            target="_blank"
          ></Link>
          Bisnar Chase is a{" "}
          <Link
            to="https://www.chamberofcommerce.com/riverside-ca/1335523916-bisnar-chase-personal-injury-attorneys"
            target="_blank"
          >
            {" "}
            proud member of the Riverside Chamber of Commerce
          </Link>
          . We have been serving injured plaintiffs in our community for over
          four decades and will fight for you.
        </p>
        <p>
          With so little to lose and so much to gain, why wait any longer? Call
          us now at <strong> 951-530-3711</strong> for your{" "}
          <strong> Free Consultation.</strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
