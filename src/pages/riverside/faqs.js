// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/attack-from-a-dog-animal-accident-personal-injury-attorney-lawyer-riverside-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="FAQS: Hiring Bisnar Chase Riverside Personal Injury Attorneys"
        pageDescription="frequently asked questions about Bisnar Chase personal injury attorneys in Riverside."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Frequently Asked Questions about our Riverside Attorneys</h1>
        <BreadCrumbs location={location} />
        <ul className="faq-list">
          <li className="faq" data-elementid="faq1">
            <p className="faq-title down-arrow">
              {" "}
              <Link to="/riverside/faqs#faq1">
                <span>
                  Why should I hire Bisnar Chase to handle my injury case?
                </span>
              </Link>
            </p>
          </li>
          <li className="faq" data-elementid="faq2">
            <p className="faq-title down-arrow">
              {" "}
              <Link to="/riverside/faqs#faq2">
                <span>
                  Will I be expected to pay the entire up front costs associated
                  with my case?
                </span>
              </Link>
            </p>
          </li>
          <li className="faq" data-elementid="faq3">
            <p className="faq-title down-arrow">
              {" "}
              <Link to="/riverside/faqs#faq3">
                <span>How does your law firm charge attorney fees?</span>
              </Link>
            </p>
          </li>
          <li className="faq" data-elementid="faq4">
            <p className="faq-title down-arrow">
              {" "}
              <Link to="/riverside/faqs#faq4">
                <span>
                  What should I expect a personal injury attorney to do for me?
                </span>
              </Link>
            </p>
          </li>
          <li className="faq" data-elementid="faq5">
            <p className="faq-title down-arrow">
              {" "}
              <Link to="/riverside/faqs#faq5">
                <span>Will I be able to get my medical bills paid for?</span>
              </Link>
            </p>
          </li>
        </ul>

        <div className="divider"></div>

        <div className="content-well">
          <h2 id="faq1">
            Why should I hire the Riverside personal injury attorneys of Bisnar
            Chase to handle my injury case?
          </h2>
          <p>
            We know you have a lot of options when it comes to{" "}
            <Link to="/riverside">Riverside personal injury attorneys</Link>{" "}
            which is why we have made it our priority to give you something you
            can't find at every law firm. Attorneys who are compassionate,
            connected to the community, experienced before the courts, and
            skilled in getting you what you deserve. All of this with a promise
            to shield you from liability.
          </p>

          <h3>Bisnar Chase Community Connection</h3>
          <p>
            When you hire Bisnar Chase you are putting your trust in attorneys
            who have a strong connection to the local community. Our attorneys
            are deeply involved in the communities of the clients they serve.
            They are familiar with its courts and judges and therefore they
            always know what to expect when it's time to bring your case to
            trial.
          </p>

          <h3>Riverside California Court Experience</h3>
          <p>
            Bisnar Chase understands what the law should be so we advocate it
            every chance we get. In fact, our advocacy before California's
            Appeals Court and Supreme Court has resulted in the California's
            Supreme Court adopting our views in procedural matters in personal
            injury lawsuits and making our opinions the law of the land.
          </p>
          <p>
            In the same way we convinced the California Supreme Court to adopt
            our views, we know how to convince a claims adjuster about the
            benefits to the insurance company of the settlement we propose. As a
            result we obtain meaningful and sometimes unimaginable monetary
            recoveries for our clients, all with less wasted time, effort,
            stress and worry for everyone involved.
          </p>

          <h3>Compassionate Injury Attorneys Serving Riverside</h3>
          <p>
            The attorneys at Bisnar Chase focus on personal injury cases because
            we find a great deal of personal satisfaction in helping people like
            you who have been injured due to no fault of your own, obtain
            financial compensation and a sense of justice. We are concerned for
            your well being and take every case as seriously as if it were our
            own. This is why we've always been on your side.
          </p>
          <p>
            Unlike many attorneys, we have never represented insurance companies
            or corporations and we never plan to. Our goal is to help people who
            have been injured or taken advantage of by insurance companies or
            corporations and we'll fight to the end to make sure you get the
            compensation your deserve.
          </p>
        </div>

        <div className="content-well">
          <h2 id="faq2">
            Will I be expected to pay the entire up front costs associated with
            my case?
          </h2>
          <p>
            No. Your Riverside County attorneys advance all costs for things
            such as hiring experts, conducting discovery and preparing for
            trial. This provision is clearly stated in the Fee Agreement you
            sign when you hire us. The only circumstance where you would have to
            pay back those costs is if we win your case. If we don't recover
            money for you, you pay nothing. If we do not win your case, you will
            not pay. We provide protection throughout by advancing all fees and
            costs until your case is settled.
          </p>

          <p></p>
        </div>

        <div className="content-well">
          <h2 id="faq3">How does your law firm charge attorney fees?</h2>
          <p>
            Riverside personal injury attorneys work solely on a contingency
            fee. This means that we don't collect a dime from you unless we win
            your case. If we win your claim, we'll never get more from your case
            than you do. We don't take on cases unless we think we can win them.
            We know you have a lot of healing to do whether it's from emotional,
            physical or financial hardship and we want you to concentrate on
            taking care of yourself, not stressing over losing more time and
            money. Our job is to win your case and get you the compensation you
            deserve.
          </p>
        </div>

        <div className="content-well">
          <h2 id="faq4">
            What should I expect a personal injury attorney to do for me?
          </h2>
          <p>
            When you hire Bisnar Chase you can expect your personal injury
            lawyer to be an educator, advisor, communicator, representative and
            your advocate all at the same time. Your lawyer will educate you
            about your rights, the legal basis of your claim, and damages you
            may be eligible to recover. Your attorney will advise you throughout
            the process of recovery (physical, emotional, or economical) as to
            what to expect and how to best care for yourself during the process.
            As your voice in your dealings with the defendants and their
            representatives your attorney will be an effective and honest
            representative as well as a tireless advocate for your rights.
            You'll never have to wonder if someone is looking out for you and
            protecting your interests.
          </p>
        </div>

        <div className="content-well">
          <h2 id="faq5">Will I be able to get my medical bills paid for?</h2>
          <p>
            This depends on your situation, but you may be entitled to receive
            compensation for:
          </p>

          <ul>
            <li>Medical and hospital costs</li>
            <li>Physical disability</li>
            <li>Mental anguish and therapy bills</li>
            <li>Pain and Suffering</li>
            <li>Lost wages</li>
            <li>Property damages</li>
          </ul>
        </div>

        <p>
          If you've been seeking a reliable personal injury attorney in
          Riverside, call us today for a{" "}
          <Link to="/contact">free case evaluation</Link> and zero obligation.
          We'll discuss your case and give you an honest answer to whether we
          can help you or not. <b>Call </b>(951) 530-3711
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
