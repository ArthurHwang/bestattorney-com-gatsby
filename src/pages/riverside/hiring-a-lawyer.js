// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/riverside-personal-injury-accidents-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hire A Personal Injury Lawyer In Riverside CA"
        pageDescription="How to hire an experienced personal injury lawyer in Riverside. Tips to find the right attorney for your injury case. Call 951-530-3711 for a Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>How to Hire a Riverside Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="How to Hire A Riverside Personal Injury Lawyer"
            title="How to Hire A Riverside Personal Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you need to hire the services of a{" "}
          <Link to="/riverside" target="new">
            Riverside Personal Injury Lawyer
          </Link>
          , contact us now at <strong> 951-530-3711</strong> for a
          <strong> Free</strong> <strong> Case</strong>{" "}
          <strong> Evaluation </strong>to discuss your legal options.
        </p>
        <p>
          <strong> Riverside personal injury attorney Brian Chase</strong> and
          team are <strong> seasoned trial lawyers</strong>. They know what it
          takes to win big cases and also know what you should avoid.
        </p>
        <p>
          Finding the right lawyer is a huge challenge as there are so many
          personal injury lawyers practicing in Riverside County. It can be hard
          to know where to start.
        </p>

        <p>
          In addition, you're probably not all that happy about having to find
          an attorney in the first place and it makes sifting through all of the
          possibilities even more difficult.
        </p>
        <p>
          How do you find a good lawyer and what do you do once you have one?
          Don't worry, we did all the hard work for you.
        </p>
        <p>
          This website will make the process of hiring a lawyer an easy one.
          We'll tell you what to keep in mind, what to avoid, what questions you
          should be asking and how to know when you have found the right one for
          you.
        </p>
        <p>Continue reading to learn more.</p>
        <h2>What to Look For When Hiring an Attorney in Riverside</h2>
        <p>Not all lawyers handle all types of cases.</p>
        <p>
          If you or your loved one has been injured in an accident, you need a
          personal injury attorney as soon as possible. Personal injury
          attorneys specialize in cases dealing with injury or even death caused
          by negligence on the part of a product, company, group or individual.
          These attorneys are highly specialized and they know how to build your
          case, how to negotiate your case with the responsible parties or
          insurance companies, and, if necessary, how to take your case to trial
          and win it.
        </p>
        <p>
          Not all personal injury attorneys are created equal. Just because
          someone calls themselves a personal injury attorney, this does not
          mean he fits the bill. You need to talk to the person and ask a lot of
          questions concerning your accident. (See below for a list of questions
          to ask your attorney).
        </p>
        <p>
          Don't get caught up with fears about money prematurely. There are a
          lot of people out there who have been injured because of someone
          else's fault but decide not to seek legal advice because they think
          they cannot afford it.
        </p>
        <p>
          Worry not about money because many personal injury lawyers operate on
          contingency basis, meaning if they lose your case or do not deliver
          you an adequate settlement, you do not pay a dime.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title=" What to Avoid When You Need to Hire a Personal Injury Lawyer"
            style={{
              backgroundImage:
                "url('/images/text-header-images/lawyers-riverside-to-avoid-accidents-how-to-find-an-attorney.jpg')"
            }}
          >
            <h2>
              What to Avoid When You Need to Hire a Personal Injury Lawyer
            </h2>
          </div>
        </LazyLoad>

        <p>
          Choosing a personal injury attorney based off proximity to where you
          live is never a good idea. Convenience is always nice to have, but it
          should never take priority over skill, experience, expertise,
          professionalism, integrity, compassion and the ability to effectively
          represent your case. These factors should take priority over all else
          when deciding who you will pick to represent you in a court of law.
        </p>
        <p>
          Referrals can be great way to find a lawyer fast, but you have to make
          sure you're choosing someone that can truly represent you in court.
          More than likely your friend's circumstances were different than yours
          and that may change your potential outcome all together. You need to
          find the attorney that is best for your situation alone.
        </p>
        <p>
          You should also avoid picking an attorney from a generic
          advertisement. Just because someone advertises on TV or has a huge ad
          in the yellow pages or has a catchy song, it does not mean that this
          lawyer is right for you. Most of the biggest and most successful firms
          never advertise on TV.
        </p>
        <p>
          Now that you know what's important to keep in mind and what to avoid,
          now it's time to get out there and find several that you can
          interview. Here are three of the best and easiest ways to start
          building your list of options.
        </p>
        <h2>How to Tell if an Attorney is Right for You</h2>
        <p>
          Ask your friends or family if they know any attorneys. This is not
          because you want to use the one they did necessarily but because you
          want more options to choose from. Attorneys know other attorneys, so
          if you start with one, you'll more than likely be referred to others.
        </p>
        <p>
          Tell them what happened and that you are looking for a personal injury
          attorney who specializes in your type of case. Most attorneys are
          happy to refer you to others because they rely on referrals for their
          business as well.
        </p>
        <ul>
          <li>
            Search the Web. Most attorneys and firms have a website. Do a search
            for your geographical area and the type of attorney you need.
          </li>
          <li>
            Call your{" "}
            <Link to="http://www.riversidecountybar.com/" target="new">
              local bar association
            </Link>{" "}
            and tell them what you are looking for.
          </li>
        </ul>
        <p>
          Once you have a list of a few attorneys you think match your needs,
          get your questions together and make some phone calls.
        </p>
        <p>
          At Bisnar Chase, we offer free consultations. This means that we'll
          meet with you wherever it is convenient (in your home or hospital room
          if that is best for you) and we'll listen to your story and talk about
          all of the options you have. At your consultation, you are free to ask
          us any questions you have about your case and our firm.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/nbnjoHTHLNQ"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>
          Questions You Should Ask Your Potential Riverside Personal Injury
          Attorney
        </h2>
        <ul>
          <li>What are your credentials?</li>
          <li>Do you specialize in personal injury law?</li>
          <li>
            What is your experience in handling cases like mine? How much of
            that experience has been in the last few years' What were the
            outcomes of those cases?
          </li>
          <li>What percentage of your cases are personal injury cases?</li>
          <li>How long have you been practicing personal injury law?</li>
          <li>
            Do you have former clients that would be willing to speak with me?
          </li>
          <li>
            How big is your caseload? Where would my matter fit with your
            priorities?
          </li>
          <li>Will you be the primary attorney handling my case?</li>
          <li>
            How many other people will be working on it and what is their level
            of experience?
          </li>
          <li>What are the chances that my case will go to trial?</li>
          <li>Do I have a say in how my case progresses?</li>
          <li>
            What can I expect over the next few weeks or months, How long does
            it take to resolve a case like this?
          </li>
          <li>
            Based on your experience, what is the likelihood of a positive
            outcome in my case, What problems do you see?
          </li>
        </ul>
        <h2>What Can I Expect For My Case?</h2>
        <p>
          Be aware that most personal injury attorneys concentrate their efforts
          in a value range. At Bisnar Chase, we rarely ever take a case to trial
          that has a realistic value under $50,000. Our firm is geared to handle
          the larger cases. Some firms handle smaller cases because they don't
          have the resources to handle the larger ones.
        </p>
        <p>
          We can, however, help you reach a settlement with the at-fault party
          if the dollar value of your case is below $50,000. The best way to
          determine the value of your case and if it worth it to settle would be
          to discuss the detail of your case at our free consultation.
        </p>
        <p>
          The majority of Riverside County personal injury law firms are going
          to charge a fee based upon a percentage of the money they recover for
          you. It is important that you check to see specifically what their fee
          percentage is because this in turn will affect the amount of
          compensation you will receive.
        </p>
        <p>
          Make sure the fee structure is spelled out clearly in the retainer
          agreement before you move forward. Our fee structure is graduated,
          meaning that the easier and quicker the case, the lower the fee and
          the more challenging and time consuming the case, the higher the fee.
          Most cases start with a fee of 25%.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/brian-chase-in-court-hiring-a-lawyer-bisnar-chase-riverside-county-california.jpg"
            width="100%"
            alt="Riverside Personal Injury Attorney Brian Chase"
          />
        </LazyLoad>

        <h2>How Much Will My Case Cost?</h2>
        <p>
          Most attorneys will advance the costs of pursuing your claim (which
          could be in excess of $25,000). However, if your case is not
          successful, the best attorneys won't make you repay those costs. Make
          sure you know up front how your attorney will handle this issue. It is
          well advised to ask these questions to your potential attorney during
          your consultation:
        </p>
        <ul>
          <li>
            What is your general estimate of how much this will cost me when
            it's all said and done?
          </li>
          <li>How will we communicate about my case?</li>
          <li>
            Will you keep me updated on my case' How often can I expect updates?
          </li>
          <li>Will everything be kept confidential?</li>
          <li>
            Is there anything that might get in the way of you being able to
            represent me?
          </li>
        </ul>
        <p>
          Once you've gotten all of your questions answered, ask yourself: are
          you comfortable with him or her and do you trust them?
        </p>

        <p>
          Trust your instincts. If you are not comfortable or if you do not
          clearly trust the attorney, that's your signal to keep looking. If you
          feel good about the attorney and trust them, you have found the
          attorney for you. Once you have chosen an attorney, take that next
          step and get started on pursuing legal action. You deserve
          compensation for your injuries and the sooner you get going, the
          sooner you can move on.
        </p>
        <p>
          If you're looking for a firm with dedicated, experienced,
          compassionate and honest attorneys, Bisnar Chase may be the perfect
          fit for you. Give us a call right now so that you can tell us your
          story. Remember, our consultations are <strong> Free</strong> and
          there's absolutely no risk to you. Our{" "}
          <strong> Riverside Personal Injury Attorneys</strong> have been
          serving clients in
          <strong> Riverside County </strong>for over <strong> 39 years</strong>{" "}
          since 1978.
        </p>
        <p>
          Call us today at <strong> 951-530-3711</strong> for your free case
          review. We will fight for you!
        </p>

        <div
          className="youtube-video"
          data-youtube="https://www.youtube.com/embed/lVUUCtM3Ebg"
        ></div>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
