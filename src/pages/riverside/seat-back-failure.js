// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/seat-back-injuries-test-riverside-seat-failure-attorneys-car-accidents-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Seat Back Failure Attorneys - Bisnar Chase"
        pageDescription="Contact the Riverside seat back failure lawyers if you've been injured in a car accident where the seat failed and injured you or a passenger. Free consultation and hundred of millions recovered. Call 951-530-3711."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Seat Back Failure Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Seat Back Failure Lawyers"
            title="Riverside Seat Back Failure Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Seat Back Failure Lawyers</strong> are here to help
          victims who have experienced injuries, traumatic experiences or have
          lost loved ones in the result of a<strong> car accident</strong>{" "}
          resulting in a <strong> seat back failure</strong>.
        </p>
        <p>
          When auto makers intentionally put out defective cars on the road for
          the sake of profit, the consumer bears the cost of safety. Not only is
          this unfair, this is also unjust and automakers must compensate
          individuals who have been hurt by their defective automobiles.
        </p>
        <p>
          Get the help you need right away. Contact the experienced Riverside
          personal injury attorneys at
          <strong> Bisnar Chase</strong> for a{" "}
          <strong> Free Case Consultation</strong>.
        </p>
        <p>
          We have obtained successful verdicts and settlements in{" "}
          <strong> Riverside seat-back failure cases</strong> on behalf of
          injured victims and their families. We know what it takes to fight
          against large automakers whose negligence causes these types of
          catastrophic product failures.
        </p>
        <p>
          Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation</strong>.
        </p>

        <h2>Tragic Injuries and Fatalities</h2>
        <p>
          According to the{" "}
          <Link to="http://www.autosafety.org/" target="new">
            Center for Auto Safety
          </Link>
          , 64 adults and 50 children die each year as a result of collapsing
          seatbacks.
        </p>
        <p>
          One of <strong> Bisnar Chase’s</strong> clients, Jaklin Romine, was
          paralyzed in a 2006 Pasadena crash in which her seat collapsed. Romine
          is a powerful voice now for victims and consumers calling for change.
        </p>
        <p>
          We secured a $24.7 million verdict for Romine, which stood on appeal
          by the defendant, Johnson Controls, manufacturer of the seat. Romine’s
          seatback collapsed during a rear-end collision at a street
          intersection.
        </p>
        <p>
          She suffered catastrophic head and spinal cord injuries in spite of
          wearing a seatbelt because her <strong> seat broke</strong> on impact,
          collapsed backward causing her body to submarine rearward under the
          seatbelt and shoulder restraint. Her head hit the back of the rear
          passenger seat causing her to suffer the severe injuries that left her
          a permanent quadriplegic.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/roAIK55nNWs"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Seeking Legal Support</h2>
        <p>
          Victims of <strong> seat back failures</strong> have the right to
          pursue financial compensation for their losses. Depending on the cause
          of the crash, they may be able to file a personal injury claim against
          the driver responsible for the collision as well as a product
          liability claim against the auto manufacturer.
        </p>
        <p>
          When an individual is killed in a{" "}
          <strong> seat back failure accident</strong>, his or her family can
          pursue compensation for their loss by filing a wrongful death claim
          against the automaker.
        </p>
        <p>
          A successful claim should include financial compensation for all of
          the losses suffered in the accident including medical bills, pain and
          suffering, lost wages, loss of earning potential, the cost of
          rehabilitation services and other related damages.
        </p>
        <p>
          Many preventable injuries occur each year as a direct result of weak
          and defectively designed seats and their components. Poorly designed
          and manufactured seat backs, recliner mechanisms and seat tracks put
          drivers and passengers at risk of suffering devastating injuries.
        </p>
        <p>
          Seat backs play a significant role in the seat restraint system of the
          vehicle, and when they fail to work properly, occupants can suffer
          life-changing neck, head and spinal cord injuries.
        </p>

        <h2>What is Seatback Failure?</h2>
        <p>
          Seatbacks are an integral part of a vehicle's restraint system. They
          work with seat belts and airbags to keep occupants in place in the
          event of a crash.
        </p>
        <p>
          When a collision occurs, a seatbelt will secure an occupant against
          the seat, an airbag will cushion the driver's head from the steering
          wheel and a seat-back will keep the occupant upright.
        </p>
        <p>
          If a seat fails to stay upright in the event of an accident, the
          effectiveness of the seat belt and airbag is virtually eliminated.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Why Seat Back Failures Are Dangerous"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dangerous-seat-back-failure-rear-end-car-accident-riverside-attorneys-lawyers.jpg')"
            }}
          >
            <h2>Why Seat Back Failures Are Dangerous</h2>
          </div>
        </LazyLoad>

        <p>
          An object in motion stays in motion. When a car comes to a sudden
          halt, the contents of the vehicle will continue to move unless they
          are restrained by a seatbelt. If a seat fails during a rear-end
          collision, the driver or front seat passenger could slip right out
          from under the seatbelt.
        </p>
        <p>
          This means that the front seat occupant in the defective seat may fly
          backwards into the rear seat, the rear seat passenger or even through
          the rear windshield.
        </p>
        <h2>Injuries Suffered in Riverside Seatback Failure Accidents</h2>
        <p>
          Front seat occupants can suffer life-threatening injuries in a seat
          back failure accident. If the back of the victim's head strikes the
          rear seat, it is possible for him or her to suffer a traumatic brain
          injury and neck trauma.
        </p>
        <p>
          In severe cases, victims suffer spinal cord trauma that lead to
          paralysis.
        </p>
        <p>
          Those who survive with only serious but not catastrophic injuries may
          suffer bone fractures, whiplash, sprains, strains and lacerations.
        </p>
        <p>
          In cases where the front seat collapses and strikes a rear seat
          passenger, the injuries can prove fatal and result in a wrongful
          death. <strong> Seat-back collapses</strong> commonly occur even in
          slow-speed, rear-end{" "}
          <Link to="/riverside/auto-accidents">car collisions</Link>.{" "}
        </p>
        <h2>Government Oversight</h2>
        <p>
          One of the reasons why there are so many seat back failure incidents
          is because the current standard for seat strength was adopted back in
          1968. Many believe the standard needs to be updated because it only
          requires seats to withstand an impact of 270 foot-pounds.
        </p>
        <p>
          In comparison, auto manufacturers are required to install seat belts
          that can withstand 6,000 pounds of force without failing.
        </p>
        <p>
          Furthermore, the current testing requirement does not involve
          real-world crashes. Manufacturers are only required to measure how
          much force the seat withstands before failing.
        </p>
        <p>
          This lack of oversight allows <strong> auto manufacturers</strong> to
          cut corners by installing inadequate seats and seat parts that can
          fail in even low-speed collisions.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/child-girl-back-seat-seat-back-failure-car-seat-car-accident-riverside-attorneys-lawyer.jpg"
            width="100%"
            alt="child-girl-back-seat-seat-back-failure-car-seat-car-accident-riverside-attorneys-lawyer.jpg"
          />
        </LazyLoad>

        <h2>Seatback Recalls</h2>
        <p>
          When a significant number of people file similar complaints about a
          specific auto part, the{" "}
          <Link to="https://www.nhtsa.gov/recall-spotlight" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          may conduct an investigation. Depending on the results of their
          findings, the auto manufacturer may be required to issue a recall.
        </p>
        <p>
          There have been a number of recalls in recent years involving{" "}
          <strong> defective seat backs</strong>. For example:
        </p>
        <ul>
          <li>
            Chevy Silverado and GMC Sierra pickups from model year 2014 were
            recalled for issues with the seat-backs and headrests. This recall
            involved 18,972 pickups that had seats that moved in the event of a
            rear-end collision.
          </li>
          <li>
            Tesla Model S cars from model year 2013 were recalled for a weakened
            connection between the left seat-back striker and the bracket
            connected to the frame of the vehicle.
          </li>
          <li>
            2012 Nissan Jukes were recalled because they have an incomplete weld
            that allows the rear seat back striker to become separated in a
            crash.
          </li>
          <li>
            Ford Explorer from model year 2011 was recalled for possible moving
            center <strong> seat backs</strong>.
          </li>
        </ul>
        <h2>Attorneys You Can Trust</h2>
        <p>
          In any case, dealing with a seat back accident can be a trying
          experience. If you or a loved one has been involved in a{" "}
          <strong> seatback failure accident in Riverside</strong>, contact an
          injury attorney at <strong> Bisnar Chase</strong> right away at
          <strong> 951-530-3711.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
