// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/nursing-home-riverside-attorneys-abuse-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Nursing Home Abuse Lawyers - Bisnar Chase"
        pageDescription="Call our Riverside nursing home abuse attorneys at 951-530-3711 if your loved one has been negliected or hurt. Free consultations. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Nursing Home Abuse Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Nursing Home Abuse Attorneys"
            title="Riverside Nursing Home Abuse Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Nursing Home Abuse Attorneys</strong> are here for
          you, your family and your loved ones, when neglect, abuse or any other
          unacceptable acts are committed.
        </p>

        <p>
          Elderly neglect in nursing homes is a serious matter that shouldn't be
          taken lightly. When we put our elderly loved one in a nursing home, we
          except their staff to care of our family with the utmost love and
          patience.
        </p>

        <p>
          Unfortunately, not all nursing homes act or feel this way and because
          of this, many cases of elder abuse go unreported each year.
        </p>

        <p>
          Contact the Riverside Personal Injury Lawyers of Bisnar Chase now at{" "}
          <strong> 951-530-3711</strong> for a
          <strong> Free Consultation</strong>{" "}
          <strong> concerning your case</strong>.
        </p>

        <h2>What is Elder Abuse?</h2>

        <p>
          <strong> Elder abuse</strong> is a term referring to any knowing,
          intentional, or negligent act by a caregiver or any other person that
          causes harm or a serious risk of harm to a vulnerable older adult.
        </p>
        <p>
          There are thousands of seniors who are neglected, abused and exploited
          each year. These victims are often frail, medicated and/or incapable
          of caring for or protecting themselves. Examples of nursing home abuse
          that can occur at any <strong> Riverside</strong> care facility
          include:
        </p>

        <ul>
          <li>
            <b>Physical abuse:</b> It is illegal to inflict injury or pain on an
            elderly patient. These types of cases may involve the slapping,
            pushing, hitting, restraining or kicking of seniors.
          </li>
          <li>
            <b>Sexual abuse:</b> Sexual abuse cases include all forms of
            unwanted sexual contact. It is important that nursing homes run
            backgrounds checks on their potential employees to ensure residents'
            safety.
          </li>
          <li>
            <b>Emotional abuse:</b> It can be just as harmful to inflict
            emotional pain as physical pain. Emotional abuse may involve
            intimidation, threats and humiliation.
          </li>
          <li>
            <b>Neglect:</b> This refers to the failure to provide adequate care
            such as failing to provide food, water, shelter, protection,
            medications and a clean and safe environment.
          </li>
          <li>
            <b>Exploitation:</b> Financial abuse involves incidents such as
            stealing, credit card fraud, check fraud or illegally taking money
            from the victim's bank account. Altering financial documents such as
            wills is also a form of abuse.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/elderly-woman-abuse-bruised-attorneys-lawyer.jpg"
            width="100%"
            alt="elder abuse lawyers of Riverside "
          />
        </LazyLoad>

        <h2>Nursing Home Care in California</h2>
        <p>
          In a 2013{" "}
          <Link to="http://www.bmj.com/content/344/bmj.e1717" target="new">
            study
          </Link>{" "}
          by Families for Better Care, each state was given a grade for the
          level of care provided at their nursing homes. In that study,
          California received a "C" rating. There are a number of reasons why
          California received such an average grade.
        </p>
        <p>
          For example, California ranked 36th in the country for the amount of
          time residents receive attention from care providers. California
          nursing home residents only receive about 43 minutes per day with
          professional nursing home staff. Furthermore, about nine out of 10
          facilities statewide were cited for a deficiency. Only 10 percent,
          however, were cited for a severe deficiency.
        </p>
        <p>
          If you or a loved one is being neglected of attention deficiency,
          contact one of our <strong> Riverside Nursing Home Lawyers </strong>at{" "}
          <strong> (951) 530-3711</strong>.
        </p>
        <h2>Warning Signs of Elder Abuse</h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/1iI3ZGIkzgs?rel=0&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          <em>Brian Chase explains the warning signs of elder abuse.</em>
        </p>
        <p>
          There are many potential warning signs of abuse in nursing homes. If
          your loved one has bruises, pressure marks, abrasions or bone
          fractures that are not listed on his or her medical records, it could
          be a telltale sign of abuse. Other common warning signs include:
        </p>
        <ul>
          <li>Unexplained withdrawal from normal activities</li>
          <li>Change in alertness</li>
          <li>Bedsores, poor hygiene and weight loss</li>
          <li>Strained relationships and frequent arguments</li>
          <li>Strained relationships and frequent arguments</li>
          <li>Unhygienic conditions</li>
          <li>Sudden changes in finances or missing property</li>
          <li>
            unexplained injuries such as a{" "}
            <Link to="/riverside/slip-and-fall-accidents">slip and fall</Link>{" "}
            injury
          </li>
        </ul>
        <p>
          It is always advisable to look for signs of abuse when you visit your
          loved one. Attempt to visit during different times of the day so that
          you can meet the morning, afternoon and night shift workers. Ask about
          the health of your loved one and try to take an active role in their
          care.
        </p>
        <h2>What to Do If You Suspect Nursing Home Abuse</h2>
        <p>
          All suspected cases of physical or sexual abuse must be reported to
          law enforcement authorities. This is the duty of all nursing home
          staff and failure to do so is not only illegal, but is also
          negligence.
        </p>
        <p>
          <strong> Call 911</strong> or the local police if there is immediate
          danger. After you have reported your concerns, an investigation should
          take place. Depending on the circumstances of your case, there are a
          number of organizations that may prove useful:
        </p>
        <ul>
          <li>
            Adult Protective Services can help you determine if wrongdoing has
            occurred.
          </li>
          <li>
            Local police may conduct an investigation if physical injuries have
            been suffered.
          </li>
          <li>
            A long-term care ombudsman can help you resolve your complaints with
            the nursing home.
          </li>
          <li>
            A <strong> nursing home abuse lawyer</strong> can help you pursue{" "}
            <strong> financial compensation</strong> from the party or parties
            responsible for the abuse.
          </li>
        </ul>
        <p>
          If you choose to report an issue or file a lawsuit, you may be able to
          receive support for your loved one's losses. Your actions may also
          help prevent future incidents by exposing neglect and wrongdoing.
        </p>

        <h2>Hiring a Lawyer for Your Loved One</h2>
        <p>
          It is unacceptable for nursing homes to take advantage of one of the
          most vulnerable sections of our society. Families entrust their loved
          ones to the care of these facilities and it is simply wrong for
          nursing homes to violate that trust.
        </p>
        <p>
          At <strong> Bisnar Chase</strong> we believe that{" "}
          <strong> negligent nursing homes</strong> should be held accountable.
          We have more than three decades of experience protecting the rights of
          injured victims and their families in <strong> Riverside</strong> and
          the <strong> Inland Empire</strong>.
        </p>
        <p>
          If you or a <strong> loved one</strong> has been the{" "}
          <strong> victim of neglect</strong> or <strong> abuse</strong> at a{" "}
          <strong> elderly care facility</strong>, please call our
          <strong> Riverside</strong> <strong> elder abuse attorneys</strong> at
          <strong> 951-530-3711</strong> for a <strong> Free</strong>,{" "}
          <strong> Comprehensive</strong> and
          <strong> Confidential Consultation</strong>.
        </p>
        <div className="gmap-addr">
          <p itemScope="" itemType="http://schema.org/Attorney">
            <span itemProp="name" className="gmap-title">
              Bisnar Chase Personal Injury Attorneys
            </span>
            <span
              itemProp="address"
              itemScope=""
              itemType="http://schema.org/PostalAddress"
            >
              <span itemProp="streetAddress">6809 Indiana Ave #148</span>
              <span itemProp="addressLocality">Riverside</span>,{" "}
              <span itemProp="addressRegion">CA</span>{" "}
              <span itemProp="postalCode">92506</span>{" "}
            </span>
            <span itemProp="telephone">(951) 530-3711</span>
          </p>
        </div>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
