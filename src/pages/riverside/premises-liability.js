// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/premise-liability-open-pothole-road-stairs-slip-and-falls-accidents-attorneys-lawyers-riverside-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Premises Liability Attorneys - Bisnar Chase"
        pageDescription="Call us now at 951-530-3711  if you have been injured on another person's property. Our Riverside Premises Liability Lawyers can help you seek justice."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Premises Liability Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="riverside premises liability lawyers"
            title="riverside premises liability lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Premises Liability Lawyers</strong> can help with
          personal injury cases that occur as the result of unsafe conditions on
          another person's property.
        </p>
        <p>
          Types of of <strong> premises liability cases</strong> include a{" "}
          <strong> slip and-fall accident</strong>, dog attacks, fires, swimming
          pool accidents, or assaults that occur due to inadequate security or
          lighting. These types of dangerous conditions could exist in a range
          of properties from malls and supermarkets to apartment complexes,
          sports facilities and government buildings.
        </p>
        <p>
          The experienced Riverside{" "}
          <Link to="/riverside"> personal injury attorneys</Link> at{" "}
          <strong> Bisnar</strong> <strong> Chase</strong> have successfully
          representing clients in the Southern California area for over 40
          years. We have experience handling a variety of claims against
          individuals, entities, property owners and even large corporations
          whose negligence caused our clients serious injury or harm.
        </p>
        <p>
          For more information about pursuing your rights and to set up your{" "}
          <strong> Free Consultation</strong>, call us now at
          <strong> 951-530-3711</strong>.
        </p>
        <h2>What Are the Legal Obligations of a Property Owner?</h2>
        <p>
          All property owners and managers in <strong> Riverside County</strong>{" "}
          have a legal obligation to exercise reasonable care to keep their
          premises safe for visitors, guests and tenants.
        </p>
        <p>
          Often, liability in such cases is determined based on who has the
          right to control the property. In other cases, several parties could
          be held liable. Identifying the negligent or responsible parties in a
          premises liability case can be a big part of the challenge.
        </p>
        <p>
          Property owners in <strong> Riverside County</strong> not only have a
          duty to keep their premises safe, but also a duty to warn visitors to
          their property about hazardous conditions that may exist.
        </p>
        <p>
          For example, if a broken stair presents a tripping hazard, the
          property owner should make every effort to fix it promptly. In the
          interim, he or she should put up a visible cautionary sign at the
          staircase warning people about the dangerous condition there.
        </p>
        <p>
          Property owners could also be held liable in cases where an unsafe
          condition caused an injury. If, for example, someone was assaulted in
          a poorly-lit parking lot or robbed because of inadequate security, the
          property owner could be held liable for the victim's losses for
          negligent security.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/security-guard-negligent-riverside-accident-attorneys-lawyer.jpg"
            width="100%"
            alt="security-guard-negligent-riverside-accident-attorneys-lawyer.jpg"
          />
        </LazyLoad>

        <h2>How to Prove Fault in a Premises Liability Claim</h2>
        <p>
          Under premises liability law, a business owner or a property owner can
          be held liable for injuries that have occurred on their properties. As
          with any <strong> personal injury</strong> case, the element of{" "}
          <strong> negligence</strong> plays a significant part in a premises
          liability case. A plaintiff must prove several elements in order to
          receive compensation.
        </p>
        <p>
          First, the plaintiff must show that the property owner owed a duty to
          the injured victim to exercise reasonable care in keeping the premises
          safe and eliminate any hazardous conditions on the premises.
        </p>
        <p>
          This duty of care could vary depending on the status of the person
          visiting the property. Also, the plaintiff must show that the property
          owner and/or manager breached this duty of care that was owed to him
          or her.
        </p>
        <p>
          The plaintiff must also prove that the property owner's action or lack
          of action caused his or her injuries. The injury must have been a
          direct result of the property owner's negligence.
        </p>
        <p>
          For example, if a supermarket employee failed to clean up spilled
          laundry detergent, ice cubes on the floor or water in one of the
          aisles or failed to warn customers about the spill by putting cones or
          tape around the spill, then it would be foreseeable that the failure
          to clean up the spill would cause a customer to{" "}
          <Link to="/riverside/slip-and-fall-accidents"> slip and fall. </Link>
        </p>
        <p>
          Plaintiffs in <strong> premises</strong> <strong> liability</strong>{" "}
          <strong> claims</strong> are also required to prove that they have
          suffered damages or losses as a result of the injury. Damages could
          include medical expenses, lost wages, hospitalization, surgeries,
          physical therapy expenses, pain and suffering and emotional distress.
        </p>
        <h2>Governmental Entities and Premises Liability Claims</h2>
        <p>
          Under California law, governmental entities can be held liable in{" "}
          <strong> premises liability cases</strong>, however, there are strict
          time limitations to file personal injury claims against governmental
          agencies.
        </p>
        <p>
          Under{" "}
          <Link
            to="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="new"
          >
            California Government Code Section 911.2
          </Link>
          , any personal injury or wrongful death claim against a governmental
          entity must be filed within 180 days or six months after the incident.
          Governmental agencies do have some immunity against injury claims.
        </p>
        <p>
          A governmental agency, for example, cannot be held liable for injuries
          caused by its planning decisions. There is also an exception to{" "}
          <strong> government liability</strong> for discretionary acts,
          however, if a government employee acts negligently or with malice,
          there will be no immunity.
        </p>
        <h2>What to Do if You Have Been Injured on Riverside Property</h2>
        <p>
          If you have been injured on someone else's property, the first step is
          to notify property management about the incident. Make sure you obtain
          a copy of the incident report. It would also be helpful to write down
          the name and contact information for anyone who witnessed the
          accident.
        </p>
        <p>
          If you have your smartphone with you, take photographs and video of
          the scene. Show the entire area and photograph the{" "}
          <strong> dangerous conditions</strong> that led to your injuries. You
          should also take photos of your injuries and continue taking
          photographs as you heal. Write down everything you remember about the
          incident and keep a journal documenting your day-to-day struggles
          since the incident.
        </p>
        <p>
          It is also advisable to seek medical attention as soon as possible.
          Receiving early diagnosis and treatment will increase your chances of
          having a full recovery. Your medical records can also serve as
          documentation of the injuries you suffered and the treatment you
          received.
        </p>
        <h2>If You Were Injured on Friend or Relatives Property...</h2>
        <p>
          Most homeowners do not know that when they purchase a home they also
          take on the responsibility to make sure others are safe on their
          property. To establish that you have been injured on a homeowners
          property you would first need to prove that the homeowner was
          negligent, the homeowners negligence caused your injuries and that
          from the accident you have acquired many medical bills.{" "}
        </p>
        <p>
          A Riverside premises liability lawyer will be able to provide you
          insight on what your claim is worth.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/qvkV_ShDjAE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What is Your Riverside Claim Worth?</h2>
        <p>
          Although each claim and case is unique, in general, a successful
          premises liability claim is one that will fairly and fully compensate
          you, the plaintiff.
        </p>
        <p>
          Individuals who have suffered a serious injury that required
          hospitalization, surgery and time away from work will receive a larger
          settlement than those who were not injured or required minimal medical
          attention. However, your medical bills are only a fraction of what
          your claim is worth.
        </p>
        <p>
          A <strong> Riverside</strong> premises liability claim could also
          compensate individuals for other damages and losses such as:
        </p>
        <ul>
          <li>
            <strong> Lost of Income</strong>
          </li>
          <li>
            <strong> Cost of Rehabilitative Treatment </strong>
          </li>
          <li>
            <strong> Pain and Suffering</strong>
          </li>
          <li>
            <strong> Emotional Distress </strong>
          </li>
        </ul>
        <p>
          In egregious cases, the at-fault party may even have to pay punitive
          damages. Make sure you understand the potential value of your claim
          before accepting a settlement. Do not rush into a settlement from
          insurance companies before you have talked about your case with your
          attorney.
        </p>
        <h2>Get the Justice and Compensation You Deserve</h2>
        <p>
          At <strong> Bisnar Chase</strong>, we represent and stick up for the
          "little guys." We realize that big government and corporations can be
          scary and that they protect their interest above else with their
          lawyers and team. This is why we choose to represent victims of
          premise liability because they do not have this same legal team or
          voice in the courtroom.
        </p>
        <p>
          When you choose <strong> Bisnar Chase</strong>, you a choosing a law
          firm that has over <strong> 39 years</strong> of experience in{" "}
          <strong> Riverside</strong> and the
          <strong> Inland Empire</strong> and will also not stop fighting for
          you until we deliver you the justice and settlement that you deserve.
          In addition, we are a contingency based firm which means in the rare
          case that we lose, you do not pay us. We say "rare" because we have a{" "}
          <strong> 96% Success Rate</strong> when it comes to our clients.
        </p>
        <p>
          With so little to lose and so much to gain, why wait any longer to see
          justice be served?
        </p>
        <p>
          If you have been injured due to a negligent property owner and/or
          staff call us today at<strong> 951-530-3711</strong> to set up your{" "}
          <strong> Free Case Review</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
