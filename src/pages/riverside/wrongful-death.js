// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/wrongful-death-mourning-accident-fatal-injury-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Wrongful Death Attorney - Bisnar Chase"
        pageDescription="The Riverside wrongful death lawyers of Bisnar Chase will help you seek justice if you've lost a loved one in a Riverside wrongful death case. Call us now at 951-530-3711. Free consultation & hundreds of millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Wrongful Death Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Wrongful Death Attorney"
            title="Riverside Wrongful Death Attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Wrongful Death Attorneys</strong> at{" "}
          <strong> Bisnar Chase</strong> have an immense amount of experience,
          professionalism and compassion for our clients. With over
          <strong> 39 years</strong> of experience, our team of lawyers have
          established a <strong> 96% Success Rate</strong>, winning over{" "}
          <strong> $500 Million</strong> for our clients.
        </p>
        <p>
          If someone you love has died because of someone else's negligence, you
          may be entitled to make a wrongful death claim. We can help you get
          the compensation you deserve.
        </p>
        <p>
          For immediate help, or to get a <strong> Free Case Evaluation</strong>
          , please contact our Riverside personal injury attorneys of{" "}
          <strong> Bisnar Chase</strong> to discuss your legal options.
        </p>
        <p>
          Our award winning fatal accident attorneys, litigation team, and
          research experts have years of experience dealing with these
          complicated cases and the resources to take them on.
        </p>
        <p>
          {" "}
          <Link to="/riverside/contact">Contact</Link> or Call us today at{" "}
          <strong> 951-530-3711</strong> to set up your{" "}
          <strong> Free Case Review </strong>and{" "}
          <strong> Complimentary Consultation</strong>.
        </p>
        <h2>Elements of a Wrongful Death Case</h2>
        <p>
          Wrongful death is any death caused by a negligent, careless,
          intentional or reckless act of another person or corporation.
        </p>
        <p>
          <b>
            The required elements for a Riverside wrongful death case include:
          </b>
        </p>
        <ul>
          <li>Death of person (Fatal accident or injury</li>
          <li>That death was caused by the negligence of another</li>
          <li>
            There are surviving family members who are monetarily suffering due
            to that death
          </li>
          <li>
            There must be a personal representative appointed to bring the suit
            on behalf of the decedent's estate.
          </li>
        </ul>
        <p>
          If you are able to prove that you will experience severe future
          emotional and financial hardship as a result of the{" "}
          <strong> fatal injury</strong> or <strong> accident</strong> of this
          person, per{" "}
          <Link
            to="http://scholarship.law.berkeley.edu/cgi/viewcontent.cgi?article=3762&context=californialawreview"
            target="new"
          >
            California law
          </Link>{" "}
          there are three kinds of damages you may be entitled to claim:
        </p>
        <ul>
          <li>
            The loss of the love, companionship, comfort, affection, society,
            solace, moral support, and (if a spouse is a claimant) consortium of
            the decedent
          </li>
          <li>
            The value of the household services the decedent would have provided
            in the future
          </li>
          <li>
            The value of the financial support which the claimant would have
            received from the decedent but for the death.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_okWKolf9qc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Who Can File a Wrongful Death Claim in Riverside?</h2>
        <p>
          Before anyone can recover any damages, first you have to determine
          whom is actually authorized to seek wrongful death damages.
        </p>
        <p>
          As a general rule, the first people in line are the surviving spouse,
          children and the surviving issue of deceased children of the decedent.
          If these relationships do not exist, then the parents, siblings,
          children of deceased siblings, grandparents and their lineal
          descendants may make a claim.
        </p>
        <p>
          The line continues further down to the putative spouse (a person who
          can prove that he or she had a good faith belief that he or she was
          married to the decedent but was, in fact, not married to the
          decedent), children of the putative spouse, stepchildren, and parents
          of the decedent.
        </p>
        <h2>How to Prove Negligence in a Wrongful Death Case</h2>
        <p>
          These factors can aid you in seeking compensation after the{" "}
          <strong> wrongful death</strong> of your loved one:
        </p>
        <ul>
          <li>
            Proof that a third party's absence or lack of action led to the
            wrongful death.
          </li>
          <li>
            Proving the action taken by the at-fault that lead to fatal injuries
            with negligence or vicious intent involved.
          </li>
          <li>
            Proving the financial toil the death of your loved one has taken on
            you and our family.
          </li>
        </ul>
        <p>
          Laws concerning <strong> wrongful death</strong> vary and this will
          determine how much compensation you and your family will get. The
          factors that determine how much compensation you will get includes:
        </p>
        <ul>
          <li>Last earning wage of your loved one.</li>
          <li>Your financial dependency on your loved one.</li>
          <li>The amount of funds your loved one saved</li>
          <li>The total loss of companionship</li>
          <li>The total costs that came after your loved one's death</li>
        </ul>
        <p>
          There is also a time limit to file a <strong> lawsuit</strong> for a{" "}
          <strong> wrongful death</strong>. If you plan to take legal action, we
          urge you to do it now.
        </p>

        <h2>How to Pick a Wrongful Death Attorney in Riverside?</h2>
        <p>
          <strong> Wrongful death cases</strong> are complicated and you need an{" "}
          <strong> attorney</strong> who knows wrongful death law inside and out
          and someone who has a success track record.
        </p>
        <p>
          When you choose <strong> Bisnar Chase</strong>, you can take comfort
          in our <strong> 39 years</strong> serving our clients in the{" "}
          <strong> Inland Empire</strong> with a
          <strong> 96% Success Rate</strong>.
        </p>
        <p>
          We know that the emotional, psychological and financial hardship you
          are going through leaves you little time for understanding all of the
          legal issues involved. That's why we do everything we can to take the
          burden off of you so you can take care of yourself and your other
          family members during this incredibly difficult time.{" "}
          <strong> </strong>
        </p>
        <p>
          Contact our <strong> Riverside</strong> <strong> Wrongful</strong>{" "}
          <strong> Death</strong> <strong> Lawyers</strong> about your{" "}
          <strong> case</strong> today at <strong> 951-530-3711.</strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
