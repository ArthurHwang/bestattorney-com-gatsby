// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/riverside-personal-injury-accidents-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Riverside",
      population: 319504,
      totalAccidents: 13035,
      intersection1: "Magnolia Ave & Tyler St",
      intersection1Accidents: 86,
      intersection1Injuries: 73,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 303.6,
      city1Name: "Moreno Valley",
      city1Index: 260.9,
      city2Name: "Corona",
      city2Index: 162.2
    },
    locationBox3: {
      intersection2: "Van Buren Blvd & Arlington Ave",
      intersection2Accidents: 78,
      intersection2Injuries: 64,
      intersection2Deaths: 1,
      intersection3: "Van Buren Blvd & Wood Rd",
      intersection3Accidents: 77,
      intersection3Injuries: 77,
      intersection3Deaths: 0,
      intersection4: "Van Buren Blvd & Jurupa Ave",
      intersection4Accidents: 76,
      intersection4Injuries: 63,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Personal Injury Lawyers - Inland Empire Injury Attorneys"
        pageDescription="Call 951-530-3711 to connect with our Riverside personal injury lawyers for a free consultation if you've been injured in a car accident, dog bite or auto defect case. The Law Offices of Bisnar Chase have collected millions for Southern California accident victims."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Personal Injury Attorneys"
            title="Riverside Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Suffered a personal injury in Riverside? Contact the
          <strong> Riverside Personal Injury lawyers</strong> of Bisnar Chase
          today for a Free case review. A devastating personal injury can affect
          a victim and his or her family for years to come. The impact of the
          injury is often physical, financial and emotional. Many Inland Empire
          victims of personal injuries are unable to work as they watch their
          medical expenses pile up.
        </p>
        <p>
          In cases where catastrophic injuries have been suffered, victims may
          never be able to return to work or earn a livelihood again. It is
          important for anyone who has suffered a serious personal injury as a
          result of someone else's negligence or wrongdoing, to understand their
          legal rights and options.
        </p>
        <ul>
          <li>
            <strong>
              We've collected over $500 Million in wins for our clients
            </strong>
            throughout Southern California
          </li>
          <li>
            <strong> Serving Riverside County for over 40 years</strong>
          </li>
          <li>
            <strong> 96% success rate</strong>
          </li>
          <li>
            <strong> Over 12,000 clients</strong>
          </li>
        </ul>
        <p>
          <strong> Call 951-530-3711</strong> for a
          <strong> Free Consultation</strong>. <strong> Bisnar Chase</strong>
          never charges a fee unless we win your case. You may be entitled to
          compensation and our firm has the resources and experience to win
          personal injury cases! You can also contact us by going to our{" "}
          <Link to="/contact">contact page</Link>.
        </p>

        <h2>How To Choose the Right Riverside Personal Injury Attorney</h2>
        <p>
          There are many law firms in the Riverside area who may be able to help
          you. Make sure you work with a Riverside injury lawyer who has recent
          experience handling cases similar to yours.
          <strong> Your initial consultation should be free</strong>.
        </p>
        <p>
          So, use that time to learn as much as possible about your case. Also,
          the lawyer you retain should work on a contingency fee basis, which
          means that he or she will not get paid until you receive compensation.
        </p>
        <p>
          The experienced Riverside personal injury lawyers at law offices of
          Bisnar Chase have more than <strong> 40 years</strong> of experience
          handling all types of personal injury claims. Since 1978, our law firm
          has recovered millions of dollars for victims and their families.
        </p>
        <p>
          We have gone after insurance companies, at-fault motorists, negligent
          nursing homes, manufacturers of dangerous products as well as
          governmental entities in an effort to help our injured clients justice
          and fair compensation for their personal injury cases and lost wages.
        </p>

        <h2>Does Your Accident Fall into Personal Injury Law?</h2>
        <p>
          A personal injury claim is recognized as tort law. A personal injury
          gives an accident victim the opportunity to hold wrong-doers
          responsible for the damages and losses they have faced. The wrong-doer
          is the party that is held accountable for the injuries and is proven
          in a court of law that their negligence has caused the catastrophic
          injuries.
        </p>
        <p>
          There are two important factors that need to be proven for negligence.
          Important factors are that the negligent party had a "duty" to care
          for the victim and also that the negligent "failed" to carry on that
          duty.{" "}
        </p>
        <h2>Common Accidents in Riverside, CA</h2>
        <p>
          Some of the most common types of incidents that result in personal
          injury claims include:
        </p>
        <ul>
          <li>
            <b>
              {" "}
              <Link to="/riverside/auto-accidents">Car accidents</Link>:
            </b>{" "}
            Traffic accidents, pedestrian accidents, bicycle accidents,
            motorcycle accidents and truck accidents are the most common cause
            of injuries in Riverside County.
          </li>
          <li>
            <b>Slip-and-fall accidents:</b> Property owners are responsible for
            the injury accidents that occur on their property. Victims of
            slip-trip-and-fall accidents may seek support for their medical
            bills and other related losses.
          </li>
          <li>
            <b>Premises liability claims:</b> All property owners would be well
            advised to inspect their premises for defects and to post warning
            signs around hazardous conditions.
          </li>
          <li>
            {" "}
            <Link to="/riverside/product-liability">
              <b>Defective product claims</b>
            </Link>
            <b>:</b> Product manufacturers are legally obligated to make
            products that do not harm consumers when used correctly. When a
            product is defective, be it a faulty auto part or a dangerous drug,
            the manufacturer can be held accountable for the victims' injuries
            and losses.
          </li>
          <li>
            <b>Dog bite claims:</b> Under California law, dog owners are
            financially responsible for the actions of their pet that result in
            injuries.{" "}
            <Link
              to="http://law.onecle.com/california/civil/3342.html"
              target="blank"
            >
              California's strict liability statute
            </Link>{" "}
            requires that dog owners be held financially responsible for
            injuries caused by their pets.
          </li>
          <li>
            <b>Nursing home abuse: </b>Nursing homes and care facilities are
            required to provide quality care to residents. When they fail to do
            so and residents are subjected to nursing home neglect or abuse, the
            care facilities can be held liable for their negligence and/or
            wrongdoing.
          </li>
          <li>
            <b>Medical malpractice claims:</b> Medical negligence can lead to
            medical malpractice claims. Examples of medical negligence include
            misdiagnoses, surgical errors and medication errors.
          </li>
          <li>
            <strong> Pedestrian accidents:</strong> The{" "}
            <Link
              to="https://www.cdc.gov/motorvehiclesafety/pedestrian_safety/index.html"
              target="_blank"
            >
              {" "}
              CDC
            </Link>{" "}
            has reported that every year almost 5,987 people were killed in
            crosswalk collisions. Drivers must be off their phones and always
            remember that pedestrians always have the right-a-way.{" "}
          </li>
          <li>
            <strong> Motorcycle accidents:</strong> It is the law that
            motorcycle riders wear a helmet. Even though there are laws set in
            place to decrease the chance of a motorcycle crash, drivers at times
            do not adhere to them. Make sure that you are careful when switching
            lanes.
          </li>
        </ul>
        <p>
          If you have suffered from serious injuries do to someone's
          carelessness seek legal representation from a personal injury lawyer
          in Riverside, CA.
        </p>
        <h2>Medical Treatment After You Are Injured</h2>
        <p>
          If you have been injured, it is critical that you seek medical
          attention right away. Getting prompt treatment and care will not only
          create a medical record for your case, but it will also give you the
          best shot at a speedy and complete recovery.
        </p>
        <p>
          If you refuse medical care, the insurance adjuster who takes your case
          may claim that you were not really hurt. It is best to prevent that
          challenge by documenting your injuries as soon as possible. If
          emergency personnel offer you medical attention at the scene, it is
          highly advisable to accept it. If not, make sure that you go to an
          emergency room or see your doctor right away.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dealing with Insurance Companies After an Injury"
            style={{
              backgroundImage:
                "url('/images/text-header-images/insurance-agent-settlements-attorney-la.jpg')"
            }}
          >
            <h2>Dealing with Insurance Companies After an Injury</h2>
          </div>
        </LazyLoad>

        <p>
          If you have suffered an injury, dealing with insurance companies can
          be extremely stressful. Here are a few useful tips to remember before
          discussing your injuries with an insurance adjuster:
        </p>
        <ul>
          <li>
            <b>Be prepared:</b> Before even picking up the phone you should have
            your notes in front of you. If you contradict yourself while
            discussing your injuries, the validity of your claim will be called
            into question. Refer to your notes and stay on point.
          </li>
          <li>
            <b>Understand your rights:</b> You have the right to refuse the
            insurance company's request to record the phone call. Tell them
            upfront that you do not consent to being recorded. Any statement you
            make during such as conversation can be used against you. File a
            claim as soon as possible. The{" "}
            <Link to="https://www.courts.ca.gov/9618.htm" target="_blank">
              {" "}
              statute of limitations
            </Link>{" "}
            state that accident victims have two years to file a personal injury
            claim.{" "}
          </li>
          <li>
            <b>Talk to your personal injury attorney in Riverside:</b> Hiring
            one of the best personal injury lawyers in California talk to the
            insurance adjuster on your behalf is a great way to avoid
            jeopardizing your case. Do not sign any papers or grant your
            insurance company access to your medical records before first
            discussing it with one of our attorneys.{" "}
          </li>
        </ul>
        <h2>
          Never Rush into a Personal Injury Settlement, Consult With a Personal
          Injury Lawyer in Riverside, CA First
        </h2>
        <p>
          Do not accept the insurance company's first offer, even if you are
          tempted to take it and move on with your life. Insurance adjusters
          often offer inadequate settlements to make these cases go away
          quickly. Their adjusters are trained to make low-ball offers to help
          protect their bottom line.
        </p>
        <p>
          Once you accept a settlement, your claim will be closed and you will
          no longer be able to pursue any further support for your losses.
          Instead of accepting your first offer, discuss how much your claim
          should be worth with an experienced Southern California personal
          injury attorney.
        </p>

        <h2>Hire One Our Best Personal Injury Lawyers</h2>
        <LazyLoad>
          <img
            src="/images/newport beach platinum chamber of commerce member.webp"
            alt="Riverside Chamber of Commerce Member"
            width="291"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Our skilled team of{" "}
          <strong> Riverside Personal Injury Attorneys </strong>have been
          winning cases for over <strong> 40 years</strong>, establishing a{" "}
          <strong> 96% Success Rate </strong>and
          <strong> collecting over $500 Million </strong>for our clients.{" "}
          <strong> Bisnar Chase </strong>will advance costs and make sure you
          are financially covered throughout your case, and
          <strong> You Don't Pay Anything Unless We Win.</strong>
        </p>
        <p>
          {" "}
          <Link
            to="https://www.chamberofcommerce.com/riverside-ca/1335523916-bisnar-chase-personal-injury-attorneys"
            target="_blank"
          ></Link>
          Bisnar Chase is a{" "}
          <Link
            to="https://www.chamberofcommerce.com/riverside-ca/1335523916-bisnar-chase-personal-injury-attorneys"
            target="_blank"
          >
            {" "}
            proud member of the Riverside Chamber of Commerce
          </Link>
          . We have been serving injured plaintiffs in our community for over
          four decades and will fight for you.
        </p>
        <p>
          <strong>
            {" "}
            Call or <Link to="/riverside/contact">contact</Link> our Riverside
            personal injury attorneys today for your free consultation.
            951-530-3711.
          </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
