// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/product-liability-defective-attorneys-riverside-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Product Liability Lawyers - Bisnar Chase"
        pageDescription="Riverside Product Liability Attorneys winning over $500 Million. No win, no fee. Call us today at 951-530-3711."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Product Liability Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Product Liability Lawyers"
            title="Riverside Product Liability Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Product Liability Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong>are here to help and represent you and
          your <strong> Product Liability </strong>or
          <strong> Product Defect </strong>case.
        </p>
        <p>
          We put our trust and faith when buying toys and other items for our
          children, friends and loved ones.
        </p>
        <p>
          Our team of highly skilled and experienced attorneys have won over{" "}
          <strong> $500 Million</strong> for our clients and have established a{" "}
          <strong> 96% Success Rate</strong> over the last
          <strong> 39 years</strong>.
        </p>
        <p>
          Let <strong> Bisnar Chase</strong> represent you and win your case.
        </p>
        <p>
          If you have experienced an injury in result of a defective product,
          contact our highly skilled
          <strong> Personal Injury Attorneys</strong>. For immediate assistance
          call<strong> 951-530-3711</strong>, and you can receive a{" "}
          <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation</strong>
        </p>
        <h2>You Should Be Able To Trust What You Buy</h2>
        <p>
          Whether it's buying Christmas presents, birthdays gifts, grocery
          shopping or even back to school shopping, we never expect what we're
          buying to cause us personal injury.
        </p>
        <p>
          When large corporations or even small businesses sell a product that
          results in a consumers injury, sickness or death, they must accept
          responsibility and be held accountable to the fullest extent.
        </p>
        <p>
          Especially when we are buying and using products for our children,
          friends, family and loved ones, we should not have to deal with a
          poisonous reaction, explosion, burn, laceration, traumatic head injury
          or other serious or potentially fatal injury.
        </p>
        <h2>No Company Too Big for Bisnar Chase</h2>
        <p>
          Enormous corporations around the world like large retailers, auto
          makers and sporting goods manufacturers provide products and services
          we are supposed to count on for our safety and well-being.
        </p>
        <p>
          When these safety guarantees are proven false at the expense of the
          consumer, large corporations need to be held accountable.
        </p>
        <p>
          <strong> Bisnar Chase</strong> will go up against anyone. Our
          attorneys have taken on and won against the largest auto makers,
          automotive part manufacturers and other automobile industry giants and
          are ready to do so for you.
        </p>
        <h2>Top 10 Scariest Defective Products</h2>
        <p>
          According to Time Magazine, this is a full list of of the{" "}
          <strong>
            {" "}
            Top 10 Most Prominent and Scary Product Recalls in History
          </strong>
          :
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1974910,00.html"
              target="new"
            >
              Infantino's Baby Slings
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1957847,00.html">
              Toyota's Faulty Pedals
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908529,00.html">
              Cribs Recalled After Infant Death
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908517,00.html">
              China's Product-Safety Recalls
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908696,00.html">
              One Bad Bump ...
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908528,00.html">
              Tires, Treads and Tragedy
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908530,00.html">
              Slaughterhouse Snafu
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908715,00.html">
              Creamy, Chunky or Contaminated?
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908535,00.html">
              Tylenol Tragedies
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://content.time.com/time/specials/packages/article/0,28804,1908719_1908717_1908537,00.html">
              Merck's Vioxx Nightmare
            </Link>
          </li>
        </ul>
        <h2>Statute of Limitations in Riverside</h2>
        <p>
          It is good to not be impulsive and strategically analyze all aspects
          of any situation.
        </p>
        <p>
          When taking a legal approach, understanding everything about your
          case, what to do and what not to do, shows the importance of having a
          skilled
          <strong> Riverside Product Liability Lawyer </strong>to represent you
          and your case, making strategic decisions and making an impact in the
          outcome of your case.
        </p>
        <p>
          While being strategic and selective throughout the process of your
          case, understanding you only have so much time to file a case, before
          the incident becomes irrelevant due to waiting too long.
        </p>
        <p>
          Waiting too long will make it legally impossible to take any legal
          action, resulting in you losing your compensation you are entitled to.
        </p>
        <p>
          Make sure you get in contact with our team of lawyers as quickly as
          possible.
        </p>
        <p>
          For immediate assistance and a <strong> Free Case Evaluation </strong>
          and <strong> Consultation</strong>,{" "}
          <strong> Call 951-530-3711</strong>.
        </p>
        <h2>You Don't Pay Anything</h2>
        <p>
          <strong> Bisnar Chase </strong>understands this is a very difficult
          time for you, mentally, physically and financially. When you choose
          our firm to represent you and your case, we will cover and advance all
          costs throughout your case.
        </p>
        <p>
          If we do not win your case, you do not pay anything, meaning
          everything we advanced and covered is complentary on{" "}
          <strong>
            {" "}
            <Link to="/">Bisnar Chase</Link>
          </strong>
          .
        </p>
        <p>
          Keep in mind this contingency guarentee is a great benefit for you,
          but we do have a <strong> 96% Success Rate, </strong>have won over{" "}
          <strong> $500 Million </strong>for our clients and have over 39 years
          of experience.
        </p>

        <p>
          To find out if you have a claim contact our{" "}
          <strong> Riverside Product Liability Attorneys </strong>for a{" "}
          <strong> Free Case Review</strong>. We will go over the case with you
          free of cost. If we represent you, there will be{" "}
          <strong> no fee unless we win</strong>.
        </p>
        <p>
          Call us today at<strong> 951-530-3711</strong>. We will fight for you!
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
