// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/parking-lot-accident-lawyers-riverside.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Parking Lot Accident Lawyers - Bisnar Chase"
        pageDescription="Experienced Riverside Parking Lot Accident Lawyers who have won over $500 Million for our clients. Call 951-530-3711 for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Parking Lot Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="riverside parking lot accident lawyers"
            title="riverside parking lot accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Our team of <strong> Riverside Parking Lot Accident Lawyers</strong>{" "}
          have the ability to take a complex case and win. We have proven this
          over and over, establishing a solid
          <strong> 96% Success Rate</strong>. Our clients are family to us and
          our staff of paralegals, mediators and other important employees at{" "}
          <strong> Bisnar Chase </strong>are dedicated to ensuring that you and
          your case <strong> always come first</strong>.
        </p>
        <p>
          Parking lot accidents happen every single day, varying from minor
          fender-benders, to fatal pedestrian hit and runs. Every situation is
          important and must be handled appropriately and with the utmost
          respect.
        </p>
        <p>
          If you have experienced an accident or personal injury in a parking
          lot accident, contact our
          <strong> Riverside Personal Injury Lawyers</strong>.
        </p>
        <p>
          <strong> Bisnar Chase </strong>has won over{" "}
          <strong> $500 Million </strong>for our clients and wants to invite you
          and your case to be a crucial part of our award winning success.
        </p>
        <p>
          For immediate assistance <strong> Call 951-530-3711</strong>, and
          receive a <strong> Free Case Evaluation </strong>and{" "}
          <strong> Consultation</strong>. If we don't win,{" "}
          <strong> you don't pay</strong>.
        </p>

        <h2>Negligent Parking Lot Design and Maintenance</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/parking-lot-design-empty-riverside.jpg"
            width="45%"
            alt="design of empty riverside parking lot "
            className="imgright-fluid"
          />
        </LazyLoad>
        <p>
          People utilize parking lots to temporarily park their vehicles,
          motorcycles, bikes and other forms of transportation, so they can take
          care of what needs to get done in our busy lives.
        </p>
        <p>
          Getting some grocery shopping done, enjoying a night at the movies,
          even working our daily jobs. Besides the every day hazards of reckless
          drivers and all types of accidents that take place in parking lots and
          parking structures, poor design and negligence maintenance is a huge
          factor.
        </p>
        <p>
          Many parking lot designs have been overlooked and have actually been
          made to act as a perfect recipe for carnage. Some poor parking lot
          design flaws can consist of:
        </p>
        <ul>
          <li>No stop signs</li>
          <li>Tight and narrow parking spaces</li>
          <li>Aisles designed for two cars that only fit one or less</li>
          <li>Poorly illuminated or no lighting at all</li>
          <li>
            Design that gives singular lane priority, causing overall traffic
            and backups
          </li>
          <li>Faulty stop lights</li>
          <li>None or too many crosswalks</li>
        </ul>
        <p>
          For information on parking lot rules and regulations, visit{" "}
          <Link
            to="http://aarestriping.com/parking_lot_rules_and_regulations.html"
            target="new"
          >
            AAR.com
          </Link>
          .
        </p>
        <h2>Top 3 Most Common Parking Lot Accidents</h2>
        <p>
          Drivers and pedestrians feel safe in parking lots and parking garages
          because of the slow speeds and confined spaces, limiting a drivers
          excess ability of motion and over-maneuverability. This is a false and
          misleading thought process.
        </p>
        <p>
          It is just as dangerous, if not more dangerous being a pedestrian and
          or driver in a parking area. Here is a list of the{" "}
          <strong> Top 3 Most Common Parking Lot Accidents</strong>:
        </p>
        <ul>
          <li>
            <strong> Hit-and-Runs</strong> - Whether the hit and run victims are
            walking or jogging pedestrians, cyclist, skateboarders or on some
            other form of transportation, hit and runs happen every day. The hit
            and run suspect avoids stopping, checking on the victim and calling
            for medical attention and exchanging insurance information because
            they do not want to deal with the situation out of pure negligence,
            has a prior history with the law, or the hit and run was
            intentional. If you witness a hit and run, call 911 and give any
            information you may have, but always stay alert and take your health
            and safety as a priority.
          </li>
          <li>
            <strong> Pedestrian Accidents - </strong>Pedestrian accidents can be
            related to distracted driving, reckless driving, auto defects and
            road rage. Many pedestrian accidents can result in serious injury
            and death because of the weight ratio between a person and a
            vehicle.
          </li>
          <li>
            <strong> Fender-Benders</strong> - Fender-benders are prevalent in
            parking lots and parking structures because of the tightly situated
            parking area and congested traffic. Rear-endings, side swipes,
            head-on impacts and other types of fender-benders can all be minor
            with no injuries, to severe with catastrophic injuries. Never take
            any fender-bender or car accident lightly. Always seek medical
            attention and file a police report so documentation of injuries are
            related to the accident.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/fender-bender-parking-lot-riverside.jpg"
            width="100%"
            alt="riverside parking lot fender bender"
          />
        </LazyLoad>

        <h2>If We Don't Win, You Don't Pay</h2>
        <p>
          <strong> Bisnar Chase </strong>wants to make sure you are properly
          represented by the best legal staff available. Our team of parking lot
          attorneys have over
          <strong> 39 Years of Experience </strong>and have won over{" "}
          <strong> $500 Million </strong>for our clients.
        </p>
        <p>
          We will fight the good fight and always stand by your side. Our firm
          will also advance all costs and enable leans to make sure you are
          taken care of for when we win your case. If we do not win your case,
          you do not pay us anything.
        </p>
        <h2>Why Avoid Immediately Contacting Insurance Companies?</h2>
        <p>
          Insurance companies are in the money making business. As much as they
          tell you they are your friends and want to help you as much as they
          can, they will play hardball from the beginning, to ensure they give
          the least amount of money, saving them the most amount of money.
        </p>
        <p>
          Our skilled and experienced staff of paralegals negotiate with
          insurance adjusters every single day and understand what it takes to
          get you the most money possible.
        </p>
        <p>
          Many people will agree to pay off in cash and avoid minor to severe
          fender-bender and car, pedestrian and other types of accidents. This
          is a bad idea for a few reasons:
        </p>
        <ul>
          <li>
            <strong> Damage Invisible to the Eye:</strong> While it looks as
            though there is a minor scratch, crack, ding or dent, it is very
            possible and very common that the frame of your vehicle was torqued,
            bent and manipulated which can harm your vehicles health and cause
            major problems down the line, and quite possible result in a serious
            or fatal car wreck.
          </li>
          <li>
            <strong> Medical costs: </strong>Many rear-endings result in
            whiplash. Because of your body going into shock or not feeling the
            pain right away, you could experience severe neck, back or other
            body pain in the following days after the accident. Medical bills,
            doctors visits, treatments, physical therapy, medication and other
            aspects of seeking medical attention can quickly add up, and without
            insurance coverage from the person who hit you, if you paid
            everything off under the table, there is nothing more you or we can
            do.
          </li>
          <li>
            <strong> Don't Ruin Your Chances of Maximum Compensation: </strong>
            Many people will contact their insurance provider to get the process
            going as soon as possible. The only problem is that insurance
            companies want to make sure they don't pay out as much as they have
            to. They want to give as little as they have to, regardless of your
            situation.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/DkKmXYasbr4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          There are massive benefits to having insurance, but knowing when to
          contact them, how to do so and understanding your{" "}
          <Link
            to="http://www.uphelp.org/pubs/guide-your-insurance-legal-rights-california"
            target="new"
          >
            Insurance Legal Rights in California
          </Link>{" "}
          is a necessity.
        </p>
        <p>
          In order to get maximum compensation, you should contact our skilled
          and dedicated team of <strong> Riverside Parking Lot Lawyers </strong>
          and receive a <strong> Free Case Evaluation </strong>and
          <strong> Consultation</strong>.
        </p>
        <p>
          {" "}
          <Link to="/riverside/contact" target="new">
            Contact
          </Link>{" "}
          our <strong> Riverside Office</strong>.
        </p>
        <p>
          For immediate assistance <strong> Call 951-530-3711</strong>.
        </p>
        <h2>Top 5 Parking Structure Hazards</h2>
        <p>
          As if parking lots were not dangerous enough, parking structures and
          parking garages can be a scary place when it comes to reckless
          driving, congested traffic and enclosed spaces.
        </p>
        <p>
          There are many hazards but the{" "}
          <strong> Top 5 Parking Structure Hazards</strong> are as follows:
        </p>
        <ol reversed>
          <li>Concrete poles, beams and steel supports and cables</li>
          <li>Premise liabilities (stairs, elevators, railing, etc)</li>
          <li>
            Risk of structure failing or collapsing (especially in earthquake
            risk locations)
          </li>
          <li>Elevated levels</li>
          <li>Low-hanging ceilings</li>
        </ol>

        <LazyLoad>
          <img
            src="/images/text-header-images/riverside-parking-garage.jpg"
            width="100%"
            alt="riverside parking garage structure top 5 hazards"
          />
        </LazyLoad>

        <h2>Parking Lot Accident Statistics</h2>
        <p>
          You wouldn't think parking lots are on of the most common places for
          accidents, but they are. Here are some statistics provided by a{" "}
          <Link
            to="https://www.cbsnews.com/news/parking-lot-accidents-distracted-drivers-national-safety-council/"
            target="new"
          >
            CBS News report
          </Link>{" "}
          that will make you look twice before crossing the street from your
          parking spot to the store entrance.
        </p>
        <ul>
          <li>
            <strong> 25% </strong>of all parking lot accidents are caused by
            vehicles backing up. It is advisable that you watch out for other
            drivers and pedestrians while doing so.
          </li>
          <li>
            {" "}
            <Link to="https://www.nhtsa.gov/" target="new">
              NHTSA
            </Link>{" "}
            estimates that <strong> 22% </strong>(more than one-fifth) of
            children between ages 5 and 9 killed in traffic crashes were
            pedestrians. Most of these accidents occurred because drivers failed
            to see kids while backing up their vehicles.
          </li>
          <li>
            <strong> 1 out of 5</strong> car accidents take place in a parking
            lot
          </li>
          <li>
            Tragically, an average of over <strong> 500 people </strong>(drivers
            and pedestrians) were killed each year in parking lot accidents.
          </li>
          <li>50,000+ crashes per year from parking lot accidents</li>
          <li>60,000 injuries per year from parking lot accidents</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="riverside personal injury attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-riverside.jpg')"
            }}
          >
            <h2>Legal Representation for Parking Lot Accidents</h2>
          </div>
        </LazyLoad>

        <p>
          Our team of <strong> Riverside Parking Lot Accident Lawyers </strong>
          know what it takes to fight and win a case, and have proven it time
          and time again, establishing a<strong> 96% Success Rate</strong>.
        </p>
        <p>
          <strong> Bisnar Chase </strong>attorneys, paralegals and other legal
          team staff have won over <strong> $500 Million </strong> for our
          clients and strive everyday to help peoples lives and make the world a
          safer place.
        </p>
        <p>
          With over <strong> 40 Years of Experience</strong>, we have the
          knowledge, resources, reputation and skill to take on complex cases
          and win maximum compensation.
        </p>
        <p>
          If you or a loved one has been involved in a parking lot accident or
          has experienced a personal injury in a parking lot accident, contact
          us for a <strong> Free Case Evaluation </strong>and
          <strong> Consultation.</strong>
        </p>

        <center>
          <p>
            For immediate assistance<strong> Call</strong>{" "}
            <strong> 951-530-3711</strong>.
          </p>
        </center>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
