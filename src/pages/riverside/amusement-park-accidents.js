// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/theme-park-carnival-riverside-california-accident-attorney-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Amusement Park Accident Lawyers - Bisnar Chase"
        pageDescription="Injured in a theme park accident and looking for a top rated Riverside amusement park attorney? Call 951-530-3711 for a free consultation. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Amusement Park Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Amusement Park Accident Attorneys"
            title="Riverside Amusement Park Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Amusement Park Accident Attorneys</strong> are here
          to represent you and your loved ones.
        </p>
        <p>
          <strong> Theme Parks</strong> can be a great way to spend a free day
          with friends and family, however, when park staff becomes negligent to
          the safety of their attendants, things can take an ugly turn for the
          worst if you get injured on a ride or simply navigating through the
          park.
        </p>
        <p>
          If you or someone you know has suffered an injury at an amusement
          park, contact our team of skilled Riverside Amusement Park Accident
          Attorneys. You may be entitled to receive compensation for the
          physical and emotional pain suffered as a result of injury.
        </p>
        <p>
          The lawyers at the <strong> Bisnar Chase</strong> law firm have a
          collective total of over <strong> 39 years</strong> of experience and
          service representing personal injury cases including
          <strong> victims of amusement park accidents</strong> in{" "}
          <strong> Riverside</strong> and the <strong> Inland Empire</strong>.
          We can help you get the compensation you deserve for your injuries.
        </p>
        <p>
          Call to talk to our Riverside Personal Injury Lawyers now at{" "}
          <strong> 951-530-3711 </strong>if you've been injured in an amusement
          park accident and need an attorney in the Riverside area.
        </p>
        <h2>Dangers of Theme Parks</h2>
        <p>
          With a short travel, residents of Riverside can be at any number of
          theme parks in under an hour. Riverside residents have the luxury and
          privilege of entertainment options that many other cities in American
          have.
        </p>

        <p>
          California amusement and theme parks such as Castle Park, Disneyland,
          Disney's Californian Adventure, Magic Mountain, Scandia, Universal
          Studios, Sea World, Legoland and Knott's Berry Farm attract` millions
          of people from all over the country into our great state each year.
        </p>

        <p>
          Amusement parks are big businesses and the popularity and increased
          competition among amusement parks drives owners to create rides that
          are higher, faster, and scarier than the next.
        </p>
        <p>
          While amusement parks have tremendous safety and security measures in
          place, unfortunately, this desire for hosting the most extreme rides
          leads to a greater number of more severe accidents. Injuries that
          occur at amusement parks are the result of:
        </p>
        <ul>
          <li>
            <strong> Mechanical failure</strong>
          </li>
          <li>
            <strong> Structural damage</strong>
          </li>
          <li>
            <strong> Exposed electrical wiring</strong>
          </li>
          <li>
            <strong> Missing safety equipment</strong>
          </li>
          <li>
            <strong> Lap bars or safety restraints that malfunction</strong>
          </li>
          <li>
            <strong> Improper assembly or maintenance of rides</strong>
          </li>
          <li>
            <strong> Negligent operator behavior</strong>
          </li>
        </ul>

        <p>
          In the{" "}
          <Link
            to="http://abcnews.go.com/?cid=marketing_search_ABC%20News%20-%20Branded%20General%20-%20Exact"
            target="new"
          >
            ABC News
          </Link>{" "}
          video below, a tragic accident at a waterslide amusement park is the
          result of a ride defect.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IUmKpsiko0I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          These failures or malfunctions can cause broken or bloody noses, skin
          lacerations, broken ribs and limbs, whiplash, brain hemorrhages,
          paralysis and even death.
        </p>

        <p>
          In the past several decades, over a dozen people have died at
          amusement parks. A relatively low number, however when non-death
          injuries occur, they can be catastrophic causing permanent injury like
          the{" "}
          <Link
            to="http://www.businessinsider.com/worst-six-flags-accidents-2012-7?op=1"
            target="new"
          >
            young girl in New Jersey who lost both feet
          </Link>{" "}
          when a cable snapped and cut them off or the young man who was
          decapitated from the Batman roller-coaster in Georgia.
        </p>

        <p>
          Six Flags has had a history of accidents and injuries throughout the
          country. Research shows a lot of injuries happen when people disobey
          safety orders like staying behind all gates and fences of rides.
        </p>

        <p>
          Still, some people disobey the safety signs and hop over fences to
          retrieve items they may have lost on the ride, typically hats and
          wallets. Those people put themselves in great danger.
        </p>

        <p>
          Other injuries are not always caused by guests of the park but rather
          negligent security or failed safety equipment. Recently in Ohio, three
          people were shocked while they stood in line at a carnival when a live
          cable came loose.
        </p>
        <p>
          One of the injured was an 8 year old boy seriously injured by shock
          while leaning on a guard rail while standing in line.
        </p>
        <h2>Wrongful Death Claim</h2>
        <p>
          If you have lost a loved one due to an amusment park accicent, you may
          be able to recover financial compensation such as pain and suffering,
          funeral costs, loss of future income, companionship, etc.
        </p>
        <p>
          Contact our team of{" "}
          <Link to="/riverside/wrongful-death">wrongful death</Link> attorneys
          to discuss your situation with a Free Consultation and Case
          Evaluation. For immediate assistance{" "}
          <strong> Call 951-530-3711</strong>.
        </p>
        <p>
          Injuries and catastrophic results need to be properly represented so
          that those who are to blame are held fully accountable for what they
          have done.
        </p>
        <p>
          Don't let it go without doing what is right. Let{" "}
          <strong> Bisnar Chase </strong>represent you and your family so that
          we can bring peace of mind and help with financial problems in
          relation to the amusement park accident you have experienced.
        </p>
        <p>
          <strong> Bisnar Chase Personal Injury Lawyers </strong>will advance
          all costs, cover medical bills and make sure you are completely taken
          care of, because
          <strong> If we don't win your case, you don't pay.</strong>
        </p>

        <h2>Get The Best Legal Representation</h2>
        <p>
          Find out if you have a personal injury case for a theme park injury
          during your free consultation with Bisnar Chase.
        </p>

        <p>
          Premise liability cases such as these can be complex and the legal
          system hard to maneuver without the help of a trained and experienced
          <strong> Riverside amusement park accident lawyer</strong>. Our
          attorneys are very experienced in premise liability cases and have won
          over <strong> $500 Million</strong> for over
          <strong> 12,000 clients</strong>.
        </p>

        <p>
          Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation.</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
