// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/train-station-accidents-riverside-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Train Accident Lawyers - Bisnar Chase"
        pageDescription="If you have been injured in a train accident, call our Riverside Train Accident Attorneys now at 951-530-3711 for your free case review. We specialize in catastrophic and serious injuries and have recovered hundreds of millions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Train Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Train Accident Lawyer"
            title="Riverside Train Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Train Accident Attorneys </strong>have been
          representing citizens of <strong> Riverside County </strong>with over{" "}
          <strong> 39 years </strong>of experience.
          <strong> Bisnar Chase </strong>has won over{" "}
          <strong> $500 Million </strong>for our clients, establishing a{" "}
          <strong> 96% Success Rate</strong>.
        </p>
        <p>
          If you have ever taken a ride on the <strong> Metrolink</strong> in{" "}
          <strong> Riverside</strong> <strong> County</strong>, you know that
          traveling by train is a safe and cost effective way to get around.
        </p>
        <p>
          Statistically, it is safer to ride a train than to travel by car,
          however, accidents still happen and when they do, they tend to be
          catastrophic and deadly. Whether the train accident was caused by
          human error or lack of maintenance, it is important that the victim
          and the victim's family are provided the support and compensation they
          need.
        </p>
        <p>
          The experienced Riverside{" "}
          <Link to="/riverside">personal injury lawyers</Link> of{" "}
          <strong> Bisnar Chase</strong> can help fight for your rights, stay
          abreast of the official investigation and hold the at-fault parties
          accountable.
        </p>
        <p>
          Please call us today at<strong> 951-530-3711 </strong>for a{" "}
          <strong> Free</strong> <strong> Consultation</strong> and{" "}
          <strong> Comprehensive Case Evaluation</strong>.
        </p>
        <h2>Getting the Support You Need</h2>
        <p>
          Ideally, the parties responsible for your injuries will offer victims
          fair compensation that covers all medical bills, lost wages and other
          damages, however, this rarely occurs without the help of a skilled{" "}
          <strong> train accident attorney</strong>.
        </p>
        <p>
          <strong> Insurance companies</strong> and{" "}
          <strong> defense lawyers</strong> who work for these companies make it
          tough for injured victims or families of deceased victims to secure
          the compensation they rightfully deserve.
        </p>
        <h2>Causes of Metrolink Accidents in Riverside</h2>
        <p>
          <strong> Metrolink</strong> is a{" "}
          <strong> regional rail system</strong> that connects{" "}
          <strong> Riverside County</strong> to <strong> San Bernardino</strong>
          , <strong> Orange County</strong> and
          <strong> Los Angeles County</strong>. The purpose of the{" "}
          <strong> railroad system</strong> is to relieve congestion on our{" "}
          <strong> busy highways</strong> and <strong> freeways</strong>.
          <strong> Metrolink</strong> is busy year round and it is often
          relatively safe, however, there are times in which mistakes are made
          and people get hurt.
        </p>
        <p>
          There have been instances in the past of train accidents involving{" "}
          <strong> collisions</strong> between <strong> Metrolink</strong>{" "}
          <strong> commuter trains</strong> that share the same track.
        </p>
        <p>
          Some accidents occur due to the inattentiveness of the conductor or
          engineer. Others are a direct result of the engineer's failure to see
          warning signals or derailment caused by poorly maintained tracks.
        </p>
        <p>
          Driver fatigue, human error, excessive speed and a failure to obey
          safety standards are all forms of negligence that can lead to a train
          accident in Riverside County and the Inland Empire. Driver distraction
          is also a major factor in train accidents.
        </p>
        <p>
          For example, a <strong> Metrolink</strong>{" "}
          <strong> crash in Chatsworth</strong> that killed 25 and injured 135
          others was caused by a train engineer who ran a red light after being
          distracted while text messaging.
        </p>
        <p>
          According to the results of an investigation by the{" "}
          <Link to="https://www.ntsb.gov/Pages/default.aspx" target="new">
            National Transportation Safety Board (NTSB)
          </Link>
          , the engineer sent or received 43 text messages while on duty that
          day. His last text went out 22 seconds before the crash.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/owDEIR6v9ws"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Riverside Train Accident Prevention</h2>
        <p>
          It is crucial that drivers of vehicles take measures to be safe near
          railroad tracks throughout <strong> Riverside County</strong>. If the
          arm goes down and the flashing lights and bells go off, drivers must
          stop and not try to go around.
        </p>
        <p>
          Often, the responsibility for train accidents falls directly on the
          conductor, engineer, the railroad or the company that operates the
          train. These companies are considered common carriers under the law.
        </p>
        <p>
          This means that they owe the highest duty of care to their passengers.
          They have a legal obligation to transport their passengers safely.
        </p>
        <p>
          Train companies must be thorough in their hiring process, supervise
          and monitor their employees and ensure that all engineers and
          conductors are properly trained. It is also necessary to regularly
          inspect tracks for cracks and breaks.
        </p>
        <p>
          Railroad crossings should be examined to ensure that warning lights
          and speakers are working properly. A number of accidents occur at
          train crossings because of lights and arms that fail to work properly
          or malfunction at crossings.
        </p>
        <h2>What to Do If You Are Involved in a Train Accident</h2>
        <p>
          If you are the passenger on a train during an accident, you may get
          thrown from your seat or get struck by flying objects. You may even
          suffer lacerations from broken glass or a concussion from striking
          your head against the train's interior. It is important that you
          remain at the scene of the accident and notify the authorities that
          you have been injured.
        </p>
        <p>
          If you have a cell phone, take photos or even videos of where you were
          seated and the injuries you have suffered. If possible get your name
          in the incident report. Seek medical attention immediately to maximize
          your chances of a quick and full recovery.
        </p>
        <p>
          If you are a vehicle occupant, bicyclist or pedestrian struck by a
          train, it is absolutely vital that you remain at the scene and call
          the police. It can help to have photos of the crash scene and contact
          information from anyone who may have witnessed the accident. Having as
          much information and physical evidence from the scene as possible can
          be invaluable during your claims process.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Train Accidents in the Inland Empire"
            style={{
              backgroundImage:
                "url('/images/text-header-images/rail-road-inland-empire-riverside-county-dangers-train-accidents-attorney-lawyer.jpg')"
            }}
          >
            <h2>Train Accidents in the Inland Empire</h2>
          </div>
        </LazyLoad>

        <p>
          In 2015 a freight train derailed near{" "}
          <strong> Mira Loma in Riverside County</strong> killing three people.
          In early 2015 a <strong> bicyclist</strong> was killed by a
          <strong> train in Riverside</strong>.
        </p>
        <p>
          There have also been many instances of{" "}
          <Link
            to="https://en.wikipedia.org/wiki/List_of_rail_accidents_(2010%E2%80%93present)"
            target="new"
          >
            freight trains colliding with tractor trailers
          </Link>{" "}
          causing multiple injuries and deaths in the United States. An
          experienced <strong> train accident lawyer</strong> can advise you of
          your rights and potential compensation for your injuries.
        </p>
        <h2>Do I Need a Train Accident Lawyer?</h2>
        <p>
          If you plan on taking action against the train company or any other
          at-fault parties concerning your accident, it is imperative to hire a
          seasoned and skilled
          <strong> Riverside train accident lawyer</strong>.
        </p>
        <p>
          Not only will the at-fault party have their own defense lawyers, but
          the law concerning train accidents is so dense and complicated, you
          may not get the full amount of compensation that you deserve or worst
          case scenario, you could lose the case and would have ultimately waste
          time and money that could have been used to recover from your initial
          injuries.
        </p>
        <p>
          At <strong> Bisnar Chase</strong>, it is our mission to stick up for
          the "little guys" who do not have a voice against big companies or
          government entities. We know the law better than any other law firm in
          the <strong> Inland Empire</strong> and we can help you get the
          compensation that you deserve.
        </p>
        <p>
          If we cannot live up to this promise or if we lose your case, you do
          not have to pay anything, however, our track record of{" "}
          <strong> 96% Success Rate</strong> and over
          <strong> 39 years</strong> serving our clients in the{" "}
          <strong> Inland Empire</strong> are safe bets that we will not only
          fight for you, but also win.
        </p>
        <p>
          One of our leading{" "}
          <strong> Riverside train accident attorneys</strong> with sit down
          one-on-one with you and discuss the details of your case and will also
          answer any questions you may have. If you are injured and cannot come
          to us, we will come out to you.
        </p>
        <p>
          With so little to lose and so much to gain, why wait any longer to see
          justice served? Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
