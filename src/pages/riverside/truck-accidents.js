// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/large-truck-accident-banner-riverside-county-attorney-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Truck Accident Lawyers - Semi-Truck Collision Attorneys"
        pageDescription="Call us now at 951-530-3711 if you have been injured in a Riverside truck accident. Our Riverside commercial truck crash attorneys can help you get the compensation you deserve. Free consultation and hundreds of millions recovered for injury victims."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Truck Accident Attorneys"
            title="Riverside Truck Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Truck Accident Lawyers</strong> have been
          representing citizens of <strong> Riverside County</strong> with a
          strong and established <strong> 96% Success Rate</strong>.
        </p>
        <p>
          <strong> Riverside County</strong> has significant truck traffic
          compared to any other city in the <strong> Inland Empire</strong>.{" "}
          <strong> Truck accidents</strong> involving
          <strong> tractor-trailers, semi-trucks</strong> or other{" "}
          <strong> big rigs</strong> do not have to occur at high speeds in
          order to cause catastrophic{" "}
          <Link to="/riverside">personal injuries</Link> and damages to you and
          your passengers.{" "}
        </p>
        <p>
          Due to the sheer weight, mass and volume of these big rigs, even a
          slow-speed accident can result in devastating damage and severe
          injuries to you and your loved ones as well as damages to your motor
          vehicle. In the worst possible scenario, wrongful death can come out
          of these situations.
        </p>
        <p>
          If you've been injured in an accident involving a semi-truck, contact
          our <strong> award-winning</strong>
          truck accident attorneys in Riverside, CA now by calling{" "}
          <strong> 951-530-3711 </strong>for a{" "}
          <strong> Free Consultation</strong> to discuss your legal rights.
        </p>
        <p>
          You can also contact us through our{" "}
          <Link to="/contact">online form</Link>.{" "}
        </p>
        <h2>What Can Cause Riverside Truck Accident?</h2>
        <ul>
          <li>Drivers Under the Influence of Drugs and/or Alcohol</li>
          <li>Weather Conditions</li>
          <li>Drivers Who Illegally Use Electronic Devices While Driving</li>
          <li>Vehicle Defects</li>
          <li>Hazardous Materials on the Road</li>
          <li>Poor Truck Inspections</li>
          <li>Driver Exhaustion</li>
        </ul>
        <p>
          Adding to the use of electronic devices, it is important to note that
          as of January 2017, it is now illegal in to hold your{" "}
          <strong> cell phone</strong> in your hand while you drive. According
          to the new{" "}
          <Link
            to="http://www.sacbee.com/news/local/transportation/article123126354.html"
            target="new"
          >
            California cell phone law
          </Link>
          , it must be fasten to your car and all actions must be hands free.
        </p>
        <h2>Riverside Truck Collision Statistics </h2>
        <p>
          The{" "}
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts"
            target="_blank"
          >
            {" "}
            Federal Motor Carrier Safety Administration
          </Link>{" "}
          has reported that one year over 4,440 trucks were involved in fatal
          accidents. The truck and car accident reports in Riverside have stated
          that incidents involving big rigs have increased over time.{" "}
        </p>
        <p>
          According to{" "}
          <Link
            to="https://www.trucks.com/2018/10/04/large-truck-fatalities-29-year-high/"
            target="_blank"
          >
            {" "}
            Trucks.com
          </Link>{" "}
          big rig crashes had a 9% increase within one year. Of those accidents
          over 40% of truckers or their passengers did not have seat belts on.{" "}
        </p>
        <h2>Types of Common Truck Wrecks in Riverside</h2>
        <p>
          Riverside truck accidents today can cause a serious of catastrophic
          injuries. There are various different types of truck accidents that
          can result in fatal injuries.{" "}
        </p>
        <p>
          <strong> Some of the most dangerous types include:</strong>
        </p>
        <ul>
          <li>
            <b>Head-on collisions: </b>When fatigued truck drivers let their
            vehicle drift into oncoming traffic, head-on collisions can occur.
          </li>
          <li>
            <b>Underride accidents: </b>Smaller vehicles often go partially
            underneath large trailers when collisions occur. A side underride
            collision is when a vehicle collides with the side of the trailer
            and a rear underride collision is when a smaller car goes underneath
            the back of a trailer. These types of accidents often result in
            catastrophic or fatal injuries.
          </li>
          <li>
            <b>Runaway trailer accidents: </b>This is when the trailer of a
            truck travels at a different speed than the tractor. In severe
            cases, the trailer can become completely detached crushing
            everything in its path.
          </li>
          <li>
            <b>Jackknife accidents:</b> When a trailer swings to one side, it
            can cause a truck to get stuck on the roadway posing a serious
            hazard for oncoming vehicles. Jackknife accidents are often caused
            by improperly loaded trailers.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/semi-truck-car-accident-head-on-big-rig-attorneys-lawyers.jpg"
            width="100%"
            alt="semi truck crash in Riverside"
          />
        </LazyLoad>

        <h2>Injuries Suffered in Riverside Truck Wrecks</h2>
        <p>
          A truck accident in Riverside, CA can be fatal. The massive size of
          trucks makes them capable of crushing smaller vehicles. Many of the
          most devastating injuries result from high-speed collisions on the
          highway, but serious injuries can occur even in low-speed collisions.
          Truck accident victims often incur significant medical expenses as
          well as hospitalization and rehabilitation costs. They may also lose
          their income as a result of taking time off to heal from their serious
          injuries.{" "}
        </p>
        <p>
          Car occupants involved in these accidents often suffer catastrophic
          injuries including:
        </p>
        <ul>
          <li>Multiple Bone Fractures</li>
          <li>Amputations</li>
          <li>Neck Injuries</li>
          <li>Concussions</li>
          <li>Internal Bleeding</li>
          <li>Spinal Cord Injuries</li>
          <li>Traumatic Brain Injuries </li>
        </ul>
        <p>
          It's important to note that as a motorist, you have rights when it
          comes to these types of accidents and our{" "}
          <strong> truck accident attorneys in Riverside </strong>can help
          inform you of your rights as well as to help you seek compensation for
          your injuries.
        </p>
        <h2>
          What to Do if You Have Been Injured in a Truck Accident in Riverside
        </h2>
        <p>
          If you and your passengers have been involved in a truck accident, it
          is important to remain calm. The actions of your party after being
          injured in a truck accident can affect your ability to receive
          compensation from the at-fault party. After a{" "}
          <strong> truck accident</strong>, it is important to:
        </p>
        <ul>
          <li>
            <b>Seek medical attention:</b> An important part of the claim
            process is establishing the extent of your injuries. If you see a
            doctor that same day, it establishes when you were hurt and
            documents the severity of your injuries. If you fail to see a doctor
            right away, the insurance adjuster may claim that you were not truly
            injured.
          </li>
          <li>
            <b>Collect evidence:</b> You or someone on your behalf will need to
            gather evidence. Besides license and insurance information, you
            should collect the name of the trucking company and the Department
            of Transportation numbers on the truck and the trailer.
          </li>
          <li>
            <b>Locate witnesses:</b> In addition to the contact information for
            the truck driver, you may want the contact information for anyone
            who witnessed the crash. You should also take note of any nearby
            businesses that may have captured the crash on video surveillance
            cameras.
          </li>
          <li>
            <b>Take photos: </b>If you have a phone that can take photos,
            immediately photograph the scene of the crash. Take shots of
            different angles and perspectives. Show the damage to the vehicles
            and take photos of your injuries to better assist your truck
            accident attorney in Riverside with your case. It may prove useful
            to photograph your injuries as they heal as well.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/mX8wMo8QTzo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Who is Potentially Responsible for a Riverside Truck Collision?</h2>
        <p>
          <strong> Truck accident claims</strong> are complicated by the fact
          that there are a number of potentially liable parties. Victims can
          file a claim against the truck driver, the owner of the truck, the
          owner of the trailer, the person who leased the truck from the owner
          or the Riverside truck and equipment company.{" "}
        </p>
        <p>
          An important part of the claim process is determining how the crash
          occurred and who should be held responsible for the accident. In many
          cases, it is advisable for the victim to file claims against multiple
          parties.
        </p>
        <h2>Consult A Truck Accident Attorney in Riverside</h2>
        <p>
          It is important to remember that the trucking companies have defense
          attorneys and insurance companies protecting their best interests at
          all times.
        </p>
        <p>
          These companies have the potential to have their people at the crash
          site shortly after the crash, collecting evidence and gathering
          information that will help protect them against a lawsuit.
        </p>
        <p>
          It is important that victims get someone on their side who will offer
          that same protection and peace of mind to them while keeping their
          best interests at mind. This is our mission at
          <strong> Bisnar Chase</strong>.
        </p>
        <p>
          <strong> Bisnar Chase Riverside truck accident attorneys</strong> know
          what it takes to stand up and fight on behalf of our injured clients
          and win. Our attorneys have more than three decades of experience and
          during this time our firm has provided:
        </p>
        <ul>
          <li>
            <strong> A 96% success rate</strong>
          </li>
          <li>
            <strong> Over $500 Million won for our clients</strong>
          </li>
          <li>
            <strong> Serving Riverside since 1978</strong>
          </li>
          <li>
            <strong> Served over 12,000 clients</strong>
          </li>
        </ul>
        <p>
          Call <strong> 951-530-3711 </strong>now to set up your{" "}
          <strong> Free Consultation </strong>and{" "}
          <strong> Case Evaluation</strong>. If you cannot come to us, we will
          come to you and
          <strong> you don't pay unless we win</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
