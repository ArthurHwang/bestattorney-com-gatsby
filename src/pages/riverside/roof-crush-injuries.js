// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/roof-crush-riverside-car-accident-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Roof Crush Lawyers - California"
        pageDescription="Contact the Riverside roof crush attorneys of Bisnar Chase today for a FREE consultation at 951-530-3711. You may be entitled to compensation for your serious or catastrophic rollover vehicle accident. Trusted for over 40 years."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Roof Crush Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside Roof Crush Lawyers"
            title="Riverside Roof Crush Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Roof Crush Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong>are here to help you win your case.{" "}
          <strong> Car accidents </strong>that result in a severe
          <strong> roof crush </strong>can result in severe, catastrophic
          injuries with fatalities everyday.
        </p>
        <p>
          The{" "}
          <Link to="https://www.nhtsa.gov/" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          estimates that about a <strong> third</strong> of all vehicle occupant
          fatalities result from rollover accidents.{" "}
          <strong> Roof crush</strong> occurs when the roof of an overturned
          vehicle fails to withstand the pressure and caves in toward the
          occupants.
        </p>
        <p>
          This can happen as the vehicle rolls over or as the vehicle comes to
          rest upside down after the crash. Either way, when the roof fails to
          retain its shape, occupants face the risk of suffering devastating
          head, neck and back injuries.
        </p>
        <p>
          If you or a loved one has been injured in a car accident, please
          contact the Riverside personal injury attorneys of
          <strong> Bisnar Chase</strong> for a <strong> Free</strong>{" "}
          <strong> Case Evaluation </strong>now at{" "}
          <strong> 951-530-3711</strong>.
        </p>
        <h2>Do I Need an Experienced Auto Defect Lawyer?</h2>
        <p>
          It is important to consider all of your legal options before making
          any decisions. Accepting a quick settlement from an insurance company
          will negate your ability to pursue additional compensation. Therefore,
          it is important to evaluate the fairness of the offer and the
          potential value of your claim.
        </p>
        <p>
          Automakers who fail to manufacture safe vehicles despite having had
          the resources and the technological know-how to do so for years,
          should be held accountable for their negligence and for their
          eagerness to put profits before public safety.
        </p>
        <p>
          The experienced{" "}
          <strong>
            {" "}
            Riverside{" "}
            <Link to="/riverside/product-liability">
              product liability lawyers
            </Link>
          </strong>{" "}
          at <strong> Bisnar Chase</strong> have a long and successful track
          record of helping catastrophically injured victims and their families
          obtain fair and full compensation for their losses due to{" "}
          <strong> auto defects</strong> such as a{" "}
          <strong> roof crush liability</strong>.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/victims-after-rollover-roof-crush-car-accident-riverside-attorneys-lawyer.jpg"
            width="100%"
            alt="victims-after-rollover-roof-crush-car-accident-riverside-attorneys-lawyer.jpg"
          />
        </LazyLoad>

        <h2>Roof Crush Injuries In Riverside</h2>
        <p>
          Even occupants who are properly buckled during a rollover can get
          thrown around from side to side resulting in sprains, strains,
          internal injuries and bone fractures. If the structure of the vehicle
          fails, the occupants can get crushed inside.
        </p>
        <p>
          <strong> Roof crush injuries</strong> commonly involve vertebrae
          fractures, traumatic brain injuries and spinal cord injuries. It is
          common for survivors of roof crush injuries to suffer catastrophic
          injuries and disabilities including paralysis. The severity of the
          injuries is dependent on the part of the roof that failed relative to
          where the victim was seated.
        </p>

        <h2>Importance of Preserving a Vehicle After A Roof Crush Accident</h2>
        <p>
          Whenever a roof crush accident occurs, it is important to investigate
          why the vehicle failed to work properly. If it is determined that the
          vehicle was defectively designed or built, it may be possible for the
          victim or the victim's family to file a claim against the auto part
          manufacturer.
        </p>
        <p>
          This will prove difficult if the damaged vehicle is not preserved
          correctly for a thorough inspection immediately after the crash. After
          a rollover crash, it is important to take photos of the damaged
          vehicle.
        </p>
        <p>
          It is crucial to obtain close-up shots of the roof showing how far it
          caved in. The vehicle must be towed and preserved so that an expert
          can thoroughly examine it for defects, malfunctions and design flaws.
        </p>
        <h2>Product Liability Claims and Class Action Suits</h2>
        <p>
          Victims of defective products have the right to file a product
          liability claim against the at-fault manufacturer. In such cases,
          plaintiffs must prove that the product was defective, that they
          suffered a loss and that their loss was directly related to the
          defect.
        </p>
        <p>
          In the event of a roof crush incident, it must be determined if the
          roof failed because it was defectively designed or assembled. Large
          dents and cave-ins could be a sign of a defective roof.
        </p>
        <p>
          In general, an investigation should take place if the roof caved in
          five inches or more during the accident.
        </p>

        <h2>Government Regulation and Roof Crush</h2>
        <p>
          Consumer advocates have called for higher standards in rollover safety
          for many years.
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="https://www.nhtsa.gov/sites/nhtsa.dot.gov/files/fmvss/Roof_Crush_Final_Rule.pdf"
              target="new"
            >
              NHTSA
            </Link>{" "}
            estimates that 10,000 deaths result from rollover accidents each
            year and about 667 of those fatalities are caused by roof crush.
          </li>
          <li>
            In 2009, the NHTSA issued new standards that could save up to 135
            lives and prevent 1,065 injuries annually.
          </li>
          <li>
            Under the new standards that officially went into effect in
            September 2012, the roof of vehicles under 6,000 pounds must
            withstand three times the curb weight of the vehicle.
          </li>
          <li>
            Furthermore, vehicles weighing between 6,000 and 10,000 pounds are
            now required to withstand 1.5 times the weight of the vehicle.
          </li>
        </ul>
        <p>
          Before this regulation, there was no required standard for larger
          vehicles. The new regulations do not cover 12- to 15- passenger vans
          that are notorious for their involvement in catastrophic and fatal
          rollover crashes. The regulations do not affect hard- or soft-top
          convertibles either.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/PHSDRRCs-5Q"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Damages Covered</h2>
        <p>
          Victims of defective auto parts can pursue financial compensation for
          all of their losses by filing a claim against the auto manufacturer. A
          successful product liability claim can result in financial
          compensation for:
        </p>
        <ul>
          <li>
            Medical bills including the cost of hospitalization and surgeries
          </li>
          <li>Cost of medication and medical devices</li>
          <li>
            Lost wages including time away from work and lost earning potential
          </li>
          <li>Pain and suffering</li>
          <li>Disability consideration</li>
          <li>Rehabilitation costs</li>
        </ul>
        <p>
          When an individual is killed in a{" "}
          <strong> roof crush accident</strong>, the victim's family can file a
          wrongful death claim seeking compensation for their losses. Such
          claims, which are filed by immediate family members, seek compensation
          for damages such as medical expenses, funeral costs, lost future
          income and benefits and loss of love and companionship.
        </p>
        <p>
          In any instance, it is not easy to deal with an{" "}
          <strong> injury accident</strong> on your own.
        </p>
        <h2>Bisnar Chase Wants to Represent Your Roof Crush Case</h2>
        <p>
          Again, if you have been injured in a car accident involving a{" "}
          <strong> crushed roof</strong>, we urge you to contact the{" "}
          <strong> Riverside law firm</strong> of <strong> Bisnar Chase</strong>{" "}
          to speak to one of our attorneys. Recovery is only a phone call away!
        </p>
        <p>
          Call us at <strong> 951-530-3711</strong> for a{" "}
          <strong> Free Consultation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
