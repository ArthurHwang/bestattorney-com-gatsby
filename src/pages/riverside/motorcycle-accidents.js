// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/riverside-motor-cycle-accident-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Riverside Motorcycle Accident Lawyers - Bisnar Chase"
        pageDescription="If you have been involved in a motorcycle accident, call our Riverside motorcycle accident attorneys at 951-530-3711. We fight for your compensation. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Riverside Motorcycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Riverside motorcycle accident lawyers"
            title="Riverside motorcycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Riverside Motorcycle Accident Attorneys </strong>at{" "}
          <strong> Bisnar Chase </strong>are always ready to fight for you and
          your case.
        </p>
        <p>
          Our skilled attorneys have won over <strong> $500 Million </strong>for
          our clients over the last <strong> 39 years</strong> and have
          established a <strong> 96% Success Rate</strong>.
        </p>
        <p>
          Motorcycle riding is an extremely popular pastime in{" "}
          <strong> Riverside County</strong>, however, there are many negligent
          drivers out on the road that do not check their blind spots for
          oncoming motorcycle riders. Because of this, hundreds of innocent
          riders each year become the victims in motorcycle accidents.
        </p>
        <p>
          If you were injured in a motorcycle accident, contact{" "}
          <strong> Bisnar Chase</strong> Riverside personal injury lawyers now
          at
          <strong> 951-530-3711</strong> for your free no-obligation
          consultation. You may be entitled to compensation for your injuries
          and recovery and we will help you obtain it.
        </p>
        <h2>Motorcycle Accidents in Riverside</h2>
        <p>
          Because motorcycles have practically no protection for their riders,
          accidents involving motorcycles are many times very serious and often
          fatal.
        </p>
        <p>In a recent years study:</p>
        <ul>
          <li>
            Over 80,000 riders were injured and almost 5,000 riders were killed
            in motorcycle accidents. Out of 8.6 million registered vehicles, the
            fatality rate for motorcycle accidents was 57.85%
          </li>
          <li>
            Many times, alcohol and speeding are factors of motorcycle
            accidents. Out of 449 motorcycle riders killed, 29% had a BAC of
            .01+, 24% had a BAC of .08+(legally intoxicated) and 15% had a BAC
            of .15+
          </li>
          <li>
            It is common for local motorcycle accidents to result in serious or
            even fatal injuries
          </li>
          <li>
            In one recent crash, rock star and Riverside native{" "}
            <Link
              to="http://www.billboard.com/articles/news/474325/mitch-lucker-suicide-silence-frontman-dies-in-motorcycle-crash"
              target="new"
            >
              Mitch Lucker
            </Link>{" "}
            died from the injuries he sustained in a motorcycle accident while
            riding his 2013 Harley Davidson
          </li>
          <li>
            In another recent motorcycle accident, a 37-year-old motorcycle
            rider was killed after colliding with a vehicle and being struck by
            two others after he was thrown off his bike
          </li>
          <li>
            According to media reports, the fatal accident occurred at Arlington
            Avenue and Pegasus Drive in Riverside. Officials say a 2009 Toyota
            Corolla turned left in front of the motorcycle, resulting in a
            broadside collision
          </li>
          <li>
            A motorist turning left in front of an oncoming motorcycle is one of
            the most common causes of major injury or fatal motorcycle
            collisions. A number of Riverside County bike accidents are single
            vehicle crashes
          </li>
        </ul>
        <h2>Top 7 Most Common Causes of Motorcycle Accidents</h2>
        <p>
          <strong> Motorcycle accidents</strong>, like many other types of{" "}
          <strong>
            {" "}
            <Link to="/riverside/auto-accidents">traffic accidents</Link>
          </strong>
          , can happen in a split second. It only takes a moment of{" "}
          <strong> distraction</strong> or
          <strong> inattention</strong>. The{" "}
          <strong> Top 7 Most Common Causes of Motorcycle Accidents</strong> in
          Riverside include:
        </p>
        <ul>
          <li>
            <strong> Driving while distracted</strong>
          </li>
          <li>
            <strong>
              {" "}
              Running a red light or stop sign at an intersection
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Dangerous or defective roadways and poorly designed street
              intersections
            </strong>
          </li>
          <li>
            <strong> Reckless or aggressive driving</strong>
          </li>
          <li>
            <strong>
              {" "}
              Failure to look for motorcyclists before changing lanes, turning
              left or entering traffic
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Driving under the influence of drugs and/or alcohol
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Exceeding the speed limit or driving at a speed that is unsafe for
              traffic, roadway or weather conditions
            </strong>
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Motorcycle Safety Tip"
            style={{
              backgroundImage:
                "url('/images/text-header-images/motorcycle-safety-riding-tips-accident-attorney-lawyer.jpg')"
            }}
          >
            <h2>Motorcycle Safety Tips</h2>
          </div>
        </LazyLoad>
        <p>
          The following safety tips will help even the most seasoned motorist
          stay proactive to potential danger on the road:
        </p>
        <ul>
          <li>
            <b>Avoid blind spots: </b>Inattentive drivers may not notice that
            you have pulled up alongside them. Try to avoid riding where
            motorists cannot see you near the back corner of their vehicles.
          </li>
          <li>
            <b>Maintain a safe speed:</b> Make sure you obey the speed limit and
            slow down when traffic is congested or the roadways are slick.
          </li>
          <li>
            <b>Use caution when lane splitting:</b> While it is now officially
            legal to split lanes in California, it can prove problematic in some
            situations. Drivers often fail to notice motorcyclists who are
            passing them between lanes of traffic. (You can read more on the
            legality of lane splitting on our blog:
            <em>
              {" "}
              <Link
                to="/blog/lane-splitting-expected-to-become-legal-in-california"
                target="new"
              >
                Lane Splitting in California
              </Link>
              )
            </em>
          </li>
          <li>
            <b>Stay sober:</b> Riding a motorcycle while under the influence of
            alcohol and/or drugs is extremely dangerous to you and surrounding
            motorists. Intoxicants dull a rider's reflexes and impairs his or
            her cognitive and motor functions.
          </li>
          <li>
            <b>Always wear a helmet and protective gear:</b> California law
            requires the use of helmets. Riders and their passengers should also
            protect their legs, arms and hands with proper attire and
            accessories such as leather gloves and boots.
          </li>
          <li>
            <b>Take motorcycle safety courses:</b> Riders who have recently
            received skill training are less likely to be involved in an
            accident.
          </li>
        </ul>
        <p>
          Discuss your case with our team of motorcycle accident lawyers today
          for free, at <strong> 951-530-3711</strong>.
        </p>
        <h2>What To Do If You're Involved in a Riverside Motorcycle Crash</h2>
        <p>
          Even the safest motorcycle riders can be involved in an injury
          accident. What you do immediately after an accident can affect your
          ability to receive compensation for your losses.
        </p>
        <ul>
          <li>
            Make sure you notify the authorities right away, typically the
            Riverside Police Department or Sheriff
          </li>
          <li>Remain at the scene of the crash</li>
          <li>
            Gather contact information from everyone involved and eyewitnesses
          </li>
          <li>Take photos of the vehicles and accident site</li>
          <li>See a doctor right away</li>
          <li>Seek out the legal resources to protect your legal rights</li>
        </ul>
        <p>
          Reaching out to our{" "}
          <strong> Riverside Motorcycle Accident Lawyers</strong> will guarantee
          your best chance of receiving maximum compensation, but getting your
          injuries documented is very important. If you have no legal
          documentation of you getting injured resulting from the crash, it as
          though you were never injured in the first place.
        </p>
        <p>
          Having solid and legal documentation will benefit you and your case
          when the time comes. Our <strong> Bisnar Chase Lawyers</strong> know
          how to fight for you no matter how difficult your case is or becomes
          and will never give up.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wcANLLjVkVY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>
          How to Prove that the Driver Causes the Riverside Motorcycle Accident
        </h2>
        <p>
          Making sure you have an experience Riverside Motorcycle Lawyer is
          important when proving and winning your case. The most effective way
          to prove who was at-fault in your accident is to trace your accident
          to an act of negligence:
        </p>
        <ul>
          <li>Did the driver fail to check their blind spot?</li>
          <li>Did the driver fail to signal for a lane change?</li>
          <li>
            Was the driver under the influence of drugs, alcohol, and/or some
            other substance during the time of your accident?
          </li>
          <li>
            Did the driver have a vehicle malfunction that could have been
            prevented via routine maintenance?
          </li>
        </ul>
        <p>
          If you answered yes to one or more of these questions, you have ground
          to believe that the opposing driver is at-fault and you and your
          Riverside motorcycle accident lawyer can use this in a court of law.
        </p>
        <p>
          Additionally, if the accident was cause by a product defect of the
          opposing vehicle, you may also press charges against the manufacturer
          via a class action lawsuit. Filling a class action lawsuit can not
          only deliver you the compensation that you deserve, but can also save
          lives by preventing defective vehicles from staying on the road.
        </p>
        <h2>Negotiating Your Settlement With the Insurance Companies</h2>
        <p>
          It's a common belief that insurance companies put their customers
          first. This notion is not entirely true. The bureaucracy of many
          insurance companies often cut the injured short with their low-ball
          settlement offers.
        </p>
        <p>
          Regardless of the offer on the table, you should talk it over with
          your lawyer first before signing any documentation. The reason for
          this is that more often than not, the settlements that are offered by
          the insurance companies do not fully cover your injuries and losses.
          Your Riverside motorcycle accident lawyer can take what is on the
          table and increase it through persuasion so that you are fully
          compensated.
        </p>
        <p>
          With over 40 years of representing clients in both Riverside and the
          Inland Empire, Bisnar Chase has the experience you need in your
          Lawyer. Our 96% success and high approval rating have made us the
          number one personal injury law firm in the Inland Empire and a force
          to be reckoned with concerning Southern Californian auto insurance
          companies.
        </p>

        <h2>Why Choose Bisnar Chase for Your Motorcycle Collision Claim?</h2>
        <p>
          A large number of motorcycle accidents result in catastrophic injuries
          such as spinal cord injuries and traumatic brain injuries. Without a
          steel frame, seat belt or airbag to protect them, motorcycle riders
          can suffer life-altering injuries in even low-speed collisions. In
          such cases, victims can pursue financial compensation by filing a
          personal injury claim. A successful claim can result in support for
          medical bills, lost wages, loss of earning potential as well as pain
          and suffering.
        </p>
        <p>
          When a victim is killed in a Riverside motorcycle crash due to someone
          else's negligence or wrongdoing, his or her family may file a wrongful
          death claim to receive support for their losses. A successful wrongful
          death claim can result in support for medical costs, funeral expenses,
          lost future wages, loss of inheritance, pain and suffering, loss of
          emotional support and other related damages.
        </p>
        <p>
          Call our experienced{" "}
          <strong> Riverside Motorcycle Accident Lawyers</strong> at{" "}
          <strong> 951-530-3711</strong> for a free consultation.
        </p>

        {/* <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad> */}

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">6809 Indiana Ave #148</div>
                <div>
                  <span itemProp="addressLocality">Riverside</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92506</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(951) 530-3711</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3309.9154865717596!2d-117.39422308434722!3d33.94330173090969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcb04c13c80f6b%3A0x2cf1f6658186d5f5!2s6809%20Indiana%20Ave%20%23148%2C%20Riverside%2C%20CA%2092506!5e0!3m2!1sen!2sus!4v1567714957965!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
