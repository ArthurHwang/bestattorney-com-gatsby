// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Dog Bite Lawyers - Dog Attack Attorneys"
        pageDescription="The dog bite lawyers at Bisnar Chase have years of experience with dog bite cases and can recover significant damages for you!"
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you've been injured in a dog attack, contact the{" "}
          <Link to="/contact">Fresno dog bite lawyers </Link> of Bisnar Chase
          right away for a free case evaluation.
        </p>

        <p>
          Dog attacks are on the rise throughout the country. The unfortunate
          consequence of a dog attack can leave adults and children either
          seriously injured or even killed. Thankfully, new legislation
          throughout California has been seeking to reduce the number of dog
          bite cases in the state. Fresno dog bite lawyer can help you recover
          any and all damages owed to you after suffering a dog attack.
        </p>
        <p>
          The attorneys at Bisnar Chase are knowledgeable in California's dog
          bite laws and are particularly skilled in incidents occurring in the
          Fresno area. Our Fresno dog bite lawyers have years of experience
          dealing with the nuances of complicated dog bite laws and are often
          able to recover significant damages from dog attacks.
        </p>
        <h2>Fresno California Dog Bite Laws</h2>

        <img
          src="/images/aggressive-dog.jpg"
          className="imgright-fixed"
          alt="aggressive dog attack"
        />
        <p>
          Each state and city has their own set of dog bite laws. In California
          every dog that is involved in a bite is the "strict liability" of its
          owner. California has particularly severe laws concerning dog owners
          and their liability should their pet attack someone else.
        </p>
        <p>
          Our Fresno dog bite attorneys will handle each aspect of your case,
          from investigating all of the details of the incident to obtaining
          financial damages from every party involved.
        </p>
        <p>
          If the dog attack resulted in extremely serious injuries, punitive
          damages can be sought as well as the usual financial damages. Your
          Fresno dog bite attorney may also be able to recover damages from
          property owners, city and state officials, and anyone else involved in
          the dog attack.
        </p>
        <p>
          The only condition for a dog bite case in California is that a dog
          attacked someone else and the attack resulted in some injury, even if
          only psychological injuries. If you or someone you know has been
          injured by a dog in the Fresno area contact our office for a free
          consultation.
        </p>
        <h2>Who is Responsible for a Fresno Dog Attack Injury?</h2>
        <p>
          Fresno dog owners whose dogs attack others are liable for all medical
          expenses relating to the incident.
        </p>
        <h3>
          A Fresno dog attack lawyer can also seek damages for the following:
        </h3>
        <ul>
          <li>Emotional trauma</li>
          <li>Loss of wages</li>
          <li>Loss of school</li>
          <li>
            All medical bills and potential therapies needed, and pain and
            suffering
          </li>
        </ul>
        <p>
          It is important to never sign or agree to settle any amount with
          anyone before contacting our office to learn about your rights.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_LQ8M7WPjik"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>When Should You Contact A Fresno Dog Attack Attorney?</h2>
        <p>
          Because there are several time limits known as "statutes of
          limitations" in California dog bite cases, it is important to file
          your claim as early after the incident as possible.
        </p>
        <p>
          By contacting our Fresno dog bite injury lawyers you will have an
          experienced and professional legal team handling every aspect of your
          case. This allows you, the injured party, to recover from your damages
          while we work hard to obtain your legally owed amounts.
        </p>
        <p>
          Most every dog bite case we handle is done via a contingent fee.
          Contingent fees mean that your Fresno dog attack attorneys are only
          paid if we win your case. You pay nothing unless we win your case, and
          our payment is a predetermined amount of your award only. There are
          never any hidden charges.
        </p>
        <p>
          Contact one of our Fresno dog bite lawyers to discuss your rights,
          recover your legally owed damages, and move on with your life.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
