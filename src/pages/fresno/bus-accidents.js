// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Bus Accident Attorneys - Bus Crash Lawyers"
        pageDescription="Our experienced bus accident attorneys are dedicated to representing bus crash cases throughout Fresno. Contact today for FREE consultation."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Bus Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you've suffered a bus accident injury, contact our top-rated{" "}
          <Link to="/contact">Fresno bus accident laywers </Link> today for a
          Free case evaluation.
        </p>

        <h2>Working With A Fresno Bus Accident Attorney</h2>
        <p>
          We offer free consultations and our Fresno bus crash lawyers perform
          most bus accident lawsuits on a contingent fee basis. Contingent fees
          are a small amount of our client's successful award. Our clients never
          pay us unless we win their case for them and our fees only come from
          the award we win. There is no payment unless we win your bus accident
          suit. There are never any hidden charges.
        </p>
        <p>
          It is important to never sign or agree to any settlement with a bus
          company, insurance agency, or other party without contacting Bisnar
          Chase first. Our Fresno bus accident attorneys will take care of all
          communication between the involved parties. Contact us immediately for
          a Free consultation.
        </p>
        <h2>Fresno Bus Accident Injuries Are Cause To Be Alarmed</h2>
        <p>
          Over the years bus accidents have become increasingly common and have
          resulted in serious and deadly injuries and harm to many involved.
          Because the laws surrounding bus crashes and accidents in California
          are always changing, and vary depending on which type of bus is
          involved in the accident, it is always wise to seek legal advice after
          being involved in an accident.
        </p>
        <p>
          A Fresno bus accident lawyer can help you recover monetary
          compensation associated with your accident related injuries. Our
          Fresno bus accident attorneys are dedicated to representing bus
          accident cases throughout Fresno. Our{" "}
          <Link to="/fresno/personal-injury">
            Fresno personal injury lawyers
          </Link>{" "}
          are available to give you a free consultation on your potential case.
          If you or someone you love has been involved in a bus accident, you
          may have significant financial awards due to you. Please contact us
          immediately.
        </p>
        <h2>There are Several Types of Bus Accidents that Can and Do Occur.</h2>
        <LazyLoad>
          <img
            src="/images/bus-accident.jpg"
            className="imgleft-fixed mb"
            alt="fresno bus accident injury"
          />
        </LazyLoad>
        <p>
          Each bus accident has its own particular set of circumstances. Whether
          you were involved in an accident on a Greyhound, bus line charter,
          school bus accident, city bus, or privately owned and operated bus you
          will likely have a claim to make. An experienced Fresno bus accident
          attorney educated in the area's particular laws can help you recover
          damages that are owed to you as a result.
        </p>

        <h2>Damages May be Recovered for All Types of Bus Accident Injuries</h2>
        <p>
          Even if the accident took place at a bus station or somewhere near the
          bus itself rather than a crash or accident onboard the bus. More
          serious cases and significant damages are often recovered in school
          bus accidents which usually involve children. Our Fresno bus accident
          attorneys are dedicated professionals who work in our client's best
          interests. We may be able to recover considerable compensation due to
          injury, wrongful death, medical bills, loss of wages, pain and
          suffering, mental anguish and other injuries linked to bus accidents.
        </p>
        <p>
          While seeking medical attention should always be your first priority
          it is also important to file a legal claim with a Fresno bus crash
          lawyer as soon as possible. California has several laws called
          "statutes of limitations" that make an injured party unable to file a
          claim after a certain amount of time, regardless of guilt or evidence.
          Once you contact our office we will handle your case for you
          completely, so that you and your family can recover from the trauma of
          the incident.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
