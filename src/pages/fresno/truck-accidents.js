// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Truck Accident Lawyer - Semi Truck Accident Attorneys"
        pageDescription="Accidents and injuries involving large trucks are often serious. Call for a Free Consultation with a qualified Fresno truck accident lawyer."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Truck Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          The massive size and weight of big rigs such as tractor-trailers or
          semi-trucks make them much more dangerous on our highways. Because of
          their large size and the momentum they generate, large truck accidents
          are often catastrophic, resulting in serious injuries and deaths.
          Often, it is the occupants of smaller passenger vehicles that are
          severely injured in collisions involving large trucks. Truck accident
          investigations can become extremely complex because of the involvement
          of multiple parties and agencies. It is important that injured truck
          accident victims and their families get the necessary resources on
          their side to obtain justice and fair compensation for their
          significant losses.
        </p>
        <h2>Fighting for Your Rights in Fresno</h2>
        <p>
          Trucking firms usually have a large contingent of attorneys and
          insurance company representatives working for them, trying to protect
          their best interests. Injured truck accident victims need a strong,
          resourceful and tenacious advocate on their side who will fight for
          their rights and ensure that they are fairly and fully compensated for
          their losses. The{" "}
          <Link to="/fresno"> experienced Fresno truck accident attorneys</Link>{" "}
          at BISNAR CHASE have helped thousands of severely injured accident
          victims and their families get the support they need after a
          catastrophic crash. We have a 98 percent success rate with our cases.
          Our track record speaks for itself. In addition, we give all our
          clients a no-win, no-fee guarantee, which means you do not pay any
          fees or costs unless we win your case. To obtain more information
          about pursuing your legal rights, please contact us.
        </p>

        <h2>Causes of Truck Accidents</h2>
        <p>
          The Federal Motor Carrier Safety Administration (FMCSA) conducted a
          study on the causes of truck accidents nationwide and found that
          driver behavior was by far the most common cause of these types of
          crashes. In fact, truck accidents are 10 times more likely to result
          from driver behavior than other factors such as road conditions,
          weather and vehicle performance. Therefore, drivers can help prevent
          these catastrophic crashes by obeying the speed limit, remaining
          attentive and exercising safe driving habits. Acts of truck driver
          negligence that can lead to a crash include driving while distracted,
          driving while fatigued, exceeding the speed limit, operating a vehicle
          while under the influence, changing lanes without looking, turning
          across the path of other vehicles and tailgating.
        </p>
        <h2>When and Where Truck Accidents Occur in Fresno</h2>
        <p>
          About 63 percent of truck accidents occur on urban roadways such as
          interstates, freeways and expressways. This is why so many Fresno
          truck accidents occur along the 5 Westside Freeway as well as highways
          41, 99, 168, and 180. However, truck accidents can occur at any
          location at any time. About 54 percent of accidents included in the
          FMCSA study involved trucks that were going straight at a steady speed
          at the time of a crash. Another 15 percent of trucks involved in the
          study were negotiating a curve, 10 percent were stopped in traffic and
          5 percent were attempting to decelerate before the collision. The size
          of trucks makes them dangerous in all of these situations. Colliding
          with a truck, whether it is moving quickly or not, can have
          devastating consequences.
        </p>
        <h2>Types of Truck Accidents</h2>
        <p>
          One type of crash that is unique to tractor-trailers is an "underride
          crash." This is when a smaller vehicle is forced underneath the
          trailer of a large truck. Side and rear underride crashes are often
          fatal because the top of the smaller vehicle is crushed upon impact
          resulting in fatal head injuries for the occupants. When these types
          of collisions occur, it must be determined if the trailer had adequate
          underride protection and if the truck driver turned in front of the
          victim's vehicle or braked suddenly without warning. Other common
          types of truck accidents include rear-end collisions, roadside
          departures, rollovers, sideswipes, head-on collisions and jackknifing.
        </p>
        <h2>Truck Accidents and Hours of Operation</h2>
        <p>
          Driving any type of vehicle while fatigued is dangerous, but it is
          particularly hazardous to operate a large truck while drowsy. Many
          drivers, however, attempt to travel long distances without taking a
          break. Some even violate federal laws regarding the amount of hours
          they can work without rest to meet unrealistic delivery schedules and
          deadlines. Under federal law, truck drivers are not allowed to drive
          beyond the 14th consecutive hour when they come on duty after 10 hours
          off duty. Additionally, they may only drive if eight hours or less
          have passed since the end of their last off-duty or sleeper berth
          period of at least 30 minutes. These regulations are meant to reduce
          the chances of driver fatigue, but many truck drivers and trucking
          companies violate these rules in the pursuit of higher profits.
        </p>
        <h2>Preserving the Evidence</h2>
        <p>
          Fatigued, distracted, impaired and careless drivers can be held
          accountable for the damages they cause. In order to receive
          compensation, injured victims must prove how the accident occurred and
          how the driver's negligence contributed to the accident. Evidence
          preservation is critical in any accident case, but it is particularly
          crucial in truck accident cases. It is important that all vehicles
          involved in the crash are preserved, including the truck. A{" "}
          <Link to="/truck-accidents">knowledgeable truck accident lawyer</Link>{" "}
          will help victims request and preserve important pieces of evidence
          including truck driver logs, cell phone records and vehicle
          maintenance logs, which can help shed light on how or why the crash
          occurred. These crucial pieces of evidence must be preserved before
          they are lost, misplaced or destroyed.
        </p>
        <h2>Other Potentially Liable Parties</h2>
        <p>
          One of the reasons why truck accidents are so complicated is that
          there are a number of potentially liable parties. Truck drivers are
          responsible for their own actions, but they are not the only party
          that can be included in a claim. Truck owners can be held accountable
          for crashes related to their failure to properly inspect, maintain and
          repair their vehicles. Trucking companies are responsible for the
          actions of their employees when they hire drivers with a history of
          careless behavior. Truck part manufacturers are potentially liable
          when a mechanical malfunction or faulty part leads to a crash.
        </p>
        <h2>Illegal speeding is a common cause of large truck accidents</h2>
        <p>
          California has many particular laws that apply solely to trucks
          traveling on the state's roads and highways. Heavy duty trucks,
          18-wheelers, and big rigs hauling cargo must adhere to a much slower
          speed limit than other drivers. Most truck accidents in California are
          found to be the fault of the truck driver and caused by the driver
          traveling at an illegal rate of speed. A Fresno truck accident lawyer
          can help you recover all damages suffered due to the negligence of a
          truck driver.
        </p>
        <LazyLoad>
          <img
            src="/images/dangers-of-trucks.jpg"
            alt="infographic: common causes of truck accidents"
            width="100%"
          />
        </LazyLoad>
        <p>
          Our lawyers have successfully won significant monetary awards for our
          truck accident clients that include money for automobile damage,
          medical expenses, wrongful death, pain and suffering, insurance
          problems, trucking company disputes, and mental distress. The amount
          of monetary compensation that our Fresno truck accident attorneys may
          be able to recover for you will vary depending on the details of your
          case. Contact our office immediately for a free consultation.
        </p>
        <p>
          Long distance trucking is a dangerous business and many truckers run
          the risk of falling asleep behind the wheel due to long work hours. In
          many trucking accident cases both the driver and the trucking company
          they work for can be at fault and a lawsuit can be filed against both
          of them. Other cases have involved faulty truck accessory damages,
          motor freight company problems, farming equipment accidents, and drug
          or alcohol use. A Fresno trucking accident lawsuit can seek damages
          for many reasons stemming from an accident.
        </p>
        <p>
          In order to file a lawsuit with our Fresno trucking accident
          attorneys, contact us immediately, before signing any paperwork from
          anyone else or admitting any guilt. The qualified truck accident
          lawyers at BISNAR CHASE work on a contingent fee basis meaning that we
          only get paid if we win your case. If we lose then you don't pay us
          anything.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
