// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Slip and Fall Lawyers - Slip Trip and Fall Attorneys"
        pageDescription="Bisnar Chase represents clients who have been injured in slip and fall cases in Fresno, CA. Contact us for a Free Consultation."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Slip and Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          If you slipped on an unsafe surface and were injured, contact our{" "}
          <strong>Fresno slip and fall attorneys</strong> right away to discuss
          your legal options.
        </p>

        <p>
          Each year slip and fall injuries come in second to the number of
          people injured in car accidents.
        </p>
        <p>
          If you or a loved one has been injured by a slip and fall accident,
          you may have a strong legal case. Contacting a Fresno slip and fall
          lawyer as soon as possible can ensure that your rights are protected.
        </p>
        <p>
          The office of BISNAR CHASE represents clients who have been injured in
          slip and fall cases. Our{" "}
          <Link to="/fresno/personal-injury">
            {" "}
            Fresno personal injury lawyers
          </Link>{" "}
          have 30 years of experience in trying slip and fall cases including
          slips and falls. If you or someone you love been injured due to a slip
          and fall on someone elses property, you may have a case. Contact our
          offices for a free consultation and case evaluation.
        </p>
        <h2>There are Many Causes of Slip and Fall Injury Cases.</h2>
        <img
          src="/images/steps_without_handrails.png"
          className="imgleft-fixed"
          alt="wet slippery steps without handrails"
        />
        <p>
          Therefore it is important to make a list of all details after your
          injury: Unclean or unkempt areas, floor condition, any witnesses,
          warning signs such as "wet floor" and any others can help your case.
        </p>
        <p>
          An experienced Fresno slip and fall attorney can determine the fault
          of the slip and fall and can. We will handle all aspects of your case
          and keep you updated as we go. We will deal with all parties involved
          so that you do not have to. They include employers, insurance
          companies, any medical billing issues and any other sources if
          necessary.
        </p>

        <h2>Working With A Fresno Slip and Fall Lawyer</h2>
        <p>
          Our Fresno slip and fall attorneys are respected in the field and will
          handle your case as professionally and aggressively as possible. You
          may be able to recover significant financial awards relating to your
          case, such as medical expenses, loss of work, pain and suffering,
          mental distress, therapies needed and many more.
        </p>
        <p>
          Because California's slip and fall laws are so complicated it is
          always important to have qualified legal representation before making
          a claim.
        </p>
        <p>
          Most of our Fresno slip and fall lawsuits are handled on a contingent
          basis which means that we do not get paid if we lose your case. This
          fee adds incentive to our attorneys and ensures that we only take
          cases we truly believe in.
        </p>
        <p>
          Our clients only pay us a predetermined portion of their successful
          award, never anything more. There are no hidden charges.
        </p>
        <p>
          Your Fresno slip and fall lawsuit must be filed in a certain amount of
          time legally known in California as a "statute of limitations". If you
          do not file your claim in time you may not be able to file one at all,
          regardless of any obvious guilt of other people involved.
        </p>
        <p>
          While medical attention should always be your first concern, finding
          and filing your slip and fall lawsuit in a timely manner should also
          be a vital issue in your case.
        </p>
        <p>
          If you or a loved one has fallen or tripped while on another's
          property or at work then please{" "}
          <Link to="/contact">contact Bisnar Chase immediately </Link> for a
          free consultation.
        </p>
        <p>
          Our Fresno slip and fall attorneys are here for you and are one of the
          most effective, professional, and respected legal teams in the
          country.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
