// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Product Liability Lawyer - Defective Product Attorney"
        pageDescription="BISNAR CHASE has the experience which is required to successfully represent strict product liability cases in Fresno CA. Call for a free consultation."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Product Liability Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been injured due to a defective product, contact our
          experienced{" "}
          <strong>
            {" "}
            <Link to="/contact">Fresno product liability lawyers </Link>
          </strong>{" "}
          for a free consultation today.
        </p>

        <p>
          Every person in the United States is protected from defective and
          injurious products by a set of laws known collectively as products
          liability.
        </p>
        <p>
          When consumers use any product in the manner prescribed and outlined
          in that product's safety manual and they are injured or even killed
          then they likely have a strong legal case. By contacting a Fresno
          products liability lawyer, consumers can learn about their rights and
          potentially file a lawsuit.
        </p>
        <p>
          The office of BISNAR CHASE has the experience which is required to
          successfully represent product liability cases in Fresno. Our Fresno
          products liability lawyers are able to handle lawsuits throughout the
          region and have years of successful settlements.
        </p>
        <p>
          If you or someone you love has been injured by a product then please
          contact our office for a free consultation on your legal rights.
        </p>
        <h2>Common Products Liability Cases</h2>
        <p>
          One of the most common products liability cases occurs after a car
          accident caused by a{" "}
          <Link to="/fresno/auto-defects">defective auto part</Link>. Car
          manufacturers and marketers are liable for the design and performance
          of their vehicles and can be held responsible for significant damages
          when their products fail to operate safely.
        </p>
        <p>
          Other common and serious cases handled by our Fresno products
          liability attorneys include defective drugs, baby toys, medical
          equipment failure, construction supplies, all vehicle problems, and
          clothing issues.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/VBC0aa6eUEM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Working With a Fresno Defective Product Attorney</h2>
        <p>
          A Fresno defective product attorney may be able to recover significant
          financial damages that can help you pay for all medical expenses and
          future medical bills stemming from the incident. Other areas covered
          in products liability lawsuits include pain and suffering, therapy,
          property damage, wrongful death, mental distress and many others.
        </p>
        <p>
          If you or someone you love has been injured by a product it is
          important not to sign any documents relating to the accident without
          consulting with a Fresno defective products lawyer first. Because
          products liability cases can be difficult, you should document the
          incident as closely as possible and never agree to any guilt or make
          any statements without a lawyer.
        </p>
        <p>
          Our Fresno defective product lawyers will handle all aspects of your
          case and work on a contingent basis in most cases. Contingent fees are
          payments given to law firms following a successful case outcome.
        </p>
        <p>
          If we do not win your case, we receive no payment at all. Contingent
          fees ensure that frivolous lawsuits are not filed and that every
          client receives the hardest working legal team available to them.
        </p>
        <p>
          Because products liability laws are so complex a product does not
          necessarily have to be defective for a lawsuit to be filed if that
          product's normal functioning causes the injuries. Anattorney from our
          office will determine the exact legal causes of all injuries sustained
          and file an appropriate lawsuit.
        </p>
        <p>
          The office of Bisnar Chase offers a free consultation regarding any
          potential products liability lawsuit case. Please contact one of our
          Fresno product liability lawyers today for a Free case evaluation to
          learn about your rights.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
