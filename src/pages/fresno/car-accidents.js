// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Car Accident Lawyer - Fresno Auto Accident Attorney"
        pageDescription="Fresno Car Accident Lawyer John Bisnar has been helping auto accident victims for over 30 years. Free consultation."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />{" "}
        <Link
          to="https://www.flickr.com/photos/81918828@N00/2118543745/"
          title="Van Ness looking NW in Fresno in 1961 by Lance &amp; Cromwell, on Flickr"
          rel=""
          target="blank"
        >
          <img
            src="https://farm3.staticflickr.com/2186/2118543745_540877cc27.jpg"
            width="100%"
            alt="fresno car accident lawyers"
          />{" "}
        </Link>
        <b>
          <p>
            If you've been hurt in a car accident at no fault of your own,
            contact our Fresno car accident lawyers for a free case review. You
            may be entitled to compensation. Call or fill out our online case
            evaluation form for a quick response.
          </p>
        </b>
        <p>
          Some of the best{" "}
          <Link to="/fresno">Fresno accident injury attorneys </Link> provide
          much-needed advice to those who find themselves involved in a car
          accident in Fresno, California. Skilled car accident lawyers know
          that, there are endless details one must keep track of if medical
          expenses are to be fully recovered. Those who think car accidents only
          happen to somebody else should refer to Fresno's car collision
          statistics.
        </p>
        <h2>Alarming Car Accident Statistics</h2>
        <p>
          In 2006, the California Highway Patrol's Statewide Integrated Traffic
          Records System (SWITRS) reported that 36 people were killed and 1,677
          were injured in Fresno car crashes. In addition, 10 pedestrians lost
          their lives and 133 were injured in car collisions. Car accidents also
          killed three bicyclists and injured 83. Motorcycle accidents killed
          two and injured 75. Drunk drivers caused 13 fatalities and 207
          injuries. In 2007, 35 car collisions ended in 37 people losing their
          lives.
        </p>
        <h2>Keep Detailed Records After Injury</h2>
        <p>
          Highly experienced Fresno car accident lawyers frequently advise their
          clients to keep detailed records of their medical condition after an
          accident. This can give credibility to medical injury claims presented
          later in court or at the settlement table. Yet, while keeping a log or
          diary can be helpful in tracking one's medical condition after an
          accident, there are those who find these procedures limiting to their
          recovery
        </p>
        <h2>Don't Dwell on Injuries</h2>
        <p>
          Keep in mind that your main goal after an accident should be to
          recover from your injuries. If maintaining a logbook of your physical
          and emotional recovery leads you to excessively dwell on your
          injuries, no amount of eventual compensation can ever balance out this
          negative aspect of record keeping. Most every Fresno car collision
          lawyer will admit that while keeping daily records of your medical
          condition can enhance your settlement, in no way should this ever
          jeopardize your return to good health.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
