// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Premises Liability Lawyers - Bisnar Chase Attorneys"
        pageDescription="If you have been injured on another person's property, our Fresno premises liability lawyers can help you get compensation."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Premises Liability Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you've been injured due to an unsafe property, contact our{" "}
          <strong>Fresno premises liability lawyers</strong> today for a Free
          case consultation.
        </p>
        <h2>CA Premises Liability Law</h2>
        <img
          src="/images/slip-and-fall.jpg"
          className="imgright-fixed"
          alt="premises accident injury"
        />{" "}
        <p>
          Premises liability is an area of the law that deals in a land or
          property owner's necessity to properly maintain and reasonably protect
          people on their property.
        </p>
        <p>
          If you or someone you love has been injured on another person's
          property, or has been the victim of a crime on someone else's property
          you most likely have a strong premises liability case. A{" "}
          <Link to="/fresno"> Fresno premises liability attorney </Link> can
          help you file and recover any and all damages you sustained in the
          incident.
        </p>
        <p>
          Our Fresno premises liability lawyers have more than 30 years
          experience and can handle every aspect of your claim including dealing
          with all parties involved, insurance problems, medical billing
          questions, and more.
        </p>
        <p>
          Land, property, and business owners are required by law to keep their
          properties in a reasonably safe condition. When the owners fail to
          maintain their property - perhaps there is construction debris
          remaining on the ground, an unclean environment, or even broken
          lighting or security features - they can be held liable for any
          accident or crime occurring on the property.
        </p>
        <p>
          Our Fresno premises liability attorneys can determine the facts of
          your case and investigate who is ultimately at fault.
        </p>
        <h2>
          Who is responsible for injuries caused by a crime that takes place on
          a rental property?
        </h2>
        <p>
          Many times even a crime that takes place on the property can be a
          violation of premises liability laws. If a person was robbed and their
          apartment wasn't properly secured, there was inadequate or broken
          lighting in the complex, or security features were broken, the victim
          can file a lawsuit against their landlord.
        </p>
        <p>
          In many cases the landlord or property owner's negligence can lead to
          the development of crime on their property and they are legally at
          fault for the crimes. A Fresno premises liability attorney can
          investigate incidents involving injury or crime on anothers property
          to see if the case falls into this category.
        </p>
        <p>
          Another common incident occurs when someone is injured on anothers
          property and their injury was the direct result of some fault of the
          owner of the property. A store or mall that isn't properly cleaned, an
          unsecured construction site, a neighbor whose fence isn't properly
          built or safe are all instances of premise liability violations.
        </p>
        <h2>A Fresno Premises Liability Attorney Who Works With You</h2>
        <LazyLoad>
          <img
            src="/images/bc-firm.jpg"
            className="imgcenter-fixed"
            alt="Fresno premises liability law firm bisnar chase"
          />
        </LazyLoad>
        <p>
          Our Fresno premise liability lawyers have years of experience in
          successfully trying all manner of lawsuits of this type.
        </p>
        <p>
          Bisnar Chase handles most cases on a contingent basis, which means
          that we only are paid if we win your case, never before, and never
          more than a preset percentage of the award. Contingent fees ensure
          that your Fresno premise liability attorney is working in a timely
          manner and is pursuing your case to the best of their ability.
        </p>
        <p>
          Our Fresno premise liability lawyers have successfully won cases that
          award our clients' significant financial damages to cover medical
          expenses, pain and suffering, loss of wages, wrongful death, mental
          distress, and much more.
        </p>
        <p>
          It is important to contact an attorney before it is too late.
          California's statute of limitations may apply and your case may be
          thrown out of court. Please contact our Fresno Premises Liability
          Lawyers for a Free Consultation today.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
