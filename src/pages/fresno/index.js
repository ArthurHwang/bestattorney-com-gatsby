// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/culver-city-los-angeles-personal-injury.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Fresno",
      population: 527438,
      totalAccidents: 8956,
      intersection1: "East Shaw Ave & North Cedar Ave",
      intersection1Accidents: 34,
      intersection1Injuries: 16,
      intersection1Deaths: 2
    },
    locationBox2: {
      mainCrimeIndex: 365.8
    },
    locationBox3: {
      intersection2: "West Shaw Ave & Marty Ave",
      intersection2Accidents: 24,
      intersection2Injuries: 18,
      intersection2Deaths: 0,
      intersection3: "East Herndon Ave & North Cedar Ave",
      intersection3Accidents: 23,
      intersection3Injuries: 16,
      intersection3Deaths: 0,
      intersection4: "North Palm Ave & North Marks Ave",
      intersection4Accidents: 25,
      intersection4Injuries: 13,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Personal Injury Attorneys - Bisnar Chase Accident Lawyers"
        pageDescription="800-561-4887 | Free consultation with our Fresno personal injury lawyers. Get the compensation you deserve. Serving Fresno CA since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fountain Valley Personal Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            Suffering a serious personal injury – whether it is the result of an
            auto accident, or a <Link to="/fresno/dog-bites">dog attack</Link>,
            or due to a dangerous or defective product – can be a traumatic and
            life-changing event. Such an incident can effect an enormous
            disruption and change to your life and routine, leave victims and
            families with tremendous emotional and financial strain. If you have
            been seriously injured due to someone else's negligence and/or
            wrongdoing, you may be able to recover compensation for your losses.
          </p>
          <p>
            For more than 40 years, the experienced Fresno personal injury
            lawyers at <Link to="/contact">Bisnar Chase</Link> have successfully
            handled an array of personal injury cases for our clients. Our goal
            is to help injured victims and their families{" "}
            <strong>secure maximum compensation</strong> for their losses. We
            value the reputation that we have earned and secured over the years
            by providing quality legal representation and superior customer
            service to our clients and their families. Please{" "}
            <Link to="/contact">contact us</Link> if you are in need of advice
            and guidance with regard to your potential personal injury claim in
            Fresno.
          </p>
          <h2>
            Call 1-800-561-4887 to speak with a personal injury attorney in
            Fresno California.
          </h2>
          <p>
            BISNAR CHASE Personal Injury Lawyers have been successfully and
            aggressively handling cases of severe injury and wrongful death
            including auto accidents, brain injury, spinal cord injury,
            motorcycle accidents, defective products and dog bites since 1978.
          </p>
          <p>
            Our law firm is a committed group of highly effective{" "}
            <Link to="/fresno/personal-injury">
              Fresno Personal Injury Lawyers{" "}
            </Link>{" "}
            and skilled personal injury professionals, focused on the
            representation of the seriously injured and the families of wrongful
            death victims. <strong>Representing Fresno accident victims</strong>{" "}
            in personal injury cases to the best of our ability is our only
            business. It's all we do.
          </p>
          <h2>Why Do Fresno Personal Injury Victims Choose Us?</h2>
          <LazyLoad>
            <img
              src="/images/fresno-5-car-crash.jpg"
              className="imgright-fixed"
              alt="fresno 5 car crash"
            />
          </LazyLoad>
          <p>
            We are aggressive, accomplished, well known and respected by
            insurance companies, corporate wrongdoers and personal injury
            defense attorneys. We guide our Fresno personal injury clients
            through the legal, personal and financial challenges they encounter
            on their way to recovery from their accidents. WE ACHIEVE RESULTS,
            meaning we go after the money like it was our own, and we treat you
            respectfully and considerately along the way.
          </p>
          <p>
            Attorney John Bisnar and Attorney Brian Chase are passionate about
            representing personal injury clients and winning cases. They are
            personally involved in every case. Their cases are limited to the
            larger, more challenging and more economically significant personal
            injury cases. Mr. Bisnar and Mr. Chase have represented thousands of
            personal injury clients and recovered millions of dollars for them.
          </p>
        </div>

        <h2>
          Some of the types of personal injury cases our firm handles in Fresno
          include:
        </h2>
        <ul>
          <li>Automobile Accidents</li>
          <li>Motorcycle Accidents and Bicycle Accidents</li>
          <li>Dog Bite Injuries and Animal Attacks</li>
          <li>Auto Defects</li>
          <li>Head Injuries, Brain Injuries and Spinal Cord Injuries</li>
          <li>Bus Accidents and Mass Transit Accidents</li>
          <li>Premises Liability Accidents and Slip and Fall Accidents</li>
          <li>Defective Product Injuries and Product Liability Accidents</li>
          <li>Watercraft Accidents and Swimming Pool Injuries</li>
          <li>Pedestrian Accidents</li>
          <li>Hit and Run Accidents</li>
          <li>
            Commercial Truck and{" "}
            <Link to="/fresno/truck-accidents">Big Rig Accidents </Link>
          </li>
        </ul>
        <p>
          Over the years we have represented many clients in the Fresno area.
          When asked, Fresno area clients have told us, they did not hire a
          local personal injury attorney because they did not want a local,
          Fresno attorney, especially one who is part of the Fresno "good ole
          boys" lawyer society. They wanted an out of area, big city lawyer.
        </p>
        <p>
          <strong>
            <em>
              It does not cost more to hire the "out of area" professional
              personal injury attorney than it does to hire a local personal
              injury attorney.
            </em>
          </strong>
        </p>
        <p>
          If you have suffered a serious personal injury, we strongly suggest
          that you immediately seek the advice of a very serious personal injury
          lawyer or, better yet, law firm. If you are interested in attorneys
          who are skilled, passionate and down to earth, who regularly handle
          Fresno cases yet are &ldquo;big city attorneys&rdquo;, Call us now for
          an immediate, FREE consultation and evaluation, or{" "}
          <Link to="/contact">click here </Link> to fill out our online form.
          Ask us about what our previous Fresno clients have had to say about
          us.
        </p>
        <p align="center">
          <strong>
            HOLDING WRONGDOERS ACCOUNTABLE FOR THE DAMAGES THEY CAUSE SINCE 1978
          </strong>
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
