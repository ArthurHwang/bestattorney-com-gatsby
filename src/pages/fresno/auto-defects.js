// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "auto-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fresno Auto Defect Lawyer - CA Product Liability Attorneys"
        pageDescription="Call for award-winning auto defect lawyer in Fresno CA. Our Product liability attorneys offer a free consultation & have been helping CA residents since 1978."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fresno Auto Defect Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Automobile accidents are one of the top causes of injury and death in
          the United States. A growing problem has been auto accidents due to
          some defect in the automobile itself.
        </p>
        <p>
          If you or someone you love has been involved in an auto accident and
          you believe it was caused by some defect in the vehicle you may have a
          strong legal case and may be entitled to significant financial
          damages. A <Link to="/auto-defects">Fresno auto defect lawyer </Link>{" "}
          may be able to help you recover monetary damages due to you because of
          your accident.
        </p>
        <p>
          The law office of BISNAR CHASE has years of experience settling auto
          defect lawsuits for Fresno victims. Our Fresno auto defect lawyers are
          available to speak with potential clients in a free consult to
          determine the particulars surrounding your case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/GqhSTlRo3kM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Every Auto Defect Accident Is Different</h2>
        <p>
          <em>
            Because every car accident related to a defective part is different
          </em>
          , it is vital to contact an experienced Fresno auto defect attorney
          who is aware of the nuances surrounding the maker of the vehicle and
          the laws that protect you. Every defective car case consists of
          potentially hundreds of elements which need to be carefully
          investigated by an experienced and professional attorney at our firm.
          Auto defect cases are notoriously difficult to prove, which makes your
          choice of attorney even more important.
        </p>
        <h2>
          What Constitutes a Lawsuit When an Accident is Caused by a Defective
          Auto Part?
        </h2>
        <p>
          Some failure in the automobile, whether it is the breaking of a poorly
          made auto part, or a failure in the design of the entire vehicle
          itself, <em>must have led to the injury or death</em> for an auto
          defect lawsuit to be filed. Many recent lawsuits have involved{" "}
          <Link to="/auto-defects/rollovers">rollover issues </Link> in Sport
          Utility Vehicles (SUVs), pickups, and other large automobiles. These
          lawsuits allege that the design of the auto itself resulted in the
          unsafe conditions and subsequent accidents that occurred. Our Fresno
          auto defect attorneys are all qualified in the potential nuances of
          your case and will investigate any accident thoroughly and quickly.
        </p>
        <p>
          Many other auto defect cases revolve around parts including car seats
          that can fail and lead to serious injuries. Air bags, faulty tailgates
          and rear latches, engine problems, and poor brake or wheel design are
          all examples of potential defective automobile parts that could lead
          to serious accidents. Our Fresno auto defect attorney will brief
          clients before beginning work on their case and investigate every
          possible cause of the accident itself.
        </p>
        <h2>There is a Statute of Limitations for Auto Defect Lawsuits</h2>
        <LazyLoad>
          <img
            src="/images/brian-chase-caoc-award.jpg"
            className="imgright-fixed"
            alt="brian chase, auto defect lawyer"
          />
        </LazyLoad>
        <p>
          A Fresno defective auto lawsuit must be filed in a timely manner as
          there are time limits to every legal claim. Damages can be sought in
          order to pay for medical bills, property loss, mental distress, pain
          and suffering, loss of employment and more. Our lawyers will also
          attempt to recover the maximum amount of monetary compensation which
          is legally owed to you by your insurance agency as well.
        </p>
        <p>
          Brian Chase, partner at{" "}
          <Link to="/fresno/personal-injury">
            Bisnar Chase Personal Injury Attorneys
          </Link>{" "}
          has been one of the leading Product Liability Trial Lawyers for more
          than a decade and has received many awards because of his dilligence
          and passion.
        </p>
        <p>
          Our Fresno defective auto attorneys handle auto defect cases on a
          contingent basis meaning we only get paid if we win your case. There
          is never any obligation on your part as a consumer of our services.
          The law offices of BISNAR CHASE have a long history of successfully
          settling hundreds of auto defect cases. We are here for you, contact
          us today.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
