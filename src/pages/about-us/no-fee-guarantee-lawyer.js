// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California No Win, No Fee Lawyers - Fees and Costs | Bisnar Chase"
        pageDescription="Contact the California No Win, No Fee lawyers of Bisnar Chase for expert legal help with no out-of-pocket expenses. We make sure people of all means can find justice. If we don't win your case, you won't have to pay! Call (949) 203-3814 now."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California No Win, No Fee Attorneys: Fees and Costs</h1>
        <BreadCrumbs location={location} />

        <h2>No Win, No Fee Lawyers – Helping All Clients Find Justice</h2>
        <hr />
        <LazyLoad>
          <img
            src="/images/text-header-images/california-no-win-no-fee-lawyer.jpg"
            alt="California No Win No Fee Lawyer."
            title="Hiring the Best California No Win, No Fee Lawyer Near Me"
            className="imgright-fixed"
            width="200"
            height="235"
          />
        </LazyLoad>

        <p>
          Bisnar Chase is a top-rated California Personal Injury law firm which
          has helped tens of thousands of clients. To make our services
          accessible to everyone, we are
          <strong>No Win, No Fee lawyers</strong>. But what is a No Win, No Fee
          guarantee?
        </p>

        <p>
          Essentially, this means that we offer a guarantee that the client will
          not have to pay until we win their case. This is also sometimes
          referred to as working on contingency.
        </p>

        <p>
          If our team is successful in winning your case – and we have an
          outstanding 96% success rate – our pre-agreed fees will come from the
          compensation award. If we are unsuccessful, you will never have to pay
          us a penny. It’s win-win.
        </p>

        <br />

        <h2>
          Key points of a No Win, No Fee Attorney Promise – Why Is It Important?
        </h2>
        <hr />

        <p>
          At Bisnar Chase, our California No Win, No Fee lawyers believe that
          offering this service is essential. This system is designed to help
          everyone – whether they are financially well off or not – find
          justice.
        </p>

        <p>
          People of all means can suffer an injury, or be hurt in an accident
          caused by the negligence of another person or party. Some attorneys
          require up-front fees when it comes to representing victims. We know
          that not everybody can afford to do that, but we strongly believe that
          this should not stop a victim from fighting for the justice and
          compensation they deserve.
        </p>

        <p>
          With the guarantee offered by Bisnar Chase’s legal team, the
          knowledge, experience, and skills of our No Win, No Fee attorneys are
          on offer to everyone.
        </p>
        <br />

        <iframe
          width="100%"
          height="400"
          src="https://www.youtube.com/embed/D3c_pxDeswg"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
        />
        <br />

        <blockquote>
          If you hire Bisnar Chase and we don’t recover money for you in your
          case, you owe us absolutely nothing. When you retain us, we operate on
          a contingency fee. That means that we get a percentage of the recovery
          that we get for you. If there is no recovery, there is nothing owed.
          This is important to us because it allows us to offer help to those
          who really need it.
          <br />
          <cite>
            -Brian Chase
            <br />
            -Senior Partner, Bisnar Chase Personal Injury Attorneys
          </cite>
        </blockquote>

        <br />
        <h2>Why Hire a No Win, No Fee Guarantee Lawyer in California?</h2>
        <hr />

        <p>
          People who are searching for the right lawyer to handle their case may
          have suffered serious harm, or been subjected to a traumatic incident.
          Hiring the No Win, No Fee attorneys of Bisnar Chase can help ease the
          strain for victims. Here are some of the key advantages:
        </p>

        <ul>
          <li>Victims are protected from paying up-front fees and costs</li>
          <li>
            No fees or costs will ever have to be paid if the case is
            unsuccessful
          </li>
          <li>
            Our method is in writing in your attorney-client agreement. It is a
            guarantee that can give you peace of mind
          </li>
        </ul>
        <br />

        <h2>
          What Fees are Advanced Through a No Win, No Fee Legal Agreement?
        </h2>
        <hr />

        <p>
          The No Win, No Fee system put in place by Bisnar Chase will advance
          all reasonable costs required to help win your case. Depending on the
          demands of the case, this could include:
        </p>

        <h3 className="collapsible">
          <strong>Filing Fees</strong>
        </h3>
        <div className="content">
          <p>
            There are costs associated with filing lawsuits with the California
            courts. We will cover the court costs to give you one less thing to
            worry about. These costs can be considerable for people attempting
            to file their own lawsuit and represent themselves.
          </p>
        </div>
        <h3 className="collapsible">
          <strong>Records and Reports</strong>
        </h3>
        <div className="content">
          <p>
            It is common to use police reports, medical records, and other
            similar documentation as evidence when building a personal injury
            claim case. These reports are important as they can help to prove
            the legitimacy of a complaint or injury, providing unbiased
            testimony for the court. However, it will often cost a fee to obtain
            them.
          </p>
        </div>
        <h3 className="collapsible">
          <strong>Expert Witnesses</strong>
        </h3>
        <div className="content">
          <p>
            Some cases require an authoritative voice to speak on the
            circumstances of an incident and provide expert context and advice.
            Having an expert witness supporting your claim can be an important
            step in building the best case possible, but can also be costly.
          </p>
        </div>
        <h3 className="collapsible">
          <strong>Investigations</strong>
        </h3>
        <div className="content">
          <p>
            Proving fault in a personal injury case is not always easy. A
            private investigator will sometimes be employed to look into a case
            and help assemble certain evidence. The cost will depend on the
            services the investigator is hired for, but can easily rise to
            hundreds or thousands of dollars.
          </p>
        </div>
        <br />

        <h2>The No Win, No Fee Process – How to Hire a Lawyer</h2>
        <hr />

        <p>
          If you have been injured due to the negligence of another person or
          party and want to take legal action, we can help. As contingency
          lawyers, we allow all clients to take advantage of our No Win, No Fee
          system.
        </p>

        <p>
          Here’s how you can get started, along with a full rundown of the legal
          process at work when you choose to work with the personal injury
          specialists at Bisnar Chase:
        </p>
        <br />

        <center>
          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <h3 style={{ textAlign: "left" }} className="numberCircle">
              <strong>1 &#10157; Contact Bisnar Chase</strong>
            </h3>
            <h4 className="panel-body" align="left">
              Step one is to get in touch with our personal injury lawyers,
              either <Link to="tel:+1-949-203-3814">by phone</Link> or by{" "}
              <Link
                to="/contact"
                target="new"
                title="Contact Bisnar Chase Personal Injury Attorneys"
              >
                clicking here
              </Link>{" "}
              to email us.
            </h4>
          </div>

          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <div className="panel-heading">
              <h3 style={{ textAlign: "left" }} className="numberCircle">
                <strong>2 &#10157; Free Consultation</strong>
              </h3>
            </div>
            <h4 className="panel-body" align="left">
              Every prospective client gets a free consultation with our expert
              staff. We can discuss your case with no obligations, and decide
              how best to proceed.
            </h4>
          </div>

          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <div className="panel-heading">
              <h3 style={{ textAlign: "left" }} className="numberCircle">
                <strong>3 &#10157; Sign With a No Win, No Fee Guarantee</strong>
              </h3>
            </div>
            <h4 className="panel-body" align="left">
              If both parties decide it is a good fit, sign with Bisnar Chase
              without having to pay a dime up front. Our compassionate staff can
              talk you through this process to make sure you feel completely at
              ease before signing anything.
            </h4>
          </div>

          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <div className="panel-heading">
              <h3 style={{ textAlign: "left" }} className="numberCircle">
                <strong>4 &#10157; Case Progress</strong>
              </h3>
            </div>
            <h4 className="panel-body" align="left">
              Trust us to build the best case possible for you. We will work
              with you to assemble evidence, before either negotiating a
              settlement or taking your case to trial.
            </h4>
          </div>

          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <div className="panel-heading">
              <h3 style={{ textAlign: "left" }} className="numberCircle">
                <strong>5 &#10157; Stay Informed</strong>
              </h3>
            </div>
            <h4 className="panel-body" align="left">
              Our attorneys will contact you regularly to update you on the
              progress of your case and answer any questions you have. Staff are
              also diligent about returning calls and emails within one business
              day.
            </h4>
          </div>

          <div style={{ marginBottom: "2rem" }} className="panel panel-info">
            <div className="panel-heading">
              <h3 style={{ textAlign: "left" }} className="numberCircle">
                <strong>6 &#10157; Resolution</strong>
              </h3>
            </div>
            <h4 className="panel-body" align="left">
              If we win your case, our contingency percentage will come from the
              compensation award, ensuring that you are not hit by any
              out-of-pocket costs or fees. If we lose, you do not owe anything.
            </h4>
          </div>
        </center>
        <br />

        <h2>
          What Sets Bisnar Chase Apart from Other No Win, No Fee Lawyers Near
          Me?
        </h2>
        <hr />

        <p>
          Offering a No Win, No Fee promise has become commonplace among
          personal injury lawyers. It is considered an essential system to make
          sure clients are protected.
        </p>

        <p>
          What sets Bisnar Chase apart is our firm’s dedication to superior
          representation. You can count on our attorneys offering a no win, no
          fee system. But they also offer elite skill, expert knowledge, and
          unparalleled compassion. We are dedicated to winning your case and
          securing the compensation you deserve, while treating all clients with
          warmth and respect.
        </p>

        <p>
          Whether you have been injured in a car accident, hurt in a
          hit-and-run, or left in pain after a slip-and-fall accident, Bisnar
          Chase can help. Contact us now for a free consultation. Our experts
          will review your case and help guide you in the right direction.
        </p>

        <p>
          Make sure financial concerns do not stand between you and justice. Let
          our expert <strong>California No Win, No Fee lawyers</strong> shoulder
          the burden for you, making sure you do not pay a penny until we win
          your case. Click one of the buttons below to get started.
        </p>
        <center>
          <table>
            <tr>
              <td>
                <div className="button_cont" align="center">
                  {" "}
                  <Link
                    style={{ fontSize: "2rem" }}
                    className="example_b"
                    to="tel:+1-949-203-3814"
                    target="_blank"
                  >
                    Call Bisnar Chase Now! &nbsp;&nbsp;&nbsp;
                    <span>&#9742;</span>
                  </Link>
                </div>
              </td>
              <td>
                <div className="button_cont" align="center">
                  {" "}
                  <Link
                    style={{ fontSize: "2rem" }}
                    className="example_b"
                    to="/contact"
                    target="_blank"
                  >
                    Email Bisnar Chase Now! &nbsp;&nbsp;
                    <span>&#9993;</span>
                  </Link>
                </div>
              </td>
            </tr>
          </table>
        </center>

        <br />
        <div style={{ textAlign: "center", marginBottom: "2rem" }}>
          <img
            src="/images/jbsig.gif"
            alt="No Fee Guarantee"
            className="imgcenter-fluid"
            style={{ margin: "0 auto" }}
          />
        </div>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  #coins {
    padding-top: 5px;
    padding-left: 20px;
  }

  hr {
    width: 100%;
    height: 1px;
    color: #000000;
    background-color: #000000;
  }

  tbody tr {
    text-align: center;
  }

  tbody td {
    display: inline-block;
  }

  .example_b {
    color: rgb(5, 5, 5) !important;
    text-transform: uppercase;
    background: #0eeb57;
    padding: 20px;
    border-radius: 50px;
    display: inline-block;
    border: solid;
    border-color: #040405;
    width: 70%;
    margin: 30px;
  }

  .example_b:hover {
    text-shadow: 0px 0px 6px rgb(10, 10, 10);
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
    transition: all 0.4s ease 0s;
  }

  .collapsible {
    background-color: #2938bc;
    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    border-radius: 8px;
    margin: 1px;
  }

  .active,
  .collapsible:hover {
    background-color: #0b23fe;
  }

  .collapsible:after {
    content: "\002B";
    color: white;
    font-weight: bold;
    float: left;
    margin-left: 5px;
    margin-right: 50px;
  }

  .active:after {
    content: "\2212";
  }

  .content {
    padding: 0 18px;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    background-color: #f1f1f1;
  }

  .numberCircle {
    border-radius: 2%;
    padding: 8px;
    background: #fff;
    border: 2px solid #666;
    color: #666;
    text-align: left;
    font: 22px Arial, sans-serif;
  }

  .panel-info {
    background-color: #b9bbf7;
    width: 80%;
    border-radius: 8px;
    padding: 10px;
    margin: 2px;
    margin-bottom: 2rem;
  }

  blockquote {
    font-family: Georgia, serif;
    font-size: 18px;
    font-style: italic;
    width: 70%;
    margin: 0.25em 0;
    padding: 0.35em 40px;
    line-height: 1.45;
    position: relative;
    color: #383838;
  }

  blockquote:before {
    display: block;
    padding-left: 10px;
    content: "\201C";
    font-size: 100px;
    position: absolute;
    left: -20px;
    top: -20px;
    color: #7a7a7a;
  }

  blockquote:after {
    display: block;
    content: "\201D";
    font-size: 100px;
    position: absolute;
    right: 20px;
    bottom: -20px;
    color: #7a7a7a;
  }

  blockquote cite {
    color: #999999;
    font-size: 18px;
    display: block;
    margin-top: 5px;
  }

  blockquote cite:before {
    content: "\2014 \2009";
  }

  h3 {
    margin-bottom: 2rem;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
