// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"
import folderOptions from "./folder-options"

import AmericanAcademy from "../../images/AmericanLawyerAcademy200.gif"
import BBB from "../../images/bbblg.jpg"
import CAOC from "../../images/caoc.jpg"
import CBC from "../../images/cbcnews.jpg"
import Fox11 from "../../images/fox11news.jpg"
import KABC from "../../images/kabc.jpg"
import Larry from "../../images/larrykabc150.jpg"
import MADD from "../../images/madd_logo.gif"
import MartinHubbell from "../../images/martindalehubbell.jpg"
import MDAF from "../../images/mdaf.gif"
import Newsweek from "../../images/newsweek-article-img.png"
import OCMetro from "../../images/oc-metro.jpg"
import OCTop from "../../images/OCtopattorneys.jpg"
import POPON from "../../images/pop-on.gif"
import PublicJustice from "../../images/PublicJustice.jpg"
import SuperLawyers from "../../images/superlawyers.jpg"
import Top100 from "../../images/top100lawyers.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function RatingsReviewsPage({ location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Awards, Ratings & Reviews"
        pageDescription="Bisnar Chase Personal Injury Attorneys have received numerous accolades and awards. We are a part of some of the best attorney organizations such as AVVO, Super Lawyers, OC Top Attorneys and an A+ rating with the BBB of California."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ratings, Reviews & Awards</h1>
        <BreadCrumbs location={location} location={location} />
        <p style={{ borderBottom: "none", marginBottom: "1rem" }}>
          If you are looking for some of the best-rated personal injury and
          accident lawyers in California, who serve Los Angeles and Orange
          County, then Bisnar Chase Personal Injury Attorneys is the law firm to
          call. Since 1978, we've served over 12,000 clients and recovered
          hundreds of millions of dollars for them. Once you experience our
          "first-class client care" you will understand why we have earned so
          many top ratings, reviews and awards from prestigious organizations.
          You can also read what our{" "}
          <Link to="/about-us/testimonials">clients say about us</Link>.
        </p>
        <p>
          Speak to one of our award-winning attorneys by calling{" "}
          <b>(949) 203-3814</b> or <Link to="/contact">get help here</Link>.
        </p>
        <hr />{" "}
        <Link name="a">
          <LazyLoad>
            <img
              src={SuperLawyers}
              alt="SuperLawyers"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Super Lawyers is a rating service of outstanding lawyers from more
          than 70 practice areas who have attained a high-degree of peer
          recognition and professional achievement. The selection process is
          multi-phased and includes independent research, peer nominations and
          peer evaluations.
        </p>
        <p>
          <strong>Our Firm's 2012 SuperLawyers:</strong>{" "}
          <Link
            to="http://www.superlawyers.com/california-southern/lawyer/Brian-D-Chase/eeb57498-819c-48b6-816d-b41187d50a28.html"
            target="_blank"
          >
            <u>John Bisnar</u>
          </Link>{" "}
          <Link
            to="http://www.superlawyers.com/california-southern/lawyer/Brian-D-Chase/eeb57498-819c-48b6-816d-b41187d50a28.html"
            target="_blank"
          >
            <u>Brian Chase</u>
          </Link>{" "}
          and{" "}
          <Link
            to="http://www.superlawyers.com/california-southern/lawyer/Scott-Ritsema/ed2b8a78-95e6-4ebb-a1de-a5f6cd730069.html"
            target="_blank"
          >
            <u>Scott Ritsema</u>
          </Link>
        </p>
        <hr />{" "}
        <Link
          name="b"
          to="http://www.lawyers.com/California/Newport-Beach/Bisnar-Chase-LLP-168466-f.html"
          target="_blank"
        >
          <LazyLoad>
            <img
              src={MartinHubbell}
              alt="Lawyer Ratings"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          For over a century, people have relied on the LexisNexis
          Martindale-Hubbell Law Directory for authoritative information and
          reviews on the legal profession from a worldwide perspective. The
          Martindale-Hubbell Peer Review Ratings attest to a lawyer's legal
          ability and professional ethics, and reflect the confidential opinions
          of members of the Bar and Judiciary. The legal community respects the
          accuracy of ratings because it knows that its own members -- the
          people best suited to assess their peers -- are directly involved in
          the process. For many years, Bisnar Chase Personal Injury Attorneys
          has been rated "Preeminent" with a rating of 5 out of 5, which is the
          highest possible rating a firm may achieve.
        </p>
        <hr />
        <div
          className="avvo_badge"
          data-type="rating"
          data-specialty="121"
          data-target="http://www.avvo.com/professional_badges/211889"
        >
          <div className="avvo_content" align="imgleft-fixed" />
        </div>
        <p style={{ borderBottom: "none", marginBottom: "1rem" }}>
          Avvo is a national organization that rates attorneys and doctors in
          specific regions throughout the country. Avvo's comprehensive rating
          system carefully reviews a lawyer's background and overall profile,
          which includes years in practice, disciplinary history, professional
          achievements and industry recognition. Avvo performs rankings in 19
          different legal categories and periodically collects background data
          from multiple sources, including state bar associations, court
          records, lawyer websites, and other pertinent information. John Bisnar
          and Brian Chase are each rated 10.0/10.10 "Superb" which is the
          highest rating.
        </p>
        <p>
          <strong>Our Firms Top Rated Avvo Lawyers:</strong>{" "}
          <Link
            to="https://www.avvo.com/attorneys/90045-ca-john-bisnar-211889.html"
            target="_blank"
          >
            <u>John Bisnar</u>
          </Link>{" "}
          and{" "}
          <Link to="https://www.avvo.com/attorneys/92660-ca-brian-chase-339392.html">
            <u>Brian Chase</u>
          </Link>
        </p>
        <hr />{" "}
        <Link
          name="k"
          to="http://www.bbb.org/sdoc/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-llp-in-newport-beach-ca-100046710"
          target="_blank"
        >
          <LazyLoad>
            <img
              src={BBB}
              alt="Better Business Bureau"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p style={{ borderBottom: "none", marginBottom: "1rem" }}>
          Bisnar Chase Personal Injury Attorneys has been accredited with the
          Better Business Bureau since 02/16/2007 and the BBB is satisfied that
          it honors its commitment. Bisnar Chase Personal Injury Attorneys has
          agreed to uphold BBB accreditation standards, which include a
          commitment to act in accordance with ethical business practices and to
          respond to customer complaints. Bisnar Chase Personal Injury Attorneys
          upholds the ideals of the BBB.
        </p>
        <p>
          View our{" "}
          <Link
            name="k"
            to="http://www.bbb.org/sdoc/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-llp-in-newport-beach-ca-100046710"
            target="_blank"
          >
            <u>BBB A+ Rating</u>
          </Link>
        </p>
        <hr />{" "}
        <Link name="d">
          <LazyLoad>
            <img
              src={Top100}
              alt="Top 100 Trial Lawyers"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Brian Chase is a member of the National Trial Lawyers Association
          which is an organization composed of The Top 100 Trial Lawyers from
          each state. Membership is obtained through special invitation and is
          extended only to those attorneys who exemplify superior qualifications
          of leadership, reputation, influence, stature, and profile as civil
          plaintiff or criminal defense trial lawyers.
        </p>
        <hr />{" "}
        <Link name="e">
          <LazyLoad>
            <img
              src={OCTop}
              alt="Top Orange County Attorneys"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          In a special edition of Orange Coast Magazine, John Bisnar and Brian
          Chase were each named top attorneys in Orange County, based on these
          criteria: verdicts, settlements, transactions, representative clients,
          experience, honors and awards, special licenses and certifications,
          position within law firm, bar and/or other professional activity, pro
          bono and community service, scholarly lectures and writings,
          education, employment background, and other outstanding achievements.
          John and Brian are among the most highly respected attorneys in in
          Orange County and throughout Southern California. They have the
          respect of the judges and the defense attorneys, as well as their
          colleagues.
        </p>
        <hr />{" "}
        <Link name="f">
          <LazyLoad>
            <img
              src={OCMetro}
              alt="OC METRO Orange County Attorneys"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          As featured in August's OC Metro magazine, Brian Chase, a partner at
          the Orange County personal injury law firm of Bisnar Chase Personal
          Injury Attorneys, was rated and reviewed as one of the top lawyers in
          Orange County and one of the top five personal injury lawyers in the
          county. This outstanding rating makes Brian Chase one of the top legal
          minds shaping and leading the practice of personal injury law. Brian
          Chase was also named one of OCTLA's (Orange County Trial Lawyer's
          Association) Top Guns and Product Liability Trial Lawyer of the Year
          in 2004. Bisnar Chase Personal Injury Attorneys has a national
          practice that specializes in products liability, auto defects and
          catastrophic injury cases, as well as serving California, with offices
          in Newport Beach and Los Angeles.
        </p>
        <hr />{" "}
        <Link name="g">
          <LazyLoad>
            <img
              src={Newsweek}
              alt="Newsweek Best of Los Angeles Law"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Top Los Angeles personal injury law firm Bisnar Chase Personal Injury
          Attorneys was featured in Newsweek's "The Best of Los Angeles Law,"
          designed to build awareness of Los Angeles area law firms that
          specialize in particular areas of litigation, such as personal
          injuries or death due to an accident, defective product or negligence.
          In the "Best of Los Angeles Law" section, John Bisnar and Brian Chase,
          partners of Bisnar Chase Personal Injury Attorneys, speak candidly
          about their most significant legal case, their greatest legal
          achievement and the innovative things their firm is doing to help
          personal injury victims. Newsweek personally approached Bisnar Chase
          Personal Injury Attorneys to participate in its "Best of Los Angeles
          Law" issue which features a series of questions and answers.
        </p>
        <hr />
        <LazyLoad>
          <img src={Larry} alt="Larry on the Law" className="imgleft-fixed" />{" "}
        </LazyLoad>
        <p>
          As one of Larry Elder's "All-Star Team of the Best Lawyers in Southern
          California," Brian Chase was featured on "Larry on the Law" on 790
          KABC Radio Los Angeles on Saturdays, 3-5 PM. The show, hosted by "The
          Sage from South Central" Larry Elder -- an attorney, former host of
          the nationally syndicated "The Larry Elder Show" and current
          nationally-syndicated host of KABC radio's daily morning talk show in
          Los Angeles -- is designed to answer legal questions and to help
          alleviate the fear and confusion many people face when dealing with
          legal issues. Listeners called in their questions and Brian answered
          them on the air.
        </p>
        <hr />
        <LazyLoad>
          <img
            src={AmericanAcademy}
            alt="american lawyer academy"
            className="imgleft-fixed"
          />
        </LazyLoad>
        <p>
          Bisnar Chase Personal Injury Attorneys is member of The American
          Lawyer Academy (ALA) which is dedicated to promoting excellence in the
          legal profession throughout North America. Membership consists of
          lawyers, solicitors, and barristers who have been selected as members
          of the organization based upon their legal skill, integrity, and
          reputation for professional excellence in their own communities. The
          mission of the ALA is to promote legal excellence by providing members
          of the public with in depth articles on current and important legal
          issues that affect the legal rights of everyday citizens. The site was
          specifically designed to accomplish this mission and to serve as a
          library of important legal information and an open forum for
          discussion of legal topics. Members of the public and the legal
          community are encouraged to explore this website.
        </p>
        <hr />{" "}
        <Link name="l" to="https://www.caoc.com" target="_blank">
          <LazyLoad>
            <img
              src={CAOC}
              alt="consumer attorneys of california"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Bisnar Chase Personal Injury Attorneys is a member of the Consumer
          Attorneys of California (CAOC) CAOC which is an organization of more
          than 3,000 attorneys who represent plaintiffs/consumers who seek
          responsibility from wrongdoers.
        </p>
        <hr />
        <LazyLoad>
          <img
            src={MDAF}
            alt="million dollar avocates"
            className="imgleft-fixed"
          />
        </LazyLoad>
        <p>
          Bisnar Chase Personal Injury Attorneys supports the Million Dollar
          Advocates Forum which is one of the most prestigious groups of trial
          lawyers in the United States. Membership is limited to attorneys who
          have won million and multi-million dollar verdicts and settlements.
          There are over 4000 members throughout the country. Fewer than 1% of
          U.S. lawyers are members.
        </p>
        <hr />{" "}
        <Link name="o" to="http://www.peopleoverprofits.org/" target="_blank">
          <LazyLoad>
            <img
              src={POPON}
              alt="people over profits"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Bisnar Chase Personal Injury Attorneys is a member of People Over
          Profits, sponsored by the American Association for Justice, which is
          committed to fighting back against the well-organized and well-funded
          assault on the civil justice system, and protecting American civil
          rights and trial by jury. When someone you love has been seriously
          injured or killed by a careless medical error or a defective product
          or drug, shouldn't you have the right to hold the wrongdoers
          accountable?
        </p>
        <hr />{" "}
        <Link name="p" to="http://www.publicjustice.net/" target="_blank">
          <LazyLoad>
            <img
              src={PublicJustice}
              alt="public justice"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Bisnar Chase Personal Injury Attorneys supports Public Justice which
          marshals the skills and resources of over 3,000 of the nation's top
          lawyers to fight for justice through precedent-setting and socially
          significant litigation. We are involved in a broader range of
          cutting-edge, high-impact litigation than any law firm in the country
          - battling for consumers' rights, workers' rights, civil rights and
          civil liberties, environmental protection, public health and safety,
          and access to the courts.
        </p>
        <hr />
        {/*{" "}<Link name="q" to="http://www.wetip.com/" target="_blank">
        <LazyLoad>
          <img src="/images/wetip50.gif" alt="we tip" className="imgleft-fixed" />
        </Link> */}
        <p>
          Bisnar Chase Personal Injury Attorneys won WeTip's 2010 outstanding
          Achievement Award. WeTip is committed to providing the most effective
          anonymous citizens crime reporting system in the nation. WeTip
          promises and insures absolute anonymity, not just confidentiality.
          WeTip is unlike anyother hotline. Callers to WeTip can place absolute
          trust in WeTip and the fact that no one can ever know their identity.
        </p>
        <hr />{" "}
        <Link name="r" to="http://www.madd.org/" target="_blank">
          <LazyLoad>
            <img
              src={MADD}
              alt="mothers against drunk driving"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          Bisnar Chase Personal Injury Attorneys won the 2010 #1 Fundraising
          Team Award from Mothers Against Drunk Driving (MADD) which is
          dedicated to stop drunk driving, support the victims of this violent
          crime and prevent underage drinking. To date, MADD's work has saved
          nearly 300,000 lives and counting.
        </p>
        <hr />{" "}
        <Link name="s">
          <LazyLoad>
            <img src={Fox11} alt="fox 11 news" className="imgleft-fixed" />{" "}
          </LazyLoad>
        </Link>
        <p>
          Brian Chase has appeared on FOX 11 TV News discussing whether vehicles
          should have "black boxes," the dangers of faulty seat backs and a
          personal injury case that involved the Los Angeles school district.
        </p>
        <hr />{" "}
        <Link name="t">
          <LazyLoad>
            <img src={CBC} alt="cbc news" className="imgleft-fixed" />{" "}
          </LazyLoad>
        </Link>
        <p>
          Brian Chase was interviewed by Canadian News Broadcast about the
          dangers of 15-passenger vans. Recently, Canada has banned 15-passenger
          van from being used by schools in the country.
        </p>
        <hr />{" "}
        <Link name="u">
          <LazyLoad>
            <img src={KABC} alt="kabc news" className="imgleft-fixed" />{" "}
          </LazyLoad>
        </Link>
        <p>
          Brian Chase is featured on "Larry on the Law" hosted by Larry Elder on
          KABC Talkradio Los Angeles, Saturdays from 3 to 5 pm. Callers call in
          and ask Brian questions about their personal injury legal issues which
          he answers live over the air.
        </p>
        <hr />
        NAPIL : National Association of Personal Injury Lawyers. We are a proud
        member of NAPIL.
        <hr />
        <p>
          {" "}
          <Link
            to="http://www.lawguru.com"
            target="_blank"
            name="Attorney Network"
          >
            LawGuru.com
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  p {
    border-bottom: medium double ${({ theme }) => theme.colors.grey};
    margin-bottom: 4rem;
    padding-bottom: 1rem;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
