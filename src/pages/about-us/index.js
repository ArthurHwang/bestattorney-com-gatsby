// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import { graphql } from "gatsby"
import Img from "gatsby-image"
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import BestBusinessBadge from "../../images/logo/best-personal-injury-lawyers-bestrateddotcom.png"
import NAOPIABadge from "../../images/logo/NAOPIA-top-ten.png"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query aboutUsQuery {
    staffImage: file(relativePath: { regex: "/BC-2019-staff-mini.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 1000) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function AboutUs({ data, location }) {
  return (
    <LayoutDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="About Bisnar Chase Personal Injury Attorneys of California"
        pageDescription="Our mission is to provide superior client representation in a compassionate and professional manner. Bisnar Chase has been winning cases for California plaintiffs for 40 years. No win, no fee guarantee and a 96% success rate for minor to serious injury cases."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>About Bisnar Chase Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Bisnar Chase Personal Injury Attorneys"
            fluid={data.staffImage.childImageSharp.fluid}
          />
        </div>

        <h2>Our Mission Statement:</h2>
        <h3>
          <b>
            <i>
              "To provide superior client representation in a compassionate and
              professional manner while making our world a safer place."{" "}
            </i>
          </b>
        </h3>
        <p>
          This handcrafted mission statement is the Bisnar Chase guide to how we
          represent you. It sets the service standard you can expect from us. It
          explains what we do and why we do it.
        </p>
        <ul>
          <li>
            "<b>provide superior client representation</b>..." means providing
            you the highest quality of representation in our industry. It means
            being relentless on your behalf. It means being efficient in getting
            you the largest recovery your circumstances allow.
          </li>
          <li>
            "<b>in a compassionate and professional manner</b>..." means being
            sensitive to your situation and understanding your needs. It means
            accomplishing your goals in accordance with the highest standards of
            our industry. It means representing you with dignity and class.
          </li>
          <li>
            "<b>while making our world a safer place</b>..." means the work we
            do for you adds to the common good. What happened to you will be
            less likely to happen to someone else because you partnered with us
            to hold the wrongdoer accountable. By holding wrongdoers
            accountable, we encourage people to be responsible for their
            actions, we encourage manufacturers to build safer products, we
            encourage employers to treat their employees fairly and we encourage
            governments to properly carry out their duties. By holding
            wrongdoers accountable there will be less wrong-doing and our world
            will be a safer, gentler place for us all.
          </li>
        </ul>
        <p>
          To learn more about our attorneys and how they embody our mission
          statement, <Link to="/attorneys">visit our attorneys page.</Link>
        </p>

        <h2>What Makes Us Different?</h2>
        <div>
          <p>
            Other than hundreds of millions of dollars recovered for thousands
            of satisfied clients, multiple "Attorney of the Year" awards, four
            "Best Places to Work awards', and a "Community Hero" award, what
            makes us different than the other great personal injury law firms?
          </p>
          <h3>Passionate Legal Representation Is What Makes Us Different</h3>
          <p>
            The passionate representation of people like you is what makes us
            different. From your first phone call on, you will feel the passion
            our team has for taking care of you and for winning your case. You
            will sense that we are your legal dream team.
          </p>
          {/* <LazyLoad> */}
          <img
            className="imgleft-fixed"
            src={NAOPIABadge}
            alt="NAOPIA.org"
            width="170"
            height="137"
            style={{ marginBottom: "5px" }}
          />
          {/* </LazyLoad> */}
          <p>
            -- Passion is winning your case when others said it couldn't be
            done. <br />
            -- Passion is convincing the insurance adjusters or a jury that you
            were right all along. <br />
            -- Passion is collecting every cent your case, the law, and
            circumstance allow.
          </p>
          <p>
            What you cannot tell until you call us or meet with us is how well
            we take care of our clients. You will be surprised at the level of
            personal service and professionalism you experience. You will be
            surprised how much your mind can be put at ease when you are
            comfortable and trust your legal team.
          </p>
          <p>
            Give us a call. Check us out for yourself. You will immediately
            experience the difference that passionate representation makes.
          </p>
          <h2>Who We Represent -- We are Plaintiff's Attorneys</h2>
          <p>
            We represent people who have been seriously injured or lost a family
            member due to an accident or a defective product. We also represent
            people who have been denied employment rights.
          </p>
        </div>
        <h2>How We Represent</h2>

        <p>
          Our law firm consists of nine attorneys divided into four litigation
          teams. Each litigation team is led by an experienced personal injury
          trial lawyer and includes a combination of paralegals and legal
          assistants. Brian Chase is our chief litigator and winner of multiple
          Attorney of the Year awards, and manages the litigation teams. He also
          is the lead litigator in our largest and most challenging cases. We
          have 32 total members on our team.
        </p>
        <h2>The Bisnar Chase Story</h2>
        {/* <LazyLoad> */}
        <img
          src={BestBusinessBadge}
          alt="Rated Best law firm in Newport Beach by threeBestRated.com"
          width="170"
          height="151.63"
          className="imgleft-fixed"
        />
        {/* </LazyLoad> */}

        <p>
          Where does our "passion" come from? John Bisnar started this law firm
          right out of law school in 1978. John went to law school believing a
          legal education would serve him better in a business career than
          earning another business degree. In his first month of law school he
          was seriously injured in a traffic collision by a negligent driver.
          The attorney who represented Mr. Bisnar, one of his law school
          professors, did a poor job representing him. From that experience John
          learned what it was like to need and depend upon an attorney for
          quality professional assistance and not get it. The professor was a
          famous personal injury attorney, yet John knew he could do a better
          job for clients.
        </p>

        <p>
          Due to that experience, Mr. Bisnar vowed that his clients would get
          the quality of professional service that he wished he had received.
          With that vow Mr. Bisnar started Bisnar &amp; Associates, a firm that
          he successfully operated for twenty years. In 1998, that firm evolved
          into Bisnar Chase with Brian Chase leading the litigation team and Mr.
          Bisnar focusing on our clients' needs, ensuring we live up to the vow.
          Today we serve a couple hundred clients at a time. And we work to live
          up to the vow each and every day.
        </p>

        <p>
          Bisnar Chase has been named one of the{" "}
          <Link to="/press-releases/best-places-to-work-oc" target="_blank">
            Best Places To Work
          </Link>{" "}
          by the Orange County Business Journal every year since 2012. What that
          means is the Bisnar Chase team has a great working environment, and is
          highly engaged and motivated. It is Mr. Bisnar's belief that a well
          taken care of, highly engaged and motivated team of employees working
          in a cooperative team environment will result in superior
          representation and great results for the clients.
        </p>

        <p>
          Named as one of only three law firms in Newport Beach as the{" "}
          <Link
            to="https://threebestrated.com/personal-injury-lawyers-in-newport-beach-ca"
            target="_blank"
          >
            best personal injury lawyers
          </Link>{" "}
          as seen in ThreeBestRated.com. With a strict vetting process, the
          directory only lists the top three performers in a given city based on
          their reviews, reputation, attention to detail and overall client
          satisfaction. Bisnar Chase has consistently delivered exceptional
          results for over four decades.
        </p>

        <h2>Try Us Out and See For Yourself! Call 800-561-4887</h2>
        <p className="address-bottom">
          1301 Dove St. #120
          <br />
          Newport Beach, CA. 92660 <br />
          Tel: 949-203-3814
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
