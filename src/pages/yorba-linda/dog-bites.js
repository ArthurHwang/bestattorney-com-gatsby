// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Dog Bite Lawyer"
        pageDescription="If you have been bitten by a dog and have suffered serious injuries contact the Yorba Linda Dog Bite Lawyers of Bisnar Chase. You shouldn't have to pay out of pocket for medical care due to a dog owners negligence. The law offices of Bisnar Chase will fight for the financial compensation you deserve after an attack. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          It is possible to suffer serious and life-threatening injuries in a
          dog bite incident. Depending on the size and strength of the dog, the
          physical and emotional injuries suffered can negatively affect the
          victim's quality of life. Compensation for these types of injuries is
          available to victims under California law. An experienced Yorba Linda{" "}
          <Link to="/orange-county/dog-bites">dog bite lawyer</Link> can help
          victims get the compensation and support they need in order to recover
          from their serious injuries.
        </p>

        <p>
          <em>
            <strong> Call for immediate help at 949-203-3814.</strong>
          </em>
        </p>

        <h2>
          <strong> Physical Injuries</strong>
        </h2>

        <p>
          Certain dogs, such as pit bulls and rottweilers, have extremely
          powerful jaws and sharp teeth. It could take as little as one bite to
          do significant damage. Common physical injuries suffered in a dog bite
          incident include:
        </p>
        <ul type="disc">
          <li>
            <strong> Puncture wounds:</strong> A dog's sharp teeth can cause
            serious puncture wounds. This type of injury may only require
            cleaning, but in many cases, it requires more medical procedures
            such as stitches.
          </li>
          <li>
            <strong> Lacerations:</strong> When a dog jerks its mouth after
            biting down, it is possible for the skin to tear. These types of
            injuries can be quite extensive and require skin grafting. Deep and
            substantial laceration injuries may never again look the same and
            the damaged nerves may never fully heal. When this type of injury
            occurs on the wrist or the neck there is the potential for a major
            artery to get cut resulting in a loss of blood that can be life
            threatening.
          </li>
          <li>
            <strong> Broken bones:</strong> Powerful and large dogs have the
            potential to actually fracture bones when they bite down. Victims
            may require complicated surgical procedures and physical therapy to
            recover strength and physical ability in the traumatized area.
          </li>
          <li>
            <strong> Sprains and strains:</strong> Muscles and joints can be
            seriously hurt during the confrontation. In self-defense, victims
            may fall down, they may be dragged and they may have to kick and
            punch.
          </li>
          <li>
            <strong> Scars:</strong> Anytime there is significant damage to the
            skin, there is the potential for permanent scarring to occur. Many
            dog bite injuries occur on visible parts of the body such as the
            head, face, neck and hands. Some scars may be repaired by cosmetic
            surgery. However, some scars may be irreparable and last a lifetime.
          </li>
          <li>
            <strong> Nerve damage:</strong> Many dog bite injuries include
            damage to the nerves directly underneath the skin. Victims may lose
            feeling where the nerves are damaged.
          </li>
          <li>
            <strong> Rabies:</strong> Sadly, many dog owners fail to have their
            dog vaccinated for rabies. This deadly infection can prove fatal if
            not properly treated immediately following the accident.
          </li>
        </ul>
        <h2>
          <strong> Emotional Injuries</strong>
        </h2>
        <p>
          After a tragic, sudden and horrific event, a person can suffer
          substantial emotional injuries. The victim may feel depressed, fearful
          or nervous. Post traumatic stress disorder is common for victims of
          traumatic events and it needs to be properly diagnosed and treated
          following a dog bite incident. It is common for victims to be nervous
          around animals for years after the event and for some this fear is so
          severe that it affects their day-to-day life.
        </p>
        <p>
          A morbid fear of dogs can affect the victim's ability to leave the
          house. In the most severe cases, the victim could become a shut-in for
          a prolonged period of time. Many dog bite victims require extensive
          psychological counseling to recover from such severe trauma.
        </p>
        <h2>
          <strong> Disfigurement</strong>
        </h2>
        <p>
          Many victims of serious Yorba Linda dog attacks are left with
          devastating scars that will never go away. This can affect their
          social interactions, the professional livelihood and their emotional
          well-being. It is a fact that our looks and appearance affect our
          relationships with those around us. Some victims of disfigurement
          injuries may have to endure staring, pointing, social discrimination,
          painful questions and embarrassment and even humiliation.
        </p>

        <h2>
          <strong> Protect Your Rights</strong>
        </h2>

        <p>
          Under California law, you may pursue financial compensation for your
          injuries from the dog's owner. An experienced{" "}
          <Link to="/dog-bites">Yorba Linda dog bite lawyer</Link> at Bisnar
          Chase can help you protect your rights and get the support you need.
          Most dog bite injuries are covered by the dog owner's homeowner's
          insurance policy. Please call us at <strong> 949-203-3814</strong> for
          a free consultation and to find out how we can help you.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
