// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Pedestrian Accident Lawyer - Experienced Attorneys"
        pageDescription="If you have been injured in a pedestrian accident and are in need of legal representation, the Yorba Linda Pedestrian Accident Lawyers of Bisnar Chase are here to provide you relief. Accident victims who have suffered from a pedestrian injury should be rightfully compensated for damages. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been the victim of a personal injury accident such as a
          pedestrian accident, call <strong> 949-203-3814</strong> to talk to a
          Yorba Linda pedestrian accident lawyer today or fill out our
          convenient online form.
        </p>
        <p>
          When you hear about devastating pedestrian accidents, you may wonder
          how these accidents might be prevented or if it is possible to reduce
          the number of people hurt and killed in pedestrian accidents each
          year. Several studies have been done that suggest there are effective
          ways to reduce pedestrian accidents, but communities have to be
          willing to work together to implement these ideas and ensure that
          everyone follows the rules so that the measures work.
        </p>
        <p>
          Studies have found the communities that encourage walking and cycling
          among their members have fewer pedestrian accidents than communities
          that do not provide areas for pedestrians and cyclists. This may seem
          like a contradiction, but communities that encourage walking and
          cycling tend to be willing to provide bike paths, better crosswalks,
          and other areas that help pedestrians and bicycle riders stay safe.
        </p>
        <p>
          <em>
            <strong>
              {" "}
              If you need to speak with a Yorba Linda pedestrian accident lawyer
              right away, please call 949-203-3814.
            </strong>
          </em>
        </p>
        <h2>Traffic Calming Intervention</h2>
        <p>
          Another type of intervention that has been shown to prevent pedestrian
          deaths is called a "traffic calming intervention". This is something
          designed to slow down drivers, especially in areas where there are
          commonly pedestrians or cyclists such as parking lots and residential
          streets. Speed bumps are an example of a traffic calming
          intervention. Speed bumps force drivers to slow down. Since high speed
          is one of the major causes of pedestrian accidents it naturally
          follows that pedestrian accidents are reduced.
        </p>
        <p>
          The 2005 SAFETEA-LU or the{" "}
          <em>
            Safe, Accountable, Flexible, Efficient Transportation Equity Act
          </em>
          : <em>A Legacy for Users </em>Act was passed by the federal
          government. It gives funding to communities to provide safer
          sidewalks, bike paths, traffic calming interventions, and other
          pedestrian safety measures.
        </p>
        <p>
          {" "}
          <Link to="/pedestrian-accidents">
            Yorba Linda pedestrian accident lawyers
          </Link>{" "}
          represent all types of pedestrian accident victims in the event that
          the victim is injured. It does not matter if the injuries are serious
          or minor, every victim deserves good legal representation, and it is
          the job of a pedestrian accident lawyer to ensure that the victim's
          rights are protected no matter what the case is worth. The amount of
          money an attorney stands to make from a case should never determine
          the quality of legal advice you get as the client.
        </p>
        <p>
          Pedestrian accident attorneys in Yorba Linda take this responsibility
          very seriously. Pedestrian accident attorneys represent a wide variety
          of clients and do their best to get the best possible settlement for
          every victim. Pedestrian accident attorneys always provide a free
          consultation to the victim so that there is no obligation if the
          victim chooses not to pursue a pedestrian accident case.
        </p>
        <p>
          Further, a pedestrian accident lawyer often works on a contingency fee
          basis. This means that the attorney does not collect any fees until
          the case is successfully settled, so the victim does not have to come
          up with any cash to pay for legal representation. No matter what your
          financial circumstances, you can still get good legal representation
          with a Yorba Linda pedestrian accident lawyer.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
