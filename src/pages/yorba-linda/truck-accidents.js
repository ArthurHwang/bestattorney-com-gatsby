// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Truck Accident Lawyer - Experienced Truck Accident Attorneys"
        pageDescription="Truck accidents lead victims to suffer severe injuries. The Yorba Linda Truck Accident Lawyers of Bisnar Chase want to provide relief for people injured in a truck crash.  Bisnar Chase has been helping clients win millions of dollars for 40 years. Call 949-203-3814 to learn more about our legal services."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Truck Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been injured in a Yorba Linda truck accident, you may have
          a challenging road ahead of you to pursue fair compensation for your
          losses. Truck companies typically fight any and all claims and victims
          are often left wondering how they will pay for their medical bills,
          time away from work and other significant losses. For this reason,
          retaining the help of an experienced Yorba Linda truck accident lawyer
          is essential to build a solid foundation for recovery. There are steps
          that you can take to protect your rights and one of those important
          steps is preserving the evidence at the crash site.
        </p>
        <p>
          <em>
            <strong>
              {" "}
              If you need immediate legal representation, please call
              949-203-3814.
            </strong>
          </em>
        </p>
        <h2>
          <strong> Spoliation Letter</strong>
        </h2>
        <p>
          Ideally, when you are seriously injured in a truck accident in Yorba
          Linda, the at-fault party and insurance companies will rush to your
          aid and ensure that you are fairly compensation for your losses.
          Unfortunately, this happens on very rare occasions, if at all it
          happens. In most cases, injured victims are left with staggering
          medical bills and other financial losses. The emotional and financial
          burdens faced by truck accident victims are tremendous.
        </p>
        <p>
          In such cases where the victim is injured, it is important that a
          Yorba Linda injury lawyer is retained right away so he or she can get
          to work immediately to preserve key evidence in the case. In legal
          terms, "spoliaton" is the intentional alteration or destruction of a
          document. This happens very often in truck accidents. Important
          records such as driver logs or truck maintenance records can be
          intentionally or accidentally destroyed. If there is a surveillance
          camera that captured the accident, it is important that you obtain
          that piece of evidence, which could very well help bolster your case.
        </p>
        <h2>
          <strong> Evidence at Truck Accident Crash Sites </strong>
        </h2>
        <p>
          Following any truck accident, investigators will have to determine the
          circumstances of the collision and who was at fault. Victims who have
          the opportunity to collect valuable evidence from the crash site will
          have an easier time pursuing compensation for their losses. Here are
          some steps you can take:
        </p>
        <ul type="disc">
          <li>Obtain and secure a copy of the police report</li>
          <li>
            Take extensive photos of all vehicles involved, the position of the
            vehicles, the crash site, tire marks and even of your own injuries
          </li>
          <li>
            Request an immediate inspection of the truck and the trailer by a
            qualified professional
          </li>
          <li>
            Obtain evidence from the truck's data recorder to determine how fast
            the truck was going before the collision
          </li>
          <li>
            Demand from the trucking company a preservation of the trip
            inspection report and driver's log
          </li>
          <li>Collect maintenance records of the truck</li>
          <li>Seek immediate medical attention</li>
          <li>
            Collect contact information from not only the other drivers involved
            but also from anyone who may have witnessed the crash
          </li>
          <li>
            Determine who was driving the truck, who owns the truck, who was
            responsible for loading and maintaining the truck. This could help
            determine the potentially liable parties and there could be several
            in truck accident cases.
          </li>
        </ul>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
