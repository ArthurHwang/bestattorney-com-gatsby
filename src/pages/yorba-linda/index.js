// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/accident-lawyers-santa-ana-california-header-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Yorba Linda",
      population: 67032,
      totalAccidents: 814,
      intersection1: "Yorba Linda Blvd & Imperial Hwy",
      intersection1Accidents: 65,
      intersection1Injuries: 30,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 71.3,
      city1Name: "Placentia",
      city1Index: 126.3,
      city2Name: "Villa Park",
      city2Index: 100.9,
      city3Name: "Orange",
      city3Index: 121.9,
      city4Name: "Brea",
      city4Index: 185.6
    },
    locationBox3: {
      intersection2: "Yorba Linda Blvd & Village Center Dr",
      intersection2Accidents: 56,
      intersection2Injuries: 32,
      intersection2Deaths: 0,
      intersection3: "Bastanchury Rd & Valley View Ave",
      intersection3Accidents: 33,
      intersection3Injuries: 31,
      intersection3Deaths: 0,
      intersection4: "Bastanchury Rd & Imperial Hwy",
      intersection4Accidents: 50,
      intersection4Injuries: 26,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Personal Injury Lawyer - Bisnar Chase"
        pageDescription="Call 949-203-3814 to speak to one of Bisnar Chase's experienced Yorba Linda Personal Injury Lawyers. The law office of Bisnar Chase has been providing legal services to victims of car accidents, serious dog bites & auto defects for over 40 years. Contact us and receive a free consultation.  "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Yorba Linda Personal Injury Lawyers"
            title="Yorba Linda Personal injury lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Proving liability for a Yorba Linda injury accident is not always
          easy, but it is a necessary step in pursuing financial compensation
          through civil litigation. If you have been injured in a car accident,
          as the result of a defective product, slip-and-fall accident, dog
          attack, nursing home abuse or any other type of incident caused by
          someone else's negligence, you have it in your best interest to
          contact an experienced skilled Yorba Linda personal injury lawyer to
          discuss your potential case.
        </p>
        <p>
          Recovering from a debilitating injury is a heavy burden. Many of our
          clients have trouble adjusting to their new lifestyle and speak with
          us regularly to help them on a personal and professional level. Our
          commitment to our clients is unrivaled and we will make sure that you
          get the best legal representation available. Contact a Yorba Linda
          personal injury lawyer today to move your injury case in the right
          direction.
        </p>
        <p>
          Call <strong>949-446-1783</strong> for a Free Confidential
          Consultation or fill out the online form.
        </p>

        <h2>Understanding Liability</h2>
        <p>
          Most accidents occur because of someones negligence or carelessness.
          Car drivers who exceed the speed limit, for example, are being
          reckless or careless. In general, if one persons carelessness results
          in an accident, that person may be held liable for the injuries
          suffered by the victim. If the victim was careless as well, the degree
          of each persons fault will have to be determined. A Yorba Linda
          personal injury attorney can review your case and help you decide what
          steps to take going forward.
        </p>
        <h2>When the Victim is at Fault</h2>
        <p>
          There are a number of circumstances in which the victim of an injury
          accident will not be eligible for financial compensation. If someone
          is violating the law by trespassing, for example, he or she will not
          be able to pursue financial compensation for any injuries suffered in
          a falling accident or dog bite incident. Drunk drivers will also have
          a difficult time pursuing compensation for any injuries they may have
          suffered in an accident. Victims of slip-and-fall accidents will have
          to prove that they were not only legally on the premises but also that
          they fell because of a dangerous condition and not because of their
          own carelessness.
        </p>
        <h2>At-Fault Drivers</h2>
        <p>
          Yorba Linda car collisions often result in devastating injuries.
          Victims of serious injuries may pursue compensation for their medical
          bills, lost wages and other related damages. The victim will have to
          prove that the other drivers negligence was the cause of the accident.
          Was the other driver speeding or driving under the influence? Was the
          at-fault motorist talking on a cell phone or weaving through traffic?
          Did the other car driver run through a red light or stop sign? It is
          important to remember that a driver does not have to be cited by the
          authorities for a traffic violation in order to be held civilly liable
          for their actions. If their negligence was a contributing factor in
          the collision, then, he or she may be held at least partially
          responsible for the injuries suffered.
        </p>
        <h2>Property Owners Liability</h2>
        <p>
          Did the accident result from a dangerous condition? Did the victim
          fall on a slippery floor that was not properly marked by a wet floor
          sign? Property owners may be held liable for the injuries that occur
          on their premises if the victim can prove that a hazardous condition
          caused the accident and that the owner knew of the condition and
          failed to fix it.
        </p>
        <h2>Product Manufacturers</h2>
        <p>
          When a defective product causes an injury, the manufacturer of the
          product may be held responsible for the injuries suffered by the
          victim. There are several questions to ask in such cases. Was the
          product defective? Did the victim suffer an injury because of that
          defect? Was the victim using the product correctly at the time of the
          accident? Commonly defective and dangerous products include
          medications, auto parts, children's toys and food products. Anyone
          injured in these types of accidents would be well advised to discuss
          their case with an experienced product liability lawyer.
        </p>
        <h2>Filing a Personal Injury Claim</h2>
        <p>
          If you or a loved one has suffered serious personal injuries as a
          result of someone else's negligence or wrongdoing, the experienced
          Yorba Linda personal injury attorneys at Bisnar Chase can help you
          better understand your legal rights and options. Please call us at{" "}
          <strong> 949-203-3814</strong> for a free, comprehensive and
          confidential consultation.
        </p>
        <p>
          Yorba Linda residents rarely have cause for complaints, but when
          locals and tourists suffer personal injuries, speaking with
          an experienced Yorba Linda personal injury lawyer should be a
          priority. Accident victims are rarely treated with the respect and
          attention that they deserve.
        </p>
        <p>
          Businesses do not want to recognize your injuries, or pay for them,
          leaving personal injury victims with little choice other than pursuing
          personal injury claims.
        </p>
        <p>
          In some circumstances, victims are offered up-front monetary rewards
          from the entities that are responsible for their injuries, but later
          find that the amount they received will not be enough to cover their
          long-term medical expenses, let alone their pain and suffering.
        </p>
        <p>
          Speaking with one of our highly distinguished Yorba Linda personal
          injury attorneys will give you the information you need to make the
          best decision for you and your family. The state of California
          restricts the amount of time that accident victims are allowed to
          pursue a claim for damages, in some cases giving victims 6 months from
          the date of their accident to file.
        </p>
        <h2>Types of Yorba Linda Accident Injuries</h2>
        <p>
          If you have suffered from any of the following types of personal
          injuries, contact us immediately for a free case evaluation. Once you
          become a client, we begin working on your case immediately and examine
          all of the evidence. The sooner we become familiar with your case, the
          sooner we will be able to obtain maximum compensation for your
          injuries.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/yorba-linda/car-accidents">Auto Accidents</Link>
          </li>
          <li>
            {" "}
            <Link to="/yorba-linda/slip-and-fall-accidents">Trip-and-Fall</Link>
          </li>
          <li>
            {" "}
            <Link to="/yorba-linda/pedestrian-accidents">
              Pedestrian Accidents
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/yorba-linda/bicycle-accidents">Bicycle Accidents</Link>
          </li>
          <li>
            {" "}
            <Link to="/yorba-linda/truck-accidents">Truck Accidents</Link>
          </li>
          <li>
            {" "}
            <Link to="/yorba-linda/nursing-home-abuse">Nursing Home Abuse</Link>
          </li>
        </ul>
        <p>
          A successful Yorba Linda personal injury claim can award you with
          reimbursement for medical expenses, lost wages, lost future wages,
          lost benefits, and emotional distress. Choosing the right accident
          attorney can easily be the difference between a secure future, and a
          lifetime of regret. Call us today to experience the peace of mind that
          comes with one of the nation's top personal injury law firms.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
