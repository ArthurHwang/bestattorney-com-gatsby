// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Slip and Fall Attorneys of Bisnar Chase"
        pageDescription="The Yorba Linda Slip and Fall Attorneys of Bisnar Chase have over 40 years of experience dealing with premise liability cases. Our lawyers believe that you shouldn't have to take on costly medical bills all by yourself. If you are seeking legal representation for an injury in the Orange County area call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Slip and Fall Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          If you or a loved one has been injured in a Yorba Linda slip-and-fall
          accident, please contact us for a free, comprehensive and confidential
          consultation.
          <strong> Call 949-203-3814 today!</strong>
        </p>
        <p>
          Individuals over the age of 65 are at higher risk of suffering an
          injury in a slip-and-fall accident than those who are younger. The U.
          S. Centers for Disease Control and Prevention (CDC) reports that 82
          percent of fall deaths suffered in the year 2008 involved elderly
          people. In fact, one in every three elderly adults are involved in a
          falling accident each year. These potentially life-threatening
          incidents can occur at their home, at a place of business or in a
          nursing home. Those who are injured as a result of a slip-and-fall
          accident caused by someone else's negligence would be well advised to
          contact an experienced{" "}
          <Link to="/premises-liability/slip-and-fall-accidents">
            Yorba Linda slip-and-fall lawyer
          </Link>{" "}
          to obtain more information about their legal rights and options.
        </p>
        <h2>
          <strong> Fall Statistics Involving Seniors</strong>
        </h2>
        <p>
          According to the CDC, falls are the leading cause of injury death
          among adults 65 years of age or older. Falling incidents are also the
          most common cause of non-fatal injuries and hospital admissions for
          seniors. The CDC reports:
        </p>
        <ul type="disc">
          <li>
            2.2 million seniors visited the emergency room in the year 2009 for
            non-fatal fall injuries.
          </li>
          <li>Of those victims, 581,000 required hospitalization.</li>
          <li>
            19,700 seniors were killed in unintentional falling injuries in the
            year 2008.
          </li>
          <li>
            Between 20 to 30 percent of injured seniors suffer moderate to
            severe injuries involving lacerations, head trauma or hip fractures.
            These injuries often make it difficult for the victim to continue
            living independently.
          </li>
          <li>
            Most fractures suffered by seniors occur as a result of falls. The
            most common are fractures to the spine, hip, forearm, leg, ankle,
            pelvis, upper arm, and hand.
          </li>
        </ul>
        <p>
          Falls involving the elderly can occur anywhere. In fact, many seniors
          living independently are injured in their own home. These types of
          incidents are often the reason why seniors begin to consider leaving
          their home for full time care.
        </p>
        <h2>
          <strong> Fall-Related Injuries in Nursing Home </strong>
        </h2>
        <p>
          Simply because a senior is receiving full time care, it does not mean
          that they are no longer in danger of suffering an injury in a falling
          accident. In fact, a recent study published in the&nbsp;Journal of the
          American Geriatrics Society found that 21 percent of newly admitted
          nursing home residents sustained at least one fall during their first
          30 days in the facility.
        </p>
        <p>
          The CDC reports that a typical nursing home with 100 beds reports 100
          to 200 falls each year. This is a staggering statistic considering how
          many falls and incidents go unreported. According to a separate study
          by the CDC, between half and three-quarters of nursing home residents
          fall each year. Patients who have fallen are more likely to fall
          multiple times. In fact, the average for nursing home patients is
          approximately 2.6 falls per year. Tragically, approximately 1,800
          seniors die from falls in nursing homes annually.
        </p>
        <h2>
          <strong> Rights of Slip-and-Fall Victims</strong>
        </h2>
        <p>
          Injured victims of slip-and-fall accidents can seek compensation for
          damages including medical expenses, lost wages, cost of
          hospitalization, rehabilitation and pain and suffering. Victims who
          suffer slip-and-fall injuries can file a personal injury claim against
          the property owner or manager. Seniors who suffer falls in nursing
          homes that are responsible for their can file a claim against the
          nursing home seeking damages.
        </p>
        <p>
          The experienced Yorba Linda slip-and-fall lawyers at our law firm have
          a long and successful track record of protecting the rights of injured
          victims and their families. It is often challenge to prove fault and
          liability in slip-and-fall cases.
        </p>
        <p>
          In personal injury claims, including slip-and-fall cases, the burden
          of proof is on the plaintiff, which means that the claimant must prove
          negligence. We have the knowledge, skill and tenacity it takes to hold
          negligent parties accountable.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
