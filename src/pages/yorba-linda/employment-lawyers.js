// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Employment Lawyers - Discrimination"
        pageDescription="Have you been discriminated in the workplace or have experienced a wrongful termination? If so, the Yorba Linda Employment Lawyers of Bisnar Chase are here to help. The law offices of Bisnar Chase has been representing employees that faced an injustice in the work force. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Employment Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          Have you been discriminated against in the workplace? Has your
          employer not paid you fair compensation for the hours you have worked?
          Have you been sexually harassed by a co-worker, supervisor or client?
          Have you been wronfully terminated?
        </p>
        <p>
          It may be possible to hold your employer accountable for their actions
          and compensation may be available for the losses you have suffered.
          Anyone dealing with these types of issues would be well advised to
          discuss his or her legal rights and options with an experienced Yorba
          Linda <Link to="/employment-law"> employment lawyer</Link>.
        </p>

        <p>
          <em>
            <strong> Call 949-203-3814 for immediate legal assistance.</strong>
          </em>
        </p>

        <p>
          <strong> Discrimination in the Workplace</strong>
        </p>
        <p>
          Discrimination comes in many forms and none of them are acceptable in
          the workplace. Types of discrimination that are prohibited under
          federal or state labor laws include:
        </p>
        <ul type="disc">
          <li>
            <strong> Ageism or age discrimination:</strong> This is when
            supervisors or co-workers stereotype people based on their age
            alone. In such cases, individuals may not get the promotion they
            deserve or the new job they are applying for simply because they are
            a certain age. Most ageism cases involve older individuals, but
            ageism protections exist for young workers as well. Decisions at the
            workplace cannot be based solely on the age of the worker.
          </li>
          <li>
            <strong> Gender discrimination:</strong> This involves the unequal
            treatment of people because of their gender. Gender discrimination
            in the workplace often involves unequal pay, discriminatory job
            standards and withholding of promotions because of the sex of the
            worker. There are many acts that prohibit unequal and unfair
            treatment of workers because of their gender including the{" "}
            <Link to="http://www.justice.gov/crt/about/cor/coord/titlevi.php">
              Civil Rights Act of 1964
            </Link>
            , the Equal Pay Act and the Pregnancy Discrimination Act.
          </li>
          <li>
            <strong> Racial discrimination:</strong> It is wrong and illegal, to
            treat people differently because of their race. Title VII of the
            Civil Rights Act prohibits businesses from making decisions based on
            race when determining whom to hire, whom to promote, what salaries
            to offer, who should receive job training and who should be let go.
            The law also prohibits employers from stereotyping a person because
            of the race of his or her spouse, friends or acquaintances.
          </li>
          <li>
            <strong> Disability discrimination:</strong> Making business
            decisions based solely on whether a person is disabled is prohibited
            under California law. If the applicant, for example, is capable of
            performing the required job tasks despite their disability, the
            employer must make reasonable accommodations for the applicant. In
            California, all employers with five or more employees are required
            to make reasonable accommodations for someone with a known
            disability as long as those accommodations do not result in undue
            hardship.
          </li>
        </ul>
        <p>
          <strong> Hours and Wage Disputes</strong>
        </p>
        <p>
          It is common for companies to violate the Fair Labor Standards Act in
          an attempt to save money. California labor law reinforces the many
          protections provided to workers by the Fair Labor Standards Act. There
          are exceptions to the rule, but in general, workers who perform work
          related tasks for over eight hours a day or 40 hours a week must be
          paid overtime for the extra hours worked.
        </p>
        <p>
          <strong> Sexual Harassment</strong>
        </p>
        <p>
          Sadly, it is common for workers to experience unwanted and unwelcomed
          sexual attention at the workplace. Victims of sexual advances, lewd
          comments and unlawful quid pro quo threats can fight back by filing a
          sexual harassment suit against their employers. Workplaces that allow
          sexual harassment and foster a hostile environment should be held
          accountable for their wrongdoing. There are thousands of sexual
          harassment cases filed each year. It is common for sexual harassment
          complaints to go unreported because of the fear of retaliation.
        </p>
        <h2>Yorba Linda Employment Lawyers</h2>
        <p>
          If you believe you were wrongfully terminated, harassed or did not
          receive the payment you were due, the experienced Yorba Linda
          employment lawyers at Bisnar Chase can help you better understand your
          legal rights and options. The best employment lawyers work on a
          contingency basis, which means that they will not get paid until you
          win your case. Please call us at
          <strong> 949-203-3814</strong> or fill out the online form to schedule
          a free, comprehensive and confidential consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
