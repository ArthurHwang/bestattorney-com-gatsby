// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Brain Injury Lawyer - Head Injury Attorney"
        pageDescription="If you or a loved one has suffered from a traumatic brain injury contact the Yorba Linda Brain Injury Lawyers of Bisnar Chase. The attorneys of Bisnar Chase believe that liable parties should provide compensation for the damages you have suffered from. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Brain Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          According to The U.S. Centers for Disease Control and Prevention
          (CDC), an average of 1.7 million people suffer a traumatic brain
          injury each year. A traumatic brain injury is a blow or bump to the
          head that affects the normal function of the brain. Not all head
          injuries result in brain injuries, but many seemingly minor incidents
          can result in brain injuries. There are many causes of brain injuries
          and a number of potentially liable parties. A skilled Yorba Linda
          brain injury lawyer can help you understand how the accident occurred
          and who can be held accountable for your injuries and losses.
        </p>

        <h2>
          <strong> Common Causes of Brain Injuries</strong>
        </h2>
        <p>
          The CDC compiled statistics from across the country to determine the
          leading causes of traumatic brain injuries (TBI). According to their
          research, the most common causes of brain injuries include:
        </p>
        <ul type="disc">
          <li>
            <strong> Falls:</strong> Falling accidents are the cause of 35.2
            percent of all traumatic brain injuries in the United States. Falls
            are the cause of 50 percent of TBIs for children under the age of 15
            and 61 percent of all TBIs from adults aged 65 years or older. When
            a falling accident results in a TBI, it must be determined how the
            fall occurred. Was the injury the result of a slip-and-fall
            accident? In such cases, it must be determined if the property owner
            failed to post signage warning pedestrians or visitors regarding the
            hazardous conditions.
          </li>
          <li>
            <strong> Car Accidents:</strong> Motor vehicle accidents account for
            17.3 percent of all TBIs making it the second leading cause of brain
            injuries. Additionally, car accidents are the leading cause of fatal
            TBIs. Whenever a brain injury results from a Yorba Linda car
            accident, it must be determined if the at-fault driver was negligent
            at the time of the crash. Drivers who are speeding, distracted,
            fatigued, drunk or otherwise careless can be held accountable for
            the catastrophic injuries they cause.
          </li>
          <li>
            <strong> Struck-By Accidents:</strong> The third most common cause
            of brain injuries involves victims being struck by or against a
            moving or stationary object. This type of incident is the second
            leading cause of TBI among children aged 0 to 14 years.
          </li>
          <li>
            <strong> Physical Assaults:</strong> Assaults are to blame for 10
            percent of TBIs. When someone is attacked, the assailant will likely
            face criminal charges. It is important to understand, however, that
            a criminal conviction will not result in financial compensation for
            the victim's monetary losses. The victim or the victim's family can
            file a personal injury claim against the attacker to receive
            financial support for their losses.
          </li>
        </ul>
        <h2>
          <strong> Who is Most at Risk for a TBI?</strong>
        </h2>
        <p>
          Young children and elderly adults are the most at risk of suffering a
          brain injury in an accident. Approximately 18 percent of all
          TBI-related visits to the hospital involve children aged 0 to 4 years.
          TBI-related hospitalizations involve seniors aged 75 and older 22
          percent of the time. Men are the victims of brain injury accidents 59
          percent of the time.
        </p>

        <h2>Determining a TBI</h2>
        <p>
          It is very important that you do not ingnore a{" "}
          <Link to="/head-injury/treatments">serious head injury</Link>.
          Sometimes symptoms of brain injury do not show up right away. A bump
          on the head could be just a bump on the head or it could be much more
          serious. Talk to your doctor about any symptoms you have such as pain,
          loss of memory and or cognitive skills, nausea, depression to name a
          few.
        </p>

        <h2>
          <strong> Rights of Brain Injury Victims</strong>
        </h2>
        <p>
          Depending on the extent of the injuries, a brain injury can result in
          a multitude of losses. Victims may incur emergency room and
          hospitalization expenses, lost wages and even a loss of earning
          potential. Some victims may never again return to work, and victims of
          severe brain injuries may require round-the-clock assistance to
          perform basic functions. The financial, emotional and physical losses
          suffered in brain injury accidents are difficult to calculate.
        </p>

        <p>
          An experienced and skilled Yorba Linda brain injury lawyer at Bisnar |
          Chse can help you pursue fair compensation for your losses. If the
          incident resulted from someone else's negligence, then, he or she
          should be held accountable for your injuries, damages and losses. If
          you or a loved one has suffered a brain injury, please call us for a
          free, no-obligation case consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
