// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Nursing Home Abuse Lawyer - Elder Neglect Attorneys"
        pageDescription="Financial abuse and sexual assaults are unfortunately one of the forms of nursing home abuse or neglect. The Yorba Linda Nursing Home Abuse Lawyers of Bisnar Chase believe that elder care facilities that mistreat assisted living residents should be brought to justice. Call 949-203-3814 to receive a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Nursing Home Abuse Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          Financial abuse and exploitation in nursing homes are sad realities,
          which many of our seniors face. Financial abuse is a form of elder
          abuse, which amounts to criminal fraud. This type of abuse could occur
          even in the most reputed nursing homes. If your loved one has been the
          victim of nursing home abuse, it is important that you contact an
          experienced{" "}
          <Link to="/nursing-home-abuse">Yorba Linda nursing home lawyer</Link>{" "}
          to better understand your legal rights and options.
        </p>
        <p>
          <em>
            <strong> For immediate assitance, please call 949-203-3814.</strong>
          </em>
        </p>
        <h2>
          <strong> Nursing Home Financial Abuse </strong>
        </h2>
        <p>
          Financial exploitation is when someone uses the property or money of
          another with the intent to defraud that individual or wrongfully use
          his or her funds. Elderly patients in nursing homes are asked to trust
          their care providers to provide them with adequate care and treatment.
          Seniors who are in the care of a nursing home are often vulnerable.
          Their financial information such as bank account numbers or credit car
          numbers or even passwords may become compromised in such an
          environment.
        </p>
        <h2>
          <strong> Examples of Financial Abuse</strong>
        </h2>
        <p>
          There are many forms of nursing home financial abuse and many ways in
          which an elderly resident may be exploited. Common examples of
          financial abuse that can occur in a nursing home include:
        </p>
        <ul type="disc">
          <li>Forging a senior's signature for financial gain</li>
          <li>Cashing a resident's check without authorization</li>
          <li>Theft of money or possessions from the victim's room</li>
          <li>
            Deceiving an elderly person into signing a will, contract or other
            document
          </li>
          <li>Tricking residents to invest in a fraudulent scheme</li>
        </ul>
        <h2>
          <strong> Financial Abuse Facts</strong>
        </h2>
        <p>
          According to a study by The National Center on Elder Abuse, between
          one and two million Americans aged 65 or older have been exploited,
          injured or have been otherwise mistreated by someone they depend on
          for care. Current estimates put the overall reporting of financial
          exploitation at only 1 in 25 cases, suggesting that there may be at
          least 5 million financial abuse victims each year. A study by The
          National Elder Abuse Incidence Study found that for every one case of
          exploitation, elder abuse, neglect or self-neglect reported to
          authorities, about five more go unreported. Therefore, it is hard to
          understand just how significant an issue financial abuse is in nursing
          homes, but it is clearly a problem that needs to be taken seriously.
        </p>
        <h2>
          <strong> How Financial Abuse Occurs</strong>
        </h2>
        <p>
          Patients can be financially exploited by their care providers, the
          nursing home, the administrators, visitors to the building or even
          their fellow residents. Some cases involve theft from someone who is
          vulnerable or incapacitated while others involve fraud and trickery.
          It is common for the elderly in these types of situations to be
          subjected to threats, lies, manipulation, intimidation and deception.
          Sadly, many people see someone's vulnerability as an opportunity for
          financial gain.
        </p>
        <h2>
          <strong> Signs of Financial Exploitation</strong>
        </h2>
        <p>
          Unfortunately, unless you are keeping a vigilant and watchful eye over
          your loved one's finances, it may be difficult to notice financial
          abuse when it happens. When you visit the facility, make sure to take
          stock of the valuable possessions in the room. It may also be in your
          best interest to take the time to meet and speak with the workers who
          care for your loved one. Also, make sure to talk to your loved one
          about their daily activities and interactions. Opening up
          communication is sometimes the only way to become aware of potential
          wrongdoing. Sudden changes in your loved one's finances such as a
          modified will or suspicious credit card activity should be taken
          seriously and reported immediately.
        </p>

        <p>
          The skilled Yorba Linda nursing home lawyers at Bisnar Chase can help
          review the circumstances of your situation to help you determine if
          wrongdoing has occurred. In some cases, in addition to criminal
          prosecution, civil remedies may be available.
        </p>
        <p>
          If the nursing home was negligent in terms of supervision, they can
          also be held liable for the victim's losses. If your loved one has
          suffered financial abuse in a Yorba Linda nursing home, please call us
          at <strong> 949-203-3814</strong> for a free, comprehensive and
          confidential consultation or fill out the online form.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
