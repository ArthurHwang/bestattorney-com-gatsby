// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Motorcycle Accident Lawyers - Bisnar Chase Attorneys at Law"
        pageDescription="If you have experienced a  motorcycle injury due to an accident contact the experienced Yorba Linda Motorcycle Accident Lawyers of Bisnar Chase. Our bike accident attorneys will work to win your case for the compensation you deserve. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <h2>Yorba Linda Motorcycle Accident Attorneys</h2>
        <p>
          If you or a loved one has been seriously injured in a motorcycle
          accident, the experienced Yorba Linda motorcycle accident lawyers at
          Bisnar Chase can help you better understand your legal rights and
          options. We have a long and successful track record of helping
          motorcycle accident victims and their families secure fair and full
          compensation for their injuries and losses. Please call us at{" "}
          <strong> 949-203-3814</strong> to obtain more information about
          pursuing your legal rights.
        </p>
        <p>
          If you have been injured in a{" "}
          <Link to="/motorcycle-accidents">
            Yorba Linda motorcycle accident
          </Link>
          , you may be wondering where the money will come from to pay your
          bills. Have you had to miss work because of your injury? Are you
          struggling with performing everyday activities? Are you watching your
          medical bills pile up? It may be in your best interest to contact an
          experienced Yorba Linda motorcycle accident lawyer who can help you
          determine if you have a case and whether compensation may be available
          for your injuries, damages and losses.
        </p>
        <p>
          <em>
            <strong> For immediate help, please call 949-203-3814.</strong>
          </em>
        </p>
        <h2>Determining Liability in a Motorcycle Accident</h2>
        <p>
          Innocent victims should not have to pay for the losses that stem from
          an accident that was caused by the actions of a negligent driver.
          Under California law, victims of injury accidents may pursue financial
          compensation from the at-fault party through civil litigation. This
          means that injured victims will have to prove that the other motorist
          involved was negligent. There are several critical questions that must
          be asked after a motorcycle accident.
        </p>
        <p>
          Was the other driver speeding at the time of the collision? Was the
          car driver talking on a cell phone, texting or otherwise distracted?
          Was alcohol a factor in the collision? If the authorities cite the
          other driver, it will help bolster your claim by adding more
          credibility to it. A driver does not, however, have to be cited by the
          authorities to be held civilly liable for an accident.
        </p>
        <h2>The Value of a Motorcycle Injury Claim</h2>
        <p>
          Determining how much a claim may be worth can be a complicated
          process. In general, more serious injuries typically result in larger
          settlements. Did the victim suffer a disability? Have the injuries
          resulted in a loss in the victim's quality of life? Has the injury
          affected the victim's future earning capacity? The answers to these
          types of questions will affect the potential value of a successful
          personal injury claim.
        </p>
        <p>
          All losses suffered because of the accident can be included in the
          lawsuit. Victims can seek compensation for medical bills, emergency
          room charges, costs of rehabilitation services, prescription drug fees
          and hospitalization expenses. All wages that are lost during the
          healing process should be included in a claim as well.
        </p>
        <p>
          A skilled Yorba Linda motorcycle accident attorney can also help you
          calculate non-economic losses that may be included in a claim. For
          example, physical pain and mental anguish can be given a monetary
          value during the lawsuit. This type of loss should be calculated with
          the help of an attorney with experience handling similar cases.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
