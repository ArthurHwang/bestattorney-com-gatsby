// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Bicycle Accident Attorney - Bike Accident Lawyer"
        pageDescription="The Yorba Linda Bicycle Accident Attorneys of Bisnar Chase has been helping bike accident victims win millions in compensation for over 40 years. If you have experienced injuries from a bicycle accident call 949-203-3814 today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Bicycle Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <p>
          There are many injury accidents involving bicyclists in Yorba Linda
          each year. It is common for these incidents to involve negligent car
          drivers or reckless cyclists. There are some accidents, however, that
          do not result from poor decisions on the part of a car motorist or
          bicyclist. Some accidents occur as the result of a bicycle defect.
        </p>
        <p>
          In such cases, an experienced Yorba Linda bicycle accident attorney
          who has successfully handled{" "}
          <Link to="/defective-products">product liability cases</Link> on
          behalf of injured victims and their families can help hold the bike
          manufacturer or the maker of the defective product accountable for the
          crash.
        </p>

        <h2>
          <strong> Bike Accidents Involving Bicycle Defects</strong>
        </h2>
        <p>Manufacturers can be held accountable for a crash if:</p>
        <ul type="disc">
          <li>
            The bicycle was defectively designed. This means that the design of
            the bicycle was inherently flawed. This means that something about
            the design made it prone to an accident. In these types of cases, it
            is common for every bicycle built with the defective design to pose
            a risk for riders.
          </li>
          <li>
            The bicycle was improperly manufactured. This is when the design of
            the bicycle is sound, but it was poorly assembled. In these types of
            cases, only a selection of the bicycles may be potentially
            dangerous. It is common for bicycle companies to issue a safety
            recall on a portion of the bikes made at a certain factory over a
            specific period of time. Perhaps they used the wrong parts or the
            workers were not properly trained.
          </li>
          <li>
            Bicycle parts manufacturers can be held liable if a specific part
            fails and causes the accident and resulting injuries.
          </li>
        </ul>
        <h2>
          <strong> Examples of Bicycle Defects</strong>
        </h2>
        <p>
          One way to understand the way a bicycle can be considered defective is
          to review recent recalls. A bicycle or bicycle part will only be
          recalled after it is determined that it poses a clear risk to riders.
          The following list of recalled bicycles were listed in a press release
          by the CPSC on May 15, 2012:
        </p>
        <ul type="disc">
          <li>
            Defective chains: 91,000 Bridgeway Bicycles were recalled for a
            bicycle chain that was prone to breaking. The recall was issued
            after 11 reports were made involving nine injuries to bicyclists who
            were unable to retain control after their chain broke.
          </li>
          <li>
            Defective seat clamp: 27,000 Trek 2012 FX and District bicycles were
            recalled after it was determined that they had a bolt that secured
            the seat saddle clamp to the seat post that could break. At the time
            of the release, there were four reports of incidents including one
            injury.
          </li>
          <li>
            Defective bicycle frame: 10,500 Fuji Saratoga Women's Bicycles were
            recalled for having a frame that could break in the center. There
            were at least 12 reports of bicycle frames breaking resulting in at
            least two injuries.
          </li>
          <li>
            Defective pedals: 4,100 Public Bikes 2010 through 2012 model year
            bicycles were recalled for pedals that cracked and broke posing a
            falling hazard.
          </li>
          <li>
            Defective brakes: Specialized had to recall 460 bicycles from model
            year 2012 that had brake component housed within the bicycle's
            carbon fork that disengaged.
          </li>
          <li>
            Defective trailer hitch: 44,000 chariot bicycle trailers were
            recalled for having mechanisms that can crack and break.
          </li>
          <li>
            Defective brake cables: 9,700 Gore Bicycle Brake Cables for Road
            Bikes were recalled for detaching from Campagnolo style brake
            levers, resulting in a falling hazard.
          </li>
        </ul>
        <p>
          If you have been injured because of a bicycle defect or because of
          someone elses negliegence, please do not hesitate to contact an
          experienced Yorba Linda bike accident lawyer at our law firm. It is
          important to preserve the bicycle unaltered so that an expert can
          thoroughly examine it for defects, malfunctions and other evidence.
        </p>
        <p>
          <em>Please call us for a free confidential case evaluation.</em>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
