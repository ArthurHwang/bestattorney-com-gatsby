// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Bus Accident Lawyer - Bus Crash Attorney"
        pageDescription="If you have suffered injuries due to a bus crash, the Yorba Linda Bus Accident Lawyers are here for you. The law offices of Bisnar Chase will prove liability and gain the compensation you rightfully deserve. Call 949-203-3814 to speak with an experienced bus collision attorney for free."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Bus Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <p>
          Utilizing a city transit bus or a school bus is a cost effective way
          to travel that is generally very safe. Unfortunately, there are
          instances in which a bus accident results in devastating injuries for
          the passengers on board.
        </p>
        <p>
          Bus accidents are handled differently compared to many injury
          accidents and it is important that victims understand their legal
          rights and options. Anyone injured while riding a bus would be well
          advised to discuss the specifics of their case with an experienced
          Yorba Linda bus accident lawyer.
        </p>

        <p>
          <em>
            <strong> Call for immediate legal help at 949-203-3814.</strong>
          </em>
        </p>

        <h2>
          <strong> What to Do after a Bus Accident</strong>
        </h2>
        <p>
          If you have been involved in a bus crash, make sure that you and your
          loved ones receive immediate medical attention. Even if you are not
          sure how injured you are, it is in your best interest to request a
          full medical evaluation. As soon as the bus accident occurs, the
          authorities should be notified. Make sure to get your name on the
          police report.
        </p>
        <p>
          It is also important that you collect information from the crash site.
          Write down the name of the drivers involved. Write down exactly when
          and where the crash occurred. Where were you sitting? Did you witness
          what the bus driver was doing at the time? For example, was he or she{" "}
          <Link to="/blog/texting-while-walking-laws">
            talking on the cell phone
          </Link>{" "}
          or distracted by a fellow passenger? Writing down all of the details
          you can remember will help you recall important information during the
          claim process.
        </p>
        <h2>
          <strong> Liability for Bus Accidents</strong>
        </h2>
        <p>
          The circumstances of the crash often determine who can be held
          responsible for your damages and losses. Here are a few examples of
          bus accident causes and who could be the liable party:
        </p>
        <ul type="disc">
          <li>
            <strong> Bus Company:</strong> Bus companies are responsible for
            properly maintaining their vehicles. When an accident occurs as a
            result of poor vehicle maintenance, then, the bus company may be
            held responsible for the crash. When a bus driver with a poor
            driving record causes an accident, the bus company may also be held
            liable for negligent hiring.
          </li>
          <li>
            <strong> Negligent bus driver:</strong> There are many forms of
            driver negligence that a bus driver can commit. Bus drivers must not
            operate a bus while fatigued, distracted or under the influence of
            alcohol and/or drugs. Bus drivers can be held liable for the
            accidents they cause. In addition, the bus company can also be held
            liable for negligence or wrongdoing on the part of the driver, who
            is an employee.
          </li>
          <li>
            <strong> Other drivers:</strong> The bus operator is not always the
            at-fault motorist. Drivers of cars, trucks and other vehicles may be
            held accountable for the accidents they cause.
          </li>
          <li>
            <strong> School district:</strong> If the accident involved a school
            bus, the school district and school board may be liable parties as
            well. Did the district fail to properly review the bus operator's
            driving record? Was the school bus unsafe for use? Were the children
            vulnerable to injury because of the maintenance of the bus or the
            negligence of the driver? Did the school board fail to establish and
            enforce reasonable safety guidelines that could have prevented the
            crash?
          </li>
        </ul>
        <h2>
          <strong> Contacting a Bus Accident Lawyer</strong>
        </h2>
        <p>
          There are state and federal laws and regulations governing bus service
          providers. It is important to have a Yorba Linda bus accident lawyer
          who has experience holding common carriers accountable for injury
          accidents. Bus companies will have powerful lawyers who will attempt
          to shoot down your claim and to fight any allegations against their
          client. It is important to protect your rights by having a skilled bus
          accident attorney by your side.
        </p>
        <p>
          The knowledgeable Yorba Linda bus accident lawyers at Bisnar Chase
          have a long and successful track record of pursuing compensation for
          bus accident victims and their families. Depending on the types of
          injuries you have suffered, you may have potential claims to cover
          damages including medical expenses, lost income, loss of earning
          capacity, physical therapy expenses, pain and suffering, loss of
          consortium and emotional distress.
        </p>
        <p>
          If you or a loved one has been injured in a bus accident in Yorba
          Linda, please call us for a free, comprehensive and confidential
          consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
