// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Car Accident Attorney - Auto Accident Lawyer"
        pageDescription="The law firm of Bisnar Chase has been providing quality legal service for accident victims for the past 40 years. Our Yorba Linda Car Accident Attorneys continue to represent clients and win millions for victims of car crashes. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <h2>Serving Yorba Linda For Over 30 years</h2>
        <p>
          The road can be a dangerous place. There are millions of cars, trucks
          and motorcycles on the road in southern California, and if you happen
          to be unfortunate enough to be in a car accident, you should know what
          to do.
        </p>

        <p>
          Yorba Linda has experienced a rise in car accidents over the last few
          decades. The population of the inland city is growing, not to mention
          the bars/restaurants and Yorba Linda Country Club churning out
          dangerous (sometimes intoxicated) drivers.
        </p>
        <h2>One Particular Intersection</h2>
        <p>
          One intersection, just off the 91 freeway has caused several car
          accidents over the last few years. The area just off the 91 where the
          90, the Richard M. Nixon Freeway, intersects it.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=yorba+linda,+ca&aq=&ie=UTF8&hq=&hnear=Yorba+Linda,+Orange,+California&t=m&vpsrc=6&ll=33.871556,-117.802792&spn=0.049885,0.072956&z=13&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          {" "}
          <Link to="http://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=yorba+linda,+ca&aq=&ie=UTF8&hq=&hnear=Yorba+Linda,+Orange,+California&t=m&vpsrc=6&ll=33.871556,-117.802792&spn=0.049885,0.072956&z=13&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <p>
          That entry road into Yorba Linda has several merges and areas that are
          regularly under construction. It travels along several shopping
          centers' entrance/exits which can cause traffic build up and rear-end
          collisions
        </p>
        <p>
          Negligent drivers need to be held accountable for their actions. The
          road is a dangerous place, and some people make it more dangerous than
          others. Keeping yourself safe, protected, and prepared in case of a
          car accident in Yorba Linda should be your first priority.
        </p>
        <h2>What To Do If You've Been In An Accident</h2>
        <p>
          The Yorba Linda car accident attorneys of Bisnar Chase have helped car
          accident victims for over 30 years with spectacular results and
          millions of dollars recovered. We know how to litigate these cases and
          we can help you find justice.
        </p>
        <p>
          If you've been{" "}
          <Link to="/car-accidents">injured in a car accident</Link> in Yorba
          Linda, we can help you. It wasn't your fault, and we know how to get
          the compensation you deserve. The at-fault party is not only morally
          responsible, but financially responsible as well. Call us today for
          your Free, No-Obligation Consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
