// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Yorba Linda Wrongful Death Lawyers - Bisnar Chase"
        pageDescription="If you have experienced the loss of a loved one due to someone else's negligence contact the Yorba Linda Wrongful Death Lawyers of Bisnar Chase. The law firm of Bisnar Chase legally represents clients whose family member was unjustly taken from them and has won millions in compensation. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Yorba Linda Wrongful Death Lawyer</h1>
        <BreadCrumbs location={location} />
        <h2>Yorba Linda Wrongful Death Attorneys</h2>
        <p>
          A knowledgeable wrongful death lawyer can help the family of a
          deceased victim understand their legal rights and options. Depending
          on the type of accident and who was involved, financial compensation
          may be available for the resulting medical bills, funeral expenses,
          lost future wages, loss of companionship and other related damages.
          The reputed Yorba Linda wrongful death attorneys at Bisnar Chase have
          a long and successful track record of helping families obtain fair
          compensation for their losses. Please call us at{" "}
          <strong> 949-203-3814</strong> for a free and comprehensive
          consultation.
        </p>
        <p>
          A wrongful death is when someone is killed by another's negligence.
          There are many types of fatal accidents that can result in a wrongful
          death. Anyone who has lost a loved one in a tragic accident would be
          well advised to speak with a{" "}
          <Link to="/wrongful-death">Yorba Linda wrongful death lawyer</Link>{" "}
          regarding his or her legal rights and options. Depending on the cause
          of the accident, financial compensation may be available for their
          terrible loss.
        </p>
        <h2>
          <strong> Yorba Linda Wrongful Death Car Accidents</strong>
        </h2>
        <p>
          A wrongful death car accident is when someone's negligence or
          wrongdoing resulted in a fatal accident. Was the at-fault driver
          speeding or driving distracted? Did the other driver run a red light
          or drive recklessly while under the influence of drugs or alcohol?
          These are forms of negligence that can result in a wrongful death.
        </p>
        <p>
          According to California Highway Patrol's 2009 Statewide Integrated
          Traffic Records System (SWITRS), one person was killed in a bicycle
          accident and one other person was killed as the result of a DUI
          collision in Yorba Linda. In Orange County as a whole, there were 141
          traffic fatalities that year. Of those fatalities, 62 were the result
          of alcohol-related crashes, 41 involved pedestrians, 12 accidents
          involved bicyclists and 17 fatalities were reported as a result of
          motorcycle accidents. Not all traffic fatalities automatically result
          in wrongful death claims. But the family of someone killed in a Yorba
          Linda car accident would be well advised to discuss his or her legal
          options with an experienced wrongful death lawyer.
        </p>
        <h2>
          <strong> Nursing Home Wrongful Death</strong>
        </h2>
        <p>
          All nursing homes have a legal responsibility to provide adequate care
          to their patients. Sadly, many elderly patients in Orange County
          nursing homes suffer because of understaffed facilities and
          unqualified care providers. A{" "}
          <Link to="/nursing-home-abuse">nursing home wrongful death</Link> may
          involve abuse or neglect that directly resulted in the fatal injuries
          suffered by the victim.
        </p>
        <h2>
          <strong> Defective Product Wrongful Death</strong>
        </h2>
        <p>
          When someone is fatally injured while using a product, a complete
          investigation may be needed to determine if the product was defective.
          Examples of defective products that have led to wrongful deaths
          include faulty auto parts, medications, food products, child toys and
          appliances. Did the product fail to perform as advertised? Did the
          product manufacturer fail to list an ingredient on the label that the
          victim was allergic to? Did the product fail to work properly because
          of defective assembly? These are some of the questions to ask in
          wrongful death cases involving defective products.
        </p>
        <h2>
          <strong> Medical Malpractice Wrongful Death</strong>
        </h2>
        <p>
          All medical professionals have a legal obligation to provide adequate
          care and attention to their patients. Wrongful deaths that occur at a
          medical facility may involve giving a patient the wrong medication,
          failing to diagnose cancer, neglecting a patient in need of emergency
          care or an egregious error made during a surgery.
        </p>
        <h2>
          <strong> Premises Liability Wrongful Deaths</strong>
        </h2>
        <p>
          Property owners are required to provide safe conditions to all
          visitors, residents and clients. Staircases must be in proper working
          order. Visitors must be warned about wet floors that are slippery.
          Hazardous locations must be repaired within a reasonable amount of
          time. When an individual is fatally injured on someone else's
          property, it must be determined if a hazardous condition caused the
          accident. Was the property owner aware of the condition? Did the
          property owner or property management fail to prevent the accident?
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
