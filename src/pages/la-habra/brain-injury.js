// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Brain Injury Attorneys & Lawyers"
        pageDescription="Are you seeking a La Habra brain injury attorney? Call 949-203-3814 for a free attorney consultation. We have years of experience and results. Call today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Habra Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Car accidents, motorcycle accidents, bicycle accident, slip and fall
          accidents, and pedestrian accidents can all lead to brain injuries. 
          Brain injuries are the very worst and most serious type of personal
          injury besides death, because brain injuries can mean a lifetime of
          disability or low level of functioning for a victim. La Habra brain
          injury lawyers work with those who have suffered brain injuries as the
          result of the negligence, carelessness, or illegal activity of an
          individual, company, or governmental authority.
        </p>
        <p>
          Brain injuries are not always life-threatening, and some types of
          brain injuries mean only a short convalescence.  For example, a minor
          concussion from a fall or a sharp blow to the head may only mean a few
          days of headaches or blurred vision, and then the wound may heal
          completely.  On the other hand, a fall or a blow to the head can also
          produce permanent brain damage with effects that are felt for a
          lifetime.
        </p>
        <p>
          What is so disquieting about brain injuries is that there is really no
          way to predict how an individual will react to a particular type of
          trauma.  Some people receive very powerful impacts to the skull and
          have almost no damage whatsoever, while others sustain relatively
          minor head trauma but develop permanent brain damage.  Because brain
          injuries are so serious by their very nature, any blow to the head or
          neck should be considered a serious injury and should be treated
          promptly by a qualified physician.  If you are in any type of accident
          that involves any head or neck trauma, never refuse medical treatment;
          have yourself transported to a hospital emergency room immediately,
          even if you feel fine.  Similarly, if a child or relative falls and
          hits his or her head, do not shrug off the injury; take them at once
          to a hospital emergency room or doctor's office for an examination. 
          Many brain injuries can be treated successfully if they are identified
          immediately and treatment begins promptly.
        </p>
        <p>
          Brain injury attorneys in La Habra represent victims of brain injuries
          no matter what the severity of the injury.  Even if you have suffered
          only a minor brain injury, you are entitled to recover damages from
          the person who injured you.  You are also entitled to have proper
          medical care to treat your brain injury at the expense of the at-fault
          party.  You should never be denied medical treatment due to lack of
          funds to pay for expensive procedures, drugs, or treatments.  Your La
          Habra brain injury attorney can help you recover these damages and pay
          for the treatments you need to get back to normal or to manage your
          brain injury.
        </p>
        <p>
          Your first consultation with a{" "}
          <Link to="/head-injury/brain-trauma">
            La Habra brain injury lawyer
          </Link>{" "}
          is always free, and there is no obligation if you choose not to pursue
          your case.  Why not talk to a professional personal injury attorney
          today? You can get the truth about your brain injury and learn how to
          recover damages for your pain and suffering, mental and physical
          trauma, and medical costs.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
