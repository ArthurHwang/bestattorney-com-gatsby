// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/la-habra-personal-injury-attorneys-banner.png"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "La Habra",
      population: 61653,
      totalAccidents: 2240,
      intersection1: "Idaho St & Imperial Hwy",
      intersection1Accidents: 103,
      intersection1Injuries: 93,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 141.6,
      city1Name: "La Habra Heights",
      city1Index: 85.5,
      city2Name: "Brea",
      city2Index: 185.6,
      city3Name: "Fullerton",
      city3Index: 214.7,
      city4Name: "La Mirada",
      city4Index: 143.6
    },
    locationBox3: {
      intersection2: "Whittier Blvd & Harbor Blvd",
      intersection2Accidents: 82,
      intersection2Injuries: 66,
      intersection2Deaths: 0,
      intersection3: "Beach Blvd & Lampert Rd",
      intersection3Accidents: 80,
      intersection3Injuries: 71,
      intersection3Deaths: 0,
      intersection4: "La Habra Blvd & Cypress St",
      intersection4Accidents: 66,
      intersection4Injuries: 69,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Personal Injury Lawyers - Orange County, CA"
        pageDescription="If you or a loved one has been injured in an accident, you would be well advised to speak with an experienced La Habra personal injury lawyer who can help you determine the value of your potential claim. We have the most experienced attorneys with decades of winning cases."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Habra Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="la habra personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been injured in an accident, you may be wondering how much
          compensation is available to cover your injuries and losses. It is not
          always in a victims best interest to seek compensation through civil
          litigation. But for those who have suffered serious injuries, it may
          be their only real option. If you or a loved one has been injured in
          an accident, you would be well advised to speak with an experienced
          <strong> La Habra personal injury lawyer</strong> who can help you
          determine the value of your potential claim.
        </p>
        <p>
          An experienced personal injury attorney knows that there are plenty of
          ways to suffer serious injuries within La Habra city limits. La Habra
          locals have the benefit of living in an Orange County environment with
          the benefits of Los Angeles County, but car accidents, slip and falls,
          pedestrian collisions and a number of other hazards can unexpectedly
          change victim's lives forever.
        </p>
        <p>
          Whether you have suffered injuries as the result of a{" "}
          <Link to="/la-habra/truck-accidents">negligent driver</Link>, careless
          business owner, distracted dog walker, or any other entity who had a
          responsibility to keep you from being harmed, our personal injury
          attorneys in La Habra will make sure that you get the compensation
          that your injuries deserve.{" "}
          <strong>
            We have been assisting Orange County personal injury victims for
            over 40 years
          </strong>{" "}
          and have done so with integrity, compassion, and a nearly-perfect
          <strong> 96% success rate.</strong>
        </p>

        <h2>Accident Injury Claims</h2>
        <p>
          If you have been injured in an accident, you will have to notify the
          at-fault parties insurance company. Ideally, you will be offered
          compensation for all of the losses you have suffered in the accident.
          Unfortunately, it is common for individuals and insurance companies to
          deny responsibility for the accident or for an insurance company to
          offer an inadequate settlement. This is why victims of serious
          injuries have it in their best interest to have a skilled personal
          injury lawyer on their side, who will fight to protect their legal
          rights.
        </p>
        <p>
          A knowledgeable personal injury lawyer can help you file a personal
          injury claim against the at-fault party. A personal injury claim is a
          form of civil litigation that can result in a settlement either
          through negotiations or through court proceedings. The victim will
          have to prove that the defendant was careless and that his or her
          carelessness or negligence resulted in the injuries and losses that
          you sustained.
        </p>

        <h2>How Much is a Claim Worth?</h2>
        <p>
          The value of a claim is directly related to the losses suffered by
          victims. Therefore, all losses will be considered in the lawsuit. Here
          are a few examples of damages that may be included in a personal
          injury claim:
        </p>
        <ul>
          <li>
            <strong> Medical treatment:</strong> All past and future medical
            bills related to the injuries suffered in the accident should be
            included in the claim.
          </li>
          <li>
            <strong> Loss of earnings:</strong> If you have lost wages as a
            result of missing work, you may be able to seek compensation for
            lost earnings as well. In instances where serious injuries are
            suffered, compensation may be available for lost future income. In
            cases where victims suffer lifelong disabilities, compensation may
            be claimed for loss of livelihood as well.
          </li>
          <li>
            <strong> Property damage and/or loss:</strong> Vehicles and other
            damaged items may be eligible for reimbursement.
          </li>
          <li>
            <strong> Pain and suffering:</strong> The court will take into
            consideration your non-financial losses as well. Did you suffer
            substantial pain because of the accident? Are you still in pain?
            Have you become stressed or depressed since the accident? Have you
            experienced a loss in the quality of your life because of your
            injuries?
          </li>
          <li>
            <strong> Punitive damages:</strong> In cases involving egregious
            recklessness, compensation may also be available for punitive
            damages. This is meant to punish the wrongdoer and deter such
            behavior in the future.
          </li>
        </ul>
        <h2>Determining If You Have a Claim</h2>
        <p>
          Civil litigation can be complicated and it often takes months or even
          years of negotiation before a settlement is reached. If you have a
          suffered an injury and worry that you will not get the compensation
          you need or rightfully deserve, the best course of action may be to
          seek the counsel of a skilled La Habra personal injury lawyer.
        </p>
        <h2>Award Winning Attorneys</h2>
        <p>
          Our personal injury lawyers have represented over 12,000 injury
          victims and have received several awards for our superior client
          representation and courtroom accomplishments. In addition to the
          highly-coveted Trial Lawyer of the Year award, our attorneys have
          received 10 out of 10 on Avvo.com's lawyer rating service and have
          outstanding ratings on Lawyers.com and Martindale Hubbell.
        </p>
        <p>
          At{" "}
          <strong>
            {" "}
            <Link to="/">Bisnar Chase</Link>
          </strong>
          , we provide our clients with the best legal representation available
          and a first-class personal experience designed to give our clients the
          best overall experience available. Our highly skilled staff and
          nationally recognized team of  injury attorneys will give you the
          peace of mind that comes with a law firm that has a reputation for
          honesty and success. We offer a free professional evaluation of your
          case, a <strong> No Win No Fee guarantee</strong>, and we offer our
          services at fees starting as low as 20%. Accident claims require
          experienced attorneys who have local knowledge to get the job done
          right. At Bisnar Chase, you will receive everything you need to get
          maximum compensation and move on with your life.
        </p>
        <p>
          Whether you suffered your injuries in Orange County or Los Angeles
          County, we have the experience you need. We have practiced throughout
          California and have obtained multi-million dollar verdicts and
          settlements against a wide variety of defendants. We have held Fortune
          500 companies accountable for their defective products and taken
          several auto accident cases to court with tremendous results that
          could not be duplicated. We have created a winning formula for success
          that has been tried, tested, and proven. The California court system
          is complicated; hiring a California attorney who lacks the attention
          to detail and legal expertise needed for complex cases can have
          devastating results. California limits the amount of time for personal
          injury victims to file a claim, so it is important for you to seek
          counsel to establish the amount of time you have left.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
