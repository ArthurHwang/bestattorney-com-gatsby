// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Nursing Home Abuse - Nursing Home Abuse in La Habra, California"
        pageDescription="Injured in a nursing home? Call 949-203-3814 to find out if you are eligible to receive compensation. Highly rated personal injury attorneys are here to help you."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Habra Conventional Hospital - La Habra, California</h1>
        <BreadCrumbs location={location} />
        <p>
          If you trusted a <Link to="/nursing-home-abuse">nursing home </Link>{" "}
          or assisted living facility to care for a family member or loved one,
          and now you suspect abuse, you are not alone. La Habra Conventional
          Nursing Home is part of the statistics of nursing home abuse which has
          doubled in the past decade alone. Carefully observing our loved ones
          and their situation while in a nursing home is essential, and when
          abuse is apparent, contacting a California elder abuse attorney can
          help immediately put a stop to the abuse and give you the compensation
          you deserve.
        </p>
        <p>
          La Habra Conventional Hospital has received serious violations from
          the health department including severe neglect in terms of both food
          and medication care and oversight with their elderly patients. Take a
          look at the list of violations reported by the Department of Health
          Services:
        </p>
        <ul>
          <li>
            Make sure that residents who take drugs are not given too many doses
            or for too long;
          </li>
          <li>make sure that the use of drugs is carefully watched; or</li>
          <li>stop or change drugs that cause unwanted effects.</li>
        </ul>
        <p>
          Develop a complete care plan that meets all of a resident's needs,
          with timetables and actions that can be measured.
        </p>
        <ul>
          <li>
            Give each resident care and services to get or keep the highest
            quality of life possible.
          </li>
          <li>
            Give professional services that follow each resident's written care
            plan.
          </li>
          <li>Have a program to keep infection from spreading.</li>
          <li>
            Have drugs and other similar products available, which are needed
            every day and in emergencies, and give them out properly.
          </li>
          <li>
            Keep accurate and appropriate medical records. Keep each resident
            free from physical restraints, unless needed for medical treatment.
          </li>
          <li>
            Make a complete assessment that covers all questions for areas that
            are listed in official regulations.
          </li>
          <li>
            Make sure that each resident who enters the nursing home without a
            catheter is not given a catheter, unless it is necessary.
          </li>
          <li>
            Make sure that the nursing home area is safe, easy to use, clean and
            comfortable.
          </li>
          <li>
            Prepare food that is nutritional, appetizing, tasty, attractive,
            well-cooked, and at the right temperature.
          </li>
          <li>
            Provide care in a way that keeps or builds each resident's dignity
            and self respect.
          </li>
          <li>
            Provide services to meet the needs and preferences of each resident.
            Store, cook, and give out food in a safe and clean way.
          </li>
          <li>
            Write and use policies that forbid mistreatment, neglect and abuse
            of residents and theft of residents' property.
          </li>
        </ul>
        <p>
          The aforementioned information is based off data reported by CMS as of
          7/29/10.
        </p>
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several classes depending on their severity. The
          state average is a little less than one per facility per year, but
          ideally, a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself. La Habra Conventional Nursing Center is part
          of a nation-wide epidemic of nursing home abuse and neglect. These
          shameful violations of human rights can be stopped with your support.
        </p>
        <p>Watch these videos to learn more:</p>
        <p>Videos Answer Questions on Elder Abuse</p>

        <p>
          The videos listed above explain the basics of nursing home abuse and
          how to spot the signs. Elder abuse can be classified as a case of
          neglect or intentional harm. The types of intentional harm in nursing
          homes include sexual assaults, physical abuse, emotional and verbal
          abuse, and even financial abuse. Elder neglect can include more
          passively endangering the patients with inadequate food, medicine,
          medical equipment, and proper accommodations. This can cause symptoms
          such as bedsores, malnutrition, overdose on medication or the wrong
          type of prescription, ulcers, and other bodily injuries. La Habra
          Conventional Hospital and Nursing Home has shocking neglect in its
          history.
        </p>
        <p>
          If you know of someone who has been abused or neglected in an elder
          care facility, it can be disheartening, but you can get help. At
          Bisnar Chase Personal Injury Attorneys, we believe that nursing homes
          have a responsibility to provide the care that elderly individuals
          need and deserve.
        </p>
        <p>
          Nursing home and elder abuse in California is a complicated matter
          which usually requires a specialist. With over 30 years of experience,
          Bisnar Chase Personal Injury Attorneys, in Orange County, has had the
          experience to fully investigate what many miss in order to get the
          best case results possible for the victim.
        </p>
        <p>
          Besides helping your loved one recover financial compensation for
          their pain, you can also improve safety laws in the nursing home
          industry with your voice.
        </p>
        <p>
          Call a California nursing home abuse attorney at Bisnar Chase Personal
          Injury Attorneys to get the answers you are looking for. 949-203-3814.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
