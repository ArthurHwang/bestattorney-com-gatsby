// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Car Accident Lawyer"
        pageDescription="Call 949-203-3814 for highest-rated auto accident injury lawyers, serving La Habra, Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          A La Habra Car Accident Lawyer Advises: Direct Your Own Medical Care
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The most successful La Habra accident injury lawyer knows that people
          who suffer a car crash are frequently confused as to where to go for
          help. Some lawyers, in trying to help their clients, often direct them
          when it comes to medical treatment. Unfortunately, this type of advice
          occurs with increasing frequency, driven by the many car crashes that
          happen in La Habra, California.
        </p>
        <h2>A Look at Car Accidents in La Habra</h2>
        <p>
          La Habra's car accident statistics can be disturbing. In 2006, the
          California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) reported that three people died and 266 were injured
          in La Habra car crashes. Motorcyclist accidents killed one and injured
          13. One pedestrian was killed and 20 were injured in car collisions.
          Bicyclist vs. car collisions injured 19. And drunk drivers caused two
          deaths and 29 injuries. In 2007, there were three car accidents that
          resulted in five fatalities. The following year, one car accident
          resulted in one death.
        </p>
        <h2>Let Your Doctor Direct Your Medical Treatment</h2>
        <p>
          Highly regarded{" "}
          <Link to="/resources/injured-in-car-accident">
            La Habra auto accident attorney
          </Link>{" "}
          know it's ill-advised to mingle specialties. The job of a car accident
          lawyer is to safeguard your legal rights, which means, doctors should
          advise you on medical matters. If you ever feel that your lawyer is
          trying to direct you to a specific medical practitioner, chiropractor,
          clinic, or treatment option, question his or her motives, for there
          may be a conflict of interest. Remember that a conflict of interest
          can exist even without improper actions (no kickbacks or incentives
          given to the lawyer). For it's the appearance of impropriety that can
          create a number of problems with your case. For example, defense
          lawyers can and will dig out this kind of information, and they will
          use it to their advantage--to raise doubts with the jury about any
          doctor's opinions whose payments depend on the successful outcome of a
          trial. They may also discover that a large number of patients were
          referred to one particular medical practitioner by your lawyer. This
          information can cause your treatments to be questioned by all those
          who decide the merits of your case.
        </p>
        <h2>
          Consult a Car Accident Lawyer Who Keeps Law and Medicine Separate
        </h2>
        <p>
          The most accomplished car accident lawyers know it's crucial to keep
          legal and medical advice separate. The best lawyers offer no charge,
          no pressure consultations and can tell you if they can be of help in
          your particular case. Moreover, a skilled La Habra car crash lawyer
          can advise you of your legal options to ensure you are fairly
          compensated for your losses and injuries.
        </p>
        <p>
          For more advice, please read{" "}
          <Link to="/resources/book-order-form">
            "The Seven Fatal Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          by California personal injury attorney John Bisnar. To contact our
          Orange County car accident lawyers please call 949-203-3814 or email
          us by filling out the contact form for a free consultation.
        </p>
        <p>
          <strong> Orange County Office</strong>
        </p>
        <div className="vcard mb">
          <div className="adr">
            <div className="street-address">1301 Dove St. Suite 120</div>
            <span className="locality">Newport Beach</span>,{" "}
            <span className="region">California</span>{" "}
            <span className="postal-code">92660</span>
            <br></br>
            <span className="tel">949-203-3814</span>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
