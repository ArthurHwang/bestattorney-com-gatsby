// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Motorcycle Accident Lawyer Bisnar Chase"
        pageDescription="Suffering from a La Habra motorcycle accident injury? Call 949-203-3814. Law firm reps are standing by. Free consultations. No win no fee. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Habra Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been injured in a La Habra motorcycle accident, you
          probably have a number of questions regarding how to pursue
          compensation for your losses and how to protect your rights. It may be
          in your best interest to discuss the specifics of your case with an
          experienced La Habra{" "}
          <Link to="/orange-county/motorcycle-accidents">
            {" "}
            motorcycle accident lawyer
          </Link>
          .
        </p>
        <h2>La Habra Motorcycle Accident Statistics</h2>
        <p>
          According to California Highway Patrol's 2009 Statewide Integrated
          Traffic Records System (SWITRS), there were no fatalities but 10
          injuries as a result of La Habra motorcycle accidents. Throughout
          Orange County, 17 people were killed and 735 were injured in
          motorcycle accidents during that same year. Also, in California, 389
          fatalities and 10,134 injuries were reported during the same year.
        </p>
        <h2>What should I do immediately following the crash?</h2>
        <p>
          If someone has been hurt, call the authorities. Exchange information
          with the other motorists involved and obtain the contact information
          from anyone who may have witnessed the accident. Take photos of the
          crash site, the vehicles and of your own injuries.
        </p>
        <h2>Why do I need to take photos?</h2>
        <p>
          The more evidence you collect from the crash site, the stronger your
          case will be. If the other driver decides to deny responsibility for
          the accident, you will want every piece of evidence you can get.
          Dealing with insurance companies can be a complicated process and you
          will want as much proof as you can find.
        </p>
        <h2>When should I call the other driver's insurance company?</h2>
        <p>
          To pursue compensation for the accident, you will eventually need to
          speak with the at-fault driver's insurance company. It may be in your
          best interest to not make this phone call right away. Before calling
          an insurance company, you should have all of the facts straight, all
          of the details written out and a comprehensive list of damages. If you
          have suffered a serious injury, it is in your best interest to have an
          attorney make the phone call for you or have the attorney deal with
          the insurance company.
        </p>
        <h2>How is liability determined?</h2>
        <p>
          Proving who was responsible for the crash can be a complicated
          process. In some cases, the authorities may cite the at-fault driver.
          In such cases, it will be easier for the victim to prove liability for
          the collision. Usually, after an accident, several questions remain.
          Was the other motorist exceeding the speed limit? Did he or she run
          through a red light or drive while under the influence of drugs or
          alcohol? A driver does not, however, need to be cited or criminally
          charged in order to be held civilly liable for a collision.
        </p>
        <h2>What is a personal injury claim?</h2>
        <p>
          A personal injury claim is a civil lawsuit that can be filed by an
          injured victim of a motorcycle accident. While a criminal proceeding
          requires the prosecution to prove the defendant was guilty beyond
          reasonable doubt, a civil lawsuit only requires the victim to prove
          that the defendant was negligent or liable for the collision.
        </p>
        <h2>Do I have to hire an attorney?</h2>
        <p>
          Not all plaintiffs require an attorney, but it is often in the best
          interest of injured victims to discuss their case with an attorney. If
          no injuries were suffered, the victim will be able to seek
          compensation for their damaged vehicle by calling the insurance
          company involved. If, however, an injury was suffered and there are
          mounting medical bills and lost wages, you would be well advised to
          have an experienced attorney on your side.
        </p>
        <h2>What an Experienced La Habra Motorcycle Accident Lawyer Can Do</h2>
        <p>
          A skilled personal injury attorney will help determine liability for
          the accident, hold the at-fault party accountable for the collision
          and pursue fair compensation for the injuries suffered. If you or a
          loved one has been seriously injured in a motorcycle accident, the
          experienced La Habra motorcycle accident lawyers at Bisnar Chase
          Personal Injury Attorneys can help you better understand your legal
          rights and options. We will remain on your side, fight for your rights
          and strive to ensure that you get the best possible result in your
          case. Please <Link to="/contact"> contact us </Link> for a free,
          comprehensive and confidential consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
