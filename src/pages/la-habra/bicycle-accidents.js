// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="La Habra Bicycle Accident Lawyers - Bisnar Chase"
        pageDescription="Are you an injured La Habra bike accident victim? Please call 949-203-3814 for a free consultation with a bike accident attorney. Years of experience & results."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>La Habra Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          La Habra bicycle accident lawyers often study statistics published by
          groups such as the National Highway Traffic Safety Administration to
          learn about trends in bicycle accidents. Studying these statistics
          gives La Habra bicycle accident attorneys the knowledge they need to
          successfully pursue damages for the unfortunate victims of bicycle
          accidents and the families affected by these losses.
        </p>
        <p>
          For example, the NHTSA notes that nearly 70 percent of all bicycle
          accidents are in urban areas and 65 percent occur at
          non-intersections.  Armed with this knowledge, bicycle accident
          lawyers in La Habra and the groups that support bicycle safety can
          lobby for the building of more bike paths and crosswalks so that
          bicycle riders are safer when riding on city streets.
        </p>
        <p>
          The NHTSA also states that 41 is the average age of victims of bicycle
          fatalities, and 31 is the average age for bicycle injury victims. 
          These figures may comes as a surprise to some who feel that bicycle
          accidents are mainly a &ldquo;kid's problem&rdquo;; obviously many of
          the people who are hurt or killed in bicycle accidents are wage
          earners for families and people who have families that are affected
          financially by their accidents.  This means that bicycle accident
          attorneys must work hard to recover not only medical costs and sums
          for pain and suffering but also lost wages and family expenses that
          may result from bicycle accidents.
        </p>
        <p>
          Finally, the NHTSA notes that more than 80 percent of bicycle accident
          victims are men.  Does this mean that men are simply more likely to
          take dangerous chances on bicycles, that men ride bicycles more often
          than women, or that drivers are less likely to yield to a man on a
          bicycle than to a woman or a child?  No one has satisfactorily
          answered this question, but bicycle accident attorneys in La Habra use
          this information as they consider how to approach the pursuit of
          damages in a bicycle accident case.  Knowing that the average bicycle
          accident injury victim is a male of the age of 31 means that La Habra
          bicycle accident attorneys are often called on to pursue damages for
          the primary wage earner of a family and for a young man who will need
          ongoing medical care and therapy to return to pre-accident physical
          condition.
        </p>
        <p>
          Bicycle accident attorneys in La Habra use this information in many
          ways.  Most personal injury cases are settled out of court, so a La
          Habra bicycle accident attorney must be prepared to calculate a fair
          settlement amount to use in negotiations.  It helps a bicycle accident
          attorney to calculate this amount accurately when he or she has all
          the information possible to justify a claim.
        </p>
        <p>
          If you have been the victim of a{" "}
          <Link to="/orange-county/bicycle-accidents">bicycle accident </Link>{" "}
          in La Habra, no matter what your age, gender, or physical condition,
          talk to a professional bicycle accident attorney about your case.  You
          will find that you are entitled to far more in damages than you might
          believe, and that these damages are much easier to recover with the
          help of a personal injury lawyer.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
