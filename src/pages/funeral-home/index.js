// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Funeral Home Abuse Attorneys - Funeral Home Negligence Lawyers"
        pageDescription="Call 949-203-3814 for top-rated funeral home attorneys in Orange County, California. Get a Free Consultation. No-Win, No-Fee since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Funeral Home Abuse Attorneys</h1>
        <BreadCrumbs location={location} />
        <h3 style={{ color: "red", fontSize: "3rem" }}>
          We are not currently taking funeral home abuse cases
        </h3>
        <p>
          <strong> Funeral home abuse</strong> is an appalling act that is too
          common today in the United States. Because the victims are deceased
          there is often no way for the victims to protect or defend their body
          and dignity from physical abuse and desecration.
        </p>
        <p>
          Families of funeral home abuse victims can rely on the expert legal
          representation of Bisnar Chase Personal Injury Attorneys.
        </p>
        <p>
          When a loved one dies, family and friends will gather to celebrate
          their life and honor their memory. Often the celebrations involve one
          or a combination of a wake, funeral, and burial or cremation ceremony.
        </p>
        <p>
          In this extremely emotional time, family members are vulnerable and
          look to funeral homes, cemeteries and crematoriums for guidance. In
          the past the worst offense from cemeteries, funeral home or cremation
          facilities was being oversold or overcharged for the procedures.
          Unfortunately, the last few years have seen a far more disturbing
          trend of negligence; one that appears to be on the rise.
        </p>
        <p>
          These incidents regularly involve greed, corruption, and deception.
          The following atrocities have been reported by news agencies in the
          past few years:
        </p>
        <ul>
          <li>
            Coffins retrieved after burial and switched out for cheaper ones
          </li>
          <li>Multiple bodies placed in one coffin</li>
          <li>Bodies robbed of jewelry</li>
          <li>Body parts removed and sold</li>
          <li>Grave sites left untended, to be vandalized</li>
          <li>Overlooked graveyards covered by garbage dumps</li>
          <li>Bodies left to decompose in funeral home basements</li>
          <li>Sexual abuse of a corpse</li>
        </ul>
        <p>
          New allegations of misconduct surface every week and the crimes are
          often more heinous than the last. To educate families of the deceased
          the Federal Trade Commission has established guidelines regarding
          dealing with funeral homes to avoid an unfair advantage on the part of
          the facility.
        </p>
        <p>
          Some of the rules include requiring that the funeral home disclose all
          prices of goods and services, advice to the client that embalming is
          not required in many cases and that no form of preparation of the body
          or coffin will preserve the deceased.
        </p>
        <p>
          As cemeteries, crematories, and funeral homes merge or are acquired by
          other companies, questionable practices for the purpose of cost
          effectiveness have begun to attract attention. Funeral home
          exploitation includes fraud and deception, pre-need contracts,
          inappropriate methods of cremation or embalming, and other
          mishandlings of the deceased.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
