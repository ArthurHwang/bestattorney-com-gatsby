// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="IV Bag Contamination - Products Liability Attorneys"
        pageDescription="Have you or a loved one suffered injuries due to IV bag contamination or other defective products? Call 949-203-3814 for highest-rated accident injury attorneys, serving Los Angeles, Newport Beach, Irvine, Orange County & San Francisco, California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Alabama IV Bag Contamination Leads to Nine Deaths</h1>
        <BreadCrumbs location={location} />

        <p>
          When officials discovered that nine deaths in Alabama were directly
          related to contaminated IV bags, many felt that this dangerous{" "}
          <Link to="/defective-products">product defect </Link> was handled in
          the best way possible. Although nine people were killed, the death
          count was not substantially higher because employees working at the
          contaminated hospital sites were able to identify and report it.
          Unfortunately, the masses have become so accustomed to defective
          products that many forget to even consider that these deaths were the
          fault of another. As with any products liability case, these deaths
          could have been easily avoided had the manufacturers spent the time to
          ensure the safety of their consumers.
        </p>
        <h2>Birmingham-Based Meds IV Stays Quiet During Investigation</h2>
        <p>
          The culprit of this new{" "}
          <Link to="/defective-products">products liability </Link> disaster
          seems to be Birmingham-Based Meds IV. According to state health
          officials, nine of the nineteen patients who were infected with
          bacteria that got into their blood after they were fed intravenously
          have died in six Alabama hospitals. All of these cases seem to have
          stemmed from the use of an IV bag that was manufactured by
          Birmingham-Based Meds IV.
        </p>
        <p>
          The bacteria, identified as serratia marcescens bacteremia, can prove
          fatal, though investigators -- including those from the Centers of
          Disease Control and Prevention -- have not determined that they caused
          the deaths, they said, adding that the investigation is ongoing. They
          said that the bacterium that was discovered in the defective IV bags
          would have entered the bloodstream easily and "with a pretty quick
          effect in terms of blood pressure and temperature," he said.
        </p>
        <p>
          Health officials are clear that these product defects no longer pose a
          threat to anyone who has not already been identified and that the
          product, which was recalled last Thursday, was available only from
          Birmingham-Based Meds IV and sold to the hospitals.
        </p>
        <p>
          The most disturbing part of a product liability case is the fact that
          the manufacturers of these deadly products, in most cases, do not ever
          admit fault or apologize to the victim's families. This case is no
          different and the manufacturer, Birmingham-Based Meds IV, has yet to
          release a statement.
        </p>
        <h2>California Products Liability Attorneys</h2>
        <p>
          If you or a loved one has been infected by one of these deadly IV
          bags, contact one of our experienced{" "}
          <Link to="/pharmaceutical-litigation">
            California pharmaceutical litigation attorneys
          </Link>{" "}
          for a free professional evaluation of your case by attorneys who have
          represented over 12,000 clients since 1978. You will experience award
          winning representation and outstanding personal service by a friendly
          law firm in a comfortable environment.
        </p>
        <p>
          Call 949-203-3814. The call is free, but the advice may be priceless.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
