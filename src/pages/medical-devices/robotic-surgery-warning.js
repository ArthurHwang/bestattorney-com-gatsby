// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle='Robotic Surgery: Do Dangers Trump its " Wow" Factor?'
        pageDescription="Robotic surgery is a superstar is the operating room but for how long? More cases are coming out of patients suffering terrible injuries when operated on by the Da Vinci robotic system and are questioning its safety"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Robotic Surgery: Do Dangers Trump its 'Wow' Factor?</h1>
        <BreadCrumbs location={location} />

        <p>
          <img
            src="/images/davinci-robot-227x300.jpg"
            className="imgright-fixed"
          />
          More and more{" "}
          <Link to="/defective-products">product liability law firms </Link> in
          the nation are hearing about a new antagonist in the operating room.
          It's not a negligent doctor or careless member of the hospital staff.
        </p>
        <p>
          We're talking about a million-dollar, multi-armed robot named Da Vinci
          that was used in hundreds of thousands of surgeries just last year -
          about 400,000 surgical procedures to be precise. That is three times
          the number from just four years ago.
        </p>
        <p>
          It is apparent that this mechanical helper soared in popularity with
          hospitals and doctors over the last few years.
        </p>
        <p>
          Many even believe that these robots were part of the "wow" factor that
          attracted patients to certain hospitals. But now, these robots are
          under scrutiny after many reports of problems including nerve
          injuries, infections, lacerations and even deaths that have been
          linked to it.
        </p>
        <p>
          In addition, there were also been frightening incidents including a
          robotic hand that grabbed on to tissue during surgery and refused to
          let go. There was another report of a robotic arm hitting a patient in
          the face as she lay on the operating table.
        </p>
        <h2>Marketing of Robot Surgery</h2>
        <p>
          Certainly, these incidents raise red flags and send us the message
          that it is time to put the brakes on this "robotmania." Many are
          worried that there is a powerful marketing machine behind this boost
          in the use of robot surgeons. Many experts argue that there is no
          solid research, which shows that this type of surgery is good or
          better than traditional surgeries.
        </p>
        <p>
          Robotic surgery is marketed in hospital brochures, websites and even
          on highway billboards with the hope that patients eager to undergo
          such treatment will help pay for the expensive robot. So far, the{" "}
          <Link to="/blog/popular-da-vinci-robotic-surgery-raises-safety-concerns">
            Da Vinci system
          </Link>{" "}
          has been used in a variety of surgical procedures including prostates,
          gallbladders, uterus, heart and organ transplants.
        </p>
        <h2>How Robotic Surgery Works</h2>
        <p>
          Robot-assisted surgery is one where instruments and cameras are
          inserted through small incisions. The surgeon sits at a console next
          to the patient. He or she then looks into a viewfinder at the
          three-dimensional image sent back by the cameras and works the
          surgical arms using hand and foot controls.
        </p>
        <p>
          The quality of the images and the precise movement of the robotic arms
          essentially put the surgeon right next to the area in which he or she
          is operating. It is important to note that this surgery is not
          actually performed by a robot. It is performed by a surgeon who is
          assisted by a robot.
        </p>
        <h2>FDA Investigation</h2>
        <p>
          The FDA is now looking into a spike in reported problems relating to
          robotic surgery after receiving an increased number of adverse
          incident reports last year, including five deaths. According to the
          manufacturer of Da Vinci, 367,000 robot-assisted surgeries were
          performed in 2012 compared to 114,000 in 2008.
        </p>
        <p>
          There were about 500 reports of problems related to robotic surgery
          just last year, according to the FDA's database. This includes cases
          where a woman died during a hysterectomy after a surgeon-controlled
          robot sliced a blood vessel. Another man died after his colon was
          perforated during prostate surgery.
        </p>
        <p>
          There were at least two other incident reports where the robotic arms
          malfunctioned during surgery. The FDA is still looking into what is
          causing these problems during surgery. Intuitive, the company that
          makes the robots, says there has been an increase in incidents because
          of a change it made last year in the way it reported these incidents.
        </p>
        <p>
          But safety advocates are skeptical about that assertion. Some experts
          have argued that incidents relating to robotic surgery have been
          underreported. Many experts are also concerned that the surgeons are
          not receiving sufficient training about how to use these robots.
        </p>
        <h2>Patients Need More Information</h2>
        <p>
          The incidents involving robotic surgery are downright scary. There
          have not been sufficient studies into what is causing these
          malfunctions. Have the robots been defectively designed?
        </p>
        <p>
          Did the robots that malfunctioned have defective parts on them? How
          much time did the manufacturer spend testing these robots? What is the
          training that surgeons receive before they actually start using them?
          There are still these and many other unanswered questions when it
          comes to robotic surgery.
        </p>
        <p>
          In such a situation, patients must get the facts - not marketing
          material. They need to speak with their doctors about the dangers and
          potential risks of robotic surgery.
        </p>
        <p>
          It is wrong on the part of the manufacturer as well hospitals to lead
          patients into believing that robotic surgeries are fail-proof, much
          safer and a better alternative than regular surgery.
        </p>
        <p>
          The jury is out on these issues. Until then, it is important that
          anyone who is about to undergo surgery talks to his or her doctor. It
          is crucial to understand the pros and cons of this procedure so that
          you can make an informed decision regarding your course of treatment.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
