// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Johnson & Johnson Stops Selling Vaginal Mesh Implants"
        pageDescription="Vaginal mesh lawsuits stop Johnson & Johnson from continuing distribution. Call 949-203-3814 to obtain a free vaginal mesh consultation with experienced defective product attorneys.."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Johnson & Johnson Stops Selling Vaginal Mesh Implants Following
          Multiple Product Liability Lawsuits - Attorneys Discuss Potential
          Danger Facing Victims
        </h1>
        <BreadCrumbs location={location} />

        <p>
          Johnson & Johnson is no longer selling a number of their vaginal mesh
          implant products. Ethicon, the subsidiary of Johnson & Johnson
          responsible for production of its vaginal mesh devices, recently spoke
          with a federal judge about divulging plans to discontinue the sale of
          TVT Secur, Prosima, Prolift, and Prolift+ M vaginal mesh brands.
        </p>
        <p>
          Although Ethicon claims that the decision was based on "changing
          market dynamics" and "is not related to safety or efficacy," these
          same models have been involved with the hundreds of lawsuits
          pertaining to their use. Women across the nation are pursuing
          compensation from Johnson & Johnson, claiming to have suffered serious
          injuries as a result of using their vaginal mesh devices.
        </p>
        <p>
          Designed to treat women suffering from pelvic organ prolapse and
          urinary incontinence, the vaginal mesh implant devices have been
          linked to high failure rates and serious health complications. The
          Food and Drug Administration issued a warning to patients utilizing
          the mesh in July of 2011, announcing that treatment utilizing mesh
          devices may be less effective and pose a greater risk than previously
          expected.
        </p>
        <p>
          In January of 2012, the FDA mandated more extensive tests for
          transvaginal mesh manufacturers to conduct. The FDA is currently
          considering a reclassification for these devices to signify greater
          risk amongst patients who use them.
        </p>
        <p>
          Travis Siegel, Bisnar Chase{" "}
          <Link to="/medical-devices/bard-mesh-recalls">
            vaginal mesh attorney
          </Link>
          , is currently assisting dozens of vaginal mesh victims. "The
          discontinuation of these products has the potential to protect
          thousands of women from suffering life-threatening injuries. Victims
          of these devices undergo a tremendous amount of pain and
          embarrassment."
        </p>
        <p>
          "One of our goals is to help prevent future injuries from products
          that are deemed defective. If Johnson & Johnson decided to stop
          selling mesh products because they do not want to pay out large
          verdicts and settlements to those who have suffered injuries, then, we
          know we are doing our job," says Mr. Siegel.
        </p>
        <p>About Bisnar Chase Vaginal Mesh Lawyers</p>
        <p>
          Bisnar Chase vaginal mesh attorneys represent people who have suffered
          injuries as a result of using defective products. Bisnar Chase has won
          thousands of cases against big business, insurance companies, and
          governmental agencies. They have obtained several multimillion dollar
          verdicts and settlements, including a 24.7 million dollar verdict in
          August of 2011.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
