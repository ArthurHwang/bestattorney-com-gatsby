// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Medtronic Infuse Bone Graft Questioned As Allegations Unfold"
        pageDescription="Infuse Graft medical device information. Call 949-203-3814 to speak with experienced medtronic infuse bone graft lawyers about a possible defective product case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Medtronic Infuse Bone Graft Questioned</h1>
        <BreadCrumbs location={location} />

        <p>
          According to a public health notification released by the FDA in July
          of 2008, Infuse bone graft products were used in procedures that the
          FDA did not grant approval for use with this product.
        </p>
        <p>
          Senator Max Baucus has personally written a letter to Medtronic
          stating that "These reports that doctors conducting medical trials
          while on Medtronic's payroll may have hidden serious side effects for
          patients are deeply troubling".
        </p>
        <p>
          John Bisnar, managing partner of the Bisnar Chase law firm, alleges
          that patients who have suffered injuries while utilizing the Infuse
          Bone Graft devices may be eligible for compensation.
        </p>
        <p>
          According to Medtronic's numbers, 500,000 patients have been treated
          with the Infuse Bone Graft product. The product was designed to assist
          in the treatment of degenerative disc disease (DDD).
        </p>
        <p>
          Infuse (recombinant human Bone Morphogenetic Protein-2, rhBMP) is a
          genetically engineered protein that would normally be found naturally
          in one's body. Medtronic claims that if it is used properly, one can
          stimulate bone formation and avoid additional surgeries during
          treatment.
        </p>
        <p>
          The FDA's 2008 public health notification stated that reports had been
          received associating life-threatening complications with rhBMP when
          used in the cervical spine. The notification went on to say that "the
          safety and effectiveness of rhBMP in the cervical spine have not been
          demonstrated and these products are not approved by the FDA for this
          use."
        </p>
        <p>
          The numbers released by the FDA in their public health notification
          included 38 reports detailing complications when using{" "}
          <Link to="/defective-products">Infuse Bone Graft </Link> products in
          cervical spine fusion. Similar complications were reported including
          swelling of the throat tissue and compression of the airway. Some
          patients had difficulty speaking, swallowing, and breathing.
        </p>
        <p>
          According to the Journal Sentinel's June 27th Watchdog Report, a
          number of complications were associated with use of the Infuse Bone
          Graft product amongst human trial participants between 2000 and 2009.
          The report alleges that the Medtronic sponsored trials were conducted
          by physicians that failed to report the severity of the side effects
          sustained by the participants.
        </p>
        <p>
          The doctors associated with the Medtronic Infuse Bone Graft clinical
          trials stated that "Although not desirable, bone formation in the
          spinal canal does not appear to have a discernible effect on patient
          outcomes." "Even though bone formed, no negative outcomes were found,"
          according to the Journal Sentinel.
        </p>
        <p>
          Reports to the FDA, cited in and leading to their public health
          notification, produced evidence to the contrary.
        </p>
        <p>
          Brian Chase,{" "}
          <Link to="/medical-devices/medtronic-infuse-bone-graft">
            Infuse Bone Graft attorney
          </Link>
          , has been assisting defective product victims for decades. Mr. Chase
          has observed first-hand the devastation that product defect victims
          undergo following the negligence of large corporations.
        </p>
        <p>
          "Too often, large companies produce products without their consumer's
          safety in mind. Product defect victims do not want to believe that
          businesses may be operated by people who place the priority of their
          profit margins above their consumer's level of safety."
        </p>
        <p>
          "When manufacturers fail to provide a reasonable level of safety for
          those who use their products, victims have a right to pursue
          compensation for their medical expenses, as well as any pain and
          suffering that may have been caused", says Brian Chase.
        </p>
        <h2>
          About: Bisnar Chase, Infuse Bone Graft Lawyers, Orange County, CA
        </h2>
        <p>
          Bisnar Chase is an Orange County, California based defective product
          law firm that has developed a reputation as trusted professionals
          delivering outstanding results. Brian Chase, senior partner, has
          obtained several multimillion dollar verdicts and settlements on
          behalf of his product defect clients, including a 24.7 million dollar
          verdict in August of 2011.
        </p>
        <p>
          Bisnar Chase attorneys assist product defect victims throughout the
          United States.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
