// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Vaginal Mesh Press Releases- Pelvic Mesh Attorneys - California Law Firm"
        pageDescription="Vaginal mesh press releases. If you have been injured as a result of a vaginal mesh product, call 949-203-3814 to speak with a Bisnar Chase attorney and receive a free no obligation case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Vaginal Mesh Press Releases</h1>
        <BreadCrumbs location={location} />

        <ul>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-no-win-no-fee">
              Bisnar Chase Vaginal Mesh Lawyers Offer No Win No Fee Guarantee
              For Victims Throughout the United States{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-lawsuit-verdict">
              Pelvic Mesh Lawsuit Verdict Awards Victim with More than $5
              Million{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/johnson-johnson-vaginal-mesh-discontinued">
              Johnson & Johnson Stops Selling Vaginal Mesh Implants Following
              Multiple Product Liability Lawsuits{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-lawsuits">
              California Attorneys Move Forward With Vaginal Mesh{" "}
            </Link>
          </li>
        </ul>

        <h2>More Releases</h2>

        <ul>
          <li>
            {" "}
            <Link to="/press-releases/client-stories">All </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/community-outreach">
              Community Outreach{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/defective-products">
              Defective Products{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/automakers-liability">
              Auto Accident{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/motorcycle-accidents">Motorcycle Accident </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/firm-accomplishments">
              Accomplishments{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/drunk-driving-crashes">
              Drunk Driving{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/medical-devices/vaginal-mesh-press-releases">
              Vaginal Mesh{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/press-releases/defective-products">Auto Defect </Link>
          </li>
          <li>
            {" "}
            <Link to="/press-releases">Miscellaneous </Link>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
