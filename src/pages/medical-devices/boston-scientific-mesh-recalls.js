// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Boston Scientific Recall - Mesh Injuries"
        pageDescription="Boston scientific recall: Has a Boston Scientific mesh procedure caused you injuries? Some patients develop painful symptoms and side effects. Contact us for help 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Boston Scientific Mesh Recall Overview</h1>
        <BreadCrumbs location={location} />

        <p align="center">
          <strong>
            {" "}
            For Immediate Assistance To Discuss Your Case Call 949-203-3814
          </strong>
        </p>
        <p>
          <img
            src="/images/medical/boston-scientific.png"
            alt="Boston Scientific Mesh Recall"
            className="imgright-fluid"
          />
          Age, childbearing, and other medical conditions can lead to a
          condition experienced by thousands of women known as pelvic organ
          prolapse, or POP.
        </p>
        <p>
          POP occurs when pelvic organs such as the bladder, small intestine,
          and rectum move from their normal position to "prolapsed" into the
          pelvic cavity, placing pressure on the vagina and other organs.
        </p>
        <p>
          This can lead to discomfort and other complications. Many women
          experiencing POP also experience another condition known as SUI, or
          stress urinary incontinence.
        </p>
        <p>
          This occurs when pressure on the bladder causes a woman to lose
          control of her urinary flow, especially when additional stress is
          placed on the bladder, as when sneezing or coughing. These conditions,
          while not life-threatening, are often very troublesome and reduce
          quality of life for many women.
        </p>
        <p>
          In an attempt to treat POP and SUI, many manufacturers, including
          Boston Scientific, have offered a surgical solution known as
          transvaginal mesh treatment, or TVT. In a TVT procedure, a small piece
          of mesh is surgically placed to hold the pelvic organs in place,
          resulting in a cessation of the symptoms of POP and SUI. For many
          women, TVT seemed an answer to prayer, and thousands of these
          procedures were performed after the introduction of this type of
          treatment.
        </p>
        <p>
          However, not long after TVT became a widely-used procedure, side
          effects emerged which cast doubt on the wisdom of using this method of
          treatment. Some of these side effects were very serious, such as
          vaginal mesh absorption or erosion through vaginal tissue into the
          bladder or rectum, and bone and hip infections; other side effects
          included inflammation, bowel perforation and bleeding, painful sexual
          intercourse, and recurrence of POP and SUI symptoms.
        </p>
        <p>
          Due to these complications, the Food and Drug Administration issued a
          warning in October, 2008, which warned doctors and patients about more
          than 1,000 reports of adverse reactions currently on file. In 2011,
          the FDA added to its report by stating that complications from TVT
          procedures were very high in number, and that other procedures could
          provide equal relief from POP and SUI symptoms.
        </p>
        <p>
          In spite of these warnings, neither Boston Scientific nor any other{" "}
          <Link to="/medical-devices/vaginal-mesh-lawsuit-verdict">
            transvaginal mesh
          </Link>{" "}
          producers have chosen to remove their products from the market, and
          doctors are still using these appliances in the treatment of POP and
          SUI. Thousands of women have suffered complications from these
          procedures ranging from minor to serious.
        </p>
        <p>
          If you are one of those women, it is time to take control of your
          situation by contacting a personal injury attorney to discuss your
          case. Transvaginal mesh lawsuits have already been filed by several
          plaintiffs, both of the className-action and individual type.
        </p>
        <p>
          A professional personal injury lawyer can assess your case and
          determine the best way for you to file for injuries experienced due to
          this procedure. You must file in a timely manner in order to preserve
          your right to collect damages. Talk to an expert personal injury
          attorney today; delay may cost you money which is rightfully yours due
          to your medical complications from transvaginal mesh treatment.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
