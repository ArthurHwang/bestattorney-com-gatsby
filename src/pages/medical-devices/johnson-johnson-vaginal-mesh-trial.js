// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Johnson & Johnson Are Faced with Their First Vaginal Mesh Trial in New Jersey"
        pageDescription="A South Dakota woman alleges the device caused her constant pain and has had 18 subsequent surgeries to fix the problem."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Johnson & Johnson Are Faced with Their First Vaginal Mesh Trial in New
          Jersey
        </h1>
        <BreadCrumbs location={location} />

        <h2>
          A South Dakota woman alleges the device caused her constant pain and
          has had 18 subsequent surgeries to fix the problem.
        </h2>
        <p>
          <img
            src="/images/johnson-and-johnson-vaginal-mesh-lawsuit.jpg"
            alt="Johnson & Johnson Are Faced with Their First Vaginal Mesh Trial in New Jersey"
            className="imgright-fixed"
          />
          Johnson & Johnson will face its first very first trial over its
          vaginal mesh implant devices in New Jersey involving a lawsuit (Number
          Atl-L-6966-10) filed by a 47-year-old South Dakota woman who blames
          the implant for chronic pain and 18 operations she had to undergo to
          correct the issue. According to a Jan. 7 Bloomberg news report,
          several manufacturers of these transvaginal mesh implant devices are
          facing lawsuits in New Jersey and other states, which allege that the
          products caused a range of side effects from organ perforation and
          pain to scarring and even nerve damage.
        </p>
        {/* <h2 align="center">Vaginal Mesh Recall Video</h2>


        <LazyLoad offset={ 200 } height={ 500 }>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/cRc1O9BexvI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={ true }
          />
        </LazyLoad> */}
        <p>
          The woman's lawsuit (Case Number Atl-L-6966-10), which was filed in
          the Superior Court of Atlantic County, states that she was no longer
          able to sit comfortably for more than a few minutes without having to
          stand up or lie down to reduce the level of pain. In addition, the
          suit stated that the woman needed a number of pain medications daily
          to manage her chronic pain, ruining her quality of life and rendering
          her unable to do her job as a nurse. The lawsuit alleges that Johnson
          & Johnson defectively designed these products and failed to warn
          consumers regarding the dangers posed by these medical devices.
        </p>
        <p>
          'It is shocking and saddening to see the types of severe side effects
          women face as a result of receiving these mesh implants', said John
          Bisnar, founder of the Bisnar Chase personal injury law firm. "Our
          firm is representing a number of women who are experiencing
          excruciating pain and potentially lifelong health complications as a
          result of receiving these transvaginal mesh implants. Instead of
          helping treat conditions such as pelvic organ prolapse and stress
          urinary incontinence, these products seem to be creating much more
          serious complications for these women."
        </p>
        <p>
          "The goal of these product liability lawsuits against mesh implant
          manufacturers such as Johnson & Johnson and C.R. Bard is not only to
          help victims and their families secure fair compensation for their
          losses, but also ensure that medical devices are properly tested
          before they are put in the market for consumer use". "It is the
          responsibility of manufacturers to make products that are safe for
          consumers. Manufacturers are also required to warn consumers about the
          potential effects of their products so they can make an informed
          decision regarding whether to use those products."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The{" "}
          <Link to="/medical-devices/bard-mesh-recalls">
            California vaginal mesh implant attorneys
          </Link>{" "}
          of Bisnar Chase represent victims of auto accidents, defective
          products, dangerous roadways, and many other serious personal
          injuries. The firm has been featured on a number of popular media
          outlets including Newsweek, Fox, NBC, and ABC and is known for its
          passionate pursuit of results for their clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or visit / for a free
          consultation.
        </p>
        <p>
          Sources:
          http://www.bloomberg.com/news/2013-01-07/j-j-faces-first-vaginal-mesh-implant-trial-in-new-jersey.html
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
