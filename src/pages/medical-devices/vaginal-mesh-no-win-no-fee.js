// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Vaginal Mesh Lawyers Wins Compensation for Victims"
        pageDescription="Were you injured due to vaginal mesh complications? Call 949-203-3814 for top-rated vaginal mesh attorneys who can help. Free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bisnar Chase Vaginal Mesh Lawyers Offer No Win No Fee </h1>
        <BreadCrumbs location={location} />

        <p>
          Bisnar Chase defective product lawyers have developed a reputation as
          being trusted professional who deliver proven results. Now they are
          putting their money where their mouth is. With over 30 staff members
          and more than a hundred million dollars recovered on behalf of their
          clients, Bisnar Chase lawyers are confident that their representation
          will provide pelvic mesh victims with a win, or they won't charge them
          a dime.
        </p>
        <p>
          For years women have been claiming to have suffered injuries as a
          result of using one of a wide number of different vaginal mesh
          products. Bisnar Chase attorneys have received victim information
          inquiries from women using products produced by American Medical
          Systems (AMS), Boston Scientific, Bard, Johnson & Johnson (Ethicon)
          and a number of other manufacturers.
        </p>
        <p>
          The Food and Drug Administration issued a warning to women using
          surgical mesh patches in July of 2011. In their announcement, the FDA
          revealed that women who seek treatment for pelvic organ prolapse that
          utilize a surgical mesh may not experience the successful results that
          their devices were previously considered to produce. It also announced
          the potential for patients utilizing these devices to be subjected to
          greater risks.
        </p>
        <p>
          In December of 2011, the Gynecological Practice Committee made
          recommendations that doctors be more cautious in instances where
          implantation of transvaginal mesh patches could be utilized. Since
          their recommendation, the use of vaginal mesh implants has decreased.
        </p>
        <p>
          The FDA has since mandated mesh product manufacturers to conduct
          highly extensive tests for their devices. A reclassification of mesh
          products to a higher className, which are usually reserved for harmful
          medical devices, is also being considered by the FDA.
        </p>
        <p>
          Travis Siegel, Bisnar Chase{" "}
          <Link to="/">California personal injury attorney </Link>, is currently
          assisting a number of women, throughout the United States, claiming to
          have suffered injuries as a result of vaginal mesh products. "Women
          who have come to us in pursuit of compensation for vaginal mesh
          injuries are experiencing severe pain, traumatic circumstance, and
          potentially life-long health complications. The first vaginal mesh
          verdict recently awarded one woman with more than $5 million for the
          injuries she sustained as a result of C.R. Brand's product. We feel
          that our clients should receive compensation equal to the injuries
          they have suffered," says Mr. Siegel.
        </p>
        <h2>About Bisnar Chase Vaginal Mesh Attorneys </h2>
        <p>
          John Bisnar, managing partner of the Bisnar Chase law firm, has been
          assisting victims of catastrophic injuries since 1978. Brian Chase,
          Bisnar Chase partner, state trial lawyers association vice president,
          2004 Product Defect "Trial Lawyer of the Year" award recipient and the
          lead attorney in a $24.7 million verdict for a single defective
          product victim, relishes holding negligent manufacturers responsible
          for haphazardly made products.
        </p>
        <p>
          John Bisnar and Brian Chase have instilled their passion for superior
          client representation in all of their staff members and it has
          resulted a 97.5% success rate for their clients.
        </p>
        <p>
          For more information about vaginal mesh lawsuits, or a free
          professional case evaluation, please call 949-203-3814.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
