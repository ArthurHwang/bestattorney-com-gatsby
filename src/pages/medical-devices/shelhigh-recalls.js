// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Shelhigh - California Products Liability Attorneys - Orange County Defective Medical Device Lawyers"
        pageDescription="Shelhigh. Call 949-203-3814 for highest-rated personal injury attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          California Shelhigh Recall Lawyers - Orange County Defective Medical
          Device Attorneys
        </h1>
        <BreadCrumbs location={location} />

        {/* <p><strong> {" "}<Link to="/medical-devices/shelhigh-recall-articles">Click here </Link> for some important articles about the Shelhigh recall.</strong></p> */}
        <p>
          On May 2, 2007 the FDA issued a formal demand of Shelhigh, Inc. to
          recall all of its medical devices due to concern of contamination. On
          April 17, 2007 the FDA requested a voluntary recall by Shelhigh, Inc.,
          after a seizure operation by the U.S. Marshals, but the company
          refused. The FDA requested that the U.S. Marshals seize any devices
          and components from Shelhigh's Union, New Jersey facility. The seizure
          was prompted by the discovery of significant deficiencies in the
          company's manufacturing process.
        </p>
        <p>
          The FDA sent two warning letters prior to the official seizing of
          their property and even after requesting a voluntary recall, the
          company refused. If they are to refuse the most recent order for
          recall, the FDA can seek a court order mandating the recall.
        </p>
        <p>
          It is unfortunate that it takes so much convincing for a company to
          recall its products, when they have been proven to be unsafe. For a
          company that manufacturers medical devices which are placed in sick
          and injured patients' hearts, it is highly alarming that they would
          put up such a fight after learning their products were doing more harm
          than good.
        </p>
        <p>
          In an earlier inspection from the FDA of Shelhigh's New Jersey plant,
          they found "a number of sterility test failures". They put out a
          warning to doctors informing them that the way the devices were
          manufactured may have "contaminated" the devices with "bacteria, fungi
          or endotoxin."
        </p>
        <p>
          The marketing director of Shelhigh, Inc., Douglas Goldman, stated that
          the FDA had no proof or data to justify the demand and therefore "no
          recall is in effect".
        </p>
        <p>
          The following is a list of products manufactured and distributed by
          Shelhigh, Inc. These products are used for open heart surgery in
          adults, children and infants, and to repair soft tissue during
          neurosurgery and abdominal, pelvic and thoracic surgery.
        </p>
        <ul>
          <li>Shelhigh BioRingT (annuloplasty ring)</li>
          <li>Shelhigh GoldT perforated patches</li>
          <li>Shelhigh Internal Mammary Artery</li>
          <li>Shelhigh No-React&reg; Dura Shield</li>
          <li>Shelhigh No-React&reg; EnCuff Patch</li>
          <li>Shelhigh No-React&reg; Pericardial Patch</li>
          <li>Shelhigh No-React&reg; PneumoPledgets</li>
          <li>Shelhigh No-React&reg; VascuPatch</li>
          <li>Shelhigh No-React&reg; Stentless Valve Conduit</li>
          <li>Shelhigh No-React&reg; Tissue Repair Patch/UroPatchT</li>
          <li>Shelhigh Pericardial Patch</li>
          <li>Shelhigh Pre Curved Aortic Patch (Open)</li>
          <li>Shelhigh Pulmonic Valve Conduit No-React&reg; Treated</li>
          <li>Shelhigh BioConduitT stentless valve</li>
          <li>Shelhigh BioMitralT tricuspid valve</li>
          <li>Shelhigh Injectable Pulmonic Valve System</li>
          <li>Shelhigh MitroFast &reg; Mitral Valve Repair System</li>
          <li>Shelhigh NR2000 SemiStentedT aortic tricuspid valve</li>
          <li>Shelhigh NR900A tricuspid valve</li>
          <li>Endura No-React Dural Substitute*</li>
        </ul>
        <p>
          <em>
            *(Note: This is a private label device. Shelhigh, Inc. manufactured
            it. Integra Corp. marketed it.)
          </em>
        </p>
        <p>
          If you or anyone you know has been injured from the use of any of the
          above products, please call Bisnar Chase Personal Injury Attorneys for
          your free consultation. Our experienced and dedicated Orange County
          defective medical device lawyers have the determination and resources
          necessary to help you. As an injured patient it is your responsibility
          to hold the negligent company responsible for your injuries.
        </p>
        <p>
          {" "}
          <Link
            to="http://www.fda.gov/bbs/topics/NEWS/2007/NEW01625.html"
            target="_blank"
          >
            {" "}
            Click Here
          </Link>{" "}
          for the official FDA Recall.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
