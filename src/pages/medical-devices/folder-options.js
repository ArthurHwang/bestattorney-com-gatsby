/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "medical-defect",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "Defective Medical Product Attorneys",
    HTML: `<ul className='bpoint'>
              <li>If you or a loved one have suffered as a result of a medical device defect, know your rights and <a href='/contact'>contact an attorney </a> today.</li>
            </ul>`
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Medical Defect Injury Information",
    links: [
      {
        linkName: "Pharmaceutical Litigation",
        linkURL: "/pharmaceutical-litigation"
      },
      {
        linkName: "Bard Mesh Recalls",
        linkURL: "/medical-devices/bard-mesh-recalls"
      },
      {
        linkName: "Birth Control Recalls",
        linkURL: "/medical-devices/birth-control-recalls"
      },
      {
        linkName: "Boston Scientific Mesh Recalls",
        linkURL: "/medical-devices/boston-scientific-mesh-recalls"
      },
      {
        linkName: "Depuy Hip Replacements",
        linkURL: "/medical-devices/depuy-hip-replacements"
      },
      {
        linkName: "Hip Implants",
        linkURL: "/medical-devices/hip-implants"
      },
      {
        linkName: "Stryker Recalls",
        linkURL: "/medical-devices/stryker-recalls"
      },
      {
        linkName: "Transvaginal Mesh Victims",
        linkURL: "/medical-devices/transvaginal-mesh-victims"
      },
      {
        linkName: "Zimmer Nexgen Knee Replacements",
        linkURL: "/medical-devices/zimmer-nexgen-knee-replacements"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "Stomach Flu vs. Food Poisoning Medical Course",
      videoUrl: "EC7UaLIAEP4"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
