// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Infuse Bone Graft Side Effects - California Personal Injury Attorney"
        pageDescription="Searching for a medical procedure side effect attorney? Call 949-203-3814 for qualified attorneys. Free consultations. Decades of professional representation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Infuse Bone Graft Side Effect Attorneys</h1>
        <BreadCrumbs location={location} />

        <h2>Rights of Injured Patients</h2>
        <p>
          When bones are badly damaged in an accident or through illness,
          doctors can sometimes repair the damage through the use of bone
          grafts.  There are several types of bone graft procedures available to
          doctors, but one of the most popular in the last few years has been
          the use of something called a morphogenic protein.  Morphogenic comes
          from a combination of two words, <em>morph</em> which means change,
          and <em>gen</em> which means growth.  A morphogenic protein works by
          causing the bone to grow more quickly to the graft, speeding recovery
          time and increasing the likelihood that the procedure will be
          successful.  Bone graft morphogenic proteins are combined with a
          sponge-like material that gives the new bone an anchor on which to
          grow; essentially, a bone graft enhanced by a morphogenic protein is
          attempting to &ldquo;beat the clock&rdquo; by getting bone growth
          started much more quickly, theoretically giving the patient a better
          chance of successful growth and faster healing.
        </p>
        <p>
          However, the protein marketed by Medtronic under the brand name Infuse
          has been shown to have a high rate of failure and can lead to serious
          complications for bone graft patients.  Several male patients have
          experienced infertility after undergoing Infuse procedures, and others
          have experienced inflammatory reactions of soft tissue and cyst
          development.  In some cases, these side effects have led to breathing
          complications that can be life-threatening.  In other cases, bone
          grafts were unsuccessful or the patients had to undergo additional
          surgery in order to correct problems with the new bone growth.
        </p>
        <p>
          If you have undergone an Infuse bone graft and you have experienced
          any side effects, it is important that you protect your rights and
          alert both the medical community and the public of the risks involved
          in this procedure.  However, it is unlikely that you can do this
          alone; you need the help of experts who understand the problems
          associated with the use of bone graft proteins and who know how to
          recover damages for your pain, suffering, and emotional trauma.
        </p>
        <p>
          Infuse bone graft attorneys work with victims who have suffered from
          these bone graft procedures.  You may not be sure if you are a victim;
          perhaps you are not even sure if your doctor used an Infuse
          morphogenic protein in your procedure.  In order to find out, consult
          a bone graft attorney who can review your medical records and give you
          sound legal advice about the possibilities of recovering damages for
          complications with your bone graft surgery.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
