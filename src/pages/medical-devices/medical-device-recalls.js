// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Recent Medical Device Recalls - Reported by FDA"
        pageDescription="Bisnar Chase is a defective medical devices attorney serving California in defective Hip, Knee, and Vaginal Mesh cases."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Recent Medical Device Recalls</h1>
        <BreadCrumbs location={location} />

        <p>
          Please visit{" "}
          <Link
            target="_blank"
            to="https://www.fda.gov/medical-devices/medical-device-safety/medical-device-recalls"
          >
            the FDA's website
          </Link>{" "}
          for more information.
        </p>

        {/* <p>The following are some of the most recent recalls for medical devices announced by the FDA. More information can be found on the {" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/default.htm">FDA's website. </Link></p> */}

        {/* <table className="table table-hover">
          <thead>
            <tr>
              <td><strong> Defective Medical Device</strong></td>
              <td><strong> More Information</strong></td>
              <td><strong> Date of Recall</strong></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>B. Braun Medical Inc. Recalls Dialog+ Hemodialysis Systems Due Defective Conductivity Sensors</td>
              <td>{" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm499127.htm">Link </Link></td>
              <td>05/04/16</td>
            </tr>
            <tr>
              <td>Boston Scientific Corporation Recalls Fetch 2 Aspiration Catheter Due to Shaft Breakage</td>
              <td>{" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm496701.htm">Link </Link></td>
              <td>04/19/16</td>
            </tr>
            <tr>
              <td>Vascular Solutions Recalls Guardian II Hemostasis Valve Due to Low Pressure Seal Defect</td>
              <td>{" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm495913.htm">Link </Link></td>
              <td>04/13/16</td>
            </tr>
            <tr>
              <td>Focus Diagnostics Recalls Laboratory Examination Kits Due to Inaccurate Test Results</td>
              <td>{" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm495875.htm">Link </Link></td>
              <td>04/13/16</td>
            </tr>
            <tr>
              <td>Dexcom Inc. Recalls G4 Platinum and G5 Mobile Continuous Glucose Monitoring System Receivers Due to Audible Alarm Failure</td>
              <td>{" "}<Link to="http://www.fda.gov/MedicalDevices/Safety/ListofRecalls/ucm493167.htm">Link </Link></td>
              <td>04/11/16</td>
            </tr>
          </tbody>
        </table> */}

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
