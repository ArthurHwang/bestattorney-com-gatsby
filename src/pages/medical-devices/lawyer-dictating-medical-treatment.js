// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lawyer Should Not Be Dictating Medical Treatment"
        pageDescription="Should I Let My Lawyer Dictate My Medical Treatment? Call 949-203-3814 for highest-rated accident injury lawyers, serving California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Should I Let My Lawyer Dictate My Medical Treatment?</h1>
        <BreadCrumbs location={location} />

        <p>
          The job of a{" "}
          <Link to=" /car-accidents">California car accident lawyer </Link> is
          to counsel you about your legal rights. It is not their job to pick a
          doctor or a medical treatment for you. That is your doctor's job.
          Sometimes your personal injury lawyer might not have your best
          interest at heart and may send you to the wrong direction. Often
          doctors and attorneys have agreements where they recommend their
          clients to each other. However, doctors and lawyers take advantage of
          these alliances at your cost.
        </p>
        <h2>Some attorneys and doctors may have ulterior motives</h2>
        <p>
          Attorneys and doctors usually stay within their fields of practice.
          But there are some attorneys and doctors that have arrangements in
          which the doctor refers his/her clients to the attorney and vice versa
          even if it may not be in the best interest of the client. It is your
          right and responsibility to choose the right medical treatment for you
          and your family.
        </p>
        <p>
          {" "}
          If you think you need to see a specialist, then you should consult
          with your family doctor not your <strong> car accident lawyer</strong>
          . After the lawyer refers you to the doctor of his/her choosing, the
          doctor may over-treat or over-charge you for medical treatments that
          are never administered. This is followed by the lawyer in turn
          ignoring the over-billing on the doctor's part. This is a very rare
          occurrence but if your accident injury is pushing you towards a
          certain doctor, then it may serve you well to be extremely cautious.
        </p>
        <p>
          {" "}
          Among other things, my book{" "}
          <Link to="/resources/book-order-form">
            "The Seven Mistakes That Can Wreck Your Personal Injury Claim"
          </Link>{" "}
          warns of the dangers of letting your attorney direct your medical
          treatment.
        </p>
        <h2>
          Sometimes it is acceptable for attorneys to recommend a doctor but it
          is still demonstrates a conflict of interest
        </h2>
        <p>
          The only time that it is acceptable for your California personal
          injury lawyer to provide any direction in terms of your medical
          treatment is if you ask them to do so. Even still, I do not direct my
          clients to doctors because I think that the best person to guide your
          medical treatment is your family physician.{" "}
        </p>
        <p>
          I firmly believe that a lawyer must never take a position that could
          have adverse effects on the people he/she represents. Your personal
          injury lawyer is the one person who is supposed to have your best
          interest at heart while settling a claim. If your{" "}
          <strong> accident injury attorney</strong> has such an arrangement
          with a doctor or recommends you to a specific doctor then that means
          the lawyer is not completely committed to you from the beginning even
          if there aren't any other improprieties being carried out.{" "}
        </p>
        <p>
          So, If you lawyer is having a conflict of interest from the start then
          it would be wise to consult with a different car accident lawyer or at
          the very least be extremely cautious while proceeding with that
          particular attorney.
        </p>
        <p>
          If you would like to know what is the best way to choose a lawyer that
          best fits your needs, feel free to visit our{" "}
          <Link to="/resources/hiring-an-injury-lawyer">
            "How To Choose A Lawyer"
          </Link>{" "}
          page.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
