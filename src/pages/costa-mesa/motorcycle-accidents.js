// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Motorcycle Accident Lawyers - Bisnar Chase"
        pageDescription="If you have been injured in a Costa Mesa motorcycle accident, call 949-203-3814 to speak with the best Costa Mesa motorcycle accident lawyers free of charge. Helping bikers since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Motorcycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <h3>
          For immediate help with a motorcycle accident claim, call 949-203-3814
        </h3>
        <p>
          While motorcycling in Costa Mesa can be thrilling and fulfilling, it
          comes with its own set of hazards.
        </p>
        <p>
          There are many reasons why Costa Mesa motorcycle accidents occur.
          Negligent drivers, defective motorcycle, dangerous roadways, alcohol,
          speed, distracted driving, failure to follow the rules of the road are
          just some of the reasons why motorcycle accidents commonly occur in
          Costa Mesa.
        </p>
        <p>
          The experienced{" "}
          <Link to="/costa-mesa">Costa Mesa personal injury lawyers</Link> at
          Bisnar Chase have fought long and hard for the rights of motorcyclists
          and their families, so if you have been injured, contact us now.
        </p>
        <p>
          We are a reputed Orange County personal injury law firm with an
          excellent track record and frequently handle serious injuries for
          bikers.
        </p>
        <p>
          We will remain on your side, fight for your rights and make sure that
          the negligent parties are held accountable. Please contact us to
          obtain more information about pursuing your legal rights.
        </p>
        <p>
          It is important that motorcyclists understand their rights and
          responsibilities in the event they are involved in an accident.
        </p>
        <h2>Determining Liability of a Motorcycle Accident</h2>
        <p>
          An important part of the claim process following an injury accident is
          determining who was responsible for the crash.
        </p>
        <p>Some of the questions that need to be asked include:</p>
        <ul>
          <li>
            Did a car's driver run through a red light or make a dangerous lane
            change?
          </li>
          <li>Did a drunk driver recklessly crash into the victim?</li>
          <li>
            Did the accident occur because a part on the motorcycle was
            defective and failed to work properly?
          </li>
          <li>
            Did the motorcyclist lose control of the bike and suffer injuries
            because he or she went over a pothole?
          </li>
        </ul>
        <p>
          Regardless of the situation, the savvy Costa Mesa motorcycle accident
          lawyers at Bisnar Chase can help educate you on your rights and help
          you take legal action to defend your rights as a motorist.
        </p>
        <h2>Potentially Dangerous Roadway Conditions in Costa Mesa</h2>
        <p>
          Some locations are simply dangerous for motorists. There are certain
          stretches of roadways, intersections and curves in Costa Mesa that are
          notorious for injury accidents.
        </p>
        <p>
          A skilled Costa Mesa motorcycle accident lawyer can review the history
          of a location to show that either the design or the upkeep of the
          location was directly responsible for the crash.
        </p>
        <p>
          Lack of proper roadway maintenance is a common cause of Costa Mesa
          motorcycle accidents.
        </p>
        <p>
          Motorcycles are more prone to veering out of control after striking
          gravel or a large pothole. In such cases, it must be determined if the
          governing body knew of the conditions and why they failed to fix it.
        </p>
        <p>
          If the city had a reasonable amount of time to fill the pothole, they
          may be held liable for any accidents that occur because of it.
        </p>
        <p>
          Other examples of motorcycle accidents involving government liability
          include dangerous conditions created by uneven pavement, overgrown
          foliage or debris on the roadway that was not promptly cleared.
        </p>
        <h2>Pursuing Compensation After a Motorcycle Accident</h2>
        <p>
          Motorcycle accident victims usually suffer major injuries such as head
          trauma, broken bones, road rash, spinal cord damage and internal
          injuries.
        </p>
        <p>
          Victims and their families face significant financial burdens in the
          form of medical expenses, lost earnings, cost of hospitalization and
          rehabilitative treatments such as physical therapy or chiropractic
          care.
        </p>
        <p>
          If the motorcyclist was injured by a negligent driver, compensation
          may be sought from the at-fault party.
        </p>
        <p>
          Motorcycle accidents may also result in catastrophic injuries such as
          paralysis, irreversible brain damage or limb amputations. In such
          cases compensation may also be sought for loss of livelihood.
        </p>
        <p>
          The Bisnar Chase Costa Mesa motorcycle accident lawyers can help you
          receive compensation for these types of accidents but only if you call
          us now!
        </p>
        <h2>Wrongful Death Claims in Costa Mesa</h2>
        <p>
          If a motorcycle accident results in fatalities, then the family
          members of the deceased victims can file what is known as a{" "}
          <Link
            to="https://www.nolo.com/legal-encyclopedia/wrongful-death-claims-overview-30141.html"
            target="_blank"
          >
            wrongful death claim
          </Link>{" "}
          against the negligent party.
        </p>
        <p>
          In such cases, damages may be sought for medical expenses, funeral
          costs, lost future income, loss of love and companionship and other
          related damages.
        </p>
        <p>
          Wrongful death claims are usually filed by immediate family members or
          those who are financially dependent on the deceased victim.
        </p>
        <h2>How to Take Legal Action After a Motorcycle Accident</h2>
        <p>
          Regardless of the claim you may have, the experienced Costa Mesa
          motorcycle injury attorneys at Bisnar Chase will help you get the
          compensation you deserve. Call us today at
          <strong>949-203-3814</strong> to speak to one of our attorneys free of
          charge. In addition, if we take your case on and if we lose, you will
          not have to pay a cent.
        </p>
        <p>
          With nothing to lose and so much to gain, why wait any longer? Call us
          now!
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
