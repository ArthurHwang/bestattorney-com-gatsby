// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Truck Accident Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 now for the most qualified Costa Mesa truck accident lawyers. Free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <h2>Compensation for Injured Victims</h2>
        <p>
          The experienced{" "}
          <Link to="/costa-mesa" target="_blank">
            Costa Mesa personal injury lawyers
          </Link>{" "}
          at Bisnar Chase have a long and successful track record of holding
          negligent truck drivers and trucking firms accountable.
        </p>
        <p>
          Tragedies caused by poorly maintained trucks are unnecessary and can
          be easily prevented when these trucking companies do what they are
          required to do under the law.
        </p>
        <p>
          We will examine the trucking company's maintenance logs to determine
          whether poor vehicle maintenance caused or contributed to your
          accident. It is also important that such critical evidence be
          preserved right away so it is not lost or destroyed.
        </p>
        <p>
          If you or a loved one has been injured in a Costa Mesa truck accident,
          please call us at <strong>949-203-3814</strong> for a free,
          comprehensive and confidential consultation.
        </p>
        <h2>What Causes Truck Accidents?</h2>
        <p>
          Equipment failure or malfunction is one of the most common causes of
          catastrophic truck accidents.
        </p>
        <p>
          Of all types of failure, brake failure is the most common in large
          trucks such as 18-wheelers or tractor-trailers.
        </p>
        <p>
          It is the responsibility of the truck driver and the trucking company
          to ensure that the brakes are properly maintained and are in good
          working condition before the rubber hits the road.
        </p>
        <p>
          When brake failure is caused by poor maintenance, the trucking company
          or the truck's owner can be held liable for injuries and damages
          caused as a result.
        </p>
        <p>
          Do not let the negligence of the trucking companies infringe on your
          right to compensation. If you have been injured in a truck accident,
          our savvy Costa Mesa train accident attorneys can help you take legal
          action so you can acquire the compensation you deserve.
        </p>
        <h2>
          Brake Failure Extremely Common Cause for Truck Accidents in Costa Mesa
        </h2>
        <p>
          According to the{" "}
          <Link to="https://www.iihs.org/" target="_blank">
            Insurance Institute of Highway Safety (IIHS)
          </Link>
          , tractor-trailers with defective equipment are twice as likely to be
          involved in crashes as trucks without defects.
        </p>
        <p>Brake defects were most common in these types of crashes.</p>
        <p>
          In fact, brake failure was an issue in 56 percent of tractor-trailers
          that were involved in crashes. Steering equipment defects were found
          in 21 percent of the trucks involved in crashes.
        </p>
        <p>
          The skilled Costa Mesa train accident lawyers at Bisnar Chase are
          proficient at reading between the lines and finding out if the
          trucking companies have been cutting corners and in turn affected your
          well being.
        </p>
        <h2>Truck Accident Statistics</h2>
        <p>
          A recent{" "}
          <Link to="https://ai.fmcsa.dot.gov/LTCCS/default.asp" target="_blank">
            Large Truck Crash Causation
          </Link>{" "}
          study reported that almost 55 percent of the trucks involved in
          crashes had at least one mechanical violation and almost 30 percent
          had at least one out-of-service condition.
        </p>
        <p>
          Of all equipment violations, the violations in the brake (36 percent)
          and lighting (19 percent) systems were most frequent. A truck with
          brakes that are in poor condition is 1.8 times more likely to be the
          vehicle that causes a crash. In rear-end and head-on collisions, brake
          violations significantly increase the likelihood that the truck is the
          striking vehicle.
        </p>
        <h2>Laws Relating to Truck Brakes in California</h2>
        <p>
          It is important to remember that stopping distances for trucks are
          much longer compared to other passenger vehicles.
        </p>
        <p>
          When roadways are slick, there are even greater disparities between
          the braking capabilities of large trucks and cars. These disparities
          can become even worse if the truck's braking systems are not properly
          maintained.
        </p>
        <p>
          New large trucks are required to have automatic brake adjusters,
          visible brake adjustment indicators and anti-lock brakes, which keep
          the wheels from locking during hard braking. They also help drivers to
          control the trucks better during emergency stops and reduce the
          probability of a jackknife accident. Federal law requires anti-lock
          brakes on new tractors as of March 1997.
        </p>
        <p>
          Also, in July 2009, the{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          issued a rule decreasing the maximum stopping distances for air-braked
          trucks by 30 percent. This regulation went into effect in August of
          2011 for three-axle tractors that weigh 59,600 pounds or less.
        </p>
        <h2>Trucking Company's Legal Obligations</h2>
        <p>
          Trucking companies or truck owners have a legal obligation to properly
          maintain their vehicles including servicing and repairing them when
          needed.
        </p>
        <p>
          Trucking companies are also required under the law to keep maintenance
          logs detailing all the maintenance work that was done on the truck by
          date and type of work done.
        </p>
        <p>
          If a mechanical malfunction or brake failure occurs as a result of
          poor vehicle maintenance, then, the truck driver and his or her
          employer can be held liable for the injuries, damages and losses
          caused by such an accident.
        </p>
        <h2>
          If You've Been Injured in a Truck Accident, Call Bisnar Chase Now!
        </h2>
        <p>
          Call us immediately and an experienced and reputable Costa Mesa Truck
          Accident lawyer from our team will discuss your rights and help you
          take legal action. For a free consultation, call us now at{" "}
          <strong>949-203-3814.</strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
