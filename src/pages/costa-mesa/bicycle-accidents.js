// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Bicycle Accident Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 now if you have been injured in a bicycle accident. Bisnar Chase are the best Costa Mesa bicycle lawyers around! Free consultations. No win, no fee lawyers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <p>
          If you've been hurt in a Costa Mesa bicycle accident please contact us
          now for a free consultation. The bicycle accident lawyers of Bisnar
          Chase have over <strong>35 years</strong> serving Orange County. We've
          recovered over<strong> $300M</strong> in verdicts and settlements and
          we can do the same for you!
        </p>
        <p>
          Call us at <strong>949-203-3814</strong> to speak with an experienced{" "}
          <Link to="/costa-mesa">Costa Mesa personal injury lawyer</Link> and
          for your free, no-obligation consultation. You may be entitled to
          compensation, including lost wages, medical bills and mental anguish.
          We will fight to hold the at-fault party responsible.
        </p>
        <h2>Bicycle Accidents in Costa Mesa</h2>
        <p>
          Costa Mesa is relatively bike-friendly, however, this is not to say
          that bicyclists have never been injured in Costa Mesa.
        </p>
        <p>
          The California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) reported recently, 82 people were injured Costa Mesa
          bicycle accidents.
        </p>
        <p>
          Many roadways have bike lanes that make it safer for bicyclists. There
          is a beautiful bike path along Victoria Street, for example, that is
          separate from the main road. There are also a number of great parks
          with dirt paths for bicyclists.
        </p>
        <p>
          For example, Fairview Park is a great location for bicyclists to get
          some exercise and simply to enjoy a good ride.
        </p>
        <p>
          In the unfortunate case that you have been injured in a bike accident,
          the skilled Costa Mesa bicycle accident attorneys at Bisnar Chase will
          help you take legal action against the at-fault party.
        </p>
        <h2>Liability for Bicycle Accidents in Costa Mesa</h2>
        <p>
          Depending on the circumstances of the crash, a number of parties may
          be held liable for the bicycle accident. For example, if the accident
          was caused by a negligent driver, the driver can be held liable.
        </p>
        <p>
          If a dangerous roadway or intersection caused the accident, then the
          city or governmental agency responsible for maintaining the roadway
          can also be held liable.
        </p>
        <p>
          If a defective bicycle or bike part caused the accident, the
          manufacturer can be held accountable and our savvy Costa Mesa bicycle
          accident lawyers can help you seek compensation.
        </p>
        <p>
          In such cases, injured bicyclists can seek compensation for damages
          including but not limited to medical costs, hospitalization, lost
          earnings, rehabilitation and pain and suffering.
        </p>
        <h2>Passionate Legal Help from Bisnar Chase</h2>
        <p>
          The experienced Costa Mesa bicycle accident attorneys at Bisnar Chase
          have a successful track record of dealing with a variety of bike
          accident cases. We have won cases against governmental agencies for
          designing and/or maintaining dangerous roadways.
        </p>
        <p>
          We have also financially pursued drunk drivers on behalf of injured
          victims and their families.
        </p>
        <p>
          If you or a loved one has been injured in a Costa Mesa bicycle
          accident, please contact us now for a free consultation and a
          comprehensive case evaluation. Our personal injury lawyers work on a
          contingency fee basis, which means that we do not get paid until our
          clients are compensation for their losses.
        </p>
        <p>
          Call <strong>949-203-2814 </strong>for your free consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
