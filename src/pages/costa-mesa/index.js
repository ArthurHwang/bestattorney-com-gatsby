// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Costa Mesa",
      population: 112174,
      totalAccidents: 6174,
      intersection1: "Adams Ave & Harbor Blvd",
      intersection1Accidents: 62,
      intersection1Injuries: 64,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 214.3,
      city1Name: "Newport Beach",
      city1Index: 143.6,
      city2Name: "Fountain Valley",
      city2Index: 153,
      city3Name: "Huntington Beach",
      city3Index: 184.5,
      city4Name: "Santa Ana",
      city4Index: 200.6
    },
    locationBox3: {
      intersection2: "Harbor Blvd & Wilson St",
      intersection2Accidents: 74,
      intersection2Injuries: 61,
      intersection2Deaths: 0,
      intersection3: "Newport Blvd & 19th St",
      intersection3Accidents: 86,
      intersection3Injuries: 56,
      intersection3Deaths: 1,
      intersection4: "Harbor Blvd & Gisler Ave",
      intersection4Accidents: 59,
      intersection4Injuries: 50,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 for highest-rated Costa Mesa Personal Injury Lawyers.  We specialize in car accidents, wrongful death, auto defects and more! Free legal advice and you won't be charged a cent if we lose!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <p>
          The Costa Mesa personal injury lawyers at Bisnar Chase understand the
          specific needs of individuals who have suffered serious and
          life-changing injuries as a result of a traumatic event such as an
          auto accident, pedestrian accident, dog attack or due to a dangerous
          or defective product.
        </p>
        <p>
          Since 1978, our injury lawyers have served more than
          <strong> 12,000 clients </strong>and have recovered over{" "}
          <strong>$500 Million</strong> dollars for them over the past 35 years.
        </p>
        <p>
          Our law firm has not only received top ratings from prestigious
          organizations, but we also have the trust and support of thousands of
          clients who we are proud to have served over the decades.
        </p>
        <p>
          In addition, we also have and maintain an A+ rating from the{" "}
          <Link
            to="https://www.bbb.org/sdoc/business-reviews/attorneys-and-lawyers/bisnar-chase-personal-injury-attorneys-in-newport-beach-ca-100046710"
            target="_blank"
          >
            Better Business Bureau
          </Link>
        </p>
        <p>
          If you have suffered a serious personal injury, call us at
          <strong> 949-203-3814</strong> to find out how we can help you.
        </p>

        <h2>What is Your Claim Worth?</h2>
        <p>
          This is a question we often get asked during an initial consultation.
        </p>
        <p>
          The value or worth of your claim will depend on the nature and extent
          of the injuries, damages and losses you may have sustained.
        </p>
        <p>
          For example, if the incident resulted only in property damage or in
          minor injuries such as scrapes or bruises that did not require you to
          seek medical treatment, then, your case may not be worth much.
        </p>
        <p>
          <strong>
            <em>However</em>
          </strong>
          , if you did suffer injuries serious enough to require
          hospitalization, sought rehabilitative care such as physical therapy,
          spent money out of pocket for treatment and lost valuable income
          because you couldn't get back to work, you may be eligible to receive
          much more in terms of compensation.
        </p>
        <p>
          <strong>
            Negligence is also a crucial element in a personal injury case
          </strong>
          , which might affect the amount of compensation you receive.
        </p>
        <p>
          For example, if you can prove that another person or entity was
          negligent and caused your injuries and resulting losses, the value of
          your claim could become higher. An experienced Costa Mesa personal
          injury attorney can examine all aspects of your case and help you
          gather the necessary evidence to prove your claim.
        </p>
        <h2>Damages in a Personal Injury Case</h2>
        <p>
          What types of damages can you seek and obtain in a personal injury
          claim? This could vary from case to case and the circumstances.
          However, here are some of the common damages that are awarded in
          California personal injury cases:
        </p>
        <ul type="disc">
          <li>
            <strong>Medical expenses:</strong> This includes emergency
            transportation costs, hospitalization, cost of surgery, medical
            equipment, medication and other types of treatment and care you may
            need to recover from your injuries.
          </li>
          <li>
            <strong>Rehabilitation costs:</strong> In addition to medical care,
            injured victims may need extensive rehabilitative care in order to
            completely recover from an injury and regain their strength and
            mobility. Often times, rehabilitative treatments such as physical
            therapy or chiropractic care are not covered by health insurance
            companies. In such cases, patients tend to pay for such necessary
            treatment out of pocket.
          </li>
          <li>
            <strong>Lost wages:</strong> When you are injured, you will need to
            take time off work. In some cases where individuals have suffered
            major injuries, they may even lose their jobs and be unable to
            return to work in the same capacity. If you have lost your
            livelihood as a result of your injuries, you may also be able to
            claim lost future income and benefits as part of your claim.
          </li>
          <li>
            <strong>Pain and suffering:</strong> This includes the physical pain
            you endure as the result of an injury as well as emotional trauma.
            Those who are severely injured, disfigured or scarred could, for
            example, suffer from depression. Dog attack victims for example
            suffer from anxiety or post traumatic stress disorder (PTSD).
          </li>
        </ul>
        <p>
          If any of these damages apply to you, we urge to call the Costa Mesa
          personal injury attorneys at Bisnar Chase so we can start helping you
          get the compensation that you deserve!
        </p>
        <h2>Superior Representation and Client Service</h2>
        <p>
          Our <Link to="/about-us">mission statement</Link> was written with the
          goal of setting a high standard when it comes to client representation
          and customer service.
        </p>
        <p>
          "
          <strong>
            To provide superior client representation in a compassionate and
            professional manner while making our world a safer place."
          </strong>
        </p>
        <p>
          We represent our clients in a manner that they get the highest quality
          of representation that is available in our industry.
        </p>
        <p>
          We are relentless when it comes to representing our clients, fighting
          hard for their rights and helping them secure the maximum possible
          compensation for their injury claim.
        </p>
        <p>
          While we are focused on helping our clients obtain fair compensation
          for their losses, we also care deeply about their well-being and about
          being sensitive to their specific situation.
        </p>
        <h2>Passion, Sensitivity and Track Record of Success</h2>
        <p>
          We make every effort to understand their needs be it the need for
          medical equipment, for transportation or making changes to their home
          to accommodate a disability.
        </p>
        <p>
          Our attorneys represent injured victims and their families with
          professionalism and dignity.
        </p>
        <p>
          Our eventual goal is, without question, to make the world a safer
          place for all of us.
        </p>
        <p>
          We do this by holding wrongdoers accountable, be it individuals,
          entities or large corporations. In doing so, we encourage the public
          to be more responsible, governments to properly carry out their duties
          and manufacturers to make safer products.
        </p>
        <p>
          What makes us unique is our{" "}
          <strong>passionate representation and pursuit of justice</strong> for
          injured clients and their families. You will see this passion from the
          time you make that initial phone call and schedule your{" "}
          <Link to="/contact">free consultation</Link>.{" "}
          <strong>
            We've won cases where others have said it couldn't be done.
          </strong>{" "}
          Our trial attorneys have had a successful track record in proving
          tough cases to juries. We go the extra mile for our clients, to
          collect every cent in your case that the law and the circumstances
          allow.
        </p>
        <h2>Bisnar Chase Delivers Results You Can Count On</h2>
        <p>
          Our Costa Mesa personal injury lawyers have been representing injured
          clients and their families since 1978.
        </p>
        <p>
          We don't hesitate to take on big cases against giant corporations that
          typically have high-priced legal defense teams at their beck and call.
          This is because we have the experience, expertise and the resources it
          takes to get results.
        </p>
        <p>
          We are one of the few firms in the nation that conducts independent
          crash testing on vehicles. We have collected more than $500 Million in
          settlements and verdicts for our clients. We have a{" "}
          <strong>96 percent success rate</strong> with our cases. We also offer
          a <strong>no-win, no-fee guarantee</strong> to our clients. This means
          you don't pay a dime unless we secure compensation for you.
        </p>
        <p>
          We understand how a serious personal injury can turn lives upside
          down. It affects not only the individual who has suffered the injury,
          but also his or her family members. It affects victims and families
          emotionally and financially. Our lawyers will fight for your legal
          rights every step of the way and help ensure that you receive fair and
          full compensation for all your losses. Call us at
          <strong> 949-203-3814 </strong>for a no-cost, no-obligation
          consultation and comprehensive case evaluation.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
