// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Car Accident Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 now if you want the highest-rated Costa Mesa car accident lawyers fighting for you! We have over 35 years of experience and have won back millions of dollars in compensation for our clients."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          The experienced{" "}
          <Link to="/costa-mesa">Costa Mesa personal injury lawyers</Link> at
          Bisnar Chase have the drive and skill to successfully represented
          individuals who have suffered major injuries in car crashes.
        </p>
        <p>
          We have over <strong>35 years</strong> of experience when it comes to
          recovering maximum compensation for seriously injured victims.
        </p>
        <p>
          We have a reputation of fearlessly fighting and winning against
          insurance companies that like to play hardball with vulnerable
          accident victims. We provide each and every one of our clients with
          superior legal representation and quality customer service.
        </p>
        <p>
          Call us at <strong>949-203-3814</strong> to find out how we can help
          you!
        </p>
        <h2>Do I Need a Lawyer After a Car Accident?</h2>
        <p>
          If you have been involved in a car accident, you are probably asking
          yourself this question. Here are examples of a few circumstances in
          which you might need the services of an experienced Costa Mesa car
          accident lawyer:
        </p>

        <ul>
          <li>
            <strong>Serious injuries:</strong> If you suffered serious injuries
            such as broken bones, a herniated disk or whiplash, which could
            leave you hospitalized and unable to work for a while, you need a
            lawyer. If you have suffered a permanent or catastrophic injury such
            as a traumatic brain injury or paralysis, your have a potentially
            large claim. It is crucial in such cases that you retain the
            services of a lawyer who has successfully handled similar cases.
          </li>
          <li>
            <strong>Fault:</strong> If there is a dispute with regard to who or
            what caused the car crash, you may have trouble proving your case
            and holding the at-fault party accountable. If you have been
            seriously injured and need monetary support for medical expenses,
            there is too much at stake for you to do it on your own. You need a
            car accident lawyer's help since the burden in personal injury cases
            rests on the shoulders of the plaintiff.
          </li>
          <li>
            <strong>Out-of-pocket expenses:</strong> If your out-of-pocket
            expenses did not exceed a few hundred dollars, it may be possible
            for you to negotiate a settlement on your own with the insurance
            company. However, if you have spent thousands of dollars, you may
            need the help of a lawyer to ensure that you are fairly compensated
            for your losses.
          </li>
          <li>
            <strong>Jury trial:</strong> You certainly need an attorney to
            represent you if your case is going to a jury trial. You need an
            experienced lawyer who knows the court rules in order to give
            yourself a fair chance to win and protect your legal rights.
            Although a majority of personal injury claims are settled out of
            court, there are times when these cases do go to trial.
          </li>
        </ul>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/s2hGIOhPCNc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            How can I win against the insurance companies? Attorney Brian Chase
            explains how in this video.
          </blockquote>
        </center>
        <h2>What Do I Do If I Have Been Injured in a Car Crash?</h2>
        <p>
          If you have suffered injuries in a car accident, it is important that
          you take the necessary steps to protect your rights.
        </p>
        <p>
          First and foremost,{" "}
          <Link
            to="https://www.costamesaca.gov/index.aspx?page=1085"
            target="_blank"
          >
            file a police report
          </Link>{" "}
          with Costa Mesa police and obtain a copy for your own records. Take
          plenty of photographs of the accident scene including the position of
          the vehicles, license plates and roadway conditions. If you are
          injured and unable to take photos, ask a family member or friend to
          help you.
        </p>
        <p>
          Try to get as much information as you can from other parties involved
          in the crash as well as from anyone who may have witnessed the
          collision.
        </p>
        <p>
          Be sure to get immediate medical attention, treatment and care for
          your injuries.
        </p>
        <p>
          Preserve all physical and other evidence from the scene including torn
          or bloody clothing.
        </p>
        <p>
          It is also important that you keep your vehicle intact until it can be
          thoroughly examined by an accident reconstruction expert. Contact the
          skilled Costa Mesa car accident lawyers at Bisnar Chase if you have
          been injured in a car crash. We will fight hard to protect your rights
          every step of the way.
        </p>
        <h2>Is it Safe to Take the Quick Settlement After a Car Accident?</h2>
        <p>
          After a car crash, it might be tempting for you take the first
          settlement the insurance company offers you, however, rushing into a
          settlement is never a good idea.
        </p>
        <p>
          If you take a settlement without taking into account all your damages
          and losses, you will not be able to seek compensation for those
          damages.
        </p>
        <p>
          In addition, the insurance companies' intentions will be to keep
          profit margins up and with this in mind will pay you out for the bare
          minimum of your injuries.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/PJ0iP0dTqbc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <center>
          <blockquote>
            "Insurance companies are not your friend," explains Brain Chase..{" "}
          </blockquote>
        </center>
        <p>
          They cannot be trusted to look out for your best interests. In fact,
          it is in their best interest to offer you a quick and low settlement
          to make your claim go away. That's how they remain profitable.
        </p>
        <p>You need a vocal advocate and an experienced lawyer on your side.</p>
        <p>
          Do not sign off on any agreement until you have discussed it with the
          savvy Costa Mesa car accident attorneys at Bisnar Chase who will get
          you the maximum compensation possible.
        </p>
        <h2>How to Seek Compensation for Your Losses</h2>
        <p>
          If you have been injured in a car accident caused by someone else's
          negligence or wrongdoing, you may be able to seek compensation for
          damages including medical expenses, lost income, hospitalization,
          rehabilitation, pain and suffering and emotional distress.
        </p>
        <p>
          The Costa Mesa car accident lawyers at Bisnar Chase have helped
          thousands of car accident victims. We have filed lawsuits not just
          against at-fault drivers and insurance companies, but also against
          automakers whose vehicles failed to protect occupants during car
          accidents.
        </p>
        <p>
          Our attorneys will leave no stone unturned and no avenue of
          compensation unexplored. Call us at <strong>949-203-3814</strong> for
          a free consultation and comprehensive case evaluation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
