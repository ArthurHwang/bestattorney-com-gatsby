// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Employment Lawyers - Bisnar Chase"
        pageDescription="Have you been wrongfully terminated or discriminated at work? Call 949-203-3814 for top Costa Mesa employment attorneys. Highest quality client care and free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Employment Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          All forms of discrimination at the workplace are unacceptable. One
          form of discrimination that goes underreported is ageism.
        </p>
        <p>
          Anyone who has attempted to pursue a new job in their 50s or 60s is
          well aware of how hard it is to find a company willing to hire an
          experienced applicant over a young applicant. Others may have
          experienced ageism when they did not receive the promotion they
          deserved or when they were let go based solely on their age.
        </p>
        <p>
          Anyone facing this kind of discrimination should seek the council of
          an experienced{" "}
          <Link to="/costa-mesa">Costa Mesa personal injury lawyer</Link> such
          as the exceptional lawyers at Bisnar Chase.
        </p>
        <h2>Age Discrimination in the Workplace</h2>
        <p>
          {" "}
          <Link
            to="https://www.aarp.org/work/on-the-job/info-2014/workplace-age-discrimination-infographic.html"
            target="_blank"
          >
            Ageism
          </Link>
          , as seen in this AARP article, is when someone stereotypes people
          based on their age. Age discrimination commonly involves senior
          citizens in the workplace who are wrongfully terminated or not
          promoted because of their age.
        </p>
        <p>
          There are cases in which a young employee may be discriminated against
          because of their age as well.
        </p>
        <p>
          Many of the laws related to ageism in the workplace are included in
          the{" "}
          <Link
            to="https://www.dol.gov/general/topic/discrimination/agedisc"
            target="_blank"
          >
            {" "}
            Age Discrimination in Employment Act of 1967
          </Link>
          .
        </p>
        <h2>The Age Discrimination in Employment Act</h2>
        <p>
          There are many protections for employees and job applicants in the Age
          Discrimination in Employment Act.
        </p>
        <p>
          Companies that must adhere to these protections include employment
          agencies, general employers, labor unions and the federal government.
          There are some companies, however, that are exempt including
          contractors and elected officials.
        </p>
        <p>
          The ADEA includes a ban against age discrimination and also prohibits:
        </p>
        <ul>
          <li>The inclusion of age preferences on job notices.</li>
          <li>Denial of benefits to older employers.</li>
          <li>
            Discrimination in terms of hiring, promoting or terminating
            employees because of their age.
          </li>
          <li>Paying an employee less because of his or her age.</li>
          <li>
            Mandatory retirement unless the employee is an executive over the
            age of 65 who is entitled to a pension over a minimum yearly amount.
          </li>
        </ul>
        <h2>Who Isn't Protected Under the Age Discrimination Act?</h2>
        <p>Not all employees are protected under the law from ageism.</p>
        <p>
          Individuals who are considered in a high policy making position who
          will make over $44,000 a year in benefits after retirement may be
          required to retire at the age of 65.
        </p>
        <p>
          There are also exceptions for firefighters, police officers, federal
          employees who work in air traffic control and law enforcement and
          tenured university faculty.
        </p>
        <p>
          Jobs that require their employees to look a certain age may be exempt
          from ageism protections as well. This type of exception is called a
          Bona Fide Occupational Qualification and it is relevant in Southern
          California because it typically involves models or actors who have to
          look a certain age.
        </p>
        <p>
          The savvy Costa Mesa Employment lawyers at Bisnar Chase will help to
          inform you of your rights in the workplace as well as help you take
          legal action if you have been discriminated against.
        </p>
        <h2>How Employers Defend Against Age Discrimination Cases</h2>
        <p>
          Individuals who claim that they were treated unfairly because of their
          age may face a strong defense from the employer.
        </p>
        <p>
          It is common for employers to claim that they had good cause for their
          actions that did not involve age or that there were real and
          reasonable factors for their decisions such as performance issues or
          financial reasons that do not involve age.
        </p>
        <p>
          The claimant in such cases may have to prove through evaluations of
          their performance at the company that there was no real reason for
          their lack of promotion or termination. Performance evaluations and
          interviews can help an individual prove that they were a valued team
          player who was clearly wronged because of his or her age.
        </p>
        <p>
          These companies normally will bring skilled lawyers to court to defend
          themselves. If this is the case, why go into a legal dispute
          "unarmed?" The skilled team of Costa Mesa employment attorneys at
          Bisnar Chase have over 35 years fighting for their clients and can
          help you to receive justice.
        </p>
        <h2>Other Forms of Discrimination in the Workplace</h2>
        <p>
          Ageism is just one form of discrimination and an injustice against
          which a skilled Costa Mesa employment lawyer can help you fight. Other
          cases may involve discrimination based on race, sexual orientation,
          religion, gender and disabilities. Many employment lawyers handle
          wrongful termination cases and disputes regarding wages earned and
          hours worked. A skilled Orange County employment attorney can also
          help individuals who have suffered sexual harassment in the workplace.
        </p>
        <h2>How Can I Take Legal Action Against an Employer?</h2>
        <p>
          If you or a loved one has been discriminated against by a potential,
          current or former employer, the experienced Costa Mesa employment
          lawyers at Bisnar Chase can help you better understand your legal
          rights and options.
        </p>
        <p>
          Please call us at <strong>949-203-3814</strong> or contact us for a
          free and comprehensive consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
