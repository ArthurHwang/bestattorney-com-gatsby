// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Dog Bite Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 now for best Costa Mesa dog bite attorneys. Free consultations and high quality client care."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Dog Bite Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>Every year, innocent Costa Mesa residents are bitten by dogs.</p>
        <p>
          Individuals, in most cases, involved in a Costa Mesa dog attack or any
          other related accident may have a legal right to recover damages from
          the responsible parties involved.
        </p>
        <p>
          If you have been injured in a Costa Mesa dog bite accident, the{" "}
          <Link to="/costa-mesa" target="_blank">
            Costa Mesa personal injury lawyers
          </Link>{" "}
          <Link to="/costa-mesa/dog-bites" target="_blank"></Link>&nbsp; at
          Bisnar Chase will fight for you. We are proficient in dog bite law and
          know all the fine details to ensure to receive the compensation you
          deserve.
        </p>
        <p>
          Call us now at <strong>949-203-3814</strong> for a free consultation.
        </p>
        <h2>How Can I Tell if I Have a Case?</h2>

        <p>
          Seek medical attention before anything else if you have been the
          victim of a Costa Mesa dog bite accident. Your well being takes
          priority over anything else and use will be able to use your medical
          records later on as you pursue legal action with an experienced Costa
          Mesa dog bite attorney
        </p>
        <p>
          If left untreated, dog bite attacks can lead to infections, and in
          rare cases, even death.
        </p>
        <p>
          An attorney who deals with dog bite law will be able to tell you
          whether you have a legal claim, and what damages you may be able to
          recover. The savvy Costa Mesa dog bite attorneys at Bisnar Chase are
          skilled when it comes to dog attack cases and have a collective track
          record of over 35 years representing clients.
        </p>
        <h2>How Can a Lawyer Help Me After a Dog Bite Accident?</h2>
        <p>
          A Costa Mesa dog bite lawyer will ask you exactly what happened during
          your dog bite accident. After the attorney takes all the accounts into
          consideration, your attorney can advise you on the best legal course
          of action to take if your accident has the potential to go to trial.
        </p>
        <p>
          You should be able to provide the contact information of the dog's
          owner to your lawyer. If you don't have this information, ask the
          neighbors of the owner or another witness who saw the dog attack first
          hand. In addition, get the contact information for the eyewitnesses as
          they may be of help as you purse legal action against the at-fault
          party.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/LCaC1XWmRGU"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <center>
          <blockquote>
            Attorney Brian Chase breaks down who is at fault when it comes to
            dog bite cases.
          </blockquote>
        </center>

        <h2>Dog Owner Liability in California</h2>
        <p>
          Costa Mesa imposes what is known as "strict liability" upon local dog
          owners whose pets bite or attack pedestrians
        </p>
        <p>
          A dog owner is legally responsible ("liable") for a dog's behavior
          according to{" "}
          <Link
            to="https://www.justia.com/trials-litigation/docs/caci/400/463.html"
            target="_blank"
          >
            California Dog Bite Statute
          </Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/6wAqpIBOw6A"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Brian Chase explains that even though the statute is strict for dog
            bite cases, you still need to hire a knowledgeable attorney to get
            maximum compensation.
          </blockquote>
        </center>
        <p>
          Even if the owner didn't know that their dog was dangerous, if the dog
          bit someone, the owner would still be liable.
        </p>
        <p>
          In states outside of California, the dog owner can be held liable for
          the injuries it inflicts, provided that the owner knew that his or her
          dog is dangerous and could cause injury to a person. However,
          determining whether this is a case of negligence can be difficult.
        </p>
        <p>
          For example, when a owner has a pit bull as a pet, does that mean the
          owner should know the pet will be harmful just because pit bulls can
          be harmful?
        </p>
        <h2>Potential Defenses in Various Dog Bite Cases</h2>
        <p>
          There are cases in which an owner of a vicious dog in Costa Mesa might
          not be held liable for a dog attack.
        </p>
        <p>
          For example, if the owner proactively cautioned neighbors that the dog
          was vicious, and took action to keep the dog away, a individual who
          ignored these warnings and was bitten might not have a case that would
          stand up in court.
        </p>
        <p>
          This situation is what we would call in legal terms "
          <Link
            to="https://injury.findlaw.com/accident-injury-law/contributory-and-comparative-negligence.html"
            target="_blank"
          >
            contributory negligence
          </Link>
          ."
        </p>
        <p>
          An injured person is negligent when he or she puts themselves in
          danger compared to the average individual would would take measures
          for personal safety.
        </p>
        <p>
          For example, if a person is bitten by a dog after hopping a fence,
          it's possible that the dog owner wouldn't be held liable.
        </p>
        <p>
          In addition, if a "Beware of Dog" sign is promptly displayed and a
          person is bitten after ignoring this sign, the owner might not be
          responsible for that person's injury.
        </p>
        <p>
          A dog owner can also argue that the injured person(s) had provoked the
          dog as a way to escape liability.
        </p>
        <h2>Getting Legal Help in Costa Mesa</h2>
        <p>
          If a dog has bitten you or a loved one, you may be entitled to
          recovery for your damages.
        </p>
        <p>
          Determining your rights will complicated, and it may be unclear where
          to take legal action..
        </p>
        <p>
          Although this may be a complex process, have no fear because the Costa
          Mesa dog bite lawyers at Bisnar Chase will take on any dog bite cases,
          big or small, and will will you the compensation that you deserve.
        </p>
        <p>
          If you have been injured in a dog bite accident, call us now at{" "}
          <strong>949-203-3814</strong> for a free consultation. There is a
          statute of limitation with dog bite cases, so your time is limited.
          Don't wait and call us today!
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
