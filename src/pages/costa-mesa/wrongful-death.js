// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Costa Mesa Wrongful Death Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 if your loved one wrongfully died in Costa Mesa from someone else's negligence. Our experienced Costa Mesa wrongful death attorneys will fight to ensure you justice. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Costa Mesa Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Several people die every year in Costa Mesa as a result of wrongful
          death accidents.
        </p>
        <p>
          Losing a loved one is one of life's more difficult challenges and
          pursuing compensation after their death can make a very painful
          process all the more daunting.
        </p>
        <p>
          At Bisnar Chase Personal Injury Attorneys, we have been assisting
          family members of wrongful death victims for over 30 years and know
          how to get you maximum compensation as well as provide assistance to
          help in the healing process.
        </p>
        <p>
          Since 1978, our{" "}
          <Link to="/costa-mesa" target="_blank">
            Costa Mesa personal injury attorneys
          </Link>{" "}
          have been providing a first-className experience for victims
          throughout Orange County.
        </p>
        <p>
          Brian Chase, managing partner, has instilled a passion for superior
          client representation in every Bisnar Chase Personal Injury Attorneys
          staff member.
        </p>
        <p>
          Our 97.5% success rate, history of highly-satisfied clients, and
          numerous record-breaking verdicts and settlements have helped us
          become one of the top wrongful death law firms in both Costa Mesa and
          the nation.
        </p>
        <h2>Experienced Wrongful Death Lawyers in Costa Mesa</h2>
        <p>
          The Costa Mesa wrongful death attorneys at Bisnar Chase have assisted
          victims with several different types of wrongful deaths.
        </p>
        <p>
          No matter how you lost your beloved, our attorneys most likely have
          experience settling or trying a case that is similar to yours. Our
          experience could mean the difference between a large verdict, and a
          non-existent one.
        </p>
        <p>
          The following are types of wrongful death cases in which we have
          experienced tremendous success.
        </p>
        <ul>
          <li>
            <strong>Auto Accidents</strong>
            <ul>
              <li>Car Accidents</li>
              <li>Bus Accidents</li>
              <li>Motorcycle Accidents</li>
              <li>Intersection Accidents</li>
            </ul>
          </li>
          <li>
            <strong>Products Liability</strong>
            <ul>
              <li>Product Defects</li>
              <li>Auto Defects</li>
            </ul>
          </li>
          <li>
            <strong>Bicycle Accidents</strong>
          </li>
          <li>
            <strong>Premises Liability</strong>
          </li>
          <li>
            <strong>Construction Accidents</strong>
          </li>
        </ul>
        <h2>Costa Mesa Wrongful Death Attorneys Who Care</h2>
        <p>
          Few things in life are more difficult than recovering from the death
          of a loved one.
        </p>
        <p>
          At Bisnar Chase, our Costa Mesa wrongful death lawyers do not take
          your healing process lightly and do our best to help you move forward
          on a personal basis.
        </p>
        <p>
          Our clients receive straight talk and honest answers so that they can
          make the best decisions and so we can help them get the assistance
          that they need.
        </p>
        <p>We recommend that all of our clients seek therapeutic treatment.</p>
        <p>
          We have seen clients struggle with their loss and ignore their grief,
          but we make sure to do our part to help you make difficult decisions
          that will help you recover.
        </p>
        <p>
          If you had broken a leg, refusing to get treatment would keep the leg
          from healing and would most likely exacerbate the injury. Getting help
          with your emotional recovery is very similar, and sometimes a helpful
          nudge from your attorney can do wonders.
        </p>
        <p>Our passion to assist victims goes beyond the office.</p>
        <p>
          Every staff member at Bisnar Chase Personal Injury Attorneys spends
          their free time participating in community fundraising events and
          outreach programs on their free time.
        </p>
        <p>
          We feel very fortunate to be in the position that we are and want to
          spread our good fortune to those in the community who are down on
          their luck.
        </p>
        <p>
          The following are ways that we give back to the community. The money
          that we raise is matched by Bisnar Chase Personal Injury Attorneys
          partners, John Bisnar and Brian Chase.
        </p>
        <ul>
          <li>
            <strong>Walk Like M.A.D.D. Events</strong>
            <ul>
              <li>
                Every year, our team contributes money to help raise awareness
                for drunk driving victims. Too many of our clients owe their
                loss to the abuse of alcohol and we feel it is our
                responsibility to contribute to the cause.
              </li>
            </ul>
          </li>
          <li>
            {" "}
            <Link to="https://www.feedoc.org/wp-content/uploads/2016/05/News_2011_Spring.pdf">
              <strong>
                Second Harvest Food Bank OC Resident Turkey Giveaway
              </strong>
            </Link>
            <ul>
              <li>
                Not everyone is lucky enough to be able to provide their family
                with a turkey on thanksgiving. Our efforts help close the gap
                and we get to be out in the community on the front lines.
              </li>
            </ul>
          </li>
          <li>
            <strong>HIV/AIDS Bicycle Fundraisers</strong>
            <ul>
              <li>
                When we have the opportunity to get some exercise and help a
                good cause, we are all for it. Just another way that we can lend
                a hand to help those in the community.
              </li>
            </ul>
          </li>
        </ul>
        <h2>Wrongful Death Attorneys in Costa Mesa</h2>
        <p>
          Finding the right Costa Mesa wrongful death attorney at the beginning
          of your case is important. Too often, we receive cases from other law
          firms in which the clients were not happy with their services and are
          looking for attorneys with a better success rate and client
          satisfaction.
        </p>
        <p>
          If you have lost a loved one due to someone else's negligence, contact
          one of our skilled wrongful death attorneys in Costa Mesa today to get
          expert assistance from proven attorneys.
        </p>
        <p>
          Call <strong>949-203-3814</strong> now to start your case off right
          and get FREE legal information.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
