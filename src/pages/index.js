import React from "react"
import { LayoutHome } from "../components/layouts/Layout_Home"
import { ContactHome } from "../components/elements/forms/Form_Home"
import {
  BraggingRights,
  HomeFAQ,
  PracticeAreas,
  Results,
  Reviews
} from "../components/elements/home"
import { SEO } from "../components/elements/SEO"

export default function IndexPage() {
  return (
    <LayoutHome>
      <SEO />
      <ContactHome />
      <BraggingRights />
      <Results />
      <Reviews />
      <PracticeAreas />
      <HomeFAQ />
    </LayoutHome>
  )
}
