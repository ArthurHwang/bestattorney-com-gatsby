// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/M.V. Dog Bite lawyers.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Dog Bite Attorney - Orange County, CA"
        pageDescription="If you have been bitten by a dog and are suffering from serious injuries, the Mission Viejo Dog Bite Lawyers will fight for the compensation you deserve. Dog bite injuries require immediate medical attention. The law offices of Bisnar Chase has been handling dog attack cases for 39 years. Call 949-203-3814 today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Dog Bite Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The Mission Viejo{" "}
          <Link to="/orange-county/dog-bites" target="_blank">
            {" "}
            Dog Bite Lawyers
          </Link>{" "}
          of Bisnar Chase understand that experiencing a vicious dog bite can be
          mentally and emotionally debilitating. Mission Viejo residents are
          bitten by dogs each year. In many cases, a person bitten by an animal
          may have a legal right to pick up compensation from the animal's
          holder or another accountable party.
        </p>
        <p>
          Over the past <strong> 40 years</strong>, the injury attorneys of
          Bisnar Chase have earned <strong> $500 Million dollars</strong> in
          winnings for clients who have experienced serious harm due to a dog
          owners negligence. We believe that as an injury victim you should be
          compensated for the damages you have faced.
        </p>
        <p>
          If you or someone you know has suffered from catastrophic injuries
          from a dog bite victim contact the law firm of Bisnar Chase today.
          When you call <strong> 949-203-3814</strong> you will receive a{" "}
          <strong> free case evaluation</strong> from a top-notch Mission Viejo
          dog bite attorney.
        </p>
        <h2>What to Do When an Animal Bites You</h2>
        <p>
          Being involved in a dog bite incident can be stressful and terrifying
          especially if it involves a child. In one year, data had shown that
          almost half of the reported dog bite claims involved children younger
          than 11. There are steps you can take to make sure that you as a
          victim or your child is taken cared of.
        </p>
        <ol>
          <li>
            <strong> Seek medical attention immediately</strong>: If you are not
            treated,{" "}
            <Link to="https://pets.webmd.com/dogs/dog-bites#1" target="_blank">
              {" "}
              dog bites{" "}
            </Link>{" "}
            can cause grave injury, disease, and even death. The dog should also
            be put in an area where he could no longer reach the person that has
            been bitten. Stay calm and consult with a doctor urgently.
          </li>
          <li>
            <strong> Gather evidence</strong>: At a minimum, you should offer
            the name and contact of the animal's holder. If you don't have this
            information, a neighbor or a observer might be able to give it to
            you. Also, if there were witnesses, you should get their names and
            contact information as well.
          </li>
          <li>
            <strong> Call an experienced dog bite lawyer</strong>: A dog bite
            specialty attorney can assist you regarding a legal claim and what
            damages you may be able to recover. A Mission Viejo dog bite
            attorney will request details about the situation regarding your dog
            encounter. At a minimum, you should offer the name and contact of
            the animal's holder.
          </li>
        </ol>
        <h2>Legal Responsibility for Dog/Animal Bites</h2>
        <p>
          In deciding who is accountable for an animal bite, the first issue to
          decide is: who is the owner of the animal? Some states impose what is
          known as "strict accountability" upon animal owners whose animals bite
          or attack others.
        </p>
        <p>
          Under the theory of strict accountability, an owner is legally guilty
          ("liable") for an animal bite, regardless of whether the owner did
          anything wrong with regard to protecting others from attack. Under
          this conjecture, even if the owner had no reason to know that his or
          her animal was hazardous, if the animal bit someone, the owner would
          still be accountable.
        </p>
        <p>
          In other states, the owner of an animal can be held legally
          responsible for the injuries it inflicts, provided that the owner knew
          that the animal had "dangerous leanings." In other words, if an animal
          owner knows that his or her canine is perilous and could cause harm to
          a person,{" "}
          <strong>
            {" "}
            the animal owner can be held liable for the animal's injurious
            actions
          </strong>
          .
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/Dog bite Image.jpg"
            width="100%"
            alt="Mission Viejo Dog Bite attorneys"
          />
        </LazyLoad>
        <h2>Are Owners the Only Ones Responsible for Dog Bites?</h2>
        <p>
          Although dog owners are usually the individuals primarily responsible
          for the dog, if the dog is under someone else's care for a short
          period of time, that caretaker can be liable for injuries as well.
          Many other individuals, firms and official agencies can be held
          answerable.
        </p>
        <p>
          <strong> Other parties that can liable for a dog bite</strong>:
        </p>
        <ul type="disc">
          <li>
            The dog liability law might cover owners and also "harborers" and
            "keepers" of the dog.
          </li>
          <li>
            Dog day care centers and some schools sometimes let dogs to mix with
            the kids, resulting in damage to both dogs and children.
          </li>
          <li>
            Shops and eateries intermittently or even regularly allow regulars
            to bring in their dogs. Other customers can fall over leashes or get
            bitten.&nbsp;
          </li>
          <li>
            Animal control officers from time to time carelessly permit a
            acknowledged risky dog to wander the area, ultimately resulting in
            severe injuries to a person.
          </li>
          <li>
            Police now and then inattentively let their working cannines maul
            suspects or even bite people other than suspects.&nbsp;
          </li>
          <li>
            Employers may be held responsible for likely injuries incurred by
            canines belonging to staff and used in the course of their work. The
            point for legal blame is simply based on being the employer, not on
            distraction.&nbsp;
          </li>
        </ul>
        <h2>Ensuring You Receive Compensation</h2>
        <p>
          The compensation that a dog victim receives in a settlement varies
          from state to state. California has held the position as having the
          most dog bite claims in the nation. Experts say though that{" "}
          <Link
            to="https://www.insurancejournal.com/news/national/2015/05/15/368180.htm"
            target="_blank"
          >
            {" "}
            when dogs bite, home insurers pay an average of $32,000 dollars
          </Link>
          .
        </p>
        <p>
          Having the following information will increase your chances of gaining
          the compensation you need for the damages accumulated from the dog
          bite.
        </p>
        <ul>
          <li>Medical records</li>
          <li>Police report</li>
          <li>Names of the dog owner or handler, and all witnesses</li>
          <li>
            Photographs of the wounds before they are treated and afterwards
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog bite injury attorneys in Mission Viejo"
            style={{
              backgroundImage:
                "url('/images/dog-bites/Dog Teeth Text Header Image.jpg')"
            }}
          >
            <h2>Legal Representation You Can Trust</h2>
          </div>
        </LazyLoad>

        <p>
          The Mission Viejo Dog Bite personal injury lawyers of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>
          , have
          <strong>
            {" "}
            represented over 12,000 clients. With a 96% success rate
          </strong>{" "}
          our dog bite attorneys have aided clients with healing from their
          mental, physical and financial losses.
        </p>
        <p>We stand ready to assist you.</p>
        <p>
          <strong> Call us at 949-203-3814</strong> or contact us for more
          information or a <strong> free consultation</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
