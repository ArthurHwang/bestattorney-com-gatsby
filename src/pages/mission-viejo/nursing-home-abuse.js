// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "nursing-home/M.V. Nursing Home banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Nursing Home Attorney - Orange County, CA"
        pageDescription="The Mission Viejo Nursing Home Lawyers of Bisnar Chase believe that you or your family member should be compensated for negligence or intentional elder abuse in an assisted living facility. Our nursing home attorneys have been handling abuse cases with seniors for 40 years.  Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Nursing Home Abuse Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Nursing Home Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Consulting a Bisnar Chase Mission Viejo{" "}
          <Link to="/nursing-home-abuse" target="_blank">
            {" "}
            Nursing Home Abuse Lawyer
          </Link>{" "}
          should be the first step you take if you or a loved one has
          experienced nursing home abuse or neglect. They can help serve as your
          advocate when you decide to go up against the nursing home facility.
        </p>
        <p>
          It is the duty of nursing homes and assisted living centers is to
          provide quality care for our senior citizens. Mission Viejo’s nursing
          homes are expected to provide proper medical care, nutrition, quality
          of life and dignity for seniors.
        </p>
        <p>
          Unfortunately, there are thousands of elderly residents who are abused
          or neglected every year in these facilities that are supposed to care
          for them. If you know someone who has experienced pain and suffering
          by being mistreated in a senior citizen living facility contact the
          Mission Viejo nursing home abuse attorneys of Bisnar Chase. Call
          <strong> 949-203-3814 for a free consultation</strong>.
        </p>
        <h2>5 Signs of Senior Resident Abuse</h2>
        <p>
          There are many physical and emotional signs of patient abuse in
          nursing homes. Unfortunately, due to fear or the inabilty to
          communicate at times, residents do not notify authorities about the
          abuse taking place. If your loved one has shown one of the following
          signs there could be a chance that they are experiencing mistreatment.
        </p>
        <p>
          1.{" "}
          <strong>
            {" "}
            <Link to="/nursing-home-abuse/bedsores" target="_blank">
              {" "}
              Bedsores
            </Link>
          </strong>
          : Bedsores, also known as pressure ulcers occur when there has been
          constant pressure on a specific part of the body. If an elderly
          patient is bed-ridden and has not been moved to prevent the ulcers
          this is grounds for neglect. Bedsores can appear in areas where the
          skin has discoloration that is purple or red, blisters, loss of skin
          and displays a white tone when the section is touched.
        </p>
        <p>
          2. <strong> Poor hygiene</strong>: Elderly people are more prone to
          infection from bacteria and germs because of their weak immune system.
          If a nursing home resident is not being provided with proper hygiene
          this could lead to a virus. Due to neglect, elderly people who have
          experienced poor hygiene may exhibit signs of unclean hair, skin and
          also smell of urine.
        </p>
        <LazyLoad>
          <img
            src="/images/nursing-home/elderlyman.jpg"
            width="169"
            className="imgright-fixed"
            alt="nursing home neglect and abuse lawyers in mission viejo"
          />
        </LazyLoad>
        <p>
          3. <strong> Physical injuries</strong>: If an elderly person is
          consistently in the emergency room for dislocated joints, broken bones
          or is displaying intense bruising in multiple places there is a high
          chance that they have undergone a form of physical abuse. Usually when
          senior residents obtain these injuries its due to a staff member or
          caregiver biting, kicking, scratching or slapping the elder.
        </p>
        <p>
          4. <strong> Depression or aloofness</strong>: Signs of depression,
          anxiety and withdrawal are all symptoms of psychological abuse. If the
          staff is using forms of intimidation such as withholding food,
          ignoring the elderly resident, or isolating the senior from the rest
          of the elderly community this can be classified as psychological
          abuse.The best option to take when these type of signs appear from
          your elderly loved one is to contact Mission Viejo nursing home
          attorney.
        </p>
        <p>
          5. <strong> Vaginal bleeding or pelvic fractures</strong>: Elderly
          women are 6 times more likely to experience sexual abuse in nursing
          homes. If your loved one, male or female, has suddenly contracted
          STD's, exhibits bruising on the inner thighs or has difficulty walking
          these are all symptoms of your loved one being sexually abused.
        </p>
        <p>
          It is common for elderly patients to suffer minor bruises from time to
          time but his or her family should be notified of falling incidents.
          Therefore, if there are unexplained injuries that have not been
          reported and do not appear on the medical reports, then, there may be
          an issue. Sudden changes in personality may be signs of nursing home
          mistreatment as well.
        </p>
        <h2>Why Abusive Behavior Takes Place in Nursing Homes</h2>
        <p>
          There are a variety of reasons why abuse occurs in nursing home
          facilities. A big factor that usually leads to neglect is due to the
          nursing home facility being understaffed. A facility being
          understaffed can lead to numerous mistakes such as giving the wrong
          medications or overlooking a patient that has bed sores.
        </p>
        <p>
          Stress can also be a major factor in nursing home neglect and abuse.
          Holding the position of a caregiver can be very demanding. It is
          strongly suggested that if you are in a caregiver position and you are
          experiencing high levels of stress and anxiety that you receive
          counseling immediately.
        </p>
        <h2>Ways to Prevent Nursing Home Abuse</h2>
        <p>
          Due to the stresses and lack of staff of a nursing home, abuse can
          remain consistent and at times, unfortunately, can even lead to death.
          There are ways on{" "}
          <Link
            to="http://www.nursinghomeabusecenter.org/news/how-to-prevent-elder-abuse/"
            target="_blank"
          >
            {" "}
            how to prevent elder abuse
          </Link>{" "}
          and make sure that your loved one is protected.
        </p>
        <p>
          <strong>
            {" "}
            You can prevent your loved one being in an abusive situation by
          </strong>
          :
        </p>
        <ul>
          <li>
            Visiting and checking on your senior family members frequently
          </li>
          <li>Make sure that they remain aware of their financial standing</li>
          <li>
            Keeping them involved in activities outside of the nursing home
          </li>
        </ul>
        <h2>
          <strong> Elder Abuse Cases are Often Underreported</strong>
        </h2>
        <p>
          The sad reality is that there are substantially more incidents of
          abuse than will ever be reported. It is common for victims to shy away
          from reporting incidents out of humiliation or depression. In such
          cases, it is rare for them to ever complain about their suffering to
          their loved one. They feel vulnerable or helpless and unwilling to
          admit their struggles. Even attentive and loving family members that
          visit regularly are often unable to notice any telltale signs of abuse
          or neglect. Even so, there are a number of red flags that family
          members should be on the look out for.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Nursing home abuse attorneys in Mission Viejo"
            style={{
              backgroundImage:
                "url('/images/nursing-home/Mission Viejo Text Header.jpg')"
            }}
          >
            <h2>Orange County and California Elder Abuse Data</h2>
          </div>
        </LazyLoad>

        <p>
          Elder abuse in Orange County is predominant and people from all
          classes of income are affected. Each year, about 225,000 senior
          citizens home become victims of nursing home abuse.
        </p>
        <p>
          <strong>
            {" "}
            The following data displays nursing home abuse in Orange County
          </strong>
          :
        </p>
        <ul type="disc">
          <li>
            Just five of the 58 California counties (Orange, Los Angeles, San
            Diego, Riverside and Santa Clara) account for more than half of all
            reported elder abuse cases in California
          </li>
          <li>
            In 2008, California had an elderly population (individuals over 60)
            of 5,728,021.
          </li>
          <li>During that year there were 632,693 cases of elder abuse.</li>
          <li>
            Los Angeles County had approximately 25 percent of all elder abuse
            cases in California that year with 162,917.
          </li>
          <li>
            Orange County had the second most cases of elder abuse with over
            53,000 cases resulting in 8.4 percent of all elder abuse cases in
            California.
          </li>
        </ul>
        <h2>
          <strong> Elder Abuse National Statistics</strong>
        </h2>
        <p>
          Elder abuse data is difficult to collect due to seniors being
          intimidated and threatened by their abusers in the facilities. The
          statistics that are collected reflect that nursing home abuse is a
          significant dilemma.
        </p>
        <p>
          The following statistics were listed in a report by the National
          Center on Elder Abuse:
        </p>
        <ul type="disc">
          <li>
            Every year, anywhere between one and two million Americans aged 65
            or older are exploited, injured or otherwise mistreated by someone
            whom &ldquo;they depended for care or protection.&rdquo;
          </li>
          <li>
            The frequency of elder abuse ranges somewhere between 2 and 10
            percent.
          </li>
          <li>
            Approximately only one in 14 incidents of elder abuse in domestic
            settings are reported.
          </li>
          <li>
            It is estimated that only one in five cases of elder abuse, neglect,
            self-neglect or exploitation are ever reported.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/nursing-home/anahiem-nursing-home-attorneys-old-hands.jpg"
            width="100%%"
            alt="Mission Viejo nursing home abuse attorneys"
          />
        </LazyLoad>

        <h2>Your Loved One Does Not Have to Suffer Anymore</h2>
        <p>
          If you worry that your loved one is being abused, neglected or
          mistreated, the experienced Mission Viejo nursing home abuse attorneys
          at the law firm of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          will be able to help you better understand your legal rights and
          options.
        </p>
        <p>
          We have helped nursing home abuse victims and their families hold
          negligent nursing homes accountable for their actions.{" "}
          <strong> Please call us at 949-203-3814 </strong>or contact us today
          for a<strong> free and comprehensive consultation</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys
            <br /> 1301 Dove St. #120 Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
