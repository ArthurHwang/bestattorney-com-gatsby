// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Speed Traps - Intersections You Need To Look Out For"
        pageDescription="Although Mission Viejo is considered to be one of the safest cities in the US, speed traps are still very common in the area. Drivers trying to beat red lights are more prone to traffic tickets than cautious motorists.Trying to avoid Mission Viejo speed traps? Call 949-203-3814 to speak with a personal injury attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Speed Traps</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/car-accidents/Mission Viejo Car Speed Traps.jpg"
          width="42%"
          className="imgright-fluid"
          alt="mission viejo speed traps"
        />

        <p>
          Some people find it strange that Mission Viejo{" "}
          <Link
            to="https://www.motorists.org/issues/speed-traps/definition/"
            target="_blank"
          >
            {" "}
            speed traps
          </Link>{" "}
          are so common, considering that the city was named the safest city in
          the United States by the 2007 Morgan Quitno crime statistic survey.
          The fact is the safer a city is the more probable that it will have an
          increased number of speed traps, since speed traps are theoretically
          put in place to discourage speeding in areas that are of higher risk.
        </p>
        <p>
          The California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) showed that seven people died and two hundred and
          fifty were injured as a result of{" "}
          <Link to="/mission-viejo/car-accidents">
            Mission Viejo car accidents
          </Link>
          . These numbers aren't consistent with the "safest city in the U.S."
          label, so it is of no surprise that the city is increasing speed traps
          to protect its new claim to fame.
        </p>
        <p>
          Reduce your speed when approaching the following intersections, your
          chances of becoming a tragic statistic increase with every mile per
          hour.
        </p>
        <h2>Felipe Road, passing Oso Parkway to La Paz Area Speed Trap</h2>
        <p>
          According to speedtrap.org, as you're coming down the hill passing Oso
          parkway towards La Paz, there is a police officer on the left hand
          side waiting to catch speeding motorists. Nine users have confirmed
          that this is a speed trap.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Felipe+and+oso&sll=33.921466,-118.002997&sspn=0.012678,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Felipe+Rd,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.588651,-117.645453&spn=0.003128,0.00456&z=17&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=Felipe+and+oso&sll=33.921466,-118.002997&sspn=0.012678,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Felipe+Rd,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.588651,-117.645453&spn=0.003128,0.00456&z=17&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <h2>Oso Parkway Eastbound approaching Marguerite Parkway Speed Trap</h2>
        <p>
          According to speedtrap.org, a motorcycle police officer sits in the
          Oso Medical Plaza driveway waiting to catch speeding motorists.
          Thirty-one users have confirmed that this is a speed trap.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Oso+and+marguerite&sll=33.588799,-117.645456&sspn=0.012727,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Marguerite+Pkwy,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.584128,-117.659358&spn=0.003128,0.00456&z=17&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=Oso+and+marguerite&sll=33.588799,-117.645456&sspn=0.012727,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Marguerite+Pkwy,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.584128,-117.659358&spn=0.003128,0.00456&z=17&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <h2>Going from Felipe/Olympiad to I-5 on Oso Speed Trap</h2>
        <p>
          According to speedtrap.org, on the right hand side at the corner of
          Pacific Hills and Oso a motorcycle police officer parks on the
          sidewalk. He is hard to see, as drivers are usually moving pretty fast
          and there is a corner that hides the officer. Nineteen users have
          confirmed that this is a speed trap.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=felipe+and+oso&sll=33.584131,-117.659352&sspn=0.012728,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Felipe+Rd,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.588633,-117.645464&spn=0.003128,0.00456&z=17&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=felipe+and+oso&sll=33.584131,-117.659352&sspn=0.012728,0.012102&ie=UTF8&hq=&hnear=Oso+Pkwy+%26+Felipe+Rd,+Mission+Viejo,+Orange,+California+92692&t=h&ll=33.588633,-117.645464&spn=0.003128,0.00456&z=17&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <h2>Muirlands Boulevard near Alicia Speed Trap</h2>
        <p>
          According to speedtrap.org, a motorcycle police officer sits on
          southbound side of Muirlands just south of Alicia. He is monitoring
          northbound traffic. Twenty-seven users have confirmed that this is a
          speed trap.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Muirlands+and+alicia&sll=33.873011,-117.741534&sspn=1.623591,1.549072&ie=UTF8&hq=&hnear=Alicia+Pkwy+%26+Muirlands+Blvd,+Mission+Viejo,+Orange,+California+92691&t=h&ll=33.609794,-117.686641&spn=0.001564,0.00228&z=18&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=Muirlands+and+alicia&sll=33.873011,-117.741534&sspn=1.623591,1.549072&ie=UTF8&hq=&hnear=Alicia+Pkwy+%26+Muirlands+Blvd,+Mission+Viejo,+Orange,+California+92691&t=h&ll=33.609794,-117.686641&spn=0.001564,0.00228&z=18&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <h2>
          Marguerite Parkway southbound between Alicia and Casta Del Sol Speed
          Trap
        </h2>
        <p>
          According to speedtrap.org, a motorcycle police officer hides in front
          of Senior Living and in the center divider left-turn pocket on
          Marguerite and Coso. They catch you coming downhill as drivers tend to
          speed. Forty-four users have confirmed that this is a speed trap.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=marguerite+and+casta+del+sol&sll=33.584179,-117.659574&sspn=0.012728,0.012102&ie=UTF8&hq=&hnear=Marguerite+Pkwy+%26+Casta+Del+Sol,+Mission+Viejo,+Orange,+California&t=h&ll=33.617,-117.65166&spn=0.001564,0.00228&z=18&iwloc=A&output=embed"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p align="center">
          {" "}
          <Link to="https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=marguerite+and+casta+del+sol&sll=33.584179,-117.659574&sspn=0.012728,0.012102&ie=UTF8&hq=&hnear=Marguerite+Pkwy+%26+Casta+Del+Sol,+Mission+Viejo,+Orange,+California&t=h&ll=33.617,-117.65166&spn=0.001564,0.00228&z=18&iwloc=A">
            View Larger Map
          </Link>
        </p>
        <h2>Mission Viejo Personal Injury</h2>
        <p>
          If you have been injured in a car accident, call a{" "}
          <Link to="/mission-viejo/car-accidents">
            <strong> Mission Viejo Personal Injury Lawyer</strong>
          </Link>{" "}
          for a<strong> free case evaluation </strong>
          of your case by top-notch injury attorneys who have represented over
          12,000 clients since <strong> 1978</strong>. Contact the law firm of
          Bisnar Chase and receive award winning representation and outstanding
          personal service.
        </p>
        <p>
          <strong> Call 949-203-3814</strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys<br></br> 1301 Dove St. #120
            Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
