// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "employment/Mission Viejo Employment Lawyers Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Employment Attorneys - Employment Lawyers Mission Viejo"
        pageDescription="If you have experienced a wrongful termination contact the Mission Viejo Employment Lawyers of Bisnar Chase. Our experienced labor attorneys have been fighting for employee rights since 1978. Call 949-203-3814 for legal advice if you are facing discrimination in the workplace."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Employment Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Employment Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The skilled <strong> Mission Viejo Employment Lawyers</strong>{" "}
          <strong>
            {" "}
            at{" "}
            <Link to="/" target="_blank">
              {" "}
              Bisnar Chase{" "}
            </Link>
          </strong>{" "}
          have experience handling tough employment law issues.
        </p>
        <p>
          There are a number of struggles that employees have to deal with on a
          daily basis. Facing discrimination at the workplace should not be one
          of them. Anyone who believes that they were mistreated at the
          workplace would be well advised to discuss their situation with an
          experienced Mission Viejo employment attorney.
        </p>
        <p>
          <strong> Bisnar Chase's mission statement is: </strong>
          <em>
            "To provide superior client representation in a compassionate and
            professional manner while making our world a safer place."
          </em>
        </p>
        <p>
          Our Southern California injury lawyers have helped clients recover
          over <strong> $500 Million dollars</strong> in compensation for
          clients who have been wrongfully terminated. Our passionate legal
          group believes that you deserve to be compensated for the pain and
          suffering you have faced.
        </p>
        <p>
          If you or someone you know has been wrongfully terminated in the
          workplace contact the law office of Bisnar Chase. Upon your call, you
          will receive a free consultation.
          <strong> Call 949-203-3814</strong>.
        </p>
        <h2>Wrongful Termination</h2>
        <p>
          Wrongful termination involves a key element of unfairness, but the
          focal point of wrongful termination lies in if the employer violated
          any statutes or regulations. Since California is an at-will state, an
          employer can dismiss a person for any reason. Although California is
          categorized as an “at-will” state, if there is enough evidence to
          prove that an employee was terminated due to their race, gender,
          sexual orientation etc. this can be grounds for a wrongful termination
          suit.
        </p>
        <p>
          At times employees do not feel as though they have a strong enough
          case because there is not any physical evidence such as emails, texts
          or witnesses. There are different courses of action one can take if
          there is not a sufficient amount of substantial proof.
        </p>
        <ol>
          <li>
            <strong> Timeline</strong>: A timeline of events will prove whether
            you were terminated because you filed a complaint among HR. For
            instance, if you claimed that your supervisor was making racist
            remarks directed towards you and your employment is quickly
            terminated after that claim, this can be used as evidence against
            the company.
          </li>

          <li>
            <strong> Me Too</strong>: If a person who was wrongfully terminated
            gathers other former employees to testify that the same action took
            place with them as well, this can be solid evidence for an unfair
            dismissal. For example, if an employee claims sexual harassment and
            then other employees come forward and claim the same, this can
            strengthen the case.
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Mission Viejo employment attorneys"
            style={{
              backgroundImage:
                "url('/images/employment/Mission Viejo Employment Lawyers.jpg')"
            }}
          >
            <h2>Understanding Your Rights as an Employee</h2>
          </div>
        </LazyLoad>

        <p>
          Employers must adhere to the Title VII of the Civil Rights Act of
          1964. If an employer obtains 15 or more workers under their employment
          they must not fire or terminate someone on the basis of their:
        </p>
        <ul>
          <li>Religion</li>
          <li>Color</li>
          <li>Race</li>
          <li>Nationality</li>
          <li>Sexual Orientation</li>
        </ul>
        <p>
          An employee does not need to be dismissed in order to file a suit. If
          a worker becomes exempt from benefits, wages, promotions or projects
          because of the above aspects an employee can file a labor lawsuit.
          There are ways to prove that your employer is violating the Title VII
          of the Civil Rights Act of 1964. Having direct evidence such as
          witnesses, emails or verbal comments can increase your chances of
          gaining compensation. Confirming{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Circumstantial_evidence"
            target="_blank"
          >
            {" "}
            circumstantial evidence
          </Link>{" "}
          is difficult but if proven it can better your argument.
        </p>
        <h2>Sexual Harassment</h2>
        <p>
          Sexual harassment in the workforce has been classified to be a type of
          discrimination. This issue can result in an employee under performing
          in their position and the employee can experience a multitude of
          stress and fear. According to the (
          <Link to="https://www.eeoc.gov/" target="_blank">
            {" "}
            EEOC
          </Link>{" "}
          ) sexual harassment is categorized to be any unwanted sexual behavior
          or proposals.
        </p>
        <p>
          <strong> There are two forms of sexual harassment</strong>:
        </p>
        <p>
          <strong> Quid pro quo</strong>: This type of harassment is when an
          individual at a higher position, such as a manager, threatens to fire
          an employee if they do not perform the desired sexual actions.
        </p>
        <p>
          <strong> Hostile work environment</strong>: Consistent inappropriate
          verbal or physical sexual conduct such as sexual remarks, touching or
          revealing pornographic material is grounds for a hostile work
          environment.
        </p>
        <LazyLoad>
          <img
            src="/images/employment/Mission Viejo Stressed woman at work Resized.jpg"
            width="100%"
            alt="Employment lawyers in Mission Viejo"
          />
        </LazyLoad>
        <p>
          Unfortunately, sexual harassment is not always reported. Studies show
          that 87%-94% of employees who have undergone some form of sexual
          harassment do not report it. Fear of retaliation is the main factor in
          not reporting the inappropriate conduct. If you or someone you know
          has experienced sexual harassment in the workplace it is strongly
          suggested that you seek legal representation from a Mission Viejo
          employment lawyer.
        </p>
        <h2>Disability Discrimination</h2>
        <p>
          There are a number of state and federal statutes that prohibit
          discrimination against people with disabilities. It is against the law
          for employers to base decisions regarding hiring, salaries and
          terminations on someone's disabilities. The protections provided to
          individuals dealing with a disability are specified under the
          Rehabilitation Act and the Americans with Disabilities Act.
        </p>
        <p>
          The Rehabilitation Act of 1973 prohibits discrimination based on an
          individual's disability. This includes schools and other institutions
          that receive federal funding. In 1990, congress enacted the Americans
          with Disabilities Act, which enhanced these protections. This act
          specifically makes it illegal for private employers, government
          entities, employment agencies and local unions to discriminate against
          qualified individuals. Employers are prohibited from discriminating
          against someone with a disability when they:
        </p>
        <ul type="disc">
          <li>Hire</li>
          <li>Fire</li>
          <li>Promote</li>
          <li>Make decisions regarding promotions</li>
          <li>Give job training</li>
        </ul>
        <p>
          This act does not, however, cover job applicants and employees in all
          businesses. The Americans with Disabilities Act (ADA) specifically
          covers workers at businesses with 15 or more employees. There are a
          number of other potential exemptions as well.
        </p>
        <LazyLoad>
          <img
            src="/images/employment/Mission Viejo Whistleblower Resized.jpg"
            width="100%"
            alt="Employment attorneys in Mission Viejo"
          />
        </LazyLoad>
        <h2>What is Whistleblowing?</h2>
        <p>
          Unfortunately there has been many employment lawsuits in the United
          States. In one year there had been over $525 million dollars
          accumulated in settlements from employment discrimination suits. For
          the majority of time employees who have been discriminated against are
          usually the people filing the suit.
        </p>
        <p>
          Not always though is an employee fired due to their religion, sex or
          race.{" "}
          <Link
            to="https://www.whistleblower.org/what-whistleblower"
            target="_blank"
          >
            {" "}
            Whistleblowing
          </Link>{" "}
          is the act of an employee reporting illegal activity that their
          supervisor is participating in. If the employee is fired after the
          report has been fired, this is grounds for a wrongful termination.
          There are laws set in place to protect whistle blowers.
        </p>
        <p>
          <strong> Whistleblowers protected by federal laws</strong>: An
          employee may be protected on a national level if the worker can prove
          that there are security and health codes are being violated. For
          example if an employee witnesses a supervisor instructing workers to
          dispose of toxic waste into a National park river, this is a violation
          of the law.
        </p>
        <p>
          <strong> Whistleblowers protected by state laws</strong>: If an
          employer dismisses a worker, after he/she has reported an illegal act
          the employee can be re-hired. When the employee is re-instated regain
          their position, benefits and lost wages. Now, California also fines
          and gives legal repercussions to anyone who aided the supervisor in
          the illegal act. Managers or supervisors who have violated any
          California state laws can face up to 1-year in prison or a fine from
          $1,000-$5,000 dollars.
        </p>
        <h2>Superior Orange County Labor Lawyers</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/Mission Viejo
      Employment Lawyers Resized.jpg"
            width="50%"
            className="imgright-fluid"
            alt="Labor law attorneys in Mission Viejo"
          />
        </LazyLoad>
        <p>
          The skilled{" "}
          <strong>
            {" "}
            Mission Viejo{" "}
            <Link to="/employment-law" target="_blank">
              {" "}
              Employment Lawyers{" "}
            </Link>{" "}
            at Bisnar Chase
          </strong>{" "}
          have experience handling employment law issues that do not involve
          discrimination as well.
        </p>

        <p>
          For example, individuals would be well advised to speak with an
          employment attorney to represent employees if they believe that their
          employer has failed to pay them for overtime. There are some
          exceptions to the rule, but in general, employees who work over eight
          hours a day or over 40 hours a week are eligible for overtime pay.{" "}
        </p>
        <p>
          The employment attorneys of Bisnar Chase have been serving the Mission
          Viejo victims of wrongful terminations for over 40 years. Our lawyers
          have held a 96% rate and understand that being wrongfully terminated
          can take a mental and financial toll on a formal employee. Our law
          firm also specializes in practices areas such as wrongful deaths, car
          crashes and defective products.
        </p>
        <p>
          <strong> Please call us at 949-203-3814</strong> or contact us to
          obtain professional legal advice.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br></br>1301 Dove St. #120
            Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
