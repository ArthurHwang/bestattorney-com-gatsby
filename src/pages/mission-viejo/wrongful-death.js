// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "wrongful-death/Mission Viejo Funeral Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Wrongful Death Attorney - Orange County, CA"
        pageDescription="If you have suffered the loss of a loved one and are looking to file a wrongful death claim, the Mission Viejo Wrongful Death Lawyers are here to help. Our wrongful death attorneys have been serving clients who have lost a family member due to someone else's carelessness for 40 years. Contact us for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Wrongful Death Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Wrongful Death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced<strong> Mission Viejo Wrongful Death Lawyers</strong>{" "}
          of Bisnar Chase are here to hold negligent parties responsible for
          your loved ones passing. Trying to gain compensation after the death
          of a loved one can be daunting and an extremely sensitive subject to
          bring up amongst family members.
        </p>
        <p>
          A civil lawsuit is often the only way to receive monetary compensation
          from the party responsible for your tragic loss. For 40 years, the
          injury attorneys at Bisnar Chase have been helping families obtain the
          winnings they rightfully deserve after losing a family member.
        </p>
        <p>
          If you have experienced the loss of a loved one as the result of
          someone else’s negligence or wrongdoing, you may be able to pursue
          financial compensation for your loss.
          <strong> Contact us at 949-203-3814</strong> and receive a{" "}
          <strong> free case evaluation </strong>from an experienced Mission
          Viejo wrongful death attorney.
        </p>
        <h2>
          <strong> What is a Wrongful Death Claim?</strong>
        </h2>
        <p>
          A{" "}
          <strong>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Wrongful_death_claim"
              target="_blank"
            >
              {" "}
              wrongful death claim{" "}
            </Link>
          </strong>{" "}
          is a civil lawsuit filed by the victim's spouse, children, parents or
          other eligible claimants against the at-fault party. In general, the
          manner in which the death occurs does not affect the value of the
          wrongful death claim.
        </p>
        <p>
          There are cases, however, in which a person caused the death by being
          reckless or careless. Such egregious cases can result in additional
          compensation. Often the value of a Mission Viejo wrongful death claim
          is determined by how much insurance the at-fault party has – be it
          homeowner insurance, auto, or liability insurance.
        </p>
        <p>
          The claims process is not always easy and it can often take months or
          even years to settle. Therefore, anyone who has lost a loved one
          should discuss the specifics of his or her case to determine if it is
          their best interest to file a claim.
        </p>
        <p>
          One important factor in determining if a wrongful death claim is in
          the best interest of the victim's family is to estimate how much the
          claim may be worth.
        </p>
        <LazyLoad>
          {" "}
          <img
            src="/images/wrongful-death/Tustin wrongful death paperwork Tiny JPG.jpg"
            width="100%"
            alt="Wrongful death attorneys in Mission Viejo"
          />
        </LazyLoad>

        <h2>7 Questions to Ask When Filing a Wrongful Death Claim</h2>
        <p>
          There are many components that can affect how much a wrongful death
          claim may be worth. When filing a wrongful death claim there are a few
          important details that you must take into account. Along with "
          <Link
            to="https://www.thebalance.com/what-is-a-wrongful-death-lawsuit-filing-information-3505254"
            target="_blank"
          >
            how to file a wrongful death claim?
          </Link>
          " there are other significant questions to ask when filing a wrongful
          death case.
        </p>
        <p>
          <strong>
            {" "}
            7 questions to inquire when determining the value of a wrongful
            death claim
          </strong>
          :
        </p>
        <p>
          1.Was the victim partially responsible for the accident or was the
          at-fault party solely responsible for the incident? If the victim was
          50 percent responsible for the accident, then, compensation may only
          be available for 50 percent of the losses suffered by the victim's
          family.
        </p>
        <p>
          2. Did the decedent have a spouse? Were there children? Did the victim
          provide financial support to a family?
        </p>
        <p>
          3. How much did the victim earn at work? How many more years would the
          victim have worked had he or she survived the accident?
        </p>
        <p>
          4. Did the decedent consciously suffer pain following the accident
          before succumbing to the injuries? Compensation may be available for
          the victim's pain and suffering.
        </p>
        <p>5. Did the victim survive long enough to incur medical expenses?</p>
        <p>
          6. Did the decedent provide benefits for the claimant such as health
          care?
        </p>
        <p>
          7. Did the claimant have to pay for the victim's funeral and burial?
        </p>
        <p>
          {" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=377.60.&lawCode=CCP"
            target="_blank"
          >
            {" "}
            California law
          </Link>{" "}
          states that a relative of the deceased has two years to file a
          wrongful death claim.
        </p>
        <h2>Damages That Can Be Compensated for in a Wrongful Death Suit</h2>
        <p>
          If the victim survived the accident long enough to experience pain,
          then, there may be additional compensation for pain and suffering.
          There may be also additional compensation if the victim provided for
          minors or if the victim was killed by a criminal act such as drunk
          driving.
        </p>
        <p>
          <strong>
            {" "}
            Losses that can be claimed in a wrongful death lawsuit
          </strong>
          :
        </p>
        <ul>
          <li>Medical expenses the deceased acquired before their death</li>
          <li>Burial and funeral expenses</li>
          <li>
            Loss of future earnings that the deceased could have provided for
            their families
          </li>
          <li>Loss of affection or companionship</li>
          <li>
            Financial support that the deceased provided their family at the
            time
          </li>
        </ul>
        <p>
          If the insurance company acted in bad faith while handling the claim
          this also can be a factor in gaining compensation as well.
        </p>
        <p>
          A wrongful death claim may decrease in value if there is not adequate
          insurance coverage to compensate the claimant.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Mission Viejo wrongful death attorneys"
            style={{
              backgroundImage:
                "url('/images/wrongful-death/Tustin Wrongful Death Text-Header Image.jpg')"
            }}
          >
            <h2>Seeking Justice For Your Loved One</h2>
          </div>
        </LazyLoad>
        <p>
          The experienced <strong> Mission Viejo Wrongful Death Lawyers</strong>{" "}
          at{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase Personal Injury Attorneys
          </Link>{" "}
          can help you determine the value of your claim and help you hold the
          wrongdoers and negligent parties accountable for your actions.
        </p>
        <p>
          Wrongful death claims are not always about money. For most victims'
          families, it is a way to obtain justice and punish the person or
          entity responsible for their loved one's death.
        </p>
        <p>
          If you are thinking about filing a wrongful death claim,{" "}
          <strong>
            {" "}
            please call us at 949-203-3814 for a free consultation
          </strong>{" "}
          and comprehensive case evaluation.
        </p>

        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
