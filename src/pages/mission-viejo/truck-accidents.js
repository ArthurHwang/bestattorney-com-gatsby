// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "truck-accidents/M.V. Truck Accident.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Truck Accident Lawyers - Orange County, CA"
        pageDescription="Truck accidents including tractors or trailers can lead to serious injuries. Fortunately, the Mission Viejo Truck Accident Attorneys of Bisnar Chase are here to help. Since 1978, our personal injury law firm has been aiding victims involved in motor vehicle accidents gain compensation. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Truck Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <strong> Mission Viejo Truck Accident Attorneys</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            <strong> Bisnar Chase</strong>
          </Link>{" "}
          will help you earn the compensation you need if you find yourself
          seriously injured after a truck accident.
        </p>
        <p>
          Truck accidents involving runaway trailers can cause catastrophic
          injuries or even fatalities. The tragedy of these crashes involving
          runaway trailers is the fact that they do not have to happen. These
          types of accidents are entirely preventable.
        </p>
        <p>
          If you or a loved one has been injured in a truck accident, the
          experienced Mission Viejo truck accident lawyers at Bisnar Chase can
          help you better understand your legal rights.
          <strong> Call 949-203-3814</strong> and schedule a{" "}
          <strong>
            {" "}
            <Link to="/contact" target="_blank">
              {" "}
              free case evaluation{" "}
            </Link>
          </strong>{" "}
          with a highly-rated injury lawyer today.
        </p>
        <h2>5 Causes of Large Truck Accidents</h2>
        <p>
          There are a number of potential causes of runaway trailer truck
          accidents. It is critical to understand these possible causes. Truck
          size alone can cause severe harm to drivers and pedestrians alike. In
          one year over 3,986 people died as a result of large truck crashes.
          Below are{" "}
          <Link
            to="https://gtgtechnologygroup.com/five-most-common-causes-of-trucking-accidents/"
            target="_blank"
          >
            {" "}
            5 common causes of truck accidents
          </Link>
          .
        </p>
        <p>
          <strong> 5 common causes of big-rig accidents</strong>:
        </p>
        <ol>
          <li>
            <strong> Overloaded trailer</strong>: When a trailer is overloaded
            with cargo or materials, there is the possibility of brake failure.
            It is the responsibility of the trucking company to ensure that the
            trailer is properly loaded and that the load or weight is evenly
            distributed.
          </li>
          <li>
            <strong> Excessive speed</strong>: When truck drivers are traveling
            too fast while carrying a heavy load there is the danger of the
            trailer detaching and causing a serious injury accident.
          </li>
          <li>
            Sudden turns and unsafe lane changes: A trailer can come loose if
            the truck jackknifes or goes out of control while making a turn or
            an unsafe lane change.
          </li>
          <li>
            <strong> Brake issues</strong>: When the truck's brakes are
            defective or are malfunctioning as a result of poor vehicle
            maintenance, a runaway trailer accident could occur. Trucking
            companies are required to conduct periodic inspections of the
            vehicles' brakes.
          </li>
          <li>
            <strong> Driving on a steep slope</strong>: When there is a steep
            decline or incline, a truck could lose its brakes. In such cases, it
            must be examined if the truck driver made the right decision by
            taking that route and if the driver was traveling at an unsafe or
            excessive rate of speed.
          </li>
        </ol>

        <LazyLoad>
          <img
            src="/images/truck-accidents/anaheim truck accident lawyers.jpg"
            width="100%"
            alt="Mission Viejo Truck Accident Lawyers"
          />
        </LazyLoad>

        <h2>Semi-Truck Crash Prevention</h2>
        <p>
          Being involved in a truck accident can be traumatic and can result in
          serious injuries. In many instances, many fatalities occur from
          semi-truck crashes. Over 89,000 people have suffered from critical
          injuries due to large truck collisions. There are measures that you
          can take for{" "}
          <Link
            to="http://www.cardlog.com/preventing-trucking-accidents-and-ten-reasons-why-accidents-happen/"
            target="_blank"
          >
            {" "}
            preventing trucking accidents
          </Link>
          .
        </p>
        <p>
          <strong> Do not drive in a trucker's blind spot</strong>: Regular
          motor vehicles have a smaller blind spot than semi-trucks do. The
          blind spots to avoid are located on each side of the truck and also
          directly in the front and the back. Drivers must make sure that they
          can see themselves in the side mirrors of the truck.
        </p>
        <p>
          <strong> Make sure you have enough distance</strong>: Large trucks
          take a longer time to completely come to a halt. It is essential that
          drivers provide a substantial amount of space to truckers when driving
          in front and in back of them.
        </p>
        <p>
          <strong> Trying to beat a truck on a turn is dangerous</strong>:
          Drivers trying to overpass a semi-truck at a right turn increase their
          chances of an accident. Large trucks need more room when making a wide
          turn.
        </p>
        <h2>Liability Issues</h2>
        <p>
          In cases where the trailer comes loose because of the negligence of
          the driver or the trucking company, those parties can be held liable
          for the injuries and damages caused.
        </p>
        <p>
          <strong>
            {" "}
            Injured victims in such cases can seek compensation to cover
          </strong>
          :
        </p>
        <ul>
          <li>Medical expenses</li>
          <li>Loss of wages</li>
          <li>Cost of hospitalization</li>
          <li>Rehabilitation</li>
          <li>Pain and suffering</li>
          <li>Emotional distress</li>
        </ul>
        <p>
          The family of a deceased truck accident victim can file a wrongful
          death claim seeking compensation to cover damages including medical
          expenses, funeral costs, lost future income, loss of love and
          companionship and other related damages.
        </p>
        <h2>Securing the Evidence</h2>
        <p>
          In any personal injury case, collecting critical evidence can make or
          break a case. For example, immediately after an accident occurs, it is
          important to take pictures of the crash site, talk to eyewitnesses at
          the scene, determine the position of the vehicles and get images of
          the injuries and damages.
        </p>
        <p>
          A knowledgeable Mission Viejo truck accident lawyer will also take all
          the steps needed to obtain and preserve evidence such as truck
          maintenance logs and driver logs. When such crucial evidence is not
          requested and preserved, it could get misplaced, lost or destroyed.
          Such evidence can certainly help bolster your case and help ensure
          that you get maximum compensation for your losses.
        </p>
        <p></p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Mission Viejo truck collision attorneys"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/Tustin Truck Lawyers Text Header.jpg')"
            }}
          >
            <h2>Contact an Experienced Truck Accident Attorney</h2>
          </div>
        </LazyLoad>
        <p>
          If you or a loved one has been injured in a truck accident, the
          experienced Mission Viejo{" "}
          <Link to="/truck-accidents" target="_blank">
            {" "}
            Truck Accident Attorneys
          </Link>{" "}
          at Bisnar Chase can help you better understand your legal rights and
          options. Our injury lawyers have gained over{" "}
          <strong> $500 Million dollars in wins</strong> for our clients and
          have held a<strong> 96% success rate</strong> for over{" "}
          <strong> 40 years</strong>.
        </p>
        <p>
          We will remain on your side, fight for your rights and ensure that you
          are fairly compensation for your injuries, damages and losses. Please{" "}
          <strong> call us at 949-203-3814</strong> for a
          <strong> free consultation and comprehensive case evaluation</strong>.
        </p>

        <p>
          Bisnar Chase Personal Injury Attorneys <br></br>1301 Dove St. #120
          Newport Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
