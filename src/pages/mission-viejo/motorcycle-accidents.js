// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/Motorcycle Accident Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Motorcycle Accident Attorneys - Orange County, CA"
        pageDescription="Motorcycle collisions can be devastating and leave accident victims with serious injuries and medical bills. Fortunately the Mission Viejo Motorcycle Accident Lawyers are here to help. Our experienced attorneys have been handling cases involving personal injuries for over 40 years. Call 949-203-3814 today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Motorcycle Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been injured in a hit-and-run motorcycle accident, the
          experienced Mission Viejo motorcycle accident lawyers of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          can help you better understand your legal rights and options.
        </p>
        <p>
          Motorcycle riders who are injured in a traumatic accident may have
          difficulty pursuing the financial compensation they need. Depending on
          the outcome of the investigation and the type of insurance the victim
          has, compensation may be available for the losses suffered in the
          collision.
        </p>
        <p>
          The Mission Viejo motorcycle accident attorneys of Bisnar Chase can
          help victims and their families earn the compensation they deserve. If
          you or someone you love has experienced multiple wounds in a
          motorcycle crash contact the law firm of Bisnar Chase.
        </p>
        <p>
          <strong>
            {" "}
            Call 949-203-3814 and receive a free consultation today
          </strong>
          .
        </p>
        <h2>Mission Viejo Motorcycle Accidents</h2>
        <p>
          If you have been injured in an Orange County motorcycle accident, you
          are not alone. There are hundreds of motorcycle-related injury
          accidents every year in Orange County.
        </p>
        <p>
          According to{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/userLogin.jsp"
            target="_blank"
          >
            {" "}
            California Highway Patrol's Statewide Integrated Traffic Records
            System
          </Link>{" "}
          (SWITRS), in one year, there were 16 injuries reported in Mission
          Viejo motorcycle accidents. Over the years in Orange County as a
          whole, 17 people were killed and 735 were injured in motorcycle
          accidents.
        </p>
        <h2>Common Bike Crash Injuries</h2>
        <p>
          Reports conclude that in the United States, 4,976 occurred due to
          motorcycle accidents. Those who do survive bike accidents suffer from
          serious injuries that can have irreparable damage. Below are the
          injuries that frequently result from motorcycle incidents.
        </p>
        <p>
          <strong> Traumatic Brain Injuries</strong>: Wearing a helmet
          constantly may be bothersome, but it can be the number one factor that
          saves you from having permanent brain damage. A helmet is one of the
          most important protective gear for motorcyclist. If a person has
          experienced a{" "}
          <Link to="http://www.traumaticbraininjury.com/" target="_blank">
            {" "}
            TBI
          </Link>{" "}
          due to a motorcycle crash some of the symptoms may be disorientation,
          dazed or the inability to move.
        </p>
        <p>
          <strong> Biker's arm</strong>: When a biker experiences a "burn-like"
          injury on the arms, wrists or hands when a fall occurs this is known
          as a biker's arm. Signs of biker's arm can be little or no movement in
          the fingers or no feeling in the arm, hand or fingers. Biker's arm can
          lead to permanent nerve damage. Physical therapy can one of the kinds
          of treatment that can help recovery for biker's arm.
        </p>
        <p>
          <strong> Road rash</strong>: Road rash appears when the skin scrapes
          the road during an accident and the skin is then torn off. There are
          three kinds of road rashes that include open wounds,{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Avulsion_injury"
            target="_blank"
          >
            {" "}
            avulsions
          </Link>{" "}
          and compressions. Permanent scarring, excruciating pain and infections
          can be the outcome of road rashes.
        </p>
        <LazyLoad>
          <img
            src="/images/motorcycle-accidents/Motorcycle Accident Image.jpg"
            width="100%"
            alt="Mission Viejo Motorcycle Accident Attorneys"
          />
        </LazyLoad>
        <h2>3 Tips for Avoiding a Motorcycle Accident</h2>
        <p>
          Choosing to ride a motorcycle can be dangerous. Fortunately, if you
          have the knowledge of operating a motorcycle properly you can avoid
          any errors that would lead to a catastrophic accident. Below are three
          tips that you can follow to keep yourself safe when riding a
          motorcycle.
        </p>
        <p>
          <strong> 3 tips for preventing a motorcycle accident:</strong>
        </p>
        <p>
          1. <strong> Make sure you are always seen by cars</strong>:
          Motorcyclist can be difficult to spot for car drivers. Many people who
          ride a motorcycle, for the majority of time, land in drivers blind
          spots. One of the solutions you can adhere to is looking at car wheels
          to see if they are turning in your direction.
        </p>
        <p>
          2. <strong> Be aware of your surroundings</strong>: It is important
          that motorcycle riders always check their mirrors. While it is
          critical to pay attention to cars in front of you it is just as
          crucial to check for cars in back of you as well. If you pay attention
          to your mirrors you will grasp a better idea of how much distance you
          have between cars.
        </p>
        <p>
          3. <strong> Keep a safe distance between you and cars</strong>: Do not
          try to beat a car at a turn or when a car is backing up out of a
          driveway. The best thing to do is slow down and wait until the driver
          finishes its course of action. Do not drive in between cars as well.
          Driving in between cars increases your chances of being involved in a
          motorcycle crash.
        </p>
        <h2>Pursuing Compensation</h2>
        <p>
          If the authorities arrest the hit-and-run driver, the injured victim
          can file a personal injury claim against the at-fault party to pursue
          compensation for their losses. Unfortunately, not all Mission Viejo
          hit-and-run accidents result in an arrest. In cases involving a
          negligent driver who is not found by the authorities, financial
          compensation may still be available if the victim has uninsured /
          underinsured motorist coverage.
        </p>
        <p>
          <strong>
            {" "}
            Damages that may be claimed through civil litigation or through
            uninsured motorist coverage include
          </strong>
          :
        </p>
        <ul type="disc">
          <li>
            <strong> Medical bills</strong>: All past and future medical bills
            related to the accident should be included. This includes emergency
            room services, hospitalization, medical testing, medical devices,
            prescription drug costs, surgical procedures and the costs related
            to rehabilitation services.
          </li>
          <li>
            <strong> Lost wages</strong>: If the victim had to miss work while
            receiving treatment or during the healing process, the lost wages
            should be considered during the claim process. In some cases, the
            injury may result in a decrease in the victim's earning potential or
            may even make it impossible for the victim to earn a livelihood.
            Additional compensation should be available in motorcycle accident
            cases that result in catastrophic injuries or permanent
            disabilities.
          </li>
          <li>
            <strong> Pain and suffering</strong>: There are some cases where the
            victim can pursue financial compensation for non-financial losses
            such as physical pain and mental anguish. All injured motorcyclists
            would be well advised to keep a journal of their physical and
            emotional struggles after the accident.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Motorcycle accident lawyers in Mission Viejo"
            style={{
              backgroundImage:
                "url('/images/motorcycle-accidents/M.V. Motorcycle Text-Header.jpg')"
            }}
          >
            <h2>Understanding Your Rights</h2>
          </div>
        </LazyLoad>
        <p>
          The injury attorneys of Bisnar Chase hold an excellent track record of
          winning fair compensation for motorcycle accident victims and their
          families and holding at-fault motorists accountable for their actions.
          Please contact us to schedule a free, comprehensive and confidential
          consultation.
        </p>
        <p>
          <strong> Call us at 949-203-3814</strong> and have the opportunity to
          speak with a{" "}
          <Link to="/mission-viejo" target="_blank">
            {" "}
            Mission Viejo personal injury lawyer
          </Link>
          .
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys
            <br /> 1301 Dove St. #120 Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
