// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "car-accidents/Mission Viejo Speeding Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Car Accidents - The Number of Deaths and Injuries Revealed"
        pageDescription="Speeding can lead to traffic citations, injuries and even worse: death. Although Orange County is considered as one of the safest regions in America, it still has traffic faults. In Mission Viejo alone, many car accidents have left many injured and even dead. Call 949-203-3814 to speak with a vehicle injury attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Mission Viejo's High Traffic and Speeders Contribute to Car Accidents
        </h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Speeders contribute to crashes in Mission Viejo"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Master planned Mission Viejo in Orange County, California has a
          population of nearly 95,000 and has been called one of America's
          safest cities. Unfortunately, like many cities, it has had its car
          accident tragedies.
        </p>
        <p>
          The California Highway Patrol's Statewide Integrated Traffic Records
          System (
          <Link to="https://www.chp.ca.gov/" target="_blank">
            {" "}
            SWITRS
          </Link>{" "}
          ) reported that, in 2006, seven people died and 250 people were
          injured in Mission Viejo car accidents.{" "}
          <Link to="/dui">DUI accidents </Link> caused four deaths and 27
          injuries. One pedestrian was killed and 11 were injured in car
          accidents. Moreover, one bicyclist was killed and five were injured in
          car collisions. In addition, 13 motorcyclists were also injured. And
          in 2007, seven car crashes killed 11 people.
        </p>
        <p>
          "
          <em>
            When you have pedestrians and kids on bikes and skateboards coming
            and going to 50 neighborhood parks, you have to be ever vigilant as
            a motorist,
          </em>
          " noted car accident attorney
          <strong>
            {" "}
            John Bisnar of Bisnar Chase Personal Injury Attorneys
          </strong>
          .
        </p>
        <h2>Devastating Car Accidents in Mission Viejo</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/Mission Viejo Speeding Accidents.jpg"
            width="30%"
            className="imgleft-fluid"
            alt="congested traffic leads to car crashes"
          />
        </LazyLoad>
        <p>
          One tragic car accident that shook Mission Viejo occurred in 2006,
          when 14-year-old Nathan Gonzalez was struck by a Dodge Durango while
          skateboarding with friends. The horrific car crash happened in the
          Aliso Villas neighborhood near Los Alisos Boulevard. Sheriff's
          officials indicated that Gonzalez was riding his skateboard on a
          sidewalk when he veered into the street. Nathan struck the hood and
          was flung about 40 feet. The driver, identified as Johnny David Perez,
          25, was placed under arrest on suspicion of DUI. Nathan was rushed to
          CHOC at Mission with severe head trauma and never recovered from his
          injuries.
        </p>
        <p>
          Another Mission Viejo car crash in 2009 occurred on the northbound I-5
          near Oso Parkway. The car accident wrecked three cars: a silver Toyota
          Camry, a silver Ford Explorer and a black PT Cruiser. One person was
          hospitalized and two more were treated at the scene.
        </p>
        <p>
          Known as a bedroom community, Mission Viejo quickly changes to a
          high-traffic metropolis during early morning and evening rush hours.
          That's when commuters jam Marguerite Parkway, Oslo, Crown Valley and
          La Paz, which are the city's main arteries. Adding to the congestion
          are the throngs of Saddleback Valley College students who tie up
          Marguerite Parkway. Combined, all this extra traffic creates a danger
          for K through 12th grade students walking, biking or skateboarding to
          and from school.
        </p>
        <p>
          We extend our deepest condolences to the family of Nathan Gonzalez for
          their devastating loss.
        </p>
        <h2>Top-Notch Mission Viejo Car Accident Attorneys</h2>
        <p>
          Immediately call an experienced and reputable Orange County motor
          vehicle collisions lawyer for a
          <strong> free consultation at 949-203-3814</strong> or contact the{" "}
          <Link to="/mission-viejo/car-accidents" target="_blank">
            {" "}
            Misson Viejo car crash lawyers
          </Link>
          . For over <strong> 40 years</strong>, the injury attorneys of Bisnar
          Chase have been helping victims of catostrphic crash incidents gain
          compensation for losses such as property damage, medical bills and
          lost wages.
        </p>
        <p>
          Call <strong> 949-203-3814</strong> to learn more about your legal
          options.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys
            <br /> 1301 Dove St. #120 Newport Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
