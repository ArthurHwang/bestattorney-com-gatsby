// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/Personal-Injury-Claim-Mission Viejo Banner-Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Mission Viejo",
      population: 96346,
      totalAccidents: 2352,
      intersection1: "Alicia Pkwy & Jeronimo Rd",
      intersection1Accidents: 65,
      intersection1Injuries: 43,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 70.0,
      city1Name: "Laguna Hills",
      city1Index: 119.1,
      city2Name: "Lake Forest",
      city2Index: 97.2,
      city3Name: "Rancho Santa Margarita",
      city3Index: 44.7,
      city4Name: "Laguna Woods",
      city4Index: 54.2
    },
    locationBox3: {
      intersection2: "Crown Valley Pkwy & Marguerite Pkwy",
      intersection2Accidents: 65,
      intersection2Injuries: 43,
      intersection2Deaths: 0,
      intersection3: "Muirlands Blvd & Alicia Pkwy",
      intersection3Accidents: 85,
      intersection3Injuries: 40,
      intersection3Deaths: 0,
      intersection4: "Oso Pkwy & Marguerite Pkwy",
      intersection4Accidents: 83,
      intersection4Injuries: 35,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Mission Viejo Personal Injury Attorneys - Bisnar Chase"
        pageDescription="The Mission Viejo personal injury lawyers of Bisnar Chase are here for you or someone you know has experienced serious pain and suffering from an accident. Negligent parties should be held responsible for compensation. Call 949-203-3814 and receive a free consultation from an injury attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mission Viejo Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Mission Viejo Personal Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          For over <strong> 40 years</strong>, the{" "}
          <strong>
            Mission Viejo Personal Injury Lawyers of{" "}
            <Link to="/" target="_blank">
              Bisnar Chase
            </Link>
          </strong>{" "}
          have been helping accident victims get back on their feet. We know
          that you are counting on your case outcome to fix many of your
          problems. Our superior legal knowledge and history of success will
          give you the peace of mind to concentrate on getting your personal
          life back on track and trust us with the rest.
        </p>
        <p>
          If you or a loved one has been injured in an accident, you may have a
          number of questions. Contact the Mission Viejo personal injury
          attorneys at Bisnar Chase today to get in touch with a top-notch legal
          representative.
          <strong>
            {" "}
            Call 949-203-3814 and receive a free case evaluation.
          </strong>
        </p>

        <h2>3 Steps For Filing Personal Injury Claims</h2>
        <p>
          Filing a personal injury claim can be a complicated and daunting
          process. After your accident, it is important to educate yourself of
          the necessary actions so you may receive the maximum compensation in
          regards to your personal injury settlement. The following steps
          provide information on what measures to take when trying to file a
          claim after your accident.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/Man calling--Spanish Car Accident Image.jpg"
            width="100%"
            className="imgright-fluid"
            alt="mission viejo personal injury attorneys"
          />
        </LazyLoad>
        <p>
          <strong>
            {" "}
            Three steps to follow when filing a personal injury claim
          </strong>
          :
        </p>

        <p>
          1. <strong> Gather evidence</strong>: Any bodily harm or damage to
          property should be photographed. Documentation of the incident is also
          important when trying to support your case. For example, if you had
          engaged in a conversation after the accident with the driver of the
          other vehicle, it is strongly suggested that you write down what was
          said. Following the accident other documentation such as the contact,
          insurance information of the other driver, medical bills for injuries
          and witness accounts will support your claim.
        </p>
        <p>
          2. <strong> Inform the insurance company about the accident</strong>:
          As soon as you have received treatment after the accident contact your
          insurance company. If you are not able to get in touch with your
          insurance company the alternative would be to go online and file an
          accident report. If the opposing party has already filed a claim be
          given a claim number. Thereafter, an insurance adjuster from the
          at-fault driver's insurance will contact you.
        </p>
        <p>
          3. <strong> Have the law firm write a demand letter</strong>:
          Negotiating can be a complex process when dealing with the other
          party's insurance company. A{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Demand_letter"
            target="_blank"
          >
            {" "}
            demand letter
          </Link>{" "}
          is a vital piece of documentation that expresses to the oppositions
          insurance company that certain expenses must be paid for by that
          insurance company. If the insurance refuses to pay for the damages
          then a suit will be filed by your personal injury attorney.
        </p>
        <h2>Common Personal Injury Claims</h2>
        <p>
          Personal injury claims can involve people experiencing severe bodily
          harm or at times can result in death. Since occurrences such as this
          take place along with property damage, injury victims are strongly
          suggested to seek rightfully deserved compensation. Below are the most
          common personal injury cases.
        </p>
        <p>
          <strong> Motor vehicle accidents</strong>:{" "}
          <Link
            to="https://www.driverknowledge.com/car-accident-statistics/"
            target="_blank"
          >
            {" "}
            Car accident statistics in the U.S.
          </Link>{" "}
          report that an average of 6 million car crashes per year. From that
          amount over half (3 million people) experience serious injuries that
          can be debilitating. If you have been involved in a car crash you can
          be compensated for your medical bills, lost wages as well as your pain
          and suffering.
        </p>
        <p>
          <strong> Slip and falls</strong>: Being in a slip and fall accident
          may seem small but these kinds of accidents can lead to broken bones
          and also{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="_blank">
            {" "}
            different types of traumatic brain injuries
          </Link>
          . Liability of the incident always falls on the property owner.
          Normally the causes are due to uneven flooring and wet surfaces.
        </p>
        <p>
          <strong> Dog bites</strong>: The people who usually experience dog
          bites are unfortunately children. Children can survive the injuries
          but they are left with facial disfigurements, deep tears in the skin
          and are left traumatized. Damages for physical and psychological
          injuries can be compensated for and the dog owner or the person who
          was supervising the dog is legally responsible for the damages.
        </p>
        <p>
          <strong> Defective products</strong>: Product liability cases occur
          when a person suffers from serious bodily harm from a consumer
          merchandise. Manufactures that cause a design error or have sold under
          false pretenses are seen as negligent. Even if the manufacturer
          followed the necessary procedures when making the product, they are
          still held accountable for the damages.
        </p>
        <p>
          <strong> Wrongful death</strong>: If a person has died due to the
          carelessness of another then the surviving members of that individual
          can file a wrongful death suit. Funeral expenses' lost companionship
          and wages that the deceased would have accumulated in their lifetime.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Personal injury attorneys in Mission Viejo"
            style={{
              backgroundImage:
                "url('images/personal-injury/mission-viejo-text-banner-personal-injury.jpg')"
            }}
          >
            <h2>Who is Liable for Your Losses?</h2>
          </div>
        </LazyLoad>

        <p>
          Every case is different and there are a large number of potentially
          liable parties. At-fault drivers can be held liable for their actions.
          The owners of dangerous properties can be held responsible for the
          injuries suffered in slip-and-fall accidents on their properties. The
          manufacturers of defective products can be held accountable for the
          damages their products cause. Nursing homes that foster an environment
          of abuse and neglect can also be held liable for their wrongdoing.
        </p>
        <h2>The Value of Your Claim</h2>

        <p>
          The point of Tort Law is to return the victim to how they were before
          this accident. This means that all related losses suffered in an
          accident can be recovered through civil litigation. If you want to
          know how much your claim may be worth, a knowledgeable Mission Viejo
          personal injury lawyer may be able to help. Each case is unique.
        </p>
        <h2>Damages You May Be Compensated For</h2>

        <p>
          Current and future medical bills, hospitalization, the cost of
          rehabilitation services and prescription medications are just some of
          the items that may be included in a personal injury lawsuit. Current
          and future wages lost while healing should be included as well. If the
          injuries resulted in a disability, additional compensation should be
          available. In cases involving serious injuries, compensation may be
          available for non-economic losses such as pain and suffering and
          emotional distress.
        </p>
        <p>
          Losses such as physical pain and mental suffering can be included in
          an injury claim. It is not easy to calculate losses that do not have a
          set monetary value, but a skilled attorney can review similar recent
          settlements to estimate how much may be claimed. In general, the
          largest non-financial settlements involve the most severe injuries.
        </p>
        <h2>Were You Partially Responsible for the Incident?</h2>

        <p>
          The amount of compensation that may be available to you is directly
          related to the degree to which you were responsible for the accident.
          This is called comparative negligence or comparative fault. During the
          claim process, your carelessness may be compared to the carelessness
          of the defendant. When both parties are equally at fault, half of the
          losses suffered may be covered through litigation.
        </p>

        <h2>How Long Do I Have to File a Claim in Mission Viejo?</h2>

        <p>
          A statute of limitations is how much time can pass before a victim can
          no longer file a claim. In California, injury victims must file a
          claim within two years of the incident. A wrongful death claim must be
          filed within two years of death. Medical malpractice claims must be
          filed within three years of the incident. In general, it is in your
          best interest to speak with an experienced Mission Viejo attorney as
          soon as possible.
        </p>

        <h2>Do you need any Attorney for Your Mission Viejo Injury?</h2>

        <p>
          No matter how you have suffered injuries, we can help. Our personal
          injury attorneys have represented over 12,000 clients in a large
          variety of different kinds of personal injury accidents.
        </p>

        <p>
          The knowledgeable Mission Viejo personal injury lawyers at Bisnar
          Chase Personal Injury Attorneys have a long and successful track
          record and have over
          <strong> 40 years of experience</strong> protecting the rights of
          injured victims. If you or a loved one has been injured as a result of
          someone else's negligence, please call the law offices of Bisnar Chase
          at <strong> 949-203-3814</strong> to obtain more information about
          pursuing your legal rights.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove St. #120 Newport Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
