// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Officials Housed Convicted Felons in Nursing Home"
        pageDescription="Buena Vista nursing home in Midwest City was fined $168,000 by the state health department for housing four convicted felons for a month last year and has been investigated by the state each year since 2003 for a variety of safety violations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Endangering the Elderly | California Officials Housed Convicted Felons
          at Nursing Home
        </h1>
        <BreadCrumbs location={location} />
        <h2>
          Private prison company housed convicted murderer, rapist and two other
          felons at an Oklahoma nursing home.
        </h2>
        <p>
          <img
            src="/images/nursing-home-abuse-atty.jpg"
            alt="endangering the elderly"
            className="imgright-fixed"
            width="200"
          />
          The state Health Department has had a history of violations and fines
          over the last decade.
        </p>
        <p>
          California corrections officials have admitted that they knew about a
          private prison company housing a convicted murderer, rapist and two
          other felons at an Oklahoma nursing home, but say they had no role in
          "approving or objecting" to the use of the care facility.
        </p>
        <p>
          According to a{" "}
          <Link
            to="http://newsok.com/california-officials-knew-prison-company-was-housing-murderer-rapist-at-midwest-city-nursing-home/article/3741651"
            target="_blank"
          >
            {" "}
            news report in The Oklahoman
          </Link>
          , the Buena Vista nursing home in Midwest City was fined $168,000 by
          the state health department for housing four convicted felons for a
          month last year and has been investigated by the state each year since
          2003 for a variety of safety violations.
        </p>
        <p>
          The article states that the inmates were housed at the nursing home
          from Oct. 19, 2011 to Nov. 15, 2011 after they suffered severe head
          injuries in a riot at the North Fork Correctional Facility in Sayre.
          The inmates were kept shackled to their beds and watched over by armed
          guards, not far from the elderly residents at the nursing home, the
          report said.
        </p>
        <p>
          The inmates were all transferred to prisons in Arizona and California
          once they recovered from their injuries at the nursing home. Buena
          Vista was slapped with the fine for placing its 120-plus residents in
          "immediate jeopardy". State officials also told the newspaper that it
          was very "unsettling" and traumatic for residents at the nursing home
          to see the prisoners being taken through the facility.
        </p>
        <p>
          'To put felons convicted of serious crimes in a nursing home where
          vulnerable elderly people are cared for, is nothing short of
          appalling', said John Bisnar, founder of the Bisnar Chase personal
          injury law firm. "There is no question that they pose a serious danger
          to the residents.
        </p>
        <p>
          It is shocking that this could happen in nursing homes where families
          hope their loved ones will be safe while they get the care and
          attention they need." Nursing homes should be no different from
          neighborhoods and communities, Bisnar said.
        </p>
        <p>
          "Would anyone want a dangerous inmate to move next door to him or her?
          Would we want convicted felons who are still serving their time to
          wander around in parks where our children play? Why should a nursing
          home be any different? Nursing homes have a legal obligation to keep
          their residents safe and provide them with the best possible care."
        </p>
        <p>
          <em>
            The California nursing home abuse lawyers of Bisnar Chase represent
            people who have been very seriously injured or lost a family member
            as a result of{" "}
            <Link to="/nursing-home-abuse">nursing home neglect</Link> and elder
            abuse. For more information, please call 949-203-3814 for a free
            consultation.
          </em>
        </p>
        <p style={{ textAlign: "left" }}>
          Source:
          http://newsok.com/california-officials-knew-prison-company-was-housing-murderer-rapist-at-midwest-city-nursing-home/article/3741651
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
