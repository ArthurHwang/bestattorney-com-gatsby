// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Know the Signs of Nursing Home Abuse and Elder Neglect"
        pageDescription="Elder abuse and neglect can reveal itself in many ways. Know the signs of nursing home abuse and neglect."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Signs of Nursing Home Abuse</h1>
        <BreadCrumbs location={location} />
        <p>
          Patients and residents in nursing home or assisted living facilities
          expect to feel safe and protected while under the care of nursing home
          professionals.
        </p>

        <p>
          <img
            src="/images/signs-elder-abuse.jpg"
            alt="an elderly woman sits alone looking out the window."
            className="imgright-fixed"
          />
          It is the responsibility of the doctors, nurses and staff to take care
          of their residents and ensure that there is no foul treatment or
          malpractice taking place under their care. Unfortunately,{" "}
          <Link to="/nursing-home-abuse">nursing home abuse </Link> does occur,
          leaving victims physically injured and mentally scarred. The effects
          of negligence or maltreatment can be devastating to the victims and
          the victim's family, especially in these fragile golden years of a
          person's life.
        </p>
        <p>
          Nursing home abuse affects victims both physically, through bodily
          harm and medical maltreatment, and emotionally, in feelings of despair
          and helplessness. If you suspect that a loved one has been the victim
          of abuse or neglect in a nursing home or assisted living facility,
          there are several signs that may indicate that he or she has been
          abused.
        </p>
        <h2>Physical Signs of Abuse or Neglect:</h2>
        <ul>
          <li>Unexplained bruises, lacerations on the skin, or wounds</li>
          <li>Use of physical restraints</li>
          <li>Burns or black eyes</li>
          <li>Bed sores</li>
          <li>Exposure of breasts or genitals</li>
          <li>Inadequate treatment of existing medical conditions</li>
          <li>Use of unnecessary medication, such as sedatives</li>
          <li>Lack of food or water</li>
          <li>Untreated wounds or infections</li>
          <li>Unhygienic conditions in the home</li>
          <li>Mental or Emotional Signs of Abuse and Neglect:</li>
          <li>Isolating self from family and friends</li>
          <li>Heightened emotional sensitivity</li>
          <li>Extreme irritability and anger</li>
          <li>Mental disturbance</li>
          <li>Nervousness</li>
        </ul>
        <p>
          If you have noticed these or other signs of physical harm, contact a
          Nursing Home Abuse Lawyer to begin investigating your elder neglect
          case. Our attorneys at Bisnar & Chase has recovered millions for our
          injured clients. Let us help you seek damages for the pain and
          suffering caused by nursing home malpractice.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
