// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Alamitos Belmont Rehab Charged for Neglecting Clients"
        pageDescription="Has your loved one been injured in an Orange County Nursing Home? Call 949-203-3814. You may be entitled to compensation. Top client care since 1978. Call us today."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Alamitos Belmont Rehab Cited for Neglect</h1>
        <BreadCrumbs location={location} />
        <p>
          Alamitos Belmont Rehab Hospital 3901 E. 4th St. Long Beach, California
          90814
        </p>
        <p>
          Long Beach, California is part of a growing epidemic across America of{" "}
          <Link to="/nursing-home-abuse">nursing home abuse </Link> and neglect
          cases. The Alamitos Belmont Rehab Hospital in Long Beach is known for
          serious violations of its elderly residents and have become part of
          the statistics of nursing home abuse which has doubled in the past
          decade alone. Carefully observing our loved ones and their situation
          while in a nursing home is essential, and when abuse is apparent,
          contacting a California elder abuse attorney can help immediately put
          a stop to the abuse and give you the compensation you deserve.
        </p>
        <p>
          Alamitos Belmont Nursing Home has received serious violations from the
          health department including{" "}
          <Link to="/nursing-home-abuse/elder-neglect">severe neglect </Link> in
          medication administration. Take a look at the list of violations
          reported by the Department of Health Services:
        </p>
        <ul>
          <li>
            Develop a complete care plan that meets all of a resident's needs,
            with timetables and actions that can be measured.
          </li>
          <li>Have a program to keep infection from spreading.</li>
          <li>
            Keep each resident free from physical restraints, unless needed for
            medical treatment.
          </li>
          <li>
            Keep the rate of medication errors (wrong drug, wrong dose, wrong
            time) to less than 5%.
          </li>
        </ul>
        <p>
          The aforementioned information is based off data reported by CMS as of
          7/29/10. Citations are issued by the Department of Health Services
          (DHS) during the annual certification visit.
        </p>
        <p>
          Citations come in several classes depending on their severity. The
          state average is a little less than one per facility per year, but
          ideally, a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself. Alamitos Belmont Rehab Hospital and
          Retirement Home is part of a nation-wide epidemic of nursing home
          abuse and neglect. These shameful violations of human rights can be
          stopped with your support.
        </p>
        <h2>Video Answers Questions on Elder Abuse</h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/VOtsTYXb8Ik"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          The video listed above explain the basics of nursing home abuse and
          how to spot the signs. Elder abuse can be classified as a case of
          neglect or intentional harm. The types of intentional harm in nursing
          homes include sexual assaults, physical abuse, emotional and verbal
          abuse, and even financial abuse. Elder neglect can include more
          passively endangering the patients with inadequate food, medicine,
          medical equipment, and proper accommodations. This can cause symptoms
          such as bedsores, malnutrition, overdose on medication or the wrong
          type of prescription, ulcers, and other bodily injuries. Alamitos
          Belmont Rehab Hospital and Nursing Home has shocking neglect in its
          history.
        </p>
        <p>
          If you know of someone who has been abused or neglected in an elder
          care facility, it can be disheartening, but you can get help. At
          Bisnar Chase Personal Injury Attorneys, we believe that nursing homes
          have a responsibility to provide the care that elderly individuals
          need and deserve.
        </p>
        <p>
          Nursing home and elder abuse in California is a complicated matter
          which usually requires a specialist. With over 30 years of experience,
          Bisnar Chase Personal Injury Attorneys, in Orange County, has had the
          experience to fully investigate what many miss in order to get the
          best case results possible for the victim.
        </p>
        <p>
          Besides helping your loved one recover financial compensation for
          their pain, you can also improve safety laws in the nursing home
          industry with your voice.
        </p>
        <p>
          Call a{" "}
          <Link to="/contact">Long Beach nursing home abuse attorney </Link> at
          Bisnar Chase Personal Injury Attorneys to get the answers you are
          looking for. 949-203-3814.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
