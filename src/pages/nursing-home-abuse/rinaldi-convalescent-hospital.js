// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nursing Home Abuse Lawyers - Nursing Home Neglect Attorneys in Ojai (93024)"
        pageDescription="Injured in an Ojai nursing home? Call 949-203-3814 for highly-rated personal injury attorneys. No win no fee lawyers. Top client care and representation since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rinaldi Convalescent Hospital</h1>
        <BreadCrumbs location={location} />
        <h2>16553 Rinaldi St. Granada Hills CA</h2>
        <p>
          Retirement home statistics are shocking. A 1998 study conducted by the
          U.S. General Accounting Office concluded that more than half of the
          suspicious deaths studied in nursing homes were probably due to{" "}
          <Link to="/nursing-home-abuse">nursing home neglect</Link>. In 2003,
          USA Today reported that 50% of all nursing home residents were in
          distress from untreated pain. The numbers in Los Angeles alone show
          how{" "}
          <Link to="/nursing-home-abuse/elder-neglect">
            nursing home abuse and neglect
          </Link>{" "}
          has become an epidemic at the local and nation-wide levels.
        </p>
        <p>
          Rinaldi Convalescent Home in the San Fernando Valley is listed here
          because it has caused potential harm and immediate jeopardy to one or
          more patients. If you or someone you love has been mistreated or
          neglected at Ojai Hospital, call us at 949-203-3814 for a free
          consultation to let you know your options and answer your questions.
        </p>
        <p>Some saddening violations at the Rinaldi Retirement Home include:</p>
        <ul>
          <li>
            Keep each resident free from physical restraints, unless needed for
            medical treatment.
          </li>
          <li>
            Make sure that the nursing home area is free of dangers that cause
            accidents.
          </li>
        </ul>
        <p>
          This information was based off data reported by CMS as of 7/29/10. The
          violations continue and this home has literally dozens of horrifying
          reported violations.
        </p>
        {/* <h2>Videos Answer Questions About Elder Abuse</h2>
    <ul>
      <!-- <li><Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link></li> -->
      <!-- <li><Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing -->
      <!-- Home Abuse? </Link></li> -->
      <li><Link to="/nursing-home-abuse/california-abuse-neglect.html">Why Do People File Nursing Home Abuse Claims? </Link></li>
    </ul> */}
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several class depending on their severity. The state
          average is a little less than one per facility per year, but ideally,
          a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          Elder abuse and nursing home violations are severe offenses. If you or
          someone you love has been involved in nursing home abuse or neglect at
          Rinaldi Nursing call the nursing home attorneys at Bisnar Chase
          Personal Injury Attorneys at 949-203-3814 for a free consultation to
          let you know your options and answer all your questions.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
