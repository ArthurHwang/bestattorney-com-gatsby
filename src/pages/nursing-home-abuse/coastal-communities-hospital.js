// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Coastal Communities Hospital - Orange County Nursing Home Abuse Lawyers"
        pageDescription="Injured in Coastal Communities Hospital? Please call 949-203-3814 for nursing home abuse attorneys who can help. Free consultations. Since 1978. No win, no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Coastal Communities Hospital</h1>
        <BreadCrumbs location={location} />
        <h2>Nursing Home Abuse</h2>
        <h3>2701 S. Bristol St. Santa Ana, CA</h3>
        <img
          src="/images/orange-county-map.jpg"
          alt="Coastal Communities Hospital elder abuse"
          className="imgleft-fixed"
        />
        <p>
          Coastal Communities Hospital is listed because this nursing home has
          caused potential harm and immediate jeopardy to one or more patients.
        </p>
        <p>
          <strong> Some saddening violations include:</strong>
        </p>
        <ul className="bullet-inside">
          <li>
            Failure to give residents proper treatment to prevent bed sores or
            heal existing{" "}
            <Link to="/nursing-home-abuse/bedsores">bed sores</Link>.
          </li>
          <li>Undue use of physical restraints.</li>
          <li>
            Failure to quarantine a resident with an infection that could spread
            to others.
          </li>
          <li>Failure to keep patient's medical history private.</li>
          <li>
            Proper treatment violations including feeding with tubes, and proper
            eating skill restoration programs.
          </li>
        </ul>
        <p className="clearfix ">
          This information was based off data reported by CMS as of 7/29/10
        </p>

        <p>
          It is shocking that some of these violations include allowing disease
          to spread among seniors which immune systems are already weak and
          compromised. The sanitation, proper medical care, and living
          conditions of nursing homes are essential and the failure to do so can
          cause illness and even death. Watch these videos below for common
          questions regarding elder abuse:
        </p>
        <h3>Common Questions About Elder Abuse</h3>

        {/* <ul>
      <!-- <li>{" "}<Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link></li> -->
      <!-- <li>{" "}<Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing
        Home Abuse? </Link></li> -->
      <li>{" "}<Link to="/nursing-home-abuse/california-abuse-neglect.html">Why Do People File Nursing Home Abuse Claims? </Link></li>
    </ul> */}
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several class depending on their severity. The state
          average is a little less than one per facility per year, but ideally,
          a facility should not have any citations.
        </p>
        <LazyLoad>
          <img
            src="/images/nursing-home-abuse.jpg"
            alt="nursing home abuse lawyers"
            className="imgleft-fixed"
            width="300"
          />
        </LazyLoad>
        <ul className="bullet-inside">
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p className="clearfix">
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself.
        </p>
        <p>
          The abuse that goes on in this and other nursing homes can be
          segmented into two types: negligence and intentional abuse.
          Intentional abuse can range from financial abuse, verbal abuse, to
          physical assault and sexual molestation. Negligence, on the other
          hand, is a more passive yet equally serious scope of abuse that
          includes failure and violations to provide the right amount and type
          of food at certain times, medication mistakes or skipping dosages, and
          providing proper medical equipment, living quarters, and overall care
          based on individual needs. Signs of abuse can include malnutrition,
          bedsores and ulcers, visible bodily injuries and general depression or
          unhappiness in a patient.
        </p>
        <p>
          Elder abuse and nursing home violations are serious offenses. If you
          or someone you love has been involved in a nursing home abuse,
          negligence, or other crime, call the specialists at Bisnar Chase
          Personal Injury Attorneys for a free consultation to let you know your
          options and answer all your questions. We have vast experience in
          these cases and are passionate about{" "}
          <Link to="/santa-ana/nursing-home-abuse">
            stopping nursing home abuse from happening in Santa Ana
          </Link>{" "}
          and throughout California.
        </p>
        <p>
          <strong> Our phone number: 949-203-3814.</strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
