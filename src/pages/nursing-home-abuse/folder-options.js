/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "Look for These Signs of Neglect:",
    HTML: `
	<ul className='bpoint'>
		<li>Broken or fractured bones</li>
		<li>Head injuries</li>
		<li>Open wounds, cuts, punctures, untreated injuries in various stages of healing</li>
		<li>Broken eyeglasses, dentures, hearing aids</li>
		<li>Signs of punishment or physical restraint</li>
		<li>Your loved one tells you they have been mistreated</li>
		<li>A sudden change in behavior</li>
		<li>The nursing home refuses to allow unsupervised visits</li>
		<li>If your loved one exhibits any of the above signs, know your rights and <a href='/contact'>contact an attorney </a> today.</li>
	</ul>`
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Nursing Home Abuse Injury Information",
    links: [
      {
        linkName: "Abuse & Neglect in California",
        linkURL: "/nursing-home-abuse/california-abuse-neglect"
      },
      {
        linkName: "Bed Rail Deaths",
        linkURL: "/nursing-home-abuse/bed-rail-death"
      },
      {
        linkName: "Bedsores",
        linkURL: "/nursing-home-abuse/bedsores"
      },
      {
        linkName: "Elder Abuse",
        linkURL: "/nursing-home-abuse/elder-abuse"
      },
      {
        linkName: "Elder Neglect",
        linkURL: "/nursing-home-abuse/elder-neglect"
      },
      {
        linkName: "Falsified Medical Records",
        linkURL: "/nursing-home-abuse/falsified-medical-records"
      },
      {
        linkName: "FAQ's",
        linkURL: "/nursing-home-abuse/faqs"
      },
      {
        linkName: "Forced Arbitration",
        linkURL: "/nursing-home-abuse/forced-arbitration"
      },
      {
        linkName: "Fraud",
        linkURL: "/nursing-home-abuse/fraud"
      },
      {
        linkName: "Insurance Denials",
        linkURL: "/nursing-home-abuse/insurance"
      },
      {
        linkName: "Signs of Abuse",
        linkURL: "/nursing-home-abuse/signs"
      },
      {
        linkName: "Nursing Home Abuse Lawyers",
        linkURL: "/nursing-home-abuse"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "Spotting Signs of Elder Abuse",
      videoUrl: "520_pSDAx_s"
    },
    {
      videoName: "What Should I Do If I Suspect Elder Abuse?",
      videoUrl: "qwlcADHtfvY"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
