// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Elder Abuse - Star Rating Program - Nursing Home Neglect Lawyer"
        pageDescription="Were you injured in a nursing home? Call 949-203-3814 for California nursing home neglect attorneys.Free consultations. Over 30 years experience."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Elder Abuse Victims Applaud New Nursing Home Star Ratings</h1>
        <BreadCrumbs location={location} />
        <p>
          If you are familiar with nursing homes in California, then you are
          aware of the shocking number of{" "}
          <Link to="/nursing-home-abuse/elder-abuse">elder abuse </Link> cases
          that arise from these so-called safe havens. Truth is Americans no
          longer feel safe entrusting these institutions with the task of giving
          proper care to their loved ones. Those that have no other choice are
          left to make very difficult decisions on how to take care of those who
          have spent their lives taking care of others. The news confirms our
          worst fears, and the future for an increase in standards has been
          looking very unlikely . . . until now.
        </p>
        <h2>Fighting Back Against Nursing Home Abuse</h2>
        <p>
          According to the Los Angeles Times, patients and visitors are now
          being greeted with a warm welcome and a publicly posted rating of each
          facility. The federal ratings give facilities one to five stars
          depending on quality of care, much like restaurants display letter
          grades evaluating health and safety compliance. The new law is
          intended to ensure that patients and their families are aware of the
          evaluations to prevent future{" "}
          <Link to="/nursing-home-abuse/elder-neglect">elder neglect</Link>.
          Nursing home officials also must post information explaining the
          ratings and how to obtain information about the nursing home's state
          licensing record from the Department of Public Health's website.
          Facilities that fail to follow the law face a range of potential
          fines.
        </p>
        <p>
          Although this battle has been won, the opposition has been grueling
          from nursing home officials. Whether it is because they feel the
          ratings are unfair, or they know that they will have to spend
          substantially more money on taking care of their patients, many who
          own and operate these facilities don't feel that this is in their best
          interest. This opposition comes as no surprise and can be considered a
          sign that we are moving towards a future where incidents of{" "}
          <Link to="/nursing-home-abuse">nursing home abuse </Link> will be
          pubic information.
        </p>
        <p>
          California is home to 1,235 federally rated nursing homes, more than
          any other state. Of those, 195 got the lowest rating, one star, and
          187 got five stars. The new law will affect about 400 nursing homes in
          Los Angeles County serving about 30,000 people. The state is hoping to
          enforce the rating system to help victims avoid instances of abuse,
          and keep{" "}
          <Link to="/nursing-home-abuse/chemical-restraints">
            nursing home negligence attorneys
          </Link>{" "}
          from enforcing standards that should be controlled by the state. Los
          Angeles County's five-member Board of Supervisors unanimously voted in
          January to encourage Gov. Arnold Scwarzenegger and state health
          department officials to post the ratings, says the Los Angeles Times.
        </p>
        <p>
          Federal officials initially scored nursing homes based on quality,
          staffing and inspections, issuing the top 10% of facilities in each
          category nationwide five stars, the bottom 20% one star and the middle
          70% two, three or four stars, with an equal proportion in each
          category, about 23%. They have since adjusted staffing and quality
          measures so that nursing homes that improve can boost their star
          rating, according to a spokesman for the Centers for Medicare and
          Medicaid Services.
        </p>
        <p>
          "The quality measures are changing," said Edward Mortimore, technical
          director of the survey and certification group in the Baltimore office
          of the Centers for Medicare and Medicaid Services. "We're still
          evaluating what will happen."
        </p>
        <p>
          Feuer said he worked with nursing home officials and patient advocates
          to shape the law, and although the ratings need to be redefined, it
          was important to provide as much information as possible now. I
          couldn't agree more, these laws will prevent countless{" "}
          <Link to="/nursing-home-abuse">senior abuse </Link> incidents as well
          as give care-takers peace of mind.
        </p>
        <p>
          "Having as much information available as possible is crucial when
          families and patients are making what could literally be a
          life-or-death decision," Feuer said. "There's much more we need to do
          in terms of the quality of care at nursing homes, but it begins with
          patients and their families making the right decision."
        </p>
        <h2>
          Bisnar Chase Personal Injury Attorneys Nursing Home Negligence
          Attorney
        </h2>
        <p>
          If you or a loved one have become a victim of nursing home abuse or
          neglect, contact a{" "}
          <Link to="/nursing-home-abuse">
            Los Angeles nursing home negligence lawyer{" "}
          </Link>{" "}
          for a free professional evaluation of your case by attorneys who have
          represented over 12,000 clients since 1978. You will experience award
          winning representation and outstanding personal service by a friendly
          law firm in a comfortable environment.
        </p>
        <h2>Orange County Nursing Home Reviews</h2>
        <ul>
          <li>
            <Link to="/nursing-home-abuse/coastal-communities-hospital">
              Coastal Communities Hospital Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/fountain-care-center">
              Fountain Care Center Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/orange-healthcare-and-wellness-centre">
              Orange Healthcare & Wellness Centre Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/regents-point-windcrest">
              Regents Point Windcrest Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/freedom-village-healthcare-center">
              Freedom Village Healthcare Nursing Home{" "}
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
