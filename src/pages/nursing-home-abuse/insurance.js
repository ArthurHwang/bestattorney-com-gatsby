// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Unfair Denials of Long-Term Disability Insurance for Seniors"
        pageDescription="Senior insurance...Call 949-203-3814 for highest-rated nursing home neglect lawyers, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Unfair Denials of Long-Term Disability Insurance</h1>
        <BreadCrumbs location={location} />
        <p>
          Imagine that an insurance salesman has just persuaded you to sign up
          for long-term disability insurance. You're relatively young now, but
          you are thinking ahead to the time when you will no longer be able to
          work and will perhaps need residential or{" "}
          <Link to="/nursing-home-abuse">nursing home care</Link>. Long-term
          disability insurance is a serious financial investment, but you decide
          to go for it because you don't want to be a burden to your family.
          Year after year you pay your premiums on time and in full. After many
          years, maybe decades, of faithfully paying your premiums, the time
          comes for you to collect the benefits promised you by the insurance
          company those many years ago. You enter into a nursing home, confident
          that the insurance company will pay benefits as promised and you will
          be well-cared for. The insurance company maybe pays as promised for
          the first two years or so and then with a cursory form letter telling
          you that you have been "rehabilitated," the insurance company denies
          your claim and cuts off your benefits!
        </p>
        <p>
          According to a recent report from the American Association for
          Justice, entitled "Standing Up for Seniors: How the Civil Justice
          System Protects Elderly Americans," this is a scenario facing
          thousands of seniors who have had their insurance benefits denied or
          cut by insurance companies who put profits over people. According to
          Mary Beth Senkewicz, a former senior executive at the National
          Association of Insurance Commissioners (NAIC), "The bottom line is
          that insurance companies make money when they don't pay claims.
          They'll do anything to avoid paying, because if they wait long enough,
          they know the policyholders will die."
        </p>
        <p>
          And indeed insurance companies are doing everything they can to avoid
          paying claims as promised. Seniors all over the country have received
          form letters telling them that their stay in the nursing home had
          "rehabilitated" them and they should be ready to leave. Other tactics
          that insurance companies are using include deliberately mailing the
          wrong forms and then denying claims on the basis of incorrect
          paperwork; declaring policyholders to have abandoned the claim if they
          failed to submit forms within 21 days; withholding payments until the
          policyholder submits documents not even required under the terms of
          the policy; locking checks in safes until claimants complained;
          delaying payments until they are a year late; and disposing of
          important correspondence during routine "pizza parties."
        </p>
        <p>
          Insurance companies calculated that few of their terminated
          policyholders would ever do anything about it, but some seniors and
          their attorneys have been challenging these deplorable practices. Mary
          Rose Derks, a 77-year old, was one of the seniors who decided to fight
          back. Her family was forced to sell their business after their
          long-term care insurer, Conseco, denied her legitimate claim for more
          than four years. Derks' case exposed Conseco as employing many of the
          practices described above and led to a Congressional investigation of
          Conseco and the long-term care insurance industry. The case resulted
          in a settlement for the Derks family and a change in the company's
          tactic of denying every claim.
        </p>
        <p>
          If you or a loved one have been denied long-term disability it is
          important that you consult with a lawyer, so you can determine the
          strength of your case and what to what you may be entitled. Call
          Bisnar Chase Personal Injury Attorneys and our team of{" "}
          <Link to="/nursing-home-abuse" target="_blank">
            {" "}
            elder abuse attorneys
          </Link>{" "}
          at 800-849-4905. The call is free. The advice may be priceless.
        </p>
        <p>
          <em>
            Call the{" "}
            <Link to="/los-angeles" target="_blank">
              {" "}
              personal injury lawyers in Los Angeles{" "}
            </Link>{" "}
            for a free professional evaluation of your rights by attorneys who
            have been representing victims of medical malpractice and senior
            abuse since 1978. You will experience award winning representation
            and outstanding personal service by a compassionate and
            understanding law firm in a comfortable environment.
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
