// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Safety Concerns at Evergreen Healthcare Centers of Vallejo"
        pageDescription="Injured in a Vallejo nursing home? Please call 949-203-3814 for a free consultation. No win, no fee attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Safety Concerns at Evergreen Health Care Centers of Vallejo</h1>
        <BreadCrumbs location={location} />
        <p>
          According to The National Center on Elder Abuse, between one and two
          million Americans 65 years of age or older have been "injured,
          exploited, or otherwise mistreated by someone on whom they depended
          for care or protection." In an effort to monitor the many potentially
          dangerous nursing homes in California, the Operation Guardians task
          force performs surprise inspections at these facilities. On January
          19, 2010, inspectors with the Bureau of Medi-Cal Fraud & Elder Abuse
          visited the{" "}
          <Link to="/nursing-home-abuse">
            Evergreen Healthcare Centers of Vallejo-Springs Road
          </Link>
          . During the inspection, they observed a number of health and safety
          concerns.
        </p>
        <h2>
          Observations of the Evergreen Healthcare Centers of Vallejo – Springs
          Road
        </h2>
        <p>
          The following observations were made on the day of the surprise
          inspection:
        </p>
        <ul>
          <li>
            The facility was described as "filthy." According to the report, the
            nursing home was in need or "serious cleaning." Inspectors pointed
            out the need to clean the patient rooms, the food preparation areas,
            the supply rooms and the showering room.
          </li>
          <li>
            A potential for food contamination. There was no door to separate
            the kitchen from outside. This meant that bugs and other
            contaminants could get in the food that was being prepared for
            residents.
          </li>
          <li>
            There were many signs of potential neglect. One resident was lying
            on top of a dried, soiled diaper. The report noted that 70 percent
            of the facility's residents did not receive their ordered skin
            checks, pressure ulcer treatments, and other physician-ordered
            treatments. During a review of the Treatment Records, 35 out of 50
            patients were put at risk of suffering undue harm and neglect
            because of a failure to follow physician orders.
          </li>
          <li>
            The facility did not meet state-mandated staffing requirements.
            Under California law, there must be a minimum of 3.2 hours of
            nursing care per resident per day. In fact, the average hours per
            resident day at this facility was three hours instead of the
            required 3.2. Facilities that fail to meet the minimum requirements
            are much more likely to neglect their residents.
          </li>
          <li>
            Patients could not reach help when needed. Many patients did not
            have call lights within their reach.
          </li>
          <li>
            There was a failure to rotate bedridden patients. One resident had
            Stage II pressure ulcers and a history of similar issues.
          </li>
          <li>
            Patients had access to harmful items. The central supply room
            containing medications and medical supplies was not locked. The
            utility room was also unlocked and filthy.
          </li>
          <li>
            The nursing supply storage room was so dirty that inspectors
            suggested an immediate deep cleaning.
          </li>
          <li>
            There were signs of potential infection issues. For example, urinals
            were unnamed and filthy.
          </li>
          <li>
            Damaged items put residents at risk of suffering an injury.
            Wheelchairs, for example, had torn vinyl that could cut fragile
            skin.
          </li>
          <li>
            The facility's Abuse Prevention Program Policies and Procedures did
            not meet state requirements. Their policy did not include the need
            to immediately report abuse to the local ombudsman or local law
            enforcement. Workers at the facility were instructed to report their
            concerns to their supervisor instead of reporting them to the
            appropriate authorities.
          </li>
        </ul>
        <h2>If You Suspect Nursing Home Abuse or Neglect</h2>
        <p>
          When visiting your loved one in a nursing home there are several red
          flags for which you should look out. Are the rooms clean? Are there
          visible signs of abuse? Are there always workers readily available to
          answer your questions or help your loved one? Has your loved one
          undergone a significant change in personality? Are there missing items
          from your loved one's room? Constantly watching out for signs of
          neglect or abuse can help prevent a potentially tragic situation.
        </p>
        <p>
          If you suspect that elderly abuse or neglect has occurred, it would be
          in your best interest to immediately contact the local ombudsman and
          the authorities. If you want to hold the negligent facility
          accountable for their actions, it may be in your best interest to
          speak with an experienced{" "}
          <Link to="/nursing-home-abuse/elder-neglect">
            Solano County nursing home attorney
          </Link>
          . The skilled nursing home abuse lawyers at our law practice can help
          victims and their families pursue compensation for medical bills, pain
          and suffering and other related losses. If you or a loved one has been
          the victim of nursing home neglect or abuse, please contact us for a
          free, comprehensive and confidential consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
