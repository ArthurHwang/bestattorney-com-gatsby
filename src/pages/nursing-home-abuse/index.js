// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Nursing Home Abuse Lawyer - Elder Abuse Attorneys"
        pageDescription="Contact the California nursing home abuse attorneys for a free consultation. We protect your elderly and fight for fair compensation! If you or a loved one has suffered elder abuse call us for a free victim's rights consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Nursing Home Abuse Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            {" "}
            <Link to="/">Bisnar Chase</Link>,
          </strong>{" "}
          California Nursing Home abuse lawyers has recognized the growing need
          to protect the elderly against abuse and neglect. The APS (Adult
          Protective Services) reports that, in one year alone in the United
          States, there are close to 240,000 reports of elder abuse and neglect
          and in many cases, elders are being abused, physically, emotionally
          and financially while in the "care" of nursing homes and assisted
          living residences.
        </p>
        <p>
          The State of California has enacted the Adult Civil Protection Act to
          protect the rights of the elderly. <strong> Bisnar Chase</strong> has
          also taken on the social responsibility of representing abused and
          neglected seniors and will vigorously pursue all negligent parties.
        </p>
        <LazyLoad>
          <img
            className="imgleft-fixed"
            src="/images/attorney-bisnar.jpg"
            alt="John Bisnar Nursing home abuse lawyer in california"
          />
        </LazyLoad>

        <p>
          <span>
            {" "}
            <Link to="/attorneys/john-bisnar">John Bisnar</Link>, Founder, on
            the role of compassion:{" "}
          </span>
          &ldquo;You are dealing with people who are seriously injured or have
          lost a family member&hellip;Compassion is understanding someone else's
          situation.&rdquo;
        </p>
        <p>
          Nursing home abuse falls into two categories,{" "}
          <strong> intentional harm and neglect</strong>. The intentional harm
          includes physical attacks, sexual assaults, emotional abuse and
          financial abuse. Neglect includes failure to provide for basic needs
          which can result in dehydration, malnutrition, medication overdoses,
          falls, infections,{" "}
          <Link to="/nursing-home-abuse/bedsores">bedsores</Link>, pressure
          sores, skin ulcers, decubitus ulcers, osteomyelitis, septicemia and
          gangrene. Another common injury in nuraing homes is{" "}
          <Link to="/nursing-home-abuse/bed-rail-death">bedrail deaths</Link>.
        </p>
        <h2>
          <strong>
            {" "}
            Nursing homes are responsible for providing the following:
          </strong>
        </h2>
        <ol>
          <li>
            Care for their residents in such a manner and in such an environment
            as will promote maintenance or enhancement of the quality of life of
            each resident;
          </li>
          <li>
            Provide services and activities to attain or maintain the highest
            practicable physical, mental, psycho social well-being of each
            resident in accordance with a written plan of care. The plan should
            describe the medical, nursing and psycho-social needs of the
            resident and is to be periodically reviewed and updated;
          </li>
          <li>
            Conduct a comprehensive assessment that describes the resident's
            capability to perform daily functions and significant impairments.
          </li>
        </ol>
        <p>
          If you believe that your elderly loved one has been a victim of abuse
          and neglect at the hands of negligent caregivers you may be able to
          file a claim to recover all damages against your loved one and family.
          If you see any of these{" "}
          <Link to="/nursing-home-abuse/signs">signs</Link>, consult a reputable
          nursing home abuse lawyer immediately.
        </p>
        <ul>
          <li>Broken or fractured bones</li>
          <li>Head injuries</li>
          <li>
            Open wounds, cuts, punctures, untreated injuries in various stages
            of healing
          </li>
          <li>Broken eyeglasses, dentures, hearing aids</li>
          <li>Signs of punishment or physical restraint</li>
          <li>Your loved one tells you they have been mistreated</li>
          <li>A sudden change in behavior</li>
          <li>The nursing home refuses to allow unsupervised visits</li>
        </ul>
        <p>
          At <strong> Bisnar Chase</strong> we make it our business to assist
          the victims of Nursing Home Abuse or Neglect to protect themselves and
          others similarly situated from further harm and to recover financial
          compensation for the harm they have endured.
        </p>
        <p>
          We charge no fee for consultations or case evaluations. We charge no
          fee unless we recover money for you. We advance all litigation costs.
          We will provide you the clout you need to hold wrongdoers responsible
          for their actions. Our firm has won over $500 Million for our clients
          in the past thirty five years and we fight for your family's rights.
        </p>
        <h3 align="center" className="mb">
          HOLDING WRONGDOERS ACCOUNTABLE FOR THE DAMAGES THEY CAUSE SINCE 1978
        </h3>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
