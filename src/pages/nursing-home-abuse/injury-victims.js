// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nursing Home Help List for California Injury Victims"
        pageDescription="Seriously injured in a nursing home? Call 949-203-3814 for award-winning California nursing home abuse attorneys. Free consultations. No win, no fee. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Nursing Home Abuse Help List for California Injury Victims</h1>
        <BreadCrumbs location={location} />
        <p>
          To get help with your nursing home abuse or neglect case please call
          us at 949-203-3814 or choose from our list of cities below. If you do
          not find your nursing home please call or email for specific
          information.
        </p>
        <h2>Nursing Home Abuse Lawyers</h2>
        <ul>
          <li>
            <Link to="/nursing-home-abuse">
              California Nursing Home Lawyers{" "}
            </Link>
            <ul>
              <li>
                <Link to="/orange-county/nursing-home-abuse">
                  Orange County Nursing Home{" "}
                </Link>
                <ul>
                  <li>
                    <Link to="/anaheim/nursing-home-abuse">
                      Anaheim Nursing Home Abuse{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/costa-mesa/nursing-home-abuse">
                      Costa Mesa Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/cypress/nursing-home-abuse">
                      Cypress Wrongful Death Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/fullerton/nursing-home-abuse">
                      Fullerton Nursing Home Abuse Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/fountain-valley/nursing-home-abuse">
                      Fountain Valley Elder Abuse Attorneys{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/garden-grove/nursing-home-abuse">
                      Garden Grove Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/huntington-beach/nursing-home-abuse">
                      Huntington Beach Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/irvine/nursing-home-abuse">
                      Irvine Nursing Home Abuse Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/laguna-niguel/nursing-home-abuse">
                      Laguna Niguel Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/mission-viejo/nursing-home-abuse">
                      Mission Viejo Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/newport-beach/nursing-home-abuse">
                      Newport Beach Nursing Home{" "}
                    </Link>
                  </li>

                  {/* <!-- Touched by Arthur 10/24.  Linked file does not exist -->
              <!-- <li><Link to="/placentia/nursing-home-abuse">Placentia Nursing Home Lawyers </Link></li> --> */}
                  <li>
                    <Link to="/santa-ana/nursing-home-abuse">
                      Santa Ana Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/tustin/nursing-home-abuse">
                      Tustin Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/westminster/nursing-home-abuse">
                      Westminster Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                  <li>
                    <Link to="/yorba-linda/nursing-home-abuse">
                      Yorba Linda Nursing Home Lawyers{" "}
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
        <h3>
          <Link to="/los-angeles/nursing-home-abuse">
            Los Angeles Nursing Home Abuse
          </Link>{" "}
        </h3>
        <ul>
          <li>
            <Link to="/long-beach/nursing-home-abuse">
              Long Beach Nursing Home Attorneys{" "}
            </Link>
          </li>
        </ul>
        <h2>Nursing Home Abuse</h2>
        <ul>
          <li>
            <Link to="/nursing-home-abuse/brighton-gardens">
              Brighton Gardens Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/whittier-hills-healthcare-center">
              Whittier Hills Health Care Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/heritage-park-nursing-center">
              Heritage Park Center Nursing Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/inland-christian-home">
              Inland Christian Home{" "}
            </Link>
          </li>
          <li>
            <Link to="/bakersfield/nursing-home-abuse">
              Bakersfield Healthcare Center{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/braswells-hampton-manor">
              Braswell's Hampton Manor{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/desert-knolls-convalescent-hospital">
              Desert Knolls Convalescent Hospital{" "}
            </Link>
          </li>
          <li>
            <Link to="/medical-devices/golden-cross-health-care">
              Golden Cross Health Care{" "}
            </Link>
          </li>
          <li>
            <Link to="/newport-beach/nursing-home-abuse">
              Newport Beach Nursing Home Abuse Lawyers{" "}
            </Link>
          </li>
          {/* <!-- <li><Link to="/nursing-home-abuse/ojai-valley-community-hospital">Ojai Valley Community Hospital </Link></li> --> */}
          <li>
            <Link to="/nursing-home-abuse/roseville-point-center">
              Resident Safety Concerns at Roseville Point Health & Wellness
              Center{" "}
            </Link>
          </li>
          <li>
            <Link to="/locations/sacramento-nursing-home-abuse">
              Sacramento Nursing Home Fails to Provide Adequate Care{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/winsor-house-care-center">
              Safety and Health Issues Winsor House Care Center - Vacaville{" "}
            </Link>
          </li>
          <li>
            <Link to="/nursing-home-abuse/evergreen-healthcare-centers">
              Safety Concerns at Evergreen Health Care Centers of Vallejo{" "}
            </Link>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
