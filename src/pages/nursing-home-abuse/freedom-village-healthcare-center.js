// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nursing Home Abuse & Neglect - Freedom Village Health Care"
        pageDescription="Nursing Home Abuse was reported at Freedom Village Healthcare in Lake Forest.Call 949-203-3814 if you or a loved one have suffered from nursing home abuse."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Nursing Home Abuse & Neglect</h1>
        <BreadCrumbs location={location} />
        <h2>
          FREEDOM VILLAGE HEALTHCARE CENTER 23442 EL TORO ROAD LAKE FOREST CA
        </h2>
        <p>
          <img
            src="/images/old-woman-nursing-home-abuse.jpg"
            alt="nursing home abuse"
            className="imgleft-fixed"
          />
          As we watch our family members and friends grow old, we shouldn't have
          to be anxious about these loved ones falling victim to nursing home
          abuse. With a myriad of ailments to look out for, we have enough to
          worry about. We look to medication, doctors, nutrition, and exercise
          regimes anything we can find in hopes of keeping them healthy and
          content and able to live as independently as they wish to. When their
          complications turn too severe, we turn to an assisted living or
          nursing home facility to give them the same care we had provided
          before.
        </p>
        <p>
          While many of these facilities provide quality care, far too many do
          not. Often understaffed with poorly trained employees working at low
          wages, many nursing homes push the bottom line so much that they
          endanger the very lives of their elderly patients. In fact, in Orange
          County alone, between June 2005 and August 2006, nursing homes were
          cited for over 660 health violations. If not caught early and
          remedied, these could result in lost lives and the abuse and neglect
          of our senior citizens.
        </p>
        <p>
          We must stop nursing homes like Freedom Village Healthcare Center from
          violating the rights and lives of the elderly placed in their care.
        </p>
        <p>Freedom Village Health Care's main violations include:</p>
        <ul>
          <li>Have a program to keep infection from spreading.</li>
          <li>Store, cook, and give out food in a safe and clean way.</li>
          <li>
            Make sure that residents who take drugs are not given too many doses
            or for too long;
            <ol>
              <li>make sure that the use of drugs is carefully watched; or</li>
              <li>stop or change drugs that cause unwanted effects.</li>
            </ol>
          </li>
          <li>
            Provide care in a way that keeps or builds each resident's dignity
            and self respect.
          </li>
          <li>
            Provide 3 meals daily at regular times; or
            <ol>
              <li>serve breakfast within 14 hours after dinner; or</li>
              <li>offer a snack at bedtime each day.</li>
            </ol>
          </li>
          <li>
            Give professional services that follow each resident's written care
            plan.
          </li>
        </ul>
        <p>This data is based off information reported by CMS as of 7/29/10.</p>
        <p>
          The disturbing failure to care for necessities such as food and
          medication warrants a deeper look into this nursing home and the
          overall epidemic going on in America.
        </p>
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several class depending on their severity. The state
          average is a little less than one per facility per year, but ideally,
          a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself.
        </p>
        <p>
          To understand what comprises nursing home abuse, there are essentially
          two categories: intentional harm and neglect. When an elderly person
          is neglected, this may be through failure to provide basic food or
          prescriptions that he or she needs to live and maintain overall
          wellbeing. Unattended living situations and special accommodations
          needed that are forgotten are also quite common and can lead to
          disease and death in the elderly. Some signs of neglect and abuse
          include bed sores, malnutrition and weight loss, medication overdose,
          ulcers, gangrene, and bodily injuries such as from a "slip and fall."
        </p>
        <p>
          Besides neglect, intentional harm is also quite common and can include
          sexual abuse, physical assault, and even emotional and financial
          abuse. These depressing crimes needs to be put to a stop and if you or
          your loved ones have experienced any of the above violations or others
          not listed here at this or another nursing home facility, you can get
          the help and compensation that you need. To find out immediate answers
          with a fast, free, no-obligation consultation, contact the nursing
          home abuse experts at Bisnar Chase Personal Injury Attorneys and to
          let you know your options and answer all your questions. You shouldn't
          have to pay a cent for this out of your own pocket. We are here to
          help you.
        </p>
        <p>
          If you feel you or a loved one has been the victim of nursing home
          abuse, please call us at: 949-203-3814.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
