// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Golden State Colonial Health Care Center - Nursing Home Abuse"
        pageDescription="Golden State Colonial has violated several nursing home violations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Golden State Colonial Healthcare Center</h1>
        <BreadCrumbs location={location} />
        <h2>Golden State Colonial Healthcare Center</h2>
        <h3>North Hollywood, California (Los Angeles County)</h3>
        <p>
          Elderly people are vulnerable to abuse, neglect and mistreatment. If
          an elderly resident suffers injury in a nursing home, or shows a
          notable decline in health, this should raise suspicions on the part of
          the family about the care their loved one is receiving at the
          retirement center. Bruises or fearful reactions to strangers may be a
          sign of abuse. Missing medication or personal possessions can be a
          sign of abuse or neglect as well.
        </p>
        <p>
          Knowing what to look for is crucial when suspicious about nursing home
          violations. Unfortunately, Golden State Colonial Healthcare Center in
          North Hollywood appears to have violated several important statutes by
          the Department of Health Services. North Hollywood borders the city of
          Hollywood and Studio City in the County of Los Angeles, North of
          Downtown L.A.
        </p>
        <p>
          The violations listed for Golden State Colonial retirement home
          include:
        </p>
        <ul>
          <li>
            Give each resident care and services to get or keep the highest
            quality of life possible.
          </li>
          <li>
            Give each resident enough fluids to keep them healthy and prevent
            dehydration.
          </li>
          <li>
            Make sure that each resident who enters the nursing home without a
            catheter is not given a catheter, unless it is necessary.
          </li>
          <li>Provide rooms that are big enough for each resident.</li>
        </ul>
        <p>
          The aforementioned information is based off data reported by CMS as of
          7/29/10.
        </p>
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several class depending on their severity. The state
          average is a little less than one per facility per year, but ideally,
          a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself. Golden State Colonial Healthcare Center is
          part of a nation-wide epidemic of nursing home abuse and neglect.
          These shameful violations of human rights can be stopped with your
          support.
        </p>
        {/* <p>Watch these videos to learn more:</p> */}
        {/* <h2>Videos Answer Questions on Elder Abuse</h2>
    <ul>
      <!-- <li><Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link></li> -->
      <!-- <li><Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing Home Abuse? </Link></li> -->
      <li><Link to="/nursing-home-abuse">Why Do People File Nursing Home Abuse Claims? </Link></li>
    </ul> */}
        {/* <p>
          The videos above illustrate the basics of nursing home abuse and how
          to spot the signs. Elder abuse can be classified as a case of neglect
          or intentional harm.
        </p> */}
        <p>
          Elderly abuse can occur in other forms than just physical abuse or
          neglect. Different forms of elderly abuse are sometimes harder to
          ascertain because physical evidence may not be present. The definition
          of elder abuse is: any act, failure to act, or incitement to act done
          willfully, knowingly, or recklessly through words or physical action
          which causes or could cause mental or physical injury or harm or death
          to an elderly resident. This includes verbal, sexual,
          mental/psychological, or physical abuse, including corporal
          punishment, and involuntary seclusion.
        </p>
        <p>
          If you know of someone who has been abused or neglected in an elder
          care facility such as Golden State Colonial in North Hollywood,
          California, you can get help. At Bisnar Chase Personal Injury
          Attorneys, we believe that nursing homes have a duty to provide the
          care that elderly individuals need and deserve. When a loved one
          suffers from nursing home abuse or neglect, we work tirelessly to
          expose the nursing home's violations and obtain just compensation for
          your loved one's suffering. Nursing home and elder abuse in California
          is a complicated matter that often requires a specialist.
        </p>
        <p>
          With over 30 years of experience, Bisnar Chase Personal Injury
          Attorneys, in Orange County, has had the experience to fully
          investigate nursing homes throughout California for the best case
          results possible for the victim and his or her family. Besides helping
          your loved one recover financial compensation for their pain, you can
          also improve safety laws in the nursing home industry with your voice.
        </p>
        <p>
          Call 949-203-3814 for the nursing home abuse attorneys at Bisnar Chase
          Personal Injury Attorneys to get the answers you are looking for.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
