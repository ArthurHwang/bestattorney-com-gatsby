// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Insurance Companies Scam Seniors - Elder Abuse Lawyers"
        pageDescription="Senior insurance scams...Call 949-203-3814 for highest-rated senior abuse lawyers. Free no-obligation legal consultations since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Insurance Companies Are Subjecting Vulnerable Seniors to Insurance
          Scams
        </h1>
        <BreadCrumbs location={location} />
        <p>
          Seniors are often the unwitting victims of frauds and scams by
          unscrupulous individuals. In the American Association for Justice's
          recent report entitled "Standing Up for Seniors: How the Civil Justice
          System Protects Elderly Americans," it appears that seniors are often
          the victims of
          <Link to="/nursing-home-abuse" target="_blank">
            {" "}
            nursing home neglect
          </Link>{" "}
          and of frauds perpetrated by large insurance companies as well.
        </p>
        <p>
          During the 1980s and 1990s Interstate Service Insurance Agency signed
          up thousands of seniors for long-term care policies with promises of
          low premiums that would stay fixed. Despite their promises, the
          insurance company increased the "policy rate" which had the effect of
          raising premiums for everyone. Because of this scam, seniors saw their
          premiums increase by as much as 800 percent!
        </p>
        <p>
          Unfortunately, there are many other such scams that were and are being
          perpetrated against seniors. In the 1990s, National Heritage Life
          Insurance Company defrauded elderly policyholders of $450 million.
          Prudential, one of the largest insurance companies in the country,
          scammed millions of seniors by tricking customers with existing life
          insurance policies into buying new policies that were more expensive
          and offered no additional benefits.
        </p>
        <p>
          United American agents, masquerading as representatives of federal
          agencies, pressured hundreds of seniors into buying insurance that
          cost more and provided less than the insurance they already had. In
          2003, United American settled charges that it had defrauded senior
          citizens in the sale of Medicare policies.
        </p>
        <p>
          Regulators have had a hard time handling these massive insurance
          industry scams. The civil justice system has taken up the slack. In
          the case of Interstate's scam, due to regulatory loopholes, regulators
          were unable to act. Attorneys hired by the victims however were able
          to obtain more than $12 million for more than 13,000 seniors who got
          the premiums rolled back and ensured future increases were banned.
        </p>
        <p>
          Because of this and other litigation brought by insurance lawyers for
          some <Link to="/nursing-home-abuse">nursing home abuse </Link>{" "}
          victims, insurance companies like Prudential, John Hancock, Met Life,
          and New York Life have all been forced to compensate policyholders for
          scams like the ones described above.
        </p>
        <p>
          If you suspect that you or someone you love has been the victim of an
          insurance scam, there is hope. Consult with an elder abuse lawyer, so
          you can determine the strength of your case and what you may be
          entitled to. Call Bisnar Chase Personal Injury Attorneys expert elder
          abuse attorneys, at 800-849-4905. The call is free. The advice may be
          priceless.
        </p>
        <p>
          <em>
            Call the{" "}
            <Link to="/los-angeles" target="_blank">
              {" "}
              personal injury lawyers in Los Angeles{" "}
            </Link>{" "}
            for a free professional evaluation of your rights by attorneys who
            have been representing victims of medical malpractice and senior
            abuse since 1978. You will experience award winning representation
            and outstanding personal service by a compassionate and
            understanding law firm in a comfortable environment.
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
