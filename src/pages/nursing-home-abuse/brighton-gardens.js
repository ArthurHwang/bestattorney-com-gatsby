// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brighton Gardens of Yorba Linda - Nursing Home Abuse Attorneys"
        pageDescription="Have complaints about nursing home abuse? Please call 949-203-3814 for a free consultation with a CA nursing home abuse attorney. No win, no fee. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Nursing Home Neglect & Abuse Attorneys</h1>
        <BreadCrumbs location={location} />
        <h2>Brighton Gardens of Yorba Linda</h2>
        <h3>17803 Imperial Highway Yorba Linda, CA 92886</h3>
        <p>
          When our elder loved ones move into nursing homes, long-term care
          facilities or board and care facilities, we trust that they will
          receive attention, medical care, proper nutrition and will be
          generally well-cared for. Unfortunately, this does not always happen,
          even in the best of facilities. Because of their special needs, the
          elderly are particularly vulnerable to illness and injury.
        </p>
        <p>
          Brighton Gardens of Yorba Linda has violated a shocking amount of laws
          and has received a very poor rating by Medicare. Brighton Gardens Of
          Yorba Lind in Yorba Linda California has a 60% occupancy rate with 27
          residents using its 45 beds. They are part of a multiple nursing home
          ownership and are a For Profit nursing home. The home is located in
          Orange County, California.
        </p>
        <p>Brighton Gardens of Yorba Linda violations include:</p>
        <ul>
          <li>
            Develop a complete care plan that meets all of a resident's needs,
            with timetables and actions that can be measured.
          </li>
          <li>
            Give each resident care and services to get or keep the highest
            quality of life possible.
          </li>
          <li>Give or get lab tests to meet the needs of residents.</li>
          <li>Have a program to keep infection from spreading.</li>
          <li>
            Have drugs and other similar products available, which are needed
            every day and in emergencies, and give them out properly.
          </li>
          <li>Make sure that staff members wash their hands when needed.</li>
          <li>
            Make sure that the nursing home area is free of dangers that cause
            accidents.
          </li>
          <li>
            Prepare food that is nutritional, appetizing, tasty, attractive,
            well-cooked, and at the right temperature.
          </li>
          <li>Provide activities to meet the needs of each resident.</li>
          <li>
            Provide care in a way that keeps or builds each resident's dignity
            and self respect.
          </li>
          <li>
            Provide services to meet the needs and preferences of each resident.
          </li>
          <li>Store, cook, and give out food in a safe and clean way.</li>
          <li>
            Write and use policies that forbid mistreatment, neglect and abuse
            of residents and theft of residents' property.
          </li>
        </ul>
        <p>
          How an elder care facility like this can cause such violations such as
          medical equipment and nutrition violations are completely astonishing,
          inexcusable and should not be tolerated.
        </p>
        <p>
          This information was based off data reported by CMS as of 7/29/10.
        </p>
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several classes depending on their severity. The
          state average is a little less than one per facility per year, but
          ideally, a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
        </ul>
        <p>
          This information should not be used as the sole measure of quality of
          care in a nursing home. It is important to visit potential facilities
          and evaluate it yourself.
        </p>
        <p>
          Brighton Gardens of Yorba Linda is part of a nation-wide epidemic of
          nursing home abuse and neglect which untouched will only continue and
          worsen. These outrageous violations of human rights can be stopped
          with your support. If you or someone you know has been affected, you
          can get the help you need which can also improve nursing home industry
          standards.
        </p>
        {/* <h3>Watch these videos to learn more:</h3> */}
        {/* <p>
          <strong> Videos Answer Questions on Elder Abuse</strong>
        </p> */}
        {/* <ul>
      <!-- {" "}<Link>{" "}<Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link> </Link> -->
      <!-- <li>{" "}<Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing
        Home Abuse? </Link></li> -->
    </ul> */}
        {/* <p>
          The informational videos above are several examples of nursing home
          abuse, which can be partitioned as either a case of neglect or
          intentional harm. The types of intentional harm in nursing homes
          include sexual assaults, physical abuse, emotional and verbal abuse,
          and even financial abuse. Elder neglect can include harming the
          patients with inadequate food, medicine, medical equipment, and proper
          accommodations. This can cause symptoms such as bedsores,
          malnutrition, overdose on medication or the wrong type of
          prescription, ulcers, and other bodily injuries.
        </p> */}
        <p>
          It is disheartening to learn about the kind of care a loved one has
          been receiving at a nursing home or other care facility. At Bisnar
          Chase Personal Injury Attorneys, we believe that nursing homes have a
          responsibility to provide the care that elderly individuals need. When
          a loved one suffers from nursing home abuse or neglect, we work hard
          to expose the nursing home's deficiencies and obtain just compensation
          for your loved one's suffering. Nursing home and elder abuse in
          California is a complicated matter which usually requires a
          specialist. With over 30 years of experience, Bisnar Chase Personal
          Injury Attorneys, in Orange County, has had the experience to
          investigate the signs and fully investigate what many miss in order to
          get the best case results possible for the victim.
        </p>
        <p>
          We are here to help. In addition to helping your loved one recover
          financial compensation for their pain and suffering, our work has the
          added benefit of often bringing about changes within the nursing homes
          themselves. Call the nursing home abuse specialists at Bisnar Chase
          Personal Injury Attorneys to let you know of your options, for free.
        </p>
        <p>
          If you feel you have been the victim of nursing home neglect or abuse,
          please call 949-203-3814 to discuss your options.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
