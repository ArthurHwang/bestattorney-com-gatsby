// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nursing Home Abuse: How They Falsify Your Loved One's Medical Records"
        pageDescription="Was your loved one not cared for or abused in a nursing home? Call 949-203-3814 for qualified nursing home abuse attorneys. Free consultations. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          The Case of Johnnie Esco: How Nursing Homes Falsify Your Loved One's
          Medical Records
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The death of 77-year-old Johnnie Esco on March 7, 2008 after spending
          13 days at a Placerville nursing home has opened the California
          public's eyes to a phenomenon many never knew existed – the
          falsification of patient records in nursing homes. According to a
          series of investigative articles in The Sacramento Bee, patients'
          records being altered in nursing homes is a dangerous practice that is
          not much talked about or discussed. The Bee reviewed nearly 150 cases
          of alleged chart falsification and found that this practice puts
          patients at serious risk of injury and sometimes leads to death.
        </p>
        <h2>A "High Degree of Deception"</h2>
        <p>
          It ended tragically in the case of Johnnie Esco, who died after a mere
          13 days at the El Dorado Care Center in Placerville. Esco was
          recuperating from pneumonia and was expected to return home after rest
          and skilled nursing care. Instead, she died as a result of a blood
          clot that traveled to her lungs and "acute fecal impaction." The case,
          among other things, raised questions about the integrity of Johnnie
          Esco's medical records. In his wrongful death lawsuit, Don Esco,
          Johnnie's husband, alleges that the nursing home fabricated a lot of
          her records and participated in a "high degree of deception."
        </p>
        <p>
          The depositions recorded for the lawsuit told a very disturbing story.
          Several staff members conceded that "charting" at the nursing home was
          not always done precisely or carefully. A certified nurse assistant
          who helped care for Johnnie Esco said during a deposition that she was
          so busy that she did not have time to fill patients' charts. She and
          another nurse who worked for the same facility they saw several
          workers "rote charting," which is a term for hurriedly filling in
          boxes identical to the previous day's shift.
        </p>
        <h2>Unethical Charting</h2>
        <p>
          Just in the 13 days that she was there, Johnnie Esco's condition
          deteriorated from smiling and "dancing to country music" to critically
          ill. Her husband visited her everyday and quizzed the workers and they
          assured him everyday that everything was just fine. Even as doctors
          wrote orders to put her on laxatives after noticing that she was
          suffering from acute constipation, the nurses continued to check the
          boxes and move on as if nothing had happened.
        </p>
        <p>
          For example, the day before Johnnie Esco's death, the nursing had
          marked on her chart that she had an extra large bowel movement. Her
          autopsy revealed that she had not had a bowel movement in days. The
          night before her death, paramedics found Johnnie's abdomen distended
          and rigid with pain upon touch. And yet, the nursing home's charts
          documented her daily pain as "zero," which her family says was
          fabricated. When she was taken to the hospital from the nursing home,
          the fecal impaction was so severe that her rectum had dilated to 10
          centimeters or about 4 inches. A CT scan even revealed that she had
          undigested pills lodged in her colon. Her family members were told by
          hospital staff that she could need surgery for her bowel obstruction,
          but the risks of such a surgery would be very high.
        </p>
        <h2>Holding Nursing Homes Accountable</h2>
        <p>
          There are several reasons why nursing homes falsify or alter patients'
          records. First, they may do so to hide bad outcomes such as injuries
          or death. As with Esco's case, nurses in facilities that are terribly
          understaffed may not have time to meticulously fill charts. This means
          that they may simply check the boxes, copy the previous day's notes or
          make up things as they go along. The Bee investigation also talks
          about cases where misleading information about medications is posted
          on patient charts. In addition, some nursing homes have forged family
          members' signatures in order to sedate or chemically restrain
          patients.
        </p>
        <p>
          Proving these types of cases can be extremely challenging. It takes a
          resourceful, skilled and experienced California nursing home abuse
          lawyer to independently investigate these cases and discover these
          fraudulent practices. The experienced California nursing home abuse
          attorneys at Bisnar Chase Personal Injury Attorneys strongly believe
          that nursing homes that falsify patient records and defraud patients
          and families must be held accountable. If you believe that your loved
          one is being abused or neglected at a nursing home or that the
          facility is covering up information, please contact us for more
          information about your legal rights and options. In such cases, the
          only option for patients and their families is to hit these nursing
          homes where it hurts the most – their pocket books. We will use all
          the resources we have to help prove your case and ensure that justice
          is served.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
