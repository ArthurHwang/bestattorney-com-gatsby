// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Nursing Home Residents Victim to Bed Rail Deaths"
        pageDescription="Bed Rail Deaths...Call 949-203-3814 for highest-rated nursing home neglect lawyers, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bed Rail Deaths</h1>
        <BreadCrumbs location={location} />
        <p>
          In the American Association for Justice's recent report entitled
          "Standing Up for Seniors: How the Civil Justice System Protects
          Elderly Americans," there is a discussion of bed rails and the
          startling safety hazard they pose to{" "}
          <Link to="/nursing-home-abuse" target="_blank">
            {" "}
            nursing home residents
          </Link>
          . Many people believe bed rails to be a safe device meant to keep
          sick, drugged, confused seniors from falling out of beds in hospitals
          and nursing homes. Though these bed rails are presumably placed on the
          beds for the safety and security of seniors, the sad reality is that
          hundreds of seniors have been killed in connection with these bed
          rails.
        </p>
        <p>
          Bed rail deaths are often the results of poor design, and nursing home
          residents are sometimes trapped between or under the rails themselves,
          or in gaps between the mattress' edges and the bed frame. However,
          injuries are not only the result of poor design. Many times seniors
          are injured or killed by bed rails where their use was unnecessary.
        </p>
        <p>
          In a March 10, 2010 New York Times article entitled "Safe in Bed?"
          geriatrician and bioethicist Steven Miles of the University of
          Minnesota, questioned the logic of keeping seniors in beds with rails.
          He states "Rails decrease your risk of falling by 10 to 15 percent,
          but they increase the risk of injury by about 20 percent because they
          change the geometry of the fall." Seniors will try to climb over the
          bed rails and instead of falling from the lower level and landing on
          their knees or legs, they are falling farther and more likely to
          strike their heads.
        </p>
        <p>
          Though these dangers are well-known to nursing homes, few government
          regulations exist to stem the problem of bed rail and the ones that do
          are barely a slap on the wrist for nursing homes. The civil justice
          system is working to help the problem though. As Steven Miles states
          "Government sanctions cost a couple of thousand bucks. A lawsuit can
          cost $500,000 to $1 million; it gets much more attention."
        </p>
        <p>
          Costly lawsuits get the attention of nursing homes. The result is that
          lawsuits over poorly designed bed rails and/or the inappropriate use
          of bed rails have greatly reduced their use. In one case, a
          63-year-old Alzheimer's patient named Billie Trew was strangled to
          death by the restraints on her bed while sleeping. Her family brought
          the nursing home and the bed manufacturer to court. In the case of
          Billie Trew, the nursing home agreed to reduce the use of bed rails,
          and the manufacturer agreed to warn customers about the dangers of
          entrapment. The result was a 90% reduction in the use of restraints.
        </p>
        <h3> Receiving Justice for Bed Rail Injury or Death</h3>
        <p>
          Do you think you or a loved one has been the victim of a defective or
          inappropriately used bed rail? Do you want to know your rights? Want
          to know what your case is worth? Want compensation? Want justice?
          Would you like to ensure the same thing doesn't happen to other senior
          citizens?
        </p>
        <p>
          If so, call the{" "}
          <Link to="/nursing-home-abuse">nursing home abuse law firms </Link> of
          Bisnar Chase Personal Injury Attorneys at 800-849-4905. The call is
          free. The advice may be priceless.
        </p>
        <p>
          <em>
            Call the Bisnar Chase Personal Injury Attorneys{" "}
            <Link to="/los-angeles" target="_blank">
              {" "}
              personal injury lawyers in Los Angeles{" "}
            </Link>{" "}
            for a free professional evaluation of your rights by attorneys who
            have been representing victims of medical malpractice and senior
            abuse since 1978. You will experience award winning representation
            and outstanding personal service by a compassionate and
            understanding law firm in a comfortable environment.
          </em>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
