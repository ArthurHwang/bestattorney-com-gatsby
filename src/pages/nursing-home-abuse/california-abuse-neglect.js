// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Nursing Home Abuse Lawyers - Elder Abuse Attorneys"
        pageDescription="California nursing home abuse lawyers with 35 years experience in elder abuse. Our attorneys offer a free consultation & complete privacy. 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Abuse and Neglect of the Elderly</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            {" "}
            For immediate help with a California nursing home abuse case please
            call 949-203-3814.
          </strong>
        </p>
        <p>
          A study in 2009 found that 1 in 7 senior citizens were abused. Many
          dont report it out of fear. Nursing home abuse is not only a crime but
          it's something loved ones and family members can seek compensation
          for.
        </p>
        <p>
          Our nursing home abuse lawyers have spent 35 years fighting for the
          rights of our clients, whether it's elder abuse or nursing home
          neglect cases. Crimes on the elderly are devastating to family members
          because of the sheer odds against a fragile person who may be afraid
          to speak up.
        </p>
        <p>
          If you have a loved one whose suffered at the hands of others and
          think you may have an elder abuse case, please contact us right away
          to preserve your rights.
        </p>
        <p>
          <strong> Some of the types elder abuse and neglect cases are:</strong>
        </p>
        <ul>
          <li>
            Stage IV pressure ulcers{" "}
            <Link
              to="/nursing-home-abuse/bedsores"
              name="bedsores affect elderly in most abuse cases."
            >
              (severe bedsores)
            </Link>
          </li>
          <li>
            Falls with serious injuries (fractured hips, fractured knees, head
            injuries)
          </li>
          <li>
            Dehydration/malnutrition with acute side effects like kidney failure
          </li>
          <li>Physical/sexual abuse cases</li>
          <li>Wrongful death</li>
          <li>
            Sepsis or other infections (like urinary tract infections) with
            serious side effects
          </li>
          <li>
            Residents who are chemically restrained with serious consequences
          </li>
          <li>Elopement/wandering resulting in serious injury</li>
        </ul>
        <p>
          We charge no fee unless we recover money for you. We advance all
          litigation costs. Our reputation will provide you the clout you need
          to hold wrongdoers responsible for their actions. If you have fallen
          victim to Nursing Home Abuse or Neglect and you would like to discuss
          your case with a qualified and caring Lawyer, please call us
          949-203-3814.
        </p>
        <p>
          <strong>
            {" "}
            Video: Personal Injury Attorney John Bisnar discusses why family
            members file nursing home abuse cases.
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/vk19nU2G7io"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* <h2>Videos Answer Questions About Elder Abuse</h2>
    <ul>
      <!-- <li>{" "}<Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link></li> -->
      <!-- <li>{" "}<Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing
        Home Abuse? </Link></li> -->
    </ul> */}
        <p>
          With the aging of America, more and more people are less able to take
          care of themselves and their affairs. In most situations, family,
          friends, for profit corporations and social organizations provide the
          care and guidance that elders need and deserve.
        </p>
        <p>
          However, when friends and relatives cannot provide adequate care, and
          the elder cannot provide for himself, the decision may be made to
          place him or her in a <strong> nursing home</strong> or
          <strong> assisted living facility</strong>. Unfortunately, in a
          growing number of cases, elders are being abused, physically,
          emotionally and financially while under the care of a nursing home or
          assisted living facility. In addition, elders may be abused or
          neglected in their own home by hospice employees or assistance nurses.
        </p>
        <h2>Elder Abuse Can Affect Anyone</h2>
        <p>
          You may recall the story of aging actor and Hollywood legend Mickey
          Rooney coming out to speak to congress about elder abuse. Mickey was
          emotional as he explained what he had been through. Mickey says he was
          often told to shut up, lost access to basic needs like food and
          medication, even having his identification taken away from him so he
          couldn't travel. His story touches on the 700,000 to 3.5 million who
          are abused or neglected each year.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/W9ikKP5-s5A?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Recognizing the growing tide of elder abuse in California, the State
          of California enacted The <strong> Adult Civil Protection Act</strong>{" "}
          to give extra protection to elders.
        </p>
        <p>
          Also recognizing the vulnerability of elders and the growing incidence
          of nursing home neglect, Bisnar Chase Personal Injury Attorneys has
          taken on the social responsibility of representing abused and
          neglected elders in cases involving nursing homes and assisted living
          establishments.
        </p>
        <p>
          We have successfully represented thousands of injury clients,
          recovering tens of millions of dollars over the years. We thrive on
          helping victims of personal injury bring wrongdoers to justice.
        </p>
        <p>
          Nursing home abuse falls into two categories: intentional harm and
          neglect. The intentional harm includes physical attacks, sexual
          assaults, emotional abuse and financial abuse. Nursing home abuse or
          neglect includes failure to provide for basic needs which can result
          in serious health issues like dehydration, weight loss, overdosing
          medication, life threatening falls, infections, bedsores, serious skin
          ulcers, decubitus ulcers, osteomyelitis, septicemia and gangrene.
        </p>
        <p>
          <strong> Nursing homes must:</strong>
        </p>
        <ol>
          <li>
            Care for the residents in such a manner and in such an environment
            as will promote maintenance or enhancement of the quality of life of
            each resident;
          </li>
          <li>
            Provide services and activities to attain or maintain the highest
            practicable physical, mental, psycho social well-being of each
            resident in accordance with a written plan of care. The plan should
            describe the medical, nursing and psycho-social needs of the
            resident and is to be periodically reviewed and updated;
          </li>
          <li>
            Conduct a comprehensive assessment that describes the resident's
            capability to perform daily functions and significant impairments.
          </li>
        </ol>
        <h2>California Nursing Homes With History of Abuse</h2>
        <p>
          In the year 2000, the State Department of Justice launched a program
          to help identify nursing homes that fail to adhere to California
          health and safety codes. The following nursing homes were found to
          offer substandard levels of care and may be prone to nursing home
          neglect and elder abuse.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
