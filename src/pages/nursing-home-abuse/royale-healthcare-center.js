// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Royale Health Care Center - Nursing Home Abuse"
        pageDescription="Nursing Home Abuse Complaints? Information on Royale Nursing Home Neglect and Abuse."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Royale Health Care Violations</h1>
        <BreadCrumbs location={location} />
        <h2>Royale Health Care Center</h2>
        <h3>1030 W. Warner Ave. Santa Ana, CA</h3>
        <p>
          As we watch our grandparents, parents, aunts and uncles grow older, we
          shouldn't have to worry about these loved ones falling victim to
          nursing home abuse. With a plethora of ailments to look out for, we
          have enough to worry about. We look to medication, doctors, diets, and
          exercise regimes-anything we can find-in hopes of keeping them healthy
          and content and able to live as independently as they wish to. When
          their complications turn too severe, we turn to an assisted living or
          nursing home facility to give them the same care we had provided
          before.
        </p>
        <p>
          Many of these facilities provide superior care, however, far too many
          do not. Often understaffed with poorly trained employees working at
          minimum wage, many nursing homes push the bottom line so much that
          they endanger the very lives of their elderly patients. Here are
          violations made by Royale Health Care Center in Santa Ana, located in
          Orange County California. We must stop nursing homes like Royale
          Health Care Center from violating the rights and lives of the elderly
          placed in their care.
        </p>
        <img
          src="/images/nursing-home-abuse-royale-healthcare.jpg"
          alt="Nursing Home Abuse"
          className="imgright-fixed"
        />
        <p>Royale Health Care's main violations include:</p>
        <ul className="bullet-inside">
          <li>
            Properly hold, secure and manage each resident's personal money
            which is deposited with the nursing home.
          </li>
          <li>
            Give proper treatment to residents with feeding tubes to prevent
            problems (such as aspiration pneumonia, diarrhea, vomiting,
            dehydration, metabolic abnormalities, nasal-pharyngeal ulcers) and
            help restore eating skills, if possible.
          </li>
          <li>Provide 3 meals daily at regular times; or</li>
          <li>Serve breakfast within 14 hours after dinner;</li>
          <li>Offer a snack at bedtime each day.</li>
          <li>
            Make sure that residents who take drugs are not given too many doses
            or for too long;
          </li>
          <li>Make sure that the use of drugs is carefully watched;</li>
          <li>Stop or change drugs that cause unwanted effects.</li>
        </ul>
        <p>This data is based off information reported by CMS as of 7/29/10.</p>
        <p>
          The appalling failures to care for necessities such as food and
          medication warrants a deeper look into this nursing home and the
          overall epidemic going on in America. In fact, in Orange County alone,
          between June 2005 and August 2006, nursing homes were cited for over
          660 health violations. If not caught early and remedied, these could
          result in lost lives and the abuse and neglect of our senior citizens.
        </p>
        {/* <h2>Watch these videos for more information on Nursing Home Abuse:</h2>
    <ul>
      <!-- <li><Link to="/videos/elder-abuse-problems.html">Is Elder Abuse a Big Problem? </Link></li> -->
      <!-- <li><Link to="/videos/nursing-home-neglect-clues.html">What Are the Clues to Nursing Home Abuse? </Link></li> -->
      <li><Link to="/nursing-home-abuse/california-abuse-neglect.html">Why Do People File Nursing Home Abuse Claims? </Link></li>
    </ul> */}
        <p>
          Citations are issued by the Department of Health Services (DHS) during
          the annual certification visit.
        </p>
        <p>
          Citations come in several class depending on their severity. The state
          average is a little less than one per facility per year, but ideally,
          a facility should not have any citations.
        </p>
        <ul>
          <li>
            Class AA: The most serious violation, AA citations are issued when a
            resident death has occurred in such a way that it has been directly
            and officially attributed to the responsibility of the facility, and
            carry fines of $25,000 to $100,000.
          </li>
          <li>
            Class A: class A citations are issued when violations present
            imminent danger to patients or the substantial probability of death
            or serious harm, and carry fines from $2,000 to $20,000.
          </li>
          <li>
            Class B: class B citations carry fines from $100 to $1000 and are
            issued for violations which have a direct or immediate relationship
            to health, safety, or security, but do not qualify as A or AA
            citations.
          </li>
          <li>
            This information should not be used as the sole measure of quality
            of care in a nursing home. It is important to visit potential
            facilities and evaluate it yourself.
          </li>
        </ul>
        <p>
          So what comprises nursing home abuse? There are essentially two types:
          intentional harm and neglect. When a senior is neglected, this may be
          through failure to provide basic food or prescriptions that he or she
          needs to live and maintain overall wellbeing. Unattended living
          situations and special accommodations needed that are forgotten are
          also quite common and can lead to disease and death in the elderly.
          Some signs of neglect and abuse include bed sores, malnutrition and
          weight loss, medication overdose, ulcers, gangrene, and bodily
          injuries such as from a "slip and fall."
        </p>
        <p>
          Besides neglect, intentional harm is also common and can include
          sexual abuse, physical assault, and even emotional and financial
          abuse.
        </p>
        <p>
          These saddening crimes needs to be stopped and if you or your loved
          ones have experienced any of the above violations or others not listed
          here at this or another nursing home facility, you can get the help
          and compensation that you need. To find out immediate answers with a
          fast, free, no-obligation consultation, contact the nursing home abuse
          experts at Bisnar Chase Personal Injury Attorneys to let you know your
          options and answer all your questions.
        </p>
        <p>Call us for a free consultation: 949-203-3814</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
