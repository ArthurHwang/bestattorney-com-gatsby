// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Vacaville Nursing Home Abuse Attorneys - Vacaville Nursing Home Attorneys"
        pageDescription="Are you searching for a Vacaville nursing home attorney? Call 949-203-3814 for a free attorney consultation. Years of experience & results."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Safety & Health Issues Winsor House Care Center Vacaville</h1>
        <BreadCrumbs location={location} />
        <p>
          There are many dangerous situations at nursing homes that can lead to
          serious injuries or illnesses. Some facilities fail to properly manage
          the pain level of their suffering patients. Others fail to properly
          control sanitation and health issues, which can result in potentially
          fatal infections. It is even common for nursing homes to use harmful
          physical restraints. Many of these hazardous conditions were found at
          the{" "}
          <Link to="/nursing-home-abuse">
            Winsor House Care Center in Vacaville
          </Link>
          .
        </p>
        <p>
          On March 18, 2011, a surprise inspection was performed at the Winsor
          House Care Center by members of the Operation Guardians task force.
          During their tour of the facility, the inspectors reviewed the
          conditions of the building, the actions of the workers and the
          residents' living conditions. Here are some of their findings that are
          cause for serious concern.
        </p>
        <h2>Issues with the Facility</h2>
        <p>
          There are many potentially dangerous situations listed in the report.
          According to inspectors:
        </p>
        <ul>
          <li>
            Drawers of the treatment cart were unlocked. This is a safety issue.
          </li>
          <li>
            Several resident beds had half side rails in the middle of the bed
            frame. This is a form of restraint. The physician orders "did not
            include an order for half side rails or for a restraint."
          </li>
          <li>
            The oxygen storage had unsecured and poorly labeled oxygen tanks.
            This could pose an issue in the event of an emergency.
          </li>
          <li>
            Some of the rooms had damaged floor tiles, which could pose a
            serious tripping hazard.
          </li>
          <li>
            In some cases, emergency exits were blocked by wheelchairs or food
            carts.
          </li>
          <li>
            The shower room was dirty, posing a safety and infection issue.
          </li>
        </ul>
        <h2>Resident Care Issues</h2>
        <p>
          Here are four examples of residents at the facility and the care they
          were receiving:
        </p>
        <ul>
          <li>
            A resident was transferred to an acute care hospital for
            complications suffered after receiving the wrong medication at the
            facility. There was a lacked of detailed charting regarding the
            incident.
          </li>
          <li>
            One resident was suffering from severe, continuing weight loss.
            Inspectors believe this serious situation was directly linked to the
            facility's failure to provide proper denture care. The resident had
            improperly fitting dentures and the facility had failed to properly
            evaluate her for new dentures.
          </li>
          <li>
            On the day of the inspection, one resident cried throughout
            breakfast and lunch causing a disruption to the other residents. Her
            Care Plan showed absolutely no arrangement to evaluate or treat the
            troubled resident.
          </li>
          <li>
            Another resident was ordered to receive a speech screening and
            evaluation despite having no difficulty with swallowing or speaking.
            Additionally, it was not clear why the resident was still receiving
            24-hour care when documentation showed that she had improved through
            physical therapy. Her Social Services documentation did not reflect
            plans for her discharge.
          </li>
        </ul>
        <p>
          There were also a number of instances regarding a failure to provide
          proper pain management as part of end-of-life care. Also, many of the
          residents were receiving antibiotics, which may indicate infection
          issues at the facility.
        </p>
        <h2>Other Concerns with the Nursing Home</h2>
        <p>
          The facility administrator consulted with the facility's legal counsel
          before choosing to deny inspectors the ability to review the Wound
          Care Log, the Infection Control Log or the Incident/Accident Log.
          Inspectors did, however, review the treatment book, which showed
          several blank days for many residents. This indicates that treatments
          had not been performed on those days. There were also a number of days
          in which the workers made medication errors. It is unclear if those
          mistakes were reported to the appropriate authorities. Last but not
          the least, staffing levels were below the minimum required 3.2 hours
          per resident day on one of the four randomly reviewed days.
        </p>
        <h2>Protecting the Rights of Nursing Home Residents</h2>
        <p>
          If you worry that your loved one has suffered injury or harm at the
          Winsor House Care Center or another potentially dangerous nursing
          home, please do not hesitate to discuss your rights with an
          experienced{" "}
          <Link to="/nursing-home-abuse">California nursing home attorney</Link>
          . The skilled Vacaville nursing home abuse lawyers at our law firm
          have a long and successful track record of holding negligent nursing
          homes accountable. Please contact us today to find out how we can help
          you and your family obtain justice and fair compensation for your
          losses.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
