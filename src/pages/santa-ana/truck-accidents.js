// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/santa-ana-truck-accident-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Truck Accident Lawyers - Semi Truck Accident Attorneys"
        pageDescription="Please contact our Santa Ana truck accident lawyers if you've been injured in a no fault truck accident. Call 949-203-3814. $500 million in winnings, 96% success.Free Consultation. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Truck Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana truck accident lawyers"
            title="Santa Ana truck accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced <strong> Santa Ana Truck Accident Lawyers </strong>at{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          help injured victims or families of deceased victims pursue personal
          injury and wrongful death lawsuits to recover compensation for their
          losses.
        </p>
        <p>
          We can help review the trucking company's logs to determine if driver
          fatigue caused the accident. We will take the steps necessary to
          ensure that key evidence such as driver logs and vehicle maintenance
          logs are preserved in the aftermath of a truck accident.
        </p>
        <p>
          <strong>
            {" "}
            Our attorneys will fight for your rights and help ensure that your
            legal rights are protected
          </strong>
          . The Personal Injury Law Firm of Bisnar Chase has achieved a
          <strong> 96% success rate for over 40 years</strong>.
        </p>
        <p>
          If you or a loved one has been injured by a fatigued or negligent
          truck driver, please contact an experienced Santa Ana truck accident
          lawyer to obtain more information about your legal rights and options.
        </p>
        <p>
          <strong>
            {" "}
            Contact the Santa 949-203-3814 for a free consultation.
          </strong>
        </p>
        <h2>Compensation Santa Ana Injury Victims are Entitled To</h2>
        <p>
          Accident victims who have endured serious injuries and families who
          have lost a loved one are deserving of the compensation they have
          faced after the tragedy. Your Santa Ana truck accident attorney can
          win you the earnings you and your family need to overcome this
          obstacle.
        </p>
        <p>
          <strong>
            {" "}
            Truck accident victims can gain compensation for financial losses
            such as
          </strong>
          :
        </p>
        <ul>
          <li>Pain and suffering</li>
          <li>Lost wages</li>
          <li>Future earnings</li>
          <li>Emotional distress</li>
          <li>Medical expenses</li>
        </ul>
        <p>
          The Santa Ana trucking accident attorneys of Bisnar Chase have taken
          on the toughest of injury cases in the Orange County area. Our
          attorneys have helped people who have been left with serious spinal
          cord and brain injury. Insurance companies do not always fight
          vigorously for your rights. When you contact the law offices of Bisnar
          Chase at <strong> 949-203-3814</strong> you will receive a free case
          analysis.
        </p>
        <h2>
          Federal Regulations Aim To Prevent Driver Fatigue Amongst Truckers
        </h2>
        <p>
          Driver fatigue is often a prominent factor of devastating commercial
          truck accidents. There are a number of studies that have found that
          drivers in commercial trucking, especially truck drivers, who travel
          long distances often under pressure to deliver their freight under
          deadline, are sleep-deprived.
        </p>
        <p>
          Truck drivers who are sleepy or fatigued are more likely to cause a
          crash, according to studies conducted by the Insurance{" "}
          <Link to="https://m.iihs.org/mobile" target="_blank">
            {" "}
            Institute for Highway Safety
          </Link>{" "}
          (IIHS). Their research found that truck drivers who are behind the
          wheel for more than eight hours have a twofold increase in crash risk.
          IIHS also found that truck drivers reporting hours-of-service
          violations are more likely to report having fallen asleep behind the
          wheel during the past month.
        </p>
        <p>
          Federal regulations require drivers of commercial trucks to get
          sufficient rest before getting back on the road. These regulations are
          there for a reason.
        </p>
        <p>
          <strong>
            {" "}
            A sleepy truck driver behind the wheel of an 80,000-pound vehicle
            can cause significant damage
          </strong>
          .
        </p>
        <p>
          The{" "}
          <Link to="https://www.fmcsa.dot.gov/" target="_blank">
            {" "}
            Federal Motor Carrier Safety Administration
          </Link>{" "}
          (FMCSA), which monitors trucking companies, revised the
          hours-of-service (HOS) safety requirements for commercial truck
          drivers.
        </p>
        <p>
          <strong> Revisions made by FMCSA included</strong>:
        </p>
        <ol>
          <li>
            The new HOS rule reduced by 12 hours the maximum number of hours a
            truck driver can work within a week.
          </li>
          <LazyLoad>
            <img
              alt="truck swerving off the side of the road"
              width="100%"
              title="Truck crash lawyers in Santa Ana"
              src="/images/truck-accidents/truck-accident-santa-ana.jpg"
            />
          </LazyLoad>
          <li>
            In addition, truck drivers are not allowed to drive after working
            eight hours without first taking a break of at least 30 minutes.
          </li>
          <li>
            The rule also requires truck drivers who maximize their weekly work
            hours to take at least two nights' rest when their 24-hour body
            clock demands sleep the most - from 1 a.m. to 5 a.m. This is part of
            the rule's "34-hour restart" provision that allows drivers to
            restart the clock on their work week by taking at least 34
            consecutive hours off-duty.
          </li>
        </ol>
        <p>
          Trucking companies and drivers are required to keep logs showing how
          many hours the driver has worked and how many hours of rest or how
          many breaks were taken and when they were taken.
        </p>
        <p>
          <strong>
            {" "}
            Trucking companies that allow drivers to exceed the 11-hour driving
            limit by three or four hours could face up to $11,000 in fines per
            offense
          </strong>
          .
        </p>
        <p>
          The drivers could face civil penalties of up to $2,750 per offense.
          Trucking companies and their employees will be required to comply with
          this HOS rule by July 1, 2013.
        </p>
        <h2>Common Types of Truck Collisions</h2>
        <p>
          Truck collisions can leave victims with severe injuries and even are
          the cause of many deaths in motor vehicle accidents. Not all
          semi-truck accidents are caused the same way though. If a trucker is
          not paying attention to the road or operating the big-rig safely a
          serious wreck can occur.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Below you will find the most common kinds of truck accidents that
            take place
          </strong>
          .
        </p>
        <p>
          <strong> Truck rollovers</strong>: Rollovers occur when a trucker is
          not driving the big rig with caution. Truckers at times need to meet
          deadlines and to meet these deadlines they often take unsafe measures
          to do so. When a trucker is speeding and is approaching a curve they
          have an increased chance of flipping over. It is easy for truckers who
          do not have years of experience to underestimate a turn.
        </p>
        <p>
          <strong> Jackknife</strong>: Semi-trucks can weigh up 80,000 pounds.
          If the truck is suddenly skids in the road the trailer can swing and
          form a 90-degree angle. When a truck is "jackknifing" it can put other
          drivers in danger. The trucker can lose control of the semi and the
          trailer can swing onto oncoming traffic. Jackknifing takes place when
          the wheels of a truck are locked and is then driven over a slippery
          road.{" "}
        </p>
        <p>
          <strong> Blind spots</strong>: Big rigs in comparison to four doors
          are large in size. You must be cautious when driving around a truck.
          Do not follow too closely to semi-trucks. When you tailgate a
          semi-truck you are potentially driving in a blind spot. Trucks that
          are 18-wheelers are very large in height and if you are too close this
          can lead you to miss traffic lights
        </p>
        <h2>Insurance Companies Do Not Have Your Best Interest </h2>
        <p>
          Immediately after an accident, one of the first entities that will
          reach out to you will be the opposing parties insurance company. They
          will try to lure you into an offer that they think is more than fair.
          Do not accept the initial offer.{" "}
        </p>
        <p>
          Compensation that insurance companies offer will not pay for the
          property damage or the medical bills you acquired after your accident.
          The best path to take for earning the compensation you deserve is to
          hire a truck accident attorney in Santa Ana.{" "}
        </p>
        <p>
          Bisnar Chase's Santa Ana Truck Accident Lawyers have been representing
          clients in the Los Angeles, Irvine, Orange County and Inland Empire
          communities for over 40 years. When you call today you will receive a
          free consultation with one of our legal team experts.{" "}
        </p>
        <h2>Hiring an Experienced Truck Collision Lawyer </h2>
        <p>
          The most common reason why drivers may falsify HOS logs or violate{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Hours_of_service"
            target="_blank"
          >
            {" "}
            HOS laws
          </Link>{" "}
          is when they are given unrealistic deadlines for delivery. Trucking
          companies may turn a blind eye to these violations because they are
          focused on their profits. The quicker the delivery and the more
          deliveries made, the more money they make. Profits are not usually the
          only motive.
        </p>
        <p>
          <em>
            Sometimes, truck drivers "push through" fatigue to avoid rush-hour
            traffic or bad weather
          </em>
          . They may drive during the early morning hours when their bodies may
          have given up and need to rest the most.
        </p>
        <p>
          <strong> This could cause devastating truck accidents.</strong>
        </p>
        <p>
          Truck drivers who are working over-time and lying on their logs to
          meet a deadline can be seen as reckless and negligent.
        </p>
        <p>
          When a person is injured or killed as a result of a truck accident
          caused by driver fatigue, both the driver and his or her employer can
          be held liable for the injuries or wrongful death. It is often the
          occupants of the smaller vehicle who get significantly injured in
          large truck accidents.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Truck accident injury attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('../images/text-header-images/santa-ana-text-banner-pedestrian.jpg')"
            }}
          >
            <h2>Taking Legal Action After a Santa Ana Truck Collision</h2>
          </div>
        </LazyLoad>
        <p>
          Bisnar Chase's{" "}
          <strong>
            {" "}
            Santa Ana{" "}
            <Link to="/truck-accidents" target="_blank">
              {" "}
              Truck Accident Lawyers
            </Link>
          </strong>{" "}
          can help review the logs that are preserved in the aftermath of a
          truck accident. Our clients have also received compensation that
          amounted to millions of dollars.
        </p>
        <p>
          We will take the steps necessary to ensure that key evidence such as
          driver logs and vehicle maintenance logs are preserved in the
          aftermath of a truck accident. Our Santa Ana truck collision attorneys
          have been specializing in truck accident cases and other practice
          areas for over <strong> 40 years</strong>.
        </p>
        <p>
          If you have been injured in an accident involving a large truck and
          you believe you have a personal injury claim,{" "}
          <strong>
            {" "}
            please <Link to="/contact">contact us</Link> at 949-203-3814 for a
            Free Consultation.
          </strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
