// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "slip-and-fall/santa-ana-slip-fall-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Slip and Fall Lawyer - Orange County, CA"
        pageDescription="Call 949-203-3814 for top rated slip and fall attorneys in Santa Ana. Get a Free Consultation regarding your premise liability injury claim. Experienced lawyers for over 40 years. Successful results since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Slip and Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana slip and fall lawyers"
            title="Santa Ana slip and fall lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Sorting through legal issues after an accident is a complex matter.
          Hiring the <strong> Santa Ana Slip and Fall Accident Lawyers </strong>
          of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          shouldn't have to be. Our legal representation has won over{" "}
          <strong> $500 million in compensation</strong> for our injury victims
          for <strong> 40 years</strong>.
        </p>
        <p>
          We understand that after a slip and fall you may become disabled and
          rack up hefty medical expenses. The Personal Injury Law Firm of Bisnar
          Chase have held a<strong> 96% percent success rate</strong> and is
          determined to win you the earnings you need for the damages you have
          faced.
        </p>
        <p>
          When you call our offices, you will receive a free consultation with
          one of our top legal experts. Contact our Santa Ana slip and fall
          accident attorneys today at <strong> 949-203-3814</strong>.
        </p>
        <h2>Proving Fault in Santa Ana Slip and Fall Cases</h2>
        <p>
          In most Santa Ana cases, the individual injured in a{" "}
          <Link
            to="/premises-liability/slip-and-fall-accidents"
            target="_blank"
          >
            {" "}
            slip and fall
          </Link>{" "}
          on someone else's property must establish that the cause of the
          misfortune was a "treacherous condition", and that the owner or
          possessor of the property knew of the perilous circumstance. A
          <strong> hazardous situation</strong> must present an unreasonable
          threat to a person on the property, and it must have been a
          circumstance that the injured party should not have expected under the
          conditions. This latter obligation implies that people must be
          conscious of, and keep away from, obvious dangers.
        </p>
        <p>
          <strong>
            {" "}
            In order to institute that a property owner or possessor knew of an
            unsafe condition, it must be revealed that:
          </strong>
        </p>
        <ul>
          <li>
            <strong> The owner/possessor</strong> created the circumstance;
          </li>
          <li>
            <strong> The owner/possessor</strong> knew the condition existed and
            carelessly botched in correcting it; or
          </li>
          <li>
            <strong> The condition existed</strong> for such a span of time that
            the owner/possessor should have revealed and corrected it prior to
            the slip and fall occasion in question.
          </li>
        </ul>
        <p>
          For a home owner or possessor to be held answerable, it must have been
          predictable that his inattention would fashion the threat at issue. If
          a container of paint falls and spills in an aisle of a store and, one
          day later, the store has not noticed or cleaned up the spill, and
          someone slips in the paint and is injured, one might argue it was
          expected that the store's negligence in failing to inspect its aisles
          and clean up spills would cause someone to slip.
        </p>
        <LazyLoad>
          <img
            src="../images/slip-and-fall/man-falling-down-santa-ana.jpg"
            alt="person on floor after falling off a staircase"
            width="100%"
            title="slip and fall attorneys in Santa Ana"
          />
        </LazyLoad>
        <p>
          Occasionally, a claimant can prove carelessness by showing that the
          property owner violated a pertinent law and the building had dangerous
          conditions.
        </p>
        <p>
          For example, building codes often order when and where handrails and
          other like features must be installed. If you fall on a stairway that
          lacked suitable handrails, and the lack of the banister caused your
          injuries, you may have a valid claim against the building owner based
          on his or her building code contravention.
        </p>
        <h2>Who is Responsible for a Slip and Fall Injury in Santa Ana?</h2>
        <p>
          In order to recover compensation for a{" "}
          <strong> slip and fall complaint</strong> sustained on another's
          property, there must be a guilty party whose negligence caused the
          injury. This sounds evident, but many people do not recognize that
          some injuries are plainly accidents caused, if anything, by their own
          negligence.
        </p>
        <p>
          For instance, if someone falls simply because he was not looking where
          he was walking, he cannot recover against the property owner if the
          owner was in no way at fault, no matter how serious the injury. If an
          injured person is only partially at fault for his own injury, he might
          still be able to recover from another, but the dollar amount of his
          recovery might be reduced.
        </p>
        <h2>Negligence for Santa Ana Slip and Fall Injuries</h2>
        <p>
          Another condition most common, but is less clear-cut than the other
          because of the phrase "should have known", responsibility in these
          cases is decided by general intellect. The premise liability law
          determines whether the owner or occupier of property was watchful by
          deciding if the steps the owner or occupier took to keep the property
          safe were practical.
        </p>
        <p>
          You don't have to "prove" to an insurance adjuster that you were
          careful, but think about what you were doing and describe it clearly
          so that an insurance adjuster will understand that you were not
          careless.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Slip and fall attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/text-header-images/bisnar-chase.jpg')"
            }}
          >
            <h2>Excellent Legal Representation</h2>
          </div>
        </LazyLoad>

        <p>
          As minor as it sounds, a slip and fall can lead to dangerous
          consequences. <strong> The Santa Ana Slip and Fall Lawyers</strong> of
          Bisnar Chase have over
          <strong> 40 years of experience</strong> handling and winning
          compensation in slip and fall claims and other personal injury claims
          such as car accidents.
        </p>
        <p>
          Lost wages and loss of future income can be a devastating result to
          your finances after an incident.
        </p>
        <p>
          <strong>
            {" "}
            If you have been injured in a slip and fall accident, then you will
            want to contact an expert Santa Ana slip and fall accident attorney.
          </strong>
        </p>
        <p>
          Contact the law offices of Bisnar Chase for more information about
          your situation<strong> . </strong>
        </p>
        <p>
          <strong> Call 949-203-3814 for a Free Case Consultation</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
