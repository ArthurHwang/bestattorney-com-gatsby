// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "santa-ana-pedestrian-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Pedestrian Accident Lawyer - Orange County, CA"
        pageDescription="Top-rated pedestrian accident attorneys in Santa Ana assisting injured plaintiffs since 1978. You may be entitled to compensation for your injuries including time off work. Call 949-203-3814 today for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Pedestrian Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana pedestrian accident lawyer"
            title="Santa Ana pedestrian accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been the victim of a pedestrian accident, you should be
          talking to an expert in this type of law such as{" "}
          <strong> Santa Ana Pedestrian Accident Lawyers</strong>. Pedestrian
          accidents are, unfortunately, some of the most common types of{" "}
          <Link to="/santa-ana" target="_blank">
            {" "}
            personal injury incidents in Santa Ana
          </Link>
          .
        </p>
        <p>
          It is so easy for drivers to become careless and fail to yield to a
          pedestrian, and the result is usually severe injury or even death.
        </p>
        <p>
          You do not have deal with the stress that comes with communicating
          with insurance companies. Bisnar Chase's legal experts can win you the
          compensation you need to receive the proper medical treatment for your
          injuries and fair compensation for your pain and suffering as well as
          changes in your lifestyle precipitated by your accident.
        </p>
        <p>
          <strong> Since 1978</strong>, Bisnar Chase's pedestrian accident
          lawyers have won over{" "}
          <strong> $500 million dollars for our injury clients</strong>.
        </p>
        <p>
          <strong>
            {" "}
            Call us at 949-203-3814 and earn a free case analysis.
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Santa Ana Pedestrian Accident Statistics</h2>
        <p>
          At least 5,000 people are killed and 64,000 injured each year in
          pedestrian-type accidents, according to the{" "}
          <Link
            to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812124"
            target="_blank"
          >
            National Highway Transportation Safety Administration and the
            Insurance Institute for Highway Safety
          </Link>
          . The National Safety Council estimates that 85 percent of pedestrian
          accidents occur in urban areas, where traffic is heavier and more
          pedestrians are on the street. However, at least 15 percent of all
          pedestrian accidents occur in rural settings. There is no one place
          that is completely free of danger to pedestrians.
        </p>
        <h2>Determining Accountability for Your Pedestrian Accident</h2>
        <p>
          U.S. law states that pedestrians for the majority of circumstances
          have the right away. There are also other specific regulations that
          drivers must adhere to.{" "}
          <Link
            to="http://www.ncsl.org/research/transportation/pedestrian-crossing-50-state-summary.aspx"
            target="_blank"
          >
            {" "}
            California state law
          </Link>{" "}
          affirms that motorists need to take caution in areas that have
          unmarked crosswalks.
        </p>
        <h5>
          <em>
            "Vehicles must yield the right-of-way to pedestrians crossing the
            roadway within any marked or unmarked crosswalk at an intersection.
            Drivers approaching a pedestrian in a marked or unmarked crosswalk
            must reduce their speed and take other action necessary to ensure
            the pedestrian's safety."
          </em>
        </h5>
        <p>
          Pedestrians who have suffered from a serious accident that was caused
          on the driver's behalf should seek legal representation. Since 1978
          the law firm of Bisnar Chase has been helping people who have been
          involved in catastrophic crosswalk incidents gain millions in
          compensation. Drivers should be held responsible for the damages that
          you have faced throughout your accident.
        </p>
        <p>
          Contact the<strong> Santa Ana Pedestrian Accident Lawyers </strong>of
          Bisnar Chase at<strong> 949-203-3814</strong>.
        </p>
        <h2>Questions to Keep in Mind When Hiring an Injury Lawyer</h2>
        <p>
          Maneuvering through the legal process of your pedestrian accident can
          often be confusing and overwhelming. Questions of "what would I be
          charged?" or "how will I know that this lawyer is looking out for my
          best interest?" may plague your mind. There are a few questions to
          strongly consider when pursuing legal representation for your personal
          injury claim.
        </p>
        <p>
          <strong>
            {" "}
            Questions to keep in mind when hiring a Santa Ana pedestrian
            accident attorney
          </strong>
          :
        </p>
        <ul>
          <li>
            <strong> Does your lawyer have many years of experience? </strong>It
            is important to ask how many years of experience your attorney has
            of specifically dealing with pedestrian accident cases. If the
            person who is representing you in court does not have much knowledge
            of crosswalk incidents then you are lessening
            <strong> </strong>
            your chance of winning the maximum compensation you deserve. Do not
            settle for a legal representative who you do not trust from the
            beginning.
          </li>
          <LazyLoad>
            <img
              alt="team of lawyers collaborating in a room"
              className="imgright-fixed"
              title="Santa Ana crosswalk attorneys"
              src="../images/bisnar-chase/BisnarChasePhotosmall.jpg"
            />
          </LazyLoad>
          <li>
            <strong> How much compensation will I receive?</strong> Do not count
            on an exact dollar amount when you ask about the amount of
            compensation you can win in a pedestrian injury case. Pedestrian
            accident cases change continuously and are susceptible to major
            adjustments. One way to ensure that your attorney is not telling you
            an unrealistic amount is by speaking to multiple lawyers and
            comparing compensation amounts. How much you recover depends on many
            factors, including the severity of your injuries and the degree of
            fault the driver has in your particular case, but most cases recover
            at least enough damages to pay medical bills and give the accident
            victim some compensation for pain and suffering and loss of use and
            enjoyment.
          </li>
          <li>
            <strong> Will hiring a lawyer be expensive?</strong> Most of the
            time many injury attorneys charge based on a contingency fee which
            means that if there is no win there is no fee. Cost that are also
            taken into consideration include fees for expert witnesses, court
            filing and administration.
          </li>
        </ul>
        <h2>7 Typical Causes of a Pedestrian Incident in Santa Ana</h2>
        <p>
          Some of the reasons for Southern California pedestrians lose their
          lives are obvious: careless drivers, drinking and driving, and faulty
          vehicle equipment account for a large number of pedestrian accidents.
          Some pedestrians are killed when drivers simply do not look where they
          are going; others are injured when a driver loses control of a vehicle
          for various reasons. The rise of texting-related crashes has seen a
          similar rise in the number of pedestrians hit by inattentive drivers
        </p>
        <p>
          In one year exactly 4,280 pedestrians lost their lives. In California,
          a total number of 599 people lost their lives due to a crosswalk
          accident. There are many reasons why so many people just simply
          crossing the road have ended in the hospital. Some reasons are more
          common than others due to factors such as the behavior of the driver
          or congested traffic. Below are the most common reasons why
          pedestrians are severely injured or lose their lives.
        </p>
        <p>
          <strong> 7 Common causes of pedestrian accidents</strong>:
        </p>
        <ol>
          <li>Drunk driving</li>
          <li>Texting and driving</li>
          <li>Faded crosswalks</li>
          <li>Left-hand turns</li>
          <li>Dark Clothing</li>
          <li>Speeding</li>
          <li>Taking up the majority of a lane</li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pedestrian injury attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/text-header-images/santa-ana-text-banner-pedestrian.jpg')"
            }}
          >
            <h2>Our Injury Attorneys Will Defend You</h2>
          </div>
        </LazyLoad>
        <p>
          The <strong> Santa Ana Pedestrian Accident Lawyers</strong> at the law
          offices of Bisnar Chase have helped injury victims recover damages for
          their pedestrian incident. One of our experienced injury attorney's
          will meet with you and review the facts of your case. We will hold the
          person that is liable for your injuries accountable and will help you
          file a suit against that person or company to claim damages.
        </p>
        <p>
          The law firm of Bisnar Chase has held a{" "}
          <strong> 96% rate of winning compensation</strong> for personal injury
          claims.
        </p>
        <p>
          If you have been the victim of a pedestrian accident, meet a Santa Ana
          accident injury attorney immediately to discuss your case and begin
          the process of collecting damages for your injuries.
        </p>
        <p>
          <strong> Call us at 949-203-3814 for a free evaluation</strong>.
        </p>
        <p>
          <strong> Passion - Trust - Results </strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
