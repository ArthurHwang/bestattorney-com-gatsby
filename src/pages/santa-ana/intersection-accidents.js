// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/santa-ana-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Intersections - Santa Ana Accidents"
        pageDescription="Santa Ana intersections and roadways can be dangerous! Call 949-203-3814 if you have been injured in a Santa Ana intersection. You may be entitled to compensation!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Intersections</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana intersections"
            title="Santa Ana intersections"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Santa Ana is the second most populous city in Orange County. Traffic
          congestion is a serious problem in this bustling city, which is also
          the seat of county government. Locations such as where West 1st and
          Flower streets intersect and where North Main Street meets West Santa
          Ana Boulevard are common sites for intersection accidents because of
          the sheer volume of vehicles. Santa Ana also has numerous crosswalks
          marked and unmarked where many{" "}
          <Link to="/santa-ana/pedestrian-accidents" target="_blank">
            {" "}
            pedestrian accidents do occur
          </Link>
          .
        </p>
        <h2>Carnage in Crosswalks in Santa Ana</h2>
        <p>
          So far, just this year, six pedestrians have been killed on Santa
          Ana's roadways including an 11-year-old girl. Prompted by a series of
          pedestrian collisions in crosswalks, city police ticketed more than 80
          drivers during a crosswalk sting in August 2013. Police say the
          crosswalk sting is just one of their efforts to educate drivers about
          the importance of coming to a full stop at crosswalks and yielding the
          right-of-way to pedestrians. Police are also working to curb
          jaywalking, crosswalk violations and other pedestrian traffic
          infractions.
        </p>
        <h2>Santa Ana Crosswalk Laws</h2>
        <p>
          <strong> California Vehicle Code section 21950 states</strong>: "
          <em>
            The driver of a vehicle shall yield the right-of-way to a pedestrian
            crossing the roadway within any marked crosswalk or within any
            unmarked crosswalk at an intersection.
          </em>
          "
          <strong>
            {" "}
            The same section also states that the driver of a vehicle
            approaching a pedestrian within any marked or unmarked crosswalk
          </strong>{" "}
          "
          <em>
            shall exercise all due care and shall reduce the speed of a vehicle
            or take any other action relating to the operating of the vehicle as
            necessary to safeguard the safety of the pedestrian.
          </em>
          "
        </p>
        <p>
          <strong> The same section also states</strong>: "
          <em>
            This section does not relieve a pedestrian from the duty of using
            due care for his or her safety. No pedestrian may suddenly leave a
            curb or other place of safety and walk or run into the path of a
            vehicle that is so close as to constitute an immediate hazard. No
            pedestrian may unnecessarily stop or delay traffic while in a marked
            or unmarked crosswalk.
          </em>
          "
        </p>
        <h2>Intersection Accident Prevention in Santa Ana</h2>
        <p>
          <strong>
            {" "}
            There are a number of steps motorists can take to prevent these
            types of street intersection accidents
          </strong>
          :
        </p>
        <ul>
          <li>
            Come to a complete stop at red lights and stop signs and do not
            attempt to go through a yellow light when you have adequate time to
            safely slow down.
          </li>
          <LazyLoad>
            <img
              src="../images/car-accidents/santa-ana-Pedestrian-Crossing.jpg"
              alt="yellow pedestrian sign"
              width="100%"
              className="imgright-small"
              title="Intersection accident attorneys in Santa Ana"
            />
          </LazyLoad>
          <li>
            When approaching an intersection, reduce your speed if the light is
            about to turn.
          </li>
          <li>Give yourself more space and time to stop in wet conditions.</li>
          <li>You should also check your blind spots before making a turn.</li>
          <li>Yield the right-of-way to pedestrians</li>
        </ul>
        <h2>Driving Defensively</h2>
        <p>
          You can't{" "}
          <Link to="/resources/preventing-car-accidents">
            prevent an accident
          </Link>{" "}
          at all times. If another motorist runs a red light, there may not be
          time to swerve out of the way. This is why it is necessary to go above
          and beyond by using caution. As you approach an intersection, look to
          see if traffic is stopped or if an approaching vehicle is slowing
          down. Even if you have a green light, you should be aware of drivers
          that may be speeding, distracted, intoxicated or fatigued.
        </p>
        <ul>
          <li>
            <strong> Speeding drivers:</strong> It is more difficult to slow
            down in time for a changing light when you are traveling above the
            speed limit.
          </li>
          <li>
            <strong> Distracted driving:</strong> Whenever you are sending a
            text, fiddling with the radio or looking at a navigation system, you
            are not focusing on the roadway. Inattentive drivers are more likely
            to miss changes in the flow of traffic. Distracted drivers can even
            miss if a light has changed. Many of the most devastating
            intersection accidents involve drivers who failed to slow down
            because they were sending a text or talking to a passenger. Whenever
            you are driving on the congested streets of Santa Ana, make sure
            your hands are on the wheel, your eyes are on the road and your mind
            is focused on the task at hand.
          </li>
          <li>
            <strong> Intoxicated driving:</strong> Drivers who are under the
            influence of drugs or alcohol are more likely to speed, to make poor
            decisions and to miss changes in the flow of traffic.{" "}
            <Link to="/dui">Drunk drivers</Link> often try to speed through
            lights that are about to turn red.
          </li>
          <li>
            <strong> Fatigued driving:</strong> If you are feeling sleepy, it is
            vital that you pull over and have someone else drive or rest until
            you are fit to drive. Fatigued drivers struggle to focus on the
            roadway and they can easily run through an intersection without
            realizing it. If you can't remember passing certain roads or if your
            eyes are feeling heavy, you are too tired to drive safely.
          </li>
        </ul>
        <p></p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Santa Ana intersections"
            style={{
              backgroundImage:
                "url('/images/car-accidents/text-banner-image.jpg')"
            }}
          >
            <h2>Holding Careless Santa Ana Drivers Accountable</h2>
          </div>
        </LazyLoad>
        <p>
          If you are the victim of a speeding, distracted, impaired or fatigued
          driver, you have the right to hold him or her accountable for your
          injuries. In fact, anyone who has been injured in a Santa Ana
          intersection accident would be well advised to obtain more information
          about his or her legal options. It may be possible to hold the driver
          responsible for the accident financially accountable for all of the
          losses suffered.
        </p>
        <p>
          For example, a successful personal injury claim against a careless
          driver can result in financial compensation for medical bills, lost
          wages, loss of earning potential, pain and suffering and a number of
          other related damages. You may not be able to receive fair
          compensation, however, if you do not act quickly to protect your
          rights.
        </p>
        <h2>Getting the Support You Need</h2>
        <p>
          It is important that you take the steps necessary to protect your best
          interests. Dealing with insurance companies can be stressful and
          injured victims are rarely offered the compensation they need without
          legal guidance. Make sure you never admit fault for the crash, collect
          information from everyone who witnessed the accident, seek out medical
          attention that same day and research your options before making any
          legal decisions.
        </p>
        <p>
          The experienced{" "}
          <Link to="/santa-ana/car-accidents">
            Santa Ana car accident lawyers
          </Link>{" "}
          at Bisnar Chase know and understand the complexities surrounding auto
          accident cases. We have helped numerous injured victims and their
          families get fairly compensated for their losses.
        </p>
        <p>
          <strong>
            {" "}
            Please contact us at 949-203-3814 to obtain more information about
            pursuing your legal rights
          </strong>
          .
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
