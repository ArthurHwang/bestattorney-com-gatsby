// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "wrongful-death/santa-ana-wrongful-death-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Wrongful Death Lawyer - Orange, County CA"
        pageDescription="Contact the Santa Ana Wrongful Death Lawyers of Bisnar Chase. Our lawyers have been representing clients in the Orange County area since 1978. Call 949-203-3814 for a Free Case Consultation. If your loved one has been the victim of a wrongful death, contact our injury attorneys for immediate represenation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana wrongful death Lawyers"
            title="Santa Ana wrongful death Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Following the loss of a loved one, finding a{" "}
          <strong> Santa Ana Wrongful Death Lawyer</strong> may be the last
          thing on your mind. Families of wrongful death victims in Santa Ana
          can find themselves dealing with a number of problems; medical bills
          accrued by your loved one prior to their death may have been
          substantial, and the grief that you and other family members are
          dealing with can be paralyzing.
        </p>
        <p>
          Our injury attorneys understand what you are going through because we
          have helped thousands of other people in a situation very similar to
          what you are going through now. We have been representing clients who
          have faced the tragedy of losing a loved one{" "}
          <strong> since 1978</strong>.
        </p>
        <p>
          Helping wrongful death victims recover as much as they can in as
          little time as possible is the number one priority, and we have an
          unparalleled history of doing so. The law firm of Bisnar Chase has
          collected over{" "}
          <strong>
            {" "}
            $500 million dollars in compensation for the families of wrongful
            death victims
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Santa Ana wrongful death attorneys have{" "}
          <strong> sustained a 96% success rate in winning injury cases</strong>
          . The success we have sustained and our personal commitment to your
          case will give you the confidence needed to concentrate less on your
          wrongful death claim and more on you and your family.
        </p>
        <p>
          <strong>
            {" "}
            For a Free Consultation on your Santa Ana injury lawsuit contact us
            at 949-203-3814
          </strong>
          .
        </p>
        <p>
          <strong> If there is no win, there is no fee</strong>.
        </p>
        <p>Call us today.</p>
        <h2>Santa Ana Wrongful Death Car Accident Statistics</h2>
        <p>
          One of the most common reasons for hiring a Santa Ana wrongful death
          lawyer is due to the frequent occurrences of car accidents. The
          streets of Santa Ana are one of the most dangerous in Orange County.
        </p>
        <p>
          According to the{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="_blank"
          >
            California Statewide Integrated Traffic Records System
          </Link>
          (SWITRS), 11 people were killed as a result of Santa Ana auto
          collisions. Whether the problem is due to an abnormally high number of
          unsafe drivers, or the actual streets of Santa Ana are contributing to
          the high fatality rate is debatable, but those who drive through the
          area should be particularly cautious.
        </p>
        <p>
          California has constructed a law that forces negligent parties to
          compensate for the losses of the deceased surviving family members.
        </p>
        <p>
          The
          <strong>
            {" "}
            <Link
              to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=377.60.&lawCode=CCP"
              target="_blank"
            >
              {" "}
              California wrongful death statute
            </Link>{" "}
          </strong>
          states, "
          <em>
            A cause of action for the death of a person caused by the wrongful
            act or neglect of another may be asserted by any of the following
            persons or by the decedent's personal representative on their
            behalf: The decedent's surviving spouse, domestic partner, children,
            and issue of deceased children, or, if there is no surviving issue
            of the decedent, the persons, including the surviving spouse or
            domestic partner, who would be entitled to the property of the
            decedent by intestate succession.
          </em>
          "
        </p>
        <p>
          Surviving family members can file for what is known as "Pecuniary
          Damages". Pecuniary or monetary damages are defined as financial
          losses that can be calculated.
        </p>
        <p>
          <strong>
            {" "}
            This means that if a person loses their life due to a third party's
            carelessness, the family or the spouse of the deceased can claim
            compensation for damages such as
          </strong>
          :
        </p>

        <ul>
          <li>
            the income that the deceased would have earned in their lifetime
          </li>
          <li>funeral bills</li>
          <li>recovery for companionship</li>
        </ul>
        <LazyLoad>
          <img
            alt="a bronzed statue of lady justice"
            width="100%"
            title="Wrongful death attorneys in Santa Ana"
            src="../images/wrongful-death/wrongful-death-lady-justice.jpg"
          />
        </LazyLoad>
        <p>
          Non-Pecuniary losses are calculated by the pain and suffering of the
          surviving family members and are difficult to determine. The damages
          that can be classified as non-pucinary are emotional anguish, damage
          to relationships and being able to perform daily tasks due to a
          distressed mental state.
        </p>
        <p>
          Non-Pecuniary losses do not have a set amount and are dependent on
          various factors.
        </p>
        <p>
          Elements that are taken into account when deciding the amount of
          non-pucinary damages can include the age of the person who is filing
          the claim and the seriousness of the emotional trauma.
        </p>
        <h2>Seeking Legal Representation for Your Wrongful Death Claim</h2>
        <p>
          Lawsuits that pertain to a person's death due to someone else's
          negligence can be very complicated. It is strongly suggested that you
          hire a <strong> Santa Ana Wrongful Death Lawyer</strong> that has many
          years of experience dealing with these specific cases.
        </p>
        <p>
          <strong>
            {" "}
            The way in which your attorney can prove a wrongful death had taken
            place is by the following
          </strong>
          :
        </p>
        <ul>
          <li>
            the death of family member occurred due to someone's negligence
          </li>
          <li>
            the death of a loved one caused family members pain and suffering
          </li>
          <li>
            the party who was accountable for the death had a duty to the
            deceased
          </li>
        </ul>
        <p>
          We will not only handle all of the legal aspects of your case, but you
          can count on an entire staff that is passionate about obtaining
          justice. Our clients receive an experience which has set the bar for
          attorneys across the nation.
        </p>
        <p>
          The law firm of Bisnar Chase has received countless awards for our
          high verdicts and standards of excellence that has placed us among the
          top law firms in the nation, and we utilize the wisdom we have
          acquired helping others such as yourself to help you. We give you
          straight talk from expert Santa Ana attorneys who know what it takes
          to win. We give you the personal experience you need after your
          tragedy, and keep you informed throughout the entire process.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IUvwlj6ew0I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            "John Bisnar explains what type of situations can be categorized as
            a wrongful death."
          </blockquote>
        </center>

        <h2>Wrongful Death Attorneys for Any Personal Injury</h2>
        <p>
          Wrongful deaths can occur anywhere at any time. Our attorneys assist
          clients whose family members were killed in any type of accident that
          was due to someone else's negligence.
        </p>
        <p>
          <strong>
            {" "}
            Our experienced Santa Ana personal injury lawyers specialize in
            practice areas that include
          </strong>
          :
        </p>
        <ul>
          <li>Santa Ana Bicycle Accidents</li>
          <li>Santa Ana Defective Products</li>
          <li>
            {" "}
            <Link to="/santa-ana/car-accidents">Santa Ana Car Accidents</Link>
          </li>
          <li>
            {" "}
            <Link to="/santa-ana/motorcycle-accidents">
              Santa Ana Motorcycle Accidents
            </Link>
          </li>
          <li>Santa Ana Construction Accidents</li>
          <li>Santa Ana Hazardous Roadways</li>
          <li>Santa Ana Auto Product Liability</li>
          <li>Santa Ana Pedestrian Accidents</li>
          <li>
            {" "}
            <Link to="/santa-ana/slip-and-fall-accidents">
              Santa Ana Slip and Falls
            </Link>
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Wrongful death lawyers in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/wrongful-death/wrongful-death-burial.jpg')"
            }}
          >
            <h2>Experienced Santa Ana Wrongful Death Attorneys</h2>
          </div>
        </LazyLoad>
        <p>
          <strong> For 40 years</strong>, our attorneys have obtained over{" "}
          <strong> $500 million dollars</strong> for Santa Ana injury victims
          and show no signs of stopping.
        </p>
        <p>
          Brian Chase has been awarded{" "}
          <strong> Trial Lawyer of the Year</strong>, and his contributions to
          assisting wrongful death victims has been nationally recognized. Chase
          and his team of personal injury attorneys strive to not only win
          compensation for the families who have endured pain and suffering but
          also strive to have the best client satisfaction.
        </p>
        <p>
          If you have experienced the lost of the loved one due to someone
          else's negligence, contact the personal injury lawyers of Bisnar
          Chase.
        </p>
        <p>
          If you are looking for one of the best law firms in the nation,{" "}
          <Link to="/contact">Bisnar Chase</Link> injury lawyers in Santa Ana.
        </p>
        <p>
          <strong> Call 949-203-3814 for a No-Hassle, No-Obligation </strong>
        </p>
        <p>
          <strong> Free Case Consultation.</strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
