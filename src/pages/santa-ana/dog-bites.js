// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/dog-bite-santa-ana.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Dog Bite Lawyers - California Dog Attack Attorneys"
        pageDescription="Contact the Santa Ana dog bite lawyers of Bisnar Chase for a free consultation about a dog attack case you've been injured in. We have over four decades of fighting for plaintiffs justice. Call 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Dog Bite Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana Dog Bite Lawyer"
            title="Santa Ana Dog Bite Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you've been seriously injured by a dog attack in Santa Ana contact
          our experienced
          <strong>
            {" "}
            Santa Ana{" "}
            <Link to="/dog-bites" target="_blank">
              {" "}
              Dog Bite Lawyers
            </Link>
          </strong>{" "}
          for a free case analysis.
        </p>
        <p>
          We've been representing residents of Santa Ana{" "}
          <strong> since 1978 and have a 96% success rate</strong>. Our
          attorneys have the experience and resources to pursue even the most
          complex dog bite cases. The law offices of Bisnar Chase have
          specialized in dog bite cases for four decades and has won over{" "}
          <strong>
            {" "}
            $500 million dollars in compensation for our injury clients
          </strong>
          .
        </p>
        <p>
          Contact the Santa Ana dog bite attorneys of Bisnar Chase for a{" "}
          <strong> Free Consultation</strong>.
        </p>
        <h2>Santa Ana Dog Bite Statistics</h2>
        <p>
          Santa Ana is the second most populous city in Orange County. With more
          than 324,000 residents the dog population is also high. Santa Ana has
          beautiful dog parks nearby such as the Central Bark Dog Park where dog
          owners can relax on benches while their dogs play.
        </p>
        <p>
          Some of the facts about dog bites make for amazing statistics. It
          doesn't seem like it happens very often, but it certainly does.
        </p>
        <p>
          <strong>
            {" "}
            The{" "}
            <Link
              to="https://www.caninejournal.com/dog-bite-statistics/"
              target="_blank"
            >
              {" "}
              Canine Journal
            </Link>{" "}
            reported
          </strong>
          :
        </p>
        <ul>
          <li>4.7 million - dog bites per year, nationally</li>
          <li>
            15 - times faster the dog bite rate has risen compared to dog
            ownership
          </li>
          <li>850,000 - dog bites each year that require medical attention</li>
          <li>3,000 - letter carriers that suffer dog bites every year</li>
        </ul>
        <p>
          As you can see, it's a fairly common occurrence. Dogs must be kept in
          check, and when they're not, the outcome can be devastating.
        </p>
        <p>
          Dogs can bite for many reasons. One of the most common reasons is
          because the dog is afraid. If the dog does not experience very much
          socialization when it is three months or younger then it may view
          strangers as an automatic threat. This will trigger the dog to defend
          itself ultimately resolving in an attack.
        </p>
        <p>
          The most common victims of dog bites are children and senior citizens.
        </p>
        <h2>Common Santa Ana Dog Bite Injuries</h2>
        <p>
          Sometimes dogs can become aggressive for various reasons, even a dog
          that has never lashed out before. Dogs can also become aggressive
          towards other dogs. This can happen at a dog park, at home, or on a
          walk.
        </p>
        <p>
          People who are attacked by a dog suffer serious personal injuries and
          can become permanently disfigured.
        </p>
        <p>
          <strong>
            {" "}
            Types of injuries that you can sue for a dog bite claim include
          </strong>
          :
        </p>
        <p>
          <strong> Facial disfigurement</strong>: Injury victims can file a
          claim against a dog owner if they are left with punctured skin from
          the dog's teeth. If the skin that has been torn is small this can be
          healed with scar treatment. Surgery is a possibility for those who are
          looking to smooth out an obvious scar.
        </p>
        <p>
          <strong> PTSD</strong>: After being attacked by a dog, victims may
          possibly experience post-traumatic stress disorder. The victim may
          have nightmares or feel great anxiety when spotting a dog in a park.
          Counseling is strongly suggested for dog bite victims especially for
          children who have experienced an animal attack.
        </p>
        <LazyLoad>
          <img
            alt="Animal attack lawyer in Santa Ana"
            width="100%"
            className="imgright-small"
            title="Dog bite attorneys in Santa Ana, California"
            src="../images/dog-bites/image-ptsd-santa-ana-dog.jpg"
          />
        </LazyLoad>
        <p>
          <strong> Damage to nerves</strong>: There are three types of nerve
          damage that injury victims can face when bitten by a dog. The types of
          damage include neurapraxia, axonotmesis and neurotmesis nerve damage.
          The most severe of the kinds of nerve damage is neurotmesis.{" "}
          <Link to="https://nerve-injury.com/neurotmesis/" target="_blank">
            {" "}
            Neurotmesis
          </Link>{" "}
          means that the patient's injury has led to permanent damage and faces
          a small chance of recovering.
        </p>
        <p>
          One of the most important actions to take if you or someone you know
          was bitten by a dog is to immediately seek medical attention to
          determine whether the dog has received all the necessary shots.
        </p>
        <h2>California's Strict Liability Law</h2>
        <p>
          A plaintiff doesn't need to prove that the dog was dangerous or had a
          history of biting. CALIFORNIA DOG BITE LAW,{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=3342.&lawCode=CIV"
            target="_blank"
          >
            {" "}
            CIVIL CODE SECTION 3342
          </Link>
          .
        </p>
        <p>
          <strong>
            {" "}
            All you need to prove are a few simple things that include
          </strong>
          :
        </p>
        <ol>
          <li>The defendant owned the dog</li>
          <li>The dog bit the plaintiff</li>
          <li>
            The plaintiff was lawfully on the premises where the dog bite took
            place
          </li>
          <li>The dog bite caused the plaintiff injuries</li>
        </ol>
        <h2>Recovery You Can Gain From Your Santa Ana Injury</h2>
        <p>
          These simple pieces of evidence go a long way in your case. Negligent
          dog owners should not be able to get away with letting their dog
          attack another human being. You are due just compensation, and we can
          help you get it.
        </p>
        <p>
          The compensation you gain will vary due to the number of damages you
          have obtained as a result of the attack. Compensation that can be
          recovered in your dog bite injury case can be medical expenses, future
          medical bills and lost wages.
        </p>
        <p>
          Providing your Santa Ana Dog Bite Lawyer with evidence such as
          photographs, witness statements and details of the incident will
          better help your dog bite case as well.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog bite attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/dog-bites/santa-ana-dog-bite-text-header-image.jpg')"
            }}
          >
            <h2>Filing a Santa Ana Dog Bite Claim</h2>
          </div>
        </LazyLoad>

        <p>
          Our dog bite lawyers have been representing Orange County residents
          for over 40 years. We serve all major cities throughout California
          with three locations to serve you. LA County, Northern California and
          Orange County.
        </p>
        <p>
          A phone call to us regarding your dog bite claim may be the difference
          in you recovering hundreds or thousands. Your injuries and stress will
          be evaluated into your potential dog bite case.
        </p>
        <p>
          The law firm of Bisnar Chase will protect you from beginning to end of
          case.
        </p>
        <p>
          <strong> Contact a Santa Ana Dog Bite Lawyer Today</strong>.
        </p>
        <p>
          1301 Dove St. #120 <br></br>Newport Beach CA 92660 <br></br>949-
          203-3814 or toll free at 800-561-4887
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
