// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "brain-injury/santa-ana-text-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Brain Injury Lawyers - Award Winning Attorneys"
        pageDescription="Award winning Santa Ana brain injury lawyers with over 40 years in practice. Call 949-203-3814. We have recovered over $500 Million for our head injury clients. Contact our TBI attorneys today for a Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Brain Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana Brain Injury Lawyer"
            title="Santa Ana Brain Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong> Santa Ana Brain Injury Attorneys of Bisnar Chase </strong>{" "}
          can help you through the process of valuing your case and seeking a
          judgment for an adequate sum to protect you for the rest of your life.
          You owe it to yourself and your loved ones to pursue your case and
          ensure that you are adequately compensated not only for your medical
          bills, lost wages but also for your pain and suffering and other
          problems that may develop over time.
        </p>
        <p>
          If you or a loved one has suffered a traumatic brain injury, contact
          one of our experienced{" "}
          <Link to="/santa-ana" target="new">
            Santa Ana Personal Injury Lawyers
          </Link>
          .
        </p>
        <p>
          <strong> Please call us at 949-203-3814 </strong>for a
          <strong> Free Consultation</strong> and more information about what
          the personal injury law firm of Binary Chase can do for you.
        </p>
        <p>
          Click here to fill out our <Link to="/contact">contact form</Link>.
        </p>
        <h2>
          <strong> What Do You Need for Your Brain Injury Case?</strong>
        </h2>
        <p>
          With our team of experienced and successful attorneys represents
          clients with the toughest cases and knows exactly what to do to win
          your head injury case.
        </p>
        <p>
          Santa Ana is one of the most populous cities in Southern California.
          In addition there are buses, commercial trucks, motorists, and
          bicyclists sharing the roads. The streets of Santa Ana can become very
          crowded and busy at times. One of the most common types of brain
          injury causes is automobile accidents and Santa Ana has a higher than
          average percentage of auto pedestrian accidents.
        </p>
        <p>
          Brain injuries are all too common when a person has been involved in a
          serious car accident, a motorcycle accident, a bicycle or pedestrian
          accident, or other types of personal injury situations. Brain injuries
          range in severity from slight to profound, and the more serious the
          injury, the more impact it will have on you and your family's future.
        </p>
        <p>
          Brain injury attorneys look at the victim's medical history and make a
          determination about the best way to pursue a personal injury case.
          Brain injuries can take many forms and can have a wide variety of
          medical outcomes.
        </p>
        <p>
          Some people can recover from brain injuries and be "as good as new"
          others will never again enjoy the same lifestyle they had before the
          accident. No matter how minor or serious you consider your brain
          injury to be, it is wise to have the advice of a professional personal
          injury lawyer before you attempt to settle your case.
        </p>

        <h2>How a Traumatic Brain Injury is Identified</h2>
        <LazyLoad>
          <img
            src="../images/brain-injury/Doctor Visit.jpg"
            alt="doctor writing down notes on paper"
            width="100%"
            title="Santa Ana TBI lawyers"
          />
        </LazyLoad>

        <p>
          At the original occurrence of the accident, slip and fall or event the
          brain injury was administered, there are a few warning signs of a
          traumatic brain injury taking place that consist of the following:
        </p>

        <ul>
          <li>Loss of consciousness</li>
          <li>Double vision</li>
          <li>Confusion and or disorientation</li>
          <li>Slurred speech</li>
          <li>Loss of motor function</li>
          <li>Memory loss</li>
          <li>Nausea or vomiting</li>
          <li>Fatigue or drowsiness</li>
          <li>Difficulty sleeping</li>
          <li>Hallucinations</li>
          <li>Sleeping more than usual</li>
          <li>Dizziness or loss of balance</li>
        </ul>
        <p>
          Learn more about Traumatic Brain Injury symptoms{" "}
          <Link
            to="http://www.mayoclinic.org/diseases-conditions/traumatic-brain-injury/basics/symptoms/con-20029302"
            target="new"
          >
            Here
          </Link>
          .
        </p>
        <h2>Receiving Treatment for a Traumatic Brain Injury</h2>
        <p>
          Every head and brain injury vary from person to person. Typically, a
          doctor will begin by asking basic questions about the injury and how
          it took place, followed with questions that will test your ability to
          pay attention, learn, remember and solve problems.
        </p>
        <p>
          An examination for physical signs of a brain injury by checking your
          reflexes , strength, balance, coordination and sensation. CT scans and
          MRI's may be ordered to make sure your brain is not bruised or
          bleeding. Additional tests to see if proper brain function is present
          may be necessary.
        </p>
        <h2>Athletes Suffer Traumatic Brain Injuries Every Day</h2>
        <p>
          Due to a high level of impact during practice and gameplay, athletes
          encounter traumatic brain injuries very often. Many
          high-level-head-injury and impact sports can have serious long-term
          effects on brain development and health. Sports that involve speed,
          strength and one on one player-interaction offer the highest risk for
          traumatic brain injury and <strong> concussions.</strong>
        </p>

        <h2>What is a Concussion?</h2>

        <p>
          Many brain injuries in Santa Ana consist of concussions. A concussion
          is a type of traumatic brain injury which results from an intense blow
          to the head. These blows to the head can result in slipping and
          smacking your head on something, a punch to the head, orbeing tackled
          violently while playing football. Football players arent the only ones
          that encounter violent blows to the neck and head.
        </p>
        <p>
          The following list of sports report a high level of concussions and
          traumatic injuries resulting from practice and gameplay:
        </p>
        <ul>
          <li>Football</li>
          <li>Ice hockey</li>
          <li>Soccer</li>
          <li>Llacrosse</li>
          <li>MMA</li>
          <li>Snowboarding</li>
          <li>Dirtbiking</li>
          <li>Boxing</li>
          <li>Wrestling</li>
          <li>Surfing</li>
          <li>Basketball</li>
          <li>Softball</li>
          <li>Cgoogle maps creater</li>
          <li>Cheerleading</li>
          <li>Volleyball</li>
          <li>Baseball</li>
          <li>Gymnastics</li>
        </ul>
        <p>
          If you or a loved one has suffered a traumatic brain injury in Santa
          Ana that is sports related, make sure you choose an experienced team
          of Santa Ana Brain Injury Attorneys like us. We have over 39 years of
          experience with a 96 percent success rate. Our highly skilled lawyers
          have won over $500 Million for our clients.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Santa Ana brain injury attorneys"
            style={{
              backgroundImage:
                "url('/images/brain-injury/santa-ana-text-banner.jpg')"
            }}
          >
            <h2>Long-Term Effects of a Traumatic Brain Injuries</h2>
          </div>
        </LazyLoad>
        <p>
          Absolutely. Concussions are extremely dangerous, both short and long
          term. The Santa Ana brain injury lawyers at Bisnar Chase understand
          that traumatic brain injuries can cause severe problems down the road,
          especially when proper medical treatment and care is not received.
          Ensuring our clients are taken care of medically, financially,
          emotionally and professionally is our goal.
        </p>
        <p>
          Concussions are usually an indicator you have severely injured your
          brain.
        </p>
        <p>
          {" "}
          <Link
            to="https://www.cdc.gov/traumaticbraininjury/symptoms.html"
            target="new"
          >
            Symptoms of a concussion
          </Link>{" "}
          consist of:
        </p>
        <ul>
          <li>Headache</li>
          <li>Blurry Vision</li>
          <li>Nausea or vomiting</li>
          <li>Balance problems</li>
          <li>Sensitivity to noise or light</li>
          <li>Dizziness</li>
          <li>Sleeping more or less than usual</li>
          <li>Difficulty remembering new information</li>
        </ul>
        <p>
          Individuals are advised not to fall asleep immediately following a
          concussion. If dilated pupils and or trouble walking is not present,
          sleeping is encouraged for the recovery process to begin healing the
          affected parts of the brain and damaged areas.
        </p>
        <p>
          Post-concussion syndrome consists of prolonged symptoms for weeks,
          months and longer. Many professional football players suffer
          concussions so often, post-concussion syndrome can lead into{" "}
          <Link
            to="https://www.forbes.com/sites/carlosdiasjr/2015/09/30/the-long-term-effects-of-concussions-on-nfl-players/#5a3b374b11b5"
            target="new"
            onmousedown="return rwt(this,'','','','2','AFQjCNG1ArCVyhAKMaz-e6uvZZX7BhIwdw','wLej7HYM3GtZUVylhFK4ag','0ahUKEwj3zaaszvLTAhUBDmMKHZRxCOIQFggqMAE','','',event)"
            data-to="https://en.wikipedia.org/wiki/Chronic_traumatic_encephalopathy"
          >
            CTE or Chronic traumatic encephalopathy
          </Link>
          .
        </p>
        <p>
          If you or a loved one has<strong> experienced a brain injury</strong>,
          contact our highly successful and experienced an Orange County{" "}
          <strong> traumatic brain injury attorneys</strong> for a free
          consultation today at <strong> 949-203-3814</strong>.
        </p>
        <h2>Some of our Recent Case Results</h2>
        <ul>
          <li>
            <strong> $38,650,000</strong> -{" "}
            <Link to="/santa-ana/motorcycle-accidents">
              Motorcycle Accident
            </Link>
          </li>
          <li>
            <strong> $24,744,764</strong> - Auto defect
          </li>
          <li>
            <strong> $23,091,098</strong> - Product liability
          </li>
          <li>
            <strong> $16,444,904</strong> - Dangerous road condition, driver
            negligence
          </li>
          <li>
            <strong> $10,030,000</strong> - Premises negligence
          </li>
          <li>
            <strong> $9,800,000</strong> -{" "}
            <Link to="/santa-ana/car-accidents">Motor vehicle accident</Link>
          </li>
          <li>
            <strong> $8,500,000</strong> - Motor vehicle accident -{" "}
            <Link to="/santa-ana/wrongful-death">wrongful death</Link>
          </li>
          <li>
            <strong> $8,250,000</strong> -{" "}
            <Link to="/santa-ana/slip-and-fall-accidents">
              Premises liability
            </Link>
          </li>
          <li>
            <strong> $7,998,073</strong> - Product liability - motor vehicle
            accident
          </li>
        </ul>
        <h2>
          Before You Decide to Contact a Orange County Brain Injury Attorney...
        </h2>
        <p>
          Good personal injury lawyers know how to work with victims in their
          field of expertise. Brain injury lawyers know that the{" "}
          <strong> effects of brain injuries</strong> may take time to develop,
          and that settling a case too soon is asking for problems. You should
          get the advice of a professional in this field, who has access to the
          best medical professionals, before you decide what your case is worth.
        </p>

        <h2>Hire our Top-Notch California Head Injury Lawyers </h2>

        <p>
          It may be that you will need further ongoing treatment to manage your
          health and welfare for the rest of your life. Settling a case like
          this too soon usually means that, at some point, you will run out of
          funds for your care and be forced to accept less than you deserve in
          the way of medical treatment or rely on other means of support.
        </p>
        <LazyLoad>
          <img
            src="../images/bisnar-chase/BisnarChasePhotosmall.jpg"
            alt="lawyers speaking to eachother in a room"
            // width=""
            className="imgright-fixed"
            title="Head injury attorneys in Santa Ana "
          />
        </LazyLoad>
        <p>
          This is not fair when you consider that your accident was not your
          fault, and that someone else actually holds the responsibility for
          your condition. Many at-fault drivers or other people walk away from a
          shattering personal injury case without paying anything at all, and
          others never accept full legal responsibility for their actions. It is
          up to the victim to pursue the case and ensure that the at-fault party
          is held liable for the actions that led to this tragedy.
        </p>
        <p>
          Our Santa Ana brain injuries attorneys will ensure you receive maximum
          compensation for your case. Bisnar Chase will cover all expenses and
          costs you're responsible for, and you don't pay anything until we win
          your case.
        </p>
        <p>
          To discuss your personal injury claim with our team of traumatic brain
          injury lawyers today, free of charge, call the law offices of Bisnar
          Chase at<strong> 949-203-3814.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
