// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/santa-ana-bicycle-accident-lawyers-banner-photo.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Bicycle Accident Lawyer - Orange County Bike crash attorney"
        pageDescription="Call 949-203-3814 for an experienced Santa Ana bicycle accident lawyer winning tough cases since 1978. Over $500 Million won for injured plaintiffs including bike vs. motor vehicle injuries in Orange County. Free consultation and no win, no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa ana bicycle accident lawyer"
            title="Santa ana bicycle accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Vehicles must share the road with cyclists. Even if you were told you
          were at fault, a vehicle must still obey traffic laws to avoid injury.
          If you've been injured in a bike accident you may be entitled to
          compensation. The{" "}
          <strong> Bicycle Accident Lawyers of Bisnar Chase</strong> are
          experienced and aggressive. We've collected over $500 Million for our
          clients.
          <strong>
            Contact our highly skilled accident lawyers at 949-203-3814 for a
            free consultation.
          </strong>
        </p>
        <p>
          We may be able to recover damages for you including your medical
          bills. Our{" "}
          <Link to="/santa-ana" target="new">
            Santa Ana personal injury attorneys
          </Link>{" "}
          have been helping Orange County residents for decades.
        </p>
        <p>
          We have a team of 6 trial attorneys who will fight for you -- not just
          settle.
        </p>
        <p>
          <strong> Call 949-203-3814 for a free consultation</strong>.
        </p>
        <h2>Common Causes of Bicycle Accidents in Santa Ana</h2>
        <p>
          There are a number of reasons why bicycle accidents occur in Santa
          Ana. However, the most common causes are as follows:
        </p>
        <ul>
          <li>
            <strong> Impaired driving:</strong> Drivers who operate their
            vehicles under the influence of alcohol or drugs put others on the
            roadway in great danger. When drivers are under the influence, it is
            common for them not to notice pedestrians or bicyclists on the
            roadway.
          </li>
          <li>
            <strong> Distracted driving:</strong> When drivers choose to talk on
            a hand-held cell phone or text while driving, there is an increased
            risk of them swerving into a bicyclist and causing a serious injury
            accident. There have also been cases where distracted or inattentive
            motorists have swerved into bike lanes and struck bicyclists.
          </li>
          <li>
            <strong> Failure to yield:</strong> Under California law, bicyclists
            have the same rights and responsibilities as drivers of other
            vehicles. This means that bicyclists are required to yield the
            right-of-way to other vehicles when appropriate and other drivers
            are required to yield the right-of-way to bicyclists when
            appropriate. But, often, motorists do not consciously look out for
            bicyclists when they make a turn at a street intersection or when
            they pull out of a driveway or parking lot. Such acts of negligence
            can result in major injuries or even death for the bicyclists.
          </li>
          <li>
            <strong> Dangerous roadways:</strong> A roadway can become really
            dangerous for bicyclists when they lack visibility due to poor
            design. Defective roadways such as streets ridden with potholes can
            also pose a significant danger to bicyclists. If a bicycle accident
            is caused by a dangerous or defective roadway, the city or
            governmental agency responsible for maintaining the roadway can be
            held liable for the bicyclist's injuries and damages.
          </li>
        </ul>
        <h2>Causes of Injury in Santa Ana</h2>
        <p>
          Specifically, there can be a consistency of injuries relating to
          common causes of bicycle accidents. According to the{" "}
          <Link
            to="https://www.nhtsa.gov/sites/nhtsa.dot.gov/files/811841b.pdf"
            target="_blank"
          >
            {" "}
            National Survey on Bicyclist and Pedestrian Attitudes and Behaviors
          </Link>
          , nearly a third of all injuries are caused when bicyclists are struck
          by cars.
        </p>
        <ul>
          <li>29% - Hit by car</li>
          <li>17% - Falls</li>
          <li>13% - Roadway/walkway not in good condition</li>
          <li>13% - Rider error/not paying attention</li>
          <li>7% - Crashed/collision</li>
          <li>4% - Dog ran out</li>
          <li>17% - other</li>
        </ul>
        <p>
          Hazardous situations can present themselves when you least expect it
          and can cause serious injury or death. Even when you are already being
          cautious and prepare yourself for sudden hazards, you may not have the
          ability to control the outcome of the sudden situation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bicycle accident attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/text-header-images/santa-ana-bike-accident-statistics.jpg')"
            }}
          >
            <h2>Bike Accident Statistics</h2>
          </div>
        </LazyLoad>
        <p>
          Until you see the facts you don't quite understand the full extent of
          how dangerous riding a bike can be, especially when surrounded by
          speeding vehicles zipping by just feet and possibly inches away.
        </p>
        <ul>
          <li>
            Estimated bicyclist injuries in recent years averages about 45,000
          </li>
          <li>Bicyclist fatalities in recent years was 726</li>
          <li>
            In recent years fatality averages have dropped, but injuries have
            increased
          </li>
        </ul>
        <p>
          Although sharing the road with bicyclist and pedestrians is becoming
          more and more common and better respected, there are still reckless,
          distracted and dangerous drivers that pose a great threat to the
          vulnerable neighbors on the road.
        </p>
        <p>
          Too learn more about bicycling and pedestrian data, laws, statistics
          and information, visit{" "}
          <Link
            to="http://www.pedbikeinfo.org/data/factsheet_crash.cfm"
            target="new"
          >
            PedBikeInfo.org
          </Link>
          .
        </p>
        <p>
          If you or a loved one has been involved in a bike crash, contact one
          of our highly experienced{" "}
          <strong> Santa Ana Bicycle Accident Attorneys</strong> at{" "}
          <strong> 949-203-3814</strong> and get a
          <strong> free consultation</strong>.
        </p>
        <h2>Bicycle Accident Wrongful Death</h2>
        <p>
          When a bicyclist is killed as a result of negligence or wrongdoing,
          the family of the deceased victim can file what is known as a{" "}
          <Link to="/santa-ana/wrongful-death">wrongful death claim</Link>{" "}
          against the at-fault party.
        </p>
        <p>
          Family members may ask for compensation for damages including medical
          and funeral costs, loss of future income and benefits and loss of
          love, care and companionship. A wrongful death case is a civil action
          and is different from any criminal proceedings that may occur in
          connection with the incident.
        </p>
        <p>
          For example, if the at-fault driver is facing manslaughter charges, a
          conviction on those criminal charges does not guarantee monetary
          compensation for the victim's family. They may only obtain
          compensation by filing a bicycle wrongful death claim against the
          at-fault party in a civil court.
        </p>
        <h2>How a Santa Ana Bicycle Accident Attorney Can Help</h2>
        <p>
          The experienced Santa Ana bicycle accident lawyers at Bisnar Chase
          have a long and successful track record of securing compensation for
          injured victims and their families. We will remain on your side, fight
          for your rights and ensure that you receive fair compensation for your
          injuries, damages and losses.
        </p>
        <p align="center">
          <strong>
            {" "}
            Please call us at 949-203-3814 for a Free, Comprehensive and
            Confidential Consultation
          </strong>
          .
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
