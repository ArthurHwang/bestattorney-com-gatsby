// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/motorcycle-accident-santa-ana.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Accident Lawyers in Santa Ana - Bisnar Chase"
        pageDescription="Call 949-203-3814 for experienced motorcycle accident attorneys in Santa Ana. If you are the victim of a no-fault bike accident you may be entitled to compensation. Free Consultations. No win, no-fee lawyers since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Motorcycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana motorcycle accident attorneys"
            title="Santa Ana motorcycle accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Motorcycles, unlike regular four-door vehicles, do not offer
          protection for riders from factors such as careless drivers. Our{" "}
          <strong> Santa Ana Motorcycle Accident Lawyers </strong>believe that
          those careless drivers should be held accountable for a severe injury
          or wrongful death.
        </p>
        <p>
          Santa Ana is part of the second largest metropolitan area in the U.S.
          and the 4th-most densely populated city in the United States. With
          such a large number of citizens driving in a relatively small space,
          it is no wonder that the number of motorcycle accidents continues to
          warrant concern.
        </p>
        <p>
          Cyclists who have been involved in a bike wreck suffer major injuries
          such as serious harm to the brain or experience massive spinal cord
          damage.
          <strong>
            {" "}
            Most insurance companies will not cover the cost of your injuries,
            leaving you to fend for yourself
          </strong>
          .
        </p>
        <p>
          The Santa Ana motorcycle accident attorneys of Bisnar Chase are here
          to fight for you when no one else will. Our legal representation has
          held a<strong> 96% percent success rate for over 40 years</strong>.
          The law offices of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          has won an estimated
          <strong> $500 million dollars in earnings</strong> for our injury
          clients and plan to win you the maximum amount of compensation for you
          too.
        </p>
        <p>
          <strong>
            {" "}
            Call 949-203-3814 for a No-Hassle, No-Obligation Free Consultation
          </strong>
          . Our legal experts will take your call from{" "}
          <strong> Monday-Friday 8:30 a.m.-5:00 p.m</strong>.
        </p>
        <p>
          Gain the compensation you deserve with the law firm of Bisnar Chase.
        </p>
        <h2>Santa Ana Motorcycle Accidents and Statistics</h2>
        <p>
          According to the{" "}
          <Link
            to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
            target="_blank"
          >
            California Statewide Integrated Traffic Records System
          </Link>
          (SWITRS), 46 people were injured due to Santa Ana motorcycle
          accidents. Unfortunately, many of these injuries were severe because
          riders have so little protection when they are involved in a
          collision. Drivers complain that motorcycle riders are harder to see,
          but the obvious problem is that drivers tend to not pay enough
          attention to motorcyclists and it leads to devastating results.
        </p>
        <p>
          <strong>
            {" "}
            Other Santa Ana motorcycle tragedies that have occurred include
          </strong>
          :
        </p>

        <ul>
          <li>
            June of 2011, a man suffered life-threatening injuries when the
            driver of a Toyota Highlander collided with his motorcycle at the
            intersection of Maple and First Street in Santa Ana. The motorcycle
            victim was immediately transported to the nearest hospital where he
            received emergency care that kept him alive. This is not the first
            motorcycle accident at this intersection and some locals claim that
            the landscape makes it difficult to see in every direction.
          </li>
          <li>
            August of 2011, a man riding his Harley-Davidson was struck by a
            white Honda Accord. The motorcycle collision occurred on North
            Bristol Street, near Memory Lane, and the rider was immediately
            transported to a local hospital for emergency treatment. The driver
            who struck the motorcyclist was exiting his apartment complex when
            the collision occurred.
          </li>
        </ul>

        <LazyLoad>
          <img
            alt="person riding a motorcycle"
            width="100%"
            title="Santa Ana motorcycle crash lawyers"
            src="/images/motorcycle-accidents/santa-ana-motorcycle-image.jpg"
          />
        </LazyLoad>

        <h2>3 Common Causes of Santa Ana Motorcycle Crashes</h2>
        <p>
          Many motorcycle and car accidents in Southern California, specifically
          Santa Ana, could have been easily prevented. Under some circumstances
          the accident may not be a driver's fault. Some locals feel that there
          are Santa Ana intersections that are not safe, blocking the view of
          oncoming traffic from certain angles creating a dangerous
          circumstance.
        </p>
        <p>
          <strong> Three common causes of motorcycle crashes are:</strong>
        </p>
        <ol>
          <li>
            <strong> Speeding</strong>: Studies have proven that the average
            speed on impact that a motorcyclist was riding at was 86 mph in.
            Riders are required to abide by the speed limits of the road. Just
            like regular motor vehicles, motorcycle drivers should not exceed
            the speed limit of 65 mph.
          </li>
          <li>
            <strong> Lane-Changing</strong>: Drivers and cyclist should
            acknowledge each other when making a lane change. One way a driver
            can avoid hitting a motorcycle rider is by checking he/she's blind
            spot. Riders need to be cautious during heavy traffic because of the
            multiple lane changes that cars will be making.
          </li>
          <li>
            <strong> Debris in the road</strong>: Liability for a motorcycle
            crash being caused by street debris can vary and can be difficult to
            determine. There would have to be a negligent party involved in
            order to have a strong case. If debris in the road was caused by a
            natural disaster such as rocks or a tree falling then it would be
            difficult to obtain compensation for the injuries. If debris was
            caused by a driver, for example, a piece of wood flying off of the
            vehicle then the driver would be held liable.
          </li>
        </ol>
        <p>
          We contact the city to alert them of these dangerous intersections and
          have in the past sued to obtain multi-million dollar settlements and
          verdicts. If you or a loved one have been injured in a motorcycle
          collision in Santa Ana, CA it is in your best interest to contact an
          experienced Orange County motorcycle attorney today.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/iIJ67cH3Ma0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            John Bisnar shares more on the common causes of motorcycle accidents
          </blockquote>
        </center>

        <h2>Common Santa Ana Motorcycle Accident Injuries</h2>
        <p>
          Since motorcycles do not provide a barrier for riders, cyclists need
          to rely on their coordination skills when operating the bike. It is
          required under{" "}
          <Link to="http://www.bikersrights.com/states/california/california.html">
            California law
          </Link>{" "}
          that motorcycle riders wear a helmet, but it is not required that
          riders wear protective gear. Even though the law does not require
          cyclists to wear gear such as durable jackets and pants it is strongly
          recommended. Protective gear can lessen the blow to the body if an
          accident occurs. A motorcycle injury sustained in a bike crash can
          range from minor to catastrophic.
        </p>
        <p>
          <strong>
            {" "}
            Injuries that can motorcycle accident victims can experience
          </strong>
          :
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="https://www.verywellhealth.com/road-rash-treatment-1298921"
              target="_blank"
            >
              {" "}
              road rash
            </Link>
          </li>
          <li>broken bones</li>
          <li>bikers arm</li>
          <li>damage to muscle tissue</li>
          <li>brain injury</li>
          <li>neck injuries</li>
          <li>whiplash</li>
          <li>vertigo</li>
          <li>anxiety</li>
          <li>PTSD</li>
        </ul>
        <h2>Recoveries You Can Gain From Your Injury Claim</h2>
        <p>
          The amount of compensation that you can possibly receive for your
          accident will depend on the damages acquired from the accident. For
          instance if you have suffered spinal cord damage you may need on-going
          physical therapy.
        </p>
        <p>
          You can obtain compensation for more than just medical bills though.
          If the injury is very extensive and you are required to take time off
          of work you can also gain compensation for lost wages and future
          earnings.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Motorcycle wreck attorneys in Santa Ana"
            style={{
              backgroundImage:
                "url('../images/text-header-images/santa-ana-motorcycle-accident-attorneys.jpg')"
            }}
          >
            <h2>Award-Winning Legal Representation</h2>
          </div>
        </LazyLoad>
        <p>
          Finding the best Santa Ana motorcycle accident attorneys can be hard,
          but it doesn't have to be. Our 96% success rate and history of high
          client satisfaction can give you the peace of mind to concentrate on
          your treatment rather than your lawsuit.
        </p>
        <p>Our mission statement declares:</p>
        <h3>
          <strong>
            {" "}
            <em>
              "To provide superior client representation in a compassionate and
              professional manner while making our world a safer place."
            </em>
          </strong>
        </h3>
        <p>
          The law firm of Bisnar Chase has a history of winning large injury
          cases and helping clients get their lives back on track.
        </p>
        <p>
          <strong>
            If you are looking for an attorney who will give you the experience
            that you deserve, or are simply looking for free information,
            contact our experienced{" "}
            <Link to="/motorcycle-accidents" target="_blank">
              {" "}
              motorcycle accident lawyers
            </Link>{" "}
            today.
          </strong>
        </p>
        <p>
          <strong> Call 949-203-3814</strong> and earn a{" "}
          <strong> Free case evaluation</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
