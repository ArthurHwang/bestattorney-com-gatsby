// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "employment/person-terminated-job-santa-ana.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Employment Lawyers - Bisnar Chase"
        pageDescription="If you have experienced a wrongful termination or sexual harassment in a work environment call 949-203-3814 to speak with Santa Ana employment attorneys. The law firm of Bisnar Chase has recovered over $500 million dollars in compensation since 1978. Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Employment Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana employment lawyers"
            title="Santa Ana employment lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong> Santa Ana Employment Lawyers </strong>at Bisnar Chase are
          dedicated to helping wronged workers protect their rights and secure
          fair compensation for their losses.
        </p>
        <p>
          <strong>
            {" "}
            We have more than 40 years of experience handling employment law
            cases and protecting the rights
          </strong>{" "}
          of those who have been victims of negligence or wrongdoing on the part
          of others that may include individuals, entities or large
          corporations. Our Santa Ana employment attorneys have a comprehensive
          understanding of protections, safeguards state and federal laws that
          workers are entitled to in Santa Ana.
        </p>
        <p>
          The <Link to="/santa-ana">Personal Injury</Link> Law Firm of Bisnar
          Chase handles a number of employment cases such as severance, hostile
          work environments, sexual harassment claims and wrongful terminations.
        </p>
        <p>
          If you have experienced harassment, retaliation or or unfair treatment
          at work, contact us at{" "}
          <strong>
            {" "}
            <Link to="tel:+1-949-203-3814">949-203-3814</Link>
          </strong>{" "}
          for a <strong> free consultation</strong>.
        </p>
        <p>The law offices of Bisnar Chase will fight for you.</p>
        <p>
          You can fill out a <Link to="/contact">contact form </Link>here.
        </p>
        <h2>Am I Entitled to Severance Pay if I work in Santa Ana?</h2>
        <p>
          Severance agreements explain the terms for the end of an employee's
          relationship with an employer. Typically, the agreement will state a
          set amount of compensation to be paid to the employee when he or she
          leaves the company. This payment may include a set amount of wages,
          unpaid vacation time, bonuses, commissions or other types of
          compensation.
        </p>
        <p>
          If you have been let go from your job, or laid off, talking about fair
          severance pay with your employer could be intimidating. However, if
          you are not clear or don't agree with the severance agreement, do not
          sign it.
        </p>
        <p>Do not be pressured to give up your rights.</p>
        <p>
          When there is no specific contractual obligation or an employment
          policy,{" "}
          <Link to="https://www.dir.ca.gov/dlse/finalpay.pdf" target="_blank">
            {" "}
            California employers are not required by law to provide severance
            pay
          </Link>
          . However, there are a number of factors that could affect the amount
          of money or level of benefits that you could receive from your
          employer as part of your severance pay.
        </p>
        <p>
          <strong>
            {" "}
            Some of the factors that might affect your severance pay include
          </strong>
          :
        </p>
        <ul>
          <li>the amount of time you've served with the company</li>
          <li>your seniority</li>
          <li>
            the size and profitability of the company and the circumstances
            under which your employment was terminated
          </li>
        </ul>
        <p>
          Even though you may not be able to negotiate the amount of severance
          pay your employer offers, you may be able to do so for other types of{" "}
          <Link
            to="https://www.payscale.com/compensation-today/2009/08/non-monetary-compensation"
            target="_blank"
          >
            {" "}
            non-monetary compensation
          </Link>{" "}
          such as continued medical and dental benefits, a favorable letter of
          reference and retention of company property such as a laptop or cell
          phone.
        </p>
        <h2>Hostile Work Environment</h2>
        <p>
          A hostile work environment occurs when an employee experiences
          harassment in the workplace to the extent where the offensive,
          intimidating, abusive or oppressive behavior creates an intolerable
          work environment.
        </p>
        <p>
          The conduct is also considered hostile when it interferes with a
          worker's ability to perform his or her job.
        </p>
        <p>
          <strong>
            {" "}
            Often, a hostile work condition could result from workplace
            harassment due to a person's
          </strong>
          :
        </p>
        <ul>
          <li>race</li>
          <li>religion</li>
          <li>color</li>
          <li>gender</li>
          <li>disability</li>
          <li>age</li>
        </ul>
        <p>
          If you are being subjected to any unlawful employer behavior because
          of your age, race, gender, sexual orientation, disability, religion,
          national origin, pregnancy, etc. you may be able to seek compensation
          for your losses by filing an employment lawsuit. If you are facing
          consistent hostile behaviors that are pervasive and continue over time
          and disrupt your ability to perform your job duties or interfere with
          your career progress, your employer may be in violation of California
          and federal laws.
        </p>
        <p>
          The Orange County workplace harassment lawyers of Bisnar Chase
          represents clients with various types of cases that that involve a
          hostile work environment. With our legal representation our clients
          have gained over
          <strong> $500 million dollars in compensation</strong> including those
          for employment disputes.. When you call our law offices, you will
          receive a free consultation with one our top-notch legal experts.
        </p>
        <p>
          <strong> Contact us at 949-203-3814.</strong>
        </p>
        <LazyLoad>
          <img
            src="/images/employment/santa-ana-hostile-work-enviroment.jpg"
            width="100%"
            alt="Santa Ana employment attorneys"
          />
        </LazyLoad>
        <h2>Sexual Harassment in the Santa Ana Work Environment</h2>
        <p>
          It is unlawful under state and federal laws to harass a person because
          of his or her sex.
        </p>
        <p>
          {" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=51.9."
            target="_blank"
          >
            {" "}
            Sexual harassment
          </Link>{" "}
          refers to unwelcome sexual advances, requests for sexual favors or
          other verbal or physical harassment of a sexual nature. Harassment
          does not necessarily have to be of a sexual nature and could include
          offensive remarks about a person's sex. For example, it is illegal to
          harass a woman by making offensive comments about women in general.
          Both the victim and the harasser could be either a man or a woman and
          the victim and harasser could be the same sex.
        </p>
        <p>
          The law does not prohibit simple teasing, offhand comments or isolated
          incidents. However, harassment is illegal when it is so frequent or
          severe that it creates a hostile work environment or when it results
          in an adverse employment decision such as the victim being fired or
          demoted.
        </p>
        <p>
          The harasser could be the victim's supervisor, a supervisor in another
          area, a co-worker or even someone who is not an employee such as a
          customer or a client.
        </p>
        <h2>Wage and Hour Labor Law Claims</h2>
        <strong> California Labor Code 512 states that</strong>:
        <p>
          <em>
            <span>
              An employer may not employ an employee for a work period of more
              than five hours per day without providing the employee with a meal
              period of not less than 30 minutes, except that if the total work
              period per day of the employee is no more than six hours, the meal
              period may be waived by mutual consent of both the employer and
              employee. An employer may not employ an employee for a work period
              of more than 10 hours per day without providing the employee with
              a second meal period of not less than 30 minutes, except that if
              the total hours worked is no more than 12 hours, the second meal
              period may be waived by mutual consent of the employer and the
              employee only if the first meal period was not waived.
            </span>
          </em>
        </p>
        <p>
          This means that if an employer interferes with rest breaks and meal
          breaks it can be deemed as an illegal act upon a worker. The employee
          is not required to complete job duties such as simply answering phones
          or dealing with customers when on breaks or meal times
        </p>
        <p>
          If the employee does have to perform tasks during their work break
          they must be compensated. Similar regulations also apply to an
          employee working overtime. Employers must pay workers if they are
          working "off-the-clock."
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Employment lawyers in Santa Ana"
            style={{
              backgroundImage:
                "url('/images/employment/termination-letter-santa-ana.jpg')"
            }}
          >
            <h2>Bisnar Chase's Wrongful Termination Lawyers Will Help You</h2>
          </div>
        </LazyLoad>
        <p>
          Like many states, California is an "
          <Link
            to="https://en.wikipedia.org/wiki/At-will_employment"
            target="_blank"
          >
            at-will
          </Link>
          " state, which means employers can fire employees without providing a
          reason. However, there are situations where firing an employee could
          be illegal and considered "wrongful termination."
        </p>
        <p>
          When employees have an actual or implied contract with their employer,
          they cannot be terminated without just cause. Finally employees cannot
          be fired for refusing to do something illegal.
        </p>
        <p>
          Employers also cannot retaliate against an employee who is a "
          <Link
            to="https://www.whistleblower.org/what-whistleblower"
            target="_blank"
          >
            whistle blower
          </Link>
          " or someone who files a complaint with the government about unsafe
          conditions or illegal activities taking place at the workplace. If
          evidence shows that an employer was made aware that a employee was
          working "off-the-clock" and was not paid for the duration of their
          work, the employer is now in violation of specific labor laws.
        </p>
        <h2>
          Contact our Experienced Santa Ana Labor Law Attorneys for a Free
          Consultation
        </h2>
        <p>
          If you or a loved one has been the victim of wage violations, hostile
          work environment, harassment and wrongful termination you may be able
          to receive compensation for your losses.
        </p>
        <p>
          Our <strong> Santa Ana Employment Lawyers</strong> have been
          representing employees who are being mistreated in the workplace and
          being denied their basic civil rights.
        </p>
        <LazyLoad>
          <img
            alt="an attorney and a client shaking hands"
            width="100%"
            title="Wage and Hour lawyers in Santa Ana "
            src="/images/employment/santa-ana-worker-justice.jpg"
          />
        </LazyLoad>
        <p>
          <strong> Our mission statement vows</strong>
          <em>
            {" "}
            "To provide superior client representation in a compassionate and
            professional manner while making our world a safer place."
          </em>
        </p>
        <p>
          <strong> Since 1978</strong>, the law firm of Bisnar Chase has been
          specializing in various practice areas in Southern California
          locations such as Orange County and Los Angeles.
        </p>
        <p>
          Our employment law attorneys believe that employees who have suffered
          at the hands of a negligent or a predator supervisor should have
          justice.
        </p>
        <p>
          Call us at <strong> 949-203-3814</strong> for a free consultation on
          your employment claim.{" "}
        </p>
        <p>
          <strong> Contact the law offices of Bisnar Chase today</strong>.{" "}
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
