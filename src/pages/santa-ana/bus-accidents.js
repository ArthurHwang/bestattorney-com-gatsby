// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "bus-accidents/bus-santa-ana-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Bus Accident Attorneys - Public Transportation Crash"
        pageDescription="Contact the Santa Ana bus accident lawyers of Bisnar Chase for a free consultation. We've been representing injured plaintiffs in Orange County California for 40 years. You may be entitled to compensation for your public transportation bus crash including medical bills and lost wages."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Bus Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana bus accident lawyers"
            title="Santa Ana bus accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> Santa Ana Bus Accident Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          have over
          <strong>
            {" "}
            40 years experience and have retained a 96% success rate
          </strong>{" "}
          for injury victims. Our clients know that we will go to bat for them
          and are not merely attorneys who "settle cases". We are trial lawyers
          and have taken thousands of cases in front of a jury.
        </p>
        <p>
          Bus accidents are not as common as other types of motor vehicle or car
          accidents. But, they can be absolutely devastating when they do occur.
        </p>
        <p>
          In addition, buses are increasingly becoming an option for commuters
          who wish to save time and money. If you or a loved one has been
          injured in a bus crash, an experienced Santa Ana bus accident case
          attorney can help you better understand your legal rights and options.
        </p>
        <p>
          <strong> Call 949-203-3814 for a free consultation</strong>.
        </p>
        <h2>Santa Ana Bus Accident Statistics</h2>
        <p>
          Every year, buses carrying millions of passengers that include
          tourists and children are involved in catastrophic crashes. In heavily
          populated areas of Orange County such as Santa Ana, more and more
          commuters use rapid transit and public transportation. Commuters and
          college students are favoring transit buses. Below are the number of
          crashes, injuries and deaths that occur from bus incidents.
        </p>
        <ol>
          <li>
            According to the{" "}
            <Link
              to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts"
              target="_blank"
            >
              {" "}
              Federal Motor Carrier Safety Administration
            </Link>
            , California, New York and Florida had the greatest number of{" "}
            <strong> bus accidents</strong> from 2004 to 2008.
          </li>
          <li>
            The total fatalities nationwide for 2008 showed an increase of 3.4
            percent from the 2007 number of 354.
          </li>
          <li>
            In 2008, 366 people were killed in bus accidents. Of those, six were
            bus drivers and 86 were passengers on the bus. Other vehicle drivers
            and passengers represented 48.9 percent of 179 of the fatalities.
          </li>
          <li>
            Non-motorists represented 94 or 25.7 percent of the fatalities.
          </li>
          <li>
            During the five-year period covered by this study, a majority of bus
            collisions, about 32 percent, involved urban transit buses.
          </li>
          <li>
            School buses were involved in 30.8 percent of bus crashes during the
            period.
          </li>
          <li>
            Charter buses accounted for 12.3 percent of the bus accidents.
          </li>
        </ol>
        <h2>Bus Operators' Duty of Care</h2>
        <p>
          Public transportation has increasingly become an option for commuters
          who wish to save time and money. However, the unfortunate fact is that
          as more people have started to take buses, the number of injuries and
          deaths resulting from bus accidents are also on the rise.
        </p>
        <p>
          <LazyLoad>
            <img
              alt="children stepping onto a bus"
              width="100%"
              title="School bus collision attorneys"
              src="../images/bus-accidents/kids-on-school-bus-santa-ana.jpg"
            />
          </LazyLoad>
          It is the legal responsibility of all bus operators to transport their
          passengers safely. This is known as the bus operator's "duty of care";
          Bus companies have the highest "duty of care"; to their passengers -
          more than what they have toward other motorists, bicyclists or
          pedestrians.
        </p>
        <p>
          If a bus accident occurred because of the company or the driver's
          negligence, then the bus operator can be held liable for the injuries
          and damages that occur as a result of such negligence. This also
          applies to school buses.
        </p>
        <p>
          <strong>
            {" "}
            Minors who were severely injured in a Santa Ana bus accident can
            have a better chance of earning compensation if
          </strong>
          :
        </p>
        <ul>
          <li>at the time of injury students were owed the duty of care</li>
          <li>the injury could have been prevented</li>
          <li>the driver or school was violated the standards of safety</li>
        </ul>
        <h2>Bus Accident Causes in Santa Ana</h2>
        <p>
          It is best to seek legal representation from a expert Santa Ana bus
          accident lawyer to earn the winnings you need for damages. Knowing
          what caused the crash can help you better understand what you deserve
          in terms of compensation as well. There are a number of reasons why
          bus accidents may occur.
        </p>
        <p>
          <strong> The common causes include</strong>:
        </p>
        <p>
          <strong> Bus driver negligence:</strong> When a bus driver is
          impaired, distracted or fatigued, it can be extremely dangerous not
          only for passengers but others on the roadway as well. Bus companies
          are responsible for hiring qualified drivers and conducting random
          testing for alcohol and drug use. Bus drivers are also required under
          federal law to maintain a log of the hours they work to ensure that
          they get adequate rest. Drowsy driving was a factor in some of the
          most devastating bus accidents in United States history.
        </p>
        <p>
          <strong> Poor bus maintenance:</strong> Aging buses and lack of proper
          vehicle maintenance can cause serious problems. Brake failure, tire
          blowouts and transmission failure can result in major injury bus
          accidents. It is the responsibility of the bus company to properly
          service and maintain its fleet.
        </p>
        <p>
          <strong> Defective products:</strong> If a defective bus product
          causes an accident, the manufacturer of the product can be held
          liable. Victims may be able to file a products liability claim seeking
          compensation.
        </p>
        <p>
          <strong> Dangerous roadways:</strong> Steep hills, tight curves and
          large potholes can also pose significant hazards for buses. When a
          poorly designed or maintained roadway causes a bus accident, the
          governmental agency responsible for maintaining the roadway can also
          be held liable for injuries and damages caused. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            {" "}
            California Government Code Section 911.2
          </Link>
          , any personal injury or wrongful death claim against a public entity
          must be filed within six months of the incident.
        </p>
        <p></p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Advantages of Hiring a Santa Ana Bus Accident Attorney"
            style={{
              backgroundImage:
                "url('/images/bus-accidents/santa-ana-text-header-bus-image.jpg')"
            }}
          >
            <h2>Advantages of Hiring a Santa Ana Bus Accident Attorney</h2>
          </div>
        </LazyLoad>
        <p>
          The knowledgeable{" "}
          <strong>
            {" "}
            Santa Ana{" "}
            <Link to="/bus-accidents" target="_blank">
              {" "}
              Bus Accident Lawyers
            </Link>{" "}
          </strong>{" "}
          at Bisnar Chase have a long and successful track record of fighting
          for the rights of bus accident victims and their families.
        </p>
        <p>
          Our legal experts have gained over{" "}
          <strong> $500 million dollars in earnings </strong>for victims who
          have experienced great pain and suffering after a bus crash.
        </p>
        <p>
          If you or a loved one has been injured in a bus accident in Santa Ana,
          please contact the law firm of Bisnar Chase to find out how we can
          help.
        </p>
        <p>
          <strong> Call 949-203-3814 for a No-Hassle, No-Obligation </strong>
        </p>
        <p>
          <strong>
            {" "}
            Earn a Free Consultation with one of our personal injury attorneys.
          </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
