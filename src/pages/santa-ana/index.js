// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/accident-lawyers-santa-ana-california-header-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Santa Ana",
      population: 334227,
      totalAccidents: 13028,
      intersection1: "Bristol St & Warner Ave",
      intersection1Accidents: 119,
      intersection1Injuries: 110,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 200.6,
      city1Name: "Tustin",
      city1Index: 129.3,
      city2Name: "Fountain Valley",
      city2Index: 153.0,
      city3Name: "Orange",
      city3Index: 121.9,
      city4Name: "Garden Grove",
      city4Index: 181.0
    },
    locationBox3: {
      intersection2: "1st St & Harbor Blvd",
      intersection2Accidents: 107,
      intersection2Injuries: 106,
      intersection2Deaths: 0,
      intersection3: "Bristol St & Memory Ln",
      intersection3Accidents: 83,
      intersection3Injuries: 107,
      intersection3Deaths: 0,
      intersection4: "17th St & Bristol St",
      intersection4Accidents: 83,
      intersection4Injuries: 79,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Ana Personal Injury Lawyer - California Accident Attorneys"
        pageDescription="Our Santa Ana Personal Injury Lawyers have decades of success in and out of the courtroom. Find out what your case is worth for car accidents, pedestrian accidents, truck accidents and serious dog bite cases. Free consultation & 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Ana Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Ana Personal Injury Lawyers"
            title="Santa Ana personal injury lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The
          <strong> Santa Ana personal injury lawyers of Bisnar Chase </strong>
          have a long and successful track record of fighting for the rights of
          those who have been seriously injured or individuals who have lost
          loved ones as the result of someone else's negligence or wrongdoing.
          We are a strong legal team and our lawyers handle the toughest injury
          claims in California. Our personal injury attorneys have represented
          thousands of clients and are experienced in a multitude of practice
          areas. The lawyers of Bisnar Chase have held a a{" "}
          <strong> 96 percent success rate since 1978</strong>, securing
          hundreds of millions of dollars in verdicts and settlements.
        </p>
        <p>
          Santa Ana has the second largest population of residents and some of
          the best points of interest in Orange County. From the Santa Ana Zoo
          to the educational Discovery Zone, there is plenty to do in Santa Ana,
          but with it comes a lot of possibilities for injuries and accidents.
          From multiple freeway as intersecting to the dangers of the{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Santa_Ana_River"
            target="_blank"
          >
            Santa Ana riverbed
          </Link>
          ; car accidents, pedestrian accidents and premise liability injuries
          are on the rise. Bisnar Chase is located in Orange County making us
          very familiar with the rules and regulations of the Santa Ana courts.
          We have practiced in those courtrooms for decades and know the inner
          workings of the legal system in California, specifically Orange
          County.
        </p>
        <p>
          <strong>
            Looking for a law office with experienced accidents attorneys near
            92703?
          </strong>
        </p>
        <p>
          <Link to="/contact">
            <strong> Contact </strong>
          </Link>
          <strong>
            the Personal Injury Law Firm of Bisnar Chase at 949-203-3814 and
            schedule your
          </strong>
          <strong> free consultation on your personal injury case.</strong>
        </p>

        <h2>What Types of Injury Cases Do We Handle in Santa Ana?</h2>
        <p>
          Santa Ana is the county seat of government. It is home to diverse
          communities and bustling neighborhoods as well as offices, shopping
          centers and industrial buildings. The hectic activity in this vibrant
          city also creates an environment rife{" "}
          <Link
            to="https://www.neighborhoodscout.com/ca/santa-ana/crime"
            target="_blank"
          >
            {" "}
            with dangers
          </Link>{" "}
          and some of the most dangerous intersections in Orange County.{" "}
        </p>
        <p>
          Our Santa Ana personal injury lawyers represent victims of a number of
          injury-related incidents in Santa Ana including but not limited to:{" "}
        </p>
        <ul type="disc">
          <li>
            <strong>
              {" "}
              <Link to="/santa-ana/car-accidents">Car accidents</Link>:
            </strong>{" "}
            Our team of auto accident lawyers in Santa Ana have taken on some of
            the most difficult car accident cases in California. We have the
            resources, both financial and in terms of experience to represent
            cases other law firms will not.{" "}
          </li>
          <li>
            <strong> Pedestrian accidents:</strong> Pedestrian accidents can be
            difficult to pursue. Often times, the cases involve a hit and run
            and there is no one to go after for compensation. Our investigators
            work diligently to bring the wrongdoers to justice.{" "}
          </li>
          <li>
            <strong> Bus accidents:</strong> Bus congestion in Santa Ana is a
            real problem. Every day people are injured using public
            transportation and our law firm has a successful track record
            pursuing these types of cases.
          </li>
          <li>
            <strong> Truck accidents: </strong>
            Commercial truck accidents are cases that some law firms do not like
            to take on because of the difficulty dealing with the insurance
            companies playing hard ball. Bisnar Chase's Santa Ana accident
            lawyers have a reputation of being tough negotiators who aren't
            afraid of the insurance companies.{" "}
          </li>
          <LazyLoad>
            <img
              alt="truck and car crash into grassy meadow"
              width="100%"
              title="Accident attorney in Santa Ana California"
              src="/images/truck-accidents/personal-injury-lawyer-santa-ana-truck-crash.jpg"
            />
          </LazyLoad>
          <li>
            <strong> Bicycle accidents:</strong> Because there is so much
            traffic in Santa Ana we see a lot of bicycle accident cases in our
            office. We are residents of Orange County and know the courts well.
            We have continued success in and out of the courtroom.{" "}
          </li>
          <li>
            <strong> Slip-and-fall accidents: </strong>A premise liability case
            in Santa Ana is no different than the hundreds of premise cases we
            have taken on throughout Orange County. Whether it's a slip and fall
            at Walmart or a food poisoning case at Chipotle, we have the team to
            bring results.{" "}
          </li>
          <li>
            <strong> Dog bites:</strong>Dog bite cases can be very complicated
            involving Homeowners insurance and a multitude of injuries for the
            victim. Dog bite cases have strict liability therefore if the
            animals' owner has property and a homeowners insurance policy, you
            are entitled to be compensated. We have successfully taken on
            thousands of dog bite cases.{" "}
          </li>
          <li>
            <strong> Catastrophic brain injuries:</strong> One of the most
            tragic types of cases we see is where a client has had a
            catastrophic brain injury and no longer has any quality of life. the
            family is left feeling overwhelmed and unable to properly care for
            their loved ones needs. Our Santa Ana injury lawyers have had much
            success with catastrophic injury cases including a 24.7M verdict for
            a client who was hit by a car and left with serious head injuries.{" "}
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IuD-S72PMxk"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          <em>
            “…what I can guarantee you is with the resources of business with
            the verdicts that we've attained across the country and with the
            trial lawyer of the year awards that I have on others in my firm we
            will maximize the value of your case.”
          </em>
        </p>
        <blockquote>
          <strong>
            {" "}
            -California Personal Injury Attorney,{" "}
            <Link to="/attorneys/brian-chase">Brian Chase</Link>
          </strong>
        </blockquote>

        <h2>Santa Ana Workers Compensation</h2>
        <p>
          The responsibility of an employer is to make sure that their workers
          are not only compensated fairly but to also make sure the workers are
          safe on the job. When a worker is harmed due to poor working
          conditions liability for the employee's injuries can fall on the
          employer, property owner or the supervisor at the scene of the
          accident.{" "}
        </p>
        <p>
          Many injured employees might not know that they can receive
          compensation for their accident and find themselves asking "what is
          workers compensation?"
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.dir.ca.gov/dwc/employer.htm" target="_blank">
            {" "}
            Department of Industrial Relations
          </Link>
          , "California employers are required by law to have workers'
          compensation insurance, even if they have only one employee. And, if
          your employees get hurt or sick because of work, you are required to
          pay for workers' compensation benefits. Workers' comp insurance
          provides basic benefits, including medical care, temporary disability
          benefits, permanent disability benefits, supplemental job displacement
          benefits and a return-to-work supplement, and death benefits."
        </p>
        <p>
          One of the most common types of workers compensation claims is
          overexertion. Overexertion involves an employee being hurt due to
          performing tasks such as pushing, pulling or carrying heavy objects.
          When an employee is not getting paid the amount they deserve for
          example by working overtime, this can be a strong basis for a lawsuit.{" "}
        </p>
        <p>
          If you have suffered from serious injuries due to a negligent
          employer, contact the Santa Ana employment attorneys of Bisnar Chase
          today. When you call <strong> 949-203-3814</strong> our law firm at
          today you will receive a free case analysis for your personal injury
          case.{" "}
        </p>
        <LazyLoad>
          <img
            alt="injury victim and lawyer speaking"
            width="100%"
            title="Injury attorneys in Santa Ana"
            src="../images/personal-injury/santa-ana-accident-lawyers-case-worth-image.jpg"
          />
        </LazyLoad>
        <h2>What is Your Santa Ana, CA Personal Injury Case Worth?</h2>
        <p>
          If you have been injured in any type of accident, you probably have a
          number of questions racing through your mind. How much would you have
          to spend for medical expenses? How many days of work will you have to
          miss due to your injuries? Who will help pay for the cost of
          hospitalization, surgery and rehabilitative treatment such as physical
          therapy, which your health insurance may not fully cover? And, how can
          you hold the at-fault parties accountable for their negligence and/or
          wrongdoing?
        </p>
        <p>
          These are all valid questions. We have answers. If your injury was
          caused by the negligence or wrongdoing of another, the{" "}
          <Link to="https://www.courts.ca.gov/9618.htm" target="_blank">
            {" "}
            California courts
          </Link>{" "}
          state that you have a right to compensation. You may be entitled to
          receive compensation for all your injuries, damages and losses. The
          nature and extent of your injuries and losses will often determine
          what your case is worth. For example, if your injury was so minor that
          you didn't require any follow-up treatment at the hospital or any type
          of therapy, then, your case may not be worth much. Speak to a personal
          injury lawyer in Santa Ana, CA for more details.{" "}
        </p>
        <p>
          However, if you have suffered injuries that required hospitalization
          and caused you to take time off work, then your personal injury claim
          would be worth much more and
          <strong>
            {" "}
            you will need a skilled accident injury lawyer on your side as soon
            as possible to help protect your rights every step of the way
          </strong>
          . If you have suffered a catastrophic injury such as a traumatic brain
          injury or spinal cord damage that caused you to become paralyzed or
          permanently disabled, your injury claim could be worth significantly
          more.
        </p>
        <h3>
          Accident victims are able to seek compensation for damages including:
        </h3>
        <ul type="disc">
          <li>Medical expenses</li>
          <li>Lost income</li>
          <li>Cost of hospitalization</li>
          <li>Rehabilitation costs</li>
          <li>Loss of livelihood or future income</li>
          <li>Pain and suffering</li>
          <li>Emotional distress</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="lady justice statue"
            style={{
              backgroundImage:
                "url('/images/header-images/santa-ana-injury-lawyers-justice-image-final.jpg')"
            }}
          >
            <h2>How Can You Protect Your Rights in California?</h2>
          </div>
        </LazyLoad>

        <p>
          When you have been injured – be it in an auto accident, slip-and-fall
          accident, a dog attack or due to a defective product – there are a
          number of steps you can and should take in order to protect your legal
          rights. First and foremost, file a report or complaint to the
          appropriate authorities. This helps create a record of the incident.
          Gather as much evidence as you can from the scene where the incident
          took place. Take cell phone photographs, write down notes and obtain
          contact information for any people who may have witnessed the
          incident.{" "}
        </p>
        <p>
          If you are unable to do so because of your injuries, have a friend or
          family member gather that type of information for you because it could
          be extremely valuable during the claims process. Get prompt medical
          attention and treatment for your injuries. This not only puts you on
          the path to recovery, but also helps create a record of your injuries
          and the type of treatment and care you received. Keep a careful log of
          the number of workdays you missed due to the injuries. Save all
          invoices and bills relating to the incident. Contact an experienced
          Santa Ana injury attorney, who will remain on your side, fight for
          your rights and help you secure fair and full compensation for all
          your damages and losses.
        </p>
        <h2>The Importance of Accident Documentation</h2>
        <p>
          In order to present a strong claim, it is crucial that you document
          your losses diligently and methodically. Here are some of the items
          and pieces of evidence that can better your assist your{" "}
          <strong> Santa Ana personal injury lawyer</strong>.{" "}
        </p>
        <ul>
          <li>
            Photographic evidence including images from the scene of the
            incident, of your injuries and of vehicles, if the incident was a
            car accident.
          </li>
          <li>
            A journal detailing your injuries, the extent of your pain and
            things you could or could not do.
          </li>
          <li>
            Records of medical receipts, prescriptions, medical device receipts,
            co-payments, doctor office visits, physical therapy visits,
            chiropractor appointments.
          </li>
          <li>
            All correspondence including e-mails relating to your medical issues
            and expenses.
          </li>
          <li>
            Travel expenses for medical appointments and other visits related to
            the incident.
          </li>
          <li>Lost wages and any income lost due to the incident.</li>
        </ul>

        <h2>Hire a Top-Notch Southern California Accident Lawyer</h2>
        <p>
          When it comes to representing your rights and passionately pursuing
          justice on your behalf, our injury attorneys in Santa Ana leave no
          stone unturned.{" "}
        </p>
        <p>
          Asking yourself{" "}
          <strong>
            {" "}
            "What Santa Ana personal injury lawyer can I turn to near me?"
          </strong>
        </p>
        <p>
          We have a reputation with insurance companies and defense counsel for
          being fierce negotiators and advocates who are not afraid to go the
          extra mile to get justice for our clients. Our Orange County clients
          including those in the <strong> 92703 area, </strong>get superior
          legal representation and quality customer service from our entire team
          in the – of lawyers, paralegals and support staff. We support and
          cherish an office culture where we treat each other, our clients and
          community members with the dignity and respect we all deserve. We take
          cases on a contingency fee basis, giving our clients that guarantee
          that they{" "}
          <strong>
            {" "}
            won't be charged any fees unless we recover compensation
          </strong>{" "}
          for them.{" "}
        </p>
        <p>
          <strong>
            {" "}
            To obtain legal representation from a personal injury attorney in
            Orange County, CA, please call the Law Group of Bisnar Chase at
            949-203-3814.{" "}
          </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
