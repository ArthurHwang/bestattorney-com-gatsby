// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "ambulance-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Casos de mordeduras de perro en el Condado de Orange - Bisnar Chase"
        pageDescription="Una víctima de mordedura de perro puede ser compensada por las lesiones y el sufrimiento que ha tenido que experimentar como consecuencia de la negligencia del propietario del perro. ¡Comuníquese hoy mismo para ver si tiene un caso!"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Abogados especialistas en mordeduras de perro del estado de California
        </h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Abogado dedicado a mordeduras de perro en el Condado de Orange"
            title="Abogado dedicado a mordeduras de perro en el Condado de Orange"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong> Los abogados especialistas en mordeduras de perro</strong>{" "}
            del <strong> bufete Bisnar Chase de California</strong> han ganando
            casos de mordedura de perros exitosamente
            <strong> por más de 40 años</strong>.
          </p>
          <p>
            Las mordeduras de perros van más allá  de fracturas de huesos y
            cicatrices, el trauma emocional de una mordedura puede durar para
            siempre.
          </p>
          <p>
            {" "}
            <Link to="/abogados/lesiones-personales" target="_blank">
              {" "}
              Los abogados especialistas en lesiones
            </Link>{" "}
            de Bisnar Chase tratan múltiples casos de ataques de perro cada año.
            Contamos con la experiencia y  sabiduría para representar a nuestros
            clientes y ayudarles a obtener una compensación plena y justa por
            sus pérdidas.
          </p>
          <p>
            Nuestro dedicado equipo de abogados, paralegales, mediadores,
            ajustadores de seguros, entre otros miembros de nuestro bufete han
            ganado más de <strong> 300 millones de dólares</strong> para
            nuestros clientes. Ellos quieren mejorarle la vida a usted y hacer
            del mundo un lugar más seguro.
          </p>
          <p>
            Si ha sido mordido o atacado por un perro, debe consultar a un
            abogado experto en mordeduras de perro de California para determinar
            si tiene derecho a una compensación para saldar sus facturas médicas
            y daños; y para{" "}
            <strong>
              {" "}
              perseguir al acusado por una indemnización como víctima de la
              lesión.
            </strong>
          </p>
          <p>
            Para hablar con los mejores abogados en lesiones personales sobre
            sus derechos legales, comuníquese con nosotros al{" "}
            <strong> 877-958-8092</strong>.
          </p>
        </div>
        <h2>California es el Estado Número Uno en Ataques de Perro</h2>
        <p>
          Dos estudios recientes confirmaron que el estado California,
          especialmente el área de Los Ángeles, lidera en la nación en cuanto a
          incidentes de mordedura de perros.
        </p>
        <p>
          Por ejemplo, Los Ángeles ha ganado notoriedad por segundo año
          consecutivo como la ciudad en Estados Unidos con más ataques de perros
          hacia los carteros.
        </p>
        <p>
          <strong> De acuerdo al servicio postal de Estados Unidos</strong>:
        </p>
        <ul>
          <li>
            La ciudad de Los Ángeles ha tenido 69 casos de ataques de perro en
            el año fiscal 2012.
          </li>
          <li>
            En el 2011 Los Ángeles nuevamente  lideró con 83 ataques a carteros.
          </li>
        </ul>
        <p>
          De acuerdo a los expertos, el agradable clima del sur de California
          puede ser una de las razones por las que este tipo de accidentes
          prevalecen aquí.
        </p>
        <p>
          De igual manera, de acuerdo a un informe publicado por la compañía de
          seguros State Farm, California lideró en los números por reclamo de
          mordeduras de perros (527 casos) y la indemnización por este tipo de
          casos (20.3 millones de dólares) durante el año del 2012.
        </p>
        <h2>
          La Ley de Responsabilidad Estricta en el Estado de California a Las
          Mordeduras de Perro
        </h2>
        <p>
          El estado de California tiene lo que se conoce como &ldquo;la ley de
          responsabilidad estricta&rdquo; cuando se trata de ataques de perro.
          Lo que significa que los dueños son responsables por las acciones de
          sus mascotas, sin lugar a duda.
        </p>
        <p>
          La única excepción puede ser cuando el ataque del perro fue provocado
          por crueldad animal o cuando un perro muerde a alguien que estaba
          cometiendo un crimen o traspasando la propiedad privada.
        </p>
        <p>
          Sin embargo, existen pocas excepciones al respecto. En la mayoría de
          los casos, además de recibir una multa o enfrentarse a otras sanciones
          relacionadas con el ataque, el dueño puede ser responsable
          económicamente por el incidente.
        </p>
        <p>
          <strong>
            {" "}
            El código civil de California, artículo 3342 manifiesta que
          </strong>
          :
        </p>
        <p>
          <em>
            &ldquo;El dueño de cualquier perro es responsable por los daños y
            perjuicios de cualquier persona que haya sido mordida por su perro
            en lugares públicos o propiedades privadas, incluyendo la casa del
            dueño, sin importar si estaba consciente o no del riesgo amenazante
            del perro&rdquo;.
          </em>
        </p>
        <p>
          Existen casos recientes en California en que los jueces sostienen
          demandas de daños personales en contra de los propietarios por
          permitir a algún inquilino que mantenga a un perro peligroso en la
          propiedad.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Value of my personal injury claim"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dog-muzzle.jpg')"
            }}
          >
            <h2>Responsabilizar a Los Dueños de Perro por Los Ataques</h2>
          </div>
        </LazyLoad>

        <p>
          No es justo que las víctimas paguen un precio físico y económico tan
          alto por un ataque de perro, cuando el incidente no fue su culpa. Los
          dueños de perro se deben responsabilizar económicamente por las
          lesiones, daños y perjuicios y por las pérdidas causadas a las
          víctimas desprevenidas.
        </p>
        <p>
          Las víctimas lesionadas pueden solicitar una compensación por su caso
          de mordedura de perro.
        </p>
        <p>
          <strong> La compensación sería por daños y perjuicios</strong>:
        </p>
        <ul>
          <li>Gastos médicos</li>
          <li>Pérdida de sueldo</li>
          <li>Hospitalización rehabilitación</li>
          <li>Costos por cirugías</li>
          <li>Lesiones permanentes</li>
          <li>Daño moral y sufrimiento emocional. </li>
        </ul>
        <p>
          Los abogados especialistas en mordeduras de perro están familiarizados
          con las leyes de California referente a los ataques injustificados de
          perros y pelearán por usted en contra del dueño del perro.
        </p>
        <h2>Lesiones y Tratamientos en Casos de Mordeduras de Perro </h2>
        <p>
          Las víctimas o familiares a menudo gastan miles de dólares en terapias
          para que sus vidas vuelvan a la normalidad. La mayoría de las víctimas
          desarrollan  temor de largo plazo hacia los perros.
        </p>
        <p>
          En otros casos, algunas son sometidas a cirugías plásticas para tratar
          las cicatrices de las mordeduras. En ocasiones las cirugías no son
          exitosas y dejan cicatrices permanentes.
        </p>
        <p>
          <strong>
            {" "}
            Los incidentes de mordedura de perro pueden resultar en lesiones
            graves, incluyendo, pero no limitándose
          </strong>
          :
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="https://es.familydoctor.org/mordeduras-de-gatos-y-de-perros/"
              target="_blank"
            >
              {" "}
              Mordeduras
            </Link>
          </li>
          <li>Heridas punzantes</li>
          <li>Fracturas de huesos e incluso lesiones de nervios</li>
          <li>Cicatrices permanentes y desfiguración</li>
          <li>Problemas emocionales y psicológicos para las víctimas</li>
        </ul>
        <p>
          De igual manera, a las víctimas y a sus familiares se les dificulta
          pagar por los tratamientos correspondientes. La mayoría de compañías
          de seguros no cubren procedimientos de cirugía plástica, ya que por
          naturaleza se consideran procedimientos cosméticos y no para salvar la
          vida de un paciente.
        </p>
        <h2>Seguro Para Propietarios de Casa y Ataques de Perro </h2>
        <p>
          Las pólizas de seguros para propietarios de casa usualmente cubren
          mordeduras de perro. La mayoría de estas pólizas proporcionan entre
          $100.000 y $300.000 dólares en coberturas de responsabilidad. Victimas
          de mordeduras de perro pueden responsabilizar al propietario por
          permitir que un perro peligroso viva en su propiedad.
        </p>
        <p>
          En un desarrollo reciente, la compañía de seguros Farmers Group
          notificó a titulares de pólizas que ya no cubrirían mordidas de
          pitbulls, rottweilers ni perros lobo bajo su seguro para propietarios.
        </p>
        <p>
          A pesar de la aguda crítica de los defensores de pitbulls, las
          compañías de seguros dicen que las mordeduras de pitbulls y otras
          razas se han elevado considerablemente en los últimos años junto con
          el costo de reclamos.
        </p>
        <p>
          Farmers dijo que las tres razas mencionadas anteriormente
          representaron más del 25 por ciento en sus reclamos de mordeduras de
          perro. Igualmente, funcionarios de Farmers mencionaron que estas razas
          causaron más daño que cualquier otra raza cuando atacaron.
        </p>
        <LazyLoad>
          <img
            src="/images/images/Spanish Center Image.jpg"
            width="100%"
            alt="Abogados especialistas en mordeduras de perro del estado de California"
          />
        </LazyLoad>
        <h2>Consulta y Evaluación Gratuita en su Caso de Mordedura de Perro</h2>
        <p>
          Estamos aquí para ayudarle. Le brindamos una consulta y evaluación
          gratuita, y sin obligación en su caso de mordedura de perro.
        </p>
        <p>
          Cuando un perro ataca a un individuo—independientemente de su edad,
          aspecto físico, o atributos mentales—el incidente deja heridas que van
          más allá de la fisura física sufridas por la víctima.
        </p>
        <p>
          Existe un gran daño psicológico y emocional causado por mordeduras de
          perro que es irreparable, sea la víctima un hombre de seis pies de
          estatura o un niño de dos pies, ambos pueden  atravesar un periodo de
          intensa inseguridad y temor.
        </p>
        <p>
          Un ataque de un perro es traumático, totalmente. Las agresiones de un
          perro pueden causar ansiedad de larga duración.
        </p>
        <p>
          <strong>
            {" "}
            Las lesiones graves de{" "}
            <Link
              to="https://www.webconsultas.com/categoria/salud-al-dia/mordeduras-de-animales"
              target="_blank"
            >
              {" "}
              mordidas de animal
            </Link>{" "}
            puede causar:
          </strong>
        </p>
        <ul>
          <li>Contusiones profundas</li>
          <li>Fracturas</li>
          <li>Amputaciones</li>
          <li>Lesiones graves en la cara y cabeza</li>
          <li>Cicatrices emocionales de larga duración</li>
        </ul>
        <p>
          El abogado John Bisnar, especialista en mordeduras de perro en
          California, explica los pasos correspondientes a seguir después que
          haya sido mordido por un perro, incluyendo las fotografías que debe de
          tomar inmediatamente de sus lesiones.
        </p>
        <h2>Busque Ayuda Inmediata </h2>
        <p>
          Un abogado experimentado en mordeduras de perro hará la diferencia
          entre ganar o perder su caso.
        </p>
        <p>
          En{" "}
          <Link to="/abogados" target="_blank">
            {" "}
            Bisnar Chase
          </Link>
          , usted será representado por
          <strong>
            {" "}
            abogados especializados en mordeduras de perro en California
          </strong>{" "}
          con más de <strong> 40 años de experiencia</strong> y por un equipo de
          representantes legales y personal con un índice del 96 por ciento de
          éxito, quienes han ganado más de{" "}
          <strong> 300 millones de dólares</strong> para sus clientes.
        </p>
        <p>
          Usted será representado por un abogado que pasa gran cantidad de horas
          en los juzgados peleando por los derechos de querellantes lesionados y
          entiende estos casos mejor que un abogado común.
        </p>
        <p>
          Asegúrese de buscar un abogado que se especialice en mordeduras de
          perro y ataques de animales. Si usted ha sido lastimado o ha
          experimentado un suceso traumático y/o un ataque o mordedura de perro
          llámenos al <strong> 877-958-8092 </strong>para una
          <strong>
            {" "}
            consulta gratuita de ataques de perro sin problemas, confidencial y
            sin obligación.
          </strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
            Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
