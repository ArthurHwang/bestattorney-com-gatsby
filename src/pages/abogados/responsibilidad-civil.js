// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "slip-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Casos de responsabilidad civil - Bisnar Chase"
        pageDescription="Cuando hay una condición de inseguridad que es conocida y no se hace nada al respecto, existe la posibilidad de que se produzca una lesión con responsabilidad civil local. Llame a Bisnar Chase hoy para ver si tiene un caso!"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados dedicados a responsabilidad civil</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Abogados dedicados a responsabilidad civil"
            title="Abogados dedicados a responsabilidad civil"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Cuando{" "}
          <Link to="/abogados/lesiones-personales">se produce una lesión</Link>{" "}
          en una propiedad como consecuencia de la negligencia de un propietario
          o un administrador, el caso resultante se considera un caso de
          responsabilidad civil. Las lesiones de responsabilidad civil se
          producen cuando la persona a cargo de una propiedad que está abierta
          al público está al tanto de una condición peligrosa como un riesgo de
          incendio, un riesgo de tropiezo o un riesgo de seguridad, y no hace
          nada para solucionarlo, causando en consecuencia y siendo responsable
          de las lesiones o los fallecimientos que se produzcan debido a ese
          riesgo. En estas situaciones, la víctima puede iniciar una reclamación
          ante la compañía de seguros de la parte que cometió la negligencia y
          obtener compensación por la lesión que sufrió, así como por daños a la
          propiedad, salarios perdidos y otros daños en los que haya incurrido
          en consecuencia. En caso de que la compañía de seguros no pague estos
          daños, se puede iniciar un juicio contra la parte que cometió la
          negligencia y contra la compañía de seguros. Bisnar Chase tiene más de
          30 años de experiencia llevando a las partes responsables a juicio por
          sus ofensas y tenemos el conocimiento especializado para asegurarnos
          de que reciba un{" "}
          <Link to="/abogados/resultos-destacados">
            veredicto justo por sus pérdidas.
          </Link>
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Ejemplos de responsabilidad civil local"
            style={{
              backgroundImage: "url('/images/text-header-images/trip.jpg')"
            }}
          >
            <h2>Ejemplos de responsabilidad civil local</h2>
          </div>
        </LazyLoad>

        <div className="algolia-search-text">
          <p>
            Las lesiones por responsabilidad civil local se producen de muchas
            formas diferentes, y existen múltiples maneras de lesionarse si
            alguien ha permitido que hubiera una situación peligrosa en su
            propiedad. He aquí algunos de los casos de responsabilidad civil más
            comunes:
          </p>

          <ul>
            <li>
              <strong> Resbalones y caídas: </strong>
              Cuando existe riesgo de resbalarse o tropezar en el piso, es fácil
              que un cliente, especialmente una persona mayor, pierda el
              equilibrio y se lastime al caer. Esto puede ocurrir por un piso
              húmedo que no estuviera marcado como tal, por una alfombra o una
              baldosa floja que hace tropezar y caer a alguien o inclusive un
              escalón no señalizado que se fusiona con la acera.
            </li>

            <li>
              <strong> Riesgos de incendio: </strong>
              Los cables expuestos y los aparatos electrónicos fallados
              presentan un serio riesgo de incendio en un lugar público. A veces
              los electrodomésticos o los aparatos electrónicos se instalan mal
              o no cumplen con los códigos. Si, en consecuencia, un incendio o
              un choque eléctrico lesionan o matan a alguien, el propietario de
              ese lugar puede ser considerado responsable tan solo por no haber
              realizado las inspecciones o hecho un esfuerzo por descubrir si
              existía una condición de riesgo.
            </li>

            <li>
              <strong> Riesgos de seguridad: </strong>
              Los establecimientos que reciben clientes deben asegurarse de que
              sus edificios y las áreas circundantes se consideren seguros y que
              sus clientes no estén en riesgo cuando ingresen al lugar. Si un
              cliente es asaltado y lesionado en un callejón oscuro afuera de un
              bar, se puede demandar al bar por no haber proporcionado una
              protección de seguridad adecuada, así como por no proporcionar
              iluminación afuera para que sus clientes estén un poco más
              seguros.
            </li>

            <li>
              <strong> Ahogos accidentales: </strong>
              La mayoría de las piscinas comunitarias y cuerpos de agua profunda
              hoy tienen cercos que los circundan debido a lo fácil que es que
              un niño pequeño caiga en una piscina y se ahogue. Algunos cuerpos
              de agua, sin embargo, no tienen instauradas medidas de protección,
              y esto presenta un verdadero riesgo para los niños pequeños o
              inclusive para las personas que no están en su sano juicio.
            </li>

            <li>
              <strong>
                {" "}
                <Link to="/abogados/mordeduras-de-perro">
                  Mordeduras de perro:
                </Link>
              </strong>
              Los propietarios de perros tienen la responsabilidad de mantener a
              sus perros bien entrenados y socializados, para que no ataquen a
              víctimas confiadas, sin mediar provocación. Si un perro ataca a
              una víctima, el propietario es responsable por su comportamiento y
              la víctima puede presentar una reclamación a la compañía de
              seguros para el propietario o el arrendatario de la vivienda.
            </li>

            <li>
              <strong> Otras condiciones de inseguridad:</strong>
              Cuando hay una condición de inseguridad que es conocida y no se
              hace nada al respecto, existe la posibilidad de que se produzca
              una lesión con responsabilidad civil local. Bisnar Chase
              representó a una niña que quedó paralizada cuando una rama de un
              árbol que pesaba 600 libras cayó sobre ella en Lake Perris State
              Park. El Departamento de Parques y Recreación del Estado de
              California ordenó que se inspeccionaran y se podaran esos árboles
              cada dos años, pero el árbol no había sido inspeccionado en más de
              tres años, y por lo tanto fue encontrado responsable. Bisnar Chase
              también representó a un niño de dos años de edad que bebió
              detergente líquido para ropa que un personal de limpieza dejó
              abandonado en un patio. Sufrió quemaduras químicas y necesitó 76
              procedimientos para reparar su esófago. El personal de limpieza y
              el establecimiento que estaban limpiando fueron demandados y
              tuvieron que pagar los daños.
            </li>
          </ul>
        </div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Demostrar la negligencia local"
            style={{
              backgroundImage: "url('/images/text-header-images/slip.jpg')"
            }}
          >
            <h2>Demostrar la negligencia local</h2>
          </div>
        </LazyLoad>

        <p>
          Para tener un buen caso contra el propietario de un bien o una
          empresa, un fiscal debe tener evidencia contundente de que los
          administradores estaban al tanto de la situación peligrosa. Esto es
          difícil de lograr en situaciones como las lesiones por resbalones y
          caídas, o cuando un pedazo de un mueble o un equipo roto lastiman a un
          cliente, pero es más fácil de probar en casos como accidentes en
          piscinas de natación y riesgos de seguridad, en los cuales podría
          haber testigos que pueden dar testimonio de lo que sabían los
          administradores. Aunque crea que no podrá encontrar evidencia contra
          la parte responsable, debería contactar a un abogado de todos modos.
          Bisnar Chase{" "}
          <Link to="/abogados/recursos/elegir-un-abogado">
            ofrece consultas gratuitas a víctimas que han sufrido lesiones
          </Link>{" "}
          en un caso de responsabilidad civil local, y sabemos cómo probar la
          negligencia en situaciones en las cuales quizás usted crea que no hay
          evidencia para probarlo. ¡Si ha sufrido una lesión,{" "}
          <Link to="/abogados/contactenos">
            llame a Bisnar Chase Personal Injury Attorneys hoy para ver si tiene
            un caso!
          </Link>
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
