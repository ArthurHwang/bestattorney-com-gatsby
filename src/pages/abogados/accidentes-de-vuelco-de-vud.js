// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
// import Img from "gatsby-image"
// import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

// export const query = graphql`
//   query {
//     headerImage: file(
//       relativePath: {
//         eq: "text-header-images/truck-accident-spanish-banner.jpg"
//       }
//     ) {
//       childImageSharp {
//         fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
//           ...GatsbyImageSharpFluid
//         }
//       }
//     }
//   }
// `

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Accidentes por Vuelco de VUD - Bisnar Chase"
        pageDescription="Llame a los abogados de accidentes de SUV Rollovers si usted ha sido herido en un accidente en California. 1-800-561-4887."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados de Accidentes por Vuelco de VUD</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            Los abogados Californianos de Da&ntilde;os Personales de la firma
            legal Bisnar & Chase son experimentados y exitosos. Ellos
            representan a v&iacute;ctimas de da&ntilde;os severos y muerte por
            accidente y por uso de productos inseguros que incluye Accidentes
            por vuelco VUD. La firma se asienta en el Condado de Orange, pero
            representa a clientes en todo el sur de California. Si usted o un
            ser querido ha sufrido un da&ntilde;o severo como resultado de un
            accidente por vuelco de VUD u otro tipo de accidente,
            cont&aacute;ctese con nosotros hoy para hacer su consulta sin cargo.
          </p>
          <p>
            La publicidad de los Veh&iacute;culos Utilitarios Deportivos (Sport
            Utility Vehicles o SUV's) los define como fuertes, bien
            constru&iacute;dos, robustos y seguros. Pero se ha demostrado que
            esto no es necesariamente as&iacute;. El alto centro de gravedad en
            un veh&iacute;culo de este tipo es una amenaza de vuelco. Los
            veh&iacute;culos tienen un riesgo mayor de resbalar durante una
            colisi&oacute;n que otros veh&iacute;culos, lo cual resulta en
            da&ntilde;os mayores.
          </p>
          <h2>
            Las siguientes estad&iacute;sticas se basan en accidentes con
            veh&iacute;culos VUD:
          </h2>
          <p>
            En el 2001 un 25% de las muertes en autopistas se debieron a vuelcos
            de auto. En el 2000 m&aacute;s de 12,000 personas murieron en
            accidentes de vuelcos con VUD. En el 2000 m&aacute;s de el 60% de
            muertes relacionadas con VUD se debieron al vuelco de VUD.
          </p>
          <p>
            Nuestra firma est&aacute; comprometida a la protecci&oacute;n se los
            derechos de nuestros clientes que han sufrido Da&ntilde;os
            Personales y con recobrar el m&aacute;ximo capital monetario
            posible. Hemos recuperado decenas de millones de d&oacute;lares para
            nuestros clientes afectados por Da&ntilde;os Personales.
            1-800-561-4887.
          </p>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
