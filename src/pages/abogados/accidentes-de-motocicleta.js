// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
// import Img from "gatsby-image"
// import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

// export const query = graphql`
//   query {
//     headerImage: file(
//       relativePath: {
//         eq: "text-header-images/truck-accident-spanish-banner.jpg"
//       }
//     ) {
//       childImageSharp {
//         fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
//           ...GatsbyImageSharpFluid
//         }
//       }
//     }
//   }
// `

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Accidentes de Motocicletas Abogados - Bisnar Chase"
        pageDescription="Llame a los Accidentes de Motos Abogados si usted ha sido herido en un accidente en California. 1-800-561-4887."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados Californianos de Accidentes de Motocicleta</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            Los abogados Californianos de Da&ntilde;os Personales de la firma
            legal Bisnar & Chase son experimentados y exitosos. Ellos
            representan a v&iacute;ctimas de da&ntilde;os severos y muerte por
            accidente y uso de productos inseguros, incluyendo accidentes de
            motocicleta. La firma se asienta en el condado de Orange pero
            representa a clientes en todo el sur de California. Si usted o un
            ser querido ha resultado da&ntilde;ado en un accidente de
            motocicleta o en otro accidente serio, cont&aacute;ctese hoy con
            nosotros para realizar su consulta sin costo alguno
          </p>
          <h2>
            Lo que sigue son las estad&iacute;sticas de accidente en moto del
            a&ntilde;o 2002 en los Estados Unidos :
          </h2>
          <p>
            3,244 de motociclistas se murieron en accidentes. 65,000 de
            motociclistas sufrieron da&ntilde;os, un 7% m&aacute;s que en el
            2001. 8% de muertes en accidentes eran motociclistas.
          </p>
          <p>
            Las motos, comparadas a otros veh&iacute;culos, ofrecen un
            m&iacute;nimo de seguridad. Los accidentes en moto suelen acarrear
            m&uacute;ltiples da&ntilde;os al motociclista por la falta de
            protecci&oacute;n y resultar en traumatismos y da&ntilde;os severos
            al cerebro, amputaci&oacute;n, da&ntilde;o vertebral,
            par&aacute;lisis y muerte. Como los motociclistas se ven envueltos
            en un 80% de accidentes de veh&iacute;culo, usted se encuentra en
            constante riesgo si maneja una motocicleta.
          </p>
          <p>
            Nuestra firma est&aacute; comprometida a la protecci&oacute;n se los
            derechos de nuestros clientes que han sufrido Da&ntilde;os
            Personales y con recobrar el m&aacute;ximo capital monetario
            posible. Hemos recuperado decenas de millones de d&oacute;lares para
            nuestros clientes afectados por Da&ntilde;os Personales.
            1-800-561-4887.
          </p>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
