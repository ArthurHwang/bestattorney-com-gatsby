// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { FaQuoteLeft, FaQuoteRight } from "react-icons/fa"
import { IoMdStar } from "react-icons/io"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import { ReviewsData } from "../../data/reviews"
import TenStar from "../../images/10star.png"
import ReviewsButton from "../../images/testimonials-page/testimonials-reviews-button.png"
import VideoButton from "../../images/testimonials-page/testimonials-videos-button.png"
import WrittenButton from "../../images/testimonials-page/testimonials-written-button.png"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

function socialSwitch(string) {
  if (string === "Yelp") {
    return "https://www.yelp.com/biz/bisnar-chase-personal-injury-attorneys-newport-beach-4"
  }

  if (string === "Google") {
    return "https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
  }

  if (string === "Facebook") {
    return "https://www.facebook.com/california.attorney/"
  }

  if (string === "Super Pages") {
    return "https://www.superpages.com/bp/newport-beach-ca/bisnar-chase-personal-injury-attorneys-L2050662459.htm"
  }
}

export default function AboutUs({ location }) {
  return (
    <LayoutEspanolDefault sidebar={false} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bisnar Chase Personal Injury Lawyers: Client Reviews & Testimonials"
        pageDescription="Clients of Bisnar Chase discuss their experience with award winning California personal injury lawyers and staff."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}

        <div className="text-container">
          <div id="testimonials-intro">
            <div className="breadcrumb-title">
              <h1>Testimonios del Clientes</h1>
              <BreadCrumbs location={location} />
            </div>
            <p style={{ marginBottom: "1rem" }} className="first-paragraph">
              Para Bisnar Chase, los clientes son siempre lo primero y
              trabajamos incansablemente para asegurarnos de que su experiencia
              con nosotros sea positiva, en especial cuando han sufrido pérdidas
              debido a su caso. El nuestro es un bufete muy aclamado porque
              tratamos a nuestros clientes como si de nuestra familia se
              tratara, y siempre estamos dispuestos a ir un poquito más lejos
              para facilitar la vida de nuestros clientes.
            </p>
            <p>
              <strong>
                {" "}
                Vea y lea los testimonios de nuestros clientes a continuación
                que tienen `grandes cosas que decir sobre nuestro bufete.
              </strong>
            </p>
          </div>
        </div>

        <div id="testimonial-buttons">
          {" "}
          <Link to="/about-us/testimonials/#testimonial-videos-jump">
            <img src={VideoButton} alt="" />
          </Link>{" "}
          <Link to="/about-us/testimonials/#testimonial-written-jump">
            <img src={WrittenButton} alt="" />
          </Link>{" "}
          <Link to="/about-us/testimonials/#testimonials-social-jump">
            <img src={ReviewsButton} alt="" />
          </Link>
        </div>
        <div id="testimonial-videos-jump"></div>
        <div id="testimonial-videos">
          <h2>Video Testimonials</h2>
          <div className="video-grid">
            <div id="top-video" className="video-section">
              <div className="content-wrapper">
                <LazyLoad offset={200} height={300}>
                  <iframe
                    src="https://www.youtube.com/embed/o12IjKExmYk"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                  />
                </LazyLoad>
                <h3>
                  $11 Million Dollar <br />
                  Facility Negligence Verdict
                </h3>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />
                <h4>
                  I'm going to tell everybody about Bisnar Chase. You have to
                  meet with Gavin Long, he is the guy. Gavin was a Godsend for
                  me.
                </h4>
              </div>

              <h5>
                - Cindy Cunningham, former client
                <span>
                  <img className="ten-star" src={TenStar} alt="" />
                </span>
              </h5>
            </div>
            <div className="video-section">
              <div className="content-wrapper">
                <LazyLoad offset={200} height={300}>
                  <iframe
                    src="https://www.youtube.com/embed/lVUUCtM3Ebg"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                  />
                </LazyLoad>
                <h3>
                  California Personal <br />
                  Injury Attorney
                </h3>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />
                <h4>
                  John Bisnar is like family, he really took care of us. I'm so
                  happy that we found Bisnar Chase. They've been outstanding.
                  They kept us informed with what's going on with the case.
                </h4>
              </div>

              <h5>
                - Maria Chan, former client
                <span>
                  <img className="ten-star" src={TenStar} alt="" />
                </span>
              </h5>
            </div>{" "}
            <div className="video-section">
              <div className="content-wrapper">
                <LazyLoad offset={200} height={300}>
                  <iframe
                    src="https://www.youtube.com/embed/K1JggkiZOdg"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                  />
                </LazyLoad>
                <h3>
                  Orange County Personal <br />
                  Injury Lawyer Video
                </h3>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />
                <h4>
                  My first impression of Bisnar Chase is I really felt that they
                  cared about me. They made me feel special. I was confident in
                  them. I knew that they would help me.
                </h4>
              </div>

              <h5>
                - Gloria Levesque, former client{" "}
                <span>
                  <img className="ten-star" src={TenStar} alt="" />
                </span>
              </h5>
            </div>{" "}
            <div className="video-section">
              <div className="content-wrapper">
                <LazyLoad offset={200} height={300}>
                  <iframe
                    src="https://www.youtube.com/embed/9-UJdHzYPw4"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                  />
                </LazyLoad>
                <h3>
                  California Personal <br />
                  Injury Lawyer Video
                </h3>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />
                <h4>
                  I'm going to tell everybody about Bisnar Chase. You have to
                  meet with Gavin Long, he is the guy. Gavin was a Godsend for
                  me.
                </h4>
              </div>

              <h5>
                - Gloria Levesque, former client{" "}
                <span>
                  <img className="ten-star" src={TenStar} alt="" />
                </span>
              </h5>
            </div>{" "}
            <div className="video-section">
              <div className="content-wrapper">
                <LazyLoad offset={200} height={300}>
                  <iframe
                    src="https://www.youtube.com/embed/oCeN5TviMcY"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen={true}
                  />
                </LazyLoad>
                <h3>
                  Orange County Car <br />
                  Accident Lawyer Video
                </h3>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />
                <h4>
                  I'm going to tell everybody about Bisnar Chase. You have to
                  meet with Gavin Long, he is the guy. Gavin was a Godsend for
                  me.
                </h4>
              </div>

              <h5>
                - Cindy Cunningham, former client{" "}
                <span>
                  <img className="ten-star" src={TenStar} alt="" />
                </span>
              </h5>
            </div>
          </div>
        </div>
        <div id="testimonial-written-jump"></div>
        <div id="testimonial-written">
          <h2>Written Testimonials</h2>
          <div id="written-grid">
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    Bisnar Chase provided outstanding service in negotiating
                    with the other person's insurance company. They worked hard
                    to get me the best settlement and the turn around time was
                    fast. On a scale of 1 to 10 I'd have to give the staff and
                    attorneys of Bisnar Chase a 10.
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - J. McMillan, former client
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    I knew I made the right choice with Bisnar Chase because
                    they went above and beyond to help. I especially want to
                    thank Colleen and Chris for meeting all my needs and walking
                    me through every piece of information. You have excellent
                    employees in this firm! I was treated like family and the
                    service and professionalism was above and beyond.
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - M. Cheng, former client
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>{" "}
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    I was originally referred to Bisnar Chase by my uncle and
                    have used them for two cases I was involved in. The staff
                    was courteous and answered my calls and emails in a timely
                    manner. They really worked hard for a higher payout in my
                    case and kept me informed along the way. I even received an
                    extra check by first class mail that I didn't expect.
                    Exceptional Service!
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - E. Shaffer, former client
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>{" "}
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    I retired last January after 20+ years of doing house
                    counsel defense (first for Farmers, then Mercury Insurance).
                    Always enjoyed dealing with your firm. You were aggressive,
                    knowledgeable, had good cases; I knew I was in for a fight
                    when I'd first be assigned a case and saw that Bisnar Chase
                    was to be my opponent. But I also knew that I'd be dealing
                    with professionals and never had to worry about watching my
                    back!
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - Michael Hargis, former insurance house defense
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>{" "}
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    I [want to] share how grateful we at CTH are for your
                    generous donation of over one hundred and fifty new
                    backpacks. Also, thank you for providing the backpacks with
                    toiletries and a wash cloth. Backpacks are one of our most
                    sought after items by people who live on the streets.
                    Acquiring new or even used backpacks can be a challenge for
                    us and your donation is very appreciated by CTH and the
                    homeless. With Bisnar/Chase’s generous donation CTH will be
                    able to reach out and support those who need it the most.
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - Mitchell Raff, Director of “Clothing the Homeless” (CTH) of
                  Orange County
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>{" "}
            <div className="quote-container">
              <div>
                <FaQuoteLeft className="quote-left" />
                <FaQuoteRight className="quote-right" />

                <div className="max-text">
                  <h4>
                    Everything they said they would do, was done! The worst part
                    of my experience with Bisnar Chase was when the case was
                    finalized and I felt like I'd lose the friendship I
                    established with the firm. I always appreciated the kindness
                    I was shown by the staff when I was in the office. My
                    overall feeling of Bisnar Chase was that I was represented
                    by the best law firm in California.
                  </h4>
                </div>
              </div>

              <div className="max-text">
                <h5>
                  - L. Jones, former client
                  <span>
                    <img className="ten-star" src={TenStar} alt="" />
                  </span>
                </h5>
              </div>
            </div>
          </div>
        </div>
        <div id="testimonials-social-jump"></div>
        <div id="testimonials-social">
          <h2>Social Reviews</h2>
          <p className="list-title">CLIENT REVIEWS OF BISNAR CHASE</p>
          <div className="social-grid">
            {ReviewsData.slice(0, 20).map((review, index) => {
              return (
                <div key={index} className="social-review">
                  <FaQuoteLeft className="quote-left" />
                  <FaQuoteRight className="quote-right" />
                  <div className="max-text">
                    <div
                      className="content-body"
                      dangerouslySetInnerHTML={{
                        __html: review.fullReview
                      }}
                    />
                    <div className="social-person">
                      <LazyLoad>
                        <img
                          src={review.thumbnailPath}
                          alt={review.reviewer + " review"}
                        />
                      </LazyLoad>
                      <div className="social-stars">
                        <p>{"by " + review.reviewer}</p>
                        <p className="reviewed-at">
                          REVIEWED AT{" "}
                          <Link
                            target="_blank"
                            to={socialSwitch(review.reviewedAt)}
                          >
                            {review.reviewedAt}
                          </Link>
                        </p>
                        <p>
                          {[1, 2, 3, 4, 5].map(star => (
                            <IoMdStar key={star} className="star" />
                          ))}{" "}
                          <span>(5/5)</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  #testimonials-social {
    .content-body {
      font-size: 1.2rem;
      margin-bottom: 2rem;
    }

    .reviewed-at {
      font-size: 1rem;
    }

    .social-grid {
      display: grid;
      grid-template-columns: 1fr 1fr;
      margin-bottom: 2rem;

      @media (max-width: 900px) {
        grid-template-columns: 1fr;
      }
    }

    .star {
      font-size: 2rem;
      color: ${({ theme }) => theme.colors.accent};
    }

    h4 {
      margin-bottom: 2rem;
    }

    img {
      margin-bottom: 0;
      width: 80px;
    }

    .social-stars {
      p {
        margin-bottom: 0;
      }

      span {
        position: relative;
        bottom: 5.5px;
      }
    }

    .social-person {
      display: grid;
      grid-template-columns: 100px 1fr;
    }

    .quote-left {
      float: left;
      top: 0;
      right: 15px;
    }

    .quote-right {
      float: right;
      top: 0;
      left: 15px;
    }

    .max-text {
      width: 90%;
      margin: 0 auto;
      text-align: justify;
    }

    .list-title {
      background-color: ${({ theme }) => theme.colors.secondary};
      color: ${({ theme }) => theme.colors.primary};
      padding: 1rem;
      font-weight: 600;
    }

    .social-review {
      width: 95%;
      margin: 1rem auto;
      padding: 1rem 2rem;
      border: medium solid grey;
      background-color: #e8e8e8;
    }
  }

  img.ten-star {
    margin-bottom: 0;
    position: relative;
    top: 5px;
    left: 6px;
  }

  .quote-left,
  .quote-right {
    color: ${({ theme }) => theme.colors.accent};
    font-size: 1.5rem;
    position: relative;
    bottom: 1.6rem;
  }

  .quote-left {
    float: left;
  }

  .quote-right {
    float: right;
  }

  h2 {
    margin-bottom: 3rem;
    margin-top: 4rem;
    text-align: center;
    font-size: 2.7rem;
  }
  h3 {
    font-size: 2rem;
    line-height: 2rem;
    text-align: center;
    margin-bottom: 2rem;
  }

  h4 {
    font-style: italic;
    margin-bottom: 0.5rem;
  }

  h5 {
    font-size: 1.2rem;
  }

  #testimonial-written {
    .quote-left {
      float: left;
      top: 0;
    }

    .quote-right {
      float: right;
      top: 0;
    }
    #written-grid {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      grid-template-rows: auto auto;
      grid-gap: 2rem;

      @media (max-width: 900px) {
        grid-template-columns: 1fr;
      }

      div.quote-container {
        display: flex;
        flex-direction: column;
      }

      .max-text {
        width: 80%;
        margin: 0 auto;
        text-align: justify;
      }
    }
  }

  #testimonial-videos {
    .video-grid {
      display: grid;
      grid-template-columns: 1fr 1fr;
      justify-items: center;
      grid-auto-flow: row;
      grid-gap: 2rem;

      @media (max-width: 900px) {
        grid-template-columns: 1fr;
      }

      .video-section {
        display: flex;
        flex-direction: column;
        align-items: center;

        .content-wrapper {
          width: 100%;
          text-align: center;
          background-color: #eee;
          padding: 2rem;

          #top-video {
            iframe {
            }
          }

          iframe {
            width: 100%;
            height: 300px;
          }
        }
      }

      #top-video {
        grid-column: 1 / span 2;
        width: 70%;
        text-align: center;

        @media (max-width: 900px) {
          grid-column: initial;
          width: 100%;
        }

        iframe {
          width: 100%;
          height: 500px;

          @media (max-width: 900px) {
            height: 300px;
          }
        }
      }
    }
  }

  #testimonial-buttons {
    display: flex;
    justify-content: space-between;
    margin-top: 8rem;

    @media (max-width: 650px) {
      flex-direction: column;
      margin-top: 4rem;
    }

    a {
      margin: 0 1rem;
    }

    img:hover {
      transform: scale(1.03);
      transition: all 0.1s ease-in;
    }
  }

  .text-container {
    left: 0;
    right: 0;
    position: absolute;
    top: 388px;

    .first-paragraph {
      /* margin-top: 1rem; */
      @media (max-width: 1024px) {
        margin-top: 1rem;
      }
    }

    .breadcrumb-wrapper {
      margin-bottom: 1rem;
    }

    @media (max-width: 1024px) {
      top: 100px;
      position: initial;
    }
  }

  #testimonials-intro {
    top: 0;
    width: 578px;
    margin: 0 auto;
    position: relative;
    right: 278px;

    @media (max-width: 1024px) {
      right: 0;
      width: 100%;
    }
  }

  .breadcrumb-title {
    h1 {
      margin-top: 1rem;

      @media (max-width: 1024px) {
        margin-top: 3rem;
        /* margin-bottom: 4rem; */
      }
    }
    @media (max-width: 1024px) {
      position: absolute;
      top: -125px;
      width: 100%;
      /* margin-bottom: 4rem; */
    }

    @media (max-width: 414px) {
      top: -115px;
    }
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
