// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
// import Img from "gatsby-image"
// import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

// export const query = graphql`
//   query {
//     headerImage: file(
//       relativePath: {
//         eq: "text-header-images/truck-accident-spanish-banner.jpg"
//       }
//     ) {
//       childImageSharp {
//         fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
//           ...GatsbyImageSharpFluid
//         }
//       }
//     }
//   }
// `

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Abogados de danos causados por Caminos Peligrosos"
        pageDescription="Abogados de da�os causados por Caminos Peligrosos - Llame para ver si usted tiene un caso si usted fuera la parte lesionada. 1-800-561-4887."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados de danos causados por Caminos Peligrosos</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            Los abogados Californianos de Da&ntilde;os Personales de la firma
            legal Bisnar & Chase son experimentados y exitosos. Ellos
            representan a v&iacute;ctimas de da&ntilde;os severos y muerte por
            accidente y uso de productos inseguros, incluyendo Accidentes por
            Caminos Peligrosos. La firma se asienta en el condado de Orange pero
            representa a clientes en todo el sur de California. Si usted o un
            ser querido ha resultado da&ntilde;ado en un accidente por camino
            peligroso o en otro accidente serio, cont&aacute;ctese hoy con
            nosotros para realizar su consulta sin costo alguno.
          </p>
          <p>
            La construcci&oacute;n de caminos y autopistas suceden muy
            frecuentemente en California, con la expansi&oacute;n de autopistas
            y la introducci&oacute;n de nuevos trenes. Muchos peligros
            inesperados como desv&iacute;os o restos de construcci&oacute;n
            hacen peligroso y dif&iacute;cil el manejar. Las se&ntilde;ales
            indicadoras de cambios debieran estar perfectamente visibles a los
            conductores desde una distancia considerable para que &eacute;stos
            puedan prepararse ajustando la velocidad o deteniendo el
            autom&oacute;vil. Las luces de la autopista deben facilitar al
            conductor ver el camino y los otros veh&iacute;culos, as&iacute;
            como tambi&eacute;n las barandas para proteger a las personas en
            caso de accidente.
          </p>
          <p>
            Nuestra firma est&aacute; comprometida a la protecci&oacute;n de los
            derechos de nuestros clientes que han sufrido Da&ntilde;os
            Personales y con recobrar el m&aacute;ximo capital monetario
            posible. Hemos recuperado decenas de millones de d&oacute;lares para
            nuestros clientes afectados por Da&ntilde;os Personales.
          </p>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
