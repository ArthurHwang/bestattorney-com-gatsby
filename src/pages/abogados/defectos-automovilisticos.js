// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "auto-defects/Auto Defects Spanish Tiny Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Defectos automovilísticos en el condado de Orange - Bisnar Chase"
        pageDescription="Si ha resultado lesionado en un accidente automovilístico y piensa que fue debido a un vehículo defectuoso, llámenos hoy mismo al (800) 561-4846"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados especialistas en defectos automovilísticos</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Abogados especialistas en defectos automovilísticos"
            title="Abogados especialistas en defectos automovilísticos"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong>
              {" "}
              Los abogados de defectos de automóviles de California
            </strong>{" "}
            en <strong> Bisnar Chase</strong> han ganado casos por más de{" "}
            <strong> 40 años. Con</strong> un <strong> 96 por ciento</strong> de
            casos exitosos y más de{" "}
            <strong> 300 millones de dólares ganados</strong>, contamos con una
            larga lista de clientes satisfechos a quienes hemos ayudado a
            recuperarse.
          </p>
          <p>
            No todos los abogados de lesiones personales se especializan o
            tienen una amplia experiencia en casos de defectos de automóviles,
            especialmente aquellos causados por defectos graves, como la
            aceleración repentina involuntaria y la falla en los asientos.
          </p>
          <p>
            Estos defectos automotrices pueden causar lesiones graves y
            catastróficas que cambian a una persona o familia para siempre.
            Nuestros{" "}
            <Link to="/abogados/lesiones-personales" target="_blank">
              abogados de lesiones personales
            </Link>{" "}
            de California han aceptado casos importantes y han ganado.
          </p>
          <p>
            Estar en un accidente puede ser estresante y no deberías enfrentarlo
            solo. Si usted sufrió una lesión grave, contáctenos hoy para una
            revisión gratuita y averiguar si tiene un caso. El asesoramiento no
            le cuesta nada y el resultado podría ser invaluable. Para una{" "}
            <strong> consulta gratuita</strong> y la{" "}
            <strong> evaluación de su caso</strong>,<strong>llame al</strong>{" "}
            <strong> 877-958-8092</strong>.
          </p>
        </div>
        <h2>
          Un equipo legal fuerte para luchar contra los defectos de automóviles{" "}
          <strong> </strong>
        </h2>
        <p>
          Es importante recordar que los fabricantes de automóviles son grandes
          corporaciones que tienen equipos de defensa legal de alto poder a su
          disposición. Cualquiera que intente luchar contra estas corporaciones
          no puede y no debe hacerlo solo.
        </p>
        <p>
          Si usted o un ser querido ha sido lesionado por una pieza de automóvil
          defectuosa o un defecto del vehículo, necesita un abogado experto en
          defectos de automóviles de California a su lado que luchará por sus
          derechos en cada paso del camino.
        </p>
        <p>
          Abogados especialistas en accidentes que tenga la motivación y los
          recursos para realizar investigaciones y pruebas independientes
          aumentaran sus posibilidades de ganar su caso. Necesita un equipo
          sólido de abogados especializados en defectos de automóviles que
          tengan la misión de responsabilizar a los infractores y hacer del
          mundo un lugar más seguro para los consumidores.
        </p>
        <LazyLoad>
          <img
            src="/images/auto-defects/Auto Reform- Auto Defects-Spanish-Final Image.jpg"
            width="100%"
            alt="Abogados de lesiones personales en California"
          />
        </LazyLoad>
        <h2>Defectos automotrices peligrosos necesitan reforma</h2>
        <p>
          Los fabricantes de automóviles retiraron más de 250 millones de
          automóviles en las últimas dos décadas por defectos potencialmente
          fatales que podrían haber causado lesiones graves o la muerte en
          cualquier momento.
        </p>
        <p>
          Cualquier persona que haya resultado herida debido a un defecto en el
          automóvil como este merece ser compensado por el fabricante del
          automóvil que permitió que el defecto que causó sus lesiones
          permaneciera en sus automóviles.
        </p>
        <p>
          El 2015 fue un año destacado por los retiros. Las bolsas de aire
          Takata tuvieron el mayor retiro de la historia. Los fabricantes de
          automóviles retiraron tantos vehículos como los que vendieron en el
          año 2012 y 2015. De acuerdo con la{" "}
          <Link to="https://es.wikipedia.org/wiki/NHTSA" target="_blank">
            {" "}
            Administración Nacional de Seguridad en el Tráfico en las Carreteras
            de Estados Unidos
          </Link>{" "}
          (NHTSA) y otros informes de los medios:
        </p>
        <ul>
          <li>
            La industria automotriz vendió alrededor de 14.5 millones de
            vehículos en 2012, lo que representó un aumento del 13 por ciento
            con respecto al año anterior.
          </li>
          <li>
            También retiraron más de 14.3 millones de modelos de vehículos
            actuales y pasados durante el mismo año.
          </li>
        </ul>
        <p>
          Esta es una estadística asombrosa porque resalta la cantidad de
          vehículos en nuestras carreteras que están defectuosos o tienen partes
          defectuosas. Hay millones de vehículos más que no han sido retirados a
          pesar de tener piezas defectuosas.
        </p>
        <p>
          A medida que los vehículos se retiran en grandes cantidades debido a
          diversos defectos de fabricación y fallas de diseño, es muy importante
          que los consumidores estén al tanto de la seguridad del vehículo y de
          los retiros de autos defectuosos. Dicho esto, los fabricantes de
          automóviles tienen el deber de cuidar a los consumidores para fabricar
          vehículos seguros y confiables.
        </p>
        <p>
          Desafortunadamente, hay evidencias que durante décadas los fabricantes
          de automóviles, a veces deliberadamente, eligen ignorar ese deber y
          arriesgan imprudentemente las vidas de millones de personas que
          compran sus vehículos creando lucrativas corporaciones.
        </p>
        <h2>Nueve Ejemplos de piezas de automóviles defectuosas</h2>
        <p>
          Las lesiones devastadoras pueden ocurrir cuando una pieza de automóvil
          no funciona correctamente y crea un mal funcionamiento del vehículo.
          Los fabricantes de automóviles están obligados a someter sus vehículos
          a pruebas rigurosas antes de ponerlos en el mercado.
        </p>
        <p>
          Pero, a menudo, los fabricantes de automóviles ponen las ganancias
          antes que la seguridad del consumidor y deciden instalar piezas
          deficientes en el vehículo.
        </p>
        <p>
          La calidad de las piezas de automóviles y las características de
          seguridad en un vehículo afectan significativamente a su resistencia a
          choques, que es la capacidad de un vehículo para proteger a sus
          ocupantes en caso de un choque.
        </p>
        <p>
          <strong>
            {" "}
            Algunos de los defectos más comunes del vehículo que causan
            accidentes automovilísticos y retiros de estos incluyen:
          </strong>
        </p>
        <ol>
          <li>
            <strong> Llantas: </strong>Uno de los{" "}
            <Link
              to="https://www.pruebaderuta.com/danos-frecuentes-en-las-llantas.php"
              target="_blank"
            >
              daños mas frecuentes en las llantas
            </Link>{" "}
            son los cortes en la llanta. Para esto, las llantas pueden explotar.
            Cuando un neumático defectuoso sufre una separación de la banda de
            rodadura a velocidades de autopista, el vehículo puede girar fuera
            de control o volcarse. En tales casos, puede ser muy difícil incluso
            para un conductor experimentado recuperar el control del vehículo.
          </li>

          <li>
            <strong> Cinturones de seguridad:</strong> cuando un pasajero es
            expulsado de un vehículo, se debe determinar si él o ella llevaba un
            cinturón de seguridad que no funcionó correctamente.
          </li>
          <li>
            <strong> Asientos para el automóvil:</strong> cuando un asiento para
            bebé o un asiento elevador para un infante está defectuoso existe la
            posibilidad de lesiones graves a los niños pequeños.
          </li>

          <li>
            <strong> Asientos traseros:</strong> un respaldo endeble o
            defectuoso puede colapsar y provocar que el ocupante del vehículo
            sea tirado del vehículo. Esto a menudo resulta en lesiones
            catastróficas.
          </li>
          <li>
            <strong> Frenos:</strong> es extremadamente peligroso operar un
            vehículo que no tiene frenos que funcionen correctamente.
          </li>
          <li>
            <strong> Pedales del acelerador:</strong> Últimamente se han
            realizado varios retiros que implican pedales del acelerador que se
            pegan y permanecen presionados, lo que causa una aceleración
            repentina o una aceleración involuntaria.
          </li>
          <li>
            <strong> Bolsas de aire:</strong> una bolsa de aire defectuosa puede
            no proteger al conductor durante un accidente o inflarse
            repentinamente y causar lesiones graves a los ocupantes del
            vehículo.
          </li>
          <li>
            <strong> Volcaduras de vehículos:</strong> el diseño defectuoso del
            vehículo podría aumentar la propensión de volcaduras de vehículos.
            Por ejemplo, las camionetas de 15 pasajeros y las SUV de modelos
            anteriores son particularmente propensas a volcarse debido a su
            diseño pesado.
          </li>
          <li>
            <strong> Techos aplastados:</strong> los vehículos que se vuelcan
            deben tener techos fuertes para resistir el peso del automóvil, de
            modo que los pasajeros en su interior se mantengan seguros. Cuando
            los techos fallan puede ocasionar lesiones graves o la muerte.
          </li>
        </ol>
        <p>
          Es importante que todos los automovilistas de California estén al
          tanto de los retiros y las investigaciones. Cuando han habido una
          serie de quejas similares sobre un vehículo o pieza específica, el
          fabricante tal vez debe realizar una investigación.
        </p>
        <p>
          En algunos casos, la Administración Nacional de Seguridad del Tráfico
          en Carreteras (NHTSA) intervendrá para realizar una investigación
          independiente y/o un análisis de ingeniería. Si la investigación
          determina que la pieza de automóvil es defectuosa, se iniciará un
          retiro.
        </p>
        <LazyLoad>
          <img
            src="/images/auto-defects/Auto Defect--Spanish center image-tiny jpg.jpg"
            width="100%"
            alt="Abogados especialistas en defectos automovilísticos"
          />
        </LazyLoad>
        <h2>¿Qué hace que un auto sea defectuoso?</h2>
        <p>
          La pregunta mas importante que teine las personas es"¿qué hago si mi{" "}
          <Link
            to="https://www.autocasion.com/actualidad/reportajes/que-hacer-si-tu-coche-tiene-un-defecto-de-fabrica"
            target="_blank"
          >
            coche tiene un defecto de fábrica
          </Link>
          ?" Un producto defectuoso no puede ejecutar la tarea para la que fue
          diseñado. Hay muchas razones por las que una pieza de un automóvil
          puede resultar defectuosa.
        </p>
        <p>
          Algunas piezas tienen un diseño defectuoso que las hace propensas a
          romperse o funcionar mal. Las piezas que están mal diseñadas pueden
          fallar incluso cuando están ensambladas correctamente.
        </p>
        <p>
          En otros casos, las piezas de automóviles están mal construidas. Los
          defectos de fabricación generalmente implican un número limitado de
          partes que se construyeron en una planta específica durante un período
          corto de tiempo cuando las especificaciones requeridas no fueron
          seguidas.
        </p>
        <h2>Demostrar que una pieza esta defectuosa</h2>
        <p>
          Es absolutamente vital para su caso que su vehículo se conserve para
          una inspección minuciosa y completa. Resista la tentación de reparar o
          deshacerse de su automóvil.
        </p>
        <p>
          De hecho, el vehículo es la prueba más importante en un caso de
          defecto automotriz. Un experto podrá analizar minuciosamente el
          vehículo en busca de defectos, mal funcionamiento y otras pruebas
          cuando se conserve en su estado original sin modificaciones.
        </p>
        <p>
          <strong>
            {" "}
            Para tener un reclamo exitoso de defecto de auto contra un
            fabricante de automóviles, deberá probar que
          </strong>
          :
        </p>
        <p>
          • La pieza del automóvil estaba defectuosa de alguna manera • Que el
          producto defectuoso resultó en el accidente o lesiones • Usted sufrió
          una pérdida física, financiera o emocional como resultado del defecto
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Letrado de lesiones personales"
            style={{
              backgroundImage:
                "url('/images/images/california class action lawsuit text header.jpg')"
            }}
          >
            <h2>Busque al abogado correcto de defectos automotrices </h2>
          </div>
        </LazyLoad>
        <p>
          Con más de <strong> 40 años de experiencia</strong> hemos recaudado
          más de <strong> 300 millones de dólares</strong>, hemos establecido
          una <strong> tasa de éxito del 96 por ciento</strong> y contamos con
          los recursos para atender casos muy difíciles.
        </p>
        <p>
          El socio principal, Brian Chase, se especializa en casos de defectos
          de automóviles en California, al igual que su equipo de abogados
          litigantes. Podemos perseguir su caso con experiencia y pasión.{" "}
          <strong>
            {" "}
            Con más de 10.000 clientes atendidos y cientos de millones de
            victorias
          </strong>
          , Bisnar Chase está listo para luchar por usted.
        </p>
        <p>
          Hemos ayudado a muchas victimas de accidentes muy especiales con casos
          graves de defectos de automóviles.
        </p>
        <p>
          Si necesita un abogado para llevar su caso a otro nivel, considere a{" "}
          <strong> Bisnar Chase</strong>.
        </p>
        <p>Vea todos los resultados de casos.</p>
        <p>
          {" "}
          <Link to="/abogados/contactenos" target="_blank">
            Contáctenos
          </Link>{" "}
          hoy para una <strong> consulta gratuita</strong> y una{" "}
          <strong> evaluación de su caso</strong> de defecto automotriz de
          California.
        </p>
        <p>
          No debería tener que pagar debido a la negligencia de otra persona.
        </p>
        <p>
          Llame al <strong> 1-877-958-8092</strong>.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
