// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
// import Img from "gatsby-image"
// import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Abogados especialistas en accidentes de autobus"
        pageDescription="Herido en un accidente de auto? Accidentes de auto abogados ofrecen una consulta gratis. Llame al Herido en un accidente de autobus? Contacte a los abogados especialistas en accidentes de autobus para una consulta gratuita. Si no ganamos, no pagas! 561-4846."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogados especialistas en accidentes de autobus</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            Las v&iacute;ctimas de accidente de autob&uacute;s tienen derechos
            sustanciales bajo las leyes de California si como pasajero de
            autob&uacute;s se ve envuelto en un accidente, si es envestido por
            un autob&uacute;s como peat&oacute;n o envestido en alg&uacute;n
            otro veh&iacute;culo por un autob&uacute;s.
          </p>
          <p>
            <img
              src="/images/bus-accident-victim.jpg"
              alt="Abogados especialistas en accidentes de autobus"
              className="imgright-fixed"
            />
            Sus derechos incluyen el derecho a compensaci&oacute;n por gastos
            m&eacute;dicos, p&eacute;rdida de ingresos o ganancias, da&ntilde;os
            a la propiedad, dolor y sufrimiento, gastos m&eacute;dicos futuros,
            p&eacute;rdida de futuros beneficios de vida, cicatriz, incapacidad
            y gastos de su propio bolsillo. Las leyes de California
            prev&eacute;n tambi&eacute;n l&iacute;mites muy estrictos dentro de
            los cuales las v&iacute;ctimas de accidentes de autob&uacute;s deben
            accionar en orden de preservar sus derechos.
          </p>
          <p>
            Los accidentes de autob&uacute;s que envuelven un autob&uacute;s
            perteneciente u operado por una agencia del gobierno presenta
            desaf&iacute;os adicionales en la b&uacute;squeda de lograr
            compensaciones por da&ntilde;os y perdida. Como en cualquier reclamo
            hecho California, un reclamo formal (no un litigio judicial) debe
            ser correctamente archivado dentro de los seis meses del accidente
            si quien opera el autob&uacute;s es una agencia gubernamental. La
            cl&aacute;usula de limitaci&oacute;n (tiempo dentro del cual la
            causa judicial debe ser archivada luego de producirse el
            da&ntilde;o) es muy diferente cuando el reclamo se realiza contra
            una agencia gubernamental.
          </p>
        </div>
        <h3>Consejos para Elegir un Abogado</h3>
        <p>
          La firma Legal Bisnar & Chase representa en forma exclusiva a
          v&iacute;ctimas seriamente da&ntilde;adas que tienen derecho legal y
          moral a recuperar los perjuicios econ&oacute;micos sufridos. Los
          socios, abogado John Bisnar y abogado Brian Chase est&aacute;n
          comprometidos a proporcionar &#147; Excelencia Profesional &#148; a
          cada uno de sus clientes que hayan sufrido accidentes de
          autob&uacute;s. Esto significa brindar una representaci&oacute;n legal
          de excelencia , gu&iacute;a personal y RESULTADOS.
        </p>
        <p>
          Los abogados Bisnar & Chase han recuperado muchos millones de
          d&oacute;lares de agencias gubernamentales y compa&ntilde;&iacute;as
          de autobuses para sus clientes. En el proceso de buscar
          compensaci&oacute;n para sus clientes, los abogados Bisnar & Chase han
          seguido casos a trav&eacute;s de las Cortes de Apelaci&oacute;n y la
          Corte Suprema de California donde sus definiciones han sido adoptadas
          por las cortes de California como ley que rige en todo el estado.
        </p>
        <p>
          <strong> CONSULTA SIN CARGO</strong>- para v&iacute;ctimas de
          accidente de autob&uacute;s. V&iacute;ctimas seriamente perjudicadas
          por accidentes de autob&uacute;s tendr&aacute;s una reuni&oacute;n con
          el socio Senior, abogado John Bisnar, de estar disponible.
        </p>
        <p>
          <strong> SIN COBRO DE ARANCELES</strong> hasta que ganemos. Nuestros
          aranceles se basan en el porcentaje de lo recuperado en beneficio de
          nuestros clientes. Los mismos van desde un 25% a un 45% de lo
          recuperado.
        </p>
        <p>
          <strong> COSTOS POR ADELANTADO</strong>- nosotros adelantaremos todos
          los costos que sean razonablemente necesarios para proseguir con los
          casos de nuestros clientes con serios accidentes de auto en tanto
          cumplan nuestro asesoramiento.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
