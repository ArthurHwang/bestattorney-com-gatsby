// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import Helmet from "react-helmet"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ThankYouPageEspanol({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Gracias - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Our personal injury lawyers will be in touch with you to discuss your case. For immediate help call 949-203-3814."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Mensaje enviado - gracias</h1>
        <BreadCrumbs location={location} />
        <h2 align="center">Gracias - Tu mensaje ha sido enviado.</h2>
        <p>
          Sabemos que su tiempo es importante y nos pondremos en contacto con
          usted para discutir su caso tan pronto como sea posible. Normalmente
          el plazo de un día hábil. Si necesita ayuda inmediata y quiere hablar
          con un experto en la ingesta de ahora, llame 949-203-3814. El horario
          de atención es de 8:30 am a 17:00 M-F. Nosotros evaluamos todas las
          reclamaciones presentadas en los fines de semana.
        </p>

        <iframe
          width="100%"
          height="500"
          src="https://www.youtube.com/embed/P4cWzcUwGxQ?rel=0"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
        />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
