// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "medical-device-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Defectos en dispositivos médicos - Bisnar Chase Abogados"
        pageDescription="Los abogados de dispositivos médicos deficientes de Bisnar Chase han mantenido el 95% de éxito y ganado millones para las víctimas que han experimentado daños graves. Al llamar recibirá una consulta gratis con un abogado de lesiones. Llame al 877-958-8092."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Abogado Especialista En Dispositivos Médicos Defectuosos</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="defectos dispositivos"
            title="defectos dispositivos"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Los <strong> abogados especialistas en dispositivos médicos</strong>{" "}
          <strong> defectuosos</strong> de
          <strong>
            {" "}
            <Link to="/abogados" target="_blank">
              {" "}
              Bisnar Chase
            </Link>
          </strong>{" "}
          creen que los fabricantes de dispositivos médicos tienen la obligación
          de informar a los pacientes sobre los posibles riegos de un producto
          médico, ya sea una prótesis de cadera o rodilla, malla vaginal o
          eslinga de vejiga. El fabricante de dispositivos médicos tiene que
          advertir de los posibles efectos secundarios o problemas.
        </p>

        <p>
          Cuando este tipo de dispositivos médicos salen a la venta con mínima
          investigación, los efectos secundarios pueden producirse tiempo
          después y en algunos casos son graves.{" "}
        </p>
        <p>
          Si usted ha sufrido una lesión por un dispositivo médico defectuoso,
          comuníquese con los abogados de Bisnar Chase para revisar su caso.
          Hemos representado a residentes de California por más de
          <strong> 40 años</strong> y ganado más de{" "}
          <strong> 300 millones de dólares en fallos y acuerdos.</strong>
        </p>
        <p>
          Nuestros abogados especialistas en dispositivos médicos defectuosos
          tienen la experiencia y los medios para ganar los casos más difíciles.
          Si cree que tiene un caso de prótesis de cadera defectuosa, rodilla o
          malla vaginal, comuníquese con nuestro bufete de abogados para saber
          cómo le podemos ayudar.
        </p>
        <p>
          <strong> Llame al 877-958-8092</strong> para una{" "}
          <strong> consulta gratis.</strong>
        </p>
        <h2>Tipos Comunes De Dispositivos Médicos Defectuosos</h2>
        <p>
          El diseño o medicamentos deficientes son los factores causantes de los
          defectos en los dispositivos médicos. La administración de
          alimentación y farmacéuticos (FDA) retira los dispositivos o
          medicamentos que no cumplen con los estándares establecidos. Si el
          producto no cumple con los requisitos puede causar lesiones graves e
          incluso la muerte.
        </p>
        <p>
          <strong> Prótesis de cadera defectuosas</strong>: En un año se han
          llevado a cabo un total de 310.800 implantes de cadera. Una de las
          razones principales porque las personas necesitan un implante de este
          tipo es por el deterioro de las coyunturas, causados por la
          osteoartritis, artritis reumatoide o necrosis avascular. Miles de
          personas han ganado exitosamente compensaciones por demandas
          relacionadas a remplazos de cadera defectuosas. A lo largo de los años
          empresas de remplazos de cadera han pagado alrededor de 7.5 mil
          millones de dólares en acuerdos.
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="https://www.mayoclinic.org/es-es/diseases-conditions/pelvic-organ-prolapse/in-depth/transvaginal-mesh-complications/art-20110300"
              target="_blank"
            >
              Complicaciones de la malla transvaginal
            </Link>
            :{" "}
          </strong>
          Las pacientes de malla transvaginal buscan este tipo de tratamiento
          cuando experimentan dolor, incomodidad o goteo de orina de la vagina.
          La fuente de esta irritación se origina de un prolapso de la vejiga.
          Con el paso del tiempo, por consecuencia de partos o de la edad, el
          músculo que levanta la uretra se debilita y causa que cuelgue.
          Desafortunadamente no se han hecho muchos estudios en el material que
          se usa para fabricar la malla y muchas mujeres han comenzado a
          experimentar dolor y sangrados.
        </p>
        <LazyLoad>
          <img
            src="/images/medical/trans-vaginal-mesh-image-.jpg"
            width="100%"
            alt="Abogado Especialista En Dispositivos Médicos Defectuosos"
          />
        </LazyLoad>
        <p>
          <strong> Retiro de anticonceptivos</strong>: Marcas como
          &ldquo;Glenmark&rdquo; fueron responsabilizadas por mal empaquetado.
          El empaquetado de algunas marcas alteró la posición de algunas
          pastillas, invirtiéndolas para que el usuario tomara primero la
          &ldquo;pastilla de placebo&rdquo; en vez de las pastillas que fueron
          destinadas para tomar al comienzo del mes. De igual manera el
          empaquetado de las pastillas dificultó localizar la fecha de
          caducidad. Las víctimas afectadas por este error solicitaron millones
          de dólares ya que experimentaron embarazos no planificados y por daños
          y perjuicios.
        </p>
        <p>
          <strong> Filtro de la vena cava inferior (IVC)</strong>: El propósito
          del filtro IVC es prevenir coágulos en los pulmones en individuos que
          fueron víctimas de accidentes. El diseño del filtro IVC se asemeja a
          una &ldquo;reja de metal&rdquo; y es colocado en la vena principal del
          cuerpo (vena cava inferior) la cual está conectada al corazón. El
          dispositivo fue aprobado por la administración de alimentos y fármacos
          (FDA) pero tiempo después debido a su fabricación deficiente,
          pacientes que tenían el filtro introducido experimentaron coágulos y
          lesiones graves en los órganos. Varias de las demandas interpuestas
          años atrás hasta la fecha siguen en espera de juicio.
        </p>
        <p>
          <strong> Desfibrilador cardioversor / marcapasos</strong>: Pacientes
          que sufren de arritmia (latidos cardiacos irregulares) necesitan este
          dispositivo para normalizar los latidos del corazón. Un desfibrilador
          cardioversor, también conocido como marcapasos, manda descargas
          eléctricas al corazón para que lata a un ritmo normal. La FDA retiró
          miles de marcapasos a consecuencia de defectos que ocasionaron muchas
          muertes y porque gran número de personas necesitaron cirugías para
          remplazarlos.
        </p>
        <p>
          <strong> Otros casos de dispositivos médicos incluyen</strong>:
        </p>
        <ul>
          <li>Remplazo de cadera de metal</li>
          <li>Robot de cirugía laparoscópica</li>
          <li>Endoprótesis</li>
          <li>Toxicidad de cobalto</li>
          <li>Injerto oseo</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Abogado Especialista En Dispositivos Médicos Defectuosos"
            style={{
              backgroundImage:
                "url('/images/medical/xray-spanish-medical-devices-text-header.jpg')"
            }}
          >
            <h2>¿Qué Hay Sobre Los Medicamentos Defectuosos?</h2>
          </div>
        </LazyLoad>

        <p>
          El proceso de estudio e investigación de medicamentos suele ser muy
          largo. Proteger al público de posibles efectos secundarios y riesgos
          es lo más importante durante los estudios clínicos y elaboración de
          medicamentos.
        </p>
        <p>
          Sin embargo, en ocasiones los efectos secundarios que se consideran
          leves o de plazo corto, ocasionan problemas de salud de largo plazo o
          graves para los pacientes que toman el medicamento. Los abogados de
          lesiones personales de Bisnar Chase representan a clientes que han
          sufrido discapacidades de corto plazo o incluso permanentes y lesiones
          debido a efectos secundarios de medicamentos peligrosos. Visite
          nuestra <strong> página de litigios farmacéuticos </strong>para
          informarse que casos relacionados a medicamentos tomamos.
        </p>
        <p>
          <strong>
            {" "}
            Cómo los medicamentos y dispositivos son caracterizados por la
            administración de alimentos y fármacos (FDA)
          </strong>
        </p>
        <p>
          <strong> </strong>Los dispositivos deficientes no tienen la intención
          hacer daño a terceras personas. La razón que un dispositivo defectuoso
          se pone ampliamente a la venta es por la falta de investigación y
          verificación. A una empresa fabricante de medicamentos y dispositivos
          médicos se le puede responsabilizar por daños graves y muerte
          negligente si no cumple con los requisitos establecidos con la
          administración de alimentos y fármacos. A continuación, se encuentran
          los elementos que medicamentos o instrumentos médicos necesitan para
          ser aprobados por la
          <strong>
            {" "}
            <Link
              to="https://www.fda.gov/AboutFDA/EnEspanol/default.htm"
              target="_blank"
            >
              {" "}
              FDA
            </Link>
          </strong>
          .
        </p>
        <p>
          <strong> </strong>
          <strong>
            {" "}
            La FDA aprueba que los medicamentos sean los siguientes:{" "}
          </strong>
        </p>
        <ul>
          <li>
            Uso autorizado por un funcionario que manifieste que el medicamento
            cumple con los códigos de la FDA
          </li>
          <li>
            Que tenga el propósito de ayudar, curar o prevenir enfermedades
          </li>
          <li>
            Que la función del medicamento sea para mejorar la estructura del
            cuerpo o ayudar a otro medicamento a cumplir su función
          </li>
        </ul>
        <p>
          <strong>
            {" "}
            La FDA aprueba dispositivos médicos que sirven como herramienta con
            la finalidad de:
          </strong>
        </p>
        <ul>
          <li>
            Diagnosticar condiciones médicas, enfermedades o usar como
            tratamiento<strong> </strong>
          </li>
          <li>
            Ser reconocidos oficialmente por el formulario nacional de
            medicamentos o farmacopea de Estados Unidos<strong> </strong>
          </li>
          <li>
            Destinar a alterar la estructura del cuerpo o afectar la forma en
            que el cuerpo de un animal o humano opera <strong> </strong>
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/medical/pills-spanish-medical-devices-resized.jpg"
            width="100%"
            className="imgright"
            alt="Abogado Especialista En Dispositivos Médicos Defectuosos"
          />
        </LazyLoad>
        <h2>¿Por Qué Debería Interponer Una Demanda?</h2>
        <p>
          Interponer una demanda por un dispositivo médico o medicamento que le
          ocasionó un daño imprevisto le dará la compensación que necesita para
          recuperarse. Las víctimas de negligencia médica o medicamentos
          deficientes pueden solicitar un reembolso por las facturas médicas,{" "}
          <Link
            to="https://es.wikipedia.org/wiki/Trauma_ps%C3%ADquico"
            target="_blank"
          >
            {" "}
            trauma psíquico
          </Link>{" "}
          y pérdida de sueldo. Hemos recopilado recursos de todas estas
          deficiencias y sabemos cómo pelear contra los grandes fabricantes que
          causan daños graves.
          <strong>
            {" "}
            Los abogados de dispositivos médicos deficientes de Bisnar Chase han
            mantenido el 95% de éxito y ganado millones para las víctimas que
            han experimentado daños graves.
          </strong>
        </p>
        <p>
          Si usted o alguien que conoce ha sufrido de daños graves debido a la
          fabricación deficiente de instrumentos médicos, comuníquese al bufete
          de abogados Bisnar Chase. Al llamar recibirá una consulta gratis con
          un abogado de lesiones.
        </p>
        <p>
          Llame al <strong> 877-958-8092</strong>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
