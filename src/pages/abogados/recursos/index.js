// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Recursos para Victimas de Accidentes - Bisnar Chase"
        pageDescription="See our legal resources - Determine if you have a valuable case, learn about the process of a car accident case, or learn how to bring your own case to small claims court."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Recursos Utiles</h1>
        <BreadCrumbs location={location} />
        <h2 style={{ marginBottom: "4rem", borderBottom: "none" }}>
          Encuentre abogados de otros estados, aplicaciones para accidentes
          automovilísticos, libros y más.
        </h2>
        <div className="resources-page">
          <div style={{ marginBottom: "2rem" }}>
            <h2 className="resource-category" id="resource-category-1">
              <i className="fa fa-ambulance"></i> - Recursos para Accidentes
              Automovilísticos 
            </h2>

            <div className="resource-link">
              {" "}
              <Link
                to="/abogados/recursos/que-hacer-despues-un-accidente"
                target="_blank"
              >
                <div className="link-row">
                  Qué hacer si se lesiona en un accidente automovilístico
                </div>
              </Link>
              <div className="text-row">
                Lea esta lista de acciones para saber qué y qué no hacer después
                de un accidente automovilístico, para asegurar su seguridad y
                como pueda obtener la compensación máxima.
              </div>
            </div>
            <div className="resource-link">
              {" "}
              <Link to="/car-accidents/who-is-at-fault" target="_blank">
                <div className="link-row"></div>
              </Link>
            </div>
            <div className="resource-link"></div>
          </div>

          <div>
            <h2 className="resource-category" id="resource-category-2">
              <i className="fa fa-gavel"></i> - El Proceso Legal
            </h2>
            <div className="resource-link">
              {" "}
              <Link
                to="/resources/value-of-your-personal-injury-claim"
                target="_blank"
              >
                <div className="link-row"></div>
              </Link>
            </div>
            <div className="resource-link">
              {" "}
              <Link to="/abogados/recursos/elegir-un-abogado" target="_blank">
                <div className="link-row">
                  Cómo Contratar a un Abogado de Lesiones Personales
                </div>
              </Link>
              <div className="text-row">
                Si usted no sabe cómo contratar a un abogado de lesiones
                personales, asegúrese de encontrar a las personas adecuadas y
                hacer las preguntas correctas.
              </div>
            </div>
            <div className="resource-link">
              {" "}
              <Link to="/faqs/" target="_blank">
                <div className="link-row"></div>
              </Link>
            </div>
            <div className="resource-link"></div>
          </div>

          <div>
            <h2 className="resource-category" id="resource-category-4">
              &nbsp;
            </h2>
            <div className="resource-link"> </div>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .link-row {
    font-size: 1.6rem;
  }

  .text-row {
    font-size: 1.4rem;
  }
  .resources-page .resource-category {
    font-size: 32px;
    text-align: center;
    color: #21323f;
  }

  #content .resources-page .resource-link a {
    color: #416390;
    font-size: 18px;
    font-weight: normal;
  }

  .resources-page .text-row {
    margin-bottom: 10px;
  }

  .resources-page .resource-link .link-row {
    background-color: #ebe6d0;
    padding-left: 10px;
  }

  .resources-page .resource-link .link-row:hover {
    background-color: #e7deb5;
    transition: background-color 0.5s;
    -moz-transition: background-color 0.5s;
  }

  #content .resources-page .resource-link .text-row {
    box-shadow: 0px 0px 50px -8px rgb(235, 226, 190) inset;
    padding: 10px 10px 10px 50px;
    font-size: 12px;
  }

  #content .resources-page .resource-link {
    outline: 1px solid #e8e6db;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
