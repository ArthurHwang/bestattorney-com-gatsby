// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
// import Img from "gatsby-image"
// import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Conducir ebrio v&iacute;ctimas abogado - Bisnar Chase"
        pageDescription="conducir ebrio victimas abogado Bisnar Caza puede ayudarle a obtener una indemnizacion de la parte culpable de que lo lesion. Llame al 1-800-561-4887."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Conducir ebrio v&iacute;ctimas abogado - Bisnar Chase</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <p>
            Los abogados de Da&ntilde;os Personales de la firma Bisnar & Chase
            son experimentados y exitosos. Ellos representan a v&iacute;ctimas
            con da&ntilde;os severos y muerte por error provocadas por un
            conductor ebrio. La firma est&aacute; asentada en el Condado de
            Orange, pero representa a sus clientes en todo el sur de California.
            Si usted o un ser querido ha sufrido da&ntilde;os personales en un
            accidente por conductor ebrio u otro accidente serio,
            cont&aacute;ctese hoy con nosotros y h&aacute;ganos su consulta sin
            cargo.
          </p>
          <h2>
            Las siguientes estad&iacute;sticas son sobre conductores ebrios en
            los Estados Unidos en el 2002:
          </h2>
          <ul>
            <li>
              Hab&iacute;an m&aacute;s de 17,400 muertes relacionadas con el
              alcohol.
            </li>
            <li>
              Se estim&oacute; que un 41% de los choques fatales estuvieron
              relacionados con el abuso de alcohol.
            </li>
            <li>
              258,000 personas fueron v&iacute;ctimas de da&ntilde;os en
              accidentes relacionados con el alcohol.
            </li>
          </ul>
          <p>
            Los accidentes causados por conductores ebrios est&aacute;n
            aumentando en forma alarmante en este pa&iacute;s. La imprudencia de
            los conductores ebrios est&aacute; costando las vidas de muchas
            personas inocentes. Muchas familias est&aacute;n siendo destruidas
            en un instante cuando inesperadamente un familiar resulta seriamente
            da&ntilde;ado por un conductor ebrio. &Eacute;stos deben hacerse
            responsables por el da&ntilde;o que causaron.
          </p>
          <p>
            Nuestra firma se compromete a proteger los derechos de nuestros
            clientes que sufrieron da&ntilde;os personales y a recobrar el
            m&aacute;ximo capital monetario posible. Hemos recuperado millones
            de d&oacute;lares para nuestros clientes con deudas por da&ntilde;os
            personales.
          </p>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
