import React from "react"
import { EspanolContactHome } from "src/components/elements/forms/Espanol_Form_Home"
import Helmet from "react-helmet"
import { LayoutEspanolHome } from "src/components/layouts/Layout_Espanol_Home"
import { EspanolBraggingRights } from "src/components/elements/espanol/home/Espanol_Home_BraggingRights"
import { EspanolFAQ } from "src/components/elements/espanol/home/Espanol_Home_FAQ"
import { EspanolPracticeAreas } from "src/components/elements/espanol/home/Espanol_Home_PracticeAreas"
import { EspanolResults } from "src/components/elements/espanol/home/Espanol_Home_Results"
import { EspanolReviews } from "src/components/elements/espanol/home/Espanol_Home_Reviews"
import { SEO } from "src/components/elements/SEO"

export default function IndexPage() {
  return (
    <LayoutEspanolHome>
      <SEO
        pageTitle="Abogados de Accidentes en California | (800) 561-4846"
        pageDescription="Póngase en contacto con Bisnar de Chase Abogados para una evaluación gratuita de su caso por un accidente de coche, defecto de auto u otras lesiones graves. Sirviendo a California desde 1978. (800) 561-4846"
      />
      <Helmet>
        <meta httpEquiv="content-language" content="es" />
      </Helmet>
      <EspanolContactHome />
      <EspanolBraggingRights />
      <EspanolResults />
      <EspanolReviews />
      <EspanolPracticeAreas />
      <EspanolFAQ />
    </LayoutEspanolHome>
  )
}
