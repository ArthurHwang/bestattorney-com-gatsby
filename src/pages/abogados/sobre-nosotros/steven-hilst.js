// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Steven Hilst – Abogado Especialista en Daños Personales"
        pageDescription="El abogado Hilst comenta que valora el ambiente de equipo en Bisnar Chase. El abogado Hilst ha obtenido el rango de asociado de la asociación de American Inns of Court."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Steven Hilst – Abogado Especialista en Daños Personales</h1>
        <BreadCrumbs location={location} location={location} />
        <h2>El Abogado Hilst Trabaja para hacer la Diferencia</h2>
        <LazyLoad>
          <img
            src="/images/steven-hilst.jpg"
            width="286"
            height="384"
            className="imgleft-fixed mb"
            alt="Steven Hilst"
          />
        </LazyLoad>
        <p>
          Steven Hilst se convirtió en abogado especialista en daños personales
          porque observó cómo su trabajo podía cambiar la vida de las víctimas
          lesionadas gravemente en accidentes, o la vida de familiares que
          perdieron a sus seres queridos por circunstancias trágicas.
        </p>
        <p>
          Cuando el abogado Hilst estaba en la escuela de derecho trabajó para
          un bufete de abogados que representaba los intereses de negocios y
          empresas. Él obtuvo muy poca satisfacción de ese empleo. Sin embargo,
          cuando se encargó de casos de daños personales, sintió un placer por
          el contacto directo con los clientes y por la oportunidad valiosa que
          obtuvo al ayudarlos a lidiar con sus penas y para recuperar sus vidas.
        </p>
        <h2>Hacer la Diferencia en la Vida de las Personas</h2>
        <p>
          El abogado Hilst recuerda particularmente en su carrera un caso de
          homicidio por imprudencia cuando ayudó a una joven – inmigrante de la
          India- quien estaba en Estados Unidos con su esposo, ingeniero. Ella
          aún asistía a la escuela con visa de estudiante cuando su esposo murió
          trágicamente en un estacionamiento, lo cual ocurrió como consecuencia
          de un defecto de la construcción.
        </p>
        <p>
          &ldquo;Pudimos llegar a un acuerdo sustancial para la mujer mediante
          el cual obtuvo los medios económicos para permanecer en el país y
          terminar sus estudios&rdquo;, comentó el abogado Hilst. &ldquo;En el
          campo de daños personales, como profesional, experimentas
          personalmente como al ayudar a alguien le cambia su vida. Conoces a
          personas que están en situaciones terribles en su vida y les ayudas a
          obtener compensación y apoyo monetario. ¿Qué podría tener más
          satisfacción que eso?
        </p>
        <p>
          El abogado Hilst en algún tiempo representó a corporaciones de
          seguros, ahora trabaja del lado contrario. Asi es como él lo ve.
        </p>
        <p>
          &ldquo;Si represento a las empresas de seguros, privo a alguien de una
          compensación y prácticamente le estoy denegando la justicia&rdquo;,
          comentó. &ldquo;Pero cuando represento al demandante lesionado, le
          ayudo a solicitar y obtener justica y además una compensación
          justa&rdquo;.
        </p>
        <h2>
          Pelear una Buena Batalla <strong> </strong>
        </h2>
        <p>El abogado Hilst trata cada caso como un juego de ajedrez.</p>
        <p>
          &ldquo;Soy competitivo y me gusta ganar&rdquo;, comentó. &ldquo;Veo el
          panorama en general y encuentro una estrategia. Tomo cada caso como si
          fuera a llegar a juicio. Recuerdo a cada momento que defiendo a
          alguien quien de otra manera no tendría la oportunidad de obtener
          justicia&rdquo;.
        </p>
        <p>Para el abogado Hilst no sólo se trata de dinero y victorias.</p>
        <p>
          &ldquo;El punto es tratar a tus clientes como si son parte de tu
          familia o amigos&rdquo;, relató. &ldquo;Es algo personal para
          mí&rdquo;.
        </p>
        <p>
          El abogado Hilst hace lo que dice cuando representa a víctimas de
          accidentes en contra de empresas de seguros. Se  enfoca en relatar las
          historias de sus clientes a las empresas de seguros, humanizándolos en
          lugar de hacerlos parecer números en una página.
        </p>
        <p>
          &ldquo;Trato que la empresa de seguros comprenda que la persona es un
          ser humano y que su vida importa&rdquo; comenta el abogado Hilst.
          &ldquo;Con la esperanza de ayudar a las empresas que entiendan la
          historia de la persona, de lo contrario no dudo y apuesto todo para
          llevar el caso ante un tribunal y obtener lo que legítimamente le
          corresponde a mis clientes&rdquo;.
        </p>
        <h2>Un Ambiente de Equipo</h2>
        <p>
          El abogado Hilst se incorporó recientemente al equipo de abogados
          litigantes de{" "}
          <Link to="/abogados" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          y cree que se encuentra en el lugar correcto.
        </p>
        <p>
          &ldquo;El bufete de abogados Bisnar Chase es muy conocido por ser
          líder cuando <strong> de lesiones catastróficas</strong> y{" "}
          <strong> casos de responsabilidad de productos</strong> se
          trata&rdquo;, explicó. &ldquo;Aquí el equipo va más allá de
          representar a los clientes. Este es un bufete que no tiene temor a
          enfrentarse a empresas multimillonarias&rdquo;.
          <LazyLoad>
            <img
              src="/images/bisnar-chase/usc-steve.png"
              width="145"
              height="215"
              className="imgright-fixed"
              alt="Steven Hilst – Abogado Especialista en Daños Personales"
            />
          </LazyLoad>
        </p>
        <p>
          El abogado Hilst comenta que valora el ambiente de equipo en Bisnar
          Chase. &ldquo;Trabajamos en conjunto por el bien en común, el cual es
          obtener una justa compensación para los clientes&rdquo;, relató.
          &ldquo;Pero los honorarios en sí no son la motivación de la labor
          satisfactoria que hacemos. La meta principal es hacer el mejor trabajo
          posible para los clientes y tratarlos bien. Es un honor ser parte de
          este equipo&rdquo;.
        </p>
        <p>
          El abogado Hilst nació en una ciudad pequeña al centro de Illinois, en
          el área donde el presidente Abraham Lincoln ejercía como abogado. Se
          mudó al sur de California cuando tenía tres años y ha permanecido aquí
          desde entonces. El abogado Hilst obtuvo su licenciatura de la
          Universidad de sur de California en 1984 y un doctorado de la facultad
          de derecho de la Universidad Santa Clara en 1988. También tiene un
          título de posdoctorado de la facultad de derecho de la Universidad de
          San Diego. Vive en Redondo Beach y le encanta hacer ejercicio e ir a
          la playa. El abogado Hilst también es fanático del equipo de fútbol
          americano de la USC.
        </p>
        <h2>Logros Profesionales</h2>
        <p>
          Desde el año 2001 el abogado Hilst ha sido miembro de la prestigiosa{" "}
          <strong> asociación de abogados litigantes estadunidenses</strong>{" "}
          (ABOTA), una asociación solo para invitados que se compone de los
          mejores abogados litigantes sobresalientes en Estados Unidos. También
          es miembro vitalicio del foro de defensores del Million Dollar un
          honor otorgado solo al 5% de abogados en Estados Unidos.
        </p>
        <p>
          El abogado Hilst ha obtenido el rango de asociado de la asociación de
          &ldquo;American Inns of Court&rdquo;. Ha prestado servicios al consejo
          de directores de la asociación &ldquo;William J. Rea/ABOTA Inn of
          Court&rdquo;. También fue parte de la facultad de la escuela de
          derecho LA-ABOTA. Además, presta servicios al consejo del club
          &ldquo;The beach cities&rdquo; el cual recauda fondos para estudiantes
          atletas.
        </p>
        <p>
          ¿Cuál es su lema? &ldquo;trabajar duro, siempre estar preparado,
          hablar desde el corazón, nunca rendirse y siempre seguir
          luchando&rdquo;.
        </p>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/133465"
            target="_blank"
          >
            {" "}
            Perfil de Steven Hilst en la barra de abogados
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
