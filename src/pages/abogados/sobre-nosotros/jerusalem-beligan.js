// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Jerusalem Beligan, Abogado a Bisnar Chase"
        pageDescription="La pasión del abogado Beligan es defender a personas que han sido perjudicadas. El abogado Beligan se considera un “fiscal civil”, abogado que escogió como misión representar a la gente común, proteger sus derechos y pelear por sus intereses. Para programar una consulta gratis por favor llame al 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Jerusalem Beligan, Abogado Laboral de California</h1>
        <BreadCrumbs location={location} location={location} />

        <h2>Buscar La Justica Para Todos – Es El “Llamado”</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/J.B.Bio Photo.jpg"
            width="45%"
            className="imgleft-fluid mb"
            alt="Jerusalem Beligan"
          />
        </LazyLoad>
        <p>
          Para el abogado Jerusalem Beligan la justicia es más que una palabra,
          para él, la &ldquo;justicia&rdquo; es un llamado, una misión y la
          esperanza del futuro.
        </p>
        <p>
          El abogado Beligan es nativo del sur de California y su sueño de toda
          la vida fue ser abogado. Su padre, uno de sus héroes, también fue
          abogado.
        </p>
        <p>
          El abogado Beligan creció mirando a su padre servir al pueblo de las
          Filipinas, viendo la admiración que la gente le tenía  y los
          resultados que él obtuvo para ellos.
        </p>
        <p>
          Siguiendo los pasos de su padre, Jerusalem se hizo abogado para el
          pueblo, como si fuera un llamado de familia, algo que los Beligans han
          sido llamados a ser. El abogado Beligan está agradecido por el modelo
          que su padre estableció en él y por orientarlo a través del proceso
          para ser abogado del pueblo. Para continuar con la tradición familiar,
          el abogado Beligan, a{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>
          , está pasando lo que aprendió de su padre a sus hijos.
        </p>
        <h2>Fiscal Civil – “La Misión”</h2>
        <p>
          <em>
            El abogado Beligan se considera un &ldquo;fiscal civil&rdquo;,
            abogado que escogió como misión representar a la gente común,
            proteger sus derechos y pelear por sus intereses. En otras palabras,
            ser un héroe cotidiano para estas personas.  La pasión del abogado
            Beligan es defender a personas que han sido perjudicadas. Él
            disfruta y supera los enfrentamientos en contra  de gigantes
            corporativos, empresas de seguros y empleadores abusivos quienes han
            perjudicado a los clientes, tal como superó a oponentes mucho más
            grandes y aparentemente más fuertes cuando era parte del equipo de
            fútbol americano en la secundaria.
          </em>
        </p>
        <h2>Algunos De Los Éxitos Profesionales Del Abogado Beligan</h2>
        <p>
          <strong> Resultados de los casos del abogado Beligan</strong>
        </p>

        <p>
          <strong> Monto otorgado                          Tipo de caso</strong>
          <br />$ 2.000.000                                  Salario y horas
          laborales <br />$ 1.478.819                                  Salario y
          horas laborales <br />$ 600.000                                    
          Salario y horas laborales <br />$
          575.000                                     Salario y horas laborales{" "}
          <br />$ 600.000                                     Salario y horas
          laborales <br />$ 250.000                                     Salario
          y horas laborales
        </p>

        <h2>Educación</h2>
        <p>
          Jerusalem Beligan creció en Diamond Bar, California y fue estudiante
          destacado y atleta de la escuela secundaria, Roland. Fue parte del
          equipo de fútbol americano y atletismo de donde proviene su apodo,
          &ldquo;el cohete&rdquo;, por su velocidad y habilidad de burlar a los
          tacleadores. En 1997 se recibió de la Universidad de California,
          Riverside con la distinción magna cum laude.{" "}
        </p>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/ucr-logo.7db66ff.png"
            // width="25%"
            width="250"
            className="imgright-fixed"
            alt="jerusalem beligan"
          />
        </LazyLoad>
        <p>
          El desempeño de sus estudios le otorgó ser parte de la sociedad Phi
          Beta Kappa, la sociedad nacional de honores &ldquo;Golden Key&rdquo; y
          a la única sociedad de honores para estudiantes universitarios de
          civismo en Estados Unidos, Pi Sigma Alpha. Continuó  sus estudios para
          obtener un doctorado en jurisprudencia en la Universidad de derecho
          Southwestern en el 2000, mismo año que aprobó el examen jurídico de
          California. El abogado Beligan comenzó a ejercer a la temprana edad de
          veinticinco años.
        </p>
        <h2>Esposo y Padre Dedicado</h2>
        <p>
          El abogado Beligan reside en Ladera Ranch, California con su esposa
          Jackie y sus tres hijos. Jackie es maestra de inglés e inglés como
          segundo idioma del séptimo grado en la escuela primaria Bernice Ayer
          (&ldquo;BAMS&rdquo;) en San Clemente, California. La hija mayor de
          ambos, Jaylene, cursa el segundo año de secundaria y su hijo Jayden
          está en el segundo año de primaria.
        </p>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/Jerusalem and Wife.jpg"
            width="100%"
            className="imgright"
            alt="Jerusalem Beligan"
          />
        </LazyLoad>
        <p>
          El abogado Beligan nombró a su hija menor &ldquo;Justice&rdquo;
          (Justicia) por su devoción a las leyes y desea que algún día siga los
          pasos de su padre y abuelo. El abogado Beligan visualiza a
          &ldquo;Justice&rdquo; ejerciendo derecho como es la tradición familiar
          o mejor aún, presidiendo en el tribunal como &ldquo;jueza de
          justicia&rdquo;.
        </p>
        <p>
          Cuando se toma un tiempo libre, disfruta estar activo físicamente
          jugando básquetbol (&ldquo;el cohete&rdquo; sigue siendo veloz) y
          pasar tiempo con su familia.
        </p>
        <p>
          <em>
            {" "}
            <Link
              to="http://members.calbar.ca.gov/fal/Member/Detail/211258"
              target="_blank"
            >
              {" "}
              Perfil de la barra del estado del abogado Beligan
            </Link>
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
