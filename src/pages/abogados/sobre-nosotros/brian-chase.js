// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brian Chase - Abogado de Lesiones Personales en California"
        pageDescription="Brian Chase es socio gerente y abogado litigante principal que dirige el departamento de litigios de Bisnar Chase.El abogado Chase es un abogado apasionado de lesiones personales que comprende lo que les está pasando a sus clientes y protegerá sus derechos."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Brian Chase - Abogado de Lesiones Personales en California</h1>
        <BreadCrumbs location={location} location={location} />
        <h2>Qué significa ser un defensor de lesiones para Brian</h2>
        <LazyLoad>
          <img
            src="../../images/brian-chase-2014.png"
            width="253"
            height="397"
            className="imgleft-fixed"
            style={{ marginBottom: "2rem" }}
            alt="Brian Chase - Abogado de Lesiones Personales en California "
          />
        </LazyLoad>
        <p>
          Significa ayudar a las personas lesionadas a obtener justicia de las
          grandes corporaciones que fabrican productos peligrosos que causan
          lesiones. Esto ocurre cuando dichas corporaciones ponen las ganancias
          arriba de la seguridad.
        </p>
        <p>
          Ayudar a las víctimas de accidentes a cobrar lo que les deben las
          compañías de seguros, las cuales creen que solo están en el negocio
          para cobrar las primas en contraste al negocio de pagar los reclamos
          válidos.
        </p>
        <p>
          La satisfacción de ayudar a alguien a recuperar su vida después de un
          accidente o lesión, especialmente uno debido a la mala conducta de los
          demás.
        </p>
        <h2>Acerca de Brian D. Chase</h2>
        <p>
          Brian Chase es socio gerente y abogado litigante principal que dirige
          el departamento de litigios de la firma. El abogado Chase fue nombrado
          Litigante de Alta Participación 2017, Abogado Líder de Distinción en
          2016, así como ganador de SuperLawyer 2016 y uno de los Mejores
          Abogados de Estados Unidos 2016. Fue el Presidente de los Abogados del
          Consumidor de California en 2015 y ex Presidente de la Asociación de
          Abogados Litigantes del Condado de Orange. Es miembro de ABOTA. En
          2014, la Asociación de Abogados Litigantes del Condado de Orange lo
          nombró Abogado Litigante del Año en responsabilidad por productos
          defectuosos.
        </p>
        <p>
          En 2012, el abogado Chase fue nombrado Abogado Litigante del Año por
          los Abogados del Consumidor de California y nominado como Abogado de
          Litigio del Año por la Asociación de Abogados del Consumidor de Los
          Ángeles. En el 2004 fue nombrado Abogado Litigante del Año por
          responsabilidad de productos por la Asociación de Abogados Litigantes
          del Condado de Orange.
        </p>
        <p>
          Está en la lista como uno de los Mejores Abogados Litigantes de la
          Asociación Americana de Abogados Litigantes desde el 2007. También ha
          sido incluido en el uno por ciento de los mejores abogados de la
          nación por la Asociación Nacional de Abogados distinguidos. Se ha
          ganado el título de SuperLawyer del sur de California desde el 2007 y
          un Top 50 SuperLawyer del condado de Orange en los últimos cuatro
          años.
        </p>
        <LazyLoad>
          <img
            src="/images/brian-scott-caoc.jpg "
            width="100% "
            alt="Brian Chase - Abogado de Lesiones Personales en California "
          />
        </LazyLoad>
        <p>
          En 2009, el abogado Chase publicó su libro &ldquo;Still Unsafe at Any
          Speed&rdquo; (Inseguro a cualquier velocidad), que trata sobre la
          industria automotriz y los vehículos defectuosos. En el 2017 lanzó una
          versión actualizada. Es un conferencista frecuente por todo el país
          sobre temas relacionados con litigios y ha sido invitado en la radio y
          televisión, con apariciones en &ldquo;Informes especiales de
          CBS&rdquo;, &ldquo;Peter Jennings / World News Tonight&rdquo;,
          &ldquo;Fox 11 News&rdquo; y &ldquo;America's Best Lawyers&rdquo;.
        </p>
        <p>
          El abogado Chase fue abogado principal en cuatro importantes
          sentencias de apelación que establecieron precedentes: Martínez-Mazon
          v. Ford Motor Company (caso de productos defectuosos de automóviles
          que trata con <em>Forum Non Conveniens</em>); Romine v. Johnson
          Controls (caso de productos defectuosos de automóviles que trata con
          la prueba de expectativas del consumidor para probar defectos);
          Schreiber v. Estate of Kiser (caso del Tribunal Supremo de California
          que trata con designaciones de testigos expertos); y Hernández v.
          Estado de California (que se ocupa de la inmunidad de diseño
          gubernamental).
        </p>
        <h2>Algunos de los resultados de casos del abogado Brian Chase</h2>
        <p>
          <strong> Monto otorgado        Tipo de caso</strong>
        </p>
        <ul>
          <li>
            <span>$ 38,650,000</span> - <span>Accidente de motocicleta</span>
          </li>
          <li>
            <span>$ 24,700,000 </span> - <span> Defecto automotriz- MVA</span>
          </li>
          <li>
            <span>$ 23,091,000</span> - <span> Producto defectuoso- MVA</span>
          </li>
          <li>
            <span>$ 16,444.904</span> - <span>MVA</span>
          </li>
          <li>
            <span>$ 11,694.904</span> -{" "}
            <span>Responsabilidad de las instalaciones</span>
          </li>
          <li>
            <span>$ 10,591.098</span> -{" "}
            <span> Responsabilidad del producto</span>
          </li>
          <li>
            <span>$ 10,030,000</span> - <span> Defecto automotriz- MVA</span>
          </li>
          <li>
            <span>$ 8,500,000</span> - <span> MVA</span>
          </li>
          <li>
            <span>$ 8,250,000</span> - <span>MVA</span>
          </li>
          <li>
            <span>$ 4,750,000</span> -{" "}
            <span> Responsabilidad de las instalaciones</span>
          </li>
          <li>
            <span>$ 3,780,000</span> - <span> Negligencia</span>
          </li>
          <li>
            <span>$ 2,432.250</span> - <span>Responsabilidad del producto</span>
          </li>
          <li>
            <span>$ 2,000,000</span> -{" "}
            <span> Responsabilidad de las instalaciones</span>
          </li>
        </ul>
        <h2>Mejores Abogados en América 2014- 2016</h2>
        <p>
          El abogado Brian Chase fue seleccionado por sus colegas para su
          inclusión en la edición número 21 de The Best Lawyers in America en
          las áreas de práctica de: Litigios de lesiones personales -
          Demandantes y Litigios de responsabilidad por productos defectuosos -
          Demandantes. Best Lawyers es la publicación de revisión por pares más
          antigua y respetada en la profesión legal. Un listado en Best Lawyers
          es ampliamente considerado por los clientes y profesionales legales
          como un honor significativo, otorgado a un abogado por sus colegas.
          Durante más de tres décadas, las listas de Best Lawyers se han ganado
          el respeto de la profesión, los medios y el público como la fuente más
          confiable e imparcial de referencias legales en cualquier lugar.
        </p>
        <LazyLoad>
          <img
            src="/images/office favs 544.jpg"
            width="100%"
            className="imgright"
            alt="brian chase - abogado de lesiones personales en california"
          />
        </LazyLoad>
        <p>
          El abogado Chase es un abogado apasionado de lesiones personales que
          comprende lo que les está pasando a sus clientes y protegerá sus
          derechos.
        </p>
        <p>
          El abogado Chase centra sus esfuerzos en defectos automotrices y
          productos defectuosos o peligrosos. Se ha ganado un nombre luchando
          contra algunos de los gigantes corporativos más grandes en nombre de
          la justicia para sus clientes. Desde los defectos del automóvil hasta
          los productos de consumo que no advierten ni perjudican, Brian es el
          hombre indicado para el trabajo.
        </p>
        <p>
          &ldquo;Fui específicamente a la facultad de derecho para convertirme
          en un abogado defensor de lesiones personales. Es lo que amo, lo que
          hago y lo que soy&rdquo; - Brian Chase.{" "}
        </p>
        <p>
          Uno de los momentos de mayor orgullo del abogado Chase, entre muchos,
          fue la victoria que obtuvo representando a una cliente que resultó
          gravemente herida en un caso de defectos de autos que la dejó
          paralizada.
        </p>
        <p>
          Brian se hizo cargo de la industria automotriz y obtuvo una victoria
          de 24,7 millones de dólares para su cliente y aseguró la atención
          médica y la seguridad financiera para el resto de su vida. Aquí está
          Brian discutiendo el caso con Fox News. Otro caso favorable y
          conmovedor para el abogado Chase fue obtener más de un millón de
          dólares para la mujer de 95 años más increíble que haya conocido, cuya
          vida fue arruinada debido a la negligencia de otro, después de que la
          compañía de seguros declaró * por escrito que su reclamo era una
          &ldquo;farsa&rdquo;.
        </p>
        <h2>Brian Chase - Autor con una pasión por la seguridad</h2>
        <LazyLoad>
          <img
            src="/images/stillunsafe100.jpg"
            width="100"
            height="162"
            className="imgcenter-fixed"
            alt=""
          />
        </LazyLoad>
        <p>
          El abogado Brian Chase es el autor de &ldquo;Still Unsafe at Any
          Speed&rdquo;, un libro sobre la industria del automóvil y los peligros
          que esconde. Desde muertes injustas hasta lesiones catastróficas,
          Brian Chase se hace cargo de la parte vulnerable de la industria
          automotriz, hablando acerca del poderoso dólar antes de la seguridad
          en su libro sobre defectos de automóviles.
        </p>
        <LazyLoad>
          <img
            src="/images/Brian Chase - Next Big Thing Cover.jpg"
            width="107"
            height="160"
            className="imgcenter-fixed"
            alt=" Brian Chase - Abogado de Lesiones Personales en California"
          />
        </LazyLoad>
        <p>
          El abogado Chase es coautor de &ldquo;The Next Big Thing&rdquo; (La
          siguiente cosa más grande), un mapa vial a las principales tendencias
          a los principales expertos en la navegación de la nueva economía.{" "}
        </p>
        <p>
          ¿Tiene preguntas? ¿Necesita ayuda? Contáctenos para saber acerca del
          defecto de automóvil o caso de responsabilidad por productos
          defectuosos para una <strong> evaluación gratuita al</strong>
          <strong> (949) 829-4504</strong>.
        </p>
        <p>
          Tenemos la reputación, los medios y la pasión para llevar adelante su
          caso en nombre de la justicia. El abogado Chase ha recuperado millones
          de dólares en acuerdos y veredictos por lesiones graves y
          catastróficas.
        </p>
        <h2>Artículos de Brian D. Chase</h2>• Utilizando testigos expertos •
        Testigos expertos y mociones a límine • Identificando y litigando casos
        de defectos de productos de autos de calidad • Pruebas de vanguardia
        para casos de vuelco: maniobra de recuperación de borde en la carretera
        de NHTSA
        <LazyLoad>
          <img src="/images/contact-bg.jpg " width="100% " alt=" " />
        </LazyLoad>
        <h2>Certificado en MCLE</h2>
        <p>
          Brian Chase cuenta con la certificación interna para enseñar el curso
          &ldquo;Cómo identificar casos de defectos automotrices&rdquo; con un
          crédito de 1.5 horas para su mínimo de educación legal continua
          (MCLE).
        </p>
        <p>
          <strong>
            {" "}
            Llámenos al 877-958-8092 para ver cómo le podemos ayudar a obtener
            la compensación que se merece.
          </strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
