// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tom Antunovich - Abogado de Daños Personales en California"
        pageDescription="El abogado Antunovich representa a víctimas de accidentes automovilísticos y aquellos que han sido lesionados por dispositivos médicos defectuosos o por fármacos recetados. Si le gustaría comunicarse con el abogado Antunovich para una consulta gratuita referente a una lesión por favor llame al 877-302-1639."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tom Antunovich - Abogado de Daños Personales en California</h1>
        <BreadCrumbs location={location} location={location} />3
        <LazyLoad>
          <img
            src="/images/bisnar-chase/tom-antunovich-2-small.jpg"
            width="220"
            height="330"
            className="imgleft-fixed mb"
            alt="Tom Antunovich - Abogado de Daños Personales en California"
          />
        </LazyLoad>
        <p>
          {" "}
          Tom Antunovich es abogado de daños corporales en California y abogado
          litigante del bufete. Su su especialidad se enfoca en reclamos de
          negligencia y productos defectuosos. El abogado Antunovich representa
          a víctimas de accidentes automovilísticos y aquellos que han sido
          lesionados por dispositivos médicos defectuosos o por fármacos
          recetados.
        </p>
        <p>
          A mitad del año 2016, el abogado Antunovich fue asignado como abogado
          asociado principal del bufete de abogados 
          <strong>
            {" "}
            Johnson & Johnson en casos de cáncer ovárico por el uso de talco
            para bebé.
          </strong>
          Para el abogado Antunovich es una satisfacción responsabilizar a los
          criminales y a las corporaciones por sus actos y asegurar
          compensaciones para los clientes perjudicados.
        </p>
        <h2>Logros Profesionales</h2>
        <p>
          El abogado Antunovich es nativo del condado de Orange. Se recibió de
          la escuela secundaria Troy, escuela prestigiosa de Fullerton y obtuvo
          su licenciatura en quinesiología en de la Universidad Politécnica
          Pomona en el año 2010 donde compitió en el equipo varonil de fútbol
          por medio de una beca deportiva. Asistió a la Universidad estatal de
          derecho Western y se recibió con el nivel cum laude en mayo de 2015.
          En julio del 2015 aprobó el examen jurídico de California.
        </p>
        <p>
          En el bufete {" "}
          <Link to="/abogados" target="_blank">
            Bisnar Chase
          </Link>
           el abogado Antunovich ha demostrado ser un joven apasionado y con
          potencial, comentó H. Gavin Long, uno de los abogados del despacho.
        </p>
        <p>
          <em>
            &ldquo;Si vas a ser abogado en daños personales, tienes que ser
            compasivo pero a la vez un buldog que pelea por los derechos de los
            clientes. Tom tiene el 100% de esas cualidades&rdquo;
          </em>
           – <strong> H. Gavin Long</strong>.
        </p>
        <h2>Casos Ganados</h2>
        <p>
          El abogado Antunovich ha tenido gran impacto en el bufete. Ha
          asegurado compensaciones considerables para sus clientes y tiene un
          caso por jurado a su favor el cual resultó en un veredicto para el
          demandante en un litigio de responsabilidad. 
          <strong> Brian Chase</strong>, socio director, predice un futuro largo
          y exitoso para el abogado Antunovich.
        </p>
        <p>
          &ldquo;
          <em>
            En los 25 años que he ejercido derecho, no he visto a un abogado
            nuevo con impulso, determinación, talento y habilidades como las que
            tiene Tom. Vi su potencial cuando era  ayudante legal y supe que lo
            deberíamos contratar como colaborador una vez que se recibiera de la
            escuela de derecho. Esta es la mejor decisión que he tomado en
            Bisnar Chase. Su nivel de habilidades reta a abogados que han estado
            ejerciendo por más de diez años. Se ha ganado tanto mi confianza,
            que trabajamos mano a mano en algunos de los casos más grandes del
            bufete
          </em>
          &rdquo;.
        </p>
        <p>
          El abogado Antunovich es brillante, joven, lleno de energía y
          comprometido con sus clientes. Como abogado litigante disfruta
          preparar sus casos para los juicios y presentarlos ante el jurado.
        </p>
        <h2>
          El Abogado Antunovich esta Profundamente Consciente de la Importancia
          de su Trabajo
        </h2>
        <p>
          &ldquo;
          <em>
            Nuestros clientes ponen su confianza en nosotros y me aseguro que el
            trabajo que realizo en cada caso lo refleje. Cada caso significa
            mucho para mí porque significa mucho para mi cliente. Me motiva a
            querer pelearlo y ganarlo
          </em>
          &rdquo;
        </p>
        <h2>
          La Lista de Logros del Abogado Antunovich hasta hora Habla por sí Sola
        </h2>
        <p>
          El abogado Antunovich es miembro de la asociación de abogados del
          condado de Orange, de la asociación de abogados litigantes y de la
          asociación de abogados del consumidor de California.
        </p>
        <div className="panel panel-primary">
          <div className="panel-heading">
            Resultados de casos del abogado Antunovich
          </div>
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>
                  <strong>Monto otorgado </strong>
                </th>
                <th>
                  <strong>Tipo de caso</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$575,000</td>
                <td> Tropiezo y caída </td>
              </tr>
              <tr>
                <td> $225,000</td>
                <td> Motocicleta v. Automóvil </td>
              </tr>
              <tr>
                <td>$185,000</td>
                <td>Defecto de la malla vaginal</td>
              </tr>
              <tr>
                <td>$150,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td> $115,000</td>
                <td>Accidente Automovilístico</td>
              </tr>
              <tr>
                <td>$100,000</td>
                <td> Accidente peatonal</td>
              </tr>
              <tr>
                <td>$100,000</td>
                <td>Accidente peatonal</td>
              </tr>
              <tr>
                <td>$100,000</td>
                <td>Remplazo de rodilla defectuoso</td>
              </tr>
              <tr>
                <td>$75,000</td>
                <td>Remplazo de rodilla defectuoso</td>
              </tr>
            </tbody>
          </table>
        </div>
        <h2>Interés Personal</h2>
        <p>
          Cuando el abogado Antunovich no está trabajando para sus clientes,
          disfruta jugar fútbol, pasar tiempo con sus amigos, y apoya a sus
          equipos deportivos favoritos: los Lakers de Los Ángeles, los Diablos
          Rojos del Manchester United, los Kings de Los Ángeles y el Galaxy de
          Los Ángeles. 
        </p>
        <p>
          &ldquo;
          <em>
            Existen riesgos y costos en una acción, pero son mucho menos que los
            riesgos a largo plazo de una cómoda inacción
          </em>
          &rdquo;.  –<strong> John F. Kennedy</strong>
        </p>
        <p>
          El abogado Antunovich es el mayor de tres hermanos y ha estado casado
          recientemente.
        </p>
        <h2>Comuníquese con el Abogado Antunovich</h2>
        <p>
          Si le gustaría comunicarse con el abogado Antunovich para una consulta
          gratuita referente a una lesión por favor llame al 877-302-1639.
        </p>
        <p>
          Vea el {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/305216"
            target="_blank"
          >
            {" "}
            Perfil del abogado Antunovich
          </Link>
           en el sitio web de la barra de abogados de California.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  tr {
    width: 100%;
  }

  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
