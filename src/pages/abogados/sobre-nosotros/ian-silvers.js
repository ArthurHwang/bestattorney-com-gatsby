// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Ian Silvers-Abogado de Lesiones Personales de California"
        pageDescription="Ian Silvers ha sido un apasionado de defender a las víctimas de la discriminación en el lugar de trabajo, negligencia y acoso durante años. Si usted se siente como si sus derechos han sido violados como un trabajador llamar a la firma de abogados de Bisnar Chase hoy."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ian Silvers-Abogado de Lesiones Personales de California</h1>
        <BreadCrumbs location={location} location={location} />
        <LazyLoad>
          <img
            src="/images/bisnar-chase/ian-headshot.jpg"
            className="imgleft-fixed"
            width="243"
            height="352"
            style={{ marginBottom: "2rem" }}
            alt="Ian Silvers"
          />
        </LazyLoad>
        <p>
          Ian Silvers tiene un sustancial de antecedentes en el derecho del
          empleo y es ferviente en proteger y luchar por los derechos de los
          empleados.
        </p>
        <p>
          Representa a los empleados en una serie de casos, incluyendo la
          discriminación, el acoso, las represalias y los asuntos salariales y
          horarios. Silvers es miembro del Departamento de litigios de acción de
          clase de Bisnar Chase que maneja acciones de clase de salario y hora.
        </p>
        <p>
          También aborda asuntos de acción no-clase que abordan el acoso sexual,
          la terminación ilícita, la represalia y la discriminación.
        </p>
        <h2>Una Pasión por Ayudar a los Empleados</h2>
        <p>
          Silvers se graduó de la Universidad de California en San Diego, en
          junio de 2003 con una licenciatura en economía. Obtuvo su título de
          abogado en la Facultad de derecho de la Universidad de Santa Clara en
          mayo de 2006. Silvers fue miembro de la fraternidad de honores legales
          de Phi Delta Phi International.
        </p>
        <p>
          Cuando estaba en la Facultad de derecho, él internó en el centro de
          derecho comunitario de Katharine y George Alexander asesorando a
          empleados de bajos ingresos bajo la supervisión de otros abogados en
          asuntos de empleo, incluyendo discriminación, acoso, represalias,
          desempleo y salarios y hora.
        </p>
        <p>
          Fue aquí, dice Silvers, que se dio cuenta de lo que es ayudar a
          "aquellos que realmente no eran capaces de ayudarse a sí mismos."
        </p>
        <p>
          "Pude ver que en materia de derecho laboral, había tantos empleados
          que no conocían plenamente sus derechos y, como resultado, estaban
          siendo aprovechados por sus empleadores", dijo. "Sin embargo, estos
          empleados no siempre son capaces de tomar medidas contra los
          empleadores, a menudo por razones financieras."
        </p>
        <h2>
          <strong> Demandas Laborales y Acciones de Clase</strong>
        </h2>
        <p>
          Silvers también ha representado a empleados antes de la división de
          cumplimiento de normas laborales de salarios y horas y obtuvo
          decisiones favorables para sus clientes. Se convirtió en miembro de la
          barra del estado de California en diciembre de 2006 y tiene licencia
          para practicar en las cortes estatales de California y ante los
          tribunales de distrito de los Estados Unidos para los distritos
          central y oriental de California.
        </p>
        <p>
          Silvers persevere en sus esfuerzos para ayudar a aquellos que no
          pueden permitirse la representación legal por el voluntariado
          servicios de asesoría. Recibió el premio Wiley W. Manuel por el
          servicio legal pro bono en el 2009.
        </p>
        <p>
          Antes de unirse a Bisnar Chase, Silvers trabajó en un bufete de
          abogados de empleo de Long Beach donde regularmente se ocupó de
          asuntos de salarios y horas, incluyendo acciones de clase y asuntos de
          abogados privados. Ha sido capaz de llegar a asentamientos
          significativos, incluyendo múltiples asentamientos de acción de clase
          de siete cifras para las clases pequeñas y grandes de empleados.
        </p>
        <h2>
          <strong> Lo que Más Valora</strong>
        </h2>
        <p>
          Silvers dice que sus éxitos de caso para los empleados a los que ha
          ayudado a lo largo de los años cuentan entre sus mayores logros.
        </p>
        <p>
          "No se trata sólo de ayudarles a defenderse en contra de sus
          empleadores, sino también de ayudarles a entender cómo pueden seguir
          adelante y conseguir algún tipo de cierre", dijo. "Estos casos no se
          trata sólo de dinero. A menudo, los empleados se obsesionan con lo que
          les pasó y no sólo afecta su futuro empleo, sino también sus
          relaciones con los demás."
        </p>
        <p>
          Cuando representa a estas personas cuyas vidas han sido profundamente
          impactadas, Silvers dice que hace un punto de mirar hacia el futuro en
          su nombre y ayudarles a averiguar cómo superar la situación. Él recibe
          tremenda satisfacción cuando ve la diferencia que ha hecho para un
          empleado individual.
        </p>
        <p>
          "Los empleadores pueden ganar, perder o resolver una demanda y no va a
          cambiar realmente su negocio o la forma en que se comportan", dijo
          Silvers. "Sin embargo, cada vez que recibo aprecio de un empleado que
          ve que yo creía en él o ella y luché en su nombre para ayudar a la
          injusticia y la maldad correctas, me recuerda por qué me encanta esta
          área de la ley.
        </p>
        <h2>
          Orgullo en ser un Miembro del Equipo de Bisnar<strong> Chase</strong>
        </h2>
        <p>
          Silvers dice que ha estado impresionado con el compromiso de Bisnar
          Chase de ayudar y retribuir a la comunidad, lo que califica de una
          calidad excepcional. "Muchos bufetes de abogados solo prestan atención
          a sus casos y al negocio, pero Bisnar Chase se centra regularmente en
          lo que puede hacer para ayudar a la comunidad, ya sea donando algunos
          elementos esenciales, como mochilas, artículos de punto, etc. o
          contribuyendo a una variedad. De las organizaciones benéficas no solo
          financieramente sino también con su tiempo ", dijo.
        </p>
        <p>
          Silvers creció en el área de Los Ángeles. Vive en el Condado de Orange
          con su esposa, Jennifer, una maestra de jardín de infancia en Anaheim
          y sus dos hijos, Lucas y Makenna. Cuando Silvers no lucha por los
          derechos de los empleados, disfruta pasar tiempo con su familia, ver
          deportes, especialmente fútbol y baloncesto, y viajar.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  tr {
    width: 100%;
  }

  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
