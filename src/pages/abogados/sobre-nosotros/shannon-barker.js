// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Adminastradora de Bisnar Chase-Shannon Barker"
        pageDescription="Adminastradora legal, Shannon Barker ha creado una cultura de trabajo segura y satisfactoria para los empleados de Bisnar Chase."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Shannon Barker- Administradora</h1>
        <BreadCrumbs location={location} location={location} />
        <LazyLoad>
          <img
            src="/images/bisnar-chase/shannon-barker.jpg"
            width="230"
            height="345"
            className="imgleft-fixed mb"
            alt="Shannon Barker- administradora"
          />
        </LazyLoad>
        <p>
          Shannon Barker es responsable de las operaciones diarias completas de
          BISNAR CHASE. El día típico de Shannon incluye una miríada de deberes
          incluyendo decisiones financieras, selección de empleados,
          negociaciones legales duras y administración de todo el personal.
        </p>
        <p>
          Shannon Barker no sólo escanea a través de currículos cuando está a
          punto de contratar a alguien. Ella mira a la persona de adentro hacia
          fuera, para cerciorarse de que él o ella encajaría realmente en la
          cultura de la persecución de BISNAR-una cultura que ella ha trabajado
          difícilmente para construir en los últimos veinte años.
        </p>
        <p>
          &ldquo;Busco gente que no sólo tenga los conjuntos de habilidades
          necesarios", dice Shannon. "Tienen que querer estar aquí."
        </p>
        <h2>Construyendo una Cultura de Civismo</h2>
        <p>
          Shannon llegó a la empresa en julio de 1995 como asistente legal y
          negociador después de trabajar cuatro años en otro bufete de abogados.
          Rápidamente se convirtió en líder de equipo y construyó una sólida
          reputación para manejar una multitud de temas relacionados con la
          empresa en su conjunto. Las habilidades de Shannon en la gestión de la
          firma de abogados crearon una cultura que otros juristas buscan
          modelar después.
        </p>
        <p>
          Shannon es una líder de equipo natural y experimentado con un ojo para
          la satisfacción del cliente. Sus ideas y métodos crean un equipo
          cohesivo de empleados que realmente disfrutan trabajando juntos. Esa
          habilidad natural también muestra en cómo los clientes de BISNAR CHASE
          son tratados.
        </p>
        <p>
          "Fue muy importante para mí tener un equipo equilibrado y feliz porque
          la forma en que nos tratamos se traduce en cómo tratamos a los
          clientes. Queríamos que fuera el tipo de atención personal que se
          obtiene en el Hotel Ritz ", dijo. "Los clientes lo notan todo-cómo se
          viste, cómo hablas, nuestra decoración. Nuestro personal es feliz y
          apreciado y se muestra en todo lo que hacen, y que afecta directamente
          a nuestros clientes.
        </p>
        <h2>Un Lider Inspiradora</h2>
        <p>
          Shannon siempre ha sido organizada y orientada a objetivos. La Sra.
          Barker es una Paralegal certificada en ABA y tiene su MBA en la
          Universidad de Chapman. Además de establecer metas para ella misma,
          Shannon también inspira a su personal. Ella se esfuerza por la calidad
          en sus empleados y proporciona educación continua en una variedad de
          programas y entrenamiento cruzado.
        </p>
        <p>
          Shannon dice que trabajar para John Bisnar le enseñó a siempre empujar
          a sí misma, y continuar con su educación abrió su proceso de
          pensamiento que impactó sus habilidades liderando un equipo exitoso.
        </p>
        <p>
          Shannon es muy clara y muy directa en su estilo de gestión, que es el
          sello distintivo de un buen administrador. Shannon lidera con el
          ejemplo, y su hábito de ser un oyente activo hace que los miembros del
          personal se sientan apreciados y escuchados. Su puerta siempre está
          abierta para todos.
        </p>
        <h2>Mantener un Delicado Equilibrio</h2>
        <p>
          El abogado H. Gavin Long dice que Shannon sabe cuándo empujar y cuándo
          retroceder, si está trabajando con un mediador o dirigiendo la
          oficina.
        </p>
        <p>
          "Ella es muy buena haciendo que todos crean en sí mismos", dice. "Si
          un empleado tiene una idea o enfoque que le gustaría probar, ella es
          muy de apoyo y siempre es positivo. Ella está constantemente animando
          a la gente a sacar el máximo provecho de sus trabajos ".
        </p>
        <p>
          "Sé que los empleados la respetan mucho y miran a ella no sólo por
          orientación profesional, sino también por orientación personal", dice
          John.
        </p>
        <p>
          Shannon cree que la empresa ha tenido éxito no sólo por ella, sino por
          el fantástico equipo que está en su lugar.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
