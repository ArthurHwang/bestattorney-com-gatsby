// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="H. Gavin Long- abogado de Lesiones personales"
        pageDescription="El abogado Long tiene una amplia gama de experiencia en juicios por jurado, en juzgados estatales y federales, en casos que van desde asesinato y fraude a negligencia médica. Para programar una consulta gratis con el abogado Long, por favor llame al (949) 537-2832."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>H. Gavin Long- Abogado Especialista en Lesiones Corporales</h1>
        <BreadCrumbs location={location} location={location} />
        <h2>La Emoción de Pelear por lo Justo</h2>
        <LazyLoad>
          <img
            src="/images/gavin-long-small.jpg"
            width="296"
            height="443"
            style={{ marginBottom: "2rem" }}
            className="imgleft-fixed"
            alt="Abogado Especialista en Lesiones Corporales-H. Gavin Long"
          />
        </LazyLoad>
        <p>
          Para Gavin Long, el juzgado es un lugar de justicia para las personas
          que fueron lesionadas por culpa de otros.Ya sea una compañía de
          Fortune 500 o un conductor ebrio que causó lesiones catastróficas a su{" "}
          cliente, el abogado Long es un campeón en la ley de daños corporales.
        </p>
        <p>
          <em>
            <strong>
              &ldquo;Tenemos leyes justas en California&rdquo; comenta el
              abogado Long. &ldquo;El reto para los abogados expertos en daños
              corporales es asegurarse de que las personas y las corporaciones
              acaten esas leyes&rdquo;
            </strong>
            .
          </em>
        </p>
        <h2>Logros Profesionales</h2>
        <p>
          El abogado Long tiene una amplia gama de experiencia en juicios por
          jurado, en juzgados estatales y federales, en casos que van desde
          asesinato y fraude a negligencia médica. Por ejemplo, tan solo el año
          pasado llevó seis casos y en todos obtuvo fallos por encima de lo que
          ofrecieron los demandados en todos los casos. Venció al código civil,
          artículo 998 en tres de los casos y &ldquo;atrapó&rdquo; la póliza de
          seguro del demandado en tres de los juicios.
        </p>
        <p>
          El abogado Long fue nombrado como abogado del consumidor en California
          (CAOC) en la <strong> junta directiva </strong>y forma parte del{" "}
          <strong> comité</strong> de la Asociación de Abogados Litigantes del
          Condado de Orange. Recientemente, ha ingresado como miembro a la junta
          directiva de defensores litigantes (
          <strong>
            {" "}
            <u>ABOTA</u>
          </strong>
          ), un honor otorgado a la elite de abogados en la nación. El abogado
          Long forma parte del{" "}
          <strong>
            {" "}
            uno por ciento de abogados más importantes en la nación
          </strong>{" "}
          que forman parte de esta organización más antigua y prestigiosa del
          país. De igual manera, el abogado Long ha sido otorgado con el
          reconocimiento{" "}
          <strong>
            {" "}
            &ldquo;Top Gun&rdquo;, abogado litigante del año 2016
          </strong>{" "}
          de la
          <strong>
            {" "}
            <Link to="http://www.octla.org/default.asp?" target="_blank">
              {" "}
              Asociación de Abogados Litigantes del Condado de Orange
            </Link>
          </strong>
          .
        </p>
        <h2>Luchando Contra las Compañías de Seguros</h2>
        <p>
          El abogado Long ha sido parte del equipo de Bisnar Chase desde abril
          de 2012 y ha tratado un sin número de casos de accidentes
          automovilísticos, muchos de los cuales llevó a  juicio ante un jurado,
          ganando la gran mayoría. En tan solo este año, el abogado Long ha
          ayudado a sus clientes a asegurar aproximadamente 5 millones para su
          recuperación total.
        </p>
        <p>
          Lo que el abogado disfruta del proceso es la lucha y la victoria en
          contra de las compañías de seguros sin escrúpulos – corporaciones que
          no se tocan el corazón ante las victimas que tratan de recuperar su
          vida normal después de haber sufrido un cambio traumático.
        </p>
        <LazyLoad>
          <img
            src="/images/Gving Long-.jpg"
            width="320"
            height="266"
            className="imgright-fixed"
            alt="H. Gavin Long-Abogado Especialista en Lesiones Corporales"
          />
        </LazyLoad>
        <p>
          <strong>
            {" "}
            <em>
              &ldquo;Cuando expongo a esas compañías de seguros y sus trucos, me
              encanta&rdquo;
            </em>
          </strong>
          , comenta el abogado Long.&ldquo;Disfruto derrotar a las compañías de
          seguros. Las personas pagan diligentemente por las primas pensando que
          serán indemnizados en un momento necesario. Sin embargo, se dan cuenta
          después de sufrir un accidente que tienen que caminar sobre brasas
          para ser indemnizados y al final puede que no obtengan nada.   
        </p>
        <p>
          Es erróneo suponer que la compañía de seguros es su amigo. &ldquo;En
          los casos que he llevado, las compañías de seguros no els importa.
          Cuando alguien se lastima, la persona deja de ser humano. Para ellos
          solo es un expediente. Puede que la persona se lastime gravemente o
          termine en silla de ruedas, pero para ellos, solo son un
          expediente&rdquo;.
        </p>
        <h2>Luchar y Ganar</h2>
        <p>
          El abogado Long sabe emprender una lucha. Recientemente en un caso
          aseguró un veredicto por la cantidad de 127.347 dólares para una
          víctima de accidente automovilístico cuando su compañía de seguro se
          negó a pagarle 15.000 dólares. La mujer se sometió a una cirugía de
          hombro, y su aseguradora, Fred Loya Insurance, se negó a hacer el pago
          cuestionando la necesidad de la intervención. Incluso argumentaron que
          la mujer no cedió el paso, cuando de hecho fue al otro conductor al
          que se le requería cederlo antes de girar a la izquierda.
        </p>
        <p>
          <strong>
            <em>
              &ldquo;En este caso, la compañía de seguro pudo haber pagar a
              nuestro cliente la cantidad de 15.000 dólares, lo cual era el
              límite del seguro&rdquo;, comentó. &ldquo;Pero se negaron y al
              final terminaron pagando más de ocho veces del monto original.
              Esta es la magnitud a lo que las compañías de seguros llegan para
              evitar pagos."
            </em>
          </strong>
        </p>
        <p>
          Pero el abogado Long lucha y gana la buena batalla. Cuando todo fue
          dicho y hecho, la aseguradora Fred Loya Insurance pagó el monto total
          del fallo por $149.661 incluyendo gastos e intereses conforme al
          código civil, articulo 998. En el 2015 solamente, para el mes de julio
          el abogado Long ya había llevado ocho casos a juicio. Tres de ellos
          excedieron el límite de la póliza y obtuvo la  máxima cantidad
          posible  para sus clientes.
        </p>
        <h2>Sus Primeros Años y Educación</h2>
        <p>
          El abogado Long es nativo del condado de Orange. Se recibió de la
          escuela secundaria Servite en la ciudad de Anaheim en la cual fue una
          estrella del fútbol americano. En 1996 se recibió de la Universidad
          del sur de California y en 1999 de la escuela de derecho Whittier. Se
          le otorgó el reconocimiento de jurisprudencia estadounidense por
          lograr las calificaciones más altas en su estudio de agravio y prueba.
          Durante mucho tiempo trabajó con el nacionalmente reconocido abogado
          litigante Herbert Hafif con quien trató todas las fases de preparación
          en casos de litigios y juicios, incluyendo lesiones personales
          catastróficas, litigios comerciales complicados,  reclamos falsos
          federales y mala fe en seguros.
        </p>
        <p>
          <strong>
            {" "}
            Algunos resultados de los casos del abogado Gavin Long{" "}
          </strong>
        </p>
        <div className="panel panel-primary">
          <div className="panel-heading">
            Algunos resultados de los casos del abogado Gavin Long
          </div>
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>
                  <strong>Monto otorgado </strong>
                </th>
                <th>
                  <strong>Tipo de caso</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$3,000,000</td>
                <td>Veredicto de auto v. Peatón</td>
              </tr>
              <tr>
                <td>$1,950,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$1,150,000</td>
                <td>Automóvil v. Peatón</td>
              </tr>
              <tr>
                <td>$1,000,000</td>
                <td>Responsabilidad de premisa</td>
              </tr>
              <tr>
                <td>$600,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$555,000</td>
                <td>Automóvil v. Automó vil</td>
              </tr>
              <tr>
                <td>$500,000</td>
                <td> Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$500,000</td>
                <td> Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$450,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$360,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
              <tr>
                <td>$243,000</td>
                <td>Veredicto del jurado en accidente de motocicleta</td>
              </tr>
              <tr>
                <td>$160,000</td>
                <td>Veredicto del jurado en caso Automovil v. Automóvil</td>
              </tr>
              <tr>
                <td>$155,000</td>
                <td>Veredicto del jurado en caso Automovil v. Automóvil</td>
              </tr>
              <tr>
                <td>$145,000</td>
                <td>Veredicto del jurado en caso Automovil v.Automóvil</td>
              </tr>
              <tr>
                <td>$135,000</td>
                <td>Automóvil v. Automóvil</td>
              </tr>
            </tbody>
          </table>
        </div>
        <h2>Un Miembro Valioso del Equipo</h2>
        <p>
          Brian Chase, socio mayoritario del bufete Bisnar Chase comenta que el
          abogado Long es un miembro valioso de su equipo. &ldquo;Gavin esta
          sumamente motivado y apasionado en lo que hace, sus resultados
          demuestran el calibre de superioridad en su trabajo, en sus
          habilidades y en sus  capacidades. Somos muy afortunados de tenerlo
          como parte del equito de Bisnar Chase&rdquo;.
        </p>
        <h2>El Lado Personal del Abogado Long</h2>
        <p>
          El abogado Long tiene 18 años de estar felizmente casado. Conoció a su
          esposa, Rebekah, cuando ella tenía 19 años de edad y ha estado
          firmemente a su lado durante todo el transcurso de su vida. Rebekah
          continúa siendo la inspiración y mejor amiga del abogado Long – no
          pudo haber pedido mejor pareja en su vida.
        </p>
        <LazyLoad>
          <img
            src="/images/IMG_3083_IJFR.jpg"
            width="100%"
            alt="H. Gavin Long-Abogado Especialista en Lesiones Corporales"
          />
        </LazyLoad>
        <h2>Comuníquese con el Abogado H. Gavin Long</h2>
        <p>
          Para programar una consulta gratis con el abogado Long, por favor
          llame al (949) 537-2832.
        </p>
        <p>
          <em>
            {" "}
            <Link
              to="http://members.calbar.ca.gov/fal/Member/Detail/204034"
              target="_blank"
            >
              {" "}
              Perfil jurídico de Gavin
            </Link>
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  tr {
    width: 100%;
  }

  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
