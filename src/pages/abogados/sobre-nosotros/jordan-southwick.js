// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Jordan Southwick - Abogado de Lesiones Personales en California"
        pageDescription="Jordan desempeña un papel clave en la creciente lista de abogados expertos en lesiones personales de Bisnar Chase.Comuníquese con Jordan para una consulta de caso gratuita llamando al (800) 561-4887 ahora."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Jordan Southwick - Abogado de Lesiones Personales en California</h1>
        <BreadCrumbs location={location} location={location} />
        <LazyLoad>
          <img
            src="../../images/bisnar-chase/jordan-head-shot-spanish.jpg"
            width="237"
            height="349"
            className="imgleft-fixed mb"
            alt="Jordan Southwick - Abogado de Lesiones Personales en California"
          />
        </LazyLoad>
        <p>
          Para Jordan Southwick, la profesión legal es una vocación. Jordan,
          abogado asociado de Bisnar Chase, se enfoca en casos de{" "}
          <Link to="/abogados/lesiones-personales" target="_blank">
            {" "}
            lesiones personales
          </Link>
          , en particular aquellos relacionados con la responsabilidad de las
          instalaciones, muertes injustas y accidentes automovilísticos. Como la
          última línea de defensa para aquellos que han sufrido lesiones y
          pérdidas, le apasiona lograr los resultados correctos para sus
          clientes.{" "}
        </p>
        <h2>Educación y Logros</h2>
        <p>
          Jordan es nativo del sur de California, que crecio y asistio a la
          escuela secundaria en Murrieta. Como aspirante a abogado, obtuvo una
          beca del Colegio de Abogados del Condado de Southwest Riverside y
          asistió a la Escuela de Leyes Chapman en Orange. Jordan se graduó de
          Chapman con un título de abogado en el 2018, antes de aprobar el
          examen del Colegio de Abogados del Estado de California.
        </p>
        <p>
          Mientras se encontraba en Chapman, Jordan obtuvo un prestigioso premio
          CALI a la excelencia en Transacciones de la Fundación como el mejor de
          su clase. También amplió su experiencia legal trabajando en la Clínica
          de Ley de Entretenimiento Avanzado de la escuela. Esto le dio la
          oportunidad de brindar ayuda legal a los productores de películas de
          bajo presupuesto, trabajando para asegurarse de que las películas
          independientes recibieran los acuerdos y las aprobaciones necesarias
          para que fueran iluminadas para la producción.
        </p>
        <h2>Agente de Deportes para Abogado de Lesiones Personales</h2>
        <p>
          Antes de unirse a Bisnar Chase, Jordan trabajó en una agencia de
          deportes donde ayudó a negociar y redactar contratos para estrellas
          deportivas de alto perfil, incluidos jugadores de la NFL y de la MLB.
          Pero le apasionaba pasar al campo de las lesiones personales, donde
          sentía que podía tener un impacto mayor y más positivo en las vidas de
          aquellos que están desesperados por obtener ayuda.{" "}
        </p>
        <p>
          "Estoy centrado en ayudar a las personas que más lo necesitan", dijo.
          &ldquo;Algunos de nuestros clientes han sido gravemente heridos por la
          negligencia, el descuido o las acciones de otros, pero pueden no saber
          a dónde acudir o tener los medios para combatirlo.
        </p>
        <p>
          "Esas son las personas en las que quiero concentrarme. "Es importante
          para mí que obtengan la representación que necesitan y la justicia que
          merecen".
        </p>
        <p>
          Después de pasar por el Colegio de Abogados de California, Jordan se
          unió a Bisnar Chase como asistente legal y fue promovido rápidamente
          para convertirse en abogado asociado y abogado litigante.
        </p>
        <p>
          Añadió: "Se siente muy bien comenzar en Bisnar Chase. Mi padre también
          es abogado, y siempre supe que quería seguir este camino. Sé cómo las
          vidas de las personas pueden verse afectadas por un accidente o
          lesión, y quiero luchar por esas personas ".
        </p>
        <h2>Intereses Personales</h2>
        <p>
          En medio de obtener justicia para sus clientes, Jordan es un gran
          fanático del fitness y ex jugador de hockey. Comenzó a patinar sobre
          hielo cuando tenía solo tres años y pasó a jugar al hockey Junior A en
          California. A Jordan también le gusta mantenerse en forma jugando
          baloncesto y ir al gimnasio.
        </p>
        <h2>Contactando a Jordan Southwick</h2>
        <p>
          Jordan desempeña un papel clave en la creciente lista de abogados
          expertos en lesiones personales de Bisnar Chase. Ofrece servicios
          legales de alta calidad "Sin ganancia, sin cargo", lo que garantiza
          que cada cliente reciba la representación que se merece. Comuníquese
          con Jordan para una consulta de caso gratuita llamando al (800)
          561-4887 ahora.
        </p>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Licensee/Detail/322473"
            target="_blank"
          >
            {" "}
            Ver el perfil de Jordan en el sitio web del Colegio de Abogados de
            California
          </Link>
          .{" "}
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
