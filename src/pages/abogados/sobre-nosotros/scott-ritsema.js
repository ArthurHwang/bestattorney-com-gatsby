// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Scott Ritsema - Abogado de Lesiones Personales - Socio a Bisnar Chase"
        pageDescription="Scott es socio de Bisnar Chase y ha estado en la empresa desde 2008.Él también ve en la firma de abogados cuántos clientes catastróficamente heridos enfrentan importantes desafíos emocionales y financieros.Para programar una consulta gratis por favor llame al 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Scott Ritsema- Socio</h1>
        <BreadCrumbs location={location} location={location} />
        <h2>El Mejor Abogado</h2>
        <LazyLoad>
          <img
            src="/images/scott-ritsema - image.jpg"
            width="275"
            className="imgleft-fixed mb"
            alt="Scott Ritsema"
          />
        </LazyLoad>
        <p>
          Scott Ritsema se convirtió en abogado de lesiones personales en
          California porque quería representar a personas, no a corporaciones.
        </p>

        <p>
          Scott es socio de{" "}
          <Link to="/abogados" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          y ha estado en la empresa desde 2008.
        </p>
        <p>
          El hecho de estar luchando todos los días por las personas con
          lesiones catastróficas para proteger sus derechos y ayudarlos a
          obtener la única forma de justicia que está disponible para ellos es
          la máxima satisfacción para el abogado Ritsema.
        </p>
        <p>
          Durante la primera década de su carrera, Ritsema trabajó como abogado
          defensor en representación de los intereses de grandes corporaciones y
          compañías de seguros. Con el tiempo descubrió que aunque ganar casos
          para ese tipo de clientes le proporcionaba cierta satisfacción
          profesional, obtenía muy poca satisfacción personal.
        </p>
        <p>
          Aunque hizo bien su trabajo, no era un apasionado de sus victorias.
          Por lo tanto, en 2001, decidió que era hora de cambiar de bando y
          empezar a representar a las personas heridas. Su experiencia de más de
          10 años de trabajo de defensa le dio una comprensión única de cómo
          ganar casos contra corporaciones y compañías de seguros.
        </p>
        <h2>Ley De Lesiones Personales -Un Llamado</h2>
        <p>
          La práctica de ley de lesiones personales no es solo un trabajo para
          el abogado Ritsema, es una vocación. Él se dio cuenta de las
          situaciones difíciles en que se encuentran muchos clientes cuando
          acuden a un abogado de lesiones personales.
        </p>

        <p>
          <LazyLoad>
            <img
              src="/images/Scott Small Image.jpg"
              alt="Riverside auto defect lawyer Brian Chase"
              width="168"
              className="imgleft-fixed"
              height="117"
            />
          </LazyLoad>
          <span>Scott Ritsema -</span>
          &ldquo;
          <em>
            Estas son personas que, sin nosotros y sin el sistema de justicia
            civil, no tienen recursos ni remedios. No es exageración cuando digo
            que ponen sus vidas y su futuro financiero en nuestras manos
          </em>
          &rdquo;.
        </p>

        <p>
          Algunos pueden ver eso como mucha presión, pero para el abogado
          Ritsema es un desafío bienvenido. Recientemente, lideró un equipo
          legal de Bisnar Chase que obtuvo un fallo de 30 millones de dólares
          para Lucas Vogt, de 25 años, quien sufrió una lesión cerebral
          catastrófica en un accidente de motocicleta.
        </p>
        <p>
          Para el abogado Ritsema, la satisfacción en estos casos proviene de la
          posibilidad de proporcionarle a la familia los medios para enfrentar
          una lesión catastrófica. La esperanza del abogado Ritsema en este caso
          era que Lucas Vogt pudiera tener la mejor recuperación, la mejor
          calidad de vida bajo sus circunstancias y que pudiera maximizar su
          tiempo con su pequeña hija.
        </p>
        <p>
          Él también ve en la firma de abogados cuántos clientes
          catastróficamente heridos enfrentan importantes desafíos emocionales y
          financieros. No son solo las víctimas, sino también sus familias las
          que sufren consecuencias que cambian la vida después de un evento
          traumático como un accidente catastrófico.
        </p>
        <h2>Vida a Temprana Edad Y Educación</h2>
        <LazyLoad>
          <img
            src="/images/Scott Bio.jpg"
            width="228"
            height="341"
            className="imgright-fixed mb"
            alt="Scott Ritsema"
          />
        </LazyLoad>
        <p>
          Scott Ritsema nació en Michigan, pero creció principalmente en Newport
          Beach. Regresó a Michigan en su adolescencia y se graduó de la
          secundaria Grandville en 1979, entre los mejores de su clase y como
          miembro por dos años de la Sociedad Nacional de Honor. El abogado
          Ritsema obtuvo su licenciatura en psicología de UCLA en 1984 y su
          título de abogado en UC Davis en 1988, graduándose con el 10% superior
          de su clase y recibiendo el prestigioso premio Order of the Coif para
          graduados de leyes. Fue admitido en la barra de abogados de California
          ese mismo año.
        </p>
        <h2>Logros Profesionales</h2>
        <p>
          El abogado Ritsema ha llevado más de 20 juicios a veredicto y llevó a
          cabo más de 50 arbitrajes a conclusión. Él ha manejado más de 25 casos
          con más de 100 millones de dólares en recuperaciones y juicios totales
          para nuestros clientes. Estos son algunos de los resultados de casos
          recientes del abogado Ritsema: Esto puede incluir acuerdos, sentencias
          y veredictos.
        </p>
        <p>
          <strong> Resultados de los casos del abogado Ritsema</strong>
        </p>

        <p>
          <strong> Monto Otorgado    Tipo de caso</strong>
        </p>
        <p>
          $38.650.000         Motocicleta v. Auto $24.700.000         Veredicto
          del jurado - accidente automovilístico - respaldo defectuoso $
          7,000,000          Caso de ingestión cáustica $ 2.000.000         
          Accidente por conducir ebrio $ 1.000.000          Auto v. Peatón $
          1.500.000          Carretera peligrosa: negligencia $
          10.030.000        Negligencia (compañía confidencial) $
          6.850.000          Responsabilidad del producto
        </p>

        <h2>Carrera y Logros</h2>
        <p>
          El abogado Ritsema pertenece a una serie de organizaciones
          profesionales incluyendo la Asociación de Abogados Litigantes del
          Condado de Orange, la Asociación Americana de Abogados de Justicia y
          Abogados del Consumidor de California. El abogado Ritsema fue nombrado
          Mejor Abogado en 2015 por la publicación nacional, BestLawyer. Ha sido
          nombrado SuperLawyer todos los años desde 2011, y nombrado mejor
          abogado cada año desde 2015 por publicaciones que clasifican los
          niveles de habilidades dentro de la profesión legal.
        </p>
        <p>
          Recientemente fue nombrado Abogado del Año para el Estado de
          California por una prestigiosa publicación legal. En 2012, Scott
          Ritsema y Brian Chase fueron nombrados Abogados del Consumidor del Año
          en todo California por la Consumer Attorneys of California (CAOC), una
          asociación profesional para abogados que representan a los demandantes
          que buscan responsabilizar a los infractores.
        </p>
        <LazyLoad>
          <img
            src="/images/Scott Award Image.jpg"
            width="100%"
            className="imgright"
            alt="Scott Ritsema"
          />
        </LazyLoad>
        <p>
          A pesar de su agenda extremadamente ocupada, el abogado Ritsema se
          compromete a retribuir a su comunidad. Apoya activamente a Mothers
          Against Drunk Driving (Madres contra conductores ebrios) y participa
          en Susan G. Komen Walk for the Cure (caminata para la cura) para
          ayudar a crear conciencia y generar fondos para la investigación del
          cáncer de mama.
        </p>

        <h2>Un Miembro Importante De La Familia Bisnar Chase</h2>
        <p>
          El abogado Ritsema dice que valora la oportunidad que Bisnar Chase le
          ha dado de unirse a las &ldquo;grandes ligas&rdquo; de litigios por
          lesiones personales.
        </p>
        <p>
          &ldquo;
          <em>
            <strong>
              {" "}
              Bisnar Chase tiene el mejor entorno de cualquier lugar en el que
              haya trabajado&rdquo;, dice. &ldquo;Esta firma es verdaderamente
              única en ese sentido
            </strong>
          </em>
          &rdquo;.
        </p>
        <p>
          El abogado Ritsema siente que realmente ha encontrado su vocación como
          abogado de lesiones personales en Bisnar Chase. &ldquo;Estoy en
          compañía de personas de ideas afines que tienen la pasión de luchar y
          ganar para aquellos que han sufrido enormes pérdidas en sus vidas.
          Considero que es un honor y un privilegio poder darles a los clientes
          lesionados una voz y luchar por sus derechos&rdquo;.
        </p>
        <p>
          <em>
            {" "}
            <Link
              to="http://members.calbar.ca.gov/fal/Member/Detail/138193"
              target="_blank"
            >
              {" "}
              Perfil jurídico de Scott
            </Link>
          </em>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
