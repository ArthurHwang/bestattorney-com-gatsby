// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import { graphql } from "gatsby"
import Img from "gatsby-image"
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    staffImage: file(relativePath: { regex: "/BC-2019-staff-mini.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 1000) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function AboutUs({ data, location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sobre Nosotros"
        pageDescription="Los abogados de Bisnar Chase representa a personas que han sufrido lesiones graves o que han perdido un miembro de su familia debido a un accidente o un defectuoso. Llama al 877-958-8092 y experimentaras la diferencia que hace la representación apasionada."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>About Bisnar Chase Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Acerca de Bisnar Chase"
            title="Acerca de Bisnar Chase"
            fluid={data.staffImage.childImageSharp.fluid}
          />
        </div>

        <h2>Nuestros Objetivos De Cliente:</h2>
        <p>
          <strong>
            {" "}
            <em>
              "Brindar una representación superior al cliente de una manera
              compasiva y profesional a la vez que hacemos de nuestro un lugar
              más seguro.
            </em>
          </strong>
          <em>"</em>
        </p>

        <p>
          Esta declaración de misión artesanal es la guía de Bisnar Chase sobre
          como te representamos. Establece el servicio superior que puede
          esperar de nosotros. Explica lo que hacemos y por que lo hacemos.
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              &ldquo;proporcionar una representación superior al cliente…&rdquo;{" "}
            </strong>
            significa proporciónale la mas alta calidad de representación en
            nuestra industria. Significa ser determinado en tu nombre. Significa
            ser eficiente para obtener la mayor recompensación que permitan sus
            circunstancias.
          </li>
          <li>
            <strong>
              {" "}
              &ldquo;de una manera compasiva y profesional…&rdquo;{" "}
            </strong>
            significa cumplir sus objetivos de acuerdo con los mas altos valores
            de nuestra industria. Significa representarte con dignidad y clase.
          </li>
          <li>
            <strong>
              {" "}
              &ldquo;mientras haciendo de nuestro mundo un lugar mas
              seguro…&rdquo;{" "}
            </strong>
            significa que el trabajo que hacemos por usted es para el bien
            común. Lo que le sucedió le resultara menos probable a otra persona
            porque se asoció con nosotros para responsabilizar al culpable. Al
            responsabilizar a los culpables apoyamos a las personas a ser
            responsables de sus acciones, apoyamos a los empleados de manera
            justa y apoyamos a los gobiernos a llevar a cabo sus tareas de
            manera adecuada. Al responsabilizar a los culpables habrá menos
            maldades y nuestro mundo será un lugar más seguro para todos
            nosotros. <strong> </strong>
          </li>
        </ul>
        <h2>¿Nuestra Diferencia?</h2>
        <p>
          Además de cientos de millones de dólares recuperados para miles de
          clientes satisfechos, varios premios &ldquo;Abogado del ano&rdquo;,
          cuarto &ldquo;Premios mejores lugares para trabajar&rdquo; y un premio
          &ldquo; Héroe comunitario&rdquo;, lo que nos hace diferentes de la
          otra gran lesión personal bufetes de abogados?
        </p>
        <p>
          <strong>
            {" "}
            Representación Legal Apasionada es lo que nos hace diferentes
          </strong>
        </p>
        <p>
          La representación apasionada de personas como usted es lo que nos hace
          diferente. Desde su primera llamada telefónica, sentirá la pasión que
          nuestro equipo tiene por cuidarlo y por ganar su caso. Sentirás que
          somos el equipo sonado legal.
        </p>

        <LazyLoad>
          <img
            src="/images/bisnar-chase/brian-speaking-with-past-clients.jpg"
            width="100%"
            alt="Mas informacion de los abogados de Bisnar Chase"
          />
        </LazyLoad>
        <ul>
          <li>
            <strong> La pasión </strong>es ganar tu caso cuando otros dijeron
            que no se podía hacer.
          </li>
          <li>
            <strong> La pasión </strong>es convencer a los ajustadores de
            seguros o al jurado de que siempre has tiendo razón.
          </li>
          <li>
            <strong> La pasión </strong>es recolectar cada centavo que tu caso,
            la ley y las circunstancias permiten.
          </li>
        </ul>
        <p>
          Lo que no puede distinguí hasta que nos llama o se encuentra con
          nosotros es como nos ocupamos de nuestros clientes. Te sorprenderá el
          nivel de servicio personal y profesionalismo que experimentas. Se
          sorprenderá de lo mucho que puede tranquilizar su mente cuando se
          sienta cómodo y confíe en su equipo legal.
        </p>
        <p>
          <strong>
            {" "}
            Llámanos. Compruébalo por ti mismo. Inmediatamente experimentaras la
            diferencia que hace la representación apasionada.
          </strong>
        </p>
        <h2>
          A Quienes Representamos – Somos Abogados De Los Abogados Demandante
        </h2>
        <p>
          Representamos a personas que han sufrido lesiones graves o que han
          perdido un miembro de su familia debido a un accidente o un
          defectuoso.
        </p>
        <p>
          También representamos a las que se les han negado los derechos
          labóreles.
        </p>
        <h2>Como Representamos</h2>
        <p>
          Nuestro bufete de abogados esta formado por seis abogados divididos en
          cuatro equipos de litigación. Cada equipo de litigio esta dirigido por
          un abogado experimentado en juicios por lesiones personales e incluye
          una combinación de asistentes jurídicos y asistentes legales.
        </p>
        <p>
          <strong>
            {" "}
            Brian Chase es nuestro principal litigante y ganador de varios
            premios de Abogado del Ano, y administra los equipos de litigio
          </strong>
          .
        </p>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/brian-john-image-re-done.jpg"
            width="100%"
            alt="Los Abogados de lesiones personales de Bisnar Case"
          />
        </LazyLoad>
        <h2>La Historia de Bisnar Chase</h2>

        <p>
          <strong> ¿De dónde viene nuestra &ldquo;pasión&rdquo;?</strong>
        </p>
        <p>
          John Bisnar comenzó este bufete de abogados recién salido de la
          facultad de derecho en 1978. John ingreso a la facultad de derecho
          creyendo que una educación legal le serviría mejor en une carrera de
          negocios que obtener otro título.
        </p>
        <p>
          En su primer mes de la facultad de derecho fue herido de gravedad en
          una colisión de trafico por un conductor negligente. El abogado que
          represento al Señor Bisnar, uno de sus profesores de derecho, hizo un
          mal trabajo representándolo. A partir de esa experiencia, John
          aprendió lo que era necesitar y depender de un abogado para obtener
          asistencia profesional de calidad y no obtenerla. El profesor era un
          famoso abogado de lesiones personales, pero John sabia que podía hacer
          un mejor trabajo para los clientes.
        </p>
        <p>
          Debido a esa experiencia, el Señor Bisnar juro que sus clientes
          obtendrían la calidad del servicio profesional que desearía haber
          recibido. Con ese voto, el Señor Bisnar fundo Bisnar & Asociaste, una
          empresa que funciono con éxito durante veinte anos.
        </p>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/john-shaking-hand-client.jpg"
            width="100%"
            alt="Acerca de Bisnar Chase Abogados de Lesiones Personales"
          />
        </LazyLoad>
        <p>
          convirtió en Bisnar Chase, con Brian Chase liderando el equipo de
          litigios y Bisnar enfocándose en las necesidades de nuestros clientes,
          asegurando que cumplimos con lo prometido. Hoy atendemos a un par de
          cientos de clientes a la vez. Y trabajamos para cumplir con voto todos
          lo días.
        </p>
        <p>
          Bisnar Chase ha sido nombrado uno de los &ldquo;mejores lugares para
          trabajar&rdquo; por el Orange County Busniess Journal cada ano desde
          2012. Lo que eso significa es que el equipo de Bisnar Chase tiene un
          excelente ambiente de trabajo, y esta muy comprometido y motivado. La
          creencia del Señor Bisnar es que un equipo de empleados bien atendido,
          altamente comprometido y motivado que trabaje en un ambiente de equipo
          cooperativo resultara en una representación superior y grandes
          resultados para los clientes.
        </p>
        <p>
          Nombrada como una de las tres firmas de abogados en Newport Beach como
          los mejores abogados de lesiones personales como se ve en
          ThreeBestRated.com.  Con un estricto proceso de investigación, el
          directorio solo enumera los tres mejores interpretes en una cuidad
          determinada en función de sus opiniones, reputación, atención a los
          detalles y satisfacción general del cliente. Bisnar Chase ha entregado
          constantemente resultados excepcionales durante mas de cuatro décadas.
        </p>
        <p>¡Pruébanos y compruébelo usted mismo!</p>
        <p>
          <strong> Llama al 877-958-8092</strong>
        </p>

        <p>1301 Dove St. #120 Newport Beach, CA. 92660 Tel: 949-203-3814</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .banner-image {
    margin-bottom: 2rem;
    min-height: 200px;
  }

  @media (max-width: 1024px) {
    .address-bottom {
      text-align: center;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
