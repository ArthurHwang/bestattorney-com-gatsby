// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutEspanolDefault } from "src/components/layouts/Layout_Espanol_Default"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="John Bisnar - Abogado de Lesiones Personales"
        pageDescription="John Bisnar es el fundador de una de las principales firmas de abogados de lesiones personales de California, Bisnar Chase Personal Injury Attorneys. Avvo.com, el servicio de calificación de abogados en línea más conocido, calificó al abogado Bisnar con “un 10 perfecto."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>John Bisnar - Abogado de Lesiones Personales</h1>
        <BreadCrumbs location={location} location={location} />
        <LazyLoad>
          <img
            src="/images/John-Bisnar-Bio-Image.jpg"
            width="300"
            className="imgleft-fixed mb"
            alt="John Bisnar"
          />
        </LazyLoad>
        <p>
          John Bisnar es el fundador de una de las principales firmas de
          abogados de lesiones personales de California,
          <strong>
            {" "}
            <Link to="/abogados" target="_blank">
              {" "}
              Bisnar Chase Personal Injury Attorneys
            </Link>
          </strong>
          , cuyas raíces se remontan a Bisnar & Associates, la cual fundó en
          1978.
        </p>
        <p></p>

        <h2>
          Haciendo lo Correct
          <p>por los Clientes</p>
        </h2>

        <p>
          El abogado Bisnar fundó su propio bufete de abogados de lesiones
          personales bajo la premisa de "hacer lo correcto por los clientes y
          las ganancias seguirán" después de tener una terrible experiencia como
          cliente de lesiones personales después de su propio accidente serio
          mientras estudiaba en la escuela de leyes.
        </p>
        <p>
          Dicha experiencia le mostró cómo se siente necesitar una guía
          compasiva y una representación enérgica y no obtener ninguna de las
          dos de su propio abogado de lesiones personales.
        </p>
        <h2>Representación Superior al Cliente</h2>
        <p>
          Debido a su terrible experiencia como cliente, Bisnar juró que sus
          clientes tendrían la experiencia que él deseó tener cuando fue
          cliente.
        </p>
        <p>
          Bisnar Premise y Bisnar Chase Personal Injury Attorneys (Abogados de
          Lesiones Personales Bisnar Chase), más de diez mil clientes de
          lesiones personales han recuperado cientos de millones de dólares al
          ser representados por el abogado Bisnar y su bufete. Él,
          personalmente, asume la responsabilidad de cada cliente y su caso.
        </p>
        <h2>Ganador del Premio Mejor Abogado  del Condado de Orange</h2>
        <LazyLoad>
          <img
            src="/images/john-bisnar-top-attorney-2013a.jpg"
            width="150"
            height="218"
            className="imgleft-fixed mb"
            alt=""
          />
        </LazyLoad>
        <p>
          John Bisnar también ha sido galardonado con el premio a los mejores
          abogados del Condado de Orange, publicado en el Orange Coast Magazine.
        </p>
        <p>
          John y Brian ambos han ganado el prestigioso premio Mejor Abogado del
          condado de Orange por seis años consecutivos.
        </p>
        <h2>
          Nativo del Sur de California, Veterano Militar y Graduado con Honores
        </h2>
        <LazyLoad>
          <img
            className="imgleft-fixed"
            src="/images/attorney-bisnar.jpg"
            alt="Abogado de Lesiones Personales,John Bisnar"
          />
        </LazyLoad>
        <p>
          El abogado Bisnar es originario del sur de California. Sirvió como
          miembro activo en el ejército de Estados Unidos en el Pacífico y el
          sudeste de Asia de 1968 a 1970. En 1974 se graduó con honores de la
          Universidad Estatal de California, Long Beach, con una licenciatura en
          Finanzas e Inversiones. John formaba parte de la Lista del Decano cada
          semestre. En 1978, John recibió su título de abogado en la Facultad de
          Derecho de la Universidad Pepperdine y fue admitido en la Barra de
          Abogados del Estado de California dos meses después, poco después de
          graduarse en el Colegio de Abogados Litigantes de la universidad
          Pepperdine.
        </p>

        <p>
          <span>Bisnar Chase </span>Declaración Misión: <span>"</span>Brindar
          una representación superior al cliente de una manera compasiva y
          profesional, mientras experimenta una gran satisfacción laboral y
          hacemos de nuestro mundo un lugar más seguro"
        </p>

        <h2>
          <span>
            Un &ldquo;10&rdquo; Perfecto en Calificación como Abogado{" "}
          </span>
        </h2>
        <p>
          Avvo.com, el servicio de calificación de abogados en línea más
          conocido, calificó al abogado Bisnar con &ldquo;un 10 perfecto" y
          "Excelente" basado en su historial, años de experiencia como abogado
          de lesiones personales, trayectoria exitosa, reputación impecable con
          clientes y compañeros, y logros profesionales. Además de ser nombrado
          un Súper Abogado, también recibió el Premio al Logro Sobresaliente por
          WeTip, un destacado servicio anónimo de denuncia de delitos y defensa
          de la aplicación de la ley.
        </p>
        <h2>
          <span>Enseña Atención al Cliente a Otros Abogados</span>
        </h2>
        <LazyLoad>
          <img
            src="/images/personalinjurybooksm.gif"
            width="150"
            height="239"
            className="imgright-fixed"
            alt="John Bisnar, Abogado de Lesiones Personales"
          />
        </LazyLoad>
        <p>
          El abogado Bisnar es conferencista frecuente en programas de educación
          continua para abogados, enseñándoles el manejo de la oficina de
          abogados y técnicas de atención al cliente. La Asociación de Abogados
          de California le pidió a Bisnar que escribiera partes del libro para
          abogados, "La Guía de California para abrir y administrar un bufete de
          abogados". Es autor de "
          <strong>
            {" "}
            Los siete errores fatales que pueden arruinar su reclamo de lesiones
            personales en California
          </strong>
          ", un libro para ayudar a los que no son abogados a manejar sus
          propios reclamos por lesiones personales.
        </p>
        <p>
          El abogado Bisnar es un ex miembro de la Junta Directiva y la Junta de
          Fideicomisarios de varias empresas, organizaciones sin fines de lucro
          y religiosas. Ha sido miembro activo de dos grupos de apoyo para
          hombres desde 1996: uno está dedicado al crecimiento personal y el
          otro al crecimiento del negocio.
        </p>
        <p>
          John Bisnar abogado de lesiones personales 1301 Dove St. # 120 Newport
          Beach, CA 92660 (949) 446-1783{" "}
          <Link to="http://members.calbar.ca.gov/fal/Member/Detail/80894">
            Perfil de John en la Asociación del Estado
          </Link>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/7ndkwhGi8i8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutEspanolDefault>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
