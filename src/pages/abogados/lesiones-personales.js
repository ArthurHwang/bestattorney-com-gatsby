// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "ambulance-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lesiones personales en el condado de Orange - Bisnar Chase"
        pageDescription="Si ha sido lesionado a causa de la negligencia de alguien, comuníquese con la firma Bisnar Chase Personal Injury Attorneys y programe una consulta gratuita!"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Somos evaluados como los mejores abogados de lesiones personales
        </h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Abogados especialistas en lesiones personales"
            title="Abogados especialistas en lesiones personales"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Sabemos que contratar un{" "}
          <strong> Abogado de Lesiones Personales</strong> puede ser una
          decisión seria, pero estamos aquí para facilitarle el proceso.
          Nuestros clientes son la vanguardia de nuestra práctica jurídica por
          eso nuestros especialistas se aseguran de que cada uno se sienta
          escuchado, entendido y fortalecido.
        </p>
        <p>
          <strong>
            {" "}
            Nuestro trabajo es ser litigantes agresivos en los juzgados pero
            oyentes compasivos con nuestros clientes
          </strong>
          . Estamos <strong> aquí para pelear por sus derechos</strong> y
          brindarle la justicia que se merece, ya sea en un accidente
          automovilístico, una muerte por negligencia o un ataque grave de un
          perro.
        </p>
        <p>
          <strong>
            {" "}
            Llámenos al 877-958-8092 para ver cómo le podemos ayudar a obtener
            la compensación que se merece.
          </strong>
          <strong> </strong>
        </p>
        <h2>Los 5 Tipos de casos de lesiones personales</h2>
        <p>
          <strong> Bisnar Chase acepta los siguientes casos de lesiones</strong>
          :
        </p>
        <p>
          1. <strong> Accidentes automovilísticos</strong>: Los accidentes
          automovilísticos son una de las causas más frecuentes de lesiones y
          muerte en el país. Es por eso que el seguro de responsabilidad civil
          es obligatorio en casi todos los estados del país para los
          automovilistas.
        </p>

        <p>
          Cuando se producen estas lesiones y las compañías de seguros{" "}
          <strong>
            {" "}
            no quieren pagar por los errores que cometió el conductor causante
          </strong>
          , es necesario que intervenga un abogado para asegurarse de que la
          compañía de seguros no intente pagarle lo menos posible.
        </p>
        <p>
          2.
          <strong>
            {" "}
            <Link to="/abogados/defectos-automovilisticos">
              Defectos en los autos
            </Link>
          </strong>
          : Los defectos en los autos son los casos en los que el fabricante no
          fabrica el auto con todas las características de seguridad que podría
          incluir, y sabe que el auto tiene fallas en la seguridad. Esta actitud
          de privilegiar las ganancias por sobre la seguridad del cliente se
          considera negligencia grave y se pueden iniciar acciones judiciales
          contra los fabricantes de autos para cobrar una indemnización por
          daños para la víctima o su familia en el caso de lesiones
          catastróficas o muerte.
        </p>
        <p>
          3. <strong> Defectos en productos</strong>: Los productos que se
          comercializan deben adherir a ciertas normas de seguridad y no
          presentar peligros para el consumidor. Cuando un producto se vuelve
          involuntariamente peligroso para el usuario al ser utilizado
          correctamente, los fabricantes de ese producto son responsables de
          crear un producto peligroso.
        </p>
        <p>
          Si ha resultado lesionado por un producto defectuoso, merece ser
          indemnizado por sus gastos médicos y por el daño moral que ha
          experimentado. Algunos ejemplos de productos defectuosos que existen
          hoy son los cigarrillos electrónicos que pueden explotar en la cara de
          los usuarios o en sus bolsillos causando quemaduras de tercer grado.
        </p>
        <p>
          4.{" "}
          <Link to="/abogados/mordeduras-de-perro" target="_blank">
            {" "}
            <strong> Mordeduras de perros</strong>
          </Link>
          : La mayor parte de las mordeduras de perros no son graves y no
          requieren una atención médica muy intensa. Sin embargo, cuando se
          trata de perros más grandes y más agresivos, sus mordeduras pueden
          lastimar mucho y lesionar gravemente a sus víctimas e incluso,
          matarlas. En situaciones como esas, se considera que el propietario
          del perro es el que está en falta y las mordeduras estarán cubiertas
          por cualquier seguro del hogar o del alquiler que el propietario
          tenga.
        </p>

        <p>
          Los propietarios de perros tienen{" "}
          <Link
            to="https://www.iii.org/es/article/responsabilidad-civil-en-el-caso-de-mordeduras-de-perros"
            target="_blank"
          >
            {" "}
            la responsabilidad
          </Link>{" "}
          de entrenar a sus perros para que no muerdan, o de mantenerlos
          alejados de otras personas, si el perro tiene un temperamento
          agresivo. Si un perro ataca a una persona, el abogado especialista en
          lesiones personales le ayudará a obtener una indemnización adecuada
          por parte de la compañía de seguros y demandará por daños al
          propietario del perro, si no tiene seguro.
        </p>
        <p>
          5.{" "}
          <strong>
            {" "}
            Responsabilidad civil en propiedades y establecimientos
          </strong>
          : Las lesiones que se ocasionan en la propiedad de otra persona como
          resultado de la negligencia de alguien se clasifican como lesiones de
          responsabilidad civil. Cubren resbalones y caídas cuando un
          establecimiento debería haber puesto una advertencia acerca del riesgo
          de resbalones, o lesiones por crímenes violentos donde debería haber
          habido seguridad o iluminación más adecuadas.
        </p>
        <p>
          También pueden incluir muertes por ahogamiento si un lugar con agua no
          fue cercado o cerrado al acceso. Las víctimas en estos casos deben
          encontrar la forma de probar que el propietario o gerente de la
          propiedad sabía de las condiciones de peligro y no hizo nada para
          evitar las lesiones a las personas en su propiedad.
        </p>

        <LazyLoad>
          <img
            src="/images/images/Personal Injury Claim Form Tiny JPG.jpg"
            width="100%"
            alt="Somos evaluados como los mejores abogados de lesiones personales"
          />
        </LazyLoad>
        <h2>Asegúrese Cuánto Tiempo Tiene Para Hacer un Reclamo</h2>
        <p>
          Tenga en cuenta que las leyes limitan el plazo para que las víctimas
          sean compensadas en cubrir sus gastos, tales como:
        </p>
        <ul>
          <li>Facturas médicas</li>
          <li>Pérdida de sueldo</li>
          <li>
            Servicios de rehabilitación vocacional o por los daños físicos y{" "}
            <Link
              to="http://uwmsktc.washington.edu/sites/uwmsktc/files/files/TBI-emotional_Sp.pdf"
              target="_blank"
            >
              {" "}
              problemas emocionales
            </Link>{" "}
            que le hayan causado las lesiones corporales
          </li>
        </ul>
        <p>
          <strong>
            {" "}
            En algunas ocasiones ese plazo se limita a 180 días, en otras, se
            extiende hasta &nbsp;dos años o más
          </strong>
          . En todo caso, entre más rápido interponga la demanda más rápido se
          solucionará su caso.
        </p>
        <h2>Preguntas Comunes Relacionadas a Casos de Lesiones Corporales </h2>
        <p>
          Unos de los grandes errores que observamos en las personas es que se
          apresuran a llegar a un acuerdo en su caso porque creen que el
          ajustador de seguros está de su parte. Sin embargo, la industria de
          seguros es una organización lucrativa y quiere llegar a un acuerdo de
          inmediato, que les ahorra miles de dólares en compensaciones.
        </p>
        <p>
          Lo último que quiere un ajustador de la parte involucrada, es que la
          víctima conozca sus derechos o que sea representado por un abogado de
          lesiones corporales, que esté capacitado para enfrentarse a la
          industria de seguros, a los fabricantes de automóviles y a las partes
          culpables.
        </p>
        <ul>
          <li>¿El ajustador está a mi favor?</li>
          <li>
            ¿Sería prudente apresurar el acuerdo de la demanda de lesiones
            corporales?
          </li>
          <li>¿Debería firmar un derecho de retención médico?</li>
          <li>¿Debería dejar que mi abogado imponga mi tratamiento médico?</li>
          <li>¿Qué valor tiene conservar un registro de daños y perjuicios?</li>
          <li>
            ¿Cómo debo prepararme para interponer una demanda por daños
            corporales?
          </li>
          <li>¿Qué debo saber para negociar mi caso?</li>
          <li>
            ¿Cuándo debería hacerme cargo de mi propio reclamo por daños
            corporales?
          </li>
          <li>
            ¿Debería conservar pruebas en la demanda por daños corporales?
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Letrado de lesiones personales"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-los-angeles.jpg')"
            }}
          >
            <h2>
              La Satisfacción de Nuestros Clientes es Nuestra Principal
              Prioridad
            </h2>
          </div>
        </LazyLoad>
        <p>
          Muchos de nuestros casos son recomendados, lo cual indica que estamos
          llenando la satisfacción del cliente una y otra vez.  Los abogados
          especializados en lesiones corporales de
          <strong>
            {" "}
            Bisnar Chase han representado a más de 10.000 clientes con un 96% de
            éxito desde 1978
          </strong>
          .
        </p>
        <p>
          <strong>
            {" "}
            Estamos listos para atenderle con una consulta privada gratis y sin
            compromiso.{" "}
          </strong>
        </p>
        <p>
          Comuníquese ya con los abogados de daños corporales de Bisnar Chase
          para averiguar qué podemos hacer por usted.
          <strong>
            {" "}
            Llame para hacer una consulta gratuita y sin obligación al
            949-537-2651.
          </strong>
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
            Beach, CA 92660
          </span>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
