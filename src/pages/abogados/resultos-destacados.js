// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../components/elements/SEO"
import { LayoutEspanolDefault } from "../../components/layouts/Layout_Espanol_Default"
import folderOptions from "./folder-options"
import { Link } from "src/components/elements/Link"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/spanish-premise-liabilty.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ location, data }) {
  return (
    <LayoutEspanolDefault sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Casos destacados - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Los casos que Bisnar Chase ha ganado asentamientos o sentencias por un da�o personal destacado."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Resultados Destacados</h1>
        <BreadCrumbs location={location} />
        <div className="algolia-search-text">
          <h3>
            La firma legal Bisnar Chase Personal Injury Attorneys est&aacute;
            dedicada a cada uno de sus clientes, quienes reciben absolutamente
            la mejor representaci&oacute;n legal posible.
          </h3>

          <p>
            Los casos publicados abajo son una muestra de los resultados
            conseguidos por nuestros clientes. Esta es una lista parcial y no
            constituye una promesa de ninguna clase. Por favor recuerde que
            estos casos y los resultados logrados dependen de varios factores y
            los resultados pueden diferir de un caso a otro dependiendo de las
            circunstancias particulares de cada uno.
          </p>
        </div>

        <div id="case-list">
          <div className="case-divider"></div>

          <div id="case1" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/motorcycle-vogt.jpg"
                    alt="Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">Vogt v. Saldate</div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Lucas Vogt v. Andrew Saldate</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Motorcycle Accident
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> July 25, 2011
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>2 autos chocan contra una
                    motocicleta en la carretera
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $30,000,000
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/lucas-vogt" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case2" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/seatback-dummy-romine.jpg"
                    alt="Romine Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  A dummy in a seatback failure test for Romine's case
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Romine v. Johnson Controls</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Seatback Failure Auto
                  Defect
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> October 21, 2006
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Colisión por la parte trasera, el
                    respaldo del asiento se rompe provocando cuadriplejia
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $24,744,764+
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/jaklin-romine" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case3" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/christopher-chan-interview.jpg"
                    alt="christopher chan Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Christopher Chan suffered a brain injury after being hit by a
                  bike.
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Christopher Chan vs City of Hanford</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Bicycle Accident
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> May 31, 2005
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Colisión de una furgoneta con un
                    ciclista, intersección peligrosa creada por la ciudad
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $16,444,904
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/christopher-chan" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case4" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/blue-cleaning-maciel.jpg"
                    alt=""
                  />
                </div>

                <div className="case-caption gutter">
                  Olivier Maciel was 2 years old when he drank a cleaning agent
                  that was left out on a patio.
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Maciel v. Defendant Cleaning Corporation</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Premises Liability
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> November 10, 2008
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Un niño bebió detergente líquido
                    que se había dejado abandonado en un patio.
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $10,030,000
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/olivier-maciel" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case5" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/flowers-gonzales.jpg"
                    alt="simon doe Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Lucia Doe was fatally injured when her car rolled over after
                  being struck from behind.
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Simon Doe et al. vs. [Defendant Corporation]</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Auto Defect
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> July 16, 2006
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Vehículo arrollado tras una
                    colisión por la parte trasera y falla de los cinturones de
                    seguridad.
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $8,500,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case6" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/eucalyptus-tree-chairez.jpg"
                    alt="veronica chairez Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Veronica Chairez was rendered a paraplegic when she was struck
                  by a falling branch.
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Veronica Chairez v. The State of California</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Premises Negligence
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> July 26, 1998
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Víctima golpeada por la caída de
                    una rama que el estado no había inspeccionado.
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $8,250,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case7" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/car-door-griselda.jpg"
                    alt="jane doe Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Jane Doe was thrown from her car due to a defective door latch
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Jane Doe vs. [Defendant Corporation]</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Defective Door Latch
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> October 6, 2013
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>La víctima sale expulsada del
                    vehículo cuando una puerta defectuosa del auto se abrió
                    durante una colisión
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $5,500,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case8" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/dog-in-road-butenhoff.jpg"
                    alt="butenhoff Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Joseph Butenhoff was trying to protect a husky who had been
                  hit by a car, when he was struck by a speeding driver
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Joseph Butenhoff v. Bautista</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Pedestrian Accident
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> December 3, 2012
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Víctima arrollada por un vehículo
                    que circulaba a un exceso de velocidad mientras trataba de
                    ayudar a un perro en la carretera
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $3,000,000
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link
                    to="http://www.prlog.org/12484570-bisnar-chase-secures-3-million-jury-verdict-for-client-with-traumatic-brain-injury"
                    target="_blank"
                  >
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case9" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/azusa-trolley-farris.jpg"
                    alt="julie farris Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Julie Farris was hit by a trolley at APU
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Julie Farris vs. City of Azusa</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Trolley vs Bicycle
                  Accident
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> February 19, 2008
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Víctima arrollada por un tranvía
                    mientras circulaba en bicicleta
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $3,000,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case10" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/motorcycle-prock.jpg"
                    alt="rickey prock Resultados Destacados "
                  />
                </div>

                <div className="case-caption gutter">
                  Rickey Prock was hit by a delivery driver who was on the job
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Rickey Prock v. A. Levy and J. Zentner Corporation</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Motorcycle Accident
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> July 7, 1993
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Motociclista arrollado por un
                    conductor de reparto mientras desempeñaba su trabajo
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $3,000,000
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/rickey-prock" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case11" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/broken-wall-valentine.jpg"
                    alt="greg valentine Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Greg Valentine was injured when an improperly built wall fell
                  on him
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Greg Valentine vs. Jeffrey Stoddard</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Premises Liability
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> December 30, 1998
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Un muro mal construido cayó sobre
                    la víctima
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $2,815,958
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case12" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/guard-rail-hernandez.jpg"
                    alt="hernandez Resultados Destacados "
                  />
                </div>

                <div className="case-caption gutter">
                  Kathleen Hernandez was rendered paraplegic in a car accident
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Hernandez v. Department of Transportation</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Poor Road
                  Design/Premises
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> September 10, 1999
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>La falta de guardarraíl en una
                    carretera provocó numerosas lesiones durante el choque
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $2,800,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case21" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/red-light-nehrlich.jpg"
                    alt=""
                  />
                </div>

                <div className="case-caption gutter">
                  Mary Nehrlich was unable to receive disability benefits from
                  her own insurance company who she worked for
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Mary Nehrlich v. [Defendant Insurance Company]</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Insurance Bad Faith
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> October 18, 1995
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>El seguro se negó a pagar la
                    reclamación de discapacidad de la víctima
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $500,000+
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/mary-nehrlich" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case24" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/stop-sign-mai.jpg"
                    alt="mai doe Resultados Destacados"
                  />
                </div>

                <div className="case-caption gutter">
                  Mai was ejected from her car during a rollover
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Mai Doe vs. [Defendant Corporation]</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Defective Seatbelt
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> May 31, 2014
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>El cinturón de seguridad se abrió
                    durante un choque
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $300,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case25" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/crossing-street-villamor.jpg"
                    alt="maria silva Resultados Destacados "
                  />
                </div>

                <div className="case-caption gutter">
                  Raul Silva was killed in a DUI
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Maria Silva vs Mark Levander</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> DUI Pedestrian Hit
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> January 17, 2015
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>Viandante arrollado por conductor
                    bajo los efectos del alcohol.
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> $100,000
                </h2>
              </div>
            </div>
          </div>

          <div className="case-divider"></div>

          <div id="case26" className="case-container">
            <div className="case-top border-box row">
              <div className="case-picture-container col-md-4 col-sm-4 col-xs-4 border-box no-gutter ">
                <div className="case-picture">
                  <img
                    width="100%"
                    src="/images/case-results/seatbelts-rojas.jpg"
                    alt="felipe rojas Resultados Destacados "
                  />
                </div>

                <div className="case-caption gutter">
                  Felipe Rojas lost his wife and sustained extensive injuries in
                  a DUI accident
                </div>
              </div>

              <div className="case-summary-container col-md-8 col-sm-8 col-xs-8 border-box no-gutter">
                <div className="case-header border-box">
                  <h2>Felipe Rojas v. General Motors</h2>
                </div>

                <div className="case-type border-box gutter">
                  <strong> Tipo de accidente: </strong> Defective Seatbelt
                  <p className="result-date">
                    <strong> Fecha del incidente: </strong> October 24, 2003
                  </p>
                  <p className="result-summary">
                    <strong> Resumen: </strong>El cinturón de seguridad de la
                    víctima se rompió y salió expulsado del auto
                  </p>
                </div>
              </div>

              <div className="clearfix"></div>
            </div>

            <div className="case-bottom border-box">
              <div className="case-award">
                <h2>
                  <strong> La sentencia:</strong> [Confidential]
                </h2>
                <p className="case-full-story">
                  {" "}
                  <Link to="/case-results/felipe-rojas" target="_blank">
                    <span className="nomobile">
                      Lee la historia (Inglés){" "}
                      <i className="fa fa-chevron-circle-right"></i>
                    </span>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutEspanolDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .case-container {
    border: 1px solid grey;
    margin-bottom: 2rem;
    padding: 2rem;
    padding-bottom: 0;
    background-color: #eee;
  }

  .case-top {
    display: grid;
    grid-template-columns: 1.6fr 5fr;
    grid-gap: 1rem;

    img {
      margin-bottom: 0;
    }
  }

  .case-bottom {
    .case-award {
      h2 {
        border-bottom: none;
      }
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
