// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "wrongful-death/Tustin wrongful death banner image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Wrongful Death Lawyer - Orange County, CA"
        pageDescription="If you experienced the death of a family member due to a third party's carelessness contact the Tustin Wrongful Death Attorneys of Bisnar Chase. Our experienced injury lawyers will be with you every step of the way fighting for compensation in your wrongful death case. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Wrongful Death Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Wrongful Death Attorney"
            title="Tustin Wrongful Death Attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/wrongful-death" target="_blank">
              {" "}
              Wrongful Death Attorneys
            </Link>
          </strong>{" "}
          of Bisnar Chase know that having to make the decisions after your
          loved one's death is a difficult burden to bear. After the death of a
          close friend or family member, many suffer from extreme depression and
          anxiety trying to cope with their loss while taking care of their new
          responsibilities. Since <strong> 1978</strong>, our lawyers have been
          fighting to relieve some of the pain and suffering that goes along
          with losing a close relative. With a{" "}
          <strong> 96% success rate</strong>, our legal representatives have won
          over <strong> $500 Million</strong> in compensation for the families
          of the deceased. If the death of your loved one could have been
          prevented and you are seeking to hold negligent parties accountable
          please <strong> call 949-203-3814</strong> to speak with a
          <strong> Tustin Wrongful Death Lawyer</strong>. Upon your call, you
          will receive a <strong> free case analysis</strong> as well. We are
          passionate about helping families hold wrongdoers responsible for the
          death of their loved ones. <strong> Contact us today.</strong>
        </p>
        <h2>What Is a Wrongful Death?</h2>
        <p>
          A wrongful death suit can be brought to the courts if the living
          relatives claim that the death of their loved one could have been
          avoided. Wrongful death cases give family members the opportunity to
          seek justice and hold those responsible for their loved one's death.
          California is known to be one of the few states with the highest
          amount of accidental deaths. Bisnar Chase's
          <strong> Tustin Wrongful Death Attorneys</strong> have over{" "}
          <strong> 40 years</strong> of experience with wrongful death suits and
          know what the families of the deceased deserve. The following is what
          you can claim as damages in a wrongful death suit.
          <strong>
            {" "}
            Types of Damages You Can Be Compensated for in a Wrongful Death Suit
          </strong>
          :
        </p>
        <li>Funeral and medical costs</li>

        <li>Loss of future earnings of the deceased</li>

        <li>Loss of coverage or benefits that the deceased was providing</li>

        <li>Mental and emotional pain and suffering</li>

        <li>The loss of an inheritance</li>

        <h2>Who Can File a Wrongful Death Claim?</h2>
        <p>
          Before hiring a Tustin Wrongful Death lawyer it must be determined
          that the person making a wrongful death claim is deserving of
          compensation. Children and spouses are not usually the only claimants
          of a wrongful death.
        </p>
        <p>
          According to{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=377.60.&lawCode=CCP"
            target="_blank"
          >
            {" "}
            Civil Procedure section 377.60
          </Link>{" "}
          the following individuals can bring forth a wrongful death case:
        </p>
        <li>Children of the deceased</li>

        <li>A living spouse</li>

        <li>
          Domestic Partner( the partner and the deceased needed to be legally
          registered in a domestic partnership)
        </li>

        <li>
          Minors who have been dependent of the deceased for more than 180 days
        </li>

        <li>Punitive spouse( a former spouse)</li>
        <LazyLoad>
          <img
            src="/images/wrongful-death/Tustin wrongful death paperwork Tiny JPG.jpg"
            width="100%"
            alt="Wrongful death lawyers in Tustin"
          />
        </LazyLoad>
        <h2>How Do You Prove a Wrongful Death?</h2>
        <p>
          Damages that can be compensated for in a wrongful death suit are
          different from those in an average negligence suit. The plaintiff must
          provide a specific type of proof for the claim. There are{" "}
          <Link
            to="http://www.bsfdea.com/elements-of-wrongful-death/"
            target="_blank"
          >
            {" "}
            four elements
          </Link>{" "}
          that a family member or domestic partner must prove in a wrongful
          death case.
        </p>
        <p>
          <strong> 4 Elements to Prove in a Wrongful Death Case</strong>
        </p>
        <p>
          1.<strong> Negligence</strong>: The family of the deceased must prove
          that their loved one died out of the employers or third parties
          carelessness. 2. <strong> Duty</strong>: First, the family needs to
          prove that it was the duty of the negligent party to care for the
          deceased. For example, if a doctor does not perform to the best of
          his/her ability to maintain the health of a patient, this is seen as a
          carelessness. The plaintiff must prove that the third party abandoned
          their duty as a health provider. 3. <strong> Causation</strong>: The
          plaintiff must prove that the defendant was the sole cause of the
          death of a loved one. For example, if a drunk driver kills a
          pedestrian, the driver is seen as the primary cause of the victim's
          death. 4. <strong> Damages</strong>: It must be proven that the
          deceased accumulated many funeral expenses, medical bills and
          potential earnings.
        </p>
        <p>
          It is important to keep in mind for the family members of wrongful
          death victims that there is a limited amount of time to pursue
          compensation for their loved one's death. In California, some families
          have as little as 6 months from the date their loved one passed.
        </p>
        <h2>Determining Damages in a Wrongful Death</h2>

        <p>
          Financial or "Pecuniary" factors are the main determinants when
          claiming damages for a wrongful death. Pecuniary is anything related
          to the costs that the loved one has acquired after their death. There
          are a few elements to take into consideration when trying to recover
          pecuniary losses.
        </p>
        <p>
          <strong>
            {" "}
            Determinants for Claiming Pecuniary Damages from the Deceased
          </strong>
          :<li>Age</li>
          <li>State of health of the deceased</li>
          <li>Life expectancy</li>
          <li>Capacity of earnings (present and future)</li>
          <li>Loss of an inheritance</li>
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Wrongful death attorneys in Tustin"
            style={{
              backgroundImage:
                "url('/images/wrongful-death/Tustin Wrongful Death Text-Header Image.jpg')"
            }}
          >
            <h2>Our Top-Rated Attorneys Will Represent You</h2>
          </div>
        </LazyLoad>
        <p>
          {" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase Personal Injury Attorneys
          </Link>{" "}
          have assisted more than <strong> 12,000 clients</strong> while
          sustaining a<strong> 96% success rate</strong>. Over the years we have
          represented many families whose loved one's death was the result of a
          personal injury that our other clients fell victim to as well.
        </p>
        <p>
          We know exactly how to win your case, and have proven results
          assisting other families with similar situations. Our success will
          give you the peace of mind that you need to concentrate on recovering
          from your loss.
        </p>
        <p>
          No matter how your loved one suffered fatal injuries, we can help.
        </p>
        <p>
          If you are seeking out an experienced injury to fight for your loved
          one, contact us today. Our Tustin wrongful death lawyers offer free
          professional evaluations and will inform you of your options with no
          obligation to sign with our firm. Call{" "}
          <strong> call 949-203-3814</strong> and receive a{" "}
          <strong> free consultation</strong>.
        </p>

        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br /> Newport Beach, CA 92660
          </span>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
