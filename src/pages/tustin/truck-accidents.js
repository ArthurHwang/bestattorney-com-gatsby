// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "truck-accidents/Tustin Truck Accident Official Banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Truck Accident Attorney - Semi Truck Accident Attorney"
        pageDescription="If you have been involved in an accident and are seeking legal representation, contact the Tustin Truck Accident Lawyers of Bisnar Chase. Our experienced truck crash attorneys have been providing legal services to clients in the Orange County for over 40 years. Call 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Truck Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Truck Accident Lawyer"
            title="Tustin Truck Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The skilled Bisnar Chase{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/truck-accidents" target="_blank">
              {" "}
              Truck Accident Lawyers
            </Link>
          </strong>{" "}
          have been helping aid clients win millions in compensation for the
          past <strong> 40 years</strong>. Fatigued and distracted driving is a
          serious problem for all motorists, but truck drivers who work beyond
          the federally mandated hours of service are operating large and
          dangerous vehicles that can do great damage. Truck accidents can cause
          severe injuries and can even cost a driver or pedestrian their life.
          Survivors can be left with a substantial amount of medical bills and
          lost wages. If you or someone you know has experienced serious
          injuries from a truck accident the Tustin truck accident attorneys of
          Bisnar Chase are here to relieve some of the pain and suffering of the
          collision. Call <strong> 949-203-3814</strong> and receive a
          <strong> free consultation</strong> today.
        </p>

        <h2>The Danger of Drowsy Driving</h2>
        <p>
          There are several reasons why there are federal regulations that limit
          the number of hours that a truck driver can work without rest.
        </p>
        <p>
          First, as is true in all workplaces, it is important to restrict the
          ability of an employer to overwork their employees. Secondly, drivers
          who work longer than is allowed under the law are more likely to
          become drowsy.
        </p>
        <p>
          The U.S. Department of Transportation issued a report stating that in
          2012 3,887 people were killed in truck accidents. It also noted that
          in one year 13 percent of{" "}
          <Link to="/blog/truck-driver-worksheet-reduction-law">
            truck drivers in big rig accidents were sleep deprived
          </Link>
          .
        </p>
        <p>
          <strong>
            {" "}
            The National Highway Traffic Safety Administration (
            <Link to="https://www.nhtsa.gov/" target="_blank">
              NHTSA
            </Link>
            ) released the following data on the dangers of drowsy driving
          </strong>
          :
        </p>
        <ul>
          <li>
            Over a five-year period,{" "}
            <strong> 198,708 people were killed</strong> in truck accidents.
          </li>
          <li>
            Of those accidents, 4,430 were a direct result of drowsy driving.
          </li>
          <li>
            In those drowsy driving accidents,{" "}
            <strong> 4,432 drivers were killed</strong> and 5,021 people were
            killed.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/truck-accidents/Tustin Truck Accident Lawyers Tire Problems.jpg"
            width="100%"
            alt="Truck accident attorneys in Tustin"
          />
        </LazyLoad>
        <h2>3 Prevalent Truck Defects</h2>

        <p>
          Unfortunately broken equipment is not something that is uncommon in
          semi-trucks. Plenty of truck companies neglect to maintain a
          well-functioning vehicle for their drivers. Below are the average
          issues with machinery that often lead to many becoming accident
          victims.
        </p>

        <ul>
          <li>
            1. <strong> Brake Complications: </strong>The average big rig weighs
            up to 80,000 pounds, that is 20 more times than the typical Sudan.
            If it is necessary for a truck to brake immediately the amount of
            weight can be too much to handle. If a brake is overused constantly
            it can damage the brakes and more then likely lead to an accident.
          </li>
          <li>
            2. <strong> Tire Issues: </strong>When a tire does not obtain the
            standard quality and depth it is probable that it is in violation of
            FMSCA regulations. Instead of purchasing new tires, to save on
            costs, most trucking companies re-tread or re-cap their tires. If a
            tire is constantly re-treaded and contains insufficient tire wear
            this could lead to the tire heating up and shedding easily.
          </li>
          <li>
            3. <strong> Cargo Shift: </strong>Even though this is not in any
            relation to the quality of the equipment, cargo that is not evenly
            loaded can be problematic. If the cargo is not properly distributed
            then when a driver turns it can cause a commercial truck to tip
            over.
          </li>
        </ul>
        <h2>Hours-of-Service Regulations</h2>
        <p>
          Since truck drivers often cross multiple state lines, there are
          federal regulations that control how long a truck driver can work
          without rest.
        </p>
        <p>
          The{" "}
          <Link to="https://www.fmcsa.dot.gov/" target="_blank">
            {" "}
            Federal Motor Carrier Safety Administration
          </Link>{" "}
          (FMSCA), which monitors commercial vehicles such as trucks and buses,
          enforces the following restrictions with regard to how much rest
          drivers should get between shifts:
        </p>
        <ul>
          <li>
            Property carrying commercial drivers may work for 11 hours after 10
            consecutive hours off duty
          </li>
          <li>
            May not drive beyond the 14th consecutive hour after coming on duty,
            following a 10-hour rest period
          </li>
          <li>
            Drivers are not allowed to drive after 60/70 hours on duty in 7/8
            consecutive days. A driver may begin a 7/8 period after taking 34 or
            more consecutive hours off duty
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Tustin truck accident attorneys"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/Tustin Truck Lawyers Text Header.jpg')"
            }}
          >
            <h2>Experienced Orange County Truck Accident Lawyers</h2>
          </div>
        </LazyLoad>
        <p>
          Proving that a truck driver was speeding or under the influence is
          often relatively easier than proving that the driver was drowsy. A
          skilled injury lawyer can review the police report and the driver log
          to see if the truck driver was violating federal hours-of-service
          regulations.
        </p>
        <p>
          A knowledgeable Tustin truck accident attorney will also look into
          whether the company has had a history of encouraging drivers to
          falsify these logs in order to meet unrealistic delivery deadlines.
        </p>
        <p>
          The lawyers at Bisnar Chase have a long and successful track record of
          helping victims who have been involved in a truck accident obtain
          maximum compensation for their tremendous losses.
        </p>
        <p>
          Please contact us at <strong> 949-203-3814 </strong>to schedule your{" "}
          <strong> free, comprehensive consultation</strong>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove St. #120 <br />
          Newport Beach, CA 92660
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
