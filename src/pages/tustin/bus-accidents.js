// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "bus-accidents/Tustin Bus Accident Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Bus Accident Attorneys - Bisnar Chase"
        pageDescription="The Tustin Bus Accident Lawyers of Bisnar Chase have been handling bus crash cases for 40 years. Negligent drivers should be held liable for the injuries you have endured. If you have experienced injuries in an accident involving public transportation, call 949-203-3814 to speak with a bus collision attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Bus Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Bus Accident Lawyer"
            title="Tustin Bus Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin <Link to="/bus-accidents">Bus Accident Lawyers</Link>
          </strong>{" "}
          of Bisnar Chase believe that passengers involved in a bus accident
          should be fairly compensated for their pain and suffering. Bus crashes
          can result in catastrophic injuries not only for commuters but also
          for pedestrians and nearby drivers.
        </p>
        <p>
          For over <strong> 40 years</strong> our attorneys have held a{" "}
          <strong> 96% success rate</strong> in winning compensation for victims
          of bus incidents. Bus riders are vulnerable to painful head, neck and
          back injuries because of the lack of airbags and seatbelt.
        </p>
        <p>
          In many cases, the injuries can be major or even fatal. If you or
          someone you know has been severely injured in a bus collision, the law
          firm of <Link to="/">Bisnar Chase</Link> will fight for you. Call{" "}
          <strong> 949-203-3814</strong> to receive a{" "}
          <strong> free consultation</strong> and speak with an experienced
          injury lawyer.
        </p>
        <h2>U.S. Bus Accident Statistics</h2>
        <p>
          Just how common are bus accidents? According to the Federal Motor
          Carrier Safety Administration (
          <Link
            to="https://www.fmcsa.dot.gov/safety/data-and-statistics/large-truck-and-bus-crash-facts-2015"
            target="_blank"
          >
            FMCSA
          </Link>
          ), 247 people were killed in bus accidents throughout the United
          States in one year alone. Of those fatalities, 31 occurred in
          California.
        </p>
        <p>
          Why do bus accidents occur? Below are the most prominent causes for
          bus accidents.
        </p>
        <h3>
          <strong> 6 Factors That Lead To Bus Accidents</strong>
        </h3>
        <p>1. Bus driver is not paying attention</p>
        <p>2. Very crowded areas (car congestion)</p>
        <p>3.Lack of proper bus training</p>
        <p>4. Highway conditions</p>
        <p>5. Equipment defects</p>
        <h2>School Bus Collisions</h2>
        <p>
          Over 119 young adults (below the age of 19) were killed in a single
          year in a school bus-related crash. What's even more surprising is
          that injuries are not only experienced by those inside of the bus, but
          serious injuries can occur for people outside of the bus as well. The
          majority of bus crash victims are pedestrians, cyclist and motorist
          driving alongside a bus. The{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            {" "}
            NHTSA
          </Link>{" "}
          has reported that trucks and buses were the dominant mode of
          transportation that has killed pedestrians.
        </p>
        <p>
          Children that are on the bus are not safe either. Minors who fall
          victim to school bus injuries are harmed because of sudden rough
          turns, falling or by children roughhousing. Research indicates that
          17,000 children are sent every year to the emergency room due to bus
          incidents.
        </p>
        <p>
          National School Transportation Association Lead Robin Leeds, said that
          the reason school buses have gone far too long without seatbelt is
          because of the cost. &ldquo;It's an expensive proposition to outfit
          school buses with lap-shoulder belts, not just because of the cost of
          the equipment but because it also reduces seating capacity," Leeds
          shared with NBC News. There are precautions students can take for
          maintaining school bus safety.
        </p>
        <p>
          <strong> Tips for to Maintain School Bus Safety</strong>:
        </p>
        <li>Never walk behind a bus</li>

        <li>
          Notify the bus driver if you have dropped something close to the bus
        </li>

        <li>Be sure that a bus driver sees you when you cross the street</li>

        <li>Make sure the bus has come to a complete stop when getting off</li>

        <li>Use handrails to prevent falling off the bus at your stop</li>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/XTBl6BoqOFc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Common Injuries from a Bus Crash</h2>
        <p>
          Public transportation commuters are more likely to suffer from severe
          injuries in a bus crash due to the absence of seat belts and airbags.
          The most predominant of harms that bus crash victims suffer from are
          spinal cord, traumatic head wounds and whiplash like injuries.
        </p>
        <h3>Spinal Cord Injuries</h3>
        <p>
          Injuries involving harm to the spine result from a bus rider being
          thrown with an incredible amount of force to the side or outside of
          the bus.{" "}
          <Link to="/head-injury/spinal-cord-injuries" target="_blank">
            {" "}
            Spinal cord injuries
          </Link>{" "}
          can result to a victim being paralyzed or dead.
        </p>
        <h3>Traumatic Head Wounds</h3>
        <p>
          It is very common for a person to experience a traumatic head injury
          in a bus crash. Passengers often hit their skull vigorously on the bus
          seat, floor or window. There{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="_blank">
            {" "}
            different types of traumatic brain injuries
          </Link>{" "}
          but in a bus crash some can range from mild to severe and can take
          years to detect.
        </p>
        <h3>Whiplash</h3>
        <p>
          The word whiplash is used to describe the head experiencing a sudden
          jolt of force that causes the head to move back and forth. This can
          cause severe neck injuries and it can take up to 6 weeks to 6 months
          to heal.
        </p>
        <h2>Liable Parties in Bus Accidents</h2>
        <p>
          Injured victims of bus accidents may hold the at-fault parties
          accountable for their losses. Examples of potentially liable parties
          include the bus company, the bus driver, the company responsible for
          repairing the bus, another negligent driver and the bus manufacturer.
        </p>
        <p>
          Every case is different and a complete investigation will be needed to
          determine the cause of the crash and who should be held liable for the
          injuries suffered.
        </p>
        <p>
          Once liability is determined, the at-fault party can be held
          accountable for all of the losses suffered by the victim.
        </p>
        <p>
          <strong>
            {" "}
            A Victim Can Gain Compensation for the Following Losses
          </strong>
        </p>

        <li>Medical bills</li>
        <li>Hospitalization costs</li>
        <li>Lost income</li>
        <li>Loss of earning potential</li>
        <li>Disability</li>
        <li>Pain and suffering</li>
        <li>Loss of consortium</li>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Tustin bus crash attorneys"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/anaheim truck accident lawyers text header.jpg')"
            }}
          >
            <h2>Who Do I Turn to After a Bus Accident?</h2>
          </div>
        </LazyLoad>
        <p>
          Being in a bus crash can be traumatic and seeking out legal
          representation can be complicated. The trusted{" "}
          <strong> Bus Accident Attorneys</strong> of Bisnar Chase are here to
          make it simple. Our injury lawyers have gained over{" "}
          <strong> $500 Million </strong>dollars in wins since{" "}
          <strong> 1978</strong>.
        </p>
        <p>
          We believe in holding parties responsible for the damages you and your
          family have had to face.You do not have to face this alone. Contact us
          today at <strong> 949-203-3814</strong> and receive a
          <strong> free case analysis</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br />
            Newport Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
