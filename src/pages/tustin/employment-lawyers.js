// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "employment/Tustin Wrongful Termination Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Employment Lawyers - Discrimination - Wrongful Termination"
        pageDescription="If you have experienced a wrongful termination contact the Tustin employment lawyers of Bisnar Chase. Our Orange County employee rights attorneys have been representing clients who have faced discrimination and harassment in the workplace for 40 years. Receive a free consultation - 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Employment Lawyers Tustin</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Employment Lawyers"
            title="Tustin Employment Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you believe your rights have been violated or that you were
          mistreated in the workplace, the{" "}
          <strong> Tustin Employment Lawyers</strong> of Bisnar Chase are here
          to help. Suffering from a wrongful termination can be devastating to
          your finances and stress level. The law firm of Bisnar Chase wants to
          fight for the lost wages you have experienced from being unemployed.
          Over the last <strong> 40 years</strong>, our attorneys have garnered
          over
          <strong> $500 Million dollars</strong> in wins for our clients.
          Throughout your case, we help you understand your rights, and in cases
          of wrongdoing, we also help hold the at-fault company accountable for
          their actions. If you are seeking legal representation or have
          questions on what to do after being unjustly let go from an employer,
          call <strong> 949-203-3814</strong> to receive a
          <strong> free consultation</strong>.
        </p>
        <h2>What Is a Wrongful Termination?</h2>
        <p>
          A wrongful termination or a{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Wrongful_dismissal"
            target="_blank"
          >
            {" "}
            wrongful dismissal
          </Link>{" "}
          is when an employer releases you on illegal grounds or the company's
          code has been violated.  There are five types wrongful discharge
          practices that have been very common in the workforce.
        </p>
        <h3>5 Forms of a Wrongful Termination</h3>
        <p>
          <strong> 1. Discrimination</strong>: An employee is released on the
          grounds of their personal religious beliefs, gender, ethnic background
          or disabilities.
        </p>
        <p>
          2. <strong> Retaliation</strong>: Some employers will go to the
          lengths of firing a staff member because they will not take part in an
          illegal activity that is asked of them. If an employer retaliates
          towards an employee, for this reason, this can be seen as breaching
          company code.
        </p>
        <p>
          3. <strong> Refusing to perform an illegal action</strong>: An example
          of this form of wrongful termination can be if an employer pays you
          below the minimum wage of that state and expects you to continue
          working. This is a{" "}
          <Link to="/employment-law/wage-and-hour">
            wage and hour violation
          </Link>
          .
        </p>
        <p>
          4. <strong> Rejecting company termination procedures</strong>: Upon
          hiring the employee may receive a contract or gain a mutual{" "}
          <em>unwritten understanding</em> stating the process of dismissal. If
          the employer breaches the contract of being let go this is grounds for
          a wrongful termination.
        </p>
        <p>
          5. <strong> Sexual Harassment</strong>: If a manager corners a staff
          member and threatens their employment if they{" "}
          <Link to="/employment-law/sexual-harassment">
            do not perform sexual acts
          </Link>{" "}
          on them, this is illegal. If the employee refuses to do so and is then
          terminated this is also against the law and can be seen as a wrongful
          discharge.
        </p>
        <LazyLoad>
          <img
            src="/images/employment/Tustin Employment Attorneys Man Fired.jpg"
            width="100%"
            alt="Employment attorneys in  Tustin"
          />
        </LazyLoad>
        <h2>Knowing You Have Been Wrongfully Terminated</h2>
        <p>
          In order to prove that you have been unfairly discharged a breach of
          contract must be proven. Upon a job offer, there should be a clear
          understanding of wages, hours and the termination process of a
          company.
        </p>
        <p>
          If for example, your contract specifies that you work two weeks before
          the end of your employment, but you are immediately let go and asked
          not to work, your employer has breached the contract.
        </p>
        <p>
          According to California's labor law policy, an employer has the right
          and ability to discharge a staff member at any time for any reason.
          What employers must take into account for the{" "}
          <Link
            to="http://www.ncsl.org/research/labor-and-employment/at-will-employment-overview.aspx"
            target="_blank"
          >
            {" "}
            at-will
          </Link>{" "}
          capability is that the termination must be under fair, honest and
          legal terms.
        </p>
        <p>
          Likewise, an employee may quit a job whenever he or she likes. This
          right to terminate an employee at-will does not, however, allow a
          company to make firing decisions based on discrimination.
        </p>
        <p>
          When someone is let go because of his or her race, sex, disability or
          age, for example, it is considered a wrongful termination.
        </p>
        <h2>Steps Before Hiring an Employment Attorney</h2>
        <p>
          Experiencing a termination in employment can be difficult to handle
          but there are measures you can take when dealing with a wrongful
          termination suit that will make the process easier.
        </p>

        <li>
          <strong>
            {" "}
            Remain calm and do not retaliate negatively towards your employer
          </strong>
          : The reason why you would want to keep your composure after being
          handed the news that you have let go is because you don't need your
          employer having more ammunition on you. If you do act irrationally,
          this could lead to your employer being justified in discharging you.
          It is wise just to take the news peacefully.
        </li>

        <li>
          <strong> Record everything</strong>: If you have determined that you
          have been unjustly let go then it is best to have a time line of
          events that have led up to your dismissal. The reason being is that
          this can prove to be evidence against your employer. Every little
          detail counts especially the small ones.
        </li>

        <li>
          <strong> Seek out legal representation</strong>: You may have a number
          of questions regarding what to do next and if you can pursue
          compensation for your losses. A skilled employment lawyer can help you
          understand your rights, and in cases of wrongdoing, they can also help
          hold the at-fault company accountable for their actions.
        </li>

        <h2>Actions to Take in the Meantime</h2>
        <p>
          <strong> Looking for work</strong>: Just because you pursue a wrongful
          termination does not mean your job hunt should come to a halt. In the
          meantime, it is
          <strong> encouraged that you continue to seek employment.</strong>
        </p>
        <p>
          <strong> Unemployment benefits</strong>: A victim of a wrongful
          termination can also apply for unemployment benefits. It may be
          challenging to actually receive the benefits, but if you have proof
          that it was a wrongful termination they will be granted to you.
        </p>
        <p>
          <strong> Keep details to yourself</strong>: If you are preoccupied
          with not knowing what to say at your next job interview, don't be. It
          is an employee's right to keep their prior work experience
          confidential. If you feel the need to bring it up do not express many
          details.
        </p>
        <p>
          <strong> Severance pay</strong>: In terms of severance pay an
          organization usually provides the package when a bulk of employees are
          being laid off. If you were let go on the basis of a company
          downsizing you would be eligible for severance pay. The amount of
          severance pay you receive depends on the position and the number of
          years you had been employed by that company.
        </p>
        <p>
          The expectant severance package is one week of pay for every year
          served as an employee. Note that under California law an company is{" "}
          <em>
            <strong> not required</strong>
          </em>
          <strong> to give you a severance package.</strong>
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Employment lawyers in Orange County"
            style={{
              backgroundImage:
                "url('/images/employment/Employent Attorneys Text Header.jpg')"
            }}
          >
            <h2>Is It Still a Wrongful Termination If I Quit?</h2>
          </div>
        </LazyLoad>
        <p>
          If you quit not because you wanted to leave but because you were
          pressured to leave, this can also qualify as a wrongful termination
          case. A{" "}
          <Link
            to="https://www.personneltoday.com/hr/the-qualification-for-a-constructive-dismissal-claim/"
            target="_blank"
          >
            {" "}
            constructive dismissal
          </Link>{" "}
          is when an employee leaves an organization due to extremely unfair
          circumstances or is poorly mistreated.
        </p>
        <p>
          Again the employee needs to prove that they were entitled to their
          immediate discharge because of the abuse that was endured. An employer
          violating the employee's contract is something that also needs to be
          proven.
        </p>
        <p>
          It is not easy to confirm that mistreatment or abuse took place in a
          work environment. When presenting the courts with a constructive
          dismissal suit there are a few questions that an employee must keep in
          mind.
        </p>
        <ul>
          <li>
            Was the employee compelled to quit because of the hostile work
            environment that the employer provided?
          </li>
          <li>
            Would a reasonable individual in the same situation have resigned as
            well?
          </li>
          <li>
            Did the employer have knowledge of the intolerable work environment
            that led the worker to terminate their employment?{" "}
          </li>
        </ul>
        <p>
          <strong>
            {" "}
            Below are some factors that would contribute to a constructive
            dismissal of an employee
          </strong>
          :
        </p>
        <ul>
          <li>A major cut in wages</li>
          <li>Specific occurrences of mistreatment</li>
          <li>Demotion</li>
          <li>Dishonest work evaluations</li>
          <li>Scheduled to work graveyard shifts suddenly.</li>
        </ul>
        <p>All of these situations can pressure an employee to quit.</p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wmv1HKnhW7U"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Our Employment Attorneys Will Fight for You</h2>
        <p>
          Employment law is complicated and no one should face his or her
          current or former employer without quality legal representation. If
          you believe that your rights have been violated, you would be well
          advised to discuss your potential case with a knowledgeable and{" "}
          <Link to="/attorneys/jerusalem-beligan">
            experienced employee rights lawyer
          </Link>{" "}
          who can provide you with a free case evaluation.
        </p>
        <p>
          The experienced<strong> worker's rights attorneys</strong> of Bisnar
          Chase have a long and successful track record of representing
          employees and upholding the rights of workers in Southern California.
          If you or a loved one has experienced discrimination or harassment at
          work, the law firm of Bisnar Chase will give you the legal
          representation to win the compensation you rightfully deserve. Call{" "}
          <strong> 949-203-3814</strong> for a free consultation.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
