// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "nursing-home/Tustin Nursing Home Banner Image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Nursing Home Abuse Attorney - Elder Abuse and Neglect"
        pageDescription="If you believe your loved one is the victim of elder abuse and neglect, the skilled Tustin Nursing Home Abuse Lawyers of Bisnar Chase are here to help. The attorneys believe that the chosen care facility for your elderly family member should be held accountable. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Nursing Home Abuse Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Nursing Home Abuse Lawyer"
            title="Tustin Nursing Home Abuse Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/nursing-home-abuse" target="_blank">
              {" "}
              Nursing Home Abuse Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase understand that many residents of elderly care
          facilities do not provide adequate care for your loved one. Poor care
          can have devastating physical and emotional consequences on a nursing
          abuse victim.
        </p>
        <p>
          The law firm of <Link to="/">Bisnar Chase</Link> has been holding
          wrongdoers accountable for <strong> 40 years</strong> and have held a{" "}
          <strong> 96% success rate </strong>in winning compensation for senior
          clients who have experienced abuse. If your loved one has experienced
          abuse while living in a senior living facility please{" "}
          <strong> call 949-203-3814 </strong>and receive a{" "}
          <strong> free consultation</strong> with an experienced Tustin nursing
          home attorney today.
        </p>
        <h2>3 Signs of Nursing Home Abuse</h2>
        <p>
          Some care facilities fail to provide the most basic needs to their
          residents. All care facilities must make sure that their patients
          receive adequate water and food; yet, many elderly residents become
          dehydrated and malnourished. Has your loved one lost a significant
          amount of weight?
        </p>
        <p>
          Have you ever visited during lunch or dinner to see if your loved one
          was being fed? Is your loved one always in his or her room and never
          in the common area or an area where the residents eat?
          <strong> Elder abuse and neglect is easy to overlook</strong>. The
          many signs of nursing home abuse can remain hidden even from the most
          attentive and loving family members.
        </p>
        <p>
          Contacting an experienced Tustin nursing home abuse lawyer will be a
          major step towards helping your loved one. In the meantime be
          proactive and check for the following signs that indicate that your
          loved one may be suffering from elderly abuse.{" "}
          <strong>
            {" "}
            How to know if your elderly loved one is being abused
          </strong>
        </p>
        <p>
          1.<strong> Poor Hygiene: </strong>Many nursing home residents require
          assistance when taking a bath, using the restroom, changing their
          clothes and brushing their hair. Over time, this form of nursing home
          neglect will become obvious. The patient's clothes may be dirty and he
          or she may appear unclean. No patient in a nursing home should appear
          unkempt or dirty.
        </p>
        <p>
          2. <strong> Medical Neglect</strong>: It is important to choose a care
          facility that can provide the level of medical care and attention that
          your loved one needs. Not all care facilities are capable of providing
          adequate medical treatments to particularly at-risk patients.
        </p>
        <p>
          Even so, there is a basic level of medical attention that must be
          provided at all care facilities. It is crucial, for example, for
          nursing home staff to quickly treat cuts, infections, bedsores and
          other injuries that can become significantly worse over time if not
          properly treated.
        </p>
        <p>
          <strong>
            {" "}
            Questions to keep in mind when you are seeking to prove medical
            neglect:
          </strong>
        </p>
        <ul>
          <li>
            Is your loved one taking the pills as prescribed for his or her
            condition?
          </li>
          <li>
            Does your loved one have{" "}
            <Link to="/nursing-home-abuse/bedsores" target="_blank">
              {" "}
              bedsores and pressure ulcers
            </Link>{" "}
            that are either spreading or not getting better?
          </li>
          <li>
            Does your loved one have diabetes but is clearly not getting the
            proper amount of insulin?
          </li>
        </ul>
        <p>
          Facilities that fail to provide adequate treatments put their
          residents at great risk of suffering a life-changing injury.
        </p>
        <p>
          3. <strong> Emotional Neglect: </strong>Emotional neglect is probably
          the most difficult form of abuse to detect. It may not manifest itself
          in physical ways, but your loved one may undergo changes in his or her
          demeanor and personality. Stressed out workers may react angrily to
          their residents.
        </p>
        <p>
          Eventually, these harsh interactions may result in depression. Victims
          of emotional neglect may begin to withdraw from social interactions
          when what they need most is to connect with others.
        </p>
        <p>
          An important part of a healthy lifestyle in a nursing home is
          communication and interaction. Sadly, individuals who are being
          neglected or poorly treated rarely discuss their suffering with their
          loved ones. Instead, they often become embarrassed, shy, agitated or
          uncommunicative.
        </p>
        <LazyLoad>
          <img
            src="/images/nursing-home/Nursing Home Abuse Lawyers Center Image.jpg"
            width="100%"
            alt="Nursing home abuse attorneys"
          />
        </LazyLoad>
        <h2>How Can You Protect Your Loved One?</h2>
        <p>
          Nursing homes have a responsibility to provide quality care to the
          elderly. Unfortunately, not all senior community facilities are
          equipped with this mentality. There are steps that you can take for{" "}
          <Link
            to="https://www.nursinghomeabusecenter.com/elder-abuse/prevention/"
            target="_blank"
          >
            {" "}
            elder abuse prevention
          </Link>
          .
        </p>
        <h3>Staff Instruction and Guidance</h3>
        <p>
          Staff that were not trained properly can lead to serious of
          unintentional and non-intentional mistakes. For example, if a staff
          member is not informed that a patient is prescribed to take a
          medication daily, the elderly patient can suffer an immense amount of
          health repercussions and in some cases die. It is best to know that
          all code and standard regulations are up to par. You can find this out
          by speaking to management or check the quality of the nursing home on
          the Medicare.gov website.
        </p>
        <h3>Sanitation and Odor</h3>
        <p>
          One of the first signs of whether or not your loved one is being taken
          care of is if the odor in the facility or their room is unpleasant.
          Does the nursing home smell of urine? Most elderly need assistance
          going to the bathroom and a high-quality nursing home would have a
          sufficient amount of staff to care for the client in this way. Make
          sure that the senior center that your loved one is living in is clean
          regularly, the more often you visit, the better chance you have of
          spotting this mistreatment.
        </p>
        <h3>
          <span aria-describedby="ui-id-47">Always</span>{" "}
          Make Sure That Your Loved One Is Never Isolated
        </h3>
        <p>
          The number one way where you could prevent your loved one from being
          neglected is by having regular visits with them. Not only can
          isolation lead to your elderly loved one having an increased chance of
          being abused but your loved one can suffer from severe depression as
          well.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Nursing home lawyers in Tustin"
            style={{
              backgroundImage:
                "url('/images/nursing-home/Text Banner Image.jpg')"
            }}
          >
            <h2>Put a Stop the Pain and Suffering</h2>
          </div>
        </LazyLoad>

        <p>
          No amount of attention and education can help you prevent abuse or
          neglect at a nursing home. All we can do as loved ones is visit, ask
          questions and stay involved. It is also possible for us to be
          proactive.
        </p>
        <p>
          Doing your homework well, asking questions and visiting the facility
          several times to make an assessment are some of the actions you can
          take in order to be proactive and to ensure that your loved one goes
          to the right nursing home.
        </p>
        <p>
          Our skilled <strong> Tustin nursing home abuse lawyers</strong> have
          helped victims and their families obtain justice and fair compensation
          for the injuries and damages they have sustained. Please call
          <strong> 949-203-3814</strong> for a free consultation and to obtain
          more information about pursuing your legal rights.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br />
            Newport Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
