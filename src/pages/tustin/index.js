// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/Tustin Personal Injury Lawyers Official Banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Personal Injury Attorneys - Accident Injury Lawyers"
        pageDescription="The Tustin Personal Injury Lawyers of Bisnar Chase have been providing legal representation for injury victims for the past 4 decades. If you have suffered serious injuries from a car accident, dog bite or a defective product contact the attorneys of Bisnar Chase at 949-203-3814 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Personal Injury Lawyers"
            title="Tustin Personal Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            If you have suffered severe injuries due to someone else's
            carelessness, contact the
            <strong>
              {" "}
              Tustin{" "}
              <Link to="/orange-county" target="_blank">
                {" "}
                Personal Injury Lawyers
              </Link>
            </strong>{" "}
            of Bisnar Chase. With <strong> 40 years of experience </strong>and a
            <strong> 96% success rate</strong>, our lawyers guarantee that if
            there is no win there is no fee.
          </p>
          <p>
            Our passionate legal team works hard on your case so the only
            concern you have to have is recovering. Our law firm is fully
            equipped with a total of six top-notch lawyers and four litigation
            groups that have earned over
            <strong> $500 Million dollars</strong> in wins for our clients.
          </p>
          <p>
            Victims of personal injuries need attorneys who can help them inside
            and outside of the courtroom. A person involved in an accident
            should hire a highly-rated Tustin personal injury attorney if there
            is any chance on receiving maximum compensation for injuries and
            getting life back on track.
          </p>
          <p>
            If you or a loved one has endured serious injuries from a car crash
            or a slip and fall call<strong> 949-203-3814</strong> and{" "}
            <strong> receive a free consultation</strong> for your injury case.
          </p>
          <h2>How Do I File a Personal Injury Claim?</h2>
          <p>
            A personal injury is when a victim has undergone immense pain and
            suffering due to a third party's negligence. The process of where to
            begin when filing a personal injury claim is difficult to navigate
            through. There are steps that can help guide you through this
            process.
          </p>
          <p>
            <strong>
              {" "}
              Establish If an Insurance Company Will Cover the Damages
            </strong>
          </p>
          <p>
            After a car crash it is strongly suggested that you find out whether
            or not the negligent driver has insurance. This is important because
            it will verify whether or not you will be awarded any compensation
            for your damages. If the driver does not have any insurance or any
            income for that matter there would not be much to collect in terms
            of losses.
          </p>
          <p>
            <strong>
              {" "}
              Determining Whether or Not to File a Personal Injury Claim{" "}
            </strong>
          </p>
          <p>
            If the driver's insurance company is not proceeding immediately to
            follow up on your claim it is recommended that you file a personal
            injury suit. An important detail to ask about is the statue of
            limitations regarding personal injuries in your state. Many states
            have a standard limited amount of time regarding filing a suit. In
            California, an accident victim has up to two year to file their
            personal injury claim.
          </p>
        </div>
        <h2>Finding a Lawyer who is Right for You</h2>
        <p>
          There are hundreds, if not thousands, of attorneys within your region.
          Finding the right one to represent you can be a challenging process.
          It may be in your best interest to ask your friends for a
          recommendation. If you are searching online for a personal injury
          lawyers in Tustin here are some questions to ask yourself:
        </p>
        <ul>
          <li>Do they have a list of cases that they have recently handled?</li>
          <li>Is helpful information posted on their web sites?</li>
          <li>
            Do they have client reviews? What are their clients saying about
            them?
          </li>
          <li>Do they offer free consultations?</li>
        </ul>
        <p>
          The best way to hire an injury attorney is by scheduling a meeting and
          talking face to face. This way you can decide if this particular law
          firm is a good fit for you.
        </p>
        <h2>5 Types of Personal Injury Cases</h2>
        <p>
          The way in which a personal injury victim can benefit from filing a
          claim is being able to gain compensation for damages such as lost
          wages and medical bills. There are several kinds of personal injury
          suits that many people do not know they can claim. Determining the
          type of case is important for you and your lawyer to better understand
          what the outcome would be for your legal dispute.
        </p>
        <p>
          <strong>
            {" "}
            5 commonly known types of personal injury legal cases:
          </strong>
        </p>
        <p>
          1. <strong> Motor Vehicle Accident</strong>: Over six million people
          are involved in car accidents every year in the United States. Studies
          also show that results can be deadly. Experts say that the best car
          accident prevention lies in paying attention, not speeding and relying
          on a seat belt for safety.
        </p>
        <p>
          2. <strong> Slip, Trips and Falls</strong>: It may seem insignificant
          but victims of slip and falls can suffer from broken bones or from a
          traumatic brain injury. The number one cause of slip, trips and falls
          is due to slippery or uneven floors. About 55% of people experience
          severe injuries from walking on slippery surfaces, potholes in the
          parking lot or staircases that have not been constructed well.
        </p>
        <p>
          3. <strong> Dog Bites</strong>: The majority of dog bite victims are
          unfortunately children. The size of a child makes them vulnerable and
          the dog may believe it has a greater chance of victory in an attack.
          Professionals say that if you encounter a dog that looks dangerous do
          not start running, this will signal to the dog that you are prey.
        </p>
        <p>
          5. <strong> Wrongful Death</strong>: If a family member or loved one
          dies due to a property owner's or employer's negligence this can be
          considered a wrongful death. Family members such as children or
          spouses can claim a wrongful death. If the deceased does not leave
          behind a surviving member of family then a domestic partner or
          dependent can file a wrongful death claim.
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/Tustin personal injury lawyers middle image.jpg"
            width="100%"
            alt="Tustin personal injury attorneys"
          />
        </LazyLoad>
        <h2>What Damages Can I Be Compensated For?</h2>
        <p>
          Most losses are easy to determine the cost of such as medical bills
          and property damage. What is not easy to determine though is the
          losses that a crash or slip and fall victim emotionally or mentally
          faces after an accident. For example, if a person faces deep
          depression after an accident it is difficult to indicate how long and
          what kind of therapy the victim will need.
        </p>
        <p>
          The purpose of compensating a victim after they have experienced
          severe injuries is so the person, in the court's eyes is made "whole"
          again, meaning that the plaintiff is financially covered for the
          damages.
        </p>
        <p>
          <strong>
            {" "}
            Types of damages an accident injury victim can be awarded
          </strong>
          :
        </p>
        <ul>
          <li>Medical care</li>
          <li>Lost wages</li>
          <li>
            Emotional pain and suffering such as post traumatic stress disorder
            and depression
          </li>
          <li>Property damages</li>
          <li>
            Loss of consortium (negative repercussions a plaintiff will face
            with their spouse whether it be companionship or being able to
            possess a sexual relationship)
          </li>
        </ul>
        <h2>Common Injuries Suffered in an Accident</h2>
        <p>
          <strong> Traumatic Brain Injuries and Head Wounds</strong>
        </p>
        <p>
          The most frequent injury experienced is harm to the head and brain. In
          a vehicle accident, the injury is brought upon by the head slamming
          against the steering wheel. Brain injuries can range from a minor
          concussion to severe internal bleeding. Medical attention is required
          immediately so that you may follow up and understand{" "}
          <Link to="/head-injury/treatments" target="_blank">
            {" "}
            how to treat a head injury after the incident.
          </Link>
        </p>
        <p>
          <strong> Internal Bleeding</strong>
        </p>
        <p>
          If an accident victim endures serious trauma to the body this can
          result in internal bleeding. Internal bleeding is when blood vessels
          inside of the body are ripped apart due to a great force that the body
          has experienced. If the bleeding progresses serious precautions must
          be taken such as surgery.
        </p>
        <p>
          <strong> Spinal Cord Damage</strong>
        </p>
        <p>
          Each year over 12,500 people are diagnosed with spinal cord injuries.
          The leading causes of{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/spinal-cord-injury/symptoms-causes/syc-20377890"
            target="_blank"
          >
            {" "}
            spinal cord injuries
          </Link>{" "}
          are car accidents and falls. If the spine is ruptured it can lead to a
          person being immobile or worse, paralyzed.
        </p>
        {/* <LazyLoad>
          <img
            src="/images/personal-injury/Tustin Personal Injury text header image.jpg"
            width="100%"
            alt="cracked windshield"
          />
        </LazyLoad>

    

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Personal injury attorneys in Tustin"
            style={{
              backgroundImage:
                "url('/images/personal-injury/Tustin Personal Injury text header image.jpg')"
            }}
          >
            <h2>Superior Legal Representation for You</h2>
          </div>
        </LazyLoad> */}
        <h2>Superior Legal Representation for You</h2>
        <p>
          Our <strong> Tustin personal injury lawyers</strong> have been
          assisting victims since 1978, and have done so with honesty and
          professionalism. The law firm of Bisnar Chase is passionate about
          obtaining justice for injury victims and have instilled their mission
          in every one of their staff members.
        </p>
        <p>
          Do not settle for an attorney without experience and proven results.
        </p>
        <p>
          The experienced Tustin personal injury attorneys at Bisnar Chase have
          a long and successful track record of representing seriously injured
          victims and their families. We specialize in personal injury cases.
          Our goal is to provide quality legal representation to our clients and
          help them achieve the best possible outcome.
        </p>
        <p>
          If you or a loved one has suffered a serious personal injury as a
          result of someone else's negligence or wrongdoing please contact us at
          call 949-203-3814, Monday- Friday 8 a.m.-5 pm and receive a free
          comprehensive case evaluation.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
