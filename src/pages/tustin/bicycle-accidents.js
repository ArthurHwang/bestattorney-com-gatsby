// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bike-accidents/Tustin Bicycle Lawyers Banner Image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Bicycle Accident Lawyers - Orange, CA"
        pageDescription="If you have suffered severe injuries contact the Tustin Bicycle Accident Lawyers of Bisnar Chase. Our bike injury attorneys have been helping victims involved in bicycle collisions in Orange County for over 40 years. Call 949-203-3814  and receive a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Bicycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Bicycle Accident Lawyers of Bisnar Chase"
            title="Tustin Bicycle Accident Lawyers of Bisnar Chase"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/bicycle-accidents" target="_blank">
              {" "}
              Bicycle Accident Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase believe that negligent drivers should be held
          accountable for their faults. Bicyclists who are involved in a car
          accident often need immediate medical attention and accumulate an
          abundance of medical expenses instantaneously.
        </p>
        <p>
          Bike accident victims and their families need to remember that help is
          available.
        </p>
        <p>
          Bisnar Chase's bike accident attorneys have won over{" "}
          <strong> $500 Million dollars</strong> in compensation for victims of
          bicycle accidents and have achieved a
          <strong> 96% success rate</strong> for clients. Our group of
          experienced <strong> Tustin bicycle accident attorneys</strong> are
          here to advise injured victims and their families about their legal
          rights and options. If you or a loved one has experienced severe
          injuries from a bike accident call 949-203-3814 and speak to an
          experienced injury lawyer today.
        </p>

        <h2>Seeking Compensation for your Accident</h2>
        <p>
          According to{" "}
          <Link
            to="http://iswitrs.chp.ca.gov/Reports/jsp/CollisionReports.jsp"
            target="_blank"
          >
            {" "}
            California Highway Patrol's Statewide Integrated Traffic Records
            System{" "}
          </Link>
          (SWITRS), 22 people were injured in Tustin bicycle accidents recently.
        </p>
        <p>
          Throughout Orange County, 12 people were killed and 1,199 were injured
          in bike crashes during that same year. Bisnar Chase's{" "}
          <strong> Bicycle Accident Lawyers</strong> understand that being
          injured in a bike accident can drastically affect the physical and
          emotional state of a victim. Costs can be expensive and you may be
          wondering how you can afford to recover from the accident.
        </p>
        <p>
          <strong>
            {" "}
            When you file a personal injury claim you can be compensated for the
            following losses
          </strong>
          :
        </p>

        <li>Medical bills</li>

        <li>Hospital visits</li>

        <li>Pain and suffering</li>

        <li>Damage to property</li>

        <li>Counselor or therapy aid</li>

        <h2>Hit-and-Run Law</h2>
        <p>
          All motorists are legally required to remain at the scene of the crash
          when someone has been injured. California Vehicle Code 20001 (a)
          states:
        </p>
        <p>
          <em>
            "The driver of a vehicle involved in an accident resulting in injury
            to a person, other than himself or herself, or in the death of a
            person shall immediately stop the vehicle at the scene of the
            accident."
          </em>
        </p>
        <p>
          A hit-and-run is a term used when a vehicle hits a car or person and
          then leaves the scene immediately after the collision. If an animal is
          involved in a hit-and-run some states would consider that a legal
          issue as well. Few states would also provide relief for drivers who
          have left the accident in order to seek immediate help.
        </p>
        <p>
          It can be complicated to determine the difference between a felony and
          a misdemeanor hit-and-run. The difference lies in the damages that
          have been acquired in the accident. A hit-and-run misdemeanor involves
          solely the damage of property and the hit-and-run felony concerns the
          injury of a person. If you have experienced a serious bicycle accident
          injury there three factors to take into account when filing an injury
          claim.
        </p>
        <p>
          <strong>
            {" "}
            3 Questions to answer before hiring a Tustin bicycle accident
            attorney
          </strong>
          :
        </p>
        <p>
          1. Who was the person at fault? 2. How much damage took place? 3. What
          was the level of severity of the injuries?
        </p>
        <LazyLoad>
          <img
            src="/images/bike-accidents/Tustin Bicycle-Male-Cyclist.jpg"
            width="100%"
            alt=" Tustin bike accident attorneys"
          />
        </LazyLoad>
        <h2>Bicycle Hit-and-Run Injuries</h2>
        <p>
          Victims of bicycle hit-and-run accidents are often left on or
          alongside the roadway. If no one else witnessed the crash, the victim
          may be left there without medical attention. In cases involving head
          injuries and spinal cord injuries, every second counts. The longer
          they are left without care or medical attention, their chance of
          survival diminishes.
        </p>
        <p>
          This is why drivers have a moral and legal obligation to notify the
          authorities immediately following the crash.
        </p>
        <p>
          Bicyclists can suffer life-changing or fatal injuries if a vehicle
          strikes them. Below is a collection of common injuries that bicycle
          collision victims can suffer from.
        </p>
        <p>
          <strong> 7 Common Bike Incident Injuries</strong>:
        </p>

        <li>Arm, leg or chest fracture</li>

        <li>
          {" "}
          <Link to="/head-injury/tbi" target="_blank">
            {" "}
            Traumatic brain injuries
          </Link>
        </li>

        <li>Organ injuries</li>

        <li>Lacerations</li>

        <li>Skin abrasions</li>

        <li>Burn injuries</li>

        <li>Spinal cord injuries</li>

        <p>
          All of these injuries require immediate medical attention. Victims of
          hit-and-run accidents are also in danger of being struck a second
          time. Victims who are struck a second time are likely to suffer
          injuries that often prove fatal. It is unacceptable to leave a
          bicyclist without stopping, calling the authorities and offering
          assistance to protect the victim from further harm.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bike injury lawyers in Tustin"
            style={{
              backgroundImage:
                "url('/images/bike-accidents/Tustin Bike Accident Attorneys Lady Justice.jpg')"
            }}
          >
            <h2>Obtain Fair Compensation</h2>
          </div>
        </LazyLoad>
        <p>
          There are legal options available for victims of bicycle{" "}
          <Link to="/pedestrian-accidents/hit-and-run">
            accidents involving a hit-and-run driver
          </Link>
          . If the authorities track down the motorist, then, financial
          compensation may be available through a personal injury claim against
          the at-fault driver.
        </p>
        <p>
          Unfortunately, not all drivers are found. In such cases, a skilled
          Tustin bicycle accident attorney can help the victim recover losses
          through his or her own insurance policy.
        </p>
        <p>
          If you or a loved one has been injured in a bicycle accident you need
          the representation of an experienced{" "}
          <strong> Tustin bicycle accident lawyer</strong>.{" "}
          <Link to="/">Bisnar Chase injury lawyers</Link> have a long and
          successful track record of obtaining just compensation for injured
          victims and their families and have a long track record in Orange
          County courts. Please contact us for a{" "}
          <strong> free, comprehensive and confidential consultation</strong>.
          Call <strong> 949-203-3814</strong> today.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys<br></br> 1301 Dove St. #120{" "}
          <br></br>
          Newport Beach, CA 92660
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
