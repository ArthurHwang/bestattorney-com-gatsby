// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "brain-injury/Tustin Brain Injury Banner Image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Brain Injury Attorney - TBI - Traumatic Brain Injury"
        pageDescription="Experiencing a traumatic brain injury can be emotionally and financially draining. The Tustin Brain Injury Lawyers of Bisnar Chase are here to relieve some of that stress. The pain and suffering that comes with a serious head injury should be compensated for. call 949-203-3814 to speak with a catastrophic injury attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Brain Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Brain Injury Lawyers"
            title="Tustin Brain Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Victims of traumatic brain injuries can trust the{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/head-injury" target="_blank">
              {" "}
              Brain Injury Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase to fight for rightfully deserved financial
          compensation. Since <strong> 1978</strong>, our injury attorneys have
          been holding negligent parties accountable for their wrongs and have
          gained over
          <strong> $500 Million dollars</strong> in winnings for clients.
        </p>
        <p>
          If you or someone you know is one of the millions of people that has
          suffered from a catastrophic head wound please Call{" "}
          <strong> 949-203-3814</strong>. The
          <strong> Tustin Brain Injury Attorneys</strong> of Bisnar Chase
          guarantees that if there is no win there is no fee.
        </p>
        <p>
          The law firm of Bisnar Chase feels that you should not have take on
          this fight alone. Contact us today and receive a{" "}
          <strong> free consultation</strong>.
        </p>
        <h2>5 Types of Major Brain Injuries</h2>
        <h3>
          Brain injuries can range from a mild concussion to an open head wound.
          After you have experienced a brain injury after harsh blow to the head
          it is strongly recommended that you seek medical attention right away.
          The sooner you receive medical attention the earlier the injury can be
          diagnosed and treated.
        </h3>
        <h3>1. Concussion</h3>
        <p>
          A concussion is described to be a wound that occurs when you have
          undergone a blow to the skull or have collided with an object. When
          the head is suddenly hit, the brain quickly bounces forwards and
          backward. This type of movement can cause changes of the chemicals in
          the brain and can also damage brain cells. Concussions can have{" "}
          <Link to="/head-injury/effects-of-concussions" target="_blank">
            {" "}
            short and long term side effects
          </Link>
          so it is best to pursue medical attention as quickly as possible.
        </p>
        <h3>2. Hematoma</h3>
        <p>
          When a group of blood cells collects outside of the blood vessels this
          is known as a hematoma. The hematoma develops after a vein, artery or
          blood vessel has been damaged and and blood has seeped into tissues
          where they do not belong.
        </p>
        <h3>3. Skull fracture</h3>
        <p>
          Since the skull is one of the few bones in the body that does not
          contain bone marrow it is very durable and not easily broken. When the
          cranium is cracked open then the brain becomes very vulnerable and can
          result in an extreme injury.
        </p>
        <h3>4. Edema</h3>
        <p>
          This is one of the most severe of the injuries because when swelling
          occurs in the brain it puts pressure on the skull. The skull does not
          expand so when the brain swells the stress upon the brain can be
          deadly.
        </p>
        <h3>5. Diffuse Axonal</h3>
        <p>
          In this circumstance, tears in the brain are made apparent and can
          lead to a coma or even be fatal. If the patient survives this type of
          injury serious impairment of motor skills or motor sensations can take
          place
        </p>
        <h2>The Healing Stages for a TBI</h2>
        <p>
          In the beginning stages of a traumatic brain injury, a person may be
          in an unconscious state due to the swelling or bleeding of the brain.
          As the swelling of the brain goes down a TBI victim will eventually
          open their eyes and be responsive to the environment around them.
          <strong>
            {" "}
            There are three commonly used terms to describe the beginning phases
            of recovery for a TBI
          </strong>
          :
        </p>
        <p>
          1.<strong> Coma</strong>: The patient is not conscious, communicating
          or responding to his surroundings. The eyes are usually closed as
          well.
        </p>
        <p>
          2. <strong> The Vegetative State</strong>: At this point of the{" "}
          <Link
            to="http://www.msktc.org/tbi/factsheets/Understanding-TBI/The-Recovery-Process-For-Traumatic-Brain-Injury"
            target="_blank"
          >
            {" "}
            recovery process
          </Link>{" "}
          the patient experiences wake and sleep cycles. He/She is awake but
          reacts minimally to noise or visuals. 3.{" "}
          <strong> Slightly Conscious</strong>: The TBI victim displays emotion
          and is responsive to visuals, noise and is able to perform basic motor
          skills.
        </p>
        <p>
          Although a person who has suffered from a TBI may seem to be
          functioning well on a daily basis, there can be severe emotional and
          mental symptoms that can follow.
        </p>
        <p>
          <strong> Symptoms That Can Develop after a TBI</strong>:
        </p>
        <ul>
          <li>Being flustered</li>

          <li>Anxious</li>

          <li>Inability to focus</li>

          <li>Depression</li>
          <li>Overacting</li>
          <li>Aggressive</li>
          <li>Disrupted sleeping patterns</li>
        </ul>
        <LazyLoad>
          <img
            src="/images/brain-injury/Tustin TBI Treating Brain Injury.jpg"
            width="100%"
            alt="Tustin brain injury attorneys"
          />
        </LazyLoad>
        <h2>Treating a Traumatic Brain Injury</h2>
        <p>
          Experiencing a serious head injury can affect a person's communication
          skills, focus and mobility. Recovery is extensive and may serve
          difficult at times but is vital. Depending on the brain injury there
          are various options one can take for therapy.
        </p>
        <h3>Acute Care</h3>
        <p>
          This type of therapy is known to be one of the initial stages of
          recovery.{" "}
          <Link to="https://en.wikipedia.org/wiki/Acute_care" target="_blank">
            {" "}
            Acute care
          </Link>{" "}
          treatment serves to teach the patient how to carry out daily tasks
          such as putting on clothes, walking, eating and talking.
        </p>
        <h3>Rehabilitation after Acute Care</h3>
        <p>
          This step in brain rehabilitation is known as the post-acute care
          process. This form of recovery can be vigorous because it's purpose is
          to help the patient become more independent. Therapy can last up to 6
          hours a day.
        </p>
        <h3>Sub-Acute Therapy</h3>
        <p>
          This stage in healing provides patients that are not in need of
          intense therapy to go home at night. Patients that have reached an
          advanced level of development but have not entirely recovered from a
          brain injury can be transferred to an after-care facility such as a
          nursing home.
        </p>
        <h3>Day-time Treatment</h3>
        <p>
          Patients who are not in need of round-the-clock care and who are
          allowed to go home at night would be more likely to choose this option
          of treatment. This kind of care is also followed once a hospital has
          discharged a patient.
        </p>
        <h3>Outpatient Treatment</h3>
        <p>
          Patients who were not diagnosed with a severe head injury would be
          urged to partake in this type of treatment. This kind of therapy is
          used to maintain brain recovery.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Brain injury lawyers in Tustin"
            style={{
              backgroundImage:
                "url('/images/brain-injury/Tustin Brain Injury Liabilty Text Header.jpg')"
            }}
          >
            <h2>Pursuing Compensation after a TBI</h2>
          </div>
        </LazyLoad>
        <p>
          An estimated 350,000 Californians are living with a traumatic brain
          injury. The{" "}
          <Link to="https://www.cdc.gov/" target="_blank">
            {" "}
            CDC
          </Link>{" "}
          reports that in one year, 19,415 California residents were
          hospitalized with a non-fatal TBI.
        </p>
        <p>
          Victims can pursue financial compensation by filing a claim against
          the at-fault party.
        </p>
        <p>
          After a slip-and-fall accident, for example, it must be determined if
          the accident resulted from a hazardous condition, if the property
          owner knew of the condition and if the property owner failed to
          prevent the accident.
        </p>
        <p>
          In such cases, a premises liability claim can hold the property owner
          accountable for his or her negligence. Following a car accident, it
          must be determined if the at-fault driver was negligent at the time of
          the crash. Was the driver speeding, drunk, fatigued, distracted or
          otherwise careless?
        </p>
        <p>
          In such cases, the at-fault driver can be held liable for the victim's
          injuries and losses. When you hire a{" "}
          <strong> Tustin Brain Injury Lawyer</strong> you can recover losses
          such as lost wages, medical expenses and emotional distress.
        </p>
        <h2>What Causes a Traumatic Head Injury?</h2>
        <p>
          There are various factors that can lead to a serious head injury. Even
          though the cause can be as minor as a fall, this small incident can
          cause a person their life. Below are the three common causes that can
          lead to a traumatic brain injury.
        </p>
        <h3>
          <strong> 3 Primary Causes a TBI</strong>
        </h3>
        <ul>
          <li>
            <strong> 1. Motor-Vehicle Accidents</strong>: More than 50% of head
            injuries are a result of a car crash. Generally, the brain injury is
            due to the head colliding severely into the window or steering
            wheel.
          </li>
          <li>
            <strong> 2. Slip and Falls:</strong> It may seem trivial but many
            serious head wounds are caused by slip and falls accidents. A
            majority of these traumatic brain injuries are experienced by
            elderly individuals.
          </li>
          <li>
            <strong> 3. Athletic Activities</strong>: Children or young adults
            involved in contact sports such as football, have a greater risk of
            suffering from a severe head injury. Athletes who experience a head
            wound have the risk of enduring second impact syndrome or the blow
            can even cost them their lives.
          </li>
        </ul>
        <LazyLoad>
          <img
            src="/images/brain-injury/Tustin Brain Injury Gavel Tiny.jpg"
            width="100%"
            alt="Tustin catostrophic head injury attorneys"
          />
        </LazyLoad>
        <h2>Our Tustin Brain Injury Lawyers Will Face Your Case Head-On</h2>
        <p>
          Victims of serious head injuries often require emergency room
          treatments, expensive MRI or CAT Scan testing, hospitalization or
          surgical procedures.
        </p>
        <p>
          The experienced <strong> Tustin brain injury lawyers</strong> at our
          law firm have helped victims of catastrophic injuries obtain fair
          compensation for damages that include medical expenses and lost wages.
        </p>
        <p>
          If you or a loved one has endured a traumatic brain injury and has
          suffered emotionally and financially please call{" "}
          <strong> 949-203-3814</strong> and obtain a
          <strong> free case analysis</strong> from a top-rated{" "}
          <strong> Tustin brain injury attorney</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br />
            Newport Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
