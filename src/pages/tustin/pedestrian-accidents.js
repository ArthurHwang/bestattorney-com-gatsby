// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "pedestrian-accidents/Tustin Pedestrian Lawyers Official Banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Pedestrian Accident Attorneys - Bisnar Chase"
        pageDescription="If you have been the victim of a pedestrian accident, please contact the Tustin Pedestrian Accident Lawyers of Bisnar Chase. An injury can leave you with costly medical expenses. At Bisnar Chase we believe that when an accident has occurred someone should be held liable for the damages. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Pedestrian Accident Lawyer"
            title="Tustin Pedestrian Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have suffered an injury due to a pedestrian accident, the{" "}
          <strong> Tustin</strong>
          <strong>
            {" "}
            <Link to="/pedestrian-accidents" target="_blank">
              {" "}
              Pedestrian Accident Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase are here to help. Pursuing financial compensation for
          your losses can stem as a difficult process. The attorneys of Bisnar
          Chase are to make winning compensation for your losses easy.
          <strong> Since 1978</strong>, the law firm of Bisnar Chase has been
          representing clients who have endured pain and suffering from a
          pedestrian accident. With a <strong> 96% rate</strong>, we have gained
          over <strong> $500 Million</strong> in wins for accident victims. If
          you or someone you know has been involved in a pedestrian incident our
          Tustin pedestrian accident attorneys will fight for you. Call{" "}
          <strong> 949-203-3814</strong> and receive a
          <strong> free case analysis</strong>.
        </p>
        <h2>Proving Liability in Pedestrian Accident Cases</h2>
        <p>
          In order to prove negligence, a pedestrian victim must prove that they
          have suffered major injuries or have experienced serious damages to
          their emotional and mental health. There are a few important questions
          to keep in mind to make your claim successful. Important Questions to
          Ask When Filing an Accident Claim
        </p>
        <ul>
          <li>Were you legally crossing the road at the time of the crash?</li>
          <li>Were you in a crosswalk or at an intersection?</li>
          <li>
            Did the driver strike you while committing a traffic violation?
          </li>
        </ul>

        <p>
          The answers to these types of questions will likely affect the outcome
          of your claim. A skilled Tustin pedestrian accident lawyer can help
          you obtain fair compensation for your losses and help hold the
          at-fault party accountable.
        </p>
        <h2>What the Opposing Party Will be Asking You</h2>
        <p>
          Once you have made an accident claim, insurance companies and the
          defenses legal team will be wanting specific details when you are
          claiming charges in a pedestrian accident. Investigators assigned to
          your accident will carefully look at when and where the accident took
          place.
        </p>
        <p>
          <strong> 4 Factors Investigators Will Inquire About</strong>:
        </p>
        <p>
          1.
          <strong>
            {" "}
            Were you in a crosswalk or at an intersection at the time of the
            collision
          </strong>
          ? Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=21950.&lawCode=VEH"
            target="_blank"
          >
            California Vehicle Code section 21950
          </Link>
          :{" "}
          <em>
            "The driver of a vehicle shall yield the right-of-way to a
            pedestrian crossing the roadway within any marked crosswalk or
            within any unmarked crosswalk at an intersection."
          </em>
        </p>
        <p>
          2. <strong> Did you have the right of way?</strong> Pedestrians are
          required to cross only in designated areas. However, drivers have the
          responsibility to exercise due care and caution especially in areas
          where there are pedestrians. For example, a driver who strikes a
          pedestrian while exceeding the posted speed limit could be held liable
          for the accident even if the pedestrian was walking outside of a
          marked crosswalk.
        </p>
        <p>
          3.<strong> Why was the driver unable to avoid the collision?</strong>{" "}
          Unless you were running quickly across traffic, it is reasonable to
          assume that an alert and defensive driver should have been able to
          avoid the collision. Therefore, it must be determined why the accident
          occurred.
        </p>
        <p>
          4. <strong> Did the driver commit a traffic violation?</strong> For
          example, did the driver run a red light? Did he or she run a stop sign
          or make an illegal or unsafe turn?
        </p>
        <p>
          In some cases, a dangerous roadway or intersection may also cause or
          contribute to an injury pedestrian accident. In such cases, the city
          or governmental agency responsible for maintaining the roadway can
          also be held liable. Any personal injury claim against a governmental
          agency must be properly filed within six months of an incident.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pedestrian accident attorneys in Tustin"
            style={{
              backgroundImage:
                "url('/images/pedestrian-accidents/Tustin Lawyers Official Text header Image.jpg')"
            }}
          >
            <h2>Common Causes of Pedestrian Accidents</h2>
          </div>
        </LazyLoad>
        <p>
          Most{" "}
          <Link to="/pedestrian-accidents/statistics" target="_blank">
            {" "}
            pedestrian accidents
          </Link>{" "}
          occur because of the drivers failure to pay attention or precautions
          for safety. Pedestrians can suffer from injuries ranging from a
          sprained ankle or even loosing their life. In one year over 5,000
          people were killed in a pedestrian-related accident and every year 15
          people are either severely injured or die as a result as well.
        </p>
        <p>
          Below are common causes along with precautions that you can take to
          avoid being an accident victim.
        </p>
        <p>
          <strong> Drivers not paying attention</strong>: About 330,000
          accidents have taken place due to a driver texting and driving. One
          way to prevent yourself from being injured is making sure you make eye
          contact with driver first before crossing.
        </p>
        <p>
          <strong> Speeding</strong>: Speed limits are there to protect drivers
          and pedestrians alike. According to{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            {" "}
            NHSTA
          </Link>{" "}
          over 9,000 people have died due to a driver speeding. The best way to
          avoid being hit by a speeding driver is by taking a moment to look
          before you cross.
        </p>
        <p>
          <strong> Drunk drivers</strong>: Unfortunately there are drivers who
          have been very irresponsible with alcohol consumption and operating a
          vehicle. A significant amount of pedestrians loose their lives due to
          a drunk driver. The number one thing a pedestrian can do to avoid
          being hit by a driver under the influence is to pay attention.
        </p>
        <h2>Top-Rated Legal Representation</h2>
        <p>
          The experienced Tustin pedestrian accident lawyers at our firm have a
          long and successful track record of protecting the rights of injured
          victims and their families. We can help victims claim compensation for
          damages including medical expenses, loss of wages, cost of
          hospitalization, rehabilitation and other related damages.
        </p>
        <p>
          If you or a loved one has been injured in a pedestrian accident in
          Tustin, please call <strong> 949-203-3814</strong> for a{" "}
          <strong> free case analysis </strong>and speak with an experienced
          Tustin pedestrian accident lawyer.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br />
            Newport Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
