// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/Tustin Dog Bite Lawyers Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Dog Bite Lawyer - Canine Attacks, CA."
        pageDescription="If you have received serious medical attention due to a dog owners carelessness call 949-203-3814 to speak to a Tustin Dog Bite lawyer. The injury attorneys of Bisnar Chase have won millions in compensation for canine attack bite victims. Contact our Orange County office and receive a free case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Dog Bite Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Dog Bite Attorney"
            title="Tustin Dog Bite Attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/dog-bites" target="_blank">
              {" "}
              Dog Bite Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase understand that an animal attack can be physically and
          psychologically traumatizing. We believe that you should be fairly
          compensated for the emotional and catastrophic injuries you have
          experienced from a dog bite.
        </p>
        <p>
          For <strong> 40 years</strong> our personal injury attorneys have been
          helping clients who have endured pain and suffering recover over{" "}
          <strong> $500 Million dollars</strong> in settlements. If you or
          someone you know is seeking compensation for damages from being bitten
          by a dog,<strong> call 949-203-3814 for a free consultation</strong>{" "}
          and to speak to an experienced dog bite lawyer.
        </p>
        <h2>Dog Bite Statute of Limitations</h2>
        <p>
          Time is of the essence to file your dog bite injury claim in
          California.{" "}
          <strong> The statute of limitations for victims is two years.</strong>{" "}
          This statute applies to serious and not so serious dog bites, wounds,
          and scratches to the victim.
        </p>
        <h2>Recovering Costs for Your Losses</h2>
        <p>
          Dog bite victims who suffer severe injuries must go through months of
          treatment that include procedures such as cosmetic surgery. Even with
          such advanced treatments, they may be left with permanent scars or
          disfigurement. The injured party not only faces a painful healing
          process but dog bite victims also face hefty medical bills.
        </p>
        <p>
          The{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=3342."
            target="_blank"
          >
            {" "}
            California Dog Bite Law
          </Link>{" "}
          states: "
          <em>
            The owner of any dog is liable for the damages suffered by any
            person who is bitten by the dog.
          </em>
          "
        </p>
        <p>
          <strong>
            {" "}
            Below are possible damages you may be entitled to as a dog bite
            victim
          </strong>
          :
        </p>
        <ul>
          <li>Plastic surgery</li>
          <li>Dental reconstruction</li>
          <li>Psychiatric or psychological treatment</li>
          <li>Physical therapy</li>
          <li>Past and future hospital or medical costs</li>
        </ul>
        <h2>Children Suffering from Dog Bites</h2>
        <p>
          As a parent watching your child undergo a serious injury can be
          difficult and can invoke a feeling of helplessness. Before seeking out
          legal representation for your little one, there are actions you can
          take beforehand to make the process much easier.
        </p>
        <p>
          <strong>
            {" "}
            3 steps to follow if your child has been bitten by a dog
          </strong>
          :
        </p>
        <p>
          <strong> 1.</strong>{" "}
          <strong> Exchange information with the owner</strong>: After you have
          sought out immediate medical attention and your child is being
          examined by a doctor, trading information with the owner of the dog is
          vital. The reason is that you need to inquire if the dog has the
          necessary shots for illnesses such as rabies. The sooner you know this
          information the quicker you will be able to notify a medical
          professional.
        </p>
        <p>
          <strong> 2.</strong> <strong> Gather evidence</strong>: Putting
          together a portfolio of eyewitness reports and photos would be very
          beneficial for your child's dog bite case. The more physical evidence
          you can provide to your attorney, the better your chances will be of
          gaining compensation.
        </p>
        <p>
          <strong> 3. Hire a dog bite lawyer</strong>: After you have collected
          ample evidence for your suit finding legal representation for your
          child is vital. An experienced dog bite attorney can inform you of
          your rights and can give you an estimate of the amount you will gain
          in a court of law.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog bite Lawyers in Tustin"
            style={{
              backgroundImage: "url('/images/dog-bites/Dog chasing boy.jpg')"
            }}
          >
            <h2>Euthanization and Dog Attacks</h2>
          </div>
        </LazyLoad>
        <p>
          If your dog was the culprit in an attack, euthanization depends on the
          severity of the attack and also if the dog has had a previous dog bite
          incident. As the owner of the dog, you have to understand that you are
          100% liable for the injuries that the victim has endured.
        </p>
        <p>
          Although there are many{" "}
          <Link
            to="/dog-bites/factors-causing-canine-aggression"
            target="_blank"
          >
            {" "}
            factors that can invoke dog aggression
          </Link>{" "}
          a prominent cause can be a rabies infection. By law, a dog must a have
          a rabies vaccination. If the dog is not vaccinated this is a violation
          of the California Penal Code § 398, but because your dog is considered
          your property you can opt out of them having a rabies shot. If you do
          not provide your dog with a rabies vaccination though and they bite
          someone, depending on the state you live in your pet can be
          euthanized.
        </p>
        <h2>Determining the Severity of the Dog Bite</h2>

        <p>
          When evaluating the severity of a dog bite, animal and medical
          professionals have categorized the wounds in six{" "}
          <Link
            to="https://www.usadogbehavior.com/blog/20141096-types-of-dog-bites"
            target="_blank"
          >
            {" "}
            dog bite levels
          </Link>
          . These positions are based on the behavior of the dog, the wound
          inflicted by the dog and also the medical attention that is needed for
          the injuries.
        </p>
        <p>
          <strong> 6 types of dog bite wounds: </strong>
        </p>
        <li>
          <strong> Grade 1</strong>: The dog exhibits hostile behavior which
          usually results in frightening an animal or human.
        </li>

        <li>
          <strong> Grade 2:</strong> A human or animal has been bitten by a dog
          but the skin did not suffer any tears.
        </li>
        <li>
          <strong> Grade 3</strong>: People who have experienced a level 3 bite
          display teeth bites from the dog and the wound is no deeper than half
          of the dog tooth.
        </li>
        <li>
          <strong> Grade 4</strong>: At this stage, one to four injuries have
          been accounted for and those injuries result and bleeding and
          bruising. Th dog will often jerk it's head from each side to apply the
          wound.
        </li>
        <li>
          <strong> Grade 5</strong>: Multiple injuries and aggression can lead
          for the dog to be euthanized at this level. The animal is seen as a
          threat and an extreme danger to people.
        </li>
        <li>
          <strong> Grade 6</strong>: Although it is uncommon, there are fatal
          dog attacks, this is known to be the most severe of dog attacks and
          the animal is immediately euthanized.
        </li>
        <h2>How to Survive a Dog Attack</h2>
        <p>
          There are certain behaviors that may trigger or incline a dog to
          attack you. Do you run or do you fight the dog off? The following
          video will provide you with information on{" "}
          <Link to="/dog-bites/why-dogs-bite" target="_blank">
            {" "}
            why dogs bite
          </Link>{" "}
          you and how you can you defend yourself.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/vX-OOfbnD9w"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Experienced Dog Bite Lawyers</h2>
        <p>
          At Bisnar Chase we have a long and successful track record of helping
          dog bite victims pursue fair and full compensation for their losses.
          We have also helped these victims hold negligent dog owners
          accountable.
        </p>
        <p>
          If you or someone you know has been injured in a Tustin dog bite
          attack please call&nbsp;<strong> 949-556-9602 to </strong>
          <strong>
            {" "}
            speak to top rated injury lawyers specializing in animal bite cases{" "}
          </strong>
          and find out how we can help you pursue your legal rights.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys <br />
            1301 Dove St. #120 <br />
            Newport Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
