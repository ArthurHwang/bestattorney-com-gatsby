// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/Tustin Car Accident Lawyers.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Car Accident Lawyer - Auto Accident Attorney"
        pageDescription="Medical bills that you have accumulated from injuries from a car crash can be expensive. The Tustin Car Accident Lawyers of Bisnar Chase believe that injury victims involved in an accident shouldn't have to pay for someone's negligence. Call 949-203-3814 to speak to an experienced auto accident attorney."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Car Accident Attorneys"
            title="Tustin Car Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you or someone you know has been injured in a car accident the
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/orange-county/car-accidents" target="_blank">
              {" "}
              Car Accident Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase are here to help. Being in a car accident can be
          traumatic and can affect a person both physically and emotionally. The
          car accident attorneys of Bisnar Chase are here to provide relief to
          the pain and suffering a car crash victim has endured. The law firm of
          Bisnar Chase has been <strong> gaining millions</strong> in
          compensation for our clients for over <strong> 40 years</strong>. Our
          attorneys have taken on the toughest of cases and continue to hold
          negligent drivers responsible for costly damages. Contact us today and
          you will receive a<strong> free case evaluation</strong>.{" "}
          <strong> Call 949-203-3814</strong> for highly-rated legal
          representation.
        </p>
        <h2>5 Documents You Need After an Accident</h2>
        <p>
          Taking care of the legal aftermath of a car accident can stem to be
          difficult. There are measures you can take that will help the legal
          process go much smoother. You can raise your chances of winning
          compensation for your losses by providing your attorney with the
          following evidence.
        </p>
        <ul>
          <li>Medical costs</li>
          <li>Accident report</li>
          <li>Witness reports</li>
          <li>Photos at the scene of the car crash</li>
          <li>
            Insurance adjuster names and phone numbers with whom you have spoken
            to
          </li>
        </ul>
        <h2>Car Accident Lawyers Referring Doctors</h2>
        <p>
          Car accident attorneys know that referring doctors or other medical
          professionals to their clients walk on thin ice. The mere appearance
          of impropriety can be a problem if the doctor so recommended
          ultimately benefits from a trial's outcome. That said, there are
          instances where lawyers can legitimately recommend a medical
          professional.
        </p>
        <p>
          One example would be if a client specifically asks for a
          recommendation, and if the lawyer has allowed the client to evaluate
          other payment options and taken prudent steps to prevent the client
          from signing a medical lien (medical bills to be paid from funds
          recovered for an injury). At this point, the lawyer has done his duty
          and allowed the client to choose a doctor without the lawyer's
          influence.
        </p>
        <h2>Medical Referrals Can Be Risky</h2>
        <p>
          People who have been in a car accident face a number of critical
          decisions. A car accident attorney will advise some car crash victims
          towards a certain doctor, chiropractor or other medical practitioner.
          There are times when this is beneficial for the client, but there are
          also instances when steering clients toward a doctor can jeopardize a
          client's case. Doctor referrals made by a lawyer can be harmful to a
          case because the argument of a doctor using deception to demonstrate
          that injuries were more drastic than they actually were can be made.
        </p>
        <p>
          The experienced injury attorneys at Bisnar Chase are knowledgeable
          about the risks involved with medical referrals. Medical attention
          should be immediately sought for after a car accident. If you do not
          have a doctor consult with co-workers, friends or family members for a
          referral. The most important thing is to address your injuries. Get a
          medical and police report. Before you take any information to the auto
          insurance company, please contact a car accident lawyer.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Tustin car crash attorneys"
            style={{
              backgroundImage:
                "url('/images/car-accidents/Tustin emotional distress man.jpg')"
            }}
          >
            <h2>How Can You Prove Emotional Distress?</h2>
          </div>
        </LazyLoad>
        <p>
          The{" "}
          <Link
            to="http://asirt.org/initiatives/informing-road-users/road-safety-facts/road-crash-statistics"
            target="_blank"
          >
            {" "}
            Association for Safe International Road
          </Link>{" "}
          travel reported that over 2.3 million people are injured in a car
          accident every year. Researchers have discovered that over one-third
          of injuries result in mental and emotional distress. Proving a
          physical injury is easy because of the tangible evidence that can be
          brought forth to the courts. Confirming that you have experienced
          feelings of anxiousness, depression or PTSD is not impossible but can
          be challenging. Providing this evidence can help prove that you have
          endured mental pain and suffering after a collision.
          <strong> Three Ways You Can Prove Psychological Trauma</strong>
          1. <strong> Witnesses to your emotional state:</strong> Employers,
          family members or friends can provide statements or written
          communication such as letters can be concrete evidence to your case.
          2. <strong> Psychiatric evaluation</strong>: If you have been
          evaluated by a psychologist or therapist, notes taken can present that
          you have been clinically diagnosed to have a mental condition stemmed
          from the accident. 3. <strong> Medications</strong>: Keeping a record
          of receipts or bottles of prescriptions can help aid your case because
          it presents the mental trauma as tangible.
        </p>
        <h2>The Signs of Psychological Trauma</h2>
        <p>
          Usually, when people are involved in a car accident the priority is
          more than likely to be the physical state of the victim. Having the
          ability to perform day-to-day tasks is important, but having the
          ability to mentally function is vital as well.
        </p>
        <p>
          During a car crash, experts say that adrenaline flows through your
          bloodstream and when these hormones slow down the impact can cause a
          person to feel exhausted, depressed and stressed. Sufferers of car
          incidents can experience{" "}
          <Link
            to="http://floridaphysicalmedicine.com/mental-emotional-injuries-after-car-accident/"
            target="_blank"
          >
            {" "}
            mental and emotional injuries
          </Link>{" "}
          from a car crash such as PTSD, insomnia and deep depression. These
          types of mental traumas can be distinguished by a number of factors.
        </p>
        <ul>
          <li>Crying</li>
          <li>Nightmares</li>
          <li>Lost of Appetite</li>
          <li>Withdrawn from activities</li>
          <li>Sudden mood swings</li>
          <li>Fearful</li>
          <li>Anxiety</li>
        </ul>
        <h2>3 Hidden Symptoms You May Have From a Collision</h2>
        <p>
          Obvious injuries such as open head wounds, broken bones and bruises
          can easily and faster to detect and treat. The harm that is not made
          apparent on the body can take weeks even months to notice and even
          longer to recuperate. Below are the most common hidden injuries that a
          person can obtain after a car accident.
          <strong> Most Common Hidden Injuries</strong>:
          <strong> Whiplash</strong>: Whiplash, otherwise known as a neck strain
          happens when the neck and head are unexpectedly jerked from front to
          back. The movement puts severe stress upon the spine resulting in pain
          in the neck, shoulders and back. Sometimes whiplash takes 24 hours to
          occur and the recovery time can be up to three months.
          <strong>Herniated discs</strong>: It can take weeks to diagnose a
          herniated disk,&nbsp; it can take a very long time for the symptoms to
          appear. A disc is a donut-like cushion that is between the bones of
          the spine. If that disc slips out it dispenses chemicals that push and
          put pressure on the nerves. It takes over 4-6 weeks to heal a
          herniated disc.&nbsp;
        </p>
        <p>
          <strong> Knee injuries</strong>: Usually when a person experiences
          knee pain after a car accident it is a result of the knee roughly
          colliding against the dashboard. Knee injuries brought on by car
          accidents can be deceptive because they can at first emerge as a
          simple soreness and later progress into severe inflammation causing
          serious pain.
        </p>
        <h2>Rain Is the Major Cause of Tustin Motor Wrecks</h2>
        <p>
          In Tustin, the rain has become the leading factor in causing vehicle
          accidents. In late January of last year storms led to trees falling on
          cars, crash collisions and overturns. One fatal accident that occurred
          involved an elderly woman striking a pedestrian and dragging her body
          for half a mile. Police claimed that "blinding rain" may have been the
          culprit in the accident. Each year over 7,000 fatalities have occurred
          due to harsh weather conditions. Between snow, fog and rain, rain is
          to blame for fatalities in over 39 states. States that have had
          fatalities due to rain are represented in the graphic below.
        </p>

        <LazyLoad>
          <img
            src="/images/car-accidents/Tustin Rain Statistics.jpg"
            width="100%"
            alt="Car accident lawyers in Tustin"
          />
        </LazyLoad>

        <center>
          <blockquote>Image courtesy of the Auto Insurance Center</blockquote>
        </center>

        <p>
          Sheldon Drobot of the Boulder, Colorado NCAR shared why the rain
          causes so many accidents. "Rain is always a surprisingly high
          factor... rain can make roads rather slick, especially if it hasn't
          rained in awhile and oil residue builds up...rain can impair
          visibility, leading to trouble," said Drobot. States that have had
          fatalities due to rain are represented in the graphic below.
        </p>
        <p>
          There are precautions you can take so you may have a safe drive in the
          rain.
        </p>
        <p>
          <strong> Safety Tips When Driving in the Rain</strong>
        </p>
        <ul>
          <li>Drive slowly</li>
          <li>Leave enough space for you and the other car</li>
          <li>Switch on your headlights</li>
          <li>Drive a car you know is in good condition</li>

          <li>Do not go out in drive unless you have to</li>
        </ul>
        <h2>Can I Go Back to Work After a Crash?</h2>
        <p>
          The number one question crash victims have after an accident is "can I
          go back to work?" The other question that normally follows is "will
          going back to work affect my case." If you have suffered from injuries
          after a car accident you must seek medical attention immediately. If a
          doctor notifies you (a written document stating you are well is
          necessary) that you still have the ability to work you may return to
          work. This will not hinder your case. Most victims of car accidents
          must take time off of work and worry about the lost wages. Lost wages
          can be compensated for if you have claimed a loss of income.
        </p>
        <h2>Highly-Rated Car Accident Attorneys</h2>
        <p>
          If you were injured in a car accident in Tustin, an experienced Tustin
          car accident lawyer at{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          can offer insight and provide answers to your questions. We do this
          through a free case review and you are under no obligation to choose
          us to represent you. Our car accident attorneys goal is to help you
          take the right steps in getting your case handled properly so that you
          can get the settlement you deserve.
        </p>
        <p>
          The best lawyers offer no charge, no pressure consultations. They can
          help you avoid costly mistakes and ensure that you are fairly
          compensated for your losses.
        </p>
        <p>
          <strong> Please call 949-203-3814 </strong>and receive a
          <strong> free case analysis</strong>.
        </p>
        <p>
          <span>
            Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
            Beach, CA 92660
          </span>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
