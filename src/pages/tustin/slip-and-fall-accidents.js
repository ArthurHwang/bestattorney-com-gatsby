// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "slip-and-fall/santa-ana-slip-fall-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tustin Slip and Fall Attorney - OC Premises Liability Attorney"
        pageDescription="If you have suffered an injury from a slip and fall accident due to a property owner's negligence contact the Tustin Slip and Fall Lawyers of Bisnar Chase. Our premise liability attorneys have been helping injured clients gain compensation for medical bills or lost wages since 1978. Call 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tustin Slip and Fall Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Tustin Slip and Fall Lawyer"
            title="Tustin Slip and Fall Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Tustin{" "}
            <Link to="/orange-county/slip-and-fall-accidents" target="_blank">
              {" "}
              Slip and Fall Lawyers
            </Link>
          </strong>{" "}
          of Bisnar Chase believe that all property owners have a duty to keep
          visitors safe. When a slip and fall accident occurs someone should be
          held responsible for their negligence. For <strong> 40 years</strong>,
          the law firm of Bisnar Chase has gained over{" "}
          <strong> $500 Million dollars</strong> in compensation for clients and
          continues to hold a<strong> 96% success rate</strong>. There are many
          steps that property owners in Tustin can and should take to prevent
          slip, trip and fall accidents, which could result in serious injuries
          and a premises liability lawsuit. If you or someone you know has
          experienced a severe injury due to a slip and fall call{" "}
          <strong> 949-203-3814 </strong>and speak with an experienced{" "}
          <strong> Tustin slip and fall attorney.</strong>
        </p>

        <h2>What Can Cause a Serious Slip and Fall Injury?</h2>
        <p>
          Many slip-and-fall accidents result from a hazardous condition such as
          a slippery floor or broken stair. It is the responsibility of all
          property owners to
          <strong>
            {" "}
            fix the problem within a reasonable time after learning about the
            dangerous condition
          </strong>
          .
        </p>
        <p>
          For example, a spill on a shopping mall floor should be mopped up
          right away or caution tape should be placed around it so visitors are
          warned. It only takes a small puddle or wet spot for a serious
          slip-and-fall injury accident to occur.
        </p>
        <p>
          <strong>
            {" "}
            The following are dangerous conditions that can lead to a serious
            slip and fall injury
          </strong>
          :
        </p>
        <ul>
          <li>Cracked steps</li>
          <li>Damaged railings</li>
          <li>Uneven floors</li>
          <li>Rugs should be placed over torn or ripped carpets</li>
          <li>Elevators that are not leveled</li>
          <li>Burned out bulbs or lights</li>
        </ul>
        <h2>5 Common Slip, Trip and Fall Injuries</h2>
        <p>
          Every year, over eight million people are admitted to the emergency
          room for severe slip and fall injuries. Slip & fall injuries can range
          from a minor sprain to a catastrophic head wound. It is strongly
          recommended that you seek medical attention as soon as possible. Below
          are five injuries slip and fall victims commonly suffer from after an
          accident.
        </p>
        <p>
          1. <strong> Traumatic Brain Injury</strong>: The{" "}
          <Link
            to="https://www.cdc.gov/traumaticbraininjury/get_the_facts.html"
            target="_blank"
          >
            {" "}
            CDC
          </Link>{" "}
          reported that in one year 2.8 million people died, were hospitalized
          and were taken to the emergency room due to a head injury caused by a
          slip and fall accident. A trip and fall is one of the{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="_blank">
            {" "}
            common causes for a brain injury
          </Link>
          . A traumatic brain injury is when the head experiences a sudden blow
          or force that leads to temporary or permanent brain damage.
        </p>
        <p>
          2. <strong> Hip Rupture</strong>: A staggering 90% of hip fractures
          are the consequence of a fall. Even if the fracture is initially
          minor, there is an increased probability that an older man or woman
          will suffer from a more severe hip fracture over the next five years.
          Symptoms of a hip fracture can be a failure to be mobile, intense pain
          in the groin or hip area and stiffness.
        </p>
        <p>
          3. <strong> Spinal Cord and Back Wounds</strong>: According to the{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/spinal-cord-injury/symptoms-causes/syc-20377890"
            target="_blank"
          >
            {" "}
            Mayo Clinic
          </Link>
          , about 15% of serious spinal cord injuries are a result from a fall.
          Falls involving harm to the spinal cord can lead to a slip and fall
          victim to suffer from a herniated disk or paralysis.
        </p>
        <p>
          4. <strong> Dislocation of a Shoulder</strong>: Experiencing a slip
          and fall accident can lead to a severe shoulder injury. If the
          "brachial plexus", a collection of nerves that connects the hand, arm
          and spinal cord, are wounded, in order for the slip and fall victim to
          heal it would either take rehabilitation or surgery.
        </p>
        <p>
          5. <strong> Fractures and Sprains</strong>: Slipping or falling can
          cause multiple fractures, especially in elderly people. One of the
          most common injuries is a knee fracture. When a person falls directly
          on their knee this causes the "patellar" to be immobile, bruised or a
          person can lose the ability to stretch or straighten their leg.
        </p>
        <LazyLoad>
          <img
            src="/images/slip-and-fall/Tustin Slip and Fall Image tiny.jpg"
            width="100%"
            alt="Tustin slip and fall attorney Bisnar Chase"
          />
        </LazyLoad>
        <h2>Prevent Yourself from Being a Slip and Fall Victim</h2>
        <p>
          Property owners are not responsible for all accidents that occur on
          their premises. When someone suffers an injury because of their own
          negligence, they may not be able to seek compensation from their slip
          and fall case.
        </p>
        <p>
          <strong> Therefore, all pedestrians should</strong>:
        </p>
        <ul>
          <li>
            Pay attention to where they are going to avoid hazardous situations.
          </li>
          <li>
            Wear comfortable shoes that have adequate soles to grip the floor.
          </li>
          <li>
            Never send text messages or talk on a cell phone while walking.
          </li>
          <li>
            Use caution when on a property with which they are not familiar.
          </li>
          <li>Avoid areas that are poorly lit or covered in debris.</li>
        </ul>
        <p>
          If you are acting responsibly and you suffer an injury because of a
          hazardous condition, you should immediately:
        </p>
        <ul>
          <li>File an injury report with the property management.</li>
          <li>Collect contact information from the witnesses.</li>
          <li>Take photos of where the incident occurred.</li>
          <li>Seek out medical attention.</li>
          <li>Contact an experienced Slip and fall lawyer.</li>
        </ul>
        <h2>Being a Responsible Property Owner</h2>
        <p>
          Not all hazardous conditions are as easy to fix. Some hazards, such as
          broken steps, require repair work. In such cases, a skilled
          maintenance or construction company may not be available right away.
        </p>
        <p>
          Property owners need to take action to ensure that visitors are made
          aware of hazardous conditions for as long as the problem exists.
        </p>
        <p>
          This means not only posting warning signs around a falling hazard, but
          also checking to make sure that signs are still up later on. Some
          hazards require orange cones and warning tape. In some cases,
          landlords should warn tenants with paper notices or emails so that
          they are properly notified of the dangers they face.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Slip and fall lawyers in Tustin"
            style={{
              backgroundImage:
                "url('/images/slip-and-fall/Tustin slip and fall text-header.jpg')"
            }}
          >
            <h2>Winning Compensation for Your Injuries</h2>
          </div>
        </LazyLoad>
        <p>
          The skilled <strong> Slip and Fall Lawyers </strong>at Bisnar Chase
          have helped injured victims and their families pursue financial
          compensation for their losses.
        </p>
        <p>
          Injured slip & fall victims can seek compensation to cover medical
          expenses, lost wages, cost of hospitalization, rehabilitation and
          other related damages.
        </p>
        <p>
          If you or a loved one has been injured in a slip-and-fall accident,
          please contact us for a <strong> free consultation</strong>. Upon your
          call you will have the opportunity to speak with a top-rated{" "}
          <Link to="/premises-liability">premise liability lawyer</Link>. Call{" "}
          <strong> 949-203-3814</strong> today.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys <br />
          1301 Dove St. #120 <br />
          Newport Beach, CA 92660
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">1301 Dove St., #120</div>
                <div>
                  <span itemProp="addressLocality">Newport Beach</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">92660</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(949) 203-3814</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1567375815541!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
