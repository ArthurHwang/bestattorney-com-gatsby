// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/motorcycle-accident-los-angeles-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Motorcycle Accident Lawyers Los Angeles - Biker Attorneys, CA"
        pageDescription="Motorcycle injuries can often be permanent and disabling. Get your life back and have the Los Angeles Motorcycle Accident Lawyers of Bisnar Chase fight for you. This top-rated law firm has been representing and winning motorcycle crash victims for over 40 years. Call for your Free Consultation  323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Motorcycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles motorcycle accidents lawyer "
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have been involved in a crash, contact our{" "}
          <strong>
            {" "}
            Los Angeles{" "}
            <Link to="/los-angeles/motorcycle-accidents">
              motorcycle accident attorneys{" "}
            </Link>
          </strong>{" "}
          and have experience and skill fight for your rights. We'be helped over
          12,000 clients recover from an injury physically and financially. We
          have the resources to get the job done.{" "}
          <Link to="/los-angeles/contact">Get a free consultation today. </Link>
        </p>
        <p>
          Harley Davidsons, Choppers, cruisers, street bikes, enduros; it
          doesn't matter as long as you have the throttle in your hand and the
          wind in your face. Motorcycles attract everyone from thrill seekers
          with a desire to travel cross country and through the winding 2-laned
          mountain highways, to the daily rider just trying to get through some
          heavy traffic, it's an addicting culture.  But beware, it's one of the
          most dangerous activities possible.
        </p>
        <h2>
          What Are the First Things I Should do After a Motorcycle Accident in
          Los Angeles?
        </h2>
        <p>
          Making sure you seek medical attention immediately is important to
          your health and your case by documenting your injuries in association
          with the accident for proof throughout the case. If you don't have
          documented proof an injury occurred due to the accident, it never
          happened.
        </p>
        <p>
          Exchanging insurance, identification and contact information with
          everyone associated with the accident. Contacting a Los Angeles
          motorcycle accident attorney immediately to discuss the specifics of
          your case. Make sure you write down as much information right after
          the accident so you don't forget important details later on that may
          be important to your claim. Take photographs and file a police report
          immediately with LAPD.
        </p>
        <h2>
          Why do I Need a Lawyer After a Motorcycle Accident in Los Angeles?
        </h2>
        <p>
          Having strong legal representation on your side is an important
          benefit. Not only do the motorcycle accident attorneys at{" "}
          <Link to="/">Bisnar Chase </Link> have a strong knowledge in
          motorcycle accident laws, history and protocol, but we have
          established a distinct and respected reputation within the courtroom
          and entire legal system. We've spent decades in Los Angeles court
          rooms and are very familiar with LA defense teams. Those that have
          gone up against Bisnar Chase experience first hand how hard we fight
          for our clients.
        </p>
        <h2>What If the Other Driver Doesn't Have Insurance?</h2>
        <p>
          Your policy should always have uninsured motorist coverage because if
          the other party has no assets and no coverage then there is no one to
          go after. You would want to do an asset search to find out if they own
          property or have income that you could perhaps go after in lieu of
          having Motorcycle coverage.
        </p>
        <h2>
          How Can I Get Help If it was a Road Condition that Caused my Accident?
        </h2>
        <p>
          Contact Bisnar Chase and one of our skilled premise liability or
          governmental entity attorneys can decide whether you have a case or
          not. Depending on your situation, the type of case you have can
          slightly differ.
        </p>
        <h2>
          How do I Know If my Situation is a Premise Liability or Governmental
          Entity Case?
        </h2>
        <p>
          Premise liability cases consist of hazardous or dangerous conditions
          on privately owned establishments, residential or commercial
          properties. Governmental entity cases consist of dangerous or
          hazardous conditions to city streets, airports, municipal
          transportation, etc.
        </p>
        <p>
          Lastly, there are possible scenarios for the two to combine and create
          both a governmental entity and premise liability case. A possible
          situation could consist of a property owner who is having construction
          done on their home has a dangerous piece of material fall onto city
          property (sidewalk or alleyway) and a bicyclist turns the corner,
          crashes and becomes injured.
        </p>
        <p>
          The property owner has an obligation to create a hazard free zone to
          any public interaction with their property, and the city has a
          responsibility to ensure all municipal and public access is free of
          hazardous materials and dangerous conditions.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Motorcycle danger"
            style={{
              backgroundImage:
                "url('/images/text-header-images/why-are-motorcycles-dangerous-los-angeles.jpg')"
            }}
          >
            <h2>Why are Motorcycles Dangerous?</h2>
          </div>
        </LazyLoad>

        <p>
          Every year, 50,000 motorcyclists are injured and an average of 2,000
          are killed in collisions. Over 80% of all motorcycle accidents result
          in injury or death. If you ride, first and foremost, be careful.
        </p>
        <p>
          Most people preach when or if they become a motorcycle rider, they are
          skilled, cautious and on the lookout for all road hazards possible.
          The reality is, when suddenly presented with a road hazard, you have
          only moments, many times milliseconds to react, and sometimes that is
          just not enough time to avoid danger or potentially catastrophic
          disaster.
        </p>
        <h2>
          Are There any Special Protection Laws for Motorcyclists in California?
        </h2>
        <p>
          Motorcyclists have to adhere to the same safety laws and car drivers
          however extra caution should be paid to motorcycle riders especially
          when lane splitting is legal. Every year thousands of bikers are
          injured and car drivers need to pay extra attention to the presence of
          bikers behind or next them. The California DMV has a motorcycle
          handbook on{" "}
          <Link
            to="https://www.dmv.ca.gov/portal/dmv/?1dmy&urile=wcm:path:/dmv_content_en/dmv/pubs/dl655/mcycle_htm/preparing"
            target="_blank"
          >
            {" "}
            Preparing to Ride
          </Link>{" "}
          that can help answer some very important safety questions.
        </p>
        <h2>
          What do I Need to Bring to my Initial Consultation with a Los Angeles
          Motorcycle Injury Lawyer?
        </h2>
        <p>
          Bring as much evidence with you as you can because the more you can
          tell and show your attorney the better he will be able to understand
          the circumstances of your case. Typically, you'll want to have
          pictures of the accident witness reports and a police report for an
          initial consultation. If you don't have all of those items just tell
          your attorney as much as you can.
        </p>
        <h2>
          How Does Your Firm Stand Out When Helping People Through Their
          Motorcycle Accident Cases?
        </h2>
        <p>
          Bisnar Chase has accepted many cases that had already been turned down
          due to the other firms lacking the skill, confidence and ability to
          win the cases. The injury attorneys at Bisnar Chase never back down
          from a difficult fight, we cover all costs throughout the case, and
          you don't pay unless we win your case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9nqLzrBI8FE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Hazardous Scenarios Motorcycle Riders Encounter</h2>
        <p>
          Even if you feel you have cat-like reflexes and can dip and dodge your
          way out of any scenario, here are some possible dangers you may
          experience while riding a motorcycle.
        </p>
        <ul>
          <li>
            Other vehicles: Vehicles pulling out from blind spots, running red
            lights, stop signs or the situation of a moving obstruction suddenly
            impacting the path of a moving motorcycle. Because you only have
            moments to respond when faced with a sudden obstruction.
          </li>
          <li>
            Road hazards: This ranges from debris in the street like items that
            have fallen out of trucks, like ladders, couches, desks, broken
            wood, metal and other potential hazards. Sometimes you don't get a
            chance to see hidden road hazards.
          </li>
          <li>
            Traveling on the{" "}
            <Link to="/los-angeles/freeway-accident-lawyers" target="new">
              freeway{" "}
            </Link>{" "}
            or fast-moving roadways behind vehicles offers the chance of the
            vehicle in front of you blocking your early awareness of the hazard,
            running the item over and having it kicked up airborne in front of
            you, making it many times impossible to avoid.
          </li>
          <li>Oil, ice or any potentially slippery surfaces.</li>
          <li>
            Potholes, cracks or any other road damage motorcycle tires can have
            issues with.
          </li>
          <li>
            The ability of a substance hitting you and blocking your vision;
            your goggles, helmet, or eyes directly.
          </li>
          <li>Poor weather like rain, snow, sleet, hail, fog and black ice.</li>
          <li>
            Poor motorcycle condition is also a hazard. This could potentially
            lead to flat tires or blow outs, engine failure, sudden stalls,
            parts falling off and other issues that could lead to a sudden
            crash.
          </li>
        </ul>
        <h2>Recent Motorcycle Accident Case Results</h2>
        <ul>
          <li>$30,000,000.00&nbsp;- Motorcycle Accident</li>
          <li>$3,000,000.00&nbsp;- Motorcycle Accident</li>
          <li>$1,000.000.00 MVA - Motorcycle Accident</li>
        </ul>
        <p>
          More than $500 Million recovered and over 12,000 clients served. We've
          represented Los Angeles accident victims for over three decades and we
          may be able to help you too. Our experienced legal team will provide
          you with first class personal attention and take care of every detail
          of your motorcycle accident claim. From settlements to trial, we will
          have your back every step of the way. Our reputation speaks for itself
          with our 4.8 rating&nbsp;{" "}
          <Link to="/about-us/testimonials">
            <strong> reviews from satisfied clients</strong>
          </Link>
          .
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Motorcycle safety"
            style={{
              backgroundImage:
                "url('/images/text-header-images/are-cars-safer-than-motorcycles-accidents-attorneys.jpg')"
            }}
          >
            <h2>Are Cars Safer Than Motorcycles?</h2>
          </div>
        </LazyLoad>
        <p>
          When traveling on the road, according to{" "}
          <Link to="http://www.iii.org/" target="new">
            www.iii.org
          </Link>
          , the odds of a car occupant dying in a transportation accident were 1
          in 47,718 in 2013; the lifetime odds were 1 in 60 for a person born in
          2013.
        </p>
        <p>
          When inside a vehicle, you are mostly protected from debris, rain, and
          other hazards, you have a metal barrier surrounding you and is
          equipped with seatbelts. Unlike a car, you and your motorcycle are
          vulnerable to the elements, debris, being thrown from your bike and
          immediate danger with zero protection.
        </p>
        <h2>
          Los Angeles Motorcycle Accidents are Caused by a Number of Issues
          Including;
        </h2>
        <ul>
          <li>Motorist negligence</li>
          <li>Hazardous road issues</li>
          <li>
            {" "}
            <Link to="/los-angeles/hit-and-run-accidents">Hit and run </Link>
          </li>
        </ul>
        <p>
          If you've come out of a motorcycle crash with just a few scrapes then
          consider yourself very lucky. Most motorcycle accident cases we see
          are very serious with catastrophic injuries and even death. With
          little protection that a car would afford, the injuries tend to be
          serious in most cases.
        </p>
        <p>
          If you've had a serious motorcycle accident and don't know where to
          turn, contact us to speak with award winning accident attorneys free
          of charge. We can evaluate your accident details to determine if you
          are entitled to compensation.
        </p>
        <p>
          You need to call a motorcycle accident attorney who can plan for the
          many turns in the legal system, the defendants' lawyers and the red
          tape created by insurance companies. Call 323-238-4683 for a free
          consultation with our passionate and professional legal team.
        </p>
        <p>
          You may have a case for any number of reasons: carelessness on the
          part of another driver, unsafe roadways, or product defects. Winning a
          motorcycle accident case is complex. A{" "}
          <Link to="/los-angeles">
            <strong>Los Angeles personal injury attorney</strong>
          </Link>
          &nbsp;at Bisnar Chase has the knowledge to effectively prepare your
          case.
        </p>
        <h2>You Don't Pay Anything Unless We Win</h2>
        <p>
          We've had great success with motorcycle accident cases for our
          California clients. Additionally we protect you from liability while
          we handle your case. This allows you to get back to getting better.
          Our promise to you is that we will not charge you a fee unless we
          recover for you. Simple as that. We don't win, you don't pay.
        </p>
        <h2>
          High-risk Accidents Require Highly Qualified Motorcycle Accident
          Attorneys
        </h2>
        <p>
          Motorcycle accidents usually come with an attitude from defense
          attorneys and insurance companies. Most assume the biker is always at
          fault no matter what. But the truth is that the motorcycle rider is
          usually the victim.
        </p>
        <p>
          Riders being cut off by impatient motorists who rarely give a double
          look before they merge or change lanes are an unfortunate aspect of
          the world of riding motorcycles. Fighting for the no-fault biker is
          something we've always done. We don't represent the other side under
          any circumstances. A plaintiff's lawyer has the perspective needed to
          help you get fair compensation.
          <strong> Call 323-238-4683</strong> for your{" "}
          <strong> Free Case Evaluation </strong>with our team of{" "}
          <strong> Los Angeles Motorcycle Accident Attorneys.</strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
