// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import Helmet from "react-helmet"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/toxic-chemicals-clean-up-crew-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <Helmet>
        <meta name="robots" content="noindex" />
      </Helmet>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Toxic Chemical Injury Lawyer | Toxic Tort Attorneys"
        pageDescription="Poisoning, burns & personal injuries can result from negligent use & disposal of dangerous chemicals. The Los Angeles Toxic Chemical Injury Lawyers of Bisnar Chase have won over $500 Million & have obtained a 96% success rate, due to our attorneys skill & dedication. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Toxic Chemical Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles toxic chemical injury lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        {/* <h2>Rights of Injured Victims</h2> */}
        <p>
          If you or a loved one has been injured as a result of toxic chemical
          exposure, the experienced Los Angeles toxic chemical injury lawyers at
          Bisnar Chase can help you better understand your legal rights and
          options. We have the knowledge and resources it takes to assemble
          evidence including expert testimony that is imperative in toxic tort
          cases. Please contact one of our top rated{" "}
          <Link to="/los-angeles">personal injury lawyers </Link> to obtain more
          information about pursuing your legal rights.
        </p>

        <p>
          Los Angeles Victims of toxic chemical exposure would be well advised
          to explore their legal options with a toxic tort attorney that
          understands this complicated area of law. Financial support may be
          available for their medical bills, lost wages, loss of livelihood,
          pain and suffering and other related damages. How much compensation is
          available and who can be held accountable depends on the cause of the
          exposure and the facts of the case.
        </p>

        <p>
          Injured Los Angeles workers can pursue financial support for their
          medical bills and a portion of their lost wages through workers'
          compensation benefits. They will have to prove that they were exposed
          to the chemicals while working and that the exposure resulted in an
          injury or illness. Depending on the circumstances of the exposure, it
          may be possible to also file a third-party claim to receive additional
          compensation for damages.
        </p>
        <h2>Top 5 Most Common Injury Causing Chemicals</h2>
        <p>
          According to the{" "}
          <Link
            to="https://www.cdc.gov/mmwr/preview/mmwrhtml/ss6402a6.htm"
            target="new"
          >
            Centers for Disease Control and Prevention
          </Link>
          , Four of the top five chemicals (ammonia, chlorine, hydrochloric
          acid, and sulfuric acid) have been documented as being the most
          frequently reported releases involved in injuries and evacuations.
        </p>
        <p>
          One quarter of all 354 deaths during this time period were
          attributable to these five chemicals. These five chemicals were also
          the top five chemicals released in the top five industries that
          resulted in injuries.
        </p>
        <ol>
          <li>Carbon Monoxide</li>
          <li>Ammonia</li>
          <li>Chlorine</li>
          <li>Hydrochloric Acid</li>
          <li>Sulfuric Acid</li>
        </ol>
        <h2>How Toxic Chemicals Enter Your Body</h2>
        <p>
          A chemical is considered toxic if it is harmful to human health when
          inhaled, swallowed or absorbed through the skin. There are naturally
          occurring toxins that are found in certain plants and synthetic toxic
          chemicals that are man-made. You may be exposed to dangerous toxins
          every time you clean a drain, polish furniture, paint the walls or
          kill the bugs in your yards. Individuals are at risk for toxic
          exposure on the job or even at home. There are three ways in which a
          toxic chemical can get into your body and cause harm: through the air,
          your skin and your mouth. When you breathe in a chemical through your
          nose and mouth, it can cause immediate damage to your lungs. You can
          also absorb toxic chemicals into your body through you skin or your
          eyes. If a chemical is on your hands and you touch food, that chemical
          may get inside your mouth and travel throughout your body.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/hazmat-team-railroad-tracks-square.jpg"
            width="100%"
            alt="hazmat-team-railroad-tracks-square.jpg"
          />
        </LazyLoad>

        <h2>Health Issues Related to Toxic Exposure</h2>
        <p>
          It is common for victims of toxic chemical exposure to suffer symptoms
          such as itchy skin and eyes, coughing, sore throat, runny nose and
          sneezing. This type of health issue will usually go away shortly after
          you are away from the chemical. You may also experience allergy-like
          symptoms every time you are near a dangerous chemical. In severe
          cases, the allergic reaction may result in asthma or breathing
          problems.
        </p>
        <h3>Other health issues related to toxic chemical exposure include:</h3>
        <ul>
          <li>
            <b>Burns</b>: You could suffer burns on the outside or inside of
            your body depending on how you are exposed to the chemical.
          </li>
          <li>
            <b>Cancer</b>: Different chemicals can cause cancer of the lungs,
            liver, skin, blood, bone marrow and other body parts.
          </li>
          <li>
            <b>Organ Damage</b>: Some chemicals can cause immediate poisoning
            that makes the victim very ill. Others cause slow, severe damage to
            the inside of the body.
          </li>
          <li>
            <b>Sexual and reproductive health issues</b>: The sperm of a man and
            a woman's ability to conceive a child can be affected by toxic
            exposure. In cases where a woman is exposed to chemicals while
            pregnant, the baby may have serious health issues.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Examples of Dangerous Chemicals"
            style={{
              backgroundImage:
                "url('/images/text-header-images/types-of-dangerous-chemicals.jpg')"
            }}
          >
            <h2>Examples of Dangerous Chemicals</h2>
          </div>
        </LazyLoad>

        <p>
          There are many different toxic chemicals that you may come into
          contact with at work or even at home. Here are a few examples of
          harmful chemicals and the symptoms they can cause:
        </p>
        <ul>
          <li>
            <b>Ammonia</b>: This is a volatile compound that can irritate the
            respiratory system when inhaled.
          </li>
          <li>
            <b>Antifreeze</b>: This is a chemical that is poisonous if swallowed
            resulting in brain, heart, kidney and other organ damage.
          </li>
          <li>
            <b>Asbestos</b>: This is a mineral that was used for decades to make
            insulation. It does not cause any short-term health effects, but it
            can develop into the fatal lung disease, mesothelioma.
          </li>
          <li>
            <b>Bisphenol A</b> (BPA): This is a building block of lightweight
            plastic products found in water bottles, baby bottles, jar lids and
            food containers. The majority of people in the United States are
            exposed to BPA with little to no effect. There are studies, however,
            that show that factory workers exposed to BPA may have issues with
            their brain, behavior and prostate gland.
          </li>
          <li>
            <b>Bleach</b>: Household bleach is regularly found in cleaning
            products. It can cause irritation to the skin when touched and
            damage to the respiratory system when inhaled.
          </li>
          <li>
            <b>Formaldehyde</b>: Workers in industries that treat fabric, cast
            metal and process plastic can cause formaldehyde exposure. Symptoms
            may include cancer, irritation, rashes and blindness.
          </li>
          <li>
            <b>Lead</b>: Lead is used in paints, pigments and metal pipes. Lead
            poisoning symptoms can range from stomach aches and headaches to
            kidney damage and cancer.
          </li>
          <li>
            <b>Phthalates</b>: This is a family of chemicals that soften
            plastic. These dangerous toxic chemicals are found in soap,
            shampoos, conditioners, hair sprays and perfumes. Studies have shown
            a connection between phthalate exposure and male sexual development
            issues.
          </li>
          <li>
            <b>Sodium hydroxide</b>: Exposure to the lye that is commonly found
            in drain cleaners can result in extremely serious chemical burns.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/uPntGkMcJxg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Toxic Exposure in Los Angeles</h2>
        <p>
          A recent years{" "}
          <Link
            to="http://losangeles.cbslocal.com/2014/01/09/study-lax-neighbors-may-be-exposed-to-toxic-particles-from-airport-activity/"
            target="_blank"
          >
            {" "}
            study
          </Link>{" "}
          found that residents may be exposed to tiny particles from airport
          toxins that affect the lungs. These toxic{" "}
          <Link
            to="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5129264/"
            target="new"
          >
            Ultrafine Particles (UFPs)
          </Link>{" "}
          are much higher near airports than urban communities. The Southern
          California Particle Center at UCLA found that this exposure may
          contribute to cardiovascular disease for some Los Angeles residents.
          Hazardous waste is another concern in a county as large as LA.
        </p>
        <p>
          {" "}
          <Link to="https://www.epa.gov/mold" target="new">
            Toxic mold
          </Link>{" "}
          in older Los Angeles homes and buildings is another area for concern
          due to the serious health issues from toxic molds. Landlords who
          knowingly rent toxic spaces to tenants could be liable for damages.
          Toxic exposure at jobsites is fairly common and people suffering ill
          effects not only need medical evaluations but legal advice to protect
          themselves.
        </p>
        <h2>Skilled Lawyers Who Care and Win</h2>
        <p>
          The experienced <strong> Los Angeles personal injury lawyers</strong>{" "}
          at Bisnar Chase can help you in determining if you have a case because
          of exposure to hazardous or toxic chemicals, and will help you proceed
          with legal action if you do. Give us a call today for a free case
          evaluation,<strong> 323-238-4683 </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
