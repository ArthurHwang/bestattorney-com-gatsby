// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/pedestrian-crosswalk-los-angeles.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Pedestrian Accident Lawyers - Crosswalk Accident Attorneys"
        pageDescription="Drunk or distracted driving often results in walkers & bicycle riders getting run over. The Los Angeles Pedestrian Accident Lawyers are dedicated to winning for our injured clients or you do not pay. Our injury attorneys know the law & have over 40 years of experience. Call for your Free Consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Pedestrian Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Injured pedestrians in Los Angeles"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Growing up as a child, you're taught to look both ways before crossing
          a street and sometimes that is not enough to keep you safe, but the
          experienced
          <strong> Los Angeles Pedestrian Accident Lawyers</strong> at our firm
          have a long and successful track record of helping injured victims and
          their families obtain fair compensation for their losses. Insurance
          companies may not have your best interest.
        </p>
        <p>
          Please contact our Pedestrian Accident Attorneys
          <strong> today for a Free Case Evaluation </strong>and comprehensive
          consultation if you've been hurt in a pedestrian accident or have lost
          a loved one from a wrongful death in the Los Angeles area. The call
          and consultation are <strong> Free</strong> and your compensation may
          be waiting.&nbsp;For immediate assistance
          <strong> call 323-238-4683</strong>.
        </p>
        <h2>Dangerous Drivers in Los Angeles County</h2>
        <p>
          Driver negligence is one of the most common causes of pedestrian
          accidents in Los Angeles. Examples of driver negligence that can
          result in Los Angeles pedestrian accidents to occur include:
        </p>
        <ul>
          <li>
            <strong> Excessive speed</strong>: Drivers who are traveling too
            fast will not be able to avoid a collision with a pedestrian or stop
            in time for a red light.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Driving drunk</strong>: Motorists who are under the
            influence of alcohol may have impaired reaction times. This means
            that they may struggle to react in time to avoid striking a
            pedestrian.
            <ul>
              <li>
                Drunk driving is clearly a problem because in recent years,{" "}
                <strong> 79 fatalities </strong>and{" "}
                <strong> 1,989 injuries </strong>occurred as the result of drunk
                driving collisions in the city of Los Angeles. Many of those
                victims were pedestrians.
              </li>
            </ul>
          </li>
          <li>
            <strong> Driving distracted</strong>: Motorists who have their hands
            or eyes on a phone may not even notice a pedestrian on the roadway
            until it is too late.
          </li>
        </ul>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/o1TVRpjC36I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Dangerous drivers are not the only reason why walking in Los Angeles
          is dangerous. Los Angeles is a large city that has many areas that are
          poorly designed and maintained. Potentially dangerous intersections
          may have:
        </p>
        <ul>
          <li>
            Inadequate line of sight for speed limit postings and stop signs.
          </li>
        </ul>
        <ul>
          <li>
            Faded stop, yield and crosswalk signs. It is the responsibility of
            the government to ensure that roadways and signs are properly
            maintained.
          </li>
        </ul>
        <ul>
          <li>
            Poorly coordinated traffic signals. Some intersections are confusing
            for pedestrians and many simply do not allow enough time for
            pedestrians to safely cross.
          </li>
          <li>Traffic signals in disrepair.</li>

          <li>
            Poor design. Is the intersection inherently dangerous because of
            where it's located?
          </li>
        </ul>
        <ul>
          <li>Lack of crosswalks.</li>
        </ul>
        <ul>
          <li>
            Dangerous crosswalks that are hazardous due to their location or
            because they lack lights or visible signs to warn motorists.
          </li>
        </ul>
        <p>
          {" "}
          <Link to="/los-angeles">
            <strong> Personal injury attorneys</strong>
          </Link>
          &nbsp;are seeing more pedestrian accidents cases said John Bisnar..
          "There are a lot of transportation options in Los Angeles and that
          included many pedestrians on bicycles and walking. The more congested
          the city becomes with commuters, the more injuries we see to those
          walking and dealing with aggressive drivers."
        </p>

        <h2>Is It Safe to Ride Bicycles in Los Angeles?</h2>
        <p>
          Bicycle accidents and{" "}
          <Link to="/los-angeles/scooter-injury-lawyers" target="_blank">
            {" "}
            scooter collisions
          </Link>{" "}
          are more prevalent in Los Angeles than anywhere else in the country.
          Many bicycle riders and pedestrians are cautious of vehicle traffic
          and reckless speeding drivers, but don't always have the opportunity
          to see danger coming.
        </p>
        <p>
          Bicycle riders and joggers who share the road with automobiles and
          motorcycles have a larger chance of getting struck, rundown, seriously
          injured or killed than the majority of pedestrians who walk sidewalks
          and public pathways that are in close proximity to roadways.
        </p>
        <p>
          If you have been hit by a car while riding a bicycle, contact our Los
          Angeles pedestrian accident attorneys at{" "}
          <strong> 323-238-4683</strong>. To learn more about how to avoid hit
          and runs,{" "}
          <Link to="http://www.bicyclesafe.com" target="new">
            {" "}
            click here
          </Link>
          .
        </p>
        <h2>Ways Pedestrians Can Get Hit in Los Angeles</h2>
        <p>
          Besides a vehicle running someone down in a crosswalk or by hopping a
          sidewalk, there are many other ways pedestrians can get hit:
        </p>
        <ul>
          <li>
            <strong> Walking behind a car when it is backing up</strong>: When
            you see bright white lights on the back of a vehicle, this means the
            vehicle is currently in reverse and most likely about to start
            reversing. Avoid walking behind a car with reverse lights on.
          </li>

          <li>
            <strong> Not using a crosswalk: </strong>Use a crosswalk
            appropriately when attempting to cross a roadway. Vehicles are not
            expecting pedestrians to cross in the middle of the road. Also, try
            not to run through a crosswalk to beat the oncoming traffic. You
            risk getting run down, could fall and hurt yourself or could cause a
            car accident due to approaching vehicles having to suddenly stop.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Eye Contact: </strong>Cars yielding to moving traffic on a
            right turn: when a car is pulled out on a right corner, the driver
            is most likely waiting for a break in the oncoming traffic so they
            can proceed with their right turn.
            <ul>
              <li>
                Their attention is most likely focused to the left side of the
                car watching the vehicles coming towards them and are most
                likely not expecting to see a pedestrian walking in front of
                their car when they look forward to proceed with their right
                turn.
                <ul>
                  <li>
                    Wait to make eye contact with the driver so you are
                    confident in their awareness of your location in front of
                    their car.
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/hollywood-pedestrian-sunset-strip-square.jpg"
            width="100%"
            alt="pedestrian accident lawyers"
          />
        </LazyLoad>
        <h2>Pedestrians Can Cause Car Accidents</h2>
        <p>
          Vehicles have dangers of their own and don't need any hindrance:{" "}
          <em>
            a thing that provides resistance, delay, or obstruction to something
            or someone.{" "}
          </em>
          When you see a vehicle or motorcycle approaching, wait for them to
          pass, make the turn or whatever maneuver they are preparing for. If
          you bolt out in front of a vehicle approaching, not only do you risk
          getting hit, but you risk putting them in danger as well.
        </p>
        <p>
          If a pedestrian runs out when a vehicle is approaching and they must
          suddenly brake for the pedestrian, they could be rear ended, cause a
          traffic jam which could result in multiple vehicle related accidents,
          and could potentially cause severe property damage, serious injury or
          catastrophic results.
        </p>
        <p>
          If you have been involved in a{" "}
          <Link to="/los-angeles/car-accidents">car accident </Link> in result
          of a pedestrian, call and speak with one of our Los Angeles pedestrian
          accident attorneys for a <strong> free consultation</strong>.
        </p>
        <h2>Los Angeles Pedestrian Accident Dangers</h2>
        <p>
          A recent magazine report listed Los Angeles as third among the most
          dangerous cities in the world for pedestrians. According to the
          report, for every <strong> 100,000 pedestrians </strong>in Los
          Angeles, there are <strong> 7.64 fatalities </strong>suffered in
          pedestrian accidents.
        </p>
        <p>
          Why is Los Angeles ranked only below Atlanta and Detroit? While there
          are a number of roadways in Los Angeles&nbsp;that can be conducive for
          pedestrians, dangerous drivers and hazardous roadways and
          intersections can make walking risky in this city.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles, California Crosswalk Laws"
            style={{
              backgroundImage:
                "url('/images/text-header-images/california-crosswalk-laws.jpg')"
            }}
          >
            <h2>Los Angeles, California Crosswalk Laws</h2>
          </div>
        </LazyLoad>
        <p>
          Under California laws, drivers are required to yield the right-of-way
          to pedestrians in crosswalks. Drivers are also required to exercise
          due care when it comes to pedestrians. Here are some of the laws
          relating to drivers and pedestrians:
        </p>
        <ul>
          <li>
            <strong> At crosswalks</strong>: Under{" "}
            <Link
              to="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?sectionNum=21950.&lawCode=VEH"
              target="new"
            >
              {" "}
              California Vehicle Code 21950
            </Link>
            , drivers must yield the right-of-way to pedestrians legally
            crossing the roadway "within any marked crosswalk or within any
            unmarked crosswalk at an intersection."
          </li>
        </ul>
        <ul>
          <li>
            <strong> Outside of crosswalks</strong>: Under{" "}
            <Link
              to="http://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=VEH&sectionNum=21954."
              target="new"
            >
              {" "}
              California Vehicle Code 21954
            </Link>
            , pedestrians attempting to cross the road outside a crosswalk and
            away from an intersection "shall yield the right-of-way to all
            vehicles upon the roadway." This vehicle code does not, however,
            excuse drivers from failing to exercise due care when it comes to
            avoiding striking pedestrians.
          </li>
        </ul>
        <h2>Los Angeles Pedestrian Safety Tips</h2>
        <p>
          Even pedestrians who are obeying the law can be involved in an
          accident if a driver is behaving negligently. Here are a few tips to
          help you avoid being injured in a Los Angeles pedestrian accident:
        </p>
        <ul>
          <li>
            <strong> Walk sober</strong>: Walking while intoxicated can be as
            dangerous as driving under the influence. According to The Governors
            Highway Safety Council, one third of pedestrians killed nationwide
            have blood alcohol levels of .08 percent or higher.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Wear bright clothing</strong>: When walking late at night,
            it is important to wear bright clothing and to carry a flashlight.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Walk toward traffic</strong>: If there is no sidewalk, you
            should walk to the far left side of the road facing traffic.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Stay attentive</strong>: It is easy to forget that drivers
            can pull out of driveways at any moment. Keep your phone in your
            pocket and your eyes on your surroundings.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Never enter traffic from between cars</strong>: Motorists
            will have a difficult time seeing you if you are standing between
            parked vehicles or if you walk into the roadway from between parked
            cars.
          </li>
        </ul>
        <ul>
          <li>
            <strong> Never dart into traffic</strong>: This is extremely
            dangerous and could result in serious pedestrian injuries.
          </li>
        </ul>
        <h2>Injuries Suffered in Los Angeles Pedestrian Accidents</h2>
        <p>
          Victims of pedestrian accidents typically have to miss work while
          healing. In some cases, they are never able to return to work or at
          least to earn what they were once capable of earning. Losses may also
          include medical bills such as hospitalization, the cost of
          rehabilitation services, prescription drugs, medical expenses relating
          to devices, lost wages, pain and suffering and emotional distress.
        </p>
        <p>
          Fortunately, compensation may be available for all of these losses if
          the at-fault driver's negligence contributed to the crash.
        </p>
        <h2>Getting Fair Compensation for Your Losses</h2>
        <p>
          If you have been injured in a pedestrian accident, you could seek
          compensation for damages from the at-fault parties such as negligent
          drivers or governmental agencies that allow dangerous conditions to
          exist on the roadway.
        </p>
        <p>
          Any personal injury claim against governmental entities must be filed
          within 180 days of the incident. Your insurance policy may not cover
          you fully for damages. Contact a top rated personal injury lawyer to
          preserve your rights.
        </p>
        <p>
          If you or a loved one has been injured in a pedestrian accident,
          please call and ask to speak with one of our highly skilled and
          experienced Los Angeles pedestrian accident lawyers for
          <strong> free at 323-238-4683.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
