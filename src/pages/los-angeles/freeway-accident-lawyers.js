// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/freeway_crash_resized.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Freeway Accident Lawyers in Los Angeles - Bisnar Chase"
        pageDescription="Contact the freeway accident lawyers of Bisnar Chase if you are a no-fault victim of a car crash in Los Angeles. Over $500M recovered, 96% success rate. Call 323-238-4683 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Freeway Accident Lawyers in Los Angeles</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Freeway accident lawyers in Los Angeles at Bisnar Chase"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Every day, the freeways around Los Angeles are flooded with traffic.
          These routes act as the primary paths into and around the city, and
          they are always extremely busy. Whether it is a part of your work
          commute or a trip to a neighboring city, freeway driving is a normal
          part of the day for many people. Unfortunately, freeway accidents are
          also very common. Anyone who drives on freeways frequently will have
          driven past the wreckage of a{" "}
          <Link to="/los-angeles/car-accidents" target="new">
            vehicle crash
          </Link>{" "}
          at the side of the road on plenty of occasions.
        </p>
        <p>
          A freeway accident can be a life-changing event. The same can be said
          for a crash that occurs on any surface street – even a minor
          fender-bender or red-light run-in can leave an unsuspecting driver in
          pain or facing financial struggles. But a crash on a freeway is far
          more likely to result in major vehicle damage, a serious injury, or
          even a fatality. If the worst happens and you or a loved one are
          involved in a traffic collision in or around Los Angeles, our team can
          help. Bisnar Chase’s{" "}
          <b>expert freeway accident lawyers in Los Angeles</b> can provide the
          help you need in the aftermath of any incident.
        </p>
        <h2>Freeway Dangers in Los Angeles</h2>
        <p>
          Some statistics point to a greater number of car accidents occurring
          on surface streets than freeways. This is due to the intersections,
          turns and traffic lights – all of which are among the most common
          contributing factors for car crashes. Despite this, freeways are
          widely considered to be more dangerous. This is because any accidents
          on freeways will often have more severe results. Here are some of the
          chief reasons why:
        </p>
        <ul>
          <li>High speeds</li>
          <li>Greater volume of traffic</li>
          <li>Many lanes</li>
          <li>Sudden lane changes</li>
          <li>Long journey fatigue</li>
          <li>Distractions</li>
        </ul>
        <h2>High Impact Crashes on Freeways</h2>
        <p>
          Speed and traffic volume are the main factors in the risk surrounding
          freeway accidents. The speed limit on freeways around Los Angles is 65
          or 70mph, depending on the route. Wherever you drive you will see
          people speeding though, and some drivers push these limits by driving
          at speeds in excess of 80mph. The higher the speed, the greater the
          force carried into a crash, and the worse the result will be.
        </p>
        <p>
          Not only does a higher speed make for a more significant crash impact,
          it also makes it harder for a driver to react quickly enough to
          prevent a collision from happening. The stopping distance (the total
          distance travelled while the driver thinks and brakes) is 120 feet
          when a car is travelling at 40mph, compared to 315 feet for a car
          travelling at 70mph – the more likely speed in a freeway crash.
        </p>
        <p>
          The sheer number of cars on the road also plays a role. Everyone knows
          that the freeways in Los Angeles are always busy, even outside of peak
          hours. While these roads have multiple lanes specifically to
          accommodate the traffic and ease congestion, these factors can add up
          to create a potentially deadly situation. When you have drivers trying
          to cross multiple lanes to make an exit, navigating the large number
          of surrounding cars, and doing so at speeds topping 70mph, you have a
          combination which leads to many a freeway accident.
        </p>
        <h2>Other Causes of Freeway Car Crashes</h2>
        <p>
          While carelessness at high speeds is undoubtedly a major cause of
          freeway vehicle collisions, there are plenty of other contributing
          factors. Some people might hop onto the freeway to shave a few minutes
          off a short trip, but freeways will be used mainly by drivers on
          longer journeys between cities. Anyone who has been on a long drive
          knows that it can take its toll and leave you feeling fatigued –
          especially at the end of a long work day. A fatigued driver will
          naturally be more prone to making mistakes on the road, especially{" "}
          <Link
            to="/blog/new-survey-shows-driver-fatigue-is-a-major-issue-for-transportation-companies"
            target="new"
          >
            fatigued drivers of commercial vehicles
          </Link>
          .
        </p>
        <p>
          Another factor boosting the risk of a crash on Los Angeles freeways is
          the tendency for drivers to become distracted, usually by a phone or
          another similar device. Most people manage to stay off their phones on
          short journeys, but on longer freeway trips, the tendency to sneak a
          glance at the screen can creep in.
        </p>
        <p>
          We all know that extreme weather conditions are a rare occurrence in
          California, but it is a common observation that people forget how to
          drive when it rains. When it does rain, the freeways are the
          worst-affected areas. Rainwater will leave the road surface in a slick
          and slippery condition, with the initial coating of moisture mixing
          with oil deposits on the roads. It also makes visibility worse, and a
          combination of slippery surfaces, high speeds and low visibility makes
          a crash on a freeway far more likely.
        </p>
        <p>
          It is important to note that not all freeway accidents will involve
          multiple vehicles. It is entirely possible for you to crash without an
          error from another driver causing the accident. However, just because
          no other vehicle is involved, it does not necessarily mean you are at
          fault – or that there is no liability case to be pursued.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Freeway accident lawyers"
            style={{
              backgroundImage:
                "url('/images/car-accidents/los_angeles_freeways_aerial_rsz_fade.jpg')"
            }}
          >
            <h2>The Most Common Los Angeles Freeway Accident Locations</h2>
          </div>
        </LazyLoad>
        <p>
          There are plenty of freeways running through and around the Los
          Angeles area which see accidents on a daily basis. The most common
          accident-ridden freeways in the area are:
        </p>
        <ul>
          <li>The 110</li>
          <li>I-5</li>
          <li>I-405</li>
          <li>I-10</li>
        </ul>
        <h3>Freeway crashes on the 110</h3>
        <p>
          The 110 is a highway including the I-110, with the entire route
          running between San Pedro and Pasadena and including the Downtown LA
          area. A study looking at all of the reported Los Angeles freeway
          accident data over the course of a year showed that the segment of the
          110 between the 105 and the 101 has the most accidents per mile of any
          LA freeway.
        </p>
        <h3>Accidents on the I-5</h3>
        <p>
          The I-5 runs through California, Oregon and Washington as one of the
          main west coast routes. It covers a stretch of nearly 800 miles in
          California alone, including the LA area. According to the Los Angeles
          freeway crash statistics, this highway has several segments which rank
          highly in accidents per mile, including the section running south from
          the 10 to the 405.
        </p>
        <h3>Freeway collisions on the 405</h3>
        <p>
          The I-405 is one of the most congested freeways in the U.S., built to
          ease the pressure on I-5. It runs between San Bernardino and Irvine,
          past Los Angeles. The section running between the 110 and the 10
          running both north and south has been identified by the data as having
          a particularly high freeway accidents per mile ratio.
        </p>
        <h3>Traffic accidents on the I-10</h3>
        <p>
          Last but not least is the I-10, which runs east to west. The 242-mile
          stretch in California runs through Santa Monica and LA, among other
          cities. As with the other freeways flagged in and around Los Angeles,
          the I-10 has a high volume of traffic and has several sections listed
          at the top of the accidents per mile data charts.
        </p>
        <h2>The Most Common Injuries from Freeway Accidents</h2>
        <p>
          The kind and severity of injuries suffered in freeway crashes can vary
          hugely depending on the circumstances of the accident. Did it involve
          another vehicle? What part of your car was hit? How much force was
          involved in the impact? All of these questions and many more play a
          role in the kind of injuries you could sustain in a freeway collision.
          Here are some of the most common:
        </p>
        <ul>
          <li>Whiplash</li>
          <li>Cuts and bruises</li>
          <li>Concussion</li>
          <li>Bone fractures</li>
        </ul>
        <div className="mini-header-image">
          <LazyLoad>
            <img
              alt="Los Angeles freeway accident attorneys"
              src="../images/car-accidents/two_car_crash_resized.jpg"
            />
          </LazyLoad>
        </div>
        <h2>What Should You do After a Freeway Crash?</h2>
        <p>
          If you are involved in an accident on a freeway in LA, your first step
          will depend on your injuries.
        </p>
        <ul>
          <li>
            If your car is still mobile, move it to the side of the road if it
            is safe to do so.
          </li>
          <li>
            If you have sustained serious or life-threatening injuries, you need
            to sit tight and wait for medical assistance.
          </li>
          <li>
            If your injuries do not prevent you from doing so – and it is safe
            to do so – you should move to a safer area. Freeways are dangerous,
            and you should not remain on or by the road if possible.
          </li>
          <li>Call 911 to alert the emergency services.</li>
          <li>
            Gather as much information as you can. This includes, but is not
            limited to: The names, license plates, registration information,
            contact numbers and insurance details of anyone else involved.
          </li>
          <li>
            Document the scene of the crash. Takes pictures of the vehicles, the
            road, and any injuries, as long as it is safe to do so. Collect
            witness contact details and reports.
          </li>
          <li>
            File a police report and make sure you request a copy. Keep any
            bills or expenses relating to the accident, your injuries, and any
            treatment.
          </li>
          <li>
            Contact a Bisnar Chase attorney who is experienced in handling LA
            freeway accident cases.
          </li>
        </ul>
        <h2>Contacting an Experienced LA Freeway Accident Lawyer</h2>
        <p>
          After a crash, you should contact a collision attorney as soon as
          possible. Our expert injury attorneys are experienced in dealing with
          freeway accident cases. They will be able to examine your case and
          walk you through your legal options.
        </p>
        <p>
          Our personal injury attorneys have won over $500M and have been
          representing injured plaintiffs in Los Angeles for over 40 years. We
          recommend not speaking to the insurance companies or defendants until
          you have consulted a qualified lawyer. You may put your injury case at
          risk if you do.
        </p>
        <h2>
          What Will You Be Entitled to if you have Been in a Freeway Accident?
        </h2>
        <p>
          Every case is different! Any claim and financial award will depend on
          a wide range of factors, including the specific circumstances of your
          accident, the injuries you may have sustained, and the impact of the
          accident on your day-to-day life. As a general rule, the size of a
          claim will correlate with the losses or damages you have suffered as a
          result of the car crash. Here are some of the considerations in
          establishing the value of a claim:
        </p>
        <ul>
          <li>
            <b>Injuries</b> – One of the key considerations in any freeway crash
            case is the severity of injuries suffered, as well as how those
            injuries impact your life, and how long they take to heal.
          </li>
          <li>
            <b>Medical Expenses</b> – Medical care is expensive, and hospital
            bills can add up after an accident. Any claim should account for the
            level of treatment required, from emergency care at the scene of the
            accident to rehabilitation.
          </li>
          <li>
            <b>Loss of Earnings</b> – If the effects of an accident leave you
            unable to work through no fault of your own, you should be
            compensated for that lost earning potential.
          </li>
          <li>
            <b>Non-economic Damages</b> – This is the term given to those
            effects that cannot be seen. Being involved in a car accident can
            lead to emotional pain and suffering, which should be just as big a
            consideration as the physical effects.
          </li>
          <li>
            <b>The Future</b> – It is not just the immediate impact of a
            collision that needs to be assessed. A claim should account for any
            future treatments or therapy you will require as a result of this
            incident.
          </li>
        </ul>
        <h2>How Can Bisnar Chase Help?</h2>
        <p>
          When it comes to <b>freeway accident lawyers in Los Angeles</b>, the
          firm of Bisnar Chase is at the cutting edge. Our attorneys are
          dedicated, knowledgeable, and accomplished. As a firm we pride
          ourselves on our outstanding track record of protecting the rights of
          our clients and reaching favorable settlements to ensure they receive
          proper compensation for damages and loss.
        </p>
        <p>
          We have the experience to take on even the toughest cases. Our trial
          lawyers have successfully taken on insurance firms and giant
          companies. Above all, they are dedicated to fighting for their
          clients. Some of the cases closed by Bisnar Chase have resulted in
          eight-figure payouts.
        </p>
        <p>
          We work on no-win, no-fee basis, ensuring that you pay nothing unless
          we win or settle your case for you. Our{" "}
          <b>freeway accident attorneys</b> have been serving Los Angeles since
          1978. Call 323-508-2703 for legal help now.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
