// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/funeral-wrongful-death-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Wrongful Death Attorneys Los Angeles – Accidental Death Lawyers, CA"
        pageDescription="Experiencing the death of a loved one is traumatic, painful & can cause grief & mourning issues with the survivors. The Los Angeles Wrongful Death Lawyers have been representing & winning cases for over 40 years. Call our injury & accident attorneys for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Wrongful Death Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles wrongful death lawyer at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          There is no question that the loss of a loved one is devastating and
          often, life-changing. The Los Angeles wrongful death attorneys at
          Bisnar Chase-advise our clients to obtain grief counseling in the
          aftermath of a sudden loss.
        </p>

        <p>
          We understand that money cannot bring a loved one back. It's a cold,
          hard fact. However, we have been able to help thousands of clients
          cope with their financial struggles. Some of the types of California{" "}
          <Link
            to="http://dictionary.law.com/Default.aspx?selected=2268"
            target="new"
          >
            wrongful death claims
          </Link>{" "}
          we pursue are;
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/car-accidents" target="new">
                Car accident fatalities{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/dog-bites" target="new">
                Animal or dog attack (fatal){" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/truck-accidents" target="new">
                Commercial truck fatalities{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/pedestrian-accidents" target="new">
                Pedestrian fatalities{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/bus-accidents" target="new">
                Bus fatalities{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/auto-defects" target="new">
                Catastrophic auto defects{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/product-liability" target="new">
                Product defect fatalities{" "}
              </Link>{" "}
              and{" "}
              <Link to="/pharmaceutical-litigation" target="new">
                prescription drug deaths{" "}
              </Link>
            </strong>
          </li>
        </ul>
        <p>
          There are so many examples. Families that have lost a breadwinner.
          Families that are struggling to pay off medical expenses and funeral
          costs after the sudden death of a loved one. Families that have lost
          benefits and considerable future income as the result of a sudden
          death. These are the realities of life. We help families obtain a
          sense of justice and fair compensation for their losses while holding
          at-fault parties accountable for their negligence and/or wrongdoing.
          We may be able to help you too.
        </p>

        <p>
          <b>
            Please contact us at 323-238-4683 to schedule your free consultation
          </b>
          . We are familiar with Los Angeles courts, defense teams and judges
          and will fight for you with trust, passion and results. We've
          recovered millions of dollars for our California wrongful death
          clients.
        </p>
        <h2>Wrongful Death Liability in California</h2>
        <p>
          The sudden and unexpected loss of a loved one can have a long lasting
          impact on a family emotionally and financially. If an individual's
          death was caused by an act of negligence or wrongdoing, there are
          civil remedies available for the families of decedents. While filing a
          criminal case in such situations is entirely up to the investigating
          police agency and the Los Angeles District Attorney's office, the
          decedent's family can still pursue monetary damages for their losses
          by going through the civil justice system and filing what is known as
          a wrongful death claim. In California, a "wrongful death claim" arises
          when one person dies due to the negligence or wrongful act of another
          person or entity.
        </p>
        <p>
          A "wrongful death" differs from a criminal prosecution for murder and
          homicide in the sense that the civil action is brought to court
          directly by the survivors of the deceased person. The defendant in a
          wrongful death action is found "liable" for the damages. In a criminal
          case, the defendant faces penalties such as jail or prison time or
          even the death penalty. In a civil case, the family members of the
          decedent receive monetary damages for their losses. Also, in a
          criminal case, jurors find the defendant guilty "beyond a reasonable
          doubt." In a civil wrongful death case, jurors find the defendant
          liable through a "preponderance of the evidence."
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9nqLzrBI8FE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Filing a Wrongful Death Claim in Los Angeles?</h2>
        <p>
          There are several "representatives" who may be able to file a wrongful
          death claim, and location such as LA County is based on the statute
          for the state, not the city. The following can typically file a
          wrongful death claim.
        </p>
        <ul>
          <li>
            <b>Immediate family members</b>: Family members who can file a
            wrongful death claim include spouses, children, adopted children and
            parents of unmarried children.
          </li>
          <li>
            <b>Partners</b>: Life partners, financial dependents and putative
            spouses may also file a wrongful death claim. A "putative spouse" is
            a person who was not legally married to the victim, but who, for
            example, participated in a ceremonial marriage.
          </li>
          <li>
            <b>Other family members</b>: In some cases, a distant family member
            may also be able to file a wrongful death claim. For example, a
            grandparent who is raising a child may be able to bring wrongful
            death action on behalf of the child's deceased parents.
          </li>
          <li>
            <b>Financial dependents</b>: Anyone who has been financially
            dependent on the victim can also seek compensation by filing a
            wrongful death claim.
          </li>
        </ul>
        <h2>Who Can Be Held Liable in Los Angeles Wrongful Death Cases?</h2>
        <p>
          Wrongful death lawsuits in Los Angeles are based on California law and
          can be brought against a wide variety of individuals, entities,
          corporations, employees and even governmental agencies. Here are some
          examples of individuals and entities against whom a wrongful death
          claim may be filed:
        </p>
        <ul>
          <li>
            A negligent driver who was at fault for a car accident. If a driver
            is on the job at the time of the crash, his or her employer can also
            be held liable.
          </li>
          <li>
            The manufacturer of a defective auto or a faulty vehicle part that
            caused a crash.
          </li>
          <li>
            The manufacturer of any dangerous or defective product, which caused
            the death.
          </li>
          <li>
            A governmental agency that allowed a hazardous condition to exist on
            a roadway it was responsible for maintaining.
          </li>
          <li>An agency, which defectively designed a roadway.</li>
          <li>
            A daycare center that was responsible for taking care of a child.
          </li>
          <li>
            A nursing home that was responsible for taking care of an elderly
            resident.
          </li>
        </ul>
        <h2>The Value of a Wrongful Death Claim in Los Angeles, CA</h2>
        <p>
          The value of an LA County wrongful death claim depends on a number of
          different factors.
        </p>
        <ul>
          <li>
            <b>Economic damages</b>: This may include immediate financial losses
            suffered because of the accident as well as compensation for
            financial contributions the victim will no longer be able to
            provide. Economic damages may include funeral expenses, medical
            bills, loss of the victim's expected earnings, loss of benefits, and
            loss of inheritance.
          </li>
          <li>
            <b>Non-economic damages</b>: It can be challenging to put a monetary
            value on non-economic damages because they are invaluable. The court
            may, however, attach a monetary value to the survivor's mental
            anguish, pain, suffering, loss of care, loss of love and
            companionship and loss of consortium.
          </li>
          <li>
            <b>Punitive damages</b>: In cases involving egregious conduct, it
            may be possible to pursue punitive damages from the at-fault party.
            For example, the manufacturer of a defective product can be slapped
            with punitive damages if they knew that the product was defective
            when it was sold to consumers.
          </li>
        </ul>

        <LazyLoad>
          <img
            src="/images/text-header-images/mourning-a-loss-siblings (1).jpg"
            width="100%"
            alt="Accidental or wrongful death lawyers los angeles - Bisnar Chase Personal Injury Attorneys"
          />
        </LazyLoad>

        <h2>Wrongful Death Statute for California</h2>
        <p>
          The statute of limitations on a wrongful death claim in California is{" "}
          <strong> 2 years</strong>. This includes all counties including Los
          Angeles. If you do not file within this time you may lose your right
          to file a claim unless you can prove that the wrongful death was
          discovered later rather than sooner. Typically a two year period is
          plenty of time for a person to file their wrongful death lawsuit but
          trauma and stress often cause people to lose track of time. If you
          have lost a loved one in a wrongful death let an lawyer help you
          determine if you are due compensation.
        </p>
        <h2>What is Wrongful Death?</h2>
        <p>
          A wrongful death is anytime a person is killed by negligent means. A
          wrongful death claim can be filed by the family members, spouses.
        </p>
        <h3>Who Can be Held Liable in a Wrongful Death Claim?</h3>
        <p>Anyone found liable for the person's death by negligent actions.</p>
        <h3>Who Can Claim Wrongful Death on Someone's Behalf?</h3>
        <p>Family members, spouses.</p>
        <h3>
          What Type of Wrongful Death Claims Have You Filed and Helped Change
          Families' Lives?
        </h3>
        <p>
          We have filed wrongful death claims in a number of various cases
          including auto defects and car accidents. We also have extensive
          experience filing wrongful death claims against dangerous product
          injuries.
        </p>
        <h3>How Can Your Firm Help Me with My Wrongful Death Claim?</h3>
        <p>
          We will evaluate the compensation you are entitled to based on lost
          and future earnings, loss of compassion-ship and any other factors
          that weigh into how the loss of life has affected your future and
          income.
        </p>
        <h2>
          Why is a Los Angeles Wrongful Death Attorney Needed in Your Case?
        </h2>
        <p>
          To determine how much compensation you are entitled to based on the
          loss of current and future earnings of your loved one. Typically, loss
          of a child will pay less than a spouse who was earning income, but
          special circumstances can apply.
        </p>
        <h3>What Type of Compensation is Covered in a Wrongful-Death Case?</h3>
        <p>
          That depends on your case and how the decedent's earnings affected
          your way of life and future income.
        </p>

        <h2>Let our Los Angeles Wrongful Death Lawyers Assist You</h2>

        <p>
          Our firm has a lot of resources, talent, and experience -- and those
          resources extend throughout California. Whether it's a Los Angeles
          death claim or Northern California, we have the financial resources to
          pursue your case with optimal results.
        </p>

        <p>
          Bisnar Chase doesn't take just any case. We take cases that we know
          deserve justice, and also justify the cost of pursuing them
          vigorously. We will invest heavily in your case in an effort to get
          you the fair compensation you're entitled to.
        </p>
        <p>
          You will not be charged a fee until the case settles. Only then will
          we charge you a recovery fee.{" "}
          <strong> If we don't win, you'll pay nothing</strong>. It's a promise
          that's worked for over
          <strong> 40 years</strong>. <strong> Bisnar Chase </strong>has
          established a <strong> 96% success rate </strong>and has won over{" "}
          <strong> $500 Million </strong>for our clients.
        </p>
        <p>
          Contact one of our{" "}
          <strong> Los Angeles Wrongful Death Lawyers </strong>today for a{" "}
          <strong> Free Case Evaluation</strong>..
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
