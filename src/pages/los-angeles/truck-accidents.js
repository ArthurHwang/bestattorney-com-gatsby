// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/los-angeles-truck-accident-lawyer.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Truck Accident Attorneys Los Angeles – 18 Wheeler Accident Lawyers, CA"
        pageDescription="Big rigs, semi trucks and 18-wheelers can pose extreme dangers on the road. The Los Angeles truck accident lawyers of Bisnar Chase are dedicated to helping crash victims. Call (323) 238-4683 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Truck Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="A large semi truck crossing a bridge in front of a picturesque sunset"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Victims who need the representation of a Los Angeles{" "}
          <Link to="/truck-accidents">truck accident lawyer </Link> are likely
          to be seeking justice after being involved in a truck crash accident.
          If you have suffered an injury due to the negligence of a truck
          driver, you may be entitled to compensation for any related damages,
          injuries, or losses.
        </p>

        <p>
          Crashes involving large trucks can be extremely dangerous and have
          devastating consequences. In addition, truck accident claims can be
          complicated, and many injured accident victims do not get the help
          they need to find justice after a crash. This is why it is important
          to hire a high-quality
          <strong> truck accident lawyer in Los Angeles</strong>.
        </p>

        <p>
          Call <Link to="tel:+1-323-238-4683">(323) 238-4683 </Link> for a free
          case evaluation with Bisnar Chase's top-rated{" "}
          <Link to="/los-angeles">personal injury attorneys</Link>, and take
          advantage of their decades of experience and success.
        </p>

        <div className="mb">
          {" "}
          <Link to="/los-angeles/truck-accidents#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Do I Know What My Truck Accident Claim is Worth?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Should I Do After a Truck Accident in Los Angeles?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Can Bisnar Chase Protect Me Against a Trucking Company?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Can I Help Preserve Evidence After a Truck Crash?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Do I Need to Bring When I Meet with a Los Angeles Truck
              Accident <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Kind of Truck Accident Compensation Do Victims Get?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Can an LA Attorney Still Help if a Truck Crash Happened Out of
              State? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Truck Accident Statistics <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Truck Accident Liability <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Common Causes of Los Angeles Truck Accidents{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Proving the Cause of the Truck Crash <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/truck-accidents#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Do I Find the Best Los Angeles Truck Accident Lawyer Near Me?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>How Do I Know What My Truck Accident Claim is Worth?</h2>

        <p>
          <b>
            Important: Do not accept a settlement offer unless you are confident
            that all of your losses are adequately covered.
          </b>
        </p>

        <p>
          The best LA truck accident attorneys can help victims secure
          compensation for medical bills, pain and suffering, lost wages, and
          much more.
        </p>

        <p>
          The experienced <strong> Los Angeles truck accident lawyers</strong>{" "}
          of Bisnar Chase have a long and successful track record in dealing
          with trucking companies, as well as their attorneys and stubborn
          insurance firms.
        </p>

        <p>
          We understand the serious challenges our injured clients and their
          families face physically, financially and emotionally after a crash.
        </p>

        <p>
          Our dedicated team is passionate about doing everything possible to
          ensure that victims and their families have access to the help and
          support they need through these challenging times.
        </p>

        <div id="header2"></div>

        <h2>What Should I Do After a Truck Accident in Los Angeles?</h2>

        <ol>
          <li>
            <span>
              <b>Seek medical attention.</b> This is important, and should be
              done as soon as you can. Semi trucks are larger and heavier than
              most vehicles, which makes truck accidents highly dangerous. A
              crash involving a big rig could be deadly.
              <p>
                Getting immediate medical help will ensure that a crash victim's
                injuries are treated quickly. It will also make sure that those
                injuries are documented and help strengthen a later legal claim.
              </p>
            </span>
          </li>
          <li>
            <span>
              <b>Document the scene.</b> It is helpful to have photos or video
              of the scene of the accident. This can help a Los Angeles truck
              accident attorney make the case for their client when it comes to
              taking legal action.
            </span>
          </li>
          <li>
            <span>
              <b>Get witness statements.</b> If possible, also acquire the
              information of any witnesses to the crash, as well as their
              contact information.
            </span>
          </li>
          <li>
            <span>
              Contact a <strong> Los Angeles truck crash lawyer</strong> for
              expert help.
            </span>
          </li>
        </ol>

        <div id="header3"></div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Value of my personal injury claim"
            style={{
              backgroundImage: "url('/images/text-header-images/gavel.jpg')"
            }}
          >
            <h2>How Can Bisnar Chase Protect Me Against a Trucking Company?</h2>
          </div>
        </LazyLoad>

        <p>
          Trucking companies will look to protect themselves at all costs. As a
          result, if you are involved in a collision with a commercial vehicle,
          you might end up battling for fair compensation against expensive
          attorneys and insurance adjusters who will do anything to avoid paying
          up. This is not a fair fight.
        </p>

        <p>
          Anyone injured by a commercial truck driver should turn to the law
          firm of Bisnar Chase. Our LA truck accident lawyers can take the lead
          against opposing attorneys and insurance firms to give victims the
          best chance of success.
        </p>

        <div id="header4"></div>

        <h2>How Can I Help Preserve Evidence After a Truck Crash?</h2>

        <p>
          There are several steps which should be taken after a truck crash to
          preserve evidence. This will be important if legal action is taken.
        </p>

        <ol>
          <li>
            <span>
              Be sure to take photos and notes of everything if possible,
              including the scene, road, and vehicle damage.
            </span>
          </li>
          <li>
            <span>
              Call the police and make sure a report is filed. A copy of this
              report can be requested later and will provide proof of important
              facts.
            </span>
          </li>
          <li>
            <span>
              Get written statements or contact details from any witnesses.
            </span>
          </li>
        </ol>
        <p>
          These steps will give you the best chance of proving what happened.
        </p>
        <div id="header5"></div>
        <h2>
          What Do I Need to Bring When I Meet with a Los Angeles Truck Accident
          Attorney?
        </h2>

        <p>
          When you meet with a personal injury lawyer, bring any notes and
          evidence which pertain to your truck accident.
        </p>

        <p>
          It is important that your lawyer is able to review photos and video
          footage from the scene, as well as written statements and witness
          accounts if you have them.
        </p>

        <p>
          Be sure to include medical bills and proof of treatment if possible,
          as well as a copy of the police report if you have obtained one from
          the LAPD.
        </p>

        <p>
          The more evidence you provide, the stronger the case your truck
          accident lawyer will be able to build.
        </p>

        <div id="header6"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Kind of Truck Accident Compensation Do Victims Get?"
            style={{
              backgroundImage:
                "url('/images/truck-accidents/los-angeles-truck-crash-lawyer.jpg')"
            }}
          >
            <h2>What Kind of Truck Accident Compensation Do Victims Get?</h2>
          </div>
        </LazyLoad>

        <p>
          The level of compensation a victim could receive will always vary from
          case to case.
        </p>

        <p>
          It can depend on the insurance coverage of the trucking company, as
          well as your own coverage. But it will also depend on factors such as
          the severity of the crash, how bad the victim's injuries are, their
          medical expenses, lost wages, and level of suffering.
        </p>

        <p>
          A Los Angeles truck accident lawyer who has experience in dealing with
          such cases will have the best chance of maximizing compensation.
        </p>

        <div id="header7"></div>

        <h2>
          If a Truck Accident Happened Out of State, Can a Los Angeles Truck
          Accident Attorney Still Help?
        </h2>

        <p>
          In most cases, it does not matter where an accident happened. Your
          insurance coverage travels with you, and it is very common to hire an
          attorney in your home state.
        </p>

        <p>
          Even if the crash happened across the country, and the offending
          driver lives elsewhere, it is normal to get legal advice from a truck
          accident attorney in Los Angeles.
        </p>
        <div id="header8"></div>
        <h2>Truck Accident Statistics</h2>
        <p>
          According to the National Highway Traffic Safety Administration (
          <strong> NHTSA</strong>), in 2015 there were 4,067 people killed and
          about 116,000 people injured in crashes involving large trucks in the
          U.S.
        </p>

        <p>
          Across the United States, an estimated 433,000 large trucks were
          involved in police-reported traffic crashes during 2015.{" "}
          <Link
            to="https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812373"
            target="_blank"
          >
            {" "}
            See full injury report from the NHTSA.
          </Link>{" "}
        </p>

        <div id="header9"></div>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Truck Accident Liability"
            style={{
              backgroundImage:
                "url('/images/text-header-images/2-semi-trucks-on-highway-banner.jpg')"
            }}
          >
            <h2>Truck Accident Liability</h2>
          </div>
        </LazyLoad>

        <p>
          When it comes to the aftermath of an accident, truck crashes are often
          more complicated than other auto incidents.
        </p>

        <p>
          As well as having to deal with the driver of the truck, victims may
          have to negotiate with the truck owner, the trucking company and all
          insurance companies associated with the potentially liable parties.
        </p>

        <p>
          Individuals and corporations that may be named in a truck accident
          lawsuit include the driver, the manufacturer of the truck, the
          manufacturer of truck parts, the truck owner, the company that leased
          the truck from the owner, and the company responsible for maintaining
          or loading the truck.
        </p>

        <p>
          It is all too common for these potentially liable parties to deny
          responsibility and refuse compensation for victims. This is where an
          experienced Los Angeles truck accident attorney comes in.
        </p>
        <div id="header10"></div>
        <h2>Common Causes of Los Angeles Truck Accidents</h2>

        <p>
          <i>
            Some of the most common causes of Los Angeles truck accidents
            include:
          </i>
        </p>

        <ul>
          <li>
            <b>
              <u>Driver Error:</u>
            </b>{" "}
            Drivers who fail to operate their truck carefully put everyone on
            the roadway in harm's way. All road users need to drive safely, but
            truck drivers can cause more damage than most if they do not drive
            carefully at all times. Speeding, running red lights, making unsafe
            turns or dangerous lane changes are common examples of truck
            accident causes involving negligent drivers.
          </li>
          <LazyLoad>
            <img
              src="/images/truck-accidents/truck-accident-attorney-in-los-angeles.jpg"
              width="43%"
              className="imgright-fluid"
              alt="A big rig truck next to an overturned van, which has been damaged in a crash between the vehicles"
            />
          </LazyLoad>

          <li>
            <b>
              <u>Driver Fatigue:</u>
            </b>{" "}
            <Link
              to="http://www.smartmotorist.com/traffic-and-safety-guideline/driver-fatigue-in-truck-drivers.html"
              target="_blank"
            >
              {" "}
              Driving while fatigued or drowsy{" "}
            </Link>{" "}
            can be very dangerous. In order to reduce the number of fatigued
            driving accidents, the federal government restricts the hours of
            service drivers can work without rest. For example,
            property-carrying truck drivers can only drive for 60 hours over 7
            consecutive days before having to take 34 hours off for rest.
          </li>

          <li>
            <b>
              <u>Speed:</u>
            </b>{" "}
            Truck drivers must adjust their speed relative to roadway
            conditions, traffic congestion and the weight of the cargo they are
            hauling.
          </li>

          <li>
            <b>
              <u>Maintenance:</u>
            </b>{" "}
            Drivers must perform a thorough inspection before getting behind the
            wheel. When a truck part failure leads to an accident, it must be
            determined if there was a failure to replace a worn-down part, or if
            it was originally a{" "}
            <Link to="/los-angeles/product-liability">defective part</Link>.
            Trucking companies are also required to keep vehicle records that
            show when parts were replaced or when vehicles were serviced.
          </li>

          <li>
            <b>
              <u>Improperly loaded cargo:</u>
            </b>{" "}
            Driving a truck that has improperly-loaded cargo can be challenging.
            Too much weight in the front, back or side of the trailer can make
            it challenging to maneuver or brake. Improper loading could result
            in jack-knifing accidents.
          </li>
        </ul>
        <div id="header11"></div>
        <h2>Proving the Cause of the Truck Crash</h2>

        <p>
          There are many ways in which investigators and truck accident lawyers
          can prove who was responsible for the crash. It is helpful to have
          eyewitnesses to the accident and to have photos of the crash site and
          the damaged vehicles.
        </p>

        <p>
          An inspection of the accident site and the truck can prove useful as
          well, but trucking companies often try to have their vehicles repaired
          before an evaluation can take place. In many cases, a formal request
          has to be made for a truck to be preserved after a crash.
        </p>

        <p>
          Reviewing a truck driver's log and driving history can also be useful.
          In some cases, the driver's log may reveal a history of violations and
          careless behavior. Lawyers may even be able to obtain the driver's
          cell phone records if distracted driving was a factor. All possible
          pieces of evidence should be preserved and reviewed when filing a
          claim.
        </p>

        <p>
          Safety groups are always pushing for stricter safety rules to keep
          dangerous truckers off the road. The latest suggestions include
          mandatory drug testing. This could impact crash compensation claims in
          the future.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/mX8wMo8QTzo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <div id="header12"></div>
        <h2>
          How Do I Find the Best Los Angeles Truck Accident Lawyer Near Me?
        </h2>

        <p>
          If you or a loved one have been injured in a truck accident – either
          in Los Angeles or further afield – the expert truck accident lawyers
          of Bisnar Chase can help.
        </p>

        <p>
          Our firm has been helping road crash victims for <b>40 years</b>. We
          have an outstanding <b>96% success rate</b>, collecting more than{" "}
          <b>$500 million</b> for our clients.
        </p>

        <p>
          Our highly-skilled team of Los Angeles truck accident lawyers excels
          in taking on trucking companies and insurance firms. We are passionate
          about providing superior representation while seeking the compensation
          and justice our clients deserve.
        </p>

        <p>
          Call now on <Link to="tel:+1-323-238-4683">(323) 238-4683 </Link> or
          click to <Link to="/contact">contact us </Link> for a free
          consultation.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
