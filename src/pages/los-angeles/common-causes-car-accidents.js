// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/los angeles car accident rsz.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Common Causes of Car Accidents in Los Angeles, California"
        pageDescription="Explore the most common causes of car accidents in Los Angeles and find out where liability lies. Call Bisnar Chase for a free car crash consultation: 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Common Causes of Car Accidents in Los Angeles</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="common causes of car accidents in Los Angeles"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          You cannot go a day without hearing about a vehicle wreck or car crash
          in Los Angeles. Anyone spending any time on the road is likely to have
          witnessed a crash, heard the familiar crunch or squeal of tires, or
          even been involved in a collision of some kind themselves. But what
          are the main <b>causes of car accidents</b> on our roads?
        </p>

        <p>
          There are a great many reasons for car accidents happening in Los
          Angeles. As a city, LA is an incredible place to live and work – a
          tourism hub with a thriving entertainment scene. It is also the second
          most densely populated city in the United States, trailing only New
          York, and is known as one large accident hotspot.
        </p>

        <p>
          From driver errors to adverse conditions, heavy traffic and poor road
          layouts, we break down these all-encompassing terms and get to the
          root of the <b>top causes of road accidents in Los Angeles</b> – and
          how they can be avoided.
        </p>

        <p>
          View the top-15 road accident causes below and read on for a full
          rundown of each crash cause.
        </p>

        <h2>The Top 15 Causes of Car Accidents in Los Angeles</h2>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/los-angeles/common-causes-car-accidents#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Distracted Driving
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Speeding
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Traffic Volume
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Intoxication
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Bad Weather
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Running Red Lights
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Ignoring Stop Signs
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Tailgating
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Road Design and Maintenance
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Dangerous Lane Changes
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Improper Turns
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Road Rage
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header13">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Drowsy Driving
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header14">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Blown Tires
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/common-causes-car-accidents#header15">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Vehicle Fault
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <div id="header1"></div>
        <h2>Distracted Driving</h2>

        <p>
          According to the most recent car crash statistics from the California
          Office of Traffic Safety, there were nearly 22,000 collisions
          involving distracted driving across the state in 2017. Officials
          believe the real number is much higher, with many distracted drivers
          refusing to admit that they were using their cell phones when they
          crashed. The California DMV reported more than 249,000 phone-related
          convictions through 2017.
        </p>

        <p>
          The rise of mobile technology has made distracted driving one of the
          greatest road dangers – and one of the
          <b> leading causes of car accidents</b>. It is a wide term, covering
          texting behind the wheel, checking emails, watching videos, eating and
          drinking, tinkering with your GPS, or anything else which diverts your
          attention from the road.
        </p>

        <div id="header2"></div>
        <h2>Speeding</h2>

        <p>
          It should come as no surprise that speeding is one of the chief causes
          of car accidents in and around Los Angeles. Everyone is in a hurry in
          LA, and it is a seemingly-accepted myth that driving above the speed
          limit is okay. But speed limits are in place for a reason. At higher
          speeds it is harder for drivers to react, while stopping distances are
          also greatly increased. Higher speeds not only mean it is more likely
          a crash will occur, but also make any accident more likely to result
          in an injury, due to the increased force of a collision when cars are
          traveling faster. The{" "}
          <Link to="https://www.nhtsa.gov/risky-driving/speeding" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          says that 27% of all crash deaths in 2016 were caused by speeding.
        </p>

        <div className="mini-header-image mb">
          <LazyLoad>
            <img
              src="/images/car-accidents/Los Angeles heavy traffic rsz.jpg"
              alt="leading causes of car crashes"
            />
          </LazyLoad>
        </div>

        <div id="header3"></div>
        <h2>Traffic Volume</h2>

        <p>
          A high volume of traffic alone does not cause car crashes, but it
          creates more difficult driving conditions and, in turn, a greater
          chance of a crash occurring. Los Angeles is known as a sun-drenched
          paradise, the home of Hollywood, and…one of the most congested cities
          in the world. With a high population, not to mention the busy tourist
          spots, and a host of successful crowd-drawing sports teams in the
          Rams, Dodgers, Lakers, Clippers, Galaxy, Chargers and Kings, the roads
          are often heavily congested – and not just at peak times. It stands to
          reason that more traffic means there is a greater chance of being
          involved in a road crash with another driver.
        </p>

        <div id="header4"></div>
        <h2>Intoxication</h2>

        <p>
          Driving while intoxicated can refer to getting behind the wheel while
          under the influence of alcohol, as well as driving while on drugs.
          This can include legal and illegal drugs, depending on the effect they
          have on the body. Alcohol and drugs can have a huge impact on reaction
          time, judgement, coordination and motor skills. It is not difficult to
          see why driving while intoxicated is one of the most{" "}
          <b>common causes of road accidents</b>. Car accident statistics show
          that 15% of all drivers killed in California crashes in 2016 tested
          positive for a form of drug, while there were more than 1,000
          alcohol-related crash deaths. These figures follow the national trend,
          with an estimated 32% of fatal crashes across the U.S. involving a
          driver or pedestrian who is intoxicated, according to NHTSA.
        </p>

        <div id="header5"></div>
        <h2>Weather Conditions</h2>

        <p>
          Los Angeles is not known for poor weather conditions. However, when
          the city is hit by rain, ice, sleet or fog, car accidents due to
          weather conditions become very common. Adverse weather can create
          slippery road surfaces and poor visibility, making it much harder for
          a driver to control their car. There is a particular danger of car
          accidents caused by weather occurring in Southern California, where
          drivers are not used to dealing with weather variations and do not
          know how to adjust their driving style accordingly.
        </p>

        <div id="header6"></div>
        <h2>Running Red Lights</h2>

        <p>
          It seems that more drivers than ever are running red lights. On almost
          any trip, you will see cars zipping through an intersection when a
          light has already changed. According to data released by the{" "}
          <Link
            to="https://www.iihs.org/iihs/topics/t/red-light-running/qanda"
            target="new"
          >
            Insurance Institute for Highway Safety (IIHS)
          </Link>
          , accidents caused by running red lights were responsible for 811
          fatalities in 2016, and 137,000 injuries in 2015. Whether it is done
          accidentally due to the driver being distracted, or deliberately to
          reach a destination faster, drivers running red lights is a
          significant danger and a{" "}
          <b>common cause of car accidents in Los Angeles</b>.
        </p>

        <div className="mini-header-image mb">
          <LazyLoad>
            <img
              src="/images/car-accidents/running red light LA crash rsz.jpg"
              alt="accidents caused by running red lights"
            />
          </LazyLoad>
        </div>

        <div id="header7"></div>
        <h2>Stop Sign Crashes</h2>

        <p>
          There are many reasons for an accident occurring at an intersection
          controlled by a stop sign. We have all seen and heard of the
          California rolling stop, where dangerous situations can arise from
          cars not coming to a full stop at a sign. But stop sign accidents can
          also be caused by other factors, including an obstructed view and
          driver impatience or distraction. Stop sign crashes account for about
          700,000 traffic collisions across the U.S. every year, making it a top
          cause of injuries and even fatalities.
        </p>

        <div id="header8"></div>
        <h2>Tailgating</h2>

        <p>
          The dangers of tailgating – driving too closely to the car in front –
          are well established. Research by NHTSA indicates that about 28% of
          all traffic accidents are rear-end collisions, and one of the leading
          causes of traffic accidents like this is tailgating. When you are
          following a car too closely, you often do not have enough time to
          react when the driver in front brakes. Tailgating is particularly
          dangerous on freeways, where speeds are higher, but also presents a
          significant crash risk on surface streets.
        </p>

        <div id="header9"></div>
        <h2>Roadway Failure</h2>

        <p>
          One of the more complicated <b>reasons for car accidents</b> occurring
          is a problem with the road itself. This can be hard to identify, as it
          may not be immediately apparent what the cause of a car crash was. A
          roadway issue could mean a poor road design, a confusing construction
          zone layout, or subpar maintenance. Some roads might have a lack of
          signage or unclear layout, or have surfaces made dangerous by
          potholes, oil spills or debris. Each of these issues can create
          dangerous situations and lead to collisions.
        </p>

        <div id="header10"></div>
        <h2>Unsafe Lane Changes</h2>

        <p>
          One of the most frustrating actions you can see on the road is another
          driver making an abrupt lane change, cutting in front of you without
          signaling. Rather than just frustrating though, this can be extremely
          dangerous, especially when the offending driver does not look before
          merging. This is another of the most common causes of car crashes and
          can easily lead to a side-swipe incident or a rear-end collision.
          These accidents can very easily be avoided, simply by drivers using
          their mirrors, checking blind spots and signaling before merging.
        </p>

        <div id="header11"></div>
        <h2>Improper Turns</h2>

        <p>
          An improper turn can refer to a turn made which violates the rules of
          a particular road. For instance, making a left when there is a sign
          indicating that left turns are not allowed. This may sound innocuous
          enough, but it is also a regular occurrence, and one of the most{" "}
          <b>common causes of road accidents</b>. When a car makes an improper
          turn, there is a serious risk it will end up facing into traffic, or
          stuck in a vulnerable position in a roadway. From making illegal
          U-turns to turning into the wrong lane, an improper turn crash can
          result in serious injuries. Obey the rules of the road to stay safe!
        </p>

        <div id="header12"></div>
        <h2>Road Rage</h2>

        <p>
          Many people confuse road rage with the common emotions you might
          experience when you see another vehicle driving badly. But real road
          rage is a <b>leading cause of car crashes in Los Angeles</b>. The term
          is defined as aggressive driving which puts other road users in
          danger. Road rage statistics from NHTSA show that aggressive driving
          is a factor in 66% of road crash deaths. It may result in many of the
          other actions listed in our Top 15 Causes of Car Accidents list,
          making it an extremely dangerous underlying cause to a wide range of
          crashes.
        </p>

        <div id="header13"></div>
        <h2>Drowsy Driving</h2>

        <p>
          Getting behind the wheel while feeling extreme tiredness can have a
          drastic impact on a person’s driving ability. Drowsy driving can lead
          to lapses in concentration and slower reaction times, as well as bad
          decision making and involuntary actions – such as drifting across
          lanes. It is a common cause of car crashes on freeways, when drivers
          are making longer journeys late at night, but can be a factor on any
          road at any time. No one should drive when they are fighting
          tiredness, or they risk putting themselves and other drivers in harm’s
          way.
        </p>

        <div className="mini-header-image">
          <LazyLoad>
            <img
              src="/images/car-accidents/crash cause tire blowout rsz.jpg"
              alt="tire blowout car accident"
            />
          </LazyLoad>
        </div>

        <div id="header14"></div>
        <h2>Blown Tires</h2>

        <p>
          A tire blowout accident can be scary and put road users at risk, no
          matter when or where it occurs. This happens when a tire suddenly
          loses air pressure. Depending on the driving situation, the tire may
          simply go flat, or it could break apart. A blowout can result in the
          driver losing control, while remnants of a blown tire can also fly
          into nearby vehicles and cause further damage. In 2017 alone,
          tire-related accidents accounted for thousands of accidents and nearly{" "}
          <Link to="https://www.nhtsa.gov/equipment/tires" target="new">
            750 fatalities
          </Link>{" "}
          in the U.S., making one of the more frequent types of accident. The
          best way to avoid this happening is for every driver to check their
          tires regularly and ensure their vehicle is well maintained.
        </p>

        <div id="header15"></div>
        <h2>Vehicle Fault</h2>

        <p>
          While the majority of car accident causes are driver error-related,
          there are times when the vehicle itself is at fault. This might result
          in a loss of power, or a loss of control over braking or steering, due
          to an internal fault with the car. When this happens, it can be almost
          impossible to keep a vehicle on track, which can easily result in a
          serious collision. This can occur for different reasons – the vehicle
          may be defective due to a faulty component or an error on the part of
          the manufacturer, or a fault may occur due to a lack of proper vehicle
          maintenance.
        </p>

        <h2>Who or What is at Fault for Your Car Crash?</h2>

        <p>
          As you can see from our lengthy list of the top causes of car
          accidents, assigning blame after a collision is not always
          straightforward. The fault must be determined based on the specific
          circumstances of an incident. In many cases, fault lies with the
          driver, but in some circumstances it can be applied to other parties –
          such as a vehicle manufacturer, or a city council responsible for road
          maintenance.
        </p>

        <h2>How an Attorney can Help in a Car Crash Liability Case</h2>

        <p>
          The car accident lawyers at Bisnar Chase have been serving LA for more
          than 40 years. When it comes to these
          <b>common causes of road accidents</b> – and even some not-so-common
          car crash causes – our lawyers have been there and done it. A Bisnar
          Chase{" "}
          <Link to="/los-angeles/car-accidents" target="new">
            car accident attorney
          </Link>{" "}
          will have the expertise to explore your legal options and seek
          compensation for your damages, injuries, and losses.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
