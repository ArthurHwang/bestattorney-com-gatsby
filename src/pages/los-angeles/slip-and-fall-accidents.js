// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/slip-and-falls-banner-los-angeles.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Slip & Fall Lawyers - Bisnar Chase"
        pageDescription="Slippery surfaces, uneven concrete and other premise liabilities can cause catastrophic injuries. Our Los Angeles Slip & Fall Lawyers have represented injured victims of slips & falls for over 40 years & have obtained a 96% success rate. Call our injury attorneys for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Slip & Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles slip and fall lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you are the victim of a fall on someone else's property, Los
          Angeles Slip and Fall Attorneys can represent you and ensure that your
          interests are protected.  Although injuries from falls often go
          unreported and untreated, it is wise to seek medical treatment
          immediately and a{" "}
          <Link to="/los-angeles/premises-liability" target="new">
            Los Angeles Premises Liability Attorney
          </Link>{" "}
          can help you recover the costs of doing so.
        </p>
        <p>
          As a victim, you should not have to bear the burden of medical bills
          for your accident, so talk to a Los Angeles slip and fall lawyer
          immediately about your case so you can get the money you need to pay
          for your medical care and other expenses.
        </p>
        <h2>Top 7 Most Common Types of Slip and Fall Accidents</h2>
        <p>
          Slip and fall accidents can happen anywhere, but there are some
          accident scenarios that are more common than others.  According to the{" "}
          <Link to="https://www.osha.gov/" target="new">
            {" "}
            Occupational Safety and Health Administration or OSHA
          </Link>
          , common causes of falls are:
        </p>
        <ol>
          <li>Oil, water, soap, and other liquids on walkways</li>
          <li>Loose doormats or rugs</li>
          <li>Ice, snow, or rain on sidewalks</li>
          <li>Dangerous shoes such as flip flops on hazardous terrain</li>
          <li>Walkways that are broken or need repair</li>
          <li>Very smooth floors or walkways that do not provide traction</li>
          <li>Poorly illuminated areas</li>
        </ol>
        <h2>Slip & Falls Are Common</h2>
        <p>
          Although car accidents account for the most personal injury lawsuits,
          slip and fall accidents are by far the most common type of accident. 
          The National Safety Council states that falls are most common cause of
          emergency room treatment, and the Department of Labor estimates that
          the average fall on the job costs roughly $28,000 to treat.  Since the
          injuries from these accidents are so expensive and the accidents
          themselves are so common, it is vital that victims choose experts such
          as Los Angeles slip and fall lawyers to represent them in collecting
          damages.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Slip and Fall Statistics"
            style={{
              backgroundImage:
                "url('/images/text-header-images/common-slip-and-falls.jpg')"
            }}
          >
            <h2>Slip and Fall Statistics</h2>
          </div>
        </LazyLoad>

        <p>
          All of us have encountered slipping on an icy or slippery surface and
          hitting the ground, having to get up embarrassed brushing ourselves
          off. But not everyone is so lucky.
        </p>
        <p>
          Many slip and falls can result in serious injury, and many can end up
          in fatalities. Here are some scary and shocking statistics about slip
          and fall accidents throughout the United States in a recent years
          study.
        </p>
        <ul>
          <li>
            Biggest slip and fall risks come from mother nature: rain, snow,
            ice, unstable foundations and many other factors that can end up in
            a loss of contact with a sturdy surface beneath your feet.
          </li>
          <li>
            Unintentional falls are the second leading cause of nonfatal injury
            for 10-24 year-olds.
          </li>
          <li>
            29% of all worker's compensation claims are slip and fall related.
          </li>
          <li>
            {" "}
            <Link to="https://www.cdc.gov/" target="new">
              CDC{" "}
            </Link>{" "}
            statistics show slip and falls estimate $34 billion in medical costs
            every year.
          </li>
        </ul>
        <h2>The Value of a Slip & Fall Claim</h2>
        <p>
          Every case is different and the value of your claim will depend upon
          many factors including the severity of injuries and amount of medical
          care you received. An experienced personal injury attorney has years
          of experience dealing with premise liability cases and can properly
          evaluate the value of your claim.
        </p>
        <h2>When To File Your Claim</h2>
        <p>
          Time is of the essence to file a case; waiting too long can affect
          your ability to bring action against the business owner. The statute
          of limitations in California is two years unless you are going after a
          state or federal entity in which case it is reduced to six months from
          the date of injury.
        </p>

        <h2>Seeking Proper Legal Representation</h2>
        <p>
          Someone who has suffered a slip and fall or other premise injury
          should seek a qualified and experienced personal injury lawyer. Hiring
          a standard civil attorney may not be in your best interest as you will
          want an attorney highly skilled in injury cases.
        </p>
        <p>
          Personal injury lawyers do nothing but injury law so they are much
          more qualified to represent an injury victim. Most personal injury
          lawyers will not charge a fee unless they win your case. This leaves
          you with little to worry about and if the case is not won, you don't
          pay. If you are asked to pay before the case goes forward, be cautious
          of that request.
        </p>
        <p>
          If you or a loved one has experienced a slip and fall accident and
          have suffered a personal injury, call and get a{" "}
          <strong> Free Case Evaluation </strong>with our team of
          <strong> skilled Los Angeles Slip and Fall Attorneys </strong>at{" "}
          <strong> 323-238-4683.</strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/qvkV_ShDjAE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
