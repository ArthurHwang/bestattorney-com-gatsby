// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/employment-lawyers-attorneys-los-angeles-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Employment Lawyers - Wrongful Termination Attorneys"
        pageDescription="Working our daily jobs is essential to how we live our life and provide for our family. When that is taken advantage of, justice will be served. Our Los Angeles Employment Lawyers at Bisnar Chase fight against employment discrimination & worker's rights violations. Call for your Free Consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Employment Lawyers Los Angeles</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="employment lawyers los angeles"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Call an experienced employment lawyer in Los Angeles at{" "}
          <b>323-238-4683</b> today for a Free consultation to discuss your
          legal options. There are a number of rights that all employees have
          under California and federal law. When an employee's rights are
          violated, he or she would be well advised to discuss his or her legal
          options with an experienced{" "}
          <Link to="/los-angeles">Los Angeles personal injury attorney</Link>.
          In cases where a number of employees have been mistreated by the same
          employer, the workers can combine their claims into one class action
          lawsuit against the company.
        </p>
        <h2>Class Action Lawsuits Against Employers</h2>
        <p>
          class action lawsuits are common in cases involving employment issues
          because an employer who violates the rights of one employee is likely
          violating the rights of many employees. It is common for wronged
          employees to have difficulty obtaining a skilled employment lawyer
          because the damages are so small.
        </p>
        <p>
          If, however, the rights of multiple employees have been violated, a
          reputed Los Angeles class action lawyer with a history of holding
          large corporations accountable may be more willing to consider the
          case and hold the company liable for its wrongful actions.
        </p>
        <h2>Requirements for an Employment Class Action Lawsuit</h2>
        <p>
          Not all lawsuits qualify as a class action. Here are a few examples of
          requirements that should be met before creating a class action suit:
        </p>
        <ul>
          <li>
            <b>Commonality</b>: All potential class members should have suffered
            similar violations and have similar concerns.
          </li>
          <li>
            <b>Impracticality</b>: There must be so many employees and class
            members that it is unreasonable for each to file an individual claim
            against the company.
          </li>
          <li>
            <b>Typicality</b>: There must be a class representative who has been
            damaged in a similar fashion as the other members of the class.
          </li>
          <li>
            <b>Beneficial</b>: It must be beneficial for the employees and the
            employer for the claims to be combined into a class action suit.
          </li>
        </ul>
        <h2>Wrongful Termination Statistics</h2>
        <p>
          In a recent year's study, approximately 6,000 wrongful termination
          charges were filed throughout California. These reports consist of
          terminations based on the following:
        </p>
        <ul>
          <li>
            <strong> Disability</strong>
          </li>
          <li>
            <strong> Sex</strong>
          </li>
          <li>
            <strong> Gender</strong>
          </li>
          <li>
            <strong> National Origin</strong>
          </li>
          <li>
            <strong> Color</strong>
          </li>
          <li>
            <strong> Religion</strong>
          </li>
          <li>
            <strong> Age</strong>
          </li>
          <li>
            <strong> Retaliation</strong>
          </li>
        </ul>
        <p>
          If you have been the victim of wrongful termination, contact a Los
          Angeles Employment Attorney at <b>323-238-4683</b>. Discuss your
          situation with a Bisnar Chase lawyer and have your
          <strong> Free Case Evaluation </strong>today<strong>.</strong>
        </p>
        <h2>Equal Pay for All</h2>
        <p>
          Governor Jerry Brown signed 2 bills last year that made it much more
          difficult to pay women or people of color less than others. One bill
          prohibits employers from paying women less than male colleagues based
          on prior salary. The other will bar employers from paying workers
          doing "substantially similar" jobs different wages based on their race
          and or ethnicity.
        </p>
        <p>
          Learn more{" "}
          <Link
            to="https://www.latimes.com/politics/essential/la-pol-sac-essential-politics-updates-governor-signs-bills-to-expand-equal-1475261036-htmlstory.html"
            target="new"
          >
            Here
          </Link>
          .
        </p>
        <h2>Benefits of Filing a Class Action Lawsuit Against Your Employer</h2>
        <p>
          If you are unsure about filing a class action lawsuit, you may want to
          consider the many potential benefits.
        </p>
        <ul>
          <li>
            Most class action lawsuit attorneys in Los Angeles will work on a
            contingency basis. This means that you will not have to pay anything
            until after your case is won.
          </li>
          <li>
            There is also a lesser chance of facing backlash for filing a claim
            against your employer if you are part of a class action lawsuit.
            Instead of your name being on the claim, only the class
            representative will be listed.
          </li>
          <li>
            Additionally, a class action suit will actually save the court
            resources because instead of having to deal with many claims, there
            will only be one case. For the individual who decides to be the
            class representative, there is the potential for a service award in
            addition to the damages.
          </li>
          <li>
            Last but not the least, a successful class action lawsuit will put a
            stop to the illegal conduct of the company. Ideally, this will
            create a fair workplace for the current and future employees.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Labor law attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/labor-laws-examples-of-infractions-los-angeles.jpg')"
            }}
          >
            <h2>Examples of Labor Law Infractions</h2>
          </div>
        </LazyLoad>

        <p>
          There are a number of violations that employers may commit that can
          lead to a class action lawsuit. Many class action suits involve
          companies that have spent years denying employees proper overtime pay.
          Others involve a failure to provide legally required breaks.
        </p>
        <p>
          Companies that violate these types of California labor laws often fail
          to provide adequate records of the hours worked as well. Another
          serious type of infraction that can lead to a class action lawsuit
          involves companies exercising a pattern of racial, gender or age
          discrimination against their employees.
        </p>
        <h2>Feeling Comfortable with Your Attorney</h2>
        <p>
          Experiencing a wrongful termination can be traumatic and leave people
          in uncomfortable, scary and risky financial situations. This is a
          difficult time for anyone, and you need to be able to talk to someone
          about how you are doing throughout this time in your life.
        </p>
        <p>
          Having the comfort and ability to discuss personal, emotional and
          financial hardships in your life can make a difference in your case.
          Trust and honesty can lead to details and specific factors being
          brought up in everyday discussion with your lawyer which could
          possibly lead to the victory of your case.
        </p>
        <p>
          Attorneys and other individuals who represent people in legal matters
          can be misjudged and perceived as intimidating, uncaring and all about
          the money.
        </p>
        <p>
          At Bisnar Chase, you can rest assured that you will be taken care of
          and treated as part of our family. Our clients needs come first. We
          strive to make sure you are comfortable and completely satisfied that
          our team is doing everything possible to win your case and find
          justice and compensation for you and your family.
        </p>
        <h2>Hire The Best Los Angeles Employment Rights Law Firm</h2>
        <p>
          Anyone who believes that his or her rights as an employee have been
          violated would be well advised to get in touch with an experienced Los
          Angeles employment lawyer. The knowledgeable{" "}
          <Link to="/los-angeles">personal injury lawyers </Link> of Bisnar
          Chase have what it takes to fight for your rights. You do not have to
          take abuse, harassment or mistreatment from your employer.
        </p>
        <p>
          Call us today at <b>323-238-4683</b> for a Free no obligation
          consultation with one of our top rated employment trial lawyers.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
