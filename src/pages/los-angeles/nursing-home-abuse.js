// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/nursing-home-abuse-neglect-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Nursing Home Abuse Lawyers - Bisnar Chase"
        pageDescription="Having trust in the care facility your loved ones live is important. The Los Angeles Nursing Home Abuse Lawyers have zero-tolerance for aggressive caretakers, abusive staff, negligent management and property maintenance. Call our team of experienced attorneys of Bisnar Chase for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Nursing Home Abuse Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles nursing home abuse lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you need a skilled{" "}
          <strong> Los Angeles Nursing Home Abuse Lawyer</strong>, Our{" "}
          <strong> Elder Neglect Attorneys </strong>are not only skilled but we
          are passionate when it comes to fighting for the rights of nursing
          home residents. We work with attorneys in our network that specialize
          in nursing home abuse cases and have the resources to fight for you.
        </p>
        <p>
          We've recovered hundreds of millions of dollars for our clients.&nbsp;
          <strong>
            {" "}
            Please call our highly skilled attorneys at 323-238-4683 for a Free
            confidential consultation.
          </strong>
        </p>
        <h2>California Nursing Home Abuse is More Common Than We Think</h2>
        <p>
          Research from recent years indicates that up to half of all nursing
          home attendants have admitted abusing or neglecting elderly patients.
          More than half of nursing home attendants have admitted to verbally
          abusing, yelling at or using foul language with elderly residents at
          care facilities. Read more about{" "}
          <Link
            to="http://www.nursinghomeabuseguide.org/nursing-home-abuse-statistics/"
            target="new"
          >
            {" "}
            nursing home abuse, prevention and information
          </Link>
          .
        </p>
        <ul>
          <li>
            1 out of 10 elderly individuals will experience some form of elder
            abuse
          </li>
          <li>This adds up to about 2 million cases reported every year</li>
          <li>40 percent of nursing home residents report abuse</li>
          <li>
            More than 90 percent report that they or another resident of the
            assisted living facility has been neglected.
          </li>
        </ul>
        <p>
          Sadly, the elderly are the least likely demographic to report elder or
          nursing home abuse. Many times, elderly patients do not report abuse
          because they do not have the capacity or ability to verbally express
          what has occurred to them or one of their household residents.
        </p>
        <p>
          The caretakers who are trusted by family members to care for these
          vulnerable and elderly individuals are supposed to be upstanding and
          caring people. But when they abuse an elderly resident, they need to
          be delt with accordingly and held responsible for their actions to the
          fullest extent of the law.
        </p>
        <p>
          The law firm of Bisnar Chase has over 40 years of experience and a 96%
          success rate. Let our nursing home injury lawyers fight for the pain
          and suffering of your loved ones.
        </p>
        <h2>Inexcusable Crime</h2>
        <p>
          The elderly are among the most vulnerable sections of our population.
          To take undue advantage of an elderly or disabled individual is an
          egregious violation and must be punished. Nursing homes that foster a
          hostile, unhealthy environment in their facilities in order to boost
          their profits must be made to pay for their wrongdoing. Nursing home
          abuse and neglect, in our book, are inexcusable acts.
        </p>
        <p>
          Often referred to as medical malpractice, nursing home neglect and
          abuse is a sad reality in our communities today. The Los Angeles area
          is no exception. Even the best-looking nursing homes in Los Angeles
          may have incidents of abuse and neglect.
        </p>
        <p>
          Elder abuse in long-term facilities is grossly underreported. The
          California Department of Public Health, which monitors nursing homes,
          is terribly short-staffed and hardly diligent about keeping an eye on
          these facilities.
        </p>
        <p>
          When nursing homes fail to meet required cleanliness and provide the
          appropriate health care and amenities their residents deserve, the
          situation can fall into a premise liability issue. Although the law
          offices of Bisnar Chase does not accept medical malpractice, if the
          nursing home is held accountable for a premises liability issues, our
          experienced{" "}
          <Link to="/los-angeles/premises-liability" target="new">
            Los Angeles premise liability lawyers
          </Link>{" "}
          are here to help.
        </p>
        <p>
          Under staffing in Los Angeles nursing homes is a serious issue, which
          creates an environment that is ripe for neglect and abuse. If your
          loved one is being neglected or abused at a Los Angeles nursing home,
          it is important that you understand your legal rights and options. A
          seasoned&nbsp;personal injury attorney&nbsp;can help you identify your
          rights to compensation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Nursing home abuse"
            style={{
              backgroundImage:
                "url('/images/text-header-images/nursing-home-abuse-statistics.jpg')"
            }}
          >
            <h2>Nursing Home Abuse Statistics in Los Angeles</h2>
          </div>
        </LazyLoad>
        <p>
          Here are a few eye-opening statistics relating to nursing home abuse
          and neglect, compiled by the{" "}
          <Link to="https://www.justice.org/" target="new">
            American Association for Justice (AAJ)
          </Link>{" "}
          :
        </p>
        <ul>
          <li>
            1.4 million people are currently living in U.S. nursing homes.
          </li>
        </ul>
        <ul>
          <li>
            There were 20,673 complaints of abuse, gross neglect and
            exploitation on behalf of nursing home residents in 2003. That
            number has only been increasing over the last decade with more and
            more of our elderly residents getting admitted into nursing homes.
          </li>
        </ul>
        <ul>
          <li>
            One in 14 incidents of elder abuse is actually reported to the
            authorities.
          </li>
        </ul>
        <ul>
          <li>
            90 percent of U.S. nursing homes have staffing levels that are too
            low to provide adequate care.
          </li>
        </ul>
        <ul>
          <li>
            The nursing home industry received $75 billion in state and federal
            funding in 2006.
          </li>
        </ul>
        <ul>
          <li>
            Nursing home residents and their families contributed $34 billion to
            the nursing home industry in 2001.
          </li>
          <li>
            {" "}
            <Link
              to="http://abcnews.go.com/US/story?id=92689&page=1"
              target="new"
            >
              Elderly Abused at 1 in 3 Nursing Homes{" "}
            </Link>
          </li>
        </ul>
        <h2>Types of Nursing Home Abuse and Neglect Often Seen in LA</h2>
        <p>
          There are several types of abuse and neglect that occur in nursing
          homes throughout California and it is important for family members to
          be able to identify them and recognize the red flags.
        </p>
        <p>
          <strong> Neglect</strong>:&nbsp;This is the most common form of
          nursing home abuse. Nursing home neglect is when a caregiver does not
          follow the standard of reasonable care. Neglect may include failing to
          provide residents with food, water, hygiene, medication, security and
          supervision. Symptoms of neglect may include malnourishment,
          dehydration, unkempt appearance and smell of urine or feces in the
          resident's room.
        </p>
        <p>
          <strong> Physical</strong> <strong> abuse</strong>:&nbsp;When
          caregivers use excessive force that causes bodily injury to a
          resident, it is physical abuse. Examples of physical abuse include
          pushing, shoving or hitting, burning, kicking, using physical
          restraints, depriving residents of food or drinks and forcing
          residents to eat or drink.
        </p>
        <p>
          <strong> Sexual abuse</strong>:&nbsp;Nursing home sexual abuse occurs
          when a visitor, employee or resident of the assisted living facility
          engages in non-consensual sexual act with the victim. There are cases
          in nursing homes where individuals with dementia and other
          debilitating conditions become victims of sexual abuse. Any incidents
          of sexual assault or abuse should be reported to local law enforcement
          authorities right away.
        </p>
        <p>
          <strong> Emotional abuse</strong>:&nbsp;Although we don't talk much
          about emotional abuse, it can still cause significant harm and affect
          the health and well-being of elderly residents. Common examples of
          emotional abuse may include verbal abuse, bullying, intimidation,
          harassment, isolation and silent treatment.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/810mV4zwA6Y"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>What Can You Do to Prevent Neglect and Abuse?</h2>
        <p>
          There are simple steps that family members can take to ensure that
          their loved ones receive quality care in a Los Angeles County nursing
          home.
        </p>
        <ul>
          <li>
            Make your visits count. Visit at different times and shifts or on
            different days of the week. The unpredictability of your visits can
            keep the facility on its toes. Also, visiting at different times of
            day and the week is important for you to get a complete picture of
            the patterns of care in the facility. It will also help you assess
            the performance and attitude of caregivers at different shifts.
          </li>
        </ul>
        <ul>
          <li>
            Build relationships with the staff. Get to know key staff members on
            a first-name basis. When you develop a rapport with the staff and
            administrators, it becomes much easier for you to communicate and
            interact with them. When they know you well, any questions you ask
            or concerns you may raise will not be viewed as complaints or
            intrusions. Be generous with your praise when it is warranted.
          </li>
        </ul>
        <ul>
          <li>
            Participate in care plan meetings. Your loved one's care plan is
            critical to his her physical and psychological well-being. Be there
            when your loved one's care plan is being reviewed or updated to
            ensure that the patient's needs are being met.
          </li>
        </ul>
        <ul>
          <li>
            Monitor the care of your loved one. Make sure that the facility is
            carrying out your loved one's care plan. You can do so by keeping
            notes, checking records, getting copies of violations or complaints
            and making a physical inspection of your loved one.
          </li>
        </ul>
        <ul>
          <li>
            Look actively for signs of abuse and neglect. If you find any,
            report them to the administrator. If the administrator fails to take
            action, report your complaint to a state ombudsman and contact an
            experienced Los Angeles personal injury lawyer to help protect your
            loved one's rights.
          </li>
        </ul>
        <p>
          We all have a legal duty to report nursing home abuse or senior
          neglect. The state of California's Department of Justice has outlined
          factors in many&nbsp;elder abuse and neglect cases&nbsp;including the
          all too common problem of nursing homes allowing its residents to
          suffer{" "}
          <Link to="/nursing-home-abuse/bedsores" target="new">
            bedsores and dehydration
          </Link>
          .
        </p>

        <h2>Hiring a Los Angeles Nursing Home Abuse Lawyer</h2>
        <p>
          If you or a loved one has been a victim of nursing home abuse or
          neglect in a Los Angeles nursing home or think you may even have a{" "}
          <Link to="/los-angeles/wrongful-death">wrongful death </Link> case.
        </p>
        <p>
          If you or a loved one has experienced <strong> Negligence </strong>or{" "}
          <strong> Abuse,</strong> contact our{" "}
          <strong> Los Angeles Nursing Home Abuse Lawyers </strong>at
          <strong> 323-238-4683</strong> for a{" "}
          <strong> Free Case Evaluation</strong> and to obtain more information
          about pursuing your legal rights.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
