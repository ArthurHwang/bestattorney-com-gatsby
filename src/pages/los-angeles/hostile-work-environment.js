// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/hostile-workplace-los-angeles-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Hostile Work Environment Lawyer"
        pageDescription="Everyone deserves to be comfortable and not harassed when at work. If you have been abused, violated, experienced any hostility or aggression at your place of work or job, contact our skilled team of Los Angeles Hostile Work Environment Lawyers for your Free consultation with an experienced attorney at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Hostile Work Environment Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles hostile workplace attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you are working or were formerly employed in a hostile workplace,
          call the award-winning Los Angeles work hostile work environment
          attorneys of Bisnar Chase at <b>323-238-4683.</b>
        </p>
        <p>
          Everyone has heard of sexual harassment, but do you know what it
          really means? What about discrimination? Unfortunately, many employees
          are unable to determine if certain behaviors fall under the umbrella
          of workplace harassment or discrimination, and continue in jobs where
          they are subjected to this type of behavior.
        </p>
        <p>
          {" "}
          <Link to="/los-angeles/employment-lawyers">
            Los Angeles employment lawyers
          </Link>{" "}
          can help you decide if the bad treatment you have experienced is a
          violation of your civil rights.
        </p>
        <h2>You Don't Have To Tolerate A Hostile Work Environment</h2>
        <p>
          Los Angeles employment lawyers work with the victims of harassment,
          abuse, and discrimination to re-establish their civil rights in the
          workplace. No matter what type of problem you are experiencing at
          work, the chances are an employment attorney can help.
        </p>
        <p>
          Some workplace problems cannot be defined as harassment or
          discrimination but still fall under the umbrella of civil rights
          violations. In these cases, a hostile workplace lawyer will carefully
          examine the case to see what can be done to help you recover your
          workplace security. You have the right to work free of problems caused
          by other's negligence or bad behavior, and you should exercise that
          right if you are being subjected to negative treatment at work.
        </p>
        <p>
          It is important to stand up for yourself in these situations.
          Harassment and discrimination rarely go away on their own. Many
          employees quietly transfer or take another job if they are subjected
          to this type of treatment, but while this may solve your problem it is
          likely that someone else at your office or workplace will be subjected
          to the same treatment once you leave.
        </p>
        <p>
          It is important not only for your sake but also for the sake of your
          fellow employees that you do not allow anyone to get away with
          harassment or discrimination on the job and that you stand up for your
          own rights as well as those of others, including future employees.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Hostile work environment attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/sexual-harassment-discrimination-in-workplace.jpg')"
            }}
          >
            <h2>Sexual Harassment & Discrimination In The Workplace</h2>
          </div>
        </LazyLoad>

        <p>
          Sexual harassment is the most commonly-cited type of harassment,
          although other forms are probably more common in actual practice.
          Sexual harassment includes everything from being unwillingly subjected
          to lewd talk and jokes to rape. Most sexual harassment revolves around
          some form of unbalanced power; the harasser has the power to force the
          employee to listen to or participate in some type of sexual behavior
          under the threat of negative consequences if the employee does not
          comply. Sexual harassment can affect both men and women and anyone can
          be a harasser including a boss, a supervisor, or another employee.
        </p>
        <p>
          Discrimination occurs when someone is treated differently for an
          arbitrary reason. Some employers practice discriminatory hiring and
          firing, while others simply treat some employees better because of
          their race, religion, or gender. Both of these are examples of
          discrimination and are illegal under California and United States law.
        </p>
        <p>
          A recent year's study showed over{" "}
          <Link
            to="https://www.eeoc.gov/eeoc/statistics/enforcement/charges.cfm"
            target="new"
          >
            91,000 reports
          </Link>{" "}
          of discriminatory charges for the following{" "}
          <Link to="https://www.eeoc.gov/laws/types/" target="new">
            {" "}
            types of discrimination
          </Link>
          :
        </p>
        <ul>
          <li>Race</li>
          <li>Sex</li>
          <li>National Origin</li>
          <li>Religion</li>
          <li>Color</li>
          <li>Retaliation - All Statutes</li>
          <li>Retaliation - Title VII only</li>
          <li>Age</li>
          <li>Disability</li>
          <li>
            {" "}
            <Link to="https://www.eeoc.gov/policy/epa.html" target="new">
              Equal Pay Act{" "}
            </Link>
          </li>
        </ul>
        <h2>Top 10 Most Common Types of Workplace Discrimination</h2>
        <p>
          There are many types of discrimination in the workplace, but here is a
          list of the{" "}
          <strong>
            {" "}
            Top 10 Most Common Types of Discrimination in the Workplace:
          </strong>
        </p>
        <ol>
          <li>
            <strong> Race or color discrimination</strong>
          </li>
          <li>
            <strong> Sex-based discrimination</strong>
          </li>
          <li>
            <strong> Pregnancy discrimination</strong>
          </li>
          <li>
            <strong> Disability discrimination</strong>
          </li>
          <li>
            <strong> Height and weight discrimination</strong>
          </li>
          <li>
            <strong> Sexual-preference discrimination</strong>
          </li>
          <li>
            <strong> Age discrimination</strong>
          </li>
          <li>
            <strong> Religious discrimination</strong>
          </li>
          <li>
            <strong> National Origin discrimination</strong>
          </li>
          <li>
            <strong> Retaliation</strong>
          </li>
        </ol>
        <h2>A Los Angeles Hostile Work Environment Attorney Can Get Justice</h2>
        <p>
          A person who is practicing harassment or discrimination should be
          brought to justice, and a hostile workplace attorney can help you
          accomplish this goal. Take the time today to talk to a Los Angeles
          employment lawyer and solve your workplace problems the right way by
          holding those accountable who have caused them or allowed them to
          continue.
        </p>
        <p>
          Don't Tolerate A Hostile Workplace. Contact the Los Angeles Hostile
          Work Environment law firm of Bisnar Chase at <b>323-238-4683</b> for a{" "}
          <strong> Free Case Evaluation</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
