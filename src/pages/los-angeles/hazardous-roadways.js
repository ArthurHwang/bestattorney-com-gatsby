// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/los-angeles-roadways.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Hazardous Roadway Lawyers – Dangerous Road Attorneys"
        pageDescription="The Los Angeles Hazardous Roadway Attorneys of Bisnar Chase has been helping victims of negligent parties gain over $500 million dollars in compensation for 40 years. When you call the law offices of Bisnar Chase you will receive a free consultation. Contact us at (323) 238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Hazardous Roadway Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Los Angeles Hazardous Roadways "
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <strong> Los Angeles Hazardous Roadway Lawyers</strong> at Bisnar
          Chase have had significant success specifically in handling cases
          involving road hazards. We have secured millions of dollars in
          verdicts and settlements for injured clients and their families.
        </p>

        <p>
          Dangerous roadways are one of the most frequently cited causes of car
          accidents in Los Angeles and across the nation. However, they are also
          the least discussed when it comes to reducing injury accidents due to
          these hazardous conditions.
        </p>
        <p>
          As Los Angeles hazardous roadway attorneys, we've seen how roadways
          that are dangerously designed or maintained have the potential to
          cause catastrophic and/or fatal crashes. Investigators in such cases
          focus on the driver's actions and whether or not all vehicle occupants
          were buckled up. But, often times, they tend to overlook the part that
          the roadway had in causing or contributing to the{" "}
          <Link to="/los-angeles/car-accidents" target="_blank">
            {" "}
            car accident
          </Link>
          . Dangerous road defects are usually filed against government
          entities, which makes them costly and intimidating to most plaintiffs.
        </p>
        <p>Proving such a case can also be challenging.</p>
        <p>
          If you're a victim of such an incident, you need an attorney who has
          the resources and access to experts to evaluate the roadway in
          question and help you build a solid case against the governmental
          agency.
        </p>
        <p>
          Please call us to find out how we can help you. Contact us at{" "}
          <strong> (323) 238-4683</strong> and earn a{" "}
          <strong> free consultation with a top-notch legal expert.</strong>
        </p>
        <h2>Los Angeles's Challenge with Defective Roadways</h2>
        <p>
          It is no secret that the city of Los Angeles has long struggled with
          dangerous conditions such as crumbling infrastructures. This includes
          city streets that are riddled with potholes, uneven pavement,
          crumbling asphalt and cracked sidewalks.
        </p>
        <p>
          According to a recent report in the Los Angeles Times, TRIP, a
          nonprofit organization that reports on transportation issues, noted
          that the average Los Angeles-area driver spends $832 or 71 percent
          more than the average American on additional vehicle operating costs
          each year, mostly because driving on these deteriorated roadways uses
          up more gas and damages car parts such as wheels, suspensions and
          tires.
        </p>
        <p>
          About 25 percent of the city's roadways are said to be in a deplorable
          state of disrepair. As a result, drivers, motorcyclists, bicyclists
          and pedestrians are placed in a vulnerable situation.
        </p>
        <LazyLoad>
          <img
            src="../images/car-accidents/pot-hole-dangerous.jpg"
            width="100%"
            alt="Los Angeles Hazardous Roadway Attorneys"
          />
        </LazyLoad>

        <h2>Three Types of Roadway Hazards</h2>
        <p>
          Dangerous roadways exist primarily due to defective design and/or
          construction, poor maintenance or the failure by a governmental entity
          to make changes to the roadway in order to adapt to evolving
          conditions.
        </p>
        <p>
          <strong> Three types of common roadway hazards include</strong>:
        </p>
        <ol>
          <li>
            <strong> Faulty design:</strong> Examples of faulty design include
            dangerous curves in the roadway, slopes and dips, any types of
            hazards that block visibility at crosswalks or intersections and
            exit and entrance ramps that are too short and don't allow for safe
            merging. Other examples include dangerous or missing guardrails,
            lack of proper signage that warn motorists about dangerous
            conditions, poorly placed traffic signals and shoulders that are not
            visibly distinct from the roadway, etc.
          </li>
          <li>
            <strong> Poor construction:</strong> When construction plans are not
            followed properly or when the contractor uses substandard materials
            or hires inexperienced workers for the construction project, the
            resulting roadway may be one that proves dangerous for those using
            it.
          </li>
          <li>
            <strong> Inadequate maintenance:</strong> The proper maintenance of
            a roadway is usually the responsibility of the governmental entity
            under whose jurisdiction that roadway is located. Examples of shoddy
            maintenance include pothole-riddled roadways, dangerous guardrails,
            allowing erosion of the roadway, lowered visibility of painted
            markers and allowing signage to deteriorate or be covered by
            foliage, etc.
          </li>
        </ol>
        <h2>How Dangerous Roadway Cases Are Different</h2>
        <p>
          If you believe a dangerous or defective roadway caused your Los
          Angeles car accident, it is important that you quickly contact an
          experienced
          <strong> Los Angeles dangerous roadway lawyer</strong> who has handled
          similar cases. Your attorney may ask that you preserve your wrecked
          vehicle right away so it can be examined by an engineer who has
          expertise in roadway design and vehicle crashworthiness.
        </p>
        <p>
          During these inspections and evaluations, a number of details
          including information about the speed at which the car left the road
          and what caused it to leave the roadway may be revealed.
        </p>
        <p>
          Experts will look at crucial physical evidence such as skid marks,
          which must be photographed and measured. A knowledgeable lawyer will
          also work to get other important pieces of evidence such as
          photographs of the roadway, plans that were used to build the roadways
          and topographical maps of the area where your accident occurred. These
          items may provide valuable information about roadway design,
          construction and maintenance issues, if any.
        </p>
        <p>
          Dangerous roadway cases, in particular, can be complex. The defendant
          in these cases are not another person or insurance company, but a
          governmental agency such as a city, county or state, which has
          abundant resources at its disposal and deep pockets. Your lawyer will
          need to work with a traffic engineer and an accident reconstruction
          expert to determine what type of evidence is needed to prove your
          case. It is then important to establish that governmental immunity
          doesn't apply to the case.
        </p>
        <p>
          Please remember, you may have a case even if you are partly at fault
          for the accident. It is a good idea to consult with a dangerous
          roadway lawyer even if you have been determined at fault for the
          accident. The basis for these lawsuits is whether the government
          failed to fulfill its agreements to make the roadway safe. So, if a
          dangerous or defective roadway led to your accident, you may still
          have a dangerous roadway case against the governmental entity.
        </p>
        <LazyLoad>
          <img
            src="../images/car-accidents/investigating-car-accident.jpg"
            width="100%"
            id="body-image-positions"
            alt="dangerous roadway attorneys in Los Angeles"
          />
        </LazyLoad>
        <p>
          It is also important to remember that in dangerous roadway cases, the
          statute of limitations, or the deadline for filing a personal injury
          or wrongful death claim, is very short. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            {" "}
            California Government Code Section 911.2
          </Link>
          , plaintiffs have only six months from the date of an accident to file
          a claim with the governmental entity.
        </p>
        <p>
          If a claim is not filed within six months of 180 days from the date of
          the incident, then a petition should be immediately filed with the
          court seeking relief for the failure to file the claim within that
          required period. This petition must be filed within one year of the
          date of the incident. A failure to do so may bar any claim against the
          governmental entity. So, it is important that you act quickly to
          preserve your rights.
        </p>
        <p>
          In such cases, plaintiffs (injured victims) have the burden of proof,
          which means you, as the plaintiff, must show evidence of how the
          governmental entity was negligent and why they must be held
          responsible for your losses. In these types of cases, our attorneys
          collect valuable evidence about how the government knew or should have
          known about the dangerous condition – be it a maintenance issue or
          poor design – and why they should have repaired the condition, which
          caused injury or harm. Plaintiffs must also show that the dangerous or
          defective roadway condition directly caused or contributed to their
          injuries.
        </p>
        <h2>What is Your Claim Worth?</h2>
        <p>
          The value or worth of your car accident claim may depend on the nature
          and extent of injuries you sustained and the extent of negligence of
          the other parties involved. For example, if you have sustained a
          severe traumatic brain injury or if you have been paralyzed by a
          spinal cord injury, your claim may be worth millions of dollars.
        </p>
        <p>
          <strong>
            {" "}
            Compensation you can seek for your hazardous roadway claim
          </strong>
          :
        </p>
        <ul>
          <li>
            <strong> Medical expenses:</strong> This includes everything from
            emergency transportation to hospitalization, surgeries, cost of
            prescription drugs, medical equipment and other expenses relating to
            your medical care and treatment.
          </li>
          <li>
            <strong> Lost income:</strong> Dangerous roadway accidents often
            cause significant if not catastrophic injuries that lead to victims
            missing workdays and losing valuable income. You can claim lost
            earnings due to your injury. If you have lost your capacity to earn
            a livelihood, you may also be able to seek lost future income.
          </li>
          <li>
            <strong> Rehabilitation costs:</strong> When you suffer injuries, in
            addition to medical treatment, you may also require rehabilitative
            care on an ongoing basis. These costs may add up very quickly and
            often, are not covered by health insurance policies. Examples of
            rehabilitation might include physical therapy, chiropractic care,
            occupational therapy and so on.
          </li>
          <li>
            <strong> Pain and suffering:</strong> There is no question that
            accident victims undergo significant physical stress and emotional
            pain in the aftermath of a traumatic event. As part of your injury
            claim, you can also seek compensation for past and future pain and
            suffering caused by the defective roadway car accident.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="dangerous roadway lawyers in Los Angeles"
            style={{
              backgroundImage:
                "url('../images/text-header-images/brian-pointing.jpg')"
            }}
          >
            <h2>How Bisnar Chase Can Help</h2>
          </div>
        </LazyLoad>

        <p>
          The <strong> Los Angeles Hazardous Roadway Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            {" "}
            Bisnar Chase
          </Link>{" "}
          have a long and successful track record of handling and winning
          dangerous roadway cases on behalf of injured clients.
        </p>
        <p>
          Our law group offers clients a{" "}
          <strong> no-win-no-fee guarantee</strong>, which means we don't charge
          you any fees or costs unless we win compensation for you. This is
          particularly beneficial to victims of dangerous roadway cases as the
          initial costs to retain experts, obtain depositions and preserve
          evidence will be significant.
        </p>
        <p>
          If you or a loved one has been injured as the result of a hazardous
          roadway condition in Los Angeles, please contact us at
          <strong>
            {" "}
            323-238-4683 for a free, comprehensive and confidential consultation
          </strong>
          .
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
