// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/santa-clarita-city-overview-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Santa Clarita Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Dog bites & swimming pool accidents, to car accidents & premise liabilities, our Santa Clarita Personal Injury Lawyers have experienced it all. Our law firm has a 96% success rate and over 40 years of experience. Let our skilled attorneys represent you. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Santa Clarita Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Santa Clarita Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Santa Clarita is the third most populated city in Los Angeles county
          and the seventeenth most populated city in the state of California,
          making it important to have a skilled
          <strong> Santa Clarita Personal Injury Lawyer</strong> available if
          the time comes. Santa Clarita is home to the popular amusement park
          known as Six Flags Magic Mountain as well as the California Institute
          of the Arts and the recreational area of Castaic Lake that has be
          featured in many television programs.
        </p>
        <p>
          This community, just north of the San Fernando Valley is home to a
          handful of families in Southern California as well as many personal
          injury cases that occur all year long.
        </p>
        <p>
          Between Six Flags, the busy freeways and the boating that goes on in
          Castaic Lake, a lot of Santa Clarita citizens suffer personal injury
          due to the negligence of a third party. Personal injury may put you
          out of work and leave you drowning in debt, however, if you are
          injured, there is hope.
        </p>
        <p>
          The attorneys at Bisnar Chase have been representing clients in the
          Santa Clarita for over <strong> 39 years</strong>. Our{" "}
          <Link to="/los-angeles/boating-accidents">
            Los Angeles boating accident attorneys
          </Link>{" "}
          have helped may victims who have been injured at Castaic Lake and
          other recreation bodies of water throughout Southern California. It
          does not matter if you have been injured in a car, bus or boat and it
          does not matter where you have been injured as well. If you have been
          injured, call us today and we will fight for your compensation.
        </p>
        <p>
          Bisnar Chase offers our clients, both present and future, the
          reassurance of our <strong> 96% success rate</strong> as well our{" "}
          <strong> "No Win, No Fee" promise</strong>. We win or
          <strong> you do not pay us at all</strong>. Take comfort in the fact
          that when you choose the Santa Clarita personal injury attorneys of
          Bisnar Chase, you are choosing the best legal team in Southern
          California to represent your personal injury claim. Call us now at{" "}
          <strong> 323-238-4683</strong> to schedule your{" "}
          <Link to="/los-angeles/contact">
            <strong> Free Case Evaluation</strong>
          </Link>{" "}
          with our award-winning team.
        </p>
        <h2>Common Personal Injuries Sustained in Santa Clarita</h2>
        <p>
          The following incidents are some of the many injuries you can sustain
          in Santa Clarita from the negligence of a third party:
        </p>
        <ul>
          <li>
            <strong> Amusement park accidents:</strong> Santa Clarita, as we
            stated above, is home to{" "}
            <Link to="https://www.sixflags.com/magicmountain" target="new">
              Six Flags Magic Mountain
            </Link>
            , home of some of the fastest and most thrilling roller coasters in
            Southern California. To keep this title, the park keeps pushing for
            faster and more exciting rides, however, with this push, more
            visitors are prone to accidents. If you have been injured in a theme
            park, call our law office now.
          </li>
          <li>
            <strong> Traffic accidents:</strong> Santa Clarita is situated
            between{" "}
            <Link to="/highway-locations/interstate-route-5-freeway-accidents">
              Interstate 5
            </Link>
            , California State Highway 14 and California State Highway 126.
            These are some of the busiest freeways in the state. It goes without
            saying that hundreds of accidents occur around these areas. If you
            have been injured, call us now.
          </li>
          <li>
            <strong> Nursing home abuse: </strong>Santa Clarita is just North of
            Los Angeles and makes for a perfect spot for seniors to retire,
            however, due to the high population count, nursing homes may not be
            able to fully accommodate our elderly loved ones and may cut corners
            without us knowing. If your loved one was abused or neglected in a
            nursing home, we urge you to call us now.
          </li>
          <li>
            <strong> Premises liability claims:</strong> Property owners have
            the legal obligation to take proactive measure to ensure your
            safety. You should feel safe and secure at all times in Santa
            Clarita. If you have been injured and a property owner's negligence
            contributed to your injuries, you may be entitled to compensation.
            Call us today for more information on these types of cases.
          </li>
          <li>
            <strong> Dog bite claims:</strong> All dog owner's in Santa Clarita
            are legally and financially responsible for their dogs according to{" "}
            <Link
              to="http://law.onecle.com/california/civil/3342.html"
              target="blank"
            >
              {" "}
              California's strict liability statute
            </Link>
            . If you or someone you know has been injured in a dog bite
            accident, you may be entitled to compensation. Call us now for more
            information.
          </li>
        </ul>
        <p>
          These are just some of the many types of personal injury a person can
          sustain in Santa Clarita. Other injuries include:
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/product-liability" target="new">
                Product Liability{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/brain-injury" target="new">
                Brain Injury{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/workplace-harassment" target="new">
                Workplace Harassment{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/employment-lawyers" target="new">
                Wrongful Termination{" "}
              </Link>
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/hit-and-run-accidents" target="new">
                Hit and Run Accidents{" "}
              </Link>
            </strong>
          </li>
        </ul>
        <p>
          If you have become a victim of any of the accidents stated above, you
          may be entitled to compensation. You have rights as a victim and our
          Santa Clarita personal injury lawyers will help you to uphold them in
          court.
        </p>
        <h2>Preventable Injuries Caused by Negligent Security</h2>
        <p>
          Santa Clarita can make for a great get away from the hustle and daily
          grind of Los Angeles. Its proximity to LA makes it a popular
          destination to get away for an afternoon or weekend. Wherever you
          spend your time, be sure you know your rights as a citizen and
          consumer.
        </p>
        <p>
          Under the law, business and property owner have the legal
          responsibility of ensuring safety and security to all visitors in
          attendance. These property owners must have adequate security guards
          and measures in place to prevent injuries and accidents. A property
          owner who follows the law will have the following on their premises at
          all times:
        </p>
        <ul>
          <li>
            Well trained security guards to reflect the number of visitors
            present whom have passed background checks for criminal behavior
          </li>
          <li>Cameras and other recording devices to document accidents</li>
          <li>Adequate lighting in parking lots and designated areas</li>
          <li>
            Fences, locks and barriers to secure the premises when deemed
            necessary
          </li>
          <li>Policies and protocol in place to ensure safety of visitors</li>
        </ul>
        <p>
          If the theme park, shopping center or other form of property open to
          the public does not have one or more of these measures in place, then
          this is legally defined as negligence and you may be entitled to
          compensation if you become injured on their property.
        </p>
        <p>
          Cases of negligent security has the potential to occur in a wide
          variety of places within Santa Clarita. Some of these places include:
        </p>
        <ul>
          <li>Schools</li>
          <li>Restaurants/Bars</li>
          <li>Amusement Parks</li>
          <li>Universities/Dorms</li>
          <li>Malls/Shopping Centers</li>
          <li>Apartments/Hotels</li>
        </ul>
        <p>
          As a law-abiding citizen in Santa Clarita, you deserve the peace of
          mind that you are protected and safe from danger at all times no
          matter where in California you spend your time. If a property owner
          does not take the time to ensure this safety on their own premises,
          they should be held responsible for their negligence.
        </p>
        <p>
          If you have been injured on someone else' property and their
          negligence contributed to your injury, you may be entitled to
          compensation and our Santa Clarita personal injury attorneys may be
          able to help you take legal action against the property owner.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="The Importance of Medical Treatment After an Accident"
            style={{
              backgroundImage:
                "url('/images/text-header-images/santa-clarita-medical-services.jpg')"
            }}
          >
            <h2>The Importance of Medical Treatment After an Accident</h2>
          </div>
        </LazyLoad>

        <p>
          If you have been injured, your first course of action should be to
          seek medical attention.
        </p>
        <p>
          Seeking medical attention is essential for two reason: your health is
          secured and your injuries are legally documented.
        </p>
        <p>
          At Bisnar Chase, our greatest concern is your well-being. We want you
          to live a long and prosperous life with your friends and loved ones
          and the best way to secure that future is to see a medical
          professional immediately after you have been injured. In addition,
          your visit to the hospital serves as legal documentation of your
          injures that our Santa Clarita personal injury attorneys can use to
          fight for compensation for the injuries that you sustained from your
          accident as well as any additional cost pertaining to your recovery.
        </p>
        <h2>How to Fight the Insurance Companies for Fair Compensation</h2>
        <p>
          One of the worst things to deal with after a personal injury accident
          are the insurance companies. These companies like to claim they have
          your best interests in mind, however, they have trained their
          adjusters to low-ball you and make this time in your life as
          uncomfortable as possible so you can take a low-ball settlement for a
          quick resolution. Do not to give into this type of manipulation. You
          have rights as a victim that the insurance companies tend to step on.
          The following tips will help you take on the bureaucratic insurance
          companies to win a fair settlement for your injuries.
        </p>
        <ul>
          <li>
            <strong> Only discuss case details with your attorney</strong>: It
            may be tempting to take to social media and tell all your friends
            that you're okay after an incident, however, doing so may affect
            your settlement offer. The insurance companies are relentless and
            will use anything to prevent you from receiving adequate
            compensation. If you take pictures of the accident scene, do not
            post them Online. Do not state what happened pertaining to your
            accident. Do not even announce that you plan to work with an
            attorney. Keep everything pertaining to your case confidential and
            only share with your attorney.
          </li>
          <li>
            <strong> Be prepared and do your research:</strong> Insurance
            companies will harass you while you are recovering. They want you to
            issue a statement while you are off-guard so they can use your words
            against you. You have a right to talk to your lawyer before talking
            to the insurance companies.
          </li>
          <li>
            <strong> Know your rights:</strong> You also have a right to opt-out
            of a recorded statement. Make this clear when you do plan to talk to
            the insurance companies and state this first before all else. You
            also have the right to tell them just the necessary information and
            nothing more. Do not let these companies take advantage of you.
          </li>
          <li>
            <strong> Always consult with your attorney:</strong> Update your
            lawyer on the facts of your case as it unfold. Only your attorney
            can inform you on the best course of action.
          </li>
        </ul>
        <h2>Do Not Settle for a Low-Ball Settlement Offer</h2>
        <p>
          Whatever offer is on the table will be your final offer as soon as you
          accept it. This is why it is crucial to have a knowledgeable Santa
          Clarita personal injury attorney with you during this time because
          your lawyer will be able to assess the offer on the table and let you
          know if it will adequately cover your damages and recovery costs.
        </p>
        <p>
          If you rush into a settlement offer and find out later that the
          settlement did not cover all the costs of your injury, you will be out
          of luck. Victims can no longer pursue compensation after this point.
        </p>
        <h2>Contingency Based Law Firm</h2>
        <p>
          You may be looking at the information on this page and wondering how
          you can afford such successful Santa Clarita personal injury lawyers
          to represent your case in court. We want to remind you that Bisnar
          Chase is a contingency based law firm.
        </p>
        <p>
          This means that if you choose our legal services, you only pay once we
          have claimed victory for you and your loved ones.{" "}
          <strong> "No Win, No Fee."</strong> This has been our promise to our
          clients since the beginning and will continue to be our promise for
          clients to come.
        </p>
        <p>
          We understand the stress you and our family are under after a personal
          injury accident. In some rare cases, injures can be catastrophic and
          you may lose the ability to earn income and be left with emotional
          distress.
        </p>
        <p>
          It is our firm belief that anyone who has cause you injury, be it
          direct or indirect, should be held legally responsible and compensate
          you accordingly.
        </p>
        <p>
          We will help you to seek justice and in the rare case we cannot
          deliver that promise to you or lose your case in court, you will not
          have to pay us at all.
        </p>

        <h2>Southern California Lawyers that You Can Trust</h2>
        <p>
          We see our clients as family and we will treat you just the same when
          you sign up for your free consultation today.
        </p>
        <p>
          We have been serving Santa Clarita for over{" "}
          <strong> 39 years </strong>and have a{" "}
          <strong> 96% success rate</strong> with our past clients and we may be
          able to help your with your personal injury claim as well.
        </p>
        <p>
          Put your faith in a legal team that you can trust, and if you do not
          take our word for it, see what our former clients have to say about
          our firm and services.
        </p>
        <p>
          If you or a loved one has experienced a{" "}
          <strong> Personal Injury, </strong>contact our
          <strong>
            {" "}
            highly experienced Santa Clarita Personal Injury Attorneys{" "}
          </strong>
          for a<strong> Free Case Evaluation</strong> at{" "}
          <strong> 323-238-4683.</strong>
        </p>
        <p>
          Our team at Bisnar Chase helped Alexis when her life turned upside
          down after a car accident. We helped her get back on her feet in no
          time at all and we may be able to do the same for you!
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
