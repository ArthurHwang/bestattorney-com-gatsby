// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/hostile-work-meeting-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Workplace Harassment Attorneys of Bisnar Chase"
        pageDescription="Working environments should be efficient. Our Los Angeles Workplace Harassment Attorneys have zero-tolerance towards disrespect, violations & hostility in the office. If you have been experiencing inappropriate or aggressive behavior at your job, call our discrimination lawyers for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Workplace Harassment Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles workplace harassment lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you have suffered from workplace harassment contact the{" "}
          <Link to="/los-angeles">
            <strong> Los Angeles Personal Injury Attorneys</strong>
          </Link>{" "}
          of Bisnar Chase to see if you might be entitled to compensation.
        </p>
        <p>
          Workplace Harassment and Discrimination is a{" "}
          <strong> civil rights violation.</strong> If your civil or
          constitutional rights have been violated by discrimination or
          harassment at your job, contact Bisnar Chase at <b>323-238-4683.</b>
        </p>
        <p>
          Civil rights are the basic rights afforded to all Americans by the{" "}
          <Link
            to="https://www.archives.gov/founding-docs/constitution-transcript"
            target="new"
          >
            <strong> Constitution of the United States</strong>
          </Link>{" "}
          and to all Californians by the Constitution of the State of
          California. These rights are not to be breached under any
          circumstances according to the law. Unfortunately, people have their
          civil rights violated every day, and one place this happens frequently
          is in the workplace.
        </p>
        <p>
          The Constitutions of both the United States and the State of
          California prohibit discrimination based on:
        </p>
        <p>
          <strong> Race, National Origin, Religion, Gender, and Age.</strong>
        </p>
        <p>
          But these violations are consistently practiced by employers. One of
          the reasons for this is that people often do not stand up for their
          rights when they are violated, allowing employers to continue these
          harmful practices.
        </p>
        <h2>Subtle and Light Harassment Is Not Okay</h2>
        <p>
          Poor management and teamwork skills is indicated when any type of
          hostile, unnecessary or aggressive behavior is shown. There is no
          reason for their to be unwanted situations in the workplace.
        </p>
        <p>Many types of inappropriate and unwanted activities can include:</p>
        <p>
          <strong> Aggressive Behavior</strong>: Holdings, squeezing, pushing,
          shoving, inappropriate horseplay.
        </p>
        <p>
          <strong> Sexual Behavior</strong>: The workplace is supposed to be a
          professional environment, where sexual chat, actions or any types of
          deeply intimate encounters or behaviors is completely unacceptable.
        </p>
        <p>
          <strong> Bribes or Threats</strong>: Any form of persuasion or
          bribery, especially hinting or threatening to do so in order to keep
          your job, receive a promotion or make upper management pull in your
          direction is not only a crime, but extremely unprofessional and
          unacceptable.
        </p>
        <p>
          <strong> Bullying or Harassment</strong>: This is not middle school,
          instead, a professional environment where mature adults conduct
          business. There is no reason and no tolerance for any form of
          bullying, taunting, harassing or unkind provoking of any form.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/bullying-in-the-workplace.jpg"
            width="100%"
            alt="bullying-in-the-workplace.jpg"
          />
        </LazyLoad>

        <p>
          Civil rights violations in the workplace can include discrimination
          based on arbitrary measures, such as:
        </p>
        <li>Race</li>
        <li>Gender</li>
        <li>Sexual orientation</li>
        <li>Handicap</li>

        <p>
          Civil rights violations also include sexual harassment and other forms
          of{" "}
          <Link to="/los-angeles/hostile-work-environment">
            <strong> job-based harassment</strong>
          </Link>
          , and targeted behavior toward individuals who threaten the company
          with exposure of these practices.
        </p>

        <p>
          California discrimination and harassment attorneys stand up for those
          who have experienced civil rights violations in the workplace. They
          work with these victims to obtain the best possible outcome for their
          cases, which may include monetary damages, reinstatement of job
          rights, or removal of the person or people responsible for the
          harassment. No matter what the remedy, civil rights attorneys are able
          to make the situation better for the injured employee and return the
          balance of power to its rightful status.
        </p>
        <h2>Workplace Harassment Statistics</h2>
        <p>
          The place you call work shouldn't be a feared place. It should be warm
          and welcoming, resulting in efficient productivity, team building and
          professionalism. Workplace harassment consists of:
        </p>
        <ul>
          <li>
            <strong> Bullying</strong>
          </li>
          <li>
            <strong> Yelling</strong>
          </li>
          <li>
            <strong> Verbal abuse</strong>
          </li>
          <li>
            <strong> Physical abuse</strong>
          </li>
          <li>
            <strong> Discriminatory intimidation</strong>
          </li>
          <li>
            <strong> Unwanted sexual advances or remarks</strong>
          </li>
          <li>
            <strong> Hate speech</strong>
          </li>
        </ul>
        <p>
          For more information on workplace hostility, terminology and other
          aspects of workplace harassment visit{" "}
          <Link to="http://www.aauw.org/" target="new">
            <strong> American Association of University Women</strong>
          </Link>
          <strong> .</strong>
        </p>
        <h2>A Los Angeles Workplace Harassment Law Firm That Fights For You</h2>
        <p>
          Civil rights lawyers in California not only help the individual
          victims of civil rights violations but serve an important function in
          maintaining the rights of all employees. Harassment or discrimination
          that is allowed to continue against one employee means that all
          employees are vulnerable, even if these employees are not directly
          affected by the harassment or discriminatory practices.
        </p>
        <p>
          An employer who is allowed to mistreat one employee has
          <strong>
            {" "}
            no reason not to try the same thing with others, and eventually
            other employees will feel the effects of this harassment.
          </strong>{" "}
          Civil rights are based on the idea that there are no exceptions;
          everyone deserves equal protection under the law, and everyone has the
          right to a workplace free of violence, harassment, discrimination, and
          other practices designed to deprive people of their rights.
        </p>
        <p>
          Further, workplace discrimination and harassment{" "}
          <strong> destroy morale</strong> and cause employees to be{" "}
          <strong> fearful and angry at work.</strong> Your workplace should be
          an environment in which you feel safe, and if there are discriminatory
          or harassing practices going on, you cannot possible feel secure in
          your job. Civil rights attorneys benefit all employees by taking up
          the case of an individual and restoring safety and security to the
          workplace environment.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/414yNyALsPo"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Protecting Your Constitutional and Statutory Rights</h2>
        <p>
          If you have been the victim of any type of harassment, discrimination,
          or other treatment at the hands of an employer or fellow employee, it
          is time to take action. Contact a workplace harassment lawyer today
          about your case and stand up for what is right for yourself and your
          fellow employees.
        </p>
        <p>
          Let us fight for you. Speak with one of our award-winning Los Angeles
          civil rights attorneys at <b>323-238-4683</b>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
