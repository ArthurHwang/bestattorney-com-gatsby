// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/aviation-crash-airplane-accident-premise-liability-attorneys-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Aviation Accident Lawyers Los Angeles - Plane Crash Attorneys"
        pageDescription="Airplanes are one of the safest ways to travel. But when negligence, severe weather or mechanical issues occur, it can be catastrophic. The Los Angeles Aviation Accident Lawyers at Bisnar Chase are aggressive and will win, or you do not pay. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Aviation Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles aviation accident lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Bisnar Chase has an experienced aviation disaster team that handles
          commercial plane accidents, private plane crashes, and suspicious
          helicopter accidents coming in or leaving Los Angeles. Additionally,
          if you are a resident of L.A. and were involved in an international or
          domestic plane crash you may be entitled to compensation.
        </p>
        <p>
          Airline companies and their insurers have extremely skilled
          professionals looking out for their best interests soon after a
          catastrophic crash. In the immediate aftermath of a tragedy, most
          families are focused on caring for injured loved ones or arranging
          funeral services. Many victims and their families know little about
          their own legal rights under these circumstances.
        </p>
        <p>
          Monetary damages may not be the first thing in your mind during this
          difficult time. But, negotiating directly with the airline or the
          insurance company may not result in a positive outcome for you or your
          family.
        </p>
        <p>
          An experienced Los Angeles aviation accident lawyer who has obtained
          significant verdicts and settlements on behalf of injured clients and
          their families, can make a big difference in your case.
        </p>
        <p>
          The skilled{" "}
          <Link to="/los-angeles/wrongful-death">
            Los Angeles wrongful death lawyers
          </Link>{" "}
          at Bisnar Chase have the knowledge and experience it takes to ensure
          that your rights are protected. Los Angeles Aviation accidents often
          involve complex issues and a number of parties. Our law firm has
          access to nationally renowned engineers, pilots and other experts who
          can help bolster your case. We work on a contingent fee basis, which
          means you don't pay us anything until we recover compensation for you.
        </p>
        <p>
          <b>
            Please contact us at (323) 238-4683 for a free consultation and
            comprehensive case evaluation pertaining to a plane crash from or to
            Los Angeles.
          </b>
        </p>
        <h2>Causes of Aviation Accidents</h2>
        <p>
          Determining the cause of an aviation accident or a near crash that
          results in injuries can be complicated. The leading causes of aviation
          accidents include:
        </p>
        <ul>
          <li>Pilot error</li>
          <li>Defective equipment</li>
          <li>Poorly maintained equipment and faulty repairs</li>
          <li>Defective parts</li>
          <li>Defectively designed airplanes</li>
          <li>Structural issues with the plane</li>
          <li>Fuel problems</li>
          <li>Mistakes made by federal air traffic controllers</li>
        </ul>
        <p>
          In many instances, there is a combination of factors that lead to the
          accident. The NTSB is charged with investigating aviation accidents,
          but the Federal Bureau of Investigation (FBI) and the Federal Aviation
          Administration (FAA) often get involved in the investigation of large
          aviation accidents. In some cases, the plaintiffs and their attorneys
          conduct private investigations as well.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles aviation accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/aviation-accident-statistics-attorneys-lawyers.jpg')"
            }}
          >
            <h2>Aviation Accident Statistics</h2>
          </div>
        </LazyLoad>
        <p>
          Traveling by air is statistically safer than any other mode of travel.
          However, aviation accidents do occur and when they do, the results are
          often tragic and devastating. There is no such thing as a minor crash
          landing or an insignificant airplane collision.
        </p>
        <p>
          These events are typically life altering. Aviation accidents could
          involve a mass carrier, a small private airplane or even a helicopter.
          It is crucial that victims and their families understand their legal
          rights and options in such cases.
        </p>
        <p>
          According to the National Transportation Safety Board (NTSB), there
          were 1,471 general aviation accidents in the United States in 2012,
          which resulted in 432 fatalities. During the same year, there were
          1,539 civil aviation accidents resulting in 447 fatalities. To
          compare, in the year 2011, there were 32,367 motor vehicle accident
          fatalities, 759 railroad-related fatalities, 800 boat accident
          fatalities and 494 aviation fatalities.
        </p>
        <h2>Potential Claims</h2>
        <p>
          An investigation into an aviation accident may not be resolved for
          several months. But the results of an investigation are crucial for
          victims' families because it helps them find answers to nagging
          questions about what caused their loved ones' injury or death. Injured
          victims or families of deceased victims can also file civil personal
          injury or wrongful death claims seeking compensation for their
          damages. These claims may be filed against a negligent or at-fault
          party. For example, if the crash occurred due to pilot error, a
          lawsuit can be filed against the pilot and his or her employer. If the
          accident occurred due to a defective airplane or faulty part, the
          manufacturer of the aircraft or the defective part can be held liable.
          When mistakes made by air traffic control contribute to an accident,
          victims or their families may be able file a lawsuit against the FAA
          under the{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Federal_Tort_Claims_Act"
            target="_blank"
          >
            {" "}
            Federal Tort Claims Act
          </Link>{" "}
          (FTCA). In such cases plaintiffs must show that the negligent or
          wrongful conduct of a federal employee led to the crash. In cases
          where reckless conduct on the part of a pilot or an airline leads to
          serious injury or death, criminal charges may also be filed by the
          state and/or federal government.
        </p>
        <h2>Assistance for Families</h2>
        <p>
          The federal government provides support to aviation accident victims
          and their families in the event of a major airplane accident. Under
          the{" "}
          <Link to="http://www.ntsb.gov/tda/" target="_blank">
            {" "}
            Aviation Disaster Family Assistance Act
          </Link>
          , the NTSB is responsible for assigning a nonprofit organization to
          coordinate services for the victims. That independent organization
          will help the victims and their families receive mental health
          services and grief counseling as well. They will also help the
          families of victims with travel arrangements and provide them with
          daily briefings regarding the investigation. They can even provide
          assistance in arranging a memorial service and identifying the
          victims.
        </p>
        <p>
          The airlines involved in the crash are responsible for providing a
          number of services as well. The airline must establish a free phone
          number for the families of the victims. They must provide a list of
          all the passengers on the flight to the families before releasing the
          list to the public. They must also notify families of deceased victims
          and assist them with room and board.
        </p>
        <p>
          To learn more information about your rights contact our Los Angeles
          plane crash attorneys for a free case evaluation. Call 323-238-4683
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        >{/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
