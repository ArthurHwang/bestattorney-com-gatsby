// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/south-bay-personal-injury-los-angeles.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="South Bay Personal Injury Lawyers - Bisnar Chase"
        pageDescription="Premise liabilities, car accidents and other types of injury related accidents can always happen. The South Bay Personal Injury Lawyers have over 40 years of experience fighting & winning these cases. Our attorneys will win maximum compensation or you don't pay. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>South Bay Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="south bay personal injury attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The South Bay is one of the many unique regions that make up Southern
          California. The area gets its name from its geographic location that
          is southwest of Los Angeles. Some of the many cities that makes this
          region what it is include: El Segundo, Manhattan Beach, Torrance,
          Hermosa Beach, Inglewood, Redondo Beach, Rancho Palos Verdes, San
          Pedro, Carson and Hawthorne. The area is also home to institutions of
          higher learning including Cal State Dominguez Hills and Loyola
          Marymount University as well as LAX, one of the nation's largest and
          busiest airports.
        </p>
        <p>
          The South Bay has much to offer residents and visitors alike. However,
          between the beaches, the streets, and the prevailing aerospace
          industry, chances are high for a personal injury accident.
        </p>
        <p>
          No one wishes to be hurt and most people are good about taking
          proactive measures to defend themselves from these types of accidents
          occurring to them. Unfortunately, life can hit you and your family
          when you least expect. In your time of need, you are going to need an
          experienced legal team around to help you get back on your feet. Our
          South Bay personal injury attorneys are here to help.
        </p>
        <p>
          Our{" "}
          <Link to="/los-angeles/car-accidents">
            Los Angeles car accident attorneys
          </Link>{" "}
          excel in representing injured motor vehicle accident victims and our
          experienced team can take on any other personal injury case you may
          have. Our legal team will be there for you when you need help the
          most. Our past South Bay clients have taken comfort in our
          <strong> 96% success rate</strong> and{" "}
          <strong> "No Win, No Fee" promise</strong>. We put our clients'
          interests above our own so we can get them fully compensated for their
          injuries and serve as their legal voice against big corporations and
          government.
        </p>
        <p>
          Call us now at <strong> 323-238-4683</strong> to schedule your{" "}
          <Link to="/los-angeles/contact">free case evaluation </Link> with our
          legal team.
        </p>
        <h2>South Bay Personal Injuries Accidents</h2>
        <p>
          Our South Bay personal injury lawyers have helped and represented
          clients in Los Angeles County and the South Bay collectively for over
          35 years. Most of our former clients sought out our services because
          we are experts in dog bite law. Dog bite accidents occur more often in
          the South Bay than anywhere else and thanks to{" "}
          <Link
            to="http://law.onecle.com/california/civil/3342.html"
            target="blank"
          >
            {" "}
            California's strict liability statute
          </Link>
          , getting compensation for your dog bite injuries may be easier than
          you think. In addition to dog bite cases, we also take the following
          personal injury cases:
        </p>
        <ul>
          <li>
            <strong> Swimming pool accidents:</strong> The beaches of South Bay
            are often populated with visitors from out of town. This is why many
            residents prefer to spend the day at the pool. Swimming pools can be
            a great way to beat the SoCal heat, however, pools can be dangerous
            if safety measures are not enforced. If your loved one drowned or
            was injured in a swimming pool accident, you may be entitled to
            compensation. Call us now for more information.
          </li>
          <li>
            <strong> Premises liability claims:</strong> South Bay is home to
            many industrial businesses, music venues, night clubs and other
            businesses that open their doors to the public. Likewise, it is the
            legal obligation for these properties to keep their premises safe at
            all times. If someone is hurt or harassed on the property, the
            property owner may be held liable. Call us now for more information.
          </li>
          <li>
            <strong> Defective products:</strong> South Bay is home to many
            companies in the aerospace, oil and automotive industries. Likewise,
            these companies have the legal obligation to rigorously test their
            products for safety before putting them on the market. If you have
            been injured by a defective product, you may be entitled to
            compensation. Call us today for more information
          </li>
          <li>
            <strong> Traffic accidents:</strong>{" "}
            <Link to="/highway-locations/interstate-route-405-freeway-accidents">
              The 405 freeway
            </Link>
            , arguably one of the busiest highways within California and often
            faces traffic congestion. These conditions put drivers "on edge"
            which ultimately leads to more car accidents. If you have been
            injured via a car, truck, bike or motorcycle, call us now. Even if
            you are a pedestrian, if you have been injured, you may qualify for
            compensation.
          </li>
          <li>
            <strong> Workplace accidents: </strong>When the South Bay is
            combined with Long Beach, this area is the fifth busiest port in the
            nation. Since demand is so high, workplaces across the South Bay may
            turn toxic due to stress and demand. In addition, your employer may
            short change you on your pay and/or benefits to keep profits margins
            high. Both instances are direct violations of your rights as a
            worker. You can also be hurt on the job if dangerous conditions are
            present in the workplace and your injuries may prevent you from
            earning a steady income for an extended amount of time. If you have
            been affected by any of the previously mentioned scenarios, remember
            you have rights and you also may qualify for compensation for your
            losses. Call us now for more information
          </li>
        </ul>
        <p>
          During your free consultation, our lawyers will sit down with you to
          discuss the facts of your incident. We will make you feel right at
          home while we inform you of your rights and help you take legal action
          if your case qualifies.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Negligent Security in South Bay"
            style={{
              backgroundImage: "url('/images/text-header-images/gavel.jpg')"
            }}
          >
            <h2>Negligent Security in South Bay</h2>
          </div>
        </LazyLoad>

        <p>
          As we mentioned above, South Bay is home to many businesses,
          institutions and forms of entertainment. There is always something to
          do and something to see when you are in the area, however, no matter
          where you go, the owner for the given property you are vising must
          always ensure a safe and secure environment that guarantees your
          safety.
        </p>
        <p>
          Negligent security is defined as a form of premises liability. If an
          individual is hurt on the property and can prove inadequate or
          non-existent security personnel and measures contributed to the
          injury, the individual can hold the owner or tenant legally
          responsible for the injuries sustained on the property.
        </p>
        <p>
          To prove negligent security contributed to your injuries, one of the
          following factors must have been a present variable that contributed
          to your accident:
        </p>
        <ul>
          <li>
            Security was understaffed and thus could not accommodate the safety
            needs of those present
          </li>
          <li>
            Cameras and other surveillance devices were not present around the
            area of the accident during the time of injury
          </li>
          <li>
            Safety protocol and measures were not instated at the time of the
            accident
          </li>
          <li>
            Security personnel and other responsible employees were not
            adequately trained prior to the accident.
          </li>
          <li>
            The absence of locks, fences and barriers prior and during the
            accident contributed to the incident
          </li>
          <li>
            Property owner neglected use of criminal background checks on
            security personnel prior to the accident
          </li>
        </ul>
        <p>
          A responsible and law-abiding property owner will ensure none of the
          factors are present when guests and prospects are on the premises.
        </p>
        <p>
          Negligent security is a horrifying to think about because it has the
          potential to occur in many places in the South Bay, including:
        </p>
        <ul>
          <li>Schools/Universities</li>
          <li>Apartments/Dorms</li>
          <li>Malls/Shopping Centers</li>
          <li>Beaches</li>
          <li>Night Clubs/Bars</li>
          <li>Music Venues</li>
        </ul>
        <p>
          If you believe the property owner's negligence contributed to the
          injuries you sustained on their property, we urge you to call our
          South Bay personal injury lawyers now. You may be entitled to
          compensation and we want to see justice served.
        </p>
        <h2>What to Do After an Accident</h2>
        <p>
          Your highest priority after a personal injury accident should be to
          seek immediate medical help for you and your injured party.
        </p>
        <p>
          Our South Bay personal injury attorneys care about your well-being
          more than anything else and a trip to the hospital will be able to
          address the injuries you have sustained as well as to prevent any
          long-term disabilities from forming.
        </p>
        <p>
          In addition, when you visit a medical professional, they will document
          any injuries that you have sustained the day of your accident. You
          lawyer can later use these records to prove the validity of your
          injuries to the court and ultimately help your attorney fight for your
          compensation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="The Insurance Companies Are Not Your Friends"
            style={{
              backgroundImage:
                "url('/images/text-header-images/insurance-companies-not-your-friend.jpg')"
            }}
          >
            <h2>The Insurance Companies Are Not Your Friends</h2>
          </div>
        </LazyLoad>

        <p>
          Clever and enticing television ads make many Americans think that
          insurance companies are working in our best interest. Although we
          would like to think the trustworthy image of insurance companies that
          they have painted in our minds is true, the reality is in fact the
          complete opposite.
        </p>
        <p>
          Insurance companies put profit margins at a higher priority compared
          to their loyal customers. These companies care more about having a
          successful business quarter than being fair and adequately
          compensating their paying customers. It may be hard to take on the
          insurance companies, however, it definitely is not impossible. The
          following tips from our South Bay personal injury lawyers will
          increase your chances of a better settlement offer from these
          companies:
        </p>
        <ul>
          <li>
            <strong> Keep your mouth shut</strong>: Do not post any facts about
            your case Online. Do not even let your friends know that you are
            okay or post pictures of the accident scene. Anything posted Online
            can potentially be used against you by the insurance companies.
          </li>
          <li>
            <strong> Take time to prepare:</strong> Insurance companies will try
            to corner you for a statement when you are the least prepared and
            the most distraught. You are well in your rights to put them off
            until you have regained your composure and have spoken to your
            attorney.
          </li>
          <li>
            <strong> Opt-out of a recorded statement:</strong> You have the
            right to not have your statement recorded. If you are recorded, the
            insurance companies could potentially use your statement against
            you. Make it clear before you issue your statement that you do not
            want to be recorded and ask for conformation from the adjuster that
            you are speaking to.
          </li>
          <li>
            <strong> Discuss your case details to only your attorney:</strong>{" "}
            Only your lawyer should know what happened to you during your
            accident. Avoid conveying case details with friends, family and
            co-workers. Additionally, inform your attorney of new facts as your
            case unfolds.
          </li>
        </ul>
        <h2>Always Fight for a Better Settlement Offer</h2>
        <p>
          Most victims who are injured want nothing more than to put their
          ordeal behind them and move on. The insurance companies are well aware
          of this and will use your anxious emotions against you to put you in a
          corner so you will accept a low-ball settlement offer. Our South Bay
          personal injury lawyers urge you not to give in.
        </p>
        <p>
          Only a trained injury attorney can assess the offer that is on the
          table and let you know if it adequately covers the costs of your pain,
          suffering, injuries, recovery and any other miscellaneous costs
          pertaining to your claim.
        </p>
        <p>
          If you go through this process alone and accept a low-ball settlement
          offer, then your claim will end as soon as you sign the agreement.
          What this ultimately means is that if you find out later down the road
          that the settlement offer that you were presented did not cover the
          full cost of recovery, you will be forced to pay the existing
          remainder out of pocket. Your attorney will no longer be able to help
          you pursue further compensation for you claim. This is why we cannot
          stress enough the importance of a skilled personal injury attorney
          during this time of negotiation after your accident.
        </p>

        <h2>Contingency Based Law Firm</h2>
        <p>
          Being involved in a personal injury accident can put your life in a
          tail spin. One moment life is great and the next, you are struggling
          to pay your bills while not being able to work.
        </p>
        <p>
          It is an intimidating and difficult time and many of you reading this
          may think that you cannot afford the top-quality service our South Bay
          personal injury attorneys offer. We want to take this moment to remind
          you that anyone in need of legal help can afford our award-winning
          services thanks to our <strong> "No Win, No Fee"</strong> promise.
        </p>
        <p>
          Our promise to you is that we will win your case or you do not pay us
          at all. If we cannot win your case in court or deliver you a
          settlement offer that covers the cost of your recover, you do not have
          to worry about paying us at all. In addition, we front any legal fees
          pertaining to your case in the beginning so we can get the ball
          rolling and deliver you justice sooner than later.
        </p>
        <p>
          It is rare that our South Bay personal injury attorneys ever lose. Our{" "}
          <strong> 96% success rate</strong> has spoken volume about us in Los
          Angeles County and the South Bay for the past
          <strong> 39 years</strong>. The odds are in your favor that we will
          win your case in court.
        </p>
        <p>
          If you want justice to be served, all you have to do is pick up the
          phone and call us now.
        </p>
        <h2>Our Lawyers Will Fight for You</h2>
        <p>
          Call us now and find out why our past clients in South Bay picked
          Bisnar Chase personal injury attorneys to represent their claim. Our
          seasoned team of lawyers will be right by your side through the legal
          process so you can get back to the life that you lived before your
          accident. If you do not take our word for it, here what our former
          clients have to say about our services and our team:
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/OTnl7vXCODs?rel=0&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            We helped Penny quickly recover from her auto accident. If you call
            us now, we may be able to do the same for you.
          </blockquote>
        </center>
        <h3>
          Call 323-238-4683 now for your free case review. Our team is ready to
          fight for you.
        </h3>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
