// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/gears-in-your-head-brain-injury-los-angeles-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brain Injury Attorneys Los Angeles - Traumatic Head Injury Lawyers, CA"
        pageDescription="Traumatic brain injuries, or TBI's are often permanent if not fatal. The experienced Los Angeles Brain Injury Lawyers of Bisnar Chase have been winning head injury cases for over 40 years, establishing a 96% success rate. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Brain Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles brain injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Call <b>323-238-4683</b> to speak to an experienced{" "}
          <strong> Los Angeles brain injury lawyer</strong> for a{" "}
          <Link to="/los-angeles/contact">
            <strong> Free consultation</strong>
          </Link>{" "}
          if you or a loved one has experienced a traumatic brain injury.
          Traumatic head or brain injuries are referred to as "catastrophic
          injuries" because they are often permanent and life changing and have
          a tremendous impact on the lives of victims and their families.
        </p>
        <p>
          These injuries are also often caused by another person's negligence or
          fault. They may occur as the result of{" "}
          <Link to="/los-angeles/car-accidents">auto accidents</Link>, medical
          malpractice, nursing home abuse or neglect, slip-and-fall incidents,
          product defects,{" "}
          <Link to="/los-angeles/workplace-harassment">
            workplace accidents
          </Link>
          , acts of violence or even medical negligence.
        </p>
        <p>
          It is important victims of brain damage and their families have an
          experienced Los Angeles{" "}
          <Link to="/los-angeles">personal injury attorney </Link> on their side
          who has a deep understanding of the medical, physical, financial and
          psychological effects of a traumatic brain injury.
        </p>
        <h2>What Is A Traumatic Brain injury?</h2>
        <p>
          A traumatic brain injury is when an individual experiences a physical
          injury to the head that alters their state of consciousness, cognitive
          function and competency. Traumatic brain injuries can alter one's
          speech, sight, thought process, capability to understand and remember
          new and existing information and other debilitating issues.
        </p>
        <h2>How Can I Be Sure I Need An Attorney For My Brain Injury Case?</h2>
        <p>
          If you have experienced a traumatic brain injury as a result of
          someone else's negligence contact a brain injury attorney to find out
          your rights. An attorney with a lot of experience dealing with
          traumatic brain injuries or catastrophic injuries will be your best
          bet.
        </p>
        <p>
          <strong>
            {" "}
            Everything is confusing—from medical bills to the future—can an
            attorney help me get everything sorted out?
          </strong>
        </p>
        <p>
          Having an attorney handle your case means the firm will handle every
          aspect of your claim. A good personal injury attorney with an
          experienced team we'll make sure you focus on getting better while
          they do all the work.
        </p>
        <h2>
          What Experience Does Your Los Angeles Firm Have With Brain Injury
          Cases?&nbsp;
        </h2>
        <p>
          At the Personal Injury Law Firm of Bisnar Chase our team of highly
          skilled and experienced Brain Injury Attorneys have over 40 years of
          experience with a 96% success rate and have won over $500 Million for
          our clients. We specialize in catastrophic injuries.
        </p>

        <h2>Top 6 Most Common Traumatic Brain Injuries in Los Angeles</h2>
        <p>
          Head injuries happen every day from sports, work, recreational,
          accidental or other related accidents. While some occurrences can be
          very rare and unique, many can be quite common.
        </p>
        <p>The following list identifies the top most common brain injuries:</p>
        <ul>
          <li>
            <strong> Hematoma</strong>: A collection also known as clotting of
            blood outside the blood vessels. Hematoma's in the brain can be very
            dangerous and even cause you to pass out.
          </li>
          <li>
            <strong> Hemorrhage</strong>: &nbsp;Uncontrolled bleeding that can
            happen in the brain and have very serious or life threatening
            consequences. Sub-arachnoid bleeds can cause headaches and vomiting.
            The severity of depends on how much bleeding there is initially,
            however over time any amount of blood can cause pressure to build.
          </li>
          <li>
            <strong> Concussions</strong>: This type of brain injury happens
            when the brain bounces against the school. Typically concussions are
            not life threatening although sever cases happen. There is a greater
            risk when it's a second impact syndrome concussion before the first
            concussion has had a chance to heal.
          </li>
          <li>
            <strong> Edema</strong>: &nbsp;Is a pressure build up in the brain.
            Since your skull can't stretch, this continued swelling causes
            pressure and can lead to complications. Often after a serious head
            injury, doctor's may need to relieve the pressure to release the
            swelling.
          </li>
          <li>
            <strong> Skull Fracture</strong>: Bones in your body have bone
            marrow except the skull. If you fracture the skull chances are it
            can be a very serious injury in need of immediate medical care to
            protect the brain.
          </li>
          <li>
            <strong> Diffuse Axonal Injury</strong>: Though it isn't as
            outwardly visible as other forms of brain injury, diffuse axonal
            injury is one of the most dangerous types of head injuries and can
            lead to permanent brain damage, lead a person to be permanently
            disabled and even dead.
          </li>
        </ul>
        <p>
          {" "}
          <Link
            to="http://www.healthline.com/health/head-injury#types3"
            target="new"
          >
            <cite>(Information provided by Healthline.com)</cite>
          </Link>{" "}
        </p>
        <h2>The Impact of Brain Injuries on Athletes</h2>
        <p>
          From decades of football injuries to the now popular MMA sport,
          athletes have suffered serious and often catastrophic brain damage
          from dangerous sports. Concussions are a serious issue with most high
          school football teams and a growing number of students have been
          injured over the years. Higher standards are now in place with most
          athletic organizations to help prevent injuries, however with the
          nature of high impact sports, most are unavoidable.
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Second-impact_syndrome"
              target="_blank"
            >
              {" "}
              Second impact concussions{" "}
            </Link>
          </strong>{" "}
          can be extremely dangerous and happens when you receive consecutive
          blows to the head in a short period of time. An example would be a
          football player getting two minor concussions in one week that lead to
          a more serious and possibly catastrophic brain injury. If the brain
          swells or congestion in the vessels takes place it can be life
          threating.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/prUNbysh96I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Los Angeles Traumatic Brain Injury Statistics</h2>

        <p>
          First, it is important to understand just how common brain injuries
          are. According to the most recent data compiled by the{" "}
          <Link to="https://www.cdc.gov/" target="new">
            {" "}
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>{" "}
          :
        </p>
        <ul>
          <li>
            There are approximately 1.7 million traumatic brain injuries
            suffered in the United States each year
          </li>
          <li>
            Approximately 30.5 percent of all injury-related deaths involve a
            traumatic brain injury
          </li>
          <li>
            Almost half a million children aged 0 to 14 years visit the
            emergency room each year for treatment of a traumatic brain injury
            or injury effected to the brain tissue.
          </li>
          <li>
            Adults aged 75 years or older have the highest rates of brain
            injuries resulting in hospitalization or death
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles brain injury attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/traumatic-brain-injuries-symptoms-los-angeles.jpg')"
            }}
          >
            <h2>Symptoms of a Traumatic Brain Injury</h2>
          </div>
        </LazyLoad>
        <p>
          Brain injuries can be a very scary experience. Sometimes you won't
          realize you have suffered a traumatic brain injury (tbi) , and other
          times your symptoms may be severe and debilitating.
        </p>
        <p>
          <strong>
            {" "}
            Here are some possible signs you may encounter after experiencing a
            blow to the head or any other type of head injury:
          </strong>
        </p>
        <ul>
          <li>Loss of consciousness</li>
          <li>Persistent headache that worsens</li>
          <li>Repeated vomiting or nausea</li>
          <li>Convulsions or seizures</li>
          <li>Dilation of both or one pupil(s) of eye(s)</li>
          <li>Clear fluids draining from the nose and or ears</li>
          <li>Inability to awaken from sleep</li>
          <li>Weakness or numbness in fingers or toes</li>
          <li>Loss of coordination</li>
        </ul>
        <p>
          <strong> Other serious symptoms may include:</strong>
        </p>
        <ul>
          <li>Profound confusion</li>
          <li>Agitation, combativeness or unusual behavior</li>
          <li>Slurred speech</li>
          <li>Coma and other disorders of consciousness</li>
        </ul>
        <h2>How Negligence Can Result In A Brain Injury</h2>
        <p>
          There are many different ways in which the negligence of an individual
          or an entity can result in catastrophic injuries such as a brain
          injury. A skilled catastrophic injury lawyer will be able to identify
          the aspects of your case and provide an honest recommendation on how
          we should move forward appropriately.
        </p>
        <p>
          <strong>
            {" "}
            Examples of negligent behavior that can result in a traumatic brain
            injury include:
          </strong>
        </p>
        <ul type="disc">
          <li>
            <strong> Careless or reckless driving:</strong> The CDC reports that
            approximately
            <strong>
              {" "}
              17.3 percent of traumatic brain injuries (tbi) occur as the result
              of motor vehicle accidents
            </strong>
            . Many head and brain injuries occur in Los Angeles car accidents
            each year. As is true for all types of injury accidents, the
            negligent party can be held accountable for the victim's losses.
            There are several important questions that must be asked in these
            cases. Was the at-fault driver exceeding the speed limit? Was the
            negligent driver drinking, fatigued, distracted or otherwise
            reckless? Did the driver run a red light or make a turn without
            properly yielding the right of way?
          </li>
        </ul>
        <ul type="disc">
          <li>
            <strong> Negligent property management:</strong> Some brain injuries
            occur as the result of{" "}
            <Link to="/los-angeles/slip-and-fall-accidents">
              slip-and-fall accidents
            </Link>
            . In fact, the CDC reports that 35.2 percent of all TBIs occur as
            the result of falls. When a slip-and-fall accident results in a
            brain injury, there are a few questions that must be asked. Did a
            hazardous condition on the property cause the slip-and-fall
            accident? Was the property owner or manager aware of the condition
            and failed to fix it? In such cases, a premises liability claim can
            hold the at-fault property owner accountable for the injuries that
            occurred in the fall.
          </li>
        </ul>
        <ul type="disc">
          <li>
            <strong> Defective products:</strong> The manufacturer of a{" "}
            <Link to="/los-angeles/product-liability">defective product </Link>{" "}
            can be held liable for the injuries suffered by the consumer. If a
            car crashes because of a defective floor mat or tire, for example,
            the manufacturer of the car can be included in the lawsuit.
          </li>
        </ul>
        <ul type="disc">
          <li>
            <strong> Acts of violence:</strong> The CDC also reports that 10
            percent of brain injuries are caused by acts of violence. While an
            act of violence can result in criminal charges, it will not
            automatically result in financial compensation for the victim. In
            order to hold the wrongdoer financially responsible for the injuries
            suffered in an assault, the victim's family will have to file an
            injury claim in civil court.
          </li>
        </ul>
        <h2>Seeking Compensation For A Los Angeles Brain Injury</h2>
        <p>
          Brain injuries can be extremely costly to treat. Victims end up
          spending a long time in hospitals and rehabilitation facilities. Many
          victims also need nursing care. The cost in such cases can add up to
          millions of dollars over a person's lifetime. Other losses such as the
          victim's inability to work or provide for his or her family must also
          be taken into consideration while filing an injury claim.
        </p>
        <p>
          Southern California brain injury victims can seek compensation for
          damages including but not limited to medical expenses, rehabilitation,
          lost wages, lost future income, loss of life's enjoyment, pain and
          suffering and emotional distress.
        </p>
        <h2>Feel Comfortable With Your Los Angeles TBI Attorney</h2>
        <p>
          Experiencing a traumatic head or brain injury (tbi) can be very, well
          traumatic. In more severe cases where permanent disabilities have
          occurred, individuals have had a tendency to feel self-conscious and
          uncomfortable in public, around friends, families, strangers and even
          their own therapists, doctors and legal representation or lawyers.
        </p>
        <p>
          At the law offices of Bisnar Chase you become a part of our family.
          Our goal is <strong> to</strong>
          <strong>
            {" "}
            provide superior client representation in a compassionate and
            professional manner while making our world a safer place.{" "}
          </strong>
        </p>
        <p>
          We want you to feel confident that choosing our team of experienced
          Traumatic Brain Injury Lawyers was the right decision.
        </p>
        <p>
          Having the ability to speak with your attorney on a personal level
          about your thoughts, feelings, emotions and what you are going through
          is what makes a difference.
        </p>
        <h2>Superior Legal Representation in Los Angeles</h2>
        <p>
          The team of{" "}
          <strong> Los Angeles Traumatic Brain Injury Attorneys</strong> have
          established a <strong> 96% success rate</strong> with over{" "}
          <strong> $500 Million won</strong> for our clients by expert attorneys
          with <strong> over 40 years of experience</strong>. Becoming a part of
          our family, and allowing the Personal Injury Law firm of Bisnar Chase
          to be a part of yours is a winning combination.
        </p>
        <p>
          Our Southern California law firm has helped catastrophically injured
          victims and their families obtain fair compensation for their losses
          and hold the at-fault parties accountable for their negligence.
        </p>
        <p>
          For immediate assistance call <b>323-238-4683</b> and receive a{" "}
          <strong> Free Case Evaluation. </strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
