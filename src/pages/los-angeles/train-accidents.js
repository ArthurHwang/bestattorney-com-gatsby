// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/los-angeles-train-accident-metrolink-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Train Accident Metrolink Attorneys - Bisnar Chase"
        pageDescription="Train accidents can be extremely dangerous, resulting in major injuries and deaths. Victims should contact the Los Angeles train accident Metrolink lawyers of Bisnar Chase for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Train Accident Metrolink Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Los Angeles Train Accident Metrolink Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you or a loved one has suffered an injury due to a train accident,
          please contact the experienced Los Angeles train accident lawyers of
          Bisnar Chase.
        </p>

        <p>
          Trains are extremely large and powerful machines which can pose
          serious dangers to both passengers and pedestrians. Train accidents
          are traumatic, and can result in major injury or wrongful death.
        </p>

        <p>
          Bisnar Chase <Link to="/los-angeles">Personal Injury Attorneys </Link>{" "}
          have more than 40 years of experience when it comes to taking on the
          rail industry. We make it our mission to pursue justice for those hurt
          in train accidents.
        </p>

        <p>
          The railroad industry won't look out for your best interests when
          faced with an accident injury lawsuit. You will need an{" "}
          <strong> LA train accident attorney</strong> on your side who can
          protect your rights and fight for compensation and justice. A top{" "}
          <Link to="/misc/train-accidents" target="new">
            train accident lawyer
          </Link>{" "}
          from Bisnar Chase can help.
        </p>

        <p>
          Our skilled and passionate attorneys can help victims seek{" "}
          <strong> train accident injury compensation</strong>. Call{" "}
          <Link to="tel:+1-323-238-4683">(323) 238-4683 </Link> now to discuss
          your case.
        </p>

        <h2>What Should You Do After a Train Accident?</h2>

        <p>
          Once it is safe to do so, you should seek medical help and record your
          train crash experience. To help your LA train accident attorney, you
          should:
        </p>

        <ul>
          <li>
            <b>Get medical attention.</b>
            Obtain copies of hospital records.
          </li>
          <li>
            <b>Contact the police.</b>
            Later, request a copy of the incident report.
          </li>
          <li>
            <b>Document your experience.</b>
            Take pictures of any injuries you have suffered.
          </li>
        </ul>

        <p>
          It is important that you contact one of our Los Angeles train accident
          attorneys right away if you or a loved one has been hurt on a
          Metrolink, Amtrak, or any other type of train in or around Los
          Angeles.
        </p>

        <p>
          The expert staff at our law firm can provide a free consultation,
          assessing the details of your case and offering the best possible
          legal advice.
        </p>

        <p>
          We offer a 'No Win, No Fee' guarantee. This means that you won't pay
          unless we win your case.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/train-accident-lawyers-in-los-angeles.jpg"
            width="100%"
            alt="train accident lawyers in Los Angeles"
          />
        </LazyLoad>

        <h2>The Most Common Rail Accident Types and Causes</h2>

        <p>
          There are plenty of potential causes of train accidents which can lead
          to lawsuits. Some of the key causes of train accidents and injuries
          include:
        </p>

        <ul>
          <li>Mechanical failures on trains, tracks, and at stations.</li>
          <li>Mistakes and negligence by conductors and drivers.</li>
          <li>A lack of proper inspections or maintenance.</li>
          <li>Objects or debris on the tracks.</li>
          <li>Traveling at unsafe speeds.</li>
          <li>Faulty seats, luggage compartments, and other equipment.</li>
        </ul>

        <p>
          These failures can create dangerous situations. They might include
          sudden stops and harsh braking, high-speed collisions, dangerous
          debris inside the train, and more.
        </p>

        <h2>Causes of Train Crashes in Los Angeles</h2>

        <p>
          The most serious incidents seen by train accident lawyers in Los
          Angeles are crashes, collisions, and derailments. These can be due to
          driver or engineer negligence, cargo explosions, faulty parts, or a
          lack of maintenance.
        </p>

        <p>
          Some of the key causes of train accidents according to the{" "}
          <Link
            to="http://safetydata.fra.dot.gov/OfficeofSafety/publicsite/Query/inctally1.aspx"
            target="new"
          >
            {" "}
            Federal Railroad Administration
          </Link>{" "}
          are defective train parts.
        </p>
        <LazyLoad>
          <img
            src="/images/train-accidents/los-angeles-train-accident-attorney.jpg"
            width="75%"
            className="imgcenter-fluid"
            alt="Los Angeles train accident attorney"
          />
        </LazyLoad>

        <p>
          There is also a growing trend of trains colliding with cars. Since the
          Metrolink commuter system travels throughout Los Angeles and other
          cities, there are more opportunities for disaster.
        </p>

        <p>
          A Metrolink train crash involving a car can occur on unprotected rail
          crossings, or when the crossing warning lights and gates fail to work.
        </p>

        <p>
          Deaths can also occur when pedestrians walk too close to these
          Metrolink commuter rails. In some cases, deaths have been caused by
          pedestrians standing on the tracks where it is prohibited.
        </p>

        <h2>Types of Train Accident Injuries</h2>

        <p>
          A train collision will often cause serious and even{" "}
          <Link to="/catastrophic-injury" target="new">
            catastrophic injuries
          </Link>
          . Trains are a great means of public transport, but they are large and
          heavy, travel at high speeds, and have a large stopping distance.
        </p>

        <p>
          The most common types of injuries suffered in train accidents include:
        </p>

        <ul>
          <li>Cuts and scrapes</li>
          <li>Broken bones</li>
          <li>Whiplash</li>
          <li>Neck and spinal cord injuries</li>
          <li>Dislocated limbs and torn ligaments</li>
          <li>Serious head injuries</li>
          <li>Emotional trauma</li>
        </ul>

        <p>
          Train crashes involving cars, pedestrians, and other trains are
          usually the most severe. They can cause death, paralysis, amputation
          and more. Fortunately, train crashes are not very common. But when
          they do occur, they impact the lives of many, based on the number of
          passengers onboard one of these speeding bullets.
        </p>

        <p>
          One example of a train crash in Spain, in 2013, killed 80 people and
          injured scores of others. The train operator later admitted that he
          was going over 120 mph around a dangerous curve at the time. Countless
          lives were impacted by his actions.
        </p>

        <p>
          A similar crash in greater Los Angeles, throughout the city, or into
          Orange County, would have a similarly devastating effect.
        </p>

        <h2>California Train Accident Statistics</h2>

        <p>
          The urban population in the United States is quickly expanding, and
          train travel to and from work has become a necessary source of
          transportation for millions of Americans.
        </p>

        <p>
          California has witnessed a substantial increase in train travel due to
          the Metrolink expansion over the last ten years. Unfortunately, as the
          number of trains grows to meet demand, so does the number of train and
          Metrolink accidents - and the need for{" "}
          <strong> train accident attorneys in Los Angeles</strong>.
        </p>

        <ul>
          <li>
            In 2018, there were 11,476 rail-related accidents and incidents in
            the United States.
          </li>
          <li>
            There were 7,999 injuries recorded as a result of train and rail
            incidents in 2018. That number was even higher in 2015, topping
            9,000.
          </li>
          <li>
            There were 878 rail fatalities in 2018 - the highest annual total
            over the last five years.
          </li>
          <li>
            The last major train collision in Los Angeles was a Chatsworth
            Metrolink crash in 2008, which killed 26 and injured 135.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Who is Liable for a CA Train Accident Injury?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/los-angeles-train-crash-lawyers.jpg')"
            }}
          >
            <h2>Who is Liable for a CA Train Accident Injury?</h2>
          </div>
        </LazyLoad>

        <p>
          Train operations are referred to as common carriers. The California
          Metrolink is contracted with operating firm Amtrak and covers more
          than 500 miles of track. Our Metrolink trains can travel up to 90 mph,
          covering 55 stations across California and carrying about 40,000
          people every weekday.
        </p>

        <p>
          These public transport systems are responsible for the safety of their
          passengers. It is the train operator which is ultimately responsible
          for ensuring the highest degree of care is taken. Operators such as
          Amtrak can be found liable if their negligence leads to a railroad
          accident.
        </p>

        <h2>Feel Comfortable with Your Train Accident Attorney</h2>

        <p>
          Train accidents are traumatic and can result in serious and fatal
          injuries. When you hire a Los Angeles train accident attorney, it is
          important that you are comfortable with them, and know that they are
          doing everything possible to help.
        </p>

        <p>
          Having an honest and trusting relationship with your attorney is
          important because it can help victims open up and discuss crucial
          aspects of their experience.
        </p>

        <p>
          Having faith in your attorney is important because you will feel more
          confident throughout your case, resulting in better sleep, less
          anxiety, a more positive and optimistic attitude and approach to your
          case.
        </p>

        <p>
          The train accident lawyers of Bisnar Chase are proud to provide
          superior client representation. We believe in maintaining a close
          attorney-client relationship to help train accident victims through
          the legal process.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Our Los Angeles Train Accident Attorneys Will Fight for You"
            style={{
              backgroundImage:
                "url('/images/train-accidents/best-train-accident-attorneys-in-LA.jpg')"
            }}
          >
            <h2>Our Los Angeles Train Accident Attorneys Will Fight for You</h2>
          </div>
        </LazyLoad>

        <p>
          Bisnar Chase will leave no stone unturned while investigating every
          aspect of your claim. We are proud to support the Los Angeles
          community, and make it our mission to help train accident victims get
          the compensation they deserve for their injuries or losses.
        </p>

        <p>
          Our skilled team of Los Angeles train accident attorneys has more than{" "}
          <b>40 years</b> of experience. The firm has established a{" "}
          <strong> 96% success rate</strong> and won more than{" "}
          <strong> $500 million</strong> for clients.
        </p>

        <p>
          If you have been injured in a train,{" "}
          <Link to="http://www.metrolinktrains.com/" target="new">
            Metrolink
          </Link>{" "}
          or{" "}
          <Link to="https://www.amtrak.com" target="new">
            Amtrak
          </Link>{" "}
          accident, contact one of our Los Angeles train accident Metrolink
          attorneys for a free case evaluation at{" "}
          <Link to="tel:+1-323-238-4683">(323) 238-4683</Link>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
