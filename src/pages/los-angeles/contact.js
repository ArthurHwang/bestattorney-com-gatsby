// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { FaAccessibleIcon, FaExclamationCircle } from "react-icons/fa"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { Contact } from "../../components/elements/forms/Form_Contact"
import { Reviews } from "../../components/elements/home/Home_Reviews"
import { SEO } from "src/components/elements/SEO"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ContactPageLA({ location }) {
  return (
    <LayoutDefault {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Contact Bisnar Chase - Los Angeles Location"
        pageDescription="Call the Los Angeles Injury Lawyers for your Free consultation at 323-238-4683. Contact our Los Angeles office location at 6701 Center Drive West, 14th FL Los Angeles CA."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bisnar Chase | Los Angeles</h1>
        <BreadCrumbs location={location} />
        <div className="address-wrapper">
          <h2>BISNAR | CHASE PERSONAL INJURY ATTORNEYS</h2>
          <p>6701 Center Drive West, 14th Fl. LA, CA 90045</p>
          <p>
            <span className="orange">LOCAL:</span> (323) 238-4683{" "}
            <span className="orange">TOLL FREE:</span> (800) 561-4887
          </p>
        </div>
        <p>
          If you need a Los Angeles Personal Injury Lawyer to help you get
          proper compensation, contact Bisnar Chase today! Our LA office is
          available to call from 8:30 a.m. to 8:30 p.m., seven days a week
          (excluding major holidays). If you fill out our contact form, you will
          receive a telephone call between 8:30 a.m. and 4:30 p.m. on business
          days.{" "}
        </p>
        <div className="grid-wrapper">
          <Contact />
          <div className="iframe-holder">
            <iframe
              id="cn-map"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              width="100%"
              height="100%"
              frameBorder="0"
            />
          </div>
        </div>
        <div className="text-wrapper">
          <div className="warning">
            <FaExclamationCircle className="warning-icon" />
            <h3 className="handicap-warning">
              No attorney client relationship is established by submitting this
              initial contact information to our office.The forms are intended
              to assist Bisnar Chase with a preliminary evaluation of your case.
              You may reach our office Monday- Friday 8:30 am to 5:00pm.
            </h3>
          </div>
          <div className="handicap">
            <FaAccessibleIcon className="handicap-icon" />
            <div className="handicap-text-wrap">
              <h3>Unable to Come In? We'll Come To You!</h3>
              <p>
                In the event that your injury makes traveling difficult, we can
                arrange to visit you in your home or hospital.
              </p>
            </div>
          </div>
        </div>
        <Reviews invert={true} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutDefault>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .iframe-holder {
    background-color: #eee;

    iframe {
      @media (max-width: 700px) {
        height: 100%;
      }
    }
  }

  .address-wrapper {
    text-align: center;

    span {
      color: ${({ theme }) => theme.colors.accent};
      font-weight: 600;
    }

    h2 {
      border-bottom: none;
      font-size: 2rem;
      margin-bottom: 0.3rem;

      @media (max-width: 500px) {
        font-size: 1.2rem;
      }
    }
    p {
      text-align: center;
      font-weight: 600;

      @media (max-width: 500px) {
        font-size: 1rem;
      }
      &:nth-of-type(1) {
        margin-bottom: 0;
      }
    }
  }
  .grid-wrapper {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;

    @media (max-width: 1024px) {
      grid-template-columns: 1fr;
      grid-template-rows: 1fr 1fr;
    }
  }

  div.text-wrapper {
    border: thick double ${({ theme }) => theme.colors.grey};
    margin-top: 2rem;
    display: grid;
    grid-template-columns: 1fr 1fr;
    padding: 2rem;

    @media (max-width: 1024px) {
      grid-template-columns: 1fr;
      grid-template-rows: auto auto;
      align-items: center;
    }

    @media (max-width: 550px) {
      padding: 0;
    }

    .warning {
      display: grid;
      grid-template-columns: 1fr 8fr;
      align-items: center;
      padding: 2rem 0;

      @media (max-width: 550px) {
        grid-template-columns: 1fr;
        grid-template-rows: auto auto;
        justify-items: center;
        padding: 2rem 0 !important;
      }

      @media (max-width: 1024px) {
        padding: 0 2rem;
      }

      .warning-icon {
        font-size: 10rem;
        color: red;

        @media (max-width: 550px) {
          font-size: 8rem;
        }
      }
    }

    .handicap-warning {
      padding: 2rem;
      text-align: justify;
    }

    .handicap {
      display: grid;
      grid-template-columns: 1fr 8fr;
      align-items: center;
      padding: 2rem 0;

      @media (max-width: 550px) {
        grid-template-columns: 1fr;
        grid-template-rows: auto auto;
        justify-items: center;
        padding: 2rem 0 !important;
      }

      @media (max-width: 1024px) {
        padding: 0 2rem;
      }

      .handicap-text-wrap {
        padding: 2rem;
      }

      .handicap-icon {
        font-size: 10rem;
        color: #004c97;

        @media (max-width: 550px) {
          font-size: 8rem;
        }
      }
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
