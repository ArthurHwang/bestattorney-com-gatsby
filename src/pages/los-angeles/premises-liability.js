// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/los-angeles-premises-liability-attorney.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Premises Liability Lawyer - Bisnar Chase"
        pageDescription="The Los Angeles premises liability attorneys of Bisnar Chase specialize in holding negligent property owners accountable when dangerous conditions lead to injuries. Contact us for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Premises Liability Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles premises liability attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          A Los Angeles premises liability attorney can help when a victim has
          been injured due to hazardous or dangerous conditions on someone
          else’s property.
        </p>

        <p>
          While{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Premises_liability"
            target="new"
          >
            premises liability
          </Link>{" "}
          is a broad term, it basically refers to any accident involving
          negligence which happens in a location that is owned or run by someone
          else. This can include public and private properties such as parks,
          beaches, apartment complexes, theaters, shops, malls and amusement
          parks.
        </p>

        <p>
          Premise liability cases can involve a wide range of causes, including
          fires, swimming pool accidents, slip-and-fall injuries, dog bite
          attacks, sub-par security, and much more. When the negligence of a
          property owner/manager contributes to the accident or injury, a good
          <strong> premises liability lawyer in Los Angeles</strong> can help
          victims seek compensation for the injuries, damages and losses they
          have suffered.
        </p>

        <p>
          For immediate assistance, please call today on{" "}
          <Link to="tel:+1-323-238-4683">(323) 238-4683 </Link> for help from a
          top-rated Los Angeles premises liability attorney.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Premises Liability Law in Los Angeles"
            style={{
              backgroundImage:
                "url('/images/text-header-images/types-of-premise-liability-claims-los-angeles.jpg')"
            }}
          >
            <h2>
              What Are the Laws Concerning Premises Liability in Los Angeles?
            </h2>
          </div>
        </LazyLoad>

        <p>
          Business and property owners are required to protect visitors to their
          locations, and this includes upkeep on the property and regularly
          checking for safety hazards.
        </p>

        <p>
          If an injury takes place at a location such as a mall, school or other
          business in Los Angeles – and the property owner is found to be
          negligent – then the victim will have a valid claim against the
          property or business owner.
        </p>

        <p>
          A Los Angeles premises liability lawyer can help a victim navigate the
          legal system and fight for justice.
        </p>

        <h2>Types of Premises Liability Claims in Los Angeles</h2>

        <p>
          There are several types of premises liability claims that are filed in
          Los Angeles. Here are just a few examples that we commonly encounter:
        </p>

        <ul>
          <li>
            <strong> Slip-and-fall accidents:</strong> These are accidents that
            occur regularly, on a daily basis in all different locations.
            However, some{" "}
            <Link to="/los-angeles/slip-and-fall-accidents" target="new">
              slip-and-fall accidents
            </Link>
            , as well as tripping accidents, are more serious than others.
            Slip-and-fall accidents can result in severe and debilitating
            injuries, especially for the elderly.
            <p>
              A common example of a slip-and-fall accident is when an individual
              slips on a grocery store floor. This is often due to a spillage
              not being cleared in a timely manner, with no wet floor sign
              deployed.
            </p>
          </li>

          <li>
            <strong> Fires and explosions:</strong> It is a key responsibility
            of property owners to maintain their premises and ensure that vital
            safety features are in place. For example, an apartment complex in
            Los Angeles is required to be compliant with earthquake code and
            must have fire alarms and sprinklers in good working order. Failure
            to install mandatory features can be a basis for a premises
            liability case.
          </li>
          <LazyLoad>
            <img
              src="/images/premises-liability/swimming-pool-accident-lawyer-LA.jpg"
              width="50%"
              className="imgright-fluid"
              alt="swimming pool accident lawyer in LA"
            />
          </LazyLoad>
          <li>
            <strong> Swimming pool accidents:</strong> With its year-round good
            weather, pools are popular in and around Los Angeles. Unfortunately,{" "}
            <Link to="/los-angeles/swimming-pool-accidents" target="new">
              swimming pool accidents{" "}
            </Link>
            are also very common. Drowning and near-drowning incidents are
            particularly dangerous. Slip injuries are also common at pools. The
            property or pool owner may be liable in such cases.
          </li>

          <li>
            <strong> Government Property Accidents:</strong> Incidents can
            happen at public places, such as city-run parks and government
            buildings. In such cases, a claim can be filed against the
            government entity in charge of maintaining and ensuring the safety
            of the property.
            <p>
              Under California Government Code 911.2, any personal injury claim
              against a governmental entity must be properly filed within 180
              days of the incident.
            </p>
          </li>

          <li>
            <strong> Dog attacks:</strong> Animal attacks, when they occur on
            someone's property, can become the basis for premises liability
            claims. For example, if a landlord knows of a dangerous dog on their
            property and does nothing about it, they could be held liable for a{" "}
            <Link to="/los-angeles/dog-bites" target="new">
              dog bite attack
            </Link>
            .
          </li>

          <li>
            <strong> Amusement park accidents:</strong> The Greater Los Angeles
            area is home to world-famous amusement parks including{" "}
            <Link to="https://www.sixflags.com/magicmountain" target="new">
              {" "}
              Six Flags{" "}
            </Link>{" "}
            and{" "}
            <Link to="https://www.universalstudioshollywood.com/" target="new">
              {" "}
              Universal Studios
            </Link>
            , while{" "}
            <Link to="https://disneyland.disney.go.com/" target="new">
              {" "}
              Disneyland{" "}
            </Link>{" "}
            is nearby in Orange County.
            <p>
              When an{" "}
              <Link to="/los-angeles/amusement-park-accidents" target="new">
                amusement park accident{" "}
              </Link>{" "}
              is caused by poor maintenance or operator negligence, there may be
              a premises liability claim.
            </p>
            <p>
              <u>Amusement park premise liability claims can include:</u>
            </p>
            <LazyLoad>
              <img
                src="/images/premises-liability/los-angeles-amusement-park-accident-attorney.jpg"
                width="50%"
                className="imgright-fluid"
                alt="los angeles amusement park accident attorney"
              />
            </LazyLoad>
            <ul style={{ marginLeft: "0" }}>
              <li>
                Wet floor slips. Bathroom floors, aquarium floors, and areas
                which have been rinsed off can all be dangerous.
              </li>
              <li>
                Falls from raised platforms with no handrails or loose bases.
              </li>
              <li>
                Whiplash from unexpected sudden jarring movements, braking or
                acceleration on rides.
              </li>
              <li>
                Staff misconduct on park property. This can consist of bad food
                safety practices, reckless endangerment with transportation
                vehicles, mechanical devices, poor maintenance, and other
                potentially dangerous activities to park goers.
              </li>
            </ul>
            <p>
              Amusement Parks are run by large firms. They have strong legal
              teams and attorneys standing by at all times, prepared for any
              claim and any situation. It is important to hire the best Los
              Angeles premises liability lawyers possible to fight large
              corporations. Bisnar Chase has the experience and resources to
              handle the biggest cases.
            </p>
          </li>
          <li>
            <strong> Other types of premise liability case:</strong> Other
            premise liability causes can include sub-par property maintenance,{" "}
            <Link
              to="/premises-liability/escalator-elevator-injury-lawyers"
              target="new"
            >
              elevator and escalator accidents
            </Link>
            , leaks and flooding, toxic fumes, chemicals, mold and more.
          </li>
        </ul>

        <h2>
          How Can Your Law Firm Help Me If I was Injured on Someone's Premises?
        </h2>
        <p>
          Here are just a few premise liability case examples that we see
          regularly:
        </p>
        <LazyLoad>
          <img
            src="/images/premises-liability/slip-and-fall-accident-attorney.jpg"
            className="imgleft-fixed"
            width="34%"
            alt="slip and fall accident attorney"
          />
        </LazyLoad>
        <p>
          <strong>
            {" "}
            <u>Store slip-and-fall injury:</u>
          </strong>
        </p>

        <p>
          The law firm would investigate if negligence was involved to pursue a
          claim. When a person is hurt in a fall, it does not automatically mean
          they can sue. A good premises liability attorney in Los Angeles needs
          to prove that negligence was involved.
        </p>

        <p>
          <strong>
            {" "}
            <u>Workplace fall injury:</u>
          </strong>
        </p>

        <p>
          This may be considered a worker’s comp claim if it is an on-the-job
          injury. Bisnar Chase does not take on workers comp cases unless it is
          a third-party injury. For example, if builders left power cords in the
          way at your place of work, causing a trip accident, we could file a
          claim against the builders.
        </p>

        <p>
          <strong>
            {" "}
            <u>Handrail accident:</u>
          </strong>
        </p>

        <p>
          We have seen many incidents involving apartment handrails giving way.
          A building owner is in charge of making sure rails are safe. If they
          have been negligent, then you have a case.
        </p>

        <p>
          There are countless other examples of potential premises liability
          cases. Contact us to find out if we can help.
        </p>

        <h2>
          What is the Benefit of Consulting with a Los Angeles Slip and Fall
          Attorney?
        </h2>
        <p>
          Los Angeles slip-and-fall attorneys specialize in these cases. By
          opting for one of our slip-and-fall specialists, a victim is sure to
          be counseled by someone who has years of experience in premise
          liability law.
        </p>

        <h2>Why is Proving Negligence Important?</h2>

        <p>
          Not all injuries will result in successful claims. In some cases,
          accidents happen, and there is no one to blame. For example, if you
          slip on a spilled soda at the mall, but the spillage happened moments
          before the slip, there is no way it could have been dealt with. In
          this case, there is no premise liability.
        </p>

        <p>
          But if the soda was on the floor, had been reported, but was not dealt
          with, then there may be a case for negligence. It is vital that
          negligence can be proved by your Los Angeles premises liability lawyer
          for your compensation claim to be successful.
        </p>

        <h2>What Should I do After a Premises Accident in Los Angeles?</h2>

        <ol>
          <li>
            <span>
              <b>Immediately seek medical attention.</b> This will ensure your
              safety, as well as providing proof of your injuries.
            </span>
          </li>

          <li>
            <span>
              <b>Document everything.</b> Write down exactly what happened, and
              get contact details for anyone who saw the incident. You could
              also take pictures of the location, your injuries, and anything
              else which could be relevant – such as your shoes or clothing.
            </span>
          </li>

          <li>
            <span>
              <b>Report the accident.</b> Inform the relevant people of your
              accident, such as a mall manager. But do not give a formal
              statement if possible, or accept any blame. Always speak to a
              lawyer first.
            </span>
          </li>

          <li>
            <span>
              <b>Contact a lawyer.</b> Get a free consultation with an
              experienced premise liability attorney at Bisnar Chase.
            </span>
          </li>
        </ol>

        <p>
          We can help explore your rights and find out if there is a case to
          open a claim. It is important to get the ball rolling since there are
          time limits on personal injury cases.
        </p>

        <h2>Successful Premises Liability Case Study</h2>

        <p>
          Bisnar Chase has provided top-class representation to countless
          clients in premises liability cases.
        </p>

        <p>
          One of the first cases that firm partner Brian Chase went to trial on
          was a slip-and-fall case against retail giant Walmart. Our client was
          Christmas shopping with her kids at a Walmart store when she slipped
          in semi-dried vomit on the floor. She suffered a serious back injury
          in the fall.
        </p>

        <p>
          The Walmart attorneys were relentless, but Bisnar Chase provided
          expert representation and won a large verdict for the client. The
          chain lodged an appeal, but our firm prevailed again – securing extra
          costs and interest.
        </p>

        <p>
          One of the most eye-opening aspects of premises liability cases is the
          wide variety of events which fall under this label. It is much more
          than just slip-and-fall accidents. The Los Angeles premises liability
          attorneys of Bisnar Chase have extensive experience in all kinds of
          cases and can offer the help you need.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Damages Sought in Premises Liability Claims"
            style={{
              backgroundImage:
                "url('/images/text-header-images/injured-premise-liability.jpg')"
            }}
          >
            <h2>Damages Sought in Premises Liability Claims</h2>
          </div>
        </LazyLoad>
        <p>
          Injured victims need to protect their rights. They should seek prompt
          medical attention, file a report with the right authorities, and
          document evidence including doctor's visits, medical expenses and days
          missed at work. These could all affect the amount of compensation they
          receive.
        </p>
        <p>Injured victims can seek damages including:</p>
        <ul>
          <li>Medical expenses</li>
          <li>Lost wages</li>
          <li>Hospitalization and rehabilitation</li>
          <li>Permanent injuries</li>
          <li>Lost future income</li>
          <li>Disabilities</li>
          <li>Pain and suffering</li>
          <li>Emotional distress</li>
        </ul>

        <h2>Negotiating with Insurance Companies</h2>

        <p>
          Many defendants in premises liability cases have insurance companies
          and legal defense teams that will do their best to make the claims go
          away.
        </p>

        <p>
          It is important to understand that insurance companies do not have
          your best interests in mind. It is the job of insurance agents to save
          money for their firm, not to ensure the best outcome for you. For
          example, an insurance firm may try to gain access to your medical
          records, Often, they will do so to try to prove that victims have
          prior conditions which may be responsible for their injuries. A{" "}
          <strong> Los Angeles premises liability attorney</strong> may be able
          to help you take on the insurance company and win.
        </p>

        <p>
          It would be in your best interests as the plaintiff to refrain
          speaking with insurance firms or defense lawyers without first talking
          to your own lawyer. Make sure you have someone in your corner who will
          fight for your rights.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Contacting an Experienced Los Angeles Premises Liability Attorney"
            style={{
              backgroundImage:
                "url('/images/premises-liability/bisnar-chase-premises-liability-lawyers.jpg')"
            }}
          >
            <h2>
              Contacting an Experienced Los Angeles Premises Liability Attorney
            </h2>
          </div>
        </LazyLoad>

        <p>
          If you or a loved one has been injured, please contact an experienced
          Los Angeles premises liability attorney at Bisnar Chase today. Our
          team of personal injury lawyers has an outstanding record when it
          comes to premise liability cases, and we are dedicated to helping
          victims.
        </p>

        <p>
          Bisnar Chase has been handling personal injury cases for more than 40
          years, building up a <b>96% success rate</b> and winning more than{" "}
          <b>$500 million</b> for our clients.
        </p>

        <p>
          Set up a free, no-obligation consultation to find out if we can help
          you!
        </p>

        <p>
          For immediate help, call{" "}
          <Link to="tel:+1-323-238-4683">(323) 238-4683 </Link>, or{" "}
          <Link to="/los-angeles/contact" target="new">
            CLICK HERE
          </Link>{" "}
          to contact us.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
