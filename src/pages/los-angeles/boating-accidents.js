// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/boating-boat-accidents-los-angeles-attorneys-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Boating Accident Attorney | Boat Accident Lawyer"
        pageDescription="The Los Angeles Boating Accident Attorneys understand that serious injuries and wrongful death are a reality of boat and vessel crashes. Our skilled team of lawyers have won over $500 Million and understand what it takes to win your case, or you do not pay. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Boating Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles boating accident lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Boating is as popular as it is dangerous. Whether you're cruising up
          the Colorado river on your family party boat, racing past the Los
          Angeles harbor, or wake-boarding behind your buddy's new jet boat in
          Lake Havasu, it's all fun and games until someone gets hurt.
        </p>
        <p>
          Los Angeles is conveniently located just to the east of the Pacific
          Ocean and just west of the Colorado River. Many boaters live in Los
          Angeles and commute to nearby docks in Marina del Rey, Long Beach and
          San Pedro. Whether it is on the Pacific Ocean, the Colorado River or
          one of California's many beautiful lakes, boating is an exhilarating
          way to beat the heat and enjoy fun and togetherness on the water.
          However, it is important to remember that boating can get extremely
          dangerous very quickly. An accident on the water can prove
          catastrophic or even fatal.
        </p>
        <p>
          The experienced Los Angeles boat accident lawyers at BISNAR CHASE have
          a long and successful track record of representing seriously injured
          victims and their families. We have a 96 percent success rate with our
          cases. Our team of attorneys, paralegals and support staff are
          committed to providing the highest level of customer service to our
          clients and their families.
        </p>
        <p>
          If you or a loved one has been injured in a Los Angeles boating or
          watercraft accident, please contact us at 323-238-4683 for a free and
          comprehensive consultation with our experienced<strong>&nbsp;</strong>{" "}
          <Link to="/los-angeles">
            <strong> Los Angeles Personal injury attorneys</strong>
          </Link>
          <strong> .</strong>
        </p>
        <h2>Legal Options for Injured Boaters in Los Angeles</h2>
        <p>
          After seeking out medical attention, you may begin to wonder how you
          will pay for the many damages you have suffered. Depending on the
          circumstances of the crash, you may be able to seek support from the
          insurance provider of the other boater. It is common, however, for
          boaters and insurance providers to deny responsibility. If this
          happens to you, it may be necessary to explore your legal options.
        </p>
        <p>
          You have the right as the injured victim of a Los Angeles boat
          accident to appeal the decision of an insurance company and to file a
          claim in court. For victims of serious injuries, civil litigation
          against the at-fault boater is often the only way to receive fair
          compensation.
        </p>
        <h2>Top 12 Most Common Boating Accidents</h2>
        <p>
          There are many reasons an accident on the water can happen. The
          highest percentage of water-based accidents is from inattention of the
          driver. Things can happen and environments can change in the blink of
          an eye when on the water.
        </p>
        <p>
          If you aren't paying attention for a fraction of a second, you could
          miss your turn, run ashore, hit another vessel, water hazard, object
          or worse, a human being. Here's a list of the
          <strong> Top 15 Most Common types of boating accidents:</strong>
        </p>
        <ul>
          <li>
            <strong> Operator Inattention</strong>
          </li>
          <li>
            <strong> Improper Lookout</strong>
          </li>
          <li>
            <strong> Operator Inexperience</strong>
          </li>
          <li>
            <strong> Excessive Speed</strong>
          </li>
          <li>
            <strong> Machinery Failure</strong>
          </li>
          <li>
            <strong> Alcohol Use</strong>
          </li>
          <li>
            <strong> Navigation Rules Violation</strong>
          </li>
          <li>
            <strong> Force of Wave or Wake</strong>
          </li>
          <li>
            <strong> Hazardous Waters</strong>
          </li>
          <li>
            <strong> Weather</strong>
          </li>
          <li>
            <strong> Sudden Medical Condition or Emergency</strong>
          </li>
          <li>
            <strong> Distracted Operator</strong>
          </li>
          <li>
            <strong> Reckless Driving</strong>
          </li>
          <li>
            <strong> Mechanical Defect</strong>
          </li>
          <li>
            <strong> Stunt driving or controlled maneuvers</strong>
          </li>
        </ul>
        <h2>Boating Accident Injuries in Los Angeles</h2>
        <p>
          Boating or watercraft accidents can result in injuries similar to
          those suffered in car accidents. However, in these cases, there is the
          additional danger of drowning. Victims of Los Angeles boating
          accidents could sustain brain injuries in a high-impact collision,
          especially when an individual's head hits the interior of a boat.
        </p>
        <p>
          Victims may sustain severe lacerations and amputations if they come
          into contact with a propeller or other sharp objects. Unless the
          victim is wearing a U.S. Coast Guard-approved life jacket, there is
          the potential risk of drowning as well. These types of serious,
          life-changing injuries can occur at a moment's notice. Therefore, it
          is important for all boat operators and passengers to exercise safe
          riding habits.
        </p>
        <h2>5 Ways to Be Proactive</h2>
        <p>
          It is possible to seek monetary compensation for all financial and non
          financial losses suffered in an accident. First, you should consider
          the medical costs that you have incurred as a result of the accident
          and injuries.
        </p>
        <ul>
          <li>
            Calculate not only the immediate treatment costs, but also the
            future medical bills that you may incur including the cost of
            rehabilitation services, medical devices and prescription drugs.
          </li>
        </ul>
        <ul>
          <li>
            You can also pursue support for your lost wages and your loss of
            earning potential if your long-term injuries will affect your
            ability to earn wages in the future.
          </li>
        </ul>
        <ul>
          <li>
            There are also non-economic losses that may be included in a claim.
            For example, victims of severe injuries can seek support for their
            physical pain and mental anguish.
          </li>
        </ul>
        <ul>
          <li>
            It is advisable to keep a journal of your day-to-day struggles since
            your injuries occurred.
          </li>
        </ul>
        <ul>
          <li>
            Create a log of the activities you are not able to perform because
            of your injuries. You are entitled to receive compensation for all
            the losses you have sustained as a result of the injuries you
            suffered.
          </li>
        </ul>
        <h2>Types of vehicles covered</h2>
        <p>
          There are many types of accidents that can occur on the water. Whether
          you're enjoying a lake, the ocean, rivers, inlets, bays or even large
          ponds, creeks or streams. Vehicles that are typically involved in
          water-based accidents consist of:
        </p>
        <ul>
          <li>Motorized Boats</li>
          <li>Inboard Engines</li>
          <li>Outboard Engines</li>
          <li>Sailboats</li>
          <li>Personal Watercraft's</li>
          <li>Jet Skis</li>
          <li>Seadoos</li>
          <li>Kayaks</li>
          <li>River Boats</li>
          <li>Jet Boats</li>
          <li>Commercial Boats</li>
          <li>Fishing Boats</li>
          <li>Dive Boats</li>
          <li>Submersibles</li>
          <li>Pontoons</li>
          <li>Ferries</li>
          <li>Hydrofoils</li>
          <li>Oil Tankers</li>
          <li>Patrol Boats</li>
          <li>Row Boats</li>
          <li>Trawlers</li>
          <li>Steam Boats</li>
          <li>Yachts</li>
          <li>Charter Boats</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Boat maintenance"
            style={{
              backgroundImage:
                "url('/images/text-header-images/boat-repair-maintenance-accident-attorneys-lawyers.jpg')"
            }}
          >
            <h2>Regular Boat Maintenance</h2>
          </div>
        </LazyLoad>
        <p>
          If you own a watercraft, vessel or boat, you should regularly perform
          routine maintenance to ensure it is properly functioning. Making sure
          your boat does not have any current or outstanding recalls, defects or
          issues that could result in a boating accident.
        </p>
        <p>
          There are a number of things to check and make sure are not in need of
          repair. If repair or maintenance is needed, it is better to have
          completed it before you head out on the water.
        </p>
        <p>Here is a general list of important items to check:</p>
        <ul>
          <li>Batteries</li>
          <li>Electrical System</li>
          <li>Fuel System</li>
          <li>Corrosion Prevention</li>
          <li>Navigation Lights</li>
          <li>Through-hulls</li>
          <li>Bilge Pumps</li>
          <li>Safety Equipment</li>
          <li>Personal flotation devices (PFD's)</li>
          <li>Fire Extinguishers</li>
          <li>Ground Tackle (Anchors, etc.)</li>
          <li>Stoves</li>
          <li>Bathrooms</li>
        </ul>
        <p>
          Checkout the entire detailed{" "}
          <Link
            to="http://www.discoverboating.com/owning/maintenance/semiannual.aspx"
            target="new"
          >
            Semi-Annual Boating Maintenance Checklist
          </Link>{" "}
          for more details.
        </p>
        <h2>Boating Accident Prevention</h2>
        <p>
          First and foremost, everyone who is on a boat must wear a life jacket.
          Even individuals who are simply coasting at a low rate of speed should
          wear a life jacket so that they are protected in the event of an
          accident or a collision. Wearing a jacket will help keep you afloat in
          the event of a crash even if you lose consciousness.
        </p>
        <p>
          It is advisable for anyone who wants to operate a boat to take a
          boating safety course. A refresher course can help even the most
          experienced boater avoid a crash. When behind the controls, make sure
          you only travel at a speed that is safe for the conditions on the
          water and that you're capable of. When there are multiple boats
          nearby, slow down. When the wind and rain pick up, dock your boat
          until the weather improves.
        </p>
        <p>
          When it is safe to boat, make sure you have a spotter. It can be easy
          to over look small watercraft and swimmers. Your spotter should help
          you survey the surrounding area. Get rid of all distractions when you
          are operating a boat or watercraft. Put away your phone. Make sure you
          are not under the influence while boating and look out for other
          boaters who are behaving erratically.
        </p>
        <h2>Can you get a DUI on a boat?</h2>
        <p>
          Yes. The same rules apply whether you are intoxicated and driving a
          car, boat, motorcycle or personal watercraft. If you're at or above
          a&nbsp;blood-alcohol concentration, or&nbsp;BAC&nbsp;of .08%,
          penalties, fines and punishment include:
        </p>
        <ul>
          <li>Thousands of dollars in fines and penalties</li>
          <li>License restriction or suspension</li>
          <li>Treatment programs and workshops</li>
        </ul>
        <p>
          Second, third and more time offenders are subject to much more intense
          punishment and fines. Learn more about DUI penalty specifics, and
          information on if anyone is injured or killed in the process Here.
        </p>
        <p>
          (http://dui.drivinglaws.org/resources/dui-laws-state/penalties-dui-california.htm#)
        </p>
        <h2>Involved in a DUI accident?</h2>
        <p>
          If you have been involved or have lost a loved one due to a drunk
          driver, let Bisnar Chase experienced team of lawyers be there for you
          and your family. We have over 35 years of experience and a 96 percent
          success rate.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles boating accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/aftermath-of-a-crash-boating-accidents-attorneys-lawyers.jpg')"
            }}
          >
            <h2>The Aftermath of a Crash</h2>
          </div>
        </LazyLoad>
        <p>
          If you are involved in a boating accident, it is important that you
          quickly determine if anyone has been hurt. If any injuries have been
          suffered, the authorities must be notified. Get the victims out of the
          water and wait for emergency services. You should exchange information
          with any other boaters involved and get the contact information from
          anyone who may have witnessed the accident. Witnesses are very
          important and will benefit your case.
        </p>
        <p>
          Before you begin to forget important details, write down everything
          you remember about the collision. It is also important that you get
          prompt medical attention and document your injuries.
        </p>
        <p>
          After all of the information is recorded and necessary actions are
          taken, contact Bisnar Chase for immediate assistance.
        </p>
        <h2>Top quality litigation and legal representation</h2>
        <p>
          Bisnar Chase is passionate about our clients. Since 1978, John Bisnar
          built the practice with one goal in mind; the best legal care possible
          for his clients. Bisnar Chase has helped over 12,000 clients and won
          over $500 Million in verdicts and settlements. If you are seeking
          qualified and caring Los Angeles boating attorneys, please contact our
          office to speak with one of our Los Angeles boat accident lawyers. To
          communicate with attorney immediately, call 323-238-4683 for a&nbsp;{" "}
          <Link to="/los-angeles/contact">
            <strong> Free consultation</strong>
          </Link>
          .
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
