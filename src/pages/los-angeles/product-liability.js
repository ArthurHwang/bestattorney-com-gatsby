// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/product-liability-los-angeles-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Product Liability Lawyers – Defective Product Attorneys"
        pageDescription="You shouldn't have to worry about the health & safety of your family when buying something. The Los Angeles Product Liability Lawyers have over 40 years of experience with fighting & winning against the largest corporations in the world. Call our defective product attorneys for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Product Liability Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Los Angeles Product Liability Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          When you put your faith in a company and product, you are trusting
          they are looking out for the health, safety and best interest of you,
          your children, and your family, and the
          <strong> Los Angeles Product Liability Lawyers </strong>of Bisnar
          chase make sure those who fail in doing so are held accountable.
        </p>
        <p>
          Injuries and property damage can can be mild to catastrophic, and
          should not even exist, but unfortunately they do. Understanding how to
          be prepared in the case of a product defect related injury can be
          beneficial to your health and cases victory.
        </p>
        <p>
          If you have experienced an injury or property damage in result of a
          product defect and or product liability situation, <b>Call </b>our{" "}
          <b>top-rated Los Angeles Product Liability attorneys </b>at
          <b> 323-238-4683 </b>to discuss a legal course of action & a{" "}
          <b>Free Case Evaluation. </b>Our <strong> highly skilled </strong>team
          of <b>attorneys </b>have recovered over <b>$500 Million.</b>
        </p>
        <h2>No Corporation Too Large for Bisnar Chase</h2>
        <p>
          At Bisnar Chase we have proudly represented numerous clients who have
          suffered an injury or loss due to a defective or unsafe product - and
          we have won. Our Los Angeles product liability lawyers don't shrink
          away from a fight, even when we are dealing with the largest automaker
          in the United States, a giant corporation or an industry leader.
        </p>

        <p>
          We have the knowledge, experience, skill and the resources it takes to
          stand up and fight against large corporations that refuse to take
          responsibility for their defectively manufactured or poorly designed
          products.
        </p>
        <p>
          Companies vigorously fight product liability claims to protect their
          bottom line and reputation. If they accept your claim without a fight,
          they are essentially
          <b> admitting that their product is defective</b>. This can hurt their
          sales and stock value. Therefore, it is common to see companies employ
          teams of lawyers to aggressively fight the allegations in court.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/VBC0aa6eUEM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What is a Product Defect?</h2>
        <p>
          A product defect is when a product has an imperfection, manufacturing
          defect or is faulty and poses an unsafe quality for use, injury or
          hazardous condition for pocession.
        </p>
        <p>
          A product defect is not always visible and may not become apparent
          until multiple uses. Sometimes a product will work as it should prior
          to its defective recognition, but if a product becomes faulty in an
          unexpected mannor and causes injury, damage or loss, you may be
          entitled to compensation.
        </p>
        <h2>Types of Product Defects</h2>
        <p>
          <strong> Automotive</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="/blog/ford-and-honda-add-1-million-more-vehicles-to-takata-airbag-recall"
              target="new"
            >
              Airbag failure{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/auto-defects/defective-seatbelts/gm-seat-belt-failure"
              target="new"
            >
              Seatbelt failure{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/gm-recall-vehicle-rollaways" target="new">
              Rollover tendencies in high profile and top heavy vehicles{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/blog/bridgestone-firestone-recalls-tires-for-possible-tread-separation"
              target="new"
            >
              Tire defects{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/blog/toyota-and-honda-recall-vehicles-for-brake-defects"
              target="new"
            >
              Brake failure{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/auto-defects/buying-used-cars" target="new">
              Mechanical Defects{" "}
            </Link>
          </li>
        </ul>
        <p>
          <strong> Children's Toys</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="/blog/oball-baby-rattles-recalled-for-choking-hazard"
              target="new"
            >
              Small parts that break off causing a choking hazard{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/cribs-recalled-for-entrapment-dangers" target="new">
              Entrapment dangers in collapsable products like playpens{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/blog/millions-of-mcdonalds-happy-meal-fitness-trackers-recalled-after-injury-reports"
              target="new"
            >
              Hazardous chemicals within a product that may leak{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://www.10tv.com/article/childrens-shoe-recalled-due-sharp-edges"
              target="new"
            >
              Sharp edges{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/auto-defects/car-seat-injuries" target="new">
              Unsafe Car Seats{" "}
            </Link>
          </li>
        </ul>
        <p>
          <strong> Food Hazards</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="/blog/nestle-issues-product-recall-for-possible-glass-contamination"
              target="new"
            >
              Harmful or non-edible fragments in food products{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/blog/tyson-foods-recalls-chicken-wings-for-potential-contamination"
              target="new"
            >
              Contamination and food poisoning{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/food-poisoning/types" target="new">
              Bacteria{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://www.foodsafetynews.com/tag/mislabeling/#.WO5p4_nyuUk"
              target="new"
            >
              Mislabeled products{" "}
            </Link>
          </li>
        </ul>
        <p>
          <strong> Other types of products that can have defects</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link
              to="https://www.automd.com/recall/coast-machinery_m/"
              target="new"
            >
              Machinery{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://www.consumeraffairs.com/recalls04/aaarecalls_sport.htm"
              target="new"
            >
              Sports euipment{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://wemakeitsafer.com/Power-Hand-Tools-Recalls"
              target="new"
            >
              Power tools{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://wemakeitsafer.com/Cleaners-Cleaning-Products-Recalls"
              target="new"
            >
              Cleaning products{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="http://wemakeitsafer.com/Furniture-Recalls" target="new">
              Furniture{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="http://wemakeitsafer.com/Computers-Electronics-Recalls"
              target="new"
            >
              Electronics{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.recalls.gov/cpsc.html" target="new">
              Clothing{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://www.recalls.gov/food.html" target="new">
              Food and beverages{" "}
            </Link>
          </li>
        </ul>
        <h2>Proving a Product Liability Case</h2>
        <p>
          Companies are responsible for the safety and effectiveness of the
          products they manufacture. If you have suffered an injury or other
          damages because of a defective product, you may be able to file a
          claim against the manufacturer of that product to recover financial
          compensation for your injuries and damages. This type of lawsuit is
          called a product liability claim. These types of claims not only aim
          to help injured victims obtain fair compensation, but also help hold
          the negligent product manufacturers accountable.
        </p>
        <p>
          An experienced Los Angeles product liability lawyer understands the
          intricate details of these types of cases and winning them requires a
          talented{" "}
          <Link to="/" target="new">
            personal injury lawyer
          </Link>{" "}
          that has specialty in this field. Proving that a product is defective
          is an important part of any product liability lawsuit. There are three
          ways in which a product can be defective:
        </p>
        <ul>
          <li>
            <b>Defective manufacturing</b>: This is when mistakes are made
            during the making or assembly of the product. In such cases, a
            limited number of the products may be defective because of errors
            made during the manufacturing process. Products that are typically
            involved in this type of defective product liability claim are
            children's products and vehicles.
          </li>
          <li>
            <b>Defective design</b>: Some products are dangerous no matter how
            well they are manufactured. These types of cases do not involve a
            single faulty product, but an entire line of products that are
            inherently dangerous. For example, many SUV models are extremely
            dangerous because they are designed with a higher center of gravity
            and narrower tack width, which makes them more prone to rollover
            crashes.
          </li>
          <li>
            <b>Defective marketing</b>: This type of claim involves a{" "}
            <Link to="/los-angeles/failure-to-warn" target="new">
              failure to adequately warn{" "}
            </Link>{" "}
            or instruct consumers. Promoting a product to do something it cannot
            is a form of defective marketing. Failing to inform consumers about
            the potential consequences of using the product is a form of
            negligence as well.
          </li>
        </ul>
        <p>
          There are many potentially liable parties in cases involving defective
          products. In many cases, it is possible to include all parties
          involved in the products chain of distribution. This means that
          potentially liable parties include the manufacturer as well as the
          retailer, supplier, wholesaler and distributor. It is even possible to
          file a claim against designers, quality control engineers and
          consultants if their negligence contributed to your injury.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Successful Los Angeles Defective Product Lawsuits"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-los-angeles.jpg')"
            }}
          >
            <h2>Successful Los Angeles Defective Product Lawsuits</h2>
          </div>
        </LazyLoad>

        <p>
          There are a number of basic elements that must be proved for a product
          liability case to be successful:
        </p>
        <ul>
          <li>You have suffered an injury or other type of loss.</li>
          <li>The product involved in the case was defective in some way.</li>
          <li>
            The nature of the defect directly caused your injuries or damages.
          </li>
          <li>
            You were using the defective product as intended at the time of the
            accident.
          </li>
        </ul>
        <h2>The Los Angeles Statute of Limitations</h2>
        <p>
          There is a limited amount of time that available to plaintiffs to file
          any injury claim. This time limit or deadline is known as statute of
          limitations. Once this time has passed, you will no longer able to
          receive compensation for your losses.
        </p>
        <p>
          There are exceptions, but in general, there is a two-year statute of
          limitations on defective product cases in Los Angeles.
        </p>
        <p>
          If you or a loved one has been injured due to a defective product,
          contact one of our highly skilled Los Angeles liability lawyers at{" "}
          <b>323-238-4683</b>, for immediate assistance.
        </p>
        <h2>A Los Angeles Product Liability Laywer For Your Claim</h2>
        <p>
          Before deciding whether or not to file a claim, it is helpful to know
          how much your claim may be worth. In order to do this, you must have a
          basic understanding of the current and future losses you have suffered
          as a result of your injuries. Have you missed work? Are there medical
          bills, hospitalizations fees or the cost of rehabilitation services
          that need to be paid or covered? These are economic losses that can be
          included in your claim.
        </p>
        <p>
          In cases involving serious injuries, it is possible to pursue
          non-economic damages such as pain and suffering. In some product
          liability cases, it is even possible to pursue compensation for
          punitive damages. This is when a company is penalized for their
          negligence or wrongdoing. The size of the punitive damages is related
          to the severity of the company's wrongdoing. This form of punishment
          is reserved for cases where a product manufacturer has been
          particularly reckless.
        </p>
        <p>
          For a <strong> Free</strong> legal assessment of your claim, speak to
          a <strong> Los Angeles Product Liability Attorney</strong> at{" "}
          <b>323-238-4683</b> for a{" "}
          <Link to="/contact" target="new">
            <strong> Free Case Consultation</strong>
          </Link>{" "}
          today.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
