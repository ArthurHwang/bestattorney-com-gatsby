// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident",
  sidebarLinks: {
    title: "Los Angeles Car Accident Information",
    links: [
      {
        linkName: "Hazardous Roads",
        linkURL: "/car-accidents/hazardous-roads"
      },
      {
        linkName: "Head-On Car Accidents",
        linkURL: "/car-accidents/head-on-car-accident-lawyer"
      },
      {
        linkName: "Hit and Run Accidents",
        linkURL: "/los-angeles/hit-and-run-accidents"
      },
      {
        linkName: "Injury Compensation",
        linkURL: "/car-accidents/injury-compensation"
      },
      {
        linkName: "Insurance Bad Faith",
        linkURL: "/car-accidents/insurance-bad-faith"
      },
      {
        linkName: "Insurance Hardball",
        linkURL: "/car-accidents/insurance-hardball"
      },
      {
        linkName: "Lemon Law",
        linkURL: "/car-accidents/lemon-laws"
      },
      {
        linkName: "Lower Extremity Injuries",
        linkURL: "/car-accidents/lower-extremity-injuries"
      },
      {
        linkName: "Parking Lot Accidents",
        linkURL: "/los-angeles/parking-lot-accidents"
      },
      {
        linkName: "Premature Insurance Settlements",
        linkURL: "/car-accidents/premature-insurance-settlements"
      },
      {
        linkName: "Rental Car Safety",
        linkURL: "/car-accidents/rental-car-safety"
      },
      {
        linkName: "Rights of Passengers",
        linkURL: "/car-accidents/rights-of-passengers"
      },
      {
        linkName: "Rear-End Accidents",
        linkURL: "/car-accidents/rear-end-accident-lawyer"
      },
      {
        linkName: "Rideshare Car Accidents",
        linkURL: "/car-accidents/rideshare-accident-lawyer"
      },
      {
        linkName: "Road Debris Accidents",
        linkURL: "/los-angeles/road-debris-car-accidents"
      },
      {
        linkName: "SUV Rollovers",
        linkURL: "/car-accidents/suv-rollovers"
      },
      {
        linkName: "SUV Roof Crush",
        linkURL: "/car-accidents/suv-roof-crush"
      },
      {
        linkName: "T-Bone Accidents",
        linkURL: "/car-accidents/t-bone-car-accident-lawyer"
      },
      { linkName: "Bus Accidents", linkURL: "/los-angeles/bus-accidents" },
      {
        linkName: "Boating Accidents",
        linkURL: "/los-angeles/boating-accidents"
      },
      { linkName: "Train Accidents", linkURL: "/los-angeles/train-accidents" },
      {
        linkName: "Tour Bus Accidents",
        linkURL: "/los-angeles/celebrity-tour-bus-lawyer"
      },
      {
        linkName: "Free Case Evaluation",
        linkURL: "/los-angeles/contact"
      },
      {
        linkName: "Los Angeles Home",
        linkURL: "/los-angeles/"
      }
    ]
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/you-hit-my-car-accident-los-angeles-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Car Accident Lawyer – Car Crash Attorneys, CA"
        pageDescription="Car crashes happen daily, often due to an auto defect, drunk, distracted driving or involving a pedestrian. Our Los Angeles Car Accident Lawyers have been winning vehicle accident cases for over 40 years. Call our car crash attorneys who have won over $500 Million for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Accident Lawyers in Los Angeles</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles car accident lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Driving is a casual activity most people are comfortable with, feel
          safe and think nothing of, but
          <strong>
            {" "}
            Los Angeles <Link to="/car-accidents">car accident lawyers </Link>
          </strong>
          know otherwise. Driving to work, picking the kids up from school,
          picking up groceries, road trips and practically anything you can
          think of. The aspect that is overlooked is the fact that these
          vehicles we use for daily transportation and recreation are giant
          pieces of metal that are very heavy, very dangerous and can cause
          catastrophic damage.
        </p>
        <p>
          If you have been injured in a car accident, contact Bisnar Chase
          injury attorney in Los Angeles. For immediate assistance and free
          consultation, <strong> call (323) 238-4683</strong>.
        </p>
        <h2>Situations That Can Result in a Car Accident in LA</h2>
        <p>
          Throughout the last five years, an average of just over 600 people
          were killed in motor vehicle accidents in a variety of situations:
        </p>
        <ul>
          <li>Alcohol and drug impaired driving</li>
          <li>Speeding</li>
          <li>Using a digital device</li>
          <li>Texting, phone calls, social media, emails, etc.</li>
          <li>Auto defects and mechanical errors</li>
          <li>Sudden road hazards and dangerous road conditions</li>
          <li>Roadway departure</li>
          <li>Intersection related</li>
          <li>Falling asleep</li>
          <li>Rear endings</li>
          <li>Running stop signs and red lights</li>
          <li>Single vehicle, passenger cars, truck accident</li>
          <li>Looking away from the road for too long</li>
          <li>Other distracting activities while driving</li>
        </ul>
        <p>
          The <strong> experienced car accident lawyers in Los Angeles</strong>{" "}
          at Bisnar Chase have successfully represented those who have suffered
          serious or catastrophic injuries as a result of car crashes. Our goal
          in each and every case is to provide superior legal representation, a
          very high quality of customer service and to help our severely injured
          clients and their families seek and obtain maximum compensation for
          their tremendous losses.
        </p>
        <p>
          Our dedicated and experienced&nbsp;accident and injury
          lawyers&nbsp;have been helping people injured in car accidents for
          over 40 years.&nbsp;We've won millions of dollars&nbsp;and may be able
          to help you too.&nbsp;Call the Personal Injury Law Firm of Bisnar
          Chase at (323) 238-4683&nbsp;to find out how we can help you.
        </p>
        <h2>Injured in a Car Accident in Los Angeles, CA?</h2>
        <p>
          If you or someone you love has been injured in a motor vehicle
          accident, there are a number of steps you can take to ensure that your
          legal rights and best interests are protected:
        </p>
        <ul>
          <li>
            File a police report and make sure you get a copy for your own
            records.
          </li>
          <li>
            Get as much information as you can about other parties involved in
            the collision including driver's license, vehicle license plates,
            auto insurance policy information, contacts (telephone, address,
            e-mail), vehicle registration information, etc.
          </li>
          <li>
            Gather as much evidence as possible from the accident scene. Take
            photographs of the vehicles involved, damage to the vehicles and any
            injuries sustained. Get contact information for any eyewitnesses. If
            you are unable to collect this information, request a family member
            or friend to do so. Such critical pieces of evidence will be
            difficult or impossible to obtain once the accident scene is cleared
            out.
          </li>
          <li>
            Seek and obtain prompt medical attention and treatment for your
            injuries.
          </li>
          <li>
            Save all bills and receipts of expenses related to the accident and
            injury.
          </li>
          <li>
            Contact an experienced Los Angeles car accident attorney who will
            help you understand your legal rights and options.
          </li>
        </ul>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/WSuOEz40ZPA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Los Angeles Car Accident Statistics</h2>
        <p>
          Car accidents in Southern California specifically Los Angeles County,
          are inevitable and happen every single day.
        </p>
        <ul>
          <li>Over one million people die in road crashes each year.</li>
          <li>That's over 3,200 deaths per day.</li>
          <li>
            Approximately 1,600+ children under the age of 15 die every year.
          </li>
          <li>
            Over $230 Billion are spent on road crashes by the United States
            Government each year.
          </li>
        </ul>
        <p>
          For more statistics for annual global road crashes, visit{" "}
          <Link
            to="http://asirt.org/initiatives/informing-road-users/road-safety-facts/road-crash-statistics"
            target="new"
          >
            ASIRT (Association for Safe International Road Travel)
          </Link>{" "}
        </p>

        <h2>Hit and Runs are Prevalent in Los Angeles</h2>
        <p>
          The Los Angeles Police Department report over 20,000 hit and runs
          every year, ranging from minor car collisions to catastrophic
          accidents resulting in multiple fatalities.
        </p>
        <p>
          If you have encountered a{" "}
          <Link to="/los-angeles/hit-and-run-accidents">hit and run </Link>,
          file a police report at the scene of the accident, seek medical care
          and contact a Bisnar Chase hit and run lawyer.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles car accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/do-you-need-a-car-accident-lawyer.jpg')"
            }}
          >
            <h2>Do You Need a Los Angeles Car Accident Attorney?</h2>
          </div>
        </LazyLoad>
        <p>
          If you did not suffer an injury in a car accident or if you escaped
          with scratches or relatively minor injuries that did not require you
          to go to the doctor or get hospitalized, you can probably handle the
          accident claim on your own by&nbsp;{" "}
          <Link to="/resources/negotiation-tips">
            talking to your auto insurance company
          </Link>
          .
        </p>
        <p>
          However, if you did suffer an injury that caused you to be
          hospitalized, seek physical therapy and miss workdays, then, you have
          a more complicated injury claim in your hands. It would be in your
          best interest to retain the services of a reputed car accident law
          firm that has a long and successful track record negotiating with
          insurance companies and handling injury cases that are similar to
          yours. Let the highly skill team of lawyers at Bisnar Chase be of
          assistance to you.
        </p>
        <h2>Why You Cannot Trust the Insurance Company</h2>
        <p>
          Here's something everyone needs to understand very clearly: The
          insurance company is not your friend. You cannot trust them to do
          what's in your best interest. And that's because they are for-profit
          corporations that are only concerned about one thing – profits.
        </p>
        <p>
          One of the ways in which insurance companies make sure their bottom
          line is secure is by denying claims, even when they are valid and
          justified. This is why it is not a good idea for injured victims --
          who are already in a vulnerable position because of their present
          condition and recent traumatic experience – to stand alone against a
          multi-billion-dollar corporation that has defense lawyers on its beck
          and call, to protect its interests.
        </p>
        <p>
          You need an advocate on your side, who will look out for your best
          interests. An experienced Los Angeles car accident lawyer can be that
          advocate who will not rest until you have been awarded just
          compensation for your losses. Let our skilled negotiating attorneys
          work on your behalf while you recover and regain your strength.
        </p>
        <h2>What is Your Los Angeles Car Accident Claim Worth?</h2>
        <p>
          The worth or value of your car accident or{" "}
          <Link to="/los-angeles/scooter-injury-lawyers" target="_blank">
            scooter accident
          </Link>{" "}
          claim will depend on the nature and extent of your injuries and the
          types of damages and losses you have sustained. The higher the damages
          and losses, the more your claim will be worth. Some of the elements
          that would determine the value of your claim include:
        </p>
        <ul>
          <li>
            Medical bills:&nbsp;This refers to any expenses you may have
            incurred in terms of emergency treatment, hospitalization,
            surgeries, cost of medication, medical equipment and rehabilitative
            therapy.
          </li>
          <li>
            Lost wages:&nbsp;If you lose income because you had to take days off
            work, then you can seek compensation for the wages you lost as you
            took time off to recover from your injuries.
          </li>
          <li>
            Loss of earning capacity:&nbsp;If you suffered a permanent injury or
            disability that prevents you from going back to your job or reduces
            your earning capacity, you can seek compensation for those losses
            that will include lost future income.
          </li>
          <li>
            Noneconomic damages:&nbsp;Seriously injured car accident victims
            suffer not only physical pain, but also mental anguish. A car
            accident is a traumatic event and it can take a toll on your body
            and emotions. Victims can also seek compensation from the at-fault
            parties for pain and suffering and emotional distress.
          </li>
        </ul>
        <h2>
          What Are the First Things I need to do After a Car Accident in LA?
        </h2>
        <p>
          &nbsp;Seeking proper medical attention immediately to ensure safety to
          your health and well-being, as well as documenting your injuries in
          association with the accident. If no documentation exists of your
          injuries in result of the accident, it never happened.
          &nbsp;Exchanging insurance, identification and contact information
          with everyone associated with the accident. Contacting a car accident
          attorney immediately to discuss the specifics of your injury case.
        </p>
        <h2>
          What Am I Entitled to if I Have Been in A Los Angeles Auto Accident?
        </h2>
        <p>
          Every car accident varies. What you are entitled to depends on the
          circumstances relating to the accident such as cases of reckless
          driving, to accidents involving intoxicated drivers, to premise
          liabilities that initially caused the wreck.
        </p>
        <p>
          When compensation is awarded to the victim or victims, it can include
          an amount to cover medical expenses, property damage, pain and
          suffering, the loss of future income, loss of companionship, etc.
        </p>
        <p>Some cases Bisnar Chase has closed have reached 7 and 8 figures.</p>
        <h2>
          What Happens if the Accident Was with Someone Who Has No Insurance?
        </h2>
        <p>
          Your policy should always have uninsured motorist coverage because if
          the other party has no assets and no coverage then there is no one to
          go after. You would want to do an asset search to find out if they own
          property or have income that you could perhaps go after in lieu of
          having auto coverage.
        </p>
        <p>
          Consulting with an attorney is important because it will help you
          avoid any possible mistakes that could harm your case. Insurance
          companies only want your money and you need to protect yourself from
          losing the majority of your compensation and let an attorney fight for
          the maximum amount.
        </p>
        <h2>
          What is Your Experience in Handling Los Angeles Auto Accident Claims?
        </h2>
        <p>
          The law offices of Bisnar Chase has spent over 39 years representing
          plaintiffs injured in a car accident. We have the resources the
          experience and the tenacity to win some of the toughest cases. Every
          attorney at Bisnar Chase is a trial lawyer and you can be assured your
          case will not be handed off to some other firm when the going gets
          tough.
        </p>
        <h2>
          What if No Other Car was Involved in the Accident, But it Wasn't My
          Fault?
        </h2>
        <p>
          There can be several factors when considering an accident including a
          victim only, such as premise liability, negligence, wrongdoing and
          many other reasons for the cause of your accident. Fortunately,{" "}
          <Link to="/">Bisnar Chase </Link> never backs down from our clients
          fight and will continue to the end until we win, or you don't pay a
          cent.
        </p>
        <h2>
          Is it Okay if I Consult with Insurance Companies After an Auto
          Accident?
        </h2>
        <p>
          No. Insurance companies want to pay the least amount of money they can
          for a claim and will advise you in the opposite direction of what will
          benefit you. And attorney will be sure to make sure you receive
          maximum compensation and the insurance companies know that so they
          will tend to persuade you not to hire an attorney. You have to
          remember the insurance company is for profit and not your friend.
        </p>
        <h2>What Do I Need to Bring to My Initial Consultation?</h2>
        <p>
          Bring as much evidence with you as you can because the more you can
          tell and show your attorney the better he will be able to understand
          the circumstances of your case. Typically, you'll want to have
          pictures of the accident witness reports and a police report for an
          initial consultation. If you don't have all of those items just tell
          your attorney as much as you can.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Car accident legal process"
            style={{
              backgroundImage:
                "url('/images/text-header-images/how-the-legal-process-works-car-accident-los-angeles.jpg')"
            }}
          >
            <h2>How the Process Works in Los Angeles</h2>
          </div>
        </LazyLoad>
        <p>
          The first step for Los Angeles car accident victims would be to call
          our law firm. We always offer free initial consultations where you
          will get an opportunity to discuss your case with us. You will receive
          a comprehensive evaluation and sense of what the process entails and
          how long it would take. You can get an idea of how the process works
          by using our&nbsp;{" "}
          <Link to="/resources/case-timeline">
            interactive car accident case time-line
          </Link>{" "}
          &nbsp;tool.
        </p>
        <p>
          If you choose to retain us, we will file a lawsuit on your behalf
          against one or more defendants, depending on the circumstances of your
          case. We pride ourselves in providing our clients with a superior
          level of customer service by keeping them posted about the status of
          their case and keeping them informed every step of the way.
        </p>
        <h2>Don't Pay Anything Until We Win Your Los Angeles Injury Case</h2>
        <p>
          We offer a no-win, no-fee guarantee to our clients. This means that
          you don't pay any fees or costs until we win or settle the case for
          you. If you don't get paid, we don't get paid.
        </p>
        <p>
          Our experienced and knowledgeable Los Angeles car accident attorneys
          are here to ensure that you receive fair and full compensation for
          your losses. We have an impeccable record when it comes to fighting
          insurance companies and protecting our clients' rights. We will not
          allow insurance companies or anyone else to take advantage of your
          vulnerability. Our lawyers also specialize in a multitude of practice
          areas. If you are unable to come to us because of your injuries, we
          will come to you.&nbsp;
        </p>
        <p>
          <strong>
            {" "}
            For immediate assistance, call us at (323) 238-4683 for a no-cost,
            no-obligation consultation and a comprehensive case evaluation.
          </strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
