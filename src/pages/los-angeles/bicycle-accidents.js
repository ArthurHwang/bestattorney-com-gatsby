// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/cyclist-bicycle-accident-attorney-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bicycle Accident Attorneys Los Angeles – Bike Crash Lawyers, CA"
        pageDescription=" Our experienced Los Angeles bicycle accident lawyers can help if you’ve been involved in a bike accident. Call us for a free consultation at 323-238-4683. free consultation and 96% success rate. No win, no fee guarantee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Bicycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Los Angeles Bicycle Accident Lawyers at Bisnar Chase"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Contact a <strong> Los Angeles bicycle accident attorney</strong> for
          a free consultation. Find out if you are entitled to injury
          compensation. Victims of Los Angeles bicycle accidents and their
          families can seek financial compensation for their losses by filing a
          personal injury claim against the at-fault party.
        </p>
        <p>
          Every accident is different and many serious injury accidents
          potentially have multiple liable parties. A skilled{" "}
          <Link to="/los-angeles">Los Angeles personal injury attorney </Link>{" "}
          will stay abreast of the official investigation to ensure that fair
          compensation is offered to the victim or the victim's family.
        </p>
        <h2>Top 8 Types of Bike Accidents in Los Angeles</h2>
        <p>
          Riding bicycles in LA can range from recreational use, for work, for
          transportation and many other uses. But regardless of the reason for
          utilizing a bikes transportation capabilities, they can be dangerous,
          whether it is you, the bike or someone else's fault that causes the
          bicycle accident or personal injury.
        </p>
        <p>
          Here is a list of the{" "}
          <strong> Top 8 Most Common Reasons for Bicycle Accidents:</strong>
        </p>
        <ol>
          <li>A motorist made a right turn in front of bicyclist</li>
          <li>A motorist made a left turn in front of an oncoming bicyclist</li>
          <li>
            Motorist opened vehicle door resulting in bicyclist crashing into
            door
          </li>
          <li>Sudden road debris obstructing bicyclist path</li>
          <li>Premise liability or Governmental entity</li>
          <li>
            Rider error - Losing control, excessive speed or distraction causing
            crash
          </li>
          <li>
            Bicycle malfunction - Tire blow out, frame snapping or pieces coming
            apart
          </li>
          <li>
            Alcohol: Many times, alcohol is associated with bicycle accidents
          </li>
        </ol>
        <h2>Bike Injuries Can Be Deadly</h2>
        <p>
          Every year thousands of pedestrians sustain minor injuries in a bike
          accident but many of them are serious and even life threatening. It's
          typical to suffer severe head trauma in bike accident even while
          wearing a helmet.
        </p>
        <p>
          Fractures are common and can take a long time to heal resulting in an
          impaired quality of life. Immediate care right after the bike accident
          is vital to recovery.
        </p>
        <p>
          Even if you feel fine immediately following a bike accident, you
          should have a full medical exam to make sure there are no underlying
          injuries that may come up later.
        </p>
        <p>
          Most common causes of bicycle accidents are the bike riders using the
          wrong side of the street or making unsafe turns in traffic. The rider
          should always be aware of oncoming traffic and not rely on the
          vehicles to see them. <strong> Always ride WITH traffic.</strong>
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Top 10 Most Common Injuries from Bicycle Accidents</h2>
        <p>
          Growing up we learned how to ride bikes, and many times it resulted in
          smashing into a mailbox, stop sign or parked car, leaving us crying
          and holding scraped up and bloodied knee or elbow. But for many
          bicyclist who ride to work, on roadways with other vehicles or any
          other reason, injuries can result from severe to fatal.
        </p>
        <p>
          Here is a list of the{" "}
          <strong> Top 10 Most Common Injuries from Bicycle Accidents:</strong>
        </p>
        <ol>
          <li>
            <strong> Skin and soft tissue</strong>: contusions, lacerations and
            abrasions (scrapes, cuts and bruises)
          </li>
          <li>
            <strong> Bone fractures</strong>: arms, wrists, legs, ankles, etc.
          </li>
          <li>
            <strong> Traumatic head injuries</strong>: concussions, coma,
            aphasia, etc.
          </li>
          <li>
            <strong> Pulled, strained or tore tendons and ligaments</strong>
          </li>
          <li>
            <strong> Hip and pelvis</strong>: iliopsoas tendinitis and
            trochanteric bursitis
          </li>
          <li>
            <strong> Handlebar neuropathies</strong>: Ulnar nerve, median nerve
            (deep palmar branch)
          </li>
          <li>
            <strong> Face and eye</strong>: corneal foreign bodies, facial and
            dental fractures
          </li>
          <li>
            <strong> Dismemberment or amputations</strong>: if an accident is
            severe enough, the loss of limbs or body parts is a high probability
          </li>
          <li>
            <strong> Brain Injuries</strong>: aneurysm, stroke, hemorrhage or
            permanent brain damage
          </li>
          <li>
            <strong> Death</strong>
          </li>
        </ol>
        <p>
          Victims' families can file a <strong> Wrongful Death</strong> claim if
          it was not entirely the bicyclists fault. Compensation that is
          rewarded does depend on your states current laws, but can consist of
          the following:
        </p>
        <ul>
          <li>Pain and suffering</li>
          <li>Medical costs</li>
          <li>Funeral costs</li>
          <li>Loss of future income</li>
          <li>Loss of companionship</li>
        </ul>
        <h2>Rights of Injured Cyclist in Los Angeles</h2>
        <p>
          The experienced Los Angeles bicycle accident lawyers at our law firm
          have helped numerous accident victims protect their legal rights,
          obtain just compensation and hold negligent parties accountable.
        </p>
        <p>
          We fight hard for the rights of our clients and help ensure that they
          receive <Link to="/case-results">maximum compensation </Link> for
          their significant losses.
        </p>
        <p>
          If you or a loved one has been injured in a Los Angeles bicycle
          accident, please contact us to obtain more information about pursuing
          your legal rights.
        </p>
        <p>
          There are a number of class I bike paths in Los Angeles where only
          bicyclists are allowed. These paths are often a safe distance from
          motorists as they run along rivers, parks and beaches. Unfortunately,
          just about anywhere else you travel in Los Angeles on a bicycle can be
          potentially dangerous.
        </p>
        <p>
          A review of recent Los Angeles bicycle accidents shows that there are
          many causes of injury accidents and many locations where an accident
          can occur.
        </p>
        <p>
          According to California Highway Patrol's 2010 Statewide Integrated
          Traffic Records System (SWITRS), 11 people were killed recently and
          2,065 were injured in Los Angeles bicycle accidents.
        </p>
        <p>
          During that same year, there were 25 fatalities and 4,201 injuries
          reported due to Los Angeles County bike crashes. Los Angeles is moving
          towards promoting more bicycle laws and rights.
        </p>
        <p>
          You can download the Bicycle Riding Tips brochure from the CHP for
          tips on protecting your safety.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/injured-cyclist-rights.jpg"
            width="100%"
            alt="Bicycle Accident Law firms Los Angeles"
          />
        </LazyLoad>
        <h2>Unsafe Locations in Los Angeles for Bicyclists</h2>
        <p>
          A 34-year-old Los Angeles man was killed during the early morning
          hours of August 19, 2012, while biking in the Mount Washington area of
          Los Angeles. Jean Carlos Galavis was traveling southbound on Canon
          Crest Street toward Avenue 45 when he crashed into a hillside.
        </p>
        <p>
          He suffered abdominal and internal bleeding and succumbed to his
          injuries at a nearby hospital. Officials say the dangerous nature of
          the roadway, which is steep and narrow, may have contributed to the
          fatal bicycle accident.
        </p>
        <p>
          Similarly, on August 5, 2012 a man in his 50s was seriously injured in
          a Los Angeles bicycle accident along San Gabriel Road. According to
          The San Gabriel Valley Tribune, he lost control downhill on the
          winding mountain road and collided with the mountainside.
        </p>
        <p>
          Had he not been wearing a helmet, his injuries could have been far
          worse.
        </p>
        <p>
          Were these roadways safe for bicyclists? Were they safely designed and
          adequately maintained? Should there have been warning signs to
          encourage bikers to slow down on the winding, mountainous roads?
        </p>
        <p>
          These are some of the important questions that must be asked in
          bicycle accidents involving dangerous roadways. If a hazardous roadway
          did cause or contribute to a bicycle accident, the governmental agency
          (such as a city or county) can be held liable for the incident.
        </p>
        <p>
          Under California Government Code Section 911.2, any personal injury or
          wrongful death claim against a governmental agency must be filed
          within 180 days of the incident.
        </p>
        <h2>Passionate Legal Representation in Los Angeles</h2>
        <p>
          Bisnar Chase has been helping injured bike accident victims in Los
          Angeles since 1978 and has established a{" "}
          <strong> 96% success rate</strong>. The firm has spent over
          <strong> 40 years</strong> working to obtain justice for plaintiffs
          and has won over <strong> $500 Million</strong> for our clients. We
          are familiar with LA courts and defense teams after spending decades
          taking cases to trial and mediation.
        </p>
        <p>
          If you've been injured in a bicycle accident contact a{" "}
          <strong> Bisnar Chase Los Angeles Bicycle Accident Lawyer</strong> for
          a <strong> Free Case Evaluation. </strong>You may be entitled to
          recovery and compensation.
        </p>
        <p align="center">
          <strong> For immediate assistance Call 323-238-4683.</strong>
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        >{/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
