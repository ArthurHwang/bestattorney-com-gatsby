// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "images/bird-scooter-header.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Scooter Accident Lawyer | Electric Scooter Injury Attorneys"
        pageDescription="The Los Angeles Scooter Accident Lawyers of Bisnar Chase are here to help victims who have suffered from severe collision injuries from Bird motorized scooters. Call 323-238-4683 for a free consultation"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Scooter Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Scooter accident lawyer in Los Angeles"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The{" "}
          <strong>
            {" "}
            Los Angeles Scooter Accident Lawyers of{" "}
            <Link to="/" target="_blank">
              {" "}
              Bisnar Chase{" "}
            </Link>
          </strong>{" "}
          have been winning compensation for victims who have suffered
          physically and emotionally due to a manufacturers negligence. Users of
          motorized scooters have found themselves in the emergency room and
          have been left with not only severe injuries but also hefty medical
          bills along with lost wages.
        </p>
        <p>
          The law firm of Bisnar Chase has held a{" "}
          <strong> 96% success rate</strong> for over <strong> 40 years</strong>{" "}
          and are determined to win even the toughest of cases.
        </p>
        <p>
          If you or someone you know has endured pain and suffering from a
          "Bird" scooter accident contact the law firm of Bisnar Chase. Upon
          your call, you will be immediately connected to one of our
          highly-skilled legal team members. Contact the{" "}
          <Link to="/los-angeles">Los Angeles </Link> scooter injury attorneys
          of Bisnar Chase at<strong> 323-238-4683. </strong>Upon your call you
          will receive a<strong> free consultation</strong> and legal advice for
          your injury case.
        </p>
        <p>
          You don't have to pay out-of-pocket for someone else's carelessness,{" "}
          <strong> call now</strong>.{" "}
        </p>
        <h2>How Bird and Lime Scooter Share Programs Operate</h2>
        <p>
          There are a wide variety of electric scooters which are comprised of
          bikes, motorcycles or electric-kick scooters. Scooter rental companies
          such as "Bird" have accumulated millions of dollars in revenue due to
          accessibility. The motorized scooters that are available for rent in
          beach areas such as Santa Monica, California are accompanied by a
          phone app which allows you to find a scooter company near you.
        </p>
        <p>
          The phone app also unlocks the scooter for riding usage. The "Bird"
          scooter cost is $1 dollar to rent and it 15 cents per minute to ride.
          Motorized scooter companies have claimed that this new form of
          transportation makes getting around easier and also gives an
          environmentally friendly option for short-distance commuters.
        </p>
        <h2>3 Common Types of Bird Scooter Injuries in Los Angeles</h2>
        <div>
          <p>
            Motorized scooters such as "Bird" scooters can go up to 15 mph and
            without a helmet or any safety gear can have serious consequences.
            Not only that but without proper driving laws instilled many
            motorized scooter users have endured severe bodily harm due to being
            struck by a vehicle and have faced a tremendous amount of legal
            issues. Below are the three most common kinds of injuries that are
            experienced in scooter incidents.
          </p>
        </div>
        <p>
          <strong> Head Injury</strong>:
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/head-injury-image.jpg"
            width="218"
            height="199"
            className="imgleft-fixed"
            alt="Motorized scooter injury lawyers in Los Angeles"
          />
        </LazyLoad>

        <p>
          Each year over 1.7 million people experience{" "}
          <Link
            to="http://www.aans.org/Patients/Neurosurgical-Conditions-and-Treatments/Traumatic-Brain-Injury"
            target="_blank"
          >
            {" "}
            traumatic brain injuries
          </Link>
          . Recreational activities account for 21% percent of the brain trauma
          that has been reported. Scooters do not provide any protection and
          leave riders vulnerable on the street.
        </p>
        <p>
          Motorized scooter brand, "Bird Scooters" has gained notoriety for many
          users being involved in dangerous instances. Riders have suffered from
          catastrophic head injuries and have accumulated various kinds of head
          trauma such as concussions.
        </p>

        <p className="clearfix">
          <strong> Broken Bones</strong>:
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/broken-arm-image.jpg"
            width="218"
            height="199"
            className="imgleft-fixed"
            alt="Los Angeles scooter injury lawyers"
          />
        </LazyLoad>

        <p>
          "Bird" scooters having defective brake systems have caused riders to
          break multiple bones. Users in Santa Monica, in particular, have
          reportedly had many broken bones and the scooters have led people to
          be involved in a car accident due to being unable to stop at dangerous
          intersections. Not only are the brakes on a "Bird" scooter have been
          unreliable but the instantaneous acceleration is also an issue riders
          face. By-standers or pedestrians have undergone injuries due to
          collisions as well. If you have been a victim of a scooter collision
          the
          <strong> Los Angeles Scooter Injury Lawyers </strong>of Bisnar Chase
          are here to provide you with quality legal representation. Riders who
          have experienced a "Bird" scooter injury
          <strong> call 323-238-4683 </strong>for a{" "}
          <strong> free case analysis</strong>.
        </p>

        <p className="clearfix">
          <strong> Paralysis</strong>:
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/paralyzed-image.jpg"
            width="218"
            height="199"
            className="imgleft-fixed mb"
            alt="Electric scooter injury attorneys in Los Angeles"
          />
        </LazyLoad>
        <p>
          The loss of sensation or movement of all muscles or specific muscles
          are symptoms of being paralyzed. What causes a person to be paralyzed
          can range from having a genetic disorder or experiencing damage to the
          spinal cord. If a scooter rider endures spinal cord trauma or a "Bird"
          scooter death occurs from being hit by a vehicle and the cause was due
          to a malfunction in the scooter then more than likely liability for
          the accident will fall on the scooter manufacturer.
        </p>

        <h2>Accidents Involving Electric Scooters in Los Angeles</h2>
        <p>
          Brands such as "Bird" have become a staple in Los Angeles specifically
          in beach cities such as Santa Monica. Unfortunately, there have been
          many mishaps with the scooter itself and also many injuries.
        </p>
        <p>
          The{" "}
          <Link
            to="https://www.washingtonpost.com/national/sudden-appearance-of-electric-scooters-irks-santa-monica-officials/2018/02/10/205f6950-0b4f-11e8-95a5-c396801049ef_story.html?utm_term=.a61903eec9ff"
            target="_blank"
          >
            {" "}
            Washington Post
          </Link>
          reported that Santa Monica law enforcement took issue with the new
          self-transportation.
        </p>
        <p>
          “These scooters literally just began showing up on our streets last
          fall, the challenge is that they decided to launch first and figure it
          out later. People have been riding on these scooters for months
          without helmets, with young people on them, so there’s a sense of this
          is how they’re meant to be ridden,” stated Santa Monica deputy city
          manager and director of policy Anju Gupta.
        </p>
        <p>
          Just in a few months after the release of the "Bird" scooter the Santa
          Monica police department had made 97 citations and officers responded
          to over eight accidents.
        </p>
        <p>
          City officials have stated that their main concern is to ensure the
          safety of the scooter riders.
        </p>
        <p>
          Santa Monica Mayor Ted Winterer shared “Nothing would make me happier
          than to have them out there operating legally and safely with the
          appropriate permits. It’s just a matter of talking; talk helps a lot.”
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_88-A0hRJxg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p></p>
        <p>
          <span>
            Bird scooters have been appearing all over Southern California and
            have not only been infamous for their accessibility but also for
            their dangerous malfunctions. The Los Angeles scooter accident
            attorneys of Bisnar Chase will win you the compensation you need
            after a scooter accident.
          </span>
        </p>
        <h2>What Kind of Electric Scooter Accident Can I Claim?</h2>
        <p>
          Multiple factors can play into a scooter injury incident. Some of
          these elements can include potholes, uneven concrete or even a{" "}
          <Link to="/los-angeles/car-accidents">car crashing </Link> into the
          scooter. Most of the time though the key cause of a motor-scooter
          accident can rely on the design or on faulty parts.{" "}
        </p>
        <p>
          <strong>
            {" "}
            Who is responsible for your injuries and medical bills that follow
            though?{" "}
          </strong>
        </p>
        <p>
          It depends on how you were harmed in the accident. For example, if the
          scooter itself had faulty brakes than liability can rely on the
          manufacturer of the defective product. Manufacturers have what is
          known in legal terms as a duty of care. A personal injury victim will
          need to prove this key component in order to win compensation for a
          claim. This means that the defendant had a duty to keep its consumers
          safe. Another key aspect that needs to be proven in an accident claim
          is that the cause of the accident and injuries was due to the
          negligence of the defendant. This is known as "breach of duty".
        </p>
        <p>
          For more information <Link to="/los-angeles/contact">contact </Link>{" "}
          the personal injury attorneys of Bisnar Chase at{" "}
          <strong> 323-238-4683</strong> for a free case analysis.{" "}
        </p>
        <h2>Deaths at the Hands of Defective Scooters</h2>
        <p>
          Being injured in a scooter accident can not only leave a person with
          serious injuries but can also kill a rider.
        </p>
        <p>
          Twenty-four-year-old Jacoby Stoneking died due to falling off a
          scooter.{" "}
          <Link
            to="http://www.dailymail.co.uk/news/article-6131245/Aspiring-doctor-24-dies-freak-accident-fell-rented-electric-scooter.html"
            target="_blank"
          >
            {" "}
            Reports
          </Link>{" "}
          stated that Stoneking attempted to save his own life by calling a Lyft
          when he fell. Unfortunately when the Lyft driver arrived on scene
          Stoneking lost consciousness.
        </p>
        <p>
          The next day the aspiring doctor was pronounced dead at Baylor
          University Medical Hospital. Dallas Officials stated that when they
          were examining the location in which Stoneking was hurt they found a
          "Lime" scooter in pieces.
        </p>
        <p>
          The father of Jacoby Stoneking said that his son "...cared for people"
          and "was all about life."
        </p>
        <p>Our thoughts and prayers are with the Stoneking family.</p>
        <p>
          {" "}
          <Link to="/los-angeles/wrongful-death" target="_blank">
            {" "}
            Wrongful death
          </Link>{" "}
          incidents occur every day due to the negligence manufacturers. A
          family who has suffered a wrongful death is strongly advised to seek
          compensation and legal aid from an experienced scooter injury lawyer.
          Expenses that can be compensated include medical expenses and funeral
          costs.
        </p>
        <h2>Los Angeles Electric Scooter Safety Regulations</h2>
        <p>
          The abundance of scooter collisions had not only taken place in Los
          Angeles beach areas but also in downtown regions in Los Angeles as
          well. The L.A. Department of Transportation took the initiative to
          introduce a "one-year test program" to regulate the number of scooters
          and bikes that would be allowed in the city.
        </p>
        <p>
          The initiative would only allow a maximum of 2,500 self-transportation
          vehicles within city boundaries.
        </p>
        <p>
          University campuses had also taken safety measures to limit the usage
          of scooters in certain areas of the school. The{" "}
          <Link
            to="https://dailybruin.com/2018/02/20/ucpd-begins-to-enforce-traffic-rules-on-bird-scooter-riders/"
            target="_blank"
          >
            {" "}
            Daily Burn
          </Link>{" "}
          revealed that there has been numerous accounts of students riding
          scooters running into pedestrians.
        </p>
        <p>
          University Police Officer Kevin Dodd said that the regulations are set
          in place to bring awareness of the proper usage of scooters.
        </p>
        <p>
          "Students are dismissing all the rules and regulations they’re
          supposed to read before they ride them. We don’t want to cite. We want
          to get out here and educate," Dodd stated.
        </p>
        <p>
          Campus Police Officers had also enforced students to wear helmets when
          operating a scooter.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles scooter injury legal representation"
            style={{
              backgroundImage:
                "url('/images/personal-injury/scooter-text-header-image.jpg')"
            }}
          >
            <h2>Free Consultations for Los Angeles Accident Victims</h2>
          </div>
        </LazyLoad>
        <p>
          Riding a scooter should not be dangerous and the personal injury
          lawyers of Bisnar Chase believe that if that becomes the result,
          manufacturers should pay for the outcome.
        </p>
        <p>
          Our mission at Bisnar Chase is to provide you exceptional legal
          representation. We will be with you each step of the way to keep you
          aware of what is happening in your case.
          <strong> Since 1978</strong>, the{" "}
          <strong> Los Angeles Scooter Injury Lawyers</strong> of Bisnar Chase
          has compassionately been helping victims gain the compensation they
          deserve to get their lives back.
        </p>
        <p>
          When you call<strong> 323-238-4683</strong> you will automatically
          earn a <strong> free consultation</strong>.
        </p>
        <p>
          You do not have to face negligent manufacturers or a big insurance
          company alone.
        </p>
        <p>
          <strong>
            {" "}
            Contact the law offices of Bisnar Chase to receive legal advice for
            your scooter injury case
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Personal Injury Lawyers 6701 Center Drive West, 14th Fl.
          Los Angeles CA 90045 (323) 238-4683
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
