// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite",
  sidebarLinks: {
    title: "Los Angeles Dog Bite Information",
    links: [
      {
        linkName: "Dangerous Breeds",
        linkURL: "/dog-bites/dangerous-dog-breeds"
      },
      {
        linkName: "Factors Causing Canine Aggression",
        linkURL: "/dog-bites/factors-causing-canine-aggression"
      },
      {
        linkName: "First Aid For Dog Bites",
        linkURL: "/dog-bites/first-aid-dog-bites"
      },
      {
        linkName: "Injury & Infections from Dog Bites",
        linkURL: "/dog-bites/injury-infections-dog-bites"
      },
      {
        linkName: "Dog Bite Prevention",
        linkURL: "/dog-bites/prevention-tips"
      },
      {
        linkName: "Dog Bite Statistics",
        linkURL: "/dog-bites/statistics"
      },
      {
        linkName: "Victim's Rights",
        linkURL: "/dog-bites/victim-rights"
      },
      {
        linkName: "Why Dog's Bite",
        linkURL: "/dog-bites/why-dogs-bite"
      },
      {
        linkName: "California Dog Bite Laws",
        linkURL: "/dog-bites/california-dog-bite-laws"
      },
      {
        linkName: "Criminal Penalties",
        linkURL: "/dog-bites/criminal-penalties"
      },
      {
        linkName: "Insurance Breed Bans",
        linkURL: "/dog-bites/insurance-ban-breeds"
      },
      {
        linkName: "Los Angeles Home",
        linkURL: "/los-angeles"
      }
    ]
  }
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/german-shepard-dog-bite-los-angeles-banner-attorneys-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dog Bite Lawyers Los Angeles – Dog Attack Attorneys, CA"
        pageDescription="Experiencing an animal attack from a dog can be traumatizing, resulting in serious personal injury and even death. The Los Angeles Dog Bite Lawyers represent dog bite victims very often, and having won over $500 Million shows our attorney's dedication and skill. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Dog Bite Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles dog bite lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          California leads the nation in fatal dog bite injuries. Finding an
          experienced <strong> Los Angeles dog bite lawyer</strong> after a dog
          attack can make or break your case. In California, if you are bitten
          by a dog you have the right to pursue compensation against that
          person's home owner's insurance as long as you&nbsp;
          <strong> did not provoke the dog</strong>&nbsp;and were not on the dog
          owner's property without their permission.
        </p>

        <p>
          If you've been injured by a dog please call our offices to speak with
          an experienced&nbsp;{" "}
          <Link to="/los-angeles">
            <strong> personal injury lawyer</strong>
          </Link>
          &nbsp;free of charge. There is no cost unless we win your case.{" "}
          <strong> Call 323-238-4683. Free consultation</strong>.
        </p>
        <h2>Passionate Representation in Los Angeles Dog Bite Cases</h2>
        <p>
          At Bisnar Chase we have achieved a 96% success rate for over 12,000
          clients; we know how to win your case and get you the help you need to
          move past your traumatic experience with as little pain as possible.
        </p>
        <p>
          Our team of experienced Los Angeles dog bite attorneys have
          represented Los Angeles clients who have been bitten by an array of
          different canines; every dog attack reminds us how a seemingly
          innocent and friendly animal can turn into a frightening monster in a
          matter of seconds. In some cases, a reason for the attack is never
          identified and the dog had not had any previous record of being
          violent or dangerous. To learn more about the reasons why some dogs
          suddenly attack{" "}
          <Link
            to="/dog-bites/factors-causing-canine-aggression"
            target="_blank"
          >
            {" "}
            read this article on dog aggression
          </Link>
          .
        </p>
        <p>
          According to{" "}
          <Link
            to="http://www.dogsbite.org/dog-bite-statistics-fatalities-2016.php"
            target="_blank"
          >
            {" "}
            Dogbite.org
          </Link>
          , last year's statistics showed 31 fatalities from dog bites.
          Specifically, Pit bulls, make up about 6 percent of the total U.S. dog
          population and contributed to 71 percent, 22 of the 31 dog bite
          fatalities.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles dog bite attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dog-holding-newspaper-california-dog-bite-law-attorneys-lawyers.jpg')"
            }}
          >
            <h2>The Law Relating to Los Angeles, California Dog Bites</h2>
          </div>
        </LazyLoad>

        <p>
          Under{" "}
          <Link
            to="http://publichealth.lacounty.gov/vet/procs/civildog.htm"
            target="_blank"
          >
            {" "}
            California California's Civil Code Section 3342
          </Link>
          : "The owner of any dog is liable for the damages suffered by any
          person who is bitten by the dog while in a public place or lawfully in
          a private place, including the property of the owner of the dog,
          regardless of the former viciousness of the dog or the owner's
          knowledge of such viciousness." This means that the state of
          California "
          <Link
            to="http://www.nolo.com/legal-encyclopedia/dog-bite-statutes.html"
            target="_blank"
          >
            <strong>strict liability statute</strong>
          </Link>
          " dog owners are held responsible for the injuries and recovered
          damages caused to victims regardless of whether the attack occurs in a
          public or private place and whether or not the dog owner knew about
          prior instances when the dog had attacked. The only exceptions to such
          dog owner liability may be when a dog is provoked or attacked by a
          victim or when the victim was trespassing or in the process of
          committing a crime.
        </p>
        <h2>Seeking Dog Bite Damages in California</h2>
        <p>
          Dog bite victims often incur significant medical expenses. In many`
          cases, the cost of reconstructive surgery may not be covered because
          health insurance companies consider it to be "cosmetic" as opposed to
          a life saving procedure. Dog bite victims can claim damages including:
        </p>
        <ul>
          <li>Medical expenses</li>
          <li>Lost wages</li>
          <li>Cost of hospitalization</li>
          <li>Surgeries</li>
          <li>Rehabilitative treatments such as physical therapy</li>
          <li>Pain and suffering</li>
          <li>Cost of psychological counseling</li>
          <li>Emotional distress</li>
          <li>Permanent injuries and disabilities</li>
        </ul>
        <h2>Who Can Be Held Strictly Liable for a Dog Bite in Los Angeles?</h2>
        <p>
          Negligent dog owners can be held financially responsible for their
          actions in addition to facing citations or criminal charges. In
          addition to owners, caretakers of dogs can be held liable as well.
          However, they can only be held liable if they had prior knowledge of
          the dog's viciousness. Landlords can be held responsible in a dog bite
          case if the landlord knew that a tenant's dog was the dangerous and
          had the right to remove the dangerous dog from the property. Depending
          on the facts and circumstances of a case, there may be other
          potentially liable parties.
        </p>
        <p>
          <strong> In this video:</strong>&nbsp;Managing partner Brian Chase
          discusses who's at fault in a California dog bite case.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/LCaC1XWmRGU"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Los Angeles Dog Bites on the Rise</h2>
        <p>
          Our clients suffer from more than just physical injuries; the trauma
          associated with an unexpected attack by a dog can leave lifetime
          emotional scars. Some of our clients experience terror when being
          approached by a dog or even hearing a dog bark. Unfortunately, despite
          several organizations efforts to help prevent dog attacks, instances
          of&nbsp;dog bites are continuing to rise&nbsp;throughout the United
          States, particularly, Los Angeles. L.A. has one of the highest rates
          of injury when it comes to dog bites. Having a law firm who is
          familiar with LA courts and dog attack cases is recommended.
        </p>
        <h2>How Bad Does a Dog Bite Have to be to Have a Case?</h2>
        <p>
          There are many variations of dog bites. Some can be extremely damaging
          and cause severe injury or death, while other dog bites can leave a
          dog bite victim with severe mental trauma. A physical injury does not
          have to be present in order for a dog owner to be sued, but it helps
          having strong physical evidence, such as stitches, lacerations,
          bruises and medical expenses, rather than just stating severe mental
          trauma was inflicted.
        </p>
        <p>
          Mental trauma can be very serious as well and should never be
          downplayed. Being emotionally scarred by an incident with a dog or
          other animal can leave a victim with long-lasting and even permanent
          damage that can hinder their ability to live a normal life from that
          moment on.
        </p>
        <h2>Why Would I Need an Attorney in my Los Angeles Dog Bite Case?</h2>
        <p>
          Dog bite cases can be very involved and sensitive. Who was breaking
          what laws, does the dog have a history of aggressive behavior, was the
          dog bite victim given permission to be on the property or where they
          trespassing? Understanding the laws, rules and best course of action
          to take throughout the legal process can entirely benefit your case.
          Experienced attorneys spend countless hours acquiring the newest
          information and studying law to best defend you and your case. The
          ability to receive the best legal representation is always advised,
          especially in dog bite cases.
        </p>
        <h2>
          What Type of Compensation is Covered in a California Dog bite Claim?
        </h2>
        <p>
          Depending on the severity and type of dog bite case, your Los Angeles
          dog bite attorney can earn you compensation that can consist of the
          following:
        </p>
        <ul>
          <li>Medical cost</li>
          <li>Pain & suffering</li>
          <li>Loss of future income</li>
          <li>Loss of companionship</li>
        </ul>
        <h2>
          What Information do I Need to Gather When Preparing to Talk to a Dog
          Bite Attorney About My Case?
        </h2>
        <p>
          If there was a personal injury involved in the dog attack, having
          documentation of the injuries, photos, a detailed summary of the event
          including location, time and how the attack began through how it
          ended. Having witness information is also very important, including
          eye-witness accounts of the attack, who the witnesses are, where they
          were during the attack and their contact information. Any information
          regarding the attack including witnesses, medical costs, photos,
          documentation and any police reports are important to keep handy.
        </p>
        <h2>
          What Does Your Firm do to Stand Out From Other Firms When it Comes to
          Representing Dog Bite Clients in Los Angeles?
        </h2>
        <p>
          Los Angeles has the highest number of dog bites and attacks in the
          entire nation. Bisnar Chase will take on{" "}
          <Link to="/dog-bites">dog bite cases </Link> consisting of aggressive
          breeds, trespassing violations and uncommon situations when other
          firms will not. Bisnar Chase also does not back down from the high
          volume of dog bite cases because of our experience, skill and
          reputation in the courtroom. We have successfully settled and
          litigated hundreds of dog bite cases in Los Angles.
        </p>
        <h2>Dog Bite Statistics in Los Angeles</h2>
        <p>
          The U.S. Centers for Disease Control and Prevention (CDC){" "}
          <Link
            to="https://www.cdc.gov/features/dog-bite-prevention/index.html"
            target="_blank"
          >
            {" "}
            reports that about 4.5 million dog bites happen each year
          </Link>{" "}
          to people in the United States and 1 out of five bites will result in
          an infection. Children between the ages of 5 and 9 are most at risk
          for dog attacks. Nearly two-thirds of dog bite injuries among children
          ages 4 years and younger are to the head and neck region. Also,
          statistics show that injury rates in children are significantly higher
          for boys than for girls. A CDC study shows that Pit bulls and
          Rottweilers account for more than 75 percent of all fatal dog attacks.
          Clients looking to obtain a dog bite lawyer has increased over the
          last decade.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog bite trauma"
            style={{
              backgroundImage:
                "url('/images/text-header-images/emotional-trauma-from-dog-bite-attack-accident-attorneys.jpg')"
            }}
          >
            <h2>The Value of my Claim: Contents</h2>
          </div>
        </LazyLoad>
        <p>
          Experiencing a dog attack can be very traumatic, and can leave long
          lasting and even permanent PTSD (Post Traumatic Stress Disorder),
          night terrors (suddenly waking in your sleep from extreme fear) and
          even Cynophobia (phobia of dogs).
        </p>
        <h2>PTSD</h2>
        <p>
          PTSD or Post Traumatic Stress Disorder is experienced by many
          individuals around the world. Ranging from military,{" "}
          <Link to="/los-angeles/car-accidents">car accident victims </Link>,
          violent assault, dog bite victims and many others who have experienced
          a traumatic event which is difficult for them to overcome emotionally
          and mentally.
        </p>
        <p>
          PTSD is a disorder which is diagnosed in individuals experiencing
          symptoms for at least one month following a traumatic event, even
          though symptoms may not appear for several months or years later.
          Symptoms may include:
        </p>
        <ul>
          <li>
            Re-living the traumatic event through flashbacks and nightmares.
          </li>
          <li>
            Avoiding people, places and activities with a lack of emotional
            stimulation.
          </li>
          <li>
            Difficulty sleeping, concentrating, feeling on-edge and being easily
            agitated.
          </li>
        </ul>
        <p>
          Learn more about PTSD, treatments, counseling, therapy and get in
          touch with others who suffer from PTSD at{" "}
          <Link
            to="https://www.adaa.org/understanding-anxiety/posttraumatic-stress-disorder-ptsd/symptoms"
            target="new"
          >
            ADAA.org
          </Link>
          .
        </p>
        <h2>Signs of Cynophobia</h2>
        <p>
          Just the thought or seeing a dog can trigger fears and anxious
          feelings. Typical responses are usually both physical and
          psychological. Some signs of cynophobia include the following:
        </p>
        <ul>
          <li>Panic attacks just thinking of dogs</li>
          <li>Freezing when seeing or in the presence of a dog(s)</li>
          <li>Avoiding friends and family who have a dog(s)</li>
          <li>
            Avoiding places dogs could possibly be present (beach, parks,
            running paths, etc.)
          </li>
        </ul>
        <p>
          <strong> Dog Bite Victims</strong> who show signs of cynophobia and
          who have experienced severe emotional trauma many times experience the
          following:
        </p>
        <ul>
          <li>Depression</li>
          <li>Anxiety</li>
          <li>Social phobias</li>
          <li>
            Agoraphobia (fear of places and situations that might cause panic,
            helplessness or embarrassment)
          </li>
        </ul>
        <p>
          Many dog bite victims seek treatment at an emergency room and some
          others undergo surgeries to repair bite marks and scars. A dog attack
          can also prove costly for victims because of the medical expenses they
          incur. Many are unable to work and earn a livelihood as they recover
          physically and emotionally from the attack.
        </p>

        <h2>Dog Bite Injuries in Los Angeles</h2>
        <p>
          Dog attacks can cause serious injuries and&nbsp;wrongful death,
          depending on the nature of the attack. Dog bites can result in
          significant bodily injuries. Some of the common bodily injuries
          sustained in dog attacks include puncture wounds, nerve injuries, torn
          flesh, broken bones, laceration, tissue loss, and internal injuries.
          Individuals could be permanently scarred as a result of the bite
          marks. Amputations of limbs or fingers may also result from dog
          attacks.
        </p>
        <p>
          Dog bite victims may also suffer life-threatening infections from the
          dog attack. John Bisnar, specializes in dog bite cases in Los Angeles
          and says seeking medical attention is critical to making a full
          recovery --{" "}
          <strong>
            {" "}
            even if the victim initially doesn't think the bite is serious
          </strong>
          . In addition to physical injuries, dog bite victims suffer severe
          psychological issues such as post-traumatic stress disorder (PTSD),
          depression, anxiety, nightmares and fear of dogs.
        </p>
        <h2>Protecting Your Rights in California</h2>
        <p>
          If you have been bitten by a dog, it is important that you take the
          steps necessary to protect you own legal rights. First and foremost,
          it is important that you seek medical attention as soon as possible.
          Try to identify the dog that bit you so that the owner can be located.
          Report the attack to local law enforcement officials. Make sure you
          take photos of the injuries. Save evidence such as torn or bloody
          clothing.
        </p>
        <p>
          Approximately 57 million households in the United States have a dog,
          estimated at about 83 million dogs throughout the country. At an
          average of almost 4 million dog bites per year in the United States,
          you should know your rights if you were or happen to be attacked, and
          contact our highly skilled dog bite attorneys immediately. Read more
          about your{" "}
          <Link to="/dog-bites/victim-rights">rights as a dog bite victim</Link>
          .
        </p>
        <p>
          Last but not the least, it is critical that you contact a Los Angeles
          dog bite attorney with experience. They will help protect your rights
          and ensure that the at-fault parties are held accountable.
        </p>

        <p>
          A skilled and knowledgeable injury lawyer will work quickly to protect
          the evidence, locate all the parties involved and obtain a fair
          settlement in your case. It would be best not to give specific
          information to insurance companies about the incident.
        </p>

        <p>
          Bisnar Chase Personal Injury lawyers have been taking on these cases
          for over 39 years, in the process winning thousands of dollars in
          settlements and compensation for dog bite victims. We have the best
          skilled connections, the necessary resources to engage in expensive
          personal injury litigation, and a reputation that gives us the
          advantage in winning these types of cases.
        </p>

        <p>
          If you have been the victim of a dog bite or animal attack in Los
          Angeles, our Los Angeles dog bite lawyers at Bisnar Chase can help.
          Call&nbsp;<strong> 323-238-4683</strong>&nbsp;or fill out a request
          for a&nbsp;<strong> Free case evaluation</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
