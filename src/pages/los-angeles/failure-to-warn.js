// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/failure-to-warn-product-warning-labels-dangerous-products-los-angeles-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Failure to Warn Attorneys - Bisnar Chase"
        pageDescription="We trust the products we buy & share with our families & should never experience repercussions. When we do, the results can be life altering & catastrophic. The Los Angeles Failure to Warn Attorneys have over 40 years of defective product liability experience & more. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Failure to Warn Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles failure to warn attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The knowledgeable Los Angeles failure to warn lawyers at Bisnar Chase
          have a long and successful track record of helping those who have been
          severely injured as a result of poorly designed or manufactured
          products.
        </p>

        <p>
          Victims injured by defective products find themselves up against large
          corporations that are guarded by formidable legal defense teams. We
          put our knowledge, resources and skills to work for our clients so
          they have what it takes to stand up and fight against these large
          entities. If you have been injured by a defective product or wish to
          find out if you have a failure-to-warn lawsuit in Los Angeles, please{" "}
          <strong>
            {" "}
            contact us at 323-238-4683 for a free consultation and comprehensive
            case evaluation.
          </strong>
        </p>
        <p>
          Each day in the United States, millions of products are used by
          consumers under the assumption that they are safe and could cause no
          harm. Unfortunately, we know that this is not the case. In fact,
          millions of consumers are injured or even killed each year as a result
          of dangerous or defective products. A number of these products are
          dangerous because they are unsafe when used as they should have been
          used. A number of product liability lawsuits filed in our courts
          involve products that were faulty as a result of a manufacturing
          defect, design flaw or a Failure to Warn.
        </p>
        <h2>What is Failure to Warn?</h2>
        <p>
          When manufacturers of products fail to warn of the dangers associated
          with the goods they produce, it is known as "Failure to Warn." The
          labeling on the product and instructions to use a specific product
          must be clear and concise. Manufacturers must also post warnings about
          the dangers posed by the product. Some useful products, however, can
          present certain inherent dangers that cannot be eliminated without
          reducing the functionality of the product.
        </p>
        <p>
          For example, kitchen knives may be extremely sharp. But if the knives
          are not sharp, it they may not be useful in the kitchen any more.
          Product manufacturers have a duty to warn consumers of the risks
          associated with the use of a product. Manufacturers have this duty
          only if they knew or should have known about the risks and dangers
          presented by the product at the time it was made.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Product liability attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/duty-to-warn-children-child-hazard-premise-liability.jpg')"
            }}
          >
            <h2>Did the Manufacturer Have a Duty to Warn?</h2>
          </div>
        </LazyLoad>

        <p>
          The plaintiffs in such{" "}
          <Link to="/los-angeles/product-liability">
            product liability lawsuits
          </Link>{" "}
          must how that the manufacturers knew about the danger posed by the
          product and had a duty to warn consumers about such hazards.
          Plaintiffs must also show that the product manufacturers were
          negligent in their duty to warn, which caused the consumer to suffer
          injuries while using the product. It is important to ask the following
          questions:
        </p>
        <ul>
          <li>Did the product function as intended?</li>
          <li>
            Could the manufacturer of the product have foreseen or anticipated
            the dangers associated with its use?
          </li>
          <li>
            Would the warnings or instructions have prevented the accident?
          </li>
        </ul>
        <p>
          A company's biggest responsibility is to provide a safe product or
          service for their customers, regardless. When children are involved,
          extra precautions are always needed. Mothers and fathers put their
          trust in product safety, and when that trust is neglected, serious
          consequences can occur.
        </p>
        <h2>Examples of Failure to Warn</h2>
        <p>
          Failure-to-warn lawsuits often involve hazards that consumers could
          not have reasonably known without proper warning. Here are a few
          examples:
        </p>
        <ul>
          <li>
            Manufacturers of drugs must provide a list of potential side effects
            that consumers can expect to suffer as a result of using their
            product.
          </li>
          <li>
            Manufacturers of medical devices must warn consumers about the
            possible adverse effects they could suffer from as a result of using
            their products.
          </li>
          <li>
            Companies that make children's products such as furniture and toys
            must warn parents and caregivers about any potential hazards such as
            the danger of falling or choking.
          </li>
          <li>
            Manufacturers of household appliances should warn consumers about
            potential burn or electrocution hazards.
          </li>
          <li>
            Tobacco companies should warn consumers about the additive nature of
            their product and any potential adverse effects such as cancer and
            birth defects.
          </li>
          <li>
            Companies that make household or industrial cleaners should post
            warnings about how consumers or workers can protect themselves from
            toxic exposure.
          </li>
        </ul>
        <h2>When is a Warning Good Enough?</h2>
        <p>
          The law requires than an adequate product warning must include
          complete disclosure of the hazards that exist and the degree of risk
          or danger that is involved. The warning must also be posted visibly
          where a consumer can see it. Elements of an adequate or effective
          warning include cautionary words written in bold or large letters
          alerting the consumer such as "DANGER" or "WARNING." The warning
          should clearly identify the nature of the hazard.
        </p>
        <p>
          For example, does the product pose a risk of choking or burn injuries?
          A proper warning should also include details about what could go wrong
          if the warning is not obeyed. In addition, the warning should have
          information about how consumers can avoid a dangerous situation while
          using the products. Finally, the use of pictures showing how to use
          the product and how not to use it will also adequately alert
          consumers, emphasizing the hazards involved with the product.
        </p>
        <p>
          Another example are the dangers of talcum powder, and companies likes
          Johnson & Johnson who refuse to put warning labels on dangerous
          products, as explained in the following video:
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/81fmiEyrsRg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Compensation for Injured Consumers</h2>
        <p>
          Anyone injured by a defective product has the right to file a product
          liability claim against the negligent manufacturer for injuries,
          damage and losses suffered. For a failure-to-warn claim to be
          successful, injured consumers must prove that the defendant
          manufactured, sold and distributed the product; that the product had
          dangers or risks about which the defendant knew or should have known;
          that these known risks presented a danger to the consumer when the
          product is used in a reasonably foreseeable way; and that the
          manufacturer failed to warn consumers about the potential risks.
        </p>
        <p>
          Plaintiffs in such cases must also prove that he or she was harmed and
          that the lack of warning was a substantial factor in causing the
          victim's injuries.
        </p>
        <p>
          Our team of skilled lawyers at Bisnar Chase have over 39 years of
          experience and have established a 96% success rate. We have won over
          $500 Million for our clients and continue to provide a compassionate
        </p>
        <p>
          If you have experienced an injury in result of a product that was
          mislabeled, unlabeled or failed to give proper warning instructions,
          contact our team of
          <strong> Los Angeles Failure to Warn Attorneys</strong> at{" "}
          <strong> 323-238-4683</strong> and receive a{" "}
          <strong> Free Case Evaluation </strong> to see if your situation
          qualifies. You may be entitled to generous compensation.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
