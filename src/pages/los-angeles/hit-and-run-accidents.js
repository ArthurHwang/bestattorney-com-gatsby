// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/hit-and-run-los-angeles-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Hit and Run Victim Attorney - Bisnar Chase"
        pageDescription="In the event of a car accident, pedestrian accident or bicycle accident, hit and runs can leave a devastating impact on the situation & victims. The Los Angeles Hit & Run Lawyers of Bisnar Chase have over 40 years of experienced wins. Call our accident attorneys for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Hit-and-Run Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles hit and run lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong> experienced Los Angeles hit and run lawyers</strong> at
          Bisnar Chase are here to help those who have been seriously injured by
          hit-and-run drivers or families of victims who have been killed in
          such crashes. Our law firm has{" "}
          <Link to="/case-results">
            recovered hundreds of millions of dollars
          </Link>{" "}
          for more than 12,000 injured clients and their families since 1978. We
          have represented those who have been injured by drunk drivers and
          hit-and-run drivers.
        </p>
        <p>
          We are passionate in our pursuit for justice for our clients and are
          committed to helping injured victims secure fair and full compensation
          for all their losses. If you require assistance with your Los Angeles
          hit-and-run case,{" "}
          <strong>
            {" "}
            please contact us at (323) 238-4683 for a free and comprehensive
            consultation.
          </strong>
        </p>
        <h2>Hit and Runs are Very Common</h2>
        <p>
          Over 18 million people live in Los Angeles which is only 4,850sq mi.
          Many roadways do not have crosswalks, have poorly lit streets and a
          high amount of vehicles. Consider the following:
        </p>
        <ul>
          <li>
            <strong>
              {" "}
              Southern California has the highest percentages of hit and runs in
              the entire country
            </strong>
          </li>
          <li>
            <strong>
              {" "}
              Individuals convicted of felony hit and runs that injured someone
              face up to a year in prison, $10,000 fine and points on their DMV
              record
            </strong>
          </li>
          <li>
            <strong> 60 percent of hit and runs involve pedestrians</strong>
          </li>
          <li>
            <strong>
              {" "}
              1 in 5 of all pedestrian fatalities are hit-and-runs
            </strong>
          </li>
        </ul>
        {/* <p>
          Los Angeles has introduced a city-wide hit and run alert system, the
          following video by CBS News explains, published by the
          {" "}<Link
            to="https://www.latimes.com/local/lanow/82776433-157.html"
            target="new"
          >
            LA Times</Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://launch.newsinc.com/?type=VideoPlayer/Single&widgetId=1&trackingGroup=69016&playlistId=19132&siteSection=4011_trb.latimes_news_local_blog_lanow&videoId=28530932"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad> */}
        <h2>Top 5 Most Common Hit and Run Accidents</h2>
        <p>
          Hit and runs are more common than most people think. Here is a list of
          the top 5 most common types of hit and run accidents:
        </p>
        <ol>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/pedestrian-accidents">Pedestrian</Link>
            </strong>
            : Whether a person is crossing the street or a car jumps the curb
            striking pedestrians on the sidewalk, over 60% of hit and runs
            involve a pedestrian
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/bicycle-accidents">Bicycle</Link>
            </strong>
            : Bicyclist ranging from kids riding to school, to cyclist riding
            long distance in lane-sharing roads risk the chance of being struck
            by a vehicle. Many times these accidents involve the driver of the
            vehicle fleeing the scene.
          </li>
          <li>
            <strong> Parked Car Side Swipe</strong>: Many times drivers who
            slide up against parked cars on the side of the road feel safe
            leaving the scene in hopes that nobody was around to witness it
            happen
          </li>
          <li>
            <strong> Rear Ending</strong>: In rear ended crashes, the driver
            being rear-ended is many times caught off guard, injured or becomes
            unconscious. The rear-ender can easily flee as long as their car is
            still able to drive, if not, fleeing on foot is common as well. The
            rear-ended personal might get out to inspect the damages and
            confront the rear-ender to exchange information or call for help.
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/los-angeles/car-accidents">Car Accident</Link>
            </strong>
            : Same situations can occur in all types of car accident.
          </li>
        </ol>
        <p>
          Many times a person flees the scene of the accident is because of the
          following reasons:
        </p>
        <ul>
          <li>No insurance</li>
          <li>Warrants for their arrest</li>
          <li>Illegal immigrant</li>
          <li>Afraid of getting in trouble with the law</li>
          <li>Intoxicated by drugs and or alcohol</li>
          <li>Mental conditions</li>
          <li>Individual does not care</li>
          <li>Stolen vehicle</li>
          <li>Other criminal activity</li>
        </ul>
        <h2>What to Do after a Hit-and-Run Crash?</h2>
        <p>
          If you have been the victim of a hit-and-run collision in Los Angeles,
          there are a number of steps you can take to protect your rights. First
          and foremost, stay calm. Do not try to chase the driver. Look out for
          your safety first. However, if you were able to get the full or even
          partial license plate of the hit-and-run driver, write it down as soon
          as you can. If you are unable to move, wait for emergency personnel to
          arrive. If you are able to get to your cell phone call 911. Make sure
          your account of the incident gets into the police report. Make sure
          you give a factual, unbiased and detailed account of what occurred.
          Obtain a copy of the report.
        </p>
        <p>
          Take as many photographs as you can. Take photos of the accident
          scene, any debris from the suspect vehicle as well as your injuries.
          Preserve all evidence including torn or bloody clothing. If you were
          riding a bicycle, preserve it in its damaged condition. Do not repair
          it.
        </p>
        <p>
          Obtain contact information for anyone who may have witnessed the
          incident. Talk to area businesses to see if their surveillance cameras
          may have caught the incident on tape. Such evidence can prove
          invaluable in hit-and-run accidents. Make sure that you contact an
          experienced{" "}
          <Link to="/los-angeles" target="new">
            personal injury lawyer
          </Link>{" "}
          who will stay abreast of the official investigation and ensure that
          your legal rights and best interests are preserved.
        </p>
        <h2>The Dilemma of Hit-and-Run Victims</h2>
        <p>
          After a car accident, typically, fault is assigned and liability
          established. This enables the injured victim to seek compensation for
          damages such as medical expenses, lost wages, hospitalization, surgery
          and rehabilitation costs. However, in a hit-and-run accident, there is
          no one to hold accountable. The Los Angeles Police Department has an
          abysmal record when it comes to apprehending hit-and-run drivers. When
          hit-and-run drivers are found, they can be held both criminally and
          civilly liable. However, when a hit-and-run driver is not found, how
          do victims pay for medical expenses? How do they pay their everyday
          bills when they are not working and trying to recover from their
          injuries?
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="uninsured motorist"
            style={{
              backgroundImage:
                "url('/images/text-header-images/uninsured-motorist-covered-hit-and-run-accidents.jpg')"
            }}
          >
            <h2>Uninsured Motorist Coverage</h2>
          </div>
        </LazyLoad>
        <p>
          {" "}
          <Link to="/resources/uninsured-underinsured-motorist">
            Uninsured Motorist insurance
          </Link>{" "}
          is a clause in your auto insurance coverage that protects you if you
          are involved in an accident with someone who does not have liability
          insurance or lacks sufficient liability insurance coverage to pay for
          your damages. Uninsured Motorist insurance also protects you and your
          passengers in the case of a hit-and-run driver. Covered Uninsured
          Motorist insurance for you and your passengers include medical
          expenses, lost wages and pain and suffering.
        </p>
        <p>
          If you are a motorist in Los Angeles, it is imperative that you have
          Uninsured Motorist coverage. The odds of you getting injured in a
          hit-and-run accident or in an accident involving an uninsured or
          underinsured motorist are extremely high in Los Angeles. While
          liability insurance coverage protects others, Uninsured Motorist
          coverage protects you and your family.
        </p>
        <h2>Hit-and-Run Laws</h2>
        <p>
          Leaving the scene of a car accident is not only irresponsible and
          inhumane, but it is also illegal. California Vehicle Code 20001 (a)
          states: "The driver of a vehicle involved in an accident resulting in
          injury to a person, other than himself or herself, or in the death of
          a person shall immediately stop the vehicle at the scene of the
          accident."
        </p>
        <p>
          Motorists involved in injury collisions are required under the law to
          remain at the scene, exchange pertinent information with other parties
          involved and most importantly render aid to the injured victim. The
          actions of the motorist after a crash could determine whether or not
          the victim gets prompt medical attention and whether he or she
          survives. When the motorist speeds away from the scene, the victim is
          often left the middle of the roadway exposed to further danger.
        </p>
        <p>
          Often, we see that these victims are repeatedly run over by other
          vehicles who may not have seen the individual in the roadway.
          Remember, it is your duty to stop and remain at the scene after an
          accident - regardless of who is at fault.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
