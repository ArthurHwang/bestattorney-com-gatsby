// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/road-debris-accidents-lawyers-attorneys-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Road Debris Accident Attorneys, California"
        pageDescription="Driving a vehicle is dangerous enough, but when road debris in the roadway suddenly appears, car accidents can happen fast. Our skilled Los Angeles Accident Lawyers have over 40 years of representing & winning vehicle debris accident cases. Call our injury attorneys for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Vehicle Debris Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Vehicle Debris Accident Lawyers los angeles - Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Sudden and unexpected debris lying in the road can cause panic and
          erratic traffic, potentially causing serious injuries, property damage
          and fatalities, making vehicle debris accidents a highly dangerous and
          very common situation, and a potentially solid case for our team of{" "}
          <Link to="/los-angeles/car-accidents" target="new">
            Los Angeles Car Accident Lawyers
          </Link>
          .
        </p>
        <p>
          Drivers and riders of the road expect clear, open and safe roadways
          for travel and are dependent on their conditions. During the nighttime
          and daytime, vehicle debris lying in a lane, off the side of the curb,
          in an alleyway or even falling from above or below can cause a sudden
          need for braking, aggressive direction change and even acceleration.
        </p>
        <p>
          These unforeseen circumstances can cause horrific consequences for
          safe drivers, motorcycle riders, pedestrians, bicycle riders, animals
          and even property.
        </p>
        <p>
          It is advised you contact an experienced vehicle debris injury
          attorney for legal representation due to these cases involving
          complicated details and specific laws that can manipulate your chances
          of winning your case. If you currently have an attorney who is not
          confident with your case, contact our team of experienced vehicle and
          road debris car accident lawyers who never back down from a difficult
          case.
        </p>
        <p>
          If you have been injured in a road debris accident, call{" "}
          <strong> 323-238-4683</strong> for a{" "}
          <strong> free consultation </strong>with a Bisnar Chase lawyer, and
          see if you have a case.
        </p>
        <h2>What Types of Debris Qualify in a Case?</h2>
        <p>
          Roadway and vehicle debris can literally consist of anything blocking
          the road that shouldn't be. With the exception of living people
          blocking roadways such as in the situation of a riot or protest, the
          following can constitute as dangerous road debris that can cause a
          tragic event:
        </p>
        <ul>
          <li>Wood</li>
          <li>Metal</li>
          <li>Carpet</li>
          <li>Tile</li>
          <li>Glass</li>
          <li>Garbage</li>
          <li>Rocks</li>
          <li>Vehicle parts or vehicles including motorcycles</li>
          <li>Roadkill</li>
          <li>Trees, bushes and other greenery</li>
        </ul>
        <p>
          Vehicle debris accidents can usually be avoided if safe driving is
          practiced. Another important factor is making sure when loading up a
          car with items to be strategic and organized, making sure everything
          is secure and properly fastened down. Using common sense is a must and
          but many times the main reason for road debris and road hazards that
          cause car accidents.
        </p>
        <p>
          Every day due to natural causes, we experience road debris hazards.
          Many times a lawyer will deny a case involving natural cause related
          car accidents because they lack the confidence, skill and ability to
          win it for you. If your attorney rejects your case, Bisnar Chase will
          always be there for you.
        </p>
        <p>
          When dealing with a road debris car accident that is the result of a
          natural cause, such as a landslide of some sort, consisting of trees,
          greenery, rocks, dirt and other objects, many attorneys will turn the
          case down because they feel it is a risky case. Our skilled and
          experienced team of road debris accident attorneys know what it takes
          to win a case, and never back down from a difficult situation. To get
          a free quote to see if your case qualifies give us a call at{" "}
          <strong> 323-238-4683</strong> for immediate assistance.
        </p>
        <h2>Top 15 Types of Road Debris</h2>
        <p>
          Over 25,000 car accidents are caused by road debris, around 80 to 90
          tend to be fatal, according to a n{" "}
          <Link
            to="http://abc27.com/2016/08/14/200000-accidents-caused-by-road-debris-and-objects-flying-from-vehicles/"
            target="new"
          >
            {" "}
            ABC7 News
          </Link>{" "}
          report on a{" "}
          <Link
            to="https://www.aaafoundation.org/sites/default/files/Crashes.pdf"
            target="new"
          >
            {" "}
            AAA Foundation for Traffic Safety
          </Link>{" "}
          study published last year.
        </p>
        <p>
          The following list is the top 15 most common types of road debris that
          can cause accidents and injuries:
        </p>
        <ol>
          <li>Ladders</li>
          <li>Car Parts</li>
          <li>Tires</li>
          <li>Furniture</li>
          <li>Trees or branches</li>
          <li>Signs</li>
          <li>Metal Debris</li>
          <li>Railroad Ties</li>
          <li>Construction Barrels</li>
          <li>Hay Bales</li>
          <li>Light Poles</li>
          <li>Miscellaneous Trash</li>
          <li>TV's and stereo speakers</li>
          <li>Moving dollies</li>
          <li>Carpet</li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="What Are the Causes of Road Debris?"
            style={{
              backgroundImage:
                "url('/images/text-header-images/what-are-the-causes-of-road-debris.jpg')"
            }}
          >
            <h2>What Are the Causes of Road Debris?</h2>
          </div>
        </LazyLoad>
        <p>
          There is an endless amount of sources that road debris can result
          from.
        </p>
        <ul>
          <li>
            People loading their trucks with as much furniture and boxes as they
            physically can so they can make their move in one trip can cause a
            hazardous situation because items could come un-strapped and fall
            into the road while driving.
          </li>
          <li>
            Construction trucks overloading with sloppy and unorganized loading
            techniques poses a safety factor for not only unsuspecting drivers
            behind the truck, but while unloading the truck. Unloading
            improperly loaded materials can become a premise liability factor
            and lead to a personal injury and workers compensation case.
          </li>
          <li>
            A tree falling in a wind storm, large tumbleweeds and even dirt, mud
            and rock slides can block entire roads, cover personal property and
            can even cause serious injuries or fatalities.
          </li>
          <li>
            Accidents that causes auto parts and other random items to be
            dispersed over the road can cause drivers following to suddenly
            brake, swerve and potentially cause accidents, property and vehicle
            damage, injuries and fatalities.
          </li>
        </ul>
        <h2>5 Ways You Can Avoid Causing Road Debris</h2>
        <p>
          There are many ways you can practice safe driving while doing your
          part in maintaining a safe environment for the drivers behind you on
          the road. Here are a few tips to avoid being the cause of a
          catastrophic disaster:
        </p>
        <ol>
          <li>
            <strong> Take multiple trips if needed</strong>: If you are
            transferring materials that require a vehicle for transportation,
            don't try and play tetris and load every single item to ensure you
            get everything delivered on the first trip. Regardless of how snug
            and tightly you have packed everything, materials may come
            unstrapped and move around, causing them to potentially fall off of
            the vehicle and causing a dangerous road condition for the drivers
            behind you.
          </li>
          <li>
            <strong> Use appropriate restraints: </strong>It is recommended you
            use properly functioning tie-downs, locking straps and other types
            of undamaged restraints to keep your items and materials from
            falling off of your vehicle. Bungee-chords, tape and twine are risky
            assurances that will not guarantee the items from falling free onto
            the road.
          </li>
          <li>
            <strong> Top covers, nets and tarps: </strong>Even when your vehicle
            is not overflowing with materials, there is still a possibility of
            those items ending up on the road causing a road hazard for drivers
            behind you. Items such as carpet, cardboard boxes, plywood, drywall
            and typically flat surfaces that wind is able to get underneath and
            lift into the air can do exactly that. Using a properly attached
            tarp, net or top-covering material to ensure flying debris won't be
            a factor during your time transporting materials on the road.
          </li>
          <li>
            <strong> Make sure items are fixed in place: </strong>Especially
            items on wheels, sliders or that have moving parts like dressers,
            skateboards, bikes, desks and other types of furniture, sporting
            goods, etc.
          </li>
          <li>
            <strong> Use common sense: </strong>Millions of people transport
            items and materials safely from one play to another safely every
            hour of every day. While some individuals get lucky, others take the
            time to strategically load, appropriately strap down and cautiously
            drive their load to the desired destination. Don't put heavy items
            on top of light items, don't leave any items hanging over the side
            of your vehicle, remain unstrapped, or any other potential dangers
            for causing road debris.
          </li>
        </ol>
        <h2>The Reality of Road Debris Accidents</h2>
        <p>
          Road debris accidents, injuries and fatalities are not always caused
          by huge pieces of debris like heavy washing machines, couches, trees
          and so on.
        </p>
        <p>
          A 24-year-old was killed when a small piece of metal was kicked up and
          flew through his windshield, piercing his heart and killing him
          instantly.
        </p>
        <p>
          Robin Abel's daughter had every bone in her face broken and was nearly
          decapitated when a piece of material board flew out of a truck and
          came crushing through her windshield.
        </p>
        <p>
          Road debris played a role in over 200,000 car accidents reported from
          2011 to 2014, killing more than 500 people and injuring another
          39,000. According to AAA, its a 40% increase since 2001.
        </p>
        <p>
          {" "}
          <Link to="https://www.cbsnews.com/evening-news/" target="new">
            CBS Evening News
          </Link>{" "}
          published a segment on how devastating road debris accidents can be,
          and how life-changing they can be.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/FBEuXeny7pA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>How To Avoid Road Debris Accidents</h2>
        <p>
          It can happen in a fraction of a second, even when you think you are
          driving safe. Staying aware of your surroundings and what's ahead of
          you in the oncoming distance.
        </p>
        <p>
          Here are some helpful tips that could keep you and your family alive
          and safe.
        </p>
        <ul>
          <li>
            Don't tailgate. Give yourself enough room between you and the car in
            front of you. If the vehicle in front of you experiences a hazard
            and suddenly brakes, you will have more reaction time to brake,
            minimizing the chance of being rear-ended yourself.
          </li>
          <li>
            Avoid distractions while driving. Don't use electronic devises while
            driving or become obsessed with what channel you're listening to on
            the radio. If you are driving with a friend or family member, ask
            them to adjust the station or change the climate controls. If you
            are driving by yourself, try and find a comfortable climate setting
            and radio station you are happy with before putting your car in
            drive and driving off.
          </li>
          <li>
            Keep your eyes on the road ahead and all around. Staying aware of
            your surroundings can save your life. As daily drivers, we become
            very comfortable with driving because we do it so often. Being
            comfortable while driving is fine, as long as you don't become
            careless and clueless to the dangers around your car, and the danger
            you pose to others around you.
          </li>
        </ul>
        <h2>
          What Do You Do If You Have Been Injured in a Road Debris Accident?
        </h2>
        <p>
          Seek medical attention immediately following any type of accident. It
          is always a safe practice to make sure you are alright even if you
          feel fine.
        </p>
        <p>
          If you have experience injuries, medical attention will help with your
          current medical state and will also be an opportunity to document your
          injuries in relation to the accident. The sooner you can document your
          injuries the better, because if you have no documentation of your
          injuries, they never happened.
        </p>
        <p>
          Photos, videos, eye witness accounts and their contact information,
          documentation of your injuries and the place, time and how the
          accident happened are all essential information that will help your
          case win.
        </p>

        <h2>Why Choose Bisnar Chase to Represent You?</h2>
        <p>
          If you or someone you know has been injured or experienced damage due
          to roadway vehicle debris, contact our skilled team of{" "}
          <strong> Los Angeles Vehicle Debris Attorneys</strong> who have won
          over <strong> $500 Million</strong> for our clients. Our lawyers and
          staff have established a <strong> 96% success rate</strong>, have over{" "}
          <strong> 39 years of experience</strong> and know exactly what it
          takes to win your case. If we don't win, you don't pay.
        </p>
        <p>
          Call <strong> 323-238-4683</strong> to{" "}
          <strong> discuss your case for Free</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
