// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/if-you-are-injured-in-a-los-angeles-bus-accident-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Bus Accident Lawyer - Bisnar Chase"
        pageDescription="Shuttles, trolleys, charters and other means of public transportation can be dangerous ways of getting around. Our Los Angeles Bus Accident Attorneys have experience fighting for victims rights, demanding maximum compensation. Call our team of lawyers for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Bus Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles bus accident lawyers at Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced <strong> Los Angeles Bus Accident Attorneys </strong>
          at Bisnar Chase have successfully represented individuals who have
          been seriously injured in all types of auto accidents including bus
          accidents. We have also helped families that have lost loved ones
          obtain fair compensation for their significant losses.
        </p>
        <p>
          Please contact us for a
          <b>
            {" "}
            free, comprehensive and confidential consultation. Call (323)
            238-4683
          </b>
          .
        </p>
        <p>
          Thousands of Americans are seriously injured or killed each year as a
          result of bus accidents. Due to their size and weight, buses can be
          particularly dangerous when they are involved in crashes with smaller
          passenger vehicles.
        </p>
        <p>
          At the same time, when a large bus rolls over or is involved in a
          violent crash, its passengers may get ejected on to the roadway or
          suffer other types of blunt force trauma that may even prove fatal. If
          you or a loved one has been injured in a bus accident, an experienced{" "}
          <Link to="/los-angeles">Los Angeles accident injury lawyer </Link>{" "}
          will be able to better advise you regarding your legal rights and
          options.
        </p>
        <p>
          Traveling by bus is a cost effective and relatively safe way to get
          around Los Angeles. LA County bus crashes, however, can occur, and
          when they do, the results can be devastating. Many victims of Los
          Angeles bus accidents need immediate medical attention and have to
          miss work while they heal. To make matters worse, bus companies have a
          long history of refusing injured victims the support they need. It is
          important that negligent drivers and careless bus companies are held
          accountable for their actions.
        </p>
        <h2>If You Are in a Bus Accident in Los Angeles</h2>
        <p>
          If you are injured while riding a bus in Los Angeles, there are a
          number of steps that you can take to protect your best interests.
          First, you should make sure that the authorities have been notified
          about the crash. Remain at the site of the collision and try to
          determine if anyone needs assistance.
        </p>
        <p>
          As you wait for the police to arrive, write down everything you
          remember about the crash. Keep these questions in mind when making
          notes. Where were you seated? What were you doing at the time of the
          crash? When and where did the accident occur? What types of injuries
          did you sustain? Who were the people who witnessed the collision?
          These are all important questions to ask in order to gather critical
          information, says California bus accident lawyer John Bisnar.
        </p>
        <p>
          If you are able to move around without suffering additional harm,
          write down the contact information for the bus company and the bus
          driver. Photograph the bus, the damaged vehicles and where you were
          seated. When the police arrive, make sure that your name is on the
          police report. Last, but not least, seek medical attention right away.
          Your medical records can help you prove that you were injured in the
          crash.
        </p>
        <h2>Common Causes of Bus Accidents in LA</h2>
        <p>
          Recently there was a stream of California bus accidents involving tour
          bus companies. The Feds stepped in and cracked down on unsafe bus
          companies. Typically these charter buses are poorly maintained and put
          lives at risk. Some of the most common causes of Los Angeles bus
          crashes include:
        </p>
        <ul>
          <li>
            <b>Driver inexperience:</b> It is the responsibility of all bus
            companies to ensure that their drivers are skilled and experienced.
            Bus drivers who are new to the job should not be allowed to
            transport passengers on the congested and dangerous streets of Los
            Angeles.
          </li>
          <li>
            <b>Bus driver negligence:</b> It is crucial that bus drivers check
            for traffic before changing lanes or entering traffic. Bus drivers
            must obey the speed limit and slow down on the few days during the
            year when roads are wet in Los Angeles. They also must remain
            attentive to the road and not let themselves get distracted by their
            passengers or other electronic devices. It is illegal under
            California for all drivers to use the cell phone or text while
            driving.
          </li>
          <li>
            <b>Driver fatigue:</b> It is irresponsible to drive a bus full of
            passengers while drowsy. Drivers who push through fatigue are more
            likely to doze off, to lose focus and to miss important changes in
            the flow of traffic. All commercial drivers are required under
            federal law to comply with hours-of-service regulations that limit
            the number of hours bus drivers can drive at a stretch.
          </li>
          <li>
            <b>Faulty equipment:</b> Some Los Angeles bus accidents could have
            been prevented had the bus company performed proper safety
            inspections and repairs. When bus companies cut corners by failing
            to replace worn or broken parts, everyone on the roadway is put in
            harm's way. An uninspected bus or a bus where repairs have not been
            made may have over-inflated tires that could blow, brakes that are
            ready to fail and engines that can catch fire.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles bus accident lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/if-you-are-in-a-bus-accident-in-los-angeles.jpg')"
            }}
          >
            <h2>If You Are in a Bus Accident in Los Angeles</h2>
          </div>
        </LazyLoad>

        <h3>Getting Fair Compensation</h3>
        <p>
          You have the right to request compensation for all of the damages you
          have suffered if injured in a LA bus crash. The bus company or its
          insurance provider may claim that you were not hurt or that they are
          not responsible for your losses. Remember that you have rights.
          Whether it is through an insurance claim or an bus injury lawsuit,
          there are ways to ensure that they are responsible for your
          considerable losses. It's best to consult with a bus accident attorney
          familiar with Los Angeles, its roads, and its courts.
        </p>
        <h3>Driver Negligence</h3>
        <p>
          A number of bus accidents are caused by bus driver error or
          negligence. Bus drivers and bus companies have a legal obligation to
          safely transport their passengers. There are many ways in which a bus
          driver's negligence can cause a bus to crash. Some of the actions that
          can lead to a catastrophic bus accident include:
        </p>
        <ul>
          <li>
            Making an unsafe lane change. Buses are large vehicles that may have
            several blind spots. However, it is the bus operator's duty to
            ensure that it is safe to change lanes before doing so. Changing
            lanes without looking properly is one of the most common reasons for
            auto accidents.
          </li>
          <li>
            Exceeding the speed limit. Because of the size and weight of buses,
            they are often difficult to stop. Bus drivers who exceed the speed
            limit may not be able to adjust to the changing traffic conditions
            or to come to a stop in time to avoid a collision.
          </li>
          <li>
            Driving distracted. Under California law, school, transit and
            commercial bus drivers are not allowed to use a cell phone or text
            while driving. There are other forms of distracted driving that can
            prove dangerous as well. Any driver who takes his or her hands of
            the wheel, eyes off the road or mind off the task at hand is
            considered distracted.
          </li>
          <li>
            Failing to stop at a train track. All bus drivers should stop at
            train tracks to look and listen for approaching trains.
          </li>
          <li>
            Driving while fatigued. Operating a bus while drowsy can be as
            dangerous as driving under the influence of drugs or alcohol.
            Commercial drivers including bus drivers are required to get
            adequate rest before they start a shift.
          </li>
        </ul>
        <h3>Bus Company Negligence</h3>
        <p>
          Bus companies have a legal obligation to ensure that they hire the
          right people to operate their buses. They should also make sure that
          their bus drivers are properly trained and supervised. In addition,
          the responsibility of maintaining the buses also falls on the bus
          company. Companies should perform regular inspections on their
          vehicles while paying special attention to the tires and brakes to
          reduce the number of bus crashes annually.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bus Accident Prevention"
            style={{
              backgroundImage:
                "url('/images/text-header-images/accident-prevention-bus-accidents-los-angeles.jpg')"
            }}
          >
            <h2>Accident Prevention</h2>
          </div>
        </LazyLoad>

        <p>
          When you are a passenger on a bus, there is very little you can do to
          prevent an accident beyond making sure that you are not a distraction.
          Bus drivers can prevent accidents by consistently checking their
          mirrors, traveling at a safe speed and by keeping their eyes on the
          roadway at all times. Bicyclists, pedestrians and motorists can reduce
          their chances of being in a
          <strong>
            {" "}
            Los Angeles <Link to="/bus-accidents">bus accident </Link>
          </strong>{" "}
          by giving buses plenty of room.
        </p>
        <h2>Experienced Trusted Legal Help in Los Angeles</h2>
        <p>
          Bisnar Chase and our trial attorneys have over 40 years of experience
          and have won over $500 Million in verdicts and settlements. Our{" "}
          <Link to="/about-us/lawyer-reviews-ratings">ratings and reviews</Link>{" "}
          speak for themselves. We are passionate about our clients and will
          ensure you not only receive the compensation you are entitled to, but
          a first class client experience as well.{" "}
          <strong> Call 323-238-4683</strong> for a free consultation with a
          trusted
          <strong> Los Angeles Bus Accident Lawyer</strong>.
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
