// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/pasadena-personal-injury-attorneys-banner (1).jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Pasadena Personal Injury Lawyers - Bisnar Chase"
        pageDescription="From car accidents to premise Liabilities, the experienced accident attorneys of Bisnar Chase have been representing and winning cases for injury victims for over 40 years. Our Pasadena Personal Injury Attorneys have a 96% success rate and have won over $500 Million. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Pasadena Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Pasadena personal injury attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Los Angeles is an amazing location, full of adventure and excitement,
          but it is also densely populated which is why the experienced{" "}
          <strong> Pasadena Personal Injury Lawyers</strong> are here to make
          sure your back is covered. In a world full of negligence, accidents
          and unfortunate events, injuries are an everyday part of life and many
          times the result of wrong doing or negligence.
        </p>
        <p>
          If you have been involved in an accident or have experienced a
          personal injury, you may be entitled to generous compensation. Call
          our highly skilled team of Personal Injury Lawyers for a free
          consultation at <strong> 323-238-4683</strong>.
        </p>
        <h2>Don't Pay Unless We Win</h2>
        <p>
          Bisnar Chase offers an exclusive promise to every one of our clients,
          you don't pay anything until we win your case. We will also cover
          medical and other expenses throughout the case until your receive your
          settlement.
        </p>
        <p>
          It's just another way of showing our clients they can put their faith
          and confidence in our amazing team of{" "}
          <em>personal injury attorneys</em>.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Pasadena personal injury attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-los-angeles.jpg')"
            }}
          >
            <h2>Experience and Success</h2>
          </div>
        </LazyLoad>
        <p>
          The caring Pasadena personal injury attorneys at Bisnar Chase have
          established a strong reputation in the courts and have developed a{" "}
          <Link to="/case-results" target="new">
            96% success rate
          </Link>{" "}
          over the last <strong> 39 years, with over $500 Million </strong>in
          Wins for our clients. Bisnar Chase focuses on putting our clients
          first until we win your case.
        </p>
        <p>
          Our clients become a part of our family as we make sure all of your
          needs are met. We fight until maximum compensation is received and
          make sure you get the highest quality service and respect there is.
        </p>
        <h2>The Facts of Personal Injuries In Pasadena, CA</h2>
        <ul>
          <li>
            <strong> Pasadena ranks 19 out of 100</strong> on community safety
          </li>
          <li>
            Total <strong> population is 145,000 people</strong>
          </li>
          <li>
            Annually, there are approximately <strong> 4,200+ crimes</strong>{" "}
            commited, the majority of them <strong> violent</strong>
          </li>
          <li>
            Your chances of being a <strong> victim of a violent crime</strong>{" "}
            in <strong> Pasadena</strong> is <strong> 1 in 320</strong>
          </li>
        </ul>
        <p>
          Knowing the facts can open your eyes to the importance of knowing when
          and where to be in your community and staying safe.
        </p>
        <p>
          For more information on safety and violence statistics, visit{" "}
          <Link
            to="https://www.neighborhoodscout.com/ca/pasadena/crime"
            target="new"
          >
            {" "}
            www.neighborhoodscout.com/ca/pasadena/crime.
          </Link>{" "}
        </p>
        <h2>What Could My Case Be Worth?</h2>
        <p>
          Personal injuries vary from viscous dog bites to catastrophic car
          accidents. Bisnar Chase has a legal team that is ready to address any
          personal injury caused by another. We've had great success helping our
          clients win compensation they truly deserved.
        </p>
        <ul>
          <li>
            <strong> $16,444,904</strong> - Dangerous road condition, driver
            negligence
          </li>
          <li>
            <strong> $10,030,000</strong> - Premises negligence
          </li>
          <li>
            <strong> $9,800,000</strong> - Motor vehicle accident
          </li>
          <li>
            <strong> $8,500,000</strong> - Motor vehicle accident - wrongful
            death
          </li>
          <li>
            <strong> $8,250,000</strong> - Premises liability
          </li>
          <li>
            <strong> $7,998,073</strong> - Product liability - motor vehicle
            accident
          </li>
          <li>
            <strong> $5,000,000</strong> - Auto Defect
          </li>
          <li>
            <strong> $4,250,000</strong> - Product Liability
          </li>
        </ul>
        <h2>Why You May Need an Attorney</h2>
        <p>
          It's important to have an experienced lawyer on your side throughout a
          legal battle. Our team of skilled and knowledgeable attorneys know the
          ins and outs and understand what every client needs for his or her
          case.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/bigstock-Work-Injury-Compensation-Claim-150456701 copy.jpg"
            width="100%"
            alt="pasadena personal injury attorneys"
          />
        </LazyLoad>
        <p>
          Not only will having a Bisnar Chase lawyer working for you help you
          win your case, it will allow you to recover from your injury, reduce
          your stress and get your life back on track.
        </p>
        <h2>Waiting Too Long Can Lower Your Personal Injury Settlement</h2>
        <p>
          There is a{" "}
          <strong>
            {" "}
            <Link to="/resources/dont-rush-to-settle" target="new">
              Statute of Limitations{" "}
            </Link>
          </strong>{" "}
          for claims and cases, so don't keep yourself from a generous
          settlement you deserve.
        </p>
        <p>
          To discuss your case in a free consultation with an experienced team
          of personal injury attorneys, call <strong> 323-238-4683</strong>.
        </p>
        <h2>Passion. Trust. Results.</h2>
        <p>
          We are trial lawyers who don't quit when the case gets difficult. With
          decades of experience in and out of courtrooms, our trial lawyers have
          the expertise and resources to fight for you. Our reputation is strong
          and earns us priority in the community and legal system.
        </p>
        <p>
          Bisnar Chase has over 39 years of experience with an established 96%
          success rate. Choose the best personal injury lawyers and you will get
          the best legal representation there is.
        </p>
        <p>
          For immediate assistance call <strong> 323-238-4683</strong> to speak
          with our experienced team of attorneys today.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
