// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/los-angeles-personal-injury-attorneys-lawyers-main-index-page-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Los Angeles",
      population: 3884307,
      totalAccidents: 219238,
      intersection1: "Rodeo Rd & La Brea Ave",
      intersection1Accidents: 188,
      intersection1Injuries: 191,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 251.3,
      city1Name: "West Hollywood",
      city1Index: 326.0,
      city2Name: "Beverly Hills",
      city2Index: 188.0,
      city3Name: "Culver City",
      city3Index: 286.0,
      city4Name: "Burbank",
      city4Index: 148.1
    },
    locationBox3: {
      intersection2: "Sherman Way & Sepulveda Blvd",
      intersection2Accidents: 196,
      intersection2Injuries: 161,
      intersection2Deaths: 1,
      intersection3: "Victory Blvd & Sepulveda Blvd",
      intersection3Accidents: 140,
      intersection3Injuries: 139,
      intersection3Deaths: 1,
      intersection4: "Crenshaw Blvd & Adams Blvd",
      intersection4Accidents: 145,
      intersection4Injuries: 141,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Personal Injury Attorneys - Over $500 million recovered"
        pageDescription="Get the best legal representation in LA California by the top 1% attorneys in the Nation. Our Los Angeles Personal Injury Attorneys have won over $500 Million for our clients suffering bodily injury & property damage. Having over 40 years of experience, our law firm will win your case, or you don't pay. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Personal Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles personal injury lawyers - Bisnar Chase Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Our team of highly educated and
          <Link
            to="https://www.thenationaltriallawyers.org/profile-view/Brian/Chase/16192/"
            target="_blank"
          >
            <strong> top 100 rated</strong>
          </Link>{" "}
          <strong>
            Los Angeles <Link to="/">Personal Injury Lawyers </Link>
          </strong>
          will speak with you free of charge and can be contacted at
          <strong> 323-238-4683 </strong>for immediate assistance regarding your
          accident case. If you or a loved one has been injured at the fault of
          another, Bisnar Chase Personal injury Attorneys are here to help. Our
          office is located at 6701 Center Drive West, 14th Fl. in Los Angeles.
          You can schedule a time that is convenient for you, or we can even
          handle most matters by telephone if time is of the essence. We can
          also come to you for your car accident if you are not near us.
        </p>
        <p>
          <strong> Bisnar Chase</strong> represents people in Los Angeles who
          have been seriously injured in&nbsp;car accidents,&nbsp;dog
          bites,&nbsp;auto defects, truck accidents and other catastrophic or
          serious personal injuries. With over 40 years representing those who
          have suffered bodily harm, we fight for you with commitment, passion
          and results.{" "}
          <strong>
            We'll provide aggressive representation and get results or you won't
            pay.
          </strong>{" "}
          Brian has received hundreds of awards and honors including the{" "}
          <Link
            to="https://distinguishedjusticeadvocates.com/listing/attorney-brian-chase/"
            target="_blank"
          >
            Distinguished Justice Advocates
          </Link>{" "}
          award.
        </p>

        <h2>Pay Nothing If We Don't Win Your Accident Case</h2>
        <p>
          Our law firm promises to charge you nothing if we don't win, and we
          protect you throughout the case for financial liability. In other
          words, we front all the costs to run your case, and if in the end, we
          don't win, you won't pay. That's a personal promise our law practice
          has stood by since 1978. In addition, we advance all costs during your
          case. You will not be overwhelmed by medical fees and expenses related
          to the case while it is ongoing.
        </p>

        <h2>Why Hire Our Legal Team in Los Angeles, California</h2>
        <p>
          <strong> We are trial attorneys</strong>.{" "}
          <strong>
            {" "}
            Not all personal injury lawyers go to trial for their clients but we
            do
          </strong>
          . It's important to our team that we provide the best legal
          representation possible for our clients and handing your file off to
          another firm is not our idea of great representation. Our personal
          injury attorneys will be by your side throughout the entire process.
        </p>
        <p>
          In{" "}
          <Link
            to="https://study.com/academy/lesson/what-is-tort-law-definition-and-examples.html"
            target="_blank"
          >
            {" "}
            tort law
          </Link>
          , the plaintiff has suffered an injury or a loss. This is a direct
          result of someone else's negligence. Torts are a civil action, not
          criminal which means that the wrongdoer is charged financially not by
          incarceration. From depositions to discovery, Bisnar Chase will wade
          through the often confusing injury laws for you and allow you to get
          back to recovery.
        </p>
        <p>
          Founding Partner <Link to="/attorneys/john-bisnar">John Bisnar </Link>{" "}
          is the visionary and negotiator. He has successfully mediated
          thousands of cases resulting in millions of dollars a year in money to
          his clients. He teaches negotiation and interpersonal relationship
          skills and leads seminars in law office management for other personal
          injury attorneys. John knows how to get results and he's proven it
          again and again.
        </p>
        <p>
          Senior Partner <Link to="/attorneys/brian-chase">Brian Chase </Link>{" "}
          is the head litigator of the firm and leads the litigation team. Mr.
          Chase was the past President of the Consumer Attorneys of California
          (CAOC) and was named "Attorney of the Year". Mr. Chase is in several
          consumer oriented lawyer groups throughout the country.
        </p>
        <p>
          He is a regular speaker at conferences teaching litigation and trial
          techniques to others injury attorneys. Brian is a California Supreme
          Court and Appellate Court case winner and the 2012 VP of the State
          Trial Lawyers Association. Brian Chase gets results and wins big cases
          including catastrophic injuries and auto defect cases. Let his skill
          and knowledge represent you.
          <strong>
            {" "}
            When it comes to accident lawyers, Mr. Chase's reputation stands
            out.
          </strong>
        </p>
        <ul>
          <li>2019 Lawyer of the Year</li>
          <li>2017 Top 1% by the Natl. Assoc. of Distinguished Counsel</li>
          <li>2017 Top 100 High Stakes Litigator</li>
          <li>2016 Top 10% Lawyers of Distinction</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/types-of-cases-we-take-trail-lawyers-attorneys.jpg')"
            }}
          >
            <h2>Types of Personal Injury Cases We Represent in Los Angeles</h2>
          </div>
        </LazyLoad>

        <ul>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/bus-accidents">Bus Accidents </Link>
            </h3>
            {/* <p> */}
            <p>
              There are far more bus accidents in Los Angeles than any other
              metro area. We have been successfully winning bus accident cases
              for Los Angeles residents for close to 40 years. We have extensive
              trial experience and are familiar with the LA courts, defendants'
              counsel and judges. If you've suffered bodily injury from a bus
              accident contact our injury attorney lawyers today.
            </p>
            {/* </p> */}
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/car-accidents">Car Accidents </Link>
            </h3>
            <p>
              Every year millions of people die in accidents, especially car
              accidents. Our knowledge and experience in handling difficult car
              accident cases in Los Angeles is unmatched. Our top rated trial
              attorneys review and approve any car accident settlement whether
              it goes to trial or not. With decades of winning difficult motor
              vehicle accidents, Bisnar Chase has earned a reputation as one of
              the best car accident law firms in Los Angeles. Negligence and bad
              conduct are held accountable for their actions when you rely on
              the best attorneys with decades of experience.
            </p>
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/dog-bites">Dog Attacks </Link>
            </h3>
            <p>
              Dog bite cases are some of the toughest cases we see. The injuries
              are usually significant and often times involve children. The dog
              bite lawyers of Bisnar Chase have prosecuted thousands of injury
              cases, many from serious dog bites in Los Angeles. We advance all
              costs and shield you from liability until your case is won. We
              fight the legal system for you.
            </p>
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/road-debris-car-accidents">
                Dangerous Roadways{" "}
              </Link>
            </h3>
            <p>
              Debris, dangerous construction zones and poorly designed roadways
              are everyday life for many Angelenos. Los Angeles is one of the
              most congested areas, and dangerous roadways are often the cause
              of many accidents. Our law firm has extensive experience dealing
              with dangerous roadway cases causing bodily injury and property
              damage.
            </p>
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/truck-accidents">Truck Accidents </Link>{" "}
              (commercial and standard)
            </h3>
            <p>
              If you've been involved in a truck accident in Los Angeles,
              commercial or otherwise, we may be able to help you collect
              sufficient compensation for your injuries including time off work
              pay. Our attorneys will guide you through the process of dealing
              with a serious truck injury and help you onto the road to
              recovery. We pride ourselves on taking all of the stress of the
              accident off of you.
            </p>
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/product-liability">
                Defective Products{" "}
              </Link>{" "}
              (cars, appliances etc.)
            </h3>
            <p>
              Defective and dangerous products are a part of every day living.
              Each day thousands of defective or dangerous products can be
              recalled and pose a threat to consumers. Our team of LA trial
              lawyers have spent decades fighting against dangerous or defective
              products that put profit over people.
            </p>
          </li>
          <li>
            <h3 style={{ display: "inline" }}>
              <Link to="/los-angeles/pedestrian-accidents">
                Pedestrian Accidents{" "}
              </Link>
            </h3>
            <p>
              Pedestrian accidents in Los Angeles are some of the highest in the
              nation. Every day thousands of people throughout California are
              injured in a pedestrian accident, many of them in Los Angeles
              county. Our firm has the experience and drive to take on tough
              injury cases with great results. We have a reputation of being
              some of the fiercest litigators in California.{" "}
            </p>
          </li>
        </ul>
        <p>
          We've represented Los Angeles residents for over{" "}
          <strong> 40 years</strong> for a variety of personal injury cases.{" "}
          <strong> LA is the dog bite capital</strong>, has more pedestrian
          accidents than other California cities and with its high population,
          there is a need for experienced trustworthy legal help. Bisnar Chase
          Personal Injury Attorneys has strong relationships with the courts and
          insurance companies, and we work to get you the best compensation from
          your accident or injury case.
        </p>
        <p>
          To speak with a qualified and experienced Los Angeles personal injury
          lawyer, contact us at <strong> 323-238-4683</strong>. We'll provide
          you with a no obligation consultation about your case. The
          consultation is completely confidential.
        </p>
        <h2>Common Los Angeles Personal Injuries</h2>
        <p>
          There are a wide variety of injuries that accident victims can endure.
          Injuries can range from being minor to catastrophic and can even
          result in death. Below are the most common injuries experienced in the
          Los Angeles area.
        </p>
        <ul>
          <li>
            <strong> Brain injury</strong>: In one year over 2.8 million
            patients were admitted to the hospital due to a traumatic brain
            injury. Brain injuries can be an outcome of a slip and fall or more
            commonly a car accident. Symptoms of a brain injury can even present
            themselves as late as six months after the incident. Some signs of a
            traumatic an injury to the brain can be trouble remembering, mood
            swings or even constant migraines. It is strongly suggested by
            medical professionals that you visit a doctor to examine you after a
            car crash.
          </li>
          <li>
            <strong> Broken bones</strong>: Broken bones that an injury victim
            can sustain in an accident can be an arm fracture, a leg fracture or
            broken ribs. If right after the crash you feel like you can not move
            a body part without extreme pain then it is more than likely you
            have a fracture. Many people do not know that they have a broken
            bone after because they are still able to move, with little
            discomfort but they still have the ability to function. Indicators
            that you may have a broken bone can be swelling or bruising in one
            area of your body.
          </li>
          <li>
            <strong> Chipped teeth</strong>: When involved in a personal injury
            incident, you may think your teeth are the last body part to be
            effected. Loosing teeth or having chipped teeth after an accident is
            very typical occurrence though. The way in which accident victims
            can obtain injuries to their mouth is by hitting the steering wheel
            with great force or an airbag exploding in their face. In more
            serious cases teeth are not the only part of a mouth to be broken
            but a jaw can also be fractured as a result of a personal injury
            accident.
          </li>
        </ul>
        <h2>What is a Personal Injury?</h2>
        <p>
          A personal injury consists of an injury that is inflicted upon ones
          own body or mind and does not include damage to personal belongings.
          Injuries can range from minor scrapes and bruises to lacerations,
          fractures, poisoning, brain injuries, permanent disabilities and most
          other negative occurrences to your body in result of a sudden or
          prolonged situation. Personal injuries can result from accidents,
          negligence, wrong doing, criminal activity and many other situations.
          Depending on the amount of negligence involved, you will be
          compensated through a settlement or judgment, depending on if your
          case goes to trial.
        </p>
        <p>
          <strong>
            {" "}
            Typically the 4 elements of a personal injury claim are;
          </strong>
        </p>
        <ul>
          <li>Duty to act in a reasonable manner</li>
          <li>Breaching the duty</li>
          <li>The breach caused you injury or harm</li>
          <li>You suffered financially because of the breach</li>
        </ul>
        <h2>Why Do I Need a Los Angeles Personal Injury Lawyer?</h2>
        <p>
          Understanding what's at stake with your personal injury case is
          important when deciding whether to hire an attorney or not. Taking on
          a personal injury case on your own, only to hire a lawyer later in the
          case can complicate your case even more so. A personal injury attorney
          specializes in accident cases and understands your rights. You
          wouldn't hire a divorce lawyer for a bankruptcy so it's best to hire
          an attorney who focuses his sole practice on your injury. For example,
          a car accident lawyer in Los Angeles will understand the streets,
          intersections and dangers associated with the area. It's a worthwhile
          effort to seek out legal care specifically qualified in your practice
          area of injury.
        </p>
        <h2>Why You Should Hire a Los Angeles Accident Attorney</h2>
        <p>
          Attorneys are professionals who spend years and countless hours
          studying the specifics. Every day lawyers acquire a vast amount of
          knowledge, build crucial relationships in the law industry and learn
          strategies and techniques of negotiation, critical thinking and
          aggressive leadership skills to make sure they win their case.
          Understanding rules and laws in acute detail can be the deciding
          factor in the outcome of a case, and who better to do so than a
          devoted and experienced accident injury attorney dedicated to winning
          your case.
        </p>
        <p>
          At Bisnar Chase, our team of dedicated, skilled and experienced Los
          Angeles Personal Injury Attorneys have over{" "}
          <strong> 40 years of experience</strong>, have won over
          <strong> $500 Million</strong> for clients and have established a{" "}
          <strong> 96% success rate</strong>.
        </p>
        <h2>What If I Was Partially To Blame?</h2>
        <p>
          An attorney will protect your interests and if he or she is a great
          lawyer, win you the compensation you deserve. We fight to recover
          actual damages and are extremely detail oriented in re-couping every
          penny that this no-fault accident or injury has cost you. Even if you
          are found to be <strong> partially responsible</strong> for the
          accident we fight using{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Comparative_negligence"
            target="_blank"
          >
            {" "}
            <strong> comparative negligence</strong>
          </Link>{" "}
          to recover a portion for you. Rather than having a lawyer tell you
          that there is no chance, our team will fight to prove that the other
          party is to blame as well if warranted. There are important factors in
          accidents that have more than one at-fault party and a really good
          experienced trial lawyer is your best option to uncover these facts
          and win you compensation.
        </p>
        <p>
          In this video, founder of Bisnar Chase discusses why he became a
          personal injury attorney:
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/BhVPzkYsLuE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What Do I Need to Bring to My Initial Consultation?</h2>
        <p>
          Bring in your notations of the accident along with any photos or video
          footage of the accident you may have. Witness statements are also
          important for your attorney to review. Be sure to include any medical
          bills if you have them along with a copy of the police report. We will
          go over the details of the accident with you to uncover important
          factors that could increase the value of your claim.
        </p>
        <h2>What Are 5 Things I Should Do Following a Personal Injury?</h2>
        <ul>
          <li>Seek medical attention</li>
          <li>Take detailed notes</li>
          <li>Photos of your injuries</li>
          <li>Don't admit fault</li>
          <li>Contact a personal injury attorney</li>
        </ul>
        <h2>Finding Experienced Car Accident Lawyers in Los Angeles</h2>
        <p>
          The majority of cases we take in Los Angeles are car accidents. The
          congestion, construction and sheer number of vehicles on the road add
          up to thousands of car accidents a year. Some major, some minor. The
          difference is when it's major, you don't want to risk having a
          personal injury lawyer with limited experience dealing with LA car
          accidents. Bisnar Chase has earned a reputation as one of the most
          reputable and reliable law firms representing Los Angeles residents.
        </p>
        <h2>Legal Expertise and Unlimited Resources</h2>
        <p>
          The Los Angeles Personal Injury Attorneys at Bisnar Chase have
          successfully represented thousands of clients in Los Angeles and
          throughout California since 1978. Not only do we offer the initial
          case review free, but we front the costs until your case wins. If at
          the end we don't win your case, you pay nothing.
        </p>
        <ul>
          <li>
            We have an impressive string of{" "}
            <Link to="/case-results">
              <strong>multi-million dollar results</strong>
            </Link>
            &nbsp;for our seriously injured clients.
          </li>
          <li>
            Unique combination of legal expertise, financial resources and
            client commitment to handle each client's cause in a highly
            effective manner in order to get great results.
          </li>
          <li>
            Represent people who have been seriously injured or lost a family
            member in all types of accident and injury cases including personal
            injury, auto accidents, dog bites, product liability, defective
            products, roll-overs, defective seatbelts, motorcycle accidents,
            defective automobiles and wrongful death.
          </li>
        </ul>
        <h2>
          Aggressive Respected Legal Team of Los Angeles Personal Injury Lawyers
        </h2>
        <p>
          Our Los Angeles personal injury attorneys<strong> &nbsp;</strong>are a
          dedicated group of highly skilled professionals, focused solely on the
          representation of the seriously injured. They are aggressive,
          accomplished and respected by insurance companies, corporate
          wrongdoers, governmental agencies and their law firms. They guide
          their clients through the legal and financial challenges they
          encounter on the way to recovery from their injuries and these
          attorneys&nbsp;<strong> GET RESULTS</strong>.
        </p>
        <p>
          The partners of the law firm - attorneys John Bisnar and Brian Chase -
          were born, raised and educated in the Los Angeles area. They both
          graduated from one of the more prestigious law schools in the Los
          Angeles area, Pepperdine School of Law. The firm was founded in the
          Los Angeles area in 1978 and includes attorneys that are law school
          professors, legal issue lecturers, seminar leaders and litigation
          award winners.
        </p>
        <p>
          Brian Chase leads the Bisnar Chase litigation department. He has a
          string of stunning trial and appellate victories that are the envy of
          his peers, thus the recent awards and accolades.
        </p>
        <p>
          He is an active member of various professional organizations where he
          regularly lectures and teaches trial techniques and strategies. Among
          other professional organizations that include Mr. Chase on their rolls
          are the American Trial Lawyers Association, Trial Lawyers for Public
          Justice, Million Dollar Advocates Forum and the Western States Trial
          Lawyers Association.{" "}
        </p>
        <p>
          John Bisnar has lived his entire life (excluding active military duty)
          and raised his 3 children in the Los Angeles area. He was named
          "Community Hero" by The United Way in 1996. He was chosen in that same
          year to be an Olympic Torch carrier and carried the Olympic Flame
          through the streets of Southern California.
        </p>
        <p>
          He has spent countless hours serving the community that he loves in
          various official capacities, including the Board of Directors, of
          various Los Angeles area organizations. He is a certified workshop
          leader involved in the "Mankind Project - Los Angeles", www.mkp.org,
          an organization devoted to teaching interpersonal relationship skills.
        </p>
        <p>
          Mr. Bisnar is a member of the Los Angeles Trial Lawyers Association,{" "}
          <Link to="https://www.caoc.org/" target="_blank">
            <strong>Consumers Attorneys of California</strong>
          </Link>
          , American Trial Lawyers Association, Trial Lawyers for Public
          Justice, Million Dollar Advocates Forum among many other professional
          organizations. He has personally been involved in the recovery of $75
          Million through trial or settlement.
        </p>
        <h2>Reviews For our Los Angeles Personal Injury Attorneys</h2>
        <p>
          "Bisnar and Chase was without a doubt my best choice when T-boned
          while I was fully in the right of way. They are formed with an
          unbelievably knowledgeable team, all with your absolute best interest
          in mind and heart. They remained diligent with my case over it's
          entire duration of 3.5 years. I would recommend them to anyone.
          Fransica and Gavin, I can't thank you and your entire staff enough." -
          Ashley Swanson via{" "}
          <Link to="https://goo.gl/maps/gQ4kcu36jGD2" target="_blank">
            {" "}
            Google
          </Link>{" "}
        </p>
        <p>
          "I've worked with Bisnar Chase Personal Injury attorneys in Los Angles
          for close to 3 years. I am constantly impressed with the level of
          professionalism I receive from the entire staff. If we have clients in
          the LA area that need a reliable and results driven injury lawyer we
          would always recommend Bisnar Chase." - Anthony Flores via{" "}
          <Link to="https://goo.gl/maps/oV8LRVUYEgQ2" target="_blank">
            {" "}
            Google
          </Link>{" "}
        </p>
        <p>
          "I first contacted Bisnar Chase to request a donation for my
          organization's back to school giveaway. Kristi was amazing. Though she
          couldn't promise the 500 we requested she knew they would be able to
          donate at least 100 backpacks. I was then connected to Danielle, who
          organizes distribution and filling of backpacks and is as equally
          amazing. Needless to say, Bisnar Chase went ABOVE AND BEYOND! Today,
          we picked up our donation of 400+ backpacks filled with school
          supplies and hygiene kits. I am stunned by their amazing commitment to
          serve the community. I can't wait to see the faces of our parents and
          their children when they receive their new backpacks and school
          supplies! Thank you Bisnar Chase for all that you do!" - Kenicia Black
          via{" "}
          <Link to="https://goo.gl/maps/u8ueuBLhyGz" target="_blank">
            {" "}
            Google
          </Link>
          .
        </p>
        <h2>
          Communicate with Our Los Angeles Personal Injury Lawyers Immediately
        </h2>
        <p>
          Trusting Bisnar Chase is something we value deeply. Our number one
          priority is always protecting our clients. Our strong legal team will
          be there throughout the whole process to protect your best interest.
          If you've suffered a <strong> personal injury in Los Angeles</strong>{" "}
          please contact the law offices of Bisnar Chase today at&nbsp;
          <strong> 323-238-4683</strong>.{" "}
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
