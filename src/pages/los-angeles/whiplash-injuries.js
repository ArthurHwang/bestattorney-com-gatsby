// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"
import { HorizontalBar } from "react-chartjs-2"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/whiplash-injury-collision.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  const chartData = {
    labels: [
      "Registered Vehicles in U.S.",
      "Licensed Drivers in U.S.",
      "Total Crashes Per Year",
      "Annual Crash Deaths in U.S.",
      "Annual Deaths in California"
    ],
    datasets: [
      {
        label: "United States Crash Statistics",
        backgroundColor: [
          "#003f5c",
          "#58508d",
          "#bc5090",
          "#ff6361",
          "#ffa600"
        ],
        data: [264000, 218000, 63000, 35000, 3623]
      }
    ],
    options: {
      responsive: true,
      title: {
        display: true,
        text: "United States Crash Statistics",
        position: "top",
        fontStyle: "bold"
      },
      scales: {
        xAxes: [
          {
            display: true,
            type: "logarithmic"
          }
        ]
      },
      legend: {
        display: false,
        position: "bottom"
      }
    },

    borderColor: "#fff"
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Whiplash Injury Compensation Attorneys – Los Angeles Car Accidents"
        pageDescription="Whiplash injuries are the most common car accident injuries, resulting in serious pain and other symptoms. Trust Bisnar Chase whiplash injury lawyers to handle your case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Whiplash Injury Lawyers in Los Angeles</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="whiplash injury collision"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Everyone has heard of whiplash. After all, it is the most common form
          of car crash injury that you can suffer. Chances are high that you
          have either experienced it yourself or know someone else who has. Even
          a car accident at low speeds can leave a driver or passenger with a{" "}
          <b>whiplash injury</b>. Most people think of it as a minor injury, but
          whiplash can hit victims with wide-ranging symptoms and lasting
          effects.
        </p>
        <p>
          Read on to explore the causes, symptoms and treatments for whiplash,
          as well as the process of seeking whiplash injury compensation after a{" "}
          <Link to="/los-angeles/car-accidents" target="new">
            car accident in Los Angeles
          </Link>
          . The car accident lawyers of Bisnar Chase are here to help.
        </p>
        <div className="mb">
          {" "}
          <Link to="/los-angeles/whiplash-injuries#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What is Whiplash? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How Do People Get Whiplash? <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Whiplash Symptoms <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Whiplash Caused by Car Accidents <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              The Best Treatments for Whiplash <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Who is at Fault in a Rear-end Collision Causing Whiplash?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Should You Do After Suffering a Whiplash Injury?{" "}
              <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Bisnar Chase Can Help <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/los-angeles/whiplash-injuries#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              At-a-Glance - Whiplash FAQs <FaRegArrowAltCircleRight />
            </DefaultGreyButton>
          </Link>
        </div>
        <div id="header1"></div>
        <h2>What is Whiplash?</h2>
        <p>Whiplash is a form of neck strain injury.</p>
        <p>
          It is caused by a specific motion – when the head and neck snap back
          and forth quickly in a sharp and forceful movement.
        </p>
        <p>
          This back-and-forth movement forces the neck to accelerate rapidly,
          ‘whipping’ backward very suddenly, and then snapping forward again.
          The motion can put a huge amount of strain on the top of the spine, as
          well as the muscles, ligaments, and tendons in the neck. These tendons
          and ligaments may be pushed beyond their normal and comfortable range
          of motion, causing hyperextension and hyperflexion as the neck flexes
          too hard in each direction. This can result in strains and tears.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/whiplash-injury.jpg"
            width="100%"
            alt="whiplash injury"
          />
        </LazyLoad>
        <p>
          While whiplash is technically a form of neck strain, the extreme
          stress it puts on the body can produce a wide range of life-altering
          symptoms which extend far beyond neck pain.
        </p>
        <div id="header2"></div>
        <h2>How Do People Get Whiplash?</h2>
        <p>
          Whiplash pain can be caused by a wide variety of different actions –
          essentially anything which can cause your neck to whip back and forth
          as demonstrated above. These can include:
        </p>
        <ul>
          <li>
            <b>Car accidents –</b> Any collision which causes the head to jolt
            back.
          </li>
          <li>
            <b>Rollercoasters –</b> A particularly violent or{" "}
            <Link to="/los-angeles/amusement-park-accidents" target="new">
              high-speed amusement park ride{" "}
            </Link>{" "}
            can take a toll on the neck.
          </li>
          <li>
            <b>High-impact contact sports –</b> From the abuse suffered in
            boxing and martial arts, to a hard on-ice hockey hit or a thunderous
            collision on the football field – these are all accepted parts of
            the various sports, but can result in whiplash injuries.
          </li>
          <li>
            <b>Sporting or hobby-related accidents –</b> Unwanted tumbles,
            crashes and impacts, such as falling from a bicycle, being thrown
            from a horse, or being involved in a snowboard crash.
          </li>
          <li>
            <b>Physical abuse –</b> This can include anything from being hit in
            a fight to being shaken rigorously.
          </li>
          <li>
            <b>Miscellaneous falls and head injuries –</b> Any action which
            causes the neck to move violently, such as falling down the stairs
            or hitting your head on an object.
          </li>
        </ul>
        <div id="header3"></div>
        <h2>Whiplash Symptoms</h2>
        <p>
          If you have been involved in a car accident in LA – or any of the
          other activities above – you may have sustained a whiplash injury. But
          how can you identify the signs of whiplash and get the treatment you
          need?
        </p>
        <p>
          The main thing that any victim needs to know is that whiplash can hit
          you with a huge range of symptoms. Neck pain is just one of many
          potential side-effects which can extend far beyond simple aches and
          pains.
        </p>
        <p>
          Here are some of the key <b>whiplash symptoms</b> that can present
          themselves.
        </p>
        <h3>Physical Whiplash Symptoms</h3>
        <ul>
          <li>Neck pain which get worse with attempted movement</li>
          <li>
            Neck stiffness and deteriorating range of motion in the head and
            neck
          </li>
          <li>Feelings of tingling or numbness in your arms</li>
          <li>Pain spreading to the shoulders and back</li>
          <li>Feelings of fatigue and lethargy</li>
          <li>Severe headaches which start at the base of the skull</li>
          <li>Ears ringing</li>
          <li>Sight problems including blurred vision</li>
        </ul>
        <h3>Other Whiplash Symptoms</h3>
        <ul>
          <li>Feeling dizzy and disoriented</li>
          <li>Not being able to sleep</li>
          <li>Sudden short-term memory issues</li>
          <li>Being unusually bad-tempered and irritable for no reason</li>
          <li>Depression</li>
          <li>Being unable to focus or concentrate</li>
        </ul>{" "}
        <LazyLoad>
          <img
            src="/images/car-accidents/whiplash-symptoms2.jpg"
            width="22%"
            className="imgright-fixed"
            alt="whiplash symptoms"
          />
        </LazyLoad>
        <p>
          The presence of any of the symptoms listed – or a combination of
          multiple symptoms – could indicate that a person has a whiplash
          injury.
        </p>
        <p>
          Every case of whiplash is slightly different. Some people may
          experience no symptoms at all, while others might be hit hard by
          severe pain, headaches, and even depression. Symptoms might even be
          slightly delayed in occurring. The severity of a{" "}
          <b>whiplash injury</b> can depend on a range of factors, including how
          the injury occurred. If you were involved in a car accident in LA,
          your symptoms may depend on the nature of the collision and the force
          of the impact.
        </p>
        <p>
          The effects of whiplash might be more severe for older car accident
          victims. The severity of a whiplash injury might also be worse for
          those people who have already suffered similar injuries in their
          lives, as well as people who already have back or neck issues.
        </p>
        <div id="header4"></div>
        <h2>Whiplash Caused by Car Accidents</h2>
        <p>
          The single most common cause of whiplash is through car crashes. Of
          all the potential ways of being hit by another car, the type of crash
          most like to result in you suffering whiplash is being rear ended.
        </p>
        <p>
          When another car crashes into the back of your vehicle, this is
          referred to as a rear-end collision. This impact naturally forces the
          body into the exact motion which most commonly causes whiplash. As the
          other car collides with the rear of your vehicle, it will jolt your
          car forward. This will force your head and neck to snap back,
          potentially resulting in the hyperextension and hyperflexion
          associated with whiplash.
        </p>
        <p>
          <b>Whiplash injuries</b> can also occur through other types of car
          crashes. It is important to remember that any impact which causes your
          neck back into a sudden straining motion can result in whiplash
          injuries, no matter where the other car hits you. However, figures
          show that rear-end collisions are the biggest culprits, responsible
          for more than a quarter of all whiplash injuries.
        </p>
        <h3>How Common are Rear-End Car Accidents?</h3>
        <p>
          A huge number of car accidents happen every year in the United States,
          and{" "}
          <Link
            to="https://www.statista.com/topics/3708/road-accidents-in-the-us/"
            target="new"
          >
            the statistics
          </Link>{" "}
          are frightening. In 2015 there were more than 6.3 million road crashes
          which were reported to police, resulting in either property damage,
          injury, fatality, or a mixture – and that is just in the United
          States.
        </p>
        <LazyLoad>
          <HorizontalBar data={chartData} />
        </LazyLoad>
        <p style={{ marginTop: "2rem" }}>
          The statistics also show that being involved in an auto accident is
          the leading cause of death for people under the age of 55. Every year
          in the U.S. there are about 35,000 vehicle-related deaths. Of those
          recorded road deaths countrywide, California has the second highest{" "}
          <Link
            to="https://www.iihs.org/iihs/topics/t/general-statistics/fatalityfacts/state-by-state-overview"
            target="new"
          >
            {" "}
            fatality figures
          </Link>
          , with 3,623 deaths in 2016.
        </p>
        <p>
          While car accidents happen a lot, research by the National Highway
          Traffic Safety Administration (
          <Link to="https://www.nhtsa.gov/" target="new">
            NHTSA
          </Link>{" "}
          ) shows that rear-end collisions are the most common type of road
          accident, accounting for about 29% of all crashes. That means that
          there is an average of nearly 1.9 million rear-end collision accidents
          in the U.S. every year.
        </p>
        <p>
          These can be caused by anything from sudden braking, to driver error,
          poor weather conditions, and distracted driving.
        </p>
        <p>
          This makes whiplash the number one type of car accident injury in the
          United States, and one of the leading causes of car crash compensation
          claims in and around Los Angeles. Rear-end accidents happen especially
          regularly in and around Los Angeles, where drivers are always in a
          hurry and traffic is always heavy.
        </p>
        <p>
          Whiplash pain and associated conditions and injuries affect about 4
          million Americans annually, according to{" "}
          <Link
            to="https://www.sciencedaily.com/releases/2015/04/150401133234.htm"
            target="new"
          >
            {" "}
            research by Science Daily
          </Link>
          . The cost of tests, treatment, and rehabilitative care relating to
          these injuries? <b>A whopping $30 billion every year.</b>
        </p>
        <h3>Can a Low Speed Crash Cause Whiplash Injuries?</h3>
        <p>
          Yes! Even a crash at lower speeds can leave victims suffering from
          whiplash injuries.
        </p>
        <p>
          While a high velocity crash may result in more severe injuries,{" "}
          <Link to="https://www.ncbi.nlm.nih.gov/pubmed/9455663" target="new">
            studies have shown
          </Link>{" "}
          that even low speed crashes can result in whiplash. Researchers used
          live volunteers as crash victim test subjects to reveal the necessary
          speeds for these injuries to occur. They found that the approximate
          boundary is a range between 6.2mph and 9.3mph.{" "}
          <b>
            This means that any crash with a velocity of more than 10 miles per
            hour can cause whiplash.
          </b>
        </p>
        <div id="header5"> </div>
        <h2>The Best Treatments for Whiplash</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/whiplash-pain.jpg"
            width="50%"
            alt="whiplash pain"
            className="imgright-fluid"
          />
        </LazyLoad>
        <p>
          If you are showing symptoms of having a whiplash injury, you should
          visit your doctor. It is important to get a swift and accurate
          diagnosis to make sure any injury is not made worse.
        </p>
        <p>
          Whiplash treatments will depend on the severity of your injury.
          However, for the most part, whiplash injuries just need time to heal
          properly. These treatments can be used to aid recovery and reduce
          pain:
        </p>
        <ul>
          <li>Over the counter painkillers – for mild to medium injuries</li>
          <li>Prescription painkillers – for more severe cases</li>
          <li>Application of ice or heat to the neck area</li>
          <li>
            Foam collar – this should not be worn for long, as neck muscles can
            seize and cause more pain
          </li>
          <li>
            Rest – the injury often has to heal before you rehabilitate the neck
          </li>
          <li>
            Physical therapy - simple flexing and stretching of the neck to
            regain motion and loosen muscles
          </li>
          <li>Chiropractic care and massage therapy</li>
          <li>Alternative treatments, such as acupuncture</li>
        </ul>
        <p>
          <b>Suggested therapy exercises -</b> Exercises aimed at improving
          motion and reducing pain include: - Rotating your head slowly in both
          directions - Moving your head forward and backward in a nodding
          motion, as well as side to side Tip: Only perform exercises as
          recommended by a medical professional, and as pain will allow.
        </p>
        <h3>Recovery Time for Whiplash Injuries</h3>
        <p>
          Just like the suggested treatment method, the recovery time for
          whiplash injuries depend on their severity.
        </p>
        <p>
          <b>
            It can range from a few days, to weeks, months, or even years in
            extreme cases.
          </b>
        </p>
        <p>
          Studies have shown that the average recovery time ranges from 17 days
          to about four months, though some people may continue to suffer
          symptoms up to and beyond six months. It is possible for whiplash
          symptoms to turn into chronic conditions if they are not properly
          diagnosed and managed.
        </p>
        <div id="header6"></div>
        <h2>Who is at Fault in a Rear-end Collision Causing Whiplash?</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/whiplash-injury-compensation.jpg"
            width="100%"
            id="compensation"
            alt="whiplash injury compensation"
          />
        </LazyLoad>
        <p>
          In almost every case involving a rear-end collision, the driver of the
          car hitting the car in front will be partially liable, at the very
          least. In most cases, the car rear-ending a vehicle in front of it
          will be entirely culpable. This is because the following car has a
          responsibility to drive at a safe distance while being attentive and
          alert, leaving plenty of reaction time for sudden stops.
        </p>
        <p>
          In some cases, the driver of the leading car may also be considered
          partly culpable. For instance, if their brake lights are not working,
          or if they are not following the rules of the road.
        </p>
        <div id="header7"></div>
        <h2>What Should You Do After Suffering a Whiplash Injury?</h2>
        <p>
          It is concerning how common it can be for people to suffer a whiplash
          injury in a car accident in or around Los Angeles. But what should you
          do if you are involved in a crash that leaves you in pain from this
          form of neck damage?
        </p>
        <ul>
          <li>Call 9-1-1 and report the accident.</li>
          <li>
            Get medical attention as soon as possible – Allow paramedics to
            examine you at the scene, or seek medical attention that day if
            possible.
          </li>
          <li>
            Keep all medical records relating to the crash, as well as proof of
            your expenses.
          </li>
          <li>
            Chronicle the scene of the accident – Take pictures of the scene,
            including the damage to the cars. If your injuries are too severe to
            do so at the time of the crash, return to the scene later.
          </li>
          <li>Locate witnesses and secure statements.</li>
          <li>Contact an expert car accident attorney at Bisnar Chase.</li>
        </ul>
        <div id="header8"> </div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bisnar Chase Can Help"
            style={{
              backgroundImage:
                "url('/images/car-accidents/whiplash-injury-compensation-attorneys.jpg')"
            }}
          >
            <h2>Bisnar Chase Can Help</h2>
          </div>
        </LazyLoad>
        <p>
          The specialist car accident lawyers at Bisnar Chase can help victims
          of injuries suffered in car crashes in and around Los Angeles. Our{" "}
          <b>whiplash injury attorneys</b> are experts in fighting for their
          clients, seeking the best possible compensation settlements, or go to
          trial if necessary.
        </p>
        <p>
          <b>Whiplash compensation</b> payouts may depend on a wide range of
          factors, including the severity of the injury, medical bill
          expenditure, loss of wages, and much more.
        </p>
        <p>
          Bisnar Chase is dedicated to securing an outcome that accurately
          represents the client’s pain and suffering. We have been winning legal
          battles for our valued clients for 40 years, and have a 96% success
          rate, with{" "}
          <Link to="/case-results" target="new">
            more than $500 million won
          </Link>
          . Contact us now on <b>877-705-6556</b>.
        </p>
        <div id="header9"></div>
        <h2 id="header9">Whiplash FAQs</h2>
        <p>
          If you haven’t got time to read our in-depth whiplash rundown, skim
          the most frequently-asked whiplash questions here for quick
          at-a-glance answers.
        </p>
        <p>
          <b>Q: What is whiplash?</b>
        </p>
        <p>
          A neck strain injury – most commonly caused by rear-end car accidents.
        </p>
        <p>
          <b>Q: What are the symptoms of whiplash?</b>
        </p>
        <p>
          Neck pain, stiffness, limited movement, back and shoulder pain,
          headaches.
        </p>
        <p>
          <b>Q: What are some symptoms of severe whiplash?</b>
        </p>
        <p>Vision issues, fatigue, increased irritability, depression.</p>
        <p>
          <b>Q: How long will it last?</b>
        </p>
        <p>It varies. On average, between two weeks and four months.</p>
        <p>
          <b>Q: How should I treat my whiplash injury?</b>
        </p>
        <p>
          Visit a doctor. The most common treatments are pain medication, heat
          packs, light therapy exercises, and time.
        </p>
        <p>
          <b>Q: How do I know if I have a personal injury case?</b>
        </p>
        <p>
          Contact a whiplash injury lawyer at Bisnar Chase who can examine your
          case.
        </p>
        <p>
          <b>Q: How much are whiplash compensation payouts?</b>
        </p>
        <p>
          Every case is different and depends on circumstance. Trust the expert
          guidance of Bisnar Chase for the best possible result.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
