// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/parking-lot-accidents-los-angeles.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Parking Lot Attorneys - Parking Lot Accident Lawyers"
        pageDescription="Busy and congested car parks and parking structures have a high volume of traffic, fender benders, car accidents & injuries. The Los Angeles Parking Lot Accident Lawyers of Bisnar Chase have been winning cases for over 40 years. Have our skilled attorneys fight for you. Call for your Free consultation at 323-238-4683."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles Parking Lot Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Los Angeles Parking Lot Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong> Los Angeles Parking Lot Accident Lawyers </strong>at{" "}
          <strong> Bisnar Chase </strong> has you covered with our team of
          highly skilled and experienced team of attorneys ready to fight and
          win your case.
        </p>
        <p>
          The majority of people encounter all different types of situations in
          parking lots that can pose a serious risk to their health and safety.
          From finding a parking space, to walking from your parked car to the
          entrance of the store can be a traumatic experience.
        </p>
        <p>
          If you or a loved one has been injured in a{" "}
          <strong> parking lot accident</strong>, call our skilled team of
          highly experienced{" "}
          <Link to="/los-angeles" target="new">
            <strong> Los Angeles Personal Injury Attorneys</strong>
          </Link>
          <strong> </strong>for a <strong> Free Consultation </strong>at{" "}
          <strong> 323-238-4683</strong>.
        </p>
        <h2>Parking Lot Chaos</h2>
        <p>
          Parking lots can be very cramped, congested and chaotic, increasing
          the risk factor for:
        </p>
        <ul>
          <li>
            <strong> Car Accidents</strong>
          </li>
          <li>
            <strong> Pedestrian Accidents</strong>
          </li>
          <li>
            <strong> Hit and Runs</strong>
          </li>
          <li>
            <strong> Personal Injury</strong>
          </li>
          <li>
            <strong> Property Damage</strong>
          </li>
          <li>
            <strong> Wrongful Death</strong>
          </li>
        </ul>
        <p>
          Dangerous and potentially deadly situations can occur in milliseconds,
          especially in parking lots. With the close proximity, tight parking
          spaces and heavy pedestrian foot traffic, it was a miracle there was
          nobody crossing the street when the driver in the vehicle below
          careened out of control.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/4HisjdEj93Q"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Top 3 Most Common Parking Lot Accidents</h2>
        <p>
          Although we are familiar with parking the car in the nearest parking
          lot to go shopping for groceries, clothes or even at the dealership,
          there is always danger all around. Serious and fatal injuries are
          prevalent in parking lots because of the tight-packed spaces and
          powerful and heavy automobiles all cramped together. Mix in the rush
          hour traffic trying to grab some groceries after work on a weekday and
          you have chaos and danger painted all over it. The{" "}
          <strong> Top 5 Most Common Parking Lot Accidents</strong> consist of
          the following:
        </p>
        <ol>
          <li>
            <strong> Fender-Benders</strong>: This can consist of mild accidents
            resulting in two cars going for the same parking spot at the same
            time, to backing into a car while pulling out, side swiping a car by
            getting to close, or even drivers that are attempting to reverse or
            pull forward, when the car is still in the opposite direction.
          </li>
          <li>
            <strong> Pedestrian Accidents</strong>: Drivers running down
            pedestrians in the parking lot is very common. Shoppers leaving the
            store many times walkout into the crosswalk expecting traffic to
            stop immediately, but sometimes fail to do so and are struck by a
            vehicle.
          </li>
          <li>
            <strong> Hit-and-Runs</strong>: Drivers often times do not want to
            deal with the protocol of exchanging insurance and contact
            information and will flee the scene of the accidents.
          </li>
        </ol>
        <p>
          Watch this{" "}
          <Link
            to="https://www.cbsnews.com/news/parking-lot-accidents-distracted-drivers-national-safety-council/"
            target="new"
          >
            CBS News Video
          </Link>{" "}
          on
          <strong> Parking Lot Car Accidents</strong> and{" "}
          <strong> Pedestrian Accidents</strong>, and how{" "}
          <strong> Distracted Driving</strong> can lead to{" "}
          <strong> deadly consequences</strong>.
        </p>
        <h2>Facts You Didn't Know About Parking Lots</h2>
        <ul>
          <li>
            <strong> 1 out of 5</strong> car accidents take place in a parking
            lot
          </li>
          <li>
            <strong> 25% </strong>of all parking lot accidents are caused by
            vehicles backing up. It is advisable that you watch out for other
            drivers and pedestrians while doing so.
          </li>
          <li>
            Tragically, an average of over <strong> 500 people </strong>(drivers
            and pedestrians) were killed each year in parking lot accidents.
          </li>
          <li>
            {" "}
            <Link to="https://www.nhtsa.gov/" target="new">
              <strong> NHTSA</strong>{" "}
            </Link>{" "}
            estimates that <strong> 22% </strong>(more than one-fifth) of
            children between ages 5 and 9 killed in traffic crashes were
            pedestrians. Most of these accidents occurred because drivers failed
            to see kids while backing up their vehicles.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dangers of parking structures"
            style={{
              backgroundImage:
                "url('/images/text-header-images/parking-structure-danger-los-angeles.jpg')"
            }}
          >
            <h2>Dangers of Parking Structures</h2>
          </div>
        </LazyLoad>

        <p>
          Parking garages and structures pose risks of danger because of blind
          spots, limited visibility and the compact area.
        </p>
        <p>Other hazards include:</p>
        <ul>
          <li>Elevated levels</li>
          <li>Concrete beams</li>
          <li>Low-hanging ceilings</li>
          <li>Concrete poles and posts</li>
          <li>Steel cables</li>
        </ul>
        <p>There is also the risk of:</p>
        <ul>
          <li>
            Collapse; especially in Southern California and along the San
            Andreas fault
          </li>
          <li>The structure failing</li>
          <li>Premise liabilities</li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Los Angeles parking lot accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-los-angeles.jpg')"
            }}
          >
            <h2>Legal Representation That Cares</h2>
          </div>
        </LazyLoad>

        <p>
          For over <strong> 39 Years,</strong> the{" "}
          <strong> Los Angeles Parking Lot Attorneys</strong> at{" "}
          <strong> Bisnar Chase </strong>have been representing their clients
          with a<strong> compassionate understanding </strong>and{" "}
          <strong> professional foundation</strong>.
        </p>
        <p>
          Our team of highly skilled and experienced lawyers at{" "}
          <strong> Bisnar Chase</strong> have established a{" "}
          <strong> 96% success rate, </strong>winning over{" "}
          <strong> $500 Million </strong>for our clients.
        </p>
        <p>
          If you need a lawyer who won't back down from a difficult case, give
          us a call and receive a <strong> Free Case Evaluation. </strong>Our
          lawyers and staff are available to answer any questions you may have.
        </p>
        <p align="center">
          <strong> Call</strong> for your <strong> Free Consultation</strong> at{" "}
          <strong> 323-238-4683</strong>.
        </p>
        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
