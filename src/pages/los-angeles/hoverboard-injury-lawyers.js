// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "product-defect"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/hoverboard-injuries-accidents-los-angeles-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Los Angeles Hoverboard Injury Attorneys - Bisnar Chase"
        pageDescription="Personal transporters like segways, hovertrax & hoverboards are dangerous due to personal injuries, fire hazards & property damage. Call our Los Angeles Hoverboard Injury Attorneys for your Free consultation at 323-238-4683. The Injury Lawyers of Bisnar Chase will win you maximum compensation, or you do not pay."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Los Angeles HoverBoard Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="los angeles hoverboard injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The Los Angeles Hoverboard Lawyers of Bisnar Chase are investigating
          hoverboard injuries and explosions. If you or a loved one has been
          injured in a hoverboard-related incident, call us at
          <strong> (323) 238-4683</strong> for a free consultation and
          comprehensive case evaluation.
        </p>

        <p>
          Injury as the result of a defectively manufactured and designed
          hoverboard makes it important that you have a thorough understanding
          of your legal rights and options.
        </p>
        <p>
          The safety of hoverboards has come into question after scores of
          injuries and incidents where these devices have spontaneously exploded
          or caught fire, have been reported nationwide. The incidents have
          piled up to such an extent that the U.S. Consumer Product Safety
          Commission (CPSC) is investigating the cause of the hoverboard
          explosions and fires and the serious dangers they pose to consumers.
        </p>

        <div className="alert alert-info alert-dismissible" role="alert">
          <h2>Do You Have A Case?</h2>
          <p>
            Hoverboard manufacturers are obligated to make sure their products
            are safe and are considered negligent if their products are prone to
            explode and injure anyone nearby. However,
            <span>
              injuries that occur from simple interaction with a hoverboard such
              as falls or wipeouts
            </span>{" "}
            are not considered negligence on behalf of the manufacturer, and
            therefore <span>are not eligible for a lawsuit.</span>
          </p>
          <p align="center">
            Receive a <strong> Free Case Evaluation </strong>with a
            <strong>
              {" "}
              <Link to="/los-angeles" target="new">
                Los Angeles Personal Injury Lawyer
              </Link>
              . For immediate assistance Call (323) 238-4683.
            </strong>
          </p>
        </div>

        <h2>What is a Hoverboard?</h2>
        <p>
          {" "}
          <Link to="https://www.nbcnews.com/" target="new">
            NBC News
          </Link>{" "}
          Explains
          <strong>
            {" "}
            What is a Hoverboard, Why are Hoverboards Dangerous, and How
            Hoverboards are Causing House Fires and Injuries,{" "}
          </strong>
          in the video below.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9ArKRNWVcvI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Hoverboards became a buzzword after the movie "Back to the Future II"
          was released in 1989. In the film, the hoverboards were skateboards
          that actually hovered above the ground. But the hoverboards in
          question now are merely self-balancing, two-wheeled boards. They
          consist of two wheels arranged side by side with two small platforms
          between the wheels on which the rider stands. The device is basically
          controlled by the rider's feet, as he or she stands on built-in pads
          with gyroscopic sensors.
        </p>
        <h2>Hoverboard Explosions</h2>
        <p>
          Los Angeles saw its first recorded hoverboard explosion in late
          December 2015 when a man who was riding his hoverboard in the
          Koreatown district saw his wheels begin to smoke.{" "}
          <Link
            to="http://ktla.com/2015/12/30/caught-on-video-hoverboard-fire-on-koreatown-sidewalk-prompts-lafd-responds/"
            target="new"
          >
            The device eventually caught on fire and exploded
          </Link>
          . This fire happened just days before the devices were banned on
          Metrolink trains in Southern California. Several airlines have already
          banned the hoverboards as have college campuses in Los Angeles and
          California-wide. Many airlines have also banned hoverboards on their
          flights because of the potential fire hazard.
        </p>
        <p>
          {" "}
          <Link to="http://ktla.com/" target="new">
            KTLA 5 News
          </Link>{" "}
          video below of a <strong> Hoverboard Exploding </strong>on a public
          city sidewalk.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="//player.ooyala.com/static/v4/stable/4.14.8/skin-plugin/iframe.html?ec=BqNmt3eTpIfDz9rjPx3yh_3gCBbOGBpA&pbid=f987944e2b8d47c5ad7da7977780b8bd&pcode=9vOTQyOvfOKTDwM65FXm0S1biBeX"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          There are several reasons why these hoverboards may explode. Experts
          have already said that the one common reason why they do ignite is
          because the products themselves are poorly manufactured or the
          lithium-ion batteries that power these boards, are substandard. In a
          number of cases where the hoverboards exploded or caught fire, the
          batteries were overcharged.
        </p>
        <p>
          While many devices we use such as cell phones and laptops are also
          powered by lithium-ion batteries, they don't explode when overcharged
          because the quality of the batteries they use are well-regulated.
          That's not the case with hoverboard batteries, which are made by a
          number of factories in China. Their quality and consistency is simply
          not as good as top-tier manufacturers. A number of the lithium-ion
          batteries we see in cheap hoverboards are not knockoffs, but
          mass-manufactured cells that lack quality control.
        </p>
        <h2>Top 5 Most Common Hoverboard Injuries</h2>
        <p>
          In addition to explosions, hoverboards are also causing serious
          physical injuries to riders. In fact, emergency rooms around the
          nation have reported a spike in people who come in for treatment of
          hoverboard-related injuries. CPSC has reported receiving dozens of
          injury reports from emergency rooms nationwide. Some of the most
          commonly reported injuries involving hoverboards are concussions, bone
          fractures, contusions, abrasions and internal organ injuries.
        </p>
        <p>
          California has also passed a new law that became effective January 1,
          2016, which requires riders of hoverboards to be at least 16 years old
          to ride in public. All riders are required to wear helmets and can
          only ride on streets where the speed limit for vehicles is under 35
          mph. It is also illegal to ride hoverboards while under the influence
          of alcohol and/or drugs. Additionally, college campuses across the
          nation{" "}
          <Link
            to="/blog/us-colleges-want-hoverboards-off-their-campuses"
            target="_blank"
          >
            {" "}
            want hoverboards off their campuses
          </Link>
          .
        </p>
        <p>
          The Top 5 Most Common Hoverboard Injuries consist of the following:
        </p>
        <ol>
          <li>
            <strong> Fractured bones</strong>
          </li>
          <li>
            <strong> Pulled ligaments and tendons</strong>
          </li>
          <li>
            <strong> Traumatic head and brain injuries</strong>
          </li>
          <li>
            <strong> Lacerations</strong>
          </li>
          <li>
            <strong> Deep bruising</strong>
          </li>
        </ol>
        <h2>Common Hoverboard Accident Types</h2>
        <p>
          Many injuries were in result of rider error. Accident report
          occurrences consist of the following:
        </p>
        <ul>
          <li>
            Losing balance resulting in hoverboard slipping out from underneath
            the rider
          </li>
          <li>
            Unable to control speed resulting in crashing into parked cars,
            poles, stairs, etc.
          </li>
          <li>
            Getting fingers caught in wheel-well while performing maneuvers and
            or tricks
          </li>
          <li>Burns from hoverboards exploding and catching fire</li>
        </ul>
        <h2>Product Liability Issues</h2>
        <p>
          If you have been injured by a malfunctioning hoverboard, you may be
          able to recover compensation for damages by filing a product liability
          lawsuit. For such a claim to be successful, injured parties or
          plaintiffs must prove that they were injured while using the product
          as intended and that the product was defective. There are three types
          of product defects:
        </p>
        <ul type="disc">
          <li>
            Manufacturing defects that occur when the product are not made
            according to design or specifications or when mistakes occur during
            the manufacturing or assembly of the product.
          </li>
          <li>
            Design defects occur when the product's design makes the device
            inherently dangerous to use.
          </li>
          <li>
            Defective marketing or advertising is when the company fails to
            provide proper instructions to use the product or warnings for safe
            use of the device.
          </li>
        </ul>
        <h2>Compensation for Injured Victims</h2>
        <p>
          Victims of hoverboard injuries, whether they are caused by fires,
          explosions or falls, can seek compensation for damages including
          medical expenses, lost income, hospitalization, rehabilitation,
          permanent injuries, disabilities, pain and suffering and emotional
          distress.
        </p>
        <p>
          Injured victims would also be well advised to contact an experienced
          Los Angeles hoverboard injury lawyer who will thoroughly examine the
          product for manufacturing or design defects and flaws, and help hold
          the manufacturer accountable for victims' losses.
          <strong> Call (323)238-4683 for a free case evaluation.</strong>
        </p>

        <div className="mb" itemScope itemType="http://schema.org/Attorney">
          {/* <div className="gmap-addr"> 
            <div itemScope="" itemType="http://schema.org/Attorney">
              <div itemProp="name" className="gmap-title">
                Bisnar Chase Personal Injury Attorneys
              </div>
              <div
                itemProp="address"
                itemScope=""
                itemType="http://schema.org/PostalAddress"
              >
                <div itemProp="streetAddress">
                  6701 Center Drive West, 14th Fl.
                </div>
                <div>
                  <span itemProp="addressLocality">Los Angeles</span>{" "}
                  <span itemProp="addressRegion">CA</span>{" "}
                  <span itemProp="postalCode">90045</span>{" "}
                </div>
              </div>
              <div itemProp="telephone">(323) 238-4683</div>
            </div>
          </div>*/}
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>{" "}
          <Link
            itemProp="hasMap"
            to="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13234.129329151763!2d-118.393645!3d33.978858!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x535c138c7cf4bd0f!2sBisnar%20Chase%20Personal%20Injury%20Attorneys!5e0!3m2!1sen!2sus!4v1566846223309!5m2!1sen!2sus"
            target="_blank"
          >
            View Map
          </Link>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
