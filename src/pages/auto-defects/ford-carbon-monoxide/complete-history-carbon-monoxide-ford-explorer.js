// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "carbon-monoxide-complete-history-ford-explorer-auto-defect-attorneys-lawyers-bisanar-chase-personal-injury-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Ford Carbon Monoxide Poisoning in Vehicles - Auto defect"
        pageDescription="The complete history of the Ford Explorer carbon monoxide auto defect that has been injuring civilians and police officers across the country. Learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Complete History of Ford Carbon Monoxide Auto Defect</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="ford explorer police cruiser interceptor carbon monoxide"
            title="ford explorer police cruiser interceptor carbon monoxide"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Ford released their <strong>Ford</strong> <strong>Explorer</strong>{" "}
          model in 1990. Since then it has become one of the most popular
          mid-sized SUV's on the road.{" "}
        </p>
        <p>
          All in all, the Ford Explorer has been the quintessential vehicle for
          driving kids to school and soccer practice, a teenagers first car and
          the sports enthusiast.
        </p>
        <p>
          <strong>Police departments</strong> have made the Ford Explorer their
          choice vehicle for Sheriff's, California Highway Patrol, K9 Units and
          other specialty law enforcement.
        </p>
        <p>
          Until a flaw in the design of the Ford Explorer's{" "}
          <strong>exhaust system</strong> began allowing{" "}
          <strong>poisonous</strong> and <strong>harmful</strong>{" "}
          <strong>carbon</strong> <strong>monoxide</strong> <strong>gas</strong>{" "}
          <strong>fumes</strong> to seep into the occupants cabin.
        </p>
        <h2>Ford Explorers Begins to Silently Poison </h2>
        <p>
          In 2012 to 2017 model year Ford Explorers, a design flaw caused a
          chain reaction of injuries, complaints and reports of carbon monoxide
          gas fumes to seep inside the occupant's cabin, silently poisoning them
          and their families while they are innocently driving down the road.{" "}
        </p>

        <p>
          Drivers and riders inside the vehicle's occupant cabin were
          complaining about headaches, nausea and even loss of consciousness.
          Law enforcement, doctors and experts could not understand why so many
          people were experiencing such a high and consistent volume of similar
          symptoms.
        </p>
        <p>
          Ford has stated, that due to Ford Explorer police interceptor vehicles
          having emergency flashing lights, strobes and other after-market
          police equipment installed, the vehicles that were not properly sealed
          were reported having carbon monoxide issues. They immediately follow
          that statement by saying that is why normal civilian Ford Explorers
          have no problems with carbon monoxide at all.{" "}
        </p>
        <p>
          If you or a loved one has been subjected to and injured due to this{" "}
          <Link to="/auto-defects/ford-carbon-monoxide" target="new">
            Ford Explorer Carbon Monoxide Auto Defect
          </Link>
          , contact an <strong>auto defect attorney</strong> at{" "}
          <strong>Bisnar Chase</strong> and receive a{" "}
          <strong>Free Consultation</strong> and{" "}
          <strong>Case Evaluation</strong>.{" "}
        </p>
        <p>
          For immediate assistance, <strong>Call 800-561-4887</strong>.
        </p>
        <h2>
          Newport Beach Police Officer Narrowly Escapes Death by Carbon Monoxide
        </h2>

        <p>
          <LazyLoad>
            <img
              src="/images/text-header-images/McDowell.png"
              width="200"
              alt="brian mcdowell newport beach police officer carbon monoxide poisoning ford explorer"
              className="imgleft-fixed"
              style={{ marginBottom: "2rem" }}
            />
          </LazyLoad>
          <strong>Newport Beach police officer Brian McDowell</strong> was
          reporting to a call when he suddenly lost consciousness while driving
          his Ford Explorer police cruiser. His police cruiser veered to the
          left and hopping the center boulevard. As he careened through the
          lanes and narrowly missing oncoming traffic, he jumped the sidewalk
          and smashed head-on into a tree.
        </p>

        <p>
          Officer McDowell suffered a{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="new">
            traumatic brain injury
          </Link>{" "}
          and has scars from a fractured eye socket, dislocated shoulder and has
          no memory of the accident. McDowell had no drugs or alcohol in his
          system.
        </p>
        <p>
          <em>
            Below is an{" "}
            <Link
              to="https://abc7.com/former-oc-officer-files-lawsuit-against-ford-over-co-leak-in-patrol-cars/2274376/"
              target="new"
            >
              <strong>ABC 7 Eyewitness News</strong>
            </Link>{" "}
            video on Newport Beach Officer <strong>Brian McDowell</strong>'s
            experience with the{" "}
            <strong>Ford Explorer Carbon Monoxide Auto Defect</strong>
          </em>
        </p>

        <h2>Carbon Monoxide: The Silent Killer</h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="570"
            src="https://abc7.com/video/embed/?pid=2273610"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Being subjected to carbon monoxide for extended periods of time can{" "}
          <strong>result in severe injury and death</strong>, even when you
          don’t know it’s present.{" "}
        </p>
        <p>
          This leaking of carbon monoxide auto defect in the{" "}
          <strong>2012-2017</strong> Ford Explorers usually occurs when the
          vehicle's engine is running and the air conditioning is in circulation
          mode.{" "}
        </p>
        <p>
          It has also been reported when the vehicle is driving and while
          stopped or parked with the engine in idle or running in park.
        </p>
        <p>
          One of the most dangerous aspects of carbon monoxide is the fact you
          can not smell it. It is a poisonous and deadly gas that can saturate
          the air inside cars, houses, buildings and basically anywhere, and you
          would have no clue that you are slowly being poisoned.
        </p>
        <p>
          Below is a diagram that shows the signs, symptoms and possible dangers
          one can experience when subjected to <strong>carbon monoxide</strong>.
        </p>
        <LazyLoad>
          <img
            src="/images/carbon-monoxide-signs-symptoms-chart-injuries-attorneys-lawyers.jpg"
            alt="signs and symptoms of carbon monoxide poisoning ford explorer"
          />
        </LazyLoad>

        <p>
          According to the{" "}
          <strong>
            {" "}
            <Link to="https://www.nejm.org/" target="new">
              New England Journal of Medicine
            </Link>
          </strong>
          , Governmental air-quality limits for exposure to carbon monoxide are
          intended to keep carboxyhemoglobin levels in non-smokers below 3%.{" "}
        </p>
        <p>
          Carboxyhemoglobin levels of 3% or more can adversely affect high-risk
          groups such as the elderly, pregnant women, fetuses, infants and
          patients with cardiovascular or respiratory diseases.{" "}
        </p>
        <h2>
          Ford Explorer Carbon Monoxide Crash, Injury and Complaint Statistics
        </h2>
        <p>
          <strong>Ford Motor Company</strong>,{" "}
          <Link to="https://www.nhtsa.gov/" target="new">
            National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          and governmental entities have seen a surge of complaints and reports
          of heavy exhaust odors, lightheadedness, nausea and other factors that
          most likely are a result of the Ford Explorer carbon monoxide auto
          defect.
        </p>
        <ul>
          <li>
            So far there have been over <strong>2,700 complaints</strong> and
            warranty claims to Ford{" "}
          </li>
          <li>
            Another <strong>800 complaints </strong>to the government
          </li>
          <li>3 crashes</li>
          <li>41 injuries</li>
          <li>
            Over <strong>1.3 Million Ford Explorers recalled</strong>
          </li>
        </ul>
        <p>
          <strong>Accidental carbon monoxide poisoning</strong> is preventable,
          and that is exactly what police departments and law enforcement
          agencies are doing around the country after a second police officer
          reported carbon monoxide poisoning.
        </p>
        <h2>
          Second Police Officer in Texas Suffers Carbon Monoxide Poisoning from
          Police Cruiser
        </h2>

        <p>
          <LazyLoad>
            <img
              src="/images/text-header-images/LaHood (1).jpg"
              width="300"
              className="imgleft-fluid"
              alt="sergeant zachary lahood poisoned by carbon monoxide ford explorer austin police"
            />
          </LazyLoad>
          While many police departments have installed hundreds and hundreds of
          carbon monoxide detectors in Ford Explorer police interceptors and
          cruisers, the city of Austin, Texas has pulled all 446 Ford Explorers
          from its police fleet after detecting potentially dangerous levels of
          carbon monoxide in the vehicles over the past 5 months.
        </p>
        <p>
          62 workers' comp reports have been filed by officers for exposure to
          carbon monoxide, 20 of those officers had a measurable level of carbon
          monoxide in their system when they were tested, and three of them have
          still not returned to work.
        </p>

        <p>
          According to{" "}
          <Link
            to="https://kxan.com/2017/06/20/apd-officer-with-carbon-monoxide-poisoning-sues-ford-dealership/"
            target="new"
          >
            KXAN
          </Link>
          , <strong>Austin Police Sergeant Zachary LaHood</strong> is suing{" "}
          <strong>Ford Motor Company</strong> and{" "}
          <strong>Leif Johnson Ford</strong> stemming from a carbon monoxide
          poisoning incident in March.
        </p>
        <p>
          &ldquo;Essentially, I died from the inside out that night. I just
          suffocated from the inside out.&rdquo; LaHood said in a{" "}
          <strong>KXAN</strong> interview.
        </p>
        <h2>Another Officer Posioned While On Duty</h2>

        <p>
          <LazyLoad>
            <img
              src="/images/text-header-images/brandy-sickey-officer-ford-explorer-carbon-monoxide-poisoning-victim-auto-defect-attorneys-lawyers.jpg"
              width="315"
              className="imgleft-fixed"
              style={{ marginBottom: "2rem" }}
              alt="brandy-sickey-officer-ford-explorer-carbon-monoxide-poisoning-victim-auto-defect-attorneys-lawyers"
            />
          </LazyLoad>
          Recently, officer Brandy Sickey of Henderson, Louisiana, blacked out
          and suffered severe injuries due to carbon monoxide fumes seeping into
          the cabin of her Ford Explorer police interceptor.
        </p>

        <br />

        <p>
          Officer Sickey was treated for her injuries where doctors tested and
          confirmed she experienced carbon monoxide poisoning. In fact, doctors
          said the amount of carbon monoxide Sickey had in her blood was near
          lethal, and could have killed her.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/officer-brandy-sickey-injuries-carbon-monoxide-photos.jpg"
            width="100%"
            alt="officer-brandy-sickey-injuries-carbon-monoxide-photos.jpg"
          />
        </LazyLoad>

        <p>
          Officers McDowell, LaHood and Sickey are just 3 of many police
          officers now coming forward to tell their stories and experiences of
          being poisoned by carbon monoxide.{" "}
        </p>
        <p>
          Many of them, like these 3 officers experienced extreme circumstances,
          blacking out, crashing in motion and suffering prolonged and permanent
          injuries and damage.
        </p>
        <center>
          <p>
            <em>
              Images of officer Brandy Sickey courtesy of{" "}
              <Link
                to="https://www.cbsnews.com/news/ford-police-cruiser-carbon-monoxide-leaking/"
                target="new"
              >
                CBS News
              </Link>
            </em>
          </p>
        </center>

        <h2>Ford Explorer Carbon Monoxide Cases</h2>
        <p>
          <strong>Bisnar Chase </strong>is representing officer{" "}
          <strong>Brian McDowell</strong>, <strong>Zachary</strong>{" "}
          <strong>LaHood </strong> and a <ins>handful of other officers</ins>{" "}
          who have had similar experiences with the dangerous{" "}
          <strong>carbon monoxide poisoning </strong>in their cases against{" "}
          <strong>Ford </strong>and other affiliated entities.{" "}
        </p>
        <p>
          The traumatic experiences and permanent injuries they encountered
          while driving their <strong>Ford Explorer police interceptors</strong>{" "}
          on duty, will not be disregarded or unrecognized. Instead they will be
          brought to justice and stand as a symbol for the future safety of
          police officers and civilians who put their trust and faith in these
          vehicles.
        </p>
        <hr />

        <center>
          <LazyLoad>
            <img
              src="/images/text-header-images/family-ford-explorer-carbon-monoxide-poisoning-accident-auto-defect-attorneys-lawyers.jpg"
              width="100%"
              alt="family-ford-explorer-carbon-monoxide-poisoning-accident-auto-defect-attorneys-lawyers"
            />
          </LazyLoad>
        </center>

        <p>
          <strong>Bisnar Chase </strong> is{" "}
          <ins>
            currently representing dozens of these cases across the country
            regarding civilians
          </ins>
          . Mothers, fathers, children and loved ones have been injured due to
          the contamination and poisoning of carbon monoxide fumes inside these
          Ford Explorer family cars.{" "}
        </p>
        <p>
          These trusting and unsuspecting individuals were unknowingly risking
          their lives while driving to work, transporting young and vulnerable
          children to school, picking loved ones up from sports practice and
          going grocery shopping on Sundays after church.
        </p>
        <p>
          The skilled and dedicated{" "}
          <strong>Ford Explorer Carbon Monoxide Auto Defect Lawyers</strong> at{" "}
          <strong>Bisnar Chase</strong> are here to help those who have already
          been a victim to this deadly auto defect.{" "}
        </p>
        <p>
          You, your family and loved ones deserve the safety, peace of mind and
          satisfaction in knowing your vehicle is not trying to silently kill
          you. If an auto defect or recall becomes apparent, the auto maker or
          manufacturer should take safety over profit, and step up to fix and
          make sure you are back on the road safe and sound.
        </p>
        <p>
          Have more questions? Read through our{" "}
          <Link
            to="/auto-defects/ford-carbon-monoxide/ford-explorer-carbon-monoxide-faq"
            target="new"
          >
            Frequently Asked Questions
          </Link>{" "}
          about the <strong>Ford Explorer Carbon Monoxide Auto Defect</strong>,
          and Call our skilled <strong>attorneys</strong> who are{" "}
          <ins>currently representing these cases</ins> at{" "}
          <strong>800-561-4887</strong>.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Ford Escape Carbon Monoxide Poisoning"
            style={{
              backgroundImage:
                "url('/images/text-header-images/ford-escape-carbon-monoxide-poisoning-exhaust-leak-attorneys-lawyers-ford.jpg')"
            }}
          >
            <h2>
              Are Ford Escapes Endangering Lives with Carbon Monoxide Poisoning
              Too?
            </h2>
          </div>
        </LazyLoad>

        <p>
          There have been many reports and complaints about heavy odors of
          exhaust and gas fumes when accelerating and when in slow traffic in
          other Ford models, including the Ford Escape.
        </p>
        <p>
          Although carbon monoxide is an odorless gas, heavy exhaust fumes can
          be dangerous and still be saturated with carbon monoxide.
        </p>
        <p>
          Ford Escapes are popular vehicles just like the Ford Explorer,
          especially for families, sports enthusiasts and the common driver who
          still appreciates the space for transporting larger items that can not
          fit in the back seat of a sedan or coupe.
        </p>
        <p>
          Drivers of any car including Ford Explorers Ford Escapes are urged to
          contact a skilled<strong> Carbon Monoxide Auto Defect Lawyer </strong>
          if they have excessive amounts of exhaust fumes inside the occupant
          cabin while the engine is on.{" "}
        </p>
        <p>
          If you have signs of carbon monoxide poisoning, seek medical attention
          immediately. Getting medical attention is the best way to care for
          your health and safety, as well as documenting any injuries inflicted
          in result of the carbon monoxide auto defect in your vehicle. Having
          the ability to prove your injuries are the result of an auto defect is
          the best way to win your case.
        </p>

        <h2>Bisnar Chase is Here to Represent You</h2>

        <p>
          For over <strong>39 years</strong>, The skilled lawyers at{" "}
          <strong>Bisnar Chase </strong> have been representing clients,
          establishing a <strong>96% Success Rate</strong> and winning over{" "}
          <strong>$500 Million </strong>for their clients.
        </p>
        <p>
          Currently, <strong>Bisnar Chase </strong>is representing over a dozen
          civilians and several police officers across the country for the{" "}
          <strong>Ford Explorer Carbon Monoxide Auto Defect</strong>.{" "}
        </p>
        <p>
          Our attorneys prove their dedication, passion and professionalism,
          along with their skill, resources and knowledge is a winning combo.{" "}
        </p>
        <p>
          We can not allow large corporations like <strong>Ford </strong>to
          choose profit over you and your family's safety and well-being. They
          won't get away with it this time, because{" "}
          <strong>Bisnar Chase </strong>is fighting this fight.
        </p>
        <p>
          If you have been injured by a Ford carbon monoxide auto defect please
          contact our{" "}
          <Link to="/auto-defects/ford-carbon-monoxide" target="new">
            <strong>Ford Explorer Carbon Monoxide Auto Defect Attorneys</strong>
          </Link>
          .
        </p>
        <p>
          For immediate assistance, give us a
          <strong> Call at 800-561-4887.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
