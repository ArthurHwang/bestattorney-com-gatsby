// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/rollover-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Catastrophic Car Roof Crush Injury Attorneys - Auto Defects"
        pageDescription="Contact the Catastrophic Roof Crush Injury Attorneys at 800-561-4887 for a free consultation. Auto Defect Lawyers with a 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Vehicle Roof Crush Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Vehicle Roof Crush Injury Lawyers"
            title="Vehicle Roof Crush Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            <em>
              "Automobile and truck roofs that crush in on its passengers are
              responsible for more catastrophic injuries and deaths than any
              other type of <Link to="/auto-defects">automotive defect</Link>
               that we see in our practice" says attorney John Bisnar. "For less
              than $100 these roofs could have been strengthened to the point
              that most lives lost due to roof failures would have been saved."
            </em>
          </p>
          <p>
            The <strong>Roof Crush Injury Lawyers</strong> of Bisnar Chase have
            been settling and litigating vehicle roof crush injury cases for the
            past <strong>39 years</strong>. Being involved in a roof collapse
            accident can have serious repercussions. To take on large automotive
            corporations for justice is a battle between you, the defense and
            engineering experts; you need strength, experience and skill to win.
          </p>
          <p>
            {" "}
            Bisnar Chase attorneys balance the scales, pursue your rights and
            hold wrongdoers accountable. We have won over{" "}
            <strong>$500 Million dollars</strong> for our clients and believe
            that holding irresponsible corporations accountable for the harm
            they cause is a civic responsibility to the safety of the motoring
            public.
          </p>
          <p>
            If you or someone you know has been involved in a roof crush car
            accident involving serious injuries, contacting a personal injury
            lawyer is one of the best decisions you could make. When you contact
            the attorneys of Bisnar Chase, you will receive a{" "}
            <strong>
              free consultation &amp; a promise to shield you from liability
              during your case
            </strong>
            . Call <strong>800-561-4887</strong> and we will start fighting for
            you today.
          </p>

          <h2>How Common Are Car Roof Crush Injuries?</h2>
          <p>
            The most common of roof crush injuries occurs in car accidents,
            specifically in rollover incidents. A{" "}
            <Link to="https://en.wikipedia.org/wiki/Rollover" target="_blank">
              rollover accident
            </Link>{" "}
            takes place when a vehicle is knocked over onto the side or on the
            roof of a car.
          </p>
          <p>
            {" "}
            <Link to="https://www.nhtsa.gov/" target="_blank">
              The National Highway Traffic Safety Administration{" "}
            </Link>
            (NHTSA) reported that an estimated 11,000 fatalities have come from
            vehicle roof rollover accidents. These types of roof crush accidents
            are due to a number of factors that can range from the weather to
            poor driving.
          </p>
          <p>
            <strong>6 Reasons Why Rollover Accidents Occur:</strong>
          </p>

          <li>Type of vehicle</li>
          <li>Under the influence of a substance</li>
          <li>Auto defect in design</li>
          <li>Speeding</li>
          <li>Weather conditions such as heavy rain</li>
          <li>Poor road conditions</li>
        </div>
        <p>
          In order to prevent roof crush injuries, 35 years ago, NHTSA adopted{" "}
          <Link
            to="https://icsw.nhtsa.gov/cars/rules/import/FMVSS/"
            target="_blank"
          >
            Rule 216
          </Link>{" "}
          of the Federal Motor Vehicle Safety Standard.
        </p>
        <p>
          In order to prevent roof crush injuries, the NHTSA adopted Rule 216 of
          the Federal Motor Vehicle Safety Standard. NHTSA officials suggest{" "}
          <Link to="/auto-defects/roof-crush/new-car-roof-standards">
            updating the regulation
          </Link>{" "}
          with today's technology advancements, requiring roofs to withstand
          direct pressure of 2.5 times the vehicle weight.{" "}
        </p>
        <p>
          This rule presents a major loophole for SUVs weighing over 5,000
          pounds. Vehicles over 5,000 pounds are not affected by government
          regulation regarding their roofs. Automakers are free to design and
          build this class of vehicle without the safety standards normally
          associated with passenger vehicles<em>.</em>{" "}
        </p>
        <p>
          It is the judgments and settlements wrongdoers have to pay that punish
          shortsighted companies who favor profit over lives and encourages them
          to prevent future damages by quashing irresponsible advertising,
          design and manufacturing. Bisnar Chase has held companies including
          General Motors, Toyota, Nissan, Chrysler and Ford accountable and
          forced to them compensate victims who purchased SUVs and other
          vehicles that caused unnecessary deaths and injuries.
        </p>
        <p>
          Bisnar Chase continues to fight for clients whose lives will never be
          restored through money alone, by holding the auto industry accountable
          for product liability encouraging vehicle design safety.
        </p>
        <h2> Why You Need a Roof Crush Injury Lawyer</h2>
        <p>
          Drivers who suffer a catastrophic injury such as{" "}
          <Link to="/head-injury/tbi" target="_blank">
            traumatic brain injuries
          </Link>{" "}
          or back problems in a roof related accident can have lifelong affects
          often requiring long-term medical care. Hiring a personal injury
          attorney will not only help you gain compensation for damages but it
          can also improve the safety regulations of a company.
        </p>
        <p>
          <strong>
            Reasons why you need to hire a roof crush injury lawyer
          </strong>
        </p>
        <li>
          <strong>Improved safety standards may result from the lawsuit</strong>
          : Sometimes certain procedures or regulations are not set in place to
          protect the driver. By proceeding with a roof crush accident case
          other drivers can benefit by having a better standard of safety put in
          place.
        </li>
        <li>
          <strong>Damages and losses deserve to be covered</strong>: Lost wages
          and physical therapy can become increasingly problematic for a roof
          injury victim over time. By hiring an experienced roof crush injury
          attorney you can receive the compensation you rightfully deserve
          including medical bills.
        </li>
        <li>
          <strong>Liability</strong>: Proving negligence will aid in your case
          and can add to the amount you would receive for damages. A roof crush
          injury attorney can hold someone responsible for your injuries and
          losses you have accumulated because of the incident.
        </li>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Vehicle Roof Crush Injury Attorneys"
            style={{
              backgroundImage:
                "url('../../images/images/Auto Crush Roof Text header.jpg')"
            }}
          >
            <h2>Our Roof Crush Attorneys Are Here to Help</h2>
          </div>
        </LazyLoad>
        <p>
          The California law firm of Bisnar Chase stands behind its commitment
          to represent clients who are seriously injured during a roof crush
          accident.
        </p>
        <p>
          Brian Chase is the litigation team leader at the law firm. In 2012,
          his peers nominated Mr. Chase "Trial Attorney of the Year" for his
          success in litigating cases against negligent automakers.
        </p>
        <p>
          Our attorneys understand the history and intricacies between vehicle
          manufacturers, NHTSA and the liability laws that hold automakers
          financially accountable for negligent design. Our roof crush injury
          litigation team has successfully concluded a number of California
          cases with the lawyers from Ford and General Motors, involving people
          who died as a result of{" "}
          <Link to="/auto-defects/rollovers/suv-rollovers">
            SUV and light duty truck rollover accidents
          </Link>{" "}
          where the roofs failed and crushed the vehicle's occupants.
        </p>
        <p>
          Contact our law office today and receive a{" "}
          <strong>free evaluation</strong>. If there is no win, there is no fee.
        </p>
        <p>
          Call <strong>800-561-4887 a</strong>nd we start fighting for the
          compensation we know you deserve.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys
          <br />
          1301 Dove St. #120
          <br />
          Newport Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13283.243886899194!2d-117.8664064!3d33.6620595!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1565381809249!5m2!1sen!2sus"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
