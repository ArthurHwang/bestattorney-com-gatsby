// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Auto Products Liability - Bisnar Chase"
        pageDescription="Auto products liability refers to motor vehicle manufacturers and their legal duty to the public to design and manufacture safe - crashworthy vehicles"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Auto Products Liability</h1>
        <BreadCrumbs location={location} />
        <p>
          Auto products liability refers to motor vehicle manufacturers and
          their legal duty to the public to design and manufacture safe -
          crashworthy vehicles. This means that all vehicles must be designed to
          be safe in foreseeable crashes and accidents.
        </p>{" "}
        <p>
          The protection that an automobile provides to the occupants in the
          event of an accident is referred to as crash worthiness. If an
          automaker produces a car or truck that is determined not to be
          crashworthy and the design defect contributed to the accident or
          worsened the injuries sustained, the automobile manufacturer could be
          held liable for the damages incurred.
        </p>
        <p>
          The automobile product liability lawyers of the law firm of Bisnar
          &amp; Chase have a track record of success against automakers. Brian
          Chase was named Trial Lawyer of the Year - Product Liability by his
          trial lawyer peers. The firm represents victims of serious injury and
          wrongful death from accidents and unsafe products including defective
          automobiles and defective automobile products. If you or a loved one
          has been injured, contact us today for an immediate free consultation
          with an experienced lawyer.
        </p>
        <p>
          Automobile manufacturers and product designers have a responsibility
          to produce safe products to consumers. Some of the types of automobile
          product defects cases Bisnar &amp; Chase litigates include:
        </p>
        <ul>
          <li>Seatbelt Failures and Defects </li>
          <li>Air Bag Injuries and Defects </li>
          <li>Defective Tires </li>
          <li>Automobile Recalls</li>
          <li>15-Passenger Van Defects</li>
          <li>Exploding Gas Tanks </li>
          <li>
            {" "}
            <Link to="/auto-defects">Seat Back Failures</Link>
          </li>
          <li>Roof Crush Injuries</li>
          <li>Door Latch Failures </li>
          <li>Occupant Ejections </li>
          <li>SUV Rollovers</li>
        </ul>
        <p>
          Our firm is committed to protecting the rights of our clients and to
          recovering the maximum monetary recovery possible. We have recovered
          tens of millions of dollars from automakers for our personal injury
          and product liability clients.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
