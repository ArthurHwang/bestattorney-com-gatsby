// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="U.S. Puts Out 15 Passenger Van Advisory Over Banning Them"
        pageDescription="Injured in a 15 Passenger Van accident? The United States has put out a safety advisory rather than ban these dangerous vehicles."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>NHTSA Advisory for 15-Passenger Vans</h1>
        <BreadCrumbs location={location} />
        <p>
          It is common knowledge that there have been fatal accidents and{" "}
          <Link to="/auto-defects/rollovers">
            rollovers of 15-passenger vans
          </Link>
          . These vans appear to be inherently dangerous, but interestingly
          enough rather than ban them the US Government has put out a safety
          advisory on how to manage these vans. Some of this advisory will be
          listed below for the benefit of those people still driving these vans.
          The full advisory can be found at{" "}
          <Link to="https://www.nhtsa.gov" target="_blank">
            https://www.nhtsa.gov
          </Link>
          .
        </p>
        <p>
          One of the more interesting advisories is that one should not put more
          than 10 people in these 15-passenger vans.{" "}
          <Link to="/defective-products">Product liability cases</Link> have
          demonstrated that this van has caused many injuries and fatalities to
          innocent people, so the advice is not to put 15 people in them, rather
          than consider the inherent design defects, is interessting. The
          following advisories seem to be the best that the government can do
          about these vans and it is very important to adhere to this advice.
          These are some of the NHTSA guidelines:
        </p>
        <p>
          Tire Pressure - Inspect the tires and check tire pressure before each
          use. The van's tires need to be properly inflated and the tread should
          not be worn down. Excessively worn or improperly inflated tires can
          lead to a loss of vehicle control and ultimately a rollover. Pressure
          for front and back tires may be different and pressure is likely
          higher than that required for car tires. A placard on the driver's
          side B-pillar or the owner's manual lists manufacturer recommended
          tire size and pressure.
        </p>
        <p>
          Driver - 15-passenger vans should only be operated by trained,
          experienced drivers who operate these vehicles on a regular basis. The
          driver needs to possess a valid driver's license for state of
          residence (a commercial driver's license is preferred). 15-passenger
          van drivers need additional training since these vehicles handle
          differently than passenger cars, especially when fully loaded.
        </p>
        <p>
          Size - A 15-passenger van is substantially longer and wider than a
          car, and thus requires more space to maneuver. It also requires
          additional reliance on the side-view mirrors for changing lanes.
        </p>
        <p>
          Speed - Drive at a safe speed based on driving conditions. Driver
          should never exceed the posted speed limit. Slow down if the roads are
          wet because these vehicles do not respond well to abrupt steering
          maneuvers and require additional time to brake.
        </p>
        <p>
          Cargo - Cargo should be placed forward of the rear axle and placing
          any loads on the roof should be avoided. Do not tow anything behind
          the van. See vehicle owner's manual for maximum weight of passengers
          and cargo and avoid overloading the van.
        </p>
        <p>
          Seat Belts - All occupants need to wear seat belts at all times.
          Inspect seat belts regularly and replace any missing, broken or
          damaged belts and/or buckles. An unrestrained 15-passenger van
          occupant involved in a single-vehicle crash is about three times as
          likely to be killed as a restrained occupant.
        </p>
        <p>
          Please be very cautious in these 15 passenger vans. It is important to
          note that some colleges have banned their use for all of their
          activities and many schools have done the same. For more information,
          see{" "}
          <Link to="/blog/canada-ban-15-passenger-van">
            Canada Comes Closer to Ban on 15-Passenger Vans
          </Link>
          .
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
