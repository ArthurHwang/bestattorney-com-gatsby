// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="15 Passenger Van Rollover Accidents - Rollover Crashes"
        pageDescription="Severely injured in a 15 passenger van rollover collision? Call 949-203-3814 for top-rated van rollover accident attorneys. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>15 Passenger Van Rollover Accidents</h1>
        <BreadCrumbs location={location} />
        <h3>
          Video: <Link to="/attorneys/brian-chase">Brian Chase</Link>, an{" "}
          <Link to="/auto-defects">auto defect attorney</Link> Talks to CBC News
          Television about the Dangers of 15 Passenger Vans
        </h3>
        <div align="center">
          <LazyLoad>
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/PHSDRRCs-5Q"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
          </LazyLoad>
          <br />
        </div>

        <center>
          <blockquote>
            For immediate assistance with a 15 passenger van accident <br />
            call 949-203-3814
          </blockquote>
        </center>

        <p>
          15 passenger vans are used by various groups and organizations due to
          their ability to transport a large amount of people and their
          accompanying cargo. Many people who drive or are passengers in these
          vans are unaware of the risks of rollover accidents and subsequent
          injuries involved. These types of vehicles have a higher likelihood
          than any other vehicle to be involved in dangerous rollover accidents,
          many of which are single vehicle accidents. This is due to the design
          of the large vehicle, which when it is fully loaded with passengers
          and cargo becomes unstable and may fishtail and rollover.{" "}
        </p>
        <p>
          According to a{" "}
          <Link
            to="https://www.cbsnews.com/2100-500164_162-506479.html"
            target="_blank"
          >
            CBS special report
          </Link>
          , the 15 passenger van is one of the most dangerous vehicles on the
          road.{" "}
        </p>
        <p>
          <em>
            'These vans looks like any other van, except they have been
            lengthened to hold more riders. The problem is, when the van is
            fully loaded, it is three times more likely to roll over in an
            emergency. All the American carmakers build a version of the van.
            Ford sells the most.{" "}
          </em>
        </p>
        <p>
          <em>
            Step off a plane and the airport hotel is likely to pick you up in a
            15-passenger van. Back home at the day care center, the kids are
            climbing aboard. Often it's the shuttle for the park &amp; ride, the
            lift to the university. The YMCA drives them, the Post Office, too.
            And when Disabled American Veterans move en masse to lobby Congress,
            it's the 15-passenger van that carries them.
          </em>
        </p>
        <p>
          <em>
            They seem to be everywhere. There are about 500,000 of them on the
            road. Millions of Americans who ride in them don't give them a
            thought until the unique character of the van is suddenly,
            tragically revealed.'
          </em>
        </p>
        <h2>Why are the 15 passenger vans so dangerous?</h2>
        <p>
          Fifteen-passenger vans are popular small vehicles for many
          organizations that need to transport small groups over long distances
          -- groups like school sports teams, church youth groups, senior center
          groups or summer campers. But even though thousands of organizations
          entrust these vans with their members' safety each year, we now know
          that the vans don't deserve our trust.{" "}
        </p>
        <p>
          According to the National Highway Traffic Safety Administration,
          15-passenger vans are more likely to be involved in a single-vehicle
          rollover accident than any other type of vehicle. A study by the NHTSA
          shows that 15-passenger vans are more than twice as likely to roll
          over when filled to above half their capacity than when they're at or
          under half of their capacity. That is, when used for their intended
          purpose -- carrying large numbers of people -- the vans are
          significantly more likely to be involved in a fatal accident. That's
          because their center of gravity shifts upward and backward, a design
          flaw that makes the van more difficult to control in an emergency.{" "}
        </p>
        <p>
          Mistakes made by the driver and organizers of the van's trip may also
          be a factor in crashes. According to the NHTSA, the following are
          mistakes that increase the chances of a rollover accident:{" "}
        </p>
        <ul>
          <li>Overloading the van with more than 15 passengers.</li>
          <li>
            {" "}
            Not requiring everyone to wear a seat belt or age-appropriate child
            restraint.{" "}
          </li>
          <li>
            Not requiring passengers in a partially empty van to sit in front of
            the rear axle, preventing dangerous changes in the van's center of
            gravity.{" "}
          </li>
          <li>Storing luggage on the roof. </li>
          <li>
            Overinflating or underinflating tires, which can cause the vans to
            handle differently.{" "}
          </li>
          <li>
            Not leaving enough space and distance between the van and other
            vehicles to accommodate the van's large size and weight.{" "}
          </li>
        </ul>
        <p>
          If you or a loved one have been in an accident involving a
          15-passenger van and are considering legal action, you need an
          experienced van rollover attorney with proven results. The lawyers at
          BISNAR CHASE have decades of experience aggressively representing
          victims of 15-passenger van accidents and auto accidents. We've
          recovered tens of millions in settlements with major automobile
          manufacturers and others for auto accident victims and families.{" "}
        </p>
        <p>
          For a free, confidential case evaluation please call 949-203-3814 or
          complete our easy <Link to="/contact">free legal case review</Link>{" "}
          request.
        </p>
        <img
          src="/images/superlawyers.jpg"
          alt="Bisnar Chase are SuperLawyers"
        />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
