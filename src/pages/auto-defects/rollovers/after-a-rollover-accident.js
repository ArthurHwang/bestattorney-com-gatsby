// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import folderOptions from "./folder-options"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="What to Do After a 15 Passenger Van Rollover Accident "
        pageDescription="Call 949-203-3814 for award-winning 15 Passenger van accident injury lawyers and van accident Attorneys in California. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>After A Van Rollover Accident</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>BISNAR CHASE</strong> is a well respected 15 Passenger Van
          Rollover law firm representing victims who have been injured in 15
          passenger van accidents. Our experienced van accident attorneys and
          van rollover lawyers have a successful track record of impressive
          recoveries on behalf of our injured clients. Our firm has been
          representing cases of severe injury and wrongful death since 1978.
          Representing people who have suffered severe injuries or lost a loved
          one is all we do. BISNAR CHASE is based in Orange County, but
          represents seriously injured clients in many areas. In cases of
          catastrophic and/or multiple severe injuries we will handle or assist
          on cases outside of California in association with local counsel. If
          you or a loved one has been injured in a 15 passenger van rollover
          accident, contact BISNAR CHASE today for a free consultation.
        </p>
        <h2>WHAT TO DO AFTER A 15 PASSENGER VAN ROLLOVER ACCIDENT</h2>
        <p>
          Stay calm and collected. Calm behavior will reassure other people who
          may have been injured in the accident.
        </p>
        <p>
          Call 911 right away to report the accident so that help will arrive as
          quickly as possible. Even if there were no serious injuries you should
          contact the police.
        </p>
        <p>
          Determine if you were injured in the rollover accident. If you were
          not seriously injured check the status of other passengers. Let them
          know that you have called 911, and reassure them that everything is
          alright and try to keep them calm.
        </p>
        <p>
          Check other accident victims for serious or life threatening injuries.
        </p>
        <p>
          Apply pressure to any bleeding areas on yourself or other victims.
        </p>
        <p>
          Unnecessary movement of injured victims may cause more pain and
          suffering; only move an injured person if he or she is in an area that
          may cause more harm.
        </p>
        <p>
          Assess the scene of the accident for hazardous conditions, such as
          gasoline leaking from a vehicle or power lines in the middle of the
          street.
        </p>
        <p>
          Do not interfere with the assistance provided by paramedics, police
          officers, highway patrol, or fire fighters.
        </p>
        <p>
          Write down anything that you can remember leading up to the accident,
          and the location, time of day, road condition, and weather when the
          accident occurred. If a camera is available take photos of where the
          accident occurred and any damage to your vehicle.
        </p>
        <p>
          If you or a loved one has been injured as the result of a 15 passenger
          van accident, please contact a 15 Passenger Van Accident Lawyer at
          BISNAR CHASE immediately. Call today for a free consultation.
        </p>
        <p align="center">
          <strong>
            "WHEN RESULTS COUNT"
            <br />
            BISNAR CHASE
          </strong>
        </p>
        <h3 align="center">949-203-3814</h3>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
