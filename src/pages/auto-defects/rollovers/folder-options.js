/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "auto-defect",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "Auto Defect Attorneys",
    HTML: `
    <ul className='bpoint'>
      <li>Several different factors can lead to being injured in a rollover accident:</li>
      <li><strong>Type of Car: </strong>Evidence shows that 15 passenger vans are by far the most likely vehicles to roll over. SUVs and other tall cars also have a higher likelihood of rolling over.</li>
      <li><strong>Luggage on the Roof: </strong>If a lot of heavy luggage is on the roof, the car will be top-heavy and more likely to roll over on a sharp turn.</li>
      <li><strong>Tire inflation: </strong>If the Tires are over or under-inflated, cars can handle differently and it can be easier to tip over. </li>
      <li><strong>ESC: </strong>If the car has electronic stability control (ESC), the car is more likely to avoid loss of traction, which can lead to a rollover</li>
      <li><strong>Roof Crush: </strong>If the car roof is not reinforced, a rollover accident can result in a roof crush, which can injure or kill passengers inside.</li>
      <li>If you or a loved one have suffered as a result of a rollover or roof crush, know your rights and <a href='/contact'>contact an attorney</a> today. We may be able to get you the compensation you deserve.</li>
    </ul>
    `
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Rollover Injury Information",
    links: [
      {
        linkName: "After a Rollover Accident",
        linkURL: "/auto-defects/rollovers/after-a-rollover-accident"
      },
      {
        linkName: "15 Passenger Van Advisory",
        linkURL: "/auto-defects/rollovers/15-passenger-van-advisory"
      },
      {
        linkName: "Avoiding a 15 Passenger Van Rollover",
        linkURL: "/auto-defects/rollovers/avoiding-a-rollover"
      },
      {
        linkName: "SUV Rollovers",
        linkURL: "/auto-defects/rollovers/suv-rollovers"
      },
      {
        linkName: "SUV Rollover Victims",
        linkURL: "/auto-defects/rollovers/suv-rollover-victim"
      },
      {
        linkName: "Van Rollovers",
        linkURL: "/auto-defects/rollovers/van-rollovers"
      },
      {
        linkName: "Rollovers Home",
        linkURL: "/auto-defects/rollovers"
      },
      {
        linkName: "Back to Auto Defects",
        linkURL: "/auto-defects"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "Why Focus on Auto Defect Cases?",
      videoUrl: "hpi1tBCvEtw"
    },
    {
      videoName: "How Does The Auto Recall Process Work?",
      videoUrl: "-wv3MjBd6pg"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
