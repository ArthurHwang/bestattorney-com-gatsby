// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="SUV Rollover Attorneys Bisnar Chase"
        pageDescription="Bisnar Chase represent people who are involved in serious and catastrophic SUV rollover accidents. Call 949-203-3814 to see if you have an auto defect rollover case"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>SUV Rollover Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <b>For Immediate Help with an SUV Rollover Case Call 949-203-3814</b>
        </p>
        <p>
          The dangers of SUV rollover accidents on our Nation's highways
          continue to climb at alarming rates. SUVs are the most common vehicles
          to roll over, mainly due to their high center of gravity. More than
          10,000 motorists are killed in rollover accidents each year.
        </p>
        <p>
          In a recent court case, a jury awarded{" "}
          <Link to="/pdf/Press-Release.pdf" target="_blank">
            $51.5 million
          </Link>{" "}
          to California and other States from Ford Motor Company. The money was
          designated to educate the public on the{" "}
          <Link to="/pdf/Safer-car.pdf" target="_blank">
            dangers of SUV rollovers
          </Link>
          . The jury determined that Ford knew their SUVs were unreasonably
          dangerous. The jury also determined that Ford misled the public
          through false advertising regarding the dangerous propensities of
          Ford's SUVs.
        </p>
        <p>
          Attorneys John Bisnar and Brian Chase recover millions of dollars
          annually for their clients who have lost family members or sustained
          very serious injuries in SUV rollover accidents. Ford and General
          Motors knowingly sell some of the most dangerous SUVs on our streets.
          Only by holding automakers accountable for their poor designs and the
          resulting injuries to the motoring public, will we convince the less
          socially conscience automakers to build safer vehicles.{" "}
        </p>
        <p>
          {" "}
          Bisnar &amp; Chase continue to fight vehicle manufacturers whose
          faulty designs and defects cause accidents that abruptly end lives or
          disable people. If your family has been involved in an SUV rollover or
          has been in an accident involving a rollover, contacting Bisnar &amp;
          Chase is one of the best decisions you can make to preserve your legal
          rights and rebuild your life.
        </p>{" "}
        <LazyLoad>
          <img
            src="/images/closecar_3.jpg"
            width="100%"
            alt="an SUV lies on a hillside after a serious rollover accident"
          />
        </LazyLoad>
        <p>
          Settling and litigating serious injury and wrongful death cases since
          1978, Bisnar &amp; Chase fight for clients whose lives will never be
          restored through money alone. It is the payments to the victims of
          corporate insensitivity and greed that punish negligent companies and
          encourage them to put an end to their irresponsible advertising and
          dangerous vehicle designs.
        </p>{" "}
        <p>
          Companies selling SUVs and other vehicle designs causing unnecessary
          deaths and injuries including General Motors, Chrysler and Ford, each,
          have been forced to compensate Bisnar &amp; Chase clients for the
          damages they have suffered.
        </p>
        <p>
          {" "}
          <Link to="/pdf/crash-killer-rates.pdf" target="_blank">
            Government statistics
          </Link>{" "}
          show that if you are between the ages of 3 to 31 your untimely death
          will be caused by a vehicle collision. Bisnar &amp; Chase wages a
          personal and professional war against companies who have the power to
          make safer vehicles but instead choose to profit from unsafe designs.
          The 15-passenger van is the most dangerous vehicle on our highways.
          The common vehicle dangers that are rarely in the headlines include:
        </p>
        <ul>
          <li>Side-impact injuries</li>

          <li>Air bag Failure</li>

          <li>Seatbelt Failure</li>

          <li>Seat Back Failures</li>

          <li>Tire Failures</li>

          <li>Roof Crush Injuries</li>

          <li>Defective Door Latches</li>

          <li>Seat Ejection Injuries</li>
        </ul>
        <p>
          <b>Contact us to discuss your SUV rollover case. 949-203-3814</b>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
