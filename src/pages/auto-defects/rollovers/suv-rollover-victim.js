// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Auto Defect Victim - Defective SUV Rollover"
        pageDescription="Auto defect victim's story. Call 949-203-3814 for highest-rated auto defect attorneys in Southern California.  Specializing in catastrophic injuries, car accidents, wrongful death & auto defects.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Auto Defect Victim</h1>
        <BreadCrumbs location={location} />
        <h2>Defective SUV Rollover</h2>
        <p>
          A young girl named Amanda climbed into her family's Ford Explorer with
          her mother Emily and 8-year-old brother Ryan to venture onto the 91
          freeway. Suddenly, a U-Haul truck next to them moved into their lane,
          and Amanda's mother, Emily, had but a moment to decide what to do. She
          abruptly swerved away from the U-Haul. In doing so, her vehicle
          immediately spun and rolled out of control coming to a stop upside
          down. Thankfully, everyone in the car was wearing a seatbelt.
        </p>
        <p>
          Ryan's seat belt was enough to keep him from getting hurt.
          Unfortunately, Emily's face slammed into the steering wheel, and
          because she had no air bag, the impact fractured most of the bones in
          her face and caused her to sustain an injury to her brain. Amanda's
          defective seat belt did not hold, and so she was helplessly tossed
          while the car rolled, and by the time it came to rest, Amanda was
          paralyzed from the waist down.
        </p>
        <h2>Staying Strong</h2>
        <p>
          Amanda proved to be a very tough girl. She battled severe depression
          and came out with enthusiasm, optimism, and newfound vigor. Meanwhile,
          her family hired a products liability attorney and filed a personal
          injury lawsuit against Ford Motor Company and the driver of the U-Haul
          truck. The Ford Explorer being driven at the time of the accident had
          proven to have two serious defects: specifically, it was a rollover
          inclined SUV and had an outdated seat belt buckle prone to coming
          unlatched in accidents. The U-Haul driver's negligence was a shame,
          but the real shame came from Ford's carelessness in the construction
          of this vehicle. If it weren't for these defects, Amanda would have
          sustained relatively minor injuries.
        </p>
        <p>
          Brian Chase always keeps in contact with his clients, and the Taylor
          family was no different. However, one time Brian called, Amanda was
          unavailable. She was found lifeless in her bed, and the cause of death
          was attributed to a blood clot -- the kind that afflicts victims of
          paraplegia. When the accident first happened, Amanda Taylor was robbed
          of the opportunity to enjoy a normal life. This time, she was robbed
          of life itself. The family was devastated, and nothing could
          substitute for the love and joy that Amanda brought to those around
          her.
        </p>
        <h2>No Justice</h2>
        <p>
          Justice was needed, but what was stripped from the Taylor family could
          not be given back. No matter how their case turns out, there is no way
          to compensate for their losses. The callous disregard for public
          safety put on display by these auto manufacturers is unacceptable.
          Helping innocent accident victims who've been catastrophically injured
          by corporate greed -- who've been victimized by auto manufacturers
          that routinely place profit over safety -- is what drives Bisnar Chase
          Personal Injury Attorneys.
        </p>
        <p>
          Many times, a vehicle defect may be overlooked. Bisnar Chase Personal
          Injury Attorneys has successfully litigated many cases against the top
          auto manufacturers in the country and have experience in recognizing{" "}
          <Link to="/auto-defects">auto defects</Link>. The cause of an accident
          is not always clear; having an experienced personal injury attorney
          will help you avoid these pitfalls.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
