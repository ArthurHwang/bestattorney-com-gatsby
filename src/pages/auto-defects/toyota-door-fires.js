// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Toyota Door Fires in 2007 Camry Sedans and RAV-4"
        pageDescription="Currently, federal safety regulators are investigating reports of fires in the driver's side doors of 2007 Toyota Camry sedans and RAV-4 crossover SUVs."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Toyota Door Fires Investigated</h1>
        <BreadCrumbs location={location} />
        <h2>Toyota Rav4 and Camry Suffering from Door Fires</h2>
        <p>
          <img
            src="/images/check engine light.jpg"
            alt="Toyota Door Fire Attorneys"
            width="200"
            className="imgleft-fluid"
          />
          Our{" "}
          <Link to="/defective-products">Toyota product defect attorneys</Link>{" "}
          have their hands full with a potentially deadly defect surfacing
          across the country. Currently, federal safety regulators are
          investigating reports of fires in the driver's side doors of 2007
          Toyota Camry sedans and RAV-4 crossover SUVs.
        </p>
        <p>
          The massive probe could affect as many as 830,000 vehicles, the
          National Highway Traffic Safety Administration said Friday in
          documents posted on its website. The vehicles have not yet been
          recalled, but the investigation continues.
        </p>
        <p>
          These spontaneous fires appear to start in the power window switch on
          the door. Six fires have been reported to the agency, but NHTSA has no
          reports of anyone being hurt. The agency said it started the
          investigation on Monday. With over 830,000 vehicles using the switch
          currently on the road, the possibilities for more widespread problems
          and failures are manifest.
        </p>
        <h2>Negligent Manufacturers under Fire</h2>
        <p>
          Toyota's reputation has taken a major hit over the past three years
          due to a string of massive recalls that have ballooned to more than 14
          million vehicles worldwide. Millions were recalled for acceleration
          problems, and Toyota replaced floor mats that can trap gas pedals.
          Additionally, they have corrected pedal assemblies that can stick and
          cause cars to take off by surprise.
        </p>
        <p>
          Still, the latest investigation is troubling for the automaker because
          the Camry is consistently is the top-selling car in the U.S., and the
          RAV-4 also is a big seller. The probe also includes the Solara, which
          is a coupe version of the Camry.
        </p>
        <p>
          Toyota sold 473,108 Camrys and 172,752 RAV-4s in 2007. Some 2007
          models may have been sold in late 2006.
        </p>
        <h2>Toyota Product Liability Attorneys</h2>
        <p>
          If you drive one of the vehicles under investigation and have
          additional questions, Toyota encourages you to check the recall alerts
          from the NHTSA for further information. In the meantime, we also
          suggest contacting a{" "}
          <Link to="/defective-products" target="_blank">
            Toyota product liability lawyer
          </Link>{" "}
          at 949-203-3814 or fill out our contact form for suggestions and
          directions as this problem unfolds.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
