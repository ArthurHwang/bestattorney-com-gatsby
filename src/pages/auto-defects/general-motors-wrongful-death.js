// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Family Files Wrongful Death Suit Against GM for Airbag Defect"
        pageDescription="A family of a Dallas woman has filed a wrongful death lawsuit against General Motors for airbag defect"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Wrongful Death Case Filed against General Motors for Airbag Defect
        </h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Family of a deceased Dallas County woman has filed a wrongful death
            lawsuit against General Motors for a defective Chevy Impala.
          </strong>
        </p>
        <p>
          <img
            src="/images/wrongful-death-GM.jpg"
            alt="wrongful death suit filed against General Motors"
            width="300"
            className="imgright-fixed"
          />
          According to a Jan. 13 news report in The Southeast Texas Record, the
          lawsuit alleges that Shonnie Bryan died because the car's airbags did
          not deploy during the crash. The family of Shonnie Bryan, who died in
          a Nov. 26, 2010 crash has filed a{" "}
          <Link to="/wrongful-death">wrongful death lawsuit</Link> (Case Number:
          6:13-cv-00036) against General Motors alleging that the car's airbags
          did not deploy during the accident.
        </p>
        <p>
          Bryan's family members filed the lawsuit at the Texas Eastern District
          Court, according to court documents. The article states that the
          incident occurred on Nov. 26, 2010 as Shonnie Bryan was driving her
          2003 Chevrolet Impala when she lost control of the vehicle and crashed
          into a culvert on the side of the road.
        </p>
        <p>
          The lawsuit (Case Number: 6:13-cv-00036) alleges that the airbags of
          the Impala did not deploy and accuses General Motors of negligently
          designing, manufacturing and marketing a "defective and unreasonably
          dangerous" vehicle. The lawsuit is seeking an award of damages,
          interest and court costs, the report states.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/_okWKolf9qc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          According to a report in The Detroit News, General Motors Co. had 17
          recall campaigns in 2012 involving nearly 1.5 million vehicles
          compared to 22 campaigns covering just 500,000 vehicles in 2011. GM's
          most significant recall came in June 2012 when it called all 413,000
          Chevy Cruze vehicles built for the U.S. market to prevent possible
          engine fires due to fluids trapped in the engine compartment after an
          oil change, Detroit News reported.
        </p>
        <p>
          Auto defects such as faulty airbags can have devastating consequences,
          said John Bisnar, founder of the Bisnar Chase personal injury law
          firm. "Airbags are faulty when they fail to deploy or when they deploy
          inadvertently and both can cause significant injuries. When airbags do
          not deploy during a crash, there is the potential for victims to
          suffer severe trauma, which can prove fatal. We count on airbags for
          that additional layer of protection in the event of a crash. Not
          having that can be catastrophic for vehicle occupants."
        </p>
        <p>
          When vehicle parts or safety features malfunction or fail to work as
          they are supposed, the auto maker or the manufacturer of the defective
          part can be held liable, said Bisnar. "Auto manufacturers have the
          legal obligation to make vehicles that are safe for consumers. In the
          event of a manufacturing of a design defect, it is crucial that
          automakers issue a prompt recall so the necessary repairs can be made
          and consumers are not exposed to further danger."
        </p>
        <h2>About Bisnar Chase</h2>
        <p>
          The California auto product liability attorneys of Bisnar Chase
          represent victims of auto accidents, defective products, dangerous
          roadways, and many other personal injuries. The firm has been featured
          on a number of popular media outlets including Newsweek, Fox, NBC, and
          ABC and is known for its passionate pursuit of results for their
          clients.
        </p>
        <p>
          For more information, please call 949-203-3814 or{" "}
          <Link to="/contact">contact us</Link> for a free consultation.
        </p>
        <p>Sources:</p>
        <ul>
          <li>
            http://setexasrecord.com/news/280452-wrongful-death-lawsuit-filed-against-gm-after-cars-air-bags-failed-to-deploy
          </li>
          <li>
            http://www.detroitnews.com/article/20130108/AUTO0104/301080357
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
