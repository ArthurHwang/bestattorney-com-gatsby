// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Drunk Driving Plaintiff Attorneys Bisnar Chase"
        pageDescription="Drunk driving plaintiff attorney John Bisnar wants more sanctions, better protection for victims and a legal system that protects victims of drunk drivers."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Drunk Driving Plaintiff Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <b>For Immediate Help with Your Case Call 949-203-3814</b>
        </p>
        <p>
          <img
            src="/images/dui-murder.jpg"
            alt="drunk driving plaintiff attorneys"
            className="imgright-fixed"
          />
          Our legal system is designed to punish drunk drivers in both civil and
          criminal courts. Despite these sanctions, drivers continue to endanger
          lives by operating vehicles under the influence of alcohol and drugs.
        </p>
        <p>
          Plaintiff's Attorney John Bisnar has seen the devastation caused by
          intoxicated drivers and knows how to fight for families torn apart by
          drunken driving accidents.
        </p>
        <p>
          &ldquo;Although money cannot put a family back together, it can send a
          threatening message to people who consider driving while
          intoxicated,&rdquo; Bisnar said.
        </p>
        <p>
          Bisnar helps families file personal injury and punitive damage claims
          against drivers who have been prosecuted in criminal law for drunken
          driving charges ranging from vehicular homicide to reckless
          endangerment. If the convicted drunk driver will not settle out of
          court, Brian Chase, an award-winning litigator, will argue your case
          in court. Not all personal injury attorneys go to trial. We do.
        </p>
        <p>
          Drunken driving accidents continue to cause serious injury and{" "}
          <Link to="/catastrophic-injury">wrongful deaths</Link> among Americans
          despite substantial efforts by the government to regulate the illegal
          activity.{" "}
        </p>
        <p>
          If your loved one has been injured by a drunk driver, grieving is
          often worsened by the drunk driver's reckless disregard that ended a
          precious life.
        </p>
        <p>
          Despite the unlawful behavior, every driver suspected of driving while
          intoxicated can prove their sobriety, or lack thereof, through a
          battery of breathalyzer tests, blood samples, or field sobriety exams.
          The driver is not required to provide any further evidence to
          authorities that could possibly assist in his or her prosecution.{" "}
        </p>
        <p>
          If you are ever involved in a drinking and driving accident, take
          appropriate steps to exercise your legal rights:
        </p>
        <ul>
          <li>
            No matter how minor, contact the police immediately, as a lack of
            necessary evidence will prove difficult to validate intoxication
            and/or prosecute the driver.
          </li>
          <li>
            Pay attention to all details of a drunken driving accident, as
            victims may be recruited for witness purposes.
          </li>
          <li>
            Make sure all parties present when the accident occurs give
            statements to the authorities.
          </li>
        </ul>
        <p>
          Many people mistakenly believe that following a drunken driving
          accident, there is little recourse for victims beyond auto insurance
          claims. This is not true. The attorneys of Bisnar &amp; Chase can help
          victims seek compensation above and beyond the driver's insurance
          coverage.
        </p>
        <p>
          Attorneys at Bisnar &amp; Chase can also help victims recover
          compensation from the government under crime victim compensation laws
          that were designed to aid hospital and funeral expenses.{" "}
          <b>Please Call 949-203-3814 for a free consultation</b>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
