// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Hyundai Sonata Steering Defect - Auto Product Liability"
        pageDescription="Have you had an incident? Call 949-203-3814 for highest-rated personal injury lawyers, serving all of SoCal!."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Hyundai Recalls Defective Cars</h1>
        <BreadCrumbs location={location} />
        <p>
          {" "}
          <Link to="/auto-defects">Auto defects</Link> have run rampant this
          year with numerous top manufacturers recalling their vehicles, and it
          seems Hyundai has reluctantly joined the 2010 recall club. Hyundai, a
          major auto manufacturer based out of South Korea, has recently
          recalled their Sonata sedans due to a steering-related product defect.
          The implications of the auto defect are disastrous.
        </p>
        <h2>NHTSA Spurs Mass Auto Defect Recall</h2>
        <p>
          Hyundai announced the recall after the National Highway Traffic Safety
          Administration opened a probe into steering problems in August, a move
          analysts said was aimed at heading off criticism that the company was
          slow to respond. 139,500 Sonatas Sonata sedans sold in the United
          States have now been recalled.
        </p>
        <p>
          The most disturbing part of the mass recall is that the vehicles
          prompting the investigation were all manufactured during the same
          month and had fewer than 600 miles of service at the time of the
          incidents. According to the National Highway Traffic Safety
          Administration, Hyundai dealerships will fully inspect the vehicles
          and update the steering software in the vehicles free of charge.
        </p>
        <h2>Future Product Recall Injuries</h2>
        <p>
          Although there have yet to be any reported injuries because of the
          defective steering wheels, they will almost certainly come. NHTSA
          reports that Hyundai is recalling most 2011 Sonata vehicles
          manufactured from December 11, 2009 through September 10, 2010. On
          some of these vehicles the steering column intermediate shaft
          universal joint connections may have been either improperly assembled
          or insufficiently tightened. Improper assembly or insufficient
          tightening of the connections could result in a complete separation or
          compromised attachment of the connections, such that the driver could
          experience a loss of or reduction in, steering capability increasing
          the risk of crash.
        </p>
        <p>
          Defective cars such as these should not be able to make it through the
          assembly line. Over the years I have seen countless dangerous autos
          with defects released to the public. Many times the auto manufacturers
          knew that cars were dangerous, had test results proving they were
          dangerous, but refused to fix the problem because of projected loss of
          profit. I'm sick of being considered acceptable loss; my family does
          not deserve to be a casualty of big business.
        </p>
        <p>
          If you have experienced a similar auto defect or any defect in your
          automobile whatsoever, we encourage you to report it to NHTSA
          (http://www-odi.nhtsa.dot.gov/ ) so that they can look into it.
        </p>
        <h2>Your Auto Defect Legal Advocates</h2>
        <p>
          Seriously injured by a defective Hyundai automobile? Want to know if
          you have a case? Want to know what your case is worth? Want
          compensation for your injuries? Want justice? Want to make sure the
          same thing doesn't happen to someone else?
        </p>
        <p>
          Call your best friends in the legal industry after an auto defect
          related accident.
          <br />
          Call 949-203-3814.
          <br />
          The call is free. The advice may be priceless.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
