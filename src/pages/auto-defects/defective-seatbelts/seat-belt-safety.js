// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Seat Belt Safety - Wearing your seatbelt can save your life"
        pageDescription="Seat belt safety affects us all. These seatbelt safety tips provided by the personal injury attorneys at Bisnar Chase could save a life."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Seat Belt Safety Factors</h1>
        <BreadCrumbs location={location} />
        <h2>Seat Belt Safety Affects Everyone</h2>
        <p>
          This video from the{" "}
          <Link to="https://www.iihs.org/" target="_blank">
            Institute of Highway Safety
          </Link>{" "}
          shows the difference in impact when a person wears a seat belt and
          when they don't. The impact to the head, chest and neck is obvious.
          Seat belt safety is an issue that affects all of us. We all share the
          roads and highways and at anytime we or a loved one could be a victim
          of a car accident.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/d7iYZPp2zYY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          The saying "click it or ticket" is pressed upon us for a reason. The
          leading cause of death in people under age 35 is car accidents. 40,000
          people die each year in car accidents: This number could be{" "}
          <b>reduced by almost half if seat belts were worn</b> by everyone who
          traveled in an automobile. If you are aware of these statistics on
          seat belt safety and are still not willing to wear one every time you
          are in an automobile, ask yourself:
        </p>
        <ul>
          <li>
            <i>Why am I not wearing my life saving device?</i>
          </li>
          <li>
            <i>How do I benefit by not wearing my life saving device?</i>
          </li>
          <li>
            <i>
              What might it cost my family and I if I don't wear my life saving
              device?
            </i>
          </li>
        </ul>
        <h3>Seat belts, when manufactured and used properly, SAVE LIVES</h3>
        <p>
          <strong>
            Consider the factors involved in an automobile collision, either
            with another vehicle or an immobile object:
          </strong>
        </p>
        <p>
          In a collision a car stops in the first tenth of a second, but
          occupants' bodies keep moving at the same rate the automobile was
          traveling until something stops the occupant's body.
        </p>
        <p>
          If you are not wearing your safety belt at the time of a collision,
          your body will impact with the steering wheel, dashboard, windshield
          or seat-back at that same rate of speed as the vehicle was traveling
          prior to the collision. If you were not wearing your seat belt in a
          collision at 30 miles per hour, your body would impact with the same
          intensity as if you fell from the top of a three-story building.
        </p>
        <p>
          As you can imagine, it would be difficult to escape such a collision
          without severe injury. A properly worn seat belt keeps the second
          collision - the collision of your body with the interior of the car -
          from happening. This assumes of course, that your body is not ejected
          from the car.
        </p>
        <h3>What is "Worn Properly"?</h3>
        <p>
          "Worn Properly" can mean the difference between life and death, quite
          literally. Seat belts are designed to absorb the impact of the
          collision and transfer that impact from your body. "Worn Properly"
          means both straps snugly fitted to the body *shoulder strap across the
          chest area and lap belt securely fastened across your hips* to avoid
          injury and provide safety.
        </p>
        <h3>Is There a "Good Reason" Not to Wear a Seat Belt?</h3>
        <p>
          "I'm a good driver." Even if you have a perfect driving record and
          have never been involved in a traffic collision, it is not a sound
          reason to avoid wearing your seat belt to insure safety.
        </p>
        <p>
          Being a good driver can be a determining factor in your ability to
          avoid accidents, but there are many other drivers on the road who are
          not as careful. Poor drivers can cause accidents with even the best of
          drivers. In most automobile vs. automobile accidents, there is a
          "good" driver who is "not at fault".
        </p>
        <p>
          "I'm just going to the grocery store." Nearly 80% of all{" "}
          <Link to="/wrongful-death">FATAL traffic accidents</Link> occur within
          25 miles of the victims' residence and at speeds of under 40 mph. So
          when you are "just going to the grocery store", is the best time to
          wear a safety belt.
        </p>
        <p>
          "My car has airbags, so I don't need to wear my seat belt." Airbags
          are not meant to take the place of seat belts, they were meant to
          enhance their safety. An air bag increases the effectiveness of a seat
          belt by up to 40 percent. Airbags are an ADDED safety feature and
          unless you car has side airbags as well as front airbags, they don't
          protect against side impacts at all.
        </p>
        <p>
          "They're uncomfortable." There are many ways to make your seat belt
          more comfortable. At the local dealer or auto parts store you can find
          clips and extenders to customize the fit of your seat belt. You can
          also buy adapters for children to make sure their seat belts fit
          properly for maximum protection. Seat belts are much more comfortable
          than hospital beds.
        </p>
        <p>
          Let's face it, there is no such thing as a "good reason" not to wear a
          seat belt while traveling in your vehicle, either to the corner store
          or across the country. Seat belts save thousands of lives each year
          and reduce the severity of injuries.
        </p>
        <p>
          {" "}
          If you've been involved in an auto accident that was caused by a seat
          belt or defective safety belt issue, call our experienced{" "}
          <Link to="/auto-defects">auto defect attorneys</Link> for a free
          consultation or fill out our free case review request form. Call
          1-800-561-4887.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
