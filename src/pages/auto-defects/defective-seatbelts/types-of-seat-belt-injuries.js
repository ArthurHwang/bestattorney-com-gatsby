// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}
export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/seat-belt-injuries-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lawyers Representing Seat Belt Injury Cases - (800)561-4887"
        pageDescription="Experienced auto product liability lawyer at Bisnar Chase discusses types of injuries caused by seat belts and seat belt failure. Free consultation at (800)561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Types of Injuries Caused by Seat Belts</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Seat belt injury attorneys"
            title="Seat belt injury attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Seat belts are a crucial part to saving lives in the event of a car
          crash, but when they fail to do their job, catastrophic results are
          many times inevitable. If you have experienced an injury or have lost
          a loved one due to seat belt failure, contact our{" "}
          <strong>California Seat Belt Injury Attorneys</strong> at{" "}
          <strong>800-561-4887</strong>.
        </p>
        <p>
          Our <strong>auto defects attorneys</strong> and senior partner,{" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link> discusses the
          issues in the auto industry of putting profit before safety. The
          following is a list of injuries most commonly associated with faulty
          or defective seat belt restraints suffered in automobile accidents.
        </p>
        <ul>
          <li>Traumatic Brain Injury</li>
          <li>Closed Head Trauma</li>
          <li>Other types of Head and Brain Injuries</li>
          <li>Spinal Cord Injuries</li>
          <li>Paraplegia</li>
          <li>Quadriplegia</li>
          <li>Death </li>
        </ul>
        <h2>Seat Belt Statistics</h2>
        <p>
          With the exception of seat belt defects, properly wearing a seat belt
          increases the chance of survival in a car accident. The following list
          compiles statistics about people who do and people who do not wear
          seatbelts.
        </p>
        <ul>
          <li>
            Wearing your seat belt reduces the risk of fatal injury to front
            seat passengers by 45% and risk of moderate-to-critical injury by
            50%
          </li>
          <li>
            Rear car seat belts in SUV's are 73% better at preventing fatalities
          </li>
          <li>
            Victims are not properly restrained in more than one-half of all
            fatal car accidents
          </li>
          <li>78.7% of people involved in a car accident wore seat belts</li>
          <li>
            48.1% of people in fatal car accidents were not wearing seat belts
          </li>
          <li>Of those who were wearing a seat belt 76,452 were not injured</li>
          <li>
            Of those who were not wearing a seat belt 4,605 were not injured
          </li>
          <li>Since 1994, drivers use seat belts 16.5% more often</li>
        </ul>
        <p>
          So make sure to buckle up and stay safe in the event of a car
          accident. It could just save your life.
        </p>
        <p>
          For more information on seat belt statistics you can visit{" "}
          <Link
            to="https://www.cdc.gov/motorvehiclesafety/seatbelts/facts.html"
            target="new"
          >
            cdc.gov
          </Link>
          .
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Child Car Seat Safety"
            style={{
              backgroundImage:
                "url('/../images/text-header-images/child-car-seat-safety.jpg')"
            }}
          >
            <h2>Child Car Seats</h2>
          </div>
        </LazyLoad>
        <p>
          As parents and individuals making sure children are safely transported
          to and from the doctors, school or a day at the beach, we put our
          trust in <strong>Childrens Car Seats</strong>. Seat belts are the main
          support system that ensure a child's car seat is secure and stays in
          place in the event of a sudden maneuver or car accident. When a seat
          belt devices fails, the risk of serious injury or death to a child in
          a car seat is increased dramatically.
        </p>
        <p>
          To make sure your child's drive to the park, playhouse or school is a
          safe drive, here are some tips:
        </p>
        <ul>
          <li>
            Make sure the seat belts in your car are working properly. Have them
            checked out by a professional or dealership
          </li>
          <li>
            Check to see if your vehicle has any recalls by entering the VIN
            number at{" "}
            <Link to="https://vinrcl.safercar.gov/vin/" target="new">
              vinrcl.safercar.gov
            </Link>
            . You can find your VIN number on the lower driver side of the
            windshield, driver door or on your vehicles registration
          </li>
          <li>
            Ensure the car seat is secure and properly locked in place tightly
          </li>
          <li>
            Drive safely and avoid sudden turning, braking and accelerating if
            possible
          </li>
        </ul>
        <h2>Seat Belt Recalls</h2>
        <p>
          A defective seat belt can actually do more harm than good in the event
          of a car accident. Seat belts have been known to detach from their
          mounting points, rip or tear on impact, lock-in-clips breaking under
          pressure and other significantly dangerous situations.{" "}
        </p>
        <p>
          Automotive makers like Hyundai and Ford have dealt with massive seat
          belt recalls as have many other auto makers and seat belt
          manufacturers.{" "}
        </p>
        <p>
          At{" "}
          <Link
            to="https://automotiveoem.com/Sub-Category-Seat-Belts"
            target="new"
          >
            www.AutomotiveOEM.com
          </Link>{" "}
          you can view a list of the world's top seat belt manufacturers.
        </p>
        <h2>
          Many Secondary Injuries Are Also the Result of the Use of Seat Belts,
          Such as:
        </h2>
        <ul>
          <li>Contusions </li>
          <li>Cracked Sternum (sternal fracture)</li>
          <li>Organ Injury</li>
          <li>Aneurysm</li>
          <li>Burst Appendix</li>
          <li>Ruptured Spleen</li>
          <li>Vertebrae Injuries</li>
          <li>Soft-Tissue Damage</li>
        </ul>
        <p>
          Although the use of seat belts has been strongly promoted in the
          United States for years, many children and adults suffer from serious
          seat belt related injuries each year. Listed below are just a few of
          the injuries which may occur as the result of faulty or defective seat
          belts:
        </p>
        <ul>
          <li>Concussions</li>
          <li>Spinal Cord Injury</li>
          <li>Brain or Head Injury</li>
          <li>Paralysis</li>
          <li>Abdominal and Chest Injuries</li>
          <li>Thoracic Injury</li>
          <li>Bowel Injury</li>
          <li>Intestinal Injury</li>
        </ul>
        <p>
          For over three decades, many of the restrain system manufacturers and
          major vehicle makers in the U.S have been aware that certain seat belt
          systems are poorly made, and have the ability to unlatch in the event
          of an accident, causing serious injury, and even death.
        </p>
        <h2>Seat Belt Injury Representation</h2>
        <p>
          If you or a loved one has suffered from a faulty seat belt injury you
          should contact a skilled{" "}
          <Link to="/auto-defects/defective-seatbelts">
            seat belt injury attorney
          </Link>{" "}
          at Bisnar Chase right away. Our auto defect lawyers have the knowledge
          and experience to successfully represent clients in seat belt defect
          cases, as well as other auto part liability cases throughout the
          state. We will use our experience, knowledge and resources to achieve
          the best possible results for you and your family. Many of these cases
          are catastrophic and require long term medical care. Our team of
          experienced attorneys will fight for your right to fair compensation.
        </p>
        <h2>Some of Our Auto Defect Victories</h2>
        <p>
          $24,744,764 - Auto defect
          <br />
          $7,998,073 - Product liability - motor vehicle accident
          <br />
          $5,000,000 - Auto Defect
          <br />
          $3,075,000 - Product defect - motor vehicle accident
          <br />
          $2,500,000 - Govt. claim, product defect - motor vehicle accident
          <br />
          $2,600,000 - Auto Defect
          <br />
          $2,432,250 - Product liability - motor vehicle accident
          <br />
          $2,250,000 - Auto defect - motor vehicle accident
          <br />
          $2,000,000 - Defective seatback - motor vehicle accident
          <br />
          $1,900,000 - Auto Defect
        </p>
        <h2>Why Contact Bisnar Chase Today?</h2>
        <p>
          Due to statute of limitations, you only have a limited amount of time
          to legally file a claim for any injuries suffered from a seat belt.
          You can discuss your case with one of our attorneys today at no
          charge.
        </p>
        <p>
          If we accept your case,{" "}
          <strong>you pay nothing unless we win your case</strong>. Bisnar Chase
          will cover all medical and other fees at no cost to you. We have over
          39 years of experience and have established a 96 percent success rate
          for our clients.
        </p>
        <p>
          Our highly skilled and experienced team of California Seat Belt Injury
          Lawyers are available for immediate assistance.
          <br />
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
