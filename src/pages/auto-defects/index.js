// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import Img from "gatsby-image"
import { graphql } from "gatsby"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-auto-defect-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Auto Defect Attorney - Bisnar Chase - 800-561-4887"
        pageDescription="California Auto Defect Lawyers at Bisnar Chase have been winning cases for over 39 years. With a 96% success rate and over $500 Million won, we have a long list of satisfied clients we were able to help get their lives back.  Call us if you've been injured in a defective car accident."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1> California Auto Defect Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california auto defect lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong>California Auto Defect Lawyers </strong>at{" "}
            <strong>Bisnar Chase </strong>have been winning cases for over{" "}
            <strong>40 years</strong>.
          </p>
          <p>
            With a <strong>96% success rate</strong> and over{" "}
            <strong>$500 Million </strong>won, we have a long list of satisfied
            clients we were able to help get their lives back.
          </p>
          <p>
            Not every{" "}
            <Link to="/" target="new">
              personal injury lawyer
            </Link>{" "}
            specializes or has extensive experience in auto defect cases,
            especially those caused by serious defects like sudden unintended
            acceleration and seat-back failure.{" "}
          </p>
          <p>
            These auto defects can cause serious and catastrophic injuries that
            change a person or family forever. Our California auto defect
            attorneys have taken on big cases and won.
          </p>
          <p>
            Contact us today for a free case review to find out if you have a
            case. The advice is free and the outcome may be priceless.{" "}
          </p>
          <p>
            For a <strong>Free Consultation </strong>and{" "}
            <strong>Case Evaluation </strong>call <strong>800-561-4887</strong>.
          </p>
          <h2>A Strong Legal Team to Fight Auto Defects</h2>
          <img
            className="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase, auto defect attorney"
          />
          <p>
            It is important to remember that automakers are large corporations
            that have high-powered legal defense teams at their beck and call.
            Anyone who intends to fight these corporations cannot and should not
            do it alone.{" "}
          </p>
          <p>
            If you or a loved one has been injured by a defective auto part or a
            vehicle defect, you need a renowned California auto defect lawyer on
            your side who will fight for your rights every step of the way.
          </p>
          <p>
            You need a law firm that has the motivation and resources to conduct
            independent investigations and testing. You need a strong team of
            auto defect attorneys who are on a mission to hold wrongdoers
            accountable and make the world a safer place for consumers.
          </p>
        </div>

        <div className="snippet">
          <p>
            <span>
              {" "}
              <Link to="/attorneys/brian-chase" target="new">
                Brian Chase
              </Link>
              , Senior Partner:
            </span>
            <br />
            “I went to law school knowing I wanted to be a personal injury
            attorney. I wanted my life’s work to have a positive impact on other
            people’s lives. We have to hold big corporations responsible. It's
            the right thing to do.”
          </p>
        </div>
        <p>
          <strong>Some of Brian's Honors</strong>:
        </p>
        <ul>
          <li>2019 Million Dollar Advocate</li>
          <li>2019 Lawyer of the Year</li>
          <li>2019 Nation's Premiere Top Ten Attorneys by NAOPIA</li>
          <li>2019 America's Top 100 Attorneys</li>
          <li>2017 Top 1% by the Natl. Assoc. of Distinguished Counsel</li>
          <li>2017 Top 100 High Stakes Litigator</li>
          <li>2016 Top 10% Lawyers of Distinction</li>
          <li>2016 SuperLawyer</li>
          <li>2016 Best Lawyers</li>
          <li>2015 Consumer Attorneys of California President </li>
          <li>2015 Top 1% Lawyers by the NADC</li>
          <li>2014 Trial Lawyer of the Year for Product Defects</li>
          <li>2013 Top Attorney in Orange County</li>
          <li>California Supreme Court and Appellate Court Case Winners</li>
        </ul>

        <h2>Dangerous Auto Defects Need Reform</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/crash-test-california-auto-defect-attorneys.jpg"
            width="100%"
            height=""
            id="crash-test"
            alt="california auto defect attorneys"
          />
        </LazyLoad>
        <p>
          Automakers recalled over 250 Million cars in the past 2 decades for
          potentially fatal defects that could cause serious injuries or death
          at any time.{" "}
        </p>
        <p>
          Anyone who has been injured because of an auto defect like this
          deserves to be compensated by the car manufacturer that allowed the
          defect that caused their injuries to remain in their cars.
        </p>
        <p>
          2015 was a big year for recalls.{" "}
          <Link
            to="https://www.safercar.gov/rs/takata/"
            title="takata airbag recalls"
            target="new"
          >
            Takata airbags had the largest recall in history
          </Link>
          . Automakers recalled as many vehicles as they sold in the year 2012
          and 2015.{" "}
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.nhtsa.gov" target="new">
            U.S. National Highway Traffic Safety Administration (NHTSA)
          </Link>{" "}
          and other media reports:
        </p>
        <ul>
          <li>
            The auto industry sold around 14.5 million vehicles in 2012, which
            was a 13 percent increase over the previous year.
          </li>
          <li>
            They also recalled more than 14.3 million current and past vehicle
            models during the same year.{" "}
          </li>
        </ul>

        <p>
          This is an amazing statistic because it highlights the number of
          vehicles out on our roadways that are defective or have faulty parts
          in them. There are millions more vehicles that have not been recalled
          despite having defective parts in them.
        </p>

        <p>
          As vehicles are being recalled in larger numbers due to various
          manufacturing defects and design flaws, it is very important that
          consumers are aware of vehicle safety and defective auto recalls. That
          said, automakers have a duty of care to consumers to manufacture safe
          and reliable vehicles.{" "}
        </p>
        <p>
          Unfortunately, we have seen evidence for decades that automakers,
          sometimes deliberately, choose to disregard that duty and recklessly
          endanger the lives of millions of people who buy their vehicles and
          make them profitable as corporations.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california auto defect examples"
            style={{
              backgroundImage:
                "url('/images/text-header-images/examples-of-auto-defects-california.jpg')"
            }}
          >
            <h2>Examples of Defective Auto Parts</h2>
          </div>
        </LazyLoad>

        <p>
          Devastating injuries can occur whenever an auto part fails to work
          properly and a vehicle malfunctions. Automakers are required to put
          their vehicles through rigorous testing before putting them on the
          market.{" "}
        </p>
        <p>
          But, often, automakers put profits before consumer safety. As a
          result, they install substandard parts in the vehicle to boost
          profits.{" "}
        </p>
        <p>
          The quality of the auto parts and safety features in a vehicle
          significantly affect its crash-worthiness - which is the ability of a
          vehicle to protect its occupants in the event of a crash.
        </p>
        <p>
          <strong>
            Some of the most common vehicle defects that cause car accidents and
            spur recalls involve:
          </strong>
        </p>
        <ul>
          <li>
            <strong>Tires</strong>: A tire blowout can be extremely dangerous.
            When a defective tire undergoes tread separation at freeway speeds,
            the vehicle may veer out of control or roll over. In such cases it
            may be very difficult even for an experienced driver to regain
            control of the vehicle.
          </li>
          <li>
            <strong>Seat belts</strong>: Whenever a passenger is ejected from a
            vehicle, it must be determined if he or she was wearing a seatbelt
            that failed to work properly.
          </li>
          <li>
            <strong>Car seats</strong>: When an infant or booster seat is
            defective, there is the potential for serious injuries to young
            children.
          </li>
          <li>
            <strong>Seat backs</strong>: A flimsy or defective seat-back may
            collapse and cause the vehicle occupant to get thrown around in the
            vehicle. This often results in catastrophic injuries.
          </li>
          <li>
            <strong>Brakes</strong>: It is extremely dangerous to operate a
            vehicle that does not have properly working brakes.
          </li>

          <li>
            <strong>Accelerator pedals</strong>: There have been a number of
            recalls lately involving accelerator pedals that stick and remain
            depressed, causing sudden acceleration or unintended acceleration.
          </li>
          <li>
            <strong>Airbags</strong>: A defective airbag can either fail to
            protect the driver during an accident or suddenly inflate and cause
            serious injuries to vehicle occupants.
          </li>
          <li>
            <strong>Vehicle rollovers</strong>: Defective vehicle design could
            increase the propensity of vehicle rollovers. For example, 15
            passenger vans and older-model SUVs are particularly prone to
            rolling over because of their top-heavy design.
          </li>
          <li>
            <strong>Roof crush</strong>: Vehicles that rollover are meant to
            have strong roofs to resist the weight of the car so that the
            passengers inside are kept safe. When roofs fail it can result in
            major injuries or death.
          </li>
        </ul>
        <p>
          {" "}
          It is important for all California motorists to stay abreast of
          recalls and investigations. When there have been a number of similar
          complaints about a specific vehicle or part, the manufacturer may have
          to conduct an investigation.
        </p>
        <p>
          In some cases, the National Highway Traffic Safety Administration
          (NHTSA) will step in to conduct an independent investigation and/or an
          engineering analysis. If the investigation results in the
          determination that the auto part is defective, a recall will be
          initiated.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/sN9x5IOkGv0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>What Makes a Car Part Defective?</h2>
        <p>
          A defective product fails to execute the task it was designed to
          perform. There are many reasons why an auto part can become defective.{" "}
        </p>
        <p>
          Some parts have a defective design that makes them prone to breaking
          or malfunctioning. Parts that are poorly designed can fail even when
          they are assembled correctly.{" "}
        </p>
        <p>
          In other cases, auto parts are poorly built. Manufacturing defects
          typically involve a limited number of parts that were built at a
          specific plant for a short period of time when the required
          specifications were not followed.
        </p>
        <p>
          <strong>Proving That a Part is Defective</strong>
        </p>
        <p>
          It is absolutely vital to your case that your vehicle is preserved for
          a complete and thorough inspection. Resist the temptation to repair or
          get rid of your car.{" "}
        </p>
        <p>
          In fact, the vehicle is the most important piece of evidence in an
          auto defect case. An expert will be able to thoroughly analyze the
          vehicle for defects, malfunctions and other evidence when it is
          preserved in its original state unaltered.
        </p>
        <p>
          In order to have a successful auto defect claim against an auto
          manufacturer, you will have to prove that:
        </p>
        <ul>
          <li>The auto part was defective in some way;</li>
          <li>
            That the faulty product resulted in the accident and or injuries;
          </li>
          <li>
            You suffered a physical, financial or emotional loss as a result of
            the defect.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california auto defect lawyers in california"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-california-auto-defect-lawyers.jpg')"
            }}
          >
            <h2>Finding The Right Auto Defect Lawyer</h2>
          </div>
        </LazyLoad>

        <p>
          With over <strong>40 Years of Experience</strong>, we have collected
          over <strong>$500 Million</strong>, established a{" "}
          <strong>96% Success Rate </strong> and have the resources to take on
          very difficult cases.{" "}
        </p>
        <p>
          Senior partner, Brian Chase specializes in California auto defect
          cases as does his team of trial attorneys. We can pursue your case
          with experience and passion. With over 12,000 clients served and
          hundreds of millions in victories, Bisnar Chase is ready to fight for
          you.{" "}
        </p>
        <p>
          We've helped some pretty special people with serious auto defect
          cases.{" "}
          <Link to="/case-results/jaklin-romine" target="new">
            Jaklin Romine received $24.7 million
          </Link>{" "}
          for her defective driver's seat case.{" "}
          <Link to="/case-results/felipe-rojas" target="new">
            Felipe Rojas
          </Link>{" "}
          recovered a very large confidential settlement for a seatbelt failure
          after being hit by a drunk driver.
        </p>
        <p>
          {" "}
          If you need an attorney to take your case to another level, please
          consider <strong>Bisnar Chase</strong>. See all{" "}
          <Link to="/case-results" target="new">
            case results
          </Link>
          .
        </p>
        <p>
          Contact us today for a <strong>Free Consultation </strong>and{" "}
          <strong>Case Evaluation</strong> of your California auto defect case.{" "}
        </p>
        <p>
          Call <strong>1-800-561-4887</strong>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
