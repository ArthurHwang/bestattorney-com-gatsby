// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Defective Automobile Seatbacks - Dangerous Car Seat Failure"
        pageDescription="Dangerous seatbacks. Call 949-203-3814 for highest-rated personal injury and wrongful death attorneys, serving Los Angeles, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>The Dangers of Defective Car Seatbacks Exposed</h1>
        <BreadCrumbs location={location} />
        <p>
          Over the past decade, the media has widely covered instances of fuel
          system, airbag and seatbelt defects, but there is another dangerous
          defect that has not reached the public's attention. This defect is{" "}
          <Link to="/auto-defects/seatback-failure">car seatback failure</Link>{" "}
          which occurs during low- and moderate-speed rear-end collisions.
        </p>
        <p>
          Millions of vehicles on the road today have this defective seat design
          and allow thousands of otherwise preventable injuries to occur each
          year. Passengers in vehicles with defective seatbacks have sustained{" "}
          <Link to="/catastrophic-injury">catastrophic injury</Link>, brain
          damage and quadriplegia, all because the federal regulation governing
          the requirements for these car seats is inadequate.
        </p>
        <h2>Car Seat as Safety Mechanism</h2>
        <p>
          Automotive safety principles require that a vehicle be designed to
          reduce or eliminate the risk of injury in possible collisions and
          impacts, and car manufacturers are liable to consider their model's
          ability to withstand collision despite the fact that it is not the
          intended use of the automobile. The structure of the vehicle itself
          must protect passengers, but crash safety relies more upon adequate
          occupant retraining devices.
        </p>
        <p>
          The vehicle seat plays a significant role in a passenger's safety as
          it performs the same function in rear-end collisions as a seatbelt
          does for frontal collisions: to prevent the occupant from being thrust
          rearward or ejected from the vehicle altogether. Automobile seats have
          been called "the most important single lifesaving device available"
          although manufacturers are permitted to produce defective car seats
          under weak and ineffective Federal Motor Vehicle Safety Standard 207.
        </p>
        <h2>FMVSS 207</h2>
        <p>
          No standards controlled the design and performance of automobile seats
          before the 1960s and the basic car seat design created in the
          mid-1930s is what was familiar in later, mid-century cars. Minimum
          standards for car seat performance were not improved upon since this
          early design. By 1966, an initial standard finally required car seats
          to protect a load equal to thirty times the weight of the seat, and to
          prove so in tests, but this was quickly changed to only twenty times
          the weight of the seat after General Motors submitted a critique of
          the standard. This became the standard applicable to multipurpose
          passenger vehicles, trucks and buses, and was soon adopted in Europe
          as well.
        </p>
        <h2>Car Seat Failure</h2>
        <p>
          The standard may sound efficient, but FMVSS 207 has been widely
          criticized for its lack of safety since its inception. It shows no
          improvement to earlier designs of seats and is even weaker than some
          seats produced in the 40s and 50s. The standard is static, not
          dynamic, and most auto manufacturers have internal standards that are
          better than FMVSS 207, though they are all insufficient to prevent
          seat failure in reasonably foreseeable rear-impact collisions.
        </p>
        <p>
          Even testing required by FMVSS 301, a standard for fuel system
          integrity, exposes the inadequacy of 207. As rear impact collisions of
          30 mph are tested for 301, almost all bucket and split bench seatbacks
          fail and strike rear seats.
        </p>
        <p>
          Safety experts spoke out with respect to car seat defects decades ago,
          explaining "high-speed impacts may force the front seat passenger up
          the plane of the back-rest to whiplash him or break the seatback,
          releasing him to the rear seat area or even out the rear door,
          notwithstanding the use of a lap belt; neither of these extremes
          represent acceptable or satisfactory solutions to a serious and very
          frequently occurring type of accident."
        </p>
        <p>
          Tests involving anthropomorphic dummies have exposed the total danger
          of car seatback failure where a permanently deformed seatback was left
          on the scene of a crash where the dummies in the vehicle sustained a
          variety of serious injuries, even when wearing seatbelts.
        </p>
        <h2>Car Seat Failure</h2>
        <LazyLoad>
          <img
            src="/images/seatback-failure-300x162.jpg"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          The key issues of defective car seat systems, as summarized by safety
          experts, are as follows:
        </p>
        <ol>
          <li>
            Loss of vehicle control by a driver when seatback collapses during
            rear impact.
          </li>
          <li>
            Reduced effectiveness of restraint system when seatback collapses
            and allows the front seat passenger to slide rearward and impact
            rear seats, objects and passengers.
          </li>
          <li>
            Ejection of occupants who have slipped out from collapsed restraint
            systems as a result of car seatback failure.
          </li>
          <li>
            Risk of injury posed to rear passengers who will be struck by
            rearward motion of collapsed front seats and passengers.
          </li>
          <li>
            Injury to rear passengers whose bodies may be trapped under the
            deformed plastic of collapsed front seats.
          </li>
          <li>
            Injury to restrained front seat passengers during frontal impact
            when seats collapse from rear loading of lap belted or unrestrained
            rear seat passenger.
          </li>
        </ol>
        <p>
          The consequences of car seat failures are serious injuries and
          sometimes fatal. Estimates pose that in 1990 alone, 1,100 people died
          and 1,600 more suffered catastrophic injury because of defective car
          seatbacks collapsing in rear-end collisions.
        </p>
        <h2>Car Seat Design Solutions</h2>
        <p>
          Experts have been proposing new, safer car seat designs for a long
          time. One example is a rigid seatback design with head support
          structures that are sufficient to restrain a motorist in a normal
          seated posture during a collision. Rigid seats stay in an anchored
          position despite impact, and allow the occupant to remain within the
          seat restraints.
        </p>
        <p>
          In 1970, a Ford Motor Company engineer predicted: "As measures are
          developed to provide better restraint of the head, seatback strength
          will probably be increased to better retain the occupant in his seat.
          The intent, in this instance, would be to prevent his contact with
          rear seat occupants. The degree of increase and resistance to rearward
          bending may be anywhere from two or three times, to more than ten
          times the current level."
        </p>
        <p>
          Despite recommendations to improve safety over the last few decades,
          occupants in contemporary automobiles are continuously injured because
          of the low safety standards of the ineffective seats that have been
          produced.
        </p>
        <p>
          Auto manufacturers will continue to resist efforts to eliminate the
          defects in seats in most vehicles people drive today. The requirements
          in FMVSS 207 must be changed in order to prevent catastrophic and
          fatal injuries to occupants. Pressure from competitors, defective
          product lawyers and auto safety experts will hopefully force
          manufacturers to adhere to safer designs and promote a necessary
          standard of safety that even governmental safety standards have failed
          to require.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
