// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Car Seat Back Testing - Auto Defects"
        pageDescription="Car Seat back failure warning. In this Fox News report, see how car seat back testing is failing and leaving people exposed to this auto defect."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Seat Back Testing Gets an F</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/defective-seatbacks-brian-320.jpg"
          alt="Car Seat Back Testing Gets an F"
          className="imgright-fluid"
        />

        <p>
          The National Highway Traffic Safety Administration (NHTSA) standards
          for car seat backs have not changed since 1967, when cars were built
          much stronger and roomier. These standards are weak and allow even the
          flimsiest seats to pass their tests.
        </p>
        <p>
          In addition, as discussed recently on Fox 11 News Los Angeles in their
          report titled{" "}
          <Link to="/auto-defects/seatback-failure">
            The Dangers of Defective Car Seatbacks Exposed
          </Link>
          , car seat back testing is insufficient and does not represent
          real-world scenarios. Testing standards do not put car seat backs
          through a rigorous battery of scenarios. Seat belts, air bags, and
          roofs all have a minimum level of safety that must be achieved before
          the NHTSA will approve them for use. There is no such test for car
          seat backs.
        </p>
        <p>
          NHTSA tests dramatically illustrate how a front seat may collapse in a
          rear-impact car crash and still meet the performance requirements of
          Federal Motor Vehicle Safety Standards (FMVSS) 207. GM, Ford, and
          other U.S. car companies, are aware that their car seats will collapse
          in rear-end crashes where the cars are traveling at speeds as little
          as 20 mph.
        </p>
        <p>
          When a car seat collapses, the occupant may be thrust into the back
          seat of the vehicle. The passenger's head may strike the back seat, or
          other occupants, and cause severe injuries to everyone involved. Many
          children have died as a result of internal injuries when the front
          seat passenger's head rammed into their small bodies.
        </p>
        <p>
          The goal for car makers is to design vehicles that enhance passenger
          safety, not ones that create or aggravate injuries when involved in a
          car crash. If your car is involved in a front- or side-impact crash,
          your forward movement is stopped by your seat belt and your car's air
          bags. Assuming both are working properly, if you are hit from behind,
          there is nothing to stop your body from flying backward. Your head
          could strike the back seat, you could slip out of your safety harness,
          or you could snap your neck from being thrown around the car.
        </p>
        <p>
          Car seats that keep occupants in an upright position will prevent most
          head and{" "}
          <Link to="/head-injury/spinal-cord-injuries">
            spinal cord injuries
          </Link>
          . If the car seat back collapses, the occupant can be ejected or lose
          control of the vehicle and be exposed to otherwise avoidable multiple
          injuries. The occupant can also be hurled into the vehicle's rigid
          interior structures or other occupants.
        </p>
        <LazyLoad>
          <img
            src="/images/auto-defects/car-seat.jpg"
            className="imgright-fluid"
          />
        </LazyLoad>
        <p>
          The collapsed car seat back can make it difficult for crash victims to
          get out of the car, an especially hazardous defect when a fuel system
          has ruptured and a vehicle is on fire. In real world, low-speed,
          rear-impact crashes, flimsy car seat backs have failed to provide
          adequate protection. Fully investigated "fender bender" cases
          dramatically demonstrate that car seat back failures in low-impact
          accidents have resulted in severe or fatal injuries. Poorly designed
          adjustable head restraints add to the hazard because they can be
          adjusted to lie flush with the top of the seat back, allowing the
          occupant's heads to pivot over the headrest. This can cause severe
          spinal injury that could lead to paraplegia and quadriplegia.
        </p>
        <p>
          The best-known car seat back designs are in the Chrysler Sebring
          convertible and the Mercedes SL series convertibles. Both vehicles
          have front seats that are strong, and remain upright in rear-end
          collisions. There is also no increase in injury due to whiplash from
          stiff seats. The risk of whiplash is often the reason auto
          manufacturers claim they should not change the designs of their car
          seats. They claim that if they make them more solid, it will increase
          whiplash incidents.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
