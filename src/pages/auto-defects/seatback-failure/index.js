// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "auto-defects/Seat Back failure Banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Seat Back Failure Lawyers - Bisnar Chase"
        pageDescription="Call 949-203-3814 for highest-rated seat-back failure Lawyers in California. Specialize in catastrophic & serious injury cases. No win, no-fee attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Seat Back Failure Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California seat back failure lawyers"
            title="California seat back failure lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Contact the experienced{" "}
          <strong>California Seat Back Failure Lawyers</strong> of{" "}
          <Link to="/">Bisnar Chase</Link> if you've been seriously injured in a
          seat-back failure crash. We've represented more than 12,000 clients
          and won over{" "}
          <strong>$500 million dollars in verdicts and settlements</strong>.{" "}
        </p>
        <p>
          Our firm has extensive experience when it comes to handling these
          seat-back failure cases. Bisnar Chase has pioneered the way for seat
          back failure cases and we may be able to help you too.{" "}
        </p>
        <p>
          <strong>Call 800-561-4887 for a free case review</strong> with an
          experience California seat back failure attorney that has tried these
          cases to victory.
        </p>
        <h2>What is Product Liability?</h2>
        <p>
          A consumer that suffers at the hands of a manufacturer or retailer can
          file a claim for negligence. There is not a law set in place for
          product liability in all states. Since there is not one standard to
          abide by, accountability can vary from state to state. California has
          a &ldquo;strict liability&rdquo; law for defective products. Strict
          liability can apply to three different categories which include design
          defects, warning defects and manufacturing defects.{" "}
        </p>
        <p>
          To strengthen your product liability claim you need to have evidence.{" "}
        </p>
        <p>
          <strong>
            Your product defect claim can be better supported if it contains
            evidence of
          </strong>
          :{" "}
        </p>
        <ul>
          <li>The product upon sale being defective</li>
          <li>The product was defective when it left the sellers possession</li>
          <li>
            The consumer used the product in a reasonable way and still suffered
            injuries
          </li>
        </ul>
        <p>
          Food poisoning can also be considered a &ldquo;product defect&rdquo;
          in California. {" "}
        </p>
        <h2>Seatback Failure and Product Liability</h2>
        <p>
          Defective brakes, tires and airbags are the most common auto claims
          brought to the courts. Seatback failures, although rare, end up almost
          every time with a deadly or paralyzing outcome.  What is more shocking
          is that many legal representatives say that the auto industries
          downplay the severity of seatback failures.
        </p>
        <p>
          Seatback failure can occur by a car being rear-ended in the lowest of
          speeds such as 25mph. Many injury attorneys have stressed the
          importance to automakers that the issue of seatback failure should be
          handled upon a driver getting behind the wheel.{" "}
        </p>
        <p>
          Trial Lawyer Brian Chase weighs in on how the industry experts
          misconstrue certain facts about seatback failure.
        </p>
        <p>
          Chase states &ldquo;When you combine the results of this crash-test
          data along with one of the industry's top experts being caught
          red-handed &ldquo;fudging&rdquo; in his &ldquo;authoritative&rdquo;
          research it conclusively concludes that the auto industry is flat out
          wrong, and, at best, misguided – at worst, lying to us.&rdquo;
        </p>
        <p>
          Legal experts suggest doing your research on the safety and
          reliability of a car before your purchase.
        </p>
        <h2>Why Seat Back Failures are So Dangerous</h2>
        <LazyLoad>
          <img
            src="/images/seatback_failure.png"
            width="300"
            className="imgleft-fixed"
            alt="Seatback lawyers in California"
          />
        </LazyLoad>
        <p>
          The main safety features in a vehicle are the safety belt and airbag
          system. If the seatback collapses, neither of these critical safety
          features will be effective in the event of a crash. The airbag will
          likely not protect the victim at all and the seatbelt may not be able
          to prevent the occupant from being ejected. In some cases, the lowered
          chair can act as a ramp, causing the victim to be jettisoned from the
          rear window.
        </p>
        <p>
          <strong>
            Other potentially serious conditions that may arise include
          </strong>
          :
        </p>
        <p>
          <strong>Loss of control</strong>: If the seatback fails while the
          vehicle is in motion, the driver may be unable to sit upright. This
          situation can make it difficult for the driver to avoid an accident
          before pulling over and coming to a stop.
        </p>

        <p>
          <strong>Vehicle ejection</strong>: During a high-speed collision, a
          seatback failure can result in partial or complete ejection of the
          occupant from the vehicle.
        </p>
        <p>
          <strong>Rear-passenger injuries</strong>: If there is an occupant in
          the back seat at the time of the failure, he or she may be crushed by
          the seat or the front seat occupant. A collapsed seat can also trap
          the rear passenger resulting in catastrophic and even fatal injuries.
        </p>
        <p>
          <strong>Flying head rest</strong>: A defective headrest, in addition
          to failing to protect the head of the occupant, can also become a
          deadly flying projectile in the event of a high-speed collision.
        </p>
        <p>
          <strong>Blocked exits</strong>: Vehicles are full of combustible
          fluids and gases. In the event of a car fire, a defective seat can
          trap the occupants inside the vehicle preventing them from escaping to
          safety.
        </p>
        <p>
          <strong>Blunt force trauma</strong>: A vehicle occupant can get thrown
          around in the vehicle during a crash. This could result in severe head
          and spinal injuries, which could leave victims with permanent injuries
          and disabilities.
        </p>
        <h2>Causes of Seat Back Failure</h2>
        <p>
          Seatback failure occurs when the weight of a vehicle occupant forces
          the back of the seat to collapse. This can result from a faulty
          design, poor assembly or improper installation. If strong materials
          are properly assembled, it is much more likely that the seat will stay
          upright during a collision. When flimsy materials are used, the
          seatback frame may become deformed.
        </p>
        <p>
          The two most common types of seatback collapse involve the failure of
          the seatback support system to maintain an upright position and the
          deformation of the seatback frame. Failures that have been attributed
          to the deformation of the mounting system that holds the seat to the
          vehicle floor or to a collapse of the vehicle floor pan are less
          common. Another common problem with seatbacks is the detachment of
          adjustable head restraints during an accident.
        </p>
        <h2>Rear-End Collisions</h2>
        <p>
          Rear end crashes are characterized as a car being hit from behind.
          Rear end crashes can cause an abundance of issues and the consequences
          can be grave if there are defects in the seatbelt or if there is a
          seatback failure. California law states that the reason why you should
          obtain a 4-second distance between cars is to avoid rear-ending
          vehicles.
        </p>
        <h2>Tragic Avoidable Loss &amp; Injury</h2>
        <p>
          We represented the mother of a 7-year-old girl who died as the result
          of seatback failure. The plaintiff and her daughter were rear-ended at
          25 mph while traveling in a Ford Escort. As a result of the seat back
          failure, the mother's head struck the daughter in the chest, causing
          the young girl's heart to rupture. She also suffered from internal
          bleeding.{" "}
        </p>
        <p>
          We represented another young client who is paralyzed from the chest
          down due to seatback failure. You can{" "}
          <Link to="https://youtu.be/wQ0uauw5neY" target="_blank">
            watch their stories here
          </Link>{" "}
          along with an interview with{" "}
          <Link to="/auto-defects" target="_blank">
            auto defects
          </Link>{" "}
          attorney, Brian Chase explaining what took place when these car seats
          failed.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wQ0uauw5neY"
            frameBorder="0"
            style={{ marginBottom: "0" }}
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center style={{ marginBottom: "2rem" }}>
          <blockquote>
            Trial attorney Brian Chase, shares the dangers drivers face when
            they ride with defective car seats
          </blockquote>
        </center>
        <h2>Defective Seatback Recalls</h2>
        <p>
          When a number of people have been injured in seatback failure
          accidents, the{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            National Highway Traffic and Safety Administration
          </Link>{" "}
          (NHTSA) will likely conduct an investigation into the safety of the
          vehicle and determine if a recall is warranted. When a seat is deemed
          defective, the auto manufacturer may have to issue a recall.
        </p>
        <p>
          In March of 2013, Ford recalled 230,000 Freestar and Mercury Monterey
          minivans that have third-row seats that could come lose because of
          rust issues.
        </p>
        <p>
          In June of 2013, Tesla recalled Model S vehicles for faulty rear seat
          latches that allowed the seat to move during a crash.
        </p>
        <p>
          Nissan recalled 11,000 Juke crossovers from model year 2012 for an
          incomplete welding issue that allowed the rear seatback to become
          loose in an accident.
        </p>
        <h2>Faulty Seatbelts</h2>
        <p>
          Over three million people each year suffer from severe injuries caused
          by defective seatbelts. Research has also concluded that an estimated
          40,000 people die from faulty seatbelts. When a car crashes into a
          driver, upon impact the driver relies on their seat-belt for
          protection. Seatbelts act to lessen the blow of a car collision.{" "}
        </p>
        <p>
          If a seatbelt is defective, injuries sustained can include serious
          chest and head injuries. There are five kinds of instances that can
          cause your seatbelt to be defective.
        </p>
        <p>
          <strong>5 types of seatbelt defects</strong>:{" "}
        </p>
        <ol>
          <li>
            <strong>Unlatching</strong>: Some seatbelts unlatch during a crash
            because of the great force of the impact. This has to do with
            certain types of seatbelts. Automakers claim that this is not a
            common occurrence.
          </li>
          <li>
            <strong>Inadequate latching</strong>: Seatbelts can appear to be
            perfectly fastened but there are occasions where a seatbelt would
            unlatch in a small car crash and cause a driver to be unprotected.{" "}
          </li>
          <li>
            <strong>Defects in seatbelt webbing</strong>: The material of a
            seatbelt should be able to withstand any rips. There are instances
            though where the seatbelt fabric can rip during a vehicle collision.{" "}
          </li>
          <li>
            <strong>Seatbelt failing to retract</strong>: The purpose of a
            seatbelt is to make sure you're secure in a sedentary position. If
            your seatbelt fails to retract this means that your seatbelt can be
            loosened at the time of an accident and you can potentially be
            thrust out of your seat.{" "}
          </li>
          <li>
            <strong>Improper installation</strong>: When installed, seatbelts
            must be anchored well to protect the driver and their passengers. If
            a seatbelt is not mounted correctly then a person in the car can be
            severely injured and can even die.
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Seat back failure attorneys in California"
            style={{
              backgroundImage: "url('/images/text-header-images/gavel.jpg')"
            }}
          >
            <h2>Federal Safety Standards</h2>
          </div>
        </LazyLoad>
        <p>
          Federal safety standards relating to automobile seatbacks have not
          been updated in a long time and are way too lenient. According to
          safety experts, who have studied seatback safety for decades, the
          seats in most of our automobiles are no better than lawn chairs.
          Automobile seats are only required to pass a strength requirement.
          They don't have to go through a crash test rating system although
          seatbacks routinely fail during NHTSA's 30-mph rear-impact crash tests
          (FMVSS 301).
        </p>
        <p>
          {" "}
          <Link
            to="https://www.law.cornell.edu/cfr/text/49/571.207"
            target="_blank"
          >
            FMVSS 207
          </Link>
          , the federal rule pertaining to seating systems in automobiles, has
          not been updated in more than three decades. This rule mandates that a
          seat must withstand a pull of 3,300 inch-pounds applied on the
          seatback in a rearward direction. Despite numerous petitions over the
          years to revise this standard, there has been no action on the part of
          NHTSA. The seatback strength in most vehicles needs to increase
          six-fold to provide a reasonable degree of safety to vehicle occupants
          in the event of a crash.
        </p>
        <h2>Important Facts about Seatback Failure</h2>
        <p>
          The National Highway Traffic Safety Administration (NHTSA) received
          390 complaints regarding seatback failure in one year resulting in 112
          injuries and one death. Many of these injuries could have been
          prevented had the auto manufacturer designed and manufactured these
          seatbacks using technology that has been available for decades now.
        </p>
        <p>
          There are several vehicle models in which seatback failure is a
          routine occurrence and automakers are aware of these serious defects:
        </p>
        <ul>
          <li>
            Ford Motor Company has been aware of dangerous seats for over 30
            years. This deadly problem was investigated by Ford in 1992, and the
            outcome was that the seats cause serious harm in the event of
            rear-end collisions. Ford did not make any design changes to combat
            the seriousness of the seat back failure. Our law firm has seen
            first-hand evidence of weak seatbacks in the Ford Escort and Ford
            Explorer models.
          </li>
          <li>
            Virtually every front seat produced by General Motors Corporation
            from 1970 to the mid 1990's was designed to collapse rearward in an
            impact in which there was a speed change of 15 mph or greater.
            According to documents obtained by CBS for its series on seats
            collapsing in rear-end collision in 1992, GM attorneys advised top
            executives that there standard seats could no longer be defended.
          </li>
          <li>
            GM knew, as early as 1966, that seat strength is directly related to
            occupant safety in a rear impact collision. They have known that the
            occupant survival depends largely upon a front seat structure that
            holds the passenger in an upright position, and yet, they have not
            upgraded their seats. A leading GM engineer, David C. Viano, in a
            1994 internal GM study, projected that 376 to 470 lives could be
            saved each year and estimated that improvements would prevent 1,000
            serious injuries each year in rear-end collisions if the company
            strengthened its seat backs.
          </li>
          <li>
            DaimlerChrysler has been known to have seats collapse as well. In
            2001, a mother, one of our clients, was driving one of the
            manufacturer's minivans and was ejected from her seat during a crash
            due to a seat back collapse. The collapsed seat caused the mother to
            fatally strike her 8-month old child, who was in a car seat behind
            her. In 1999, a woman was rendered quadriplegic after an accident in
            which the driver's seat recliner bracket of a DaimlerChrysler Dodge
            Ram Pickup broke. This caused the seat to collapse backwards and
            eject the woman out of her seat, striking her head on the roof of
            the cab. This structural weakness exists in all 1995, 1996, and 1997
            Dodge Ram extended cab models.
          </li>
          <li>
            Cars equipped with bucket seats and/or split bench seats have a
            higher risk for seat back failure. These models have been known to
            bend or collapse in rear-end collisions. They are unable to safely
            withstand impacts, even in a collision occurring at a speed as low
            as a 15 mph.
          </li>
        </ul>
        <h2>Contact an Experienced Seat Back Failure Attorney</h2>
        <LazyLoad>
          <img
            src="../../images/bisnar-chase/seatback-failure-injury-lawyers.jpg"
            width="332"
            className="imgleft-fluid"
            alt="California seatback failure attorneys"
          />
        </LazyLoad>
        <p>
          Auto accidents pose a threat for serious injuries such as fractures,
          head injuries, internal organ damage, scarring, disfigurement and
          spinal cord injuries. We help victims who have been injured or killed,
          secure fair compensation for medical bills, funeral costs, lost wages
          and all other related damages.
        </p>
        <p>
          The <strong>Seat Back Failure Lawyers</strong> of Bisnar Chase have
          over four decades in personal injury law including serious and
          catastrophic auto defect cases. We have the talent, funds and
          reputation to take your auto defect or product malfunction case to the
          finish line.
        </p>
        <p>
          Seat back failure is a serious vehicle defect in many automobiles that
          causes numerous catastrophic injuries and fatalities each year. Poorly
          designed and defective seats, for example, can collapse backward on
          impact resulting in devastating injuries to vehicle occupants.{" "}
        </p>
        <p>
          If you or a loved one has been impacted by a defective auto part such
          as a failed seat-back, faulty seatbelt, or defective air bag call our
          office to discuss your case. You may be entitled to compensation. Our
          experienced auto defect trial lawyers are compassionate and highly
          skilled in the field of defective products.
        </p>
        <p>
          {" "}
          <strong>
            Call 800-561-4887 for a free comprehensive consultation.{" "}
          </strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
