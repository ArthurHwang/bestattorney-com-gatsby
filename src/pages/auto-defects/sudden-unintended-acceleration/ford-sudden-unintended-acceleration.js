// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Toyota Sudden Unintended Acceleration  Reminiscent of Ford Explorer Defects"
        pageDescription="Toyota sudden acceleration reminiscent of Ford Explorer defects. he complaints made by Toyota and Lexus drivers of sudden unintended acceleration has been largely ignored by the car manufacturer."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Toyota Sudden Unintended Acceleration Problem is Reminiscent of Ford
          Explorer Defects
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The complaints made by Toyota and Lexus drivers of{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            sudden unintended acceleration
          </Link>{" "}
          (SUA) has been largely ignored by the car manufacturer over the last
          eight years. Any attempts to treat the issue through vehicle repairs
          in recalls -- six recalls since 2005 -- have dealt strictly with
          mechanical root causes like sticking accelerator pedals, and has
          failed to address possible electronic defects or that there is likely
          a variety of contributing defects that cause SUA.
        </p>
        <p>
          SUA has finally been thrust to the forefront of news as more consumers
          experience the frightening inability to control their vehicles, more
          car accidents occur, and no reliable repair has been found.
        </p>
        <p>
          The predicament Toyota finds itself in, with SUA occurring in many of
          its makes and models over many years, is reminiscent of the Ford
          Explorer and Firestone tire defects which flooded the news about a
          decade ago. In the case of the Explorers there were, too, a variety of
          design and manufacturing defects which contributed to the failure of
          the Firestone tires and the rollover of the Explorer.
        </p>
        <p>
          Explorers were a best-selling SUV, much like certain models of Toyota
          are widely used and popular vehicles. The reluctance of both Ford at
          the time of the Explorer defects, and Toyota now with SUA, to address
          all of the possible contributing factors and{" "}
          <Link to="/auto-defects">auto defects</Link> in a timely manner is
          disappointing.
        </p>
        <p>
          The Early Warning Reporting (EWR) system was implemented during the
          Ford Explorer recalls and was intended to detect consumer complaints
          at an early stage. It has showed SUA claim rates rising, though little
          attention has been paid by car manufacturers to these numbers until
          recently.
        </p>
        <p>
          SUA can occur when the vehicle is maneuvered in a variety of ways; at
          low speeds while parking with a foot on the brake, while maintaining a
          consistent speed on a highway, and many other situations have been
          reported. It also can occur during different driving conditions.
        </p>
        <p>
          Though a complex issue, safety precautions should have been taken long
          ago to install brake-to-idle features across all makes and models of
          Toyota vehicles. This would allow the driver to override any SUA. As
          of yet, Toyota has only acted to install this feature on some Camry
          models.
        </p>
        <h3>See Related Articles:</h3>
        <p>
          {" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            Toyota Sudden Unintended Acceleration
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-floor-mat-recalls">
            Toyota Recalls Almost 4 Million Vehicles For Floor Mat Entrapment
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/sua-complaints">
            Consumer Complaints About Sudden Unintended Acceleration (SUA) in
            Toyotas
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/sua-claims-lives">
            Sudden Unintended Acceleration Claims Lives
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
