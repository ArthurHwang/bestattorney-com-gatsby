// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Avoid Auto Defects When Buying a Used Car - If Your Used Car is Defective"
        pageDescription="before you purchase a used car research the vehicle for any auto defects that could put your safety in jeopardy. If the used car is defective seek legal help."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fighting Auto Manufacturers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been injured in a Toyota sudden acceleration accident in
          California, contact our product liability attorneys to discuss your
          case.
        </p>
        <p>
          Our firm has gone after and won against vehicle manufacturers that put
          money before safety. We have successfully litigated auto defect cases
          and have the experience and resources to take on these expensive and
          complicated lawsuits.
        </p>
        <p>
          For over 35 years we have been helping victims seek justice
          <strong>. Call 949-203-3814 for a free consultation.</strong>
        </p>
        <p>
          If you suspect sudden acceleration has taken place, it is important
          that you <strong>preserve your vehicle.</strong> An experienced{" "}
          <Link to="/auto-defects">auto products liability attorney</Link> will
          be able to not only preserve your vehicle, which is the most important
          piece of evidence in your case, but also obtain vital information from
          the vehicle's event data recorder or "black box" to determine what
          exactly occurred.
        </p>
        <h2>Independent Testing and National Experts</h2>
        <p>
          The experienced and knowledgeable auto product liability lawyers at
          Bisnar Chase have many years of experience fighting large corporations
          including automakers on behalf of injured victims and their families.
          We perform independent testing of the vehicles utilizing leading
          national experts. We get to the core of the problem so we can
          determine the facts and help you obtain fair compensation for your
          losses.
        </p>
        <p>
          We are all too familiar with stalling techniques and strategies
          employed by large corporations and their legal defense teams. We have
          the courage, conviction and the tenacity to fight and emerge
          successful in these types of complex, challenging cases. We do not
          collect any fees until we have recovered compensation for you.{" "}
          <strong>Please contact us at 949-203-3814</strong> to obtain more
          information about pursuing your legal rights.
        </p>

        <h2>What Has Gone Wrong With Toyota?</h2>
        <p>
          Toyota vehicles are the top-selling brands in the United States. Over
          the decades, this Japanese automaker has worked on building a
          reputation for quality, efficiency and reliability. However, over the
          last few years, the automaker has been dealt a severe blow with
          several recalls involving more than 10 million vehicles for a problem
          known as "sudden or unintended acceleration." The problems involving
          mechanical and software issues included components such as sticky gas
          pedals and faulty floor mats.
        </p>
        <p>
          In many cases, sudden acceleration or loss of vehicle control led to
          serious injury or even fatal accidents. There were even instances
          where individuals received prison sentences for causing fatal
          accidents when in fact the drivers had lost control of their Toyota
          vehicles due to sudden acceleration. It is unclear how these
          accelerator issues have affected Toyota sales, but many experts say
          that the recalls involving sudden acceleration turned into a "PR
          nightmare" for Toyota tarnishing the image of the company, which had
          stood many years as the bastion of safety and reliability.
        </p>
        <p>
          In December 2012, Toyota agreed to pay $1.3 billion to settle
          multi-district litigation over financial damages associated with
          Toyota and Lexus acceleration problems. The attorney for the carmaker
          claims that the settlement is not an admission of wrongdoing but
          rather a way to turn "the page on this legacy legal issue." In 2010,
          Toyota paid nearly $49 million for three violations relating to
          stalling auto safety recalls.
        </p>
        <h2>What Exactly is Unintended Acceleration?</h2>
        <p>
          If your vehicle has ever sped up beyond your control, or accelerated
          when you weren't even pressing the gas pedal, it may mean that you
          have experienced unintended acceleration. There are many instances,
          however, where a defective auto part, mechanical malfunction, a
          software glitch or an electrical issue makes the vehicle accelerate
          beyond the control of the driver. This can be extremely dangerous
          because it is often impossible to stop the vehicle when it accelerates
          out of control. Many victims of unintended acceleration find
          themselves unable to slow down or stop until they crash. Often, this
          has devastating consequences because of the high speeds involved.
        </p>
        <h2>Toyota and Unintended Acceleration - A Timeline:</h2>
        <p>
          Since 2001, Toyota has allegedly struggled with unintended
          acceleration malfunctions resulting from electronic issues, poorly
          designed floor mats and sticky accelerators. Here is a timeline of the
          Japanese automaker's struggles with sudden acceleration:
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
