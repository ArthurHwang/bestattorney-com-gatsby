// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Toyota SUA Whistleblower - Betsy Benjaminson Personal Statement"
        pageDescription="Betsy Benjaminson was a translator working for Toyota when she came upon evidence that Toyota was covering up its' acceleration defects. She blew the whistle with this statement."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Toyota SUA Whistleblower - Personal Statement</h1>
        <BreadCrumbs location={location} />
        <h3>
          The following is a personal statement from{" "}
          <Link to="https://betsybenjaminson.blogspot.com/">
            Betsy Benjaminson
          </Link>
          , the professional translator in charge of translating Japanese
          documents from Toyota.
        </h3>{" "}
        <p>
          Benjaminson reviewed the documents that she was translating and
          realized that Toyota was covering up the problem of sudden unintended
          acceleration in many of their cars. She then blew the whistle on
          Toyota and went to the US congress with her knowledge, ultimately
          resulting in Toyota being forced to recall millions of vehicles, pay
          1.3 billion dollars in a className action lawsuit settlement, and pay
          1.2 billion dollars to the department of Justice.{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            You can read about the Toyota recalls here.
          </Link>
          This statement is directly from Benjaminson's facebook page in March
          2013, which is no longer online.
        </p>
        <h3>Personal statement of Toyota Whistleblower Betsy Benjaminson:</h3>
        <blockquote style={{ marginBottom: "2rem" }}>
          I am a professional translator. I have been translating from Japanese
          to English professionally since I lived and studied in Japan in the
          1970s. My decades of experience in this profession have earned me many
          important freelance translating assignments. In 2010 I started a
          freelance job editing about 1,500 internal documents from Toyota about
          unintended acceleration (UA).
        </blockquote>
        <h2>Finding the cover-up</h2>
        <p>
          Living on a remote farm in Israel, I had not heard of Toyota's
          unintended acceleration (UA) problem, although it was common knowledge
          in the U.S. I therefore had no preconceptions that might have
          influenced my judgment while reading these documents. The documents
          spoke for themselves. I soon noticed that something was very wrong. I
          gradually came to understand that the documents contained many
          contradictory versions of reality. First and most shocking were the
          reports horrified drivers wrote about their runaway cars. Second were
          startling emails Toyota’s engineers had sent each other. They were
          searching for UA’s root causes, but they could not seem to find them.
          They sometimes admitted it was the electronic parts, the engine
          computer, the software, or interference by radio waves. Meanwhile,
          efforts were made to find floor mats that would trap gas pedals and
          conveniently explain UA. The R&D chief admitted that incompletely
          developed cars had gone into production and that quality control of
          parts was poor or non-existent. Third, I read many descriptions by
          executives and managers of how they had hoodwinked regulators, courts,
          and even Congress, by withholding, omitting, or misstating facts.
          Last, and most damning, I found Toyota’s press releases to be bland
          reassurances obviously meant to help maintain public belief in the
          safety of Toyota’s cars—despite providing no evidence to support those
          reassurances. I saw a huge gap between the hard facts known by
          engineers and executives and the make-believe produced for public
          consumption by Toyota’s PR department.
        </p>
        <h2>The Crown Prince and the people</h2>
        <p>
          A moment of truth came when I was shocked at the contrast between the
          intense determination of Toyota’s electronics engineers to find and
          fix the cause of a speed control problem in the car of Japan’s Crown
          Prince Naruhito – and how the company stonewalled government
          investigators and ordinary American families whose loved ones had been
          injured or killed when their cars hit a tree, launched off a cliff, or
          landed in a river.
        </p>
        <h2>Compelling evidence</h2>
        <p>
          I am neither an electronics nor automotive engineering expert. I
          therefore had to educate myself until I was confident that my
          conclusions were correct. The more I learned, the better I understood
          how complex the evidence was and how challenging for me to master it,
          but when combined with other sources of information, the evidence was
          absolutely compelling. I became convinced that ordinary people were
          certainly at risk, cars on the road were dangerous, and that inside
          the company, they seemed to know it, but not to care.
        </p>
        <h2>Telling the truth</h2>
        <p>
          he [<i>sic</i>] truth must get out, I thought. Indifference was
          impossible. I could not hold it in and allow more people to die or be
          injured. I first consulted my Jerusalem rabbi, who is also a respected
          economist and business ethics expert. His wise advice: I could speak
          out to prevent future damage. Afterwards, I approached Tel Aviv’s
          Heskia – Hacmun Law Firm. The partners, Amos Hacmun and Dor Heskia,
          provided valuable advice and genuine, steadfast support during this
          extremely difficult journey. I began seeking outlets for the truth for
          the sake of furthering public safety and saving lives. Eventually,
          media coverage alerted some in Congress to the unsolved, ongoing
          problem. One TV news report revealed a document that Toyota did not
          give to Congress but that absolutely should have been turned over.
        </p>
        <h2>The Senate Judiciary Committee investigates</h2>
        <p>
          When this fact became public, Senator Charles Grassley of Iowa wanted
          to know whether the National Highway Traffic Safety Administration
          (NHTSA) had allowed Toyota off the hook by accepting the results of a
          too-narrow NASA study of car electronics. Through the Senator’s
          whistleblower program, I gave hundreds of documents to his Judiciary
          Committee staffers. I sorted the documents to show that many
          electronics issues related to UA were known inside Toyota but not even
          touched upon by NHTSA and NASA in their studies of Toyota electronics
          and UA. I also organized the documents to show that it seemed the
          executives were misrepresenting facts in their sworn testimony before
          three Congressional committees. Senator Grassley was thus concerned
          about whether NHTSA had done a proper job, especially with the NASA
          study it had commissioned, and sent a public letter of inquiry to
          NHTSA administrator David Strickland. NHTSA’s response to Senator
          Grassley was cleverly worded and noncommittal. Then the staffers
          invited me to explain the documents and permitted me to bring experts
          to do so. I invited four experts. We all flew in as volunteers from
          our far flung homes and participated in eight hours of meetings.
          Following these meetings, the investigation was suspended awaiting
          more evidence or developments. Mother Nature doesn’t lie: Working with
          scientists and engineers More scientific and engineering evidence was
          needed. I contacted UA vehicle owners who had complained and helped
          them give failed vehicles and parts to independent forensic engineers.
          There were two stunning successes. In one, a runaway Camry gas pedal
          was found to have a short circuit just like the one found by the NASA
          study team, corroborating evidence previously dismissed by NASA. In
          another, an engineer inspected a UA Prius and found a serious
          manufacturing defect in the Prius steering column assembly. He later
          made a defect petition to NHTSA that got nationwide news coverage.
          Hopefully, a full technical investigation of this potentially
          dangerous defect will soon be launched. Scientists and engineers are
          frustrated when technical information is kept secret. I occasionally
          acted as a kind of switchboard operator among them to facilitate
          exchanges and reviews of their UA related findings. Work is ongoing in
          various labs, and more findings will be forthcoming.
        </p>
        <h2>Going public, come hell or high water</h2>
        <p>
          Toyota settled a UA className action lawsuit for $1.3 billion in
          December, 2012. The settlement kept all discovered technical facts
          secret by court order. The public’s curiosity was aroused…what were
          the secrets? The publishers of Corporate Counsel magazine decided to
          find out. They conducted an in-depth investigation and had experts
          analyze the documents. After more than two years in anonymity, in this
          report I stepped boldly into public view as a means of catalyzing more
          action. The article was published in March of 2013. I hope this and
          everything else I’ve done will alert the public and will save lives.{" "}
        </p>
        <h2>Solving the problem</h2>
        <p>
          We must fix the ultimate root cause of UA and other dangerous vehicle
          behavior, not only in Toyota but in the entire auto industry. Today’s
          cars are controlled by complex electronics that must work perfectly to
          keep people safe. UA and other tragedies can happen because there have
          been no public standards for safety-critical parts and systems. That
          needs to change, and concerned citizens can lead the industry and
          government to effect that change, just as they did with seat belts and
          air bags. A new ISO safety standard for road vehicles was published
          recently. It requires rigorous engineering of safety-critical car
          systems such as throttle, brakes, or steering. Third-party safety
          certification is part of it. Compliance is still difficult for
          automakers like Toyota. I want this standard fully implemented to keep
          people safer. To help push the U.S. government and auto industry to
          fully adopt the standard, I have founded a new advocacy group,
          Citizens for Auto Electronics Safety. Its mission will be to gather
          stories, facts, and scientific evidence about vehicle electronics
          malfunctions, then to encourage government and industry to use the
          standard to boost safety. Please visit this Facebook page for more
          information.{" "}
        </p>
        <h2>Sudden Unintended Acceleration is still a problem</h2>
        <p>
          Although Toyota has been forced to pay billions in fines and
          settlements due to misleading their customers, there are still cars on
          the road that have sudden acceleration problems. Bisnar Chase has more
          resources about{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration">
            sudden unintended acceleration for you to read
          </Link>{" "}
          and information on{" "}
          <Link to="/auto-defects">other dangerous auto defects here.</Link>{" "}
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
