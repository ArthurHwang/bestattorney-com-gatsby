// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Sudden Unintended Acceleration - Information and Resources"
        pageDescription="Sudden Unintended Acceleration is a serious and dangerous auto defect. Read more about the cause and which cars have the potential to be affected."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sudden Unintended Acceleration (SUA) Information</h1>
        <BreadCrumbs location={location} />

        <div
          className="text-header content-well"
          title="Sudden Unintended Acceleration"
          style={{
            backgroundImage:
              "url('/images/text-header-images/car-dashboard.jpg')"
          }}
        >
          <h2>What is Sudden Unintended Acceleration?</h2>
        </div>

        <div className="algolia-search-text">
          <p>
            Sudden Unintended Acceleration was defined by a 1980s National
            Highway Traffic Safety Administration (NHTSA) report to be:
          </p>

          <blockquote>
            <em>
              "Unintended, unexpected, high power accelerations from a... low
              initial speed accompanied by an apparent loss of braking
              effectiveness."
            </em>{" "}
            <Link to="https://en.wikipedia.org/wiki/Sudden_unintended_acceleration">
              Wikipedia
            </Link>
          </blockquote>

          <p>
            Unintended Acceleration has been blamed on many issues including
            driver error, a sticky accelerator pedal, incorrectly fitting floor
            mats, and a faulty electronic control system. It is likely that all
            of these issues have caused an unintended acceleration incident over
            the span of multiple car manufacturers since 1980.
          </p>

          <p>
            Incidents before the year 2000 were noted to have most likely come
            about immediately when the car was shifted from park into drive.
            Recent defects, however, usually result in unintended acceleration
            when the car is already in motion. Survivors recall their cars
            accelerating at full speed and the car not responding to the brakes
            when applied.
          </p>
        </div>

        <div
          className="text-header content-well"
          title="Unintended Acceleration"
          style={{
            backgroundImage: "url('/images/text-header-images/brake-pedal.jpg')"
          }}
        >
          <h2>What Causes Unintended Acceleration?</h2>
        </div>

        <p>
          Early incidents of unintended acceleration in the 1950s would happen
          in GM cars when drivers would accidentally select L in the
          transmission when they intended to select Reverse. Again in the 80s,
          many Audi drivers reported their car suddenly lurching forward from a
          parked or stationary position, but it is suspected that many of these
          reports were due to driver error, as the brake and gas pedals were
          situated closer to each other than the average distance on an American
          car.
        </p>

        <p>
          Within the last 20 years however, sudden acceleration has occured for
          a variety of different reasons, most notably in Toyota cars, but also
          in cars made by other manufacturers such as Ford. In 2008, the NHTSA
          ran one of several investigations into unintended acceleration in
          Toyota vehicles, and found that a plastic piece from the driver-side
          dashboard could come loose and prevent the gas pedal from coming back
          into its' original state from being depressed. Similar results were
          seen with floormats that had been moved out of place. In 2009, a
          Toyota dealership witnessed a car experiencing unintended acceleration
          without a floor mat, meaning that there was an issue with the
          electronic control system.
        </p>

        <p>
          Over the years, many SUA accidents have been caused by all of these
          factors, but it is thought that the electronic control system issue
          was the underlying cause of the majority of incidents.
        </p>

        <div
          className="text-header content-well"
          title="Speeding Car"
          style={{
            backgroundImage:
              "url('/images/text-header-images/speeding-car.jpg')"
          }}
        >
          <h2>In Case of Sudden Acceleration:</h2>
        </div>

        <p>
          When your car accelerates and pressing the brake does nothing to slow
          the car down, your natural reaction might be to panic. Try to remember
          the following steps:
        </p>

        <ol>
          <li>
            <h3>
              Make sure your foot is on the brake and keep it pressed down:
            </h3>

            <p>
              Most modern cars will have a brake that overrides the accelerator,
              but if you happen to have an older car without this feature, you
              may still be able to slow the car down with enough force on the
              brake pedal.
            </p>
          </li>

          <br />

          <li>
            <h3>
              If the car is slowing down when you are braking but it has not
              stopped:
            </h3>

            <ul>
              <li>
                <strong>
                  Automatic Transmisison: Put your car into the lowest gear
                  possible.
                </strong>

                <p>
                  As soon as you've made sure that your foot is firmly on the
                  brakes, put your car into the lowest gear possible. In an
                  automatic transmission car, this will be the "L", or "1" gear.
                  An automatic transmission will utilize engine braking to slow
                  the car down in addition to the brakes being applied. Continue
                  pushing the brakes until the car comes to a complete stop or
                  you are able to put the car in neutral and pull over to the
                  side of the road.
                </p>
              </li>

              <li>
                <strong>
                  Manual Transmisison: Downshift slowly and safely.
                </strong>

                <p>
                  If your car is slowing down gradually but you need it to slow
                  down more quickly, downshift through every gear, making sure
                  your engine has time to slow the car down safely without
                  shifting too soon and causing your car to skid. If you have an
                  open road you may just want to put the car in neutral and let
                  it coast to a stop.
                </p>
              </li>
            </ul>
          </li>

          <br />

          <li>
            <h3>
              If the brake is not working and the car is still accelerating:
            </h3>

            <ul>
              <li>
                <strong>Put your car into neutral and pull the e-brake.</strong>

                <p>
                  If the brakes are not working, then a lower gear will not
                  necessarily benefit a car experiencing full acceleration. Put
                  the car in neutral and pull the e-brake to slow the car down
                  faster. This will manually disengage the engine from the
                  transmission and the car will no longer accelerate.
                </p>
              </li>
            </ul>
          </li>

          <br />

          <li>
            <h3>
              If you are coasting but you still need to slow down suddenly, put
              the vehicle in park:
            </h3>

            <p>
              Unfortunately this will most likely destroy your transmission, but
              if the previous steps have not slowed down your car enough, this
              may be your last option for keeping yourself and your passengers
              safe.
            </p>
          </li>
        </ol>

        <div
          className="text-header content-well"
          title="Unintended Acceleration Road"
          style={{
            backgroundImage: "url('/images/text-header-images/street-blur.jpg')"
          }}
        >
          <h2>History of Sudden Unintended Acceleration?</h2>
        </div>

        <p>
          After the GM and Audi incidents of sudden acceleration, which were
          both determined to be caused by driver error, incidents from the 90's
          onward involved multiple makes of cars - most notably Toyota, but also
          from Ford and Kia.
        </p>

        <p>
          One of the first Toyota incident occurs in 2002 with a complaint of a
          Toyota Camry experiencing acceleration when the brakes were fully
          depressed. Many reported incidents followed, which led to multiple
          investigations by the NHTSA. Most of these investigations however were
          closed citing inconclusive evidence. Eventually though with enough
          evidence, the problem of sudden acceleration was finally taken
          seriously. Multiple car companies were brought to court for wrongful
          death cases, and in 2014 the Department of Justice fined Toyota $1.2
          billion dollars for misleading their consumers regarding their
          dangerous cars. You can read about the{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            timeline of the Toyota scandal here.
          </Link>
        </p>

        <h3>
          The following are some of the more notable incidents of sudden
          acceleration:
        </h3>

        <ul>
          <li>
            <strong>December 1998:</strong> Denise Keenan and Blake McCarty are
            killed when a Ford police vehicle accelerates through the crowd at a
            'Holidazzle' parade in Minneapolis. Investigators find that the
            officer responsible had pressed the accelerator instead of the brake
            accidentally, but that the accident could have been prevented if the
            shift-lock, which prevents the car from shifting into drive if the
            foot is not on the brake pedal, was functioning properly. The
            shift-lock was found to be disengaged due to an electrical error
            that occured when the van's police lights were flashing.
          </li>

          <li>
            <strong>September 2007:</strong> A 2005 Camry stars accelerating
            coming off of a highway in Oklahoma. Tire skidmarks show that the
            driver was braking and the account of the driver asserted that the
            main brakes were used in addition to the parking brake. The car
            continued to move and hit an embankment, injuring the driver Jean
            Bookout and killing her passenger Barbara Schwarz.
          </li>

          <li>
            <strong>August 2009:</strong> Noriko Uno was driving in Southern
            California when she was struck by a car rolling through a stop sign.
            Immediately, her car began accelerating without being able to stop
            and resulted in Uno traveling the wrong way on a street accelerating
            up to speeds close to 100 miles per hour. Uno collided with several
            objects and was killed upon impact with a tree. Witnesses say they
            observed Uno looking terrified as she tried to steer out of the way
            of traffic and it was determined that Uno's foot was lodged
            underneath the brake pedal and she was unable to brake effectively.
            This was the first unintended acceleration trial to go to court
            against Toyota, with the claim that if Toyota had installed a brake
            override in the Camry, Uno's attempts to brake would have put the
            engine in neutral and she would have been safe.
          </li>

          <li>
            <strong>August 2009:</strong> On the same day as Noriko Uno's death,
            a family of four experience their 2009 Lexus ES350 accelerating
            without pressing the gas pedal. The driver was California Highway
            Patrol officer Mark Saylor, who had many hours of experience on the
            road and was unlikely to make the mistake of pressing on the gas
            pedal instead of the brakes. A 911 call was recorded of either
            Saylor or his brother reporting that the brakes weren't working and
            that the car was not slowing down. The call ended when the car hit
            an embankment and crashed, killing all four passengers. This
            accident prompted Toyota to recall 3.8 million cars so they could
            replace the floor mats which allegedly made the accelerator pedal
            stick.
          </li>
        </ul>

        <h2>Resources</h2>

        <ul>
          <li>
            {" "}
            <Link to="https://articles.latimes.com/2010/feb/28/business/la-fiw-toyota-deaths-list28-2010feb28/">
              http://articles.latimes.com/2010/feb/28/business/la-fiw-toyota-deaths-list28-2010feb28/
            </Link>
          </li>

          <li>
            {" "}
            <Link to="https://en.wikipedia.org/wiki/Sudden_unintended_acceleration">
              https://en.wikipedia.org/wiki/Sudden_unintended_acceleration
            </Link>
          </li>

          <li>
            {" "}
            <Link to="https://www.consumerreports.org/cro/news/2010/02/timeline-of-toyota-acceleration-investigations/index.htm">
              http://www.consumerreports.org/cro/news/2010/02/timeline-of-toyota-acceleration-investigations/index.htm
            </Link>
          </li>

          <li>
            {" "}
            <Link to="https://usatoday30.usatoday.com/money/autos/2004-04-13-unintended-acceleration_x.htm">
              http://usatoday30.usatoday.com/money/autos/2004-04-13-unintended-acceleration_x.htm
            </Link>
          </li>

          <li>
            {" "}
            <Link to="https://www.safetyresearch.net/toyota-sudden-acceleration-timeline">
              http://www.safetyresearch.net/toyota-sudden-acceleration-timeline
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
