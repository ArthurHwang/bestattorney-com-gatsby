// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Toyota Sudden Unintended Acceleration Claims Lives - SUA"
        pageDescription="At least 19 occupants of Toyota vehicles have died in crashes due to unintende acceleration that the driver could not prevent nor stop."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Sudden Unintended Acceleration Claims Lives</h1>
        <BreadCrumbs location={location} />
        <h2>
          What Should You Do If You Experience Sudden Unintended Acceleration?
        </h2>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/gTN_AsX9SPA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          In 2009, California Highway Patrol officer, Mark Saylor, and his wife,
          daughter and brother-in-law died in a car accident. Mark was driving a
          2009 Lexus ES 350 -- loaned from Bob Baker Lexus of El Cajon, CA -- at
          the end of Highway 125 when the vehicle did not respond to pressure he
          placed on the brake, the Lexus collided with a Ford Explorer and
          continued accelerating through a T intersection until it struck an
          embankment, tumbled into a riverbed and burst into flames.
        </p>
        <p>
          Since Mark Saylor was a CHP officer, no one could understand why he
          was not able to harness the vehicle he was driving and bring it to a
          safe, complete stop. However, the details of the crash became clearer
          when the frantic 911 phone call Mark's brother-in-law, Chris
          Lastrella, made as the car was accelerating out of control was
          broadcast publicly. There was nothing they could do to stop the
          vehicle.
        </p>
        <p>
          At least 19 occupants of Toyota vehicles have died in crashes similar
          to this one. The only understanding the public has for these fatal
          accidents is that they were all caused by{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration">
            sudden unintended acceleration
          </Link>{" "}
          that the driver could not prevent nor stop.
        </p>
        <p>
          The Saylor accident happened at the pinnacle of what has since been
          called the Sudden Unintended Acceleration (SUA) problem. Toyota had
          initially denied there was anything wrong with their vehicles, and
          blamed consumers for installing floor mats incorrectly. The extent to
          which they attempted to fix the problem was offering to replace mats
          or post warning signs, but were otherwise negligent.
        </p>
        <LazyLoad>
          <img
            src="/images/GTY_toyota_plant_ml_140319_12x5_992.jpg"
            alt="Sudden Unintended Acceleration"
            width="100%"
          />
        </LazyLoad>
        <p>
          The National Highway Traffic Safety Administration has conducted eight
          investigations into the causes of SUA in Camry, Lexus ES 350, Sienna
          and Tacoma models, but most of these investigations have been short
          and not thorough enough to locate the root cause. Any results claimed
          that the acceleration was caused from nothing more than interference
          with the pedal caused by floor mats or trim.
        </p>
        <p>
          The tragic death of Mark Saylor and his family has exposed the serious
          and real nature of this problem and has put pressure on Toyota and the
          NHTSA to locate the root cause. In spite of this, Toyota attributed
          the Saylor crash to an "unsecured all-weather floor mat" that was
          stuck in the accelerator pedal. Reports stated the mat was not
          properly secured and had melted with the accelerator pedal during the
          fire. They also said the installed mat was not the correct kind of mat
          for the vehicle, though the correct brand.
        </p>
        <p>
          Some are skeptical that this was the true cause of the crash, because
          the report ambiguously states that the accelerator pedal was burned on
          to the mat, not the mat on top of the accelerator. Further, the
          concern is that this is not the true cause of SUA and that Toyota and
          Lexus owners, as well as the rest of the public, are still at risk for
          these types of accidents. It seems the manufacturers are not treating
          the dangers of having an unidentified defect, or many defects, in
          vehicles widely driven by the public appropriately.
        </p>
        <h3>See related articles:</h3>
        <p>
          {" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            Toyota Sudden Unintended Acceleration
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-floor-mat-recalls">
            Toyota Recalls Almost 4 Million Vehicles For Floor Mat Entrapment
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/sua-complaints">
            Consumer Complaints About Sudden Unintended Acceleration (SUA) in
            Toyotas
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/ford-sudden-unintended-acceleration">
            Toyota Unintended Acceleration Problem is Reminiscent of Ford
            Explorer Defects
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sua-whistleblower-personal-statement">
            Toyota UA Whistleblower Betsy Benjamin - Personal Statement
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
