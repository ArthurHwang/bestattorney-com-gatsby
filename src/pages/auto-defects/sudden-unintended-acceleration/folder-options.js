/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "auto-defect",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "Auto Defect Attorneys",
    HTML: `
    <ul className='bpoint'>
	  	<li>Sudden unintended acceleration (SUA) is an auto defect that makes a car start accelerating without pressing the gas pedal. It is often accompanied by loss of braking power and can usually only be stopped by putting the car in neutral.</li>
      <li>The SUA defects have caused hundreds of car crashes, and as many as 89 deaths and 52 injuries, according to <a href='https://www.cbsnews.com/news/toyota-unintended-acceleration-has-killed-89/'>CBS News.</a></li>
      <li>Toyota has recalled over 10 million cars due to SUA, and it still remains an issue. Nissan recalled 300,000 cars as recently as 8/12/15 for acceleration issues caused by the trim panel.</li>
      <li>Toyota has been fined over 1.27 Billion dollars for their negligence regarding sudden acceleration, and has had to pay more in confidential settlements and verdicts to victims of SUA. </li>
    	<li>If you or a loved one have suffered in a bicycle accident as a result of someone else’s negligence, know your rights and <a href='/contact'>contact an attorney</a> today.</li>
    </ul>
    `
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Unintended Acceleration Injury Information",
    links: [
      {
        linkName: "Toyota SUA Timeline",
        linkURL:
          "/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration"
      },
      {
        linkName: "Toyota Whistleblower Statement",
        linkURL:
          "/auto-defects/sudden-unintended-acceleration/toyota-sua-whistleblower-personal-statement"
      },
      {
        linkName: "Sudden Acceleration Claims Lives",
        linkURL: "/auto-defects/sudden-unintended-acceleration/sua-claims-lives"
      },
      {
        linkName: "Sudden Acceleration Complaints",
        linkURL: "/auto-defects/sudden-unintended-acceleration/sua-complaints"
      },
      {
        linkName: "Ford Sudden Unintended Acceleration",
        linkURL:
          "/auto-defects/sudden-unintended-acceleration/ford-sudden-unintended-acceleration"
      },
      {
        linkName: "Toyota Floor Mat Recalls",
        linkURL:
          "/auto-defects/sudden-unintended-acceleration/toyota-floor-mat-recalls"
      },
      {
        linkName: "Sudden Unintended Acceleration Home",
        linkURL: "/auto-defects/sudden-unintended-acceleration"
      },
      {
        linkName: "Back to Auto Defects",
        linkURL: "/auto-defects"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "What is Sudden Unintended Acceleration?",
      videoUrl: "K0vYdW7h9-A"
    },
    {
      videoName: "Common Causes of Sudden Acceleration?",
      videoUrl: "M6mzQXFv4T0"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
