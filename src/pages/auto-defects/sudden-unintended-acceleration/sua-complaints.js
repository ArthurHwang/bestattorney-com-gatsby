// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "../../../components/elements/SEO"
import { LayoutPAGEO } from "../../../components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Consumer Complaints About Toyota Sudden Unintended Acceleration - SUA"
        pageDescription="Which Toyotas have been known to unexpectedly accelerate? See the list here, watch our video and read what some consumers have experienced."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Consumer Complaints About Sudden Unintended Acceleration (SUA) in
          Toyotas
        </h1>
        <BreadCrumbs location={location} />
        <p>
          The number of injuries resulting from Toyota{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration">
            sudden unintended acceleration
          </Link>{" "}
          (SUA) incidents, organized by vehicle model, reported from 1999 to
          January of 2010 are as follows:
        </p>
        <div>
          <ul>
            <li>4Runner: 2</li>
            <li>Avalon: 13</li>
            <li>Camry Models: 131</li>
            <li>Corolla: 16</li>
            <li>ES Models: 48</li>
            <li>GS Models: 2</li>
            <li>Highlander Models: 15</li>
            <li>IS Models: 5</li>
            <li>LS Models: 6</li>
          </ul>
        </div>
        <div>
          <ul>
            <li>Other Models: 10</li>
            <li>Prius: 13</li>
            <li>Rav4: 9</li>
            <li>RX Models: 12</li>
            <li>Sienna: 20</li>
            <li>Tacoma: 31</li>
            <li>Tundra: 2</li>
            <li>Unknown Camry or ES Models: 6</li>
          </ul>
        </div>
        <p>
          The number of deaths potentially resulting from Toyota SUA incidents
          reported from 1999 - January of 2010 are as follows:
        </p>
        <ul>
          <li>Camry Models: 12</li>
          <li>ES Models: 5</li>
          <li>Highlander Models: 1</li>
          <li>IS Models: 1</li>
        </ul>
        <h2>
          What Should You Do If You Experience Sudden Unintended Acceleration?
        </h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/gTN_AsX9SPA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          These numbers are based on complaint data analyzed by Safety Research
          & Strategies where SUA is defined as any incident in which the driver
          reported an engine acceleration that was unintended.
        </p>
        <p>
          In all of these incidents, SUA occurred under any of these conditions:
          the vehicle was in idle mode, the vehicle was in reverse at a low
          speed, the operator's foot was on the brake, the vehicle was traveling
          at a constant highway speed, the vehicle contained no all-weather
          accessory floor mats (which have been largely blamed as the cause for
          SUA), and the accelerator pedal was not "sticking" (also another
          alleged cause).
        </p>
        <p>
          One consumer, an owner of a 2007 Avalon, reported SUA five times. His
          experiences showed the vehicle accelerating without his foot on the
          gas, and then it would return to idle after a few miles or if he was
          able to turn off the car or put it in park. He took his Avalon to the
          dealership, but they did not locate any issue.
        </p>
        <p>
          <strong>
            This consumer's account of driving to work in December of 2009 is as
            follows:
          </strong>
        </p>
        <p>
          "&hellip;I was driving to work on a major highway. The car began to
          accelerate without my foot on the gas pedal. As I pushed on the brake,
          the car continued to accelerate. At that time I was not able to stop
          my vehicle by pressing hard on the brake. The only way I was able to
          slow the car down was to put the car into neutral. I took the next
          exit, which was the exit for the Toyota dealership. I called the
          dealership and told the service manager to meet me outside because I
          was experiencing acceleration problems. I drove approximately 5 miles
          by alternating from neutral to drive and pressing firmly on the
          brakes. As I pulled into the front of the dealership I put the car
          into neutral and exited the car. With the brakes smoking from
          excessive braking and the car's rpm's racing the manager entered my
          car. He confirmed that the mats were properly in place and confirmed
          the rpm's were very high."
        </p>
        <p>
          This account is from the National Highway Safety Administration's
          document of this drivers complaint on December 28, 2009.
        </p>
        <p>
          Toyota technicians witnessed the vehicle accelerating and could
          provide no mechanical causes. The Avalon was repaired and at the
          expense of Toyota but the owner was told by the Toyota dealer that the
          vehicle's computer showed no error codes and that did not know if the
          repairs would really fix the vehicle.
        </p>
        <p>
          Toyota has insisted that SUA is not caused by an electronic defect in
          car models, despite what this owner, many owners of Toyota vehicles,
          and the Toyota dealership has witnessed in this case.
        </p>
        <p>
          The search for the cause of SUA is ongoing, with many complaints
          similar to the one above. Many believe that a complete assessment of
          possible electronic throttle defects is absolutely necessary to locate
          the problem and keep Toyota costumers safe.
        </p>
        <h3>See related articles:</h3>
        <p>
          {" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/sua-claims-lives">
            Toyota Sudden Unintended Acceleration Claims Lives
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sudden-unintended-acceleration">
            Toyota Sudden Unintended Acceleration
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-floor-mat-recalls">
            Toyota Recalls Almost 4 Million Vehicles For Floor Mat Entrapment
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/ford-sudden-unintended-acceleration">
            Toyota Sudden Unintended Acceleration Problem is Reminiscent of Ford
            Explorer Defects
          </Link>
          <br />{" "}
          <Link to="/auto-defects/sudden-unintended-acceleration/toyota-sua-whistleblower-personal-statement">
            Toyota UA Whistleblower Betsy Benjamin - Personal Statement
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
