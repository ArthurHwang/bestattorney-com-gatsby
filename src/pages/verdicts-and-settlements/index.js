// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Personal Injury Lawyer Client Stories - Bisnar Chase"
        pageDescription="Review some of the injury and accident stories from our clients on their road to recovery and of hiring a personal injury lawyer from Bisnar Chase to help them win more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Personal Injury Attorney Client Stories</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            Ron and Denise Gowell Motorcycle Accident - Confidential Settlement
          </strong>
        </p>
        <p>
          Motorcycle Accident Throws A Wrench in Orange County Couple's
          Retirement Plans
        </p>
        <p>
          Ron and Denise Gowell had plans. Denise had retired in the fall of
          2006. They were going to travel and enjoy life. She was going to start
          her own customer service consulting firm. And then, on Oct. 4, 2006,
          their secure world fell apart. Ron was riding his motorcycle in Santa
          Ana, not far from their home, when a young driver coming from the
          opposite direction made an illegal U-turn in front of him causing a
          nightmarish auto verses motorcycle accident.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/ron-denise-gowell">
            Click Here
          </Link>{" "}
          to read the rest of their story.
        </p>

        <p>
          <strong>
            Joshua Newman Auto Accident - $1.25 Million Seat Belt Failure -
            Confidential Settlement with Nissan Motors
          </strong>
        </p>
        <p>
          Keith Newman was at work when he got the call every parent dreads.
        </p>
        <p>
          His son, Joshua, had been in a car accident. It was pretty bad, he was
          told.
        </p>
        <p>
          "Please God, let him be alive," he remembers praying that instant.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/joshua-newman">
            Click Here
          </Link>{" "}
          to read the rest of his story.
        </p>

        <p>
          <strong>
            Joseph Ranieri and Linda Hahn Auto Accident - Confidential
            Settlement
          </strong>
        </p>
        <p>
          Couple Values Attorney's Personal Experience and Knowledge of Auto
          Accidents
        </p>
        <p>
          What Joseph Ranieri remembers distinctly about Dec. 26, 2005 is the
          sight of a Ford Ranger pickup truck ramming into the back of his new,
          leased Cadillac CTX.
        </p>
        <p>
          Ranieri and his wife, Linda Hahn - both Realtors in Fountain Valley --
          were returning home from San Diego after having breakfast with friends
          the day after Christmas, when a speeding pickup truck driver slammed
          into the back of their car.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/linda-hahn">Click Here</Link> to
          read the rest of their story.
        </p>

        <p>
          <strong>
            Alvin Austin Traffic Accident - Debris on Freeway - Wrongful Death -
            $1 Million
          </strong>
        </p>
        <p>
          Alvin Austin was glad he had that lunch with his wife, Beverly, the
          afternoon of July 31, 2006.
        </p>
        <p>
          That was the last meal he would share with his wife of 23 years. The
          last chitchat he would have with the mother of his two children and
          the 20 foster children they raised together.
        </p>
        <p>
          That afternoon, Alvin headed out to a doctor's appointment after lunch
          and Beverly set out to get some chores done.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/alvin-austin">Click Here</Link> to
          read the rest of his story.
        </p>
        {/* <!-- Touched by Arthur 10/24.  There is no related file for this. -->
    <!-- <hr> -->
    <!-- <p><strong> Felipe Rojas
      Seat Belt Failure during Auto Accident -
      Confidential Settlement with General Motors</strong></p>
  <p>Life as you know it can change in a split second. Felipe Rojas knows that better than most of us.</p>
  <p>Felipe had a beautiful family. A son and a daughter, a beautiful wife he had been married to for 13 wonderful
    years.</p>
  <p>On Oct. 24, 2003, everything changed.</p>
  <p>{" "}<Link to="/verdicts-and-settlements/felipe-rojas">Click Here</Link> to read the rest
    of his story.</p> --> */}

        <p>
          <strong>
            Jessica Auto Accident - Confidential Settlement from Insurance
            Company
          </strong>
        </p>
        <p>Auto Accident Leaves College Student with Unexpected Back Injury</p>
        <p>
          Jessica's case was a rare one for Bisnar Chase Personal Injury
          Attorneys. Of course, it was an auto accident case, which is nothing
          out of the ordinary. But her case was special because she was referred
          to us by another client - her father. Nearly 10 years ago, her father
          was in a serious auto accident that caused him to have back surgery
          and stay out of work for a year.
        </p>
        <p>
          Jessica, 20, who is pursuing an undergraduate degree in psychology in
          Orange County, was fortunate in the sense that she was not as
          seriously injured as her father. The incident occurred in August 2006.
          She was crossing an intersection in her vehicle when a driver made a
          left turn in front of her. Jessica had the right of way, but the
          driver did not stop.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/college-student-settlement">
            Click Here
          </Link>{" "}
          to read the rest of her story.
        </p>

        {/* <!-- Touched by Arthur 10/24.  There is no related file for this. -->
    <!-- <hr>
  <p><strong> Mary Nehrlich
      Auto Accident - $500,000 Recovery
      Insurance Bad Faith - Confidential Settlement</strong></p>
  <p>For many years, Mary Nehrlich sold insurance. It's how she made a living.</p>
  <p>But never did she dream that when she needed money from the insurance companies, whose products she sold for
    years, she would be shown the door.</p>
  <p>Mary will never forget that day - Oct. 18, 1995. It was her son's birthday. It was around 4:30 in the afternoon
    and she just had enough time to make a run to the store to buy her dear son a birthday present.</p>
  <p>{" "}<Link to="/verdicts-and-settlements/mary-nehrlich">Click Here</Link> to read the rest
    of her story.</p> --> */}

        <p>
          <strong>
            Bob Myatt Auto Accident - $100,000 Settlement from Insurance Company
          </strong>
        </p>
        <p>Auto Accident Victim Thankful For Efficiency and Sensitivity</p>
        <p>
          It was a drive Bob Myatt wishes he had never embarked on. The Fresno
          resident had bought a new Toyota Prius. It was a beautiful Sunday
          afternoon and he was driving on the foothills of the Sierras. He
          decided to climb up the mountain in his Prius wondering what kind of
          mileage it would give him.
        </p>
        <p>
          Myatt never found out. Half way up the hill, about two miles away from
          Lake Shaver, the driver of a pickup truck who took a wide turn and was
          traveling over the speed limit slammed into Myatt's Prius head-on from
          the opposite direction.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/bob-myatt">Click Here</Link> to
          read the rest of his story.
        </p>

        <p>
          <strong>
            Ernest Sevilla Dog Attack - Confidential Settlement Legal
            Malpractice - Confidential Settlement
          </strong>
        </p>
        <p>A Dog Bite Case With Two Attacks</p>
        <p>Man severely injured by dog and then by his attorney.</p>
        <p>
          If getting chunks of flesh bitten off his hand by a Japanese fighter
          dog wasn't painful enough for Ernest Sevilla, being ignored and failed
          by a personal injury attorney who was supposed to help him through it
          all, was agony.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/ernest-savilla">
            Click Here
          </Link>{" "}
          to read the rest of his story.
        </p>

        <p>
          <strong>
            Daniel James SUV Rollover Accident Confidential Settlement
          </strong>
        </p>
        <p>
          Daniel James had seen enough strife and suffering to last him a
          lifetime, by the time he turned 17.
        </p>
        <p>"I had walked through the gates of hell," he says.</p>
        <p>And anyone who knows Daniel, knows that he does not exaggerate.</p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/daniel-james">Click Here</Link> to
          read the rest of his story.
        </p>
        {/* <!-- Touched by Arthur 10/24.  There is no related file for this. -->
    <!-- <hr> -->
    <!-- <p><strong> David Iorillo
  Drunk Driving Accident
  $100,000 from the drunk driver's insurance company.</strong></p>
<p>When you're a street cop in a big city, you're pretty much prepared for almost anything, almost all of the time.  This is especially so of motor officers, a special breed of peace officer that patrol our streets on motorcycles.  But David Iorillo, a motor officer for the San Diego Police Department, was caught off guard when he was rear-ended by a drunk driver while riding his personal motorcycle, off duty.</p>
<p>It was the last thing David expected. It was close to Christmas - Dec. 8, 2002. David was on his personal motorcycle and stopped for a red light at Ted Williams Parkway and the 15 Freeway. He was waiting to turn onto the freeway when he got hit from behind. It all happened in a flash. The next thing he knew he was on the ground, off his bike, laying in the intersection.</p>
<p>{" "}<Link to="/verdicts-and-settlements/david-iorillo">Click Here</Link> to read the rest of his story.</p> --> */}

        <p>
          <strong>
            Gloria McVicar Defective Medical Product - Confidential Settlement
          </strong>
        </p>
        <p>
          Client Thankful for Firm's Work In Defective Dow Breast Implant Case
        </p>
        <p>
          Gloria McVicar loathes the day she decided to get breast implants.
        </p>
        <p>
          "It was a long time ago," says Gloria, who got them in the 1970s, when
          Dow Corning silicone implants were the way to go. "I was young and
          stupid," she says.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/gloria-mc-vicar">
            Click Here
          </Link>{" "}
          to read the rest of her story.
        </p>
        <p>
          <strong>
            Connie Matthews Auto Accident - Confidential Settlement Property
            Damage - $2,300 Settlement
          </strong>
        </p>
        <p>A Grandmother Stings Farmers' Insurance in Small Claims Court</p>
        <p>
          Connie Matthews, a 69-year-old grandmother from San Lorenzo, was in a
          horrible auto accident on the San Leandro (880) Freeway in July 2007.
          When Farmers Insurance Co. tried bullying her and her attorneys over a
          few hundred dollars of damaged property, Connie fought back.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/connie-matthews">
            Click Here
          </Link>{" "}
          to read the rest of her story.
        </p>

        <p>
          <strong>Robert Rife Salmonella Poisoning - Foodborne Illness</strong>
        </p>
        <p>
          It was four years ago. But Robert Rife remembers it as if it was
          yesterday. He'll never forget the excruciating pain. It felt as if
          someone were sticking a knife through his intestines, the 60-year-old
          Helendale resident says. Well, he didn't get in a car accident or
          suffer from a debilitating disease. All he did was eat sushi.
        </p>
        <p>
          Robert remembers the date because the day, its events and the horrific
          aftermath are all etched in his memory. August 28, 2003. Robert worked
          in Buena Park at the time, commuting 100 miles each way to Helendale
          and back. He and some of his colleagues decided to go to Karuta, a
          sushi place close to work.
        </p>
        <p>
          "As far as I can remember, it was a nice lunch," Robert says. "I
          remember it because it was the only food I ate that day."
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/robert-rife">Click Here</Link> to
          read the rest of his story.
        </p>

        <p>
          <strong>
            Stephanie Collins Seatback Failure during Auto Accident Confidential
            Settlement with Ford Motor Company
          </strong>
        </p>
        <p>
          There is no closure when a mother loses her child. A mother never gets
          over a child's death. On the contrary, a child's passing leaves a void
          in her life - an emptiness that is only filled by memories.
        </p>
        <p>
          Stephanie Collins lives that sad reality every day of her life. She
          blames Ford Motor Co. for it and for good reason.
        </p>
        <p>
          July 14, 2000 started like any other day for Stephanie and her
          daughter, Crystal, who was just a month away from her eighth birthday.
          Crystal was sitting in the rear passenger seat of Stephanie's Ford
          Escort, right behind her mother in the front passenger seat.
          Stephanie's mom, Crystal's grandmother, was driving. When an ambulance
          approached, she pulled over to the side of the road to make way for
          the ambulance.
        </p>
        <p>
          {" "}
          <Link to="/verdicts-and-settlements/stephanie-collins">
            Click Here
          </Link>{" "}
          to read the rest of her story.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
