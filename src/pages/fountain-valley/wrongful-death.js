// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import Helmet from "react-helmet"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fountain Valley Wrongful Death Attorneys - Bisnar Chase"
        pageDescription="Call 949-203-3814 now for the best Fountain Valley wrongful death lawyers. We can help you seek justice."
      />
      <Helmet>
        <meta
          name="DC.title"
          content="Wrongful Death Lawyers Fountain Valley"
        />
        <meta name="geo.region" content="US-CA" />
        <meta name="geo.placename" content="Fountain Valley" />
        <meta name="geo.position" content="37.09024;-95.712891" />
        <meta name="ICBM" content="37.09024, -95.712891" />
      </Helmet>

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fountain Valley Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          If someone you love has died because of someone else's negligence, you
          may be entitled to make a wrongful death claim. It is important to
          note that although these types of cases are tragic, they are not
          criminal cases in the eyes of the law and may qualify you for
          compensation for the loss of companionship.
        </p>
        <p>
          Our{" "}
          <Link to="/fountain-valley">
            Fountain Valley personal injury attorneys
          </Link>{" "}
          can sit down with you during this difficult time to discuss your legal
          options. Our team has over 35 years of serving clients in Orange
          County and surrounding areas.
        </p>
        <p>
          Call us now <strong>949-203-3814</strong> so we may help you seek the
          justice that you deserve.
        </p>
        <h2>What Makes a Wrongful Death Case?</h2>
        <p>
          Wrongful death is defined as any loss of a family member or loved one
          caused by negligence, carelessness, or reckless act from a third
          party.
        </p>
        <p>
          <b>The anatomy of wrongful death case contains:</b>
        </p>
        <ul>
          <li>Death of person (Fatal accident or injury)</li>
          <li>Death was caused by the negligence of another party</li>
          <li>
            Surviving family members who are now financially suffering due to
            this death
          </li>
        </ul>
        <p>
          If you can prove suffering and unrecoverable damages from the death of
          your loved one, you may be entitled to compensation according to{" "}
          <Link
            to="http://scholarship.law.berkeley.edu/cgi/viewcontent.cgi?article=3762&context=californialawreview"
            target="_blank"
          >
            {" "}
            California law
          </Link>
          . The following must be present in order to have a successful wrongful
          death claim:
        </p>
        <ul>
          <li>Loss of the love/companionship from the deceased.</li>
          <li>The value of the household services from the deceased.</li>
          <li>
            The value of the financial support the deceased provided before
            death.
          </li>
        </ul>
        <h2>Who Can File a Wrongful Death Claim in Fountain Valley?</h2>
        <p>
          Not anyone can file a wrongful death claim in the name of the
          deceased. Only certain in individuals who have a direct relationship
          with the deceased can file a claim.
        </p>
        <p>
          The surviving spouse and children are the first in line that are able
          to file a claim. Parents, brothers, sisters, nephews, nieces,
          grandparents and their lineal descendants are next in line to file a
          claim.
        </p>
        <p>
          The line can progress to a spouse not legally married to the deceased,
          children of the putative spouse, and stepchildren.
        </p>
        <h2>Compensation in a Wrongful Death Case</h2>
        <p>
          If you have proof that someone's lack of action led to the wrongful
          death, you may be qualified for compensation. Additionally, proving
          the action taken by the at-fault lead to fatal injuries can also aid
          in you claim. Finally, proving the financial toil the death of your
          loved one has taken on you and our family will help you obtain
          compensation.
        </p>
        <p>
          Wrongful death laws are different in each state and this may affect
          the amount of compensation you receive. The factors that determine you
          compensation amount includes:
        </p>
        <ul>
          <li>Last earning wage of the deceased.</li>
          <li>Your financial dependency on the deceased.</li>
          <li>The amount of funds the deceased saved</li>
          <li>The total loss of companionship</li>
          <li>The total costs that came after the deceased death</li>
        </ul>
        <p>
          Wrongful death lawsuits can only be filed a certain time after the
          deceased has passed. If plan on taking legal action, we suggest you do
          it now.
        </p>
        <h2>Best Wrongful Death Attorneys in Fountain Valley</h2>
        <p>
          Wrongful death law is complex. You will need a lawyer who knows the
          law front to back.
        </p>
        <p>
          Bisnar Chase has proven time and time again that our attorneys have
          what it takes to take on these cases with 35 years of service in
          Orange County and surrounding areas with a 96% success rate
          additionally.
        </p>
        <p>
          We know this can be a tough time for your family. We want to help you
          recover and get back on your feet. Let us do all the hard work while
          you and your family start to rebuild our lives. As with all our cases,
          if we lose in court or cannot deliver you an adequate settlement, you
          do not pay us at all.
        </p>
        <h3 align="center">
          Call us now at 949-203-3814 to set up your FREE case review.
        </h3>
        <h3 align="center">Passion - Trust - Results</h3>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
