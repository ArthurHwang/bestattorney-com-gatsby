// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fountain Valley Brain Injury Attorneys - Bisnar Chase"
        pageDescription="If you need a Fountain Valley brain injury attorney, call us now at 949-203-3814. Free case review."
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fountain Valley Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Brain injuries in Fountain Valley are caused by car accidents in most
          instances. A traumatic brain injury (TBI) is a catastrophic injury,
          which means that it has either long-term or lifelong impact on a
          victim. These type of injuries can lead to medical bills that will
          take everything from you while you suffer. At Bisnar Chase{" "}
          <Link to="/fountain-valley">
            {" "}
            Fountain Valley personal injury attorneys
          </Link>
          , we not only think this is unfair, we also think this is unjust and
          the person(s) that contributed to your pain and suffering should be
          held responsible and compensate you accordingly.
        </p>
        <p>
          If you have become the victim of a brain injury due to someone else's
          negligence or carelessness, call us now at{" "}
          <strong>949-203-3814</strong> to set up your free case review with us.
          If your case qualifies for compensation, we will fight relentlessly
          until you are fully compensated for your injuries. We have helped
          victims all across Orange County for over 35 years with a 96% success
          rate and we may be able to help you too.
        </p>
        <p>
          <em>
            <strong>
              Victims of brain injuries are faced with physical, emotional and
              financial challenges.
            </strong>
          </em>
        </p>
        <h2>How Do Brain Injuries Affect Victims?</h2>
        <p>
          A catastrophic brain injury can result in a number of different
          permanent symptoms and disabilities. The type of symptoms that the
          victim may struggle with usually depends on the severity of the damage
          and the location of the trauma.
        </p>
        <ul>
          <li>
            Victims who suffer trauma to their frontal Lobe (behind the
            forehead) could suffer from paralysis, have issues with focus,
            memory, mood changes, changes in personality or even speech and
            language.
          </li>
          <li>
            When an injury occurs in the occipital lobe (back of the head),
            victims may experience vision problems such as difficulty
            identifying colors as well as cognitive problems.
          </li>
          <li>
            When the trauma occurs in the parietal lobe (near the top and back
            of the head), victims may struggle with a lack of awareness of
            certain body parts or a reduced eye and hand coordination. Victims
            may not be able to even perform functions such as identifying
            objects.
          </li>
          <li>
            Victims who suffer trauma to the temporal lobes (sides of the head
            above ears) may no longer recognize faces or understand spoken words
            and may also experience short-term memory or long-term memory loss.
          </li>
          <li>
            When an injury occurs in the cerebellum (base of skull), victims
            could lose their ability to walk, to reach out and grab objects or
            to coordinate finer movements. Symptoms may also include tremors,
            vertigo or slurred speech.
          </li>
          <li>
            Brain stem injuries may cause difficulties swallowing, balancing or
            sleeping.
          </li>
        </ul>
        <h2>What Are The Effects of a Brain Injury?</h2>
        <p>
          Many victims of severe brain trauma need help performing everyday,
          basic tasks. Having round-the-clock care is simply too expensive for
          most victims of brain trauma. Therefore, much of the responsibility
          for caring for the victim could squarely fall on his or her family
          members. This can obviously lead to substantial physical, financial
          and emotional issues for the victim and his or her family.
        </p>
        <p>
          The expenses faced by brain injury victims in the months and years
          after the incident can be significant. Most health insurance policies
          will cover the bulk of the initial diagnosis and treatment of a brain
          injury.
        </p>
        <p>
          This may include a number of expensive medical tests, surgical
          procedures and prolonged hospitalization. Victims will then need
          inpatient rehabilitation services and then outpatient rehabilitation
          services.
        </p>
        <h2>Outpatient Rehabilitation Services Not Covered by Insurance</h2>
        <p>
          Some victims may require expensive outpatient rehabilitation services
          for years. Unfortunately, this type of vital treatment is often not
          covered by insurance. Not receiving rehabilitative treatment and care
          will greatly affect the chances of the victim having a proper
          recovery.
        </p>
        <h2>Expenses Not Covered by Insurance</h2>
        <p>
          It is also rare for victims to return to work or ever earn the kind of
          money they were earning before the incident. Therefore, victims could
          pile up hundreds of thousands of dollars in treatment bills without
          being able to earn wages. Additionally, victims may need to move or
          redesign their residence to fit their new physical limitations. These
          types of expenses are necessary, but they are rarely covered by
          insurance.
        </p>
        <h2>Getting Fair Compensation for Your Injuries</h2>
        <p>
          In cases where the brain injuries are caused by someone else's
          negligence or wrongdoing, financial compensation may be available to
          victims and their families.
        </p>
        <h3>Compensation Can Be Sought For The Following Damages</h3>
        <ul>
          <li>Medical expenses</li>
          <li>Lost wages</li>
          <li>Hospitalization</li>
          <li>Rehabilitation</li>
          <li>Loss of earning</li>
          <li>Potential and even long-term nursing care</li>
        </ul>
        <h2>Choosing a Lawyer that is Right for You</h2>
        <p>
          Are you and your family coping with an extremely difficult time in
          your lives due to a brain injury accident? We at Bisnar Chase
          understand as we have had a long and successful track record with
          similar victims and we are passionate about helping brain injury
          victims and their families.
        </p>
        <h3 align="center">
          Call us now at 949-203-3814 for your FREE Consultation.
        </h3>
        <h3 align="center">Passion - Trust - Result</h3>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
