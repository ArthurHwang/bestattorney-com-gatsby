// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fountain Valley Car Accident Lawyer - Bisnar Chase"
        pageDescription="Bisnar Chase Fountain Valley car accident attorneys helps victims who have been injured. Call us today at 949-203-3814 to get a Free Case Evaluation!"
      />

      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fountain Valley Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Fountain Valley, just like many other cities in Orange County, faces
          issues with traffic congestion and thus auto accidents. Car accidents
          in Fountain Valley have affected many innocent victims and
          pedestrians.
        </p>
        <p>
          A skilled{" "}
          <Link to="/fountain-valley">
            Fountain Valley personal injury attorney
          </Link>{" "}
          knows that being compensated after you've suffered a car crash injury
          involves both economic and pain and suffering. This is where the
          attorneys of Bisnar Chase can help you out.
        </p>
        <h3 className="mb">
          Call us today at 949-203-3814 for your free case review!
        </h3>
        <h2>Car Accidents in Fountain Valley</h2>
        <p>
          Recent statistics compiled by the California Highway Patrol's
          Statewide Integrated Traffic Records System (SWITRS) showed that four
          people died and 476 people were injured in Fountain Valley car
          crashes.
        </p>
        <p>
          Pedestrian accidents injured 14. Bicycle and motorcycle accidents
          injured 28 and 34 people, respectively. DUI crashes caused 28
          injuries. In the same year, four car accidents caused four fatalities
          and three car crashes resulted in as many deaths.
        </p>
        <h2>What Makes a Auto Accident Injury Claim?</h2>
        <h3>Pain and Suffering makes an injury claim.</h3>
        <p>
          Experienced Fountain Valley car accident lawyers will tell you that
          "damages" are defined by California law as the monetary value you can
          be awarded when a driver's wrongdoing or negligence causes you injury.
        </p>
        <p>
          "The pain and suffering you endured then, now and in the future, as
          the result of your accident can be difficult to define and quantify,"
          says{" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/80894"
            target="_blank"
          >
            {" "}
            John Bisnar
          </Link>
          . "These damages comprise the mental anguish and/or physical pain,
          embarrassment and emotional stress caused by any disfigurement and
          other results of the car collision." Pain and suffering often makes up
          a major part of an injured party's overall claim to recovery.
        </p>
        <h2>How to Get Maximum Compensation For Your Injuries</h2>
        <p>
          California's tort liability system was set up to define how pain and
          suffering claims are litigated. Under this system, juries have the
          power to decide the monetary awards paid out to compensate a car
          accident victim. "A jury will evaluate your dollar request on a case
          by case basis for each item of harm," notes John Bisnar. "They will
          decide if the amount requested is fair and reasonable." Before
          deliberation on these amounts, the jury will be instructed by the
          court on how a state's laws impact their decisions.
        </p>
        <h2>Bisnar Chase Car Accident Case Results</h2>
        <ul>
          <li>
            $16,444,904.00&nbsp;- Dangerous road condition, driver negligence
          </li>
          <li>
            $9,800,000.00&nbsp;- Motor vehicle accident with serious injuries
          </li>
          <li>
            $8,500,000.00&nbsp;- Motor vehicle accident - wrongful death claim
          </li>
        </ul>
        <p>
          We've handled thousands and thousands of claims for car accident
          victims. Our attorneys are trial lawyers which means when the case
          gets tough we don't hand off your file to another law firm, we go to
          court! Every attorney working for Bisnar Chase must be trial certified
          and able to represent you with the highest standards. &nbsp;Our
          experience and resources has brought in over $500 Million and counting
          for our clients.
        </p>
        <h2>Schedule Your Free Consultation Today</h2>
        <p>
          The trusted Fountain Valley car accident attorneys of Bisnar Chase can
          explain many areas of personal injury law and provide answers to
          questions concerning injury compensation.
        </p>
        <p>
          Some of the best lawyers offer no charge, no pressure consultations.
          Bisnar Chase can ensure that you are fully aware of the laws regarding
          compensation for pain and suffering.
        </p>
        <h3 align="center">Call 949-203-3814 today for a FREE Consultation.</h3>
        <h3 align="center">Passion - Trust - Results</h3>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
