// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/fountain-valley-personal-injury-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "Fountain Valley",
      population: 56707,
      totalAccidents: 3478,
      intersection1: "Brookhurst St & Talbert Ave",
      intersection1Accidents: 147,
      intersection1Injuries: 108,
      intersection1Deaths: 0
    },
    locationBox2: {
      mainCrimeIndex: 153.0,
      city1Name: "Huntington Beach",
      city1Index: 184.5,
      city2Name: "Westminster",
      city2Index: 229.8,
      city3Name: "Costa Mesa",
      city3Index: 214.3,
      city4Name: "Santa Ana",
      city4Index: 200.6
    },
    locationBox3: {
      intersection2: "Brookhurst St & Ellis Ave",
      intersection2Accidents: 121,
      intersection2Injuries: 86,
      intersection2Deaths: 0,
      intersection3: "Slater Ave & Brookhurst St",
      intersection3Accidents: 102,
      intersection3Injuries: 77,
      intersection3Deaths: 1,
      intersection4: "Edinger Ave & Brookhurst St",
      intersection4Accidents: 103,
      intersection4Injuries: 71,
      intersection4Deaths: 1
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Fountain Valley Personal Injury Lawyers - Orange County, CA"
        pageDescription="If you've been injured contact our Fountain Valley personal injury lawyers for a free comprehensive evaluation. We have been representing injured plaintiffs in Orange County for over forty years and you may be entitled to compensation. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Fountain Valley Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Fountain Valley Personal Injury Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Suffering a serious personal injury – whether it is the result of an
          auto accident, or a{" "}
          <Link to="/fountain-valley/dog-bites">dog attack</Link>, or due to a
          dangerous or defective product – can be a traumatic and life-changing
          event. Such an incident can effect an enormous disruption and change
          to your life and routine, leave victims and families with tremendous
          emotional and financial strain. If you have been seriously injured due
          to someone else's negligence and/or wrongdoing, you may be able to
          recover compensation for your losses.
        </p>
        <p>
          For more than 40 years, the experienced Fountain Valley personal
          injury lawyers at <Link to="/contact">Bisnar Chase</Link> have
          successfully handled an array of personal injury cases for our
          clients. Our goal is to help injured victims and their families{" "}
          <strong>secure maximum compensation</strong> for their losses. We
          value the reputation that we have earned and secured over the years by
          providing quality legal representation and superior customer service
          to our clients and their families. Please contact us if you are in
          need of advice and guidance with regard to your potential personal
          injury claim in Fountain Valley.
        </p>

        <h2>Have You Been Injured in Fountain Valley CA?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/personal-injury-fountain-valley.jpg"
            width="100%"
            id="pedestrian-accident"
            alt="Fountain Valley Personal Injury Attorney"
          />
        </LazyLoad>

        <p>
          If you or a loved one has been injured in Fountain Valley, there are a
          number of steps you can take in order to ensure that your legal rights
          and best interests are protected.
        </p>
        <p>
          <strong>Document the incident.</strong> Whether you have been injured
          in an auto accident on a local roadway or freeway or you've hurt
          yourself in a slip-and-fall accident in a neighborhood supermarket,
          documenting the incident is extremely important. For example, if you
          have been injured in a car accident, make sure you{" "}
          <Link
            to="https://www.fountainvalley.org/700/Obtain-a-Police-or-Traffic-Collision-Rep"
            target="_blank"
          >
            {" "}
            file a report with the police
          </Link>{" "}
          while also ensuring that your account of the incident is mentioned in
          the report. It would also be a good idea to obtain a copy of this
          police report for your own records. If you slip and fall in a
          supermarket aisle, file an incident report with the store's owner or
          manager. This helps determine the time, date and the location where
          the incident occurred.
        </p>
        <p>
          <strong>Gather as much evidence as possible from the scene.</strong>{" "}
          The value of collecting pertinent evidence from the accident scene
          cannot be overemphasized. In many instances, your case may only be as
          good as the evidence you have. So, if you have been involved in a{" "}
          <Link to="/fountain-valley/car-accidents">
            {" "}
            Fountain Valley car accident
          </Link>
          , it would be a good idea to get as many photos as possible from the
          scene of the crash. In addition, it is also crucial to get relevant
          information from the other parties that are involved such as other
          drivers as well as contact information for any eyewitnesses. It is
          important to collect this information immediately after the crash as
          accident scenes tend to get cleared out pretty quickly. If you are
          injured and are unable to get these critical pieces of evidence, ask a
          family member or friend to help you out. Your Fountain Valley personal
          injury lawyer will also be able to help you gather such evidence.
        </p>
        <p>
          <strong>Obtain prompt medical attention.</strong> If you have suffered
          personal injuries in any type of incident, it is very important that
          you get prompt medical attention, treatment and care. For example, if
          you have taken a fall on someone else's property, make sure you call
          911 and get to an emergency room so you can get treated right away.
          Similarly, if you have suffered injuries in a car accident, get
          medical attention immediately. This helps establish and document the
          nature of your injuries as well as the treatment you received. So, in
          addition to putting you on the road to recovery, getting prompt
          medical care helps create a record that shows the extent of the
          injuries you sustained in the accident.
        </p>
        <p>
          This type of documentation can prove invaluable to your Fountain
          Valley personal injury claim, especially if your case goes to trial.
          Even if you "feel fine" after the accident, it would be in your best
          interest to schedule a visit with your doctor. Our Fountain Valley
          personal injury lawyers often find that injury symptoms may not
          surface for days or even weeks after a traumatic event. So, the first
          step to take here is to get the right diagnosis of your injuries and
          start recovering your health.
        </p>
        <p>
          <strong>Be wary of what you say.</strong> Providing statements about
          the incident to anyone but the police or your personal injury lawyer
          can work against you. If possible, you should talk to your attorney
          before you talk to anyone else. If you have been involved in a car
          accident, simply exchange basic information with the other parties
          involved. Don't engage them in a conversation by talking about who was
          at fault. Worse, do not admit fault or make statements that suggest
          you may have been at fault. Remember, anything you say can and in all
          likelihood will be used against you.
        </p>
        <p>
          If you have been injured in an accident, the other party's insurance
          company will call you soon after. While you should report the details
          of the crash to your auto insurance company, please refrain from
          speaking with the other parties' insurers or attorneys. Never agree to
          provide a recorded statement. It is generally a good idea
          <strong>
            {" "}
            not to talk to anyone regarding the incident other than your injury
            lawyer
          </strong>
          .
        </p>
        <p>
          <strong>Do not rush into a settlement.</strong> The insurance company
          will often attempt to pressure injured victims into agreeing to a
          quick settlement. What you need to remember is that such an agreement
          tends to work in an insurance company's favor. In the aftermath of a
          personal injury accident,{" "}
          <Link to="/car-accidents/insurance-hardball">
            {" "}
            the insurance company is not your friend
          </Link>
          . Their business model is based on not paying their customers or
          settling for the least amounts possible.
        </p>
        <p>
          Minimizing these payouts is what insurance adjusters are trained to
          do. They are not looking out for you. They work for the insurance
          company and their role hinges on working out deals that benefit their
          employer. Your personal injury lawyer is the only one working for you
          and bearing your interest at heart. So, do not rush into a settlement
          or sign any type of agreement until you've talked to your injury
          lawyer. Once you do, there may not be a way to undo that step and the
          subsequent terms that it involves. You may not be able to seek any
          further compensation once you've agreed to a settlement.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="fountain valley personal injury law firm"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-fountain-valley-attorneys.jpg')"
            }}
          >
            <h2>Hiring the Right Fountain Valley Personal Injury Lawyer</h2>
          </div>
        </LazyLoad>

        <p>
          One of the most important steps for securing the compensation you
          deserve after a traumatic injury is to seek and obtain the counsel of
          an experienced Fountain Valley personal injury lawyer. There are
          certain aspects in this regard that will help you choose the right
          lawyer for your case. First, you need to find someone in your area who
          is familiar with local courts and specializes in personal injury law.
          Look for someone who has significant experience handling personal
          injury cases exclusively. Visit the attorney's website to see what
          types of cases they handle, what their success rate is and the types
          of verdicts and settlements they have obtained for injured clients in
          the past.
        </p>
        <p>
          Retain a lawyer who also has trial experience. While it is a fact that
          a vast majority of personal injury cases are settled outside a
          courtroom, you may still need a lawyer with trial experience – one who
          is not afraid to take your case to trial should the need arise. In
          some instances, the{" "}
          <Link to="https://legaldictionary.net/litigation/" target="_blank">
            litigation process
          </Link>{" "}
          may be your best option to ensure the defendant pays fair
          compensation. When you are looking to retain a lawyer, you may wish to
          consider a personal injury lawyer who is an active member of local and
          state trial lawyer groups. Attorneys who are committed to success with
          their personal injury practice tend to collaborate with other peers.
        </p>
        <p>
          You also need the law firm handling your case to have substantial
          resources, not just in terms of staffing but also when it comes to
          their ability to have expert witness and subject matter experts
          provide testimony if required. Preparing a personal injury case may
          quickly become expensive. There are a number of steps that bear
          associated costs such as taking depositions and getting the necessary
          experts to testify.
        </p>
        <p>
          Try to make an assessment regarding the law firm during your initial
          consultation. How do their offices look? Do they seem successful? How
          did the person at the front desk treat you? How did the lawyers and
          the other employees of the firm treat you during your visit? Did they
          offer you a free initial consultation? Your personal injury lawyer
          should be someone you like and trust as cases may take several months
          to years to be settled or decided.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2>Damages in a Personal Injury Case</h2>
        <p>
          There are a number of different types of damages that you could
          receive in a Fountain Valley personal injury case. Here are some of
          the damages that are most commonly awarded in such cases:
        </p>
        <ul>
          <li>
            <strong>Medical expenses:</strong> This includes costs relating to
            emergency transportation, hospitalization, cost of surgeries,
            medication, medical equipment and doctor visits. Some severely
            injured victims may incur future medical costs. For example, if you
            need to undergo back surgery as the result of an auto accident in
            which you were injured a year ago, then, that expense should also be
            part of the compensation you receive.
          </li>
          <li>
            <strong>Lost wages:</strong> When you are injured in a traumatic
            incident, you may not be able to return to work for a while or until
            you completely recover from your injuries. You may lose days, weeks
            or even months of work resulting in loss of income and severe
            financial challenges for you and your family. This is particularly
            true in cases where the injured victim is the sole breadwinner or
            primary wage earner. Families may also suffer without health or
            other benefits during this time. These types of losses should be a
            part of the personal injury claim.
          </li>
          <li>
            <strong>Rehabilitation costs:</strong> Most injured victims need
            some type of rehabilitative treatment of therapy to get them back to
            their pre-accident condition. For example, individuals who have
            suffered broken bone injuries may require several weeks of
            rehabilitation to regain strength and mobility in the affected area.
            This might include physical therapy, chiropractic care and so on.
            Often, health insurance may not cover these types of rehabilitative
            costs. The patient may have to pay these expenses out of pocket.
            However, these are expenses you should include in your personal
            injury claim.
          </li>
          <li>
            <strong>Pain and suffering:</strong> When you are injured in a
            Fountain Valley accident, you may not only experience physical pain,
            but also emotional trauma. Injured victims may become depressed or
            suffer a post-traumatic stress disorder or PTSD, which might require
            counseling and treatment. A number of these traumatic incidents such
            as car accidents and dog bites also result in scarring and
            disfigurement. This could have a serious psychological impact on
            victims as well.
          </li>
          <li>
            <strong>Permanent injuries:</strong> Unfortunately, a number of
            victims may never completely recover from their injuries. This is
            particularly true in cases where victims have sustained catastrophic
            injuries such as amputations. In these situations, they may never be
            able to return to their jobs or earn a subsequent livelihood. Such
            victims may be entitled to receive disability benefits and
            additional compensation for permanent injuries, disabilities and
            loss of future income and inability to earn a livelihood.
          </li>
          <li>
            <strong>Wrongful death:</strong> When a loved one is killed as a
            result of someone else's negligence or wrongdoing, you may be able
            to seek compensation by filing a wrongful death claim. Such claims
            seek compensation for damages including medical expenses, funeral
            and burial costs, lost future income, pain and suffering and loss of
            love, care and companionship.
          </li>
        </ul>
        <h2>Why Contact Bisnar Chase?</h2>
        <p>
          At Bisnar Chase, we are committed to providing our clients with
          quality legal representation and recovering just compensation for
          them. We are proud of the track record we have achieved of getting
          positive results for our clients throughout Orange County. We have
          recovered money in over <strong>96 percent of our cases</strong>. We
          have been advocating for average citizens and consumers for forty
          years – inside and outside of the courtroom.
        </p>
        <p>
          We also provide a <strong>no-win-no-fee guarantee</strong>, which
          means you do not pay anything unless we recover compensation for you.
          Initial consultations are always free. With more than $500 Million won
          including punitive damages and thousands represented, our firm has
          earned a reputation as one of California's toughest injury law firms.
          Please call 949-203-3814 for a free consultation with one of our
          experienced injury lawyers in Fountain Valley.
        </p>

        <MiniLocationWidget {...locationWidgetOptions} />

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
