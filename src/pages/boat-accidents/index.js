// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Boat Accident Lawyers - Boating Accident Attorney"
        pageDescription="The experienced California boat accident lawyers at Bisnar Chase have the skill, knowledge, and resources to hold negligent watercraft operators accountable. Our top rated maritime injury lawyers have won hundreds of millions for people hurt in boating accidents including cruise ships."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Boat Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          The experienced California boat accident lawyers at Bisnar Chase have
          the skill, knowledge and resources to hold negligent watercraft
          operators and defective product manufacturers accountable. If you or a
          loved one has been injured in a boating accident, please contact our
          experienced team at <strong>800-561-4887</strong> for a free
          consultation and a comprehensive case evaluation.
        </p>

        <div
          className="text-header content-well"
          title="Boat dock"
          style={{
            backgroundImage: "url('/images/text-header-images/Boat-crash.jpg')"
          }}
        >
          <h2>Boat Accidents</h2>
        </div>
        <div className="algolia-search-text">
          <p>
            Each year, thousands of people are injured in accidents involving
            boats and other watercraft. Whether the accidents occur on
            speedboats, sailboats, jet skis, party boats, cruise ships or a
            privately owned vessel, these accidents have the potential to result
            in major injuries or even fatalities. Determining fault and
            liability in watercraft accidents can be challenging and complex.
          </p>
          <p>
            If you or a loved one has been injured in a boating accident please
            contact an experienced{" "}
            <Link to="/" target="new">
              <strong>California Personal Injury Lawyer</strong>
            </Link>{" "}
            at Bisnar Chase today to schedule a free no-obligation case
            evaluation
          </p>
          <p>
            Victims and their families would be well advised to learn about
            their legal rights in such cases. Owners and operators have a
            responsibility to pilot the craft in a safe and lawful manner and to
            ensure the safety of passengers at all times. Boating injuries
            usually occur because of a reckless or careless operator, operating
            the vessel under the influence, boat malfunction, or an
            inexperienced operator.
          </p>
        </div>
        <h2>California Watercraft Accident Statistics</h2>
        <p>
          There are close to 1,000,000 watercraft registered in the State of
          California including pleasure boats, sailboats, powerboats and
          personal watercraft such as jet skis. California also has the second
          highest accident rate involving watercraft in the United States. The
          most common boating accidents are collisions with other boats,
          personal injuries while on the boat, sinking, and boat disappearance.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.dbw.ca.gov/" target="new">
            California Department of Boating and Waterways
          </Link>{" "}
          (DBW) accident report, 244 boating accidents occurred when a boat was
          just cruising and another 72 crashes took place while a boat was
          drifting in the water. Many of the most devastating boating accidents
          involved high-speed collisions and water skiing accidents, but any
          type of crash can prove fatal when it occurs on the water.
        </p>
        <p>
          If you have lost a loved one in result of a boat accident, contact our
          highly experienced <strong>Boat Accident Lawyers</strong> at{" "}
          <strong>800-561-4887</strong> for a free consultation and a
          comprehensive case evaluation.
        </p>
        <p>
          The agency states that 627 California boating accidents resulting in
          398 injuries and 49 fatalities happen in a single year. The most
          popular locations for California boating accidents include:
        </p>
        <ul>
          <li>Southern Coast: 203 accidents, 11 fatalities, 96 injuries</li>
          <li>
            Northern Lake: 185 boat crashes, seven fatalities, 143 injuries
          </li>
          <li>Delta: 81 watercraft accidents, 10 fatalities, 60 injuries</li>
          <li>Southern Lake: 69 crashes, six fatalities, 41 injuries</li>
          <li>
            Colorado River: 30 boating accidents, three fatalities, 30 injuries
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dangerous Boating"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dangerous-watersports.jpg')"
            }}
          >
            <h2>Fun But Dangerous Activities</h2>
          </div>
        </LazyLoad>
        <p>
          Taking family and friends out on the water can be a great time for
          relaxation, action sports and fun. As most people are aware, water can
          make things much more slippery, difficult to stay in control and
          dangerous.
        </p>
        <p>
          Action sports and activities usually involve the use of speed,
          accessories (skis, rafts, boards, etc.) and the need for open
          water-space. When the environment becomes restrictive, the
          maneuverability or driving of the watercraft is neglected or misused
          or alcohol is involved, an extremely hazardous and potentially deadly
          situation is created.
        </p>
        <p>
          Here are some steps you can take to make sure your boating and
          watersport activities are performed as safely as possible:
        </p>
        <ul>
          <li>
            Never consume alcohol while driving. You can get a DUI just as easy
            on a boat as you can in a vehicle
          </li>
          <li>
            Don't allow intoxicated individuals to even ride on the watercraft.
            Intoxicated individuals do not have a good sense of judgment,
            function or perception and can pose a danger to not only themselves,
            but other passengers and pedestrians in and out of the water
          </li>
          <li>
            Always make sure the captain or driver of the boat or watercraft is
            always at attention, not driving recklessly and following all safety
            precautions, rules, laws and speedlimits
          </li>
          <li>
            Never drive a boat or watercraft at night without properly
            illuminated navigation lights on the bow, stern, starboard and port
            sides
          </li>
          <li>
            Don't allow or encourage dangerous stunts or activities while the
            boat is in motion, in place or at all.
          </li>
        </ul>
        <p>
          You should contact one of our Personal Injury Attorneys if you or a
          loved has been injured in a boating accident. For immediate assistance
          call <strong>800-561-4887</strong>.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Drinking on a boat"
            style={{
              backgroundImage:
                "url('/images/text-header-images/alcohol-and-boats.jpg')"
            }}
          >
            <h2>Alcohol and Boating</h2>
          </div>
        </LazyLoad>
        <p>
          Many times boating activities like the fourth of July, bachelor's
          party, guys fishing trip or just a sunday of fishing, include
          alcoholic beverages on-board. When alcoholic substances are involved,
          the risk of accidents, injuries and death is greatly increased.
        </p>
        <p>
          Although people tend to believe drinking alcohol and driving boats is
          acceptable, you can still get a DUI as easy on a boat, vessel or
          watercraft as you can in a vehicle because the same rules apply.
        </p>
        <p>
          If you are caught driving a boat, watercraft or vessel while
          intoxicated by drugs and or alcohol, you may be arrested, fined
          thousands of dollars, additional penalties, AA workshops, community
          service, the suspension of your license and more.
        </p>
        <p>
          For more information on DUI laws, DUI charges and driving laws, visit{" "}
          <Link to="https://www.drivinglaws.org/" target="new">
            www.drivinglaws.org/
          </Link>
        </p>
        <h2>Types of Boating Accidents</h2>
        <p>
          The majority of California boating accidents involve a collision with
          another vessel. Some of the other most common types of watercraft
          accidents include:
        </p>
        <ul>
          <li>Flooding</li>
          <li>Skier mishaps</li>
          <li>Grounding</li>
          <li>Sinking</li>
          <li>Falls overboard</li>
          <li>Capsizing</li>
          <li>Crashes with submerged objects</li>
          <li>Collisions with fixed objects</li>
        </ul>
        <h2>Causes of Boating Accidents</h2>
        <p>
          The Department of Boating and Waterways reports that operator
          inattention is the most common cause of California boating accidents.
          Operator inexperience and excessive speed are the second and third
          most common causes of boat accidents. This means that the top three
          causes of California boat accidents involve operator negligence. In
          such cases, the at-fault boat operator can be held accountable for the
          victims' injuries, damages and losses.
        </p>
        <p>
          Operators are not solely responsible for all boating accidents. In the
          year Machinery failure is responsible for nine percent of all crashes.
          When a defective boat part causes an injury accident, it may be
          possible to hold the manufacturer accountable for the victims' losses.
          Victims of mechanical malfunctions would be well advised to preserve
          their boats for a thorough inspection by a qualified expert.
        </p>
        <h2>California Boating Laws</h2>
        <p>
          Owners and operators of boats in California are required to follow
          state and federal laws when it comes to the maintenance and operation
          of their vessels. Similar to car drivers, boat operators are
          prohibited from driving in a reckless or negligent manner, which may
          endanger others. Operators must not drive near or through areas
          designated for swimmers, surfers or body boarders.
        </p>
        <p>
          All California boat operators must have an adequate number of life
          jackets and fire extinguishers onboard. They also must display
          navigation lights, fix fuel leaks and ensure that there is proper
          ventilation. It is also vital for all boat operators to remain
          completely sober. Impaired driving is a particularly common cause of
          boating accidents. Many boat operators make the mistake of thinking
          they are allowed to drive drunk because they are on the water. This is
          a tragic error that leads to many fatalities each year.
        </p>
        <h2>If You Have Been Injured</h2>
        <p>
          If you or a loved one has been harmed in a watercraft accident, it may
          be possible for you to seek compensation for your injuries, damages
          and losses. It is important that you act quickly so that the incident
          can be properly investigated before evidence is lost. Similar to car
          accidents, the authorities should be notified whenever someone has
          been injured in a California boat accident. It is also advisable to:
        </p>
        <ul>
          <li>
            Write down specific details regarding how and where the crash
            occurred.
          </li>
          <li>Take photos of the damaged boat and of the injuries suffered.</li>
          <li>
            Collect contact information from anyone who may have witnessed the
            crash.
          </li>
          <li>Get your side of the story into the official report.</li>
          <li>Seek immediate medical attention.</li>
        </ul>
        <p>
          Depending on the cause of the accident, you may be able to seek
          financial support for damages including medical bills, lost wages,
          hospitalization, permanent injuries, pain and suffering and emotional
          distress.
        </p>
        <p>
          Please contact an experienced{" "}
          <strong>California Boat Accident Lawyer at Bisnar</strong> Chase today
          to schedule a free no-obligation case evaluation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
