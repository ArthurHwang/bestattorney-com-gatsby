import React from "react"
import Helmet from "react-helmet"
import { LayoutDefault } from "src/components/layouts/Layout_Default"
import { AdminLogin } from "../../components/elements/admin/Admin_Login"

export default function LoginPage() {
  return (
    <LayoutDefault>
      <Helmet meta={[{ name: "ROBOTS", content: "NOINDEX, NOFOLLOW" }]} />
      <AdminLogin path="/admin/login" />
    </LayoutDefault>
  )
}
