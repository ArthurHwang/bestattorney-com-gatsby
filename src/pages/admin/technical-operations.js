import { Router } from "@reach/router"
import React from "react"
import { LayoutAdmin } from "../../components/layouts/Layout_Admin"
import { PrivateRoute } from "../../components/elements/admin/Admin_PrivateRoute"
import { TechnicalOperations } from "../../components/elements/admin/Admin_TechnicalOperations"

export default function TechnicalOperationsPage() {
  return (
    <LayoutAdmin>
      <Router>
        <PrivateRoute
          path="/admin/technical-operations"
          component={TechnicalOperations}
        />
      </Router>
    </LayoutAdmin>
  )
}
