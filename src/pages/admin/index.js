import { Router } from "@reach/router"
import { navigate } from "gatsby"
import React from "react"
import { LayoutAdmin } from "../../components/layouts/Layout_Admin"
import { AdminHome } from "../../components/elements/admin/Admin_Home"
import { PrivateRoute } from "../../components/elements/admin/Admin_PrivateRoute"
import { isLoggedIn } from "../../components/utilities/auth"

export default function AdminPage() {
  return (
    <LayoutAdmin>
      {isLoggedIn ? (
        <Router>
          <PrivateRoute path="/admin" component={AdminHome} />
        </Router>
      ) : (
        navigate(`/admin/login`)
      )}
    </LayoutAdmin>
  )
}
