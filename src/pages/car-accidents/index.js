// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/california-car-accident-lawyers.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Car Accident Lawyer | Auto Injury Attorneys - Bisnar Chase"
        pageDescription="Contact the experienced California car accident lawyers with over 40 years in winning auto injury cases. Free consultation and a 96% success rate, we have won over $500 Million for our clients. No win, no fee guarantee. Experienced Trial Attorneys specializing in motor vehicle accidents."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Car Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california car accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            Have your case reviewed for free. The
            <strong> California Car Accident Lawyers</strong> at{" "}
            <strong>Bisnar Chase</strong> have{" "}
            <strong>40 years of experience</strong> and have won over{" "}
            <strong>$500 Million</strong> for our clients. Having a trial
            attorney is important in winning your auto accident case and getting
            you the best compensation possible. Not all personal injury
            attorneys go to trial. We do with winning results.
          </p>
          <p>
            If you've been involved or injured in an motor vehicle accident
            please call our Southern{" "}
            <Link to="/" target="new">
              California personal injury lawyers
            </Link>{" "}
            at <strong>800-561-4887</strong> to discuss your rights and
            compensation.
          </p>
          <p>
            Time is of the essence to preserve your rights. Our law firm will
            provide you a free consultation that will allow us to assess the
            accident and determine the best option for you.
          </p>

          <p>
            Bisnar Chase has been representing auto accident victims since 1978
            and has always put our clients and their case first. Our clients are
            treated to a first class experience:{" "}
            <Link to="/about-us/no-fee-guarantee-lawyer" target="new">
              You won't pay anything unless your case is won
            </Link>
            . We'll also shield you from financial responsibility until the case
            is over. Year after year we've been rated as one of the best car
            accident lawyers in California.
          </p>
        </div>
        <h2>Choosing the Right Car Accident Lawyer for Your Case</h2>
        <LazyLoad>
          <img
            className="imgleft-fixed"
            src="/images/attorney-chase.jpg"
            height="141"
            alt="Brian Chase, California car accident lawyer"
          />
        </LazyLoad>
        <p>
          Having the right <strong>California Car Accident Lawyer</strong> on
          your side is paramount to a fair outcome of your case. Inexperience
          and lack of resources could cost you. Our legal team has the
          resources, experience and passion to win very difficult cases.
        </p>
        <p>
          We provide a superior client experience giving you the highest quality
          of care. We will fight to get you the largest recovery possible from
          your automobile accident.
        </p>
        <p>
          From protecting your lost wages to shielding you from financial
          responsibility, we provide a framework of experience crafted by 9
          trial lawyers with decades of winning difficult car accident cases
          throughout California
        </p>
        <div className="snippet">
          <p>
            <span>
              {" "}
              <Link to="/attorneys/brian-chase" target="new">
                Brian Chase
              </Link>
              , Senior Partner:
            </span>{" "}
            “I always knew I wanted to practice injury law. I wanted to make a
            difference in people's lives and hold wrongdoers accountable.”
          </p>
        </div>
        <h2>Negligent Parties Must Pay Fair Compensation</h2>
        <p>
          If you are injured in any type of auto accident, including a{" "}
          <Link to="/pedestrian-accidents/hit-and-run" target="new">
            hit-and-run car accident
          </Link>
          , you may be eligible to collect compensation from any negligent
          parties. If you have sustained an injury, you should hold the
          negligent parties responsible for any loss of income and medical
          expenses. Through no fault of your own, an injury has occurred.{" "}
          <strong>You are entitled to fair compensation.</strong>
        </p>
        <p>
          By contacting one of our qualified auto accident attorneys in
          California that specialize in serious motor vehicle injuries you are
          making the smartest choice in legal representation. We will
          investigate the car accident scene, the vehicles, along with any other
          evidence that came into play at the time of your auto accident.{" "}
        </p>
        <p>
          We will stand up for you against any insurance company, wrongdoer,
          trucking company or anyone else who has caused you harm through no
          fault of your own. It is our responsibility to make sure your rights
          are upheld and defended.
        </p>
        <h2>The Dangers of Driverless Vehicles</h2>
        <p>
          Self driving cars also referred to as robot cars are becoming a
          reality. With Tesla and Google taking the helm in leading the
          autonomous car revolution,{" "}
          <strong>more people are being injured</strong> while they continue to
          use our highways to test the self driving cars. So far there have
          already been several deaths linked to driverless cars as well as a few
          that have caught on fire while parked and not running. Some of these
          fires have been linked to the ion batteries in the vehicles. There are
          pros and cons to driverless cars but our national highways should not
          be a test track. Until the safety features of these driverless cars is
          further tested we should not have no one at the wheel. If you've been
          injured by a self driving car, contact an injury attorney well versed
          in these type of accidents.
        </p>
        <h2>Auto Defects Can Kill</h2>
        <p>
          Millions of cars are recalled every month for a variety of auto
          defects. While vehicle recall laws do help, many times the recall
          doesn't take place until someone is injured or killed. From safety
          belts to exploding airbags, we have built a reputation for being one
          of the best <Link to="/auto-defects">auto defect law firms</Link> in
          the country. Our managing partner Brian Chase is a national auto
          defect lawyer with decades of experience and regularly teaches other
          lawyers how to try these difficult and complicated cases.
        </p>
        <h2>Top 5 Most Common Causes of Motor Vehicle Accidents</h2>
        <p>
          Driving is a daily task most of us need to do, to ensure we take care
          of our professional, personal and recreational lives.
        </p>
        <p>
          Getting from one place to another isn't always easy without a vehicle,
          that being said, with the large amount of others using similar forms
          of transportations, along the same highways, at the same time, can
          become hazardous.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/insurance-negotiating-car-accident-victim.jpg"
            width="100%"
            id="insured-car-accident"
            alt="vehicle accident lawyer"
          />
        </LazyLoad>
        <p>
          Here is a list of the{" "}
          <strong>Top 5 Most Common Causes of Car Accidents</strong>:
        </p>
        <ol>
          <li>
            <strong>Speeding</strong> - The problem with speeding, is that it is
            unsafe. Traveling at excessive speeds, especially around other
            drivers and pedestrians, can prove fatal in a matter of
            milliseconds. When speeding, it is more difficult to observe road
            hazards, judge a turn, or stop in the situation of a sudden and
            unavoidable obstacle. The speed limit is set for a reason.
          </li>
          <li>
            <strong>Drunk Driving</strong> - Driving under the influence of
            substances can very often result in catastrophic accidents. Drunk
            driving kills too many people every day, being entirely avoidable.
            Innocent and vulnerable lives that are driving in nearby cars,
            pedestrians or people in buildings, nobody is safe from a drunk
            driver.{" "}
            <Link to="https://www.madd.org/" target="new">
              <strong>Mothers Against Drunk Driving</strong> (
              <strong>MADD</strong>)
            </Link>{" "}
            is a{" "}
            <Link
              to="https://en.wikipedia.org/wiki/Nonprofit_organization"
              title="Nonprofit organization"
              target="new"
            >
              nonprofit organization
            </Link>{" "}
            in the United States and Canada that seeks to stop{" "}
            <Link
              to="https://en.wikipedia.org/wiki/Driving_under_the_influence"
              title="Driving under the influence"
              target="new"
            >
              drunk driving
            </Link>
            , support those affected by drunk driving, prevent underage
            drinking, and strive for stricter impaired driving policy, whether
            that impairment is caused by alcohol or any other drug.
          </li>
          <li>
            <strong>Distracted Driving</strong> - We all think we are able to
            perfectly drive and use our cell phones to talk and text, use social
            media, as well as other devices, music players, cameras and anything
            that can distract our attention from driving a heavy and dangerous
            vehicle. The problem is, we can't, and unfortunately it is usually
            too late before we realize ourselves that we can not safely
            multi-task while driving.
          </li>
          <li>
            <strong>Auto Defects</strong> - Cars, trucks, vans, motorcycles and
            basically any other type of vehicle is human-made, which means it is
            not perfect. The down side of having vehicles that are not perfect
            means that their are going to be auto defects and recalls on certain
            products, auto parts and accessories that can be fatal to those
            driving them or nearby drivers and pedestrians. Brake failure,
            seatbelt malfunctions, roof crush and other defects can make your
            drive unsafe and in the event in an accident, possibly fatal.
          </li>
          <li>
            <strong>Driver Error</strong> - Next to distracted driving, a driver
            unable to properly judge a turn in the road, distance between them
            and a stopped object or other vehicle can result in
            driver-related-accidents, caused by driver error. Inexperienced
            drivers, teenagers or adults getting their license for the first
            time or first time in a long time can be dangerous to themselves,
            passengers and other drivers and or pedestrians. Stay alert, stay
            safe, stay alive.
          </li>
        </ol>
        <h2>Most Common Types of Car Accidents</h2>
        <ul>
          <li>
            <strong>Head-on Collisions</strong>: head on crashes are extremely
            injurious most of the time. Many head on collisions occur when a
            vehicle is passing and typically cause catastrophic front end damage
            often leading to very serious injuries.{" "}
          </li>
          <li>
            <strong>T-Bone Accidents</strong>: A T-Bone collision is when the
            middle of the vehicle is hit by another vehicle's front or back end.
          </li>
          <li>
            <strong>Rollover Crashes</strong>: A rollover accident can be very
            deadly especially if there is a roof crush involved.
          </li>
          <li>
            <strong>Sideswiping</strong>: This usually happens when one vehicle
            drifts into another lane or when a driver attempts to pass.
          </li>
          <li>
            <strong>Hit and Run:</strong> Fleeing the scene of a car accident is
            not only illegal but it can put the injured person in grave danger.
          </li>
        </ul>
        <h2>Common injuries from a car accident</h2>
        <ul>
          <li>Spinal cord injuries</li>
          <li>Head injuries</li>
          <li>Broken bones</li>
          <li>Brain damage</li>
          <li>Paralysis</li>
          <li>Sever fractures</li>
          <li>Massive bruising</li>
          <li>PTSD and emotional issues</li>
        </ul>
        <h2>Time to File a Claim for Car Accidents</h2>
        <p>
          Auto accident victims have a limited amount of time to file a claim
          for damages. If you or a loved one have suffered injuries in a serious
          vehicle accident,{" "}
          <Link to="/car-accidents/injury-compensation" target="new">
            obtaining injury compensation
          </Link>{" "}
          for your medical bills as well as pain and suffering is practically
          impossible if you have passed the time limit to file a claim. You are
          limited to{" "}
          <strong>
            2 years to file your car accident claim and 3 years to file a
            property damage claim.
          </strong>{" "}
          The benefits of hiring a California auto accident lawyer will be{" "}
          <strong>getting the lawsuit filed in time</strong> if negotiations
          with the other party have gone on too long.
        </p>
        <p>
          <strong>Contact us at 800-561-4887</strong> to receive a{" "}
          <strong>Free Case Evaluation </strong>and{" "}
          <strong>Consultation</strong> and find out what needs to be done with
          your case.
        </p>
        <h2>Protecting Your Interests</h2>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/n549C_jLzjU"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Vehicle accidents are one of the{" "}
          <Link
            to="https://www.cdc.gov/nchs/fastats/accidental-injury.htm"
            target="new"
          >
            leading causes of injury and death
          </Link>{" "}
          in the United States. A car accident lawyer is essential to help
          protect your interests. As more and more vehicles enter our roadways,
          more risks and threats are present to drivers. If you are involved in
          a car accident, it is important to seek the help of qualified car
          accident attorneys who have practiced in California. Bisnar Chase has
          been representing injured plaintiffs in California for 40 years and
          have won over $500 Million for our clients.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california car accident attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/after-car-accident-california.jpg')"
            }}
          >
            <h2>Auto Accident Checklist</h2>
          </div>
        </LazyLoad>

        <ul>
          <li>
            Seek immediate medical attention and tell your physician everything
            you can about the accident including any pain you are having.
          </li>

          <li>File a police report as soon as possible.</li>

          <li>
            Get the insurance information from the other driver(s) including
            their driver's license number.
          </li>

          <li>Get all witnesses names, addresses and telephone numbers.</li>

          <li>
            Take a good amount of pictures of the accident scene. Include all
            cars involved and photograph all visible injuries of all victims in
            the auto.
          </li>

          <li>
            Do not talk to anyone about the accident or injuries especially an
            insurance adjuster. Only discuss with your doctor or attorney
            regarding treatment.
          </li>

          <li>
            Before you sign anything consult a qualified California car accident
            lawyer to avoid mistakes.
          </li>

          <li>
            If you are disoriented, confused or having trouble remembering
            things, please report it. These are all symptoms of{" "}
            <Link to="/head-injury" target="new">
              brain trauma
            </Link>
            .
          </li>
          <li>
            Educate yourself with pertinent{" "}
            <Link to="/resources" target="new">
              car accident resources
            </Link>
            .
          </li>
        </ul>
        <h2>Common Mistakes After a Vehicle Accident</h2>
        <ul>
          <li>
            Posting information on social media including pictures or making
            written statements that can be used against you.
          </li>
          <li>
            Not recognizing the seriousness of your injuries right away. This
            can lead to additional injuries down the road.
          </li>
          <li>
            Speaking with insurance companies from either side. The first phone
            call should be to your lawyer.
          </li>
          <li>
            Not being 100% truthful with your attorneys. This can affect your
            case if information comes out later to the defense.
          </li>
          <li>
            Not collecting evidence. Take pictures, obtain witness statements
            &amp; file a police report.
          </li>
          <li>
            Admitting liability. Only your lawyer should be discussing the case
            with you.
          </li>
        </ul>
        <h2>The Settlement Process</h2>
        <p>
          Your injury attorney will discuss the details of your case with you in
          a straightforward manner. Including the odds of having to go to trial.
          A good car accident legal team will have the ability to go to trial if
          needed. Of course, if this can be avoided while still getting you the
          maximum coverage then that is a good outcome. Don't let an attorney
          talk you down in terms of settlement if you feel its not a fair
          representation of your injuries and suffering.{" "}
          <strong>
            To reach an experienced California car accident lawyer please call
            us toll free at 800-561-4887.
          </strong>
        </p>
        <h2>Awards &amp; Honors</h2>
        <ul>
          <li>2019 Million Dollar Advocate</li>
          <li>2019 Lawyer of the Year</li>
          <li>2019 Nation's Premiere Top Ten Attorneys by NAOPIA</li>
          <li>2019 America's Top 100 Attorneys</li>
          <li>2017 Top 1% by the Natl. Assoc. of Distinguished Counsel</li>
          <li>2017 Top 100 High Stakes Litigator</li>
          <li>2016 Top 10% Lawyers of Distinction</li>
          <li>2016 SuperLawyer</li>
          <li>2016 Best Lawyers</li>
          <li>
            2015 {" "}
            <Link
              to="https://www.caoc.org/index.cfm?pg=CAOC-Board"
              target="_blank"
            >
              CAOC
            </Link>
             President
          </li>
          <li>2015 NADC Top One Percent Lawyers</li>
          <li>
            2014, 2004 OCTLA Trial Lawyer of the Year for Products Liability {" "}
            <Link to="/press-releases/brian-chase-award" target="new">
              OCTLA
            </Link>
          </li>
        </ul>
        <h2>Some of Our Car Accident Case Results</h2>
        <ul>
          <li>$16,444,904.00 - Dangerous road condition, driver negligence</li>
          <li>$9,800,000.00 - Motor vehicle accident</li>
          <li>$8,500,000.00 - Motor vehicle accident - wrongful death</li>
          <li>$2,432,250.00 - Motor vehicle accident</li>
          <li>
            $2,360,000.00 - Wrongful death, commercial vehicle - motor vehicle
            accident
          </li>
        </ul>
        <h2>Superior Client Representation</h2>
        <p>
          Too often, victims of serious injuries due to car accidents receive
          incorrect information and make several decisions that negatively
          affect the outcome of their car accident lawsuit.
        </p>
        <p>
          {" "}
          Car crash victims do not have the luxury of receiving second chances;
          insurance companies will utilize any means to give them grounds for
          denying your car accident claim and are counting on you to dig your
          own hole. Unfortunately, this strategy is extremely effective and many
          auto collision victims are left with a lifetime of regret.{" "}
        </p>
        <p>
          {" "}
          Bisnar Chase car accident attorneys have been setting the bar for
          superior client representation and courtroom accomplishments for over
          40 years. Having represented over 12,000 clients our accident lawyers
          know exactly how to get maximum compensation for ones injuries in as
          little time as possible.{" "}
        </p>
        <p>
          {" "}
          Our accident attorneys utilize aggressive litigation combined with
          extensive experience and professionalism to reach case outcomes that
          could not be obtained anywhere else. Our reputation precedes us and we
          are able to utilize our national presence to come to quick and
          favorable resolutions to our client's cases.{" "}
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents" target="new">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents" target="new">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        <p>
          {" "}
          <Link to="/car-accidents/rental-car-safety" target="new">
            Related: A Guide to Dealing With Rental Car Accidents
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
