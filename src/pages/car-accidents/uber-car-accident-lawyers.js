// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/banner-uber-accident-lawyer.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Uber Car Accident Lawyers California - Bisnar Chase"
        pageDescription="The experienced Uber car accident lawyers of Bisnar Chase can help you receive fair compensation from a ride-share car crash. Free consultation & 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Uber Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Uber Car Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced{" "}
          <Link to="/car-accidents">California car accident lawyers</Link> at
          Bisnar Chase understand the challenges presented by incidents
          involving rideshare companies such as Uber. If you have been injured
          in a car accident involving an Uber vehicle, there are a number of
          complex legal questions, particularly those involving liability.
        </p>
        <p>
          Whether you are a passenger in the Uber vehicle or a driver or
          passenger in a vehicle that was struck by an Uber vehicle, or a
          bicyclist or pedestrian involved in a collision with an Uber vehicle,
          it is important that you understand your legal rights and options.
        </p>
        <p>
          <strong>
            Call us at 800- 561-4887 to find out how we can help you.
          </strong>
        </p>
        <h2>How Does Uber Work?</h2>
        <p>
          Uber is a{" "}
          <Link to="/car-accidents/rideshare-accident-lawyer">
            ride-sharing service
          </Link>
          , which allows customers to request a ride from nearby drivers using
          the company's iPhone or Android app. Within minutes, the vehicle
          arrives and transports the passenger to the desired location.
        </p>
        <p>
          Uber bills itself as a "technology platform" that connects smart phone
          users through its app to drivers in the area. Uber has been growing at
          a rapid rate and along with other rideshare companies, is really
          giving local taxi companies a run for their money.
        </p>
        <p>
          However, an increase in Uber ridership has come with a rise in the
          number of accidents involving Uber vehicles.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/witness-car-accident-sized.jpg"
            width="100%"
            alt="Uber car crash attorneys in California"
          />
        </LazyLoad>
        <h3>If You are Involved in an Accident</h3>

        <p>
          There are several steps you can and should take if you are involved in
          an accident with an Uber driver or just any driver:
        </p>

        <ul type="disc">
          <li>
            Contact the police. File a report with the appropriate law
            enforcement agency and obtain a copy of that report for your own
            records.
          </li>
          <li>
            Get the license plate number and contact information for other
            drivers or parties involved.
          </li>
          <li>
            Talk to any potential witnesses, anyone who may have witnessed the
            crash.
          </li>
          <li>
            Take photographs of the accident scene including the position of the
            vehicles and any injuries you may have suffered.
          </li>
          <li>
            Collect insurance information for all parties involved. If you are
            an injured Uber passenger, make sure you get all information from
            your Uber driver including his or her driver's license number,
            vehicle license plate and insurance information.
          </li>
          <li>
            Get prompt medical attention, treatment and care for any injuries
            you may have sustained.
          </li>
        </ul>
        <h2>&nbsp;What's Different about an Uber Car Accident?</h2>
        <p>
          Uber's drivers are "independent contractors" and not employees. This
          often helps them deny responsibility for any injuries sustained in an
          accident.
        </p>
        <p>
          If you are a passenger in an Uber car during an accident, remember
          that you are entitled to obtain compensation for your losses both from
          the driver's insurance policy and Uber's additional $1 million per
          ride policy.
        </p>
        <p>
          It is also important to remember that Uber defines itself as a
          technology platform, not as a transportation company or taxi service.
        </p>
        <p>
          This allows the company to point its finger at the driver, making it
          more challenging for passengers, pedestrians and other drivers to make
          complaints and claims against Uber.
        </p>
        <p>
          If you are involved in an Uber crash, there is no customer service
          phone number to call, just an e-mail address. This makes communication
          extremely difficult. It is quite easy for victims to get discouraged.
        </p>
        <p>
          Also, until recently, Uber did not provide coverage for drivers
          between trips when there is no customer in the car.
        </p>
        <p>
          But after intense criticism and scrutiny following a tragic pedestrian
          accident in San Francisco, the company changed its policy allowing
          some coverage for drivers between trips.
        </p>
        <p>
          It is important to remember that regardless of how confusing these
          details may be, you are entitled to compensation if you have been
          injured by an Uber driver, whether you are a passenger in the Uber,
          the driver or passenger in another vehicle, or if you are a bicyclist
          or pedestrian.
        </p>
        <p>
          You need an experienced car accident lawyer on your side who can fight
          for your rights and help you obtain maximum compensation for your
          losses.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Uber collision lawyers in California"
            style={{
              backgroundImage:
                "url('/images/car-accidents/uber-victim-rights-text-header.jpg')"
            }}
          >
            <h2>Protecting Your Rights</h2>
          </div>
        </LazyLoad>
        <p>
          Fighting with insurance companies or large corporations such an Uber
          can be an uphill task for the average person.
        </p>
        <p>
          But, for a law firm like Bisnar Chase, it is part of what we do on a
          daily basis. We stand up against insurance companies and large
          corporations on behalf of our seriously injured clients and fight hard
          to secure maximum compensation for their losses.
        </p>
        <p>
          We have a 96 percent success rate with our cases and also offer a{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            no-win, no-fee guarantee
          </Link>{" "}
          to our clients.
        </p>
        <p>
          This means you don't pay any fees or costs unless we secure
          compensation for you.{" "}
          <strong>
            Call us at 800-561-4887 for a free consultation and comprehensive
            case evaluation.
          </strong>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
