// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { regex: "/bone-broken-image.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bone Fractures in a Car Accident - Types of Broken Bone Injuries"
        pageDescription="Broke a bone in a car accident? Find out types of bone fractures and other broken bone injuries and treatment from a serious or catastrophic motor vehicle accident. A car accident can result in pelvic fractures, broken ribs and even skull or spinal fractures."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Types of Bone Fractures in a Car Accident</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Different kinds of broken bones in a car crash"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong>Broken bones</strong> are often a common consequence of{" "}
            <Link to="/car-accidents">car accidents</Link> because of the
            extreme force and impact of such incidents. A car accident can
            result in pelvic fractures, broken ribs and even skull or spinal
            fractures. Other broken bone injuries that can prove debilitating
            include fractures to the ankle, thighbone, collarbone, neck and
            spine. Sometimes, these fractures may require surgery.
          </p>
          <p>
            In other cases, recovery could take a substantial amount of time.
            Victims may have to undergo physical therapy or extensive
            rehabilitation. This could result in significant medical expenses
            and loss of wages due to time away from work.
          </p>
          <h2>Types of Bone Fractures</h2>
          <p>
            There are many ways in which your bone can break during a traumatic
            incident such as a car accident. Types of{" "}
            <Link
              to="https://orthoinfo.aaos.org/en/diseases--conditions/fractures-broken-bones/"
              target="_blank"
            >
              bone fractures (broken bones)
            </Link>{" "}
            that commonly result from car crashes include:
          </p>
        </div>
        <ul>
          <li>
            <p>
              <strong>Displaced fracture</strong>: A displaced fracture is when
              the bone snaps and moves so that the end of the broken parts no
              longer line up. When the bone is in many pieces, it is called a
              comminuted fracture.
            </p>
          </li>
          <li>
            <p>
              <strong>Non-displaced fracture</strong>: When a fracture is
              non-displaced, that means that the bone has cracked but it retains
              its proper alignment.
            </p>
          </li>
          <li>
            <p>
              <strong>Open fracture</strong>: When someone suffers an open bone
              fracture, the bone actually breaks through the skin. The bone may
              recede back into the wound or it may remain visible. This is a
              particularly dangerous kind of fracture because it comes with the
              risk of a deep bone infection.
            </p>
          </li>
          <li>
            <p>
              <strong>Closed fracture</strong>: A closed fracture is when the
              bone is broken but it does not puncture the skin.
            </p>
          </li>
        </ul>
        <h2>Who is at Risk?</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/old-man-hurt-image.jpg"
            width="160"
            className="imgleft-fixed"
            alt="types of bone fractures in a car accident"
          />
        </LazyLoad>
        <p>
          Since car accidents are extremely common, everyone is at risk of
          suffering these types of injuries. There are some people, however, who
          are particularly in danger of suffering a bone fracture in a crash.{" "}
        </p>
        <p>
          As we age, our bones become more brittle.{" "}
          <Link
            to="https://www.cdc.gov/motorvehiclesafety/older_adult_drivers/index.html"
            target="_blank"
          >
            Older adult drivers
          </Link>{" "}
          can more easily sustain a bone fracture in a collision. Young children
          could sustain fractures as well because their bones are not fully
          developed. Children, however, typically heal much faster. Therefore,
          the process of healing and getting back to normal is much easier for
          them.
        </p>
        <h2>4 Phases of Bone Fracture Treatment</h2>
        <p>
          Bone fractures require emergency medical treatment at a hospital. If
          you have suffered a bone fracture in a car accident, you may not even
          want to move until help arrives. If you have hurt your head, back,
          neck or hip, do not change your position.
        </p>
        <p>
          <strong>
            The following are possible stages that will take place after you
            experience a bone fracture
          </strong>
          :
        </p>
        <ol>
          <li>
            <strong>
              Call for emergency medical assistance and stay still
            </strong>
            :You can cause additional damage by moving a broken bone or
            fractured body part improperly. Before transporting you to the
            hospital, emergency workers may need to protect the injured area
            with a splint and gauze. This will limit the your ability to move
            the broken part. They may need to apply pressure to the injury as
            well to slow the bleeding.
          </li>
          <li>
            <strong>
              Once at the hospital, your bone break will need to be properly set
              and held there
            </strong>
            : Repositioning the bone without surgery is called
            <em>
              {" "}
              <Link
                to="https://medlineplus.gov/ency/patientinstructions/000521.htm"
                target="_blank"
              >
                {" "}
                closed reduction of a broken bone
              </Link>
            </em>
            . Most bone fractures suffered by children only require a closed
            reduction procedure. Serious fractures, however, require "open
            reduction." This is when the bone is repositioned with surgery. You
            may need pins, screws, rods or plates to hold the fracture in place
            as it heals.The cost of treating a bone fracture does not end with
            surgery.
          </li>
          <li>
            <strong>
              Medication is typically needed as well to reduce pain and swelling
              or to prevent infection
            </strong>
            : As soon as you are able, you will need to begin rehabilitation.
            Even if you are still in a cast, you should begin your
            rehabilitative therapy to promote blood flow and to maintain your
            muscle tone. Exercising with the help of a skilled physical
            therapist is not cheap, but it can help increase your chances of a
            full recovery and prevent blood clots and stiffness.
          </li>
          <li>
            <strong>
              Even with proper treatment, you will likely have stiffness and
              weakness long after the splint or cast is removed
            </strong>
            : It may take four to six weeks for the bone to regain strength and
            the types of activities you can do may be limited as you heal. For
            many, returning to work is simply not possible soon after the crash.
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Injuries from a motor vehicle accident"
            style={{
              backgroundImage:
                "url('/images/car-accidents/text-header-image.jpg')"
            }}
          >
            <h2>Earn the Compensation You Deserve</h2>
          </div>
        </LazyLoad>
        <p>
          The cost of surgery, rehabilitation services, hospitalization and
          medication can result in a huge financial burden for injured victims
          and their families. Furthermore, if you have to miss work while
          healing, you may wonder how you are going to afford your medical
          bills.
        </p>
        <p>
          If you have sustained an injury because of the negligence or
          wrongdoing of another, you may be able to seek financial compensation
          by filing a personal injury claim. A successful injury lawsuit can
          result in support of all of your related financial losses as well as
          non-economic losses such as pain and suffering and emotional distress.
        </p>
        <p>
          <strong>
            The experienced{" "}
            <Link to="/">California personal injury attorneys</Link> at Bisnar
            Chase have helped injured victims of car accidents obtain fair and
            full compensation for their losses
          </strong>
          . We can also help you avoid the costly mistakes many plaintiffs and
          claimants make during this complex process. When you are in the
          process of recovering physically and emotionally from a traumatic
          accident, managing legal and procedural issues with insurance
          companies can become an added burden.
        </p>
        <p>
          <strong>
            Please contact us at 949-203-3814 for a free consultation and
            comprehensive case evaluation
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  #B {
    float: right;
    padding-left: 15px;
  }

  .scroll-to-top {
    cursor: pointer;
    margin: 10px 0 20px;
    border: 1px solid #888;
    width: 115px;
    padding: 10px;
    border-radius: 5px;
    background-color: #eaf5ff;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
