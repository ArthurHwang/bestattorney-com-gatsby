// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/california-insurance-bad-faith-.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Insurance Bad Faith Attorneys - Insurance Claim Denial Lawyers"
        pageDescription="Call 949-203-3814 for highest-rated insurance bad faith lawyers, serving Los Angeles, Newport Beach, Orange County & San Francisco, California. We specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Bad Faith Insurance Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California bad faith lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Most people purchase insurance as protection against loss or an injury
          and believe that they are in good hands after a catastrophic event.
          This is not always true. The{" "}
          <strong>California Bad Faith Insurance Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          recognize insurance companies do not have your best interest. As many
          consumers can tell you, insurance companies often look for ways to
          deny claims, even if their excuses for denying claims are
          unsubstantiated.
        </p>
        <p>
          The injury attorneys at Bisnar Chase are dedicated to making sure that
          your rights are protected and that insurance companies live up to the
          "faith" you have placed in them. Our clients have been compensated
          with amounts up to a million dollars and have gained recognition for
          superior legal representation. The law firm of Bisnar Chase has been
          the recipient of numerous awards which include "Attorney of the Year",
          "Community Hero" and "Best Places to Work."
        </p>
        <p>
          Call <strong>877-705-6556</strong> to receive a{" "}
          <strong>free consultation </strong>from a five-star legal
          representative.
        </p>
        <p>
          You deserve to have an insurance company that will take care of you
          and if they fail to do so they should be held accountable. Call now.
        </p>
        <h6>
          {" "}
          <Link to="/about-us/testimonials" target="_blank">
            <strong> Client reviews</strong>
          </Link>
        </h6>

        <div className="row">
          <div className="col">
            <center>Client Review</center>

            <span className="quote">
              <em>
                "Bisnar Chase provided outstanding service in negotiating with
                the other person's insurance company. They worked hard to get me
                the best settlement and the turn around time was fast. On a
                scale of 1 to 10 I'd have to give the staff and attorneys of
                Bisnar Chase a 10."
              </em>
            </span>
            <em> - J. McMillan, former client </em>
          </div>
          <div className="col">
            <center>Client Review</center>
            <span className="quote">
              <em>
                "I knew I made the right choice with Bisnar Chase because they
                went above and beyond to help. I especially want to thank
                Colleen and Chris for meeting all my needs and walking me
                through every piece of information. You have excellent employees
                in this firm! I was treated like family and the service and
                professionalism was above and beyond."
              </em>
            </span>
            <em> - M. Cheng, former client </em>
          </div>
        </div>
        <h2>What is a "Bad Faith" Insurance Lawsuit?</h2>
        <p>
          The attempt to avoid paying justified claims is what has come to be
          known as "
          <Link
            to="https://en.wikipedia.org/wiki/Insurance_bad_faith"
            target="_blank"
          >
            bad faith
          </Link>
          ". You are placing your "faith" in your insurance company when you buy
          a policy from them; "bad faith" is the insurance company's breach of
          your trust, its betrayal in denying your claim for an insincere,
          fraudulent or false reason.
        </p>
        <p>
          "Bad faith" can also be the result of your insurance company's refusal
          to pay the full benefits for which you have contracted. For example,
          you are injured and your medical bills are $25,000.00 and your
          insurance company agrees to pay only $15,000.00.
        </p>
        <p>
          When an insurer denies your claim, there may be a good reason, but
          there may also be no reason at all. Many consumers feel that they must
          automatically agree to a denial of benefits. This is not the case.
          Question any denial; challenge it. An insurance company approaches a
          claim as something to avoid if at all legally possible. Insurance
          companies do not make money paying out claims, but from collecting
          premiums from policyholders. Bear that in mind when you submit your
          claim. Your own diligence and complete documentation can be your best
          weapon in dealing with an insurance company over a disputed claim in
          California.
        </p>
        <h2>3 Steps to Filing a Claim Against an Insurance Company</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/insurance-company-bad-faith-resized.jpg"
            width="338"
            className="imgleft-fluid"
            alt="Bad faith insurance lawyers in California"
          />
        </LazyLoad>
        <p>
          Insurance companies can be very strategic when it comes to dodging a
          lawsuit. Many may make you feel and state that you were the one at
          fault in the incident. The insurance company would state that you were
          responsible for the accident so they do not have to pay for your
          expenses and losses. If your insurance company denies your claim for
          any reason you have the right to file a lawsuit.
        </p>
        <p>
          It can be an intense and confusing process to go up against an
          insurance company. There are actions that you can take to ease the
          process though.
        </p>
        <p>
          <strong>
            3 steps to take when filing a lawsuit against an insurance company
          </strong>
          :
        </p>

        <ol>
          <li>
            <strong>Look back at your insurance policies</strong>: Although you
            can file a "bad faith" claim for any reason, a prime aspect of your
            case would be identifying if your insurance company violated your
            contract in the first place. Review a complete copy of your
            insurance contract. This information can also aid your California
            Bad Faith Insurance Lawyer with your case.
          </li>
          <li>
            <strong>Don't take "no" for an answer the first time</strong>: If
            your claim is denied the first time you can take matters into your
            own hands by giving the claim to the{" "}
            <Link
              to="https://www.insurance.ca.gov/0500-about-us/02-department/index.cfm"
              target="_blank"
            >
              California Department of Insurance
            </Link>{" "}
            to reexamine your claim. This governmental entity serves to protect
            consumers from their insurance company. If they agree that there is
            a violation in the contract then you can make a final demand letter
            in which the insurance company has 15-60 days to respond.
          </li>
          <li>
            <strong>Hire a California bad faith insurance attorney</strong>: The
            basic duty of your insurance company is to cover your losses for
            your physical and emotional suffering after an accident. If they
            fail to do so then you should be compensated for their negligence.
            The injury lawyers of Bisnar Chase have provided legal
            representation for countless victims of bad faith insurance
            companies for <strong>40 years</strong>. You don't have to go up
            against these big insurance companies alone. Call{" "}
            <strong>877-705-6556</strong> for a{" "}
            <strong>free case analysis</strong>.
          </li>
        </ol>
        <h2>Common Types of Insurance Lawsuits</h2>
        <p>
          The insurance industry makes up to one trillion dollars per year and
          many different kinds of insurance are made available to consumers.
          There are certain categories of insurance that are brought to the
          courts more often than others though. No matter what category your
          insurance company falls into you still have the right to file a
          lawsuit. Below are the common types of insurance companies that are
          brought to the courts.
        </p>
        <p>
          <strong>Auto accident</strong>: After suffering from a devastating
          industry many consumers have faith in their auto insurance company to
          take care of the damages. In order to receive compensation for the
          costs that you have obtained in a{" "}
          <Link to="/car-accidents" target="_blank">
            car accident
          </Link>
          , there needs to be a thorough investigation. If your car insurance
          company does not conduct a comprehensive analysis of the car wreck
          this can be seen as negligence by the auto insurance companies fault.
        </p>
        <p>
          <strong>Health and disability insurance</strong>: Many people who have
          either experienced extreme health problems or have suffered injuries
          at work can file a disability claim. The U.S. Census has reported that
          1 in 5 people are living with a disability. What does it take for a
          person to qualify as disabled? The{" "}
          <Link
            to="https://www.ssa.gov/planners/disability/qualify.html"
            target="_blank"
          >
            {" "}
            Social Security Administration
          </Link>{" "}
          states that a "Your condition must significantly limit your ability to
          do basic work such as lifting, standing, walking, sitting, and
          remembering – for at least 12 months. If it does not, we will find
          that you are not disabled."If your insurance denies your claim and
          your disability falls under this definition you are encouraged to seek
          legal representation.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bad faith insurance attorneys in California"
            style={{
              backgroundImage: "url('/images/text-header-images/gavel.jpg')"
            }}
          >
            <h2>We Will Hold Insurance Companies Accountable</h2>
          </div>
        </LazyLoad>

        <p>
          The <strong>California Bad Faith Insurance Lawyers</strong> of Bisnar
          Chase understand that many people rely on insurance companies to be
          there for them in their time of need.
          <strong>Since 1978</strong>, our legal representation has won clients
          over <strong>$500 Million dollars in compensation</strong>.
        </p>
        <p>
          Our legal representation has held a <strong>96% success rate </strong>
          and we have tackled the most complicated of cases. The Bisnar Chase
          bad faith insurance attorneys of Southern California will earn you the
          compensation you deserve for the punitive damages you have faced from
          your incident.
        </p>
        <p>
          Contact the law offices of Bisnar Chase from{" "}
          <strong>Monday-Friday from 8 a.m. to 5 p.m</strong>. One of our
          top-notch legal representatives will take your call along with give
          you a<strong>free case evaluation</strong> on your "bad faith" claim.
        </p>
        <p>
          Call <strong>877-705-6556</strong> and let the trial lawyers of Bisnar
          Chase take on the insurance companies for you.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .row {
    display: flex; /* equal height of the children */
  }

  .col {
    flex: 1; /* additionally, equal width */

    padding: 1em;
    border: thin;
  }
  .quote {
    font-family: Baskerville, "Palatino Linotype", Palatino,
      "Century Schoolbook L", "Times New Roman", "serif";
  }
  div center {
    color: #2a699d;
    font-size: 20px;
    font-family: Constantia, "Lucida Bright", "DejaVu Serif", Georgia, "serif";
  }

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
