// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="T-Bone Car Accident Lawyer - Bisnar Chase"
        pageDescription="Contact the T-bone car accident attorneys of Bisnar Chase for a free consultation. You may be entitled to compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>T-Bone Car Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/attorney-chase.jpg"
          height="141"
          className="imgleft-fixed"
          alt="Brian Chase"
        />
        <div className="algolia-search-text">
          <p>
            If you've been injured in a T-bone car accident please contact our
            experienced{" "}
            <Link to="/car-accidents">California car accident lawyers</Link> for
            a free consultation. Call <strong>(800)-561-4887</strong> to speak
            confidentially to our passionate and trusted legal team. We've
            recovered over $500 Million for our clients.
          </p>

          <p>
            A T-bone car accident, also known as a side-impact collision or a
            broadside crash, occurs when the front of one vehicle crashes into
            the side of another. These types of accidents often occur at street
            intersections when one driver runs a red light or stop sign or fails
            to yield to an oncoming vehicle or a car that has the right of way
            at the intersection.
          </p>

          <p>
            T-bone collisions have the potential to result in severe or even
            catastrophic injuries both to drivers and passengers involved in the
            crash. According to the U.S. National Highway Traffic Safety
            Administration (
            <Link to="https://www.nhtsa.gov/" target="_blank">
              NHTSA
            </Link>
            ), T-bone car accidents account for a little over 25 percent of all
            fatal car accidents in the United States. If you or a loved one has
            been injured in a T-bone crash, it is important that you better
            understand your legal rights and options.
          </p>
        </div>

        <div className="snippet">
          <p>
            <span>
              {" "}
              <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
              Partner:
            </span>{" "}
            “I went to law school knowing I wanted to be a personal injury
            attorney. I wanted my life’s work to have a positive impact on other
            people’s lives.”
          </p>
        </div>

        <h2>Proving Liability in a T-bone Car Accident?</h2>
        <p>
          Proving liability or responsibility for a T-bone car accident is the
          basis of a successful personal injury claim. In order to prove fault
          and liability, plaintiffs must be able to prove that the other driver
          was negligent. Looking into what caused the collision is the first
          step in the direction. Here are some of the most common causes of
          side-impact collisions:
        </p>
        <ul type="disc">
          <li>
            <strong>Failing to yield the right of way.</strong> When a vehicle
            fails to yield the right of way at an intersection, a T-bone
            collision could occur. Failing to yield the right of way includes
            running a red light, not properly stopping at a stop sign and making
            an unsafe turn in front of an oncoming vehicle. A number of
            broadside crashes involve motorcyclists because motorists fail to
            spot and yield the right of way to oncoming motorcycles at street
            intersections.
          </li>
          <li>
            <strong>Turning across traffic lanes.</strong> When a motorist is in
            a turning lane and can legally make a turn across traffic, he or she
            must wait until it is safe to do so. Often, drivers fail to see
            oncoming vehicles until it is too late.
          </li>
          <li>
            <strong>Aggressive driving.</strong> Aggressive drivers also tend to
            be reckless. They use their vehicles to intimidate others on the
            roadway.
          </li>
          <li>
            <strong>Distracted driving.</strong> Talking on a cell phone, even
            if it's hands-free, can have devastating consequences as can texting
            while driving. Such behavior can take the driver's eyes,
            concentration and focus away from driving. Distracted driving often
            results in broadside crashes.
          </li>
          <li>
            <strong>Driving under the influence.</strong> Alcohol and/or drugs
            impair a driver's reflexes, judgment and sight.
          </li>
          <li>
            <strong>Dangerous intersections.</strong> Street intersections that
            have poor visibility often cause or contribute to broadside crashes.
          </li>
        </ul>
        <h3>Injuries in T-Bone Car Accidents</h3>
        <p>
          The most common types of injuries in broadside crashes or T-bone car
          accidents are chest injuries and{" "}
          <Link to="/car-accidents/lower-extremity-injuries">
            injuries to the lower extremities
          </Link>
          , head, abdomen and pelvis. The nature and extent of the injuries
          could depend on several factors. The position of the vehicle and where
          the vehicle's occupants are sitting can play a part in the severity of
          the injuries sustained in a broadside crash. If a victim was sitting
          on the side of the vehicle that was struck, they could be severely
          injured when the door panel hits them. If they are sitting on the side
          that is not struck, they may suffer head injuries or other blunt
          trauma injuries.
        </p>
        <p>
          The victim's age could also be a factor. Children, for example, are
          likely to suffer more severe injuries in broadside crashes compared to
          older adults. In fact, children could be severely injured even when
          the T-bone crash is low-speed or low-impact. Children between the ages
          of 5 and 9 represent about 19 percent of those injured in car
          accidents and those between 10 and 15 represent about 27 percent of
          car accident injury victims. Yet another factor is the use of
          seatbelts or child safety seats. Failure to buckle up or use
          appropriate safety seats for children could result in catastrophic
          injuries or even fatalities in a T-bone car accident.
        </p>
        <h3>Liability and Compensation</h3>
        <p>
          In a majority of the car accident cases, plaintiffs must prove
          negligence on the part of the other motorist in order to seek and
          obtain maximum compensation for their losses. Injured victims can seek
          compensation for damages such as medical expenses, lost wages,
          hospitalization, rehabilitation costs, permanent injuries such as
          scarring or disfigurement, pain and suffering and emotional distress.
        </p>
        <p>
          In cases where a T-bone car accident results in death, the family
          members of the deceased victims can file what is known as a{" "}
          <Link to="/wrongful-death"> wrongful death claim</Link>, which
          compensates them for damages including medical and funeral costs, lost
          future income, pain and suffering and loss of love and companionship.
        </p>
        <p>
          Obtaining just compensation for your losses can be a struggle,
          especially in cases where you must prove negligence or wrongdoing. The
          experienced California T-bone car accident lawyers at Bisnar Chase
          have more than three decades of experience representing the rights of
          seriously injured car accident victims and their families.
        </p>
        <p>
          We have the will, resources and reputation of fighting insurance
          companies on behalf of our injured clients. We have recovered millions
          in compensation for victims and their families from negligent drivers,
          manufacturers of defective autos and governmental agencies that have
          failed to maintain and design safe streets and intersections.
        </p>
        <p>
          <strong>
            Call us at (800) 561-4887 to schedule a free consultation and
            comprehensive case evaluation.
          </strong>
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
