// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/head-on-crash-banner-image.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Head-on Car Accident Lawyers of California - Bisnar Chase"
        pageDescription="Contact the head-on car accident attorneys of Bisnar Chase for a free consultation. You may be entitled to compensation for expenses such as medical bills and lost wages. Our head-on car accident lawyers have held a 96% success rate for over 40 years. Call 1(800)561-4887 for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Head-On Car Accident Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Head-On Car Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced <strong>Head-On Collision Attorneys</strong> at Bisnar
          Chase help victims of California car accidents seek and obtain maximum
          compensation for their losses. We have decades of experience and a
          successful track record when it comes to fighting and winning against
          insurance companies who like to play hardball and deny our seriously
          injured clients the compensation they deserve.
        </p>
        <p>
          We also offer a{" "}
          <strong>no-win no-fee guarantee to our clients</strong>, which means
          that you pay no fees or costs until we recover compensation for you.
        </p>
        <p>
          If you have suffered a loss or been injured in a head-on collision
          please call now for a <strong>free consultation</strong> at{" "}
          <strong>(800) 561-4887</strong> with one of our experienced{" "}
          <Link to="/car-accidents">California car accident lawyers.</Link>
        </p>

        <h2>If You Have Been Injured in a Head-On Collision</h2>
        <p>
          Head-on collisions are almost always caused by the negligence of
          another motorist, such as a wrong-way driver who is impaired, a
          motorist who was looking at his or her phone or a fatigued driver who
          falls asleep at the wheel. Regardless of the cause, injured victims or
          families of deceased victims in such cases would be well advised to
          understand their legal rights and options.
        </p>
        <p>
          <strong>
            There are a number of steps you can take to protect your rights if
            you have been injured in a head-on collision
          </strong>
          :
        </p>
        <ol>
          <li>
            Make sure you file a police report and obtain a copy of the report
            for your records.
          </li>
          <li>
            Gather as much evidence as possible from the scene including
            photographs of the vehicles, your injuries, contact information for
            eyewitnesses and pertinent information from other parties that were
            involved in the collision.
          </li>
          <li>
            If you are unable to do so, it would be a good idea to have a friend
            or a family member gather these important pieces of evidence for
            you. Seek and obtain medical attention, treatment and care right
            away.
          </li>
          <li>
            Follow the doctor's advice with regard to continuing treatment and
            care. Save all receipts and invoices relating to your accident and
            injuries.
          </li>
          <li>
            Contact experienced head-on car accident lawyers who will remain on
            your side, fight for your rights and help you secure maximum
            compensation for your injuries, damages and losses. To reach an
            experienced car accident lawyer please{" "}
            <strong>call us toll free at (800)-561-4887</strong>.
          </li>
        </ol>
        <p>
          <strong>Some of Our Car Collision Case Results</strong>:
        </p>
        <ul>
          <li>$8,500,000.00 - Motor vehicle accident - wrongful death</li>
          <li>$2,432,250.00 - Motor vehicle accident</li>
          <li>
            $2,360,000.00 - Wrongful death, commercial vehicle - motor vehicle
            accident
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="California Head-On Car Accident Attorneys"
            style={{
              backgroundImage:
                "url('/images/car-accidents/ambulence-text-banner-image.jpg')"
            }}
          >
            <h2>Collisions and Injuries in a Head-On Car Accident</h2>
          </div>
        </LazyLoad>
        <p>
          Head-on collisions can result in{" "}
          <Link to="/catastrophic-injury">catastrophic injuries</Link>.
          According to statistics from the{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            National Highway Traffic Safety Administration
          </Link>{" "}
          (NHTSA), even though head-on accidents represent only 2 percent of all
          traffic accidents, they account for more than 10 percent of traffic
          accident fatalities. Head-on crashes can result in serious injuries or
          fatalities even at slower speeds.
        </p>
        <p>
          <strong>Common Causes of Head-On Collisions</strong>
        </p>
        <ul type="disc">
          <li>
            <strong>Driving under the influence:</strong> This is one of the
            most common reasons why drivers veer or drift into opposing lanes of
            traffic and cause a head-on collision. In fact, driver impairment
            causes most head-on crashes. This is because intoxicated drivers
            have impaired reflexes and poor judgment when it comes to assessing
            distance and direction. A majority of head-on crashes that involve
            impaired drivers tend to be wrong-way crashes.
          </li>
          <li>
            <strong>Drowsy driving:</strong> Long-haul truck drivers, night
            shift employees and other sleep-deprived drivers can be fatigued
            while driving. A driver who hasn't had sufficient sleep or rest is
            more likely to fall asleep at the wheel and drift into opposing
            lanes of traffic. These types of crashes can have devastating
            consequences.
          </li>
          <li>
            <strong>Cell phone use:</strong> Talking on a cell phone, texting
            while driving or checking e-mail or social media are all examples of
            distracted driving that could lead to a head-on collision. It only
            takes a few seconds of the driver taking his or her eyes off the
            road to cause a devastating head-on collision. Other examples of
            driver distractions include, eating, drinking, reading, checking the
            GPS, fiddling with temperature controls or the radio, grooming,
            applying makeup, etc.
          </li>
          <li>
            <strong>Faulty traffic signals:</strong> When traffic signals at
            street intersections fail to work as they should, they could cause
            head-on collisions as well as broadside crashes.
          </li>
          <li>
            <strong>Senior drivers:</strong> Recently, older drivers who are
            confused or disoriented have caused catastrophic head-on collisions
            by traveling the wrong way on the highway or by exiting on entrance
            ramps.
          </li>
          <LazyLoad>
            <img
              src="/images/car-accidents/Senior-woman-driving-image.jpg"
              width="100%"
              alt="head-on auto accident lawyers in california"
            />
          </LazyLoad>

          <li>
            <strong>Excessive speed:</strong> When drivers operate at a speed
            that is above the posted limit or when they travel at a speed that
            is unsafe given the traffic, roadway, or weather conditions, there
            is a higher likelihood that they will lose control of their vehicles
            and veer into opposing lanes of traffic.
          </li>
          <li>
            <strong>Vehicle defect:</strong> Sometimes, a vehicle defect that
            triggers a mechanical malfunction could also cause a driver to lose
            control and crash head-on into an oncoming vehicle.
          </li>
        </ul>
        <h2>What is Your Head-On Vehicle Crash Claim Worth?</h2>
        <p>
          If you have suffered serious injuries in a head-on collision that
          causes you to seek emergency treatment, hospitalization and continuing
          rehabilitative care, your claim will be worth a lot more than if you
          had simply suffered a few scrapes and bruises.
        </p>
        <p>
          <strong>
            Victims of head-on car accidents can seek compensation for damages
            including
          </strong>
          :
        </p>
        <ul type="disc">
          <li>Emergency transportation and treatment costs</li>
          <li>Hospitalization costs</li>
          <li>Expenses relating to surgery</li>
          <li>Cost of rehabilitative treatment and therapy</li>
          <li>Lost wages</li>
          <li>Permanent injuries such as scarring or disfigurement</li>
          <li>Disabilities</li>
          <li>Lost future income or loss of livelihood</li>
          <li>Loss of life's enjoyment</li>
          <li>Loss of consortium</li>
          <li>Pain and suffering</li>
          <li>Emotional distress or mental anguish</li>
        </ul>
        <p>
          <strong>
            Please call 1(800)561-4887 for a free consultation with an
            experienced and compassionate lawyer today
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
