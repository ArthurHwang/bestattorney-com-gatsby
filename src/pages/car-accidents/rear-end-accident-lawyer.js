// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/auto-accident-banner-image.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rear End Accident Attorney - Bisnar Chase"
        pageDescription="Contact the rear end accident attorneys of Bisnar Chase for aggressive representation & a free consultation. You may be entitled to compensation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rear End Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Rear End Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The experienced California Rear-End Accident lawyers at Bisnar Chase
          have a long and successful track record of representing the rights of
          crash victims and their families. Injured victims of rear-end
          collisions can file a personal injury claim against the at-fault party{" "}
          <Link to="/car-accidents/injury-compensation">
            seeking compensation for damages
          </Link>{" "}
          including medical expenses, lost income, hospitalization,
          rehabilitation, permanent injuries, scarring or disfigurement, loss of
          livelihood, pain and suffering and emotional distress.
        </p>
        <p>
          Families that have lost loved ones in
          <strong> rear-end collisions</strong> can file a wrongful death claim
          against the at-fault party seeking compensation for damages such as
          medical and funeral costs, lost future income and loss of love and
          companionship.
        </p>
        <p>
          We offer a{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            <strong>no-fee guarantee</strong>
          </Link>{" "}
          to our clients, which means we don't charge you any fees or costs
          until we recover compensation for you. Our team has what it takes to
          fight insurance companies and advocate for the rights of our seriously
          injured clients.
        </p>
        <p>
          <strong>Call us at (800) 561-4887</strong> for a{" "}
          <strong>free consultation</strong> and comprehensive case evaluation.
        </p>
        <p>
          <strong>Some of Our Case Results</strong>
        </p>
        <ul>
          <li>$30,000,000.00 - Catastrophic Motorcycle Accident</li>
          <li>$9,800,000.00 - Motor Vehicle Accident</li>
          <li>$3,000,000.00 - Rear End Motorcycle Accident</li>
          <li>$2,360,000.00 - Wrongful Death Commercial Vehicle Accident</li>
          <li>$2,000,000.00 - Seatback Injury- Rear End</li>
        </ul>
        <h2>Rear End Accidents All Too Common</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/car-speeding-image.jpg"
            width="100%"
            alt="rear end vehicle collisions"
          />
        </LazyLoad>
        <p>
          Rear-end car accidents occur when the front of one vehicle strikes the
          back of another.
        </p>
        <p>
          This is one of the most common types of car accidents we encounter on
          our roadways. Whether it's a fender-bender or a high-speed rear-end
          collision, these types of crashes have the potential to cause
          significant injuries or even fatalities.
        </p>
        <p>
          The nature and extent of the injuries in rear-end collisions often
          depends on the speed at which a vehicle is struck, the point of impact
          and the types of vehicles that are involved in the collision. Anyone
          who has sustained injuries in a car accident caused by someone else's
          negligence or wrongdoing would be well advised to obtain more
          information regarding his or her legal rights and options from an
          experienced{" "}
          <Link to="/car-accidents">California car accident lawyer</Link>.
        </p>
        <h2>Rear-End Accident Statistics</h2>
        <p>
          According to the{" "}
          <Link to="https://www.ntsb.gov/Pages/default.aspx" target="_blank">
            National Transportation Safety Board
          </Link>{" "}
          (NTSB), over the last three years, the agency has investigated nine
          rear-end accidents involving passenger or commercial vehicles. The
          result was 28 fatalities and 90 injuries. In 2012 alone, rear-end
          crashes resulted in 1,705 fatalities and represented almost half of
          all two-vehicle collisions in the United States. During the same year,
          there were more than 1.7 million rear-end crashes on our nation's
          highways resulting in 500,000 injuries.
        </p>
        <p>
          <strong>Common Causes of Rear-End Car Accidents</strong>
        </p>
        <p>
          <strong>
            There are a number of reasons why rear-end collisions occur. Her are
            some of the most common causes
          </strong>
          :
        </p>
        <ul type="disc">
          <li>
            <strong>Inattention:</strong> Driver inattention is one of the
            leading causes of rear-end collisions. Examples of inattention may
            include daydreaming or taking one's eyes off the road for a moment.
          </li>
          <li>
            <strong>Distracted driving:</strong> When drivers take their hands
            off the wheel, eyes off the road or attention away from the task of
            driving, a rear-end collision is often the result. Examples of
            distracted driving include texting, talking on the cell phone,
            talking to a passenger, eating, drinking, grooming, applying makeup,
            etc.
          </li>
          <li>
            <strong>Drowsy driving:</strong> The National Highway Traffic Safety
            Administration (
            <Link to="https://www.nhtsa.gov/" target="_blank">
              NHTSA
            </Link>
            ) estimates that 100,000 police-reported crashes nationwide are the
            direct result of driver fatigue each year. These result in about
            1,550 fatalities, 71,000 injuries and $12.5 billion in monetary
            losses. Driving while drowsy or fatigued often results in rear-end
            collisions.
          </li>
          <li>
            <strong>Excessive speed:</strong> Another common example of when
            rear-end collisions occur is when drivers exceed the posted speed
            limit or drive at a speed that is unsafe given traffic, roadway or
            weather conditions. When vehicles drive at an excessive or unsafe
            speed, they may not be unable to slow down in time to avoid vehicles
            in front of them.
          </li>
          <li>
            <strong>Impaired driving:</strong> Drivers who operate under the
            influence of alcohol and/or drugs also commonly cause rear-end
            collisions.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Rear-end head-on auto crash attorneys in California"
            style={{
              backgroundImage:
                "url('/images/car-accidents/rear-end-text-header.jpg')"
            }}
          >
            <h2>The Element of Negligence</h2>
          </div>
        </LazyLoad>
        <p>
          Any type of reckless or careless behavior on the part of an at-fault
          driver amounts to negligence in a rear-end collision. Often, the
          driver of the striking vehicle or the vehicle that rear-ends is
          determined to be at fault for a rear-end collision. However, in some
          cases, where there is evidence to show otherwise, exceptions could be
          made.
        </p>
        <p>
          In order to prove negligence in a rear-end accident case, it is
          important to get as much evidence as possible to support your claim.
          Police reports, photographs from the scene and eyewitness statements
          are just some examples of evidence from the accident scene that can
          help bolster your case and put you in a position to receive maximum
          compensation for your losses.
        </p>
        <p>
          <strong>
            Call us at (800) 561-4887 to schedule a free consultation and
            comprehensive case evaluation.
          </strong>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
