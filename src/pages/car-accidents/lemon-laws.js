// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lemon Laws: Consumer Rights"
        pageDescription="Auto product liability and lemon law are two separate things. Learn the difference here and let Bisnar Chase help you!"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Lemon Laws</h1>
        <BreadCrumbs location={location} />
        <h2>Auto Product Liability Law Versus Lemon Law</h2>
        <p>
          The auto product liability attorneys at Bisnar Chase handle serious
          injury cases that stem from auto accidents that are caused by
          dangerous or defective products. We do not handle cases that pertain
          to defective vehicles that did not result in an injury accident. When
          a person is injured in a car accident caused by a{" "}
          <Link to="/auto-defects">dangerous or defective auto</Link>, he or she
          has the right to file a product liability claim against the automaker
          and/or the manufacturer of the defective product. If you have a
          vehicle in your possession that has a serious safety defect such as
          brake failure, we recommend that you contact your dealer and/or
          manufacturer right away to get the necessary repairs done.
        </p>
        <h3>Examples of Auto Product Defects</h3>
        <p>
          Here are some examples of auto defects that can result in catastrophic
          injuries or fatalities:
        </p>
        <ul>
          <li>Seatbelt defects</li>
          <li>Tire defects</li>
          <li>Defective auto glass</li>
          <li>Airbag defects</li>
          <li>Seatback defects</li>
          <li>Roof crush</li>
          <li>Child safety seat defects</li>
          <li>Poor design that causes rollovers</li>
          <li>Gas tank explosions</li>
          <li>Accelerator defects (sudden or unintended acceleration)</li>
        </ul>
        <h3>Injuries Caused by Auto Defects</h3>
        <p>
          Auto defects can result in serious or catastrophic injuries or even
          death. Defective auto products can cause injuries including:
        </p>
        <ul>
          <li>Traumatic brain injuries</li>
          <li>Head trauma</li>
          <li>
            {" "}
            <Link to="/head-injury/spinal-cord-injuries">
              Spinal cord damage
            </Link>{" "}
            (quadriplegia, paraplegia, paralysis)
          </li>
          <li>Broken bones</li>
          <li>Neck and back injuries</li>
          <li>Internal organ damage</li>
          <li>Amputations</li>
          <li>Burn injuries</li>
        </ul>
        <h3>California's Lemon Law</h3>
        <p>
          California's lemon law is designed to protect consumers who discover a
          serious flaw in a vehicle they have purchased or leased, which cannot
          be repaired. The lemon law applies to any problem, which
          "substantially impairs, the use, value or safety" of a car covered by
          a manufacturer's new vehicle warranty. The catch is that the problem
          must be discovered within 18 months or 18,000 miles of purchase or
          lease. Even if you purchased a used car, the law applies to the
          vehicle if the original warranty is still in effect.
        </p>
        <h3>Consumers' Rights</h3>
        <p>
          If the manufacturer cannot fix the problem, they must replace your car
          with a new one that is "substantially identical" or refund your money.
          It is your choice. The manufacturer can charge you for the mileage you
          have put on the flawed vehicle. Manufacturers are allowed a
          "reasonable" number of repair attempts before a car is branded as a
          lemon. Typically, the law states that the manufacturer has four
          attempts to repair the vehicle. However, if your vehicle has
          potentially dangerous defects such as faulty brakes, then, they get
          only two attempts. Also, if your vehicle has been in the shop for more
          than 30 days and is still not fixed, you are entitled to a replacement
          vehicle or a refund.
        </p>
        <p>
          In order to settle Lemon Law disputes, many manufacturers participate
          in the state-certified arbitration program. If you go to arbitration,
          you could either accept or reject the arbitrator's decision. If you
          reject it, you can still take your case to court. You could either
          represent yourself in these types of cases or you could choose to
          retain the services of an experienced California Lemon Law attorney
          who will help you prove your case and help you obtain the compensation
          you rightfully deserve. We do not handle Lemon Law cases.
        </p>
        <h3>How Bisnar Chase Can Help You</h3>
        <p>
          Our law firm helps victims who have been injured as a result of a
          defective vehicle. We examine the circumstances of the incident based
          on the police report, eyewitness accounts and physical evidence. If
          you have been{" "}
          <Link to="/car-accidents">injured in a car accident</Link>, it is
          important to preserve the vehicle because it is the most important
          piece of evidence in an auto accident case or an auto product
          liability case. We have access to nationally renowned experts who will
          examine the vehicle for evidence of defects, malfunctions and design
          flaws. We examine a vehicle for its crashworthiness or its ability to
          protect its occupants in the event of a crash.
        </p>
        <p>
          We help injured victims obtain compensation for damages including
          medical expenses, lost wages, hospitalization, rehabilitation, pain
          and suffering and emotional distress. We have represented injured
          clients against large automakers. We fight for the rights of those
          consumers who bought a poorly manufactured or poorly designed vehicle
          and paid a horrible price for it. We are passionate in our pursuit for
          justice when it comes to these injured victims and their families. We
          find automakers' practice of putting profits before people's lives
          simply unacceptable. If you or a loved one has been seriously injured
          as the result of a defective auto or faulty auto part, please contact
          us at 1-800-561-4887 for a free consultation and comprehensive case
          evaluation.
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
