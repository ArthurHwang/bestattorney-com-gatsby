// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Roadside Debris Accidents - Possible Legal Action - California Attorneys"
        pageDescription="California residents have grown accustomed to avoiding road debris. Whether you are driving on the 5 Freeway, Pacific Coast Highway, or a common side street, another persons mess is a common obstacle that must be avoided to get from point A to point B. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Roadside Debris Auto Collisions</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/101029_highway_trash.jpg"
          alt="Road Debris Accident Lawyers"
          className="imgleft-fluid"
        />
        <p>
          California residents have grown accustomed to avoiding road debris.
          Whether you are driving on the 5 Freeway, Pacific Coast Highway, or a
          common side street, another person's mess is a common obstacle that
          must be avoided to get from point A to point B. Most of us have
          accepted that{" "}
          <Link to="/car-accidents/hazardous-roads">debris hazards</Link> are a
          part of transportation on the road, but for some unfortunate
          motorists, road debris has left them with life altering injuries, or
          worse.
        </p>
        <p>
          Road debris can be caused by a number of different factors. Weather,
          natural disasters, or objects falling from vehicles can leave roadways
          in dangerous conditions. Road debris has been known to cause traffic
          accidents by forcing motorists to initiate dangerous evasive
          maneuvers. Flat tires, fishtailing, and collisions with other vehicles
          are common results of large items in areas of heavy traffic.
        </p>
        <h2>Road Debris Statistics</h2>
        <p>
          Although the majority of us have had to avoid some form of road debris
          at some point in our lives, the number of people who suffer
          catastrophic injuries and death as a result of foreign objects on the
          road may come as a shock. The AAA Foundation for Traffic Safety
          released a study in 2004 revealing 100 deaths and 25,000 motor vehicle
          accidents resulting from road debris on a yearly basis. That's nearly
          70 injuries a day and 3 per hour.
        </p>
        <p>
          Road debris can cause injuries and deaths in a number of ways.{" "}
          <Link to="/auto-defects">Auto product recalls</Link> have been issued
          to help reduce injuries and innovative technologies may soon provide
          more protection from flying and stationary debris than we have grown
          accustomed to. The following are auto product recalls related to road
          debris.
        </p>
        <ul>
          <li>
            Rocks have been known to collide with a vehicle's catalytic
            converter and result in clogs and breaks. Vehicles, such as the 2005
            Scion TC, have been recalled as a result of potential shattering
            upon impact from flying debris.
          </li>
          <li>
            In February of 2010, the 2004 Mitsubishi Endeavor was recalled
            following reports insinuating possible corrosion resulting from a
            mixture of road debris and road salt. Experts argued whether this
            mixture could become trapped between a bracket and fuel filler pipe
            resulting in a dangerous condition.
          </li>
          <li>
            Due to a possible problem with pressure relief valves coming into
            contact with road debris, the 2001 Chevrolet C/K chassis cab truck
            was recalled to prevent possible injuries.
          </li>
        </ul>
        <h2>Victims of Road Debris Auto Collisions</h2>
        <LazyLoad>
          <img
            src="/images/royce-munns.jpg"
            alt="Royce Munns Motorycle Collision"
            className="imgleft-fixed mb"
          />
        </LazyLoad>
        <p>
          California is not the only state struggling to keep their streets
          clean. According to deseretnews.com, Royce Munns, a 46-year-old male
          from Brigham City, died after colliding with a mattress that was left
          in the middle of the I-15. Authorities believed that the mattress was
          being transported by another vehicle, but was not properly fastened.
          The Utah Highway Patrol (UHP) receives up to 20 calls a day regarding
          hazardous debris on the highways and issue fines of up to $500 for
          violators.
        </p>
        <p>
          Fatalities that could have been easily avoided are especially
          difficult to bear. It's hard to believe that had someone spent 5
          minutes securing a mattress onto their vehicle, Royce Munns may be
          with us today. My thoughts and prayers go out to Royce's family whom
          I'm sure are still struggling to accept their loss.
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
