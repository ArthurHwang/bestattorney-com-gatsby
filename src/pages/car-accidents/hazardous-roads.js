// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { regex: "/dangerous-road-banner.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Dangerous Roads Injury Lawyer"
        pageDescription="Call 800-561-4887 for highest-rated California dangerous roads injury lawyers. If you're injured from a dangerous road condition, we can help. The law firm of Bisnar Chase has over 40 years of experience helping accident victims of hazardous roadways. Contact our law offices for a free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Dangerous Roads Injury Attorney</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California dangerous roads injury attorney"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            The <strong>California Dangerous Roads Injury Lawyers</strong> of
            Bisnar Chase specialize in Cal-trans accidents, dangerous roads and
            negligence issues by city and government entities.
          </p>
          <p>
            Hazardous road conditions lead to tens of thousands of accidents
            every year. These conditions include unsafe guard rails, slippery
            roads due to bad weather, inadequate or inappropriate road design,
            bad lighting, debris in lanes of traffic, and badly marked
            constructions zones.
          </p>
          <p>
            Whether you're driving a car, truck or motorcycle you can be
            affected by dangerous and hazardous road conditions. The last thing
            you expect is to be in a serious{" "}
            <Link to="/car-accidents">car accident</Link> over faulty road
            design. Many of these dangerous conditions should not be there to
            begin with and you deserve to be compensated for your injuries.
          </p>
          <p>
            If you or someone you know has physically, emotionally or mentally
            suffered from injuries in a hazardous road accident contact the law
            firm of Bisnar Chase.
          </p>
          <p>
            <strong>
              Upon your call you will receive a free consultation with a
              top-notch legal representative
            </strong>
            .
          </p>
          <p>
            <strong>Call 1-800-561-4887</strong>
          </p>
        </div>
        <h2>Actions to Take After a Road Accident</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/dangerous-road-photo.jpg"
            width="100%"
            alt="Unsafe road injury lawyer"
          />
        </LazyLoad>
        <p>
          You immediately need to contact a dangerous road lawyer if you have
          been involved in a incident and the cause of the accident was poorly
          constructed road. A hazardous road lawyer can make a thorough
          investigation of the highway in which you were received your injuries.
          The process that your attorney may take is first hiring an engineer to
          examine the area in which you were hurt.
        </p>
        <p>
          If they are to blame, a governmental entity should be held accountable
          for not properly designing a safe road for drivers. The dangerous road
          lawyers of Bisnar Chase have been taking on the toughest cases
          involving dangerous road conditions for 40 years.{" "}
          <strong>
            Call 1-800-561-4887 to speak with a high-quality legal
            representative
          </strong>
          .
        </p>
        <h2>Making a Claim Under the Federal Tort Claims Act</h2>
        <p>
          Under the{" "}
          <Link
            to="https://www.house.gov/content/vendors/leases/tort.php"
            target="_blank"
          >
            Federal Tort Claims Act
          </Link>{" "}
          you can make a claim for a negligent act. Under the FTCA, the federal
          government acts as a self-insurer, and recognizes liability for the
          negligent or wrongful acts or omissions of its employees acting within
          the scope of their official duties. An experienced dangerous roads
          lawyer will understand the difficulties dealing with the federal
          government.
        </p>
        <p>
          <strong>Typical Hazardous Road Conditions</strong>
        </p>
        <ul>
          <li>Loose rocks and gravel</li>
          <li>Road debris</li>
          <li>Potholes &amp; road resurfacing</li>
          <li>Overgrown trees and bushes blocking view</li>
          <li>Signs and signals in disrepair</li>
          <li>Bad lighting</li>
          <li>Unsafe construction zones</li>
        </ul>
        <p>
          These dangerous road conditions lead to accidents all the time, some
          very serious like{" "}
          <Link to="/head-injury/spinal-cord-injuries">
            spinal cord injuries
          </Link>
          , permanent back damage, head injuries and{" "}
          <Link to="/head-injury/brain-trauma">lifelong brain damage</Link>.
          Regardless of the nature of your hazardous road accident, you may want
          to consult an attorney to determine whether you can recoup damages for
          your personal injury.
        </p>
        <p>
          In many cases, hazardous road conditions coexist with other liability
          factors. For instance, a negligent or speeding driver may have dodged
          debris and driven your car off the road. In such cases, you may be
          able to collect damages from either (or both) the negligent driver and
          the entity responsible for maintaining the road.
        </p>
        <p>
          <strong>Recent Dangerous Roads Case Victories</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/case-results/christopher-chan">
              $16,444,904.00 - Dangerous road condition, driver negligence
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/case-results/olivier-maciel">
              $10,030,000.00 - Premises negligence
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/case-results/rickey-prock">
              $3,000,000.00 - Dangerous road design - Trolly vs. pedestrian
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/case-results">
              $2,800,000.00 - Negligent Road Design
            </Link>
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dangerous road attorney in California"
            style={{
              backgroundImage:
                "url('/images/car-accidents/pot-hole-text-header.jpg')"
            }}
          >
            <h2>Who is Liable for a Hazardous Road Crash?</h2>
          </div>
        </LazyLoad>

        <p>
          Many may think that the entities that are responsible for hazardous
          road accident a government entity on a city, county or state level.
          There are other sources that can be held accountable for car crashes
          as well. Construction or landscaping companies can pose a threat to
          drivers. These organizations hold the duty of taking preventative
          measures to ensure that those who travel around or through these sites
          are kept safe.
        </p>
        <p>
          Some of these safety measures can be speed limits, proper lighting and
          traffic cones. There are situations were a driver can be held
          accountable for an accident on a dangerous roadway.  An example of a
          driver being responsible for an accident on a road is if they are
          speeding and carrying heavy material. If a driver is going above the
          speed limit and does not have a home appliance such as an oven
          strapped down correctly, this can pose a danger to the person driving
          from behind.
        </p>
        <h2>3 Common Hazardous Conditions</h2>
        <p>
          Whether its harsh weather conditions or a lack of traffic signals,
          roads can be very dangerous if there are not enough safety measures
          taken. Hazardous roads are the cause of many traffic injuries and
          deaths each year in the United States. Inadequately designed roads can
          also potentially affect pedestrians and cyclists. The following lists
          the top five common unsafe road conditions.
        </p>
        <p>
          <strong>Bad weather conditions</strong>
        </p>
        <p>
          Over a million accidents occur because of harsh weather conditions. Of
          that amount, about 5,900 deaths took place as well. What is also
          surprising is that most accidents occur not during the winter time,
          but in the summertime. During the summer there is more congestion on
          the roads due to students being out of school, people vacationing and
          construction.
        </p>
        <p>
          <strong>Poorly designed roads</strong>
        </p>
        <p>
          Roads that are not properly constructed can lead to numerous
          accidents.  Poorly constructed roads can also leave wear and tears on
          your car. One of the causes that can lead to a vehicle crashing or can
          lead to serious damage to your automobile, is potholes. Potholes are
          formed when water leaks in the soil under the asphalt. Once the water
          has leaked into the soil (beneath the asphalt) it weakens the
          foundation. The asphalt then cannot withstand the weight of the cars
          and over time creates the hole. Los Angeles is the city with the most
          potholes in the nation.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/pot-holes-image.png"
            width="75%"
            className="imgcenter-fluid"
            alt="Road injury Lawyer in California"
          />
        </LazyLoad>
        <center>
          <blockquote>Image Courtesy of Summit County Engineers</blockquote>
        </center>

        <p>
          <strong>Construction </strong>
        </p>
        <p>
          Construction sites can pose a threat to drivers when there is not a
          sufficient amount of safety measures taken. For instance, if there are
          not any traffic cones to direct drivers towards a clear path then this
          can confuse drivers and can lead to head-on collisions. Another hazard
          that construction sites can have is poor lighting. If motorists cannot
          see the road then they can potentially crash into the construction
          site itself.
        </p>
        <h2>Expertise When You Need It Most</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/BisnarChasePhotosmall.jpg"
            width="322"
            className="imgright-fixed"
            alt="Dangerous road injury lawyers in California"
          />
        </LazyLoad>
        <p>
          Proving dangerous road conditions exist can be a complex case. Our
          trial attorneys have over{" "}
          <strong>40 years in hazardous and dangerous road conditions</strong>{" "}
          and we've collected and settled close to{" "}
          <strong>$500 Million for our clients</strong>.
        </p>
        <p>
          A good trial attorney with vast knowledge of dangerous road condition
          cases can be the difference between winning and losing your case. Just
          because a police report says you were at fault doesn't mean its true.
          You'll need a seasoned hazardous road attorney by your side.
        </p>
        <p>
          When a a dangerous road accident occurs reach out to the{" "}
          <strong>
            California Dangerous Road Injury Lawyers of Bisnar Chase at
            1-800-561-4887
          </strong>{" "}
          and speak to professional for legal advice.
        </p>
        <p>
          <strong>
            Consultations are always free and if there is no win there is no fee
          </strong>
          .
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
