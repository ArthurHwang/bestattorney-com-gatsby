// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { regex: "/rideshare-banner-image.jpg/" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Rideshare Accident Lawyer - California"
        pageDescription="please call our experienced California rideshare accident lawyers for a free consultation if you've been injured and need compensation. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rideshare Accident Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Rideshare Accident Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <div className="algolia-search-text">
          <p>
            The experienced <strong>California auto accident lawyers</strong> at
            Bisnar Chase represent victims of all types of auto accidents
            including car accidents involving rideshare vehicles like{" "}
            <Link to="/car-accidents/lyft-car-accident-lawyers">Lyft</Link>{" "}
            &amp;{" "}
            <Link to="/car-accidents/uber-car-accident-lawyers"> Uber</Link>.
            Issues involving fault and liability can prove challenging in
            rideshare accidents. Our accident lawyers will thoroughly
            investigate the case, identify all possible sources of compensation
            and strive to secure the best possible settlement or jury award for
            you. We will work diligently to help ensure that you are fairly and
            fully compensated for your injuries, damages and losses.{" "}
            <strong>Call us at 1-800-561-4887</strong> for a free, comprehensive
            and confidential consultation.
          </p>
          <h2>How Do Rideshare Companies Work?</h2>
          <p>
            The business of rideshare has been booming in recent years with
            companies such as Uber, Lyft , Sidecar and Juno taking over the
            local transportation industry, making billions of dollars with their
            app-based business model, creating a nightmarish situation for taxi
            companies and other for-hire vehicle operators. As more and more
            people are warming up to the idea of a "sharing economy" and turning
            to their smart phones for everything from making online purchases to
            ordering food, rideshare companies are just a push away on the touch
            screen. They are reasonably priced, too. But that doesn't mean it's
            been smooth sailing for these businesses.
          </p>
          <h2>Legal Problems Facing Rideshare Companies</h2>
          <LazyLoad>
            <img
              src="/images/car-accidents/ride-share-driver-image.jpg"
              width="100%"
              alt="Rideshare crash attorneys in California"
            />
          </LazyLoad>
          <p>
            In the few years they have been in existence, rideshare companies
            have been faced with a number of legal issues. Here are some of
            those issues:
          </p>
        </div>
        <ul type="disc">
          <li>
            <strong>ADA compliance:</strong> Disabled people have filed federal
            complaints against companies such as Uber and Lyft because these
            vehicles are not wheelchair friendly. Under the Americans with
            Disabilities Act, it is illegal for private taxi services to
            discriminate against the disabled. They are required to have
            vehicles that can transport wheelchairs. Since rideshare drivers are
            mostly app users who drive people around for extra cash, they don't
            follow ADA requirements. There are lawsuits that have been filed
            around the country with the hope of changing this situation.
          </li>
          <li>
            <strong>Local regulations:</strong> It is a fact that rideshare
            drivers don't have to comply with the same regulations as taxicab
            drivers. City councils and local governments are starting to pay
            attention to this fact. While taxi drivers need to pay licensing
            fees, obey commercial insurance laws, charge uniform rates and
            follow a number of other rules, Uber and other rideshare companies
            have had pretty much a free hand when it comes to doing business.
            Also, in most cities, taxi drivers are required to have a commercial
            driver's license and undergo regular vehicle inspections while
            rideshare drivers only have to meet an age requirement, have a
            regular driver's license and a fully functioning car.
          </li>
          <li>
            <strong>Criminal acts:</strong> There have been a number of media
            reports of robberies, sexual assaults or physical assaults by
            rideshare drivers against passengers. These incidents have landed
            rideshare companies in hot water, posing questions and concerns
            about how these companies hire or recruit their drivers and if they
            perform thorough background checks.
          </li>
        </ul>
        <h2>Who is Liable in a Crash?</h2>
        <p>
          Uber and Lyft state on their websites that they offer $1 million
          liability insurance plans for their drivers, but 14 states have issued
          warnings to passengers who use rideshare services that these companies
          may not cover them in the event of an accident. The issue of insurance
          coverage remains ambiguous when it comes to rideshare companies. There
          are several questions here.
        </p>
        <p>
          If a rideshare driver gets into an accident, who is liable? The driver
          alone, or both the driver and the company? In a traditional
          employer-employee relationship, if an employee is in the course and
          scope of his or her employment, and his or her negligence results in
          injury or property damage, the employer is liable for the employee's
          actions. In the case of rideshare companies, they maintain that their
          drivers are not their employees. Rideshare drivers are viewed as
          "independent contractors."
        </p>
        <p>
          So, rideshare companies maintain that as independent contractors, the
          drivers are solely responsible for the injuries, damages and losses
          they cause while driving or transporting passengers. The companies
          claim that they don't have influence over the drivers or the cars they
          operate. Rather, they say, they merely control the app that
          facilitates the connection between passengers and drivers. Therefore,
          rideshare companies maintain that they are not liable for the driver's
          negligence.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Rideshare crash attorneys in California"
            style={{
              backgroundImage:
                "url('/images/car-accidents/woman-injured-car-crash-text-header.jpg')"
            }}
          >
            <h2>If You Have Been Injured</h2>
          </div>
        </LazyLoad>
        <p>
          The laws and regulations relating to rideshare companies are still
          evolving. There are several pending lawsuits not just in California,
          but also around the country that could very well change the laws that
          govern rideshare companies. Even though rideshare companies might bill
          themselves as technology companies as opposed to transportation
          companies, they could still be held liable for injuries, damages and
          losses their drivers cause, even if they are independent contractors.
          More and more local governments are also enforcing insurance
          requirements for rideshare drivers, which could help injured victims
          seek compensation for their losses.
        </p>
        <p>
          If you have been injured in a car accident caused by a rideshare
          company, it is important that you retain the services of an
          experienced California rideshare accident lawyer who is well versed
          with the changing laws in the area and has an excellent track record
          when it comes to representing auto accident victims. The experienced
          car accident attorneys at Bisnar Chase have helped seriously injured
          clients{" "}
          <strong>
            secure hundreds millions of dollars in settlements and jury awards
          </strong>
          . We have a{" "}
          <Link to="/about-us/no-fee-guarantee-lawyer">
            {" "}
            no-win, no-fee guarantee
          </Link>
          , which means that you don't pay anything unless we recover
          compensation for you. Contact us at 1-800-561-4887 to find out how we
          can help you.
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
