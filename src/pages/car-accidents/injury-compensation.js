// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Car Accident Injury Compensation -- What You Must Know"
        pageDescription="How to protect yourself in a car accident injury compensation claim. Find out what to do about medical bills, pain and suffering and legal representation after an accident or injury."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Car Accident Compensation: Personal Injury Protocol</h1>
        <BreadCrumbs location={location} />

        <p>
          Looking up car accident injury compensation is typically not a task
          you want to find yourself performing. It usually means that you or a
          loved one have been{" "}
          <Link to="/resources/injured-in-car-accident">
            injured in a car accident
          </Link>
          . If that's your current situation, then you need to know some
          important facts and details about compensation, what you should do
          next, and how to take action.
        </p>
        <p>
          First, as a general guideline, the car accident compensation which you
          receive will depend on three main or overarching elements: What You
          Lost: There are many forms of monetary compensation and damages,
          broken down into two key groups: economic and non-economic
          compensatory damages. Together, they include current and future
          medical bills, lost wages, emotional pain and suffering, lost earning
          capacity, and more.
        </p>
        <p>
          Another type of non-economic damages will be the loss of consortium,
          which is always a sensitive and tricky issue. How much you have and
          will lose across these various categories sets your baseline car
          accident injury compensation.
        </p>
        <p>
          Extent and Severity of Injuries: What actual types of injuries did you
          suffer, and what kind of impact have they had on you physically,
          emotionally, financially, and so forth? A broken finger and a broken
          leg could result in equal medical bills, but drastically different
          impacts elsewhere.
        </p>
        <p>
          Their Ability to Pay: If they don't have insurance or assets, or only
          have an insurance policy set to a minimum amount, then you won't be
          able to receive more compensation than that. Therefore, their ability
          to pay is a major determining factor in what you actually end up
          receiving and how much your claim will be worth.
        </p>
        <h2>What to Do To Receive Car Accident Injury Compensation</h2>
        <p>
          The absolute first thing you need to do is get in touch with an
          experienced and qualified{" "}
          <Link to="/car-accidents/">car accident attorney</Link> or legal team,
          so that they can take up your case and begin guiding you through the
          process. You should never speak with insurance claims inspectors or
          agents, as even simple or benign questions could come back to haunt
          you down the line in terms of the level of compensation you receive.
          Stay quiet, and seek out legal assistance first. If you're still on
          the scene of an accident, be sure to document as much about the
          process as possible.
        </p>
        <p>
          Call the police and have them create a report. Write down details
          about how the accident happened, and see if any witnesses are
          available. Of course, always be sure to get their insurance
          information, and never give them any of your personal contact
          information. There's no need or purpose to be talking to that
          individual directly.
        </p>
        <h2>Take Action to Protect Yourself Today</h2>
        <p>
          Learning about car accident injury compensation is a great way to
          begin protecting yourself when you're in this unenviable position.
          Make sure that you get what is entitled to you, and make sure you
          fight for your rights and the compensation you deserve as a victim of
          an accident.
        </p>
        <p>
          Over more than three decades, we have successfully represented
          thousands of clients suffering serious injuries from car accidents,
          and we'll offer you our complete dedication for your case.
        </p>
        <p>
          We'll ensure that you receive the full level of car accident injury
          compensation that your injuries and circumstances warrant. Of course,
          you also never need to pay us any fees until your case is successful
          as well. That means there's absolutely no reason not to pick up the
          phone and begin seeking your compensation.
        </p>
        <p>
          Call us today at <strong>949-203-3814</strong> to receive your{" "}
          <Link to="/contact">free legal consultation</Link>, and we'll show you
          how we can help you when it's time to go after the rightful
          compensation you deserve.
        </p>
        <h2>Helpful Resources</h2>
        <ul>
          <li>
            {" "}
            <Link to="/resources/top-causes-of-car-accidents">
              Top Causes of Car Accidents
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/california-motor-vehicle-code">
              California Rules of the Road
            </Link>
          </li>
          <li>
            <p>
              <strong>Other Service Locations for Car Accidents</strong>
            </p>
            <ul>
              <li>
                {" "}
                <Link to="/los-angeles/car-accidents">
                  Los Angeles Car Accident Lawyers
                </Link>
              </li>
              <li>
                {" "}
                <Link to="/orange-county/car-accidents">
                  Orange County Car Accident Lawyers
                </Link>
              </li>
            </ul>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
