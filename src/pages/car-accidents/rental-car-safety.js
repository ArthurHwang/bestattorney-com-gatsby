// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/rental-car-accident-banner.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="A Safety Guide to Dealing with Rental Car Accidents - Bisnar Chase"
        pageDescription="Have you experienced a car accident or issue while driving a rental car? The Rental Car Accident Lawyers of Bisnar Chase Personal Injury Attorneys have your back. We know what it takes to represent your case and win you maximum compensation. Call for your free consultation at 949-203-3814."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Your Guide to Rental Car Accidents & More</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="rental car accidents"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          <strong>Rental cars</strong> are an important decision when traveling.
          Unless you are somewhere like New York or Chicago, where public
          transportation is really all you need to get around town, having a
          vehicle to drive to and from places like your hotel, restaurants,
          family and friends you may be visiting and site seeing, is a must.
        </p>
        <p>
          Although it is nice to have transportation when on vacation, the
          entire process can be a scary experience. The actual time of renting,
          making choices about rental car insurance, how many miles you want to
          purchase, crossing T's and dotting I's on the rental car agreement
          paperwork can be overwhelming and confusing.
        </p>
        <p>
          Our skilled and experienced team of{" "}
          <Link to="/" target="new">
            Personal Injury Lawyers
          </Link>{" "}
          understand that rental cars can be a "finicky" area when it comes to
          rules, guidelines and all aspects with renting a car. But that's not
          the only factors that make renting a car intimidating and dangerous.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/intimidated-and-cautious-rental-car-driver.jpg"
            width="100%"
            alt="rental car accident attorneys"
          />
        </LazyLoad>
        <p>
          Once you get the rental car keys, driving the rental car off the lot
          can be intimidating and exhausting, making sure not to hit any poles
          or cars. When you're on the road and driving, the car is foreign;
          unfamiliar controls, a difference in the vehicles performance compared
          to your own daily driver back at home, even the view-of-site you have
          from the drivers seat. Maybe the windshield is more slanted, or more
          open, seat positioning could be lower set, the vehicles profile could
          be closer to the ground or higher in the air.
        </p>
        <p>
          All factors contribute to your driving performance and ability to
          maneuver out of any potentially dangerous obstructions, hazardous
          conditions or sudden and unexpected situations.
        </p>
        <p>
          Whether you are on vacation or renting a car while your daily driver
          is in the auto shop, experiencing a car accident in a rental vehicle
          can be a nightmare. But it doesn't have to be when you hire our
          skilled and experienced Personal Injury Lawyers, at our highly
          respected and successful firm,{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>
          , to represent you and your rental car accident case.
        </p>
        This page will cover the following:
        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/car-accidents/rental-car-safety#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Crashing a Rental Car{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Which Rental Car Insurance Coverage Did You Buy?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              4 Types of Rental Car Insurance{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Top 5 Most Common Rental Car Accidents{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are Rental Cars Dangerous?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Do Rental Car Companies Rent-Out Defected Cars?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Watch Out for This!{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/rental-car-safety#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Hire Bisnar Chase?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>
        <h2 id="header1">Crashing a Rental Car</h2>
        <p>
          Being involved in a car accident is never a good experience,
          especially when you are in a rental car. Depending on the type of
          insurance coverage you purchased when renting the car, could make a
          big difference with damages encountered during the crash.
        </p>
        <p>
          So, what do you do when you get into a wreck while driving a rental
          car? The same as you would in any other vehicle. Safety first, don't
          downplay anything, and stay aware of your surroundings and current
          situation at all times.
        </p>
        <p>
          Remember to always stay calm and don't panic. Although it seems like
          easy advice to give when not in a traumatic situation, there are no
          benefits from freaking out and panicking, ever, in any situation.
        </p>
        <p>
          Once the vehicles have come to a halt, make sure you are clear to exit
          the vehicle and assist any other injured car accident victims if you
          are able to do so. Don't move anyone that could possibly have a
          serious injury or is unconscious, unless they are in direct danger,
          such as a gas leak, fire or in danger of being struck by a moving
          vehicle.
        </p>
        <p>
          Call 911 for emergency medical services, even as a precautionary
          measure, not only to ensure the health and safety of you and others
          involved, but to document your injuries in affiliation to the
          accident.
        </p>
        <p>
          Your next step is going to make that call. Call your rental car
          company and explain to them the situation and what happened. You will
          then go through their process of a rental car accident procedure.
        </p>
        <div id="header2"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="rental car insurance coverage"
            style={{
              backgroundImage:
                "url('/images/text-header-images/rental-car-insurance-coverage.jpg')"
            }}
          >
            <h2>Which Rental Car Insurance Coverage Did You Buy?</h2>
          </div>
        </LazyLoad>
        <p>
          Depending on what kind of coverage you picked, could either help you,
          or you could be regretting not purchasing that extra or full coverage
          on your rental car. Either way, you are in the situation you are in,
          and there is no changing it. So continue forward with a positive
          attitude, and know that Bisnar Chase will take care of you.
        </p>
        <h2 id="header3">4 Types of Rental Car Insurance</h2>
        <p>
          Some people think that they don't need to buy any extra coverage or
          purchase any insurance when renting a car.
        </p>
        <p>
          Depending where you are renting a car, it would be smart to do some
          research before you sign the paperwork.
        </p>
        <p>
          Here are{" "}
          <strong>
            the 4 main types of insurance and insurance-like coverages sold at
            rental counters
          </strong>
          : (information provided by{" "}
          <Link
            to="https://www.consumeraffairs.com/travel/carrent_ins.html"
            target="new"
          >
            ConsumerAffairs.com/travel
          </Link>
          )
        </p>
        <ul>
          <li>
            <strong>Collision Damage Waiver (CDW)</strong>
          </li>
          <li>
            <strong>Supplemental Liability Protection (SLP)</strong>
          </li>
          <li>
            <strong>Personal Accident Insurance (PAI)</strong>
          </li>
          <li>
            <strong>Personal Effects Coverage (PEC)</strong>
          </li>
        </ul>
        <p>
          Some of these plans cover things you may never need, but then again,
          you'll never know if you are going to need them, since we can never
          know if you are going to experience an accident or not.
        </p>
        <div id="header4"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="rental car crashes"
            style={{
              backgroundImage:
                "url('/images/text-header-images/top-5-most-common-rental-car-accidents.jpg')"
            }}
          >
            <h2>Top 5 Most Common Rental Car Accidents</h2>
          </div>
        </LazyLoad>
        <p>
          Driving an unfamiliar car on unfamiliar roads can be very dangerous.
          Getting used to a vehicle takes time, patience and spending time
          getting to know how things work, feel and react
        </p>
        <p>
          When renting vehicles you are not used to, you don't always have the
          luxury of spending time with the vehicle to safely feel confident
          with. Usually, you get the rental car in a hurry, and end up driving
          away in a hurry.
        </p>
        <p>
          When you go somewhere new, you don't know what lurks around the curve.
          Is there a large dip? A narrow bridge maybe? A sudden turn before a
          large drop-off? When negligent and careless driving is combined with
          an unfamiliar vehicle, the chance of a car accident increase.
        </p>
        <p>
          Here is a list of the{" "}
          <strong>Top 5 Most Common Rental Car Accidents:</strong>
        </p>
        <ol>
          <li>
            <strong>Backing Up!:</strong> Some people have subconscious habits
            with their driving. People who are used to backup sensors become
            reliant on the beep warning when they get too close to a wall or
            car. Rental cars, being heavily used and abused, are usually
            lower-end models, lacking the bells and whistles that higher-end
            models have, such as backup sensors. When a person used to having
            backup sensors gets a rental car without back up sensors, backing
            into walls, posts, cars, people or other objects can become a
            surprisingly common issue.
          </li>
          <li>
            <strong>Running into Things:</strong> Because our brains take some
            time to acclimate to new cars, the way they turn and the way they
            drive, sometimes we can overturn or under turn, causing the vehicle
            to run into walls, cars and pedestrians.
          </li>
          <li>
            <strong>Reckless Driving:</strong> When on vacation, we tend to get
            excited about being on vacation and where we are. Even the
            excitement of renting a car that you've always wanted, like a sports
            car or large SUV, we can bite off a little more than we can chew.
            Burning out, speeding, driving offroad and other terrain, donuts,
            racing and even purposely damaging the vehicle itself can result in
            serious injuries, property damage and fatal events.
          </li>
          <li>
            <strong>Auto Defects: </strong>Because rental cars are used so much,
            abused and pushed past their limits, they can become dangerous, due
            to broken parts, loose bolts or parts, and even inefficient and
            irregular performance. If this goes unseen, rental car companies can
            be held accountable for negligent maintenance and car service.
            Rental cars should be at 100% safe and reliable performance,
            otherwise not rented out to the public. Many rental car companies
            will even brush{" "}
            <Link to="/auto-defects" target="new">
              auto defects
            </Link>{" "}
            and vehicle recalls under the rug, to avoid the headache of fixing
            it and save money.
          </li>
          <li>
            <strong>Distracted Driving:</strong> Being on vacation can be
            exciting and distracting at the same time, especially if you are
            driving. Tourists want to take pictures, photos, videos and even
            posting on social media like instagram, facebook and snapchat. Doing
            this while driving is not only difficult, not only dangerous, but
            illegal. Don't text, make phone calls or use hand-help cell phones
            while driving, that includes cameras and any other electronic device
            that can be held.
          </li>
        </ol>
        <p>
          Why is a <strong>Personal Injury Law Firm </strong>who only represents
          the plaintiff point out
          <strong> the Top 5 Most Common Rental Car Accidents</strong>, most of
          which applies to the wrong-doers?
        </p>
        <p>
          This is what is dangerous about tourists and rental car drivers on the
          road. Even though you are a safe driver, or take the correct and safe
          steps to avoiding possible car wrecks doesn't mean someone else can't
          or won't run into you.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/dangerous-rental-car-defects.jpg"
            width="100%"
            alt="dangerous rental cars"
          />
        </LazyLoad>
        <h2 id="header5">Are Rental Cars Dangerous?</h2>
        <p>
          All vehicles are dangerous. Some more than others due to overall
          performance and safety. But are rental cars more dangerous than your
          typical daily drivers?
        </p>
        <p>
          Rental cars are heavily used and worn down quicker than someone's
          daily driving vehicle because of the constant renting to tourist and
          travelers, being used to throw luggage, suitcases and zipping around
          town or across country.
        </p>
        <p>
          Because these rental vehicles undergo so much wear and tear, they must
          have the proper amount of attention and maintenance to ensure they are
          safe to rent out for driving. Negligence in doing so can result in
          severe and catastrophic consequences.
        </p>
        <p>
          Some of the issues rental cars can experience consists of the
          following:
        </p>
        <ul>
          <li>Brake issues and failure</li>
          <li>Overheating</li>
          <li>Transmission problems</li>
          <li>Cosmetic Damage</li>
          <li>Occupational malfunctions (malfunctioning parts)</li>
          <li>Seatbelt failure</li>
          <li>Tire blowouts</li>
        </ul>
        <h2 id="header6">Do Rental Car Companies Rent Out Defected Cars?</h2>
        <p>
          Rental cars around the world were renting cars that should not have
          been rented out for a long time. Sadly, there are still companies that
          try and sweep things under the rug, for profit and avoiding paperwork
          and more work.
        </p>
        <p>
          Two sisters were driving a rental car when it caught fire. They ended
          up passing away, and after looking into the rental car companies
          records, found that the rental car (Chrysler PT Cruiser) had a recall,
          1 month before the accident, regarding a power steering leak that
          could cause a fire, which is exactly what happened.
        </p>
        <p>
          {" "}
          <Link to="https://www.congress.gov/bill/114th-congress/senate-bill/1173">
            The Raechel and Jacqueline Houck Rental Car Safety Act
          </Link>{" "}
          put a law in place that President Obama signed, making it illegal for
          rental companies to rent cars that had recalls or auto defects, making
          them fix the issue first before renting again.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://abcnews.go.com/video/embed?id=40832344"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2 id="header7">Watch Out For This!</h2>
        <p>
          It is always good to know red flags and warning signs to avoid
          accidents and possibly dangerous situations. Here are a few things to
          keep in mind and to watch out for next time you are renting a car.
        </p>
        <ul>
          <li>
            <strong>Does the vehicle appear to be severely damaged?</strong>{" "}
            When a rental car company allows you to rent a car even though it is
            in bad shape, looks battered or has obvious wear and tear, this is a
            red flag. Either the company does not have enough money to keep the
            cars health and appearance up, isn't taking good care of their
            vehicles, or does not care if your vehicle is safe or not.
          </li>
          <li>
            <strong>
              Does the vehicle make any loud and/or unusual noises?{" "}
            </strong>
            Cars should not squeak, squeal, hiss or squack, those noises are for
            animals. Loud and unusual noises are usually a red flag that
            something is broken, about to break or putting unnecessary strain on
            the vehicle, possibly resulting in a sudden change in the vehicles
            ability to drive, which could cause a wreck.
          </li>
          <li>
            <strong>
              Is there a burning smell, smoke or shaking/vibrating?{" "}
            </strong>
            When a vehicle is running efficiently, that means that all of the
            vehicles parts are working together smoothly and efficiently. When a
            burning smell occurs, parts are either rubbing together, wearing
            out, over heating or a mess of other possible issues could be
            resulting. If there is white, black or any other colored smoke, pull
            the car over to a safe spot and exit the vehicle as soon as you are
            able to do so. Cars don't smoke, and is a direct indication there is
            a disturbance in the normal flow of the vehicles performance. Any
            shaking, vibrating or rattling is an indication that there is heavy
            pressure on the vehicles engine, transmission or other important
            parts, loose parts and should be attended to as soon as possible.
          </li>
          <li>
            <strong>
              Are there important parts that are not properly working?
            </strong>{" "}
            If there are parts of the vehicle that are difficult to get working,
            not working, or only work a part of the time, refuse to rent the
            vehicle. This consists of:
            <ul>
              <li>Seatbelt failure or malfunctions</li>
              <li>Proper door latching (doors closing and opening properly)</li>
              <li>Brake failure or malfunctions</li>
              <li>Acceleration, engine idling and speed aspects</li>
              <li>Cracked windshield or windows</li>
              <li>Crooked alignment or bent wheels</li>
              <li>
                Malfunctioning horn or head/brake/tail lights and turn signals
              </li>
              <li>Windshield wipers that don't work</li>
              <li>
                Heavy exhaust fumes inside the vehicles occupant/rider cabin
              </li>
              <li>Dark tinted windows, taillights, headlights or reflectors</li>
            </ul>
          </li>
          <li>
            <strong>
              Inconsistent and sketchy sales/unprofessional company conditions:{" "}
            </strong>
            If you suspect that the rental car company you are about to rent
            from is being sketchy in any ways, remember the phrase, "If in
            doubt, don't go out." Find another company you can trust has your
            best interest in mind. Some small rental car companies are poorly
            managed, don't perform regular maintenance on their vehicles and
            don't care if they get in trouble or have a bad reputation. If you
            experience a poorly managed and dangerous rental car company, report
            them to law enforcement immediately. You could be saving lives.
          </li>
        </ul>
        <div id="header8"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="rental car accident law firm"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-rental-car-accident-lawyers.jpg')"
            }}
          >
            <h2 id="header8">Why Hire Bisnar Chase?</h2>
          </div>
        </LazyLoad>
        <p>
          Often, people figure hiring a lawyer is going to be expensive, cause
          them more stress and be a negative experience. With Bisnar Chase, you
          don't have to worry about stereotypes of attorneys and law firms. We
          are hear to help.
        </p>
        <p>
          Our skilled and experienced Rental Car Accident Lawyers have been
          representing victims for over <strong>40 years</strong>, winning over{" "}
          <strong>$500 million </strong>for our clients. Our
          <strong> 96% success rate </strong>proves that we know what we are
          doing.
        </p>
        <p>
          If you have any questions about your situation, talk to our legal team
          and receive your <strong>Free consultation </strong>and{" "}
          <strong>Free case evaluation</strong>. Give us a call at
          <strong> 800-561-4887</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
