// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/emergency-sign-banner-image.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="SUV Roof Crush Lawyers- Bisnar Chase"
        pageDescription="Call 800-561-4887 and have the California Roof Crush Lawyers of Bisnar Chase fight for you. Upon your call, you will receive a free consultation. If we don't win your case, you don't pay. Over $500M recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California SUV Roof Crush Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California SUV roof crush lawyer "
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong>California SUV Roof Crush Lawyers</strong> of{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          can guide you through the process of pursuing justice and help you get
          the best results for your catastrophic vehicle accident.
        </p>
        <p>
          Very often, consumers looking for family vehicles turn to SUVs because
          they have the room a growing family needs and their large dimensions
          give the appearance of safety. The frightening truth, however, is that
          many of these SUVs are designed poorly and that they are prone to
          accidents.
        </p>
        <p>
          The law office of Bisnar Chase has been representing victims of injury
          accidents for 40 years. Our California SUV roof crush attorneys have
          accumulated over<strong> $500 Million dollars in winnings</strong>
          and <strong>guarantee that if there is no win there is no fe</strong>
          e.
        </p>
        <p>
          If you have been involved in a serious SUV roof crush incident, you
          are entitled to compensation for your injuries and those of your
          family.
        </p>
        <p>
          For a <strong>free consultation</strong>, call the law firm of Bisnar
          Chase at <strong>1 (866) 909-6161</strong>.
        </p>
        <h2>Causes of California SUV Roof Crush Accident?</h2>

        <p>
          Sports Utility Vehicles (SUVs) are some of the most popular
          automobiles on California highways. It seems that all car
          manufacturers have their own versions of this type of vehicle, and
          consumers have snatched them up as quickly as they leave the assembly
          lines. Unfortunately, consumers have experienced multiple
          complications when driving out of the car dealership with these
          vehicles.
        </p>
        <p>
          The{" "}
          <Link to="https://www.nhtsa.gov/" target="_blank">
            National Highway Traffic Safety Administration
          </Link>{" "}
          (NHTSA) has shown that SUVs roll over almost three times as often as
          passenger cars and it is estimated that more than 10,000 people are
          killed in roof crush accidents each year.
        </p>
        <p>
          There are specific factors that have been linked to crashes that
          involve SUV's though.
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/red-car-wheel-resized.jpg"
            width="100%"
            alt="SUV roof crush lawyer in California"
          />
        </LazyLoad>
        <p>
          <strong>
            Below is a compiled list of what elements can lead to a roof crush
            accident
          </strong>
          .
        </p>
        <p>
          <strong>3 Causes of a California Roof Crush Accident</strong>
        </p>
        <ol>
          <li>
            <strong>Vehicle tripping</strong>: Factors that can contribute to an
            SUV roof crush accident can be the wheels of a vehicle treading on
            soft soil, a high slope or a driving too close to a guardrail. The
            weight of an SUV more top heavy than a four-door automobile. If an
            SUV is driven at a high speed on a highway with two lanes and there
            is a sudden problem, may have limited time to react and control the
            vehicle. SUV's should be driven with caution just in case the
            structure of the road is unreliable.
          </li>

          <li>
            <strong>Poor maintenance</strong>: Since an SUV is bigger in size,
            maintenance is necessary for not only for the driver but for other
            drivers on the road. Tires that are extremely deteriorated or low in
            air supply have been proven to put SUV drivers at risk. If a small
            puncture goes unnoticed this can lead to a tire blow-out. Statistics
            have concluded that over 78,000 drivers have been involved in
            serious crashes as a result of tire blow-outs.
          </li>
          <li>
            <strong>Swerving</strong>: SUV's are difficult to maneuver because
            of their structure. It can be hard to see behind and many drivers
            also underestimate a turn. Unlike regular motor vehicles, SUV's have
            an increased chance of tipping over if swerved aggressively. Drivers
            of larger vehicles are encouraged not to over exceed the highway
            speed limit. Studies show that over 40% of roof crush accidents
            could have been prevented if the driver of the SUV was not speeding.
          </li>
        </ol>
        <h2>Liable Parties in SUV Roof Crush Cases</h2>
        <p>
          Defective seat belt buckles, door locks or latches that fail to stay
          locked, seatback failures, airbag deployment failures, post-collision
          fuel fed fires, and seatbelt buckles that open during a crash can all
          result in catastrophic injuries for drivers and passengers.
        </p>
        <p>
          Not all drivers are at fault though when an SUV Roof crush accident
          occurs. Auto manufacturers have a duty to keep automobilist safe while
          driving. In 1966 the federal government instilled the "
          <Link
            to="https://en.wikipedia.org/wiki/National_Traffic_and_Motor_Vehicle_Safety_Act"
            target="_blank"
          >
            Motor Vehicle Safety Act
          </Link>
          " which required automobile manufacturers to follow specific standards
          of safety. The act came after studies indicated that almost 94,000
          people died in one year from a motor vehicle accident. Even though
          safety standards have been set in place this has not hindered drivers
          from being involved in auto accidents.
        </p>
        <p>
          Besides negligent drivers, manufacturers can be held liable for roof
          crush accidents as well.
        </p>
        <p>
          Manufacturers responsible for{" "}
          <Link to="/auto-defects" target="_blank">
            auto defects
          </Link>{" "}
          fall into two categories in product liability.
        </p>
        <p>
          <strong>Defective auto parts</strong>: If an auto part is made
          incorrectly, there is a high possibility that an accident will occur.
          Vehicle parts may not be defected initially but during shipment have
          been improperly cared for. Lack of care when transporting an auto part
          can lead to a crucial part of a vehicle, such as brakes to be damaged.
        </p>
        <p>
          <strong>Vehicle design</strong>: If a part of the vehicle was
          manufactured correctly, but the design itself caused severe injuries
          or deaths, the manufactured can still be held accountable. One of the
          most earliest examples of dangerous car designs were not implementing
          seat belts in automobiles.
        </p>
        <p>
          An experienced <strong>California SUV Roof Crush Lawyer</strong> can
          earn you the compensation you deserve for the losses and damages you
          have faced.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title=" SUV roof crush attorneys in California"
            style={{
              backgroundImage:
                "url('/images/text-header-images/ford-escape-carbon-monoxide-poisoning-exhaust-leak-attorneys-lawyers-ford.jpg')"
            }}
          >
            <h2>SUV Roof Crush Accident Statistics</h2>
          </div>
        </LazyLoad>
        <p>
          Sport Utility Vehicles were initially designed for off-road driving.
          Because of this, most SUVs have high suspensions and a small wheel
          base to allow the autos to navigate rugged terrain. These factors,
          along with the SUVs size, result in a higher center of gravity, which
          when paired with highway speeds and sudden turns can be a recipe for
          disaster.
        </p>
        <p>
          In 2003, 35.7 percent of fatal SUV crashes resulted in a{" "}
          <Link to="/car-accidents/suv-rollovers" target="_blank">
            rollover
          </Link>{" "}
          incident. That same year just 15.8 percent of fatal passenger vehicle
          accidents resulted in a roof crush. The NHTSA has concluded that roof
          crush accidents are one of the most significant safety problems for
          all classes of light trucks. For the years 1992 through 1996, there
          were an average of approximately 227,000 roof crash crashes per year
          which resulted in an average of 9,063 deaths and over 200,000 injuries
          per year.
        </p>
        <h2>Our Attorneys Will Get You The Compensation You Deserve</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/BisnarChasePhotosmall.jpg"
            className="imgleft-fixed"
            width="300"
            alt="California SUV roof crush attorneys"
          />
        </LazyLoad>
        <p>
          Sub-par safety standards can contribute to the dangerous nature of
          roof crush crashes. Roof crush accidents are especially dangerous
          because of the blunt force trauma the passengers experience during the
          crash. Passengers involved in roof crush accidents are often thrown
          from the vehicles during the crash, resulting in catastrophic injury.
        </p>
        <p>
          Occupants in a roof crush accident can also be injured by weak roof
          support reinforcements that collapse under the weight of the vehicle.
          Roofs that collapse can result in traumatic brain injuries, spinal
          cord injuries which can result in paralysis, and even fatalities.
        </p>
        <p>
          If you or someone you know has experienced serious injuries due to a
          California SUV roof crush accident the personal injury attorneys of
          Bisnar Chase are here to help. We have won millions of dollars for our
          clients and have taken on the toughest cases. Our SUV Roof Crush
          Lawyers have held at{" "}
          <strong>96% percent success rate for forty years</strong>.
        </p>
        <p>
          When you call today you will receive a free case analysis from one of
          our top legal team experts. Insurance companies do not have your best
          interest.
        </p>
        <p>
          <strong>
            Contact the California SUV roof crush attorneys of Bisnar Chase at
            1(866) 909-6161
          </strong>
          .
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
