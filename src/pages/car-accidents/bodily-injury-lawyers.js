// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { graphql } from "gatsby"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {
  spanishPageEquivalent: "/abogados/accidentes-de-auto"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        regex: "/california-bodily-injury-injured-leg-victim.jpg/"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Bodily Injury Lawyers - Bisnar Chase"
        pageDescription="Have you or a loved one suffered a personal injury, leaving you with pain & suffering? Has your injuries left you disabled & unable to work or perform your daily routine? We can help. The California Bodily Injury Lawyers of Bisnar Chase have over 40 years of experience. Call for your Free consultation at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bodily Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california bodily injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong>California Bodily Injury Lawyers</strong> of{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          offer highly skilled, aggressive and dedicated legal representation,
          by attorneys who have an established <strong>96% success rate</strong>
          .
        </p>
        <p>
          Bodily injuries range from mild to severe. Either of which should
          never be taken lightly or downplayed. We would like to run through
          some information, facts and tips that can help you and your case,
          which can ultimately make or break your case, because you could be
          entitled to compensation.
        </p>
        <p>
          If you have experienced a bodily injury and need a{" "}
          <Link to="/" target="new">
            California Personal Injury Lawyer
          </Link>
          , call <strong>800-561-4887</strong> for your Free consultation and
          case evaluation. Our skilled attorneys have won over{" "}
          <strong>$500 Million </strong>for our clients and will win you maximum
          compensation, or you do not pay anything.
        </p>
        <p>
          <strong>This page will cover the following:</strong>
        </p>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Does Bodily Injury Mean?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Top 7 Most Common Types of California Bodily Injury Claims{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Seeking Medical Attention Immediately{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Documenting Your Injuries ASAP{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Personal Injury and Bodily Injury{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              You Don't Pay If We Don't Win{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              The Process of a Bodily Injury Claim{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/car-accidents/bodily-injury-lawyers#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Hire Bisnar Chase?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">What Does Bodily Injury Mean?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/knee-injury-bodily-injury-lawyers-bisnar-chase.jpg"
            width="100%"
            alt="california bodily injury lawyer"
          />
        </LazyLoad>
        <p>
          Bodily injuries can have many different variations, but for the most
          part is any form of personal injury to one's body. At first you may
          think the term "bodily" is associated with fluids or liquids of some
          kind, when in fact the term bodily actually refers to, of or
          concerning the body.
        </p>
        <p>
          When the two terms are combined, the term bodily injury is used to
          describe a specific type of personal injury.
        </p>

        <h2 id="header2">
          Top 7 Most Common Types of California Bodily Injury Claims
        </h2>
        <p>
          Injuries happen every single day, when we least expect them to.
          Identifying why these circumstances arise and who is to be blamed is
          the next important step. Bisnar Chase wants to help you do so.
        </p>
        <p>
          There are many different ways a person or group of people can become
          injured, fortunately Bisnar Chase have the resources to help you in
          this great time of difficulty.
        </p>
        <p>
          Here is a list of the{" "}
          <strong>
            Top 7 Most Common Types of California Bodily Injury Claims:
          </strong>
        </p>
        <ol>
          <li>
            <strong>Sprains, pulls and tears:</strong> From car and motorcycle
            accidents to slips and falls, sprained ankles, torn ligaments and
            pulled muscles are common types of bodily injuries. From teenage
            soccer players to elderly staircase accidents, nobody is safe from a
            mild to severe personal injury.
          </li>
          <li>
            <strong>Broken bones</strong>: Bicycle accidents and premise
            liabilities can often result in bone fractures due to being
            unprotected from hazardous objects and surroundings.
          </li>
          <li>
            <strong>Lacerations or deep cuts:</strong> Working in the yard with
            sharp and dangerous tools can prove hazardous if precautions are not
            taken. Taking a fall on a skateboard or slipping on the bathroom
            floor and hitting the corner of the counter top can result in some
            nasty injuries and a trip to the emergency room for stitches.
          </li>
          <li>
            <strong>Burns or skin irritation:</strong> Chemicals, stoves,
            lighters, water-heaters, acid, mufflers, fireworks, light bulbs,
            house-hold cleaners, hot plates and surfaces; their truly is a never
            ending list of objects and substances that can result in first,
            second and even third degree burns.
          </li>
          <li>
            <strong>Traumatic brain and head injuries:</strong> Most common are
            concussions. Seen when the impact or blow to a persons head shakes
            their brain so violently, that it actually bounces off the skull,
            causing bruising, swelling and deadly symptoms. Strokes,
            hemorrhaging and other serious bodily injuries can be
            life-threatening if not properly attended to in a timely manner.
          </li>
          <li>
            <strong>Disfigurement:</strong> Humans are not made of steel,
            therefor disfigurements, bends and many different variations of
            disfigurement are possible, especially around heavy machinery, high
            speeds and uncommon motions and activities.
          </li>
          <li>
            <strong>Amputations:</strong> Again, especially around sharp items,
            power tools, machinery and heavy objects, the loss of limbs,
            fingers/amputations and partial amputations can happen in the blink
            of an eye.
          </li>
        </ol>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lVUUCtM3Ebg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2 id="header3">Seeking Medical Attention Immediately</h2>
        <p>
          In minor car accidents or fender benders, slip and falls, or any type
          of situation that causes personal injury, bodily injuries or any pain
          at all, victims should always seek medical attention if they think
          there is some legitimate possibility of having a known or unknown
          injury.
        </p>
        <p>
          Sometimes a person may not even know they have a serious injury and
          can suffer much more severe symptoms later on, and in some conditions
          those "minor injuries" can be deadly.
        </p>
        <p>
          Serious injuries that could be present within a victim but not
          visually apparent consist of:
        </p>
        <ul>
          <li>
            <strong>Head injuries</strong>
          </li>
          <li>
            <strong>
              {" "}
              <Link to="/head-injury/brain-trauma" target="new">
                Brain trauma
              </Link>
            </strong>
          </li>
          <li>
            <strong>Internal bleeding</strong>
          </li>
          <li>
            <strong>Bruised organs</strong>
          </li>
          <li>
            <strong>Broken bones</strong>
          </li>
          <li>
            <strong>Torn ligaments</strong>
          </li>
          <li>
            <strong>Damaged cartilage </strong>
          </li>
          <li>
            <strong>Pulled muscles</strong>
          </li>
          <li>
            <strong>Lacerations inside the body</strong>
          </li>
          <li>
            <strong>Blood clots</strong>
          </li>
          <li>
            <strong>Memory Loss</strong>
          </li>
        </ul>
        <p>
          Seeking professional medical attention immediately following an
          accident can significantly increase your chances of healing quickly
          and properly, and in serious cases, survival.
        </p>

        <h2 id="header4">Documenting Your Injuries ASAP</h2>
        <p>
          Cutting straight to the point; documenting or not documenting your
          injuries in affiliation to the accident, can make or break your case.
          Often, as stated above in the Seeking Medical Attention section, many
          people do not seek medical attention because they feel they are fine
          and don't need to be seen by a healthcare professional
        </p>
        <p>
          While getting your health checked and making sure you are in safe
          condition is always a good thing to do for yourself personally, it is
          also a great way of <strong>documenting your injuries</strong> and
          affiliating them to the accident you have just experienced.
        </p>
        <p>
          As you go through the litigation, settlement and claims process,
          evidence of your injuries, medical treatment you received, medical
          costs, prescriptions and so on, will be increasingly important, and
          can even affect the outcome of your case.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/documenting-bodily-injuries-california.jpg"
            width="100%"
            id="documenting-bodily-injuries"
            alt="california bodily injury attorney"
          />
        </LazyLoad>

        <h3>Other Ways to Document Your Injuries and Your Accident</h3>
        <p>
          Being proactive and gathering as much information as possible will
          only benefit you in your cases verdict. Here are some helpful tips to
          keep in mind when collecting data for your case:
        </p>
        <ul>
          <li>
            <strong>Evidence:</strong> This can consist of literally anything
            that had anything to do with the cause, result or other, of the
            accident. Defective products, burnt items, shrapnel, items that
            could contain fingerprints, etc. Any evidence could benefit the
            outcome of your case.
          </li>
          <li>
            <strong>Content:</strong>{" "}
            <strong>Photos, video, time-stamped audio recording </strong>(such
            as on an iPhone or Android) can help your case immensely. Photos and
            video footage documenting where the accident took place, where it
            happened, how it happened, the cars, obstruction, premise liability
            or people involved and so forth. If you or anyone else has footage
            of the accident actually happening, although could go viral on
            YouTube, save it for your case.
          </li>
          <li>
            <strong>Witnesses:</strong> Having people who witnessed the accident
            or incident is extremely helpful. Not only does it hold more weight
            on your side of the case, but it provides a backing to the truth and
            makes your story more believable. It's important to collect contact
            information of all witnesses present at the time of the accident or
            incident, consisting of:
            <ul>
              <li>Name</li>
              <li>Phone number</li>
              <li>Email address</li>
              <li>
                Their account of what happened :who, what, where, why and how
              </li>
              <li>Any evidence they may have</li>
              <li>Stay in contact with these people</li>
            </ul>
          </li>
          <li>
            <strong>Police Report:</strong> If police, Emergency Medical
            Response (EMS) or law enforcement arrived to the scene of the
            situation, there is a documented report of what happened and all
            information pertaining to the accident. Having this information can
            also be very helpful to your case.
          </li>
          <li>
            <strong>Detailed </strong>
            <strong>Information:</strong> Record what happened, where and when
            it happened, the reason for it happened, injuries resulted, people
            involved, etc for your own record. Matching this information with
            police reports, medical documentation, eye-witness testimonies and
            any evidence collected is a perfect recipe for a winning case.
          </li>
        </ul>
        <p>
          Learn more about{" "}
          <Link
            to="https://www.dmv.org/insurance/tips-on-documenting-personal-injury-expenses.php"
            target="new"
          >
            documenting your personal injury claims
          </Link>
          .
        </p>

        {/* <div
          className="text-header content-well"
          title="Doctors office visit"
          style={{
            backgroundImage:
              "url('../images/text-header-images/personal-injury-bodily-injuries-california.jpg')"
          }}
        >
          <h2 id="header3">Personal Injury and Bodily Injury</h2>
        </div> */}

        <p>
          There are a number of ways you can describe an injury to one's body.
          What is the difference between a personal injury and a bodily injury?
          There are generally speaking of the same thing, but in slightly
          different specifications.
        </p>
        <p>
          A bodily injury means physical damage to a persons body, not depending
          on how permanent or temporary. These injuries can consist of
          abrasions, disfigurements, cuts, pain, illness and basically any
          impairment of any organs, mental faculty or other parts of the body.
        </p>
        <p>
          Personal injury is simply classified as a physical injury to a
          person's body, different from damage to property, reputation or other.
          If you have experienced a personal or bodily injury in result of an
          accident, dangerous situation or incident involving hazardous
          conditions, negligence or wrong doing, call our skilled California
          Bodily Injury Attorneys for your Free consultation and case
          evaluation.
        </p>

        <h2 id="header6">You Don't Pay If We Don't Win</h2>
        <p>
          Hearing the terms lawyers and attorneys may intimidate people into
          associating them with expensive services that are not totally
          necessary. At Bisnar Chase, we guarantee that you will get the maximum
          compensation possible and best outcome for your case, or you don't pay
          anything.
        </p>
        <p>
          This is just our way of showing potential and current clients that we
          truly care and are here to help you in this time of need.
        </p>
        <p>
          Many law firms that are well-known and have strong reputations offer
          cases on a contingency-fee basis, but really have hidden fees and
          costs that you will have to pay whether they win or lose your case.
        </p>
        <p>
          We are straight forward with you, do not sugar coat anything and give
          you the facts, nothing hidden.
        </p>

        <h2 id="header7">The Process of a Bodily Injury Claim</h2>
        <p>
          The legal world and litigation can sound intimidating and expensive,
          but it doesn't have to be. With Bisnar Chase, we offer superior
          customer service, placing your needs first at all times. First, give
          us a call for your Free consultation and case evaluation, where a
          intake paralegal will speak with you and discuss your situation you
          have experienced, including accident or situation details, injuries,
          medical attention received, and so forth.
        </p>
        <p>
          Once the needed information has been received, the intake paralegal
          will discuss it with an attorney immediately to see if you have a
          case. Once your case is signed and you are a client of Bisnar Chase
          Personal Injury Law Firm, you have full access to questions and
          answers about your case, it's process, status updates and many more
          benefits that will help put your mind at ease throughout this
          difficult time in your life.
        </p>
        <p>
          Our skilled legal team will gather more information, strategize,
          negotiate and perform top-rated legal representation to ensure we win
          you maximum compensation.
        </p>
        <p>
          You can also learn more about the steps to{" "}
          <Link
            to="https://www.americanbar.org/groups/public_education/resources/law_related_education_network/how_courts_work/cases_settling.html"
            target="new"
          >
            {" "}
            settling cases
          </Link>
          .
        </p>

        <div id="header8"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="california bodily injury attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/california-bodily-injury-lawyers.jpg')"
            }}
          >
            <h2>Why Hire Bisnar Chase?</h2>
          </div>
        </LazyLoad>

        <p>
          Our skilled team of experienced{" "}
          <strong>California Bodily Injury Lawyers</strong> have been
          representing and winning cases for injured victims for over{" "}
          <strong>40 years</strong>, and wake up every morning ready to take on
          the world.
        </p>
        <p>
          With many satisfied clients who were once injured and stuck in a
          dead-end, the professional legal professionals of Bisnar Chase turned
          their lives around, and continue doing so every single day.
        </p>
        <p>
          Our law firm has won over <strong>$500 Million </strong>and has
          established an impressive <strong>96% success rate</strong>. If you
          need help figuring out what you should do in whatever situation you
          are currently in, call for your free consultation and free case
          evaluation.
        </p>
        <p>
          Even if we can't help you, we know who can. We can connect you with
          the best law firms or professionals that can offer you your best
          chance of success, because Bisnar Chase is here to help.
        </p>
        <p>
          <strong>
            To reach an experienced California Bodily Injury Lawyer and receive
            your Free consultation and case evaluation, call us toll free at
            800-561-4887.
          </strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
