// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { regex: "/lyft-banner-header-image-.jpg/" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Lyft Car Accident Lawyers California - Bisnar Chase"
        pageDescription="The experienced Lyft car accident lawyers of Bisnar Chase can help you receive compensation from a rideshare car crash. Free consultation & 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Lyft Car Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="California Lyft Car Accident Lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          If you or a loved one has been involved in a car crash with a Lyft or
          any other rideshare vehicle, the experienced{" "}
          <Link to="/car-accidents">California car accident lawyers</Link> at
          Bisnar Chase can help you better understand your legal rights and
          options. It can be quite challenging for those involved in a crash
          with a Lyft vehicle to grasp the complexity of liability and
          accountability issues involved.
        </p>
        <p>
          Our experienced Lyft accident lawyers have more than 35 years of
          experience representing the rights of seriously injured accident
          victims and their families. Contact us to obtain more information
          about pursuing your legal rights.{" "}
          <strong>Call 1-800-561-4887 for a free consultation</strong>.
        </p>
        <h2>What is Lyft and How Does It Work?</h2>
        <p>
          Lyft is a rideshare application.{" "}
          <Link to="/car-accidents/rideshare-accident-lawyer">Ridesharing</Link>{" "}
          is gaining in popularity with increase in fuel prices and the cost of
          auto insurance. Lyft is an application that you can use for finding
          rides. A user gets on the application and requests the ride. The Lyft
          driver picks him or her up. The passenger pays the Lyft driver after
          arriving the destination.
        </p>
        <p>
          While the service is a good way for drivers who have some extra time
          or are headed in the same direction as another person who is in need
          of a ride to make some quick cash, there is always the danger of a car
          crash and the question of who is liable.
        </p>
        <p>
          While Lyft has insisted on many occasions that they have extensive
          coverage in the event that one of the drivers gets into an accident,
          it is critical to make sure that the coverage actually applies to your
          specific situation.
        </p>
        <p>
          It is important to remember that the average personal car insurance
          policy will not cover damages that occur during commercial use. And
          auto insurance companies often view ridesharing as "commercial use."
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/lyft-damages-personal-injury.jpg"
            width="100%"
            alt="California Lyft car accident attorney"
          />
        </LazyLoad>
        <h2>Will Lyft's Policy Cover Damages?</h2>
        <p>
          When you look at a typical auto insurance policy, it will cover
          carpooling, which is defined as several passengers in the same car,
          going to a common location and sharing the cost.
        </p>
        <p>
          However, when an individual begins to make a profit from driving
          people around, it becomes a commercial venture and therefore necessary
          to get commercial auto insurance, similar to an insurance policy cab
          drivers have.
        </p>
        <p>
          If you are injured in an accident while riding a Lyft car, the
          insurance company might deny your claim because they will not cover
          damages for an accident that occurred when the vehicle was being used
          for commercial purposes.
        </p>
        <p>
          There is also the question of whether Lyft's policy will pay for any
          damages. The terms of the policy state that coverage is "limited to
          liability and does not pay for any collision, comprehensive or other
          types of damage."
        </p>
        <p>
          What this essentially means is that that Lyft driver could be held
          personally responsible for any injuries and damages that occur as he
          or she is transporting passengers.
        </p>
        <p>
          When a Lyft driver is in "Driver Mode," but has not picked up a
          passenger, then Lyft provides what it calls contingent liability
          insurance. This insurance applies when the Lyft driver's insurance is
          not available. This contingent liability coverage is $50,000 per
          person and $100,000 per accident for personal injury, and $25,000 per
          accident for property damage.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Lyft crash attorneys in California"
            style={{
              backgroundImage:
                "url('/images/car-accidents/reporting-incident-text-header.jpg')"
            }}
          >
            <h2>Reporting an Accident to Lyft</h2>
          </div>
        </LazyLoad>

        <p>
          If you or someone you love has been involved in a Lyft vehicle, it is
          important to file a police report and get a copy for your records. You
          also report the accident to Lyft. The claims phone number for Lyft is
          855-865-9553.
        </p>
        <p>
          It is very important to exercise caution when it comes to what you
          tell claims adjusters, regardless of whether they are with Lyft or
          work for an insurance company. Remember, anything you say could and
          probably will be used against you during the claims process.
        </p>
        <p>
          In addition to reporting the incident to Lyft, you should also report
          it to your own insurance company. In some case, particularly if the
          driver does not have proper insurance coverage, you may be able to
          seek damages through the uninsured motorist clause of your own auto
          insurance policy. But, you may not be able to do that if you don't
          report the incident to your insurance company right away.
        </p>
        <h2>Protecting Your Rights</h2>
        <p>
          If you have been injured in a crash involving a Lyft vehicle, be sure
          to get prompt medical attention and treatment. Save all receipts,
          invoices and paperwork relating to expenses you have incurred as a
          result of the accident and your injuries.
        </p>
        <p>
          Contact an experienced Lyft car accident lawyer who will help protect
          your rights every step of the way and secure maximum compensation for
          your losses.
        </p>
        <p>
          <strong>
            Call Bisnar Chase at 800- 561-4887 for a no-cost consultation and
            case assessment.
          </strong>
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
