// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Insurance Hard Ball - The Insurance Adjuster is Not Your Friend"
        pageDescription="Insurance companies have trained adjusters to minimize your payout! Injury victims should enlist the help of skilled personal injury lawyers at Bisnar Chase."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Insurance Hardball: The Insurance Adjuster</h1>
        <BreadCrumbs location={location} />
        <p>
          Most of us pay monthly premiums on our homes and vehicles with the
          hope that when we are involved in an accident or are involved in a
          traumatic incident, the insurance company will help us get back on our
          feet. However, insurance companies have proven time and again that
          they are not our friends, but average corporations whose primary goal
          is to protect their bottom line.
        </p>
        <p>
          If you are getting ready to file a claim for compensation in your car
          accident case, it is important for you know and understand that your{" "}
          <Link to="/resources/dealing-with-an-insurance-adjuster">
            insurance company is going to play hardball
          </Link>{" "}
          with you. They have trained adjusters whose job it is to ensure that
          the payout is minimal. Often, seriously injured victims need the help
          and firepower of experienced personal injury lawyers who can help them
          get fairly and fully compensated for their losses.
        </p>
        <h2>Before You Make the Call</h2>
        <p>
          If you have been{" "}
          <Link to="/car-accidents">hurt in a car accident</Link>, it is
          important that you notify your insurance company right away. You may
          want to prepare yourself, however, before calling the insurance
          company. Before making that call, you should write down how the
          accident occurred and gather your notes that have all the important
          details such as location of the crash, parties involved and damage to
          the vehicles. Refer to these notes throughout the phone call.
          Answering questions concisely and consistently will help your claim.
          If you fail to answer the questions carefully and end up contradicting
          yourself, your claim could potentially be denied.
        </p>
        <p>
          The best approach to take if you have been seriously injured is to
          refrain from talking to the insurance companies. Let your lawyer do
          that for you. This strategy will help ensure that you don't provide
          insurance companies access to your private medical or financial
          information. It will also help you avoid other missteps that could
          jeopardize your claim. Above all, it will give you the time, space and
          peace of mind you need at this time to make a quick and full recovery.
        </p>
        <h3>Understand Your Rights</h3>
        <p>
          One of the first things that the insurance company will ask you is if
          you give your consent to be recorded. You have nothing to gain by
          allowing them to record the conversation. They will only use the
          recording to listen for reasons to deny your claim. You have the right
          to tell the insurance company not to record the conversation.
          Furthermore, you do not have to answer any questions that you do not
          feel comfortable answering. If at any time during the conversation you
          are worried about your answers, you can decline to comment until you
          have spoken to an attorney.
        </p>
        <h3>More Negotiation Tips</h3>
        <p>
          After recovering from your injuries and obtaining the documents you
          need, you will need to start negotiating with the insurance company of
          the at-fault party. You should gather a copy of the police report,
          witness statements, photographs of the crash site, records of your
          medical treatment and your bills. Having an inadequate amount of
          evidence to back up your claim will hurt your chances of receiving
          fair compensation. It can also hurt the value of your claim to ask for
          too little. Understand what your claim may be worth and ask for more
          than you need so that you have more leverage.
        </p>
        <h3>Playing Hard Ball</h3>
        <p>
          According to a CNN news report, many insurance adjusters use a
          strategy developed in the mid-1990s by McKinsey & Co. to increase
          their profits. The theme of the strategy is to "deny, delay, and
          defend." Insurance adjusters regularly deny claims, delay the
          settlement of the claim and defend against the claim in court when
          necessary. In general, they hope that the claimant will give up and
          either drop the claim or accept a much smaller settlement - often much
          less than what the claim is really worth. The goal is to make the
          claim process so expensive and time consuming in order to discourage
          claimants.
        </p>
        <h3>You Can Fight Back</h3>
        <p>
          You do not have to give in to the tactics of ruthless insurance
          companies. You can appeal their decisions, refuse their offers and sue
          them when you are mistreated or when insurance companies have acted in
          bad faith. Before making any legal decisions, it is important to learn
          about your options and to seek guidance when you have questions. The
          initial consultation with an experienced car accident attorney is
          completely free. So, you have absolutely nothing to lose.
        </p>
        <h3>Knowing What to Ask</h3>
        <p>
          Knowledge is power. So do not be afraid to ask questions about your
          potential claim. Questions to consider asking an attorney should
          include:
        </p>
        <ul>
          <li>Do I have a valid claim?</li>
          <li>What is the potential value of my claim?</li>
          <li>What types of damages should I include in my claim?</li>
          <li>How long will the claim process take?</li>
          <li>Will I need to go to court?</li>
          <li>What questions will the insurance company ask me?</li>
          <li>What evidence will I need to prove my case?</li>
          <li>
            How much time do I have to file a claim before my statute of
            limitations runs out?
          </li>
        </ul>
        <h3>Experienced Lawyers Can Help</h3>
        <p>
          It is important to know how much your claim is worth, so that you know
          when you are being treated unfairly. If you have been hurt in a
          slip-and-fall accident, a car crash or as the direct result of someone
          else's negligence, you may seek support for all of your losses. For
          example, you can request compensation for your medical bills, lost
          wages, loss of earning potential, physical pain, mental anguish, the
          cost of rehabilitation services and other related damages.
        </p>
        <p>
          The experienced <Link to="/">California personal injury lawyers</Link>{" "}
          have a long track record of playing hardball with insurance companies
          - and succeeding. We know what it takes to win these cases against
          insurance companies. Our attorneys are tenacious and skilled
          negotiators who take their "A game" to the table each and every time.
          We know the strategies and tricks insurance companies commonly use to
          avoid paying you what you deserve to get. Our job is to make sure that
          you receive just compensation for all your losses. We will not rest
          until that happens. Please contact us at 949-203-3814 to find out how
          we can help you. Consultations and case evaluations are absolutely
          free.
        </p>
        <p>
          <strong>Other Service Locations for Car Accidents</strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/los-angeles/car-accidents">
              Los Angeles Car Accident Lawyers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/orange-county/car-accidents">
              Orange County Car Accident Lawyers
            </Link>
          </li>
        </ul>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
