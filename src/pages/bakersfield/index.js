// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import Img from "gatsby-image"
import { graphql } from "gatsby"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "header-images/bakersfield-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bakersfield Personal Injury Lawyers - Kern County"
        pageDescription="Bisnar Chase Bakersfield personal injury attorneys specialize in catastrophic injuries, car accidents, products liability, wrongful death & auto defects. Call us now Kern County at (949) 203-3814 for a FREE case evaluation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Bakersfield Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Bakersfield personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          If you've been injured due to someone else's carelessness contact the{" "}
          <strong>Bakersfield Personal Injury Lawyers </strong>of{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          now. Our firm has over
          <strong>
            40 years of experience and over $500 Million in verdicts, judgments
            and settlements.
          </strong>
        </p>
        <p>
          Bakersfield, California has been one of the fastest growing cities in
          the state, tripling its population . Yet, any Bakersfield personal
          injury law firm will advise the city's 320,000 residents to be aware
          of a variety of risks.
        </p>
        <p>
          Bisnar Chase personal injury attorneys have taken on the toughest of
          cases and do their due diligence to get you the compensation you need.
          Our expert team members are here waiting to take your call.
        </p>
        <p>
          If you have experienced a catastrophic injury due to someone's
          negligence contact the law offices of Bisnar Chase. As a victim of a
          severe personal injury, you may be entitled to compensation for
          medical bills and lost wages.
        </p>
        <p>
          <strong>Call 949-203-3814 and earn a free consulation</strong>.
        </p>
        <h2>5 Common Bakersfield Personal Injuries</h2>

        <p>
          According to city-data statistics, Bakersfield ranks #1 on the list of
          "Top 101 cities with men working in the mining industry" and #2 on the
          same list for women. With so many people turning a wage in mines, it
          is no surprise that personal injuries arise on the job.
        </p>
        <p>
          Other occurrences such as{" "}
          <Link to="/bakersfield/car-accidents" target="_blank">
            car accidents
          </Link>{" "}
          and even playground incidents have become prominent experiences in the
          Bakersfield area.
        </p>
        <p>
          <strong>
            Below are five common personal injuries in Bakersfield
          </strong>
          :
        </p>
        <LazyLoad>
          <img
            src="/images/car-accidents/crosswalk-image.jpg"
            width="311"
            className="imgright-fluid"
            alt="Personal injury attorneys in Bakersfield"
          />
        </LazyLoad>
        <ol>
          <li>
            <strong>Employment injuries</strong>: Trip and falls account for
            over 20% percent of employment injuries. In order to file a workers
            compensation claim, you must prove that you suffered an injury while
            on the job. The claim does not have to be directed towards your
            employer though. Negligent parties that can be held accountable for
            your injuries can include a property owner, a manufacturer of a
            machine or if you were hurt in a car accident on company time you
            could file a lawsuit against the driver or file for claim workers
            compensation.
          </li>

          <li>
            <strong>Slip and falls</strong>:{" "}
            <Link
              to="https://www.arbill.com/arbill-safety-blog/bid/203028/Painful-Statistics-on-Slips-Trips-and-Falls"
              target="_blank"
            >
              Abrill.com
            </Link>{" "}
            reported that about one million Americans suffer from slip and falls
            annually. Although slip and falls may seem like small incidences,
            accident victims of slip and falls can suffer serious injuries.
            Broken bones, cuts and even spinal cord injuries can stem from a
            slip and fall accident. Different traumatic brain injuries may also
            be the outcome of a trip and fall.
          </li>
          <li>
            <strong>Pedestrian accidents</strong>: In one year, exactly 42
            pedestrians lost their lives according to{" "}
            <Link
              to="https://www.turnto23.com/news/local-news/kcso-releases-pedestrian-death-totals-for-2017-and-current-total-for-2018"
              target="_blank"
            >
              23abc Bakersfield
            </Link>
            . Experts say that people can prevent pedestrian accidents by simply
            paying attention. In an effort to prevent pedestrian accidents Kern
            County officials have set up 25 new pedestrian crosswalk signs. The
            public works department is still working to fund more pedestrian
            signs.
          </li>
          <li>
            <strong>Auto accidents</strong>: Bakersfield like other major cities
            has not been immune to vehicle collisions over the years.
            Bakersfield, California has experienced over 1,445 injuries and
            fatalities in one year due to car crashes. A large part of the
            accidents were caused by drunk drivers. Drunk driving is not only
            illegal but dangerous. Driving under the influence of marijuana can
            also lead to a hefty fine and can put pedestrians and other drivers
            in danger.
          </li>
          <li>
            <strong>Injuries on school campuses</strong>: When parents drop off
            their children at school they are trusting that administration will
            provide a safe, well-maintained environment for their children. In
            Kern County, though there has been a number of student injuries due
            to poor maintenance on the field or students have suffered injuries
            as a result of lack of supervision. Student injuries have involved
            falling over elevated pavement to misuse of sports equipment.
          </li>
        </ol>
        <h2>Well-Known Personal Injury Cases in Bakersfield</h2>
        <p>
          In November of 2009, deputies of the Ridgecrest Substation went on
          search for 30-year-old female that had fallen into a mine shaft. The
          Indian Wells Mine Rescue Team and Kern County Fire worked together to
          reach the woman. She had fallen approximately 100 feet down a vertical
          shaft and was found dead. It is well-known that working in mines is a
          dangerous job, yet Bakersfield mining accident attorneys will tell you
          it is the responsibility of the company owner to ensure the safety of
          their employees, especially since so many Kern County -- the county in
          which Bakersfield is located -- residents take up this occupation.
        </p>
        <p>
          The city of Bakersfield is one of the fastest growing communities in
          the United States. The population was estimated at 328,692 as of 2008,
          making it the 11th largest city in California and the 58th largest
          city in the United States, according to{" "}
          <Link to="https://www.census.gov/" target="_blank">
            U.S. Census
          </Link>{" "}
          estimates.
        </p>
        <p>
          Like any growing, heavily populated area, the increased activity of
          the citizens of Bakersfield leads to increased instances of serious
          personal injury.
        </p>
        <p>
          The top Bakersfield personal injury attorneys at Bisnar Chase
          specialize in complex and economically significant Central Valley
          personal injury litigation throughout California.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/M35F4Ml0YPw"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          <em>
            Brian Chase discussing how he and the staff at Bisnar Chase have a
            deep level of compassion for our clients and how we work
            collectively to make their recovery an easier process
          </em>
          .
        </p>
        <p>
          Whether you were injured in an automobile crash, a slip and fall
          accident, a truck accident or a dog bite attack, our experienced
          California car accident lawyers at Bisnar Chase can help defend your
          rights to compensation under the law.
        </p>
        <h2>Auto Accidents in Bakersfield</h2>
        <p>
          {" "}
          <Link
            to="https://accidentdatacenter.com/us/california/bakersfield-ca"
            target="_blank"
          >
            Auto accidents in Bakersfield
          </Link>
          , especially Kern County, are a serious issue in and around the
          metropolitan area. Truck drivers hauling loads and tourists traveling
          with families often find themselves sharing the same highways in and
          around Bakersfield, often using the city as a rest stop as they travel
          to and from San Francisco and Los Angeles. As the highways become more
          congested, things like inclement weather, driver fatigue, and
          negligent driving can result in catastrophic injury on Bakersfield
          roadways.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Gavel with Money"
            style={{
              backgroundImage:
                "url('/images/text-header-images/gavel-with-money.jpg')"
            }}
          >
            <h2>We Don't Win Unless You Do</h2>
          </div>
        </LazyLoad>

        <p>
          If you are in need of a Kern County personal injury lawyer, please
          call us now at <strong>949-203-3814</strong>. This is a free,
          no-obligation call and you may be entitled to compensation. We will
          review your case for free and if we lose, you won't have to pay a
          cent!
        </p>
        <p>
          Bakersfield and Kern County residents only pay after we win your case.
          Our Bakersfield personal injury attorneys are committed to delivering
          you justice and we will fight for you Kern County!
        </p>
        <h2>Bisnar Chase Will Represent You Can</h2>
        <p>
          If you or a loved one have suffered from injury at school, work, a
          medical institution, or anywhere else, a Bakersfield personal injury
          lawyer can help right your situation.
        </p>
        <p>
          Your injury attorney will explain the legal issues of your personal
          injury case and fight for damages owed to you. The law offices of
          Bisnar Chase offer a<strong>no-fee guarantee</strong> meaning that you
          don't have to pay us unless we win your case.
        </p>
        <p>
          If you need help immediately, call one of our expert personal injury
          lawyers now at <strong>949-203-3814</strong> and we will schedule a{" "}
          <strong>free intial consultation</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
