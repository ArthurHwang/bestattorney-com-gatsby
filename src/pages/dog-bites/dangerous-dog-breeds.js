// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Top 13 Most Common Dangerous Dog Breeds - Bisnar Chase"
        pageDescription="There are many different types of dogs who can suddenly become violent even if they have no prior history of showing aggressive behavior. Our list of the Top 13 Dangerous Dogs may be helpful when identifying a dog attack situation. Call for more information at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Breeds: Top 13 Dangerous Dogs</h1>
        <BreadCrumbs location={location} />

        <p>
          Recovering from a serious dog attack can take a toll on you
          financially and emotionally. The California{" "}
          <Link to="/dog-bites">dog bite lawyers</Link> at Bisnar Chase have
          spent decades assisting injured victims. If you've been attacked by a
          dog and need legal guidance{" "}
          <strong>
            please call our office at 800-561-4887 for a free consultation
          </strong>
          . You may be entitled to compensation for your injuries.
        </p>
        <h2>Top Dangerous Dog Breeds: (alphabetical order)</h2>
        <p>
          It is important to remember that there is no such thing as a "safe"
          dog breed. All dogs can become dangerous at any point in time. The
          following list is based on breed reputation and the number of attacks
          that have been reported throughout the past.
        </p>
        <p>
          To see photos and more information about each breed, click on a breed
          name from the following list:
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/akita-dog-breed" target="new">
              Akitas
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/alaskan-malamute-dog-breed" target="_blank">
              Alaskan Malamutes
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="/dog-bites/american-staffordshire-terrier-dog-breed"
              target="_blank"
            >
              American Staffordshire Terriers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/boxer-dog-breed" target="_blank">
              Boxers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chows
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed" target="new">
              Doberman Pinschers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/german-shepherd-dog-breed" target="_blank">
              German Shepherds
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/great-dane-dog-breed" target="_blank">
              Great Danes
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds" target="new">
              Mastiffs
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/presa-canario-dog-breed" target="_blank">
              Perro de Presa Canarios
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/pit-bull-dog-breed" target="new">
              Pit Bulls
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/rottweiler-dog-breed" target="new">
              Rottweilers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/siberian-husky-dog-breed" target="_blank">
              Siberian Huskies
            </Link>
          </li>
        </ul>
        <p>
          Over a 20-year span, the{" "}
          <Link
            to="http://www.cdc.gov/features/dog-bite-prevention/"
            target="new"
          >
            CDC
          </Link>{" "}
          (Center for Disease Control and Prevention) conducted a study and
          collected data on dog bite and attack related fatalities. The
          following illustrates their findings.
        </p>
        <h2>CDC Fatal Dog Attack Study</h2>
        <div align="center">
          <table cellpadding="0" id="dog-attack-table">
            <tr>
              <td width="320" colspan="2">
                <strong>Purebred</strong>
              </td>
              <td width="320" colspan="2">
                <strong>Crossbred</strong>
              </td>
            </tr>
            <tr>
              <td width="200">
                <strong>Breed Name</strong>
              </td>
              <td>
                <strong># of Deaths</strong>
              </td>
              <td width="200">
                <strong>Breed Name</strong>
              </td>
              <td>
                <strong># of Deaths</strong>
              </td>
            </tr>
            <tr>
              <td width="200">Pit Bull Type</td>
              <td>39</td>
              <td width="200">Wolf Dog Hybrid</td>
              <td>14</td>
            </tr>
            <tr>
              <td width="200">Rottweiler</td>
              <td>39</td>
              <td width="200">Mixed Breed</td>
              <td>12</td>
            </tr>
            <tr>
              <td width="200">German Shepard</td>
              <td>17</td>
              <td width="200">German Shepard</td>
              <td>10</td>
            </tr>
            <tr>
              <td width="200">Husky Type</td>
              <td>15</td>
              <td width="200">Pit Bull Type</td>
              <td>10</td>
            </tr>
            <tr>
              <td width="200">Malamute</td>
              <td>12</td>
              <td width="200">Husky Type</td>
              <td>6</td>
            </tr>
            <tr>
              <td width="200">Doberman Pinscher</td>
              <td>9</td>
              <td width="200">Rottweiler</td>
              <td>5</td>
            </tr>
            <tr>
              <td width="200">Chow Chow</td>
              <td>8</td>
              <td width="200">Alaskan Malamute</td>
              <td>3</td>
            </tr>
            <tr>
              <td width="200">Great Dane</td>
              <td>7</td>
              <td width="200">Chow Chow</td>
              <td>3</td>
            </tr>
            <tr>
              <td width="200">Saint Bernard</td>
              <td>7</td>
              <td width="200">Doberman Pinscher</td>
              <td>0</td>
            </tr>
            <tr>
              <td>
                <strong>Total Purebred attacks</strong>
              </td>
              <td>
                <strong>153</strong>
              </td>
              <td>
                <strong>Total Crossbred attacks</strong>
              </td>
              <td>
                <strong>63</strong>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <strong>Total unknown breed attacks</strong>
              </td>
              <td colspan="1">
                <strong>238</strong>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <strong>Total deaths 1979-1998</strong>
              </td>
              <td colspan="1">
                <strong>454</strong>
              </td>
            </tr>
          </table>
        </div>
        <p>
          It is important to remember that these are only dog bites and attacks,
          which resulted in death. The number of attacks for which an injury
          occurred is unknown. The CDC does not track these types of attacks, as
          they are mostly based on eyewitness accounts and cannot be deemed
          accurate. Other studies have been conducted to provide a list of
          "safe" dogs or dogs which are best to be chosen as a child's or
          family's pet. The dogs on the following list have been deemed "safest"
          for having around small children and families. They have a good
          temperament, which means less aggression and little chance of attack.
        </p>
        <h2>Dogs with family friendly reputations:</h2>
        <p>
          To see photos of each breed, click on the breed name from the
          following list:
        </p>
        <ul>
          <li>
            {" "}
            <Link to="https://en.wikipedia.org/wiki/Beagle" target="_blank">
              Beagle
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Border_terrier"
              target="_blank"
            >
              Border Terrier
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Cavalier_King_Charles_Spaniel"
              target="_blank"
            >
              Cavalier King Charles Spaniel{" "}
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Cocker_Spaniel"
              target="_blank"
            >
              Cocker Spaniel
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/labrador-dog-breed" target="_blank">
              Labrador Retriever
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Golden_retriever"
              target="_blank"
            >
              Golden Retriever
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Golden_retriever"
              target="_blank"
            >
              Pug
            </Link>
          </li>
          <li>
            {" "}
            <Link to="https://en.wikipedia.org/wiki/Shih_tzu" target="_blank">
              Shih-Tzu
            </Link>
          </li>
          <li>
            {" "}
            <Link
              to="https://en.wikipedia.org/wiki/Staffordshire_Bull_Terrier"
              target="_blank"
            >
              Staffordshire Bull Terrier
            </Link>
          </li>
        </ul>

        <h2>Dog Safety</h2>
        <p>
          Remember, to always use caution when approaching ANY dog, regardless
          of size or breed, as it is their temperament, which determines their
          attitude and likelihood to bite or attack.
        </p>
        <p>
          If you have been bitten or attacked by any type of dog, it is
          important to seek medical treatment immediately after the attack.
          Never approach a dog that you are unfamiliar with and keep a safe
          distance between you and the dog. A fearful dog is much more dangerous
          than you might think. You can view a list of{" "}
          <Link to="/dog-bites/california-dog-bite-laws">
            {" "}
            California dog bite laws
          </Link>{" "}
          on our dog bite statutes page.
        </p>
        <p>
          <strong>
            {" "}
            <Link to="http://ht.ly/Yj8s30mLQZc" target="new">
              Dog Bite Statistics in the U.S (Infographic)
            </Link>
            .
          </strong>{" "}
          Download or share our dangerous breeds dog bite info-graphic to
          educate others on the dangers of approaching a dangerous dog.
        </p>
        <h2>Legal Assistance With a Dog Bite</h2>
        <p>
          Contact an experienced California dog bite attorney at Bisnar Chase
          who will fight for your rights and hold the negligent dog owner
          financially responsible for the damages caused by his or her pet.
          Injured victims may be entitled to damages including medical expenses,
          lost wages, hospitalization, cost of cosmetic surgery, psychological
          counseling, pain and suffering and emotional distress. Call us to
          obtain more information about pursuing your legal rights. Call
          800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  .content-well {
    padding: 15px 20px !important;
    box-shadow: 3px 3px 8px -1px #999, 0px 0px 10px 0px #e8ddbc inset;
    border: 1px solid #d4cebe;
    border-radius: 6px;
    background-color: #fffbf1;
    margin-bottom: 10px !important;
  }

  .text-header {
    padding: 95px 20px 20px !important;
    border: none;
    background-size: cover !important;
  }

  .text-header h2 {
    color: #c1b19a !important;
    font-size: 28px !important;
    text-shadow: 1px 1px 0px #423104, 2px 2px 0px #352615;
  }

  .page-navigation p {
    padding: 0 !important;
    text-decoration: underline;
    cursor: pointer;
    color: #2a699d;
    padding: 3px 0 !important;
  }
  .page-navigation p:nth-child(2n) {
    color: #2676bf;
  }

  .page-navigation p:hover {
    color: #0e395d;
  }

  .page-navigation .indented li {
    padding: 0 !important;
    font-weight: 400;
    margin: 0 0 0 30px !important;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
