// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Why Dogs Bite - Warning Signs & Prevention"
        pageDescription="You never know when a dog is going to suddenly change for the worst and attack someone. Why dogs bite: Dog bites can occur from a dog that has never had a history of vicious behavior, as well as dogs that consistently exhibit dangerous & violent demeanor. Call for your Free consultation with our attorneys at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Why Dogs Bite - Understand the Warning Signs</h1>
        <BreadCrumbs location={location} />
        <p>
          Recovering from a dog bite is a long and painful road. Many times the
          emotional pain can outweigh the physical injuries. Call our
          experienced <Link to="/dog-bites"> dog bite attorneys</Link> for a
          free consultation. You may be entitled to monetary rewards including
          pain and suffering. Call 1-800-561-4887.
        </p>
        <p>
          There are a number of{" "}
          <Link to="/dog-bites/factors-causing-canine-aggression">
            reasons why dogs bite
          </Link>
          . They may bite due to fear, to protect their territory, or to
          establish power or dominance over the person being bitten. Certain dog
          owners mistakenly teach their dogs that biting is an acceptable form
          of play behavior. Responsible dog ownership is critical when it comes
          to preventing dog bites. Many aspects of responsible dog ownership
          such as proper socialization, supervision, humane training,
          sterilization, and safe confinement-are necessary to prevent dogs from
          biting.
        </p>
        <p>
          <strong>
            Here are some of the most common reasons why a dog may bite:
          </strong>
        </p>
        <ul>
          <li>The dog is protecting a possession such as food or puppies.</li>
          <li>
            The individual has done something to provoke or frighten the dog
            such as hugging, moving into the dog's personal space, leaning or
            stepping over the dog or attempting to take something from the dog.
          </li>
          <li>The dog is scared, hungry, injured or sick.</li>
          <li>
            The dog has not learned bite inhibition and bites hard by accident
            when it is offered a food or toy.
          </li>
          <li>
            The dog views an individual or a child as "prey" because he or she
            is running or screaming near the dog.
          </li>
        </ul>
        <h2>Understanding the Warning Signs</h2>
        <p>
          It is important to know and understand the common triggers that cause
          dog bites so you can help avoid these types of dangerous situations.
          Dog bites are always followed by behavior on the part of a dog that an
          astute observer can use as warning signs and then take steps to reduce
          the dog's stress or fear and therefore, the likehood of an attack.
        </p>
        <p>
          When a dog is about to attack, its ears are typically pinned back. The
          fur along the animal's back may stand up and you may even be able to
          see the whites of their eyes. When a dog appears to be yawning, it is
          actually showing off its teeth. This should certainly be viewed as a
          warning sign. Non-social and stand-offish behavior such as freezing in
          response to a touch or look followed by direct, intense eye contact
          back from the dog, is yet another clear sign that he may bite.
        </p>
        <h2>How to Prevent Dog Bites</h2>
        <p>
          Dog owners have a responsibility and a{" "}
          <strong>legal obligation to prevent their dogs from biting</strong>.
          If you are a dog owner and don't intend to breed your dog, then,
          having it spayed or neutered will help decrease the risk of
          bite-related behaviors. Owners should also play and exercise with
          their dogs on a regular basis to help them expend excess energy that
          might otherwise be redirected toward{" "}
          <strong>nervousness or aggression</strong>. Do not indulge in rough
          play with your dog. Don't wrestle with your dog or play tug-of-war,
          which can lead to issues with dominance.
        </p>
        <ul>
          <li>
            <strong>Do take your dog to training school</strong>. Dogs should
            know basic commands such as sit, stay, come and leave it.
          </li>
          <li>
            <strong>Don't allow your dog to roam free</strong> in areas where
            they can be a danger to other people. Use a leash in public places.
          </li>
          <li>
            <strong>Do try to socialize your dog</strong> and expose it to many
            different people and situations.
          </li>
          <li>
            <strong>Keep rabies vaccinations up to date.</strong>
          </li>
          <li>
            <strong>If your dog shows signs of aggression</strong>, seek
            professional help from your veterinarian.
          </li>
          <li>
            <strong>
              If you have children, take the time to educate them{" "}
            </strong>
            about how to behave around dogs, what to watch for and how to react
            if a dog attacks.
          </li>
        </ul>
        <h2>Which Dogs Bite</h2>
        <p>
          In the United States, the list of top{" "}
          <Link to="/dog-bites/dangerous-dog-breeds">
            breeds involved in both bite injuries and fatalities
          </Link>{" "}
          changes from year to year and from one area of the country to another,
          depending on the popularity of the breed. However, several studies and
          surveys have shown consistently that
          <strong>
            {" "}
            pit bulls and rottweilers are involved in a disproportionately high
            number of dog attacks
          </strong>{" "}
          that result in serious injuries and deaths.
        </p>
        <p>
          Although genetics do play some part in determining whether a dog will
          bite, factors such as whether the dog is spayed or neutered, properly
          socialized, supervised, humanely trained, and safely confined play
          significantly greater roles. Responsible dog ownership of all breeds
          is the key to dog bite prevention.
        </p>
        <h2>Seeking Legal Help After a Dog Bite</h2>
        <p>
          Being attacked or bitten by a dog can be very traumatic, especially
          for children. Many dog bite victims have spent years trying to
          overcome the fear of being attacked again. Most{" "}
          <Link to="/dog-bites"> dog bite lawyers</Link> understand that there
          is an emotional layer to dog bite cases and clients have a long road
          to recovery, both physically and emotionally. If you've been attacked
          by a dog and sustained injuries you should contact a personal injury
          attorney immediately to secure your rights. If you'd like more
          information please contact our office for a free consultation. Call
          1-800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
