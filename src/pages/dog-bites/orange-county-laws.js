// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dog Bite Laws in Orange County California - Bisnar Chase"
        pageDescription="With a population of over 3.1 million people, dog attacks are inevitable, due to negligent dog supervision & handling. Protect your rights by learning the Orange County Dog Bite Laws & what to do in & after the event of a dog bite. Call for more information at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Orange County Dog Bite Laws</h1>
        <BreadCrumbs location={location} />

        <p>
          If you've been injured from a serious dog bite and need legal advice,
          contact our experienced dog bite lawyers for a free consultation. You
          may be entitled to damages. Call 949-203-3814.
        </p>
        <p>
          Orange County has dangerous dog laws in place in the interest of
          public safety. The county also has a process in place when a{" "}
          <Link to="/dog-bites/dangerous-dog-breeds">
            {" "}
            dog has been deemed dangerous
          </Link>
          . Here are the Orange County laws that pertain to "dangerous" or
          "vicious" dogs.
        </p>
        <h2>Sec. 4-1-95.</h2>
        <p>
          Declaration and possession of vicious or potentially dangerous dog.
        </p>
        <ol>
          <li>
            <em>General Provisions. </em>
          </li>
          <ol>
            <li>
              If the Director has cause to believe that a dog is a "vicious dog
              or potentially dangerous dog" within the meaning of section
              4-1-23, he or she may tentatively find and declare such dog a
              "vicious dog or potentially dangerous dog."
            </li>
            <li>
              Upon tentatively finding and declaring that a dog is a "vicious
              dog or potentially dangerous dog," the Director shall notify the
              owner and/or custodian in writing of his or her tentative finding
              and declaration.
            </li>
            <li>
              The notice shall inform the owner and/or custodian of such dog
              that he or she may request a hearing in writing before the
              Director within five (5) working days of receipt of such notice to
              contest the tentative finding and declaration. Any such hearing
              shall be requested and conducted as provided in subsection (d) of
              this section.
            </li>
            <li>
              Failure of the owner and/or custodian to request a hearing
              pursuant to subsection (a)(3) of this section shall result in the
              declaration becoming final.
            </li>
            <li>
              The possession or maintenance of a "vicious dog or potentially
              dangerous dog," or the allowing of any such dog to be in
              contravention of this division, is hereby declared to be a public
              nuisance. The director is hereby authorized and empowered to
              impound and/or abate any "vicious dog or potentially dangerous
              dog" independently of any criminal prosecution or the results
              thereof by any means reasonably necessary to ensure the health,
              safety and welfare of the public, including, but not limited to,
              the destruction of the dog or by the imposition upon the owner
              and/or custodian of specific reasonable restrictions and
              conditions for the maintenance of the dog. The restrictions and
              conditions may include but are not limited to:
            </li>
            <ol>
              <li>
                Obtaining and maintaining liability insurance in the amount of
                one hundred thousand dollars ($100,000.00) against bodily injury
                or death or damage to property and furnishing a certificate or
                proof of insurance by which the Director shall be notified at
                least ten (10) days prior to cancellation or nonrenewal or, at
                the owner's or custodian's option, the filing with the Director
                of proof of a bond in the amount of one hundred thousand dollars
                (100,000.00), to be able to respond in damages.
              </li>
              <li>
                Requirements as to size, construction and design of the dog's
                enclosure.
              </li>
              <li>Location of the dog's residence.</li>
              <li>
                Requirements as to type and method of restraints and/or muzzling
                of the dog.
              </li>
              <li>
                Photo identification or permanent marking of the dog for
                purposes of identification.
              </li>
              <li>
                Requirements as to the posting of a warning notice or notices
                conspicuous to the public warning persons of the presence of a
                vicious dog.
              </li>
              <li>
                Payment of a fee or fees as established by resolution of the
                Board of Supervisors to recover the costs of enforcing the
                provisions of [this] article 6, division 1 of title 4 of this
                Code as applied to the regulation of vicious dogs.
              </li>
            </ol>
          </ol>
          <li>
            <em>Notification of Right to Hearing.</em> At least five (5) working
            days prior to impoundment and/or abatement, the owner or custodian
            shall be notified in writing of his or her right to request a
            hearing in writing to determine whether grounds exist for such
            impoundment and/or abatement. If a hearing is requested, the
            impoundment and/or abatement hearing may be held in conjunction with
            the hearing provided for in subsection (a) of this section. If the
            owner or custodian requests a hearing prior to impoundment and/or
            abatement, no impoundment and/or abatement shall take place pending
            decision by the Director following a hearing, except as provided in
            subsection (c) of this section. Pending such impoundment and/or
            abatement hearing and decision by the Director, the Director may
            order the owner or custodian to keep the dog within a substantial
            enclosure or securely attached to a chain or other type of control
            which the Director may deem necessary under the circumstances. The
            Director may also order the owner or custodian to post and keep
            posted upon the premises where such dog is kept under restraint, a
            warning notice pending such impoundment and/or abatement hearing and
            decision by the Director. The form, content and display of such
            notice shall be specified by the Director. Any hearing under this
            subsection shall be conducted in accordance with subsection (d) of
            this section.
          </li>
          <li>
            <em>Immediate Impoundment.</em> When, in the opinion of the
            Director, immediate impoundment is necessary for the preservation of
            animal or public health, safety or welfare, or if the dog has been
            impounded under other provisions of this Code or State law, the
            preimpoundment hearing shall not be required; however, the owner or
            custodian shall be given written notice allowing five (5) working
            days from receipt of such notice to request in writing an abatement
            hearing. If requested, a hearing shall be held within five (5)
            working days of receipt of the request by the Director and the dog
            shall not be disposed of prior to the decision of the Director
            following such hearing. A hearing under this subsection shall be
            conducted in accordance with subsection (d) of this section except
            as otherwise indicated. If, after five (5) working days following
            receipt of such notice, no written request for a hearing is received
            from the owner or custodian, the dog in question shall be disposed
            of under applicable provisions of law.
          </li>
          <li>
            <em>Request for and Conduct of Hearings.</em> Except as otherwise
            provided in subsection (c) of this section, the Director shall
            conduct a hearing within fifteen (15) days following receipt of a
            written request from the owner or custodian requesting a hearing
            under this section, and notice of the time, date and place thereof
            shall be mailed to the person requesting the hearing at the address
            given in the hearing request, at least ten (10) days prior to said
            hearing. The Director may appoint a hearing officer to take
            evidence, summarize the evidence presented and report his or her
            findings and recommendations based on such evidence to the Director,
            or the Director may personally conduct the hearing.
          </li>
        </ol>
        <p>
          At the hearing each party shall have the right to call and examine
          witnesses, to introduce exhibits, to cross-examine opposing witnesses,
          impeach any witness, and to rebut the evidence against him or her. The
          hearing need not be conducted according to technical rules relating to
          evidence and witnesses. Any relevant evidence shall be admitted if it
          is the sort of evidence on which responsible persons are accustomed to
          rely in the conduct of serious affairs, regardless of the existence of
          any common law or statutory rule which might make improper the
          admission of such evidence over objection in civil actions. The rules
          of privilege shall be effective to the same extent that they are now
          or hereafter may be recognized in civil actions, and irrelevant or
          unduly repetitious evidence shall be excluded.
        </p>
        <p>
          Within fifteen (15) days following the conclusion of the hearing, the
          Director shall determine, on all the evidence presented to him or her,
          or on the summary of evidence and findings of fact and recommendations
          of the person holding the hearing, whether any designation,
          impoundment and/or abatement under this section should be rescinded or
          amended. Within five (5) working days following such decision, the
          Director shall notify in writing the person requesting the hearing of
          his or her determination as to any issue as to which the hearing was
          requested.
        </p>
        <ol>
          <li>
            <em>Change of Circumstances.</em> In the event of changed
            circumstances, the Director may amend or rescind any abatement
            and/or impoundment imposed pursuant to subsection (a)(5) of this
            section. Any such revision to the abatement and/or impoundment due
            to changed circumstances shall be subject to the same notice,
            hearing and other procedural requirements as required for imposing
            an initial abatement and/or impoundment set forth in subsections
            (b), (c) and (d) of this section.
          </li>
          <li>
            <em>Change of Ownership, Custody and/or Residence.</em> Owners of a
            vicious dog or potentially dangerous dog who sell or otherwise
            transfer the ownership, custody or residence of the dog shall at
            least ten (10) days prior to the sale or transfer, inform the
            Director in writing of the name, address and telephone number of the
            new owner, custodian and/or residence and the name and description
            of the dog. The owner shall, in addition, notify the new owner or
            custodian in writing of the details of the dog's record, terms and
            conditions of maintenance and provide the Director with a copy
            thereof containing an acknowledgment by the new owner or custodian
            of his or her receipt of the original. The Director shall notify the
            new owner or custodian in writing of any different or additional
            restrictions or conditions imposed pursuant to subsection (a)(5) of
            this section as a result of the change of ownership, custody or
            residence. The imposition of any such different or additional
            restrictions or conditions shall be subject to the same notice,
            hearing and other procedural requirements as required for imposing
            an initial abatement and/or impoundment set forth in subsections
            (b), (c) and (d) of this section.
          </li>
          <li>
            <em>Possession Unlawful.</em> It is unlawful to have custody of, own
            or possess a vicious dog or potentially dangerous dog within the
            meaning of section 4-1-23 unless it is restrained, confined or
            muzzled so that it cannot bite, attack or cause injury to any
            person.
          </li>
          <li>
            <em>Declared Vicious Dog or Potentially Dangerous Dog.</em> It shall
            be unlawful for the owner and/or custodian of a dog declared vicious
            or potentially dangerous pursuant to subsection (a) to fail to
            comply with any requirements or conditions imposed pursuant to
            subsection (a)(5) of this section. If a vicious or potentially
            dangerous dog escapes, the owner and/or custodian shall immediately
            notify the Director and make every reasonable effort to recapture
            it.
          </li>
        </ol>
        <p>
          The Director shall have the discretion, in any event, to directly
          petition the court to seek a determination whether or not the dog in
          question should be declared potentially dangerous or vicious. The
          Director shall follow the procedures set forth in Food and Agriculture
          Code Sections 31621 and following for this purpose.
        </p>
        <p>
          <strong>Contacting an Experienced Dog Bite Lawyer</strong>
        </p>
        <p>
          If you or a loved one has been injured in a dog attack, the
          experienced{" "}
          <Link to="/orange-county/dog-bites">
            Orange County dog bite lawyers
          </Link>{" "}
          at Bisnar Chase can help you better understand your legal rights and
          options. Injured dog bite victims may be entitled to compensation for
          damages such as medical expenses, lost wages, hospitalization, cost of
          physical and psychological therapy, pain and suffering and emotional
          distress.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
