// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Ventura County Dog Bite Laws - Bisnar Chase"
        pageDescription="Bisnar Chase is here to help you understand the Ventura County Dog Bite Laws & provide the top legal representation dog bite victims can get. For more information & a Free consultation, Call 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ventura County Dog Bite Laws</h1>
        <BreadCrumbs location={location} />
        <p>
          Ventura County has yet to put in place any county ordinances that
          pertain to dangerous dogs. However, there are several laws relating to
          dog licensing and maintenance that apply to responsible dog ownership.
          These laws are all covered under Chapter 4 of Division 4 under the
          county's code.
        </p>
        <h2>4411 License tag requirements</h2>
        <p>
          Every person who resides in the unincorporated areas of the County of
          Ventura and who   who owns, harbors or keeps any dog over the age of
          four (4) months for thirty (30) days or longer shall obtain a current
          license and license tag issues under the provisions of this Code and
          stamped with the year in which the license was issued. The license tag
          obtained shall be attached to a substantial collar or harness, which
          shall be worn by the dog at all times except as provided in Section
          4411-1. Every person who violates any of the provisions of this
          section is guilty of an infraction. Any dog found without a current
          license tag or for which there is no current license may be taken up
          and impounded by the Poundmaster or any peace officer.
        </p>
        <h2>4421-3 Prohibition against unaltered dog or cat</h2>
        <p>
          Every person who owns a dog or cat over the age of four months that is
          kept in the unincorporated areas of the County of Ventura is required
          to have such dog or cat spayed or neutered unless an exception applies
          under Section 4421-4. Nothing in this Section shall make veterinarians
          mandatory reporters of animals that have not been spayed or neutered.
        </p>
        <h2>4461 Leash Law</h2>
        <p>
          It is unlawful for any person to suffer or permit any dog owned,
          harbored, or controlled by him or her to be on any public street,
          alley, lane, park of place of whatever nature open to and used by the
          public in the unincorporated areas of the county unless such dog is
          securely leashed and the leash is held continuously in the hands of a
          responsible capable of controlling such dog. It is unlawful for any
          person to suffer or permit any dog owned, harbored or controlled by
          him or her to be on any private property in the unincorporated areas
          of the County without the permission of the person owning or occupying
          said property. Every person who violates any of the provisions of this
          Section is guilty of an infraction.
        </p>
        <h2>4467 Animal nuisances.</h2>
        <p>
          Any animal, except an animal kept or controlled by a governmental
          agency, which has committed any one or more of any of the following
          acts is a public nuisance: (a) An unprovoked infliction of physical
          injury upon any person where such person is conducting himself or
          herself lawfully. (b) Unprovoked threatening behavior toward any
          person where such person is conducting himself or herself lawfully
          which occurs in such circumstances as to cause such person reasonably
          to fear for his or her physical safety. (c) The utterance of barks or
          cries which are so loud, so frequent and continued over so long a
          period of time as to deprive persons residing in two or more
          residences in the neighborhood of the comfortable enjoyment of their
          homes. (d) An unprovoked infliction of physical injury upon any other
          animal which occurs off the property of the owner or keeper of the
          animal inflicting the injury. (e) The damaging of the real or personal
          property of some person other than the owner or keeper of the animal
          which occurs off the property of the owner or keeper of the animal.
          (f) The dumping of trash cans or the spreading of trash which occurs
          off the property of the owner or keeper of the animal. (g) The chasing
          of pedestrians, vehicles or ridden horses which occurs off the
          property of the owner or keeper of the chasing animal. Such public
          nuisance may be abated in accordance with the procedures set forth in
          Sections 4467-1 through 4467-10. Such procedures are in addition to
          any other remedies which may be available under the law.
        </p>
        <h2>4468 Destruction of wild dogs.</h2>
        <p>
          Any dog which is running at large and which is by reason of its
          vicious disposition dangerous to persons or property may be shot by
          any peace officer or taken up and destroyed in a humane manner by the
          Poundmaster.
        </p>
        <h2>Seeking Legal Help</h2>
        <p>
          If you or a loved one has been injured in a dog attack in Ventura
          County, the experienced <Link to="/dog-bites"> dog bite lawyers</Link>{" "}
          at Bisnar Chase can help you better understand your legal rights and
          options. Injured victims of dog bites can seek compensation for
          damages from negligent pet owners for medical expenses, lost wages,
          hospitalization and pain and suffering. Our knowledgeable dog bite
          attorneys have successfully handled these types of cases on a regular
          basis and understand the laws and local statutes, which pertain to dog
          attack cases.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
