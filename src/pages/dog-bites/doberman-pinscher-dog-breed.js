// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/doberman-pinscher-breed.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Doberman Pinschers Dog Breed - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="Doberman Pinschers are known for their confidence, alertness, loyalty & intelligence, but mixed with bad temperament & abuse could be a recipe for a killer. Learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Doberman Pinschers Dog Breed</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="doberman pinscher breed and dog bite information"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong>Doberman Pinschers</strong>: The iconic ears standing to
          attention, listening for every minute detail of sound, the confident
          stance and powerful physique, the Doberman Pinscher is an impressive
          breed that have many traits and historical facts that may surprise
          you.
        </p>
        <p>
          Unfortunately, there are certain dogs that are held to strong-believed
          stereotypes, such as "known aggressive breeds," such as the Pit Bull,
          Rottweiler, Akita, and the Doberman Pinscher.
        </p>
        <p>
          But are these dogs truly as vicious as the media portrays? When dog
          bite statistics are compared alongside other breeds and dog sizes, are
          the ratios of breed-to-bite stereotypes as accurate as we think? Or do
          these dogs have the ability to be warm and welcoming, compassionate
          and loyal?
        </p>
        <p>
          When it comes down to reality, the breed of the dog does not matter.
          Every animal has the potential to react differently than expected,
          especially in situations that are unknown, challenging or
          intimidating.
        </p>
        <p>This page will cover the following:</p>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Can Doberman Pinschers Be Family Companions, or Just Attack Dogs?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of Doberman Pinschers{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Doberman Pinscher Dog Bite Statistics{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Are Doberman Pinschers Notorious for being Security Dogs?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Is a Doberman Pinscher's temperament?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning a Doberman{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Doberman's Aggression Vs. Other Breeds{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              5 Steps to Protecting Yourself from an Aggressive Doberman
              Pinscher Attack{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Medical Attention After a Doberman Dog Bite{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Documenting Your Doberman Pinscher Dog Bite Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Your Case Needs to Win a Dog Bite Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/doberman-pinscher-dog-breed#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Let Bisnar Chase Represent You and Win, or You Do Not Pay{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">
          Can Doberman Pinschers Be Family Companions, or Are They Only Capable
          of Being Attack Dogs?
        </h2>

        <LazyLoad>
          <img
            src="/images/text-header-images/doberman-companions.jpg"
            width="100%"
            alt="dobermans around children"
          />
        </LazyLoad>

        <p>
          Dobermans have forever been portrayed as the blood-thirsty beasts,
          obeying their owner's commands, standing at attention at all times.
          But while Dobermans are capable of this "never resting" stereotype,
          they can be surprisingly domesticated and gentle around children,
          elderly and the sick and disabled.
        </p>
        <p>
          This intelligent breed of dog has been a family favorite for decades;
          protecting children, defending the elderly, socializing during family
          events and get togethers, Doberman Pinschers are very sociable dogs,
          especially when their upbringing was infused with consistent
          interaction and mingling with strangers and friends.
        </p>
        <p>
          Although Dobermans have the friendly and patient side, this does not
          mean that if a Doberman Pinscher is acting domesticated, gentle and
          non-aggressive, this doesn't mean that they can't suddenly bite or
          attack out of fright, excitement, danger or nervousness.
        </p>
        <p>
          Even dogs that have no history of prior aggressive behavior, can bite
          as a defense mechanism. But the overall temperament of the Doberman
          Pinscher can widely vary depending on the type of upbringing they
          experienced.
        </p>
        <p>
          If you have been attacked or experienced a dog bite from a Doberman
          Pinscher or other dog breed, you are always urged to seek medical
          attention immediately and to contact a team of skilled and highly
          experienced{" "}
          <Link to="/dog-bites" target="new">
            Dog Bite Lawyers
          </Link>
          .
        </p>
        <p>
          <strong>Call us for a Free consultation at 800-561-4887.</strong>
        </p>

        <h2 id="header2">History of Doberman Pinschers</h2>
        <p>
          Originating in the city of Apolda, Germany, in the state of Thurungia
          around 1890. After the Franco-Prussian War, Karl Friedrich Louis
          Dobermann was a local tax collector and was in charge of the dog
          pound. He experimented with breeding different types of dogs until he
          came up with the perfect defender he could bring along during his tax
          collecting; Karl Dobermann collected his taxes in locations that ran
          high with bandits and robbers.
        </p>
        <p>
          Also known as the "Tax Collector's Dog," the Doberman breed wasn't
          officially recognized until 1900. The first registered Doberman was
          registered in 1898, and the first Doberman registered with the{" "}
          <Link to="https://www.akc.org/" target="new">
            American Kennel Club
          </Link>{" "}
          was registered in 1908.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/black-and-white-doberman.jpg"
            width="100%"
            alt="history of the doberman pinscher"
          />
        </LazyLoad>
        <LazyLoad>
          <img
            src="/images/text-header-images/russian-doberman-pinscher-stamp.jpg"
            width="135"
            className="imgleft-fixed"
            alt="doberman pinscher history in law enforcement"
          />
        </LazyLoad>

        <p>
          The Doberman Pinscher breed found their way into the military and law
          enforcement and make a tremendous aid to the brave men and women who
          fight for our country and our freedom every single day.
        </p>

        <p>
          These brave and loyal dogs have carried out tasks with pride, even
          when on the battlefield; with guns firing, bombs exploding, shrapnel
          and debris flying all over the place, these heros established a valid
          name for themselves.
        </p>
        <p>
          Dobermans also fulfill their duty on law enforcement teams, sniffing
          for drugs, explosives and other dangerous tasks that help our men and
          women in uniform stay safe, so they can return home to their families.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well clearfix"
            title="doberman dog bite statistics"
            style={{
              backgroundImage:
                "url('/images/text-header-images/doberman-dog-bite-statistics.jpg')"
            }}
          >
            <h2 id="header3">Doberman Pinscher Dog Bite Statistics</h2>
          </div>
        </LazyLoad>

        <p>
          Even though Doberman Pinschers make great family companions, they are
          still animals with natural instincts and defense mechanics. Even if a
          dog has never expressed hostile behavior before, does not mean they
          can't show aggressive behavior in the future, suddenly, and without
          warning.
        </p>
        <ul>
          <li>
            Dobermans are responsible for: (September 1982- December 2014)
            <ul>
              <li>23 severe attacks</li>
              <li>12 child attacks</li>
              <li>9 adult attacks</li>
              <li>8 deaths</li>
              <li>
                12 maimings
                <ul>
                  <li>
                    *According to{" "}
                    <Link
                      to="https://petolog.com/articles/dogs-attack-statistics.html"
                      target="new"
                    >
                      Petolog.com
                    </Link>
                    *
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            Dobermans are highly unlikely to turn on their owners/handlers
          </li>
          <li>
            Dobermans have been on record for breaking a mans forearm with only
            one bite of their powerful jaws
          </li>
          <li>
            A healthy Doberman bite can measure in at a biting pressure of
            200-400 pounds
          </li>
        </ul>
        <p>
          For more information about Doberman Pinscher dog bite statistics and
          information, you can also visit{" "}
          <Link
            to="https://pethelpful.com/dogs/Most-Dangerous-Dog-Breeds"
            target="new"
          >
            PetHelpful.com
          </Link>
        </p>

        <h2 id="header4">
          Why Are Doberman Pinschers Notorious for being Security Dogs?
        </h2>

        <LazyLoad>
          <img
            src="/images/text-header-images/security-doberman-at-attention.jpg"
            width="100%"
            alt="doberman pinscher confident attention security"
          />
        </LazyLoad>

        <p>
          You've seen them in the movies, chasing the burglars, being held in
          the back of police K9-Units and even with bomb squads, swat team and
          the military, and there is a reason; their loyalty, their
          determination, focus and agility makes them a prime candidate for
          security.
        </p>
        <p>
          Their powerful body, muscular legs, disabling bite and sheer speed
          make them a machine for destruction, taking down anything in their
          path if necessary.
        </p>
        <p>
          Their deep black, mystical blue and red coats give them an appearance
          that can't be confused with any other breed. Other breeder actually
          breed Doberman Pinschers with other breeds to give them the benefits
          of a Pinscher.
        </p>
        <p>
          Rottweilers are also an unmistakable presence, with their brown and
          black markings, powerful physique, confident stance, these breeds
          carry themselves with pride and loyalty to their handlers.
        </p>
        <p>
          To have any sort of comparison of why Dobermans are the epitome of
          security and enforcement would be pointless. What would stop a burglar
          in their tracks? A Poodle, Jack Russel Terrier, maybe a Corgi?
        </p>
        <p>Other known breeds that are used in law enforcement are: 2885</p>
        <ul>
          <li>German Shepards</li>
          <li>
            {" "}
            <Link to="/dog-bites/rottweiler-dog-breed" target="new">
              Rottweilers
            </Link>
          </li>
          <li>Labrador Retrievers</li>
          <li>
            {" "}
            <Link to="/dog-bites/pit-bull-dog-breed" target="new">
              Pit Bulls
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chows
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds" target="new">
              Mastiffs
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed" target="new">
              Doberman Pinschers
            </Link>
          </li>
        </ul>
        <p>
          Although any form of law enforcement and the military is dangerous,
          these dogs are specifically trained for many different types of
          situations. From hand to hand combat, to solo-dog missions, letting
          their handlers know of oncoming dangers, there are a plethora of ways
          a military dog can serve their country.
        </p>
        <p>
          In 1944, the Japanese invaded American soldiers on the island of Guam.
          The Doberman Pinscher military dog could sense the Japanese invasion
          coming and alerted Marine Capt. William W. Putney, who led the patrol.
        </p>
        <p>
          Although Cappy was killed, he saved his men from a catastrophic
          situation. Today, there is a monument with the names of 28 other
          military dogs that passed away while serving their country. There is a
          symbolic statue of a Doberman Pinscher on top.
        </p>

        <h2 id="header5">What Is a Doberman Pinscher's temperament?</h2>
        {/* <LazyLoad>
          <img
            src="/images/text-header-images/temperament-of-doberman-pinscher.jpg"
            width="40%"
            alt="doberman pinscher temperament"
          />
        </LazyLoad> */}
        <p>
          Society and the media have created a frenzy with certain dog breeds
          and identify them as "killers from birth." This is inferring that
          these dog breeds have no control over their thoughts, actions or
          temperament, and suggest that it's strictly DNA, in the genes, and
          that the nurturing and upbringing of the animal is not a factor.
        </p>
        <p>
          All living things that learn and grow as they get older, experience
          different environments and situations that ultimately transform them
          into who they become and how they act.
        </p>
        <p>
          With exceptions for dogs that have mental issues, over-aggressive
          tendencies and even diseases from an early age, a Doberman Pinscher,
          like all other breeds have the capability to act all types of ways.
        </p>
        <p>
          A Doberman that is raised in an uncomfortable environment, neglected,
          given little or no attention may grow up to be timid,
          overly-self-defensive, a biter, barker and extremely unpredictable.
        </p>
        <p>
          A Doberman that is raised with constant training, fighting, aggressive
          roughhousing and even physical and mental abuse, are usually the
          stories of how dog fighters are raised. Dog fighting is the illegal
          and extremely abusive act of training dogs to be as vicious as
          possible, to go up against other dogs in a fight to the death, while
          the owners/handlers and spectators observe and bet on the winner and
          or loser.
        </p>
        <p>
          A Doberman that is raised in a loving environment, with people and
          even other pets that share and give affection, attention, playtime and
          learn gentleness and playfulness, is usually how a family companion is
          raised. Being trained what is right and wrong from an early age with
          appropriate training techniques, discipline and strategies is
          important.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/doberman-pinscher-breed-characteristics-chart.JPG"
            width="50%"
            className="imgcenter-fluid"
            alt="doberman breed characteristic chart"
          />
        </LazyLoad>

        <center>
          <blockquote>
            The breed characteristics chart above was provided by{" "}
            <Link
              to="https://dogtime.com/dog-breeds/doberman-pinscher"
              target="new"
            >
              DogTime.com
            </Link>
            .
          </blockquote>
        </center>

        <h2 id="header6">Owning a Doberman</h2>
        <p>
          Doberman Pinschers are extremely intelligent and loyal, hardworking
          dogs. Their ability to learn, accept training and overcome complex
          tasks take them to the top of the ranks, ranking #5.
        </p>
        <p>
          According to{" "}
          <Link to="http://petrix.com/dogint/intelligence.html" target="new">
            Petrix.com
          </Link>
          , rank 1-10 of the world's brightest dogs will:
        </p>
        <ul>
          <li>Understand new commands in less than 5 repetitions</li>
          <li>Obey first command 95% of the time or better</li>
        </ul>
        <p>
          Having a Doberman companion at home can also change your overall
          lifestyle. With breeds like Doberman Pinschers, more energy-relief,
          exercise, attention, training, bonding and so on, is needed with these
          breeds. Also, others might be more timid around these animals,
          especially at first.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/IBA6EDYdSLs?rel=0 frameborder=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          If your Doberman likes to bark at noises, strangers or anything
          period, noise complaints, reports and other frustrating issues can be
          an unpleasant experience for Doberman owners. Taking your Doberman for
          frequent walks, runs, playtime and opportunities for them to explore,
          discover, play and make other dog friends, helping with their
          cognitive abilities, training, maturity and coping with all types of
          experiences.
        </p>
        <p>
          Not allowing your Doberman to socialize with other dogs, animals, and
          people will only make your Doberman more aggressive, stand-offish, and
          hard to handle.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="doberman pinscher dog bite attack"
            style={{
              backgroundImage:
                "url('/images/text-header-images/rabid-doberman.jpg')"
            }}
          >
            <h2 id="header7">Doberman's Aggression Vs. Other Breeds</h2>
          </div>
        </LazyLoad>

        <p>
          There are many dogs we encounter that are barking, growling, tugging
          on their chain, looking like they want to bite you. Although size does
          seem to matter when it comes to the type of dog attacking, are some
          breeds more aggressive than others that have a reputation for being a
          hostile breed by nature?
        </p>
        <p>
          Although Doberman Pinschers seem to have more size, weight and power
          than say, a Pug or Chihuahua, their temperaments may not be all that
          different. Smaller dogs often indicate a desire for aggressive
          behavior. Barking, snipping, growling and gremlin-like actions.
        </p>
        <p>
          While these smaller dogs are showing signs of aggression and often
          biting at ankles, wrists, hands, toes, fingers and other easy-to-bite
          areas of the body, the injury or wound inflicted by these
          bites/attacks may not be all that damaging, compared to a larger dog,
          such as a Doberman Pinscher, Rottweiler or Pit Bull.
        </p>
        <p>
          This is where disagreements come into play regarding breed
          temperaments compared to others. Many dog breeders, owners and
          handlers are frustrated at the fact their breeds of dog are being
          focused on more than others because of the injury severity.
        </p>
        <p>
          Many people feel that overall breed temperament and average aggression
          should be categorized by specific groupings, and not identifying one
          breed "more aggressive" than another, based on injury severity.
        </p>
        <p>
          Example: If Chihuahuas have more statistics showing aggression,
          hostility and violent behavior overall, than compared to the overall
          statistics found for Doberman Pinschers, Chihuahuas should be listed
          as more aggressive than Dobermans.
        </p>
        <p>
          If Dobermans are shown to have an average of more severe injuries
          inflicted by bites than compared to Chihuahua bite injuries, that
          should be an entirely separate category than judging it strictly by
          bites/attacks, and not taking the severity of the injuries into
          consideration for the overall rating.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="5 steps avoiding dog attack"
            style={{
              backgroundImage:
                "url('/images/text-header-images/avoiding-a-dog-bite-dog-attack.jpg')"
            }}
          >
            <h2 id="header8">
              5 Steps to Protecting Yourself from an Aggressive Doberman
              Pinscher Attack
            </h2>
          </div>
        </LazyLoad>

        <p>
          Even dogs that have never shown an ounce of aggression throughout
          their lifetime can suddenly, and without warning, go off and bite,
          attack or show paralyzing aggression.
        </p>
        <p>
          Even if an actual physical confrontation or personal injury does not
          happen, the overall experience of a large and majestic dog, like the
          Doberman Pinscher, can leave a confident individual trembling.
        </p>
        <p>
          Victims of bites and near attacks can suffer from PTSD for weeks,
          months, years and in serious cases for the rest of their lives.
        </p>
        <p>
          Here are <strong>5 steps</strong> that can help prevent or stop an
          attack or dog bite from continuing or happening in the first place:
        </p>
        <ol>
          <li>
            <strong>Don't let the situation happen:</strong> Self-awareness and
            paying attention to your surroundings is very important in all
            situations and environments. Often times, people distracted by their
            cell phones are unaware of the world around them. Coming face to
            face with a dog that is ready to attack can be very scary and
            dangerous.
          </li>
          <li>
            <strong>Keep your distance:</strong> If possible, stay as far away
            from stray dogs, on or off their leash. If a dog is running around,
            keep clear of any possible way you can come into contact with it. If
            the dog suddenly sees you and shows an alertness and even begins to
            come after you, jump on top of a car, fence, wall, tree or high
            place that can put distance between you and the dog.
          </li>
          <li>
            <strong>Defending yourself:</strong> If you weren't able to avoid or
            put distance between you and the attacking dog and are now in a
            physical altercation, it is important to defend your neck and face;
            these are the areas the dog will attack first, next to hands, arms
            and legs. Being loud, holding something above your head to appear
            larger and taller can possibly deter the dog from attacking you.
          </li>
          <li>
            <strong>A stronger line of defense:</strong> Fighting back at some
            point may be your only option. At some point, the dog may have a
            solid grip on you which increases their confidence in taking down
            their prey - you. This is where it can become very dangerous and
            potentially deadly. Pepper spray, weapons of any sort, hard items
            you can hit the dog with - aim for the head and legs/joints to throw
            the dog off balance and hopefully injure it temporarily to offer you
            a chance for escape.
          </li>
          <li>
            <strong>Doing whatever it takes to survive</strong>: If you are
            injured and the dog is showing no signs of stopping, if playing dead
            does not work, it's time to do anything you can to stop the dog.
          </li>
        </ol>
        <p>
          <strong>Remember: </strong>Never stare into a dogs eyes, turn your
          back, run away or antagonize a dog or any animal, especially one you
          are unfamiliar with.
        </p>

        <center></center>

        <h2 id="header9">Medical Attention After a Doberman Dog Bite</h2>
        <p>
          Experiencing a dog bite can be dangerous, painful and leave you with
          high medical expenses, pain and suffering, and even permanent
          disabilities which can change your life, alter your abilities to make
          a living, and care for your family.
        </p>
        <p>
          The smartest thing to do immediately after a dog bite or dog attack is
          to seek medical attention. Dog bites can be very severe, needing
          stitched, staples, skin graphs, surgery and more. Not to mention that
          all animals have the ability to be carrying diseases, like{" "}
          <Link
            to="https://www.cdc.gov/rabies/transmission/virus.html"
            target="new"
          >
            rabies
          </Link>
          .
        </p>
        <p>
          Rabies is a disease that must be treated frequently over a long period
          of time. You can get rabies from any animal or person that carries the
          virus, but animals that are at a high-risk for carrying the virus are:
        </p>
        <ul>
          <li>Raccoons</li>
          <li>Skunks</li>
          <li>Dogs</li>
          <li>Cats</li>
          <li>Squirrels</li>
          <li>Rats</li>
          <li>Possums</li>
          <li>Coyotes</li>
          <li>Rabbits</li>
        </ul>
        <p>
          According the the{" "}
          <Link to="https://www.cdc.gov/rabies/index.html">
            Centers for Disease Control and Prevention (CDC)
          </Link>
          , Rabies is usually transmitted through getting bitten by an animal
          that carries the rabies virus. It is a preventable viral disease,
          showing the importance of seeking medical attention immediately.
        </p>
        <p>
          Dog bites must always be sterilized and cared for properly, otherwise
          the patient may have a high risk of infection, loss of limbs or body
          parts, and even death.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/medical-care-after-dog-bite.jpg"
            width="100%"
            alt="medical care after dog bite"
          />
        </LazyLoad>

        <h2 id="header10">
          Documenting Your Doberman Pinscher Dog Bite Injuries
        </h2>
        <p>
          Medical attention is important to receive immediately following a dog
          attack or dog bite for health and safety reasons, but did you know it
          is also a great way to have your dog bite injuries associated with the
          incident itself?
        </p>
        <p>
          Doing so documents your injuries in direct correlation with the dog
          bite incident, providing legal documentation for your dog bite case,
          proving that the injuries you received were in fact a result of the
          dog attack situation.
        </p>
        <p>
          Not having your injuries documented can create a major dilemma for
          your case and can often result in a lack of necessary evidence to
          pursue a claim against the dog owner and their home insurance.
        </p>
        <p>
          Don't make a mistake that can cost you in the long run. Take the time
          to properly identify your injuries from the incident, and document
          those injuries.
        </p>

        <h2 id="header11">What Your Case Needs to Win a Dog Bite Case</h2>
        <p>
          Dog bites can be traumatic and an overall horrible experience. Many
          people have on-going issues and experience PTSD and other mental
          problems after a severe attack. You don't have to be a victim. With
          the right preparation and tools, compensation can be yours to help pay
          for medical bills, pain and suffering, and other inconveniences. The
          best dog bite case will check off the items on the following list:
        </p>
        <ul>
          <li>Medical attention/injuries documented</li>
          <li>Police report</li>
          <li>
            Photos/video (where the incident took place, injuries, those
            involved, etc.)
          </li>
          <li>Evidence</li>
          <li>
            Eye-witnesses, their contact information and written statement
          </li>
          <li>
            Be organized and prepared: have a statement identifying the date,
            time, location, those involved, summary of what happened and any
            other information your lawyer identifies is an important piece of
            information for your case
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="dog bite lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-lawyers-dog-bite-attorneys.jpg')"
            }}
          >
            <h2 id="header12">
              Let Bisnar Chase Represent You and Win, or You Do Not Pay
            </h2>
          </div>
        </LazyLoad>

        <p>
          If you have experienced an attack by a dog or specifically a Doberman
          Pinscher, seek medical attention promptly and follow up with a skilled
          and highly experienced Dog Bite Lawyer, like our team who has won more
          than <strong>$500 Million</strong>, here at{" "}
          <strong>Bisnar Chase</strong>.
        </p>
        <p>
          Our Dog Bite Attorneys have established a{" "}
          <strong>96% success rate</strong> and have helped all types of dog
          bite victims get their life back and receive maximum compensation.
        </p>
        <p>
          Bisnar Chase has over <strong>40 years of experience</strong> and
          offers the best legal representation you can find. We will make sure
          your financial obligations are taken care of and that you receive a
          winning settlement with maximum compensation, or you do not pay.
        </p>

        <center>
          <p>
            <strong>
              To speak with a lawyer and receive your Free consultation and case
              evaluation, call 800-561-4887.
            </strong>
          </p>
        </center>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
