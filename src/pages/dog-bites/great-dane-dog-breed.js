// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"
import { HorizontalBar } from "react-chartjs-2"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "dog-bites/great-dane-bite.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  const chartData = {
    labels: ["Great Dane", "Mastiff", "Doberman", "Cane Corso", "Bulldog"],
    datasets: [
      {
        label: "Attacks doing bodily harm",
        backgroundColor: "#004cff",
        data: [37, 23, 23, 21, 20]
      },
      {
        label: "Maimings",
        backgroundColor: "#ff8c00",
        data: [19, 17, 12, 12, 14]
      },
      {
        label: "Deaths",
        backgroundColor: "#ccc2b7",
        data: [3, 5, 8, 2, 1]
      }
    ]
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Great Dane Bites - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="Great Danes are huge and powerful dogs, and a Great Dane bite can leave severe wounds. Contact the dog bite lawyers at Bisnar Chase for expert help with your case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Great Dane Dog Breed</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Great Dane bite"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Famous for being the largest breed in the world, Great Danes are known
          as majestic dogs with imposing size. They are generally considered to
          be gentle giants and good-natured family pets. But, as with all
          breeds, Great Danes can become aggressive. They are extremely
          territorial dogs and can attack with little warning. A{" "}
          <b>Great Dane bite</b> can have a serious and life-altering impact on
          victims.
        </p>

        <p>
          If a Great Dane does display aggression or biting behavior, it becomes
          a significant danger to everyone around it. Due to its daunting size,
          weight, and power, an aggressive Great Dane becomes almost impossible
          to control. This makes Great Dane attacks a very real danger.
        </p>

        <p>
          Find out everything you need to know about the Great Dane dog breed
          here, including its history, characteristics, temperament, and
          dangerous traits. More importantly, find out how to survive an attack
          from a giant Great Dane, what to do after being bitten, and how the{" "}
          <Link to="/dog-bites" target="new">
            dog bite attorneys
          </Link>{" "}
          of Bisnar Chase can help you.
        </p>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/dog-bites/great-dane-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Reputation of Great Danes{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of the Great Dane{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Great Dane Characteristics{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Temperament of Great Danes{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Great Dane: Largest Dog World Record Holder{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Great Danes in Pop Culture{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Causes a Great Dane to Bite?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Great Dane Health Problems{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Great Dane Bite Attacks{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How to Survive a Great Dane Attack{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Should You Do After Being Bitten by a Great Dane?
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Who is Liable for a Great Dane Bite in Cali fornia?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/great-dane-dog-breed#header13">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Outstanding Dog Bite Representation{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">Reputation of Great Danes</h2>

        <p>
          Great Danes are not widely considered to be dangerous. These dogs are
          referred to by some as the ‘Apollo of Dogs’ – a reference to the Greek
          God Apollo – due to its giant dimensions. Despite this colossal size,
          Great Danes are thought of as gentle and friendly dogs. This is often
          the case, but Great Danes can also be extremely dangerous, depending
          on the individual dog and the situation they are in.
        </p>

        <h2 id="header2">History of the Great Dane</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/great-dane-attack.jpg"
            width="50%"
            className="imgleft-fluid"
            alt="Great Dane attack"
          />
        </LazyLoad>

        <p>
          Don’t be fooled by the name – Great Danes are of German descent and do
          not have any actual links to Denmark, according to canine historians.
        </p>

        <p>
          The breed is believed to be upward of 400 years old, but its precise
          history is not well known. Sharing heritage with breeds such as the
          English Mastiff and Irish Greyhound, Great Danes were bred to be agile
          and powerful.
        </p>

        <p>
          In around 1680, early Great Dane-like dogs were bred by noble German
          households as hunting companions, and originally named Boar Hounds.
          These dogs were extremely large and powerful, and could take down a
          wild boar at the instruction of its owner. Such was the size and
          strength of these dogs that they were able to help catch larger game.
          Some early Great Danes were also kept inside the home, valued for
          their handsome looks and guard dog capabilities.
        </p>

        <p>
          Today’s Great Dane was developed from the early hunting hound and
          still possesses its regal look and grand size.
        </p>

        <h3 id="header3">Origins of the Great Dane Name</h3>

        <p>
          From the 1880s onward, this breed was known as Deutsche Dogge in
          Germany – which translates to ‘German Dog’. This name did not take off
          throughout the rest of the world though, and it is widely known as the
          Great Dane in English-speaking countries such as the United States,
          where it has become one of the most popular breeds.
        </p>

        <p>
          Mystery surrounds the origin of the Great Dane name itself. There are
          not many accounts documenting the adoption of this title, but some
          believe that it originated from a naturalist named{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Georges-Louis_Leclerc,_Comte_de_Buffon"
            target="new"
          >
            {" "}
            Georges-Louis Leclerc, Comte de Buffon
          </Link>
          . Legend has it that the famed Frenchman saw dogs with a similar look
          to the German hounds while travelling in Denmark in the 1700s. He
          supposedly gave the Great Dane name to the German breed and it stuck –
          though it is unclear how this happened.
        </p>

        <h2 id="header3">Great Dane Characteristics</h2>

        <ul>
          <li>
            <b>Height:</b> <i>Males</i> – 30-32 inches / <i>Females</i> – 28-30
            inches
          </li>
          <li>
            <b>Weight:</b> <i>Males</i> – 140-180 lbs / <i>Females</i> – 110-150
            lbs
          </li>
          <li>
            <b>Color:</b> Brindle, fawn, blue, black, harlequin, mantle
          </li>
          <li>
            <b>Coat:</b> Short, thick and glossy
          </li>
          <li>
            <b>Ears:</b> Naturally floppy, cropping optional
          </li>
          <li>
            <b>Life Span:</b> 7-10 years
          </li>
        </ul>

        <p>
          The chief characteristic of a Great Dane is its enormous size. Dog
          height is measured up to the shoulders, and a male Great Dane will
          typically be greater than 30 inches in height, towering above other
          large breeds like German Shepherds and Dobermans by several inches.
          When standing up on their hind legs, they are taller than most humans.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/great-dane-aggression.jpg"
            width="40%"
            className="imgright-fluid"
            alt="Great Dane aggression"
          />
        </LazyLoad>

        <p>
          This great size is not just restricted to height – Great Danes also
          carry substantial weight, with the average weight of a male dog
          reaching around 180 lbs. These combined factors mean that an
          <b> aggressive Great Dane</b> will be extremely difficult to control,
          even for a responsible owner.
        </p>

        <p>
          Great Danes have short and glossy coats which are usually a variation
          of one of six main colors. These range from a golden fawn color to a
          shade of gray known as ‘blue’. A Great Dane’s ears are also notable.
          They naturally grow to fold over, but in the United States they are
          sometimes cropped so that they stand upright. Ear cropping is banned
          in some countries, but is unregulated is most parts of the U.S.
        </p>

        <p>
          According to the{" "}
          <Link
            to="https://www.akc.org/most-popular-breeds/2017-full-list/"
            target="new"
          >
            American Kennel Club
          </Link>
          , Great Danes are the 14th most popular breed in the United States.
          Every year the authority compiles a list of the most-owned breeds,
          using registration figures from across the country. Great Danes have
          been rising in popularity in recent years and are now at their highest
          ever rank.
        </p>

        <h2 id="header4">Temperament of Great Danes</h2>

        <p>
          These are some of the key behavioral characteristics of the Great Dane
          breed:
        </p>

        <ul>
          <li>Usually good-natured</li>
          <li>Territorial</li>
          <li>Protective</li>
          <li>Laid-back</li>
          <li>Intelligent</li>
        </ul>

        <p>
          Great Danes have developed significantly from their roots as wild
          animal-subduing hunting dogs. While their colossal size still
          intimidates many people, the breed is generally thought of as
          good-natured and friendly. They are relatively relaxed and do not
          require the high level of activity that some other breeds do.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/great-dane-mauling.jpg"
            width="45%"
            className="imgleft-fluid"
            alt="Great Dane mauling"
          />
        </LazyLoad>

        <p>
          However, while Great Danes are usually friendly, they can also be
          highly territorial and extremely protective. If its space, home, or
          family is threatened, a Great Dane may become aggressive. One of the
          main causes of a <b>Great Dane biting</b> will be the dog perceiving a
          threat or danger from strangers.
        </p>

        <p>
          It is also important to remember that in some ways, dogs are like
          people – they are individuals, with different personalities. While
          Great Danes are not known to be especially violent, any breed can
          display aggressive tendencies. When <b>Great Danes attack</b>, they
          are more dangerous than many other dogs due to their size and raw
          power. These factors can make a Great Dane attack deadly.
        </p>

        <p>
          Would-be owners also need to be aware of the practicalities of having
          a Great Dane. They can knock people and furniture over unintentionally
          with their tails, and may be especially clumsy in their younger years.
          While Great Danes are not the worst choice for rookie owners, people
          do need to be aware that such a large dog can frighten strangers, and
          the dog can do serious damage if not properly trained and controlled.
        </p>

        <div>
          <table>
            <tr>
              <th>Great Dane Characteristics</th>
              <th>Rating</th>
            </tr>

            <tr>
              <td>Size</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Power</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Intelligence</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Suitability for Inexperienced Owners</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Energy</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Territorial</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Protective</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Playfulness</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Tolerance of Other Animals</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Health</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>
          </table>
        </div>

        <h2 id="header5">Great Dane: Largest Dog World Record Holder</h2>

        <p>
          Great Danes are the biggest dogs in the world – and that’s official!
          If you check the Guinness World Records, a towering Great Dane named
          Zeus is the proud holder of the tallest dog title.
        </p>

        <p>
          Zeus, who was owned by a family in Michigan, stormed into the record
          books in 2011 when he was measured at 44 inches to the top of his
          shoulders. That makes him the tallest dog of any breed ever recorded.
          When standing on his hind legs, he measured in at a whopping 7 ft 4
          ins.
        </p>

        <p>
          Zeus’s owner said he was a gentle giant, and had to be measured by a
          vet because he was scared of the tape measure. As with many larger
          dogs, Zeus’s life was short – he{" "}
          <Link
            to="http://www.guinnessworldrecords.com/news/2014/9/zeus-the-worlds-tallest-dog-passes-away-at-the-age-of-5-60340"
            target="new"
          >
            died in 2014
          </Link>
          , just short of turning six years old.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/wJwnsKDrtwE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          Before Zeus took the crown, the previous record setter was another
          Great Dane named Giant George, who was 43 inches at the shoulder and
          appeared on The Oprah Winfrey Show in 2010.
        </p>

        <h2 id="header6">Great Danes in Pop Culture</h2>

        <p>
          There are several examples of Great Danes being used in pop culture,
          from TV and movie appearances to comics and cartoons. Most of these
          appearances show a comedic look at the breed, minimizing the potential
          danger of Great Danes. Here are just some of the most famous examples
          of Great Danes in pop culture:
        </p>

        <p>
          <b>Marmaduke</b>
        </p>

        <p>
          The long-running newspaper cartoon strip features the Winslow family
          and revolves around the antics of their whacky Great Dane, named
          Marmaduke. It was created and written by illustrator Brad Anderson and
          was launched in 1954. Anderson was involved in writing and
          illustrating the strip – which was published in a huge range of
          newspapers – right up until his death in 2015. The strip was adapted
          into a live-action movie in 2010, with Owen Wilson voicing Marmaduke.
          The movie's star was played by a real Great Dane and digitally altered
          to make his mouth move.
        </p>

        {/* <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/NerHJqENCvs"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad> */}

        <p>
          <b>Harry Potter – Fang</b>
        </p>

        <p>
          The Harry Potter franchise is filled with magical creatures. But one
          animal more rooted in reality is Fang – a huge Great Dane (referred to
          as a Boar Hound) owned by Hagrid, the groundskeeper at Hogwarts School
          of Witchcraft and Wizardry. While Fang is a Great Dane in the Harry
          Potter books, he is portrayed by Neapolitan Mastiffs in the movies.
        </p>

        <p>
          <b>Scooby Doo</b>
        </p>

        <p>
          The long-running Scooby Doo franchise started life as an animated
          series in the 1960s, and has run in several forms. It is based on the
          title character – a cowardly Great Dane who joins a group of friends
          in solving mysteries.
        </p>

        <p>
          The Scooby Doo character was designed by Iwao Takamoto at animation
          company Hanna-Barbera. He consulted with a Great Dane breeder to
          purposefully give Scooby features out of keeping with purebred Danes,
          such as a sloping back and chin, as well as bowed legs.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/776rvGHfzuM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <h2 id="header7">What Causes a Great Dane to Bite?</h2>

        <p>
          There are several factors that might cause a Great Dane to bite
          someone. Here are some of the key causes:
        </p>

        <h3>Territorial Aggression in Great Danes</h3>
        <p>
          Great Danes are extremely territorial. This means that they are highly
          protective of their own property. If a person or animal that the dog
          does not recognize threatens their home or space, a Great Dane is
          likely to attack. While Great Danes are often laid-back and relaxed,
          they can possess a high prey drive and sometimes become worked up.
        </p>

        <p>
          <b>
            Even social and friendly Great Danes can be prone to barking,
            growling, biting, and showing other aggressive behavior when their
            space is challenged.
          </b>
        </p>

        <p>
          Many Great Dane owners report that the most aggressive behavior
          exhibited by their dog comes when a stranger approaches their home. In
          contrast, the dogs are often far calmer when meeting strangers and
          other dogs in neutral locations.
        </p>

        <h3>Great Dane Pain</h3>
        <p>
          Great Danes are prone to certain health issues which can leave them in
          significant pain if they go untreated. A dog being in pain can result
          in it lashing out in violent outbursts, even if this behavior is
          uncharacteristic. For more details on the health issues of Great
          Danes, read on to our next section.
        </p>

        <h3>Mistreatment Results in Aggressive Behavior</h3>
        <p>
          Another potential cause of aggression and behavioral problems in Great
          Danes is mistreatment by humans. Dogs form a bond with humans, and
          when they are abused, mistreated or abandoned, it can result in the
          animal displaying unpredictable behavior around future owners and
          strangers. This can include a <b>Great Dane biting or attacking</b>{" "}
          with little warning, due to its fear and mistrust of people.
        </p>

        <h2 id="header8">Great Dane Health Problems</h2>

        <p>
          Great Danes do not have particularly long lifespans, usually ranging
          from as little as six years to a maximum of around 10 years of age.
          Like many larger breeds, it is common for Danes to develop serious
          health issues as they get older.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/great-dane-temperament.jpg"
            width="70%"
            className="imgcenter-fluid"
            alt="Great Dane temperament"
          />
        </LazyLoad>

        <p>
          Owners are advised not to over-exercise young Great Danes, because
          their rapid growth can result in problems with their bones and joints.
          Great Danes also carry a high risk of developing a condition called
          hip dysplasia, which is a hip socket problem which can cause arthritis
          pain and even lameness in severe cases. Studies have shown that hip
          dysplasia is a significant source of aggression in dogs.
        </p>

        <p>
          Researchers at the Autonomous University of Barcelona created a study
          testing a cross section of dogs, examining the circumstances of
          violent dogs and evaluating the root cause of their aggression.
          Published in the Journal of Veterinary Behavior in March 2012, the
          study revealed that most of the aggressive canine subjects were
          experiencing pain, and this was causing their violent outbursts and
          bite attacks. Hip dysplasia was the most common source of pain found,
          showing that Great Danes have a high risk of displaying pain-induced
          aggression.
        </p>

        <p>
          Great Danes also possess a high risk of genetic heart conditions, and
          can carry a gene which causes blindness, deafness, and other health
          conditions.
        </p>

        <h2 id="header9">Great Dane Bite Attacks</h2>

        <p>
          While attacks by Great Danes are not as common as some other breeds,
          they can and do happen. Due to the size and power of the breed, a
          Great Dane bite attack can result in serious wounds, major injuries,
          or worse.
        </p>

        <p>
          Accurate data on dog bites according to breed is rare. However, one{" "}
          <Link
            to="https://www.dogsbite.org/pdf/dog-attack-deaths-maimings-merritt-clifton-2014.pdf"
            target="new"
          >
            comprehensive study
          </Link>{" "}
          was carried out by Merritt Clifton – the editor of a publication named
          Animals 24-7. This study recorded dog attacks broken down by breed,
          using media reports as the primary source of information and covering
          a huge time period between 1982 and 2014.
        </p>

        <p>
          Great Danes sit just outside the top-10 most dangerous dogs according
          to the results, ending up in 11th place. Over the study period, the
          breed was responsible for 37 reported attacks. Three of these attacks
          resulted in deaths, while another 19 involved maimed victims. The
          majority of Great Dane attack victims were children.
        </p>

        <LazyLoad>
          <HorizontalBar data={chartData} />
        </LazyLoad>

        <center>
          <blockquote>Documented Dog Attacks by Breed: 1982 - 2014</blockquote>
        </center>
        <p>
          <strong>
            {" "}
            <Link to="http://ht.ly/Yj8s30mLQZc" target="new">
              Dog Bite Statistics in the U.S (Infographic)
            </Link>
            .
          </strong>{" "}
          Download or share our dangerous breeds dog bite info-graphic to
          educate others on the dangers of approaching a dangerous dog.
        </p>

        <h3>Great Dane Attack Case Studies</h3>

        <p>
          <b>Young Boy Mauled by Great Dane - March 2018 – Australia</b>
        </p>

        <p>
          A three-year-old boy suffered serious injuries outside his home after
          being mauled by a Great Dane in a vicious attack. Tom Higgins was on
          his bike in his own yard when a neighbor’s Great Dane burst through an
          open gate.
        </p>

        <p>
          The large dog mauled the boy and left him with serious injuries to his
          neck, back and face. Tom’s ear also had to be re-attached during
          emergency surgery.
        </p>

        <p>
          The owner said that the dog disliked bikes, potentially causing it to
          attack the cycling youngster. The incident shows that dogs can have
          individual personality quirks which can lead them to violence –
          regardless of the generalized character traits of a breed.
        </p>

        <p>
          <b>Poodle Killed by Great Dane – November 2018 – Colorado, U.S.</b>
        </p>

        <p>
          A miniature poodle was killed by a Great Dane while walking in a
          Colorado park. The poodle, named Nick, was being walked by its owner,
          when they crossed paths with another owner walking a Great Dane. As
          they passed them, the Dane lunged at the much smaller dog, biting it
          and shaking it.
        </p>

        <p>
          The severity of the injuries caused meant Nick had to be put down. His
          owner sued the Great Dane’s owner, who was ordered to pay thousands of
          dollars in compensation.
        </p>

        <h2 id="header10">How to Survive a Great Dane Attack</h2>

        <p>
          Being attacked by a dog can be a terrifying and traumatic ordeal. A
          Great Dane bite can be deadly – so take these steps to make sure you
          live to tell the tale.
        </p>

        <ol>
          <li>
            <b>
              Stay away from unknown Great Danes and aggressive-looking dogs.
            </b>{" "}
            Great Danes are most dangerous when a stranger is approaching their
            territory, but can also attack in neutral locations. If a dog is
            barking, growling, or looks hostile – steer clear.
          </li>

          <li>
            <b>Do not engage with an aggressive dog.</b> If you are approached
            by an aggressive Great Dane, stay as still as possible. Do not run,
            wave your arms, try to frighten it with loud noises, or make eye
            contact. The dog will often lose interest.
          </li>

          <li>
            <b>Protect yourself using anything at hand.</b> If the Great Dane
            attacks you, try to use anything you can get your hands on as a
            buffer. Items such as clothing and bags can be used to deflect bite
            attacks and ensure the dog cannot clamp its teeth into you.
          </li>

          <li>
            <b>Avoid being knocked down.</b> Large and powerful Great Danes can
            easily knock adults off their feet. Violent dogs are most dangerous
            when a victim is defenseless on the ground, so it is important to
            stay on your feet at all costs.
          </li>

          <li>
            <b>Fight back and get to safety.</b> It is difficult to fight back
            against a Great Dane, due to its imposing size. If you are the
            victim of a sustained attack and are forced to do so, aim for
            sensitive areas. This should be a last resort, as hitting or kicking
            a violent dog can make an attack more ferocious. You must then get
            to safety. This means putting a solid door or wall between yourself
            and the dog, or finding high ground that it cannot reach.
          </li>
        </ol>

        <h2 id="header11">
          What Should You Do After Being Bitten by a Great Dane?
        </h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/aggressive-great-dane.jpg"
            width="35%"
            className="imgright-fluid"
            alt="aggressive Great Dane"
          />
        </LazyLoad>

        <p>
          The last thing most people are thinking about after being bitten by a
          dog is taking legal action. Being the victim of a dog attack is
          frightening and can leave people with serious and lasting injuries.
          These can include:
        </p>

        <ul>
          <li>Puncture wounds</li>
          <li>Torn flesh</li>
          <li>Infections</li>
          <li>Damaged muscles, ligaments and tendons</li>
          <li>Psychological scarring</li>
        </ul>

        <p>
          As such, the first step you need to take if you have a dog bite injury
          is to seek medical attention and contact the police. The severity of
          your injuries will dictate whether you need emergency treatment, or
          need to visit your doctor on a non-emergency basis.
        </p>

        <p>
          Once you are out of danger and your injuries have been tended to, you
          can consider the next step. For many people, this is to take legal
          action.
        </p>

        <p>
          Dog bites are very serious and can result in extreme pain and
          suffering. While it can be a difficult decision to sue after an owner
          after being bitten by a Great Dane, it is the responsible action to
          take and can result in compensation toward medical expenses, as well
          as other costs.
        </p>

        <div className="great-dane-border">
          <h2 id="header12">Who is Liable for a Great Dane Bite?</h2>

          <p>
            In the state of California, dog owners are solely responsible for
            the actions of their pets. Owners of Great Danes need to be aware of
            the temperament and power of their pets to ensure the safety of
            those around them. If they are careless, complacent, or negligent,
            it could result in a serious injury or a fatality.
          </p>

          <p>
            Read more about the dog bite laws in California{" "}
            <Link to="/dog-bites/california-dog-bite-laws" target="new">
              here
            </Link>
            .
          </p>
        </div>

        <p>
          If you are a dog bite victim and decide to take legal action, these
          are the steps you should take and the evidence you should gather:
        </p>

        <ul>
          <li>
            Provide photographs or video footage of the attack if possible.
          </li>
          <li>Take comprehensive pictures of your injuries.</li>
          <li>
            Document the scene of the incident and write a detailed account of
            the attack.
          </li>
          <li>
            Request copies of the police report from the incident, as well as
            your medical reports relating to any bite injuries.
          </li>
          <li>
            Secure witness accounts and contact details for anyone who saw the
            incident.
          </li>
        </ul>

        <p>
          Make sure this information is well organized, and contact the expert
          dog bite attorneys at Bisnar Chase for help.
        </p>

        <div
          className="text-header content-well"
          title="Dog Bite Attorneys"
          style={{
            backgroundImage:
              "url('/images/dog-bites/great-dane-dog-bite-attorneys.jpg')"
          }}
        >
          <h2 id="header13">Outstanding Dog Bite Representation</h2>
        </div>
        <p>
          Bisnar Chase is ready to fight for the compensation you deserve in
          your <b>dog bite case</b>. While every incident is different, our dog
          bite lawyers have a tremendous track record of success when it comes
          to canine cases.
        </p>

        <p>
          Our law firm has been in business for more than 40 years, building up
          an impressive 96% success rate and winning more than $500 million for
          our clients over that span.
        </p>

        <p>
          We take pride in going above and beyond for our clients. We know how
          traumatic a <b>dog attack</b> can be, and we want to make sure our
          clients are properly compensated. Even better, our ‘No Win, No Fee’
          guarantee makes our expert services available to everyone. Contact us
          now on <b>(800) 561-4887</b>, and let us fight for you!
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
