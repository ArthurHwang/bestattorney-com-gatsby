// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"
import { HorizontalBar } from "react-chartjs-2"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/alaskan-malamute-bites.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  const chartData = {
    labels: [
      "Malamute",
      "Chow",
      "Saint Bernard",
      "Husky",
      "Great Dane",
      "Rottweiler",
      "Doberman",
      "Mastiff",
      "Pitbull type",
      "Akita"
    ],
    datasets: [
      {
        label: "Dog Bite Fatalities per 100,000 Dogs",
        backgroundColor: "#004cff",
        data: [6.79, 2.32, 2.05, 1.73, 1.18, 1.17, 1.16, 1.15, 0.97, 0.8]
      }
    ],

    borderColor: "#fff"
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Alaskan Malamute Bites - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="An Alaskan malamute bite can be deadly. Victims should contact the dog bite injury attorneys of Bisnar Chase for expert help - 96% success rate, $500m won."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Alaskan Malamute Dog Breed</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Alaskan malamute bites"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Alaskan malamutes are best known as large, wolf-like dogs, which are
          most at home in Arctic conditions. This is a strong breed, built for
          power and endurance. Yet many people do not think of the Alaskan
          malamute as a{" "}
          <Link to="/dog-bites/dangerous-dog-breeds" target="new">
            dangerous dog breed
          </Link>
          . This can be a dangerous misconception though. These are large and
          powerful animals, and the evidence shows that an Alaskan malamute bite
          can be deadly.
        </p>

        <p>
          Are you looking for the{" "}
          <Link to="/dog-bites" target="new">
            best Alaskan malamute dog bite injury lawyer
          </Link>
          ? The skilled attorneys of Bisnar Chase can help. If you have been
          bitten by a dog, we want to make sure you get the compensation you
          deserve.
        </p>
        <p>
          If you are a dog bite victim, want information on the Alaskan malamute
          breed, or want to learn how to pursue a dog bite lawsuit case, read
          on. We provide a comprehensive profile of the Alaskan malamute, a
          rundown of the dog bite laws in California, and much more. We know
          that any dog attack can be traumatic, and are dedicated to helping
          victims find justice.
        </p>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of the Alaskan Malamute{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Alaskan Malamutes vs Huskies{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Characteristics of Alaskan Malamutes{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Alaskan Malamute Temperament{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Do Alaskan Malamutes Have Aggression Problems?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Malamute Attacks on Humans: Case Studies{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning an Alaskan Malamute{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How to Stop a Malamute Puppy from Biting{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What are the Different Types of Dog Bite?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What are the Dog Bite Laws in California?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Contacting an Alaskan Malamute Dog Bite Injury Attorney{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/alaskan-malamute-dog-breed#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Superior Client Representation with Bisnar Chase{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">History of the Alaskan Malamute</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/alaskan-malamute-attack.jpg"
            width="40%"
            className="imgright-fluid"
            alt="Alaskan malamute attack"
          />
        </LazyLoad>

        <p>
          The Alaskan malamute is one of the oldest breeds of dog in the world.
          These working dogs were originally bred by the native{" "}
          <div className="tooltip">
            Iñupiat people in Alaska
            <span className="tooltiptext">
              Iñupiat: A native Alaskan people who can be traced back more than
              1,000 years. They have a population of about 20,000.
            </span>
          </div>{" "}
          A tribe within the Iñupiat people called Mahlemuts were responsible
          for developing and raising the dogs, which is where the malamute name
          comes from.
        </p>

        <p>
          Alaskan malamutes were bred as all-purpose dogs, performing a wide
          range of vital functions in the freezing Arctic conditions. This breed
          was one of the earliest forms of sled dogs. They were also used in
          hunting missions, tracking and killing larger targets. Early malamutes
          were highly-valued companions to their human masters, who depended on
          the dogs to survive in the harsh and icy conditions.
        </p>

        <p>
          The{" "}
          <div className="tooltip">
            Klondike Gold Rush
            <span className="tooltiptext">
              Klondike Gold Rush: More than 100,000 gold prospectors attempted
              to travel to Canada’s Klondike region in a bid to mine for gold
              between 1896 and 1899.
            </span>
          </div>{" "}
          of 1896 saw the Alaskan malamute breed shoot to greater prominence.
          Malamutes were used to help gold prospectors, most of whom were new to
          the Antarctic regions, transport their gear and supplies by sled. This
          newfound fame helped the breed to spread across America. However, it
          wasn’t until 1935 that the Alaskan malamute was officially recognized
          by the American Kennel Club (AKC).
        </p>

        <p>
          Malamutes were also used as search and rescue dogs during World War
          II, but are now mostly owned as domestic pets. They remain most
          prominent in Alaska though, where the malamute was named the official
          state dog in 2010.
        </p>

        <h2 id="header2">Alaskan Malamutes vs Huskies</h2>

        <p>
          What is the difference between an Alaskan malamute and a Siberian
          husky? Many people cannot tell the two breeds apart at first glance,
          but there are a few subtle differences.
        </p>

        <p>
          Malamutes and huskies share a similar look. They were both bred to be
          sled dogs, capable of pulling heavy loads in extreme conditions. They
          also have similar coloring, as well as endless energy levels, and
          thick coats and tails that help them survive in the freezing cold. A
          malamute’s coat will often be a little longer than a husky’s, with a
          fluffier look.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/alaskan-malamute-vs-husky.jpg"
            width="100%"
            alt="A husky (left), and an Alaskan malamute (right)"
          />
        </LazyLoad>

        <p>
          While both are sled dogs, huskies are leaner, and a little more
          athletic. They are built for speed. In contrast, Alaskan malamutes are
          larger dogs. They stand slightly taller and carry considerably more
          weight. The typical weight of a malamute ranges up to about 95 lbs
          (sometimes even larger), compared to 60 lbs for a husky. Alaskan
          malamutes are about power and endurance, rather than speed. They are
          capable of pulling extremely heavy weights, and have incredible
          stamina.
        </p>

        <p>
          Of the two breeds, huskies are far more widely-owned in the United
          States. In the annual ownership rankings compiled by the AKC, Siberian
          huskies are the 12th most popular breed. Alaskan malamutes are{" "}
          <Link
            to="https://www.akc.org/most-popular-breeds/2017-full-list/"
            target="new"
          >
            sitting at number 59
          </Link>{" "}
          on the list (as of 2017).
        </p>

        <h2 id="header3">Characteristics of Alaskan Malamutes</h2>

        <ul>
          <li>
            <b>Height:</b> <i>Males</i> – 24-26 inches / <i>Females</i> – 22-24
            inches
          </li>
          <li>
            <b>Weight:</b> <i>Males</i> – 80-95 lbs / <i>Females</i> – 70-85 lbs
          </li>
          <li>
            <b>Color:</b> Typically gray/black/white blend. Other variants
            possible.
          </li>
          <li>
            <b>Coat:</b> Dense double coat
          </li>
          <li>
            <b>Eyes:</b> Usually brown, almond-shaped
          </li>
          <li>
            <b>Ears:</b> Triangular and alert
          </li>
          <li>
            <b>Tail:</b> Heavy, curled
          </li>
          <li>
            <b>Life Span:</b> 10-14 years
          </li>
        </ul>

        <p>
          Alaskan malamutes are large and powerful dogs, with a size akin to dog
          breeds such as German Shepherds. In terms of physical characteristics,
          they often look wolf-like. They have broad heads and bulky muzzles,
          with powerful and muscular bodies that allow them to pull incredibly
          heavy sled loads.
        </p>

        <p>
          The malamute’s fur often features a blend of black, white and gray.
          They can also have alternative coloring, including various shades of
          red and sable. Its thick double coat of fur is designed to keep the
          malamute warm in freezing conditions. It will also result in heavy
          shedding for domestic owners.
        </p>

        <p>
          This dog’s tail is especially thick and can be used as a tool for
          extra warmth. When a malamute is happy, its tail will usually be
          curled up over its back. This can be misconstrued as a sign of
          overconfidence or aggression by other dogs, leading to potential
          altercations. If an <b>aggressive Alaskan malamute</b> is considering
          an attack, its tail will usually stick out with a stiff wagging
          motion.
        </p>

        <h2 id="header4">Alaskan Malamute Temperament</h2>

        <p>
          Alaskan malamutes are complex creatures, with a wide range of
          behavioral characteristics and variable temperaments. Here are some of
          the key behavioral elements of the malamute:
        </p>

        <ul>
          <li>Intelligent</li>
          <li>High-energy</li>
          <li>Easily bored</li>
          <li>Playful</li>
          <li>Stubborn</li>
          <li>High prey drive</li>
          <li>Heavy digger</li>
          <li>Loyal to pack/family</li>
        </ul>

        <LazyLoad>
          <img
            src="/images/dog-bites/aggressive-alaskan-malamute.jpg"
            width="40%"
            className="imgright-fluid"
            alt="aggressive Alaskan malamute"
          />
        </LazyLoad>

        <p>
          This breed is highly intelligent and trainable – but novice dog owners
          should be wary of owning a malamute. They are known for being stubborn
          and challenging their owner. A strong-willed malamute needs a firm and
          confident master to train it properly.
        </p>

        <p>
          Without proper training, an Alaskan malamute might become prone to
          biting and other aggressive behavior. They also traditionally have a
          high level of mental toughness. This trait has always been vital to
          the breed’s ability to survive in harsh and freezing conditions.
        </p>

        <p>
          Alaskan malamutes are generally pretty calm, with an even temperament.
          But they are also extremely high energy. If they are not given plenty
          of exercise, you might experience behavioral problems. Malamutes are
          easily bored and can become destructive without regular engagment.
          These dogs also have a high prey drive and enjoy a good chase.
        </p>

        <p>
          Malamutes are prone to digging - a lot! Owners should be aware of this
          character trait, because their dog may try to dig underneath a yard
          fence and escape. An Alaskan malamute attack is much more likely if a
          dog is in a strange environment surrounded by unfamiliar people.
          Owners of a malamute should have fencing set into the ground to avoid
          this happening, or else risk facing a dog bite lawsuit.
        </p>

        <p>
          Alaskan malamutes are not natural guard dogs, but they do provide an
          intimidating presence due to their size and power. They are usually
          very loyal and affectionate with family members and, if trained well,
          can be reasonably happy around strangers. However, no two dogs are
          alike, and not all malamutes are happy and friendly with unfamiliar
          people. There have been several documented
          <b> Alaskan malamute bite incidents</b> involving strangers.
        </p>

        <div>
          <table>
            <tr>
              <th>Siberian Husky Characteristics</th>
              <th>Rating</th>
            </tr>

            <tr>
              <td>Energy</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Intelligence</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Trainability</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Good for Novice Owners</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Prey Drive</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Intensity</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Aggression</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Size</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Power</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Friendly with Other Dogs</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Tendency to Bark or Howl</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>
          </table>
        </div>

        <h2 id="header5">Do Alaskan Malamutes Have Aggression Problems?</h2>

        <p>
          Some Alaskan malamutes can display aggression problems, including
          vicious bite attacks. Statistics showing that they can be extremely
          dangerous dogs.
        </p>

        <p>
          The core numbers of <b>Alaskan malamute attacks</b> are not
          overwhelmingly high. However, they are still very concerning. One
          study looked at the number of recorded dog attacks by breed over the
          past 30 years. According to these figures, there were 17 attacks by
          Alaskan malamutes, six of which were fatal.
        </p>

        <p>
          This seems like a low number. However, when looking at dog bite
          statistics, it is important to take breed population size into
          account.
        </p>

        <p>
          As the 59th most-owned breed in the United States, there are far fewer
          malamutes out there than many other types of dog. Another study looked
          at{" "}
          <Link
            to="https://www.avma.org/Advocacy/StateAndLocal/Documents/javma_000915_fatalattacks.pdf"
            target="new"
          >
            deadly dog attacks in the U.S. between 1979 and 1998
          </Link>
          . The figures revealed by this study were then adjusted based on
          population size by dog bite documenting website PitBullInfo.org.
        </p>

        <p>
          The adjusted stats show that malamutes actually pose one of the very
          highest dog bite fatality risks. The{" "}
          <Link to="https://www.pitbullinfo.org/statistics.html" target="new">
            figures revealed
          </Link>{" "}
          that Alaskan malamute bites resulted in 6.79 deaths per 100,000
          malamutes owned. This is the most of any dog breed.
        </p>

        <LazyLoad>
          <HorizontalBar data={chartData} />
        </LazyLoad>
        <center>
          <blockquote>Breed Risk Rate via Pitbullinfo.org</blockquote>
        </center>
        <p>
          <strong>
            {" "}
            <Link to="http://ht.ly/Yj8s30mLQZc" target="new">
              Dog Bite Statistics in the U.S (Infographic)
            </Link>
            .
          </strong>{" "}
          Download or share our dangerous breeds dog bite info-graphic to
          educate others on the dangers of approaching a dangerous dog.
        </p>

        <h2 id="header6">Malamute Attacks on Humans: Case Studies</h2>

        <p>
          There have been some high-profile Alaskan malamute attacks which have
          made headlines due to their brutal nature. Here is a small selection
          of some of the most frightening in recent years – not just in the
          United States, but across the world:
        </p>

        <p>
          <b>
            <u>
              Colorado girl killed by Alaskan malamute in back yard – 2005 –
              Fruita, Colorado
            </u>
          </b>
        </p>

        <p>
          A young girl was killed in her own back yard by her family’s new dog.
          Kate-Lynn Logel, aged 7, was alone with two dogs when the incident
          happened. The pair of Alaskan malamutes had been adopted by her family
          just three weeks earlier. The girl’s mother found her bleeding heavily
          having been mauled by one of the dogs. She was rushed to hospital but
          tragically died from her severe wounds.
        </p>

        <p>
          <b>
            <u>
              Girl’s arm torn off in savage malamute attack – 2015 – Valentine,
              Australia
            </u>
          </b>
        </p>

        <p>
          A young girl lost her right arm after a horrific Alaskan malamute bite
          attack. Thalia Standley, aged 8, was playing next to a fence in her
          backyard when the incident happened. A vicious Alaskan malamute
          managed to grab Thalia’s arm through a small gap underneath the fence.
          As others rushed to help her, the aggressive malamute chewed through –
          and tore off – the girl’s arm. She recovered well after eight
          surgeries, but will be permanently disfigured by the attack.
        </p>

        <p>
          <b>
            <u>
              Baby fatally mauled by Alaskan malamute – 2014 – Pontyberem, Wales
            </u>
          </b>
        </p>

        <p>
          A baby girl who was just six days old was killed in a deadly attack by
          her family’s pet malamute. The distraught parents of newborn Eliza-Mae
          Mullane called the police after the tragic incident. The youngster was
          airlifted to a nearby hospital but did not survive. It is unclear what
          made the previously-docile Alaskan malamute turn on a defenseless
          family member.
        </p>

        <h2 id="header7">Owning an Alaskan Malamute</h2>

        <p>
          Alaskan malamutes can be difficult for inexperienced owners to deal
          with, for a variety of reasons. Forget the fact that malamutes are
          heavy shedders. This will take a toll on your vacuum cleaner, but is
          harmless enough.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/alaskan-malamute-temperament.jpg"
            width="100%"
            alt="Alaskan malamute temperament"
          />
        </LazyLoad>

        <p>
          Much more concerning is the strong will of the malamute, which can
          cause bigger issues without an experienced owner taking charge. These
          dogs have a pack mentality. They observe a hierarchy of authority and
          tend to display dominant behavior if allowed to do so. Owners must be
          confident and assertive to command an Alaskan malamute’s respect.
          Without strong and consistent training, Alaskan malamutes can be
          unpredictable. They may display quick mood changes and react
          negatively to perceived challenges.
        </p>

        <h3 id="header8">How to Stop an Alaskan Malamute Puppy from Biting</h3>

        <p>
          The best way to stop an Alaskan malamute puppy from biting is with
          calm and effective training. Many Alaskan malamutes bite when they are
          puppies. But these are some of the key steps you should take to train
          your malamute pup:
        </p>

        <ol>
          <li>
            <span>
              Alaskan malamute puppies like to nip and nibble.{" "}
              <b>Use substitutes such as chew toys</b> to teach the dog
              acceptable chewing behavior.
            </span>
          </li>

          <li>
            <span>
              <b>Compile a list of ‘must-know’ commands</b> and concentrate on
              teaching them to your malamute. These should include the biggies
              such as sit and stay orders. These commands may be vital to
              ensuring the safety of people and dogs that your pet comes into
              contact with.
            </span>
          </li>

          <li>
            <span>
              Depending on the behavior of the dog,{" "}
              <b>steer clear of overstimulating activities</b>. A game of tug of
              war could turn ugly with an aggressive dog. Try to encourage
              positive behaviors.
            </span>
          </li>

          <li>
            <span>
              <b>Make training engaging</b>. One of the greatest challenges in
              training Alaskan malamutes is combating boredom. They are
              intelligent but need to be stimulated to maintain their interest.
            </span>
          </li>

          <li>
            <span>
              <b>Include the whole family in training</b>. Malamutes are large
              and powerful, so you should be careful with them around children.
              But the dog needs to recognize the entire family as authority
              figures to obey.
            </span>
          </li>
        </ol>

        <p>
          If you have an aggressive malamute puppy who is not heeding its
          training, you should consult a professional.
        </p>

        <p>
          There are also a wide variety of reasons that might make an older dog
          bite. Follow our guide for tips on what might{" "}
          <Link to="/dog-bites/why-dogs-bite" target="new">
            {" "}
            cause an Alaskan malamute to bite
          </Link>
          .
        </p>

        <h2 id="header9">What are the Different Types of Dog Bite?</h2>

        <p>
          There are six main levels of dog bite or attack. All such incidents
          are serious and could lead to a victim taking legal action. However,
          there are varying levels of attacks (and associated{" "}
          <Link to="/dog-bites/injury-infections-dog-bites" target="new">
            dog bite injuries
          </Link>
          ) which you should be wary of when confronted by a malamute.
        </p>

        <ul>
          <li>
            <b>Level 1 –</b> Aggressive dog behavior, such as snarling,
            teeth-baring and lunging. This can cause fear and trauma without
            physical damage.
          </li>

          <li>
            <b>Level 2 –</b> Impact injuries such as scratches and bruising,
            caused by the malamute scratching, pawing or jumping up.
          </li>
          <LazyLoad>
            <img
              src="/images/dog-bites/alaskan-malamute-bite-injury.jpg"
              width="100%"
              alt="Alaskan malamute bite injury"
            />
          </LazyLoad>

          <li>
            <b>Level 3 –</b> Puncture wounds caused by a bite. The criteria for
            this level of injury includes 1-4 punctures which break the skin and
            can leave scarring.
          </li>

          <li>
            <b>Level 4 –</b> Deep punctures left by an Alaskan malamute biting
            down with force. The wounds must measure at least half the length of
            the dog’s canine tooth in depth.
          </li>

          <li>
            <b>Level 5 –</b> This level of dog bite includes multiple deep level
            4 bites. It could also include torn flesh, if the dog shakes its
            victim when clamped onto them.
          </li>

          <li>
            <b>Level 6 –</b> A dog bite attack resulting in death.
          </li>
        </ul>

        <p>
          Dog bites can result in infections, broken bones, torn ligaments and
          worse, leaving victims in considerable physical pain. Anyone who has
          been bitten by a dog should watch out for signs of infection. A dog
          bite wound should be cleaned regularly with soap and water to prevent
          problems from taking hold.
        </p>

        <p>
          There is also the emotional pain to consider. Being the victim of a
          dog attack can be frightening and traumatic, and the pain and
          suffering they can cause should not be underestimated.
        </p>

        <h2 id="header10">What are the Dog Bite Laws in California?</h2>

        <p>
          The type of dog bite laws used within a state will dictate who is
          liable in the event of a dog bite or attack.
        </p>

        <p>
          Different areas of the United States use different{" "}
          <b>dog bite laws</b>. Some states use a regulation called the
          ‘one-bite rule’. This essentially gives a dog and its owner one free
          pass if the dog attacks someone. It applies if the owner has no prior
          knowledge of a dog’s aggressive tendencies, and had no reason to
          expect such behavior.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/alaskan-malamute-bite-lawsuit.jpg"
            width="100%"
            height=""
            id="lawsuit"
            alt="Alaskan malamute bite lawsuit"
          />
        </LazyLoad>

        <p>
          However, in California, a ‘strict liability’ rule is used in dog bite
          lawsuit cases. This means that if an Alaskan malamute bites someone,
          its owner will be liable for that action. This is the case even when a
          dog has no history of violence.
        </p>

        <p>
          Some people see this hard stance taken by{" "}
          <Link to="/dog-bites/california-dog-bite-laws" target="new">
            California dog bite laws
          </Link>{" "}
          as harsh. But dog bites can be deadly, and will often leave victims
          with life-altering injuries. Owning a dog is a big responsibility.
          Owners must be held accountable when their recklessness or lack of
          care results in another person or animal being hurt by their dog.
        </p>

        <p>
          When a dog bite injury victim pursues legal action against their
          attacker’s owner, they may be awarded compensation. The amount of
          compensation will depend on the circumstances and result of the
          attack.
        </p>

        <p>Contributing factors to compensation levels can include:</p>

        <ul>
          <li>The type and severity of injury sustained</li>
          <li>Medical bills and type of medical treatment undergone</li>
          <li>Rehabilitation costs</li>
          <li>Lost wages as a result of attack injuries</li>
          <li>Pain and suffering</li>
        </ul>

        <p>
          Use the Bisnar Chase{" "}
          <Link to="/dog-bites/victim-rights" target="new">
            dog bite victim rights
          </Link>{" "}
          guide for more information.
        </p>

        <h2 id="header11">
          Contacting an Alaskan Malamute Dog Bite Injury Attorney
        </h2>

        <p>
          If you have been injured by a vicious malamute, it is time to contact
          a knowledgeable dog bite injury lawyer. They will be able to help you
          take effective legal action.
        </p>

        <p>
          Before contacting a dog attack attorney, there are a few steps you
          should take.
        </p>

        <h3>What Should a Victim Do After Being Attacked by a Dog?</h3>

        <p>
          The actions that a person should take after being attacked by a dog
          can be divided into two categories: ‘time-sensitive’ and ‘evidence
          collection’.
        </p>

        <p>
          <b>
            <u>Time-Sensitive</u>
          </b>
        </p>

        <p>Immediately after an attack you should:</p>

        <ul>
          <li>
            Seek appropriate medical attention for any personal injury sustained
            - from{" "}
            <Link to="/dog-bites/first-aid-dog-bites" target="new">
              first aid to hospital treatment
            </Link>
          </li>
          <li>Call 9-11 and make a report to police</li>
          <li>Document your injury if possible</li>
          <li>
            Collect the contact details of anyone who witnessed the incident
          </li>
        </ul>

        <p>
          <b>
            <u>Evidence Collection</u>
          </b>
        </p>

        <p>
          After dealing with the most urgent issues following a dog attack, you
          should collect all related evidence. This can include:
        </p>

        <ul>
          <li>Medical records relating to your injuries</li>
          <li>A copy of the police report from the incident</li>
          <li>Eye witness accounts from people who saw the attack</li>
          <li>
            Any proof of the attack, such as cell phone pictures or videos
          </li>
          <li>Pictures of the location</li>
          <li>A first-hand written account of the incident</li>
          <li>
            Anything else relating to the incident which you believe will boost
            your case
          </li>
        </ul>

        <p>
          At this point, you should contact one of the expert{" "}
          <b>dog bite injury attorneys</b> of Bisnar Chase. They will be able to
          assess your case and help you take it forward.
        </p>

        <div
          className="text-header content-well"
          title="Bisnar Chase Staff"
          style={{
            backgroundImage:
              "url('/images/dog-bites/alaskan-malamute-dog-bite-injury-attorneys.jpg')"
          }}
        >
          <h2 id="header12">
            Superior Client Representation with Bisnar Chase
          </h2>
        </div>

        <p>
          Those looking for the best dog bite injury attorneys in California
          should look no further than Bisnar Chase. Our personal injury lawyers
          are committed to providing quality representation in
          <b> Alaskan malamute bite</b> cases.
        </p>

        <p>
          We know how traumatic a dog attack can be. That’s why Bisnar Chase is
          dedicated to ensuring that victims receive the best compensation
          possible for their pain and suffering. Our law firm has extensive
          experience in handling all kinds of personal injury cases, having been
          in business for 40 years. Over that time, we have maintained an
          outstanding <b>96% success rate</b> and collected more than{" "}
          <b>$500 million</b> for our clients.
        </p>

        <p>
          At Bisnar Chase, we pride ourselves on providing superior client
          representation. This includes offering a ‘No Win, No Fee’ guarantee,
          ensuring everyone has access to high-quality legal services. If you
          have suffered an injury from an Alaskan malamute attack, contact our
          skilled dog bite lawyers now. Call{" "}
          <Link to="tel:+1-800-561-4887">(800) 561-4887</Link> for a free
          consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px;
    border-radius: 6px;

    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -140px;

    opacity: 0;
    transition: opacity 0.3s;
  }

  .tooltip .tooltiptext::before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
