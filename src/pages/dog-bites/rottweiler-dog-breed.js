// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/rottweiler-about-dog-bites.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="About Rottweilers - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="Rottweilers can be very intimidating due to their large & powerful appearance, but Rottweilers are also known for being both vicious attack dogs & loving family companions. Learn more about the Rottweiler breed & how you can stay safe in the event of a dog bite or dog attack. For a Free consultation call 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Rottweilers Dog Breed</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="about rottweilers dog bites "
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          <strong>Rottweilers</strong>: Vicious attack dog, or loyal family
          companion? There are many people who have strong opinions towards both
          sides of this debate, but the truth is, like every dog breed today,
          Rottweilers are a case by case basis, and should not be generalized
          into one category; aggressive or non-aggressive.
        </p>
        <p>
          Like all dogs, pets and animals, no two are the same, having different
          genetics, DNA, differences in how they were raised and their
          environment. Bisnar Chase Personal Injury Lawyers are here to help you
          better understand the Rottweiler breed, learn their history and
          statistical information about dog attacks, how to avoid an attack or
          how to better defend yourself during an attack.
        </p>
        <p>
          In the situation of a dog bite or animal attack, immediate and
          professional medical attention is always advised, followed by seeking
          legal action to help you recover your losses, medical costs, financial
          assistance and even compensation.
        </p>
        This page will cover the following:
        <div className="mb">
          {" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Can Your Rottweiler be both a Family Companion & Attack Dog?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of Rottweilers{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Rottweiler Dog Bite Statistics{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Is the Rottweiler a Better Family Companion or Attack Dog?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Are Rottweilers Temperament?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning a Rottweiler{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are Rottweiler Attack & Bite Reports Accurate?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              5 Steps to Protecting Yourself from an Aggressive Rottweiler
              Attack <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Should You Seek Medical Attention After a Rottweiler Attack?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Documenting Your Rottweiler Dog Bite Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/rottweiler-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Win Your Dog Bite & Rottweiler Attack Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>
        <h2 id="header1">
          Can Your Rottweiler be both a Family Companion & Attack Dog?
        </h2>
        <p>
          <LazyLoad>
            <img
              src="/images/text-header-images/rottweiler-attack-dogs.jpg"
              width="100%"
              alt="natural guard rottweiler attack dogs"
            />
          </LazyLoad>
          From taking down burglars to posing with infamous Los Angeles gangs,
          Rottweilers have been made into a vicious and aggressive breed my
          movies, propaganda and society. Every dog, animal and human have the
          ability to act violently without any warning.
        </p>
        <p>
          The Rottweiler breed are very powerful, intelligent and
          strong-willed,making them great defenders and loyal to their family
          and owners.
        </p>
        <p>
          Rottweilers have a long history going back to the 1800's, from herding
          livestock, to law enforcement, to loyal family companions protection
          children.
        </p>
        <p>
          This page will give an in-depth look at Rottweilers, their history,
          temperament, warning signs and so forth. If you have been attacked or
          experienced a dog bite from a Rottweiler or other dog breed, you are
          always urged to seek medical attention immediately and to contact a
          team of skilled and highly experienced{" "}
          <Link to="/dog-bites" target="new">
            {" "}
            Dog Bite Lawyers
          </Link>
          .
        </p>
        <p>Call us for a Free consultation at 800-561-4887.</p>
        <h2 id="header2">History of Rottweilers</h2>
        <p>
          Rottweilers come from a strong breed of dog, called Molossus which is
          a type of mastiff. Originating from Germany, the Romans used them to
          heard cattle. The name Rottweiler is derived from "Das Rote Wil,"
          which means "the red tile." This dog breed and their handlers built
          houses with red tile roofs, and when excavated a site nearly 600 years
          later and uncovered red tiles from the roofs.
        </p>
        <p>
          Rottweilers were of high necessity, for southern Germany was known for
          their cattle and livestock, using Rotts to heard and protect the
          animals. Rail transport made a strong impact on driving cattle, and
          the Rottweiler nearly became extinct.
        </p>
        <p>
          In 1901, after a dog show in 1882 where only one nondescript
          Rottweiler was on display, the Rottweiler and Leonberger Club was
          founded. Soon after, the first standard of the Rottweiler breed was
          written. The Rottweiler's character and appearance has changed a
          little since then.
        </p>
        <p>
          The breed was soon utilized for its intelligence and power, helping
          the Army and in other areas of defense and protection.
        </p>
        <LazyLoad>
          <img
            className="imgcenter-fluid"
            src="/images/text-header-images/rottweilers-belgian-army-1917.jpg"
            width="65%"
            alt="rottweilers in the army and military"
          />
        </LazyLoad>
        <p>
          Rottweilers became a popular service dog for law enforcement, sniffing
          for drugs, explosives, offering assistance in taking down dangerous
          criminals and so forth. There strength, high training abilities,
          knowledge and loyalty makes them very well suited for this type of
          work.
        </p>
        <LazyLoad>
          <img
            className="imgcenter-fluid"
            src="/images/text-header-images/historical-rottweiler-law-enforcement.jpg"
            width="65%"
            alt="rottweilers in law enforcement"
          />
        </LazyLoad>
        <p>
          The Rottweiler established a name for themselves; truly a breed to be
          reckoned with. But as the Rottweiler breed became popular, so did the
          over-breeding of Rottweilers in puppy-mills, underground breeders,
          black markets and in abusive, low quality and unsafe environments sold
          for the wrong reasons, such as dog fighting, dangerous muling and
          other activities that often resulted in serious injuries leaving them
          severely disabled, or death.
        </p>
        <p>
          These historical photos were provided by{" "}
          <Link to="http://tbrotties.com/" target="new">
            TBRotties.com
          </Link>
          , who focus on high-end breeding and quality Rottweilers. They
          specialize in finding homes with suitable, loving and responsible
          owners. To learn more about Rottweilers from these experts, visit
          their website.
        </p>
        <hr />
        <LazyLoad>
          <img
            src="/images/text-header-images/Allgemeiner-Deutscher-Rottweiler-Klub-(ADRK).JPG"
            width="100%"
            alt="Allgemeiner Deutscher Rottweiler Klub (ADRK)"
          />
        </LazyLoad>
        <p>
          One club that has an authoritative reputation is the{" "}
          <Link to="https://adrk.de/index.php/en/home">
            Allgemeiner Deutscher Rottweiler Klub (ADRK)
          </Link>
          , established in 1921. The very first Rottweiler registered in America
          was in 1931, and the rest is history. Throughout the 1900's the breed
          came to be more and more respected, finding responsible jobs and
          duties that would fit a Rottweiler's needs.
        </p>
        <p>
          Even though they are an intimidating breed, with territorial mindsets,
          Rottweilers soon found themselves with loving families, living with
          and being loved by families in their homes.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Rottweiler Dog Bite Statistics"
            style={{
              backgroundImage:
                "url('/images/text-header-images/rottweiler-dog-bite-statistics.jpg')"
            }}
          >
            <h2 id="header3">Rottweiler Dog Bite Statistics</h2>
          </div>
        </LazyLoad>
        <p>
          Although Rottweilers can make great family companions, protector of
          children and family members, they do carry an infamous rap sheet of
          attacks, bites and killings.
        </p>
        <ul>
          <li>
            According to{" "}
            <Link
              to="https://www.dogsbite.org/dog-bite-statistics-fatalities-2015.php"
              target="new"
            >
              Dogsbite.org
            </Link>
            , in a recent year's study, 11% of dog bite fatalities were from
            Rottweilers
          </li>
          <li>
            {" "}
            <Link to="https://pethelpful.com/dogs/Most-Dangerous-Dog-Breeds">
              PetHelpful.com
            </Link>{" "}
            states that between 1982-2013:
            <ul>
              <li>514 bodily harm attacks</li>
              <li>290 child victim attacks</li>
              <li>136 adult victim attacks</li>
              <li>81 deaths</li>
              <li>294 maimings</li>
            </ul>
          </li>
        </ul>
        <h2 id="header4">
          Is the Rottweiler a Better Family Companion or Attack Dog?
        </h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/rottweiler-family-companion-attack-dog.jpg"
            width="100%"
            alt="family rottweiler companion pet"
          />
        </LazyLoad>
        <p>
          If you grew up in the late 1980's and 1990's, you probably had or knew
          someone who had a pet Rottweiler. That is because throughout the
          1980's and 1990's the Rottweiler breed was one of the most popular
          dogs to have.
        </p>
        <p>
          Whether these Rottweilers were owned as a family companion, for
          protection or for recreational use, breeders were aware of this breeds
          popularity boom, and many took advantage of it.
        </p>
        <p>
          People were breeding Rottweilers left and right, but unfortunately,
          even though they were at the peak of their popularity, there were many
          Rottweilers that did not have homes. From puppy-mills to chain pet
          stores, Rottweilers were taken advantage of, and many of these pups
          suffered as result.
        </p>
        <p>
          Regardless of this breeds history, many wonder about getting a
          Rottweiler, but still have concerns about their ability to be trained,
          domesticated and docile, especially around infants, young children and
          the elderly. The truth is, they make great family members and offer
          loyal and protective companionship.
        </p>
        <p>
          With the right upbringing, training, established authority and
          enforcement, these dogs make great protectors of family members; from
          children to adults, making robberies and assaults a much lower risk
          for households. The overall concern for "aggressive breeds" is the
          possibility of the dog reacting negatively and biting a family member
          or guests. This is definitely a factor to keep in mind at all times.
          Never let your guard down, especially if a dog is acting oddly,
          confused, scared or defensive.
        </p>
        <p>
          If you have a Rottweiler or other dog breed that is acting concerning,
          especially around children or elderly individuals, keep the dog on a
          leash, chain or in a kennel. Muzzles add extra protection in case the
          dog suddenly decides to bite or attack.
        </p>
        <h2 id="header6">What Are Rottweiler's Temperament?</h2>
        <p>
          Society has portrayed a large variety of dog breeds, including the
          Rottweiler, as vicious, territorial man eaters that should always be
          on a chain with a muzzle. But are all Rottweilers aggressive and
          dangerous?
        </p>
        <p>
          When it comes to animals, the truth is, regardless of their genetics,
          DNA, historical background and heritage, you never know what kind of
          reactions, behavior or responses you are going to experience. Many
          believe that breed characteristics are defined by genetics and DNA
          heritage. Others will say it's solely how they were raised and
          nurtured.
        </p>
        <p>
          Given a large amount of research, a fair statement is to consider all
          factors when judging a dog's temperament, such as how they were
          raised, common genetics and their environment.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/breed-characteristics-infochart.JPG"
            width="50%"
            className="imgcenter-fluid"
            alt="breed characteristics rottweiler infographic"
          />
        </LazyLoad>
        <p>
          (Infographic courtesy of{" "}
          <Link
            to="http://dogtime.com/dog-breeds/rottweiler#/slide/1"
            target="new"
          >
            DogTime.com
          </Link>
          )
        </p>
        <h2>Owning a Rottweiler</h2>
        <p>
          Rottweilers rank 17th among 155 breeds. As there Rottweiler breed
          temperament and characteristic infographic above illustrates, they
          earn there ranking respectfully. But before you go out and purchase a
          Rottweiler, it's best to fully understand them, and ensure that your
          living arrangements, time you are able to spend with them, your
          planning for the dog's living, sleeping, eating, bathroom and
          playtime/exercise arrangements are well-suited towards the dogs needs.
        </p>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/N7vlxFfGxiw"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2 id="header7">Are Rottweiler Attack &amp; Bite Reports Accurate?</h2>
        <p>
          When you think of the world's most dangerous breeds, which type of
          dogs do you picture?
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/pit-bull-dog-breed" target="new">
              Pit bulls
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds">Mastiffs</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed" target="new">
              Doberman Pinschers
            </Link>
          </li>
          <li>Rottweilers</li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chows
            </Link>
          </li>
        </ul>
        <p>
          It actually depends which area of a dog bite and animal attack
          statistics report you look at. Whether it's attacks/bites in general,
          severity of the mauling, deaths, ages of the victims; the factors that
          go into all of the different variations of dog bites is endless
          unfortunately.
        </p>
        <p>
          But going off of the Rottweiler attack statistics listed above, are
          Rottweilers the most dangerous breed? Consider this: all dogs have the
          ability to bite and attack, but how severe are the results of their
          attack? Some breeds may have a higher chance of biting someone over
          another breed, but the severity of the bite may be far less than the
          other breed that is less likely to attack.
        </p>
        <p>
          For example, chihuahuas are a small-breed dog, with a tendency to be
          territorial and protective of their owners. Due to their small size,
          aggressive behavior and consistency in biting locations, they have
          earned the nickname, "ankle-biters." But how severe are the bite
          wounds from an "ankle-biter?"
        </p>
        <p>
          Even though Rottweilers can sometimes tend to have an aggressive
          attitude as well as any other dog, the injuries inflicted from a
          Rottweiler attack compared to an ankle-biter are going to be much
          different, and potentially deadly.
        </p>
        <p>
          But in dog bite and dog attack statistics and reports, these factors
          are not included because it is generally irrelevant. Dog bite reports
          are meant to focus on more serious analytics rather than small and
          redundant.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="5 steps avoiding rottweiler attack dog bite"
            style={{
              backgroundImage:
                "url('/images/text-header-images/5-steps-rottweiler-attack-defense.jpg')"
            }}
          >
            <h2 id="header8">
              5 Steps to Protecting Yourself from an Aggressive Rottweiler
              Attack
            </h2>
          </div>
        </LazyLoad>
        <p>
          It is impossible to read a dog to be friendly or not. Sometimes dogs
          can look like they are inviting of strangers when they really are
          planning to react aggressively, and sometimes dogs that are coming off
          aggressive are very friendly and want to lick you all over and get
          pets and affection.
        </p>
        <p>
          Here are <strong>5 steps</strong> you can take to avoid or defend
          yourself in the event of a dog bite or attack.
        </p>
        <ol>
          <li>
            <strong>Avoid the Situation:</strong> You never know how a dog is
            going to react, whether you know him or her or not. Even if the dog
            has no prior history of aggressive behavior, any dog can react
            defensively to any sudden movements, stranger smells, people or
            situations the dog is uncertain about. Don't pet any dogs you don't
            know, don't approach or agitate any dogs especially when they are
            eating, are scared or confused.
          </li>
          <li>
            <strong>Distance Yourself from the Dog:</strong> When in the
            situation of encountering a dog, whether they are in attack-mode or
            staring at you and free of a leash or restraint, if possible, jump
            up on a car, fence or high-up platform that can distance you from
            the dog. Do not run, turn your back or close your eyes when in this
            situation. Stay alert, be loud, act big and confident.
          </li>
          <li>
            <strong>1st Defense:</strong> If in the situation of a dog bite or
            attack, remember to never panic or run. Make sure you are able to
            cover your face, neck and look for objects to stand on or to shield
            yourself between you and the dog.
          </li>
          <li>
            <strong>Using Weapons or Objects:</strong> Be prepared, act
            defensively. Before you go on a walk, or jog, bring something you
            can defend yourself with in the case of a dog bite or attack. Pepper
            spray, pepper gel or mace can be carried with you in the e vent a
            dog attacks. It can also be helpful in the event of being mugged or
            targeted by humans. You can find Personal Defense Items that can
            help you prevent a dog attack in it's tracks. With all types and
            varieties of items for joggers, delivery people, hunters and so on.
            Do your research, and make sure you are prepared when a Rottweiler,
            other breed or animal attacks.
          </li>
          <li>
            <strong>Do Whatever It Takes</strong>: If time is going by, wounds
            and injuries are being inflicted, take any means necessary. When in
            a fight for your life, use anything you have to get to safety.
            Calling for help, fighting back, using weapons or objects to prevent
            the dog from continuing to attack.
          </li>
        </ol>
        <h2 id="header9">
          Why Should You Seek Medical Attention After a Rottweiler Attack?
        </h2>
        <p>
          Rottweilers, other dog breeds, as well as any animal has the
          possibility of encountering diseases which can alter their thought
          process, temperament and overall health and safety to other animals
          and humans. According to the{" "}
          <Link to="https://www.cdc.gov/rabies/index.html" target="new">
            Centers for Disease Control and Prevention (CDC)
          </Link>
          , "
          <em>
            rabies is a preventable disease, often transmitted through the bite
            of a rabid animal
          </em>
          ."
        </p>
        <p>
          Even if you think you are okay or do not need to seek medical
          attention after you are bitten by your dog or a foreign unknown dog,
          seeking medical attention is crucial. As stated above, dogs and other
          animals have high chances of carrying all types of diseases and if
          that is not enough to get you to visit a clinic, emergency room or
          urgent care, dog bites can become infected and cause severe issues
          down the road if not properly taken care of.
        </p>
        <p>
          If a dog bite goes untreated, infections can lead to loss of limbs,
          blood poisoning and even death.
        </p>
        <h2 id="header10">Documenting Your Rottweiler Dog Bite Injuries</h2>
        <p>
          Receiving medical attention also allows you to have your dog bite
          injuries documented. Not having proper or legitimate documentation of
          your injuries in association with the dog bite incident can cause
          major headaches later on in your case, and can even make or break your
          case.
        </p>
        <p>
          Don't lose out on compensation, medical bill coverage and other
          aspects of winning your case when documenting your injuries is so easy
          to do.
        </p>
        <p>
          Even if there are witnesses at the time of the dog attack, later on in
          the case, if you don't have documentation, it is going to be difficult
          for you to prove that all of your injuries were the result of the
          stated dog bite incident, and not a separate one possibly before or
          after.
        </p>
        <h2 id="header11">
          What It Takes to Win Your Rottweiler Dog Attack Case
        </h2>
        <p>
          Dog bites can be traumatic and an overall horrible experience. Many
          people have on-going issues and experience PTSD and other mental
          problems after a severe attack. You don't have to be a victim. With
          the right preparation and tools, compensation can be yours to help pay
          for medical bills, pain and suffering and so on:
        </p>
        <ul>
          <li>Find medical attention</li>
          <li>Have your injuries documented</li>
          <li>Police report</li>
          <li>Photos/video</li>
          <li>Testimonies and contact information from eye-witnesses</li>
          <li>Any evidence</li>
          <li>
            Organization: Time, date, location, everyone involved, outline of
            what happened, what initiated the incident, etc.
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="dog bite lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-lawyers-dog-bite-attorneys.jpg')"
            }}
          >
            <h2 id="header12">
              Let Bisnar Chase Represent You and Win, or You Do Not Pay
            </h2>
          </div>
        </LazyLoad>
        <p>
          Have you experienced a dog bite or dog attack and have questions only
          a skilled Dog Bite Lawyer can answer?
        </p>
        <p>
          Call our team of highly experienced and successful dog bite attorneys
          for your Free consultation and case evaluation at 800-561-4887.
        </p>
        <p>
          The legal team at <Link to="/">Bisnar Chase</Link> has been winning
          cases, helping people in need and changing lives for the better for
          over 40 years. We have won over $500 Million for our clients and have
          established an impressive 96% success rate.{" "}
        </p>
        <p>We will win you maximum compensation or you do not pay.</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
