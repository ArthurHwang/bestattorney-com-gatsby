// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/about-pit-bulls-banner-california-dog-bite-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Pit Bull Dog Breed - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="Pit Bulls have been thought of as the most dangerous & destructive dog breed in the world. But are they? This page gives an in-depth analysis of dog bite statistics & facts about Pit Bulls compared to other dog breeds. If you have any questions, call the California Dog Bite Lawyers of Bisnar Chase at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Pit Bull Dog Breed</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="pit bull dog bite lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          When you think of a vicious dog bite or dog attack, are there any
          specific breeds that come to mind? Why do you think the breed that
          comes to mind among most people is the breed that most people think
          of? Media? Society and culture anxieties? Social media? Movies? What
          about facts? Are there areas of this topic and breed that go unseen
          and unknown to the public?
        </p>
        <p>
          This page may have upsetting facts about dog bite statistics among Pit
          Bulls and other known breeds, as well as potentially controversial
          facts about Pit Bulls and other known breeds from how people respect
          and protect this breeds reputation.{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          <Link to="/" target="new">
            {" "}
            personal injury attorneys
          </Link>{" "}
          do not take any particular side when it comes to dog breed opinions
          and biased perspectives.
        </p>
        <p>
          Nurture vs nature; a common and highly controversial topic between dog
          owners, breeders, trainers, mothers and fathers, basically anyone,
          which leads to the main question... Are Pit Bulls more dangerous than
          other dog breeds?
        </p>
        <p></p>
        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Pit Bulls - Killer or Friend{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Different Types of Pit Bulls{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Where Did Pit Bulls Come From{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              The Working Dog, The Companion{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Animal Welfare Act of 1976{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are Attack & Bite Reports Accurate?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Pit Bull Attack Statistics{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              5 Steps to Protecting Yourself from an Aggressive Pit Bull Attack{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Why Is Seeking Medical Attention Important after a Pit Bull
              Attack?
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Documenting Your Dog Bite Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Win Your Dog Bite & Pit Bull Attack Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/pit-bull-dog-breed#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Successful Dog Bite Lawyers{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>
        <h2 id="header1">Pit Bulls - Killer or Friend</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/pit-bull-pink-portrait.jpg"
            width="100%"
            alt="pit bull dog bite attorneys"
          />
        </LazyLoad>
        <p>
          The Pit Bull community are very tight-nit and supportive towards their
          shared love for their Pit Bull friends. From children to teenagers,
          adults to the elderly, people around the world are in love with their
          Pit Bull companions. But from the complete and polar opposite
          perspective, People say Pit Bulls are born to kill, focused on hunting
          and making the kill, unable to have compassion and withhold urges to
          bite and attack, whether they are feeling threatened or not.
        </p>
        <p>
          When you see the pictures and videos of young toddlers climbing over
          giant and impressively muscular Pit Bull adults without any concern
          from the parents or by standers, how can it be true that Pit Bulls are
          unable to love and instead are, as many put it, "born killers; it's in
          their DNA..."?
        </p>
        <p>
          From our skilled and highly experienced{" "}
          <Link to="/dog-bites" target="new">
            California Dog Bite Lawyers
          </Link>
          , comes an informational reference page to offer the resources you can
          use to determine how you can perceive and better understand the Pit
          Bull breed. This page will cover statistical information, facts and
          why different people's opinions and stand-points mold how society, the
          media and today's culture views the Pit Bull breed.
        </p>
        <h2 id="header2">Different Types of Pit Bulls</h2>
        <p>
          You may not be aware that all Pit Bulls are not just Pit Bulls, and in
          fact, there are many different types of Pit Bulls in the Pit Bull
          family:
        </p>
        <ul>
          <li>American Pit Bull Terrier</li>
          <li>American Staffordshire Terrier</li>
          <li>American Bully</li>
          <li>Staffordshire Bull Terrier</li>
        </ul>
        <p>
          Originating from the United States of America, Ireland and Great
          Britain, "Canis L<em>upus Familiaris</em>," is a breed that can be
          traced back to the early 1800's.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="american staffordshire bull terriers pit bulls"
            style={{
              backgroundImage:
                "url('/images/text-header-images/where-did-pit-bulls-come-from.jpg')"
            }}
          >
            <h2 id="header3">Where Did Pit Bulls Come From?</h2>
          </div>
        </LazyLoad>
        <p>
          In the 1800's lower income individuals would gamble and battle their
          dogs against actual bulls. In 1835, the British Parliament enacted the
          Cruelty to Animals Act. This new act made it illegal for the baiting
          of animals like bulls and bears. Attention was turned towards
          "ratting." The objective of ratting was to see who's dog was able to
          catch and kill the most rats in a selectively confined space/arena, in
          the littlest amount of time.
        </p>
        <p>
          Eventually, this led to the development of needing faster and more{" "}
          <Link to="https://www.petside.com/dog-aggression/" target="_blank">
            aggressive dogs
          </Link>{" "}
          in order to compete with the other dogs, finally evolving into the
          infamous realm of dog fighting.
        </p>
        <p>
          These dogs were bred into the different types of the Pit Bull family,
          being trained and raised to fight, attack and kill. Pit Bulls were
          introduced to America by immigrants bringing them to North America
          with them, but most of these "fighting dogs" were turned into working
          dogs, herding cattle, sheep and protecting livestock from wolves and
          other predators.
        </p>
        <p>
          In the beginning of the American century, "Pits" maintained their
          popularity but evolved into "working className companions." Throughout
          WWII, America actually used the Pit Bull as their nation's mascot,
          showcasing their loyalty and bravery as a key point of marketing and
          advertising strategies and techniques.
        </p>
        <p>
          The Pit Bull has undergone many changes in their "animal roles,"
          `created and idealized as a fighting dog for reputation and profit,
          turned war hero, retiring as a family dog. But soon, the Pit Bull
          would turn into a man-killing machine, in some people's eyes, a step
          below the Great White shark on the predators list.
        </p>
        <h2 id="header4">The Working Dog, The Companion</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/american-staffordshire-terrier-pit-bull.jpg"
            width="25%"
            className="imgleft-fluid mb"
            alt="pit bull companions"
          />
        </LazyLoad>
        <p>
          After WWII, Pit Bulls came to be seen more and more as normal dogs.
          Underground dog fighting still existed, but not to the extent of
          before the war or back in Europe and United Kingdom (UK).
        </p>
        <p>
          As some Pit Bulls were and are bred for herding, hunting or guardian
          purposes, the majority of these dogs are bred for pets and
          companionship. But why the sudden change?
        </p>
        <h2 id="header5">Animal Welfare Act of 1976</h2>
        <p>
          The US Supreme Court passed the{" "}
          <Link
            to="https://www.nal.usda.gov/awic/public-law-94-279-animal-welfare-act-amendments-1976"
            target="new"
          >
            Animal Welfare Act of 1976
          </Link>{" "}
          in 1976, making dogfighting illegal in all 50 states.
        </p>
        <p>
          The{" "}
          <Link to="https://www.nal.usda.gov/" target="new">
            United States Department of Agriculture (USDA)
          </Link>{" "}
          states:
        </p>
        <p>
          <em>"</em>
          <strong>
            <em>Summary:</em>
          </strong>{" "}
          Enacted April 22, 1976, Public Law 94-279 is primarily refining
          previous regulations on animal transport and commerce. "Carrier" and
          "Intermediate Handler" are defined. Health certification prior to
          transport of sale is required and must be performed b y a
          veterinarian. Licenses, method of payment, and penalties for
          violations are discussed. This amendment also introduces and defines
          "animal fighting ventures" to the Act. Animals used in hunting
          waterfowl, foxes, etc. are exempt. It is illegal to exhibit or
          transport via interstate or foreign commerce animals used in fighting
          ventures such as dogs or roosters<em>."</em>
        </p>
        <h2 id="header6">Are Dog Attack & Bite Reports Accurate?</h2>
        <p>
          With a high number of reports for larger dog breeds like Pit Bulls,
          the question is asked, how accurate are these numbers in relation to
          other breeds? What does this mean?
        </p>
        <p>
          Attacks and bites from large dogs like Pit Bulls, have a higher chance
          of resulting in a more serious injury. Compared to a smaller dog, like
          a Jack Russell Terrier or chihuahua, which may only result in minor
          bruising, small puncture wounds or minor lacerations.
        </p>
        <p>
          If every dog bite was reported and documented, what would the numbers
          and statistics look like then? Would small breeds have the same or
          higher volume of attacks/bites than larger dogs? If they were, would
          that make smaller dog breeds the more aggressive or hostile?
        </p>
        <p>
          <strong>
            4 Pit Bulls kill a woman on her morning stroll; owner sentenced to
            15 years to life:
          </strong>
        </p>
        <p>
          <em>
            In cases like the video below, some individuals use pit bulls and
            other "known aggressove breeds" to protect land, livestock and for
            other reasons, but when the handling of these dogs is negligent,
            catastrophic injuries and death can occur. The man in the video
            below was sentenced to 15 years to life in prison for second degree
            murder, after his 4 pit bulls killed an elderly woman on a morning
            stroll.
          </em>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/fyC4yDoAHOY"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2 id="header7">Pit Bull Attack Statistics</h2>
        <p>
          It can't be denied, that Pit Bulls have been known for a high number
          of dog bites and attacks on humans and other animals. Many of these
          attacks have even killed and caused catastrophic injuries, resulting
          in permanent disabilities, high medical costs and severe
          disfigurement.
        </p>
        <p>
          According to an{" "}
          <Link
            to="http://time.com/2891180/kfc-and-the-pit-bull-attack-of-a-little-girl/"
            target="new"
          >
            article by TIME
          </Link>
          , "Pit bulls make up only 6% of the dog population, but they're
          responsible for 68% of dog attacks and 52% of dog-related deaths since
          1982, according to research."
        </p>
        <h3>Looking at Pit Bull Dog Bite Statistics More In-Depth</h3>
        <p>
          There are different perspectives on how the Pit Bull breed should be
          dealt with and perceived. Some think the breed should have more media
          coverage on the normalities of the Pit Bull breed, in comparison to
          other well-known docile breeds, like the golden retriever, Labrador
          and other popular family breeds.
        </p>
        <p>
          Others will go as far as supporting{" "}
          <Link
            to="http://www.aspca.org/nyc/aspca-mobile-spayneuter-clinic?ms=MP_PMK_GGSpayAndNeuter-Tri-State&initialms=MP_PMK_GGSpayAndNeuter-Tri-State&gclid=CjkKEQjw5qmdBRCn--70gPSo074BEiQAJCe7zVWLhBohN91lbTuBqJiSgYIorg54HI0Msi_CbGIMmTDw_wcB"
            target="new"
            rel="noopener noreferrer"
          >
            {" "}
            Free sterilization programs for pit bulls, like the one run by the
            ASPCA
          </Link>
          . Here are some statistics on the Pit Bull breed:
        </p>
        <ul>
          <li>Over 50% of dog bites are from a Pit Bull</li>
          <li>Approximately 87% of pit bulls are aggressive</li>
          <li>
            56% of all U.S. fatal dog bites in the past 5 years were by Pit
            Bulls
          </li>
          <li>Only 20% of Pit Bulls are sterilized (neutered/spayed)</li>
          <li>Pits make up 63% of dogs put down in animal shelters</li>
          <li>Pits make up 38% of shelter admissions</li>
          <li>PETA stands by breed-specific sterilization</li>
          <li>
            Pit bull jaws do not "lock." Most animals tend to hold onto their
            prey after biting it. Pit bull jaws are anatomically the same as all
            other dogs and show no evidence of locking
          </li>
          <li>
            The "Pit Bull" breed represents 3 types of dog:
            <ul>
              <li>American Pit Bull Terrier</li>
              <li>American Staffordshire Terrier</li>
              <li>Staffordshire Bull Terrier</li>
            </ul>
          </li>
        </ul>
        <p>
          To learn more about Pit Bull statistics, you can visit{" "}
          <Link
            to="https://www.livescience.com/27145-are-pit-bulls-dangerous.html"
            target="new"
          >
            LiveScience.com/are-pit-bulls-dangerous
          </Link>
        </p>
        <p>Other dangerous dog breeds include:</p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/rottweiler-dog-breed" target="new">
              Rottweilers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed" target="new">
              Doberman Pinschers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/akita-dog-breed" target="new">
              Akitas
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chows
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds" target="new">
              Mastiffs
            </Link>
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="pit bull attack"
            style={{
              backgroundImage:
                "url('/images/text-header-images/5-steps-avoid-pit-bull-attack-dog-bite (1).jpg')"
            }}
          >
            <h2 id="header8">
              5 Steps to Protecting Yourself from an Aggressive Pit Bull Attack
            </h2>
          </div>
        </LazyLoad>
        <p>
          It is impossible to read a dog to be friendly or not. Sometimes dogs
          can look like they are inviting of strangers when really are planning
          to react aggressively, and sometimes dogs that are coming off
          aggressive are very friendly and want to lick you all over and get
          pets and affection.
        </p>
        <p>
          Here are <strong>5 steps</strong> you can take,{" "}
          <Link
            to="https://www.thestranger.com/seattle/how-to-defeat-a-pit-bull-with-your-bare-hands/Content?oid=3708968"
            target="new"
          >
            {" "}
            according to an article on TheStranger.com
          </Link>
          , to protect yourself from an aggressive Pit Bull attack:
        </p>
        <ol>
          <li>
            <strong>Avoid Conflict:</strong> Avoiding stranger or unknown dogs
            is your best chance of avoiding a preventable attack. Don't let
            children or those who make sudden, unexpected or jerky movements
            around a pit bull. This could threaten, startle or initiate a factor
            of fear in the dog that could result in a defense or territorial
            response/attack/bite.
          </li>
          <li>
            <strong>Stand on a Car:</strong> Never turn your back to, make eye
            contact or run from a dog. This usually results in the dog chasing
            and attacking. If possible, stand, jump or climb to a higher surface
            that is difficult for the dog to get up onto, like a car, roof, shed
            or fence. Putting a distance or gap between you and the aggressive
            dog can detour the animal, allowing time for the dog to lose
            interest, avoiding an attack. Calling for help can be affective if
            the dog is persistent and does not lose interest. Don't lure others
            into your current dangerous situation and warn by standers of the
            near by situation so they avoid being attacked or mauled themselves.
          </li>
          <li>
            <strong>Cover Your Face &amp; Play Dead:</strong> When in the
            situation of unpreventable or uncontrollable attack, cover your
            face, neck and play dead. According to the article on
            TheStranger.com:
            <p>
              <em>
                If there's no car or any other higher level handy, "don't try to
                defend yourself, just protect yourself," says Emily Keegans,
                behavior program manager at the Seattle Humane Society. "That
                basically means to get down on the ground, pull your knees to
                your chest, clasp your hands behind your neck, put your elbows
                around your face." This play-dead-while-defending-your-
                tender-face-and-lap-bits approach is seconded by Wiley:
                "Stillness is the best thing. If a dog gets its teeth into you,
                fighting back can make things worse."
              </em>
            </p>
          </li>
          <li>
            <strong>Use Defense Tools:</strong> Another step you can take
            towards protecting yourself in the event of a dog charge or dog
            attack is to be prepared with pepper spray. Cyclist, mail delivery
            personnel, law enforcement, joggers and even students who walk to
            and from school are armed with this defensive and non-lethal spray.
            You can find pepper spray conveniently available on websites like
            amazon. Available products are inexpensive and reliable, such as{" "}
            <Link
              to="https://www.amazon.com/SABRE-Red-Pepper-Spray-Foundation/dp/B001CZ9MRY"
              target="new"
            >
              {" "}
              SABRE Red Pepper Spray
            </Link>{" "}
            on a key chain for on-the-go efficiency.
          </li>
          <li>
            <strong>Take Any Means Necessary</strong>: There comes a point in
            any altercation when it needs to come to a conclusion. If playing
            dead or showing any form of retreat did not work, it's time to fight
            for your life, literally. Take any means necessary means, do
            whatever it takes to save your life. Using objects you are in
            possession of or able to get a hold of quickly that can offer the
            biggest blow to the attacking dog. It sounds sad and mean, but it in
            a life or death situation, it matters. Some say if you are able to
            jam a closed fist into the mouth of an attacking dog, you can punch
            down into the throat, until the dog begins to gag and realizes they
            are unable to breath. Using heavy or sharp objects to defend
            yourself as well as utilizing wrestling and mma techniques can
            result in a positive outcome. All of these factors have a risk of
            being bitten and injured more, but if done correctly, it could be
            the last bite the dog gets on you. Stay safe, stay alert, stay
            alive.
          </li>
        </ol>
        <LazyLoad>
          <img
            src="/images/text-header-images/pit-bulls-play-fighting-aggressive.jpg"
            width="75%"
            className="imgcenter-fluid"
            alt="defending yourself during dog attack"
          />
        </LazyLoad>
        <h2 id="header9">
          Why Is Seeking Medical Attention Important after a Pit Bull Attack?
        </h2>

        <p>
          Pit bulls, any other breed of dog and any other animal period has a
          chance of carrying diseases. If a dog bites you and draws blood or
          breaks the skin, you have a serious risk of contracting a disease,
          such as rabies. According to the{" "}
          <Link to="https://www.cdc.gov/rabies/index.html" target="new">
            Centers for Disease Control and Prevention (CDC)
          </Link>
          , rabies is a preventable disease, often transmitted through the bite
          of a rabid animal.
        </p>
        <p>
          Rabid animals can include and are not limited to raccoons, skunks,
          bats, foxes, dogs and any other animals that have contracted the
          disease. Other animals such as your pet dogs and cats are at risk of
          contracting diseases if bitten by other animals that have a disease.
        </p>
        <p>
          Once bitten, seek medical attention immediately. Whether you call 911,
          EMS, ambulance services or drive to the nearest hospital, emergency
          room, urgent care of medical clinic, wounds need to be cleaned,
          sterilized, treated and made sure that your immediate health is not in
          danger.
        </p>
        <p>
          Depending on the severity of your injuries, test results and doctors
          recommendations, treatments for dog and animal bites can be extensive,
          lasting weeks, even months of consistent care, sometimes daily.
        </p>
        <p>
          Another risk factor of being attacked by a dog or animal is extensive
          bleeding. If the dog or animal bites you and breaks an artery, vein or
          delicate area, you run the risk of bleeding out or experiencing
          serious medical issues and injuries.
        </p>
        <p>
          Tourniquets can be used if needed, especially for arterial bleeding.
          If a tourniquets is not available, a belt, wire, leash, ripped up t
          shirt or some sort of item that can be wrapped around the injured
          area, tied tightly to put pressure on the area to slow or hopefully
          stop the bleeding until medical attention arrives.
        </p>
        <h2 id="header10">Documenting Your Dog Bite Injuries</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/dog-bite-personal-injury.jpg"
            width="50%"
            id="hand-injury"
            className="imgright-fluid"
            alt="documenting dog bite personal injuries"
          />
        </LazyLoad>
        <p>
          Another fantastic aspect of seeking medical attention is the fact your
          injuries will be professionally documented in association with your
          dog bite or attack incident. If you ever plan on taking legal action,
          not having your injuries documented can become a massive issue.
        </p>
        <p>
          Once you are in the litigation process and are unable to prove that
          your injuries happened when they happened, associate your injuries to
          the incident and make links tying everything together into one
          experience readily available for the court, mediators, insurance
          adjusters, lawyers and attorneys, you have a high chance of losing
          your case.
        </p>
        <p>
          If you want to take legal action, make sure you have everything you
          need to win a case.
        </p>
        <h2 id="header11">
          How To Win Your Dog Bite &amp; Pit Bull Attack Case
        </h2>
        <p>
          Experiencing a traumatic dog bite incident can leave the victim or
          victims in a mess of financial obligation, medical bills, unable to
          work and can destroy lives. Don't assume you will win your case, just
          because you know yourself, you were bitten by a dog. Be prepared, and
          don't let opportunity slip out of your hands. Compensation can be
          yours if you take the following steps:
        </p>
        <ul>
          <li>Seek medical attention</li>
          <li>Document your injuries</li>
          <li>File a police report</li>
          <li>
            Take photos/video of your injuries, animals involved, animal owners,
            etc.
          </li>
          <li>Photos/video of the location the incident took place</li>
          <li>
            Eye-witness testimonies, eye-witness contact information, etc.
          </li>
          <li>Evidence</li>
          <li>
            Be organized: Date, time, location, those involved, outline of what
            happened, what initiated the incident, etc.
          </li>
        </ul>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="pit bull dog bite lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/pit-bull-dog-bite-lawyers.jpg')"
            }}
          >
            <h2 id="header12">Successful Dog Bite Lawyers</h2>
          </div>
        </LazyLoad>
        <p>
          If you have experienced a dog bite or dog attack and have questions
          you would like to ask a skilled Dog Bite Lawyer, call 800-561-4887 for
          your Free consultation and case evaluation.
        </p>
        <p>
          Our team of legal professionals and highly successful dog bite
          attorneys have been winning cases for over 40 years, establishing an
          impressive 96% success rate and winning over $500 million.
        </p>
        <p>
          Don't let anything come between you and what you deserve; you could be
          entitled to compensation. Paying off medical bills, receiving
          compensation for your pain and suffering and more is totally
          achievable. Call us today.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
