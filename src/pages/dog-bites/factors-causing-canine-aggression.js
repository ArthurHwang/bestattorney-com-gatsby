// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Major Factors That Cause Dog Aggression - Medical conditions and More"
        pageDescription="Dog demeanor is fickle and unpredictable. When a dog has never shown violent behavior before, here are some major factors that cause canine aggression."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Major Factors That Cause Canine Aggression</h1>
        <BreadCrumbs location={location} />

        <p>
          Have you been the victim of an attack by an aggressive dog? Bisnar
          Chase represents dog bite victims in California and has spent over
          thirty five years pursuing dog bite victim's rights. Call
          1-800-561-4887 for a free consultation with an{" "}
          <Link to="/dog-bites">experienced dog bite lawyer</Link> to see if you
          have an injury case.
        </p>
        <p>
          There are many reasons why a dog can become aggressive. Sometimes, the
          level of aggression a dog exhibits may have to do with its breed. To
          see the list of breeds, which have been deemed dangerous for their
          breed reputation, visit our "
          <Link to="/dog-bites/dangerous-dog-breeds">
            Common Dangerous Dog Breeds
          </Link>
          " page. In cases where a dog's temperament issues stem from its
          genetic makeup, there is no real cure for aggressive behavior. When it
          comes to these dogs, instinct will always prevail. Other causes of
          aggression can include disease, injury or discomfort.
        </p>
        <p>
          Since all dogs have a tendency to be aggressive at some point in their
          life, you can take steps to curb that aggression. Working with a
          trainer and understanding signals from your dog can be very helpful.
          Aggression triggers can then be avoided and hopefully will not lead to
          a dog bite.
        </p>
        <h2>Aggression Caused by Medical Conditions</h2>
        <p>
          The following is a list of diseases or illnesses, which can cause
          aggression in dogs:
        </p>
        <ul>
          <li>
            <strong>Hypothyroidism:</strong> A condition in which the thyroid
            gland produces less of the hormone than normal. It is the most
            common hormonal disorder in dogs and usually develops between the
            ages of 4 and 10.
          </li>
          <li>
            <strong>Neurological Problems:</strong> Neurological issues can
            develop in dogs as a result of an injury or illness. Perception and
            judgment may be affected, causing aggression. Examples of
            neurological problems in dogs include:
          </li>
          <li>
            <strong>Hydrocephalus</strong> - A disease resulting in excessive
            accumulation of cerebrospinal fluid (CSF) within the ventricular
            system (fluid filled spaces) of the brain. This causes surrounding
            brain tissue to become thinned or compressed.
          </li>
          <li>
            <strong>Encephalitis</strong> - Inflammation of the brain caused by
            either a bacterial or viral infection.
          </li>
          <li>
            <strong>Head trauma</strong> - Either a blunt or penetrating injury
            to the head. The most common cause of head trauma to dogs is motor
            vehicle accidents. Swelling or bleeding of the brain will affect the
            dog's proper functioning, leading to aggression.
          </li>
          <li>
            <strong>Brain tumors</strong> - Tumors that form on the brain,
            caused by many known and unknown factors.
          </li>
          <li>
            <strong>Epilepsy</strong> - Two types occur in dogs: Idiopathic
            Epilepsy, which is when there is no known cause, and it is assumed
            that the condition is inherited. Secondary Epilepsy is when a
            specific cause for the seizures can be determined. Dogs are most
            often aggressive in the post-seizure phase.
          </li>
        </ul>
        <h2>Behavioral Seizures</h2>
        <p>
          Partial seizures, which occur in the region of the brain that controls
          aggression (hypothalamus or limbic system), can result in sudden
          unprovoked aggression in your pet. The signs of this type of
          aggression are as follows:
        </p>
        <ul type="disc">
          <li>
            A sudden mood change right before the occurrence of the seizure.
          </li>
          <li>Sudden violent aggression for no reason.</li>
          <li>Signs of autonomic discharge.</li>
          <li>
            Aggressive posture continuously during the seizure and possibly
            lasting from as little as several minutes, to hours or even days.
          </li>
        </ul>
        <p>
          There are several breeds, which are known for this type of aggression
          including springer spaniels, cocker spaniels, Chesapeake Bay
          retrievers, golden retrievers, bull terriers and poodles.
          Veterinarians can run a battery of tests to determine the exact
          medical cause for aggression in dogs.
        </p>
        <h2>Instinctive Aggression</h2>
        <p>
          Other types of aggression are behavioral and often due to instinct.
          They are as follows:
        </p>
        <ul>
          <li>
            <strong>Dominance-related aggression:</strong> This is the most
            common type of aggression in dogs. The aggressiveness is directed
            towards one or several family members or other household pets. Dogs
            are pack animals and related to humans as pack members.
          </li>
          <li>
            <strong>Territorial aggression:</strong> This type of aggression is
            directed towards approaching animals or people who are outside of
            their pack (outside of the dog's household). Dogs display this type
            of aggression in defense of their area (home, room, yard), owner or
            pack member.
          </li>
          <li>
            <strong>Inter-male Aggression:</strong> Aggression between adult
            male dogs involving territorial or dominance disputes.
          </li>
          <li>
            <strong>Inter-female Aggression:</strong> Aggression between adult
            female dogs living in the same household.
          </li>
          <li>
            <strong>Predatory aggression:</strong> Aggression that is directed
            towards anything that the dog considers to be its prey. This usually
            includes other species but sometimes can include any type of quick
            moving stimulus such as a car or bicycle.
          </li>
          <li>
            <strong>Pain-induced aggression:</strong> Aggression that is
            directed towards the person or item that has caused the animal pain.
            Examples include when a person attempts to touch a painful area or
            when shots are given.
          </li>
          <li>
            <strong>Fear-induced aggression:</strong> Aggression, which is
            directed towards a person when trying to approach the fearful dog.
            This type of aggression is common in situations where the dog is
            cornered or cannot escape or when being severely punished by its
            owner. Unpredictable children can also cause this type of
            aggression.
          </li>
          <li>
            <strong>Maternal aggression:</strong> Aggression that is directed
            towards any person or animal that approaches a mother dog with
            puppies.
          </li>
          <li>
            <strong>Redirected aggression:</strong> This type of aggression
            occurs when a dog redirects its aggression from one source to
            another. A common example of this is a dog barking at the door, but
            then redirects its aggression on its owner who is holding him/her
            back.
          </li>
        </ul>
        <h2>Legally Speaking: If you are bitten by a dog</h2>
        <p>
          If you have been injured in a dog attack, please contact an
          experienced dog bite attorney. We are passionate in our pursuit for
          justice and fair compensation of severely and catastrophically injured
          clients. We've handled many dog attack cases for both children and
          adults. Since{" "}
          <Link to="/dog-bites/california-dog-bite-laws">
            {" "}
            California is very clear on dog bites
          </Link>
          , chances are you have a case. It's rare that a person is not held
          liable for dog bite cases in California - even on or off their own
          premises. Dog bite compensation varies depending on how severe the
          injuries are. Many dog bite victims end up with permanent scars. To
          learn more call us for a free legal consultation at 800-561-4887.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
