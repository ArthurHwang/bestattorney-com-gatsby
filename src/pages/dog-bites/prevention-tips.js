// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dog Bite Prevention Tips: Warning signs, safety and prevention"
        pageDescription="Avoiding an animal attack is your best line of defense. Learn more here, for Dog Bite Prevention Tips & how you can be prepared before & after a dog attack occurs. Call our lawyers at Bisnar Chase for more information & a Free consultation at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Dog Bite Prevention Tips</h1>
        <BreadCrumbs location={location} />

        <p>
          You may not be able to prevent a dog bite, but in California you are
          entitled to compensation. Call our legal team to see if you have a
          case. Call 800-561-4887 for a confidential and free consultation.
        </p>
        <p>
          There is so such thing as a dog that is "safe." This includes your
          family dog or the dog that belongs to a friend or family member. In
          fact, statistics show that a majority of dog attacks that result in
          injuries involve family pets or known dogs owned by friends or
          acquaintances. Any time you or a loved one is around dogs, it is
          critical that you practice safety. There are many common{" "}
          <Link
            to="https://www.aspca.org/pet-care/dog-care/common-dog-behavior-issues/aggression"
            target="_blank"
          >
            dog behavior issues
          </Link>{" "}
          including aggression, which is the number one complaint from most
          veterinarians and trainers.
        </p>
        <p>
          <strong>
            Remember: All dogs can bite, no matter what size or breed. The
            following is a list of helpful tips to help in preventing dog bites:{" "}
          </strong>
        </p>
        <ul type="disc">
          <li>
            Never approach an unfamiliar dog, even if it seems harmless and
            especially if it is sleeping.
          </li>
          <li>Never run away from a dog. Don't yell or scream around a dog.</li>
          <li>Remain motionless when approached by an unfamiliar dog.</li>
          <li>
            If a dog knocks you over, roll into a ball and remain motionless.
          </li>
          <li>Never let children play with a dog unsupervised.</li>
          <li>
            Immediately report stray dogs or dogs that display unusual behavior,
            to the appropriate organization (Animal Control, Humane Society,
            etc.).
          </li>
          <li>
            Avoid direct eye contact with a dog, since they perceive this as
            challenging.
          </li>
          <li>
            Never disturb a dog that is sleeping, eating or caring for puppies.
          </li>
          <li>
            Never pet a dog without letting it see and smell you first. A smart
            way to do this is to make a fist and hold it in front of the dog's
            snout. Let them make the first move.
          </li>
          <li>
            When petting a dog, always start below the chin. Petting the top of
            the head is seen as a dominating gesture, which may cause dogs to
            get aggravated.
          </li>
          <li>
            Never try to intervene when a dog fight occurs. Use a hose to spray
            the dogs, or throw a blanket over them to disorient them.
          </li>
          <li>
            Never tease a dog or play rough. Regardless of breed. There are many{" "}
            <Link to="/dog-bites/dangerous-dog-breeds">breeds that bite</Link>.
          </li>
          <li>
            Be mindful around older dogs; they may be blind, hearing impaired or
            sensitive to touch.
          </li>
          <li>
            Never run past a dog. Joggers and bicyclers may trigger the dog's
            instinct to chase and attack.
          </li>
          <li>
            Avoid dogs that are chained, left in cars or cornered. They may feel
            vulnerable, causing them to attack out of fear.
          </li>
          <li>
            If bitten, immediately seek medical attention and file a report with
            authorities.
          </li>
        </ul>
        <h2>Dog Owner Safety Tips</h2>
        <p>
          <strong>
            As a dog owner, you are responsible for your dog's behavior. The
            following is a list of safety tips to help eliminate the chances
            that your dog will bite others:
          </strong>
        </p>
        <ul type="disc">
          <li>
            Always keep your dog up to date on vaccinations to prevent any
            diseases.
          </li>
          <li>Always keep your dog on a leash when outside of your yard.</li>
          <li>
            Keep your yard safe and fenced. Remember that electronic fences work
            only on the dog, not on the people that may try to approach it.
          </li>
          <li>
            Socialize your dog from the moment you bring it home. Unsocialized
            dogs may be timid and shy. They may bite simply out of fear.
          </li>
          <li>
            Train your dog with basic commands: sit, stay, down, heel and come.
            If your dog ever gets out of reach, you want to remain in control of
            the situation.
          </li>
          <li>
            Train your dog to drop any toys on command. This prevents having to
            reach into or near a dog's mouth to retrieve the toy.
          </li>
          <li>
            Be cautious when introducing your dog into any new situations. Be
            attentive and ready to respond if your dog begins to feel uneasy or
            displays any unusual behavior.
          </li>
          <li>
            Teach your dog non-aggressive games, such as "fetch." Rough play
            such as wrestling or tug-of-war may teach or encourage a dog to
            become more aggressive.
          </li>
        </ul>
        <h2>Warning Signs</h2>
        <p>
          <strong>
            Usually, dogs will not immediately attack. There are warning signs
            you should recognize when a dog becomes aggravated:
          </strong>
        </p>
        <ul type="disc">
          <li>Growling, snarling or aggressive barking.</li>
          <li>
            Signs that a dog is fearful or shy include crouching, the dog
            holding it's head low, the dog putting its tail between its legs.
            Fearful dogs are just as dangerous as aggressive ones.
          </li>
          <li>
            Avoid dogs that have raised fur, erect ears, high tails or stiff
            bodies. This is a sign that you should stay away.
          </li>
          <li>
            Stay out of the way of any unnaturally still or unresponsive dogs.
            Fighting breeds have been bred for their ability to hide their
            aggression.
          </li>
          <li>A dog in pain will bite ANYONE including its owner.</li>
        </ul>
        <h2>Legal Help After a Dog Bite</h2>
        <p>
          If you have been injured by a dog bite or dog attack, please contact
          an experienced{" "}
          <Link to="/dog-bites">California dog bite attorney</Link> at Bisnar
          Chase. We offer free consultations to answer your questions and
          determine if you have a case. If you have been injured, you may be
          entitled to monetary compensation for your injuries to cover medical
          expenses, lost income, hospitalization, cost of cosmetic surgery, pain
          and suffering and emotional distress. All of our dog bite lawyers are
          experienced trial attorneys with years of expertise in California
          courts.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
