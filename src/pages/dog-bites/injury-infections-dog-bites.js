// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Injuries and Infection Caused By Dog Bites - Bisnar Chase"
        pageDescription="Dog attacks can be violent & cause severe pain & suffering. Injuries & infections by Dog Bites can result in loss of income, financial disabilities, inability to work & an endless list of medical treatment & care. The dog bite attorneys at Bisnar Chase are here to help. Call for more information at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Injuries and Infections - Caused by Dog Bites</h1>
        <BreadCrumbs location={location} />

        <p>
          If you've been seriously injured by a dog bite please call our office
          to schedule a free consultation. We've represented{" "}
          <Link to="/dog-bites"> California dog bite victims</Link> for over 40
          years. You may be entitled to compensation for your injuries. Call
          800-561-4887.
        </p>
        <p>
          Many dog's bite but people often think it's only{" "}
          <Link to="/dog-bites/dangerous-dog-breeds">
            large breed dogs that can be dangerous.
          </Link>{" "}
          Large and small dogs can cause serious damage to skin, bone or muscle.
          A dog bite can result in physical and emotional injuries in children
          as well as adults.
        </p>
        <p>
          Dog bites often result in painful physical injuries that can be
          painful and costly to treat. At the same time, victims suffer
          emotional repercussions such as paranoia, a lifelong fear of dogs and
          even post-traumatic stress disorder or PTSD. Being attacked by a dog
          can have lasting effects on you emotionally as well as physical scars.
        </p>
        <p>
          <strong>
            The following are just some examples of types of injuries that can
            occur from a dog bite:
          </strong>
        </p>
        <ul>
          <li>
            <strong>Abrasions:</strong> This refers to superficial damage to the
            skin. Mild abrasions, also known as grazes, do not scar, but deep
            abrasions may lead to scarring. The upper layers of the skin are
            damaged or completely removed in the injury, causing slight
            bleeding, if any.
          </li>
          <li>
            <strong>Lacerations:</strong> An irregular wound which is caused by
            a blunt impact to soft tissue overlying hard tissue or the tearing
            of skin and tissues.
          </li>
          <li>
            <strong>Puncture Wounds:</strong> These occur when a dog's teeth or
            claws dig into the victim's skin completely puncturing it.
          </li>
          <li>
            <strong>Incisions or Incised Wounds:</strong> Caused by clean,
            sharp-edged objects (claws or teeth). These types of injuries are
            typically classified as cuts, rather than actual wounds.
          </li>
          <li>
            <strong>Tissue Loss:</strong> Often, cuts and wounds caused by dog
            bites can lead to the loss of tissue, causing deformity to the
            injured area.
          </li>
          <li>
            <strong>Avulsion:</strong> Defined as the tearing away or separation
            of a piece of an entire structure. This can include small pieces of
            the flesh to be torn away from the body or complete body parts being
            separated from the body.
          </li>
          <li>
            <strong>Crush Injuries:</strong> Crush injuries occur when a body
            part is subjected to high degrees of force or pressure. Crush
            injuries can cause bleeding, bruising, compartment syndrome,
            fractures and lacerations.
          </li>
          <li>
            <strong>Fractured Bones:</strong> Cracks or breaks in the bones
            caused by high force impact or stress. Types of fractures include:
            <ul>
              <li>
                Complete fracture: A fracture in which bone fragments separate
                completely.
              </li>
              <li>
                Incomplete fracture: A fracture in which the bone fragments are
                still partially joined.
              </li>
              <li>
                Linear fracture: A fracture that is parallel to the bone's long
                axis.
              </li>
              <li>
                Transverse fracture: A fracture that is at a right angle to the
                bone's long axis.
              </li>
              <li>
                Oblique fracture: A fracture that is diagonal to a bone's long
                axis.
              </li>
              <li>
                Compression fracture: A fracture that usually occurs in the
                vertebrae.
              </li>
              <li>
                Spiral fracture: A fracture where at least one part of the bone
                has been twisted.
              </li>
              <li>
                Comminuted fracture: A fracture, which results in several
                fragments.
              </li>
              <li>
                Compacted fracture: A fracture that occurs when bone fragments
                are driven into each other.
              </li>
              <li>
                Open fracture: A fracture that occurs when the bone is in
                contact with air either by piercing the skin or by severe tissue
                injury.
              </li>
            </ul>
          </li>
          <li>
            <strong>Sprain and strains:</strong> Sprains are injuries, which
            occur to ligaments and are caused by the sudden over-stretching of
            the ligament. Usually the ligament is only stretched, but is also
            common for them to completely tear.
          </li>

          <li>
            <strong>Scars:</strong> Every wound will result in a scar, ranging
            in severity depending on the type of wound. Some will fade over
            time, while others will require cosmetic surgery to repair. Some
            scars can become permanent.
          </li>
          <li>
            <strong>Infections:</strong> Infections to untreated dog bites and
            wounds are common due to the amount of bacteria carried by dogs.
            Dogs can also carry many diseases and organisms that carry diseases,
            all of which can be passed on to the victim during an attack. It is
            extremely important to wash and treat all wounds immediately to
            avoid infection.
          </li>
        </ul>
        <h2>Seeking Legal Information on Dog Bite Injuries</h2>
        <p>
          If you have suffered injuries due to a dog bite or dog attack, please
          get{" "}
          <Link to="/dog-bites/first-aid-dog-bites">
            immediate medical attention,
          </Link>{" "}
          treatment and care. Contact the experienced California dog bite
          attorneys at Bisnar Chase to obtain more information about pursuing
          your legal rights. As a victim of a dog attack, you have the right to{" "}
          <Link to="/dog-bites/california-dog-bite-laws">
            {" "}
            hold dog owners financially responsible
          </Link>{" "}
          for their negligence.
        </p>
        <p>
          A dog bite case can be very complex and requires the skills of an
          attorney who has spent many years in local courts fighting dog attack
          cases. Our attorneys are trial lawyers with years of expertise in
          catastrophic and serious injury cases. Call today for a free legal
          consultation about your dog bite case.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
