// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Dog Bite Victim Rights - Bisnar Chase Personal Injury Attorneys"
        pageDescription="Experiencing a dog attack can be very traumatic & often resulting in severe or catastrophic injuries. Know your rights & what you are entitled to. Compensation could be waiting for you, as well as justice being served. Victims of dog attacks: Your legal rights are important. Call for more information at 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Victims of Dog Attacks: Your Legal Rights</h1>
        <BreadCrumbs location={location} />
        <p>
          Please call the experienced personal injury attorneys of Bisnar Chase
          if you've been the victim of a dog attack. You may be entitled to
          compensation. Call 800-561-4887.
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#rights1">
              A Survey of the Law
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#rights2">
              Strict Liability for Trespass
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#rights3">Negligence</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#right4">Scienter</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#rights5">Special Rules</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/victim-rights#rights6">Defenses</Link>
          </li>
        </ul>
        <h2>
          {" "}
          <Link id="rights1"></Link>Civil Liability for Damages Suffered in a
          Dog Attack.
        </h2>
        <p>
          As man learned to "tame" wild animals, and then extended the benefits
          of taming, these animals became domesticated. The Restatement Second
          of Torts defines a domestic animal as "an animal that is by custom
          devoted to the service of mankind at the time and in the place where
          it is kept." The process of domestication meant that animals, which
          had been "wild", and retained the instincts of their wild ancestors,
          were in closer contact and connection with man and his society. As
          with most other interactions of society, man has made rules to
          regulate those interactions.
        </p>
        <p>
          For as long as man has had laws, he has made the owners of animals
          responsible for the{" "}
          <Link to="/dog-bites/factors-causing-canine-aggression">
            {" "}
            damage that those aggressive animals cause
          </Link>
          . The Mosaic law, Solon's laws of Athens, and the Roman Institutes of
          Justinian, for example, embodied many of the principles that later
          evolved into the early common law treatment of liability for the acts
          of animals. Until recent times, most domesticated animals were used
          for agricultural purposes. Common Law's provisions are grounded in
          this agricultural use, with infrequent interaction with "city
          dwellers".
        </p>
        <p>
          However, after the Industrial Revolution and in the huge increase in
          the world's population, farming has been relegated to a smaller sector
          of the economy, range land was decreased, and the travel of city
          dwellers through areas where wild animals still occur has increased.
          This increasing exposure of people to animals has continued the
          evolution of the laws that relate to wild and domesticated animals.
        </p>
        <p>
          In common law, there have been three theories of liability asserted
          against the owners of animals to make them responsible for the damage
          done by the animals.
        </p>
        <h2>
          {" "}
          <Link id="rights2"></Link>Strict Liability for Trespass
        </h2>
        <p>
          The owner of an animal is strictly liable for the damage arising from
          the animal's trespass on the land in possession of the plaintiff,
          including damage to the crops, which are growing on the land. There is
          a statutory duty for an owner to keep his animals from intruding onto
          another's land, or he is responsible for the damage they cause.
        </p>
        <h3>Strict Liability for Injury Caused by Dangerous Animals</h3>
        <p>
          In an effort to protect the safety of people, society has decided that
          the risk of harm from wild or dangerous animals outweighs the social
          utility of the animal. Therefore, the owner of an animal with known
          dangerous propensities is strictly liable for the injuries, which the
          animal causes. The possessor of a wild animal is strictly liable for
          harm arising from the dangerous propensities characteristic of wild
          animals of its className, whether or not the owner believes the animal
          is safe or free from those propensities.
        </p>
        <p>
          California's courts have recognized that wild animals are always
          considered dangerous, and domestic animals are usually considered
          harmless. However, if the owner knows that his domesticated animal has
          "abnormally dangerous propensities", with "dangerous" meaning "likely
          to inflict serious injury", the owner is responsible for the injuries
          caused by those abnormally dangerous propensities, applying
          Restatement Second of Torts, Section 509.
        </p>
        <h2>
          {" "}
          <Link id="rights3"></Link>Negligence
        </h2>
        <p>
          Under common law, the owner or keeper of a domestic animal is
          generally not liable for injuries inflicted by the animal unless the
          injuries were the result of a vicious propensity of which the owner
          had notice or knowledge. Liability for foreseeable harm will be
          imposed only after proof that the particular animal possessed a
          dangerous propensity that caused the plaintiff's injury and that the
          defendant had actual or constructive knowledge of such propensity. The
          legal term for this knowledge is "scienter". The main thrust of an
          action for negligence in a dog attack case is the presentation of
          evidence, which is convincing to a jury or other trier of fact that
          the defendant had scienter.
        </p>
        <h2>
          {" "}
          <Link id="rights4"></Link>Scienter may be established in several ways
        </h2>
        <p>
          The most straightforward and persuasive proof of scienter is
          establishing a history of the dog's prior attacks or other injurious
          behavior, which permits a reasonable person to draw the inference that
          the dog is likely to engage in such behavior again. The knowledge of
          such behavior, if not actual, will be imputed to the owner or keeper
          if, with the exercise of reasonable care, he should have known of it.
          One or two prior bites, however, may not be sufficient to establish a
          dangerous propensity. The circumstances surrounding the occasion of
          the biting and its extent demonstrate whether the incident of the
          prior bite is sufficient to prove scienter. For example, a dog caught
          in a door and consequently frightened and in pain, may bite as a
          natural reaction to its circumstances without exhibiting a vicious
          propensity.
        </p>
        <p>
          A second method is to show actual or constructive knowledge of a
          propensity or tendency of the animal to act in a certain way under
          certain circumstances, although no actual attack or injury ever
          occurred. Scienter will be found when the owner or keeper has seen or
          heard enough to convince a man of ordinary prudence or, at least, to
          raise a reasonable inference of the animal's inclination to commit the
          className of injury charged against it. Thus, the old English common
          law maxim, "A dog is entitled to a first bite," is no longer true. For
          example, knowledge of change in a dog's temperament from friendly and
          gentle to "ill-natured" after a brief stay in a kennel is considered
          adequate notice to the owner or keeper that, if not properly
          restrained or confined, the dog will be likely to injure someone.
        </p>
        <p>
          A third method of establishing scienter is by the use of
          circumstantial evidence, usually in conjunction with the testimony of
          an expert trial witness to establish the "commonly held" or "commonly
          understood" dangerous propensities of the dog in question. Evidence of
          the following is usually admissible to permit the trier of fact to
          draw an inference of dangerous propensities of the animal and/or the
          owner's or keeper's knowledge of them:
        </p>
        <ul type="disc">
          <li>The animal's species or breed</li>
          <li>Its size</li>
          <li>Its reputation in the neighborhood</li>
          <li>Its training;</li>
          <li>The owner's purpose for keeping it, e.g., as a watchdog</li>
          <li>The length of time it was kept</li>
          <li>
            The care exercised in its custody, e.g., chaining or confining it to
            an enclosed area
          </li>
          <li>
            The owner or keeper's warnings to others, whether written or oral
          </li>
          <li>
            Other unambiguous behavior of the defendant with regard to the
            animal, such as always using a harness or bridle when grooming a
            horse.
          </li>
        </ul>
        <p>
          Finally, scienter may be imputed vicariously either by the application
          of the "respondent superior" theory which, in part, holds that the
          knowledge gained by an agent or servant while caring for the business
          of the master or principal (such as the care or management of the
          animal on behalf of the owner) is the knowledge of his principal or
          master or, if applicable, by the knowledge of one co-owner or keeper
          being imputed to the other(s).
        </p>
        <p>
          People or entities other than owners or "keepers" can be held liable
          in a negligence action for a dog attack. One example is a landowner
          who allows or permits a dog owner or keeper to keep the dog at the
          landowner's property. In California, this theory of liability arises
          from a "premises liability" duty, which the landowner owes to people
          who may be injured by a "dangerous or defective condition" on his
          property. In California, a commercial (business property) landlord has
          a high duty of care to inspect and discover the dangerous or defective
          conditions on his property, and will be held liable for the results of
          an attack of a dangerous dog if there was any chance that he would
          have discovered the dog's presence on the property.
        </p>
        <p>
          A residential landlord's duty is usually not so high. In California, a
          residential landlord must have actual notice or knowledge of the
          dangerous propensity of the dog before the attack. Much of the
          difference in the treatment between the two types of landlords is
          explained by the ongoing right of a landlord to inspect the premises
          and the retention of control and power to force the tenant to change,
          correct, or improve the condition of the premises, even if that means
          the right to terminate the rental agreement and regain possession of
          the premises.
        </p>
        <p>
          The negligence liability arises from any type of dog attack, even one
          which does not result in a "bite" or "tearing" of the skin. In Drake
          v. Dean (1993) 15 Cal.App.4th 915, 19 Cal.Rptr.2d 325, the Court of
          Appeal upheld liability against a dog owner for injuries caused when
          the dog jumped on plaintiff and knocked her to the ground. Plaintiff
          suffered a broken hip and lacerations to her head where it struck some
          rocks.
        </p>
        <h2>
          {" "}
          <Link id="rights5"></Link>Special Rules Regarding the Liability of Dog
          Owners: Strict Liability for Any Bite
        </h2>
        <p>
          Many states, including California, have adopted special laws, which
          have modified "common law" liability rules relating to dog attacks.
          The California Legislature has enacted a law, codified as Civil Code
          Section 3342, which makes a dog owner subject to strict liability for
          any dog bite attack.
        </p>
        <p>
          The law states: "The owner of any dog is liable for the damages
          suffered by any person who is bitten by the dog while in a public
          place or lawfully in a private place, including the property of the
          owner of the dog, regardless of the former viciousness of the dog or
          the owner's knowledge of such viciousness."
        </p>
        <p>
          A California appellate court opinion has held that the word "bite"
          does not require a puncture or tearing away of the skin to cause a
          wound. The Court held that a dog must have the plaintiff or
          plaintiff's clothing in the grip of his closed jaws, and that this
          "bite" must cause plaintiff injury.
        </p>
        <h2>
          {" "}
          <Link id="rights6"></Link>Dog Bite Defenses
        </h2>
        <p>
          The claims of the injured plaintiff may be denied or recovery
          diminished by the actions of the plaintiff which are termed
          "assumption of the risk" or "contributory" or "comparative" fault.
          "Assumption of the risk" is a plaintiff's voluntary participation in
          an activity or action which, in the eyes of the law, means that the
          plaintiff has waived any duty of due care that may be owed to him by
          other people.
        </p>
        <p>
          Frequently, this absolute defense to an action in negligence arises in
          the voluntary participation in a sporting event. It might be
          applicable in a dog attack case. More frequently, the defense of
          "contributory" or "comparative" negligence may apply. This is a
          negligence of the plaintiff, which has contributed to the injuries he
          has suffered. In most states, including California, the jury may
          determine a "percentage" of responsibility of the defendant and the
          plaintiff, and will make the defendant responsible only for his share
          of liability. In California, contributory negligence applies in cases,
          which are based in strict liability.
        </p>
        <p>
          In California, "assumption of the risk" has been applied to bar claims
          by dog handlers or veterinarians who are bitten while caring for dogs
          or where a police officer is bitten by a police dog operated by
          another jurisdiction, but not other fact patterns.
        </p>
        <p>
          The most common fact scenario in which contributory negligence would
          arise is when the plaintiff has provoked the dog into attacking him.
          Provocation is defined as "something that arouses anger or animosity
          in another, causing that person to respond in the heat of passion."
          When a plaintiff has intentionally excited or stimulated a dog, he is
          partially at fault when the dog reacts in a normal or expected manner.
        </p>
        <p>
          Many states have found the following conduct as the basis of holding a
          plaintiff as being partially or wholly at fault for a dog attack:
        </p>
        <ul type="disc">
          <li>Coming into Contact with the Dog</li>
          <li>Touching</li>
          <li>Striking</li>
          <li>Petting, stroking, handling</li>
          <li>
            Pulling or pushing the dog's chain or an object in the dog's mouth
          </li>
          <li>Hugging</li>
          <li>Straddling or attempting to ride</li>
          <li>Carrying</li>
          <li>Kicking or pushing with foot</li>
          <li>Stepping on or falling over</li>
          <li>Throwing objects</li>
          <li>Spraying with a hose</li>
          <li>Coming into the proximity of the dog</li>
          <li>
            Approaching the dog nearing a fence which is restraining the dog,
            putting body parts through the fence
          </li>
          <li>Approaching the dog in a yard, porch, or in a building</li>
          <li>Mere presence in the home where the dog lives</li>
          <li>Opening the door of the house with the dog behind it</li>
          <li>Encountering a dog on the street, either walking or riding</li>
          <li>
            Encouraging the dog to enter the plaintiff's own house or yard
          </li>
          <li>Other Actions directed at or near the dog</li>
          <li>Waiving objects or hands</li>
          <li>Getting involved in a dog fight</li>
          <li>Attempting to restrain the dog</li>
          <li>
            Shouting, yelling, stamping, jumping, staring at the dog or the
            dog's owner
          </li>
          <li>
            Interacting with the dog despite plaintiff's own knowledge of the
            dog's vicious propensities or history of prior bites
          </li>
        </ul>
        <h2>ENDNOTES</h2>
        <ul type="disc">
          <li>Restatement Second of Torts, Section 506(2).</li>
          <li>Exodus 21:28-36</li>
          <li>
            1 Plutarch's Lives of the Noble Greeks and Romans, Solon, 403, 431.
          </li>
          <li>
            The body of law based on the English legal system, as distinct from
            a civil-law (such as French) system, often derived from judicial
            decisions, rather than from statutes or constitutions.
          </li>
          <li>Holmes, Oliver Wendell, The Common Law 18-24 (1881).</li>
          <li>
            "Strict Liability" is defined as "Liability that does not depend on
            actual negligence or intent to harm, but that is based upon the
            breach of an absolute duty to make something safe." Black's Law
            Dictionary, 7ed.
          </li>
          <li>
            "Trespass" is defined as "An unlawful act committed against the
            person or property of another, especially wrongful entry on
            another's real property." Black's Law Dictionary, 7ed.
          </li>
          <li>Restatement Second of Torts, Section 507(2).</li>
          <li>
            Talizin v. Oak Creek Riding Club (1959) 176 Cal.App.2d 429, 1
            Cal.Rptr. 514.
          </li>
          <li>
            Portello v. Aiassa (1994) 27 Cal.App.4th 1128, 32 Cal.Rptr.2d 755;
            Doncin v. Guerrero (1995) 34 Cal.App.4th 1832, 41 Cal.Rptr.2d 192
          </li>
          <li>
            Uccello v. Laudenslayer (1975) 44 Cal.App.3d 514, 118 Cal.Rptr. 741.
          </li>
          <li>Bar Approved Jury Instructions (B.A.J.I.), B.A.J.I. 6.67.</li>
          <li>
            Johnson v. McMahan (1998) 68 Cal.App.4th 173, 80 Cal.Rptr.2d 173.
          </li>
          <li>
            Daly v. General Motors Corp. (1978) 20 Cal.3d 725, 144 Cal.Rptr. 380
          </li>
          <li>
            Davis v. Gaschler (1992) 11 Cal.App.4th 1392, 14 Cal.Rptr.2d 679;
            Prays v. Perryman (1989) 213 Cal.App.3d 1133, 262 Cal.Rptr. 180;
            Nelson v. Hall (1985) 165 Cal.App.3d 709, 211 Cal.Rptr. 668.
          </li>
          <li>
            Farnam v. State of California (2000) 84 Cal.App.4th 1448, 101
            Cal.Rptr.2d 642.
          </li>
          <li>Drake v. Dean (1993) 15 Cal.App.4th 915, 19 Cal.Rptr.2d 325.</li>
          <li>Black's Law Dictionary, 7ed.</li>
        </ul>
        <p>
          This list is derived from the article "Defenses to Dog Bites" found at
          11 A.L.R.5th 127. It is important to note that not every state has
          found each of these actions to be the basis of comparative fault: an
          attorney will need to advise you on the facts which have served as the
          basis for this defense in your state of residence or where the dog
          attack occurred.
        </p>
        <p>
          California Dog Bite Injury Lawyer Disclaimer: The California dog bite
          legal information presented at this site should not be construed to be
          formal legal advice nor the formation of a lawyer or attorney client
          relationship. Any results set forth here were dependent on the facts
          of that case and the results will differ from case to case. Please
          contact a <Link to="/dog-bites"> California dog bite attorney</Link>{" "}
          at our law offices.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
