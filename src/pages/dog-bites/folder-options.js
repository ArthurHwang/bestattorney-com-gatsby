/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "dog-bite",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "",
    HTML: `

    `
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long. Mostly used for PA pages
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "Dog Bite Resources",
    links: [
      {
        linkName: "Dangerous Breeds",
        linkURL: "/dog-bites/dangerous-dog-breeds"
      },
      {
        linkName: "Factors Causing Canine Aggression",
        linkURL: "/dog-bites/factors-causing-canine-aggression"
      },
      {
        linkName: "First Aid for Dog Bites",
        linkURL: "/dog-bites/first-aid-dog-bites"
      },
      {
        linkName: "Injury and Infections from Dog Bites",
        linkURL: "/dog-bites/injury-infections-dog-bites"
      },
      {
        linkName: "Dog Bite Prevention",
        linkURL: "/dog-bites/prevention-tips"
      },
      {
        linkName: "Dog Bite Statistics",
        linkURL: "/dog-bites/statistics"
      },
      {
        linkName: "Victim Rights",
        linkURL: "/dog-bites/victim-rights"
      },
      {
        linkName: "Why Dogs Bite",
        linkURL: "/dog-bites/why-dogs-bite"
      },
      {
        linkName: "California Dog Bite Laws",
        linkURL: "/dog-bites/california-dog-bite-laws"
      },
      {
        linkName: "Criminal Penalties",
        linkURL: "/dog-bites/criminal-penalties"
      },
      {
        linkName: "Insurance Breed Bans",
        linkURL: "/dog-bites/insurance-ban-breeds"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Dog Bite Location Injury Information",
    links: [
      {
        linkName: "Newport Beach",
        linkURL: "/newport-beach/dog-bites"
      },
      {
        linkName: "Irvine",
        linkURL: "/irvine/dog-bites"
      },
      {
        linkName: "Fullerton",
        linkURL: "/fullerton/dog-bites"
      },
      {
        linkName: "Los Angeles",
        linkURL: "/los-angeles/dog-bites"
      },
      {
        linkName: "Costa Mesa",
        linkURL: "/costa-mesa/dog-bites"
      },
      {
        linkName: "Santa Ana",
        linkURL: "/santa-ana/dog-bites"
      },
      {
        linkName: "Fountain Valley",
        linkURL: "/fountain-valley/dog-bites"
      },
      {
        linkName: "San Bernardino",
        linkURL: "/san-bernardino/dog-bites"
      },
      {
        linkName: "Riverside",
        linkURL: "/riverside/dog-bites"
      },
      {
        linkName: "Anaheim",
        linkURL: "/anaheim/dog-bites"
      },
      {
        linkName: "Garden Grove",
        linkURL: "/garden-grove/dog-bites"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "About Dog Attack Lawsuit Claims",
      videoUrl: "aGlklcSADWA"
    },
    {
      videoName: "Dog Bite Liability - Who is at Fault?",
      videoUrl: "_LQ8M7WPjik"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
