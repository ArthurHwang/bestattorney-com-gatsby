// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "dog-bites/boxer-dog-attack.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Boxer Dogs - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="A Boxer dog attack can result in life-threatening bite wounds. Dog bite victims can get 'No Win, No Fee' help from the specialist attorneys at Bisnar Chase."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Boxer Dog Breed</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Boxer dog attack"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The proud Boxer dog is one of the most popular breeds in the United
          States, taking pride of place as trusted family pets in homes across
          the country. Boxers are graceful and athletic animals, with
          distinctive flat muzzles, alert ears, and a curious and courageous
          nature.
        </p>

        <p>
          You can see why they are popular with dog lovers – but as with many
          breeds, Boxers can be aggressive and dangerous. There is a reason why
          the Boxer appears on many lists showing the most vicious dogs around.
          Their muscular bodies and ferocious bite mean that anyone who falls
          victim to a <b>Boxer dog attack</b> could be seriously hurt.
        </p>

        <p>
          Anyone who is wounded in a Boxer bite attack should contact the expert{" "}
          <Link to="/dog-bites" target="new">
            dog bite attorneys
          </Link>{" "}
          at Bisnar Chase for comprehensive help in securing compensation for
          their pain and suffering. Read on to find out everything you need to
          know about the Boxer breed. Our guide will cover{" "}
          <b>Boxer aggression problems</b> and Boxer dog attack statistics, as
          well as outlining the steps you should take if you become a bite
          victim.
        </p>

        <div style={{ marginBottom: "2rem" }} align="center">
          {" "}
          <Link to="/dog-bites/boxer-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of the Boxer Dog Breed
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Where Does the Boxer Name Come From?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Breed Characteristics
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Dog Temperament
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Dog Popularity
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              The Boxer: A Working Dog{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning and Training a Boxer
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Dog Bite Force
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Attacks <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Surviving a Boxer Attack{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Boxer Bite Legal Action and Compensation
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header12">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Documenting a Dog Attack
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/boxer-dog-breed#header13">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              No Win, No Fee Help from Bisnar Chase
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">History of the Boxer Dog Breed</h2>

        <p>
          The Boxer breed has ancient ancestors going back to 2500 BC, but the
          modern-day breed was developed in Germany in the 1800s. Boxers were
          bred from a German breed called the Bullenbeisser, which is now
          extinct, along with other breeds from across Europe – such as the
          English Bulldog.
        </p>

        <p>
          The Bullenbeisser was once used by German nobles in hunting
          expeditions. It was capable of chasing down and taking on huge animals
          such as bison and bears – showing where the modern Boxers get their
          power and aggression from.
        </p>

        <p>
          As the German nobles fell from power, their hunting traditions also
          fell by the wayside. Over time, Bullenbeissers were cross bred with
          other dogs to create a new variety of sleeker and more versatile dogs.
          While Bullenbeissers were gradually cross-bred into extinction, this
          process resulted in the hugely popular Boxer dog.
        </p>

        <p>
          The first recorded Boxer was displayed by its German breeders in 1896.
          A year later, they founded the Deutscher Boxer Club. The club
          published a breed standard document for the Boxer in 1904, which has
          barely changed to this day. The first Boxer dog was also registered
          with the American Kennel Club in 1904, and has developed into a hugely
          popular breed across the world since then.
        </p>

        <h2 id="header2">Where Does the Boxer Name Come From?</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/boxer-dog-bite.jpg"
            width="35%"
            className="imgright-fluid"
            alt="Boxer dog bite"
          />
        </LazyLoad>

        <p>
          There are all kinds of conflicting tales over the origins of the Boxer
          name, with no cast-iron consensus. Here are just three of the
          possibilities:
        </p>

        <ul>
          <li>
            It was originally reported that the name may have come from the
            breed’s tendency to stand on its hind legs and use its front paws.
            However, this theory has been dismissed by some experts.
          </li>

          <li>
            Another theory is that the name comes from a term used for the old
            Bullenbeisser breed. A variety of the Bullenbeisser was also known
            as a ‘Boxl’, and it is believed that this may have inspired the
            Boxer name.
          </li>

          <li>
            Other historians have noted that ‘Boxer’ was once a popular dog
            name, and say the breed may have been named after one of the
            earliest so-named Boxer dogs.
          </li>
        </ul>

        <p>
          It’s virtually impossible to know which (if any) of these provided the
          true origin of the Boxer breed’s name.
        </p>

        <h2 id="header3">Boxer Breed Characteristics</h2>

        <ul>
          <li>
            <b>Height:</b> <i>Males</i> – 22-25 inches / <i>Females</i> – 21-24
            inches
          </li>
          <li>
            <b>Weight:</b> <i>Males</i> – 65-90 lbs / <i>Females</i> – 50-80 lbs
          </li>
          <li>
            <b>Color:</b> Usually fawn, brindle, or white, with black and white
            markings
          </li>
          <li>
            <b>Coat:</b> Short and shiny fur with a smooth feel
          </li>
          <li>
            <b>Life Span:</b> 9-15 years
          </li>
        </ul>

        <h3>Distinguishing Features of Boxer Dogs</h3>

        <p>
          Boxer dogs typically have long necks, with extremely distinctive
          faces. Its muzzle is described as ‘blunt’, meaning it is very short,
          giving the breed a fairly flat face compared to many other dog types.
          These dogs also have a trademark underbite, which can lead to plenty
          of doggy drooling.
        </p>

        <p>
          The coat of the Boxer is very short and lies flat, and the majority
          are either a fawn color of various hues, or a browner brindle color.
          Some Boxers of these colors also have white markings on their
          stomachs, feet, necks or faces. These marks are known as ‘flash’.
          Additionally, around 20% of Boxer dogs are completely white in color.
        </p>

        <h3>Should You Get a Boxer’s Ears Cropped?</h3>

        <p>
          Boxers have naturally flappy ears which fold over, but in some cases
          they are ‘cropped’. This is the process of trimming the ears so that
          they point straight up. This is banned in many countries, but is legal
          in most parts of the U.S. – thought regulated in some states. The
          decision over whether to do this involves aesthetic, practicality, and
          moral considerations.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="aggressive Boxer dog"
            style={{
              backgroundImage:
                "url('/images/dog-bites/aggressive-boxer-dog.jpg')"
            }}
          >
            <h2 id="header4">Boxer Dog Temperament</h2>
          </div>
        </LazyLoad>

        <p>
          Here are some of the key behavioral characteristics of the Boxer
          breed:
        </p>

        <ul>
          <li>Energetic</li>
          <li>Playful</li>
          <li>Intelligent</li>
          <li>Patient</li>
          <li>Protective</li>
          <li>Brave</li>
          <li>Active and restless</li>
          <li>Headstrong</li>
          <li>Can be aggressive</li>
        </ul>

        <p>
          Boxers are often seen as the perfect family pet, because they get on
          well with children, and are generally good-natured and playful.
          However, Boxers can also show a vicious streak, and can be trained to
          attack. They are excellent watchdogs, but their protective nature can
          sometimes cause them to show unwanted aggression. A{" "}
          <b>Boxer dog bite</b> can do a lot of damage due to that strong
          underbite jaw.
        </p>

        <p>
          Most Boxer aggression problems can often be traced back to sub-par
          training as puppies. Other causes of aggression can include the dog
          dealing with pain or discomfort. These dogs are also extremely active,
          and if they are not given adequate exercise they may misbehave. Bad
          behavior may include digging, chewing, not listening to commands, and
          biting. They can also be headstrong and strong-willed, requiring a
          firm hand and effective training to be obedient.
        </p>

        <p>
          While Boxers like company, they can be aggressive around other dogs,
          especially larger dogs and those of the same gender.
        </p>

        <div>
          <table>
            <tr>
              <th>Boxer Behavior and Characteristics</th>
              <th>Rating</th>
            </tr>

            <tr>
              <td>Suitability for Inexperienced Owners</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Size</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Power</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Intelligence</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Prey Drive</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Energy</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Friendly to Strangers</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Friendly to Other Dogs</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Weather Tolerance</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>

            <tr>
              <td>Health</td>
              <td>
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "orange",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
                <FaStar
                  style={{
                    color: "black",
                    fontSize: "1.6rem",
                    margin: "0 0.5rem 0"
                  }}
                />
              </td>
            </tr>
          </table>
        </div>

        <h2 id="header5">Boxer Dog Popularity</h2>
        <LazyLoad>
          <img
            src="/images/dog-bites/vicious-boxer-dog.jpg"
            width="25%"
            className="imgleft-fluid"
            alt="vicious Boxer dog"
          />
        </LazyLoad>

        <p>
          The Boxer is one of the most popular breeds in the United States year
          after year. The American Kennel Club unveils an annual pooch
          popularity list, using the registration number associated with each
          different breed. The{" "}
          <Link
            to="https://www.akc.org/most-popular-breeds/2017-full-list/"
            target="new"
          >
            most recent figures available
          </Link>{" "}
          have Boxers at number 11 on the list – an impressive showing out of
          nearly 200 breed variations. This is also the first time in recent
          years that the Boxer is not in the top 10. As recently as 2013 it was
          at a lofty seventh place in the rankings.
        </p>

        <p>
          Perhaps the most famous Boxer dog ever was a pup named Bang Away – an
          outstanding show dog who set a record with a whopping 121 Best-in-Show
          titles at dog shows. At the top of Bang Away’s list of accomplishments
          is the 1951 crown for Best-in-Show at the prestigious{" "}
          <Link to="https://www.westminsterkennelclub.org/" target="new">
            Westminster Dog Show
          </Link>{" "}
          – the premier show in the United States. He appeared in magazines and
          newspapers, and even got priority treatment on airplanes once the
          pilots realized they had a celebrity pup on board. Bang Away’s success
          further enhanced the growing reputation and popularity of the Boxer
          breed.
        </p>

        <p>
          Of course, dogs like Bang Away are purebred and highly trained, and
          provide no hint of the devastating aggression that a Boxer dog is
          capable of.
        </p>

        <h2 id="header6">Boxer: A Working Dog</h2>

        <p>
          Boxers are classified as working dogs by the American Kennel Club,
          while the United Kennel Club regards it as a guardian dog. This breed
          is not restricted to just being a family pet, but is trained and put
          into use in a wide range of other positions.
        </p>

        <ul>
          <li>
            <b>Police Dogs –</b> Boxers are one of the preferred breeds for
            police work. They have a supreme focus and are effective in
            performing drug searches. They can also be trained to attack when
            faced with threats, and target criminals on command.
          </li>

          <li>
            <b>War Dogs –</b> Boxers have long been used in military work, and
            are one of the breeds used in the U.S. Army. Their intelligence,
            adaptability and athleticism makes them highly valued. Boxers make
            great sentries, because of their protective nature and aggression.
            They also excel in detecting explosives and taking on search and
            rescue missions.
          </li>

          <li>
            <b>Service Dogs –</b> Boxers are often used as seeing-eye dogs for
            people who are visually impaired, due to their intelligence and
            obedience when trained properly. They might also be used to help
            people who suffer other disabilities.
          </li>

          <li>
            <b>Therapy Dogs –</b> Highly-trained Boxers are sometimes used as
            therapy dogs in schools, hospitals, hospices, retirement homes, and
            other similar venues.
          </li>
        </ul>

        <p>
          The Boxer has distinguished itself in a wide variety of roles
          throughout history. When working as a police dog or military dog, this
          breed can display its bravery – as well as its fearsome attack
          capabilities.
        </p>

        <p>
          In November 1946, two British military Boxer dogs named Punch and Judy
          were awarded gallantry medals for their service. The dogs saved the
          lives of two officers who were stationed in Israel from an
          assassination attack. After detecting the assailant, the Boxers
          attacked him, suffering serious wounds but saving their soldiers in
          the process.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/SyBvlAi-kdM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          It is important to remember that Boxers used in such jobs are always
          extremely well trained to ensure their obedience. These Boxers will
          bite and attack, but they are taught by expert handlers and do so only
          on command.
        </p>

        <p>
          Problems with Boxer dogs arise when inexperienced or careless owners
          do not provide the proper training, guidance or care that they need.
        </p>

        <h2 id="header7">Owning and Training a Boxer</h2>

        <p>
          Inexperienced and irresponsible Boxer dog owners can allow their dogs
          to become mischievous, overly aggressive, and become prone to biting.
          Here are just some of the best training methods for owners to stop{" "}
          <b>Boxer puppies biting</b>, and prevent their Boxers from attacking
          humans.
        </p>

        <h3>Top 7 Tips for Training a Boxer Dog</h3>

        <ol>
          <li>
            <span>
              <b>Learn about the breed.</b> You should learn about your dog as
              an individual, as well as the Boxer breed in general. Pay
              attention to the behavioral traits of your dog and tailor your
              training accordingly.
            </span>
          </li>

          <li>
            <span>
              <b>Socialize your Boxer puppy early.</b> Boxers are naturally
              protective, and that can mean that they are distrustful of
              strangers, sometimes even attacking both people and other dogs.
              Make sure your dog learns to handle contact with others.
            </span>
          </li>

          <li>
            <span>
              <b>Use positive motivation in training.</b> Two of the best forms
              of training motivation are praise and food. Reward your dog to
              reinforce lessons and teach good behavior.
            </span>
          </li>

          <li>
            <span>
              <b>Deploy chew toys.</b> Boxer dogs can be prone to heavy chewing
              destruction. Use a variety of chew toys to control the chewing,
              help to prevent biting, and as a reward.
            </span>
          </li>

          <li>
            <span>
              <b>Give your Boxer plenty of exercise.</b> Boxers are naturally
              very energetic and athletic. One of the leading causes of
              aggressive behavior is restlessness. Keep the dog active to help
              prevent your Boxer attacking others.
            </span>
          </li>

          <li>
            <span>
              <b>Teach your dog key commands.</b> This is one of the most
              important aspects of training a Boxer and ensuring it can
              socialize without providing a danger to others. A Boxer needs to
              be taught to obey commands telling it to stop jumping at or
              attacking someone, and to give up an item it is protective of.
            </span>
          </li>

          <li>
            <span>
              <b>Keep things positive.</b> Try to make training positive.
              Negative or aggressive behavior on the part of the trainer can be
              mirrored by a Boxer.
            </span>
          </li>
        </ol>

        <h2 id="header8">Boxer Dog Bite Force</h2>

        <LazyLoad>
          <img
            src="/images/dog-bites/boxer-dog-bite-force.jpg"
            width="35%"
            className="imgright-fluid"
            alt="Boxer dog bite force"
          />
        </LazyLoad>

        <p>
          Boxers have extremely powerful jaws and a strong bite. If a Boxer
          decides that you are a threat, or attacks for another reason, there is
          a good chance that it will result in a serious bite injury.
        </p>

        <p>
          Testing the strength of a dog bite is difficult, because the power of
          a bite can vary hugely depending on the dog’s motivation. Sometimes a
          Boxer may attack with a warning bite, while other times it might be
          intending to seriously wound its target. It is impossible to put a
          number on the power of a <b>Boxer bite</b>, simply because the
          relevant testing has never been carried out. However, some experts put
          the bite force of a Boxer dog at about 230 pounds-per-square-inch
          (PSI) as a rough estimate. It is widely believed that the Boxer’s bite
          would be in the top five of bite power statistics across different dog
          breeds.
        </p>

        <p>
          The Boxer is genetically designed to bite. Its ancestor, the
          Bullenbeisser, was bred specifically for hunting. The Boxer shares
          many traits with this extinct breed. They have short muzzles with an
          underbite, allowing it to clamp its teeth around victims with its
          incredibly strong jaw. Some breeders also believe the Boxer's wide
          nostrils are designed to help it breathe while its teeth are clamped
          onto a target.
        </p>

        <h2 id="header9">Boxer Dog Attacks</h2>

        <p>
          Between its biting power, significant size, and powerful, muscular
          body, a Boxer dog can be deadly. This is why the breed is a mainstay
          on lists chronicling the most
          <b> dangerous dogs</b> around.
        </p>

        <h3>Boxer Attacks by the Numbers</h3>

        <p>
          Dog attack statistics have been produced through detailed studies,
          including{" "}
          <Link
            to="https://www.dogsbite.org/pdf/dog-attack-deaths-maimings-merritt-clifton-2014.pdf"
            target="new"
          >
            one study
          </Link>{" "}
          which examined dog attack reports over a 32-year period. The numbers
          from this data show that Boxer dogs are the eighth most dangerous
          breed. Over that time period, there were 74 reported Boxer dog attacks
          which caused serious bodily harm, as well as nine deaths. This places
          Boxers ahead of other dangerous dog breeds, such as Akitas and Chows.
        </p>
        <LazyLoad>
          <img
            src="/images/dog-bites/boxer-dog-attack-statistics.jpg"
            width="45%"
            className="imgright-fluid"
            alt="Boxer dog attack statistics"
          />
        </LazyLoad>

        <p>
          <b>
            <u>Top 15 Dangerous Dog Breeds by Attack Numbers</u>
          </b>
        </p>

        <ol>
          <li>
            <span>Pit Bull</span>
          </li>
          <li>
            <span>Rottweiler</span>
          </li>
          <li>
            <span>German Shepherd</span>
          </li>
          <li>
            <span>Presa Canario</span>
          </li>
          <li>
            <span>Husky</span>
          </li>
          <li>
            <span>Wolf Hybrid</span>
          </li>
          <li>
            <span>Labrador</span>
          </li>
          <li>
            <span>
              <mark>Boxer</mark>
            </span>
          </li>
          <li>
            <span>Akita</span>
          </li>
          <li>
            <span>Chow</span>
          </li>
          <li>
            <span>Great Dane</span>
          </li>
          <li>
            <span>Mastiff</span>
          </li>
          <li>
            <span>Doberman</span>
          </li>
          <li>
            <span>Cane Corso</span>
          </li>
          <li>
            <span>Bulldog</span>
          </li>
        </ol>

        <p>
          <strong>
            {" "}
            <Link to="http://ht.ly/Yj8s30mLQZc" target="new">
              Dog Bite Statistics in the U.S (Infographic)
            </Link>
            .
          </strong>{" "}
          Download or share our dangerous breeds dog bite info-graphic to
          educate others on the dangers of approaching a dangerous dog.
        </p>
        <h3>Boxer Dog Bite Incidents</h3>

        <p>
          These are just some of the fatal incidents involving people being{" "}
          <b>mauled to death by Boxer dogs</b> which have made headlines in
          recent years.
        </p>

        <ul>
          <li>
            <b>Pablo Fleites, aged 56 – 2006 – Florida</b>
            Fleites was mauled to death by a male Boxer, owned by his employer,
            after walking into the dog’s pen. The dog was euthanized.
          </li>

          <li>
            <b>Michael Landry, aged 4 – 2009 – Louisiana</b>
            Youngster Michael Landry was playing in the yard outside his home
            when he was{" "}
            <Link
              to="https://blog.dogsbite.org/2008/07/2006-fatality-56-year-old-pablo-rudolfo.html"
              target="new"
            >
              {" "}
              attacked and killed by three Boxers
            </Link>
            . The dogs belonged to the family next door and had been let out of
            their kennel while it was cleaned.
          </li>

          <li>
            <b>Tom Vick, aged 64 – 2013 – Arizona</b>
            Former teacher Tom Vick, and his wife Diane (a former mayor) tried
            to break up a fight between their two dogs – a Boxer and a smaller
            cocker spaniel. The Boxer{" "}
            <Link
              to="https://www.usatoday.com/story/news/nation/2013/12/29/arizona-dog-attack-death/4243151/"
              target="new"
            >
              {" "}
              attacked its owners
            </Link>
            , killing Tom and seriously injuring Diane.
          </li>
        </ul>

        <p>
          These are just some of the recent violent deaths caused by Boxer dogs.
          While Boxers can be calm, good-natured, and friendly family pets, you
          can see that this is not always the case. Perhaps these violent Boxers
          were provoked, badly trained, feared a threat, or became distrustful
          and aggressive toward humans due to mistreatment. No matter the
          reason, the statistics and past incidents show that people should be
          wary around these dogs. Those who are too complacent could become the
          next Boxer bite victims.
        </p>

        <h2 id="header10">Surviving a Boxer Attack</h2>

        <p>
          Here are a few steps you can take to survive an attack or a bite by a
          Boxer.
        </p>

        <ol>
          <li>
            <span>
              Avoid putting yourself in a dangerous position if possible. If you
              see a hostile-looking Boxer, be aware of how dangerous it can be,
              and steer clear of it.
            </span>
          </li>

          <li>
            <span>
              If you are confronted by an aggressive Boxer, stay still and keep
              your arms down, look away and do not make any sudden moves or loud
              noises.
            </span>
          </li>

          <li>
            <span>
              Use a makeshift shield. A Boxer bite is strong, and you should use
              anything you can as a buffer between its teeth and yourself.
            </span>
          </li>

          <li>
            <span>
              Stay on your feet. Boxers are muscular, powerful, and can have a
              substantial weight and size. They will try to drag you off your
              feet and are at their most dangerous when facing a grounded
              victim.
            </span>
          </li>

          <li>
            <span>
              Fight or flight. When you are trying to fend off a vicious Boxer
              attack, you might reach the point when you need to fight back. You
              need to target sensitive areas and use any weapons available to
              you. Get to safety as soon as possible – climb to a high point
              that the dog cannot reach, or put a solid door or wall between you
              and the attacking animal.
            </span>
          </li>

          <li>
            <span>
              Get medical attention. If you have been bitten by a Boxer, you
              will probably need treatment. The severity of your bite injury
              will dictate the kind of treatment you will need.
            </span>
          </li>
        </ol>

        <h2 id="header11">Boxer Bite Legal Action and Compensation</h2>

        <LazyLoad>
          <img
            src="/images/dog-bites/boxer-aggression-problems.jpg"
            width="38%"
            className="imgleft-fluid"
            alt="Boxer aggression problems"
          />
        </LazyLoad>

        <p>
          It can be difficult decision for some people to pursue police and
          legal action after being attacked or bitten by a Boxer dog. For many
          dog owners, their pet is a huge part of their family. No one wants to
          be the reason a pet is put down, but that does not mean that you
          should not take action.
        </p>

        <p>
          If a dog is dangerous, and has bitten and caused harm to a person, it
          is an extremely serious matter. In the state of California,{" "}
          <Link to="/dog-bites/california-dog-bite-laws" target="new">
            liability for a dog attack
          </Link>{" "}
          fall solely on the owner. It is their responsibility to ensure that
          their dog is safe to be around and does not harm others. If an owner
          is careless or negligent, other people will pay the price.
        </p>

        <p>
          Any legal action will depend on the circumstances of the attack.
          However, in many cases, victims are entitled to seek compensation.{" "}
          <b>Dog attacks</b> can be terrifying and traumatizing events which
          affect victims for years to come. They can also leave people with
          serious injuries, from puncture and tear wounds to broken bones and
          torn tendons.
        </p>

        <p>
          An expert dog attack attorney can help victims seek compensation for
          the pain and suffering they have endured.
        </p>

        <p>
          Expected compensation will vary depending on the specifics of a dog
          bite attack, but may be impacted by a victim’s medical bills, lost
          wages, and ongoing care expenses, among other factors.
        </p>

        <h2 id="header12">Documenting a Dog Attack</h2>

        <p>
          Victims can help their legal case by documenting the attack incident.
          This could include:
        </p>

        <ul>
          <li>Writing a detailed personal account of the event</li>
          <li>
            Taking pictures and video footage of the attack, as well as the
            scene of the incident
          </li>
          <li>Securing police and medical reports</li>
          <li>Obtaining statements from anyone who witnessed the attack</li>
          <li>Documenting any injuries, with photos of all stages</li>
        </ul>

        <p>
          Make sure this information is organized in a clear and ordered way to
          help your dog bite lawyer build an effective case.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bisnar Chase Staff"
            style={{
              backgroundImage:
                "url('/images/dog-bites/boxer-dog-bite-compensation-attorneys.jpg')"
            }}
          >
            <h2 id="header13">No Win, No Fee Help From Bisnar Chase</h2>
          </div>
        </LazyLoad>

        <p>
          For those seeking representation from a firm with an outstanding
          reputation, look no further than Bisnar Chase. Our firm has developed
          an impressive track record over 40 years in business, with a 96%
          success rate and more than{" "}
          <Link to="/case-results" target="new">
            $500 million won for our clients
          </Link>
          .
        </p>

        <p>
          Bisnar Chase has dedicated <b>dog bite attorneys</b> who excel in
          dealing with these cases. They know how traumatic an attack can be,
          and will go above and beyond to ensure clients receive the best
          possible compensation.
        </p>

        <p>
          We offer a ‘No Win, No Fee’ guarantee. This means that if we don’t win
          your case, you do not owe us a cent. Allow the experienced dog bite
          lawyers at Bisnar Chase to fight for you by calling
          <b> 877-302-1639</b> now.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
