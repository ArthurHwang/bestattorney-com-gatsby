// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/akita-dangerous-dog-breeds.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="About Akitas - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="Akitas are as unpredictable as they are beautiful. Or are they as compassionate as they are powerful? They are popular, loving & potentially deadly. Learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Akita Dog Breeds</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="akita dangerous dog breed"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          They are unique, they are powerful, beautiful and majestic. Like all
          other breeds, the Akita has the ability to be a family companion and
          sweetheart, or aggressive and deadly attack dog.
        </p>
        <p>
          When you look at an Akita, you can probably see a mix of a few other
          dog breeds, that make up their overall appearance, but Akitas are a
          breed to be reckoned with, and this page will cover the reasons why.
        </p>
        <p>
          If you or a loved one has been attacked, injured or killed as result
          of an Akita or other dangerous breed of dog, first seek medical
          attention to ensure the health and safety of the victim.
        </p>
        <p>
          Immediately follow up with our team of skilled and experienced Dog
          Bite Lawyers and receive your Free consultation and case evaluation,
          call 800-561-4887.
        </p>

        <div style={{ marginBottom: "2rem" }}>
          {" "}
          <Link to="/dog-bites/akita-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of the Akita Dog Breed?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              What Reputation Do Akitas Have?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Temperament of an Akita{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning an Akita{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are Akitas More Aggressive Than Other Breeds?{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Medical Attention After an Akita Dog Bite{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Document Your Akita Dog Bite Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Win Your Dog Attack Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/akita-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              We Will Win or You Don't Pay{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <div id="header1"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Akita breed history"
            style={{
              backgroundImage:
                "url('/images/text-header-images/types-of-akitas.jpg')"
            }}
          >
            <h2>History of the Akita Dog Breed</h2>
          </div>
        </LazyLoad>

        <p>
          Originally, Akitas are thought to have originated from Japan in the
          1600's. Helen Keller brought the first Akita over and into America.
          Although Keller was extremely satisfied and delighted with her Akita,
          Kamikaze-go, he died at an early age due to{" "}
          <Link
            to="https://dogtime.com/dog-health/general/307-distemper"
            target="new"
          >
            distemper
          </Link>
          .
        </p>
        <p>
          The Japanese government heard this news and gave Helen Kamikaze's
          older brother, Kanzan-go. Keller wrote that the Akita breed was a
          wonderful companion, being gentle and trustworthy.
        </p>
        <p>
          American soldiers who were stationed in Japan during WWII brought back
          more Akitas with them. The first Akita Stud was created by Thomas
          Boyd, in 1956.
        </p>
        <p>
          The Akita breed developed into a much larger and robust dog than the
          original Akitas from Japan, which throughout time and even today is a
          controversial topic. While there are many who are true to the Japanese
          standard of the smaller version of the breed, the larger American
          version of the Akita were and are valued by many for their larger size
          and more powerful features.
        </p>
        <p>
          In 1972, the{" "}
          <Link to="https://www.akc.org/dog-breeds/akita/" target="new">
            American Kennel Club (AKC)
          </Link>{" "}
          accepted the Akita breed officially and the{" "}
          <Link to="https://www.akitaclub.org/" target="new">
            {" "}
            Akita Club of America
          </Link>
          , founded in 1960.
        </p>
        <p>
          The Akita breed has become known and respected for their loyalty and
          companionship. Akitas are also known for their fearlessness. These
          traits were put to the test, when the London Zoo had a Sumatran tiger
          cub that was orphaned and needed help being raised. The London Zoo
          decided to put an Akita puppy in with the young orphaned cub to help
          raise each other. The Akita had enough fur to protect him from the
          cubs sharp claws and had the power, toughness and companionship to be
          a loyal friend, for roughhousing and building each other up for
          adulthood. Once the cub started becoming of adult-age, the couple was
          separated, for obvious safety reasons.
        </p>

        <p>
          If you have been attacked or experienced a dog bite from an Akita or
          other dog breed, you are always urged to seek medical attention
          immediately and to contact a team of skilled and highly experienced{" "}
          <Link to="/dog-bites" target="new">
            Dog Bite Lawyers
          </Link>
          .
        </p>

        <p>Call us for a Free consultation at 800-561-4887.</p>
        <LazyLoad>
          <img
            src="/images/text-header-images/history-of-the-akita.jpg"
            width="100%"
            alt="akita breed history"
          />
        </LazyLoad>

        <h2 id="header2">What Reputation Do Akitas Have?</h2>
        <p>
          Akitas may be the type of breed where you ask the owner, "What kind of
          dog is that?" Because they are not as mainstream or as popular as:
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/labrador-dog-breed">Labrador</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/pit-bull-dog-breed">Pit Bull</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed">
              Doberman Pinscher
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/rottweiler-dog-breed">Rottweiler</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chow
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds" target="new">
              Mastiff
            </Link>
          </li>
        </ul>
        <p>
          Akitas have a blend of looks from other dogs, but maintain a solid and
          significant presence in the breeds to be reckoned with.
        </p>
        <p>
          Strong, powerful ans smart, these impressive and beautiful dogs come
          from Japan. Although the true Akita breed from Japan are significantly
          smaller than most of the Akitas in America and other countries around
          the world.
        </p>
        <p>
          Even though, like many other breeds, Akitas were used for herding and
          protection/security, Akitas are a favorite among households,
          especially with women and children because of their loyalty, ability
          to adapt to many types of environments, situational factors and
          confidence.
        </p>
        <p>
          Although this Akita breed can be loving, caring and protective over
          their family, Akitas also have the ability, like every dog breed, to
          become or turn out aggressive and hostile.
        </p>
        <p>
          If neglected, abused or presented with an environment that is
          suggestive towards aggressive behavior, these dogs can become vicious,
          due to their mindset, power and agility.
        </p>
        <p>
          Even though these beautiful dogs have the ability to be sweet, kind
          and loving, aggressive, dangerous and demeaning, or somewhere in
          between, Akitas carry a reputation of being somewhat of a "novelty
          dog."
        </p>
        <p>
          By definition, novelty means the quality of being new or original, or
          unusual, and even though these dogs aren't different enough to be
          considered a different species, Akitas have very unique traits and a
          rich history filled with culture, responsibility and praise.
        </p>
        <p>
          Akitas are considered an honorable breed and are often taken in as one
          of the family. A large reason due to Akitas not being inexpensive or
          easy to take care of. It takes hard work, determination, dedication,
          love and support to keep an Akita happy, properly exercised, fed and
          kept healthy.
        </p>
        <p>
          Like many other dogs, Akitas are prone to many health problems, as
          described below in <strong>Owning An Akita</strong>.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/akita-attacking-other-dog.jpg"
            width="100%"
            alt="akita dog bite statistics"
          />
        </LazyLoad>

        <h2 id="header4">Temperament of an Akita</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/akita-temperament.jpg"
            width="350"
            className="imgright-fixed"
            alt="akita temperament"
          />
        </LazyLoad>
        <p>
          Akitas are very well-known for their beautiful and fluffy coat,
          striking eyes and bold personality, as well as:
        </p>
        <ul>
          <li>Typically averaging between 10-12 years life span</li>

          <li>Normally between 70-130 lbs</li>
          <li>Shoulder height approximately 2 to 2 1/2ft</li>
        </ul>
        <p>
          The Akita resembles a mix of a few breeds; you could say their
          appearance resembles that of a Labrador, Siberian Husky, Mastiff
          breeds and depending on the mother and father, looks can range into
          features similar to other breeds.
        </p>
        <p>
          Although Akitas have a very strong presence, with a bold mindset,
          strong mentality as well as a physically strong body, stance and
          obvious confidence in their stance and body language, Akitas have a
          very strong loyalty, dedication and responsibility to protect their
          owners, handlers and family/loved ones.
        </p>
        <p>
          Akitas are very gentle with children and have been known to rescue or
          save people in trouble. There is a famous story about an Akita that
          was so loyal to it's owner, that when it's owner passed away, the
          Akita still traveled every day to greet it's owner, as usual, but
          found nobody to greet. The Akita continued this until they passed as
          well. Loyalty and dedication values are strong with this breed.
        </p>
        <p>
          But, as with all types of dogs and animals, no two animals are alike.
          When dogs, animals or any type of other animals experience neglect, a
          poor or dangerous environment and are abused from a young age or even
          at all, it can cause their temperament to fluctuate dramatically.
        </p>
        <p>
          Even if an Akita has not shown any previous signs or history of
          aggressive behavior, they can still suddenly become hostile and
          dangerous.
        </p>
        <p>
          If you have experienced an aggressive Akita or other dog breed, and
          have been injured or severely traumatized, seek medical attention as
          soon as possible and follow up with a Free consultation with one of
          our skilled Dog Bite Lawyers. Call 800-561-4887.
        </p>

        <h2 id="header5">Owning an Akita</h2>

        <p>
          Owning any dog is a massive responsibility. Not only are you in charge
          of feeding, bathing, playing training and taking care after them, but
          you must maintain their accessibility to others, making sure they
          don't pose a risk to others in any way.
        </p>
        <p>
          Akitas have a lot of energy, which means lots of exercise. If you are
          thinking about getting an Akita, here are some points to consider:
        </p>
        <ul>
          <li>High levels of energy and the need for consistent exercise</li>
          <li>Not for apartment lifestyles</li>
          <li>Very loyal and affectionate towards family/loved ones</li>
          <li>High levels of shedding/grooming maintenance</li>
        </ul>
        <p>
          While Akitas are highly playful, their ability to have their energy
          plus playfulness/roughhousing get out of control as result of their
          bold mentality and strong physique. This is a potentially dangerous
          situation.
        </p>
        <p>
          Full size puppies or young adults can confuse playtime with a sudden
          rush of excitement which could potentially result in an accidental
          bite or attack from nervousness. Even though they are puppies, their
          muscles and strength are not far behind an adult Akita.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/akita-breed-characteristics-chart.JPG"
            width="50%"
            className="imgcenter-fluid"
            alt="akita breed characteristics infographic"
          />
        </LazyLoad>
        <pre>
          Photo courtesy of{" "}
          <Link to="https://dogtime.com/dog-breeds/akita#/slide/1" target="new">
            Dogtime.com
          </Link>{" "}
        </pre>

        <h2 id="header6">Are Akitas More Aggressive Than Other Breeds?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/owning-an-akita.jpg"
            width="40%"
            className="imgright-fluid"
            alt="owning-an-akita"
          />
        </LazyLoad>

        <p>
          Is there a solid way of identifying if this breed is more aggressive
          than other breeds? Typically this is done by identifying breed
          characteristics, breed specific statistics and comparing them. But
          there is a problem with this process, in all fairness.
        </p>
        <p>
          When a dog bite occurs, the majority of the time it is only reported
          if the injury is serious enough to seek medical treatment or some sort
          of traumatic experience was encountered.
        </p>
        <p>
          But when analyzing the aggressiveness or level of hostility in
          specific breeds then compared to one another, the process should be
          different, and more realistic.
        </p>
        <p>
          Many little dogs bite or are very aggressive. Most of their
          "aggressive behavior" is harmless and actually quite funny from time
          to time. Smaller dogs bark and growl based on a few things:
        </p>
        <ul>
          <li>Territorial</li>
          <li>Feeling Threatened</li>
          <li>Confusion</li>
          <li>Fear</li>
          <li>Over-Excitement</li>
          <li>Abuse/Neglect</li>
          <li>Being a stray</li>
        </ul>
        <p>
          Akita dog bites are more likely to cause serious damage than a smaller
          dog, but that doesn't mean that the Akita breed is more aggressive
          than other breeds, like say Chihuahua's for example. Chihuahua's can
          be very "yappy" and are know to be ankle biters.
        </p>
        <p>
          Not to say that Akitas are not dangerous or are dangerous, it's
          important to remember to stay aware of your surroundings, body
          language and possible signals you could be unintentionally giving an
          aggressive Akita or other dog breed.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/zaZ2REGsTjc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Akitas are not a breed that can stay indoors for long periods of time.
          This breed is very energetic, carry lots of power and boldness. They
          need to be able to get as much exercise as possible otherwise run the
          risk of becoming aggressive and even violent.
        </p>
        <p>
          Another good reason to have outdoor space for these dogs is because
          they are know to be very vocal. Whether they are barking, howling,
          whimpering or "talking," they run a high risk of having the police or
          animal services called, if your neighbors get annoyed easily.
        </p>
        <div id="header8"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Dog Bite Attorneys"
            style={{
              backgroundImage:
                "url('/images/text-header-images/medical-attention-akita-dog-attack.jpg')"
            }}
          >
            <h2>Medical Attention After an Akita Dog Bite</h2>
          </div>
        </LazyLoad>

        <p>
          Not everyone feels they need to go to the hospital, go to urgent care
          or even follow up with a doctor after being attacked or bitten by a
          dog. The problem with not seeking medical attention is that you run
          the risk of dangerous side-effects and symptoms of dog bites.
        </p>
        <p>
          After getting bitten or attacked by a dog or Akita, make sure you do
          the following:
        </p>
        <ul>
          <li>
            Make sure your environment is safe to call for help (Do your best to
            not endanger others)
          </li>
          <li>
            Give as much information about your attack experience as possible
          </li>
          <li>Be honest and do not downplay anything</li>
          <li>Listen to your doctor/health care provider</li>
          <li>Treat as directed</li>
          <li>Document everything, especially throughout your treatments</li>
          <li>Follow up with a skilled Dog Bite Lawyer</li>
        </ul>

        <h2 id="header9">How To Document Your Akita Dog Bite Injuries</h2>
        <p>
          Not only is it important to ensure you and your loved ones safety and
          well-being, but this is a perfect way of having your injuries
          documented in association with your dog bite experience.
        </p>
        <p>
          Later on in your dog bite case, it can become very difficult and
          complex to prove that the "injuries" you state are a result of the dog
          bite, if you have no proof of them being associated with each other.
        </p>
        <p>
          You can start by collecting and saving as much evidence from the scene
          of the Akita attack, any video or photos you can capture of the
          incident, injuries, location, and this information will also be
          beneficial for many reasons.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/documenting-akita-dog-bite-organization.jpg"
            width="100%"
            alt="dog bite documentation"
          />
        </LazyLoad>
        <h2 id="header10">How To Win Your Dog Attack Case</h2>
        <p>
          Dog bite or Akita attack cases can become complex and difficult to
          win, if the proper preparations and organization is not taken care of.
          Here are some examples of crucial information you will want to make
          sure you have, to give you the best chances of winning your case and
          receiving maximum compensation:
        </p>
        <ul>
          <li>Documented medical information</li>
          <li>Police report</li>
          <li>
            Photos/video (where the incident took place, injuries, those
            involved, etc.)
          </li>
          <li>Any Evidence</li>
          <li>Eye-witnesses accounts</li>
          <li>
            Be organized and prepared: have a statement identifying the date,
            time, location, those involved, summary of what happened and any
            other information your lawyer identifies is an important piece of
            information for your case
          </li>
        </ul>

        <div
          className="text-header content-well"
          title="Dog Bite Attorneys"
          style={{
            backgroundImage:
              "url('/images/text-header-images/akita-dog-bite-injury-lawyers.jpg')"
          }}
        >
          <h2 id="header11">We Will Win or You Don't Pay</h2>
        </div>

        <p>
          Have you or a loved one experienced an Akita attack or bite? If so,
          you could be entitled to compensation, medical costs and other
          financial obligations to be covered. Just call
          <strong> 800-561-4887</strong> for your free consultation and case
          evaluation with one of our skilled <strong>Dog Bite Lawyers</strong>.
        </p>
        <p>
          Our team of legal professionals and experienced{" "}
          <strong>Dog Bite Attorneys</strong> have been representing, fighting
          and winning dog bite cases for <strong>over 40 years</strong>. With
          over
          <strong> $500 Million</strong> won, we have established an impressive{" "}
          <strong>96% success rate</strong> and would like the opportunity to
          represent your case.
        </p>
        <p>
          If you are concerned about the cost of our representation, know this -
          if we don't win, you don't pay. All costs are discussed in the
          beginning and you will not be surprised with any hidden fees or costs.
        </p>
        <p>
          If you have financial obligations you need taken care of now, we are
          happy to help you in any ways we can. Discuss your case with us at no
          charge or strings attached, just
          <strong> call 800-561-4887</strong>.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
