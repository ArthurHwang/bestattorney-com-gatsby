// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Dog Bite Laws and Regulations By County - Bisnar Chase"
        pageDescription="Understanding California Dog Bite Laws can make a difference between winning your case, receiving compensation & not. Check our list of Regulations by County for dog attacks to learn more."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California's Dog Bite Laws and Regulations by County</h1>
        <BreadCrumbs location={location} />

        <p>
          Dog bite lawyer <Link to="/attorneys/john-bisnar">John Bisnar</Link>{" "}
          presents legal information on California dog bite laws. If you've been
          bitten by a dog please contact our office for a free consultation.
          Call 1-800-561-4887.
        </p>
        <div></div>
        <p>
          The laws pertaining to{" "}
          <Link
            to="https://www.pinterest.com/pin/180214422575428887/"
            target="_blank"
          >
            dog attacks
          </Link>{" "}
          and dog bites comprise civil, criminal and administrative laws. Civil
          laws provide victims the opportunity to seek monetary compensation
          while criminal laws impose punishments on the dog owner, and
          administrative laws provide for a remedy against the dog itself -
          typically euthanization. These laws (if in place at all) vary by city,
          county and state. As the incidence of dog bites and attacks increases,
          more local communities are putting dangerous dog laws in place,
          providing protection and legal recourse for an increasing number of
          victims.
        </p>
        <p>
          Some counties have regulations, punishments and fines pertaining to
          dogs that are deemed vicious or dangerous. We have included the county
          laws for all of Southern California's counties:
        </p>
        <ul type="disc">
          <li>
            {" "}
            <Link to="/dog-bites/orange-county-laws">Orange County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/los-angeles-laws">Los Angeles County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/san-luis-obispo-laws">
              San Luis Obispo County
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/kern-county-laws">Kern County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/santa-barbara-laws">Santa Barbara County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/ventura-county-laws">Ventura County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/san-bernardino-laws">
              San Bernardino County
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/riverside-county-laws">Riverside County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/san-diego-laws">San Diego County</Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/imperial-county-laws">Imperial County</Link>
          </li>
        </ul>
        <p>
          The following is the text of California's dog bite law or what is
          commonly referred to as the "strict liability statute":
        </p>
        <h2>Calif. Civil Code § 3342.</h2>
        <ol start="1" type="A">
          <li>
            The owner of any dog is liable for the damages suffered by any
            person who is bitten by the dog while in a public place or lawfully
            in a private place, including the property of the owner of the dog,
            regardless of the former viciousness of the dog or the owner's
            knowledge of such viciousness. A person is lawfully upon the private
            property of such owner within the meaning of this section when he is
            on such property in the performance of any duty imposed upon him by
            the laws of this state or by the laws or postal regulations of the
            United States, or when he is on such property upon the invitation,
            express or implied, of the owner.
          </li>
          <li>
            Nothing in this section shall authorize the bringing of an action
            pursuant to subdivision (a) against any governmental agency using a
            dog in military or police work if the bite or bites occurred while
            the dog was defending itself from an annoying, harassing, or
            provoking act, or assisting an employee of the agency in any of the
            following:
          </li>
          <ul type="circle">
            <li>
              In the apprehension or holding of a suspect where the employee has
              a reasonable suspicion of the suspect's involvement in criminal
              activity.
            </li>
            <li>In the investigation of a crime or possible crime.</li>
            <li>In the execution of a warrant.</li>
            <li>In the defense of a peace officer or another person.</li>
          </ul>
        </ol>
        <p>
          Subdivision (b) shall not apply in any case where the victim of the
          bite or bites was not a party to, nor a participant in, nor suspected
          to be a party to or a participant in, the act or acts that prompted
          the use of the dog in the military or police work.
        </p>
        <p>
          Subdivision (b) shall apply only where a governmental agency using a
          dog in military or police work has adopted a written policy on the
          necessary and appropriate use of a dog for the police or military work
          enumerated in subdivision (b).
        </p>
        <h2>Calif. Civil Code § 3342.5</h2>
        <ol start="1" type="A">
          <li>
            The owner of any dog that has bitten a human being shall have the
            duty to take such reasonable steps as are necessary to remove any
            danger presented to other persons from bites by the animal.
          </li>
          <li>
            Whenever a dog has bitten a human being on at least two separate
            occasions, any person, the district attorney, or city attorney may
            bring an action against the owner of the animal to determine whether
            conditions of the treatment or confinement of the dog or other
            circumstances existing at the time of the bites have been changed so
            as to remove the danger to other persons presented by the animal.
            This action shall be brought in the county where a bite occurred.
            The court, after hearing, may make any order it deems appropriate to
            prevent the recurrence of such an incident, including, but not
            limited to, the removal of the animal from the area or its
            destruction if necessary.
          </li>
          <li>
            Whenever a dog trained to fight, attack, or kill has bitten a human
            being, causing substantial physical injury, any person, including
            the district attorney, or city attorney may bring an action against
            the owner of the animal to determine whether conditions of the
            treatment or confinement of the dog or other circumstances existing
            at the time of the bites have been changed so as to remove the
            danger to other persons presented by the animal. This action shall
            be brought in the county where a bite occurred. The court, after
            hearing, may make any order it deems appropriate to prevent the
            recurrence of such an incident, including, but not limited to, the
            removal of the animal from the area or its destruction if necessary.
          </li>
          <li>
            Nothing in this section shall authorize the bringing of an action
            pursuant to subdivision (b) based on a bite or bites inflicted upon
            a trespasser, or by a dog used in military or police work if the
            bite or bites occurred while the dog was actually performing in that
            capacity.
          </li>
          <li>
            Nothing in this section shall be construed to prevent legislation in
            the field of dog control by any city, county, or city and county.
          </li>
          <li>
            Nothing in this section shall be construed to affect the liability
            of the owner of a dog under Section 3342 or any other provision of
            the law.
          </li>
          <li>A proceeding under this section is a limited civil case.</li>
        </ol>
        <h2>Protecting Your Rights</h2>
        <p>
          California is one of several states that has a "strict liability
          statute" when it comes to dog attacks. This means that dog owners are
          financially responsible for compensating injured victims for the
          damages and losses caused by their pets. Injured victims may be
          entitled to damages including but not limited to medical expenses,
          hospitalization and surgery costs, pain and suffering and emotional
          distress. The knowledgeable{" "}
          <Link to="/dog-bites">California dog bite lawyers</Link> at Bisnar
          Chase understand the physical, emotional and financial repercussions a
          dog attack can cause for victims and their families. Please contact us
          to obtain more information about pursuing your legal rights.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
