// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight, FaStar } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/chow-chow-dog-breed.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="About Chow Chows - Dangerous Dog Breeds & Dog Attacks"
        pageDescription="The Chow Chow is an incredibly beautiful, majestic & powerful dog breed, but they have the capability to attack, cause severe injuries & even death. Learn More."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Chow Chow Dog Breed</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="chow chow dog breed"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          <strong>Chows: </strong>A beautiful breed that combines elegance and
          power, history and loyalty. The dignity these dogs carry in their
          confident posture is a perfect example of why these strong-minded dogs
          are loved so much.
        </p>
        <p>
          But with all dog beeds, no one kind of dog is excluded from the risk
          of an attack or bite. With the sheer power the Chow exudes, owners
          must use caution to keep their Chow Chows locked up and away from
          others,especially if their Chow has a history of aggressive behaviors.
        </p>
        <p>
          This page gives a break down on the Chow Chow breed. If you have been
          attacked by a Chow Chow or other dog breed and you need information on
          what to do next, contact us for your Free consultation and case
          evaluation at <strong>800-561-4887.</strong>
        </p>

        <div style={{ marginBottom: "2rem" }} align="center">
          {" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header1">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Getting Attacked by a Chow Chow
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header2">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              7 Tips to Remember When You're Getting Attacked by a Dog{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header3">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              History of the Chow Chow
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header4">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Reputation of the Chow Chow
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header5">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Temperament of the Chow Chow
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header6">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Owning a Chow Chow{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header7">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Are Chow Chows More Aggressive Than Other Breeds?
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header8">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              Medical Attention After a Chow Chow Dog Bite
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header9">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Document Your Chow Chow Dog Bite Injuries{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header10">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              How To Win Your Dog Attack Case{" "}
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>{" "}
          <Link to="/dog-bites/chow-chow-dog-breed#header11">
            <DefaultGreyButton className="btn btn-primary active nav-button">
              We Will Win or You Don't Pay
              <FaRegArrowAltCircleRight className="arrow-right" />
            </DefaultGreyButton>
          </Link>
        </div>

        <h2 id="header1">Getting Attacked by a Chow Chow</h2>
        <p>
          Dog bites, animal attacks and any unwanted physical confrontations can
          be mentally traumatic, physically dangerous and overall an emotional
          rollercoaster.
        </p>
        <p>
          Many people never figure they could ever be in the situation of coming
          face to face with an aggressive dog. That's why it is always a smart
          idea to plan ahead for everything, responsibly, appropriately, and
          effectively. Here are some tips to help you avoid a dog attack.
        </p>
        <h2 id="header2">
          7 Tips to Remember When You're Getting Attacked by a Dog
        </h2>
        <p>
          Imagine this: You are walking down the sidewalk on your way to school,
          the store or a friends house, and you come to a large empty dirt lot
          between houses. You start walking through it and all of a sudden, you
          hear growling... You turn around and 20 feet in front of you is a
          large, growling black dog with foam around its mouth - what do you do?
          Nothing to jump on, nothing to defend yourself with; what do you do?
        </p>
        <p>
          First, let's take a look at this brief video, <em>How to</em> Survive{" "}
          <em>a Dog Attack</em>, by{" "}
          <Link
            to="https://www.youtube.com/channel/UC4rlAVgAK0SGk-yTfe48Qpw"
            target="new"
          >
            Bright Side
          </Link>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/vX-OOfbnD9w"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Whether it's a small, medium or large-sized dog, all confrontations
          can be quite frightening and feel like a long and hostile standoff.
          Sometimes, that's all it is, is a staring contest. But often, the dog
          feels threatened, confused, or a long list of other reasons why dogs
          attack, and could result in serious injuries or even death.
        </p>
        <p>
          Here are{" "}
          <strong>
            7 Tips to Remember When You're Getting Attacked by a Dog
          </strong>
          :
        </p>
        <ol>
          <li>
            Make sure you stay standing on your feet and never get down on the
            ground or allow the dog on top of you, if you are able to keep it
            from happening.
          </li>
          <li>
            If possible, keep or place an object/item between you and the
            attacking dog, such as a purse, backpack, skateboard, bike, even a
            cell phone - anything that can keep a protective barrier between you
            and the snapping jaws of the aggressive and attacking dog.
          </li>
          <li>
            <strong>Three Things to make sure you protect </strong>during the
            dog attack:
            <ol>
              <li>
                <strong>Face</strong>
              </li>
              <li>
                <strong>Neck</strong>
              </li>
              <li>
                <strong>Groin</strong>
              </li>
            </ol>
          </li>
          <li>
            Never pull away from a bite or show that you are struggling to "hold
            your own." Don't show any signs of weakness.
          </li>
          <li>
            Strike the dog continuously in a vital area (head, back, abdomen,
            genitals, eyes/face/mouth. Use any weapons available or
            objects/items that will make the dog want to stop and protect their
            safety from your defensive attack.
          </li>
          <li>
            Try and always appear larger, taller, louder, brighter and more
            powerful at all times.
          </li>
          <li>Never turn your back and run away from the dog.</li>
        </ol>

        <h2 id="header3">History of the Chow Chow Dog Breed</h2>
        <p>
          The Chow Chow breed is one of the world's oldest dog breeds, dating
          back to before the Han Dynasty (206 B.C.)
        </p>
        <p>
          Circa eighth century, an emperor of the Tang Dynasty was believed to
          have owned a facility with 5,000 Chow Chows and a staff of twice as
          many as permanent and full-time keepers.
        </p>
        <p>
          Throughout time, the Chow Chow breed earned their stripes as guarders,
          haulers and hunters. Although the Chow Chow is not considered a
          "sport/sporting" type, they have an innate sense for tracking and
          hunting.
        </p>
        <p>
          With the combination of a dynamic persona, serious minded power-houses
          of stamina, deep skill and strategy, as well as loyalty unlike most
          other dog breeds, the chow holds it's place as the #74 out of 194
          other breeds.
        </p>

        <center>
          <h3>Did You Know?</h3>
        </center>
        <ul>
          <li>
            An ancient nickname for the Chow is derived from a Cantonese word
            meaning "edible."
          </li>
          <li>
            Chow Chows were put on exhibit at the London Zoo in the 1820's, and
            marketed as the "Wild Dogs of China."
          </li>
          <li>
            {" "}
            <Link
              to="https://chowtales.com/the-palace-chow-was-this-queen-victoria-or-alexandras-chow/"
              target="new"
            >
              Queen Victoria popularized the Chow
            </Link>{" "}
            for the Western World later in the 1890's.
          </li>
          <li>
            Chows were accepted into the{" "}
            <Link to="https://www.akc.org/dog-breeds/chow-chow/" target="new">
              American Kennel CLub (AKC)
            </Link>{" "}
            in the year 1903.
          </li>
        </ul>
        <p>
          If you have been attacked or experienced a dog bite from an Chow Chow
          or other dog breed, you are always urged to seek medical attention
          immediately and to contact a team of skilled and highly experienced{" "}
          <Link to="/dog-bites" target="new">
            Dog Bite Lawyers
          </Link>
          .
        </p>

        <p>
          Call us for a Free consultation at 800-561-4887 or visit us at{" "}
          <Link to="/" target="new">
            www.bestatto-gatsby.netlify.app
          </Link>
        </p>

        <h2 id="header4">What Reputation Do Chow Chows Have?</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/chow-chow-postage-stamp-reputation.jpg"
            width="35%"
            className="imgleft-fluid mb"
            alt="chow chow popularity"
          />
        </LazyLoad>
        <p>
          Have you ever seen a chow in person? Have you ever owned a chow or
          knew someone who has? They are truly magnificent and unique dogs.
          Their loyalty and confidence are two traits Chows carry down from
          generation to generation and continue their legacy of solid and
          attractive qualities.
        </p>
        <p>
          Besides their good looks, Chows are also very intelligent. Although
          Chows can come of stingy, stubborn and very strong-minded, with the
          right trainer, Chows can do anything other dogs can do, if not better.
        </p>
        <p>
          With power and confidence comes the elevated risk of danger. A dog
          bite from a Chow has proven to be damaging and potentially fatal.
          Being mauled by a dog is obviously horrible, but the size and strength
          of an angry chow? Steer clear.
        </p>
        <center></center>

        <h2 id="header5">Temperament of a Chow Chow</h2>
        <p>
          Known for their cleanliness, Chow owners say they are the cleanest
          dogs. The potty-train easy and quick, not a stinky breed, and are very
          fastidious.
        </p>
        <p>
          The Chow breed is appreciated for its loyal companionship. They are
          standoffish towards strangers, loving and affectionate towards family
          members, intelligent and curious. Typically very focused and rarely
          ever clumsy or oblivious.
        </p>
        <p>
          A "good Chow" should never be aggressive or shy, but we believe that
          about any dog, according to the nurture vs. nature theory.
        </p>

        <p>
          You have experienced an aggressive Chow Chow or other dog breed, and
          have been injured or severely traumatized, seek medical attention as
          soon as possible and follow up with a Free consultation with one of
          our skilled Dog Bite Lawyers. Call 800-561-4887.
        </p>

        <LazyLoad>
          <img
            src="/images/text-header-images/chow-chow-breed-characteristics-infograph.JPG"
            className="imgcenter-fluid"
            width="50%"
            alt="chow chow breed characteristic infograph"
          />
        </LazyLoad>
        <center style={{ marginBottom: "2rem" }}>
          (
          <Link
            to="https://dogtime.com/dog-breeds/chow-chow#/slide/1"
            target="new"
          >
            Dogtime.com
          </Link>
          )
        </center>

        <h2 id="header6">Owning a Chow Chow</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/owning-a-chow-chow.jpg"
            width="35%"
            className="imgright-fluid"
            alt="owning a chow chow dog"
          />
        </LazyLoad>
        <p>
          Every dog is different. Just because they come from the same breed
          doesn't mean they are going to act like all the other same breed dogs.
          Chow chows are one of the most unique popular breeds, and for good
          reasons:
        </p>
        <ul>
          <li>
            There are rough-coated chows and smooth-coated chows, vbboth of
            which need regular grooming, twice per week.
          </li>
          <li>Eye, ear and nail care regularly</li>
          <li>
            The Chow chow is a very active dog. It needs walks, run, play time
            and moderation used when roughhousing or playing aggressively
          </li>
          <li>
            Early training and socializing recommended to minimize the chances
            of aggression and any awkward social behavioral issues
          </li>
          <li>
            Involving your Chow in first hand interaction with strangers,
            friends and family can benefit their aggression and
            emotional/physical responses toward people they do and don't know
            later in their life
          </li>
          <li>Chows need a lot of praise and attention</li>
          <li>Responsible breeding regardless of the breed</li>
        </ul>
        <p>
          Chow Chows, like many other breeds require lots of attention,
          confidence in the owner, skillful handling and care when needed around
          others, such as elderly people or infants and toddlers. If you are
          looking to adopt or rescue a Chow, make sure you will be able to meet
          all of your dog's necessities.
        </p>

        <h2 id="header7">Are Chow Chows More Aggressive Than Other Breeds?</h2>

        <LazyLoad>
          <img
            src="/images/text-header-images/aggressive-chow-chows.jpg"
            width="50%"
            className="imgright-fluid"
            alt="aggressive chow chows"
          />
        </LazyLoad>
        <p>
          With all things considered, it's relatively simple to just say certain
          breeds are more dangerous than the other, or one breed has a tendency
          for aggressive behavior, these dogs are prone to, so on and so forth.
          But when it comes to breeds being more aggressive than the other, were
          all factors considered?
        </p>
        <p>
          Chow chows are relatively large dogs. This breed may be shorter and
          broader, a deeper bark and an impressive display of strength and power
          when it comes to playing, roughhousing or being aggressive.
        </p>
        <p>
          Rather than judging a breeds overall aggressiveness as a whole, we
          believe you need to look at more than just the severity of the dog
          bite wound or injury.
        </p>
        <p>
          First, we need to establish that Chow Chows are dogs and a popular
          breed at that:
        </p>
        <ul>
          <li>Some Chows go for as much as $8,500 and more</li>
          <li>
            They weigh from 40-90lbs and stand approximately 17-20 inches tall
          </li>
          <li>Live about 8-12 years</li>
          <li>
            Non-sporting dogs (even though they were used for hunting at one
            point)
          </li>
          <li>Originate from Japan</li>
          <li>The American version is larger than the Japanese version</li>
        </ul>
        <p>
          With all of the other breeds in the world, of all different shapes,
          sizes and strengths, it's impossible to state whether the Chow breed
          have aggressive-dominant genes more than other breeds, without
          including ALL breeds temperament.
        </p>
        <p>
          The Chow makes the list mostly because of its strength and size, where
          even though Chihuahuas are commonly known to be yappers, ankle biters
          and finger-tip-snippers, they aren't put on the same level as the
          chow.
        </p>
        <p>Other dog breeds we have developed pages for include:</p>
        <ul>
          <li>
            {" "}
            <Link to="/dog-bites/akita-dog-breed" target="new">
              Akitas
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/chow-chow-dog-breed" target="new">
              Chow Chows
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/doberman-pinscher-dog-breed" target="new">
              Doberman Pinschers
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/mastiff-dog-breeds" target="new">
              Mastiffs
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/pit-bull-dog-breed" target="new">
              Pit Bulls
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/dog-bites/rottweiler-dog-breed" target="new">
              Rottweilers
            </Link>
          </li>
        </ul>
        <p>
          Overall, the danger of a specific dog breed is categorized by the
          severity of the bite that breed can inflict. Chihuahua bites are not
          as severe as a Chow bite, thus, the Chow is considered more aggressive
          - even though the Chihuahua may bark and bite more.
        </p>
        <h2 id="header8">Medical Attention After a Chow Chow Dog Bite</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/chow-chow-dog-bite-lawyers.jpg"
            width="100%"
            alt="aggressive chow chow breed"
          />
        </LazyLoad>
        <p>
          Not everyone feels they need to go to the hospital, go to urgent care
          or even follow up with a doctor after being attacked or bitten by a
          dog. The problem with not seeking medical attention is that you run
          the risk of dangerous side-effects and symptoms of dog bites.
        </p>
        <p>
          After getting bitten or attacked by a dog or Akita, make sure you do
          the following:
        </p>
        <ul>
          <li>
            Make sure your environment is safe to call for help (Do your best to
            not endanger others)
          </li>
          <li>
            Give as much information about your attack experience as possible
          </li>
          <li>Be honest and do not downplay anything</li>
          <li>Listen to your doctor/health care provider</li>
          <li>Treat as directed</li>
          <li>Document everything, especially throughout your treatments</li>
          <li>Follow up with a skilled Dog Bite Lawyer</li>
        </ul>
        <p>
          Regardless of the type of dog, all dog bite victims should seek
          medical attention immediately following a dog bite attack. All animals
          could potentially be carrying a disease that can easily be transferred
          through biting you.
        </p>
        <p>
          Just because the bite is not really bad or doesn't hurt, doesn't mean
          you don't need to see a doctor. Having the dog bite wound properly
          cleaned, sterilized and bandaged is important to avoid any infections
          or issues down the road.
        </p>
        <p>
          Seeking medical attention is also another great way of having your
          injuries documented in association to the dog bite attack. Trying to
          prove your case without injury documentation can make things very
          difficult and more complex than they need to be. It doesn't take long,
          and you will thank yourself when you receive the winning verdict and
          maximum compensation.
        </p>

        <center></center>

        <h2 id="header9">How To Document Your Chow Chow Dog Bite Injuries</h2>
        <p>
          Every dog bite injury is different. Depending on the size, breed and
          aggressiveness of the attacking dog, many dog bites can often have a
          catastrophic result.
        </p>
        <p>
          Documenting your injuries is one of the most important things you can
          do for your case. Trying to prove that all of your injuries are a
          result of the dog bite incident is difficult, unless done in the
          beginning, immediately after the dog bite attack, and here is how you
          document your injuries. Here is what you should do after your dog bite
          experience to ensure your dog bite case is as full-proof as possible:
        </p>
        <ol>
          <li>Once the environment is safe, seek medical attention:</li>

          <li>Call 911</li>
          <li>
            Drive to the nearest
            <ul>
              <li>Hospital</li>
              <li>Urgent Care</li>
              <li>Walk-In Clinic</li>
            </ul>
            <strong>What not to do:</strong>
          </li>
          <ul>
            <li>
              <strong>Do Not</strong>: Go to a Veterinarian
            </li>
            <li>
              <strong>Do Not</strong>: Touch the wound with dirty hands/objects
            </li>
            <li>
              <strong>Do Not</strong>: Attempt to play with the dog again
            </li>
            <li>
              <strong>Do Not</strong>: Act like nothing happened
            </li>
          </ul>
        </ol>
        <p>
          Make sure, if you are able to safely do so, take pictures and video of
          the location, dogs involved, injuries, any witnesses and their
          testimonies, organized information of what happened, where, why, when
          and what.
        </p>

        <h2 id="header10">How To Win Your Dog Attack Case with Bisnar Chase</h2>
        <p>
          When you have experienced a traumatic dog attack, you need to make
          sure that your legal representation has the experience, skill and
          resources to win your case.
        </p>
        <p>
          Bisnar Chase has been representing and winning dog bite cases for over
          40 years. With over $500 Million won, our reputation is strong, feared
          and always respected.
        </p>
        <p>
          Our team of skilled Dog Bite Lawyers have established a 96% success
          rate and we want to represent and win your case and get you maximum
          compensation possible.
        </p>
        <p>
          If you have further questions about what you should do, actions you
          have already taken or any other comments or concerns, call the skilled
          and experienced Dog Bite Lawyers of Bisnar Chase to represent you and
          your case, at <strong>800-561-4887</strong>.
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bisnar Chase Staff"
            style={{
              backgroundImage:
                "url('/images/dog-bites/boxer-dog-bite-compensation-attorneys.jpg')"
            }}
          >
            <h2 id="header11">No Win, No Fee Help From Bisnar Chase</h2>
          </div>
        </LazyLoad>

        <p>
          For those seeking representation from a firm with an outstanding
          reputation, look no further than Bisnar Chase. Our firm has developed
          an impressive track record over 40 years in business, with a 96%
          success rate and more than{" "}
          <Link to="/case-results" target="new">
            $500 million won for our clients
          </Link>
          .
        </p>

        <p>
          Bisnar Chase has dedicated <b>dog bite attorneys</b> who excel in
          dealing with these cases. They know how traumatic an attack can be,
          and will go above and beyond to ensure clients receive the best
          possible compensation.
        </p>

        <p>
          We offer a ‘No Win, No Fee’ guarantee. This means that if we don’t win
          your case, you do not owe us a cent. Allow the experienced dog bite
          lawyers at Bisnar Chase to fight for you by calling
          <b> 877-302-1639</b> now.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #070707;
    text-align: left;
    padding: 8px;
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
