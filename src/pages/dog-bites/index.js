// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  spanishPageEquivalent: "/abogados/mordeduras-de-perro"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "text-header-images/california-dog-bite-lawyers.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Dog Bite Lawyers - Canine Attack Attorneys"
        pageDescription="Our team of California Dog Bite Lawyers have over 40 Years of Experience & Over $500 Million Won for California plaintiffs. If you've been attacked by a canine, call for a free consultation with top rated canine attack attorneys at 800-561-4887. Free consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Dog Bite Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california dog bite lawyer"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong>California Dog Bite Lawyers </strong>at
            <strong> Bisnar Chase </strong>have been successfully{" "}
            <strong>winning dog bite cases </strong>for over{" "}
            <strong>40 years</strong>. Our team of dedicated attorneys,
            paralegals, mediators, insurance adjusters and other law firm legal
            staff have won over <strong>$500 Million </strong>for our clients
            and want to help make your life better and the world a better and
            safer place.
          </p>
          <p>
            If you've been bitten or attacked by a dog, you should consult with
            a California dog bite lawyer to determine if you are entitled to
            compensation to cover your medical bills and damages; and to{" "}
            <strong>
              pursue the defendant for compensation as the victim of the injury
            </strong>
            .
          </p>
          <p>
            {" "}
            <Link
              to="https://www.mypetneedsthat.com/dog-breeds-with-strongest-bite/"
              target="_blank"
            >
              Dog bites
            </Link>{" "}
            go much further than broken bones or scarring. The emotional trauma
            of a dog bite can last a lifetime.
          </p>
          <p>
            The injury attorneys at Bisnar Chase handle numerous canine attack
            cases each year. We have the knowledge and experience it takes to
            represent our clients and help them obtain fair and full
            compensation for all their losses.
          </p>
          <p>
            To speak with a{" "}
            <Link to="/">top rated California Personal Injury Lawyer</Link>{" "}
            about pursuing your legal rights, Please contact us at
            <strong> 800-561-4887.</strong>
          </p>
        </div>
        <h2>Free Dog Bite Case Evaluation and Consultation</h2>
        <LazyLoad>
          <img
            src="/images/text-header-images/california-dog-bite-attorneys.jpg"
            id="dog-1"
            width="100%"
            alt="california dog bite attorneys"
          />
        </LazyLoad>
        <p>
          We are here to help. We'll provide you with a no obligation, free case
          evaluation and consultation about your dog bite case.
        </p>

        <p>
          When a dog attacks an individual - regardless of the victim's age,
          physical build or mental attributes - the incident leaves a mark. This
          goes beyond the physical puncture wounds and bite marks suffered by
          the victim.
        </p>
        <p>
          There is a heavy emotional and psychological toll in canine bite
          cases, which is indisputable. Whether the victim is a 6-foot-tall man
          or a 2-foot toddler, they both go through a period of intense fear and
          insecurity.
        </p>
        <p>
          A dog attack is traumatic, period.{" "}
          <Link to="/blog/dog-aggressive-possessiveness" target="new">
            <strong>Dog aggression</strong>
          </Link>{" "}
          can leave long lasting anxiety.
        </p>
        <p>
          <strong>
            Injuries associated with a serious dog bite are typically:
          </strong>
        </p>
        <ul>
          <li>Deep contusions</li>
          <li>Fractures</li>
          <li>Amputations</li>
          <li>Serious head and face injuries</li>
          <li>Long term emotional scars</li>
        </ul>

        <hr />

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/key17omhxZY?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <center>
          <blockquote>
            California Dog Bite Attorney, John Bisnar discusses steps to take
            after you've been bitten by a dog including taking pictures of your
            injuries right away.
          </blockquote>
        </center>

        <h2>California Leads the Nation in Dog Attacks</h2>
        <p>
          Two recent reports confirmed that California, especially the Los
          Angeles area, leads the nation when it comes to dog bite incidents.
          For example, Los Angeles has earned notoriety for the second year in a
          row as the U.S. city with the most dog attacks on postal workers.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.usps.com/" target="new">
            U.S. Postal Service
          </Link>
          :
        </p>
        <ul>
          <li>
            Los Angeles had 69 dog attacks on mail carriers in fiscal year 2012.
          </li>
          <li>
            In 2011, Los Angeles led again with 83 mail carrier dog attacks.
          </li>
        </ul>
        <p>
          Experts say year-round pleasant weather in Southern California could
          be one of the reasons why these incidents are so prevalent here. Also,
          according to a{" "}
          <Link
            to="https://newsroom.statefarm.com/20160516dog-bite-prevention-week/#r49Oqr7DQ0UTSWCr.97"
            target="new"
          >
            {" "}
            report released by State Farm insurance company
          </Link>
          , California led the pack when it came to the number of dog bite
          claims (527) and the payout in these types of cases ($20.3 million)
          during the year 2012.
        </p>
        <p>
          <strong>
            {" "}
            <Link to="http://ht.ly/Yj8s30mLQZc" target="new">
              Dog Bite Statistics in the U.S (Infographic)
            </Link>
            .
          </strong>{" "}
          Download or share our dangerous breeds dog bite info-graphic to
          educate others on the dangers of approaching a dangerous dog.
        </p>
        <h2>Strict Liability Statute in California for Dog Bites</h2>
        <p>
          California has what is known as a "
          <Link
            to="http://law.onecle.com/california/civil/3342.html"
            target="new"
          >
            strict liability statute
          </Link>
          " when it comes to dog attacks. What this means is that dog owners are
          responsible for the actions of their pets, no ifs or buts.
        </p>
        <p>
          The only exceptions may be when the dog attack was provoked by cruelty
          toward the animal or when a dog bites someone who was committing a
          crime or trespassing on the owner's property.
        </p>
        <p>
          However, there are very few exceptions in this regard. In a majority
          of cases, dog owners can be held financially liable in addition to
          receiving citations or facing other penalties associated with the
          attack.
        </p>
        <p>
          {" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=CIV&sectionNum=3342."
            target="new"
          >
            California's Civil Code Section 3342
          </Link>{" "}
          states: "The owner of any dog is liable for the damages suffered by
          any person who is bitten by the dog while in a public place or
          lawfully in a private place, including the property of the owner of
          the dog, regardless of the former viciousness of the dog or the
          owner's knowledge of such viciousness."
        </p>
        <p>
          There have also been several recent cases in California where courts
          have upheld personal injury claims against landlords who have allowed
          a tenant to have a dangerous dog on premises.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california dog bite injury treatment"
            style={{
              backgroundImage:
                "url('/images/text-header-images/dog-bite-injury-treatment-california.jpg')"
            }}
          >
            <h2>Injuries and Treatment in Dog Bite Cases</h2>
          </div>
        </LazyLoad>

        <p>
          Dog bite incidents can result in serious injuries including, but not
          limited to bites, puncture wounds, broken bones and even nerve
          injuries. Dog attacks also result in permanent scarring and
          disfigurement, which, needless to say, also causes serious emotional
          and psychological issues for victims.
        </p>
        <p>
          Often, victims or their families spend thousands of dollars on therapy
          so that their lives can return to normalcy. Most victims develop a
          long-term fear of dogs. Many victims also require plastic surgery to
          treat bite marks and scars.
        </p>
        <p>
          Some undergo several surgeries to repair these scars. Some are
          unsuccessful and are left with permanent scars.
        </p>
        <p>
          Victims and their families also struggle with paying for these
          treatments. Most insurance companies do not cover plastic surgery
          procedures because they are cosmetic in nature and not something that
          is needed to save the life of a patient.
        </p>
        <p>
          Patients end up paying out of pocket for these procedures and
          much-needed psychological therapy sessions to combat issues such as{" "}
          <Link
            to="https://www.nimh.nih.gov/health/topics/depression/index.shtml"
            target="new"
          >
            {" "}
            depression
          </Link>{" "}
          and{" "}
          <Link
            to="https://www.nimh.nih.gov/health/topics/post-traumatic-stress-disorder-ptsd/index.shtml"
            target="new"
          >
            {" "}
            post-traumatic stress disorder (PTSD)
          </Link>
          .
        </p>
        <h2>Holding Dog Owners Accountable for Attacks</h2>
        <p>
          It is unfair that victims pay such a high physical and financial price
          for a dog attack when the incident was not even their fault. Dog
          owners should be held financially responsible for the injuries,
          damages and losses suffered by these unsuspecting victims.
        </p>
        <p>
          Injured victims can seek compensation in dog bite cases for damages
          such as medical expenses, lost wages, hospitalization, rehabilitation,
          cost of surgeries, permanent injuries, pain and suffering and
          emotional distress.
        </p>
        <p>
          Dog bite attorneys are very familiar with the laws in California
          regarding unwarranted dog attacks and will be able to fight for you
          against the dog owner. We have handled a wide variety of dog attacks
          including{" "}
          <Link to="/dog-bites/pit-bull-dog-breed" target="_blank">
            Pitbulls
          </Link>
          ,{" "}
          <Link to="/dog-bites/rottweiler-dog-breed" target="new">
            {" "}
            Rottweilers
          </Link>{" "}
          &amp; even Labrador Retrievers.
        </p>
        <h2>Dog Attacks and Homeowners Insurance</h2>
        <p>
          Homeowners' insurance policies usually cover dog attacks. Most of
          these policies provide between $100,000 and $300,000 in liability
          coverage. However, if the dog owner is a renter or lacks insurance,
          the victim can try to hold the property owner liable for allowing a
          dangerous dog to live on the premises.
        </p>
        <p>
          In a recent development, Farmers Group notified its policyholders that
          it would no longer cover bites by pit bulls, Rottweilers and wolf
          hybrids under homeowners' insurance policies.
        </p>
        <p>
          Despite sharp criticism from pit bull advocates, insurers say bites
          from pit bulls and other breeds have risen significantly in recent
          years along with the cost of settling those damage claims.
        </p>
        <p>
          Farmers said that the three breeds it listed accounted for more than
          25 percent of their dog bite claims. Farmers officials also said that
          these breeds caused more harm than any other breed when they attacked.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="dog bite lawyers in california"
            style={{
              backgroundImage:
                "url('/images/text-header-images/personal-injury-attorneys-bisnar-chase-california-dog-bite-lawyers.jpg')"
            }}
          >
            <h2>Get Immediate Legal Help</h2>
          </div>
        </LazyLoad>

        <p>
          An experienced and seasoned dog bite lawyer is going to be the
          difference between winning and losing your case.
        </p>
        <p>
          With Bisnar Chase, you get{" "}
          <strong>California Dog Bite Attorneys </strong>that have over 39 years
          of experience and a team of legal representatives and staff with a
          <strong> 96% success rate </strong>who have won over{" "}
          <strong>$500 Million </strong>for their clients.
        </p>
        <p>
          An attorney who spends countless hours in the courtroom fighting for
          the rights of injured plaintiffs will have a much better grasp on the
          issues than just any attorney.
        </p>
        <p>
          A dog bite attorney studies these types of cases on a regular basis
          and understands the laws, courts and local statutes.
        </p>
        <p>
          Be sure to seek out an attorney who has a specialized area of practice
          for dog bites and animal attacks.
        </p>
        <p>
          If you have been injured or have experienced a traumatic event and or
          dog bite or attack, call us at<strong> 800-561-4887 </strong>for a
          <strong>
            {" "}
            Free, No-Hassle, Confidential, No-Obligation California Dog Attack
            Consultation
          </strong>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
