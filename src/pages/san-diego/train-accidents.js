// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Diego Train Accident Lawyer - Bisnar Chase"
        pageDescription="Call 949-203-3814 now if you or a loved one was the victim of a train crash. Our experienced and qualified San Diego train accident lawyer will fight fir you. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Diego Train Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been injured in a San Diego train or railroad accident and
          need legal help, contact our San Diego train accident lawyers now for
          a free case review and to discuss your right to compensation.
        </p>
        <p>
          The lawyers at Bisnar Chase are experienced legal professionals who
          can handle the intricacies of a legal case involving a train wreck. We
          will investigate every aspect of your claim to help you recover the
          highest monetary compensation for your injuries or loss.
        </p>
        <p>
          Our San Diego train accident lawyers represent the seriously injured
          victims of train accidents and Metrolink collisions throughout
          California.
        </p>
        <p>
          If you have suffered serious personal injuries due to a train
          accident, call our offices for a free consultation. We may be able to
          recover the maximum monetary compensation for your train accident
          related injuries and our{" "}
          <Link to="/san-diego">San Diego personal injury attorneys</Link> will
          fight for you and your loved ones.
        </p>

        <p>
          With the rapid expansion of urban population in the United States and
          the increased need for mass transit, train travel has become the
          primary means of transportation to and from work for millions of
          Americans.
        </p>
        <h2>Thousands of Train Accidents Happen Each Year</h2>
        <p>
          Train travel is no longer a romantic mode of travel for those looking
          for a leisurely ride.
        </p>
        <p>
          Also, with the MetroLink expansion in California, the last decade has
          seen a boom in commuter train travel in this state. However, with the
          rise in train travel, there has been a corresponding rise in train
          accidents like in the case with R&B group{" "}
          <Link
            to="/blog/members-of-tower-of-power-injured-in-train-accident"
            target="_blank"
          >
            {" "}
            Tower of Power
          </Link>{" "}
          and Amtrack.
        </p>
        <h2>U.S. Train Accident Statistics</h2>
        <ul>
          <li>There are about 3,000 train accidents every year.</li>
          <li>About 1,000 people die from train accidents every year.</li>
          <li>
            There is a chemical spill resulting from a train derailment about
            every two weeks.
          </li>
        </ul>
        <h2>Preventing Train Accidents</h2>
        <p>
          There are many possible causes of train collisions and derailments,
          including negligence on the part of the engineer, inadequate rail
          maintenance, and unsafe operators. There is a growing trend of trains
          colliding with motor vehicles, which causes great damage to the train,
          the other vehicles, and everyone involved.
        </p>
        <h2>Train Safety in San Diego</h2>
        <p>
          Safety issues are always a high priority for state and federal
          officials. Keeping the public safe is of the utmost importance to
          safety agencies across the country, unfortunately not all
          environmental aspects can be controlled. This includes unforeseen
          weather issues and the general public ignoring safety precautions.
          Many of the deaths and injuries caused by trains can also be directly
          correlated to driver failure and mechanical defects causing serious
          injury and death.
        </p>
        <p>
          Passengers riding trains should always be aware of their surroundings.
          If you feel something isn't right, say something. Recently, a train
          crash killed over 100 people and the operator was found to be
          traveling in excess of the recommended speed limit as he rounded a
          corner.
        </p>
        <p>
          The train crashed killed and seriously injuring many of its
          passengers. Trains have operators who fall into the trap of being
          distracted just as any one operating a vehicle can. Lives can be saved
          if you sense something isn't right.
        </p>
        <h2>Injured in a Train Accident? Call Us Now!</h2>
        <p>
          If you have questions about an accident involving a train or Metrolink
          and need legal advice, call the San Diego train accident lawyers of
          Bisnar Chase.
        </p>
        <p>
          If you are entitled to compensation, our San Diego personal injury
          attorneys will do the legal work for you and get you the settlement
          that you deserve.
        </p>
        <p>
          Call <strong> 949-203-3814</strong> now for a free consultation.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
