/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "",
    HTML: ``
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "San Diego Injury Information",
    links: [
      {
        linkName: "Amusement Park Accidents",
        linkURL: "/san-diego/amusement-park-accidents"
      },
      {
        linkName: "Auto Accidents",
        linkURL: "/san-diego/auto-accidents"
      },
      {
        linkName: "Aviation Accidents",
        linkURL: "/san-diego/aviation-accidents"
      },
      {
        linkName: "Boat Accidents",
        linkURL: "/san-diego/boat-accidents"
      },
      {
        linkName: "Bus Accidents",
        linkURL: "/san-diego/bus-accidents"
      },
      {
        linkName: "Dog Bites",
        linkURL: "/san-diego/dog-bites"
      },
      {
        linkName: "Motorcycle Accidents",
        linkURL: "/san-diego/motorcycle-accidents"
      },
      {
        linkName: "Nursing Home Abuse",
        linkURL: "/san-diego/nursing-home-abuse"
      },
      {
        linkName: "Pedestrian Accidents",
        linkURL: "/san-diego/pedestrian-accidents"
      },
      {
        linkName: "Premises Liability",
        linkURL: "/san-diego/premises-liability"
      },
      {
        linkName: "Product Liability",
        linkURL: "/san-diego/product-liability"
      },
      {
        linkName: "Slip and Fall Accidents",
        linkURL: "/san-diego/slip-and-fall-accidents"
      },
      {
        linkName: "Swimming Pool Accidents",
        linkURL: "/san-diego/swimming-pool-accidents"
      },
      {
        linkName: "Train Accidents",
        linkURL: "/san-diego/train-accidents"
      },
      {
        linkName: "Wrongful Death",
        linkURL: "/san-diego/wrongful-death"
      },
      {
        linkName: "San Diego Home",
        linkURL: "/san-diego"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "What is my Case Worth?",
      videoUrl: "IuD-S72PMxk"
    },
    {
      videoName: "Do I Owe Money If My Case Is Lost?",
      videoUrl: "D3c_pxDeswg"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: false,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
