// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Diego Amusement Park Injury Lawyers - Bisnar Chase"
        pageDescription="Our top-rated San Diego amusement park injury attorneys have recovered tens of millions of dollars in damages. Call now for a Free Consultation."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Diego Amusement Park Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Amusement parks draw millions of tourists each year and with that are
          many more injuries. Amusement parks like LEGOLAND California and
          Belmont Park provide locals and tourists of the San Diego area with
          some wonderful entertainment options, however, over{" "}
          <Link
            to="http://www.cnn.com/2016/06/30/health/kids-injury-amusement-rides/"
            target="_blank"
          >
            {" "}
            92,000 children have been injured in the past two decades
          </Link>{" "}
          alone due to amusement park events.
        </p>
        <h3>
          If you have been seriously injured, or suffered damages at a San Diego
          amusement park, don't hesitate contact our offices for a free,
          IMMEDIATE case evaluation.
        </h3>
        <p>
          Since 1978, Bisnar Chase has handled many different types of San Diego
          personal injury cases, including amusement park injury. Our San Diego
          personal injury attorneys have recovered tens of millions of dollars
          and counting in damages for thousands of our clients.
        </p>
        <p>
          New amusement park rides are constantly being built in California. The
          intention of these amusement parks are outdoing what has been done
          before, which is to have the fastest and the scariest rides available.
        </p>
        <p>
          These rides can make for an amazing day of adventure, unless{" "}
          <em>you</em> are one of the thousands of people who have suffered from
          minor or serious injuries on these rides each year. It is not uncommon
          for San Diego riders to wind up with broken or bloody noses and bones,
          whiplash, brain hemorrhages, paralysis or even death.
        </p>
        <p>
          There has also been an rising <em>increase</em> in the number of
          emergency room visits resulting from personal injuries from amusement
          park rides according to the{" "}
          <Link
            to="https://www.cpsc.gov/cgibin/NEISSQuery/home.aspx"
            target="_blank"
          >
            {" "}
            Consumer Product Safety Commission (CPSC)
          </Link>
          .
        </p>
        <p>
          We have the experience and expertise necessary to handle these
          difficult personal injury cases, insuring that justice is served for
          those who are injured and their families as well. Contact Bisnar Chase
          immediately if you are injured as a result of an amusement park ride
          in the San Diego area.
        </p>
        <h2>
          There are a Number of Factors that Contribute to Amusement Park
          Injuries
        </h2>
        <ul>
          <li>Mechanical failure of the ride</li>
          <li>Operator behavior</li>
          <li>Rider behavior</li>
          <li>Ride design defects</li>
        </ul>
        <p>
          It may not always be clear exactly who or what is to blame for
          amusement ride accidents, but there is no question that the injured
          parties suffer a great deal of loss as well as physical and emotional
          pain.
        </p>
        <h2>Amusement Park Accident Liability Involves Three Types of Law:</h2>
        <ul>
          <li>Product liability law</li>
          <li>Negligence or tort law</li>
          <li>Premises liability law</li>
        </ul>
        <h2>Injured By A Amusement Park Ride? Call us!</h2>
        <p>
          Call our office now at <strong> 949-203-3814</strong> to see how we
          can help you with your personal injury. There is no risk and you may
          be entitled to compensation. We have the resources and experience to
          win your case and we have been representing the residents of San Diego
          for many years. We've collected over $500 Million in settlements and
          verdicts and we will fight for you!
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
