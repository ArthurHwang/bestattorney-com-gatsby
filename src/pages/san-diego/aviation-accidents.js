// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Diego Aviation Accident Lawyer - Plane Crash Attorneys"
        pageDescription="Airline disasters require the representation of a committed, experienced San Diego aviation accident lawyer. Call Bisnar Chase for a Free Consultation about a aviation related personal injury."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Diego Aviation Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          Bisnar Chase have been representing San Diego clients for over 30
          years. We have the resources, experience, talent and drive necessary
          to take on some of the biggest cases in California. If you have lost a
          loved one due to negligence in an airplane crash please call our San
          Diego aviation accident attorneys now for a FREE case evaluation.
        </p>
        <p>
          Airline disasters require the representation of a committed,
          experienced San Diego aviation accident lawyer. Victims of this type
          of accident and their families may wish to hold a defendant publicly
          accountable and receive a fair settlement or judgment for damages.
          Bisnar Chase has the experience and resources to handle aviation
          accident cases.
        </p>
        <p>
          Air travel is considered one of the safest means of transportation,
          however, commercial airline accidents <em>STILL</em> happen every
          year. These accidents usually involve the devastating death of all of
          the passengers and crew members. Your best course of action in the
          event of such an accident is to contact an{" "}
          <Link to="/san-diego">experienced San Diego injury attorney</Link> in
          such matters for a consultation and case evaluation.
        </p>
        <p>
          Bisnar Chase understands the intricacies of filing a legal claim
          against an airline or helicopter company for a wrongful death case.
          Our lawyers understand aviation law and have a demonstrated reputation
          for success in aircraft accident cases. Call Bisnar Chase now if you
          think you have a justifiable case against an aviation company.
        </p>
        <h2>Who is Responsible for a San Diego Plane Crash?</h2>
        <p>
          Plane crashes are investigated by the{" "}
          <Link
            to="https://www.ntsb.gov/_layouts/ntsb.aviation/index.aspx"
            target="_blank"
          >
            {" "}
            National Transportation Safety Board.
          </Link>{" "}
          The NTSB leads the investigation unless there is a suspected criminal
          act such as terrorism. Whether the plane crash is the result of a
          defective product, a mechanical failure, traffic control error or
          pilot error, someone is responsible for the hundreds of innocent lives
          lost in the aviation accident.
        </p>
        <p>
          A full investigation can uncover a lot of information. In some cases a
          whistle-blower can help uncover or be a witness to a piece of evidence
          such as in the case of{" "}
          <Link
            to="http://www.nytimes.com/2000/02/03/us/passengers-and-crew-members-on-alaska-airlines-flight-261.html"
            target="_blank"
          >
            {" "}
            Alaska Airline flight 261.
          </Link>{" "}
          An aviation mechanic reported a faulty jack screw and was suspended
          from his job. This did not stop the families of the victims from
          filing lawsuits.
        </p>
        <p>
          There were 88 wrongful death lawsuits against Alaska Airlines. Several
          years later 87 of the 88 cases were settled.
        </p>
        <p>
          Plane crashes are infrequent, but catastrophic when they do happen.
          We, at Bisnar Chase, are very skilled and experienced in successfully
          settling wrongful aviation related death cases in San Diego and we
          also an in depth understanding of the laws pertaining to aviation.
        </p>
        <p>
          If you have been seriously injured, or you have a loved one that was
          the victim of a fatal plane crash, you should seek the advice of an
          experienced San Diego plane crash lawyer like Bisnar Chase. Contact
          our offices for a Free Confidential Case Evaluation.
        </p>
        <h2>Get Immediate Help from the Best Attorneys</h2>
        <p>
          Call our office now to see how we can help you with your personal
          injury. The call is free, there is no obligation. You may be entitled
          to compensation. Call <strong> 949-203-3814</strong> now. With many
          years of representing the residents of San Diego, we have the
          resources and experience to win your case. We've collected over $500
          Million in settlements and verdicts and may be able to recover money
          for you too!
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
