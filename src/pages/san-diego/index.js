// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { MiniLocationWidget } from "src/components/elements/MiniLocationWidget"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "personal-injury/san-diego-personal-injury-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  // Add location page data in object below
  // Copy locationWidgetOptions variable to all single location pages
  const locationWidgetOptions = {
    locationBox1: {
      city: "San Diego",
      population: 1355896,
      totalAccidents: 47800,
      intersection1: "Sunset Cliffs Blvd & Nimitz Blvd",
      intersection1Accidents: 62,
      intersection1Injuries: 111,
      intersection1Deaths: 1
    },
    locationBox2: {
      mainCrimeIndex: 235.4,
      city1Name: "Coronado",
      city1Index: 114.5,
      city2Name: "La Mesa",
      city2Index: 261.7,
      city3Name: "Lemon Grove",
      city3Index: 280.0,
      city4Name: "National City",
      city4Index: 315.4
    },
    locationBox3: {
      intersection2: "Mira Mesa Blvd & Westview Pkwy",
      intersection2Accidents: 63,
      intersection2Injuries: 101,
      intersection2Deaths: 1,
      intersection3: "Palm Ave & Saturn Blvd",
      intersection3Accidents: 72,
      intersection3Injuries: 94,
      intersection3Deaths: 0,
      intersection4: "Rosecrans St & Midway Dr",
      intersection4Accidents: 55,
      intersection4Injuries: 67,
      intersection4Deaths: 0
    }
  }
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="San Diego Personal Injury Lawyers |Injury Trial Attorneys"
        pageDescription="The San Diego Personal Injury Lawyers at Bisnar Chase offer free consultations for those who have been injured in an accident. Call 1-800-561-4887. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>San Diego Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="San Diego personal injury lawyers"
            title="San Diego personal injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          The <strong> San Diego Personal Injury Lawyers</strong> at
          <strong> Bisnar Chase</strong> has been representing injured victims
          <em></em> and winning complex and difficult cases since 1978.
        </p>
        <p>
          Our experienced trial lawyers have decades of experience in getting
          you compensated whether it's a{" "}
          <Link to="/san-diego/auto-accidents" target="new">
            car accident
          </Link>
          , an auto defect or anything in between.
        </p>
        <p>
          We are a highly respected law firm of trained injury and accident
          attorneys and support staff and our firm is committed to protecting
          the rights of our injured clients and to recovering the maximum
          monetary compensation possible.
        </p>
        <p>
          We are a contingent fee based law firm, which means you do not pay if
          we do not win your case. With over
          <strong> 4 decades of experience</strong>, our personal injury law
          firm has won over
          <strong> $500 Million</strong> for our clients, helping injured
          victims get their lives back and find justice.
        </p>
        <p>
          If you have been seriously injured, or experienced pain and suffering
          in a San Diego accident, please call our offices now for a free
          immediate case evaluation, at <strong> 1-800-561-4887</strong>.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/personal-injury-san-diego.jpg"
            width="100%"
            alt="san diego personal injury attorneys"
          />
        </LazyLoad>
        <p>
          Most accident injury cases stem from car accidents and our law firm
          has taken on and won millions of dollars in auto accident settlements
          over the past 40 years.
        </p>
        <p>
          Some accidents are fatal and often caused by a hidden{" "}
          <Link
            to="http://www.nbcnews.com/news/us-news/u-s-confirms-11th-death-linked-faulty-takata-airbag-inflator-n670446"
            target="new"
          >
            auto defect
          </Link>{" "}
          as in the case of faulty Takata airbags in Honda cars.
        </p>
        <p>
          Our San Diego personal injury attorneys have the experience and
          knowledge to determine if there is more to an auto accident than just
          the fact that there was a collision.
        </p>
        <p>
          There are many questions to answer and Bisnar Chase personal injury
          attorneys will take a thorough evaluation of your accident case then
          decide how to proceed.
        </p>
        <h2>What To Do If You've Suffered A Personal Injury</h2>
        <p>
          {" "}
          Personal injury accidents cause grief, financial stress, mental and
          emotional distress and can take years to recover from. Assaults,
          automobile accidents, unsafe products, and even poisoning all can
          cause personal injury and may entitle you to financial compensation.{" "}
        </p>
        <p>
          {" "}
          A personal injury, which is an injury to the body or mind, can be
          purposely caused by another individual, such as assault, or can be
          caused by negligence, such as employers providing unsafe working
          conditions.{" "}
        </p>
        <h2>Should I File A Personal Injury Claim in San Diego?</h2>
        <p>
          {" "}
          You must avoid filing and litigating your own personal injury claim.
          We urge you to please not take your chances by taking on a personal
          injury case on your own. Experienced San Diego personal injury lawyers
          such as Bisnar Chase will have access to expert witnesses, medical
          professionals and other resources that help immensely in winning the
          claim.{" "}
        </p>
        <h2>Frequently Asked Questions About Personal Injury Claims </h2>
        <p>
          If you were involved in an accident for the first time, understanding
          what you should do next can be stressful. Questions of "how much a
          lawyer will cost" or "will you be able to gain compensation" come
          immediately to mind. The legal team at Bisnar Chase handles the most
          complex cases and is here for any question that you need to be
          answered. The following are the most frequently asked questions injury
          victims have after an accident.{" "}
        </p>
        <ol>
          <li>
            <strong>
              {" "}
              Does my case qualify as a{" "}
              <Link
                to="https://en.wikipedia.org/wiki/Personal_injury"
                target="_blank"
              >
                {" "}
                personal injury
              </Link>
              ?
            </strong>{" "}
            A personal injury claim involves a person being severely injured due
            to someone's negligence. California law states that accident victims
            must be fully compensated if the injuries could have been prevented.
            If an insurance company is not offering you the full amount that you
            need to recover for your losses it is strongly suggested that you
            speak to a personal injury attorney in San Diego.
          </li>
          <li>
            <strong> How much is my personal injury claim worth?</strong> First
            and foremost do not rely on settlement calculators that you find
            online. These settlement calculators are incorrect because they are
            not taking every detail of your accident into account. Some of those
            details can include rehabilitation, vacation hours from work you
            have used up since your injury and if you have suffered the loss of
            a loved one (wrongful death), funeral expenses. The settlement
            amount that accident victims receive is dependent on the amount of
            damage that took place physically, mentally and the damage that you
            have faced to your property. This means that there is not a standard
            amount of compensation every claimant receives.
          </li>
          <li>
            <strong> Should I settle or file a personal injury lawsuit?</strong>{" "}
            The choice of whether you should settle or sue the negligent party
            can be daunting. You should not settle for anything less than you
            deserve. If you see that the offer you are handed by the insurance
            company is not sufficient enough to cover your expenses you can
            negotiate. If the insurance company does not compromise or does not
            want to negotiate you may seek legal representation from a San Diego
            personal injury law firm.{" "}
          </li>
          <li>
            <strong>
              {" "}
              How long will it take to settle my personal injury case?{" "}
            </strong>
            Each personal injury lawsuit is unique. The period of time it will
            take to settle your accident case can take months or years. The
            reason why it takes so long at times is due to factors such as the
            amount of money that is involved, the gathering of all the details
            of the case or the progression of your injuries. If your injuries
            have worsened during the trial period then the amount of
            compensation you should be awarded may need some altering.{" "}
          </li>
          <li>
            <strong>
              {" "}
              Would it beneficial for me to hire a San Diego injury attorney?
            </strong>{" "}
            If you feel as though the negligent party's insurance company is
            operating in{" "}
            <Link
              to="https://en.wikipedia.org/wiki/Insurance_bad_faith"
              target="_blank"
            >
              {" "}
              bad faith
            </Link>
            , you should hire an accident attorney in San Diego. Some examples
            of insurance companies acting in bad faith can be denying a claim
            for no reason, not conducting a full investigation of the accident
            or using complicated language to confuse or mislead the victim. Our
            personal injury lawyers in San Diego have been helping clients who
            have suffered from extreme losses from a catastrophic accident for
            over <strong> 40 years</strong>. Our legal representation has
            sustained a <strong> 96% success rate </strong>because we won't
            settle for the minimum amount of compensation an insurance company
            offers you.{" "}
            <strong>
              {" "}
              Call us today at 800-561-4887 for a free consultation
            </strong>
            .{" "}
          </li>
        </ol>
        <h2>Common Personal Injuries in San Diego</h2>
        <p>
          San Diego is one of the most beautiful locations in the world,
          attracting tourists of all kind. With high volumes of people,
          congested traffic and busy narrow one-way city streets, car accidents,
          motorcycle accidents, bus and other types of transportation accidents
          are inevitable.
        </p>
        <LazyLoad>
          <img
            src="/images/text-header-images/san-diego-personal-injury-infographic-top-7.jpg"
            width="100%"
            title="top 7 personal injury accidents san diego infographic"
            alt="san diego personal injury attorneys"
          />
        </LazyLoad>
        <h2>Our Personal Injury Experience in San Diego is Unmatched</h2>
        <p>
          What sets us apart from the rest? <strong> Trust, Passion,</strong>{" "}
          <strong> Results.</strong>
        </p>
        <p>
          Our San Diego personal injury lawyers will earn your trust by being
          ethical, honest and professional with you. We are passionate about
          winning and we have recovered over $500 Million dollars in settlements
          and verdicts.
        </p>
        <p>
          <strong>
            {" "}
            Other types of accident injury cases our firm handles with results:
          </strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/san-diego/bus-accidents">
              Truck and Big Rig Accidents and Highway Accidents
            </Link>
            - settled for $1,950,000
          </li>
          <li>
            {" "}
            <Link to="/san-diego/motorcycle-accidents">
              Motorcycle Accidents
            </Link>{" "}
            and Bicycle Accidents- settled for $3,300,000
          </li>
          <li>
            Head Injuries, Brain Injuries and Spinal Cord Injuries- settled for
            $4,500,000
          </li>
          <li>
            {" "}
            <Link to="/san-diego/dog-bites">Dog Bite Injuries </Link>and Animal
            Attacks- settled for $275,000
          </li>
          <li>Auto Defect- settled for $24,700,000</li>
          <li>Construction Accident- settled for $2,800,000</li>
          <li>
            {" "}
            <Link to="/san-diego/wrongful-death">Wrongful Death</Link>- settled
            for $2,055,000
          </li>
          <li>Car Accident- settled for $5,000,000</li>
          <li>Bike Accident- settled for $3,000,000</li>
          <li>Job Injury- settled for $2,375,000</li>
        </ul>
        We also take on cases that include{" "}
        <Link to="/san-diego/slip-and-fall-accidents">
          slip and fall injuries
        </Link>{" "}
        as well as medical malpractice cases.
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/qR8_fm8aLHw"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>What Do You Need For a Winning Case?</h2>
        <p>
          Every case is different and has many factors that can alter it's path.
          Depending on the following, it could make or break your case.
        </p>
        <ul>
          <li>
            A detailed summary of the incident that lists the individuals
            involved, location, time of occurrence, etc.
          </li>
          <li>
            Medical records that document the injuries and associate them to the
            accident.
          </li>
          <li>
            Contact information and statements of witnesses and third parties
          </li>
          <li>
            Videos, photos and any other content of injuries, those involved,
            the incident/accident, etc.
          </li>
        </ul>
        <h2>We Are a Contingency Based Law Firm</h2>
        <p>
          Helping victims, their families and loved ones get their life back is
          what we love doing. Every single day we meet with our clients and
          discuss every aspect of their case. Our lawyers in San Diego have been
          representing and winning cases for over 40 years and have an
          incredible amount of skill, precision and experience.
        </p>
        <p>
          We want to ensure that you are taken care of.{" "}
          <strong> Our firm will advance all costs</strong> reasonably necessary
          to pursue your injury claim and if we don't recover compensation for
          you, then you do not pay us.
        </p>
        <p>
          <strong>
            {" "}
            There are no upfront fees and it costs nothing to get your case
            started.
          </strong>{" "}
          Ask about our unique protection plan that shields you from having to
          repay advanced costs.
        </p>
        <h2>The Importance of Hiring a Lawyer</h2>
        <p>
          People often tell themselves they can manage their case on their own.
          This is many times a bad idea, due to the confusing way laws are
          written and enforced. Bisnar Chase has many benefits and one of them
          is all the hurdles and obstacles we are happy to overcome.
        </p>
        <p>
          The San Diego Personal Injury Attorneys are many things, including
          trial attorneys. Not all personal injury lawyers go to trial for their
          clients, but we do. Our lawyers, paralegals, negotiators and other
          legal staff are very experienced and skilled.{" "}
        </p>
        <p>
          Our law firm has a <strong> 96% success rate</strong> and If you have
          been seriously injured, or suffered damages, call us now.{" "}
        </p>
        <p>
          Contact our San Diego accident injury lawyers at
          <strong> 1-800-561-4887 </strong>for a{" "}
          <strong> Free Consultation.</strong>
        </p>
        <MiniLocationWidget {...locationWidgetOptions} />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
