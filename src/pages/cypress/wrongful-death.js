// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "wrongful-death/cypress-wrongful-death-attorneys.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Wrongful Death Attorneys - Orange County, CA"
        pageDescription="Has your loved one been wrongfully killed? Call 949-203-3814 for Cypress wrongful death attorneys. Free consultations. High client ratings. Since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Wrongful Death Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress wrongful death attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The experienced Cypress wrongful death lawyers of Bisnar Chase
          understand that the death of a loved one can affect families
          emotionally and financially. We have handled many cases where the
          decedent was the family's sole breadwinner or primary wage earner.
          Such severe losses often plunge families in a financial despair.
        </p>
        <p>
          Our wrongful death attorneys will handle all your paperwork and
          research so you do not have to worry or lift a finger about these
          issues during a very difficult grieving time in your life.
        </p>
        <p>
          We will answer all your questions and address your concerns. We will
          work diligently to help you secure the compensation you rightfully
          deserve and hold the wrongdoers accountable. Please contact us or
          experienced <Link to="/cypress">Cypress personal injury</Link>{" "}
          attorneys at<strong> 949-203-3814 </strong>to schedule your free and
          comprehensive consultation.
        </p>
        <h2>What Qualifies as Wrongful Death?</h2>
        <LazyLoad>
          <img
            src="/images/wrongful-death/man-at-grave-image.jpg"
            width="251"
            className="imgright-fixed"
            alt="Cypress wrongful death lawyers"
          />
        </LazyLoad>
        <p>
          A "wrongful death" occurs when someone is killed by a willful or
          negligent act of another party. For example, if a drunk driver strikes
          and kills a pedestrian, the family members of that deceased victim can
          file a civil wrongful death claim against the at-fault driver in
          addition to any criminal charges he or she may face.
        </p>
        <p>
          Wrongful death statutes provide a legal process whereby immediate
          family members of fatally injured victims can pursue financial support
          for their loss.
        </p>
        <p>
          A wrongful death claim is a civil lawsuit that is different from
          criminal charges and neither proceeding controls the other.
        </p>
        <p>
          If the drunk driver in the above example is found not guilty of
          vehicular manslaughter, he or she may still be found civilly or
          financially liable for the victim's death in civil court.
        </p>
        <p>
          The family will have to prove that the at-fault driver's negligence
          contributed to their tragic loss. While the burden of proof in
          criminal cases rests with the prosecution, in civil cases, it rests
          with the plaintiff.
        </p>
        <p>
          Although this may seem near impossible, the Cypress wrongful death
          lawyers at Bisnar Chase have a track record of taking on these case
          and bringing in millions of dollars for our clients over the past 30
          years.
        </p>
        <h2>Examples of Cases that Can Result in a Wrongful Death Claim</h2>
        <p>
          There are many ways in which an individual can suffer fatal injuries
          due to the negligence or wrongdoing of another.
        </p>
        <ul>
          <li>
            <strong>Negligent driving: </strong>Careless and reckless drivers
            are responsible for numerous deaths in California each year.
            Motorists who cause fatal injuries by speeding, driving distracted
            or driving while under the influence of drugs or alcohol may be held
            accountable for their actions.
          </li>
          <li>
            <strong>Defective products: </strong>The manufacturers of defective
            products can be held liable for making dangerous and defective
            products that cause deaths. There are many types of defective
            products that can result in fatal injuries, including hazardous
            prescription drugs, defective vehicles and auto parts, defectively
            designed medical devices and hazardous children's products such as
            car seats and nursery products.
          </li>
          <li>
            <strong>Nursing home neglect:</strong> When a nursing home resident
            dies as a result of neglect of abuse by the facility or its
            employees, the victim's family can file a wrongful death claim
            against the at-fault parties. Common fatal incidents in Orange
            County nursing homes include choking on food, falling, administering
            wrong medication and neglect.
          </li>
          <li>
            <strong>Dangerous premises: </strong>When an individual is killed on
            someone else's property, it must be determined what led to the
            accident. The family of someone killed in a slip-and-fall accident
            will have to show that the property owner knew of the hazardous
            conditions that led to the accident and failed to fix the problem in
            a reasonable manner.
          </li>
          <li>
            <strong>Dog attacks:</strong> Often, dog bite incidents or vicious
            dog attacks may also result in a fatality. In such cases, the dog's
            owner can be held liable for the victim's wrongful death.
          </li>
        </ul>
        <LazyLoad>
          <center
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              flexWrap: "wrap"
            }}
          >
            <img
              src="/images/wrongful-death/sad-children-image.jpg"
              width="200"
              alt="Wrongful death lawyers in Cypress"
            />
            <img
              src="../images/wrongful-death/sad-spouse-image.jpg"
              width="200"
              alt="Wrongful death attorneys in Cypress"
            />
            <img
              src="/images/wrongful-death/old-parents-sad-image.jpg"
              width="200"
              alt="Wrongful death legal representation in Cypress"
            />
          </center>
        </LazyLoad>
        <h2>Who Can File a California Wrongful Death Claim?</h2>
        <p>
          Not everyone who is close to the victim is eligible to receive
          compensation for his or her losses.
        </p>
        <p>
          According to California law, the surviving spouse, domestic partner
          and children of the decedent are the first eligible recipients of
          compensation.
        </p>
        <p>
          If there is no surviving offspring of the victim, there are a number
          of other potential claimants including: a putative spouse, children of
          a putative spouse, stepchildren, parents or a minor who was
          financially dependent on the victim.
        </p>
        <p>
          Each case is different. Victims' families would be well advised to
          find out from an experienced Cypress wrongful death attorney if they
          are eligible to file such a claim and what their claim may be worth.
        </p>
        <h2>Elements of a Wrongful Death Case</h2>
        <p>The elements of a successful wrongful death case include:</p>
        <ul>
          <li>
            Showing that the victim was killed as the result of another's
            actions or lack of necessary action.
          </li>
          <li>
            Proving that the action that resulted in the fatal injuries was
            caused by negligence or intentional conduct.
          </li>
          <li>
            Determining who has the legal right to bring action against the
            person responsible for the incident.
          </li>
          <li>
            Selecting a representative to establish an estate for the victim and
            to identify all survivors.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Cypress wrongful death lawyers"
            style={{
              backgroundImage:
                "url('/images/wrongful-death/cypress-text-header-image.jpg')"
            }}
          >
            <h2>Damages in Wrongful Death Cases</h2>
          </div>
        </LazyLoad>
        <p>
          The loss of a loved one, in addition to being emotionally
          traumatizing, can also be financially devastating to a family.
        </p>
        <p>
          If you have lost a loved one as a result of someone else's negligence
          or wrongdoing, you may be able to receive compensation for losses
          including medical and funeral costs, lost future income and benefits
          and loss of love and companionship.
        </p>
        <p>
          Families may also claim as damages, the value of the household
          services the decedent would have provided in the future. Call our
          skilled Cypress wrongful death lawyers now at
          <strong> 949-203-3814</strong> to discuss your legal rights and
          options.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
