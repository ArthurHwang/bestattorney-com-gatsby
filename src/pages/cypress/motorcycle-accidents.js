// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "motorcycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "motorcycle-accidents/cypress-motorcycle-accident-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Motorcycle Accident Lawyers - California - Bisnar Chase"
        pageDescription="Call 949-203-3814 for a free consultation with a Cypress motorcycle accident lawyer with over 4 decades of winning tough cases. Free consultation 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Motorcycle Accident Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress motorcycle accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          There are many Cypress motorcycle accidents in every year in
          California, and because motorcycles provide very little protection for
          riders, it is common for riders to suffer serious injuries that affect
          their quality of life.
        </p>
        <p>
          Riders have it in their best interest to educate themselves on
          motorcycle accident statistics as a reminder of how dangerous riding a
          motorcycle can be.Caution should be exercised at all times.
          Unfortunately, even the most careful riders can be involved in an
          accident when the other driver involved is negligent or reckless.
        </p>
        <p>
          Anyone injured in a motorcycle accident would be well advised to speak
          with an experienced{" "}
          <Link to="/cypress">Cypress personal injury attorney</Link> who will
          protect your legal rights and best interests.
        </p>
        <h2>Motorcycle Accident Statistics</h2>
        <p>
          A review of motorcycle accidents locally, county-wide, statewide and
          nationwide is a good way to understand how many accidents happen and
          how potentially dangerous riding a motorcycle can be.
        </p>
        <p>
          <strong>
            Below are bike crash statictics reported by the{" "}
            <Link
              to="https://www.chp.ca.gov/programs-services/services-information/switrs-internet-statewide-integrated-traffic-records-system"
              target="_blank"
            >
              {" "}
              SWITRS
            </Link>{" "}
            and{" "}
            <Link to="https://www.cdc.gov/" target="_blank">
              {" "}
              CDC
            </Link>
          </strong>
          :
        </p>
        <ul>
          <li>
            14 injuries were reported as a result of Cypress motorcycle
            accidents in a single year
          </li>
          <li>17 fatalities and 735 injuries took place in Orange County</li>
          <li>
            389 fatalities and 10,134 injuries reported as a result of
            motorcycle accidents in the state of California
          </li>
          <li>
            Motorcycle riders between the ages of 20 and 24 had the greatest
            chance of suffering an injury followed by riders between the ages of
            25 and 29
          </li>
          <li>
            30 percent of all motorcycle accident injuries involved leg or foot
            trauma and approximately
          </li>
          <li>
            22 percent of motorcycle injuries involve damage to the head or neck
          </li>
        </ul>
        <h2>Ways to Prevent Cypress Motorcycle Accidents</h2>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/v816vTc7BUQ?start=25"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <p>
          There are a number of safety measures motorcyclists can take to avoid
          being in an accident or suffering a serious injury in a collision. All
          Orange County motorcyclists would be well advised to wear a{" "}
          <Link
            to="https://one.nhtsa.gov/people/injury/pedbimot/motorcycle/unsafehelmetid/pages/page2.htm"
            target="_blank"
          >
            {" "}
            DOT-approved helmet
          </Link>{" "}
          and to wear appropriate riding gear such as leather jackets, long
          pants, boots and gloves. Helmets should have face shields to help
          prevent facial injuries.
        </p>
        <p>
          To help avoid being involved in a collision, all riders should keep a
          safe distance between themselves and other vehicles on the roadway.
          Even though lane splitting is technically legal in the state of
          California, riders should only split lanes when it is safe to do so.
          Pulling up alongside cars that are about to turn or change lanes is
          not a good idea and caution should be used whenever motorcyclists are
          entering traffic from a private drive or at an intersection.
        </p>
        <p>
          Riders should adapt their speed to reflect the flow of traffic and the
          conditions on the roadway. Slower speeds are necessary on wet surfaces
          and on roadways that have gravel or potholes. Last but not least,
          riders should not consume alcohol under any circumstances before
          operating a motorcycle.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Cypress bike crash attorneys"
            style={{
              backgroundImage:
                "url('/images/motorcycle-accidents/cypress-motorcycle-attorneys-text-banner.jpg')"
            }}
          >
            <h2>Protecting your Rights as a Motorcyclist</h2>
          </div>
        </LazyLoad>
        <p>
          The experienced Cypress motorcycle accident lawyers at Bisnar Chase
          have a long and successful track record of helping injured victims and
          their families obtain fair compensation for their damages and losses.
        </p>
        <p>
          For Cypress motorcycle accident victims, depending on the
          circumstances of the crash, compensation may be available to cover
          medical expenses, lost wages, cost of hospitalization, rehabilitation
          and other related damages. In cases involving fatalities, the victim's
          family can file a wrongful death claim seeking compensation for
          damages.
        </p>
        <p>
          Call our personal injury attorneys now at{" "}
          <strong>949-203-3814</strong> to discuss your rights and to see if you
          qualify for compensation. Your consultation is free and you only pay
          if we deliver you the compensation you deserve.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
