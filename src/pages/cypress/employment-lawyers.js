// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "employment/cypress-employment-lawyers-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Employment Attorneys - Orange County, CA"
        pageDescription="Have you been wrongfully terminated? Our Cypress Employment Attorneys can help! Call 949-203-3814 now for a free consultation. Over 40 years of won cases in California and an impressive 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Employment Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress employment lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The workplace is one of the most important places in the average
          American life, and this place houses some of the most important
          relationships in your life as well.
        </p>
        <p>
          For most people, the thought of a workplace lawsuit is very
          intimidating. These people take the attitude that work is far too
          important to risk filing a lawsuit just to collect a few dollars when
          it may ruin the important relationships you have with your co-workers
          and the possibility it may leave you unemployed.
        </p>
        <p>
          However, what do you do when your workplace is taking away some of
          your most basic human rights? What if workplace abuse has reached a
          level so high that you can no longer tolerate? What if you are being
          unfairly penalized in a monetary sense and see no way to increase your
          income? How do you know when the time has come to file a work-related
          lawsuit?
        </p>
        <p>
          The experienced{" "}
          <Link to="/cypress">Cypress personal injury lawyers</Link> at Bisnar
          Chase know all the "ins and outs" of employment law and have answers
          you need to defend your rights at work.
        </p>
        <p>
          If you feel harassed or that you are not being treated fairly at your
          current place of work, call us now at <span>949-203-3814.</span>This
          is a free, no-obligation call to discuss the facts of your case with
          our Cypress employment lawyers and to see if you qualify for
          compensation.
        </p>
        <p>
          No one should feel threatened or to act a certain way to keep their
          job that compromises their rights. If you feel this way, we urge you
          to call us now.
        </p>
        <h2>What are an Employee's Basic Rights?</h2>
        <LazyLoad>
          <img
            src="/images/employment/workers-rights-image.jpg"
            width="350"
            className="imgright-fixed"
            alt="Employment lawyers in Cypress"
          />
        </LazyLoad>
        <p>
          Employees are guaranteed basic rights under{" "}
          <Link
            to="https://www.dir.ca.gov/dlse/faq_overtime.htm"
            target="_blank"
          >
            California's wage and hour laws
          </Link>
          . Included in these rights are the right to fair compensation for work
          performed and the right to be free of harassment and abuse at work.
        </p>
        <p>
          While each state interprets employment law differently (and the laws
          are applied differently based on the size of the company involved)
          these basic rights are agreed upon by all states and the federal
          government. Workers have the right to pursue lawsuits when these
          rights are taken away.
        </p>
        <p>
          Many employees are frightened at the idea of filing a lawsuit against
          an employer, co-workers, or others in his or her company, but this may
          be the only way to preserve the employee's rights.
        </p>
        <p>
          For example, some employees have found themselves denied fair pay to
          the extent that they have left a job to seek another one. Instead, it
          would be better to force the current employer to pay a fair wage not
          only for the employee's benefit but also for those who might work for
          the company in the future.
        </p>
        <p>
          In addition, if an employee is considering new employment due to
          harassment at his/her current workplace, it would be a better idea to
          hire an experienced Cypress employment lawyer who knows the law well
          and can help both to defend the person in question that is being
          harassed and also to help said person receive compensation for their
          pain and suffering. Regardless if it is a boss or a co-worker, these
          individuals that incite harassment should be held responsible for
          their actions.
        </p>
        <h2>Sexual Harassment in the Workplace</h2>
        <p>
          Unfortunately, sexual harassment in the workforce is not all that
          uncommon. Data indicates that about 25% percent of women have been
          sexual harassed in the workplace. The{" "}
          <Link to="https://www.eeoc.gov/" target="_blank">
            {" "}
            EEOC
          </Link>{" "}
          had also reported that there had been over 28,000 claims in one year.
          Companies over time have pushed the message that employees must come
          forward if they have been sexually harassed, but 75 percent of those
          victim's experience claim to experience retaliation after they
          reported the harassment.
        </p>
        <p>
          If you have been terminated after claiming sexual harassment the law
          firm of Bisnar Chase is here to fight for your rights. Our Cypress
          Employment Lawyers believe that employees should not feel intimidated
          when speaking out about their concerns. When you call you will receive
          a free case analysis with one of our top legal members.{" "}
          <span>Call 949-203-3814.</span>
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Employment attorneys in Cypress"
            style={{
              backgroundImage:
                "url('/images/employment/whistle-blowing-cypress.jpg')"
            }}
          >
            <h2>What is a Whistle blower?</h2>
          </div>
        </LazyLoad>
        <p>
          A whistle blower in the workforce refers to person who discloses
          information that is illegal or unethical actions taking place within
          the company. Whistle blowers do have protections set in place to
          ensure that their employment is not compromised due to their actions.
        </p>
        <p>
          Whistle blowers possess the right to file a claim against their
          employer if they have responded to the employee's whistle blowing with
          termination or intimidation.  Under the OSHA ACT the whistle blower
          has 30 days to file a complaint stating that their employer has
          retaliated against their actions.
          <em>
            {" "}
            <Link
              to="https://www.osha.gov/whistleblower/WBComplaint.html"
              target="_blank"
            >
              You can download an online complaint form here
            </Link>
          </em>
          .
        </p>
        <h2>
          <strong>Filing a lawsuit involving Workers Compensation</strong>
        </h2>

        <div>
          <LazyLoad>
            <img
              src="/images/employment/employment-cypress-image-one.jpg"
              width="194"
              className="imgleft-fixed"
              alt="Cypress labor lawyers"
            />
          </LazyLoad>
          <p>
            When a worker is injured or claims to have been mentally impaired
            during their employment, those injurers have the right to file a
            worker's compensation claim. Each state obtains a &ldquo;no
            fault&rdquo; system. A &ldquo;no fault&rdquo; system states that
            even if your injury was not due to your employer's carelessness, if
            you were hurt on the job you need to be compensated.
          </p>
        </div>
        <div className="clearfix">
          <LazyLoad>
            <img
              src="../images/employment/employment-cypress-image-three.jpg"
              width="194"
              className="imgleft-fixed"
              alt="Labor attorneys in Cypress"
            />
          </LazyLoad>
          <p>
            To be eligible to file for{" "}
            <Link
              to="https://www.usworkerscomp.com/workers-comp-law/california"
              target="_blank"
            >
              workers compensation in California
            </Link>{" "}
            you need to be a paid employee of the company or organization. If
            you a volunteer or an independent contractor, you will not qualify
            for workers compensation.
          </p>
        </div>
        <div className="clearfix">
          <LazyLoad>
            <img
              src="/images/employment/employment-cypress-image-two.jpg"
              width="194"
              className="imgleft-fixed mb"
              alt="Labor lawyers in Cypress"
            />
          </LazyLoad>
          <p>
            Is there ever a time that you can file a lawsuit against your
            employer specifically; yes, in special incidents. If your employer
            performed an action to intentionally harm you then this is grounds
            for a lawsuit. For instance, if your employer was angered with you
            stating an opinion and then proceeded to punch you, you can file a
            lawsuit.
          </p>
        </div>
        <p></p>
        <h2>Contacting a Cypress Employment Attorney</h2>
        <LazyLoad>
          <img
            src="/images/employment/cypress-employment-lawyers.jpg"
            width="322"
            className="imgleft-fluid"
            alt="Cypress occupation lawyers "
          />
        </LazyLoad>
        <p>
          Cypress Employment attorneys understand the anxiety and stress that
          can keep employees from filing workplace lawsuits (even if the
          harassment or unfair treatment reaches a very high level.)
        </p>
        <p>
          At Bisnar Chase, we understand that it is frightening to think that
          you might be subject to recriminations and fired for no reason other
          than standing up for your rights, however, we won't tolerate threats
          like these and we will fight to defend your basic human rights.
        </p>
        <p>
          It also may be helpful to know that many employers take a more serious
          note an employee's claims when he or she is represented by an expert
          employment lawyer.
        </p>
        <p>
          Furthermore, Cypress employment lawyers, such as the prestige
          attorneys at Bisnar, Chase, are well aware of the possibility of
          repercussions on the part of the employer and know how to avoid these
          situations effectively.
          <span></span>
        </p>
        <p>
          If you have a wage/hour dispute, a harassment claim, or any claim
          involving unfair treatment at work, it is time to talk to the
          professionals at Bisnar Chase personal injury attorneys about your
          case.
        </p>
        <p>
          Please call us now at <span>949-203-3814</span> for a free,
          no-obligation consultation.
        </p>
        <p>
          You owe it to yourself and the ones that care about you to make your
          work relationship one of the best relationships in your life and to
          help your company provide a safe and healthy work environment for
          everyone who is employed there.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
