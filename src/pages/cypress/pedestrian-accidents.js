// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "pedestrian-accidents/cypress-banner-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Pedestrian Accident Attorneys - California - Bisnar Chase"
        pageDescription="Contact our experienced Cypress Pedestrian Accident lawyers - Call 949-203-3814 for a free attorney consultation in California. No win, no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Pedestrian Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress pedestrian accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          There are many potential causes for a Cypress pedestrian accident and
          many of these collisions that happen between motor vehicles and
          pedestrians involves negligence.
        </p>
        <p>
          Some accidents, however, occur at locations that are simply not safe
          for pedestrians.
        </p>
        <p>
          In such cases, it must be determined if the accident occurred as the
          result of negligence on the governing body in charge of maintaining
          the roadway.
        </p>
        <p>
          The <Link to="/cypress">Cypress personal injury lawyers</Link> at
          Bisnar Chase can help you pursue legal action in these cases. who has
          a successful track record of pursuing compensation from governmental
          agencies will be able to advise injured victims and their families
          regarding their legal rights and options.
        </p>
        <h2>Cypress Pedestrian Accidents</h2>
        <p>
          According to California Highway Patrol's Statewide Integrated Traffic
          Records System (SWITRS), only one person was killed in a Cypress car
          accident in 2013 and that person was a pedestrian.
        </p>
        <p>
          Additionally, 14 others were injured during that same year as a result
          of Cypress pedestrian accidents. Throughout Orange County, 41 people
          were killed and 864 were injured in pedestrian accidents.
        </p>
        <h2>Dangerous Intersections and Roadways in Cypress</h2>
        <p>
          If an intersection or the section of a roadway has had a history of
          pedestrian accidents, it must be determined if the roadway was
          defectively designed or maintained.
        </p>
        <p>
          Some locations are simply not safe for pedestrians. A few examples of
          hazardous conditions that can result in pedestrian accidents include:
        </p>
        <ul>
          <li>
            <strong>Dangerous design:</strong> Crosswalks and intersections
            should not be placed on tight turns or steep inclines. At these
            types of locations, drivers may fail to notice pedestrians who are
            legally crossing the street until it is too late. For example, an
            unmarked or unlit crosswalk can pose significant dangers to
            pedestrians especially in adverse weather conditions or at nighttime
            when visibility is reduced. When numerous accidents occur at the
            same intersection, it must be determined if the roadway was
            defective and if the governmental agency in charge of maintaining
            the roadway had sufficient notice and time to fix the problem.
          </li>

          <LazyLoad>
            <img
              src="/images/pedestrian-accidents/cypress-pothole-image.jpg"
              width="285"
              className="imgright-fixed"
              alt="Cypress pedestrian accident lawyers"
            />
          </LazyLoad>

          <li>
            <strong>Poor maintenance:</strong> Are there trees or shrubs
            blocking traffic control devices? Are the crosswalk lines faded or
            worn? Are there potholes or uneven surfaces on the roadway? The
            governing body in charge of the roadway is required by law to keep
            the road safe for pedestrians and motorists. When an accident occurs
            as the result of roadway defects that could have been simply fixed,
            the city or governmental entity in charge of maintaining the roadway
            can be held liable.
          </li>

          <li>
            <strong>Lack of sidewalks and shoulders:</strong> There are some
            roadways that are not designed for pedestrians. When it is not safe
            to walk alongside a roadway, there should be warning signs posted.
            Warning signs should also be posted on sidewalks that are closed for
            construction or repairs. Most cities require contractors and
            construction companies to provide an alternative route for
            pedestrians who cannot use a sidewalk or walkway during
            construction.
          </li>
          <li>
            <strong>Dangerous off-ramps:</strong> Vehicles leaving the freeway
            may still be traveling at a rate of speed that makes them extremely
            dangerous to pedestrians. If the off ramp is not long enough to
            allow the driver to slow down, pedestrians who may be legally
            crossing at the light on an off-ramp may be in danger.
          </li>
          <li>
            <strong>Lack of signs:</strong> Some intersections have no posted
            signage. For example, when a crosswalk is not marked with lights or
            bright signs, motorists may not realize that they are approaching a
            crosswalk until it is too late.
          </li>
        </ul>
        <h2>Government Liability for Pedestrian Accidents</h2>
        <p>
          Determining liability for a pedestrian accident, however, can be a
          complicated process. In order to hold a governmental entity
          accountable for a pedestrian accident, the victim must prove that the
          accident occurred at a dangerous location, that he or she was injured
          as a result of such a hazardous condition and that the dangerous
          condition was not fixed in a timely manner. Under{" "}
          <Link
            to="https://leginfo.legislature.ca.gov/faces/codes_displaySection.xhtml?lawCode=GOV&sectionNum=911.2"
            target="_blank"
          >
            {" "}
            California Government Code Section 911.2
          </Link>
          , any personal injury or wrongful death claim against a public entity
          must be properly filed within six months of the incident.
          <LazyLoad>
            <img
              src="/images/pedestrian-accidents/cypress-pedestrian-accident-image.jpg"
              width="294"
              className="imgright-fixed"
              alt="Pedestrian accident lawyers in Cypress"
            />
          </LazyLoad>
          This means if you want to purse legal action, your time is limited and
          you need to act now.
        </p>
        <p>
          Victims of dangerous roadway accidents can seek compensation for
          medical bills, lost wages, pain, suffering and other related damages.
          The experienced Cypress pedestrian accident lawyers at Bisnar Chase
          have an excellent track record of pursuing compensation from
          government agencies and other at-fault parties on behalf of severely
          injured victims and their families.
        </p>
        <p>
          To obtain more information about pursuing your legal rights, please
          contact us for a<strong> free consultation </strong>at{" "}
          <strong>949-203-3814</strong>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
