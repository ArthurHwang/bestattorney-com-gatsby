// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "personal-injury/cypress-index-page-photo.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Personal Injury Lawyers - Orange County, CA"
        pageDescription="Call 949-203-3814 now if you have suffered personal injury from an auto accident. The Cypress personal injury lawyers at Bisnar Chase will fight for you. Free Consultation. No win, no fee and an impressive 96% success rate winning cases in and out of court rooms."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Personal Injury Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress Personal Injury Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          Victims of uninsured motorists or{" "}
          <Link to="/cypress/car-accidents" target="_blank">
            car accidents
          </Link>{" "}
          can still pursue financial compensation for their losses with the help
          of an experienced <strong>Cypress Personal Injury Lawyer</strong>.
          Bisnar Chase will help you negotiate an accident settlement and help
          you get back on your feet again.
        </p>
        <p>
          If you or a loved one have suffered from a personal injury due to a
          third party's carelessness call the Cypress personal injury attorneys
          of Bisnar Chase. <strong>For 40 years</strong>, our clients who have
          obtained our legal representation have{" "}
          <strong>gained millions in compensation</strong> after their accident.
        </p>
        <p>
          Our expert legal team members are on standby to take your call{" "}
          <strong>Monday-Friday 8 a.m.-5 p.m</strong>.
        </p>
        <p>
          Contact us at <strong>(949) 203-3814</strong> and receive a{" "}
          <strong>free consultation</strong>.
        </p>
        <h2>Insurance Requirements in Cypress, California</h2>

        <p>
          All California drivers must obtain insurance that meets or exceeds the
          minimum requirements as mandated by law.
        </p>
        <p>
          In California, all drivers must have coverage for $15,000 for death or
          injury to one person, $30,000 for all persons in any one accident and
          $5,000 for any one accident. Unfortunately, cases involving serious
          injuries require more insurance than usually provided by these minimum
          requirements.
        </p>
        <p>
          Anyone who has needed hospitalization or surgery knows that $15,000
          only scratches the surface of what type of compensation will be needed
          following the accident. Even so, many California motorists fail to
          obtain even the bare minimum amount of insurance required by law.
        </p>
        <p>
          The percentage of uninsured motorists in California continues to
          increase with every passing year.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.insurance-research.org/" target="_blank">
            Insurance Research Council
          </Link>
          , in the year 2007 approximately 18 percent of California drivers did
          not have coverage. If that number is accurate, California was well
          above the national average of 13.8 percent of uninsured motorists.
          California was in the top 10 states with the most number of uninsured
          motorists during that same year.
        </p>

        <hr />
        <p></p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/s2hGIOhPCNc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Personal injury lawyer Brian Chase explains how to win against
            insurance companies{" "}
          </blockquote>
        </center>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/DkKmXYasbr4"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Personal injury lawyer Brian Chase explains what to watch out for
            and why insurance companies are not on your side.{" "}
          </blockquote>
        </center>

        <hr />

        <h2>5 Different Types of Personal Injury Accidents in Cypress</h2>
        <p>
          There are a variety of accidents that take place every day.
          Unfortunately the outcome of those accidents a majority of the time is
          tragic. Many people are left severely injured to the point of being
          disabled or may not even survive the incident. Below are five types of
          personal injury accidents that commonly occur in California.
        </p>
        <p>
          <strong>5 common types of personal injury cases</strong>:
        </p>
        <p>
          <strong>Hit and Run Accidents</strong>: One type of accident that
          proves to be challenging for victims and their families are
          hit-and-run accidents where the at-fault driver flees the scene and is
          not found by authorities. California Vehicle Code 20001 (a) states:
          "The driver of a vehicle involved in an accident resulting in injury
          to a person, other than himself or herself, or in the death of a
          person shall immediately stop the vehicle at the scene of the
          accident." Despite harsh penalties for leaving the scene of an
          accident, it is common for motorists to flee an accident without
          exchanging information or contacting the authorities. In such cases,
          financial compensation may still be available for the victim through
          his or her own insurance policy.
        </p>
        <p>
          <strong>Rear-end collisions</strong>: When driving behind a car,
          California Vehicle Code 21703 states that “the driver of a motor
          vehicle shall not follow another vehicle more closely than is
          reasonable and prudent, having due regard for the speed of such
          vehicle and the traffic upon, and the condition of, the roadway.” The
          distance that a driver should have between cars can be based off
          weather conditions, defective brakes, weight of a vehicle, driving at
          night and collapsed gravel. Drivers should leave a 4-second gap
          between cars.
        </p>

        <p>
          <strong>Pedestrian incidents</strong>: In one year over 4,735
          pedestrians were killed in an accident. California over time has
          become the state with the highest pedestrian incidents. The number one
          cause of pedestrians being injured or killed is due to texting and
          driving. Despite texting and driving being illegal in California,
          drivers still proceed to use cell-phones while operating a motor
          vehicle. The base fine for texting in driving in California is $20
          dollars.
        </p>
        <LazyLoad>
          <img
            src="/images/personal-injury/victim-crosswalk-image.jpg"
            width="384"
            className="imgright-fluid"
            alt="Personal injury attorneys in Cypress"
          />
        </LazyLoad>
        <p>
          <strong>Parking-lot crashes</strong>: Many people do not know that
          there are right-away rules when driving in a parking lot. Pedestrians
          and people who are driving through the lane always have the
          right-away. Parking lots have two different classifications of lanes
          which are known as thoroughfare lanes and feeder lanes. Thoroughfare
          lanes are lanes that exit on to a street whereas feeder lanes are the
          smaller lanes throughout the parking lot. One of the most common types
          of parking lot accidents occurs when a car is backing out of a parking
          space. The fault falls on the person who is backing out of the space,
          but if both vehicles were backing out of a space, both drivers share
          in the fault.
        </p>
        <p>
          <strong>T-bone wreck</strong>: The National Transportation Highway
          Safety Administration reports that 8,000 people lose their lives every
          year in t-bone accidents. A t-bone car crash is when one vehicle
          strikes the side of another vehicle head-on. One of the most frequent
          scenarios for t-bone accidents is when people fail to stop or yield at
          intersections. Experts say that to in order to avoid a t-bone
          collision, drivers need to proceed with caution into an intersection
          and be extra careful when there are large trucks making a turn. Spinal
          injuries and traumatic brain injuries are the most common types of
          injuries that victims of t-bone accidents suffer from.
        </p>
        <h2>Legal Liability for a Cypress Personal Injury</h2>
        <p>
          In order to determine liability, the plaintiff must prove that the
          defendant was negligent. If injuries could have been prevented but
          were not, the fault can automatically fall on the negligent p[arty.
          The injured victim also must prove that it was the duty of the
          negligent party to care for them. One prime example is that all motor
          vehicle drivers must operate the car carefully to ensure the safety of
          fellow motorists on the road.
        </p>
        <p>
          If the defendant failed to provide care for the injured party than
          this action may be looked upon as a legal breach. This can aid in the
          personal injury case of the victim because costly damages can then be
          recovered.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Personal injury attorneys in Cypress"
            style={{
              backgroundImage:
                "url('/images/personal-injury/stressed-man-text-header-image.jpg')"
            }}
          >
            <h2>Recovering your Losses in Cypress</h2>
          </div>
        </LazyLoad>
        <p>
          <strong>
            There are many ways in which people get injured in the city of
            Cypress, California
          </strong>
          :
        </p>
        <ul>
          <li>Auto accidents</li>
          <li>Assaults</li>
          <li>Nursing home abuse or neglect</li>
          <li>Dog bites</li>
          <li>Medical malpractice</li>
          <li>Defective products</li>
        </ul>
        <p>
          These instances are all common examples of the ways in which people
          get injured as a result of someone else's negligence or wrongdoing.
        </p>
        <p>
          In such cases, the civil justice system gives injured victims the
          opportunity to seek monetary compensation from the at-fault parties
          for the damages they have suffered.
        </p>
        <p>
          In personal injury cases, the damages that are commonly sought include
          medical expenses, loss of wages, cost of hospitalization,
          rehabilitation and other related damages.
        </p>

        <h2>How to Contact a Cypress Injury Lawyer</h2>
        <p>
          The experienced Cypress personal injury attorneys at{" "}
          <Link to="/" target="_blank">
            Bisnar Chase
          </Link>{" "}
          have over three decades of experience upholding the rights of injured
          victims and their families.
        </p>
        <p>
          Our personal injury lawyers will explain the legal issues surrounding
          your personal injury case and fight to help you receive fair
          compensation for your losses.
        </p>
        <p>
          The best personal injury law firms will not only offer a free and
          comprehensive consultation, but will also offer you a no-fee
          guarantee. This means that you choose Bisnar Chase, you do not have to
          pay us any fees unless they win your case.
        </p>
        <p>
          If you need immediate assistance, please contact us now at{" "}
          <strong>(949) 203-3814</strong> to schedule a free consultation now.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
