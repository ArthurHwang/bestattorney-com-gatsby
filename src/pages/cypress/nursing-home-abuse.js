// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: ""
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "nursing-home/cypress-nursing-home-abuse-lawyers-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Nursing Home Abuse Lawyers - California - Bisnar Chase"
        pageDescription="Call us now at 949-203-3814 if your loved one has been abused in a nursing home. The Cypress nursing home attorneys at Bisnar Chase will deliver justice for your family. Free consultations."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Nursing Home Abuse Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress Nursing Home Abuse Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          The <strong>Cypress Nursing Home Lawyers</strong> at Bisnar Chase work
          with the victims of nursing home abuses to collect damages for poor
          care and treatment that results in physical or emotional injury. 
          Victims of nursing home abuses suffer a wide variety of problems,
          including physical pain and trauma.
        </p>
        <p>
          There are even rare cases in which nursing home residents die due to
          poor living conditions and the lack of care offered in their
          residential facility.
        </p>
        <p>
          The{" "}
          <Link to="https://ncea.acl.gov/" target="_blank">
            {" "}
            National Center on Elder Abuse
          </Link>{" "}
          warns that nursing home abuse and neglect is a growing problem.  In
          fact, about 11 percent of all nursing home residents report some form
          of abuse or neglect.  Given that this is most likely a highly
          underreported crime, the chances are that at least one-quarter or more
          of nursing home residence have suffered abuse and neglect.
        </p>
        <p>
          If your loved one has experienced elder abuse or neglect, call us now
          at <strong>949-203-3814</strong>. The lawyers at Bisnar Chase at the
          most experienced{" "}
          <Link to="/cypress"> Cypress personal injury attorneys</Link> in the
          state of California and we will do whatever it takes to bring justice
          to your family.
          <strong> We offer</strong>{" "}
          <strong>
            free consultations, you do not have to pay if we lose, and if you
            can not come to our office, we will come to you!
          </strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/I8PzDLG0LPc?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Attorney Brain Chase speaks on elder abuse and nursing home neglect.
          </blockquote>
        </center>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/1iI3ZGIkzgs?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Attorney Brain Chase comments on what makes a good nursing home for
            your loved one. He also discusses the warning signs of elder abuse.
          </blockquote>
        </center>

        <h2>What are the Basic Rights of Every Nursing Home Resident?</h2>
        <p>
          Nursing home residents have rights that should never be violated under
          any circumstance.
        </p>
        <p>
          Unfortunately, the lack of training and supervision of some nursing
          home employees means that many patients' rights are not considered. 
          Further, nursing homes may attract a certain type of criminal that
          enjoys preying on the weak and helpless, and sees the nursing home as
          a place to supply victims.
        </p>
        <p>
          No matter what the conditions are in the nursing home, no one has a
          right to infringe on the victim's privacy and right to live free of
          pain and suffering.
        </p>
        <p>
          However, nursing home crimes are often never reported. This is why
          many of these cases never see the light of day, however, if you decide
          to chose the seasoned Cypress nursing home attorneys at Bisnar Chase
          to take legal action, we assure you that we will get you the
          compensation you deserve. We will do all the work and you and your
          family can focus on healing and recovery.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Nursing home abuse lawyers in Cypress"
            style={{
              backgroundImage:
                "url('/images/nursing-home/cypress-elder-abuse-image-text-header.jpg')"
            }}
          >
            <h2>Elder Abuse Statistics</h2>
          </div>
        </LazyLoad>

        <p>
          One study estimates that only 20 percent of all the crimes committed
          in nursing homes are ever spoken of to anyone else.
        </p>
        <p>
          This means that four out of five nursing home residents (roughly 80
          percent) who are victims of abuse do not speak out.
        </p>
        <p>
          This is sometimes from fear; other times the resident may be unable to
          express what is happening to them due to illness, strokes, or diseases
          such as{" "}
          <Link
            to="https://www.alz.org/alzheimers_disease_what_is_alzheimers.asp"
            target="_blank"
          >
            Alzheimer's
          </Link>
          .
        </p>
        <p>
          If you have a loved one in a nursing home who is non-communicative, it
          is even more important that you take responsibility for checking up on
          your loved one regularly.
        </p>

        <h2>Nursing Home Abuse Attorneys in Cypress</h2>
        <LazyLoad>
          <img
            src="/images/bisnar-chase/BisnarChasePhotosmall.jpg"
            width="322"
            height="281"
            className="imgright-fluid"
            alt="Nursing home abuse attorneys in Cypress"
          />
        </LazyLoad>
        <p>
          Nursing home lawyers in Cypress, such as the ones at Bisnar Chase,
          help you and your family keep your loved one safe and ensure that no
          one will hurt them.  This is because once nursing home abuses are
          reported they are quite likely to stop immediately.
        </p>
        <p>
          Most abuses in nursing homes are crimes of opportunity or crimes of
          neglect, and they are often relatively easy to end.  Even in cases
          where there is an active aggression against nursing home residents,
          the people committing these outrages will not be able to continue once
          the spotlight is trained on their actions.
        </p>
        <p>
          It is important to act quickly if you suspect nursing home abuse. 
          These types of problems tend to develop rapidly and can make life for
          your loved one nothing short of a nightmare.
        </p>
        <p>
          If a person who is abusing your loved one is left unchecked, he or she
          may continue until real damage is done.  Instead of waiting and
          letting the problem escalate, call the professional Cypress nursing
          home lawyers at Bisnar Chase today and begin the process of setting
          your mind at ease. Please call us now at <strong>949-203-3814</strong>{" "}
          for a free consultation.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
