// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "car-accidents/cypress-car-crash-banner.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Car Accident Lawyers - Orange County, CA"
        pageDescription="If you've been injured in a Cypress car accident please contact the experienced trial lawyers of Bisnar Chase for a free and comprehensive consultation. Our Cypress car accident attorneys have four decades of winning cases in Orange County. "
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Car Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress Car Accident Attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          A seasoned <strong>Cypress Car Accident Attorney</strong> knows that
          if you've been injured in a car crash and expect to be fairly
          compensated for your losses, you must exercise skill and caution when
          dealing with insurance companies, especially the insurance adjuster.
          Before delving into this area, a quick review of car accidents in
          Cypress, California may be helpful. This is where the experienced{" "}
          <Link to="/cypress">Cypress personal injury lawyers</Link> at Bisnar
          Chase can help you get the settlement that you deserve.
        </p>
        <h2>Cypress Auto Accident Statistics</h2>
        <p>
          California Highway Patrol's Statewide Integrated Traffic Records
          System (SWITRS) showed that four people died and 172 were injured in
          Cypress car crashes in one year alone.
        </p>
        <p>
          Drunk drivers caused one death and 16 injuries. A total of 20
          bicyclists and 12 pedestrians were injured in traffic accidents.
          Motorcycle collisions killed one and injured nine. Four car accidents
          resulted in four fatalities.
        </p>
        <h2>How to Provide Insurance Adjusters with Details of Your Losses</h2>
        <p>
          Smart Cypress car accident lawyers will tell you to provide your
          insurance adjuster with detailed documents of your damages and losses.
        </p>
        <p>
          Ask the adjuster how much the insurance company is willing to pay for
          the repair or replacement of your vehicle and what they will offer for
          your lost wages, your doctor, hospital and other medical bills. If
          your injuries call for extended long-term care or rehabilitation, ask
          about compensation for those expenses. Lastly, ask how much they will
          pay you for your pain and suffering.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/WSuOEz40ZPA"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Brian Chase explains how hiring an experienced car accident attorney
            can help you get your car repaired and how to get fully compensated.
          </blockquote>
        </center>

        <h2>How to Deal With Insurance Adjusters</h2>
        <p>
          Remember that insurance adjusters are highly skilled negotiators. They
          will "hem and haw" and insist you make the first settlement demand (a
          specific dollar sum.) This keeps them in control of the negotiations.
        </p>
        <p>
          They realize that you could easily ask for less than what they're
          willing to offer. Cypress car accident lawyers say this is where you
          play tough and let the adjuster make the first offer; a realistic one.
        </p>
        <p>
          If the adjuster makes you a poor offer, say you're considering hiring
          a lawyer.
        </p>
        <p>
          When the adjuster finally commits to an offer, you can make a counter
          offer, however, don't be quick to do so. Tell the adjuster that you
          want some time to think it over, to consult with your lawyer and other
          family members. Keep the adjuster guessing and remember to act
          disappointed (never rude or belligerent) with any offer he makes.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/PJ0iP0dTqbc"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Are the insurance companies keeping your best interest in mind after
            a Cypress, California car accident? Brian Chase weighs in and sets
            the record straight.
          </blockquote>
        </center>

        <h2>5 Steps to Take for Preventing a Traffic Collision</h2>
        <p>
          In one year over 9,500 people lost their lives by simply not wearing a
          seatbelt. Car crashes at times cannot be avoided, but there are
          preventative measures that you can easily take to help keep auto
          accidents from happening often.
        </p>
        <p className="text">Below are five steps for preventing car crashes:</p>
        <ol>
          <li>
            <strong>Always look out for pedestrians</strong>: The rule of the
            road is that pedestrians always have the right away. There are
            incidents though where a person will jaywalk or a child will
            suddenly appear on the road. If you are near a school or a
            residential area pay attention and proceed slowly and cautiously
            through the road.
          </li>
          <li>
            <strong>Keep a cushion space between cars</strong>: DMV states that
            you should wait two seconds behind a car which equals three meters.
            When you allow yourself an ample amount of space between cars, you
            will have a better chance of stopping before rear-ending a vehicle.
          </li>
          <li>
            <strong>Provide your car with the necessary maintenance</strong>:
            Auto parts such as tires, lights, brakes and oils should be double
            checked regularly. Supplying your vehicle with routine check-ups can
            not only prevent an accident but it can also prevent a great amount
            of damage to your car. For example, if you do not change your oil in
            due time, it can cause the engine to overheat and can wear it out
            immensely.
          </li>
          <li>
            <strong>Check your blind spots</strong>: A driver should not solely
            rely on their mirrors to examine the area around them. Checking your
            blind spot aids in seeing cars that are pulling up on the right or
            left-hand side of you. Making sure your blind spot is clear does not
            only keep drivers safe but when turning it is also meant to keep
            cyclist and pedestrians free from harm.
          </li>
          <li>
            <strong>Be kind on the road</strong>: Road rage for many is a daily
            reality. Road rage is one of many causes that lead to car crashes.
            Drivers who exhibit this behavior may be inclined to shove other
            automobiles off of the roads or using a weapon to threaten the other
            driver. Experts say that if you do suffer from road rage, calm
            yourself by playing relaxing music and not using vulgarity to
            express discontent with fellow drivers.
          </li>
        </ol>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Car crash lawyers in Cypress"
            style={{
              backgroundImage:
                "url('/images/car-accidents/texting-driving-text-header.jpg')"
            }}
          >
            <h2>Primary Causes of Auto Accidents</h2>
          </div>
        </LazyLoad>
        <p>
          Being involved in an auto accident can be traumatic and leave many
          victims with serious injuries. Minor actions such as looking down at
          your phone can lead to you having dangerous consequences. Your driving
          behavior not only affects you and your passengers, but poor driving
          can also endanger the lives of your fellow drivers.
        </p>
        <p className="text">Primary causes of car accidents:</p>
        <p>
          <strong>Texting and Driving</strong>: Over the years millions of
          people have had emergency room visits and have also been killed when
          texting and driving. What's even more shocking is that a driver is six
          times more likely to get in a car accident by distracted driving than
          by drunk driving. Reports also state that one in four car accidents
          are caused by cell phone usage. Do not text and drive, if the text or
          call proves to be an emergency than pull over to a safe area. .
        </p>
        <p>
          <strong>Drunk driving</strong>: In one year over one million drivers
          were arrested for being under the influence while driving a motor
          vehicle. Driving drunk can impair your motor skills and perception. If
          you are arrested for driving under the influence you can face up to 16
          months in jail and accumulate fines up to $18,000 dollars.
        </p>
        <p>
          <strong>Speeding</strong>: If you are driving over the speed limit
          especially in bad weather it can result in injuries that lead a person
          to be paralyzed or worse; dead. When a driver is speeding the
          likely-hood of them losing control of the wheel increases. In
          California, if you drive 15 MPH over the speed limit you can be fined
          an amount up to $360 dollars.
        </p>
        <h2>If Things Get Too Complicated, Call Us!</h2>
        <LazyLoad>
          <img
            src="/images/car-accidents/cypress-car-crash.jpg"
            width="279"
            className="imgright-fluid"
            alt="Cypress auto collision lawyers"
          />
        </LazyLoad>
        <p>
          An experienced Cypress car crash lawyer, like the attorneys at Bisnar
          Chase, who regularly negotiates with insurance adjusters in California
          can be a very powerful ally. At Bisnar Chase, we can help you
          negotiate a fair settlement for your losses. We are highly
          credentialed car accident lawyers in Cypress, California. Your
          consultation is free and we operate on a contingency basis, which
          means you only pay if we win.
        </p>
        <p>
          If you have been injured please call us toll free to receive a{" "}
          <span className="text">Free Consultation</span> from our experienced
          staff. <span className="text">Call 949-203-3814</span>.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
