// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "slip-and-fall/cypress-slip-fall-injury.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Slip-and-Fall Attorneys - California - Bisnar Chase"
        pageDescription="If you were injured in a Cypress slip-and-fall accident, call 949-203-3814 for essential attorney help. Free consultations. No win no fee."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Slip and Fall Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress Slip and Fall Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Slip-and-fall incidents sometimes result in scrapes or sprains and
          strains. Unfortunately, sometimes, a sudden fall can result in serious
          injuries that affect the victim's quality of life.
        </p>
        <p>
          Slip-and-fall accidents may even result catastrophic injuries, or
          injuries that may result in permanent disabilities.
        </p>
        <p>
          Injuries from these types of accidents affect a victim's ability to
          return to work, perform the activities he or she was performing prior
          to the accident or even earn a livelihood.
        </p>
        <p>
          If you have been injured due to the negligence of a property owner,
          call the experienced{" "}
          <Link to="/cypress">Cypress personal injury attorneys</Link> at Bisnar
          Chase now who will help protect your legal rights and best interests.
        </p>
        <h2>What Injuries Are Considered Catastrophic?</h2>
        <p>
          A catastrophic injury is a serious injury that has long-lasting
          negative effects on all aspects of the victim's life. Catastrophic
          injuries may make everyday activities difficult or impossible. Victims
          may require assistance throughout the day to perform routine physical
          tasks. Others may never again return to work. This is why it is
          imperative to contact a professions Cypress slip-and-fall accident
          attorney if you have been injured.
        </p>

        <p>
          At Bisnar Chase, we help victims of slip-and-fall accidents rebuild
          their lives after a fall and fight to get them the compensation they
          deserve. When you choose Bisnar Chase, you choose a law firm that will
          make the legal process as easy as possible for you and your family.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/9-UJdHzYPw4?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/qvkV_ShDjAE?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Catastrophic Injuries Suffered in Slip-and-Fall Accidents</h2>
        <p>
          A slip and fall can happen anywhere, including on the steps of a
          courthouse
          {/* {" "}<Link to="http://www.grandhaventribune.com/Courts/2017/01/19/County-sued-over-slip-and-fall-on-courthouse-steps.html?ci=stream&lp=6&p=1">
            courthouse
          </Link> */}
          . Regardless of the situation, it is the property owner's legal
          responsibility to ensure a safe environment.
        </p>
        <p>
          There are a number of serious injuries that can occur as a result of a
          Cypress slip-and-fall incident.
        </p>
        <ul>
          <li>
            <strong>Multiple bone fractures:</strong> It is common for victims
            of slip-and-fall incidents to suffer a broken wrist or ankle.
            Sometimes the fall is so severe that victims experience multiple
            bone fractures that never fully heal. Elderly victims of
            slip-and-fall accidents, for example, may suffer a broken leg and
            broken hip. In such cases, they may never regain enough strength to
            live independently. Other victims may suffer from broken bones in
            their legs, arms or torso that may require extensive treatment and
            rehabilitation.
          </li>
          <li>
            <strong>Traumatic brain injuries:</strong> If the victim slips and
            strikes his or her head, there is the potential for brain injuries
            to occur. Anyone who has lost consciousness following a
            slip-and-fall accident should seek immediate medical attention.
            Other potentially serious symptoms include severe headaches, nausea,
            dizziness and confusion. Sadly, many brain injury victims never
            fully recover. Victims of catastrophic brain injuries may experience
            a decrease in mental or physical abilities. There is a wide range of
            symptoms that may persist following a TBI and many victims require
            long-term care to help them perform everyday activities. The cost of
            a brain injury could add up to millions of dollars over the
            patient's lifetime.
          </li>
          <li>
            <strong>Spinal cord injuries:</strong> One of the reasons why
            slip-and-fall accidents are so dangerous is because of the violent
            and awkward way in which victims can strike the ground. If the
            victim twists or bends his or her neck or back during the fall,
            there is the potential for a serious injury to occur. Some victims
            of slip-and-fall accidents sustain trauma to their spinal cord. When
            this occurs, it is possible that the victim may never again regain
            feeling below the site of the trauma. Paralysis is a catastrophic
            injury that is often impossible to recover from.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Slip and fall injury lawyers in Cypress"
            style={{
              backgroundImage:
                "url('/images/slip-and-fall/cypress-slip-fall-text-header.jpg')"
            }}
          >
            <h2>The Cost of Injuries from an Accident</h2>
          </div>
        </LazyLoad>
        <p>
          The true cost of a catastrophic injury can be challenging to calculate
          because they result in permanent disabilities. They affect the injured
          victim and his or her family in a profound way. These injuries have a
          significant impact on the victim's quality of life and his or her
          ability to earn a livelihood. A skilled Cypress slip-and-fall attorney
          can help victims and their families calculate not only the expenses
          incurred as a result of the injury, but also the types of losses that
          will be sustained in the future such as lost income, cost of
          continuing treatment and care and rehabilitation services.
        </p>
        <h2>Picking Yourself Up After You Have Fallen</h2>

        <p>
          The reputed Cypress slip-and-fall lawyers at Bisnar Chase have a long
          and successful track record of helping catastrophically injured
          victims and their families. We understand the emotional and financial
          burdens victims and their families face.
        </p>
        <p>
          If a slip-and-fall injury was caused by someone else's negligence,
          then, that individual or entity could be held liable for the damages
          and losses sustained by the victim. If you or a loved one has been
          injured in a Cypress slip-and-fall accident, please contact us now at{" "}
          <strong>949-203-3814</strong> for a free consultation and
          comprehensive case evaluation.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
