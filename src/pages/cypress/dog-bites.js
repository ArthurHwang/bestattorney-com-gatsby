// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "dog-bites/dog-eye-close-up-header-image.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Dog Bite Lawyers - Orange County, CA"
        pageDescription="Call 949-203-3814 now if you have been injured in a dog bite accident in Orange County, California. Our Cypress dog bite lawyers provide quality client care & free consultations with a 96% success rate ."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Dog Bite Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress dog bite attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          No one wants to have a dog &ldquo;put down&rdquo; for biting. However,
          by allowing their pets to develop bad behaviors, many Cypress dog
          owners sentence their pets to death for this very infraction.
        </p>
        <p>
          It is the owner's responsibility to see that the dog is well-trained
          and well-behaved, but many dog owners do not think of this until
          someone has already been bitten.
        </p>
        <p>
          The experienced{" "}
          <Link to="/cypress">Cypress personal injury attorneys</Link> at Bisnar
          Chase work with the victims of dog bite attacks to give them the
          support they need to pursue a case against a negligent owner. Our dog
          bite lawyers have been representing injured plaintiffs in Orange
          County for over 4 decades. We have taken on some of the most difficult
          dog bite cases with winning results.
        </p>
        <h2>Dog Bites Accidents Statistics</h2>
        <p>
          Dog bites are some of the most common physical injuries in our
          country. Of the millions of dog bites that occur every year, at least
          850,000 make their way to emergency rooms across the country; at least
          350,000 of those require medical treatment.
        </p>
        <p>
          This means that a large percentage of dog bites are not friendly snaps
          or nips, but honest attempts by an aggressive animal to injure a
          person. In most cases, the dog bites a stranger or visitor to the
          home, although some very serious cases of bites to owners have also
          been documented.
        </p>
        <h2>How Can a Lawyer Help Me in a Dog Bite Accident Case?</h2>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/6wAqpIBOw6A?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <center>
          <blockquote>
            Injured in a dog bite accident? Attorney Brian Chase can help!
          </blockquote>
        </center>

        <p>
          The Cypress dog bite attorneys at Bisnar Chases begin the process of
          pursuing a dog bite claim by reviewing the facts of the case.
        </p>
        <p>
          Although most dog bite cases settle quickly, some require more work,
          especially if the damages have been extensive. Some types of dog
          attacks are highly publicized and emotionally charged, such as those
          by pit bulls.
        </p>
        <h2>What Causes Dog Bite Injuries?</h2>
        <div>
          <LazyLoad>
            <img
              src="/images/dog-bites/dog-bite-image-resized.jpg"
              width="213"
              className="imgleft-fixed"
              alt="Dog bite lawyers in Cypress"
            />
          </LazyLoad>
          <p>
            According to the{" "}
            <Link to="https://www.cdc.gov/" target="_blank">
              {" "}
              Centers for Disease Control
            </Link>
            , pit bull types are responsible for more than twice the number of
            fatal attacks as any other breed, followed closely by Rottweilers.
          </p>
        </div>

        <div className="clearfix">
          <LazyLoad>
            <img
              src="/images/dog-bites/sick-dog-image.jpg"
              width="213"
              className="imgleft-fixed"
              alt="Cypress dog bite lawyers"
            />
          </LazyLoad>
          <p>
            Proponents for these dog breeds have claimed that poor ownership
            skills, and not the dogs themselves, are the reasons for these
            numbers.
          </p>
        </div>

        <div className="clearfix">
          <LazyLoad>
            <img
              src="/images/dog-bites/female-pups-image.jpg"
              width="213"
              className="imgleft-fixed mb"
              alt="Dog injury attorneys in Cypress"
            />
          </LazyLoad>
          <p>
            These are two of the most popular breeds selected for dog fighting,
            so some experts speculate that while the breed itself is not
            threatening, the fact that so many of these animals are being bred
            and taught to be aggressive makes them a problem. Dogs bite due to
            multiple reasons some of which include feeling threatened, sick or
            have a need to protect their pups.
          </p>
        </div>

        <p>
          On the other hand, the vast majority of dog bites have nothing to do
          with pit bulls or any specific breed.
        </p>
        <p className="clearfix">
          In fact, a damaging bite to the hand is far more common than mauling
          of the victim, and any dog can be associated with biting a person's
          hand. Smaller breeds may actually be more likely to bite in this way
          out of fear or intimidation.
        </p>
        <h2>How to Take Legal Action After a Dog Bite Injury</h2>
        <p>
          No matter how the bite happens, once it has occurred the owner must
          take responsibility. It does no good for the owner to claim, &ldquo;He
          never bit anyone before! Any bite is a bad bite, and can lead to
          further problems as the dog discovers it can behave in this way. It is
          very important to hold dog owners accountable so that they do not
          allow their animals to become more and more aggressive.
        </p>
        <p>
          If you have been the victim of a dog bite accident, it is important
          that you discuss your case with an attorney immediately, and pursue
          damages against the owner of the dog. Please call us at
          <span> 949-203-3814</span> for a free consultation.
        </p>
        <p>
          <span>
            Immediately call the experienced and reputable Orange County
            Personal Injury Lawyers at Bisnar Chase for a free consultation at
            949-203-3814.
          </span>
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
