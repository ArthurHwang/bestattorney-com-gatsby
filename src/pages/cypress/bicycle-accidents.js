// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bike-accidents/cypress-bike-accident-header-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Cypress Bicycle Accident Attorneys – California - Bisnar Chase"
        pageDescription="Call 949-203-3814 for top rated Costa Mesa Bicycle accident lawyers specializing in serious pedestrian related accidents. No win, no fee and a 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Cypress Bicycle Accident Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Cypress bicycle accident attorneys"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Cypress is a is a wonderful place to ride a bike because of the many
          urban trails. Unfortunately, Cypress is also a city that has a fair
          share of bicycle accidents that result in serious injuries and
          fatalities.
        </p>
        <p>
          According to California Highway Patrol's 2010 Statewide Integrated
          Traffic Records System (SWITRS), three people were killed and 1,187
          were injured in Orange County bicycle accidents. During that same
          year, there were no fatalities, but 28 injuries involving bicyclists
          in Cypress.
        </p>
        <p>
          If you have suffered personal injury form a Cypress bicycle accident,
          call Bisnar Chase's experienced{" "}
          <Link to="/cypress">Cypress Personal Injury Attorneys</Link> now at
          <strong> (949) 203-3814</strong> for a free, no obligation
          consultation. You may be entitled to compensation and we will fight
          for you!
        </p>
        <h2>Ways to Prevent Bicycle Accidents in Cypress, California</h2>
        <p>
          Many of the most devastating Orange County bicycle accidents involve a
          collision between a car and a bicyclist. Here are a few tips to help
          you avoid being struck by a car while riding in Cypress:
        </p>
        <ul>
          <li>
            <strong>Use Hand Signals:</strong> It may feel strange to raise your
            arm when turning right or to point left when turning left, but hand
            signals are important for bicyclists and in some cases can be
            life-saving. Letting car drivers know where you are going can help
            you avoid being struck by a car. When you are slowing down, point
            your left arm toward the ground. Let car drivers know what your
            intentions are.
          </li>
          <li>
            <strong>Increase Your Visibility:</strong> If you choose to ride at
            night, it is crucial that you obey California's many bike laws.
            Under California Statute 21201, all bike riders must have a front
            lamp that emits a white light that is visible from 300 feet away.
            California law also requires bike riders to have a rear red
            reflector, a white or yellow reflector on each pedal or reflectors
            on the bicyclist's shoes or ankles. All riders would also be well
            advised to wear bright colored clothes. Wearing red or black will
            not help at night.
          </li>
          <LazyLoad>
            <img
              src="/images/bike-accidents/bike-accident-image-center.jpg"
              alt="Personal injury lawyers in Cypress"
              width="100%"
            />
          </LazyLoad>
          <li>
            <strong>Ride with Traffic:</strong> All Cypress bicycle riders must
            ride in the same direction as traffic.
          </li>
          <li>
            <strong>Stay to the Right:</strong> Bicycle riders have the same
            right-of-way privileges as other motorists, but staying as far right
            as possible will give motorists more room. Motorists expect riders
            on the right. So, there is a greater chance that you will be seen.
            Only leave the far right of the roadway to avoid parked vehicles,
            when turning left or when there are road hazards.
          </li>
          <li>
            <strong>Ride Defensively:</strong> Assuming that a car driver will
            properly yield the right of way is a dangerous mistake to make. Many
            drivers don't even look for bicyclists when making a turn, entering
            traffic or changing lanes. Making eye contact with a driver before
            entering an intersection is a good way to make your presence known.
            Another way to stay safe is to always obey traffic control devices.
          </li>
        </ul>
        <h2>Rights of Cypress Bicycle Accident Victims</h2>
        <p>
          In the event of a crash, bicyclists could suffer many different types
          of injuries such as traumatic brain injuries, lacerations, broken
          bones and devastating spinal cord injuries. Injured bicycle accident
          victims should seek and receive prompt medical attention.
        </p>
        <p>
          If the bike accident occurred as a result of someone else's
          negligence, that party can be held liable. At-fault drivers can be
          held accountable for medical bills, hospitalization and rehabilitation
          costs, lost revenue, loss of earning potential, pain and suffering and
          emotional distress. Many insurance companies, however, offer
          inadequate settlements that do not truly cover all of the losses
          suffered in the collision.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Bike Crash lawyers in Cypress"
            style={{
              backgroundImage:
                "url('/images/bike-accidents/cypress-bike-justice-statue-image.jpg')"
            }}
          >
            <h2>How to Pursue Legal Action After a Bike Accident</h2>
          </div>
        </LazyLoad>
        <p>
          The experienced Cypress bicycle accident attorneys at our law firm
          will fight against the negligent party, insurance company and other
          at-fault parties to ensure that injured victims are fairly compensated
          for their losses.
        </p>
        <p>
          If you or a loved one has been injured in a Cypress bicycle accident,
          please <Link to="/contact">contact us</Link> now for a free
          consultation.
        </p>
        <p className="textdropshadow">
          Call (949) 203-3814 for a Free Consultation.
        </p>
        <p>
          Bisnar Chase Personal Injury Attorneys 1301 Dove St. #120 Newport
          Beach, CA 92660
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.810972469205!2d-117.86859508471798!3d33.662059480713886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcde50b20b2deb%3A0xae2eee17ae4e213e!2sBisnar+Chase+Personal+Injury+Attorneys!5e0!3m2!1sen!2sus!4v1508876598629"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
