// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import GavinRebekah from "../../images/gavin-rebekah-2.jpg"
import GavinLong from "../../images/h.gavin.long-reduced.webp"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function GavinLongPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="H. Gavin Long - California Personal Injury Attorney"
        pageDescription="H. Gavin Long is a California Personal injury trial attorney specializing in car accidents and catastrophic injuries. Gavin has much success with jury verdicts."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>H. Gavin Long - California Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <h2>The Thrill of Fighting for What's Right</h2>
        <LazyLoad>
          <img
            src={GavinLong}
            alt="H. Gavin Long, California Personal Injury Trial Lawyer"
            className="imgright-fixed"
            height="281"
            width="278"
          />
        </LazyLoad>
        <p>
          For Gavin Long, the courtroom is a place of justice for those injured
          at the fault of another.
        </p>
        <p>
          Whether it's a fortune 500 company or a drunk driver that caused{" "}
          <Link to="/catastrophic-injury">catastrophic injury</Link> to his
          client, Gavin is a champion for personal injury law.
        </p>{" "}
        <p>
          <i>
            "We have good laws here in California," he says. "The challenge for
            personal injury attorneys is to make sure people and corporations
            follow those laws."
          </i>
        </p>
        <p>
          Gavin has a wide range of jury trial experience in cases from murder
          and fraud to medical malpractice in state and federal courts. For
          example, just in the last year, has taken seven personal injury cases
          to trial and has obtained verdicts above the defendant's offer in all
          seven trials, beat the plaintiff's CCP 998 in four of the trials and
          "busted" the defendant's insurance policy in three of the trials.
        </p>
        <h2>Professional Legal Accomplishments</h2>
        <p>
          {" "}
          <Link to="https://vimeo.com/192361119">
            Gavin Long: OCTLA Trial Lawyer of the Year
          </Link>
        </p>
        <p>
          Gavin was also appointed to the Consumer Attorneys of California's
          (CAOC) <strong>Board of Governors</strong> and currently sits on the{" "}
          <strong>Board of Directors</strong> for the Orange County Trial
          Lawyers Association.{" "}
        </p>
        <p>
          Gavin is a member of the American Board of Trial Advocates (
          <strong>ABOTA</strong>), an honor bestowed only on the most elite
          group of lawyers in the nation.
        </p>
        <p>
          <strong>
            {" "}
            <Link
              to="https://www.avvo.com/attorneys/90045-ca-hgavin-long-4874526.html"
              target="new"
            >
              Avvo 5 Star Rated{" "}
            </Link>
          </strong>{" "}
          attorney specializing in personal injury, dog attacks, brain injuries,
          car accidents and wrongful death cases. Mr. Long was a legal presenter
          for{" "}
          <Link to="http://www.scctla.org/belli-archives.html" target="_blank">
            Santa Clara County Trial Lawyers Association
          </Link>
          .
        </p>
        <p>
          Gavin is also awarded the <strong>2019 Lawyers of Distinction</strong>{" "}
          honor and in May 2019 secured an{" "}
          <Link
            to="https://www.prlog.org/12768353-bisnar-chase-secures-11-million-jury-verdict-for-wife-and-children-of-man-who-died-by-suicide-while-in-rehab.html"
            target="_blank"
          >
            <strong>$11 million dollar jury verdict</strong>
          </Link>{" "}
          for a negligence case resulting in a wrongful death. The opposing side
          offered less than $500K.{" "}
        </p>
        <p>
          <LazyLoad>
            <iframe
              src="https://player.vimeo.com/video/192361119"
              // width="100%"
              // height="600"
              className="iframe"
              frameBorder="0"
              allow="autoplay; fullscreen"
              allowFullScreen={true}
            />
          </LazyLoad>
        </p>
        <h3>Recognized in The Best Lawyers in America for work in:</h3>
        <ul>
          <li>Personal Injury Litigation - Plaintiffs</li>
          <li>Product Liability Litigation - Plaintiffs </li>
        </ul>
        <p>
          Gavin is a member of the{" "}
          <strong>top 1 percent of attorneys in the nation</strong> who are
          members in this oldest and most prestigious organization in the
          country. Gavin has also won the honor of
          <strong>
            Top Gun,{" "}
            <Link
              to="https://www.verdictvideos.com/news/h-gavin-long-2016-octla-trial-lawyer-year/"
              target="_blank"
            >
              Trial Lawyer of the Year 2016
            </Link>
          </strong>{" "}
          with the Orange County Trial Lawyers Association.
        </p>
        <h2>Fighting the Insurance Companies</h2>
        <p>
          During his time at Bisnar Chase, since April 2012, Gavin has handled a
          large number of auto accident cases, taking a number of those cases to
          a jury trial and winning an overwhelming majority of those cases. This
          year alone, Gavin has helped clients secure about{" "}
          <strong>$16 million in total recovery</strong>.{" "}
        </p>
        <p>
          What Gavin absolutely relishes about the process is fighting and
          winning against unscrupulous insurance companies - corporations that
          have absolutely no problems playing hardball with injured victims who
          are trying to get their lives back on track after a traumatic,
          life-altering event.
        </p>
        <p>
          <i>
            "When I expose these insurance companies and their tricks, I just
            love that," Gavin says. "I love taking the insurance companies down.
            People diligently pay their premiums to insurance companies thinking
            that they'll get compensated during a time of need. But, instead,
            they find out after an accident that they have to jump through
            hoops. And after all that, they may not get anything."
          </i>
        </p>
        <p>
          It's wrong to assume that the insurance company is your friend, Gavin
          says.
          <i>
            "In the cases I've handled, the insurance companies just didn't
            care. When you get hurt, you're not a person. As far as the
            insurance company is concerned, you're just a file. You could be
            severely injured, you could be in a wheelchair, but to them, you're
            just a file."
          </i>
        </p>
        <h2>Fighting and Winning Cases</h2>
        <p>
          But Gavin knows how to put up a fight. In a recent case, Gavin was
          able to secure a $127,347 jury verdict for an auto accident victim
          when her insurance company had refused to pay $15,000. The injured
          woman had to undergo shoulder surgery. But her insurer, Fred Loya
          Insurance, denied payment questioning the need for surgery. They even
          argued that the woman did not yield the right-of-way when in fact it
          was the other driver who was making a left turn and was required to
          yield.
        </p>
        <p>
          <i>
            "In this case, the insurance company could've paid our client
            $15,000, which was the insurance limit," he said. "But they refused
            and ended up paying more than eight times that amount. This is the
            length to which insurance companies will go just to prevent a
            payout."
          </i>
        </p>
        <p>
          But Gavin is fighting the good fight - and winning. When all was said
          and done, Fred Loya Insurance paid a total judgment of $149,461 with
          CCP 998 costs and interest included. In 2015 alone, Gavin took 8 cases
          to trial by July 2015. Three of those he exceeded policy limits and
          won the highest amount possible for his clients.
        </p>
        <h2>Early Life and Education</h2>
        <p>
          Gavin is an Orange County native. He graduated from Servite High
          School in Anaheim where he was a star football player. He graduated
          from the University of Southern California in 1996 and from Whittier
          Law School in 1999. He was a recipient of the American Jurisprudence
          Award for securing the highest grades in his study of Torts and
          Evidence. Gavin worked with nationally renowned trial attorney Herbert
          Hafif under whom he handled all phases of litigation and trial
          preparation work in cases involving catastrophic personal injury,
          complex business litigation, federal False Claim Act cases and
          insurance bad faith.
        </p>
        <div className="panel panel-primary">
          <div className="panel-heading">Some of Gavin Long's Case Results</div>
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>
                  <strong>Award Amount</strong>
                </th>
                <th>
                  <strong>Type of Case</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$11,000.000</td>
                <td>Jury verdict in facility negligence case</td>
              </tr>
              <tr>
                <td>$6,685.000</td>
                <td>Auto v. Auto +defect</td>
              </tr>
              <tr>
                <td>$3,000,000</td>
                <td>Jury verdict in auto v. pedestrian case</td>
              </tr>
              <tr>
                <td>$1,950,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$1,150,000</td>
                <td>Auto v. Pedestrian</td>
              </tr>
              <tr>
                <td>$1,000,000</td>
                <td>Premise Liability</td>
              </tr>
              <tr>
                <td>$600,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$555,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$500,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$500,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$450,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$360,000</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$243,000</td>
                <td>Jury verdict motorcycle accident</td>
              </tr>
              <tr>
                <td>$160,000</td>
                <td>Jury verdict auto v. auto case</td>
              </tr>
              <tr>
                <td>$155,000</td>
                <td>Jury verdict auto v. auto case</td>
              </tr>
              <tr>
                <td>$145,000</td>
                <td>Jury verdict auto v. auto case</td>
              </tr>
              <tr>
                <td>$135,000</td>
                <td>Auto v. Auto</td>
              </tr>
            </tbody>
          </table>
        </div>
        <br />
        <h2>A Valuable Team Member</h2>
        <LazyLoad>
          <img
            src={GavinRebekah}
            width="230"
            alt="Gavin Long and wife Rebekah"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          {" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link>, senior partner
          and experienced trial lawyer at Bisnar Chase says Gavin is an
          extremely valuable member of his team.{" "}
          <i>
            "Gavin is tremendously motivated and passionate about what he does.
            His results demonstrate the superior caliber of his work, skills and
            abilities. We are fortunate to have him on the Bisnar Chase team."
          </i>
        </p>
        <h2>Personal Side of Mr. Long</h2>
        <p>
          Gavin has been happily married for 20 years. Gavin met his wife,
          Rebekah, when she was 19 years old and she has stood firmly by his
          side through this incredible life journey. Rebekah continues to be
          Gavin's inspiration and his best friend - he could not ask for a
          better life partner.
        </p>
        <h2>Contact H. Gavin Long</h2>
        <p>
          To schedule a free consultation with Mr. Long, please call
          949-203-3814.
        </p>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/204034"
            target="_blank"
            name="State Bar Profile"
          >
            Gavin's State Bar Profile
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  iframe {
    /* max-width: 1024px; */
    /* width: 100%; */
    /* min-width: 100%; */
    width: 100%;
    height: 600px;

    /* max-height: 600px; */

    @media (max-width: 1675px) {
      height: 500px;
    }

    @media (max-width: 1530px) {
      height: 400px;
    }

    @media (max-width: 1350px) {
      height: 300px;
    }

    @media (max-width: 1024px) {
      height: 500px;
    }

    @media (max-width: 880px) {
      height: 450px;
    }
    @media (max-width: 724px) {
      height: 350px;
    }

    @media (max-width: 615px) {
      height: 300px;
    }
    @media (max-width: 450px) {
      height: 250px;
    }

    @media (max-width: 400px) {
      height: 200px;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
