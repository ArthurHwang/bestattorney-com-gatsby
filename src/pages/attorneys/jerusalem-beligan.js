// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import JerusalemBeligan from "../../images/j-beligan.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function JerusalemBeliganPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Jerusalem F. Belgian - California Employment Law Attorney for Employees"
        pageDescription="Attorney Jerusalem Beligan represents employees in California for wrongful termination, sexual harassment and other California labor law issues."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Jerusalem Belgian California Employment Lawyer</h1>
        <BreadCrumbs location={location} />
        <h2>Pursuit of Justice for All - The "Calling"</h2>

        <p>
          <LazyLoad>
            <img
              alt="California employment law attorney"
              width="230"
              className="imgright-fixed"
              title="Jerusalem Beligan, J.D."
              src={JerusalemBeligan}
            />
          </LazyLoad>{" "}
          To Jerusalem Beligan, justice is much more than a word. To Jerusalem,
          "Justice" is a calling, a mission and his hope for the future.
        </p>

        <p>
          Jerusalem is a Southern California native that had a lifelong dream of
          becoming an attorney. Jerusalem's father, one of his heroes, was also
          an attorney.
        </p>
        <p>
          Jerusalem grew up watching his father serving the people of the
          Philippines. He also grew up watching the admiration that people had
          for his father and his father's accomplishments for them.
        </p>
        <p>
          Following in his father's footsteps, Jerusalem became an attorney for
          the people, like it was a family calling, something the Beligans are
          called to do. Jerusalem is grateful to his father for the role model
          he set for Jerusalem and for mentoring him through the process of
          becoming a people's attorney. Following the family tradition,
          Jerusalem is passing on what he learned from his father to his
          children.
        </p>
        <h2>Civil Prosecutor - The "Mission"</h2>
        <p>
          <em>
            Jerusalem considers himself a "civil prosecutor", an attorney who's
            chosen mission is representing everyday people, protecting their
            rights and pursuing their interests. In other words, being an
            everyday hero to everyday people. Jerusalem is passionate about
            standing up for people who have been wronged. He enjoys and excels
            at facing off against corporate giants, insurance companies and
            abusive employers who have wronged clients, just the way he faced
            off against much bigger and seemingly stronger opponents when
            excelling in high school football.
          </em>
        </p>
        <h2>Some of Mr. Beligan's Professional Successes</h2>

        <div className="panel panel-primary">
          <div className="panel-heading">Jerusalem Beligan's Case Results</div>
          <table>
            <thead>
              <tr>
                <th>
                  <strong>Award Amount</strong>
                </th>
                <th>
                  <strong>Type of Case</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="126">$2,000,000.00</td>
                <td width="398">Wage and Hour</td>
              </tr>
              <tr>
                <td>$1,478,819.00</td>
                <td>Wage and Hour</td>
              </tr>
              <tr>
                <td>$600,000.00</td>
                <td>Wage and Hour</td>
              </tr>
              <tr>
                <td>$575,000.00</td>
                <td>Wage and Hour</td>
              </tr>
              <tr>
                <td>$600,000.00</td>
                <td>Wage and Hour</td>
              </tr>
              <tr>
                <td>$250,000.00</td>
                <td>Wage and Hour</td>
              </tr>
            </tbody>
          </table>
        </div>

        <h2>California Employment &amp; Class Action Lawyer</h2>
        <p>
          Mr. Beligan is a personal injury attorney specializing in class action
          and employment law. He has a deep understanding and numerous victories
          related to wage and hour cases, discrimination, unfair treatment along
          with multiple class action cases. He has spent his whole career
          focusing in on these specialty cases and understands the legal
          intricacies of the courts relating to these types of claims.
        </p>
        <p>
          {" "}
          <Link to="/">Bisnar Chase Personal Injury Attorneys</Link> can
          represent you for the following cases
        </p>
        <ul>
          <li>Discrimination</li>
          <li>Disability discrimination</li>
          <li>Wrongful termination</li>
          <li>Hostile work environment</li>
          <li>Unpaid overtime</li>
          <li>Wage and hour violations</li>
          <li>Class action</li>
        </ul>
        <h2>Education</h2>
        <p>
          Raised in Diamond Bar, CA, Jerusalem was an outstanding student and
          athlete at Roland High School. He played on the varsity football and
          track teams, which is where he got his nick name, "The Rocket" due to
          his quickness and ability to avoid tacklers. He graduated magna cum
          laude from the University of California, Riverside in 1997. His
          studious work earned memberships in Phi Beta Kappa, the Golden Key
          National Honor Society, and the only honor society for college and
          university students of government in the United States, Pi Sigma
          Alpha. He went on to obtain his Juris Doctor from Southwestern
          University School of Law in 2000. That same year he passed the
          California State Bar, and at the young age of twenty five he began
          practicing law.
        </p>
        <h2>Devoted Husband and Father</h2>
        <p>
          Jerusalem resides in Coto De Caza, California with his wife, Jackie,
          and three children. Jackie is a 8th grade English and English Language
          Development teacher at Bernice Ayer Middle School ("BAMS") in San
          Clemente, CA. Jackie and Jerusalem's oldest daughter, Jaylene, is a
          graduate of UC Berkley. Their son, Jayden, is a sophomore in High
          School. Jerusalem named his youngest daughter "Justice" because of his
          devotion to the law and hopes that one day she may follow in her
          father's and grandfather's footsteps. Jerusalem envisions "Justice"
          practicing law in the family tradition or better yet, presiding in
          court as a "Judge Justice." When he takes time for himself he enjoys
          being active, playing basketball (the "Rocket" is still quick) and
          spending time with his family.
        </p>

        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/211258"
            target="_blank"
            name="State Bar Profile"
          >
            Jerusalem's State Bar Profile
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
