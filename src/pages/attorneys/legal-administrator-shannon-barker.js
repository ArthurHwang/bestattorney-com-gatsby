// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"

import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import folderOptions from "./folder-options"

import ShannonBarker from "../../images/shannon-barker.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ShannonBarkerPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Shannon Barker - Legal Administrator of BISNAR CHASE"
        pageDescription="Shannon Barker runs the day to day operations at BISNAR CHASE and has been with the firm over 20 years managing the staff, case negotiations and company financial decisions."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Shannon Barker - Legal Administrator</h1>
        <BreadCrumbs location={location} />

        <p>
          <LazyLoad>
            <img
              src={ShannonBarker}
              alt="Shannon Barker - Legal Administrator of Bisnar Chase"
              className="imgright-fixed"
            />
          </LazyLoad>
          Shannon Barker is responsible for the full day-to-day operations of
          BISNAR CHASE. Shannon's typical day includes a myriad of duties
          including financial decisions, employee screening, tough legal
          negotiations, and managing the entire staff.
        </p>

        <p>
          Shannon Barker doesn't just scan through resumes when she is about to
          hire someone. She looks at the person inside out, to make sure that he
          or she would really fit into BISNAR CHASE culture - a culture she has
          worked hard to build in the past twenty years.
        </p>
        <p>
          "I look for people who not only have the necessary skill sets,"
          Shannon says. "They have to want to be here."
        </p>
        <h2>Building a Culture of Civility</h2>
        <p>
          Shannon came to the firm in July 1995 as a paralegal and negotiator
          after working four years at another law firm. She quickly became a
          team leader and built a solid reputation for handling a multitude of
          issues related to the firm as a whole. Shannon's skills in law firm
          management created a culture other lawyers look to model after.
        </p>

        <p>
          Shannon is a seasoned and natural team leader with an eye for client
          satisfaction. Her ideas and methods create a cohesive team of
          employees who truly enjoy working together. That natural skill also
          shows in how the clients of BISNAR CHASE are treated.
        </p>
        <p>
          "It was really important to me to have a balanced and happy team
          because how we treat each other translates to how we treat clients. We
          wanted it to be the kind of personal attention you get at The Ritz
          Hotel," she said. "Clients notice everything - how you dress, how you
          talk, our d&eacute;cor. Our staff is happy and appreciated and it
          shows in everything they do, and that directly impacts our clients."
        </p>
        <h2>An Inspiring Leader</h2>
        <LazyLoad>
          <img
            src="/images/2019-assets/ala.jpg"
            alt="Shannon Barker - Legal Administrator of Bisnar Chase"
            className="imgleft-fixed"
            width="180px"
          />
        </LazyLoad>
        <p>
          Shannon has always been organized and goal oriented. Mrs. Barker is an
          ABA certified paralegal and holds her MBA at Chapman University. In
          addition to setting goals for herself, Shannon also inspires her
          staff. She strives for quality in her employees and provides
          continuing education in a variety of programs and cross training.
        </p>
        <p>
          Shannon says working for John Bisnar taught her to always push
          herself, and continuing her education opened up her thought process
          which impacted her skills leading a successful team.
        </p>
        <p>
          Shannon is very clear and very direct in her management style, which
          is the hallmark of a good administrator. Shannon leads by example, and
          her habit of being an active listener makes staff members feel
          appreciated and heard. Her door is always open to everyone.
        </p>
        <h2>Maintaining a Delicate Balance</h2>
        <p>
          Attorney H. Gavin Long says Shannon knows when to push and when to
          back off, whether it's working with a mediator or running the office.
        </p>
        <p>
          "She is very good at making everyone believe in themselves," he says.
          "If an employee has an idea or approach that they would like to try
          out, she is very supportive and is always positive. She is constantly
          encouraging people to get the most out of their jobs."
        </p>
        <p>John Bisnar, views her as a woman of character.</p>
        <p>
          "Shannon is a woman of integrity - she is organized, strict, and
          straight forward, but also caring and personable. She knows how to
          bring out the best in everyone she manages. She also has the ability
          to make tough decisions under fire."
        </p>
        <p>
          "I know that the employees respect her a great deal and look to her
          not only for professional guidance, but personal guidance as well,"
          John says.
        </p>
        <p>
          Shannon believes that the firm has succeeded not just because of her,
          but because of the fantastic team that is in place.
        </p>
        <p>
          "We all have each other's back. We still see ourselves as a small
          mom-and-pop enterprise. I've been here twenty years. Today, when I see
          how far we've all come and what we've accomplished, I'm really proud.
          We did it as a group."
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
