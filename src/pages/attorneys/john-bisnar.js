// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import JohnBisnar2013 from "../../images/john-bisnar-top-attorney-2013a.jpg"
import JohnKimSkiing from "../../images/john-kim-bisnar-skiing-lawyer.jpg"
import JohnBisnar from "../../images/johnbisnar.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function JohnBisnarPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Personal Injury Lawyer John Bisnar"
        pageDescription="Award winning personal injury attorney John Bisnar of Bisnar Chase in California."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>John Bisnar - California Personal Injury Lawyer (Retired)</h1>
        <BreadCrumbs location={location} />
        <h2>Do Right by the Clients</h2>
        <LazyLoad>
          <img
            src={JohnBisnar}
            alt="attorney John Bisnar"
            width="190"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          John Bisnar is the founder of one of the top California personal
          injury law firms,{" "}
          <Link to="/about-us">Bisnar Chase Personal Injury Attorneys</Link>{" "}
          whose roots can be traced back to Bisnar & Associates, which he
          founded in 1978.
        </p>

        <p>
          Mr. Bisnar founded his personal injury law firm on the premise that
          you, "Do right by the clients and the profits will follow," after
          having a horrible experience as a personal injury client following his
          own serious car accident while in law school.
        </p>

        <p>
          The experience taught him how it felt to be in need of compassionate
          guidance and spirited representation and getting neither from his own
          personal injury lawyer.
        </p>
        <h2>Superior Client Representation</h2>
        <LazyLoad>
          <img
            src={JohnBisnar2013}
            alt="John Bisnar Awarded the Top Attorneys in Orange County Award"
            width="150"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Due to his horrible client experience Mr. Bisnar vowed that his
          clients would have the experience he wished he had when he was a
          client. This vow resulted in the first phrase of the Bisnar Chase
          Personal Injury Attorneys mission statement, "To provide superior
          client representation in a compassionate and professional manner,
          while experiencing high job satisfaction and making our world a safer
          place."
        </p>

        <p>
          Following the Bisnar Premise and the Bisnar Chase Personal Injury
          Attorneys mission statement, more than ten thousand personal injury
          clients have recovered hundreds of millions of dollars while being
          represented by Mr. Bisnar and his law firm. He takes personal
          responsibility for every client and every client's case.
        </p>
        <h2>Recipient - Top Attorney Orange County Award</h2>
        <p>
          John Bisnar has also been awarded the Top Attorneys in Orange County
          Award, as published in Orange Coast Magazine. John and Brian have both
          won the prestigious Top Attorneys in Orange County award for six
          consecutive years.
        </p>
        <p />
        <h2>Southern California Native, Military Veteran & Honor Graduate</h2>
        <p>
          Mr. Bisnar is a native of Southern California. He served active duty
          in the U.S. Army in the Pacific and South East Asia from 1968 to 1970.
          In 1974 he graduated with honors from California State University,
          Long Beach, with a B.S. Degree in Finance/Investments. John was on the
          Dean's List every semester. In 1978 John received his J.D. degree from
          Pepperdine University School of Law and was admitted to the California
          State Bar two months later, not long after he graduated from
          Pepperdine University College of Trial Advocacy.
        </p>
        <h2>Perfect "10" Attorney Rating</h2>
        <p>
          Avvo.com, the best known online attorney rating service, rated Mr.
          Bisnar a "perfect 10" and "Superb" based on his background, years of
          experience as a personal injury lawyer, successful track record,
          impeccable reputation with clients and peers, and professional
          accomplishments. In addition to being named a Super Lawyer, he was
          also named an Outstanding Achievement Award by WeTip, a leading
          anonymous crime reporting service and law enforcement advocacy.
        </p>
        <h2>Teaches Client Care to Lawyers</h2>
        <p>
          He is a frequent lecturer at lawyer continuing education programs,
          teaching lawyers law office management and client care techniques. The
          State Bar of California asked Mr. Bisnar to write portions of its book
          for lawyers, "The California Guide to Opening and Managing a Law
          Office." He is the author of, "The Seven Fatal Mistakes That Can Wreck
          Your California Personal Injury Claim," a book to assist non-attorneys
          in handling their own personal injury claims.
        </p>
        <p>
          Mr. Bisnar is a former member of the Board of Directors and Board of
          Trustees of various businesses, nonprofit and religious organizations.
          He has been an active member of two men's support groups since 1996:
          one is devoted to personal growth and the other to growth of your
          business.
        </p>
        <h2>His Passions</h2>

        <p>
          <LazyLoad>
            <img
              src={JohnKimSkiing}
              alt="John Bisnar"
              width="245"
              className="imgright-fixed"
            />
          </LazyLoad>
          Besides winning challenging cases for the seriously injured, Mr.
          Bisnar is a devoted husband and a father to three adult children and
          one grandchild, so far. John and wife Kimberly are avid world
          travelers and will soon have traveled to all seven continents.
        </p>

        <p>
          Yoga, meditation, scuba diving, snow skiing, organic gardening, the
          Los Angeles Lakers and most of all Kimberly, are his passions.
        </p>
        <p>
          John Bisnar wrote the{" "}
          <Link to="/resources/book-order-form">
            book(s) on personal injury law
          </Link>
          . John has spent over three decades representing plaintiffs injured by
          the fault of others.
        </p>
        <div itemScope={true} itemType="http://schema.org/Person">
          <span itemProp="name">John Bisnar</span>{" "}
          <span itemProp="jobTitle">Personal Injury Lawyer</span>
          <div
            itemProp="address"
            itemScope={true}
            itemType="http://schema.org/PostalAddress"
          >
            <span itemProp="streetAddress"> 1301 Dove St. #120 </span>{" "}
            <span itemProp="addressLocality">Newport Beach</span>,
            <span itemProp="addressRegion">CA</span>
            <span itemProp="postalCode">92660</span>
          </div>
          <span itemProp="telephone">(949) 203-3814</span>
        </div>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/80894"
            target="_blank"
            name="State Bar Profile"
          >
            John's State Bar Profile
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
