// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"

import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import ScottRitsema from "../../images/scott-ritsema.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function ScottRitsemaPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Scott Ritsema - California Personal Injury Lawyer & Partner"
        pageDescription="Attorney Scott Ritsema is a top rated personal injury lawyer in California and a partner at Bisnar Chase"
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Scott Ritsema - Trial Lawyer, Partner</h1>
        <BreadCrumbs location={location} />
        <LazyLoad>
          <img
            src={ScottRitsema}
            alt="Scott Ritsema - California personal injury attorney"
            width="275px"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Scott Ritsema became a{" "}
          <Link to="/"> California personal injury lawyer</Link> because he
          wanted to represent people, not corporations.
        </p>
        <p>
          Scott is a Partner of Bisnar Chase and has been with the firm since
          2008.
        </p>{" "}
        <p>
          He gleans maximum satisfaction from the fact that he is fighting every
          single day to protect the rights of catastrophically injured
          individuals and is helping them obtain the only form of justice that
          is available to them.{" "}
        </p>
        <p>
          For the first decade of his career, Ritsema worked as a defense lawyer
          representing the interests of large corporations and insurance
          companies.&nbsp; Over time, he found that while winning cases for
          those types of clients gave him some professional satisfaction, he got
          very little personal satisfaction.&nbsp;
        </p>
        <p>
          Although he did his job well, he was not passionate about his
          victories.&nbsp; Thus, in 2001, he decided it was time to switch sides
          and to start representing injured individuals.&nbsp; His experience
          doing over 10 years of defense work gave him a unique understanding of
          how to win cases against corporations and insurance companies. Scott
          is ranked as a National Trial Lawyer in the Top 100.
        </p>
        <h2>Personal Injury Law - A Calling</h2>
        <Link to="http://thenationaltriallawyers.org" target="_blank">
          <LazyLoad>
            <img
              src="https://www.thenationaltriallawyers.org/images/NTL-top-100-member-seal.png"
              alt="The National Trial Lawyers"
              width="150"
              title="Top 100 lawyers"
              className="imgleft-fixed"
            />
          </LazyLoad>
        </Link>
        <p>
          For Ritsema, personal injury law practice is not just a job. It's a
          calling. He realized the tough situations many clients are placed in
          when they come to a personal injury lawyer.
        </p>
        <p>
          "These are people who, without us and without the civil justice
          system, have no recourse, no remedies," he says. "It's no exaggeration
          when I say they put their lives and their financial future in our
          hands."
        </p>
        <p>
          Some may view that as a lot of pressure, but for Ritsema it's a
          welcome challenge. Recently, Ritsema led a Bisnar Chase legal team
          that obtained a $30 million judgment for 25-year-old Lucas Vogt, who
          suffered a catastrophic brain injury in a motorcycle accident.
        </p>
        <p>
          For Ritsema, the satisfaction in these cases comes from providing a
          family the means to deal with a catastrophic injury. Ritsema's hope in
          this case was that Lucas Vogt would be able to have the best recovery,
          the best quality of life under the circumstances and that he would be
          able to maximize his time with his young daughter.
        </p>
        <p>
          He also sees how many of the law firm's catastrophically injured
          clients face significant physical emotional and financial challenges.
          It's not just the victims, but also their families that suffer
          life-changing consequences after a traumatic event such as a
          catastrophic crash.
        </p>
        <h2>Early Life and Education</h2>
        <p>
          Ritsema was born in Michigan, but grew up primarily in Newport Beach.
          He moved back to Michigan in his teens and graduated from Grandville
          High School in 1979 near the top of his class and as a two-year member
          of the National Honor Society. Ritsema got his bachelor's degree in
          psychology from UCLA in 1984 and his law degree from UC Davis in 1988
          graduating in the top 10% of his class and receiving the prestigious
          Order of the Coif Award. He was admitted to the State Bar of
          California that same year.
        </p>
        <h2>Professional Accomplishments</h2>
        <p>
          Ritsema has brought over 20 trials to verdict and conducted more than
          50 arbitrations to conclusion. He has handled more than 25 cases with
          over $100,000,000 in total recoveries and judgments for our clients..
          Here are some of Ritsema's recent case results. This can include
          settlements, judgments and verdicts.
        </p>
        <div className="panel panel-primary">
          <div className="panel-heading">Scott Ritsema's Case Results</div>
          <table>
            <thead>
              <tr>
                <th>
                  <strong>Award Amount</strong>
                </th>
                <th>
                  <strong>Type of Case</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="126">$38,650,000.00</td>
                <td width="398">Motorcycle v. car</td>
              </tr>
              <tr>
                <td>$24,700,000.00</td>
                <td>Jury verdict - auto accident - defective seatback</td>
              </tr>
              <tr>
                <td>$7,000,000.00</td>
                <td>Caustic ingestion case</td>
              </tr>
              <tr>
                <td>2,000,000.00</td>
                <td>Drunk driving accident</td>
              </tr>
              <tr>
                <td>$1,000,000.00</td>
                <td>Auto v. pedestrian</td>
              </tr>
              <tr>
                <td>$1,500,000.00</td>
                <td>Dangerous roadway - negligence</td>
              </tr>
              <tr>
                <td>$10,030.000.00</td>
                <td>Negligence (company confidential)</td>
              </tr>
              <tr>
                <td>$6,850.000.00</td>
                <td>Product Liability</td>
              </tr>
            </tbody>
          </table>
        </div>
        <h2>Career and Accomplishments</h2>
        <p>
          Ritsema belongs to a number of professional organizations including
          the{" "}
          <strong>
            Orange County Trial Lawyers Association, American Association for
            Justice and Consumer Attorneys of California.{" "}
          </strong>
          Mr. Ritsema was named a{" "}
          <strong>Best Lawyer in 2015, 2017, 2018 </strong> by the national
          publication, BestLawyer. Mr. Ritsema has been named a
          <strong> SuperLawyer every year</strong> since 2011, and named a{" "}
          <strong>Best Attorney each year since 2015</strong> by publications
          which rank skill levels within the legal profession.&nbsp; He is also
          a <strong>2019 Top 100 injury lawyer</strong> in the country.
        </p>
        <p>
          He was recently named Lawyer of the Year for the State of California
          by a prestigious legal publication. In 2012, Scott Ritsema and Brian
          Chase were named{" "}
          <Link to="/press-releases/chase-ritsema-award-2012">
            Consumer Attorneys of the Yea
          </Link>
          r in all of California by the Consumer Attorney of California (CAOC),
          a professional association for attorneys who represent plaintiffs who
          seek to hold wrongdoers accountable.
        </p>
        <p>
          Despite his extremely busy schedule, Ritsema is committed to giving
          back to his community. He actively supports Mothers Against Drunk
          Driving and participates in the Susan G. Komen Walk for the Cure to
          help raise awareness and generate funds for breast cancer research.
        </p>
        <h2>An Important Member of the Bisnar Chase Family</h2>
        <p>
          Ritsema says he values the opportunity Bisnar Chase has given him to
          join the "major leagues" of personal injury litigation. "Bisnar Chase
          has the best environment of any place I've worked," he says. "This
          firm is truly unique in that respect."
        </p>
        <p>
          Ritsema feels as if he has truly found his calling as a personal
          injury lawyer at Bisnar Chase. "I'm in the company of like-minded
          people here who have the passion to fight and win for those who have
          suffered tremendous losses in their lives. I consider it an honor and
          privilege to be able to give injured clients a voice and to fight for
          their rights."
        </p>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/138193"
            target="_blank"
            name="State Bar Profile"
          >
            Scott's State Bar Profile
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
