// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"

import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import DistinguishedBadge from "../../images/logo/distinguished-justice-advocates.png"
import StevenHilst from "../../images/steven-hilst.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function StevenHilstPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Steven Hilst - Personal Injury Lawyer - Bisnar Chase"
        pageDescription="Steven Hilst is a catastrophic Injury Lawyer in California and an associates of Bisnar Chase Personal Injury Attorneys."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Personal Injury Lawyer Steven Hilst</h1>
        <BreadCrumbs location={location} />

        <h2>Steven Hilst: Working to Make a Difference</h2>

        <p>
          <LazyLoad>
            <img
              alt="Attorney Steven Hilst"
              width="235px"
              className="imgright-fixed"
              title="Personal Injury Attorney, California"
              src={StevenHilst}
            />
          </LazyLoad>
          Steven Hilst became a personal injury lawyer because he saw how his
          work could transform the lives of severely injured accident victims or
          families that have lost loved ones under tragic circumstances.
        </p>

        <p>
          When Hilst was in law school, he worked for a firm that represented
          the interests of businesses and corporations. He got little
          satisfaction from that work. But when he took on personal injury
          cases, he relished the one-on-one contact he had with clients and the
          precious opportunity he had to help them pick up the pieces and get
          their lives back together.
        </p>
        <h3>Making a Difference in People's Lives</h3>
        <p>
          Hilst particularly remembers a wrongful death case early on in his
          career where he helped a young woman – an immigrant from India – who
          was in the U.S. with her husband, an engineer. She was a still going
          to school and was on a student visa when her husband was tragically
          killed in a parking structure accident, which occurred as the result
          of a construction defect.
        </p>
        <p>
          "We were able to get a substantial settlement for the woman whereby
          she had the financial resources to remain in the country and complete
          her studies," Hilst said. "In personal injury law, you really see
          first-hand how you've helped someone and how that has helped turn
          their life around. You meet these people who are in horrible
          situations in their life and help them get compensation and monetary
          support. What could be more satisfying than that?"
        </p>
        <p>
          Now, Hilst, who once represented corporations like insurance
          companies, works for the other side. This is how he looks at it.
        </p>
        <p>
          "If I represent the insurance companies, I'm depriving someone of
          compensation and I'm essentially denying them justice," he said. "But
          when I represent injured plaintiffs, I'm helping them seek and obtain
          justice and fair compensation."
        </p>
        <h2>Fighting the Good Fight</h2>
        <p>Hilst treats every case like it's a chess game.</p>
        <p>
          "I'm competitive, I like to win," he says. "I look at the big picture
          when I come up with the strategy. I take every case like it's going to
          trial. I remember at every moment that I'm standing up for someone who
          would otherwise not have this chance to get justice."
        </p>
        <p>It's not just about money or victories for Hilst.</p>
        <p>
          "It's about treating your clients like they are your friends or
          family," he says. "This is personal for me."
        </p>
        <p>
          Hilst walks that talk when he represents accident victims against
          insurance companies. He focuses on telling insurance companies his
          clients' stories, by humanizing them instead of making them seem like
          numbers on a page.
        </p>
        <p>
          "I try to make the insurance company see how this person is a human
          being and that his or her life matters," Hilst said. "Hopefully, it
          helps insurance companies understand the person's story."
        </p>
        <p>
          But, when it doesn't, Hilst has never hesitated to go to all out, take
          the case to court and get his clients what they rightfully deserve.
        </p>
        <h2>A Team-Oriented Environment</h2>
        <p>
          Hilst, the newest addition to Bisnar Chase's team of trial lawyers,
          believes that he is in the right place.
        </p>
        <p>
          "Bisnar Chase is well known as a leader when it comes to{" "}
          <Link to="/catastrophic-injury">catastrophic injury</Link> and{" "}
          <Link to="/defective-products">product liability cases</Link>
          ," he said. "The team here goes above and beyond in representing
          clients. This is a firm that is not afraid to stand up against
          multi-billion dollar corporations."
        </p>
        <p>
          Hilst says he also values the team-oriented atmosphere at Bisnar
          Chase.
        </p>
        <p>
          "We work together for the common good, which is to get just
          compensation for clients," he said. "But fees alone is not the
          motivation for the good work we do. The primary goal is to do the best
          job we can for clients and treat them really well. I'm honored to be a
          part of this team."
        </p>
        <h2>Early Life and Education</h2>
        <p>
          Hilst was born in a small town in Central Illinois, in the area where
          President Abraham Lincoln practiced as a lawyer. He moved to Southern
          California when he was 3 and has remained here. Hilst obtained a
          Bachelor's degree from the University of Southern California in 1984
          and a Doctorate law degree from Santa Clara University School of Law
          in 1988. He also holds a post-doctorate law degree from the University
          of San Diego School of Law. He lives in Redondo Beach, loves to work
          out and go to the beach. Hilst is also a die-hard USC football fan.
        </p>
        <h2>Professional Legal Accomplishments</h2>
        <LazyLoad>
          <img
            src={DistinguishedBadge}
            alt="Steven Hilst Top 1% Attorneys in America"
            width="175	"
            // height="175"
            title="Distinguished Justice Advocate"
            className="imgleft-fixed"
          />
        </LazyLoad>

        <p>
          Since 2001, Hilst has been a member of the prestigious{" "}
          <Link to="https://www.abota.org/" target="_blank">
            American Board of Trial Advocates
          </Link>{" "}
          (ABOTA), an invitation-only bar association consisting of the most
          accomplished trial attorneys in the United States. He is also a life
          member of The Million Dollar Advocates Forum, an honor bestowed on
          only 5 percent of attorneys in the U.S.
        </p>
        <p>
          Hilst has obtained the rank of associate with the American Inns of
          Court, has served on the Board of Directors of The William J.
          Rea/ABOTA  Inn of Court, and was the first President of The Carlos
          Moreno Trial Practice In of Court. He has also served on the faculty
          of the LA-ABOTA trial school. Hilst also serves on the Board of The
          Beach Cities Trojan Club, which raises money for student athletes.{" "}
        </p>
        <p>
          Mr. Hilst is an honorary of the Distinguished Justice Advocates{" "}
          <Link
            to="https://distinguishedjusticeadvocates.com/?post_type=job_listing&p=12982"
            target="_blank"
          >
            Top 1% Attorneys in America
          </Link>
          .
        </p>
        <p>
          His motto?  "Work hard, always be prepared, speak from your heart,
          never give up and always fight on."
        </p>

        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/133465"
            target="_blank"
            name="State Bar Profile"
          >
            Steve's State Bar Profile
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
