// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

import JordanSouthwick from "../../images/Jordan-Southwick-headshot-rsz.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function JordanSouthwickPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Jordan Southwick - California Personal Injury Attorney - Bisnar Chase"
        pageDescription="Jordan Southwick is a California personal Injury Lawyer and an associate attorney of Bisnar Chase. He represents injured plaintiffs in California and joins a team with a 96% success rate and more than $500M recovered."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Jordan Southwick - California Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <h2>Education and Accomplishments</h2>
        <LazyLoad>
          <img
            src={JordanSouthwick}
            alt="Personal injury lawyer Jordan Southwick"
            width="238"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          For Jordan Southwick, the legal profession is a calling. An associate{" "}
          attorney at Bisnar Chase, Jordan focuses on{" "}
          <Link to="/" target="new">
            personal injury
          </Link>{" "}
          cases – particularly those relating to premises liability, wrongful
          deaths, and auto accidents. As the last line of defense for those who
          have suffered injuries and losses, he is passionate about achieving
          the right results for his clients.
        </p>
        <p>
          Jordan is a Southern California native, growing up and attending high
          school in Murrieta. As an aspiring lawyer, he earned a scholarship
          from the Southwest Riverside County Bar Association and attended
          Chapman School of Law in Orange. Jordan graduated from Chapman with a
          law degree in 2018, before passing the California State Bar exam.
        </p>
        <p>
          While at Chapman, Jordan secured a prestigious CALI Award for
          excellence in Foundation Transactions as the top achiever in his
          class. He also expanded his legal expertise by working on the school’s
          Advanced Entertainment Law Clinic. This gave him the chance to provide
          legal help to producers of low-budget movies, working to make sure
          independent films received the agreements and approvals needed for
          them to be green-lit for production.
        </p>
        <h2>Sports Agent to Personal Injury Lawyer</h2>
        <p>
          Before joining{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>
          , Jordan worked at a sports agency where he helped to negotiate and
          draft contracts for high-profile sports stars, including NFL and MLB
          players. But he was passionate about moving into the personal injury
          field, where felt he could make a greater and more positive impact in
          the lives of those who are desperate for help.
        </p>
        <p>
          “I’m focused on helping those people who need it most,” he said. “Some
          of our clients have been seriously hurt through the negligence,
          carelessness, or the actions of others, but might not know where to
          turn, or have the means to fight it.
        </p>
        <p>
          “Those are the people I want to focus on. It is important to me that
          they get the representation they need and the justice that they
          deserve.”
        </p>
        <p>
          After passing the California Bar, Jordan joined Bisnar Chase as a law
          clerk, and was quickly promoted to become an associate attorney and
          trial lawyer.
        </p>
        <p>
          He added: “It feels great to get started at Bisnar Chase. My dad is an
          attorney too, and I always knew that I wanted to follow this path. I
          know how dramatically people’s lives can be affected by an accident or
          injury, and I want to fight for those people.”
        </p>
        <h2>Personal Interests</h2>
        <p>
          In between securing justice for his clients, Jordan is a big fitness
          fan and former hockey player. He started ice skating when he was just
          three years old, and went on to play Junior A hockey across
          California. Jordan also enjoys staying in shape by playing basketball
          and hitting the gym.
        </p>
        <h2>Contacting Jordan Southwick</h2>
        <p>
          Jordan plays a key role in Bisnar Chase’s expanding line-up of
          accomplished personal injury attorneys. He offers high-quality ‘No
          Win, No Fee’ legal services, ensuring that every client gets the
          representation they deserve. Contact Jordan for a free case
          consultation by calling (800) 561-4887 now.
        </p>
        <br />{" "}
        <Link
          to="http://members.calbar.ca.gov/fal/Licensee/Detail/322473"
          target="new"
        >
          {" "}
          View Jordan’s profile on the State Bar of California website
        </Link>
        <br />
        <br />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
