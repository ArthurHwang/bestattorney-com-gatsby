// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import BrianChase from "../../images/brian-small-size.jpg"
import GavinLong from "../../images/gavin-long-small.jpg"
import IanSilvers from "../../images/ian-medium-crop-sm.jpg"
import JerusalemBeligan from "../../images/j-beligan.jpg"
import JohnBisnar from "../../images/john-bisnar.png"
import JordanSouthwick from "../../images/Jordan-Southwick-headshot-rsz.jpg"
import KristBiakanja from "../../images/Krist-Biakanja-Headshot-rsz.jpg"
import ScottRitsema from "../../images/scott-ritsema.jpg"
import ShannonBarker from "../../images/shannon-barker.jpg"
import StevenHilst from "../../images/steven-hilst.jpg"
import TomAntunovich from "../../images/tom-antunovich-small.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function Attorneys({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Our Attorneys - California Personal Injury Lawyers of Bisnar Chase"
        pageDescription="View all of the Attorneys at Bisnar Chase Personal Injury Attorneys"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Our Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="well nomobile">
          <table className="table table-hover table-striped">
            <tbody>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/john-bisnar">
                    <LazyLoad>
                      <img
                        src={JohnBisnar}
                        alt="John Bisnar, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/john-bisnar">
                    <strong>
                      John
                      <br />
                      Bisnar
                    </strong>
                  </Link>
                </td>
                <td>
                  <p>
                    Founding Partner (Retired)
                    <br />
                    Super Lawyer 2008, 2009, 2010 and 2011
                    <br />
                    Community Hero 1996 by The United Way
                    <br />
                    Founded Bisnar and Associates in 1978
                    <br />
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/brian-chase">
                    <LazyLoad>
                      <img
                        src={BrianChase}
                        alt="Brian Chase, persaonal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/brian-chase">
                    <strong>
                      Brian
                      <br />
                      Chase
                    </strong>
                  </Link>
                </td>
                <td>
                  <p>
                    Senior Partner &amp; Trial Lawyer
                    <br />
                    2017 Top 1% by the Natl. Assoc. of Distinguished Counsel
                    <br />
                    2017 Top 100 High Stakes Litigator <br />
                    2015{" "}
                    <Link to="https://www.caoc.org/" target="_blank">
                      CAOC
                    </Link>{" "}
                    President
                    <br />
                    2015 NADC Top One Percent of Lawyers
                    <br />
                    California Supreme Court and Appellate Court case winner
                    <br />
                    Member of{" "}
                    <Link to="https://www.abota.org/" target="_blank">
                      ABOTA
                    </Link>
                    <br />
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/scott-ritsema">
                    <LazyLoad>
                      <img
                        src={ScottRitsema}
                        alt="Scott Ritsema, personal injury lawyer"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/scott-ritsema">
                    <strong>
                      Scott
                      <br />
                      Ritsema
                    </strong>
                  </Link>
                </td>
                <td>
                  <p>
                    Partner
                    <br />
                    Auto & Products Liability
                    <br />
                    Best Lawyer in 2015 by the national publication, BestLawyer
                    <br />
                    2012{" "}
                    <Link to="https://www.caoc.org/" target="_blank">
                      CAOC
                    </Link>{" "}
                    Trial Lawyer of the Year
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/jerusalem-beligan">
                    <LazyLoad>
                      <img
                        src={JerusalemBeligan}
                        alt="Jeruselum Beligan, personal injury lawyer"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/jerusalem-beligan">
                    <strong>
                      Jerusalem
                      <br />
                      Beligan
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Employment/Wage & Hour Class Actions
                  <br />
                  Consumer Class Actions
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/gavin-long">
                    <LazyLoad>
                      <img
                        src={GavinLong}
                        alt="H. Gavin Long, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/gavin-long">
                    <strong>
                      H. Gavin
                      <br />
                      Long
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Personal Injury Law
                  <br />{" "}
                  <Link to="http://www.octla.org/" target="_blank">
                    OCTLA
                  </Link>{" "}
                  Trial Lawyer of the Year 2016
                  <br />
                  Board Member of{" "}
                  <Link to="https://www.caoc.org/" target="_blank">
                    CAOC
                  </Link>{" "}
                  and OCTLA
                  <br />
                  Member of{" "}
                  <Link to="https://www.abota.org/" target="_blank">
                    ABOTA
                  </Link>
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/steven-hilst">
                    <LazyLoad>
                      <img
                        src={StevenHilst}
                        alt="Steve Hilst, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/steven-hilst">
                    <strong>
                      Steven
                      <br />
                      Hilst
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Auto Products Liability
                  <br />
                  NTLA Top 100 Trial Lawyers
                  <br />
                  Member of{" "}
                  <Link to="https://www.abota.org/" target="_blank">
                    ABOTA
                  </Link>
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/tom-antunovich">
                    <LazyLoad>
                      <img
                        src={TomAntunovich}
                        alt="Tom Antunovich, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/tom-antunovich">
                    <strong>
                      Tom
                      <br />
                      Antunovich
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Medical Device & Pharmaceutical Liability
                  <br />
                  <ul>
                    <li>J&J Baby Powder / Ovarian Cancer</li>
                    <li>Hip Replacements Defects</li>
                    <li>Zofran, Plavix</li>
                  </ul>
                </td>
              </tr>

              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/ian-silvers">
                    <LazyLoad>
                      <img
                        src={IanSilvers}
                        alt="Ian Silvers, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/ian-silvers">
                    <strong>
                      Ian
                      <br />
                      Silvers
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Employment/Wage & Hour Class Actions
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/krist-biakanja">
                    <LazyLoad>
                      <img
                        src={KristBiakanja}
                        alt="Krist Biakanja, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/krist-biakanja">
                    <strong>
                      Krist
                      <br />
                      Biakanja
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Personal Injury Law
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  {" "}
                  <Link to="/attorneys/jordan-southwick">
                    <LazyLoad>
                      <img
                        src={JordanSouthwick}
                        alt="Jordan Southwick, personal injury attorney"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/jordan-southwick">
                    <strong>
                      Jordan
                      <br />
                      Southwick
                    </strong>
                  </Link>
                </td>
                <td>
                  Lawyer
                  <br />
                  Personal Injury Law
                  <br />
                </td>
              </tr>
              <tr style={{ borderBottomStyle: "hidden" }}>
                <td>
                  {" "}
                  <Link to="/attorneys/legal-administrator-shannon-barker">
                    <LazyLoad>
                      <img
                        src={ShannonBarker}
                        alt="Shannon barker, Legal administrator"
                      />
                    </LazyLoad>
                  </Link>
                </td>
                <td>
                  {" "}
                  <Link to="/attorneys/legal-administrator-shannon-barker">
                    <strong>
                      Shannon
                      <br />
                      Barker
                    </strong>
                  </Link>
                </td>
                <td>Legal Administrator</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="nopc notablet">
          <ul style={{ listStyle: "none" }}>
            <li>
              {" "}
              <Link to="/attorneys/john-bisnar">
                John Bisnar, Founding Partner (Retired)
              </Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/brian-chase">
                Brian Chase, Senior Partner
              </Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/scott-ritsema">Scott Ritsema, Partner </Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/jerusalem-beligan">
                Jerusalem Beligan, Lawyer
              </Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/gavin-long">H. Gavin Long, Lawyer</Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/steven-hilst">Steve Hilst, Lawyer</Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/tom-antunovich">Tom Antunovich, Lawyer</Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/ian-silvers">Ian Silvers, Lawyer</Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/krist-biakanja">Krist Biakanja, Lawyer</Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/jordan-southwick">
                Jordan Southwick, Lawyer
              </Link>
            </li>
            <li>
              {" "}
              <Link to="/attorneys/legal-administrator-shannon-barker">
                Shannon Barker, Legal Administrator
              </Link>
            </li>
          </ul>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  p {
    margin-bottom: 0;
  }
  img {
    width: 250px;
    height: auto;
    margin-bottom: 0;
  }

  td {
    line-height: 3rem;
    padding-bottom: 0;
    font-size: 1.4rem;
    text-align: left !important;

    img {
      line-height: 0;
    }
  }

  .nopc {
    display: none;
  }

  strong {
    font-size: 1.8rem;
    text-transform: uppercase;
  }

  td,
  th {
    border: none;
    /* text-align: center; */
    /* padding: 8px; */
  }

  @media (max-width: 1024px) {
    .nomobile {
      display: none;
    }

    .nopc {
      display: block;

      ul {
        li {
          text-align: center;
          margin-bottom: 1rem;
        }
      }
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
