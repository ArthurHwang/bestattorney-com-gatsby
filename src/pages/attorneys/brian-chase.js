// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

import BrianChaseImg from "../../images/brian-chase-2014.png"
import AIOLABadge from "../../images/logo/AIOLA-EA-2019.png"
import AmericasTopBadge from "../../images/logo/americas-top-100-min.png"
import AskTheLawyers from "../../images/logo/ask-the-lawyers.png"
import NationsTopBadge from "../../images/logo/badge-2018-min.png"
import BestLawyerMagBadge from "../../images/logo/bestlawyersmag.png"
import LawyerOfTheYearBadge from "../../images/logo/brian-chase-lawyer-of-the-year-2019-min.png"
import NextBigThing from "../../images/next-big-thing.jpg"
import StillUnsafe from "../../images/stillunsafe100.jpg"

const customPageOptions = {
  videosSidebar: [
    {
      videoName: "Brian Chase on American Law TV",
      videoUrl: "jKy3wa5M2yM"
    },
    {
      videoName: "Brian Chase on Master Practice",
      videoUrl: "EDTq-8PZEpA"
    },
    {
      videoName: "Brian Chase Brand Video",
      videoUrl: "wmv1HKnhW7U"
    },
    {
      videoName: "Brian Chase on Auto Defects",
      videoUrl: "LXnZuFkBXzE"
    },
    {
      videoName: "Brian Chase Defective Auto Parts",
      videoUrl: "73j7WXkdg28"
    }
  ]
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function BrianChasePage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Brian Chase - Top Rated California Personal Injury Lawyer"
        pageDescription="Nationally known and top rated auto defect and personal injury lawyer Brian Chase serves injured plaintiffs with trust, passion and results"
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Brian Chase - California Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <h2>What Being An Injury Advocate Means to Brian</h2>

        <p>
          Helping injured people obtain justice from big corporations when they
          manufacture dangerous products that cause injuries due to placing
          profit over safety.
        </p>
        <p>
          Helping accident victims collect what they are due from insurance
          companies that believe they are solely in business to collect
          premiums, as opposed to the business of paying valid claims. The
          satisfaction of helping someone recover their life after an accident
          or injury, especially one from the wrongdoing of others.
        </p>
        <h2>About Brian D. Chase</h2>

        <p>
          <LazyLoad>
            <img
              src={BrianChaseImg}
              alt="Brian D Chase, Personal Injury Lawyer"
              width="300"
              // height="423"
              className="imgleft-fixed"
            />
          </LazyLoad>
          Brian Chase is managing partner and senior trial attorney that heads
          up the litigation department of the firm. Brian was named{" "}
          <strong>2019 Lawyer of the Year</strong>,{" "}
          <strong>2019 Million Dollar Advocate</strong>,{" "}
          <strong>2018 Best Law Firm</strong>, 2017 High Stakes Litigator, Top
          Lawyer of Distinction in 2016 as well as a 2016 SuperLawyer recipient
          and one of America's Best Lawyers 2016. He was the 2015{" "}
          <strong>
            President of the Consumer Attorneys of California and is a Past
            President of the Orange County Trial Lawyers Association
          </strong>
          . He is a member of ABOTA. In 2014 he was named Trial Lawyer of the
          Year in products liability by the Orange County Trial Lawyers
          Association.{" "}
        </p>

        <p>
          {" "}
          <Link
            to="https://www.distinguishedcounsel.org/members/brian-chase/5241/"
            target="_blank"
          >
            {/* <LazyLoad> */}
            <img
              src={NationsTopBadge}
              alt="Nations Top One Percent Personal Injury Attorneys"
              width="150"
              // height="150"
              className="imgright-fixed"
            />
            {/* </LazyLoad> */}
          </Link>
          In 2012, Chase was named Trial Lawyer of the Year by the Consumer
          Attorneys of California and a Trial Lawyer of the Year Nominee by the
          Consumer Attorneys Association of Los Angeles. In 2004 he was named
          Trial Lawyer of the Year for products liability by the Orange County
          Trial Lawyers Association.
        </p>

        <p>
          He is listed as one of the{" "}
          <strong>
            Top 100 Trial Lawyers by the American Trial Lawyers Association
          </strong>{" "}
          since 2007. He's also been listed as the Nation's Top One Percent by
          the National Association of distinguished Counsel. He's earned the
          title of Southern California SuperLawyer since 2007 and a Top 50
          Orange County SuperLawyer for the past four years.{" "}
        </p>

        <p>
          {" "}
          <Link
            to="https://www.top100highstakeslitigators.com/listing/brian-d-chase/"
            target="_blank"
          >
            {/* <LazyLoad> */}
            <img
              src={AmericasTopBadge}
              width="150"
              // height="145"
              alt="Americas Top 100 Personal Injury Attorneys"
              className="imgright-fixed"
            />
            {/* </LazyLoad> */}
          </Link>
          In 2009, Mr. Chase had his book "
          <Link
            to="https://www.amazon.com/Still-Unsafe-At-Any-Speed/dp/1615845755"
            title="Brian Chase - Still Unsafe at Any Speed"
            target="_blank"
          >
            Still Unsafe at Any Speed
          </Link>
          " - dealing with the auto industry and defective vehicles, published.
          He is releasing an update in 2019. He is a frequent lecturer
          nationwide on litigation related topics and has been a frequent guest
          on radio and television, including appearances on "CBS Special
          Reports," "Peter Jennings/World News Tonight," "Fox 11 News" and
          "America's Best Lawyers."
        </p>

        <p>
          Brian was lead attorney on four important, precedent-setting appellate
          opinions: Martinez-Mazon v. Ford Motor Company (auto products defect
          case dealing with Forum Non Conveniens); Romine v. Johnson Controls
          (auto products defects case dealing with consumer expectation test for
          proving defect); Schreiber v. Estate of Kiser (California Supreme
          Court case dealing with expert witness designations); and Hernandez v.
          State of California (dealing with governmental design immunity).
        </p>

        <div className="panel panel-primary">
          <div className="panel-heading">
            Some of Brian Chase's Case Results
          </div>
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>
                  <strong>Award Amount</strong>
                </th>
                <th>
                  <strong>Type of Case</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$38,650.000.00</td>
                <td>Motorcycle Accident</td>
              </tr>
              <tr>
                <td>$24,700.000.00</td>
                <td>MVA - Auto Defect</td>
              </tr>
              <tr>
                <td>$23,091.000.00</td>
                <td>Defective Product - MVA</td>
              </tr>
              <tr>
                <td>$16,444.904.00</td>
                <td>MVA</td>
              </tr>
              <tr>
                <td>$11,694.904.00</td>
                <td>Premises Liability</td>
              </tr>
              <tr>
                <td>$10,591.098.00</td>
                <td>Product Liability</td>
              </tr>
              <tr>
                <td>$10,030.000.00</td>
                <td>MVA -Auto Defect</td>
              </tr>
              <tr>
                <td>$8,500.000.00</td>
                <td>MVA</td>
              </tr>
              <tr>
                <td>$8,250.000.00</td>
                <td>MVA</td>
              </tr>
              <tr>
                <td>$4,750.000.00</td>
                <td>Premises Liability</td>
              </tr>
              <tr>
                <td>$3,780.000.00</td>
                <td>Negligence</td>
              </tr>
              <tr>
                <td>$2,432.250.00</td>
                <td>Product Liability</td>
              </tr>
              <tr>
                <td>$2,000.000.00</td>
                <td>Premises Liability</td>
              </tr>
            </tbody>
          </table>
        </div>

        <br />
        <h2>2014 - 2019 Best Lawyers in America</h2>

        <div className="algolia-search-text">
          <p>
            {/* <LazyLoad> */}
            <img
              src={BestLawyerMagBadge}
              width="145"
              height="165"
              alt="Brian Chase featured in Best Lawyers magazine"
              className="imgleft-fixed"
              style={{ marginBottom: "2rem" }}
            />
            {/* </LazyLoad> */}
            Brian Chase selected by his peers for inclusion in the 21st Edition
            of The{" "}
            <Link to="http://www.bestlawyers.com" target="_blank">
              Best Lawyers in America
            </Link>{" "}
            in the practice areas of:{" "}
            <Link to=""> Personal Injury Litigation</Link> - Plaintiffs and
            Product Liability Litigation - Plaintiffs. Best Lawyers is the
            oldest and most respected peer-review publication in the legal
            profession. A listing in Best Lawyers is widely regarded by both
            clients and legal professionals as a significant honor, conferred on
            a lawyer by his or her peers. For more than three decades, Best
            Lawyers lists have earned the respect of the profession, the media,
            and the public, as the most reliable, unbiased source of legal
            referrals anywhere.{" "}
          </p>
          <p style={{ clear: "both" }}>
            <strong>
              Brian Chase is a passionate personal injury attorney who
              understands what you are going through and will protect your
              rights.
            </strong>
          </p>
          <p style={{ marginBottom: "2.5rem" }}>
            {/* <LazyLoad> */}
            <img
              src={LawyerOfTheYearBadge}
              alt="2019 Lawyer of the Year, Brian Chase"
              width="153"
              height="162"
              className="imgleft-fixed"
            />
            {/* </LazyLoad> */}
            Brian focuses his efforts in{" "}
            <Link to="/auto-defects">
              <strong>auto defects</strong>
            </Link>{" "}
            and defective or dangerous products. He's made a name for himself
            fighting some of the biggest corporate giants in the name of
            fairness to his clients. From car defects to consumer products that
            fail to warn or harm, Brian is the man for the job.
          </p>
          <p style={{ marginBottom: "2.5rem" }}>
            <em>
              "I specifically went to law school to become a personal injury
              trial attorney. It is what I love, what I do, and what I am."
            </em>{" "}
            - Brian Chase.
          </p>
          <br />
          <br />
          <p style={{ marginBottom: "2.5rem" }}>
            {/* <LazyLoad> */}
            <img
              src={AIOLABadge}
              alt="2019 Elite Advocate, Brian Chase"
              width="153"
              height="172"
              className="imgleft-fixed"
              id="AIOLA-badge"
              style={{
                marginBottom: "2rem",
                position: "relative",
                bottom: "20px"
              }}
            />
            {/* </LazyLoad> */}
            One of Mr. Chase's proudest moments among many, was the victory he
            obtained representing a client that was seriously injured in an auto
            defects case that left her paralyzed.
          </p>
          <p style={{ marginBottom: "2rem" }}>
            Brian took on the auto industry and garnered a $24.7 million dollar
            victory for his client and secured medical care & financial security
            for the rest of her life. Here's Brian discussing the case with Fox
            News.
          </p>
          <p style={{ clear: "both" }}>
            {/* <LazyLoad> */}
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/wQ0uauw5neY?rel=0"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            {/* </LazyLoad> */}
          </p>
          <p>
            Another favorable and touching case for Brian was obtaining over a
            million dollars for the most amazing 95 year old woman he says he
            ever met -- whose life was ruined due to the negligence of another,
            after the insurance company stated *in writing* that her claim was a
            "sham."
          </p>
          <p>
            {/* <LazyLoad> */}
            <iframe
              width="100%"
              height="500"
              src="https://www.youtube.com/embed/PFYyZrS5BLY?rel=0"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen={true}
            />
            {/* </LazyLoad> */}
          </p>
        </div>
        <h2>Brian Chase - Author With a Passion for Safety</h2>

        <p>
          {/* <LazyLoad> */}
          <img
            src="/images/2019-assets/second-collision-brian-chase.png"
            width="220"
            alt="The Second Collision - Brian Chase"
            className="imgcenter-fixed"
            style={{ marginBottom: "2rem" }}
          />
          {/* </LazyLoad> */}
          Brian Chase has released his latest version of his auto defect book
          titled Second Collision where he takes on the automotive industry as a
          follow up to Still Unsafe at Any Speed. Brian discusses a variety of
          issues with today's standards in motor vehicle safety and the dangers.
        </p>
        <p className="clearfix">
          {/* <LazyLoad> */}
          <img
            src={StillUnsafe}
            alt="Still Unsafe at Any Speed - Brian Chase"
            width="100"
            height="162"
            className="imgcenter-fixed"
            style={{ marginBottom: "2rem" }}
          />
          {/* </LazyLoad> */}
          Brian Chase is the author of "
          <Link
            to="http://www.amazon.com/Still-Unsafe-At-Any-Speed/dp/1615845755"
            target="_blank"
          >
            Still Unsafe at Any Speed
          </Link>
          ", a book about the automobile industry and dangers it hides. From
          wrongful deaths to catastrophic injuries, Brian Chase takes on the
          underbelly of the auto industry putting the mighty dollar before
          safety in his book on auto defects.
        </p>
        <p className="clearfix">
          {/* <LazyLoad> */}
          <img
            src={NextBigThing}
            alt="Brian Chase - The Next big thing"
            width="107"
            height="160"
            className="imgcenter-fixed"
            style={{ marginBottom: "2rem" }}
          />
          {/* </LazyLoad> */}
          Brian co-authored "The Next Big Thing", a road-map to the top trends
          from leading experts on navigating the new economy.
        </p>
        <p>
          Have questions, need help? Contact us about your auto defect or
          products liability case for a free evaluation.{" "}
          <strong>949-203-3814</strong>. We've got the reputation, means, and
          passion to pursue your case in the name of justice. Brian Chase has
          recovered hundreds of millions of dollars in settlements and verdicts
          for serious and catastrophic injuries.
        </p>
        <h2>Articles by Brian D. Chase</h2>
        <ul>
          <li>
            {" "}
            <Link to="/blog/identifying-auto-product-defect-cases">
              Identifying and Litigating Quality Auto Products Defect Cases
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/motions-in-limine">
              Expert Witnesses and Motions in Limine
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/road-edge-recovery-manuever">
              Cutting Edge Testing For Rollover Cases: NHTSA's Road Edge
              Recovery Maneuver
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/utilizing-expert-witnesses">
              Utilizing Expert Witnesses
            </Link>
          </li>
        </ul>
        <h2>Brian Chase featured on The Great Trials Podcast</h2>
        <LazyLoad>
          <iframe
            id="great-trials"
            title="Ep 024: Brian Chase | Romine v. Johnson Controls, Inc. | $24.7 million verdict"
            src="https://www.podbean.com/media/player/amw8i-b433ed?from=yiiadmin&download=1&version=1&skin=2&btn-skin=103&auto=0&share=1&fonts=Helvetica&download=1&rtl=0&pbad=1"
            height="122"
            width="100%"
            style={{ border: "none" }}
            scrolling="no"
            data-name="pb-iframe-player"
          />
        </LazyLoad>

        <h2>MCLE Certified</h2>
        <p>
          Brian Chase is in-house certified to teach the course "How to Identify
          Auto Defect Cases" with a 1.5 hour credit for your MCLE.
        </p>
        <h3>About Bisnar Chase</h3>
        <p>
          <em>
            Bisnar Chase is a nine attorney AV-rated national law firm founded
            in 1977 in Newport Beach, California. Brian Chase is managing
            partner and senior trial attorney that heads up the litigation
            department of the firm.  The firm specializes in automotive defect
            cases, catastrophic personal injury cases, mass torts
            (pharmaceutical and medical devices), automotive defect, employment,
            and consumer class actions. {" "}
          </em>
        </p>
        <div itemScope={true} itemType="http://schema.org/Person">
          <span itemProp="name">Brian D. Chase</span>{" "}
          <span itemProp="jobTitle">- California Personal Injury Lawyer</span>
          <div
            itemProp="address"
            itemScope={true}
            itemType="http://schema.org/PostalAddress"
          >
            <span itemProp="streetAddress">1301 Dove St. #120 </span>{" "}
            <span itemProp="addressLocality">Newport Beach</span>,{" "}
            <span itemProp="addressRegion">CA</span>
            <span itemProp="postalCode">92660</span>
          </div>
          <span itemProp="telephone">(949) 203-3814</span>{" "}
          <Link
            style={{ display: "block", marginTop: "20px" }}
            target="_blank"
            to="https://www.askthelawyers.com/listing/california-1/newport-beach/automobile-defects-attorney/brian-d-chase/"
          >
            <img
              target="_blank"
              src={AskTheLawyers}
              alt="Brian Chase featured in at AskTheLawyers.com"
              className="badge"
              style={{ marginBottom: "2rem" }}
            />
          </Link>
        </div>
        <h3>Brian Chase Online</h3>
        <li>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/164109"
            target="_blank"
            name="State Bar Profile"
          >
            State Bar Profile
          </Link>
        </li>
        <li>
          {" "}
          <Link to="https://www.linkedin.com/in/briandchase" target="_blank">
            LinkdIn
          </Link>
        </li>
        <li style={{ marginBottom: "2rem" }}>
          {" "}
          <Link
            to="http://www.avvo.com/attorneys/92660-ca-brian-chase-339392.html"
            target="_blank"
            name="Brian Chase Avvo"
          >
            Avvo
          </Link>
        </li>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */
  #AIOLA-badge {
    @media (max-width: 700px) {
      bottom: 0 !important;
    }
  }

  #great-trials {
    @media (max-width: 700px) {
      height: 122px;
    }
  }
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
