// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import IanSilvers from "../../images/ian-silvers-small.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function IanSilversPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Personal Injury Lawyer Ian Silvers - Bisnar Chase"
        pageDescription="Award winning personal injury attorney Ian Silvers of Bisnar Chase in California represents employees and class action cases."
      />
      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Ian Silvers - California Personal Injury Lawyer</h1>
        <BreadCrumbs location={location} />
        <h2>A Passion for Helping Employees</h2>
        <LazyLoad>
          <img
            src={IanSilvers}
            alt="Employment and class action attorney Ian Silvers"
            width="190"
            // height="126"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Ian Silvers has a substantial background in employment law and is
          fervent about protecting and fighting for the rights of employees. He
          represents employees in an array of cases including discrimination,
          harassment, retaliation and wage and hour matters. Silvers is a member
          of Bisnar Chase’s{" "}
          <Link to="/class-action">Class Action Litigation</Link> Department
          that handles{" "}
          <Link to="/employment-law/wage-and-hour">wage and hour</Link> class
          actions. He also tackles non-class action matters addressing sexual
          harassment, wrongful termination, retaliation and discrimination.
        </p>
        <p>
          Silvers graduated from the University of California, San Diego, in
          June 2003 with a Bachelor&#39;s degree in Economics. He obtained his
          law degree from Santa Clara University School of Law in May 2006.
          Silvers was a member of the Phi Delta Phi International Legal Honors
          Fraternity.
        </p>
        <p>
          When in law school, he interned at the Katharine and George Alexander
          Community Law Center advising low-income employees under the
          supervision of other attorneys on employment matters including
          discrimination, harassment, retaliation, unemployment and wage and
          hour.
        </p>

        <p>
          It was here, Silvers says, that he got a taste of what it&#39;s like
          to help "those who were truly not able to help themselves."
        </p>

        <p>
          "I could see that in employment law matters, there were so many
          employees who did not fully know their rights and as a result they
          were being taken advantage of by their employers," he said. "However
          these employees are not always able to take action against the
          employers, often for financial reasons."
        </p>

        <h2>Employment Lawsuits and Class Actions</h2>

        <p>
          Silvers has also represented employees before the Division of Labor
          Standards Enforcement on wage and hour matters and obtained favorable
          decisions for his clients. He became a{" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Licensee/Detail/247416"
            target="_blank"
          >
            {" "}
            member of the California State Bar
          </Link>{" "}
          in December 2006 and is licensed to practice in California state
          courts and before the United States District Courts for both the
          Central and Eastern Districts of California.
        </p>
        <p>
          Silvers perseveres in his efforts to assist those who cannot afford
          legal representation by volunteering advisory services. He received
          the Wiley W. Manuel Award for Pro Bono Legal Service in 2009.
        </p>

        <p>
          Before joining Bisnar Chase, Silvers worked at a Long Beach employment
          law firm where he regularly dealt with wage and hour matters,
          including class actions and Private Attorneys General Act matters. He
          has been able to reach significant settlements, including multiple
          seven-figure class action settlements for both, small and large
          classes of employees.
        </p>

        <h2>What He Values Most</h2>

        <p>
          Silvers says that his case successes for the employees whom he has
          helped over the years count among his greatest accomplishments.
        </p>
        <p>
          "It&#39;s not just about helping them stand up for themselves against
          their employers, but also to help them understand how they can move on
          and get some sort of closure," he said. "These cases are not just
          about money. Often times, employees get fixated on what happened to
          them and it not only affects their future employment, but also their
          relationships with others."
        </p>
        <p>
          When he represents these individuals whose lives have been deeply
          impacted, Silvers says that he makes a point of looking to the future
          on their behalf and helping them figure out how to get past the
          situation. He gets tremendous satisfaction when he sees the difference
          he has made for an individual employee.
        </p>
        <p>
          "Employers may win, lose or settle a lawsuit and it will not really
          change their business or how they conduct themselves," Silvers said.
          "However, each time I receive appreciation from an employee who sees
          that I believed in him or her and fought on his or her behalf to help
          right injustice and wrongdoing, I&#39;m reminded about why I love this
          area of law.
        </p>

        <h2>Pride in Being a Bisnar Chase Team Member</h2>
        <p>
          Silvers says he has been impressed with Bisnar Chase&#39;s commitment
          to helping and giving back to the community – calling that a rare
          quality.
        </p>
        <p>
          "Many law firms pay attention to only their cases and the business,
          but Bisnar Chase regularly focuses on what it can do to help the
          community – whether it is donating some essentials such as backpacks,
          knitting items, etc., or contributing to a variety of charities not
          only financially but also with their time," he said.
        </p>
        <p>
          Silvers grew up in the Los Angeles area. He lives in Orange County
          with his wife, Jennifer, a kindergarten teacher in Anaheim and their
          two children, Lucas and Makenna. When Silvers is not fighting for
          employees&#39; rights, he enjoys spending time with his family,
          watching sports, especially football and basketball, and traveling.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
