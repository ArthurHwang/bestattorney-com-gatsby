// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import TomAntunovich from "../../images/tom-antunovich-small.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function TomAntunovichPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Tom Antunovich - California Personal Injury Attorney - Bisnar Chase"
        pageDescription="Tom Antunovich is a California personal injury lawyer and an associate of Bisnar Chase. He is a key part of the successful Bisnar Chase team, which has a 96% success rate and has recovered more than $500m for clients."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Tom Antunovich - California Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />

        <h2>Professional Accomplishments</h2>
        <LazyLoad>
          <img
            src={TomAntunovich}
            alt="Personal injury lawyer Tom Antunovich"
            width="238"
            className="imgright-fixed"
          />
        </LazyLoad>

        <p>
          Tom Antunovich is a California personal injury attorney and trial
          lawyer at the firm. His practice focuses on negligence and products
          liability claims. He represents auto accident victims and those who
          have been injured by defective medical devices or prescription drugs.
        </p>
        <p>
          In mid-2016, Antunovich was assigned as lead associate attorney for
          the firm's{" "}
          <Link to="/pharmaceutical-litigation/talcum-powder">
            {" "}
            Johnson &amp; Johnson Baby Powder ovarian cancer cases
          </Link>
          . Antunovich finds satisfaction in holding wrongdoers and corporations
          accountable for their actions and securing compensation for his
          injured clients.
        </p>

        <p>
          {" "}
          <Link to="http://thenationaltriallawyers.org" target="_blank" />
          Antunovich is an Orange County native. He graduated from Troy High
          School, the prestigious magnate school in Fullerton and received a
          bachelor's degree in Kinesiology from Cal Poly Pomona in 2010 where he
          also competed on the Men's Soccer team under athletic scholarship. He
          attended Western State College of Law and graduated cum laude in May
          2015. In July 2015, he passed the California Bar Examination.
        </p>
        <p>
          At Bisnar Chase, Antunovich has proven himself as a young man with
          passion and potential, said H. Gavin Long, one of the firm's trial
          lawyers.
        </p>
        <p>
          <em>
            &ldquo;If you're going to be a personal injury lawyer, you have to
            be empathetic while being a bulldog that fights for clients' rights.
            Tom has those qualities 100 percent.&rdquo; – Top Rated Trial
            Lawyer, <Link to="/attorneys/gavin-long">H. Gavin Long</Link>
          </em>
        </p>
        <h2>Winning Cases</h2>
        <LazyLoad>
          <img
            alt="The National Trial Lawyers"
            src="https://www.thenationaltriallawyers.org/images/NTL-top-40-member-logo.png"
            width="250"
            className="imgleft-fixed"
          />
        </LazyLoad>
        <p>
          Antunovich has already made a huge impact at the firm. He has secured
          significant compensation for his clients and has one jury trial under
          his belt which resulted in a Plaintiff's verdict in a disputed
          liability case. Managing partner{" "}
          <Link to="/attorneys/brian-chase">Brian Chase</Link> see's a long and
          successful future for Tom.
        </p>
        <p>
          <em>
            &ldquo;In the 26 years I have been practicing law, I have never seen
            a new attorney with the drive, determination, talent and skills as I
            see in Tom. &nbsp;I saw his potential when he was a law clerk at the
            firm, and knew we had to hire him as a new associate when he
            graduated from law school. &nbsp;That has turned out to be one of
            the best decisions I've made at Bisnar Chase. &nbsp;His skill level
            rivals that of attorneys who have been practicing for 10+ years.
            &nbsp;He has earned my trust so much that he works hand in hand with
            me on some of the firm's largest cases.&rdquo;
          </em>
        </p>
        <p>
          Antunovich is a bright young lawyer full of energy and commitment to
          his clients. As a trial lawyer, he relishes the opportunity to work up
          his cases for trial and present them to the jury.
        </p>
        <h2>Antunovich is Acutely Aware of His Job's Importance</h2>
        <p>
          <em>
            &ldquo;Our clients – they put their trust in us. I make sure the
            work I put into every case reflects that. Each case means a lot to
            me, because it means a lot to my client. It makes me want to fight
            and it makes me want to win.&rdquo;
          </em>
        </p>
        <p>
          <strong>
            Antunovich's list of accomplishments thus far speaks for itself:
          </strong>
        </p>
        <p>
          Antunovich is a member of the Orange County Bar Association, Orange
          County Trial Lawyer's Association and Consumer Attorneys of
          California.
        </p>
        <div className="panel panel-primary">
          <div>
            <h3>Tom Antunovich's Case Results</h3>
          </div>
          <table>
            <thead>
              <tr>
                <th>
                  <strong>Award Amount</strong>
                </th>
                <th>
                  <strong>Type of Case</strong>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="130">$575,000.00</td>
                <td width="271">Trip and Fall</td>
              </tr>
              <tr>
                <td>$225,000.00</td>
                <td>Motorcycle v. Auto</td>
              </tr>
              <tr>
                <td>$185,000.00</td>
                <td>Vaginal Mesh Defect</td>
              </tr>
              <tr>
                <td>$150,000.00</td>
                <td>Auto v. Auto</td>
              </tr>
              <tr>
                <td>$115,000.00</td>
                <td>Auto Accident</td>
              </tr>
              <tr>
                <td>$100,000.00</td>
                <td>Pedestrian Accident</td>
              </tr>
              <tr>
                <td>$100,000.00</td>
                <td>Knee Replacement Defect</td>
              </tr>
              <tr>
                <td>$75,000.00</td>
                <td>Knee Replacement Defect</td>
              </tr>
            </tbody>
          </table>
        </div>

        <h2>Personal Interests</h2>
        <p>
          When Antunovich is not working for his clients, he enjoys playing
          soccer, hanging out with friends, and supporting his favorite sports
          teams: Los Angeles Lakers, Manchester United Red Devils, Los Angeles
          Kings, and Los Angeles Galaxy. One of his favorite quotes is,
          &ldquo;There are risks and costs to action. But they are far less than
          the long range risks of comfortable inaction.&rdquo; – John F.
          Kennedy.
        </p>
        <p>
          Antunovich is the oldest of 3 brothers and is engaged to his longtime
          girlfriend.
        </p>
        <h2>Contact Tom Antunovich</h2>
        <p>
          If you would like to reach Tom for a free consultation for an injury
          please call 800-561-4887. There is no fee if we do not win your case.
        </p>
        <p>
          {" "}
          <Link
            to="http://members.calbar.ca.gov/fal/Member/Detail/305216"
            target="_blank"
          >
            See Tom's Profile on the State Bar of California Website
          </Link>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
