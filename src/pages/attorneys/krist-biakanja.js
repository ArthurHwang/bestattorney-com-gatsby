// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import { LazyLoad } from "src/components/elements/LazyLoad"

import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutAttorneys } from "../../components/layouts/Layout_Attorneys"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"

import KristBiakanja from "../../images/Krist-Biakanja-Headshot-rsz.jpg"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function KristBiakanjaPage({ location }) {
  return (
    <LayoutAttorneys {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Krist Biakanja - California Personal Injury Attorney - Bisnar Chase"
        pageDescription="Krist Biakanja is a California personal Injury Lawyer and an associate attorney of Bisnar Chase. He represents injured plaintiffs in California and joins a team with over $500M recovered and a 96% success rate."
      />

      <ContentWrap>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Krist Biakanja - California Personal Injury Attorney</h1>
        <BreadCrumbs location={location} />
        <h2>Education and Accomplishments</h2>
        <LazyLoad>
          <img
            src={KristBiakanja}
            alt="Personal injury lawyer Krist Biakanja"
            width="238"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          Krist Biakanja is a committed{" "}
          <Link to="/" target="new">
            personal injury attorney
          </Link>{" "}
          at Bisnar Chase who thrives on fighting for the rights of his clients.
        </p>
        <p>
          After fulfilling a lifelong dream of becoming a lawyer, Krist is
          focused on making a significant impact at Bisnar Chase. He is
          dedicated to helping those who have been injured, and ensuring that
          wrongdoers are held responsible.
        </p>
        <p>
          Krist was born and raised in Orange County and graduated from
          Huntington Beach High School. A talented academic with an unparalleled
          desire to excel in the legal profession, he earned a BA in Political
          Science from California State University Long Beach in 2013. During
          his time at CSULB, he was a member of the Political Science Honor
          Society and was a key contributor to the university’s
          championship-winning American Collegiate Moot Court Association
          (ACMCA) team.
        </p>
        <p>
          From there, Krist earned his Juris Doctorate – as well as the
          prestigious LL.M. masters law degree – from the University of
          Washington School of Law.
        </p>
        <p>
          Alongside his studies, Krist honed his legal knowledge by working with
          the Los Angeles County District Attorney’s Office. While there, he
          boasted an unbeaten record in preliminary hearings and worked on high
          profile trials, including two murder trials and a death penalty case.
          Krist has also worked with the University of Washington Tribal Court
          Public Defense Clinic, as well as the Immigrant Families Advocacy
          Program.
        </p>
        <h2>Dedicated to Making a Difference</h2>
        <p>
          Krist joined{" "}
          <Link to="/" target="new">
            Bisnar Chase
          </Link>{" "}
          as a Law Clerk in 2018 and was promoted to become an associate
          attorney at the firm after passing the California Bar.
        </p>
        <p>
          “It is incredibly exciting for me to become an associate at Bisnar
          Chase,” Krist said. “I’m looking forward to being able to put my
          education and training to good use, and I am dedicated to making a
          difference for our clients.”
        </p>
        <p>
          The talented personal injury attorney takes his new role very
          seriously. He added: “I have wanted to become a lawyer since I was a
          kid, and it is a position and a responsibility that I take great pride
          in.
        </p>
        <p>
          “I want to be able to help those people who have been hurt and need
          help, and ensure that those responsible are made accountable for their
          actions.”
        </p>
        <h2>Personal Interests</h2>
        <p>
          When Krist is not fighting for the rights of his clients, he likes to
          keep active. He enjoys hiking and fishing, and achieved the elite rank
          of an Eagle Scout in his younger days. Krist also loves to travel,
          both across the United States and internationally, and is a huge
          sports fan – watching football, hockey, and baseball. In keeping with
          his Orange County roots, he is a fan of the Anaheim Ducks and the
          Angels, as well as the Los Angeles Chargers and UW Huskies.
        </p>
        <h2>Contact Krist Biakanja</h2>
        <p>
          As a member of Bisnar Chase’s outstanding roster of highly-skilled
          lawyers, Krist Biakanja works on a ‘No Win, No Fee’ basis, with a
          focus on personal injury law. To contact Krist, please call (800)
          561-4887.
        </p>
        <br />{" "}
        <Link
          to="http://members.calbar.ca.gov/fal/Licensee/Detail/323122"
          target="new"
        >
          View Krist's profile on the State Bar of California website
        </Link>
        <br />
        <br />
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrap>
    </LayoutAttorneys>
  )
}

const ContentWrap = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
