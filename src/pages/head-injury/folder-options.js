/**
 * Changing any option on this page will change all options for every file that exists in this folder
 * If you want to only update values on a specific page, copy + paste the property you wish to change into * * the actual page inside customFolderOptions
 */

// -------------------------------------- //
// ------------- EDIT BELOW ------------- //
// -------------------------------------- //

const folderOptions = {
  /** =====================================================================
  /** ============================ Page Subject ===========================
  /** =====================================================================
   * If empty string, it will default to "personal-injury"
   * If value supplied, it will change header wording to the correct practice area
   * Available options:
   *  dog-bite
   *  car-accident
   *  class-action
   *  auto-defect
   *  product-defect
   *  medical-defect
   *  drug-defect
   *  premises-liability
   *  truck-accident
   *  pedestrian-accident
   *  motorcycle-accident
   *  bicycle-accident
   *  employment-law
   *  wrongful-death
   **/
  pageSubject: "",

  /** =====================================================================
  /** ========================= Spanish Equivalent ========================
  /** =====================================================================
   * If empty string, it will default to "/abogados"
   * If value supplied, it will change espanol link to point to that URL
   * Reference link internally, not by external URL
   * Example: - "/abogados/sobre-nosotros" --GOOD
   *          - "https://www.bestatto-gatsby.netlify.app/abogados/sobre-nosotros" --VERY BAD
   **/
  spanishPageEquivalent: "/abogados",

  /** =====================================================================
  /** ============================== Location =============================
  /** =====================================================================
   * If empty string, it will default location to orange county and use toll-free-number
   * If value "orange-county" is given, it will change phone number to newport beach office number
   * If value supplied, it will change phone numbers and JSON-LD structured data to the correct city geolocation
   * Available options:
   *  orange-county
   *  los-angeles
   *  riverside
   *  san-bernardino
   **/
  location: "",

  /** =====================================================================
  /** =========================== Sidebar Head ============================
  /** =====================================================================
   * If textHeading does not exist, component will not be mounted or rendered
   * This component will render a list of values that you provide in text body
   * You may write plain english
   * If you need a component that can render raw HTML, use HTML sidebar component below.
   **/
  sidebarHead: {
    textHeading: "",
    textBody: ["", ""]
  },

  /** =====================================================================
  /** =========================== Extra Sidebar ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must write actual HTML inside the string
   * Use backticks for multiline HTML you want to inject
   * If you want just a basic component that renders a list of plain english sentences, use the above component
   **/
  extraSidebar: {
    title: "Types of Head Injuries",
    HTML: `
		<ul className='bpoint'>
			<li><strong>Concussions</strong>: This is when an individual suffers a blow to the head and experiences an instant loss of awareness or alertness. Some victims of concussions may lose consciousness for seconds, minutes or even hours. Recent studies show that those who suffer more than one concussion, especially as a result of sports injuries, may face long-term consequences such as victims of more severe traumatic brain injuries.</li>
			<li><strong>Skull fractures</strong>: A skull fracture is a break or crack in the skull bone. Individuals may suffer a linear skull fracture (a break that does not result in bone movement), depressed skull fracture (part of the skull is sunken in from the trauma), a diastatic skull fracture (breaks along the suture lines in the skull) or basilar skull fracture (break in the bone from the base of the skull).</li>
			<li><strong>Intracranial hematoma</strong>: This is when the victim suffers a blood clot in or around the brain. Depending on the type of hematoma injury, the victim may experience minor to life-threatening symptoms.</li>
			<li><strong>Diffuse axonal injury</strong>: This is when the brain is injured due to a shaking movement. Diffuse axonal injuries can occur as the result of a car crash, a fall-related accident or due to other causes such as shaken baby syndrome. </li>
			<li>If you or a loved one have suffered from any of the above injuries as a result of someone else’s negligence, know your rights and <a href='/contact'>contact an attorney </a> today.</li>
		</ul>`
  },

  /** =====================================================================
  /** ======================== Sidebar Extra Links ========================
  /** =====================================================================
   * An extra component to add resource links if the below component gets too long.
   * This option only works for PA / GEO pages at the moment
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
  **/
  sidebarExtraLinks: {
    title: "",
    links: [
      {
        linkName: "",
        linkURL: ""
      },
      {
        linkName: "",
        linkURL: ""
      }
    ]
  },

  /** =====================================================================
  /** =========================== Sidebar Links ===========================
  /** =====================================================================
   * If title does not exist, component will not be mounted or rendered
   * You must use internal links!
   **/
  sidebarLinks: {
    title: "Head Injury Injury Information",
    links: [
      {
        linkName: "Brain Trauma",
        linkURL: "/head-injury/brain-trauma"
      },
      {
        linkName: "Child Injuries",
        linkURL: "/head-injury/child-injuries"
      },
      {
        linkName: "Concussion Symptoms by Gender",
        linkURL: "/head-injury/concussion-symptoms-by-gender"
      },
      {
        linkName: "Effects of Concussions",
        linkURL: "/head-injury/effects-of-concussions"
      },
      {
        linkName: "Elderly Injuries",
        linkURL: "/head-injury/elderly-injuries"
      },
      {
        linkName: "Second Impact Syndrome",
        linkURL: "/head-injury/second-impact-syndrome"
      },
      {
        linkName: "Traumatic Brain Injury",
        linkURL: "/head-injury/traumatic-brain-injury"
      },
      {
        linkName: "Traumatic Brain Injury FAQ's",
        linkURL: "/head-injury/traumatic-brain-injury-tips"
      },
      {
        linkName: "Treatments for Head Injuries",
        linkURL: "/head-injury/treatments"
      },
      {
        linkName: "Head Injury Home",
        linkURL: "/head-injury"
      }
    ]
  },

  /** =====================================================================
  /** =========================== Video Sidebar ===========================
  /** =====================================================================
   * If the first items videoName does not exist, component will not mount or render
   * If no videos are supplied, default will be 2 basic videos that appear on the /about-us page
   **/
  videosSidebar: [
    {
      videoName: "OC Car Accident Brain Injury Client Testimonial",
      videoUrl: "lVUUCtM3Ebg"
    },
    {
      videoName: "Short and Long Term Effects of a Concussion",
      videoUrl: "gny8lZ1Ds3g"
    }
  ],

  /** =====================================================================
  /** ===================== Sidebar Component Toggle ======================
  /** =====================================================================
   * If you want to turn off any sidebar component, set the corresponding value to false
   * Remember that the components above can be turned off by not supplying a title and / or value
   **/
  showBlogSidebar: true,
  showAttorneySidebar: true,
  showContactSidebar: true,
  showCaseResultsSidebar: true,
  showReviewsSidebar: true,

  /** =====================================================================
  /** =================== Rewrite Case Results Sidebar ====================
  /** =====================================================================
   * If value is set to true, the case results items will be re-written to only include entries that match the pageSubject if supplied.  The default will show all cases.
   **/
  rewriteCaseResults: true,

  /** =====================================================================
  /** ========================== Full Width Page ==========================
  /** =====================================================================
  * If value is set to true, entire sidebar will not mount or render.  Use this to gain more space if necessary
  **/
  fullWidth: false
}

export default {
  ...folderOptions
}
// -------------------------------------- //
// ------------- EDIT ABOVE ------------- //
// -------------------------------------- //
