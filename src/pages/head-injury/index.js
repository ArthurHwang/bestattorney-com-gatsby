// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-head-injury-lawyer-banner.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Head Injury Lawyers - Bisnar Chase Attorneys"
        pageDescription="Contact the California head injury lawyers of Bisnar Chase if you have suffered a serious brain injury. Free consultation 800-561-4887."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Head Injury Lawyers</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california head injury lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            Our <strong> California Head Injury Lawyers</strong> at Bisnar Chase
            have been representing clients and winning head injury cases for
            over <strong> 39 years</strong>. We have established a
            <strong> 96% success rate</strong> and have brought in over{" "}
            <strong> $500 Million</strong>.
          </p>
          <p>
            We have gone after large corporations, governmental agencies and
            individuals who have, through negligence or wrongdoing, caused our
            clients serious injury or harm.
          </p>
          <p>
            Our skilled and experienced team of{" "}
            <Link to="/" target="new">
              California Personal Injury Lawyers{" "}
            </Link>{" "}
            have the resources and ability to take on complex cases, other law
            firms have previously declined, because at Bisnar Chase, you get the
            best.
          </p>
          <p>
            We work on a contingency fee basis, which means that we charge you
            nothing until there has been a monetary recovery in your case. We
            advance all costs and expenses.
          </p>
          <LazyLoad>
            <img
              src="/images/attorney-chase.jpg"
              height="141"
              alt="Brian Chase"
              className="imgright-fixed"
            />
          </LazyLoad>
          <p>
            <strong> Bisnar Chase</strong> is a reputed personal injury law firm
            in California, whose lawyers have more than three decades of
            experience representing seriously and catastrophically injured
            victims including adults and{" "}
            <Link to="/head-injury/child-injuries" target="new">
              children with severe head injuries
            </Link>
            .
          </p>

          <p>
            Please contact our head injury lawyers today for a free consultation
            and comprehensive case evaluation at <strong> 800-561-4887</strong>
            ..
          </p>
        </div>

        <div className="snippet">
          <p>
            <span>
              {" "}
              <Link to="/attorneys/brian-chase">Brian Chase</Link>, Senior
              Partner:{" "}
            </span>
            &ldquo;I went to law school knowing I wanted to be a personal injury
            attorney. I wanted my life's work to have a positive impact on other
            people's lives.&rdquo;
          </p>
        </div>
        <h2>Proving Fault and Liability</h2>
        <p>
          If you or a loved one has suffered a head injury because of someone
          else's negligence or wrongdoing, financial compensation should be
          available for your losses. In order to have a successful injury claim,
          you will have to prove how and why the accident occurred.
        </p>
        <p>
          Did your head injury result from a car accident? You will have to
          prove that the at-fault driver's negligence was a contributing factor
          in the accident. Did you suffer head trauma by falling on someone
          else's property? You will have to show that he or she was aware of a
          hazardous condition and failed to prevent it or warn you about it.
        </p>
        <p>
          Victims of head injuries may suffer from impairments of their muscles,
          speech, vision, taste or hearing depending on the area of their brain
          or head that is injured and the extent of those injuries. It is also
          common for victims to experience long- or short-term changes in their
          memory, behavior and personality.
        </p>
        <h2>How Much is Your Claim Worth?</h2>
        <p>
          It can be difficult to put a monetary value on these types of
          life-changing consequences. Injured head injury victims can seek
          compensation for damages including medical expenses, lost wages, lost
          future income, loss of livelihood, hospitalization, surgeries,
          rehabilitation, permanent injuries, disabilities, pain and suffering
          and emotional distress.
        </p>
        <p>
          Head injuries and brain trauma are common causes of disability and
          death in the United States. Head injuries can be as mild as bumps or
          contusions, but they can also result in internal bleeding, fractured
          skull bones and{" "}
          <Link to="/head-injury/traumatic-brain-injury-tips" target="new">
            traumatic brain injuries
          </Link>{" "}
          that can have lifelong consequences.
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/lraa_SAdLz8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>

        <p>
          Anyone who has suffered a head injury would be well advised to seek
          out immediate medical attention as well as legal guidance from a
          skilled California head injury lawyer.
        </p>
        <p>Head injuries can result from all types of accidents:</p>
        <ul>
          <li>Car accidents</li>
          <li>Motorcycle accidents</li>
          <li>Premise liabilities accidents</li>
          <li>Dog bites</li>
          <li>Slip and falls</li>
          <li>Defective products</li>
          <li>Wrongful and violent acts</li>
        </ul>
        <h2>Head Injury Statistics</h2>
        <p>
          Head injuries happen every single day. Whether they were the result of
          reckless or dangerous activities, or by complete accident, brain
          injuries and head injuries are irreversible and must be taken very
          seriously.
        </p>
        <p>
          According to the{" "}
          <Link to="https://www.cdc.gov/" target="new">
            U.S. Centers for Disease Control and Prevention (CDC)
          </Link>{" "}
          :
        </p>
        <ul>
          <li>
            At least 1.7 million people sustain a traumatic brain injury (TBI)
            annually.
          </li>
          <li>
            Statistics reported by the Ohio State University's Wexner Medical
            Center show that about 5 million people in the United States are
            currently living with a head injury that requires assistance with
            everyday activities.
          </li>
          <li>
            The cost of their medical needs and assistance costs the U.S. over
            $56 billion each year - a powerful statistic, which sheds light on
            the magnitude of this problem.
          </li>
        </ul>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="california brain injury lawyers"
            style={{
              backgroundImage:
                "url('/images/text-header-images/brain-injury-california-attorney.jpg')"
            }}
          >
            <h2>Types of Head Injuries</h2>
          </div>
        </LazyLoad>

        <p>
          There are many types of head injuries and they can result in serious
          symptoms and considerable medical expenses. Some of the most common
          types of head injuries include:
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/head-injury/effects-of-concussions" target="new">
              Concussions{" "}
            </Link>
            : This is when an individual suffers a blow to the head and
            experiences an instant loss of awareness or alertness. Some victims
            of concussions may lose consciousness for seconds, minutes or even
            hours. Recent studies show that those who suffer more than one
            concussion, especially as a result of sports injuries, may face
            long-term consequences such as victims of more severe traumatic
            brain injuries.
          </li>
          <li>
            <b>Skull fractures</b>: A skull fracture is a break or crack in the
            skull bone. Individuals may suffer a linear skull fracture (a break
            that does not result in bone movement), depressed skull fracture
            (part of the skull is sunken in from the trauma), a diastatic skull
            fracture (breaks along the suture lines in the skull) or basilar
            skull fracture (break in the bone from the base of the skull).
          </li>
          <li>
            <b>Intra-cranial hematoma</b>: This is when the victim suffers a
            blood clot in or around the brain. Depending on the type of hematoma
            injury, the victim may experience minor to life-threatening
            symptoms.
          </li>
          <li>
            <b>Diffuse axonal injury</b>: This is when the brain is injured due
            to a shaking movement. Diffuse axonal injuries can occur as the
            result of a car crash, a fall-related accident or due to other
            causes such as shaken baby syndrome.
          </li>
        </ul>
        <h2>Seeking Medical Attention</h2>
        <p>
          It is a common mistake for injured victims to not seek medical
          attention after being involved in a car accident or falling incident.
          When dealing with head injuries, the symptoms may not manifest right
          away. This is especially true of concussions.
        </p>
        <p>
          Victims may feel a little sore or nauseous but assume that they will
          begin to feel better on their own or that the symptoms will go away.
          Unfortunately, brain injury symptoms tend to get worse if they are not
          properly treated right away.
        </p>
        <p>
          All mild, moderate and severe symptoms should be taken seriously. If
          you bumped your head and are feeling:
        </p>
        <ul>
          <li>Confused</li>
          <li>Temporary loss of consciousness</li>
          <li>Lightheaded</li>
          <li>Unbalanced</li>
          <li>Irritable</li>
          <li>Ringing in ears</li>
          <li>Nausea</li>
          <li>Vomiting</li>
          <li>Slurred speach</li>
          <li>Delayed response to questions</li>
          <li>Fatigue</li>
        </ul>
        <LazyLoad>
          <img
            src="/images/brain-injury/head-trauma.jpg"
            alt="Head Injury"
            className="imgleft-fixed"
          />
        </LazyLoad>
        <p>
          You should see a doctor right away. If you lost consciousness,
          vomited, had a seizure, acquired a debilitating headache or have a
          severe cut to your head, you need emergency care. To learn more
          information about concussions, head injuries, symptoms, treatments and
          recovery time, visit{" "}
          <Link
            to="https://www.mayoclinic.org/diseases-conditions/concussion/symptoms-causes/syc-20355594"
            target="new"
          >
            {" "}
            mayoclinic.org
          </Link>
          .
        </p>
        <p>
          It is important that you see a medical professional for a blood test,
          x-ray, CAT scan, EEG or MRI right away.
        </p>

        <div className="snippet2">
          <p>
            <span className="blue-text">
              &ldquo;The most common causes of head injuries include car
              accidents and fall-related accidents. Other common causes include
              acts of violence, sporting accidents and workplace accidents.
              Determining the cause of the injury is an important part of the
              claim process. According to the CDC, falls account for about 35.2
              percent of head injuries, motor vehicle accidents for 17.3
              percent, struck by/against events for 16.5 percent and assaults
              for 10 percent.&rdquo;
            </span>
          </p>
        </div>
        <p></p>
        <h2>Causes of Head Injuries</h2>
        <p>
          The most common causes of head injuries include car accidents and
          fall-related accidents. Other common causes include acts of violence,
          sporting accidents and workplace accidents. Determining the cause of
          the injury is an important part of the claim process.
        </p>
        <p>According to the CDC:</p>
        <ul>
          <li>Falls account for about 35.2% of head injuries</li>
          <li>Motor vehicle accidents for 17.3%</li>
          <li>Struck by/against events for 16.5%</li>
          <li>Assaults for 10%</li>
        </ul>
        <h2>Legal Representation With a No Fee Guarantee</h2>
        <p>
          The <strong> California Head Injury Attorneys </strong>at Bisnar Chase
          will take your case with a no fee guarantee and will also protect you
          from liability. Our team has the resources and experience to take on
          some of the most difficult cases with great results. Call today to
          speak with a California brain injury lawyer with years of trial
          experience, and receive a<strong> Free Consultation </strong>and{" "}
          <strong> Case Evaluation</strong>. Call <strong> 800 561-4887</strong>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
