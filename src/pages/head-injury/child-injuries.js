// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Brain Injury Lawyers for Children - Bisnar Chase Law Firm"
        pageDescription="Call 949-203-3814 today for a Free Consultation with California Brain Injury Lawyers for Children. Top rated attorneys specializing in traumatic child injuries."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Brain Injury Lawyers for Children</h1>
        <BreadCrumbs location={location} />
        <p>
          According to the{" "}
          <Link
            to="http://www.cdc.gov/traumaticbraininjury/causes.html"
            target="_blank"
          >
            {" "}
            CDC
          </Link>
          , for every one child that dies there are 25 hospitalizations, 925
          emergency room visits and thousands of doctor office visits. Brain
          injuries to children are most often caused by motor vehicle accidents,
          suffocation, falls and drowning. Falls are the number one cause of
          serious brain injury accidents followed second by motor vehicle
          crashes.
        </p>
        <p>
          If your child has suffered a head injury as a consequence of someone
          else's carelessness, please contact the experienced California Brain
          Injury Lawyers at Bisnar Chase.
        </p>
        <p>
          We have represented injured children in recent cases. We understand
          the physical, emotional and financial impacts in such cases and are
          here to help you pursue fair and full compensation for your child's
          significant losses.
        </p>
        <p>
          <b>
            For immediate help please contact our head injury attorneys at
            949-203-3814
          </b>
        </p>
        <LazyLoad>
          <img
            src="/images/cdc-childhood-injuries.jpg"
            className="imgright-fluid"
            alt="Injury: #1 Killer of Children in the US.(infographic)"
            width="32%"
          />
        </LazyLoad>
        Immediate Steps After a TBI Accident
        <p>
          {" "}
          <Link to="/images/cdc-childhood-injuries.jpg" target="_blank">
            {" "}
            (Click here for full size infographic)
          </Link>{" "}
        </p>
        <p>
          If your{" "}
          <Link to="/head-injury/concussion-symptoms-by-gender">
            child suffers a brain injury
          </Link>{" "}
          through a fall, car crash, bicycle fall, sports-related trauma or
          other event, you should treat it very seriously.
        </p>
        <p>
          Sometimes it's just a little bump or bruise on the head, but in some
          cases head injuries can be very dangerous or even fatal if left
          untreated. So how do you know what to do?
        </p>
        <p>
          <strong> Immediate steps you should take to deal with a TBI:</strong>{" "}
        </p>
        <p>
          The{" "}
          <Link
            to="https://www.aap.org/en-us/Pages/Default.aspx"
            target="_blank"
          >
            {" "}
            American Academy of Pediatrics
          </Link>{" "}
          recommends that parents contact their child's healthcare provider for
          advice for anything more than a light bump on the head. If there are
          any doubts, call to make sure. In addition to calling, if your child
          exhibits any of the following symptoms they should be evaluated by a
          healthcare provider. The evaluation will determine if there is serious
          brain injury and what kind of treatment for the brain injury your
          child may need. Get your child evaluated if they experience:
        </p>
        <ul type="square">
          <li>vomiting</li>
          <li>seizures (convulsion)</li>
          <li>loss of consciousness after the injury</li>
          <li>a severe headache or the headache worsens with time</li>
          <li>
            changes in behavior (e.g., lethargic, difficult to wake, extremely
            irritable, or other abnormal behavior)
          </li>
          <li>
            unusual stumbling, difficulty walking, clumsiness, or lack of
            coordination
          </li>
          <li>has a hard time talking or seeing</li>
          <li>confused or slurred speech</li>
          <li>dizziness</li>
          <li>blood or watery fluid oozes form the nose or ears</li>
          <li>
            cut will not stop bleeding after applying pressure for 10 minutes
          </li>
          <li>develops a stiff neck</li>
          <li>cannot stop crying or looks sicker</li>
          <li>has weakness or numbness involving any part of the body</li>
        </ul>
        <p>
          When speaking with a health care professional, parents should try to
          describe how the injury occurred, if possible, including what the
          child was doing before the traumatic brain injury and how he/she
          responded after the injury. If you have a reason to believe that
          another adult or child intentionally injured the child, this should be
          discussed with the healthcare provider. One last piece of advice about
          something you do not need to do -{" "}
          <strong>
            {" "}
            there is no need to keep your child awake after a head injury
          </strong>
          . A child who is more rested and calm will behave more normally and
          will be easier to assess. We hope these tips will help your family
          stay safe and prevent further injury. Most head injuries are not too
          serious, but if your child experienced a severe brain injury due to
          the negligence or criminal behavior of another individual, you are
          entitled to compensation for your medical expenses and other expenses
          that occur as a result of the injury.
        </p>
        <h2>Children with Brain Injuries</h2>
        <p>
          There is a common misconception that kids are "made of rubber" and
          can't get seriously hurt, but that is far from true as serious brain
          trauma can occur at anytime. Children are susceptible to serious
          injuries -- especially traumatic brains injuries. The most common
          cause of minor head injury in children and adolescents are falls,
          motor vehicle crashes, pedestrian and bicycle accidents,
          sports-related trauma, and child abuse. With proper precautions, many
          of these head injuries can be avoided..
        </p>
        <p>
          Children are among that segment of the U.S. population that is the
          most susceptible to traumatic brain injuries. Among children, brain
          injuries are one of the most frequent causes of death and disability.
          Children may be left with lifelong disabilities as a result of these
          traumatic injuries.
        </p>
        <p>
          They may need rehabilitation, medical equipment and even 24/7 nursing
          care, which can all add up to millions of dollars over the child's
          lifetime. The biggest concern that parents have is to ensure that
          their dependent or disabled children are taken care of after they are
          gone. If your child has suffered traumatic brain injuries as a result
          of someone else's negligence or wrongdoing, it is important that you
          explore your legal rights and options by contacting an experienced{" "}
          <Link to="/head-injury">head injury attorney</Link>.
        </p>
        <h2>Child Brain Injury Statistics</h2>
        <p>
          According to the U.S. Centers for Disease Control and Prevention
          (CDC), the two age groups that are at greatest risk for TBI are 0 to 4
          and 15 to 19.
        </p>
        <p>Each year:</p>
        <ul>
          <li>
            Approximately 62,000 children 0 to 19 years of age require
            hospitalization after suffering a brain injury in a car accident,
            fall, sporting accident, an act of violence or some other traumatic
            event.
          </li>
          <li>
            About 564,000 children visit the emergency room to obtain treatment
            for a TBI.
          </li>
          <li>
            TBI results in an estimated 2,685 deaths, 37,000 hospitalizations
            and 435,000 emergency department visits for children ages 0 to 14
            each year.
          </li>
          <li>Falls are the leading cause of TBI for children.</li>
          <li>
            Listen to{" "}
            <Link
              to="http://www.cdc.gov/injury/podcast.html"
              target="_blank"
              name="podcasts on preventing childhood injuries."
            >
              podcasts{" "}
            </Link>{" "}
            from the CDC on injury prevention
          </li>
        </ul>
        <h2>Brain Injury Symptoms</h2>
        <p>
          The brain is an incredibly complex organ. Depending on the site of the
          injury and the nature of the brain damage, injured victims can
          experience a range of symptoms. Furthermore, it is often difficult to
          predict if victims will suffer from their impairments for the rest of
          their lives or if the symptoms will improve with proper treatment.
          Each case is different, but some young victims of brain injuries are
          never able to fully recover.
        </p>
        <p>
          <b>Physical impairments</b>: Children who sustain a head injury can
          suffer speech, vision and hearing impairments. Many young brain injury
          victims suffer from headaches, loss of motor functions, issues with
          spasticity of muscles, paralysis, seizures, balance issues and
          fatigue.
        </p>
        <p>
          <b>Cognitive issues</b>: The location and severity of the trauma often
          affects the degree to which the victim will suffer from cognitive
          impairments. While some victims suffer only temporary mental issues,
          others face a lifetime of challenges. Examples of cognitive impairment
          include short-term memory loss, shorter attention spans, perception
          issues, struggles with communication, and difficulties writing,
          reading, concentrating and planning.
        </p>
        <p>
          <b>Emotional impairments</b>: The emotional impairments that can
          result from a brain injury may surface shortly after the accident or
          hours, days or weeks later. Common emotional impairments suffered by
          young victims of head injuries include depression, anxiety,
          restlessness,struggling to control emotions,lack of motivation, mood
          swings self-centeredness and lowered self-esteem.
        </p>
        <h2>Recovering from a Brain Injury</h2>
        <p>
          One of the major differences between brain injuries suffered by
          children and adults is the way in which their brains recover. It is a
          common misconception that the developing brain of a child is better
          suited to recover from trauma compared to the adult brain. In reality,
          a brain injury can have a more devastating impact on a child because
          it has a serious impact on the child's growth and development.
          Children who sustain serious TBIs may experience delayed effects such
          as cognitive impairments that only become apparent as the child ages.
          Many young TBI victims struggle to learn and think while others
          develop social issues that make it challenging for them to have a
          normal life.
        </p>
        <h2>Financial Impact on Families</h2>
        <p>
          If your child has sustained a brain injury, your financial situation
          could change drastically. Immediately following the accident, you may
          be facing expenses related to emergency room visits, diagnostic tests,
          hospitalization, medical devices, medications and surgical procedures.
          In the months and years to follow, rehabilitative therapy may be
          needed to help your child cope with physical and mental challenges.
          Some victims require round-the-clock care in the long term, or
          possibly, for the rest of their lives. Health insurance plans may not
          cover all of these expenses.
        </p>
        <h2>Seeking Compensation</h2>
        <p>
          When brain injuries are caused by someone else's negligence or
          wrongdoing, injured victims can seek compensation for their injuries,
          damages and losses. Examples of potentially liable parties in child
          brain injury cases include:
        </p>
        <ul>
          <li>Negligent drivers</li>
          <li>Doctors or hospitals that fail to provide reasonable care</li>
          <li>
            Manufacturers of defective products such as automobiles or child
            safety seats
          </li>
          <li>Negligent property owners</li>
          <li>Perpetrators of violent crimes</li>
          <li>
            A governmental agency that allowed a dangerous condition to exist on
            public property
          </li>
        </ul>
        <p>
          Injured victims in such cases can compensation from at-fault parties
          for damages including medical expenses; lost wages for parents or
          caregivers who have to take time off work or quit their jobs to care
          for the injured child; hospitalization and rehabilitation costs; pain
          and suffering; loss of life's enjoyment; lost future income; cost of
          long-term care; and emotional distress.
        </p>
        <p>
          <b>
            Contact our brain injury lawyers for immediate help. 949-203-3814
          </b>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
