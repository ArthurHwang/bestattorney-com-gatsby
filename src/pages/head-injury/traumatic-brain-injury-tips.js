// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import { DefaultGreyButton } from "../../components/utilities"
import { FaRegArrowAltCircleRight } from "react-icons/fa"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: { eq: "brain-injury/brain-injury-x-rays.jpg" }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Traumatic Brain Injury FAQS - Bisnar Chase"
        pageDescription=" A Traumatic Brain Injury can be life-changing. You may not know what to do or who to turn to. Call 1-800-561-4887 for a Free Consultation from top-rated brain injury attorneys."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Traumatic Brain Injury FAQs</h1>
        <BreadCrumbs location={location} />
        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Traumatic brain injury FAQ"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          According to the{" "}
          <Link to="https://www.cdc.gov/" target="_blank">
            {" "}
            U.S. Centers for Disease Control and Prevention
          </Link>{" "}
          (CDC), at least 1.7 million traumatic brain injuries occur in the
          United States every year. Traumatic brain injuries or TBIs are a
          contributing factor in almost a third of all injury-related deaths.
          Not all TBIs result in fatal injuries.
        </p>
        <p>
          Approximately 75 percent of all TBIs are concussions or other forms of
          milder TBI. All brain injury victims do not end up with the same
          outcome though. Symptoms can vary depending on the impact from the
          accident. While there are differences in symptoms, there are two
          primary TBI's that are diagnosed frequently.
        </p>
        <p>
          <strong> Two types of traumatic brain injuries:</strong>
        </p>
        <p>
          <strong> Closed head injury</strong>: A closed head injury generally
          is the result of an event like a car accident. In this scenario, the
          head whips back and forth or side to side, hitting against the inside
          of the skull at a very high rate of speed as it does. This knocking
          around of the brain causes tissue to be bruised and blood vessels to
          be torn within specific areas of the brain. The brain regions that are
          most often affected by a closed head injury are the frontal and
          temporal lobes. The
          <em>
            {" "}
            <Link to="https://www.spinalcord.com/frontal-lobe" target="_blank">
              {" "}
              frontal lobe{" "}
            </Link>
          </em>
          regulates decision-making, problem solving, movement, and emotions,
          while the temporal lobe controls memory, hearing, speech, and the
          ability to learn.
        </p>
        <p>
          <strong> Open head injury</strong>: Individuals experience less
          impairment than a patient with closed head injury at times. However,
          even though there are fewer detrimental after effects from open head
          injury, they can be just as serious as those that result from closed
          head injuries. It all depends on how much destruction was caused by
          the object as it penetrated the brain.
        </p>{" "}
        <Link to="/head-injury/traumatic-brain-injury-tips#header1">
          <DefaultGreyButton className="btn btn-primary active nav-button">
            4 Common Causes of Brain injuries <FaRegArrowAltCircleRight />
          </DefaultGreyButton>
        </Link>{" "}
        <Link to="/head-injury/traumatic-brain-injury-tips#header2">
          <DefaultGreyButton className="btn btn-primary active nav-button">
            Acute Traumatic Brain Injury Treatment <FaRegArrowAltCircleRight />
          </DefaultGreyButton>
        </Link>{" "}
        <Link to="/head-injury/traumatic-brain-injury-tips#header3">
          <DefaultGreyButton className="btn btn-primary active nav-button">
            Choosing the Right Rehabilitation Center{" "}
            <FaRegArrowAltCircleRight />
          </DefaultGreyButton>
        </Link>{" "}
        <Link to="/head-injury/traumatic-brain-injury-tips#header4">
          <DefaultGreyButton className="btn btn-primary active nav-button">
            8 Tips to Help You or Your Family Member Cope With a TBI{" "}
            <FaRegArrowAltCircleRight />
          </DefaultGreyButton>
        </Link>{" "}
        <Link to="/head-injury/traumatic-brain-injury-tips#header5">
          <DefaultGreyButton className="btn btn-primary active nav-button">
            The Cost of Brain Injuries <FaRegArrowAltCircleRight />
          </DefaultGreyButton>
        </Link>
        <p></p>
        <div id="header1"></div>
        <h2>4 Common Causes of Brain injuries</h2>
        <p>
          The brain is a complex organ, which is in charge of everything we
          think, do, say and feel. It controls all vital body functions. When
          head trauma results in damage to the brain, the resulting symptoms can
          be devastating and potentially lifelong.
        </p>
        <p>
          <strong> 1. Slip and Falls</strong>: Falls are the leading cause of
          TBI in the United States. In fact, falling accidents are responsible
          for 35.2 percent of all brain injuries. Falls cause about half of all
          TBIs among children under the age of 15 and 61 percent of all TBIs
          suffered by the elderly.
        </p>
        <p>
          <strong> 2. Car Accidents</strong>: Motor vehicle accidents are the
          cause of 17.3 percent of all TBIs making it the second most common
          cause of non-fatal brain injuries. Traffic-related fatalities make up
          the largest percentage of fatal TB-related deaths with 31.8 percent.
        </p>
        <p>
          <strong> 3. Hit by an object</strong>: Victims who are struck by
          objects or equipment make up 16.5 percent of TBI incidents.
        </p>
        <p>
          <strong> 4. Assault</strong>: Another 10 percent of{" "}
          <Link to="/premises-liability/assault">
            brain injuries result from assault
          </Link>
          .
        </p>
        <p>
          A traumatic brain injury is often considered a catastrophic injury
          because victims never return to their pre-accident stage. A number of
          brain injury victims may make a significant recovery, but it may never
          be a "complete" recovery.
        </p>
        <div id="header2"></div>
        <h2>Acute Traumatic Brain Injury Treatment</h2>
        <p>
          This treatment, administered by both emergency rescue personnel and
          hospital staff, is designed to stabilize the patient, and to stop the
          progression of damage to the brain. It's objective is to minimize the
          effects of the injury immediately.
        </p>
        <p>
          <strong>
            {" "}
            The following states the process of acute TBI brain treatment
          </strong>
          :
        </p>
        <p>
          <strong> CPR (Cardiopulmonary resuscitation)</strong>: CPR or
          Cardiopulmonary resuscitation may be needed while the patient is being
          taken to the hospital. Emergency rescuers may also have to unblock
          airways, aid breathing, and keep blood circulating.
        </p>
        <p>
          <strong> Attend to the damaged tissue inside of the brain</strong>:
          Injured brain tissue typically swells, causing it to expand and take
          up available space in the skull. There may also be blood pools or
          clots from the blood that flowed out of the blood vessels that were
          torn during the accident. These will also use up available space in
          the skull. The swelled tissue and excess blood cause a pressure build
          up inside the brain that squeezes the tissue and blood cells that
          supply food and oxygen to the brain.
          <em>
            If the brain cells' supply of these items is cut off, serious damage
            can occur
          </em>
          . To prevent this from happening, doctors may intravenously administer
          the diuretic mannitol to decrease the amount of fluid in the tissue
          and increase urine output.
        </p>
        <p>
          <strong> Surgery</strong>: A surgical procedure may be performed if a
          blood clot causes increased pressure within the skull. Some clots have
          to be removed;others cannot be because of the damage that will be done
          if they are. Subcultural hematomas, (bleeding into the space between
          the brain cover and the brain itself), and intracerebral hemorrhages,
          (bleeding resulting from ruptured blood vessels), may also increase
          pressure, which may mean that the patient will need surgery.
        </p>
        <div id="header3"></div>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="head injuries"
            style={{
              backgroundImage:
                "url('/images/brain-injury/brain-injury-treatment.jpg')"
            }}
          >
            <h2>Choosing the Right Rehabilitation Center</h2>
          </div>
        </LazyLoad>
        <p>
          Comprehensive evaluation, treatment and care are critical for recovery
          in brain injury cases. While physical impairments caused by brain
          injuries can hinder functional independence, the behavioral,
          cognitive, emotional and personality changes that are associated with
          a brain injury may lead to even more complex problems.
        </p>
        <p>
          For patients with severe TBIs, the goal of rehabilitation is to
          improve the survivor's ability to function, both at home and in the
          community, even when full normalcy isn't possible. Therapists will
          help the patient adapt to his disabilities or help make modifications
          to the home so that day-to-day activities become as straightforward
          and trouble-free as possible.
        </p>
        <p>
          Once the patient is stabilized and there is no danger of additional
          brain damage, subacute treatment is provided to determine if there are
          any further complications, help the patient's recovery, and prevent
          any
        </p>
        <p>
          <strong> The facility staff will be on the alert for</strong>:
        </p>
        <ul>
          <li>bedsores</li>
          <li>muscle contractions</li>
          <li>infections that need treatment, in addition to</li>
          <li>fluid accumulation in the brain that may require surgery.</li>
        </ul>
        <p>
          <em>
            Regaining brain function requires the assistance of the physical,
            occupational, and speech therapists, nurses, neuropsychologists, and
            neurologists on staff at the facility.
          </em>{" "}
          They not only monitor recovery progress, but they also help patients
          learn new ways to perform routine tasks if they no longer have the
          physical ability to perform them as they did in the past.
        </p>
        <p>
          <em>
            Initially, many patients experience poor balance, lack of
            coordination, or cognitive impairments that make them vulnerable to
            additional injuries.{" "}
          </em>
          Although they may be agitated and restless, they cannot really be left
          to move around on their own. It is the job of the rehabilitation staff
          to keep them calm and safe, until such time as they are able to get
          around safely. At this point, the patient is usually ready to be
          discharged.
        </p>
        <p>
          Some patients who are released from the rehabilitation facility will
          still have symptoms that remain with them for the rest of their lives,
          and will require ongoing treatment. One typical long-term symptom is
          lack of muscle tone, which would mean that the patient would continue
          receiving physical therapy, or undergo minor corrective surgery.
        </p>
        <p>
          Individual patients follow different paths in their treatment,
          depending on the type and severity of the TBI and the specific needs
          of the patient.
        </p>
        <p>
          Each person's rehabilitation process is unique to his or her
          circumstances and the type of injury he or she has suffered.
          Rehabilitation for brain injury victims can last anywhere from a few
          months to years.
        </p>
        <p>
          Seizures and headaches may also continue, requiring that the patient
          remain on medication to control them. If the individual suffers from
          depression, anxiety, and behavioral problems, they will most likely be
          treated with a combination of medication and psychotherapy.
        </p>
        <div id="header4"></div>
        <h2>8 Tips to Help You or Your Family Member Cope With a TBI</h2>
        <p>
          Whether it is you or a loved one it can be very challenging and
          difficult to cope with, especially when you do not know what the long
          term effects or outcome will be. Both the person with the injury and
          the family often face difficulties they are completely unprepared for.
          There are however, practical ways of coping with traumatic brain
          injury--strategies that families can use to support and empower
          someone they love with this condition.
        </p>
        <LazyLoad>
          <img
            src="/images/brain-injury/concussion3.jpg"
            className="imgright-fixed"
            alt="concussion"
          />
        </LazyLoad>
        <ol>
          <li>
            <strong> Patience is a virtue</strong>: Every family with a member
            who copes with traumatic brain injury will need to be patient, with
            themselves and the person who suffers the consequences. Recovery
            from TBI is usually slow, painful and difficult.
          </li>
          <li>
            <strong> Knowledge is power</strong>: The more you know about TBI
            symptoms and the more you understand how it affects a person's
            behavior, emotions, memory, ability to learn and even their
            personality is going to help you. Remember, TBI can change a
            person's personality and when you understand how and why these
            changes occur, you will be able to better cope.
          </li>
          <li>
            <strong> You're not alone</strong>: There are lots of different
            support groups for people with TBI and their families. Support
            groups can provide a space to talk about our feelings, our worries,
            concerns, anxiety, fears and frustration. These emotions are common.
            It's hard to see someone we love suffer and struggle. TBI can put a
            strain on family relationships. Support groups let us vent and they
            help us to understand the process. Try not to do everything
            yourself. There are many TBI experts--doctors, counselors,
            psychologists, neuro-psychologists and other professionals who can
            help you come to terms with the situation and plan for the future.
          </li>
          <li>
            <strong> Good planning and time management go a long way</strong>:
            People with TBI often have difficulties with time management,
            organizing themselves and basic planning. If the family is
            disorganized around them, life will become difficult. Using
            practical tools like a family calendar, personal planning tools and
            dividing up duties and responsibilities, making sure everyone knows
            what is expected of them will help the family and the person with
            TBI.
          </li>
          <li>
            <strong> Communication is key</strong>: When a family stops
            communicating about important issues, problems can set in. Talk
            about the issues bothering people. Discuss your feelings, your
            concerns, your worries and your hopes for the future. Let the family
            member with TBI know that the family is open to talking about the
            problems they face.
          </li>
          <li>
            <strong> Never stop having hope:</strong> People with TBI can have a
            future. People with TBI can still return to work. Some do but some
            don't. TBI is a manageable condition and you can cope with it. Let
            the family member know you care and you still believe that even with
            TBI there is a future.
          </li>
          <li>
            <strong> Consider family counseling</strong>: Many families find TBI
            family counseling extremely helpful. It lets people vent their
            feelings, express concerns, discuss worries, explain fears and
            frustrations. A person with TBI will likely need counseling in order
            to help them understand and cope.
          </li>
          <li>
            <strong>
              {" "}
              Life goes on. Don't let TBI become the family focus
            </strong>
            : If it does, the family will have trouble coping because other
            issues and problems will be ignored. The family goes on and
            continues to love and support each other. The family member with TBI
            must learn to let others have their troubles and successes, just as
            they do.
          </li>
        </ol>
        <p>
          These are just a few <strong> traumatic brain injury tips</strong>{" "}
          that can help families coping with TBI. There are numerous books,
          articles and stories available to inspire. Some of these books are
          written by <strong> TBI survivors</strong> and/or their families.
          People coping with TBI can get the support they need through
          information, community services and the support networks they develop
          for themselves.
        </p>
        <p>
          Many times we use a strategy but somehow we forget it down the road.
          So, when you come across a technique, idea or strategy that
          helps--write it down so that you know success is possible. Each small
          success leads to bigger success and that leads to a path of hope.
        </p>
        <div id="header5"></div>
        <h2>The Cost of Brain Injuries</h2>
        <p>
          Victims of traumatic brain injuries can suffer from impairments of
          their cognitive, emotional and physical functions. It is not only the
          victims who suffer long-term consequences, but also their families
          that are often faced with mounting medical bills and rehabilitation
          costs.
        </p>
        <LazyLoad>
          <img
            src="../images/brain-injury/physical-therapy-brain-injury.jpg"
            width="100%"
            alt="Catastrophic brain injury"
          />
        </LazyLoad>
        <p>
          Insurance companies tend to drop coverage for brain injury victims
          after a certain period of time and families may be left in the dark as
          they try to come up with funds for their loved one's continued
          treatment and care. A number of patients do not get the rehabilitative
          care they need as a result of their family's financial constraints.
        </p>
        <p>
          The severity of a brain injury can be devastating not just physically,
          but emotionally as well. Our
          <strong>
            {" "}
            <Link to="/head-injury" target="_blank">
              {" "}
              Brain Injury Attorneys{" "}
            </Link>
          </strong>{" "}
          have years of experience handling head trauma cases. We work many of
          these cases on a contingent fee basis which means that you only pay us
          if we successfully win your case. Our payment is a preset amount of
          your award only and we always factor that into any agreed upon
          settlement.
        </p>
        <p>The cost of brain injury treatment can be exorbitant.</p>
        <ul>
          <li>
            Direct and indirect costs of traumatic brain injury in the U.S. have
            been estimated as $48.3 billion.
          </li>
          <li>
            The lifetime costs for one person surviving a severe TBI can exceed
            $4 million.
          </li>
          <li>
            An estimate of medical and non-medical needs including home
            modifications, health insurance and vocational rehabilitation per
            TBI averages $151,587.
          </li>
          <li>
            Acute rehabilitation costs of survivors of severe TBIs could reach
            $1,000 a day.
          </li>
          <li>
            Medical costs are highest for those who do not survive - an average
            of $454,717 per brain injury fatality.
          </li>
        </ul>
        <p>
          If you need legal assistance for a brain injury accident, please
          contact Bisnar Chase for a free case review.
        </p>
        <p>
          The personal injury attorneys at Bisnar Chase specialize in traumatic
          brain injury cases. We have been successfully winning settlements for
          more than <strong> 40 years</strong>.
        </p>
        <p>
          If you believe you have a case and would like your case evaluated by a
          professional brain injury lawyer, please contact us at{" "}
          <strong>
            {" "}
            <span>(949) 203-3814</span>
          </strong>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
