// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Concussion Symptoms by Gender | Female Concussion | Male Concussion"
        pageDescription="A recent study shows concussion symptoms may differ between girls and boys who play sports. There is ongoing research to determine if the type of sport is relevant."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Concussion Symptoms May Differ With Gender</h1>
        <BreadCrumbs location={location} />
        <p>
          R. Dawn Comstock, PhD, a professor at Ohio State University and
          co-author of the <i>the Journal of Athletic Training</i>, a study
          about brain injury in girls and boys, stated that male and female
          athletes may experience different types of{" "}
          <Link to="/head-injury/effects-of-concussions">concussion </Link>{" "}
          symptoms and present differently to health care professionals and
          clinicians. In response athletic trainers and parents and coaches all
          need to be aware of higher rates of nontraditional symptoms and "when
          in doubt, sit them out."
        </p>
        <h2>What are the Statistics and Symptoms?</h2>
        <p>
          More than 95% of all athletes of both sexes reported having headaches
          during their traumatic brain injury treatment. This was by far the
          main primary symptom and secondary symptoms tended to differ quite a
          bit in both sexes.
        </p>
        <LazyLoad>
          <img
            src="/images/brain-injury/hockey.jpg"
            className="imgright-fixed"
            alt="hockey injury"
          />
        </LazyLoad>
        <p>
          More boys reported being confused or disoriented after the head
          injury. Many more boys also reported having amnesia as part of the
          concussion.
        </p>
        <p>
          Girls were much more likely to be sensitive to noise or to feel
          drowsy.
        </p>
        <p>
          A surprising finding from the study was the loss of consciousness,
          previously considered to be a necessary component of a concussion, was
          actually one of the least reported symptoms.
        </p>
        <p>
          Comstock said, "It's kind of an old wives' tale now, but once it was
          thought that you didn't have a concussion unless you had a loss of
          consciousness. That used to be a widespread belief, but we know that's
          just not true. It's exactly the opposite." According to Comstock,
          because the loss of consciousness is no longer a sign of a concussion,
          "these gender differences become all the more important."
        </p>
        <p>
          <LazyLoad>
            <img
              src="/images/brain-injury/soccer.jpg"
              className="imgleft-fixed"
              alt="soccer injury"
            />
          </LazyLoad>
          The study does not answer why the differences exist between the sexes
          during traumatic brain injury recovery. Concussion researcher Gerard A
          Goia, chief of pediatric neuropsychology at Children's National
          Medical Center in Washington, DC, said that one of the study's
          limitations is that the reporting system didn't explain about how the
          injuries occurred.
        </p>

        <p>
          <i>
            "The presence of increased amnesia and confusion, two early injury
            characteristics, in the males suggest that the injuries between
            males and females may have been different,"
          </i>{" "}
          he said.
        </p>
        <h3>
          Injuries between gender may be different, but are the injuries also
          related to the type of sport?
        </h3>
        <p>
          According to Comstock, future studies will address this theory. For
          example, preliminary data suggests, for instance, that football
          players tend to get hit on the front of the head, while girls who play
          soccer or basketball often suffer a blow to the side of the head.
        </p>

        <p>
          If you or a loved one experiences a concussion or other severe brain
          injury due to the negligence or criminal behavior of another
          individual and you need legal help, contact one of our{" "}
          <Link to="/head-injury/brain-trauma">
            {" "}
            experienced brain injury lawyers
          </Link>
          .
        </p>
        <p align="center">Call today for free case evaluation.</p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
