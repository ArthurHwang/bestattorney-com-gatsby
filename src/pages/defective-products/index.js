// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "text-header-images/california-product-liability-lawyers.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="California Product Liability Lawyers - Bisnar Chase"
        pageDescription="California Product Liability Lawyers at Bisnar Chase have Won over $500 Million & have over 40 years of experience representing plaintiffs. See if you are entitled to compensation from an injury related to defective or dangerous products. 96% success rate."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>California Product Liability Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="california product liability lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            The <strong>California Product Liability Lawyers </strong>at{" "}
            <strong>Bisnar Chase </strong>have been winning product liability
            cases for over <strong>40 years</strong>, establishing a
            <strong> 96% success rate</strong> and winning hundreds of millions
            including many product liability verdicts.
          </p>
          <p>
            Our Product Liability Lawyers will fight for your right to
            compensation and shield you from liability along the way.
          </p>
          <h2>Design &amp; Manufacturing Flaws</h2>
          <p>
            Companies who place goods or services in the hands of consumers and
            create an environment for injury can be sued under{" "}
            <Link
              target="_blank"
              to="https://www.law.cornell.edu/wex/products_liability"
            >
              {" "}
              state laws for manufacturing and design defects
            </Link>
            . Typical products liability claims are those where the design of
            the product was expected to be safe but turned out otherwise causing
            injury or death. While not as common, marketing defects also applies
            to product liability lawsuits.
          </p>
          <p>
            The chain from the creation of product can include all parties
            involved from the assembler, manufacturer or store owner. Typically,
            product liability lawsuits are based on{" "}
            <Link
              to="http://topics.law.cornell.edu/wex/strict_liability"
              target="_blank"
            >
              {" "}
              strict liability
            </Link>{" "}
            and negligence.
          </p>
          <p>
            Most design flaws that cause injury are tables, heavy furniture,
            dressers or items that have a choking hazard to children. There are
            many different types of product liability cases including{" "}
            <Link to="/defective-products/failure-to-warn">
              {" "}
              failure to warn
            </Link>{" "}
            and each one needs to be explored by an experienced product
            liability lawyer who has spent years working these types of cases.
          </p>
          <h2>Winning Product Liability Cases</h2>
          <p>
            {" "}
            <Link to="/attorneys/brian-chase">Brian Chase</Link>, a top rated
            California personal injury lawyer talks about taking on the big guns
            of fortune five hundred companies and seeking justice for you. Our
            experience in product liability and defective or dangerous products
            allows us to be at the top of our game from the start.
          </p>
          <p>
            There is no learning curve when you've specialized in these types of
            cases for over 4 decades.
          </p>
          <p>
            Brian doesn't fear taking on Goliath. He's been protecting the
            rights of clients and pursuing negligent corporations for decades
            throughout California.
          </p>
          <p>
            Brian specializes in a lot of different defective product cases
            including auto defects, tobacco, airbags, tires, car seats,{" "}
            <Link to="/defective-products/e-cig-injury-lawyer" target="new">
              {" "}
              dangerous e-cigs
            </Link>{" "}
            and lawnmowers. Recently,{" "}
            <Link to="/pharmaceutical-litigation/talcum-powder" target="new">
              {" "}
              talcum powder
            </Link>{" "}
            has been shown to cause ovarian cancer after long term exposure, and
            we have started taking these cases too.
          </p>
          <p>
            If you've been harmed by a defective or dangerous product, please
            contact our legal team for a free consultation. Call{" "}
            <strong>1-800-561-4887</strong>.
          </p>
        </div>
        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/VBC0aa6eUEM?rel=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Negligence in Product Safety</h2>
        <p>
          {" "}
          <Link to="/">Bisnar Chase</Link> products liability attorneys focus a
          large part of their practice on representing individuals who have been
          seriously injured or killed as the result of defective products or
          negligence placed into consumer's hands. Every year thousands of
          people are injured, maimed or killed by a dangerous or defective
          product. Many times, these products should have never been in the
          marketplace in the first place because there was potential for injury
          from manufacturing or design flaws at the onset. Since there is no
          federal product liability law, most cases are based on state laws
          where the plaintiff resides or where the product was created,
          manufactured, marketed or otherwise sold.
        </p>
        <h2>Top 10 Most Common Defective Product Types</h2>
        <p>
          There are many different types of defective products and liabilities
          among consumer and commercial goods.
        </p>
        <ol>
          <li>
            <strong>Auto Defects - </strong>Everything from{" "}
            <Link
              to="/auto-defects/ford-carbon-monoxide/ford-explorer-carbon-monoxide-faq"
              target="new"
            >
              {" "}
              carbon monoxide poisoning
            </Link>
            , brakes going out,{" "}
            <Link
              to="/auto-defects/sudden-unintended-acceleration"
              target="new"
            >
              {" "}
              sudden unintended acceleration
            </Link>
            , tires blowing out,{" "}
            <Link to="/auto-defects/defective-seatbelts" target="new">
              {" "}
              seatbelt failure
            </Link>{" "}
            and more, <strong>Bisnar Chase </strong>is a trusted and reputable
            law firm when it comes to auto defects.
          </li>
          <li>
            <strong>Children's Products</strong> - When buying toys and products
            for your children, you trust that they will bring no harm, but when
            they do, somewhere along the line something was overlooked and
            caused a serious problem that needs to be acknowledged. Let us hold
            those accountable so that it does not happen again to someone else.
          </li>
          <li>
            <strong>Household Appliances</strong> - Everyday tasks in your home
            shouldn't have to be dangerous; household appliances can be very
            heavy, a fire risk and many more potentially deadly hazards that the
            manufacturer needs to make sure is at its safest possible quality,
            and if it is not, should not be released to the public for sale or
            distribution.
          </li>
          <li>
            <strong>Furniture</strong> - Recently, dressers and other furniture
            items have been causing the death of children from{" "}
            <Link
              to="/blog/more-than-one-million-dressers-recalled-for-safety-and-stability-issues"
              target="new"
            >
              {" "}
              dressers falling on children and crushing them
            </Link>
            . These are not the only potentially dangerous items available.
          </li>
          <li>
            <strong>Medical Devices</strong> - When intending to save lives and
            make living conditions easier, you should not have to worry about if
            a medical product is going to worsen your condition.
          </li>
          <li>
            <strong>Chemicals &amp; Cleaning Products</strong> - Dealing with
            household and exterior cleaners of any kind can be extremely
            hazardous. Companies that manufactured and distributed cleaning
            products did not have to list the ingredients inside their kitchen
            sprays, disinfectants, soaps, detergents, etc. Many of these harmful
            ingredients in which we do not know they are even there, have the
            ability to cause allergic reactions, illness, severe irritation,
            cancer, birth defects and many more side effects. To learn more
            visit{" "}
            <Link
              to="https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB258"
              target="new"
            >
              {" "}
              about Senate Bill No. 258
            </Link>
            .
          </li>

          <li>
            <strong>Contaminated Food</strong> - We eat the food we eat and
            drink the drinks we drink without too much thought of if it is going
            to make us violently ill or possibly kill us. Some of us worriers
            do, but the bottom line is, we shouldn't have to worry about the
            possibilities of becoming ill or dying, when purchasing food or
            drinks from manufacturers and or companies that make and sell them
            to the public. The food and beverage industry go through rigorous
            testing, must obey strict guidelines, laws and regulations, and when
            a contaminated food issue comes up, it is usually due to negligence
            or wrong doing.
          </li>
          <li>
            <strong>Industrial Equipment &amp; Machinery</strong> - When working
            in positions that require big, heavy, fast, loud and dangerous
            equipment and machines, safety is key. Organizations like{" "}
            <Link to="https://www.osha.com/" target="new">
              {" "}
              OSHA
            </Link>{" "}
            work tirelessly to ensure that proper protocol is required and met
            by businesses and companies so their workers are kept safe when on
            the job and not forced into potentially catastrophic situations.
          </li>
          <li>
            <strong>Sport Equipment - </strong>Sports in general are hazardous
            enough, with a high amount of sports related injuries reported every
            day. Sporting equipment is an essential factor of many sports around
            the globe. Sporting equipments like baseball gloves, weight-lifting
            equipment, water-sport equipment, and so forth, must meet specific
            guidelines depending on their area. Athletes and bystanders count on
            the efficiency of these products to ensure overall safety, again,
            for athletes and those observing.
          </li>
          <li>
            <strong>Jewelry, clothes and fashion accessories</strong> - Every
            day people get dressed for work, school, errands, events,
            recreational activities and so on - The tiniest aspects can be
            overlooked to the expected normalities and this is where the biggest
            mistakes occur.
          </li>
        </ol>
        <p>
          If you have been injured in result of a defective product, please call
          our highly skilled and experienced team of California Product
          Liability Attorneys. To receive a<strong> Free Consultation </strong>
          and <strong>Case Evaluation</strong>, call us at{" "}
          <strong>(800) 561-4887</strong>.
        </p>
        <h2>Act Quickly To Obtain Liability Compensation</h2>
        <p>
          <LazyLoad>
            <img
              src="/images/product-recalls.jpg"
              alt="product liability lawyer"
              className="imgleft-fixed"
            />
          </LazyLoad>
          Victims of defective products have a
          <strong> limited amount of time to pursue compensation</strong> for
          their injuries.
        </p>
        <p>
          time-restraint is known as the <strong>statute of limitations</strong>{" "}
          and varies greatly depending on the victim's geographic location as
          well as the state in which their defective product was manufactured.
        </p>
        <p>
          If the time allowed by law to pursue a claim for damages is exceeded,
          it is almost an impossibility to obtain compensation for medical
          expenses or pain and suffering arising as a result of manufacturer
          negligence.
        </p>
        <p>
          <strong>Call 800-561-4887</strong> following a defective product
          injury. The expert staff at <strong>Bisnar Chase</strong> will provide
          you with a free, no obligation consultation which will give you all of
          the information that you will need to start your case out on the right
          foot.
        </p>
        <h2>Defective Product Checklist</h2>
        <ul>
          <li>
            <strong>A design defect or flaw</strong>
          </li>
          <li>
            <strong>A manufacturing defect or flaw</strong>
          </li>
          <li>
            <strong>Inadequate instructions or appropriate warnings</strong>
          </li>
        </ul>
        <h2>Three Keys To Establish A Product Liability Case</h2>
        <ol>
          <li>The product was defective.</li>
          <li>
            The defect existed prior to the manufacturer releasing the product.
          </li>
          <li>The defect caused your injuries and or damages.</li>
        </ol>
        <p>
          Typically a product liability case will hinge on manufactured
          products, defective design, and failure to warn.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Defective Medical Devices"
            style={{
              backgroundImage:
                "url('/images/text-header-images/hip-replacement.jpg')"
            }}
          >
            <h2>What About Defective Medical Devices?</h2>
          </div>
        </LazyLoad>

        <p>
          There are a number of ways a medical device can fail such as hip
          replacements or vaginal mesh implanting. When a medical device does
          fail, it can leave victims in painful or fatal circumstances often
          causing lifelong symptoms.
        </p>
        <p>
          If you are suffering complications due to a defective medical device,
          please contact our office immediately by calling{" "}
          <strong>1-800-561-4887</strong>. We will advise you on whether or not
          we can take your medical defect case.
        </p>
        <p>
          There are some drugs and devices that we do not represent after a
          period of time. Call us to discuss your case, or find out more about
          the cases we take at{" "}
          <Link to="/medical-devices" target="new">
            {" "}
            on our defective medical devices page
          </Link>
          .
        </p>
        <p>
          We also represent clients who have experienced serious damaging
          side-effects from unsafe prescription drugs. For a list of defective
          drugs please visit our{" "}
          <Link to="/pharmaceutical-litigation" target="new">
            {" "}
            pharmaceutical litigation
          </Link>{" "}
          page.
        </p>

        <LazyLoad>
          <div
            className="text-header content-well"
            title="Auto Defect Cases"
            style={{
              backgroundImage:
                "url('/images/text-header-images/crash-dummy.jpg')"
            }}
          >
            <h2>What About Auto Defect Cases?</h2>
          </div>
        </LazyLoad>

        <p>
          The committed and experienced{" "}
          <strong>California Product Liability Lawyers </strong>at Bisnar Chase
          have been involved in many auto defect cases winning over{" "}
          <strong>$500 Million</strong>.
        </p>
        <p>
          Brian Chase has spent a large amount of his career going after
          deceptive automakers and specifically those who have catastrophically
          harmed consumers. There are many dangerous auto defects in cars that
          are still on the roads today.
        </p>
        <p>
          For more information on a{" "}
          <strong>California product liability</strong> or{" "}
          <strong>defective product case</strong>, please call our attorneys at{" "}
          <strong>800-561-4887</strong> for a free case review. Serving Southern
          California since 1978.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
