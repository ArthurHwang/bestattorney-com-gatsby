// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Defective Drug Products That Contain PPA"
        pageDescription="Call 949-203-3814 for highest-rated drug recall and product liability attorneys, serving Los Angeles, San Bernardino, Newport Beach, Orange County & San Francisco, California.  Specialize in catastrophic injuries, car accidents, wrongful death & auto defects.  No win, no-fee lawyers.  Free no-obligation legal consultations & best client care since 1978."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Products Containing PPA</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            The following is a partial list of drugs containing PPA. If you have
            any concerns about these defective drug products, contact your
            doctor. For a complete list of products containing PPA, please
            contact the FDA.
          </strong>
        </p>
        <div>
          <p>
            Acutrim 16-Hour Tablets Acutrim Gum Acutrim Max Tablets Alka-Seltzer
            Plus Cold &amp; Cough Alka-Seltzer Plus Cold &amp; Sinus
            Alka-Seltzer Plus Children's Alka-Seltzer Plus Cold Alka-Seltzer
            Plus Cold (cherry or orange) Alka-Seltzer Plus Flu Tabs Alka-Seltzer
            Plus Sinus Alka-Seltzer NT BC Allergy Sinus Powder Comtrex Deep
            Chest Cold &amp; Congestion Relief Comtrex Flu Day &amp; Night
            Comtrex ND Liquigel Contac Capsules Coricidin D Tabs CVS Cold &amp;
            Allergy Elixir CVS Cold &amp; Allergy Elixir DM CVS Cold &amp;
            Allergy Tablets Max CVS Dayhist D CVS Diet Caplet CVS Diet Caplet
            with C CVS Effervescent Cold Tabs CVS Triacting Cough CVS Triacting
            Expectorant
          </p>
        </div>
        <div>
          <p>
            CVS Triacting Multi CVS Triacting Multi Cherry CVS Triacting Sore
            Throat CVS Tussin CF Dexatrim Caffeine Free Caplet Dexatrim Caplet
            with C Dexatrim Extended Duration Tablet Dexatrim Gelcaps Dimetapp
            Chew Tabs Dimetapp DM Elixir Dimetapp Elixir Dimetapp Extentabs
            Dimetapp Cold &amp; Cough Dimetapp 4-Hour Liquigels Dimetapp Quick
            Tabs Naldecon DX Adult Syrup Naldecon DX Child Syrup Naldecon DX
            Drops Permathene - 16-Hour Tablet Robitussin CF Tavist-D Tablets
            Thinz Span Capsules Triaminic Cough Triaminic Chest &amp; Congestion
            Triaminic Cold &amp; Allergy Triaminicol Cold &amp; Cough
          </p>
        </div>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
