// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(relativePath: { eq: "e-cig-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 800, srcSetBreakpoints: [800]) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="E-Cigarette Injury Lawyers - Bisnar Chase"
        pageDescription="Contact the e-cig injury lawyers of Bisnar Chase for a free consultation. You may be entitled to money."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>E-Cig Injury Lawyers</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="E-Cig Injury Lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <div className="algolia-search-text">
          <p>
            <strong>There have been many injuries</strong> in the last few years
            e-cigarettes that explode due to a faulty battery or charging
            mechanism. Many of the injuries can lead to permanent damage to
            hands, face and neck. If you've been injured by an exploding e-cig
            contact our{" "}
            <Link to="/defective-products">defective product lawyers</Link> for
            a free consultation. You may be entitled to compensation including
            medical bills and time off work. Call 1-800-561-4887 for help.
          </p>
          <h2>E-Cigarette Explosion Lawsuits</h2>
          <p>
            E-Cigarettes mimic the real thing. When you inhale, the end glows
            and when you exhale a cloud of smoke or vapor is emitted from the
            device. They are virtually accessories for celebrities and reality
            show stars. But, unlike traditional cigarettes, electronic
            cigarettes are not regulated by the U.S. Food and Drug
            Administration (FDA). These devices are currently being aggressively
            marketed to teenagers and young adults.
          </p>
          <p>
            Recent studies and analyses of these products show that chemicals
            found in e-cigarettes have the potential to cause lung damage and
            other serious health problems. Moreover, recent media reports tell
            terrifying stories of{" "}
            <Link to="/blog/the-danger-of-exploding-e-cigarettes">
              e-cigarettes that explode
            </Link>{" "}
            in users' hands, faces, pant pockets or even luggage, causing
            serious burn injuries and fires. The Orange County product defect
            law firm of Bisnar Chase is representing victims of e-cigarette
            injuries and is continuing to investigate cases where these devices
            have exploded and caused severe injuries.
          </p>
          <h2>What is in an E-Cigarette?</h2>
          <p>
            An electronic cigarette is basically a battery-powered vaporizer.
            The parts that make up an e-cigarette are a mouthpiece, a cartridge
            or tank, an atomizer, a microprocessor, a battery and possibly an
            LED light on the end. The atomizer consists of a heating element
            that vaporizes the liquid in the cartridge. The e-liquid heats up to
            create an aerosolized vapor, which the user inhales. The aerosol is
            said to provide a flavor and feel similar to tobacco smoking. There
            are a number of variations of the device, but they are all more or
            less made up of the same components. The power source is the biggest
            component of an e-cigarette, which is most often a rechargeable
            lithium battery.
          </p>
          <h2>E-Cigarette Explosions</h2>
          <p>
            The lithium batteries and their chargers have been blamed in a
            number of explosions across the United States. Here a just a few
            reports of explosions that occurred recently as reported by the
            media:
          </p>
          <ul type="disc">
            <li>
              A 29-year-old man was paralyzed after his e-cigarette exploded in
              his face, breaking his neck, burning his mouth and knocking out
              his teeth.
            </li>
            <li>
              A truck driver in Indiana crashed his big-rig when the e-cigarette
              he was smoking exploded. He suffered facial injuries from the
              explosion.
            </li>
            <li>
              A 19-year-old Orange County teen suffered burn injuries to his
              thigh after an e-cigarette battery in his pocket blew up after
              coming into contact with his keys.
            </li>
          </ul>
        </div>
        <h2>Dangerous Batteries and Chargers</h2>
        <p>
          These are just a few recent examples. There are many more incidents
          that occur every week involving these potentially defective devices.
          In a majority of the cases, it's the device's lithium-ion batteries
          that explode or catch fire. These batteries are also commonly found in
          laptops and cell phones. However, with e-cigarettes, the batteries are
          more prone to overheating, especially when they are used with
          incompatible chargers. Batteries and chargers are also manufactured
          overseas where there is little regulation or quality control. The FDA
          has yet to regulate e-cigarettes in any way, shape or form.
        </p>
        <h2>Steps You Can Take</h2>
        <p>
          You cannot protect yourself from defectively manufactured or assembled
          e-cigarettes. However, here are a few steps you can take in terms of
          exercising caution.
        </p>
        <ul type="disc">
          <li>
            Avoid counterfeit brands. Be aware of where the devices, batteries
            and chargers are manufactured.
          </li>
          <li>
            Only charge your device with the power adapter that comes with the
            battery.
          </li>
          <li>
            Do not plug the device into computers or other USB-capable devices.
          </li>
          <li>
            Find a device that has a battery you remove from the atomizer to
            charge. These appear to be safer than the models that remain
            attached when charging.
          </li>
          <li>
            Do not overcharge your battery or leave it plugged in and
            unattended.
          </li>
        </ul>
        <h2>If You Have Been Injured</h2>
        <p>
          If you have suffered serious injuries as the result of a defective
          electronic cigarette, there are a number of steps you can take to
          ensure that your legal rights are protected. First and foremost, get
          prompt medical attention and treatment for your injuries. A number of
          victims suffer severe injuries to their eyes, face and mouth,
          particularly if the device explodes as they are using it. This could
          lead to permanent scarring or disfigurement. Users also face the risk
          of suffering burn injuries to other parts of their bodies.
        </p>
        <p>
          Be sure to preserve the device, including the different components
          such as the battery and charger. The device needs to be thoroughly
          examined by an expert for any types of defects, malfunctions or design
          flaws. Contact an experienced e-cigarette lawsuit attorney who will
          conduct an independent investigation and hold the product
          manufacturers accountable for their negligence. In such cases,
          depending on the circumstances, there may be a number of potentially
          liable parties including the shop that sold you the product, the
          manufacturer of the e-cigarette, the company that made the battery or
          the charger.
        </p>
        <h2>Compensation for Victims</h2>
        <p>
          If you or a loved one has been injured by a defective electronic
          cigarette, you may be entitled to compensation including medical
          expenses, lost income, hospitalization, rehabilitation costs,
          permanent injuries or disabilities, pain and suffering and emotional
          distress. The experienced Orange County defective product attorneys at
          Bisnar Chase will work diligently to ensure that you are fairly and
          fully compensated for your losses and that the negligent manufacturers
          are held accountable. We are passionate about the pursuit of justice
          for our injured clients. Call us at (800) 561-4887 for a free
          consultation and comprehensive case evaluation.
        </p>
        <p>
          <strong>Related E-Cig Resources </strong>
        </p>
        <ul>
          <li>
            {" "}
            <Link to="/blog/newport-beach-consumer-law-firm-files-className-action-lawsuit-against-e-cigarette-maker">
              Bisnar Chase Files Class Action Against E-Cig Maker{" "}
            </Link>
          </li>

          <li>
            {" "}
            <Link to="/blog/senate-democrats-introduce-bill-prevent-e-cig-marketing-children">
              Senate Dem's Introduce Bill to Prevent E-Cig Marketing to Children
            </Link>
          </li>
          <li>
            {" "}
            <Link to="/blog/electronic-cigarette-explodes-injuring-orange-county-man">
              Electronic Cigarette Explodes Injuring California Man{" "}
            </Link>
          </li>
        </ul>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
