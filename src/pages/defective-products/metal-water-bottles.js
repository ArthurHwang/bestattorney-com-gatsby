// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Product Liability Issues on Metal Water Bottle Injuries"
        pageDescription="metallic water bottles popular with children have resulted in injuries of their tongues being lodged in the containers and causing injury and serious harm."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>
          Product Liability - Dangers of Metal Water Bottles & Injuries to
          Children
        </h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/attorney-chase.jpg"
          height="141"
          className="imgleft-fixed"
          alt="Brian Chase"
        />
        <p>
          <strong>
            A Jan. 9, 2012 NBC report warns consumers about popular metal water
            bottles that could cause children to get their tongues stuck inside.
            The report states that several children had to undergo complicated
            surgery in order to free their tongues.
          </strong>
        </p>

        <p>
          Popular metal water bottles that are commonly used by many children
          are posing a serious hazard, causing several children to undergo
          complicated and dangerous tongue surgery, NBC reports. According to
          the Jan. 9 news article, more and more children are getting their
          tongues stuck inside these metal water bottles and one company that
          manufactures the bottles has stopped selling them.
        </p>
        <p>
          The report points to the case of 6-year-old Mary Kate Person, who got
          her tongue trapped inside a metal bottle as she put her tongue in to
          get the last drops. Doctors had to cut off the bottom of the bottle,
          but feared that the part of the bottle that was stuck to her tongue
          could block her airway and cause her to suffocate or that she would
          lose her tongue, the report said. Eventually, she had to undergo three
          hours of surgery to free her tongue and spent three days in intensive
          care as she recovered.
        </p>
        <img
          src="/images/metal-water-bottles.jpg"
          className="imgright-fixed"
          alt="dangers of metal water bottles"
        />
        <p>
          According to the report, doctors believe this happens because of the
          way these bottles are designed and that the narrow neck and strong
          brass ridges on the bottle act like a noose, trapping the tongue. An
          8-year-old girl in Georgia and a 9-year-old boy in North Carolina also
          underwent similar procedures after getting their respective tongues
          trapped, the article stated.
        </p>
        <p>
          Doctors suggest that children who drink out of metal bottles should
          get a sippy cap that screws on to the bottle, which could prevent such
          incidents from occurring, the report states. It is important that the
          companies that make these dangerous metal water bottles issue a prompt
          recall and warn consumers about the hazards presented by these
          products, said John Bisnar, founder of the Bisnar Chase personal
          injury law firm.
        </p>
        <p>
          <em>
            "There are number of these water bottles out on store shelves and
            millions of children are likely using them. It is the responsibility
            of these manufacturers to ensure that dangerous products are taken
            off the market and consumers are warned."
          </em>
        </p>
        <p>
          Bisnar questioned the delay in recalling these metal water bottles.
        </p>
        <p>
          <em>
            "If manufacturers have already stopped making them because they are
            so dangerous, why are they not being recalled? Every day a recall is
            delayed, numerous children are being put in harm's way."
          </em>
        </p>
        <p>
          The California defective product lawyers of Bisnar Chase represent
          individuals who have been injured as the result of{" "}
          <Link to="/defective-products"> defective products</Link> and other
          acts of negligence. The law firm has won a wide variety of challenging
          personal injury and wrongful death cases involving car accidents,
          work-related injuries, negligence and defective products. For more
          information, please call 949-203-3814.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
