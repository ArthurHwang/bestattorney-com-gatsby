// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "bicycle-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Bicycle Accident Attorneys Buena Park, CA"
        pageDescription="Seriously injured in a Buena Park bicycle accident? Get help from experienced bike crash attorneys who have won over 500 Million in settlements and verdicts for California victims. We take on some of the toughest cases for catastrophic injuries."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Bicycle Accident Lawyer</h1>
        <BreadCrumbs location={location} />
        <p>
          <strong>
            For immediate help with a bike crash injury call 949-203-3814
          </strong>
        </p>
        <p>
          Buena Park is a highly trafficked area in Orange County. It sets up
          against both Anaheim and Cypress which gives way to a lot of thru
          traffic from surrounding cities. Buena Park is a small city, similar
          in size to La Palma, which is 2 1/2 miles in size. Small cities such
          as these tend to be highly congested to thru traffic.
        </p>
        <p>
          If you find yourself in a bike accident in Buena Park, you may not
          find a qualified bicycle accident attorney in the immediate area. Our
          team covers Buena Park and can advise you on your{" "}
          <Link to="/buena-park">personal injury claim</Link> and if you have a
          case.
        </p>
        <p>
          A standard consumer law attorney may not offer what you need in terms
          of experience dealing with <strong>bicycle accident cases</strong>.
          You'll need an attorney that practices heavily in this area just as
          you would seek an experienced workers comp attorney for a job injury.
          Not all personal injury attorneys practice all areas of injury law.
        </p>
        <p>
          Thousands of bike crashes occur in Orange County each year in which
          many are injured or even killed. There are a number of steps that
          bicyclists and car drivers can take to avoid being involved in these
          potentially devastating collisions.
        </p>
        <p>
          Bike riders who are exercising due care and are still involved in an
          injury accident can pursue financial compensation for their losses by
          filing a claim against the at-fault driver. An experienced bike crash
          lawyer can help injured victims or families of deceased victims obtain
          just compensation and hold the negligent parties accountable.
        </p>
        <p>
          <strong>
            <em>Video: RoadID.com discusses safety tips for drivers</em>
          </strong>
          .
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/X1q8VAkBE5E"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        <h2>Tips for Drivers - be more aware of cyclists</h2>
        <p>
          It is the responsibility of all car drivers to yield the right of way
          to bicyclists as they would to other vehicles. Sadly, many drivers
          fail to even notice bicycle riders before it is too late. Many
          motorists often forget that they share the road with bicyclists and
          that bicyclists have the same rights and responsibilities as drivers
          of other vehicles, under California law. There are several safety
          measures motorists can take in order to avoid devastating bicycle
          accidents:
        </p>
        <ul>
          <li>
            <strong>Slow down in residential areas</strong>: Many tragic bicycle
            accidents involve children who are riding their bikes. Whenever you
            are traveling through a residential neighborhood, it is important to
            keep an eye out for children who may be riding their bikes. Going a
            safe speed and remaining vigilant will increase your ability to stop
            in time to avoid a tragic collision.
          </li>
          <li>
            <strong>Remain attentive</strong>: Many collisions with bicyclists
            involve drivers who are looking at their phone, speaking to a
            passenger or driving with their hands off the wheel. Do not allow
            yourself to drive while distracted. It is just too dangerous. If you
            need to use your phone, find a safe place to pull over first.
          </li>
          <li>
            <strong>Give riders plenty of space</strong>: Bicyclists are
            supposed to ride on the far right side of traffic, but they often
            have to veer left to avoid parked cars, debris, dangerous road
            conditions and other hazards. Giving a bicyclist plenty of space
            when passing them on the left can help prevent a collision.
          </li>
          <li>
            <strong>Check twice before turning or entering traffic</strong>:
            Many Orange County bike crash incidents involve a driver who has
            entered traffic without properly yielding the right of way to
            approaching bicycle riders. It is also necessary to check behind the
            vehicle before making a right or left turn.
          </li>
          <li>
            L<strong>ook for bicyclists at night</strong>: There are many signs
            that a bicyclist may be nearby when riding late at night. You may
            notice a reflective pedal or bicycle light long before you can
            actually see the bicyclist. Never drive while fatigued and stay
            attentive.
          </li>
        </ul>
        <h2>Tips for Bike Riders</h2>
        <p>It is also important that cyclists practice safe riding habits:</p>
        <ul>
          <li>
            Wear bright clothing: Bicyclists should make sure that they are seen
            at all times. They should wear something that is bright and
            reflective especially at night when visibility is lower.
          </li>
          <li>
            Make eye contact: Make eye contact with car drivers before entering
            an intersection. It is never safe to assume that they see you or
            that they will properly yield the right of way.
          </li>
          <li>
            Use hand signals: Letting motorists know about your intentions can
            help them avoid striking you.
          </li>
          <li>
            Obey traffic control devices: Many bike riders try to save time by
            breezing through stop signs and red lights. Bicyclists must obey the
            same traffic laws as motorists. Failure to do so can result in
            devastating collisions.
          </li>
          <li>
            Stay to the right: It is safe for cyclists to remain all the way to
            the right of the roadway.
          </li>
        </ul>
        <h2>What to Do if you have been Injured</h2>
        <p>
          If you have been hurt in a bicycle accident, please do not hesitate to
          call the experienced bicycle accident lawyers at at our law firm. We
          will analyze all aspects of the case and ensure that your legal rights
          and best interests are protected.
        </p>
        <p>
          Injured victims can seek compensation to cover damages such as medical
          expenses, lost wages, hospitalization, rehabilitation and pain and
          suffering.
        </p>
        <p>
          Please contact us to obtain more information about pursuing your legal
          rights.
        </p>
        <p>
          We have can meet you at your location in or around Orange County
          including Buena Park. <strong>Call 949-203-3814</strong> for a{" "}
          <Link to="/contact">free consultation</Link>.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
