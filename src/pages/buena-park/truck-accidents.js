// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "truck-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Truck Accident Lawyers - Bisnar Chase"
        pageDescription="Seriously injured in a Buena Park truck accident? Call 949-203-3814 for Buena Park truck accident lawyers who can help and have recovered hundreds of millions for injured clients. Free consultations"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Truck Accident Attorney</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have been seriously injured in a truck accident, hiring a{" "}
          <strong>Buena Park Truck Accident Attorney</strong> can be a critical
          decision that could change the course of your life.
        </p>
        <p>
          If you are using an Internet search engine such as Google to look for
          a truck accident lawyer, you will probably come across hundreds of{" "}
          <Link to="/buena-park">Buena Park personal injury lawyers</Link> who
          will state with great authority that they handle truck accidents.
        </p>
        <p>
          However, you need to find the best attorney who has the knowledge,
          skill, experience and the resources to handle your case. If you are
          looking for such a lawyer, here are a few tips that may help you find
          the right truck accident attorney.
        </p>
        <h2>When Should You Hire a Truck Accident Lawyer?</h2>
        <p>
          You need a lawyer on your side as soon as possible. What's the rush?
          Any personal injury case depends on the quality of evidence that is
          there to support your case. The sooner you hire a skilled lawyer to
          help you in your case, the more you increase the likelihood of
          recovering just compensation for your losses.
        </p>
        <p>
          There are a number of important pieces of evidence that must be
          obtained and secured after a truck accident. Examples of such evidence
          include the vehicle itself, driver logs, vehicle maintenance logs,
          police and eyewitness reports, etc. You also need a lawyer on your
          side right away because insurance companies work hard to ensure that
          the value of your claim is minimized.
        </p>
        <p>
          They will try to make offers that may seem like a quick resolution.
          Often, these offers do little or no justice to the losses the victim
          has suffered. Please do not enter into any such agreement until you
          have consulted your Buena Park truck accident lawyer.
        </p>
        <h2>Finding the Best Attorney</h2>
        <p>
          How do you know which truck accident attorney is the best? You need a
          lawyer who is an expert on truck accident cases. The best truck
          accident lawyers are constantly equipping themselves with knowledge
          about truck accident laws and changes in the trucking industry.
          Without this knowledge, it is difficult for a lawyer to represent you
          effectively. If you were involved in a truck accident in Buena Park,
          find a local lawyer who has plenty of knowledge about local courts,
          judges and juries.
        </p>
        <h2>Experience is Critical</h2>
        <p>
          Once you have established the attorney's expertise in handling truck
          accident cases, it would be a good idea to determine if he or she has
          handled cases exactly like yours. For example, if you were
          catastrophically injured in a truck accident where a sheet of metal
          flew off the truck and hit your vehicle, then, you want to look for an
          attorney who has gone after the trucking company in a case just like
          that one.
        </p>
        <p>
          How experienced is the attorney and the law firm? How many cases have
          they handled? How many have they won? How many cases did they settle
          and how many went to trial? Have they handled significant cases
          before?
        </p>
        <p>
          The experienced Buena Park truck accident lawyers at Bisnar Chase have
          established a sterling reputation in the Orange County community as
          lawyers who not only get results, but also as committed professionals
          who care about the community and continually give back. We tirelessly
          fight for the rights of injured victims and their families.
        </p>
        <p align="center">
          <strong>
            Call 949-203-3814 for a free consultation or to discuss your case
          </strong>
          .
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
