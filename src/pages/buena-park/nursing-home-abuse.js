// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Nursing Home Lawyers & Elder Abuse Attorneys"
        pageDescription="Contact the Buena PArk nursing home abuse lawyers and elder abuse attorneys about a suspected case of elder neglect. 100% free and private consultation and a 96% success rate litigating cases throughout California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Nursing Home Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          The nursing home abuse lawyers of Bisnar Chase have represented
          residents of Orange County including Buena Park since 1978. Our team
          of trial lawyers have decades of experience representing families who
          have had a loved one harmed by a neglectful nursing home. Seniors can
          be very fragile and often times staff are overworked and underpaid
          leading to frustration. Sadly, the frustration is often taken out on
          the patients. If your loved one has been abused by a nursing home
          staffer
          <strong>
            please call 949-203-3814 to speak with a top rated elder abuse
            lawyer.
          </strong>
        </p>
        <h2>Retirement Facility Dangers</h2>
        <p>
          Buena Park is a city of around 83,000 in north Orange County that
          boasts several tourist attractions, including Knott's Berry Farm.
          There are several retirement facilities in the Buena Park area and
          several more in nearby cities. If you live in Buena Park and are
          considering moving into a managed-care situation, or if you have a
          family member who needs to be placed, you want to find the very best
          home possible.
        </p>
        <p>
          Unfortunately, even facilities that have good reputations can suffer
          from poor employees or management. In some cases, residents can pose a
          threat to others as well. If you find that your dream retirement
          location has turned into a nightmare because of abuse or neglect,
          nursing home lawyers can assist you in collecting damages.
        </p>
        <p>
          No one likes to think about it, but nursing home abuses and instances
          of neglect happen frequently. In fact, experts estimate that one out
          of three residents of an assisted living facility will suffer some
          form of abuse during their stay.
        </p>
        <p>
          As most residents of nursing care facilities are women, this means a
          very high number of abuses directed toward the female gender,
          including instances of physical and sexual abuse. Because older people
          or those who are disabled make up the populations of these facilities,
          there is little chance for them to fight back or overpower an
          aggressor.
        </p>
        <p>
          Some abuses are not so frightening and are, in fact, far more common.
          Residents may suffer from petty theft, neglect of needed services, and
          verbal abuse from staff or other residents.
        </p>
        <p>
          However, some of the neglect that occurs in assisted care facilities
          is nothing to shrug off. If a resident does not receive medication
          regularly, for example, he or she may suffer health issues and could
          even face a dire medical emergency.
        </p>
        <h2>Some of the most common signs of abuse or neglect:</h2>
        <ul>
          <li>
            unexplained bruising (not typical bruising you may see in an elderly
            patient)
          </li>
          <li>loss of appetite</li>
          <li>depression or anxiety</li>
          <li>isolation</li>
          <li>sudden unexplained fear</li>
          <li>unexplained anger</li>
          <li>uncontrolled crying</li>
        </ul>
        <p>
          Many elder care and nursing home facilities fail to meet standards.
          Families of those who live in these facilities have good cause to be
          vigilant.
        </p>
        <p>
          Checking on loved ones frequently, talking to them about their
          neighbors and the staff, and observing them for changes in behavior
          are a few of the ways families can help prevent or stop elder abuse.
        </p>
        <h2>Hiring the Right Lawyer</h2>
        <p>
          Nursing home attorneys can help. Our{" "}
          <Link to="/buena-park">Buena Park personal injury lawyers</Link> have
          not only studied the law surrounding assisted living at both the state
          and federal level but have the experience to guide families through
          the process of collecting evidence to support a complaint or a suit
          against a home.
        </p>
        <p>
          You may think that you cannot prove what has happened or is happening
          to your loved one, but that may not be the case. When you talk to an
          attorney about your loved one's situation, you may find that there are
          ways of proving abuse and neglect that you may not know about.
        </p>
        <p>
          Make an appointment today to talk to a Buena Park nursing home lawyer
          about your loved one's living conditions.{" "}
          <strong>Call 949-203-3814 to discuss your case with us.</strong>
        </p>

        <LazyLoad>
          <iframe
            width="100%"
            height="500"
            src="https://www.youtube.com/embed/520_pSDAx_s"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen={true}
          />
        </LazyLoad>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
