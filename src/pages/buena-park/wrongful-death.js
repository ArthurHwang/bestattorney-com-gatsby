// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "wrongful-death"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Wrongful Death Lawyers - Wrongful Death Attorneys Buena Park"
        pageDescription="Need a wrongful death attorney? Call 949-203-3814 for top Buena Park wrongful death lawyers. Quality client care & free consultations with a 96% success rate and hundreds of millions recovered."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Wrongful Death Lawyers</h1>
        <BreadCrumbs location={location} />
        <p>
          If you have lost a loved one in a fatal accident, we understand that
          the devastating consequences such a loss can have on your family. The
          sudden loss of a loved one not only places a tremendous emotional
          burden on a family, but it also causes severe financial hardship.
        </p>
        <p>
          Family members may be left with medical expenses, funeral costs and
          other financial losses such as lost income and benefits. An
          experienced wrongful death lawyer can help you better understand your
          legal rights and options, especially if your loved one was killed by
          someone else's negligence or wrongdoing. But selecting the right
          attorney to handle your wrongful death case can be a challenge in
          itself.
        </p>
        <p>
          The <Link to="/buena-park">Personal Injury Attorneys</Link> of Bisnar
          Chase have over three decades of experience and the resources to win
          some of the toughest cases.
          <strong>Call 949-203-3814</strong> for a free consultation with our
          wrongful death attorneys.
        </p>
        <h2>What to Look for in a Wrongful Death Lawyer</h2>
        <p>
          Having the right attorney can greatly increase your chances of
          receiving fair compensation for your losses. How do you know if an
          attorney is right for you? What questions should you ask?
        </p>
        <ul>
          <li>
            Does the law firm have a recent history of handling cases similar to
            yours?
          </li>
          <li>
            Does the specific attorney you are speaking with have experience
            handling your type of wrongful death claim? Do not rely just on the
            track record of the law firm.
          </li>
          <li>
            Is the law firm financially stable enough to afford expert witnesses
            and other expenses that may arise during the process of handling
            your case?
          </li>
          <li>
            Is the attorney interested in a quick settlement or does the
            attorney plan to fight for the largest possible settlement in court?
            Find an attorney who fits your requirements and need.
          </li>
          <li>
            There are many types of wrongful death accidents including medical
            malpractice, fatal car accidents and defective product accidents.
            Has the attorney with whom you are meeting have a proven track
            record with your specific type of case? For example, if your loved
            one died as the result of a defective seatbelt, does the attorney
            with whom you are meeting have prior experience handling seatbelt
            defect cases?
          </li>
          <li>
            As obvious as it may sound, it is important you actually like your
            attorney. Wrongful death cases are sensitive and you deserve an
            attorney who will support you and treat you with care, respect and
            understanding.
          </li>
          <li>
            Does your attorney plan on including you during the process?
            Depending on your preference, it may be in your best interest to
            retain an attorney who will discuss your options with you before
            making a decision on your behalf.
          </li>
          <li>
            What is the law firm's policy on returning phone calls? The law firm
            should commit to promptly returning your phone calls and giving you
            regular updates about your case.
          </li>
          <li>
            Does the attorney want money up front? The most reputed attorneys
            work on a contingency fee basis. This means that they do not get
            paid until you win your case.
          </li>
        </ul>
        <h2>Getting Referrals</h2>
        <p>
          Do you have any friends who can refer you to a reputed attorney?
          Knowing someone who has gone through a similar experience can be
          extremely helpful. If you do not know any attorneys, do not rely
          simply on a yellow page advertisement or a television commercial.
          Meeting with an attorney in person can help you understand the type of
          law firm with which you are dealing.
        </p>
        <p>
          Don't be afraid to ask the attorney how much your claim may be worth
          and how long they think the claim process may take. These details are
          difficult to determine during an initial consultation, but a skilled
          attorney can get you a rough idea of the road ahead.
        </p>
        <p>
          If your loved one was killed because of someone else's wrongdoing or
          negligence, financial compensation may be available for medical bills,
          funeral expenses, lost future wages, loss of benefits, loss of
          inheritance and other related damages.
        </p>
        <p>
          A knowledgeable Buena Park wrongful death lawyer can answer all your
          questions during the free initial consultation.
        </p>
        <p>
          If you have lost a loved one, please contact the experienced wrongful
          death attorneys at Bisnar Chase or call us at{" "}
          <strong>949-203-3814</strong> to obtain more information about
          pursuing your legal rights.
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
