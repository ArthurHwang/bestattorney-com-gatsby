// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Brain injury Lawyer - Orange County, CA"
        pageDescription="Call our Buena Park brain injury lawyers for a free case review. We've helped thousands obtain fair compensation and may be able to help you too. If we don't win, you don't pay and we shield you from liability during your case."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Brain Injury Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          There are many ways a brain injury accident can happen in an instant.
          Car accidents, pedestrian injuries and work related accidents can all
          be the cause of a short or long term{" "}
          <Link to="/head-injury">brain injury</Link>. Regardless of what caused
          the brain injury, the consequences are often devastating, not only for
          the injured victim, but also for his or her family.
        </p>
        <p>
          Even a slight brain injury accident can cause a lot of problems in the
          future if it's not treated fast and correctly. It is important to
          understand when a brain injury has occurred and to
          <strong>seek out immediate medical attention</strong>, even if you
          think you feel okay. If the injury or accident warrants an exam, make
          sure you immediately have one. There have been cases where people had
          direct trauma to the head (say, a skateboarding accident) and waited
          to seek treatment only to have bleeding or swelling of a serious
          nature.
        </p>
        <p>
          If you have been the victim of a brain injury and think you may have a
          case, contact our experienced{" "}
          <Link to="/buena-park">Buena Park Personal Injury Attorneys</Link> for
          a free consultation. If we don't win, you don't pay.
          <strong> Call 949-203-3814</strong>. Our office has won over $500
          Million for clients throughout California and we may be able to help
          you too.
        </p>
        <h2>Types of Brain Injuries</h2>
        <p>
          Many victims who suffer head injuries sustain a concussion. A brain
          contusion is when the brain is bruised and there is some bleeding in
          the brain. A skull fracture occurs when the cranium breaks. Some skull
          bones may have cut into the brain and caused bleeding. A hematoma is
          when bleeding in the brain collects and clots. This type of{" "}
          <strong>brain injury</strong> may only become evident days after the
          incident. More serious types of brain injuries are anoxic brain
          injuries, long term concussions, secondary brain syndrome and severe
          edema or bleeding. Many athletes see these types of brain injury
          accidents.
        </p>
        <p>
          Because a brain injury is not always obvious and has no external
          injuries an experienced brain injury lawyer can be your advocate in
          getting compensation. Finding a lawyer that specializes in traumatic
          brain injuries is your best bet for a fair outcome of your case. One
          that has the experience and knowledge dealing with these difficult
          cases.
        </p>
        <h2>When to See a Medical Professional</h2>
        <p>
          Traumatic brain injuries often present symptoms, many of which can be
          extremely serious. Some of the common brain injury symptoms include:
        </p>
        <ul>
          <li>Headaches</li>
          <li>Loss of consciousness</li>
          <li>State of confusion or disorientation</li>
          <li>Nausea or vomiting</li>
          <li>Blurred vision or ringing of the ears</li>
          <li>Mood changes</li>
          <li>Feeling of depression or anxiety</li>
          <li>Difficulty sleeping or sleeping more than usual</li>
        </ul>
        <p>
          The severity of the symptoms can be a sign of the seriousness of the
          trauma. For example, someone who has lost consciousness for an hour
          may have experienced more significant trauma than someone who was out
          for only a moment. That does not, however, mean that a serious injury
          has not occurred in both circumstances. In fact, some brain injury
          symptoms do not fully manifest for days. Regardless of the severity of
          the symptoms or the pain victims may experience, it is important that
          immediate medical attention and treatment are sought when a head
          injury accident occurs.
        </p>
        <h2>Cost of a Brain Injury Accident</h2>
        <p>
          Brain injuries can be extremely costly. Medical expenses alone can
          bankrupt a brain injury victim who does not have adequate medical
          coverage. Treatment-related expenses may include emergency room
          services, testing fees, hospitalization expenses, surgical procedures,
          medical devices and prescription drugs. Future medical losses may
          include physical therapy, occupational therapy and other
          rehabilitation expenses. Brain injury accident victims may struggle to
          return to work. Others may never again earn the same wages they once
          earned. Some victims require round-the-clock nursing care. The
          expenses after a traumatic brain injury may add up to millions of
          dollars over a person's lifetime. The cost of recovery and treatment
          should not be the burden of the victim.
        </p>
        <h2>How Can A Personal Injury Attorney Help?</h2>
        <p>
          Brain injury accident victims would be well advised to contact an
          experienced brain injury lawyer who will help them throughout the
          claim process. A skilled personal injury lawyer can:
        </p>
        <ul>
          <li>
            Work with medical professionals to determine the true extent of the
            injuries suffered.
          </li>
          <li>
            Work with investigators to prove the cause of the accident and who
            should be held accountable for the incident.
          </li>
          <li>
            Protect the legal rights and best interests of the victim and the
            victim's family.
          </li>
          <li>
            Deal directly with any insurance companies involved on behalf of the
            victim.
          </li>
          <li>
            Take the at-fault party to court in order to secure fair
            compensation for the victim's losses.
          </li>
          <li>
            Ensure that all financial and nonfinancial losses are considered
            during the claim process.
          </li>
        </ul>
        <p>
          The knowledgeable brain injury lawyers at our firm have a long and
          successful track record of helping brain injury victims and their
          families secure fair and full compensation for their significant
          losses. We work on a contingency fee basis. This means that we do not
          get paid until you get compensated. If you or a loved one has suffered
          a<strong>brain injury in Buena Park</strong>, please contact us for a{" "}
          <Link to="/contact">free consultation</Link> and comprehensive case
          evaluation.
          <strong>Call 949-203-3814.</strong>
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
