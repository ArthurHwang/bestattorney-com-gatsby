// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "premises-liability"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Slip & Fall Attorneys - Orange County CA"
        pageDescription="Severely injured in slip & fall accident? Call 949-203-3814 for top rated Buena Park slip and fall attorneys who can help. Free consultations. No win no fee. Our premise liability attorneys have decades of experience"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Slip & Fall Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <img
            src="/images/slip-and-fall/slip-and-fall.jpg"
            alt="buena park slip and fall lawyers"
            className="imgright-fixed"
          />{" "}
          <Link to="/premises-liability/slip-and-fall-accidents">
            slip and fall accidents
          </Link>{" "}
          are commonly one of the leading causes of injuries and fatalities
          among elderly people.
        </p>
        <p>
          A number of worker compensation and liability claims stem from slip
          and fall or trip and fall accidents as well. In some cases a slip and
          fall accident may be just that, an accident.
        </p>
        <p>
          However, in many cases, these accidents are caused by the negligence
          of a property owner, a municipality or some other entity that failed
          to fix or repair a dangerous condition.
        </p>
        <p>
          In such cases injured victims may be able to seek compensation for
          injuries, damages and losses. Our experienced Buena Park slip and fall
          lawyer can help injured victims better understand their legal rights
          and options.
        </p>
        <h2>Slip and Fall Statistics</h2>
        <p>
          The New York Times recently reported that 33 million people require
          medical attention every year, and that falling is the most common
          cause of non-fatal injuries for all age groups. The U.S. Centers for
          Disease Control and Prevention (CDC) reports the following statistics:
        </p>
        <ul>
          <li>
            37 percent of all injury episodes in the United States involved
            falling incidents.
          </li>
          <li>
            Falling was the most common cause of external injury among
            Americans.
          </li>
          <li>
            27.3 percent of all injuries occurred inside the victim's home and
            17 percent occurred outside the victim's home.
          </li>
          <li>
            Sprains and strains were the most common injuries (30.7 percent);
            followed by contusions or superficial injuries (22.2 percent); open
            wounds (17.1 percent); and fractures (14.7 percent).
          </li>
          <li>
            In 30.8 percent of all injury accidents, the victim suffered
            injuries to their lower extremities. Upper extremities were injured
            29 percent of time followed by injuries to the torso or spine and
            injuries to the head or neck.
          </li>
        </ul>
        <h2>Falls Involving the Elderly</h2>
        <p>
          The elderly are particularly vulnerable to slip and fall accidents.
          According to statistics for adults aged 65 or older reported by the
          CDC:
        </p>
        <ul>
          <li>
            One out of three elderly adults is hurt in falling accidents each
            year, but less than half contact their healthcare provider.
          </li>
          <li>
            Falls are the leading cause of injury-related death and the most
            common cause of non-fatal injuries for the elderly.
          </li>
          <li>
            In the year 2007, 18,000 elderly people suffered fatal injuries in a
            falling accident.
          </li>
          <li>
            In the year 2008, 19,700 older adults were killed as a result of
            unintentional falling injuries.
          </li>
          <li>
            In the year 2009, 2.2 million non-fatal injuries required emergency
            room attention and over 581,000 elderly victims of fall-related
            injuries required hospitalization.
          </li>
          <li>
            Of all fatal falling deaths that occurred in the year 2007, elderly
            victims were involved 81 percent of the time.
          </li>
          <li>
            Elderly men are 46 percent more likely to suffer fatal injuries in a
            fall than women.
          </li>
          <li>
            From the statistics gathered in 2008, the rate of fall injuries for
            adults 85 years of age or older was almost four times higher
            compared to adults between the ages of 65 and 74.
          </li>
        </ul>
        <h2>Work-Related Falls</h2>
        <p>
          Fatal falls commonly occur at the workplace. According to The United
          States Bureau of Labor Statistics (BLS), in the year 2009, there were
          4,551 work-related fatalities. Falls were responsible for 14 percent
          of those fatal accidents. That makes falls the fourth most common
          cause of fatal work injuries behind transportation incidents, assaults
          and contact with objects and equipment. Of the 645 fatal fall
          accidents suffered by workers in the year 2009:
        </p>
        <ul>
          <li>20 percent involved a ladder</li>
          <li>17 percent involved a fall from a roof</li>
          <li>14 percent resulted from a fall on the same level</li>
          <li>12 percent resulted from falls from a stationary vehicle</li>
          <li>8 percent involved a fall from scaffolding or staging</li>
        </ul>
        <h2>Rights of slip and fall Victims</h2>
        <p>
          Every case is different, but slip and fall victims who have been
          injured by the negligence of another individual or entity have the
          right to pursue financial compensation for their losses. For example,
          injured workers can pursue compensation for their medical bills and a
          portion of their lost wages through workers compensation benefits.
          Victims of slip and fall accidents that were caused by hazardous
          condition on the property can file a premises liability claim against
          the negligent property owner. When an elderly resident in a nursing
          home is injured in a falling accident, the care facility can be held
          accountable for the incident if negligence was a contributing factor.
        </p>
        <LazyLoad>
          <img
            src="/images/caoc.jpg"
            alt="CAOC - Bisnar Chase Premises Liability Lawyers"
            className="imgright-fixed"
          />
        </LazyLoad>
        <p>
          The skilled Buena Park slip and fall lawyers at our law firm can help
          injured victims determine if they have a valid claim. Injured victims
          can seek compensation to cover damages including but not limited to
          medical expenses, loss of earnings, cost of hospitalization,
          rehabilitation and other related damages. Please contact us for a{" "}
          <Link to="/contact">free consultation</Link> and comprehensive case
          evaluation. <strong>Call 949-203-3814.</strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
