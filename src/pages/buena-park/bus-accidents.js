// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "car-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "bus-accidents/buena-park-bus-accident-lawyer-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Bus Accident Attorneys - Orange County, CA"
        pageDescription="Severely injured in a bus accident? Call 949-203-3814 for top rated Buena Park bus accident attorneys who can help. Free consultations. No win, no fee. No risk. 96% success rate, hundreds of millions recovered throughout California."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Bus Accident Lawyer</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="buena park bus accident lawyers"
            title="buena park bus accident lawyers"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>
        <p>
          A number of people who live and work in Buena Park use public
          transportation on a daily basis, especially buses. Many utilize the
          Anaheim Resort Transit buses (ART) when traveling to Anaheim and Buena
          Park attractions.
        </p>
        <p>
          Others use the Orange County Transit Authority (OCTA), which has
          approximately 77 bus routes and 6,200 bus stops throughout Orange
          County.
        </p>
        <p>
          Thousands of children ride Orange County school buses every day as
          well. While bus travel can be inexpensive, it can also be extremely
          dangerous. Anyone who has been injured in a{" "}
          <Link to="/bus-accidents">bus accident</Link> would be well advised to
          contact an experienced injury lawyer to obtain more information about
          his or her legal rights and options.
        </p>
        <h2>Injuries Suffered in a Bus Accident</h2>
        <p>
          There are many types of injuries that can occur as the result of a bus
          accident. Even on buses equipped with properly working seatbelts,
          there is the potential for serious injuries to occur. Here are the
          most common types of injuries that may occur in a bus accident:
        </p>
        <ul>
          <li>
            Brain injuries: There are a several ways in which a bus passenger
            may suffer a traumatic brain injury. Some victims may suffer brain
            trauma after striking their head on the window, the seat in front of
            them or on another hard object. Others may experience whiplash so
            severe that their brain can strike the inside of their skull.
            Victims who have lost consciousness or who are suffering from
            headaches after a bus crash should seek out immediate medical
            attention.
          </li>
          <li>
            Whiplash: Neck injuries are serious and can take a long time to
            heal. Some victims may not even experience symptoms until hours or
            days after the crash. Anyone who has a sore neck following a bus
            accident should seek medical attention before his or her symptoms
            get worse.
          </li>
          <li>
            Spinal cord damage: In instances involving a sudden and powerful
            collision, passengers may suffer spinal cord trauma. Victims of
            spinal cord injuries may experience numbness or even paralysis.
          </li>
          <li>
            Bone fractures: It is particularly common for victims of bus
            accidents to suffer bone fractures. Some may suffer broken bones in
            their arms if they attempted to brace themselves for impact or
            crushing bone fractures in their legs when the bus sustains
            substantial damage.
          </li>
          <li>
            Broken teeth: Another consequence of slamming your head into a seat
            or object during a bus crash is the possibility of broken teeth and
            facial disfiguration.
          </li>
          <li>
            Lacerations: Many bus accidents result in broken windows. Flying
            shards of glass and metal can cause devastating laceration injuries.
          </li>
          <li>
            Internal bleeding: This is possible if the victim was wearing a
            seatbelt that caused severe abdominal pressure or if he or she was
            crushed in the accident.
          </li>
        </ul>
        <h2>Other Bus Accident Victims</h2>
        <p>
          Bus passengers are not the only potential victims of bus accidents.
          Victims who are in a car, on a bicycle or walking on the roadway at
          the time of the accident can suffer major or fatal injuries as well.
        </p>
        <p>
          Buses are large vehicles, which means that they can cause devastating
          damage. All injured victims of bus accidents can hold the at-fault
          party accountable for their injuries, damages and losses.
        </p>
        <h2>What to Do after a Bus Accident</h2>
        <p>
          Victims of bus accidents will need to prove that they were involved in
          the accident and that they suffered an injury in the accident. It is
          important to have your name on the police report, to obtain the
          contact information from the drivers and witnesses of the crash and to
          seek out immediate medical attention.
        </p>
        <p>
          You will then need to calculate the extent of your damages and losses.
          Did you miss work because of the accident? Have you suffered
          physically and emotionally because of your injuries? Did you suffer a
          long-term or permanent disability as a result of the bus crash?
        </p>
        <p>
          Determining the potential value of a bus accident claim can be a
          challenging process. The experienced bus accident attorneys at our law
          firm can help you calculate your losses while holding the at-fault
          party accountable for the crash. If you or a loved one has been
          injured in a Buena Park bus crash, please contact us for a{" "}
          <Link to="/contact">free consultation</Link>.{" "}
          <strong>Call 949-203-3814</strong>
        </p>
        <LazyLoad>
          <img
            src="/images/superlawyers.jpg"
            alt="Bisnar Chase serves Buena Park, CA & is a SuperLawyers Member"
            className="imgcenter-fixed"
          />
        </LazyLoad>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
