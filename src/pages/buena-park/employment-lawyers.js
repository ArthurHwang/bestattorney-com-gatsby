// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"
import { graphql } from "gatsby"
import Img from "gatsby-image"

const customPageOptions = {
  pageSubject: "employment-law"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export const query = graphql`
  query {
    headerImage: file(
      relativePath: {
        eq: "employment/buena-park-employment-law-attorneys-banner-image.jpg"
      }
    ) {
      childImageSharp {
        fluid(maxWidth: 645, maxHeight: 200, srcSetBreakpoints: [645]) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default function({ data, location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Employment Lawyers | Discrimination Attorneys"
        pageDescription=" If you are looking for an employment lawyer in Orange County contact our highly rated Buena Park employment attorneys at 949-203-3814 for a free consultation. The law firm of Bisnar Chase has been fighting for clients who have been wronged in the workplace for over 40 years."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Employment Attorneys</h1>
        <BreadCrumbs location={location} />

        <div className="mini-header-image">
          <Img
            className="banner-image"
            alt="Employment attorneys in Buena Park"
            title="Employment attorneys in Buena Park"
            fluid={data.headerImage.childImageSharp.fluid}
          />
        </div>

        <p>
          Bisnar Chase's <strong>Buena Park Employment Lawyers</strong>{" "}
          represent people who work or live in Orange County. Our employment
          lawyers have been a part of a successful team that's been serving
          California since 1978.{" "}
        </p>
        <p>
          Compensation may be available for the hostility and challenges you
          have faced by your employer. A labor law attorney can help people who
          have been discriminated against because of their age, gender, race or
          because of a disability.
        </p>
        <p>
          If you believe your employer violated your rights or committed an
          unlawful act, it may be in your best interest to discuss your concerns
          with an experienced employment lawyer that serves the Buena Park area.
          Our <Link to="/buena-park">personal injury attorneys</Link> have won
          over <strong>$500 million dollars in settlements</strong> for injury
          and accident cases.{" "}
        </p>
        <p>
          <strong>
            Looking for an employment law attorney nearby the 90621 area?
          </strong>
        </p>
        <p>
          <strong>
            Call the law group of Bisnar Chase at 949-203-3814 for a free
            consultation.
          </strong>
        </p>
        <h2>The Worker's Claim Filing Process</h2>
        <p>
          It can be an anxiety-driven task to file a worker's claim against your
          employer. Employees may already be fearful that they may lose their
          job and not be able to make ends meet due to lost wages. You need to
          remember though as an employee you do have rights and under California
          law, you are protected.{" "}
        </p>
        <p>
          If you have been wronged in the workplace there are several avenues
          you can take when filing an{" "}
          <Link
            to="https://www.occourts.org/self-help/smallclaims/"
            target="_blank"
          >
            Orange County claim
          </Link>
          . One of the first ways that an employee can take when filing a charge
          is submitting a complaint form online. Note, that there are time
          limitations of when you can file a charge of a discriminatory act. A
          worker has 180 days to file a charge but if the case involves age
          discrimination than the time limit can be extended to 300 days since
          the incident.{" "}
        </p>
        <p>
          If you feel as though filing a claim through the EEOC is not enough,
          you may want to seek legal representation. An employment lawyer in
          Orange County can inform you of your rights and even protect you
          throughout the process of your claim so you will not be wrongfully
          terminated or retaliated against by your employer.{" "}
        </p>
        <h2>Common Types of Employment Claims in Buena Park, California </h2>
        <p>
          The U.S. Equal Employment Opportunity Commission reported that in one
          year, exactly 76,418 employees filed a labor law claim. Many of those
          workers experienced being discriminated against, harassed or were not
          compensated for the work they had performed. There are various kinds
          of claims that can be filed amongst employees.
        </p>
        <p>
          <strong>Common Types of worker claims can include:</strong>
        </p>
        <ul>
          <li>
            <strong>Age Discrimination: </strong>Age discrimination, or ageism,
            is the act of stereotyping someone based on his or her age alone.
            Ageism in the workplace may involve the wrongful termination of an
            experienced and capable employee for being too old. The Age
            Discrimination Act of 1967 provides protections for job applicants
            and employees from ageism, but that doesn't mean that victims of age
            discrimination will automatically qualify for compensation. There
            are certain circumstances in which companies may be exempt from
            paying damages to a wronged former employee. Each case is different,
            but employees who believe that they were treated unfairly because of
            their age would be well advised to discuss their rights with an
            employment lawyer.
          </li>
          <li>
            <strong>Gender Discrimination: </strong>{" "}
            <Link
              to="https://datausa.io/profile/geo/buena-park-ca/#economy"
              target="_blank"
            >
              Buena Park, CA
            </Link>{" "}
            employs around 39.8k people. With so many employees there are bound
            to be various types of discrimination. Gender discrimination may
            seem like a thing of the past, but it is a common and tragic reality
            for many working individuals in Buena Park. There are a number of
            companies that still violate the Civil rights Act of 1964, the Equal
            Pay Act of 1963 and the Pregnancy Discrimination Act of 1978 by
            treating men and women differently and maintaining inequity in pay.{" "}
          </li>
          <LazyLoad>
            <img
              alt="Boss yelling at an employee"
              width="100%"
              title="Discrimination lawyers in Buena Park"
              src="/images/employment/buena-park-wrongful-termination-lawyers-gender-image.jpg"
            />
          </LazyLoad>
          <li>
            <strong>Race Discrimination: </strong>Title VII of the Civil Rights
            Act of 1964 deals specifically with discrimination in the workplace.
            Under the law, employers may not discriminate against any employee
            or job applicant because of race. This includes discrimination
            relating to hiring, salary, job training, promotions and
            terminations.
          </li>
          <li>
            <strong>Disability Discrimination:</strong> Under the Rehabilitation
            Act of 1973 and the Disabilities Act of 1990, individuals with
            disabilities may not be discriminated against in the workplace.
            Employers may not discriminate against qualified individuals with
            disabilities during the hiring process or with regard to firing,
            advancement, job training and compensation considerations. Under
            California law, employers with five or more employees must make
            reasonable accommodations to a qualified applicant who has a
            disability.
          </li>
          <li>
            <strong>Sexual Harassment:</strong>{" "}
            <Link to="https://www.eeoc.gov/laws/statutes/titlevii.cfm">
              Title VII of the Civil Rights Act of 1964
            </Link>{" "}
            prohibits any sexual harassment that occurs in the workplace as
            illegal. Sexual harassment in the workplace unfortunately most of
            the time goes unreported. The majority of people that are sexually
            harassed are women. Sexual harassment is defined as any unwelcome
            physical or verbal sexual nature. A term also associated with sexual
            harassment is quid pro quo (favor for a favor) which means that if a
            worker cooperates and fulfills the sexual advances of their employer
            in return they will receive a pay raise or advance to a higher
            position in the company.{" "}
          </li>
        </ul>
        <h2>What is a Whistleblower?</h2>
        <p>
          A{" "}
          <Link
            to="https://en.wikipedia.org/wiki/Whistleblower"
            target="_blank"
          >
            whistleblower
          </Link>{" "}
          is an employee that reports illegal or ethically wrong activity that
          is occurring in the workplace. Whistleblowers work as a checks and
          balances system to ensure that upper management is not abusing their
          power. It is against the law to retaliate against a whistleblower by
          terminating their employment. It is also against the law to retaliate
          against the employee by providing a hostile work environment.{" "}
        </p>
        <p>
          There are different ways that a whistleblower can prove that there is
          illicit activity taking place at work. Whistleblowers can report the
          incident to another co-worker (internally), to a person that works
          outside of the company (external) or cyber whistleblowing where an
          employee reports a security breach.{" "}
        </p>
        <p>
          If you have been wrongfully terminated or you believe your former
          employer violated state and federal labor laws, please do not hesitate
          to call a skilled employment lawyer to better understand your legal
          rights and options.{" "}
        </p>
        <h2>California Labor Laws </h2>
        <p>
          It is common for companies to cut corners by violating the{" "}
          <Link to="https://www.dol.gov/whd/flsa/" target="_blank">
            Fair Labor Standards Act
          </Link>{" "}
          of 1938 or California labor laws. Employees have a number of wage and
          hour rights regarding when they must receive overtime pay. While there
          are exceptions for executive level employees, in general, anyone
          working over 40 hours a week or eight hours a day must receive
          overtime pay. Employees who do not receive overtime pay over weeks,
          months or even years may be able to pursue compensation for the
          substantial pay they have missed.{" "}
        </p>
        <LazyLoad>
          <div
            className="text-header content-well"
            title="money stacked on a table"
            style={{
              backgroundImage:
                "url('/images/employment/buena-park-workplace-attorney-text-banner-image.jpg')"
            }}
          >
            <h2>Have You Been A Victim of a Wrongful Termination?</h2>
          </div>
        </LazyLoad>

        <p>
          Under California law, workers are classified as at-will employees.
          This means that they may be fired at any time if their employment is
          not under contract. Being an at-will employee also means that a worker
          can quit at any time. There are, however, cases in which a person has
          been wrongfully terminated.{" "}
        </p>
        <p>
          If you believe that you were let go because of your sexual preference,
          you age, sex, race or because you were a whistleblower on a serious
          labor dispute, you may have a wrongful termination case.{" "}
        </p>
        <h2>Our Southern California Employment Lawyers Will Protect You</h2>
        <p>
          The <strong>Buena Park Employment Lawyers</strong> of Bisnar Chase has
          been representing workers whose rights have been compromised for over
          40 years. Our goal is to not only win you the compensation you need
          for your losses but to also serve you in the best way we can. Bisnar
          Chase prides itself on client satisfaction and keeps you updated on
          what is taking place in your labor law case frequently.{" "}
        </p>
        <p>
          When you <Link to="/contact">contact </Link>our law offices today you
          will immediately speak to a top legal expert about your case.{" "}
        </p>
        <p>
          <strong>Call 949-203-3814 for a free consultation.</strong>
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
