// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "pedestrian-accident"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Pedestrian Accident Lawyers - Orange County CA"
        pageDescription="Buena Park Pedestrian Accident Attorneys available to review your injury case free of charge. We have helped over 12,000 clients recover compensation for their injuries. Call 949-203-3814"
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Pedestrian Accident Attorneys</h1>
        <BreadCrumbs location={location} />
        <p>
          <em>
            <strong>
              Contact a pedestrian accident attorney to discuss your case today.
              Never a fee for a case review, and if we dont win, you dont pay.
              949-203-3814
            </strong>
          </em>
        </p>
        <p>
          {" "}
          <Link to="/pedestrian-accidents">Pedestrian accidents</Link> can
          result in devastating, life-changing and fatal injuries. One way to
          decrease your chances of being involved in a pedestrian accident is to
          understand California laws and local laws relating to pedestrians.
        </p>
        <p>
          For example, pedestrians who have the right of way can hold at-fault
          drivers responsible for their injuries, damages and losses.
        </p>
        <p>
          In some cases, if a dangerous or defective roadway caused the
          accident, the governmental agency responsible for maintaining the
          roadway can also be held liable.
        </p>
        <p>
          An experienced Buena Park pedestrian accident lawyer can help injured
          victims and their families better understand their legal rights and
          options.
        </p>
        <p>
          California Vehicle Code section 21950 grants pedestrians the right of
          way when in a crosswalk or at an intersection. The law states: "The
          driver of a vehicle shall yield the right-of-way to a pedestrian
          crossing the roadway within any marked crosswalk or within any
          unmarked crosswalk at an intersection."
        </p>
        <p>
          The same section also states that the driver of a vehicle approaching
          a pedestrian within any marked or unmarked crosswalk "shall exercise
          all due care and shall reduce the speed of a vehicle or take any other
          action relating to the operating of the vehicle as necessary to
          safeguard the safety of the pedestrian."
        </p>
        <p>
          This means that motorists must yield the right of way to pedestrians
          who are legally crossing the road in a marked crosswalk or an unmarked
          crosswalk at an intersection. Drivers who fail to yield the right of
          way to pedestrians who are legally crossing the roadway may be cited
          or criminally charged. They can also be held financially responsible
          for the pedestrian's injuries and losses.
        </p>
        <p>
          There are many right-of-way laws that can affect the safety of nearby
          pedestrians. Under California Code 21663, car drivers must not operate
          a vehicle on the sidewalk. California Vehicle Code 22517 requires all
          motorists to look for pedestrians, bicyclists and motorists before
          opening their door. According to California Vehicle Code 22106,
          motorists may only back up when it is safe to do so.
        </p>
        <p>
          It is common for pedestrian accidents to occur as the result of
          someone stepping into traffic or crossing where there is no crosswalk.
          Car drivers are less likely to look out for pedestrians where
          pedestrians cannot legally cross the road. That does not, however,
          mean that drivers are excused from exercising due care where there is
          no crosswalk.
        </p>
        <p>
          California Vehicle Code 21954 requires all motorists to exercise due
          care around pedestrians. This law states: "Every pedestrian upon a
          roadway at any point other than within a marked crosswalk or within an
          unmarked crosswalk at an intersection shall yield the right-of-way to
          all vehicles upon the roadway so near as to constitute an immediate
          hazard." It same section also states: "The provisions of this section
          shall not relieve the driver of a vehicle from the duty to exercise
          due care for the safety of any pedestrian upon a roadway."
        </p>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
