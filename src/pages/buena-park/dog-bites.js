// -------------------------------------------------- //
// ---------------IMPORT MODULES BELOW--------------- //
// -------------------------------------------------- //
import React from "react"
import styled from "styled-components"
import { BreadCrumbs } from "src/components/elements/BreadCrumbs"
import { SEO } from "src/components/elements/SEO"
import { LayoutPAGEO } from "src/components/layouts/Layout_PAGEO"
import { Link } from "src/components/elements/Link"
import folderOptions from "./folder-options"
import { LazyLoad } from "src/components/elements/LazyLoad"

const customPageOptions = {
  pageSubject: "dog-bite"
}

const FolderOptions = {
  ...folderOptions,
  ...customPageOptions
}

export default function({ location }) {
  return (
    <LayoutPAGEO sidebar={true} {...FolderOptions}>
      <SEO
        pageSubject={FolderOptions.pageSubject}
        pageTitle="Buena Park Dog Bite Lawyers - Dog Attack Attorneys"
        pageDescription="Call 949-203-3814 for experienced Buena Park dog bite lawyers with a 96% success rate. Free legal case review with top rated animal attack lawyers who have recovered hundreds of millions."
      />
      <ContentWrapper>
        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE BELOW---------------------- */}
        {/* ------------------------------------------------------ */}
        <h1>Buena Park Dog Bite Attorneys</h1>
        <BreadCrumbs location={location} />
        <img
          src="/images/john_new.jpg"
          alt="John Bisnar, Dog Bite Attorney in Buena Park"
          className="imgright-fixed"
        />
        <p>
          <em>
            John Bisnar represents{" "}
            <Link to="/dog-bites">
              <strong>dog bite victims</strong>
            </Link>{" "}
            in Buena Park. He's been a top rated injury lawyer since 1978. Call
            949-203-3814 for immediate help or legal questions about your dog
            bite claim.
          </em>
        </p>
        <p>
          Dog bites can result in devastating injuries, disfigurement, serious
          emotional and psychological issues and major medical expenses.
        </p>
        <p>
          Victims of dog bite incidents often struggle with receiving fair
          compensation for their substantial losses. There may be several
          challenges victims face in the aftermath of a dog bite including
          locating the dog or the dog's owner.
        </p>
        <p>
          If you or a loved one has been injured in a Buena Park dog bite or dog
          attack, it is important that you know and understand your rights.
        </p>
        <h2>California Dog Bite Law</h2>
        <img
          src="/images/dog-bite-lawsuit.jpg"
          alt="buena park dog bite attorney"
          className="imgright-fixed"
        />
        <p>
          Under California Code 3342 (a): The owner of any dog is liable for the
          damages suffered by any person who is bitten by the dog while in a
          public place or lawfully in a private place, including the property of
          the owner of the dog, regardless of the former viciousness of the dog
          or the owner's knowledge of such viciousness.
        </p>

        <p>
          A person is lawfully upon the private property of such owner within
          the meaning of this section when he is on such property in the
          performance of any duty imposed upon him by the laws of this state or
          by the laws or postal regulations of the United States, or when he is
          on such property upon the invitation, express or implied, of the
          owner.
        </p>
        <p>
          This strict liability statute makes it clear that dog owners are
          responsible for the actions of their pets unless the victim was
          trespassing or otherwise involved in criminal activity at the time of
          the attack.
        </p>
        <p>
          It does not matter if the dog has a record of previous incidents or if
          this is the dog's first bite. Regardless of the dog's prior history,
          the owner can be held financially responsible for injuries and damages
          caused.
        </p>
        <h2>Police Dogs</h2>
        <p>
          There are different liability issues involved if the biting dog is a
          police dog. California Code 3342 (b) prevents victims of dog bite
          accidents from filing a claim against the government if the accident
          occurred in the apprehension or holding of a suspect where the
          employee has a reasonable suspicion of the suspect's involvement in
          criminal activity. As long as the dog was used during an investigation
          of a crime or possible crime; or in the execution of a warrant, it
          will not be possible to file a claim against the governmental agency
          responsible for the dog.
        </p>
        <h2>Liability Issues</h2>
        <p>
          There are a several facts that must be established in a dog bite case.
          Was the victim legally on the property where the accident occurred? Is
          it clear which dog caused the injuries? Who is the owner of the dog?
          Did the victim suffer a physical, financial or emotional loss?
        </p>
        <p>
          If the answer is yes to these questions, then, financial compensation
          may be available for the victim through civil litigation. This process
          can become more complicated if the victim is the dog owner's relative
          or if it is not clear to whom the dog belongs. Regardless of the
          circumstances, a knowledgeable dog bite attorney will be able to guide
          victims and their families through the process and help determine
          fault and liability in complex cases.
        </p>
        <h2>Damages Covered</h2>
        <p>
          Victims of dog bite injuries can pursue financial compensation for
          damages including but not limited to emergency room costs,
          hospitalization expenses, lost wages, cosmetic surgery and pain and
          suffering. Unfortunately, it is common for dog owners to deny
          responsibility for the accident and for insurance companies to deny
          valid claims. Dog bite claims are also often covered under homeowners
          insurance.
        </p>
        <p>
          If you or a loved one has been{" "}
          <strong>injured in a Buena Park dog bite</strong>, the experienced
          animal attack lawyers at Bisnar Chase can help you better understand
          your legal rights and options.{" "}
          <strong>Please call 949-203-3814</strong> or contact us for a free and
          comprehensive consultation.
        </p>

        {/* ------------------------------------------------------ */}
        {/* ----------------------CODE ABOVE---------------------- */}
        {/* ------------------------------------------------------ */}
      </ContentWrapper>
    </LayoutPAGEO>
  )
}

const ContentWrapper = styled("div")`
  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES BELOW---------- */
  /* ------------------------------------------------ */

  /* ------------------------------------------------ */
  /* ----------ADDITIONAL PAGE STYLES ABOVE---------- */
  /* ------------------------------------------------ */
`
