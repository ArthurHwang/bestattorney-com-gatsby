import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, shrink-to-fit=no"
        />

        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          This app works best with JavaScript enabled.
        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}

        {/* <div
          id="nGageLH"
          style={{
            visibility: "hidden",
            display: "block",
            padding: "0",
            position: "fixed",
            right: "0",
            bottom: "0px",
            zIndex: "5000"
          }}
        />

        <script
          dangerouslySetInnerHTML={{
            __html: `
                function loadLiveChat() {
                var element = document.createElement("script");
                element.src = "https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=154-2-128-85-73-160-224-153";
                document.body.appendChild(element)
              }

              if (!window.location.href.includes('admin')) {
                window.onload = () => {
                  setTimeout(() => {
                    loadLiveChat()
                  }, 5000)
                }
              }
            `
          }}
        /> */}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array
}
