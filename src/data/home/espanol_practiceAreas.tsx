interface PA {
  [key: string]: {
    name: string
    href: string
    desc: string
    alt: string
  }
}

export const PAData: PA = {
  0: {
    name: "Defectos Automovilísticos",
    href: "abogados/defectos-automovilisticos",
    alt: "Abogados de Defectos Automovilísticos",
    desc:
      "Car manufacturers often skimp on safety in order to save money, which can have disastrous consequences for the consumer."
  },
  1: {
    name: "Accidentes de Auto",
    href: "/abogados/accidentes-de-auto",
    alt: "Abogados de Defectos Automovilísticos",
    desc:
      "Car Accidents can happen to anyone and can result in injuries that don't show themselves immediately. Make sure you seek medical attention!"
  },

  2: {
    name: "Productos Defectuosos",
    href: "/abogados/productos-defectuosos",
    alt: "Abogados de Productos Defectuosos",
    desc:
      "Some products are dangerous but the manufacturer does not issue a recall or a warning., and can be held accountable for injuries that their products cause."
  },
  3: {
    name: "Mordedura de Perro",
    href: "/abogados/mordeduras-de-perro",
    alt: "Abogados de Mordedura de Perro",
    desc:
      "Dog bites are often the result of negligent owners, and are usually covered by homeowners or renters insurance."
  },
  4: {
    name: "Ley de Empleo",
    href: "/abogados",
    alt: "Abogados de Ley de Empleo",
    desc:
      "The state of California requires employers to pay overtime when necessary and to allow for 10 minute breaks and 30 minute meal breaks, as well as provide a safe work environment."
  },
  5: {
    name: "Acción de clase",
    href: "/abogados",
    alt: "Abogados de Acción de clase",
    desc:
      "Defective drugs and over-the-counter medications can cause life-changing side effects. Pharmaceutical companies often rush new drugs into the marketplace."
  },
  6: {
    name: "Responsabilidad del Local",
    href: "/abogados/responsibilidad-civil",
    alt: "Abogados de Responsabilidad del Local",
    desc:
      "When an owner knows about a dangerous situation but does not fix it, they can be held accountable for any injuries that happen as a result."
  },
  7: {
    name: "Accidentes de Camiones",
    href: "/abogados/accidentes-de-camiones",
    alt: "Abogados de Accidentes de Camiones",
    desc:
      "Very often, it is the occupants of the smaller passenger vehicle who end up suffering major injuries in a semi truck accident or other large commercial truck."
  },
  8: {
    name: "Homicidio por negligencia",
    href: "/abogados",
    alt: "Abogados de Homicidio por negligencia",
    desc:
      "A wrongful death can happen in any injury situation. Our job is to determine the fault or negligence of the guilty party and pursue compensation."
  }
}

export default {
  PAData
}
