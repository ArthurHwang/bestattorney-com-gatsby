import BradyThumb from "src/images/review-thumbnails/thumb-brady-m-min.png"
import EvaThumb from "src/images/review-thumbnails/thumb-eva-shaffer-min.png"
import VanessaThumb from "src/images/review-thumbnails/thumb-vanessa-sampson.png"

export const ReviewsData: Array<{
  id: number
  reviewText: string
  name: string
  reviewSource: string
  rating: number
  img: {
    src: string
    alt: string
  }
  href: string
}> = [
  {
    id: 1,
    reviewText:
      "After a bad accident... we contacted Bisnar Chase and they took care of everything. It gave us the peace of mind we needed to heal. I am very impressed with their level of professionalism and their compassion. We definitely recommend Bisnar Chase.",
    name: "Vanessa Sampson",
    reviewSource: "Google",
    rating: 5,
    img: { src: VanessaThumb, alt: "Client Ratings & Reviews" },
    href:
      "https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
  },
  {
    id: 2,
    reviewText:
      "I have used Bisnar and Chase twice now, both for car accidents. Each time I received a fair settlement and honestly received more than I expected. The entire team of Bisnar and Chase were professional, courteous and friendly. The paralegals there are top notch!",
    name: "Eva Shaffer",
    reviewSource: "Google",
    img: { src: EvaThumb, alt: "Client Ratings & Reviews" },
    rating: 5,
    href:
      "https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
  },
  {
    id: 3,
    reviewText:
      "John Bisnar was so professional. His staff was amazing and seemed to really care about me as a person. I couldn't be happier with the level of service I received. I was constantly updated on my case status and received far more from my settlement than I expected.",
    name: "Brady M.",
    reviewSource: "Yelp",
    img: { src: BradyThumb, alt: "Client Ratings & Reviews" },
    rating: 5,
    href:
      "https://www.google.com/search?q=bisnar+chase&rlz=1C1GCEU_enUS842US842&oq=bisnar+chase&aqs=chrome..69i57j69i61l2j69i59j35i39j0.1804j0j4&sourceid=chrome&ie=UTF-8#lrd=0x80dcde50b20b2deb:0xae2eee17ae4e213e,1,,,"
  }
]

export default {
  ReviewsData
}
