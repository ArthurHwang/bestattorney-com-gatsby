export const CaseResults: Array<{
  id: number
  amountWon: number
  type: string
  href: string
}> = [
  {
    id: 1,
    amountWon: 38650000,
    type: "Motorcycle Accident",
    href: "/attorneys/brian-chase"
  },
  {
    id: 2,
    amountWon: 30000000,
    type: "Motorcycle Accident",
    href: "/case-results/lucas-vogt"
  },
  {
    id: 3,
    amountWon: 24744764,
    type: "Auto Defect",
    href: "/case-results/jaklin-romine"
  },
  {
    id: 4,
    amountWon: 23091098,

    type: "Product Liability",
    href: "/case-results"
  },
  {
    id: 5,
    amountWon: 16444904,
    type: "Driver Negligence",
    href: "/case-results/christopher-chan"
  },
  {
    id: 6,
    amountWon: 16000000,
    type: "Auto Accident",
    href: "/case-results"
  },
  {
    id: 7,
    amountWon: 14443205,
    type: "Auto Defect",
    href: "/case-results"
  },
  {
    id: 8,
    amountWon: 11000000,
    type: "Family Negligence",
    href:
      "https://www.prlog.org/12768353-bisnar-chase-secures-11-million-jury-verdict-for-wife-and-children-of-man-who-died-by-suicide-while-in-rehab.html"
  },
  {
    id: 9,
    amountWon: 10030000,
    type: "Premises Negligence",
    href: "/case-results/olivier-maciel"
  },
  {
    id: 10,
    amountWon: 9800000,
    type: "Motor Vehicle Accident",
    href: "/case-results"
  },
  {
    id: 11,
    amountWon: 8500000,
    type: "Motor Vehicle Accident",
    href: "/case-results"
  },
  {
    id: 12,
    amountWon: 8250000,
    type: "Premises Liability",
    href: "/case-results"
  },
  {
    id: 13,
    amountWon: 7998073,
    type: "Product Liability",
    href: "/case-results"
  },
  {
    id: 14,
    amountWon: 5500000,
    type: "Defective Door Latch",
    href: "/case-results"
  },
  {
    id: 15,
    amountWon: 3000000,
    type: "Pedestrian Accident",
    href:
      "https://www.prlog.org/12484570-bisnar-chase-secures-3-million-jury-verdict-for-client-with-traumatic-brain-injury.html"
  },
  {
    id: 16,
    amountWon: 3000000,
    type: "Motorcycle Accident",
    href: "/case-results/rickey-prock"
  },
  {
    id: 17,
    amountWon: 3000000,
    type: "Trolley vs Bicycle Accident",
    href: "/case-results"
  },
  {
    id: 18,
    amountWon: 2815958,
    type: "Premises Liability",
    href: "/case-results"
  },
  {
    id: 19,
    amountWon: 2800000,
    type: "Premises Liability",
    href: "/case-results"
  },
  {
    id: 20,
    amountWon: 2500000,
    type: "Auto Defect",
    href: "/case-results"
  },
  {
    id: 21,
    amountWon: 2500000,
    type: "Auto Defect",
    href: "/case-results"
  },
  {
    id: 22,
    amountWon: 2000000,
    type: "Wage & Hour",
    href: "/case-results"
  },
  {
    id: 23,
    amountWon: 1925000,
    type: "Auto Defect",
    href: "/case-results"
  }
]

export default {
  CaseResults
}
