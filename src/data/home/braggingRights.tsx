interface UrlStructure {
  [image: string]: {
    alt: string
    href: string
  }
}

// ! These URL's are dynamically insterted below in the render method.  The object property name must match the graphql query name
export const URLS: UrlStructure = {
  asSeenOnImage: {
    href:
      "https://abc7.com/former-oc-officer-files-lawsuit-against-ford-over-co-leak-in-patrol-cars/2274376/",
    alt: "As Seen On NBC FOX CBS ABC"
  },
  consumerAttorneysImage: {
    href: "/blog/chase-ritsema-attorney-award",
    alt: "Consumer Attorneys of California"
  },
  fiveMillionImage: {
    href: "https://aaoaus.com/million-dollar-club/",
    alt: "Five Millon Dollar Club"
  },
  newsWeekImage: {
    href: "/about-us/lawyer-reviews-ratings",
    alt: "Newsweek Magazine"
  },
  timeImage: {
    href: "/about-us/lawyer-reviews-ratings",
    alt: "Time Magazine"
  },
  distinguishedImage: {
    href: "https://distinguishedjusticeadvocates.com/",
    alt: "Distinguished Justice Advocates"
  },
  trialAdvocatesImage: {
    href: "https://www.abota.org/index.cfm",
    alt: "American Board of Trial Advocates"
  },
  americasTopImage: {
    href: "https://www.top100highstakeslitigators.com/listing/brian-d-chase/",
    alt: "Americas Top 100 Personal Injury Attorneys"
  }
}

export default {
  URLS
}
