interface PA {
  [key: string]: {
    name: string
    href: string
    desc: string
    alt: string
  }
}

export const PAData: PA = {
  0: {
    name: "auto defects",
    href: "/auto-defects",
    alt: "auto defect lawyers",
    desc:
      "Car manufacturers often skimp on safety in order to save money, which can have disastrous consequences for the consumer."
  },
  1: {
    name: "car accidents",
    href: "/car-accidents",
    alt: "car accident lawyers",
    desc:
      "Car Accidents can happen to anyone and can result in injuries that don't show themselves immediately. Make sure you seek medical attention!"
  },

  2: {
    name: "defective products",
    href: "/defective-products",
    alt: "defective products lawyers",
    desc:
      "Some products are dangerous but the manufacturer does not issue a recall or a warning., and can be held accountable for injuries that their products cause."
  },
  3: {
    name: "dog bites",
    href: "/dog-bites",
    alt: "dog bites lawyers",
    desc:
      "Dog bites are often the result of negligent owners, and are usually covered by homeowners or renters insurance."
  },
  4: {
    name: "employment law",
    href: "/employment-law",
    alt: "employment law lawyers",
    desc:
      "The state of California requires employers to pay overtime when necessary and to allow for 10 minute breaks and 30 minute meal breaks, as well as provide a safe work environment."
  },
  5: {
    name: "pharmaceutical litigation",
    href: "/pharmaceutical-litigation",
    alt: "pharmaceutical litigation lawyers",
    desc:
      "Defective drugs and over-the-counter medications can cause life-changing side effects. Pharmaceutical companies often rush new drugs into the marketplace."
  },
  6: {
    name: "premises liability",
    href: "/premises-liability",
    alt: "premises liability lawyers",
    desc:
      "When an owner knows about a dangerous situation but does not fix it, they can be held accountable for any injuries that happen as a result."
  },
  7: {
    name: "truck accidents",
    href: "/truck-accidents",
    alt: "truck accidents lawyers",
    desc:
      "Very often, it is the occupants of the smaller passenger vehicle who end up suffering major injuries in a semi truck accident or other large commercial truck."
  },
  8: {
    name: "wrongful death",
    href: "/wrongful-death",
    alt: "wrongful death lawyers",
    desc:
      "A wrongful death can happen in any injury situation. Our job is to determine the fault or negligence of the guilty party and pursue compensation."
  }
}

export default {
  PAData
}
