import BrianChase from "src/images/nav/brian-chase-headshot.jpg"
import GavinLong from "src/images/nav/gavin-long-headshot.jpg"
import IanSilvers from "src/images/nav/ian-silvers-headshot.jpg"
import JerusalemBeligan from "src/images/nav/jerusalem-beligan-headshot.jpg"
import JohnBisnar from "src/images/nav/john-bisnar-headshot.png"
import JordanSouthwick from "src/images/nav/jordan-southwick-headshot.jpg"
import KristBiakanja from "src/images/nav/krist-biakanja-headshot.jpg"
import ScottRitsema from "src/images/nav/scott-ritsema-headshot.jpg"
import ShannonBarker from "src/images/nav/shannon-barker-headshot.jpg"
import StevenHilst from "src/images/nav/steven-hilst-headshot.jpg"
import TomAntunovich from "src/images/nav/tom-antunovich-headshot.jpg"

export const attorneysData = [
  {
    name: "John Bisnar",
    position: "Founding Partner",
    img: `${JohnBisnar}`
  },
  {
    name: "Brian Chase",
    position: "Senior Partner",
    img: `${BrianChase}`
  },
  {
    name: "Scott Ritsema",
    position: "Partner",
    img: `${ScottRitsema}`
  },
  {
    name: "Shannon Barker",
    position: "Administrator",
    img: `${ShannonBarker}`
  },
  {
    name: "Jerusalem Beligan",
    position: "Lawyer",
    img: `${JerusalemBeligan}`
  },
  {
    name: "H Gavin Long",
    position: "Lawyer",
    img: `${GavinLong}`
  },
  {
    name: "Steven Hilst",
    position: "Lawyer",
    img: `${StevenHilst}`
  },
  {
    name: "Tom Antunovich",
    position: "Administrator",
    img: `${TomAntunovich}`
  },
  {
    name: "Ian Silvers",
    position: "Lawyer",
    img: `${IanSilvers}`
  },
  {
    name: "Krist Biakanja",
    position: "Lawyer",
    img: `${KristBiakanja}`
  },
  {
    name: "Jordan Southwick",
    position: "Lawyer",
    img: `${JordanSouthwick}`
  }
]

export default {
  attorneysData
}
