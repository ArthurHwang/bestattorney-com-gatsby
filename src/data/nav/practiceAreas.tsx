import AutoDefects from "src/images/nav/practice-areas/thumb-pa-auto-defects.jpg"
import BicycleAccident from "src/images/nav/practice-areas/thumb-pa-bicycle-accident.jpg"
import BrainInjury from "src/images/nav/practice-areas/thumb-pa-brain-injury.jpg"
import CarAccident from "src/images/nav/practice-areas/thumb-pa-car-accidents.jpg"
import MedicalDevices from "src/images/nav/practice-areas/thumb-pa-defective-medical-product.jpg"
import DefectiveProducts from "src/images/nav/practice-areas/thumb-pa-defective-product.jpg"
import DogBite from "src/images/nav/practice-areas/thumb-pa-dog-bite.jpg"
import PremisesLiability from "src/images/nav/practice-areas/thumb-pa-premises-liability.jpg"
import Pharmaceutical from "src/images/nav/practice-areas/thumb-pa-product-liability.jpg"
import TruckAccident from "src/images/nav/practice-areas/thumb-pa-truck-accident.jpg"
import WrongfulDeath from "src/images/nav/practice-areas/thumb-pa-wrongful-death.png"

export const practiceAreasData = [
  {
    name: "Car Accidents",
    img: `${CarAccident}`
  },
  {
    name: "Truck Accidents",
    img: `${TruckAccident}`
  },
  {
    name: "Bicycle Accidents",
    img: `${BicycleAccident}`
  },
  {
    name: "Auto Defects",
    img: `${AutoDefects}`
  },
  {
    name: "Dog Bites",
    img: `${DogBite}`
  },
  {
    name: "Premises Liability",
    img: `${PremisesLiability}`
  },
  {
    name: "Wrongful Death",
    img: `${WrongfulDeath}`
  },
  {
    name: "Pharmaceutical",
    img: `${Pharmaceutical}`
  },
  {
    name: "Medical Devices",
    img: `${MedicalDevices}`
  },
  {
    name: "Defective Products",
    img: `${DefectiveProducts}`
  },
  {
    name: "Brain Injury",
    img: `${BrainInjury}`
  }
]

export default {
  practiceAreasData
}
