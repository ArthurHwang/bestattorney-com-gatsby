export const serviceAreas: any = {
  OC: {
    lat: 33.787914,
    lng: -117.853104,
    areas: [
      {
        area: "Newport Beach",
        lat: 33.608768,
        lng: -117.87336,
        link: "/newport-beach"
      },
      {
        area: "Irvine",
        lat: 33.684566,
        lng: -117.826508,
        link: "/irvine"
      },
      {
        area: "Costa Mesa",
        lat: 33.641216,
        lng: -117.918823,
        link: "/costa-mesa"
      },
      {
        area: "Santa Ana",
        lat: 33.745472,
        lng: -117.867653,
        link: "/santa-ana"
      },
      {
        area: "Huntington Beach",
        lat: 33.659771,
        lng: -117.999313,
        link: "/huntington-beach"
      },
      {
        area: "Lake Forest",
        lat: 33.667461,
        lng: -117.688499,
        link: "/lake-forest"
      },
      {
        area: "Anaheim",
        lat: 33.836594,
        lng: -117.914299,
        link: "/anaheim"
      },
      {
        area: "Laguna Niguel",
        lat: 33.523674,
        lng: -117.714943,
        link: "/laguna-niguel"
      },
      {
        area: "Mission Viejo",
        lat: 33.59502,
        lng: -117.659103,
        link: "/mission-viejo"
      },
      {
        area: "Placentia",
        lat: 33.876389,
        lng: -117.85907,
        link: "/orange-county/placentia-personal-injury-attorneys"
      },
      {
        area: "Fullerton",
        lat: 33.87035,
        lng: -117.924301,
        link: "/fullerton"
      },
      {
        area: "Garden Grove",
        lat: 33.774269,
        lng: -117.937996,
        link: "/garden-grove"
      },
      {
        area: "Cypress",
        lat: 33.824249,
        lng: -118.045692,
        link: "/cypress"
      },
      {
        area: "Buena Park",
        lat: 33.867405,
        lng: -117.998138,
        link: "/buena-park"
      },
      {
        area: "Fountain Valley",
        lat: 33.710739,
        lng: -117.945732,
        link: "/fountain-valley"
      },
      {
        area: "Dana Point",
        lat: 33.467224,
        lng: -117.698097,
        link: "/locations/dana-point-car-accidents"
      },
      {
        area: "San Clemente",
        lat: 33.427353,
        lng: -117.612602,
        link: "/locations/san-clemente-car-accidents"
      },
      {
        area: "Tustin",
        lat: 33.742,
        lng: -117.8236,
        link: "/tustin"
      },
      {
        area: "Westminster",
        lat: 33.7592,
        lng: -117.9897,
        link: "/westminster"
      },
      {
        area: "Yorba Linda",
        lat: 33.8885,
        lng: -117.8133,
        link: "/yorba-linda"
      }
    ]
  },
  LA: {
    lat: 34.052235,
    lng: -118.243683,
    areas: [
      {
        area: "Torrance",
        lat: 33.836319,
        lng: -118.340042,
        link: "/locations/torrance-car-accidents"
      },
      {
        area: "Inglewood",
        lat: 33.956463,
        lng: -118.34438,
        link: "/los-angeles/inglewood-personal-injury-lawyers"
      },
      {
        area: "South Bay",
        lat: 33.8798,
        lng: -118.3813,
        link: "/los-angeles/south-bay-personal-injury-lawyers"
      },
      {
        area: "Glendale",
        lat: 34.142509,
        lng: -118.255074,
        link: "/locations/glendale-personal-injury"
      },
      {
        area: "Long Beach",
        lat: 33.77005,
        lng: -118.193741,
        link: "/long-beach"
      },
      {
        area: "Pasadena",
        lat: 34.147785,
        lng: -118.144516,
        link: "/los-angeles/pasadena-personal-injury-attorneys"
      },
      {
        area: "Culver City",
        lat: 34.021122,
        lng: -118.396469,
        link: "/los-angeles/culver-city-personal-injury-attorneys"
      },
      {
        area: "Santa Monica",
        lat: 34.019455,
        lng: -118.491188,
        link: "/locations/santa-monica-personal-injury"
      },
      {
        area: "Hollywood",
        lat: 34.092808,
        lng: -118.328659,
        link: "/locations/hollywood-personal-injury"
      },
      {
        area: "San Fernando Valley",
        lat: 34.282982,
        lng: -118.441742,
        link: "/locations/san-fernando-valley-personal-injury"
      },
      {
        area: "Compton",
        lat: 33.895847,
        lng: -118.22007,
        link: "/locations/compton-car-accidents"
      },
      {
        area: "Covina",
        lat: 34.08625,
        lng: -117.890137,
        link: "/locations/covina-car-accidents"
      },
      {
        area: "West Covina",
        lat: 34.082729,
        lng: -117.93148,
        link: "/locations/west-covina-car-accidents"
      },
      {
        area: "Pomona",
        lat: 34.054723,
        lng: -117.752099,
        link: "/los-angeles/pomona-personal-injury-lawyers"
      },
      {
        area: "East Los Angeles",
        lat: 34.022569,
        lng: -118.168566,
        link: "/los-angeles/east-los-angeles-personal-injury-lawyers"
      },
      {
        area: "Alhambra",
        lat: 34.096468,
        lng: -118.126545,
        link: "/locations/alhambra-car-accidents"
      },
      {
        area: "Monterey Park",
        lat: 34.06378,
        lng: -118.121526,
        link: "/locations/monterey-park-personal-injury"
      },
      {
        area: "Cerritos",
        lat: 33.858582,
        lng: -118.062808,
        link: "/locations/cerritos-car-accidents"
      },
      {
        area: "Downey",
        lat: 33.941342,
        lng: -118.13351,
        link: "/locations/downey-car-accidents"
      },
      {
        area: "El Monte",
        lat: 34.068599,
        lng: -118.030905,
        link: "/locations/el-monte-car-accidents"
      },
      {
        area: "Hawthorne",
        lat: 33.915857,
        lng: -118.352074,
        link: "/locations/hawthorne-car-accidents"
      },
      {
        area: "La Mirada",
        lat: 33.918027,
        lng: -118.01155,
        link: "/locations/la-mirada-personal-injury"
      },
      {
        area: "South Gate",
        lat: 33.948367,
        lng: -118.193808,
        link: "/locations/south-gate-car-accidents"
      },
      {
        area: "Sherman Oaks",
        lat: 34.149374,
        lng: -118.454843,
        link: "/locations/sherman-oaks-personal-injury"
      },
      {
        area: "Woodland Hills",
        lat: 34.16798,
        lng: -118.608917,
        link: "/locations/woodland-hills-car-accidents"
      },
      {
        area: "Santa Clarita",
        lat: 34.387924,
        lng: -118.528994,
        link: "/los-angeles/santa-clarita-personal-injury-lawyers"
      },
      {
        area: "Lancaster",
        lat: 34.688046,
        lng: -118.168913,
        link: "/los-angeles/lancaster-personal-injury-lawyers"
      },
      {
        area: "Simi Valley",
        lat: 34.271311,
        lng: -118.781963,
        link: "/locations/simi-valley-personal-injury"
      },
      {
        area: "Thousand Oaks",
        lat: 34.172545,
        lng: -118.830196,
        link: "/locations/thousand-oaks-car-accidents"
      },
      {
        area: "Ventura",
        lat: 34.280228,
        lng: -119.295509,
        link: "/locations/ventura-personal-injury"
      }
    ]
  },
  IE: {
    lat: 33.886013,
    lng: -117.833919,
    areas: [
      {
        area: "San Bernardino",
        lat: 34.108345,
        lng: -117.289764,
        link: "/san-bernardino"
      },
      {
        area: "Riverside",
        lat: 33.980602,
        lng: -117.375496,
        link: "/riverside"
      },
      {
        area: "Corona",
        lat: 33.875137,
        lng: -117.569134,
        link: "/locations/corona-car-accidents"
      },
      {
        area: "Ontario",
        lat: 34.060074,
        lng: -117.652661,
        link: "/locations/ontario-personal-injury"
      },
      {
        area: "Chino",
        lat: 34.012192,
        lng: -117.684481,
        link: "/locations/chino-car-accidents"
      },
      {
        area: "Chino Hills",
        lat: 33.988298,
        lng: -117.735724,
        link: "/locations/chino-hills-car-accidents"
      },
      {
        area: "Moreno Valley",
        lat: 33.941048,
        lng: -117.23292,
        link: "/locations/moreno-valley-car-accidents"
      },
      {
        area: "Rancho Cucamonga",
        lat: 34.102229,
        lng: -117.593574,
        link: "/locations/rancho-cucamonga-car-accidents"
      },
      {
        area: "Rialto",
        lat: 34.105057,
        lng: -117.372217,
        link: "/locations/rialto-car-accidents"
      },
      {
        area: "Fontana",
        lat: 34.09082,
        lng: -117.437724,
        link: "/locations/fontana-car-accidents"
      },
      {
        area: "Temecula",
        lat: 33.49304,
        lng: -117.14491,
        link: "/locations/temecula-personal-injury"
      },
      {
        area: "Hesperia",
        lat: 34.423334,
        lng: -117.300811,
        link: "/locations/hesperia-car-accidents"
      },
      {
        area: "Victorville",
        lat: 34.534402,
        lng: -117.307184,
        link: "/locations/victorville-car-accidents"
      }
    ]
  },
  SD: {
    lat: 33.123375,
    lng: -117.254865,
    areas: [
      {
        area: "Carlsbad",
        lat: 33.158092,
        lng: -117.350594,
        link: "/locations/carlsbad-car-accidents"
      },
      {
        area: "Escondido",
        lat: 33.13036,
        lng: -117.085358,
        link: "/locations/escondido-car-accidents"
      },
      {
        area: "San Marcos",
        lat: 33.145616,
        lng: -117.166522,
        link: "/locations/san-marcos-car-accidents"
      },
      {
        area: "Vista",
        lat: 33.20105,
        lng: -117.241831,
        link: "/locations/vista-car-accidents"
      },
      {
        area: "Chula Vista",
        lat: 32.642647,
        lng: -117.079457,
        link: "/locations/chula-vista-personal-injury"
      },
      {
        area: "El Cajon",
        lat: 32.794359,
        lng: -116.96352,
        link: "/locations/el-cajon-personal-injury"
      },
      {
        area: "Rancho Bernardo",
        lat: 33.033248,
        lng: -117.079535,
        link: "/locations/rancho-bernardo-car-accidents"
      }
    ]
  },
  NC: {
    lat: 37.491623,
    lng: -122.094881,
    areas: [
      {
        area: "San Francisco",
        lat: 37.774929,
        lng: -122.419418,
        link: "/san-francisco"
      },
      {
        area: "Berkeley",
        lat: 37.871593,
        lng: -122.272743,
        link: "/locations/berkeley-car-accidents"
      },
      {
        area: "Oakland",
        lat: 37.804733,
        lng: -122.270264,
        link: "/locations/oakland-personal-injury"
      },
      {
        area: "San Mateo",
        lat: 37.563033,
        lng: -122.327642,
        link: "/locations/san-mateo-car-accidents"
      },
      {
        area: "Santa Rosa",
        lat: 38.438616,
        lng: -122.708346,
        link: "/locations/santa-rosa-personal-injury"
      },
      {
        area: "Sonoma",
        lat: 38.289873,
        lng: -122.460945,
        link: "/locations/sonoma-personal-injury"
      },
      {
        area: "Sacramento",
        lat: 38.579672,
        lng: -121.485464,
        link: "/locations/sacramento-personal-injury"
      },
      {
        area: "Stockton",
        lat: 37.954987,
        lng: -121.296419,
        link: "/locations/stockton-personal-injury"
      },
      {
        area: "Stanislaus County",
        lat: 37.599478,
        lng: -121.025015,
        link: "/locations/stanislaus-county-personal-injury"
      },
      {
        area: "Modesto",
        lat: 37.636818,
        lng: -120.996236,
        link: "/locations/modesto-personal-injury"
      },
      {
        area: "Fremont",
        lat: 37.544927,
        lng: -121.980194,
        link: "/locations/fremont-personal-injury"
      },
      {
        area: "Los Altos",
        lat: 37.371887,
        lng: -122.100163,
        link: "/locations/los-altos-car-accidents"
      },
      {
        area: "Sunnyvale",
        lat: 37.368151,
        lng: -122.038218,
        link: "/locations/sunnyvale-car-accidents"
      },
      {
        area: "Salinas",
        lat: 36.678341,
        lng: -121.654252,
        link: "/locations/salinas-personal-injury"
      },
      {
        area: "Monterey County",
        lat: 36.274867,
        lng: -121.348194,
        link: "/locations/monterey-county-personal-injury"
      },
      {
        area: "Fresno",
        lat: 36.734219,
        lng: -119.790536,
        link: "/fresno"
      }
    ]
  }
}
