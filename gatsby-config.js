require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
})

module.exports = {
  siteMetadata: {
    siteTitle: `California Personal Injury Attorneys - Bisnar Chase`,
    siteDescription: `Injured? Contact the top rated California Personal Injury Attorneys of Bisnar Chase. Superior client representation, over $500M recovered and we advance all legal costs until we win your case. If we don’t win, you don’t pay. Voted top 1% of Personal Injury Lawyers in the nation. Free consultation.`,
    author: `Arthur Hwang <ahwang55@gmail.com>`,
    siteUrl: `https://bestatto-gatsby.netlify.app`,
    social: {
      fbAppId: `22493476353157655`,
      twitter: `bisnarchasee`
    }
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    "gatsby-plugin-root-import",
    `gatsby-image`,
    `gatsby-plugin-catch-links`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-typescript`,
    "gatsby-plugin-typescript-checker",
    `gatsby-plugin-advanced-sitemap`,
    `gatsby-plugin-remove-fingerprints`,
    `gatsby-plugin-remove-trailing-slashes`,
    `gatsby-plugin-no-sourcemaps`, // reduce upload batch size
    "gatsby-plugin-netlify-cache",
    {
      resolve: `gatsby-plugin-disqus`,
      options: {
        shortname: `bestattorney`
      }
    },
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        headers: {},
        allPageHeaders: [],
        mergeSecurityHeaders: true,
        mergeLinkHeaders: true,
        mergeCachingHeaders: true,
        transformHeaders: (headers, path) => headers,
        generateMatchPathRewrites: true
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-148084848-1",
        head: true
      }
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        policy: [{ userAgent: "*", allow: "/" }]
      }
    },
    {
      resolve: "gatsby-plugin-exclude",
      options: { paths: ["/**/folder-options", "!/app/demo/*"] }
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
        stripMetadata: true,
        defaultQuality: 90,
        failOnError: false
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`
      }
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/components/utilities/typography`
      }
    },
    {
      resolve: `gatsby-source-wordpress-experimental`,
      options: {
        url: `https://www.bestattorney.com/blog/graphql`,
        verbose: true
      }
    }
  ]
}

// {
//   resolve: "gatsby-source-wordpress",
//   options: {
//     baseUrl: "www.bestatto-gatsby.netlify.app/blog",
//     minimizeDeprecationNotice: true,
//     protocol: "https",
//     hostingWPCOM: false,
//     useACF: false,
//     verboseOutput: false,
//     perPage: 100,
//     concurrentRequests: 100,
//     includedRoutes: [
//       "**/categories",
//       "**/posts",
//       "**/pages",
//       "**/media",
//       "**/tags",
//       "**/taxonomies",
//       "**/users"
//     ]
//   }
// }
// {
//   resolve: `gatsby-plugin-amp`,
//   options: {
//     canonicalBaseUrl: "https://bestatto-gatsby.netlify.app/",
//     components: ["amp-sidebar"],
//     excludedPaths: ["/404*", "/"],
//     pathIdentifier: "/amp",
//     relAmpHtmlPattern: "{{canonicalBaseUrl}}{{pathname}}{{pathIdentifier}}",
//     useAmpClientIdApi: true
//   }
// },
