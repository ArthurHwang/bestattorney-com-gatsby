declare module "src/*"
declare module "utilities/*"
declare module "lodash/debounce"
declare module "react-lazyload"
declare module "react-lazy-load-image-component"
declare module "gatsby-plugin-disqus"
declare namespace JSX {
  interface IntrinsicElements {
    "amp-img": any
    "amp-sidebar": any
    Img: any
    button: any
  }
}
